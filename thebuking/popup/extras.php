<?php include('../../conf/baseurl.php'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo $base_url; ?>/scripts/bootstrap-switch/bootstrap-switch.css">
<style>
    .popup_roomtype{font: normal 12px "Trebuchet MS","PTSansRegular", Calibri, Arial,Helvetica,sans-serif;	color:#333;}
    .popup_roomtype span.title{ font-weight:bold; display:block; padding-top:10px;}
    .popup_roomtype .popup-img{	display:inline-block; padding:10px; vertical-align:top;}
    .popup_roomtype .popup-img img{	width:330px; height:180px; padding:2px;	border:2px solid #63BE8C;}
    .popup_roomtype .popup-desc{ display:inline-block; max-width:400px; padding:10px; vertical-align:top;}
    .popup_roomtype ul.inline-block { display: inline-block; margin:0;}
    .popup_roomtype ul.inline-block li { padding:2px; display:block; }
    .popup_roomtype ul.inline-block li:before {	content:&bull;}
    .popup_roomtype h2 { padding: 0 10px;}

    .popup_roomtype td{vertical-align: top;width:50%;}

    .popup_roomtype .popup-img .gallery{border-radius: 10px; overflow: hidden;}
    .popup_roomtype .popup-img .gallery .carousel .item{
        min-height: 180px; /* Prevent carousel from being distorted if for some reason image doesn't load */
    }
    .popup_roomtype .popup-img .gallery .carousel .item img{
        margin: 0 auto; /* Align slide image horizontally center */
        padding:0;	border:0; 
    }
</style>
<?php
	
	include('../../conf/connection_withpdo.php'); 
	
	$s_room = "select * from categoryextras where publishedoid = 1";
	$stmt = $db->query($s_room);
    $room = $stmt->fetchAll(PDO::FETCH_ASSOC);
foreach ($room as $extras) {
        
?>
<div id="white-popup-block" class="mfp-hide popup_roomtype">
    
    <table>
        <div id="div-list-hotel">
            <ul class="content-list" id="list-hotel">
        <li id="h-<?=$hotel['hoteloid']?>" class="card-v3 card-rounded white-box border-box">
                            <div class="col-md-3 col-sm-3 col-xs-4 img-link-wrapper">
                                <a href="<?=$href_hotel;?>" class="img-link">
                                    <div class="thumbnailz"><img src="<?=$hotel['hotelpicture'];?>"></div>
                                </a>
                            </div>
                            <div class="col-md-9 col-sm-9 col-xs-8 card-content-wrapper">
                                <div class="row">
                                    <div class="col-md-9 top card-placeholder-1">
                                        <a href="<?=$href_hotel;?>" class="primary-theme">
                                            <div class="title"><?php echo $extras['categoryname']; ?></div>
                                        </a>
                                         
                                        <div class="hidden-xs">
                                            <span class="small-desc"><?php echo $extras['description']; ?></span>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-3 card-placeholder-2 top">
                                        <div class="price hidden-xs">
                                            start from 
                                            <h5 class="strikethrough-rate"><span class="strikethrough light-grey">
                                                <!-- <?php if($hotel['netrate'] > $hotel['minrate']){ echo $hotel['currencycode']."&nbsp;". $hotel['shownetrate']."<br>";  }?> -->
                                                100,000
                                            </span></h5>
                                            <h3 class="rate">
                                                <span class="grey">IDR<!-- <?=$hotel['currencycode'];?> --></span>
                                                <span class="blue">300,000<!-- <?=$hotel['hotel_minrate_final'];?> --></span>
                                            </h3>
                                            <div class="clear wrapper-dates">
            <div class="col-md-12">
                
                
            <div class="col-md-12" id="availability-hotel"  style="display: none;">
                <form action="<?=$base_url; ?>/search/hotel/" method="get">
    
            <div class="inline-block top">
                <input type="text" name="checkin" id="checkin" class="calinput hasDatePicker"  readonly value="<?php echo $default_checkin; ?>">
            </div>
    
    
                </form>
            </div>
        </div>
                                    <div class="clear"></div>
                                </div>

                                <div id="listroom-<?=$hotel['hoteloid'];?>" class="listroom row" style="display:none;">
                                    <ul>
                                        <?php
                                            $qx = 1;
                                            foreach($hotel['rooms'][0] as $key => $name){
                                                //foreach($hotel['rooms'] as $key => $name){
                                                echo "<li><a href='".$href_hotel."'>";
                                                //echo "<span class='tosca-bold fl_right'>".$hotel['rates'][$key]."</span>";
                                                echo "<span class='gray-bold fl_right'>".$hotel['rates'][0][$key]."</span>";
                                                echo "<span class='gray-bold'>".$name."</span>";
                                                echo "</a></li>";
                                                if($qx == 5) break;
                                                $qx++;
                                            }
                                        ?>
                                    </ul>
                                </div>

                            </div>
                            <div class="clear"></div>
                    </li>
                </ul>
                </div>
    </table>
</div>
<?php } echo $base_url; ?>

<script  type="text/javascript" language="javascript" src="<?=$base_url;?>/scripts/bootstrap-switch/bootstrap-switch.js"></script>
