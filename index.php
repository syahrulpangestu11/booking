<?php
	session_start();
	error_reporting(E_ALL ^ E_NOTICE);
	// error_reporting(0);//(E_ALL ^ E_NOTICE);
	ini_set('display_errors', 1);
	// ini_set('display_errors', 0);
	// ini_set('log_errors', 1);
	// include('conf/baseurl.php');
	include('conf/connection.php');
	include("includes/function.star.php");
	include("includes/function.mini-thousand.php");
	$fileVersion = date("Y.m.d.H.i.s");
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
	<?php include('component/meta.php'); ?>
    <title><?=$meta_title;?></title>
    <meta charset="iso-8859-1">
    <meta name="description" content="<?=$meta_description;?>">
    <meta name="keywords" content="<?=$meta_keywords;?>">
    <meta name="robots" content="<?=$meta_robots;?>">
	<!-- <meta name="Viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" /> -->
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">

	<link rel="shortcut icon" href="<?php echo $_profile['imgfavicon'];?>">

	<link rel="stylesheet" href="<?=$base_url;?>/styles/layout_cd.css?v=<?=$fileVersion;?>" type="text/css">
	<link rel="stylesheet" href="<?=$base_url;?>/styles/layout.css?v=<?=$fileVersion;?>" type="text/css">
	<link rel="stylesheet" href="<?=$base_url;?>/styles/flags.css" type="text/css">
	<link rel="stylesheet" type="text/css" href="<?php echo $base_url; ?>/scripts/fontawesome-free-5.5.0-web/css/all.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $base_url; ?>/scripts/bootstrap/css/bootstrap.min.css">
	<!-- <link rel="stylesheet" href="<?=$base_url;?>/styles/bukingo/bootstrap-reset.css" type="text/css"> -->
	<link rel="stylesheet" href="<?=$base_url;?>/styles/bukingo/bukingo-style.css?v=<?=$fileVersion;?>" type="text/css">
    <!-- Javascripts -->
	<script type="text/javascript" language="javascript" src="<?=$base_url;?>/scripts/jquery-1.12.4.min.js"></script>

	<!-- jQuery Tabs -->
	<script type="text/javascript" language="javascript" src="<?=$base_url;?>/scripts/jquery-tabs.js"></script>
	<!-- Gmaps -->
	<script src="https://maps.google.com/maps/api/js?sensor=true&.js"></script>
    <script src="<?=$base_url;?>/scripts/gmaps.js"></script>
	<!-- Show more / read more -->
	<script type="text/javascript" language="javascript" src="<?=$base_url;?>/scripts/showmore.js"></script>
	<script type="text/javascript" language="javascript" src="<?=$base_url;?>/scripts/bootstrap/js/bootstrap.min.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Montserrat:200,400,500,700|Nunito:400" rel="stylesheet">


	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.2/css/swiper.min.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.2/js/swiper.min.js"></script>
	<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
</head>
<body>
	<div id="mainWrapper" class="container-fluid">
    	<?php include('component/header.php'); ?>

<?php if($uri2 != "" and isset($uri2) and !empty($uri2) and $uri2 != "index.php"){?>
<div class="test_info">
	<p>Sorry this website is only for testing. All reservation will not processed.</p>
</div>
<?php }?>
<!-- content -->
<div class="wrapper" id="">
    <!-- content body -->

    <!--
    <img src="images/booking.png"/>
   	<section id="shout">
      <p>Vestibulumaccumsan egestibulum eu justo convallis augue estas aenean elit intesque sed. Facilispede estibulum nulla orna nisl velit elit ac aliquat non tincidunt. Namjusto cras urna urnaretra lor urna neque sed quis orci nulla laoremut vitae doloreet condimentumst.</p>
    </section>
    -->

    <!-- main content -->
    <div id="content" style="min-height:500px">
		<?php include("includes/case.php"); ?>
    </div>
    <!-- / content body -->

</div>


<!-- Footer  id="footer"-->
<div class="wrapper footer-wrapper" id="wrapper-links">
	<footer class="container clear">
    	<?php include('component/footer.php'); ?>
	</footer>
</div>
<div class="wrapper footer-wrapper" id="wrapper-copyright">
	<footer class="container clear">
		<div class="text-center">
			<p>&copy; 2018 Travel and Trip. All rights reserved.</p>
		</div>
	</footer>
</div>
<?php
	include('component/loadingpage.php');

	include("includes/js_custom.php");
	include("includes/js_thumb.php");
?>
	</div>
</body>
</html>
