$(document).ready(function() {
	
	var ellipsestext = " ...";
	var moretext = "show more";// &raquo;
	var lesstext = "show less";//&laquo;  
	
	var showChar = 200
	$('.readmore').each(function() {
		var content = $(this).html();
		var content_text = $(this).text();
		
		if(content.length > showChar) {

			var content_less = content_text.substr(0, showChar);
			var content_more = content;
			
			var html = 
				'<div class="less-text">' + content_less + ellipsestext + '</div>' +
				'<div class="more-text" style="display:none;">' + content_more + '</div>' +
				'<div class="wrapper-toggle-readmore">' +
					'<a href="javascript:void(0);" class="toggle-readmore button expand">' + moretext + '</a>' +
				'</div>';

			$(this).html(html);
		}
		
	});
	
	var showChar2 = 75;
	$('.readmore-less').each(function() {
		var content = $(this).html();
		var content_text = $(this).text();

		if(content.length > showChar2) {

			var content_less = content_text.substr(0, showChar);
			var content_more = content;
			
			var html = 
				'<div class="less-text">' + content_less + ellipsestext + '</div>' +
				'<div class="more-text" style="display:none;">' + content_more + '</div>' +
				'<div class="wrapper-toggle-readmore">' +
					'<a href="javascript:void(0);" class="toggle-readmore button expand">' + moretext + '</a>' +
				'</div>';

			$(this).html(html);
		}

	});
	
	var showChar3 = 50;
	$('.readmore-least').each(function() {
		var content = $(this).html();
		var content_text = $(this).text();

		if(content.length > showChar3) {

			var content_less = content_text.substr(0, showChar);
			var content_more = content;
			
			var html = 
				'<div class="less-text">' + content_less + ellipsestext + '</div>' +
				'<div class="more-text" style="display:none;">' + content_more + '</div>' +
				'<div class="wrapper-toggle-readmore">' +
					'<a href="javascript:void(0);" class="toggle-readmore button expand">' + moretext + '</a>' +
				'</div>';

			$(this).html(html);
		}

	});


	/*
	$(".morelink").click(function(){
		if($(this).hasClass("less")) {
			$(this).removeClass("less");
			$(this).html(moretext);
		} else {
			$(this).addClass("less");
			$(this).html(lesstext);
		}
		$(this).parent().prev().toggle();
		$(this).prev().toggle();
		return false;
	});
	*/
	
	
	$('.toggle-readmore').click(function(e){
		//get collapse content selector
		var collapse_more = $(this).parent().parent().children(".more-text");
		var collapse_less = $(this).parent().parent().children(".less-text");					
		
		//make the collapse content to be shown or hide
		var toggle_switch = $(this);
		
		$(collapse_more).toggle('slow',function(){
		  if($(this).css('display')=='none'){
			//--- change the button label to be 'Show'
			//toggle_switch.css({"background": "#D31F2A"});
			toggle_switch.html(moretext);
			toggle_switch.addClass("expand");
			toggle_switch.removeClass("collapse");
		  }else{
			//--- change the button label to be 'Hide'
			//toggle_switch.css({"background": "#555"});
			toggle_switch.html(lesstext);
			toggle_switch.addClass("collapse");
			toggle_switch.removeClass("expand");
		  }
		});
		
		$(collapse_less).toggle('slow'); e.stopPropagation();
	});
	
	
	
	
});
// JavaScript Document