<?php
  $channel = 1;
  $roomoid = $_GET['roomid'];
  $q_roomtype = "select r.roomoid, r.name, r.publishedoid, h.hoteloid, h.hotelcode, h.hotelname, x.roomofferoid from room r inner join published p using (publishedoid) left join (select ro.roomofferoid, ro.name, r.roomoid from roomoffer ro inner join published p using (publishedoid) inner join room r using (roomoid) inner join offertype using (offertypeoid) where r.roomoid = '".$roomoid."' and p.publishedoid not in ('3') order by ro.name limit 1) as x on x.roomoid = r.roomoid inner join hotel h using (hoteloid) where r.roomoid = '".$roomoid."' and p.publishedoid not in (3)";
  $stmt = $db->query($q_roomtype);
  $roomtype = $stmt->fetch(PDO::FETCH_ASSOC);

  $startdate	= (isset($_REQUEST['startdate']) and !empty($_REQUEST['startdate']))  ? date("d F Y", strtotime($_REQUEST['startdate'])) : date("d F Y");
  $enddate	= (isset($_REQUEST['enddate']) and !empty($_REQUEST['enddate']))  ? date("d F Y", strtotime($_REQUEST['enddate'])) : date("d F Y", strtotime($startdate));

  $start = date("Y-m-d", strtotime($startdate));
  $end = date("Y-m-d", strtotime($enddate));
  /*---------------------------------------------------------------*/
  $xml_tarif =  simplexml_load_file("data-xml/".$roomtype['hotelcode'].".xml");

  // get allotment
  $allocation = 0;
  $query_tag_allotment = '//hotel[@id="'.$roomtype['hoteloid'].'"]/masterroom[@id="'.$roomtype['roomoid'].'"]/rate[@date="'.$start.'"]/allotment/channel[@type="'.$channel.'"]';
  foreach($xml_tarif->xpath($query_tag_allotment) as $allotment){
    if($allotment >= 0){ $allocation = $allotment; }
	}

  // get rate and close out
  $rate = 0; $cta = "n"; $ctd = "n";
  $query_tag_rateplan = '//hotel[@id="'.$roomtype['hoteloid'].'"]/masterroom[@id="'.$roomtype['roomoid'].'"]/rate[@date="'.$start.'"]/rateplan[@id="'.$roomtype['roomofferoid'].'"]/channel[@type="'.$channel.'"]';
  foreach($xml_tarif->xpath($query_tag_rateplan) as $rateplan){
    $rate = $rateplan->double;
    $cta = $rateplan->cta;
    $ctd = $rateplan->ctd;
  }

  $checked_cta	= ($cta == "y")  ? "checked" : "";
  $checked_ctd	= ($ctd == "y")  ? "checked" : "";

?>
<section>
  <h1>Manage <?=$roomtype['name'];?></h1>
  <i class="fa fa-building"></i> <?=$roomtype['hotelname'];?>
</section>
<section class="bg-white">
  <form class="form-horizontal" method="post" action="<?=$base_url?>/process-manage-room">
    <input type="hidden" name="roomoid" value="<?=$roomtype['roomoid']?>">
    <div class="row">
      <div class="col-xs-6">
        <div class="form-group">
          <label class="control-label col-xs-12">From :</label>
          <div class="col-xs-12">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
              <input type="text" class="form-control" name="startdate" id="startdate-today" value="<?=$startdate?>">
            </div>
          </div>
        </div>
      </div>
      <div class="col-xs-6">
        <div class="form-group">
          <label class="control-label col-xs-12">To :</label>
          <div class="col-xs-12">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
              <input type="text" class="form-control" name="enddate" id="enddate-today" value="<?=$enddate?>">
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row select-type-manage">
      <div class="col-xs-12">
        <ul id="select-type-manage" class="nav nav-tabs" role="tablist">
         <li role="presentation" class="active"><a href="#basic-manage" aria-controls="basic-manage" role="tab" data-toggle="tab">Basic</a></li>
         <li role="presentation"><a href="#advance-manage" aria-controls="advance-manage" role="tab" data-toggle="tab">Advance</a></li>
       </ul>
       <!-- Tab panes -->
       <div id="select-type-manage" class="tab-content">
         <div role="tabpanel" class="tab-pane active" id="basic-manage">
           <div class="row"><div class="col-xs-12"><h3>Availability</h3></div></div>
           <div class="form-group">
             <label class="control-label col-xs-8"><?=$roomtype['name'];?></label>
             <div class="col-xs-4">
               <input type="number" class="form-control" name="allotment" value="<?=$allocation?>">
             </div>
           </div>
           <div class="row"><div class="col-xs-12"><h3>Rate Plan</h3></div></div>
           <div class="form-group">
             <div class="col-xs-12">
              <select class="form-control" name="roomofferoid">
              <?php
              $stmt = $db->query("select ro.* from roomoffer ro inner join published p using (publishedoid) inner join room r using (roomoid) inner join offertype using (offertypeoid) where r.roomoid = '".$roomoid."' and p.publishedoid not in ('3') order by ro.name");
              $r_roomoffer = $stmt->fetchAll(PDO::FETCH_ASSOC);
              foreach($r_roomoffer as $roomoffer){
              ?>
              <option value="<?=$roomoffer['roomofferoid']?>"><?=$roomoffer['name']?></option>
              <?php
              }
              ?>
              </select>
             </div>
           </div>
           <div class="form-group">
             <label class="control-label col-xs-6">Rate</label>
             <div class="col-xs-6">
               <input type="text" class="form-control" name="double" value="<?=$rate?>">
             </div>
           </div>
           <div class="form-group">
             <div class="col-xs-6">
               <button class="btn btn-primary full-width" type="submit">Save</button>
             </div>
             <div class="col-xs-6">
               <button class="btn btn-default full-width" act="back-select-room" type="button">Back</button>
             </div>
           </div>
         </div>

         <div role="tabpanel" class="tab-pane" id="advance-manage">
           <div class="row"><div class="col-xs-12"><h3>Availability</h3></div></div>
           <div class="form-group">
             <label class="control-label col-xs-8"><?=$roomtype['name'];?></label>
             <div class="col-xs-4">
               <input type="number" class="form-control" name="allotment" value="<?=$allocation?>">
             </div>
           </div>
           <div class="row"><div class="col-xs-12"><h3>Rate Plan</h3></div></div>
           <div class="form-group">
             <div class="col-xs-12">
              <select class="form-control" name="roomofferoid">
              <?php
              $stmt = $db->query("select ro.* from roomoffer ro inner join published p using (publishedoid) inner join room r using (roomoid) inner join offertype using (offertypeoid) where r.roomoid = '".$roomoid."' and p.publishedoid not in ('3') order by ro.name");
              $r_roomoffer = $stmt->fetchAll(PDO::FETCH_ASSOC);
              foreach($r_roomoffer as $roomoffer){
              ?>
              <option value="<?=$roomoffer['roomofferoid']?>"><?=$roomoffer['name']?></option>
              <?php
              }
              ?>
              </select>
             </div>
           </div>
           <div class="form-group">
             <label class="control-label col-xs-6">Rate</label>
             <div class="col-xs-6">
               <input type="text" class="form-control" name="double" value="<?=$rate?>">
             </div>
           </div>
           <div class="form-group">
             <label class="control-label col-xs-6">Closed to Arrival</label>
             <div class="col-xs-6">
               <input type="checkbox" class="" name="cta" value="y" <?=$checked_cta?>>
             </div>
           </div>
           <div class="form-group">
             <label class="control-label col-xs-6">Closed to Departure</label>
             <div class="col-xs-6">
               <input type="checkbox" class="" name="ctd" value="y" <?=$checked_ctd?>>
             </div>
           </div>
           <div class="form-group">
             <div class="col-xs-6">
               <button class="btn btn-primary full-width" type="submit">Save</button>
             </div>
             <div class="col-xs-6">
               <button class="btn btn-default full-width" act="back-select-room" type="button">Back</button>
             </div>
           </div>
         </div>
       </div>
      </div>
    </div>
  </form>
</section>
