<script type="text/javascript">
$(function(){
  $('body').on('click','button[act="manage-room"]', function(e) {
    rid = $(this).attr("data-id");
    url = "<?php echo $base_url; ?>/manage-room/?roomid="+rid;
    $(location).attr("href", url);
  });
  $('body').on('click','button[act="back-select-room"]', function(e) {
    rid = $(this).attr("data-id");
    url = "<?php echo $base_url; ?>/select-room";
    $(location).attr("href", url);
  });
  $('a[data-toggle="tab"]').on('click', function (e) {
    target = $(this).attr('href');
    $('div#tab-content').not(target).find('input, textarea, button, select').prop('disabled',true);
    $(target).find('input, textarea, button, select').prop('disabled',false);
  });

  $(document).ready(function(){
    $('#advance-manage').find('input, textarea, button, select').prop('disabled',true);
  });

  $('div#notification-manage-rate button[act=ok]').on('click', function (e) {
    $(location).attr("href", "<?php echo $base_url; ?>/manage-room/?roomid=<?=$_POST['roomoid']?>&startdate=<?=$_POST['startdate'];?>");
  });

  $('div#notification-manage-rate').on('hide.bs.modal', function (event) {
    $(location).attr("href", "<?php echo $base_url; ?>/manage-room/?roomid=<?=$_POST['roomoid']?>&startdate=<?=$_POST['startdate'];?>");
  })

  $('body').on('click','a[act="switch-to-dekstop"], button[act="switch-to-dekstop"]', function(e) {
		$.ajax({
			type	: 'POST', cache: false,
			url		: '<?php echo"$base_url"; ?>/mobile/includes/switch-dekstop-version.php',
			success	: function(rsp){ if(rsp == "1"){ document.location.href = '<?php echo"$base_url"; ?>/dashboard'; } }
		});
	});

});
</script>
