<?php

/**
 * class Curr
 * class Func
 * class PromoCode
 *  
 * */
    
    
class Curr {
	static function getCurrencyCode($db,$defaultcurrency = '1'){
		try {
			$syntax_currency = "select currencycode from currency where currencyoid = '".$defaultcurrency."'";
			$stmt	= $db->query($syntax_currency);
			$result_currency = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($result_currency as $currency){
				$main_currency = $currency['currencycode'];
			}
			return $main_currency;
		}catch(PDOException $ex) {
			Func::logJS($ex,'Invalid Query Curr::getCurrencyCode()');
			die();
		}
	}

	static function getcurrencyname($db, $get, $default='IDR') {
		$stmt = $db->prepare("SELECT `converteroid` FROM `currencyconverter` WHERE `resultcurrency`=:a AND `publishedoid`='1'");
		$stmt->execute(array(':a' => $get));
		$r_check = $stmt->fetchAll(PDO::FETCH_ASSOC);
		if (count($r_check) == 1) {
			return $get;
		}else{
			return $default;
		}
	}

	static function convertprice($db, $price, $from, $to, $significant=2) {
		$stmt = $db->prepare("SELECT * FROM `currencyconverter` WHERE (`resultcurrency`=:a OR `resultcurrency`=:b) AND `publishedoid`='1'");
		$stmt->execute(array(':a' => $from, ':b' => $to));
		$r_data = $stmt->fetchAll(PDO::FETCH_ASSOC);
		if (count($r_data) > 0) {
			$cfrom = 1; $cto = 1;
			foreach ($r_data as $a_data) {
				if($a_data['resultcurrency']==$from){
					$cfrom = $a_data['resultnominal'];
				}
				if($a_data['resultcurrency']==$to){
					$cto = $a_data['resultnominal'];
				}
			}
			return round((floatval($price)*$cto/$cfrom), $significant);
		}else{
			return $price;
		}
	}
}

class Func {
    
    static function logJS($data=null,$title=null){
        $stack = debug_backtrace();
        $firstFrame = $stack[0];
        $oneLineData = false;
        $stringDataArray = array();
        $stringData = "";
        
        // $title = (!empty($title)?'['.$title.'] ':'');
        $title = (is_bool($title)?($title?'[true] ':'[false] '):(!empty($title)?'['.$title.'] ':''));

        if(is_string($data)||is_bool($data)||is_int($data)||is_float($data)) 
            $stringDataArray = preg_split ('/$\R?^/m',(is_bool($data)?($data?'true':'false'):(string)$data));
        
        if(count($stringDataArray)==1) 
            $stringData = $stringDataArray[0];
        
        echo '<script>console.log("%c'.basename($firstFrame['file']).'('.$firstFrame['line'].'): %c'.$title.'%c'.$stringData.'" ,"color:purple;","color:grey;","color:black;");</script>';

        if(!empty($data) && empty($stringData))
            echo '<script>console.log('.json_encode($data).');</script>';
        
    }
	
	
	static function differenceDate($arrival, $departure){
		$date1 = new DateTime($arrival);
		$date2 = new DateTime($departure);
		$interval = $date1->diff($date2);
		return $interval->days;
    }
    

	static function rateReffCode($currentrate, $discount){
		$discount = $currentrate * $discount / 100;
		$rate = $currentrate - $discount;
		return $rate;
	}

	static function matchday($subject, $key){
		$pattern = '/'.$key.'/i';
		preg_match($pattern, $subject, $matches);
		if(count($matches) > 0){
			return true;
		}else{
			return false;
		}
	}
	static function matchnight($subject, $key){
		$ar = explode(',', $subject);
		$c = count($ar);
		$res = false;
		for($i=0;$i<$c;$i++){
			if($key == $ar[$i]){
				$res = true;
				break;
			}
		}
		return $res;
    }
    
	static function promocodediscount($applydiscount, $discounttype, $discount, $total){
		if($applydiscount == 'y' and $total > 0){
			if($discounttype == "discount percentage"){
				$total = $total - ($discount/100 * $total);
			}else if($discounttype == "discount amount"){
				$total = $total - $discount;
			}
		}

		return $total;
    }
    
	static function randomString($length) {
		$characters = '0123456789';
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, strlen($characters) - 1)];
		}
		return $randomString;
    }
    static function textToImage($text, $color, $bg, $size){
        // $base_url = "http://localhost/ibe/";
          $base_url = "https://www.thebuking.com/ibe";
        return '<img src="'.$base_url.'/function.texttoimage.php?text='.$text.'&color='.$color.'&bgcolor='.$bg.'&fontsize='.$size.'">';
	  }
	
	/**
	 * Used on Footer and Header
	 * \component\footer.php
	 * \component\header.php
	 */
	static function setSpaceEachThird($text){
        $result = ""; $three = 0;
        foreach(str_split($text,1) as $i){
            $three++; 
            if($three>3) {
                $three=1;
                $result .= " ";
            }
            $result .= $i;
        }
        return $result;
	}
	
	/**
	 * Used on Sending Template Email
	 * \includes\mailer\reservation_sending_template.php
	 */
    static function getEmailDirection($name,$email,$directions){
        $result = array();
        if(!empty($email)){
            foreach($directions as $key => $value){
                $result[$key]['mailertype']=$value;
                $result[$key]['name']=$name;
                $result[$key]['email']=$email;
            }
        }
        return $result;
    }
}

class PromoCode {

	public $notification_promocode = "";

	public $pc_discount_type = '';
	public $pc_discount = 0;
	public $pc_commission_type = '';
	public $pc_commission = 0;

	public $applybar = 'y';
	public $applybardiscount = 'y';
	public $applybarcommission = 'y';

	public $show_promocode = '';
	public $show_promocode_name = '';

	public $promocode = '';
	public $hoteloid = '';
	
	public $promocodeoid;

	public $success=false;
	public $messages='';
    
    public function __construct($db,$hoteloid,$promocode){
		//$promocode = $_REQUEST['promocode']
		//$hoteloid = $_REQUEST['hi']
		$today = date('Y-m-d');

		if(empty($promocode)){ $this->promocode = "thebuking"; }

		try {
			if(isset($hoteloid)){ $this->hoteloid = $hoteloid; }

			$syntax_promocode = "select * from promocode where promocode = '".$this->promocode."' and publishedoid = '1' and (startdate <= '".$today."' and enddate >= '".$today."') and hoteloid = '".$this->hoteloid."'";
			$stmt	= $db->query($syntax_promocode);
			$row_count = $stmt->rowCount();
			if($row_count > 0) {
				$result_promocode = $stmt->fetchAll(PDO::FETCH_ASSOC);
				foreach($result_promocode as $check_rc){

					$this->promocodeoid = $check_rc['promocodeoid'];
					$this->pc_discount_type = $check_rc['discounttype'];
					$this->pc_discount = $check_rc['discount'];
					$this->pc_commission_type = $check_rc['commissiontype'];
					$this->pc_commission = $check_rc['commission'];

					$this->applybar = $check_rc['applybar'];
					$this->applybardiscount = $check_rc['applybardiscount'];
					$this->applybarcommission = $check_rc['applybarcommission'];

					$this->show_promocode = $promocode;
					$this->show_promocode_name = !empty($check_rc['name']) ? $check_rc['name'] : $promocode;
				}
			}else{
				if($this->promocode != "thebuking"){
				$this->notification_promocode = "The promo code &lsquo;".$this->promocode."&rsquo; is not applicable due to incorrect code or invalid date applied.";
				}

				$this->promocode = "thebuking";
				$this->show_promocode = '';
				$this->show_promocode_name = '';
				$this->pc_commission = 0;

			}
			$this->success=true;
			$this->messages = 'Success Query';

		}catch(PDOException $ex) {
			$this->success = false;
			$this->messages = 'Invalid Query';
			Func::logJS($ex,'Invalid Query PromoCode');
			die();
		}

    }
}

?>