<?php
	$roomoid = $_POST['rt'];
	$numroom = $_POST['numroom'];
	$adult = $_POST['adult'];
	$child = $_POST['child'];
	$allotmentalert = $_POST['allotmentalert'];
	$maxextrabed = $_POST['maxextrabed'];
	
	try {
		$stmt = $db->prepare("update room set numroom = :a , adult = :b , child = :c , allotmentalert = :d , maxextrabed = :e where roomoid = :f");
		$stmt->execute(array(':a' => $numroom ,':b' => $adult ,':c' => $child ,':d' => $allotmentalert ,':e' => $maxextrabed,':f' => $roomoid));
	}catch(PDOException $ex) {
		echo "Invalid Query";
		print($ex);
		die();
	}	


	$id = array(); $ed_id = array(); $name = array(); $standardname = array(); $type = array(); $minrate = array();
	$new_name = array(); $new_standardname = array(); $new_type = array(); $new_minrate = array();
	
	if(isset($_POST['id'])){
		foreach($_POST['id'] as $key => $value){ array_push($id, $value); }
	}
	if(isset($_POST['name'])){
		foreach($_POST['name'] as $key => $value){ array_push($name, $value); }
	}
	/*
	if(isset($_POST['standardname'])){
		foreach($_POST['standardname'] as $value){ array_push($standardname, $value); }
	}
	*/
	if(isset($_POST['type'])){
		foreach($_POST['type'] as $value){ array_push($type, $value); }
	}
	if(isset($_POST['minrate'])){
		foreach($_POST['minrate'] as $value){ array_push($minrate, $value); }
	}
	if(isset($_POST['new_name'])){
		foreach($_POST['new_name'] as $value){ array_push($new_name, $value); }
	}
	/*
	if(isset($_POST['new_standardname'])){
		foreach($_POST['new_standardname'] as $value){ array_push($new_standardname, $value); }
	}
	*/
	if(isset($_POST['new_type'])){
		foreach($_POST['new_type'] as $value){ array_push($new_type, $value); }
	}
	if(isset($_POST['new_minrate'])){
		foreach($_POST['new_minrate'] as $value){ array_push($new_minrate, $value); }
	}
	foreach($id as $key => $value){
		try {
			$stmt = $db->prepare("update roomoffer set name = :a , standardname = :b , offertypeoid = :c, minrate = :e where roomofferoid = :d");
			$stmt->execute(array(':a' => $name[$key] ,':b' => $name[$key] ,':c' => $type[$key]  ,':e' => $minrate[$key],':d' => $value));
		}catch(PDOException $ex) {
			echo "Invalid Query";
			print($ex);
			die();
		}	
	}

	try {
		$stmt = $db->prepare("update roomoffer set publishedoid = :a where roomoid = :b and roomofferoid not in (:c)");
		$stmt->execute(array(':a' => 3 ,':b' => $roomoid ,':c' => $edited_id));
 	}catch(PDOException $ex) {
		echo "Invalid Query";
		print($ex);
		die();
	}
	
	if(isset($_POST['id'])){
		$edited_id = "'".implode("','",$id)."'";
		try {
			$stmt = $db->query("update roomoffer set publishedoid = '3' where roomoid = '".$roomoid."' and roomofferoid not in (".$edited_id.")");
		}catch(PDOException $ex) {
			echo "Invalid Query";
			print($ex);
			die();
		}
	}	
	
	foreach($new_name as $key => $value){
		try {
			$stmt = $db->prepare("insert into roomoffer (roomoid, name, standardname, offertypeoid, minrate, publishedoid) values (:a , :b , :c , :d , :e, :f)");
			$stmt->execute(array(':a' => $roomoid, ':b' => $value, ':c' => $value, ':d' => $new_type[$key], ':e' => $new_minrate[$key], ':f' => 1));
		}catch(PDOException $ex) {
			echo "Invalid Query";
			print($ex);
			die();
		}	
	}
	
	echo "<script>location.replace('". $base_url ."/room-settings/edit/?rt=".$roomoid."');</script>";
?>