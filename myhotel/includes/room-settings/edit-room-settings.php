<?php
	$start = date("d F Y");
	$end = date("d F Y",  strtotime(date("Y-m-d")." +1 year" ));
	$daylist = array("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat");
	$roomoid = $_GET['rt'];
	
	try {
		$stmt = $db->query("select r.* from room r inner join hotel h using (hoteloid) where r.roomoid = '".$roomoid."'");
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
			$r_hotel = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_hotel as $row){
				$roomname = $row['name'];
				$numroom = $row['numroom'];
				$adult = $row['adult'];
				$child = $row['child'];
				$allotmentalert = $row['allotmentalert'];
				$maxextrabed = $row['maxextrabed'];
				$minrate = $row['minrate'];
			}
		}else{
			echo "No Result";
			die();
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}
?>
<section class="content-header">
    <h1>
        Room Settings - <?php echo $roomname; ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i>  Settings</a></li>
        <li>Room Settings</li>
        <li class="active">Edit</li>
    </ol>
</section>
<section class="content">
    <form class="form-box" method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/room-settings/edit-process">
    <input type="hidden" name="rt" value="<?php echo $roomoid; ?>" />
    <div class="row">
        <div class="box box-form">
            <div class="box-body">
                    <table width="100%">
                        <tr class="center">
                            <td><label>No. of Rooms</label></td>
                            <td><label>Allotment Alert Level</label></td> 
                            <td><label>Max. Occupancy on Existing Bedding</label></td>  
                            <td><label>Max. Extra Beds</label></td>  
                            <td><label>Max. Children</label></td>  
                        </tr>
                        <tr class="center">
                            <td><input type="text" class="small"  name="numroom" value="<?php echo $numroom; ?>"></td>
							<td><input type="text" class="small"  name="allotmentalert" value="<?php echo $allotmentalert; ?>"></td>
							<td><input type="text" class="small"  name="adult" value="<?php echo $adult; ?>"></td>
							<td><input type="text" class="small"  name="maxextrabed" value="<?php echo $maxextrabed; ?>"></td>
							<td><input type="text" class="small"  name="child" value="<?php echo $child; ?>"></td>
                        </tr>
                    </table>
            </div><!-- /.box-body -->
       </div>
    </div>
    <div class="row">
        <div class="box box-form">
            <div class="box-body">
                <table class="table table-fill table-fill-centered">
                    <tr>
                        <td>Room ID</td>
                        <td>Room Offer Name</td>
                        <?php /*?><td>Standard Room Offer Name</td><?php */?>
                        <td>Offer Type</td>
                        <td>Minimum Rate</td>
                        <td>&nbsp;</td>
                        <td>Delete</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td><input type="text" class="medium" name="offername"/></td>
                        <?php /*?><td><input type="text" class="medium" name="offerstandardname"/></td><?php */?>
                        <td>
                            <select name="offertype">
							<?php
                                try {
                                    $stmt = $db->query("select ot.* from offertype ot inner join published p using (publishedoid) where p.publishedoid = '1'");
                                    $r_dt = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                    foreach($r_dt as $row){
                                        echo"<option value='".$row['offertypeoid']."'>  ".$row['offername']."</option>";
                                    }
                                }catch(PDOException $ex) {
                                    echo "Invalid Query";
                                    die();
                                }                            
                            ?>        	
                            </select>
                        </td>
                        <td><input type="text" class="medium" name="offerminrate"/></td>
                        <td><button type="button" class="small-button blue add">Add</button></td>
                        <td>&nbsp;</td>
                    </tr>
                    <?php
					try {
						$stmt = $db->query("select ro.* from roomoffer ro inner join published p using (publishedoid) inner join room r using (roomoid) inner join offertype using (offertypeoid) where r.roomoid = '".$roomoid."' and p.publishedoid = '1'");
						$r_offer = $stmt->fetchAll(PDO::FETCH_ASSOC);
						foreach($r_offer as $row){
							echo"
							<tr>
								<td>".$row['roomofferoid']."</td>
								<td>
									<input type = 'hidden' name = 'id[]' value = '".$row['roomofferoid']."'>
									<input type = 'text' class='medium' name = 'name[]' value = '".$row['name']."'>
									</td>";
							//<td><input type = 'text' class='medium' name = 'standardname[]' value = '".$row['standardname']."'></td>
							echo "	
								<td>
									<select name = 'type[]'>";
                                    foreach($r_dt as $row1){
										if($row['offertypeoid'] == $row1['offertypeoid']){ $selected = "selected"; }else{ $selected = ""; }
                                        echo"<option value='".$row1['offertypeoid']."' ".$selected.">  ".$row1['offername']."</option>";
                                    }
							echo"
									</select>
								</td>
								<td><input type = 'text' class='medium' name = 'minrate[]' value = '".$row['minrate']."'></td>
								<td>&nbsp;</td>
								<td><button type='button' class='trash delete'>Delete</button></td>
							</tr>
							";
						}
					}catch(PDOException $ex) {
						echo "Invalid Query";
						die();
					}                            
					?>
                </table>
                <br /><br />
                <button type="submit" class="small-button blue">Save</button>
                <a href="<?php echo $base_url; ?>/room-settings"><button type="button" class="small-button blue">Back to Room Settings</button></a>
            </div><!-- /.box-body -->
       </div>
    </div>
    </form>
</section>
