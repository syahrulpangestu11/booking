<?php
	$start = date("d F Y");
	$end = date("d F Y",  strtotime(date("Y-m-d")." +1 year" ));
	$daylist = array("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat");
	$agenthoteloid = $_GET['aho'];

	try {
		$stmt = $db->query("select a.*, ah.agenthoteloid, ah.creditbalance, ah.creditfacility from agenthotel ah inner join agent a USING (agentoid) where ah.agenthoteloid = '".$agenthoteloid."'");
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
			$r_hotel = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_hotel as $row){
				$agentoid = $row['agentoid'];
				$agenthoteloid = $row['agenthoteloid'];
				$agentname = $row['agentname'];
				$cityname = $row['cityname']; $statename = $row['statename'];
				$email = $row['email'];
				$phone = $row['phone'];
				$status = $row['status'];
				$creditbalance = $row['creditbalance'];
			}
		}else{
			echo "No Result";
			die();
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}
?>
<section class="content-header">
    <h1>
        Agent Credit Facility - <?php echo $agentname; ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i>  Settings</a></li>
        <li>Agent Credit Facility </li>
        <li class="active">Edit</li>
    </ol>
</section>
<section class="content">
    <form class="form-box" method="post" enctype="multipart/form-data" id="data-input" action="#">
    <input type="hidden" name="rt" value="<?php echo $roomoid; ?>" />

		<div class="row">
       		<div class="box box-form">
				<div class="box-header with-border">
          			<h1>Credit Facility Balance : <?=number_format($creditbalance)?></h1>
        		</div>

           		<div class="box-body">

							<table class="table table-fill table-fill-centered">
									<tr>
											<td>Date</td>
											<td>Credit Facility Amount</td>
											<td>Action</td>
									</tr>
									<tr>
											<td><input type="text" class="medium startdate" name="date" value="<?=date('d F Y')?>"/></td>
											<td><input type="text" class="medium" name="amount"/></td>
											<td><button type="button" class="small-button blue add" ah="<?=$agenthoteloid?>">Add</button></td>
                                    </tr>
								<?php
								try {
									$stmt = $db->query("select * from agenthotelcreditfacility where agenthoteloid = '".$agenthoteloid."' order by `date`");
									$r_rate = $stmt->fetchAll(PDO::FETCH_ASSOC);
									foreach($r_rate as $row){
										
										$row['date'] = date('d F Y',strtotime($row['date']));
										$row['amount'] = floor($row['amount']);

										echo"
										<tr>
										<form>
											<td><input type='text' class='medium startdate' name='date' value='".$row['date']."'></td>
											<td><input type='text' class='medium' name='amount' value='".$row['amount']."'></td>
											<td>&nbsp;</td>
										</form>
										</tr>
										";
//										<td><button type='button' class='small-button blue edit' ahc='".$row['agenthotelcreditfacilityoid']."'><i class='fa fa-save'></i></button></td>

									}
								}catch(PDOException $ex) {
									echo "Invalid Query";
									die();
								}
								?>
								</table>
            </div><!-- /.box-body -->
       </div>
    </div>
<a href="<?php echo $base_url; ?>/agent"><button type="button" class="small-button blue">Back to Agents</button></a>
    </form>
</section>
