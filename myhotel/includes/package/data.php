<button type="button" class="pure-button blue add"><i class="fa fa-suitcase"></i>Create New Package</button> <button type="button" class="pure-button green template"><i class="fa fa-gift"></i>Create from Template</button>
&nbsp;&nbsp;&nbsp;
<span class="status-green square">sts</span> Active &nbsp;&nbsp;&nbsp; <span class="status-black square">sts</span> Inactive &nbsp;&nbsp;&nbsp; <span class="status-red square">sts</span> Expired

<?php

	include("../ajax-include-file.php");

	$datenow = date("Y-m-d");
	$range = $_REQUEST['range'];
	$startdate = date("Y-m-d", strtotime($_REQUEST['sdate']));
	$enddate = date("Y-m-d", strtotime($_REQUEST['edate']));
	$hoteloid = $_REQUEST['hoteloid'];
	$roomoffer = $_REQUEST['rt'];
	$channeloid = $_REQUEST['ch'];

	// if($range == "sale"){
	// 	//$filter_periode = "salefrom = '".$startdate."' and saleto = '".$enddate."'";

	// 	$filter_periode = "
	// (
	// 	(salefrom <= '".$startdate."' and (saleto >= '".$startdate."' and saleto <= '".$enddate."'))
	// 	or
	// 	((salefrom >= '".$startdate."' and salefrom <= '".$enddate."') and saleto >= '".$enddate."')
	// 	or
	// 	((salefrom >= '".$startdate."' and salefrom <= '".$enddate."') and saleto <= '".$enddate."')
	// 	or
	// 	(salefrom <= '".$startdate."' and saleto >= '".$enddate."')
	// 	or
	// 	(salefrom >= '".$startdate."' and saleto <= '".$enddate."')
	// )
	// 	";
	// }else{
	// 	$filter_periode = "
	// (
	// 	(bookfrom <= '".$startdate."' and (bookto >= '".$startdate."' and bookto <= '".$enddate."'))
	// 	or
	// 	((bookfrom >= '".$startdate."' and bookfrom <= '".$enddate."') and bookto >= '".$enddate."')
	// 	or
	// 	((bookfrom >= '".$startdate."' and bookfrom <= '".$enddate."') and bookto <= '".$enddate."')
	// 	or
	// 	(bookfrom <= '".$startdate."' and bookto >= '".$enddate."')
	// 	or
	// 	(bookfrom >= '".$startdate."' and bookto <= '".$enddate."')
	// )
	// 	";
	// }



	if(!empty($_REQUEST['sdate']) and empty($_REQUEST['edate'])){
		$startdate = date("Y-m-d", strtotime($_REQUEST['sdate']));

		if($range == "sale"){
			$filter_periode = "and (salefrom <= '".$startdate."' and saleto >= '".$startdate."')";
		}else{
			$filter_periode = "and (bookfrom <= '".$startdate."' and bookto >= '".$startdate."')";
		}
	}else if(empty($_REQUEST['sdate']) and !empty($_REQUEST['edate'])){
		$startdate = date("Y-m-d", strtotime($_REQUEST['sdate']));

		if($range == "sale"){
			$filter_periode = "and (salefrom <= '".$enddate."' and saleto >= '".$enddate."')";
		}else{
			$filter_periode = "and (bookfrom <= '".$enddate."' and bookto >= '".$enddate."')";
		}
	}else if(!empty($_REQUEST['sdate']) and !empty($_REQUEST['edate'])){
		if($range == "sale"){
			$filter_periode = "and
		(
			(salefrom <= '".$startdate."' and (saleto >= '".$startdate."' and saleto <= '".$enddate."'))
			or
			((salefrom >= '".$startdate."' and salefrom <= '".$enddate."') and saleto >= '".$enddate."')
			or
			((salefrom >= '".$startdate."' and salefrom <= '".$enddate."') and saleto <= '".$enddate."')
			or
			(salefrom <= '".$startdate."' and saleto >= '".$enddate."')
			or
			(salefrom >= '".$startdate."' and saleto <= '".$enddate."')
		)
			";
		}else{
			$filter_periode = " and
		(
			(bookfrom <= '".$startdate."' and (bookto >= '".$startdate."' and bookto <= '".$enddate."'))
			or
			((bookfrom >= '".$startdate."' and bookfrom <= '".$enddate."') and bookto >= '".$enddate."')
			or
			((bookfrom >= '".$startdate."' and bookfrom <= '".$enddate."') and bookto <= '".$enddate."')
			or
			(bookfrom <= '".$startdate."' and bookto >= '".$enddate."')
			or
			(bookfrom >= '".$startdate."' and bookto <= '".$enddate."')
		)
			";
		}
	}else{
		$filter_periode = "";
	}




	try {
		$stmt = $db->query("SELECT DATEDIFF('".$enddate."','".$startdate."') AS DiffDate");
		$r_datediff = $stmt->fetchAll(PDO::FETCH_ASSOC);
		foreach($r_datediff as $row){
			$diff = $row['DiffDate'];
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}

	$filter_channel = "";

	if(!empty($roomoffer)){
		$filter_roomoffer = "and pa.roomofferoid = '".$roomoffer."'";
		$join_room = "inner join roomoffer ro using (roomofferoid) inner join room r using (roomoid) inner join channel ch using (channeloid)";
		if(!empty($channeloid)){
			$filter_channel = "and ch.channeloid = '".$channeloid."'";
		}
	}else{
		$filter_roomoffer = "";
		$join_room = "";
	}
	
	$hcode = $_REQUEST['hcode'];
	function generateDirectPromotion($name, $minstay, $min_bef_ci){
		global $hcode;
		$link = 'https://thebuking.com/ibe/index.php?hcode='.$hcode.'&night='.$minstay.'&package='.$name.'&requirementcheckin='.$min_bef_ci.'&show=package_only';
		return $link;
	}

	try {
		$stmt = $db->query("
		select 'package' as promotype, p.packageoid, p.name, p.saleto, p.discountvalue, p.priority, dt.labelquestion, p.stay, p.min_bef_ci, (case when p.publishedoid = 1 and p.saleto < '".$datenow."' then 'status-red' when p.publishedoid = 1 then 'status-green' else 'status-black' end) as status, p.publishedoid, pb.note AS published_note from package p inner join discounttype dt using (discounttypeoid) inner join hotel h using (hoteloid) left join packageapply pa using (packageoid) ".$join_room." left join published pb ON (p.publishedoid = pb.publishedoid) where h.hoteloid = '".$hoteloid."' ".$filter_roomoffer." ".$filter_channel." ".$filter_periode." and p.publishedoid not in (3) group by p.packageoid
		ORDER BY p.publishedoid DESC, p.packageoid DESC
		");
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
?>
<table class="table promo-table">
    <tr align="center">
        <td>Package</td>
        <td>Benefit</td>
        <td>Night</td>
        <td>Expiry Date</td>
        <td>&nbsp; </td>
    </tr>
<?php
			$r_promo = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_promo as $row){
				$expirydate = date("d/M/Y", strtotime($row['saleto']));
				$promo_name = urlencode($row['name']);
				$promo_night = ($row['stay'] < 2) ? 2 : $row['stay'] ;
				if($row['status']=="status-green"){
					$published_class = " blue ";
					$published_note = $row['published_note'];
				}else if($row['status']=="status-red"){
					$published_class = " red ";
					$published_note = "expired";
				}else{
					$published_class = " grey " ;
					$published_note = $row['published_note'];
				}
?>
    <tr class="<?php echo $row['status']; ?>">
        <td class="<?php echo $row['status']; ?>"><?php echo $row['name']; ?></td>
        <td><?php echo number_format($row['discountvalue'])." ".$row['labelquestion']; ?></td>
        <td><?php echo $row['stay']; ?> night</td>
        <td><?php echo $expirydate; ?></td>
        <td class="algn-right">
		<button type="button" class="pure-button single change-published <?=$published_class;?>" pid="<?=$row['packageoid']; ?>" current-publishedoid="<?=$row['publishedoid'];?>" title="Click to Change Status">
		<?=$published_note;?></button>
        <button type="button" class="pure-button green single edit" pid="<?php echo $row['packageoid']; ?>"><i class="fa fa-pencil"></i></button>
		<button type="button" class="pure-button orange single copy-link-list" name="<?=$promo_name;?>" night="<?=$promo_night;?>" title="Copy Package Link" min_bef_ci="<?=$row['min_bef_ci']?>" link="<?=generateDirectPromotion($promo_name, $promo_night, $row['min_bef_ci']);?>"><i class="fa fa-link"></i></button>
        <button type="button" class="pure-button red single delete" pid="<?php echo $row['packageoid']; ?>"><i class="fa fa-trash"></i></button>
        </td>
    </tr>
<?php
			}
?>
</table>
<?php
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		print($ex);
		die();
	}
?>
