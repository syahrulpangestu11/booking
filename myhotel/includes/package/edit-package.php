<?php
	include('includes/bootstrap.php');
	include('includes/package/modal/modal-package.php');
	include('includes/package/geo-location/geo-location.php');

	$daylist = array("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat");

	$packageoid = $_GET['pid'];
	try {
		$stmt = $db->query("select package.* from package inner join hotel h using (hoteloid) where packageoid = '".$packageoid."'");
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
			$r_package = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_package as $row){
				$name = $row['name'];
                $packageimage = $row['packageimage'];
				$stay = $row['stay'];
				$salefrom = date("d F Y", strtotime($row['salefrom']));
				$saleto = date("d F Y", strtotime($row['saleto']));
				$stayfrom = date("d F Y", strtotime($row['bookfrom']));
				$stayto = date("d F Y", strtotime($row['bookto']));
				$display = explode(',',$row['displayed']);
				$checkin = explode(',',$row['checkinon']);
				$timefrom = date("H:i", strtotime($row['timefrom']));
				$timeto = date("H:i", strtotime($row['timeto']));
				$discounttype = $row['discounttypeoid'];
				$discountapply = $row['discountapplyoid'];
				$applyvalue = $row['applyvalue'];
				$discountvalue = floor($row['discountvalue']);
				$cancellationpolicy = $row['cancellationpolicyoid'];
				$published = $row['publishedoid'];
				$deposit = $row['depositoid'];
				$depositvalue = $row['depositvalue'];
				$multiply = $row['multiply'];
				$multiplier = $row['multiplier'];
				$multiplier_val = $row['multiplier_val'];

				$min_ci = $row['min_bef_ci'];
				$max_ci = $row['max_bef_ci'];

				$packageheadline = $row['headline'];
				$description = $row['description'];
				$servicefacilities = $row['servicefacilities'];
				$termcondition = $row['termcondition'];

				$popupbanner = $row['popupbanner'];
			}
		}else{
			echo "No Result";
			die();
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}
?>
<script type="text/javascript">
$(function() {
	$('textarea.html-box').trumbowyg({
		btns: ['viewHTML',
		  '|', 'btnGrp-design',
		  '|', 'link',
		  '|', 'btnGrp-justify',
		  '|', 'btnGrp-lists'],
		fullscreenable: false
	});
});
</script>
<section class="content-header">
    <h1>
        Edit Package
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i>  Allotments &amp; Rates </a></li>
        <li class="active">Package</li>
    </ol>
</section>
<section class="content" id="package">
    <form class="form-box form-horizontal" method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/package/edit-process"><input type="hidden" name="packageoid" value="<?php echo $packageoid; ?>" />
        <div class="box box-form">
        	<div class="form-group"><div class="col-md-12"><h1>PACKAGE</h1></div></div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-md-12">Package Name</label>
                            <div class="col-md-12"><input type="text" class="form-control" name="name" required="required" value="<?=$name?>" style="width:100%"></div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">Direct Booking Package Link</label>
                            <div class="col-md-12">
                                <div class="radio" style="display:inline-block;padding-right:10px;">
                                    <label><input type="radio" name="link-style" value="primary" checked="checked"> show package only</label>
                                </div>
                                <div class="radio" style="display:inline-block;padding-right:10px;">
                                    <label><input type="radio" name="link-style" value="all"> show package on top</label>
                                </div>
                            </div>
                            <div class="col-md-12"><textarea name="direct-link" class="form-control" style="width:100%"></textarea></div>
        				</div>
        				<div class="form-group">
                            <div class="col-md-12"><button type="button" class="btn btn-warning btn-sm copy-link" name="<?=htmlspecialchars($name)?>" night="<?php if($stay < 2){ echo 2; }else{ echo $stay; } ?>" min-bef-ci="<?php echo $min_ci; ?>"><i class="fa fa-link"></i> Copy Link to Clipboard</button></div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2">Source</label>
                            <div class="col-md-4"><input type="text" class="form-control utmset" name="utm_source" value="" style="width:100%"/></div>
                            <label class="col-md-2">Champaign</label>
                            <div class="col-md-4"><input type="text" class="form-control utmset" name="utm_campaign" value="" style="width:100%"/></div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2">URL Link</label>
                            <div class="col-md-10"><textarea name="direct2-link" class="form-control" style="width:100%"></textarea></div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12"><button type="button" class="btn btn-warning btn-sm copy2-link"><i class="fa fa-link"></i> Copy Link to Clipboard</button></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-md-12">Package Image</label>
                            <div class="col-md-12 preview-image">
                            <?php if(!empty($packageimage)){ ?>
                                <img src="<?=$packageimage?>" id="preview-image" class="img-responsive" alt="Responsive image" style="width:200px;">
                            <?php }else{ echo "N/A Image for Promotion"; } ?>
                            </div>
                            <div class="col-md-12">
                                <div class="checkbox">
                                    <?php if($popupbanner==1){ $checked = "checked"; }else{ $checked = ""; } ?>
                                    <label><input type="checkbox" name="popupbanner" value="1" <?=$checked?>> Show as <b>Popup Banner</b></label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">Upload New Package Image</label>
                            <div class="col-md-12"><input type="file" class="form-control" name="image"></div>
                            <div class="col-md-12"><i>recommended size : 550px x 310px</i></div>
                        </div>
                    </div>
                </div>
                <div class="form-group"><div class="col-md-12 text-right"><button class="btn btn-primary" type="submit">Save Package</button></div></div>
            </div>
            <div class="clear"></div>
            <br>

            <div class="row">
            	<div class="col-md-4 triple" style="padding:10px;">
									<h2>PACKAGE TYPE</h2>
									<div class="form-group">
										<div class="col-md-12">
											<select name="typepackage" class="form-control">
											<?php foreach($listpackagetype as $key => $value){ ?>
												<option value="<?=$key?>" <?php if($key == $row['typepackage']){ echo "selected"; } ?>> <?=$value?></option>
											<?php } ?>
											</select>
										</div>
									</div>
                	<h2>CONDITION</h2>
                    <div class="form-group">
                        <label class="col-md-5">Stay:</label>
                        <div class="col-md-4"><input type="number" class="form-control" name="stay" min="1" value="<?=$stay?>"></div>
                    </div>
										<div class="form-group long-stay-feature" <?php if($row['typepackage'] != "2"){ echo 'style="display:none;"'; } ?>>
											<div class="checkbox col-md-12">
											  <label>
											    <input type="checkbox" name="overbook" value="y" <?php if($row['overbooking'] == "y"){ echo 'checked="checked"'; } ?>>
											    Allow over booking
											  </label>
											</div>
											<div class="checkbox col-md-12">
											  <label>
											    <input type="checkbox" name="ignorenight" value="y" <?php if($row['alwaysshown'] == "y"){ echo 'checked="checked"'; } ?>>
											    Allow this package show in all night criteria
											  </label>
											</div>
										</div>
                    <div class="form-group">
                        <label class="col-md-12">Guest Booking Within:</label>
                        <div class="col-md-4"><input type="number" class="form-control" name="min_ci" min="0" value="<?php echo $min_ci; ?>"/></div>
                        <div class="col-md-1 text-center">to</div>
                        <div class="col-md-4"><input type="number"  class="form-control" name="max_ci" min="0" value="<?php echo $max_ci; ?>"/></div>
                        <div class="col-md-12">days (before check in)</div>
                    </div>
                    <div class="form-group"><label class="col-md-12">Sale Date</label></div>
                    <div class="form-group">
                        <label class="col-md-5">Sale Date From:</label>
                        <div class="col-md-7">
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                <input type="text" class="form-control" id="startdate" name="salefrom" value="<?=$salefrom?>">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5">Sale Date To:</label>
                        <div class="col-md-7">
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                <input type="text" class="form-control" id="enddate" name="saleto" value="<?=$saleto?>">
                            </div>
                        </div>
                    </div>
                    <div class="form-group"><label class="col-md-3">Stay Date</label></div>
                    <div class="form-group">
                        <label class="col-md-5">Stay Date From:</label>
                        <div class="col-md-7">
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                <input type="text" class="form-control" id="startcalendar" name="stayfrom" value="<?=$stayfrom?>">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5">Stay Date To:</label>
                        <div class="col-md-7">
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                <input type="text" class="form-control" id="endcalendar" name="stayto" value="<?=$stayto?>">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Displayed on:</label><br>
                        <div class="col-md-12">
							<?php
								foreach($daylist as $day){
									if(in_array($day, $display)){ $checked = "checked"; }else{ $checked = ""; }
									echo"<input type='checkbox' name='display[]' value='".$day."' ".$checked.">".$day."&nbsp;&nbsp;";
								}
							?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Check-in on:</label><br>
                        <div class="col-md-12">
							<?php
								foreach($daylist as $day){
									if(in_array($day, $checkin)){ $checked = "checked"; }else{ $checked = ""; }
									echo"<input type='checkbox' name='checkin[]' value='".$day."' ".$checked.">".$day."&nbsp;&nbsp;";
								}
							?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5">Book Time From:</label>
                        <div class="col-md-7">
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                <input type="time" class="form-control" name="timefrom" value="<?php echo $timefrom; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5">Book Time To:</label>
                        <div class="col-md-7">
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                <input type="time" class="form-control" name="timeto" value="<?php echo $timeto; ?>">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 triple" style="display:none;">
                	<h2>BENEFIT</h2>
                    <div class="form-group">
                        <label class="col-md-12">Discount Type</label>
                        <?php
                            try {
                                $stmt = $db->query("select dt.* from discounttype dt inner join published p using (publishedoid) where p.publishedoid = '1'");
                                $r_dt = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                foreach($r_dt as $row){
                                    if($row['discounttypeoid'] == $discounttype){ $selected = "checked"; }else{ $selected=""; }
                                    echo "<div class='checkbox'><label><input type='radio' name='discounttype' value='".$row['discounttypeoid']."' label='".$row['labelquestion']."' ".$selected.">  ".$row['name']."</label></div>";
                                }
                            }catch(PDOException $ex) {
                                echo "Invalid Query";
                                die();
                            }
                        ?>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Discount Value</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control"  name="discountvalue" value="<?php echo $discountvalue; ?>">
                            <span></span>
                        </div>
                    </div>
                    <div class="form-group applybox">
                        <label class="col-md-12">Apply On</label>
                        <?php
                            try {
                                $stmt = $db->query("select da.* from discountapply da inner join published p using (publishedoid) where p.publishedoid = '1'");
                                $r_dt = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                foreach($r_dt as $row){
                                    if($row['discountapplyoid'] == $discountapply){ $selected = "checked"; }else{ $selected=""; }
                                    echo"<div class='col-md-12'><input type='radio' name='discountapply' value='".$row['discountapplyoid']."' ".$selected.">  ".$row['name']."";
                                    if($row['discountapplyoid'] == '2'){
                                        echo"<div class='applyval'><label>Apply on Night Number</label><input type='text' class='form-control' name='applyvalue' value='".$applyvalue."'><br>
                                        ex: 2,3,4 for night number 2, 3 and 4 only</div>";
                                    }else if($row['discountapplyoid'] == '3'){
                                        echo"<div class='applyval'><label>Apply on Day</label><br>";
                                        foreach($daylist as $day){
											if(!empty($applyvalue)){
												if(in_array($day, $applyvalue)){ $checked = "checked"; }else{ $checked = ""; }
											}else{
												$checked = "checked";
											}
                                            echo"<input type='checkbox' name='applyvalue[]' value='".$day."' ".$checked.">".$day."&nbsp;&nbsp;";
                                        }
                                        echo"</div>";
                                    }
									echo "</div>";
                                }
                            }catch(PDOException $ex) {
                                echo "Invalid Query";
                                die();
                            }
                        ?>
                    </div>
                </div>

                <style>
                    #step-3 {
                        padding: 20px;
                        border: 1px solid #ddd;
                    }
                </style>

            	<div class="col-md-8">
                    <div class="" id="step-3">
                        <div class="row">
                            <div class="col-md-6"><h1><i class="fa fa-bed"></i> Set Room &amp; Rate</h1></div>
                            <div class="col-md-6 text-right"><button type="button" class="btn btn-success" data-toggle="modal" data-target="#RatePlanModal">Add New Room and Rate</button></div>
                        </div>
												<div class="row">
													<div class="col-md-12">
														<label>
															<i class="fa fa-info-circle"></i> Multiples apply for package purchases? &nbsp;&nbsp;&nbsp;
															<input type="radio" name="multiply" value="y" <?php if($multiply == "y"){ echo "checked"; }?>> Y
															<input type="radio" name="multiply" value="n" <?php if($multiply == "n"){ echo "checked"; }?>> N
														</label>
													</div>
												</div>
												<div class="row">
													<div class="col-md-12">
														<label>
															<i class="fa fa-info-circle"></i> Apply multiplier &nbsp;&nbsp;&nbsp;
															<input type="radio" name="multiplier" value="plus" <?php if($multiplier == "plus"){ echo "checked"; }?>> +
															<input type="radio" name="multiplier" value="minus" <?php if($multiplier == "minus"){ echo "checked"; }?>> -
															<input type="text" name="multiplier_val" value="<?=$multiplier_val?>"> %
														</label>
													</div>
												</div>
                        <div class="row" id="assigned-rateplan">
                        <?php
                        $stmt = $db->query("select pa.packageapplyoid, pa.roomofferoid, ro.name, ot.offername from packageapply pa inner join roomoffer ro using (roomofferoid) inner join room r using (roomoid) inner join offertype ot using (offertypeoid) where packageoid = '".$packageoid."'");
                        $r_rateplan = $stmt->fetchAll(PDO::FETCH_ASSOC);
                        foreach($r_rateplan as $rateplan){
                        ?>
                            <div class="rateplan" rp="<?=$rateplanoid?>">
                                <div class="header">
                                    <div class="row">
                                        <div class="col-md-6"><?=$rateplan['name']?><br /><small><?=$rateplan['offername']?></small></div>
                                        <div class="col-md-6 text-right">
                                            <input type="hidden" name="rateplan[]" value="<?=$rateplan['roomofferoid']?>">
                                            <button type="button" class="btn btn-danger btn-sm remove-rateplan" data-rp="<?=$rateplanoid?>"><i class="fa fa-trash-o"></i> Remove Rate Plan</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="content table-responsive" id="rate">
                                    <table class="table table-striped">
                                        <tr class="text-center"><th>&nbsp;</th><th>Start Date</th><th>End Date</th><th>Rate</th><th>Priority</th><th>Action</th></tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td><input type="text" class="from" name="from"></td>
                                            <td><input type="text" class="to" name="to"></td>
                                            <td><input type="text" name="rate"></td>
                                            <td><input type="number" name="priority" min="1" max="10" value="10"></td>
                                            <td><button type="button" class="btn btn-primary btn-xs add-rate" rp="<?=$rateplan['roomofferoid']?>">add</button></td>
                                        </tr>
                                        <?php
                                        $stmt = $db->query("select pr.* from packagerate pr where packageapplyoid = '".$rateplan['packageapplyoid']."' order by startdate, enddate, priority");
                                        $r_rate = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                        foreach($r_rate as $rate){
                                        ?>
                                            <tr>
                                                <td><input type="hidden" name="c_pr-<?=$rateplan['roomofferoid']?>[]" value="<?=$rate['packagerateoid']?>" /></td>
                                                <td><input type="text" class="from" name="c_from-<?=$rateplan['roomofferoid']?>[]" value="<?=date('d F Y', strtotime($rate['startdate']))?>"></td>
                                                <td><input type="text" class="to" name="c_to-<?=$rateplan['roomofferoid']?>[]" value="<?=date('d F Y', strtotime($rate['enddate']))?>"></td>
                                                <td><input type="text" name="c_rate-<?=$rateplan['roomofferoid']?>[]" value="<?=floor($rate['rate'])?>"></td>
                                                <td><input type="number" name="c_priority-<?=$rateplan['roomofferoid']?>[]" min="1" max="10" value="<?=$rate['priority']?>"></td>
                                                <td><button type="button" class="btn btn-danger btn-xs remove-rate"><i class="fa fa-remove"></i></button></td>
                                            </tr>
                                        <?php
                                        }
                                        ?>
                                    </table>
                                </div>
                            </div>
                        <?php
                        }
                        ?>
                        </div>
                    </div>



                </div>
            </div>
            <div class="form-group"><div class="col-md-12 text-right"><button class="btn btn-primary" type="submit">Save Package</button></div></div>
		</div>

		<?php if(!in_array($_SESSION['_hoteltemplate'], array(4))){ ?>
		<div class="box box-form" id="headline-detail">
        	<div class="row">
            	<div class="col-md-6">
                    <h1><i class="fa fa-eye"></i> Show Package with Geo Location</h1>
                    <div class="panel panel-info">
                        <div class="panel-heading text-center">
                            <button type="button" class="btn btn-primary btn-lg"  data-toggle="modal" data-target="#geoModalCountry"><i class="fa fa-map-marker"></i> Add Location</button>
                            <div class="list-country">
                                <ul class="inline-block">
                                <?php
                                try {
                                    $stmt = $db->query("select pg.packagegeooid, c.countryname from packagegeo pg inner join country c using (countryoid) where pg.packageoid = '".$packageoid."'");
                                    $row_count = $stmt->rowCount();
                                    if($row_count > 0) {
                                        $r_geo = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                        foreach($r_geo as $key => $country){
                                            echo "<li><span class='current'><input type='hidden' name='applycountry[]' value='".$country['packagegeooid']."'>".$country['countryname']."</span></li>";
                                        }
                                    }
                                }catch(PDOException $ex) {
                                    echo "Invalid Query".$ex;
                                    die();
                                }
                                ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            	<div class="col-md-6">
                    <h1><i class="fa fa-eye-slash"></i> Hide Package with Geo Location</h1>
                    <div class="panel panel-danger">
                        <div class="panel-heading text-center">
                            <button type="button" class="btn btn-danger btn-lg"  data-toggle="modal" data-target="#geoModalCountryNoShow"><i class="fa fa-map-marker"></i> Add Location</button>
                            <div class="list-country-noshow">
                                <ul class="inline-block">
                                <?php
                                try {
                                    $stmt = $db->query("select pgn.packagegeo_noshowoid, c.countryname from packagegeo_noshow pgn inner join country c using (countryoid) where pgn.packageoid = '".$packageoid."'");
                                    $row_count = $stmt->rowCount();
                                    if($row_count > 0) {
                                        $r_geo = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                        foreach($r_geo as $key => $country){
                                            echo "<li><span class='current'><input type='hidden' name='apply_noshow_country[]' value='".$country['packagegeo_noshowoid']."'>".$country['countryname']."</span></li>";
                                        }
                                    }
                                }catch(PDOException $ex) {
                                    echo "Invalid Query".$ex;
                                    die();
                                }
                                ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<?php } ?>

		<div class="box box-form" id="headline-detail">
			<h2>HEADLINE &amp; DETAIL</h2>
            <div class="row">
            <?php
                try {
										$stmt = $db->query("SELECT th.*, i.*, x.termheadlinepackageoid from termheadline th left join icon i using (iconoid) left join (select termheadlinepackageoid, termheadlineoid from termheadlinepackage thp where thp.id='".$packageoid."' and thp.type='package') x on x.termheadlineoid = th.termheadlineoid
										where th.termheadlineoid not in (select termheadlineoid from termheadlinehotelremove where hoteloid = '".$hoteloid."') and th.hoteloid in ('".$hoteloid."', '0')
										order by icon_title");
                    $r_headline = $stmt->fetchAll(PDO::FETCH_ASSOC);
                    foreach($r_headline as $headline){
                        if($headline['iconoid'] == 0){
                            $icon_src = $headline['term_icon_src'];
                        }else{
                            $icon_src = $headline['icon_src'];
                        }
												if(!empty($headline['termheadlinepackageoid'])){ $checkapply = "checked='checked'";  }else{ $checkapply = ""; }
                        ?>
                        <div class="col-md-3">
                        <input type="checkbox" name="icon[]" value="<?=$headline['termheadlineoid']?>" <?=$checkapply?>/>&nbsp;&nbsp;<img src="<?=$icon_src?>" class="icon" />&nbsp;&nbsp;<?=$headline['icon_title']?>
                        </div>
                        <?php
                    }
                }catch(PDOException $ex) {
                    echo "Invalid Query";
                    die();
                }
            ?>
			</div>
            <div class="form-group">
                <div class="col-md-6">
                    <br>
                    <label>Headline</label>
                    <textarea name="headline" class="html-box"><?=$packageheadline?></textarea>
                </div>
                <div class="col-md-6">
                    <br>
                    <label>Package Description</label>
                    <textarea name="description" class="html-box"><?=$description?></textarea>
                </div>
                <div class="col-md-6">
                    <br>
                    <label>Package Inclusive</label>
                    <textarea name="servicefacilities" class="html-box"><?=$servicefacilities?></textarea>
                </div>
                <div class="col-md-6">
                    <br>
                    <label>Package Terms &amp; Condition</label>
                    <textarea name="termcondition" class="html-box"><?=$termcondition?></textarea>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <br>
                        <label class="col-md-12">Cancellation Policy:</label>
                        <div class="col-md-12">
                            <select name="cancellation" class="form-control">
                            <?php
                                try {
                                    $stmt = $db->query("select cp.* from cancellationpolicy cp inner join published p using (publishedoid) where p.publishedoid = '1' and cp.hoteloid in ('0','".$hoteloid."')");
                                    $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                    foreach($r_room as $row){
										if($row['cancellationpolicyoid'] == $cancellationpolicy){ $selected = "selected"; }else{ $selected=""; }
                                        echo"<option value='".$row['cancellationpolicyoid']."' ".$selected.">".$row['name']."</option>";
                                    }
                                }catch(PDOException $ex) {
                                    echo "Invalid Query";
                                    die();
                                }
                            ?>
                            </select>
                    	</div>
                    </div>
										<div class="panel panel-danger">
											<div class="panel-heading" style="color:#000">
                    		<div class="form-group">
	                        <div class="panel-heading" style="color:#000">
	                            <label>Deposit Type</label>
	                            &nbsp;
	                            <span style="font-size:0.9em; color:#de0000"><i class="fa fa-info-circle"></i> This amount will be declared as booking guarantee that charged by hotel upon booking received.</span>
	                        </div>
                        <!-- <label class="col-md-12">Deposit Type</label> -->
	                        <div class="col-md-12">
								<?php
	                                try {
	                                    $stmt = $db->query("select d.* from deposit d inner join published p using (publishedoid) where p.publishedoid = '1'");
	                                    $r_dt = $stmt->fetchAll(PDO::FETCH_ASSOC);
	                                    foreach($r_dt as $row){
											if($row['depositoid'] == $deposit){ $selected = "checked"; $depositval = $depositvalue; }else{ $selected=""; $depositval = ""; }
	                                        echo"<input type='radio' name='deposit' value='".$row['depositoid']."' ".$selected.">  ".$row['name'];
											if($row['depositoid'] == '2'){
												echo"<div class='depositval' style='display:none'><input type='text' class='medium'  name='depositvalue' value='".$depositval."'> Deposit for IDR Amount</div>";
											}else if($row['depositoid'] == '3'){
												echo"<div class='depositval' style='display:none'><input type='text' class='medium'  name='depositvalue' value='".$depositval."'> Deposit for %(percentage)from total</div>";
											}else if($row['depositoid'] == '4'){
												echo"<div class='depositval' style='display:none'><input type='text' class='medium'  name='depositvalue' value='".$depositval."'> night(s) amount</div>";
											}
											echo "<br>";
	                                    }
	                                }catch(PDOException $ex) {
	                                    echo "Invalid Query";
	                                    die();
	                                }
	                            ?>
	                        </div>
                    	</div>
										</div>
									</div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <br>
                        <label class="col-md-12">Publish Package :</label>
                        <div class="col-md-12">
                            <select name="published" class="form-control">
                            <?php
                                try {
                                    $stmt = $db->query("select * from published where showthis = 'y'");
                                    $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                    foreach($r_room as $row){
                                        if($row['publishedoid'] == $published){ $selected = "selected"; }else{ $selected=""; }
                                        echo"<option value='".$row['publishedoid']."' ".$selected.">".$row['note']."</option>";
                                    }
                                }catch(PDOException $ex) {
                                    echo "Invalid Query";
                                    die();
                                }
                            ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
            	<div class="col-md-12 text-right">
                    <button class="btn btn-danger cancel" type="button">Cancel Editing</button>
                    <button class="btn btn-primary" type="submit">Save Package</button>
            	</div>
            </div>
   		</div>
    </form>
</section>
