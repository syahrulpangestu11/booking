<?php
try {

	session_start();
	error_reporting(E_ALL ^ E_NOTICE);
	include("../../../conf/connection.php");
	
	$hoteloid = $_SESSION['_hotel'];
	$currenttime = date('Y-m-d H:i:s');

	switch($_POST['request']){
		
		/* SELECT RATEPLAN ---------------------------------------------*/
		
		case "add-rateplan" :
			/* unshow rateplan that has been selected */
			$selected_rateplan = array();
			if(isset($_POST['rateplan'])){ $selected_rateplan = $_POST['rateplan']; }
			$selected_rateplan = implode("','", $selected_rateplan);
			?>
			<label class="col-md-12">Rate Plan</label>
			<div class="col-md-12">
				<select name="rateplanoid" class="form-control">
				<?php
					try {
						$stmt = $db->query("select r.roomoid, r.name, x.offernum from room r inner join hotel h using (hoteloid) left join (select count(roomofferoid) as offernum, ro.roomoid from roomoffer ro group by ro.roomoid) as x on x.roomoid = r.roomoid where h.hoteloid = '".$hoteloid."' and x.offernum > 0 and r.publishedoid = '1'");
						$r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
						foreach($r_room as $row){
							echo"<optgroup label = '".$row['name']."'>";
							try {
								$stmt = $db->query("select ro.*, ot.offername  from roomoffer ro inner join published p using (publishedoid) inner join room r using (roomoid) inner join offertype ot using (offertypeoid) where r.roomoid = '".$row['roomoid']."' and p.publishedoid = '1' and ro.roomofferoid not in ('".$selected_rateplan."')");
								$r_offer = $stmt->fetchAll(PDO::FETCH_ASSOC);
								foreach($r_offer as $row1){
								   echo"<option value='".$row1['roomofferoid']."' ".$selected.">  ".$row1['name']." - ".$row1['offername']."</option>";
								}
							}catch(PDOException $ex) {
								echo "Invalid Query";
								die();
							}     
							echo"</optgroup>";                       
						}
					}catch(PDOException $ex) {
						echo "Invalid Query";
						die();
					}
					?>   
				</select>
			</div>
			<?php
		break;
	
	
		/* ASSIGN RATEPLAN / ROOMOFFER ---------------------------------------------*/
	
		case "assign-rateplan" :
			$rateplanoid = $_POST['rateplanoid'];			
			$stmt = $db->prepare("select ro.roomofferoid, ro.name, ot.offername from roomoffer ro inner join room r using (roomoid) inner join offertype ot using (offertypeoid) where roomofferoid = :a");
			$stmt->execute(array(':a' => $rateplanoid));
			$room = $stmt->fetch(PDO::FETCH_ASSOC);
			?>
			  <div class="rateplan" rp="<?=$rateplanoid?>">
				<div class="header">
					<div class="row">
						<div class="col-md-6"><?=$room['name']?><br /><small><?=$room['offername']?></small></div>
						<div class="col-md-6 text-right">
                        	<input type="hidden" name="rateplan[]" value="<?=$rateplanoid?>">
							<button type="button" class="btn btn-danger btn-sm remove-rateplan" data-rp="<?=$rateplanoid?>"><i class="fa fa-trash-o"></i> Remove Rate Plan</button>
						</div>
					</div>
				</div>
				<div class="content table-responsive" id="rate">
                	<table class="table table-striped">
                    	<tr class="text-center"><th>&nbsp;</th><th>Start Date</th><th>End Date</th><th>Rate</th><th>Priority</th><th>Action</th></tr>
                        <tr>
                        	<td>&nbsp;</td>
                        	<td><input type="text" class="from" name="from"></td>
                        	<td><input type="text" class="to" name="to"></td>
                        	<td><input type="text" name="rate"></td>
                            <td><input type="number" name="priority" min="1" max="10" value="10"></td>
                            <td><button type="button" class="btn btn-primary btn-xs add-rate" rp="<?=$rateplanoid?>">add</button></td>
						</tr>
                    </table>
				</div>
			</div>
			<?php
		break;
		
		/* ASSIGN RATEPLAN / ROOMOFFER ---------------------------------------------*/
	
		case "add-rate" :
			$rateplanoid = $_POST['rateplan'];	
			?>
                <tr>
                    <td><i class="fa fa-check"></i> new</td>
                    <td><input type="text" class="from" name="from-<?=$rateplanoid?>[]" value="<?=$_POST['from']?>"></td>
                    <td><input type="text" class="to" name="to-<?=$rateplanoid?>[]" value="<?=$_POST['to']?>"></td>
                    <td><input type="text" name="rate-<?=$rateplanoid?>[]" value="<?=$_POST['rate']?>"></td>
                    <td><input type="number" name="priority-<?=$rateplanoid?>[]" min="1" max="10" value="<?=$_POST['priority']?>"></td>
                    <td><button type="button" class="btn btn-danger btn-xs remove-rate"><i class="fa fa-remove"></i></button></td>
                </tr>
			<?php
		break;
	}
}catch(Exception $ex) {
	echo "error";
	echo $ex->getMessage();
	die();
}
?>
