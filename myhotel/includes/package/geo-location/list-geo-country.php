<?php
	error_reporting(E_ALL ^ E_NOTICE);
	include("../../../conf/connection.php");	
	
	$showcountry = array();
	/* selected SHOW country */
	if(isset($_POST['country'])){
		$showcountry = $_POST['country'];
	}

	$noshow_country = array();
	/* selected SHOW country */
	if(isset($_POST['noshow_country'])){
		$noshow_country = $_POST['noshow_country'];
	}

?>

<div class="box-body">
    <div class="row">
        <?php
            // main query
            $batas=30; 
			$halaman=isset($_REQUEST['page']) ? $_REQUEST['page'] : 0 ;
			if(empty($halaman)){ 
				$posisi=0; $halaman=1;
			}else{ 
				$posisi = ($halaman-1) * $batas; 
			}
			$no=$posisi+1;
			$j = $halaman + ($halaman - 1) * ($batas - 1); 
			
			$paging_query = " limit ".$posisi.", ".$batas;
			$num = $posisi;
            
            $main_query ="SELECT * FROM country";
            
            // query tambahan jika ada filter
            $filter = array();
            if(isset($_REQUEST['name']) and $_REQUEST['name']!=''){
                array_push($filter, 'country.countryname LIKE "%'.$_REQUEST['name'].'%"');
            }

            // menggabungkan query utama dengan query filter
            if(count($filter) > 0){
                $combine_filter = implode(' and ',$filter);
                $syntax_list = $main_query.' where '.$combine_filter;
            }else{
                $syntax_list = $main_query;
            }
            
            $syntax_list_paging = $syntax_list." order by countryname ".$paging_query;
            $stmt = $db->query($syntax_list_paging);
			$result_list = $stmt->fetchAll(PDO::FETCH_ASSOC);
            foreach($result_list as $list){
                $num++;
        ?>
                <div class="col-md-4">
                	<?php
						if(in_array($list['countryoid'], $showcountry)){
							echo "<i class='fa fa-eye'></i> ";
						}else if(in_array($list['countryoid'], $noshow_country)){
							echo "<i class='fa fa-eye-slash'></i> ";
						}else{
							echo '<input type="checkbox" name="country[]" value="'.$list['countryoid'].';'.$list['countryname'].'">';
						}
					?>
                		 <?php echo $list['countryname']; ?></div>
        <?php
            }
        ?>
    </div>
    <?php require_once('../../crm/paging/post-paging.php'); ?>
</div>
