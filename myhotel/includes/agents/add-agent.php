<section class="content-header">
    <h1>
        Agent
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-laptop"></i> Agent</a></li>
        <li class="active">Add</li>
    </ol>
</section>
<section class="content">
	<div class="row">
        <div class="box box-form">
        	<form class="form-box" method="post" enctype="multipart/form-data" id="data-input" action="#">
            <input type="hidden" name="dfltstate" value="2">
            <input type="hidden" name="dfltcity" value="0">
            <input type="hidden" name="hoid" value="<?=$_SESSION['_hotel'];?>">
            <input type="hidden" name="published_agent" value="<?=$_SESSION['_hotel'];?>">

            <h3>AGENT PROFILE</h3>
            <ul class="block">
                <li>
                    <span class="label">Agent Name</span>
                    <input type="text" class="long" name="name">
				        </li>
            </ul>

            <h3>AGENT LOCATION</h3>
            <ul class="block">
                <li>
                    <span class="label">Street Address</span>
                    <textarea name="address"></textarea>
				        </li>
                <li>
                    <span class="label float-left" style="vertical-align:middle">&nbsp;</span>
                    <div class="side-left">
                        <ul class="inline-block">
                            <li>
                            	Country<br />
                                <select name="country" class="input-select">
									                <?php getCountry(11); ?>
                                </select>
                            </li>
                            <li class="loc-state"></li>
                            <li class="loc-city"></li>
                        </ul>
                    </div>
                    <div class="clear"></div>
                </li>
            </ul>

            <h3>AGENT CONTACT</h3>
            <ul class="block">
                <li>
                    <span class="label">Main Phone Number</span>
                    <input type="text" class="medium" name="phone">
				</li>
                <li>
                    <span class="label">Website</span>
                    <input type="text" class="long" name="website">
				</li>
                <li>
                    <span class="label">Email</span>
                    <input type="text" class="long" name="email">
				</li>
                <li>
                    <span class="label"><b>Publish Agent</b></span>
                    <select name="published">
                    <?php getPublished(0); ?>
                    </select>
                </li>
                <li>
                    <span class="label float-left" style="vertical-align:middle">&nbsp;</span>
                    <div class="side-left">
                        <div class="form-input">
							<button type="button" class="submit-add">Submit</button>
                        </div>
                    </div>
                    <div class="clear"></div>
				</li>
            </ul>
		</form>
   		</div>
    </div>
</section>
