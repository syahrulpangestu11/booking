<script type="text/javascript">
$(function(){

	function submitDataCallback(url, forminput){
		$.ajax({
			url: url,
			type: 'post',
			data: forminput.serialize(),
			success: function(data) {
				if(data == "1"){
					$dialogNotice.html("Data succesfully updated");
					$dialogNotice.dialog("open");
				}else{
					$dialogNotice.html("We&rsquo;re very sorry, we can't save your data right now"+data);
					$dialogNotice.dialog("open");
				}
			}
		});
	}


	var $dialogNotice = $('<div id="dialog-notice"></div>')
	.dialog({
		autoOpen: false,
		title: 'Notification',
		buttons: {
			"Ok": function(){
				$( this ).dialog( "close" );
				document.location.href = "<?php echo $base_url; ?>/all-agents";
			}
		}
	});

	/************************************************************************************/
	$('body').on('click','button.add-button', function(e) {
		url = "<?php echo $base_url; ?>/all-agents/add";
      	$(location).attr("href", url);
	});

	$('body').on('click','button.edit-button', function(e) {
		agenthoteloid = $(this).attr("agenthoteloid");
		url = "<?php echo $base_url; ?>/all-agents/edit/?aho="+agenthoteloid;
		$(location).attr("href", url);
	});

	$('button.submit-add').click(function(){
		url = '<?php echo $base_url; ?>/includes/agents/add-agent-save.php';
		forminput = $('form#data-input');
		submitDataCallback(url, forminput);
	});

	$('button.submit-edit').click(function(){
		url = '<?php echo $base_url; ?>/includes/agents/edit-agent-save.php';
		forminput = $('form#data-input');
		submitDataCallback(url, forminput);
	});

	/************************************************************************************/
	$('body').on('change','select[name=country]', function(e) {
		$.get("<?php echo $base_url; ?>/includes/agents/function-ajax.php", {
			func: "stateRqst",
			country: $('select[name=country]').val(),
			state: $('input[type=hidden][name=dfltstate]').val()
      	}, function(response){
			$('.loc-state').html(unescape(response));
			$('select[name=state]').change();
      	});
	});

	$('body').on('change','select[name=state]', function(e) {
		$.get("<?php echo $base_url; ?>/includes/agents/function-ajax.php", {
			func: "cityRqst",
			state: $('select[name=state]').val(),
			city: $('input[type=hidden][name=dfltcity]').val()
      	}, function(response){
			$('.loc-city').html(unescape(response));
      	});
	});

	/************************************************************************************/
	$('body').on('click','button.delete-button', function(e) {
		id = $(this).attr("agenthoteloid");
		$dialogDelete.data("url", "<?php echo $base_url; ?>/includes/agents/delete-agent.php");
		$dialogDelete.data("id", id);
		$dialogDelete.dialog("open");
	});

	/************************************************************************************/

	$(document).ready(function(){
		getLoadData();
		$('select[name=country]').change();
	});

	/************************************************************************************/
	function getLoadData(){
		$.ajax({
			url: "<?php echo"$base_url"; ?>/includes/agents/data.php",
			type: 'post',
			data: $('form#data-input').serialize()+"&usr=<?=$_SESSION['_typeusr']?>&hoid=<?=$_SESSION['_hotel']?>",
			success: function(data) {
				$("#data-box").html(data)
			}
		});
	}
	/************************************************************************************/
	function deleteAction(url, id){
		$.ajax({
			url: url,
			type: 'post',
			data: { id : id},
			success: function(data) {
				if(data == "1"){
					$dialogNotification.html("Data has been deleted");
					$dialogNotification.dialog("open");
				}else if(data == "2"){
					$dialogNotification.html("This data can't be deleted!<br>There are some hotels connected with this agent.");
					$dialogNotification.dialog("open");
				}else{
					$dialogNotification.html("We&rsquo;re very sorry, we can't save your data right now");
					$dialogNotification.dialog("open");
				}
			}
		});
	}


   var $dialogDelete = $('<div id="dialog-del"></div>')
	.html('Are you sure you want to delete this data?')
	.dialog({
		autoOpen: false,
		title: 'Confirmation',
		buttons: {
			"Delete": function(){
				var url = $(this).data('url');
				var id = $(this).data('id');
				$(this).dialog("close");
				deleteAction(url, id);
			},
			Cancel: function(){ $( this ).dialog("close"); }
		}
	});

	var $dialogNotification = $('<div id="dialog-notice"></div>')
	.dialog({
		autoOpen: false,
		title: 'Notification',
		buttons: {
			"Ok": function(){
				var callback = '<?php echo $base_url; ?>/all-agents';
				$(this).dialog( "close" );
				$(location).attr("href", callback);
			}
		}
	});

	/************************************************************************************/

	$('body').on('click','button.view_hotel', function(e) {
		var elem = $(this).parent().parent().next();
		if(elem.hasClass('hide')){
			elem.removeClass('hide');
		}else{
			elem.addClass('hide');
		}
	});

	$('body').on('click','button.unassign', function(e) {
		id = $(this).attr("agenthoteloid");
		$dialogDelete.data("url", "<?php echo $base_url; ?>/includes/agents/unassign-agent.php");
		$dialogDelete.data("id", id);
		$dialogDelete.dialog("open");
	});

});
</script>
