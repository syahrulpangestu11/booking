<?php
	$agentoid = $_GET['aho'];
	try {
		$stmt = $db->query("
		select a.agentoid, a.agentname, a.address, a.cityoid, a.website, a.email, a.publishedoid, a.phone, s.*, ct.*
		FROM agent a
        INNER JOIN published p ON a.publishedoid = p.publishedoid
        LEFT JOIN hotel h ON a.hotelcreatoroid = h.hoteloid
        LEFT JOIN users u ON a.useroid = u.useroid
        INNER JOIN city c ON a.cityoid = c.cityoid inner join state s using (stateoid) inner join country ct using (countryoid)
		where a.agentoid = '".$agentoid."'");
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
			$r_hotel = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_hotel as $row){
				$agentoid = $row['agentoid'];
				$agentname = $row['agentname'];
				$address = $row['address'];
				$website = $row['website'];
				$phone = $row['phone'];
				$email = $row['email'];
				$published = $row['publishedoid'];
				$country = $row['countryoid'];
				$state = $row['stateoid'];
				$city = $row['cityoid'];
			}
		}else{
			echo "No Result";
			die();
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}


?>
<section class="content-header">
    <h1>
        Agent
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-laptop"></i> Hotel</a></li>
        <li><?php echo $agentname; ?></li>
        <li class="active">Edit</li>
    </ol>
</section>
<section class="content">
	<div class="row">
        <div class="box box-form">
        	<form class="form-box" method="post" enctype="multipart/form-data" id="data-input" action="#">
			<input type="hidden" name="agentoid" value="<?php echo $agentoid; ?>">
            <input type="hidden" name="dfltstate" value="<?php echo $state; ?>">
            <input type="hidden" name="dfltcity" value="<?php echo $city; ?>">
            <h3>AGENT PROFILE</h3>
            <ul class="block">
                <li>
                    <span class="label">Name</span>
                    <input type="text" class="long" name="name" value="<?php echo $agentname; ?>">
				</li>
            </ul>

            <h3>AGENT LOCATION</h3>
            <ul class="block">
                <li>
                    <span class="label">Street Address</span>
                    <textarea name="address"><?php echo $address; ?></textarea>
				</li>
                <li>
                    <span class="label float-left" style="vertical-align:middle">&nbsp;</span>
                    <div class="side-left">
                        <ul class="inline-block">
                            <li>
                            	Country<br />
                                <select name="country" class="input-select">
									<?php getCountry($country); ?>
                                </select>
                            </li>
                            <li class="loc-state"></li>
                            <li class="loc-city"></li>
                        </ul>
                    </div>
                    <div class="clear"></div>
                </li>
            </ul>

            <h3>AGENT CONTACT</h3>
            <ul class="block">
                <li>
                    <span class="label">Main Phone Number</span>
                    <input type="text" class="medium" name="phone" value="<?php echo $phone; ?>">
				</li>
                <li>
                    <span class="label">Agent Website</span>
                    <input type="text" class="long" name="website" value="<?php echo $website; ?>">
				</li>
                <li>
                    <span class="label">Agent Email</span>
                    <input type="text" class="long" name="email" value="<?php echo $email; ?>">
				</li>
                <li>
                    <span class="label"><b>Publish Agent</b></span>
                    <select name="published">
                    <?php getPublished($published); ?>
                    </select>
                </li>
                <li>
                    <span class="label float-left" style="vertical-align:middle">&nbsp;</span>
                    <div class="side-left">
                        <div class="form-input">
							<button type="button" class="submit-edit">Submit</button>
                            <button type="reset">Reset</button>
                        </div>
                    </div>
                    <div class="clear"></div>
				</li>
            </ul>
		</form>
   		</div>
    </div>
</section>
