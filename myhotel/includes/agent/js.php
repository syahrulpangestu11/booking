<script type="text/javascript">
$(function(){

	function submitDataCallback(url, forminput){
		$.ajax({
			url: url,
			type: 'post',
			data: forminput.serialize(),
			success: function(data) {
				if(data == "1"){
					$dialogNotice.html("Data succesfully updated");
					$dialogNotice.dialog("open");
				}else{
					$dialogNotice.html("We&rsquo;re very sorry, we can't save your data right now"+data);
					$dialogNotice.dialog("open");
				}
			}
		});
	}


	var $dialogNotice = $('<div id="dialog-notice"></div>')
	.dialog({
		autoOpen: false,
		title: 'Notification',
		buttons: {
			"Ok": function(){
				$( this ).dialog( "close" );
				document.location.href = "<?php echo $base_url; ?>/agent";
			}
		}
	});

	/************************************************************************************/
	$('body').on('click','button.add-button', function(e) {
		url = "<?php echo $base_url; ?>/agent/add";
      	$(location).attr("href", url);
	});

	$('body').on('click','button.edit-button', function(e) {
		agenthoteloid = $(this).attr("agenthoteloid");
		url = "<?php echo $base_url; ?>/agent/edit/?aho="+agenthoteloid;
		$(location).attr("href", url);
	});

	$('button.submit-add').click(function(){
		url = '<?php echo $base_url; ?>/includes/agent/add-agent-save.php';
		forminput = $('form#data-input');
		submitDataCallback(url, forminput);
	});

	$('button.submit-edit').click(function(){
		url = '<?php echo $base_url; ?>/includes/agent/edit-agent-save.php';
		forminput = $('form#data-input');
		submitDataCallback(url, forminput);
	});

	// $('body').on('click','button.manage', function(e) {
	// 	var loginas_hc = $(this).attr('agenthoteloid');
	// 	var loginas_hname = $(this).parent().parent().children('td').eq(0).html();
	// 	$.ajax({
	// 		type	: 'POST', cache: false,
	// 		url		: '<?php echo"$base_url"; ?>/includes/agent/change-session.php',
	// 		data	: { loginas : loginas_hc, hname : loginas_hname },
	// 		success	: function(rsp){ if(rsp == "1"){ document.location.href = '<?php echo"$base_url"; ?>/dashboard'; } }
	// 	});
	// });
	$('body').on('click','button.manage', function(e) {
		agenthoteloid = $(this).attr("agenthoteloid");
		url = "<?php echo $base_url; ?>/agent-rate/edit/?aho="+agenthoteloid;
		$(location).attr("href", url);
	});
	$('body').on('click','button.creditfacility', function(e) {
		agenthoteloid = $(this).attr("agenthoteloid");
		url = "<?php echo $base_url; ?>/agent-creditfacility/edit/?aho="+agenthoteloid;
		$(location).attr("href", url);
	});
	/************************************************************************************/
	$('body').on('change','select[name=country]', function(e) {
		$.get("<?php echo $base_url; ?>/includes/agent/function-ajax.php", {
			func: "stateRqst",
			country: $('select[name=country]').val(),
			state: $('input[type=hidden][name=dfltstate]').val()
      	}, function(response){
			$('.loc-state').html(unescape(response));
			$('select[name=state]').change();
      	});
	});

	$('body').on('change','select[name=state]', function(e) {
		$.get("<?php echo $base_url; ?>/includes/agent/function-ajax.php", {
			func: "cityRqst",
			state: $('select[name=state]').val(),
			city: $('input[type=hidden][name=dfltcity]').val()
      	}, function(response){
			$('.loc-city').html(unescape(response));
      	});
	});

	/************************************************************************************/
	$('body').on('click','button.delete-button', function(e) {
		id = $(this).attr("agenthoteloid");
		$dialogDelete.data("url", "<?php echo $base_url; ?>/includes/agent/delete-agent.php");
		$dialogDelete.data("id", id);
		$dialogDelete.dialog("open");
	});

	/************************************************************************************/

	$(document).ready(function(){
		getLoadData();
		$('select[name=country]').change();
	});

	/************************************************************************************/
	function getLoadData(){
		$.ajax({
			url: "<?php echo"$base_url"; ?>/includes/agent/data.php",
			type: 'post',
			data: $('form#data-input').serialize()+"&usr=<?=$_SESSION['_typeusr']?>&hoid=<?=$_SESSION['_hotel']?>",
			success: function(data) {
				$("#data-box").html(data)
			}
		});
	}
	/************************************************************************************/
	function deleteAction(url, id){
		$.ajax({
			url: url,
			type: 'post',
			data: { id : id},
			success: function(data) {
				if(data == "1"){
					$dialogNotification.html("Data has been deleted");
					$dialogNotification.dialog("open");
				}else{
					$dialogNotification.html("We&rsquo;re very sorry, we can't save your data right now");
					$dialogNotification.dialog("open");
				}
			}
		});
	}


   var $dialogDelete = $('<div id="dialog-del"></div>')
	.html('Are you sure you want to delete this data?')
	.dialog({
		autoOpen: false,
		title: 'Confirmation',
		buttons: {
			"Delete": function(){
				var url = $(this).data('url');
				var id = $(this).data('id');
				$(this).dialog("close");
				deleteAction(url, id);
			},
			Cancel: function(){ $( this ).dialog("close"); }
		}
	});

	var $dialogNotification = $('<div id="dialog-notice"></div>')
	.dialog({
		autoOpen: false,
		title: 'Notification',
		buttons: {
			"Ok": function(){
				var callback = '<?php echo $base_url; ?>/agent';
				$(this).dialog( "close" );
				$(location).attr("href", callback);
			}
		}
	});

});
</script>
