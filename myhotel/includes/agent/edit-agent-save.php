<?php
	include("../../conf/connection.php");

	$agenthoteloid = $_POST['agenthoteloid'];
	$agentoid = $_POST['agentoid'];
	$name = $_POST['name'];
	$address = $_POST['address'];
	$phone = $_POST['phone'];
	$website = $_POST['website'];
	$email = $_POST['email'];
	$published = $_POST['published'];
	$agenttype = $_POST['agenttype'];

	$city = $_POST['city'];

	try {
		$stmt = $db->prepare("UPDATE agent SET agentname=:a, address=:b, phone=:c, website=:d, email=:e, publishedoid=:f, cityoid=:g, agenttypeoid=:h WHERE agentoid=:aoid");
		$stmt->execute(array(':a' => $name, ':b' => $address, ':c' => $phone, ':d' => $website, ':e' => $email, ':f' => $published, ':g' => $city, ':h' => $agenttype, ':aoid' => $agentoid));

		$stmt2 = $db->prepare("UPDATE agenthotel SET publishedoid=:h WHERE agenthoteloid=:ahoid");
		$stmt2->execute(array(':h' => $published, ':ahoid' => $agenthoteloid));

		$affected_rows = $stmt->rowCount();
	}catch(PDOException $ex) {
		echo "0";
		die();
	}
	echo "1";
?>
