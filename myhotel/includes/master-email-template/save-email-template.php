<script type="text/javascript">
$(function(){
   $("#dialog-error, #dialog-success").dialog({
      autoOpen: false,
	  buttons: { "Ok": redirect },
	  close: function(event, ui) { redirect }
   });

   function redirect(){
	   $(location).attr("href", "<?php echo $base_url; ?>/email-template");
   }
});

function openDialog($type,$message,$warning){
  var $dialog = $("#dialog-"+$type);
  $dialog.find(".description").empty();
  if($message!=undefined){ $dialog.find(".description").text($message); }
  if($warning!=undefined){ console.log($warning); }

  $(function(){ $( document ).ready(function(){ $dialog.dialog("open"); }); });
}
</script>

<div id="dialog-error" title="Error Notification">
  <p>We&rsquo;re very sorry, we can't save your data right now.</p><p class="description"></p>
</div>
<div id="dialog-success" title="Confirmation Notice">
  <p>Data succesfully updated</p><p class="description"></p>
</div>

<?php

// $masterprofileoid = $row['masterprofileoid'];
// $publishedoid = $row['publishedoid'];
// $email_type = $_POST['email_type'];
// $email_to = $_POST['email_to'];
$mastertemplate_emailoid = $_POST['mastertemplate_emailoid'];
$email_body = $_POST['email_body'];

    try {
      $stmt = $db->prepare("UPDATE mastertemplate_email SET
                                  email_body = :a, updated_at = :b, updated_by = :c
                            WHERE mastertemplate_emailoid = :id");
      $stmt->execute(array(':a' => $email_body, ':b' => date("Y-m-d H:i:s"), ':c' => $_SESSION['_user'], ':id' => $mastertemplate_emailoid));
  		?>
         <script>openDialog("success"); </script>
  		<?php
		}catch(PDOException $ex) {
  		?>
        <script>openDialog("error","",<?=json_encode($ex->getMessage());?>); </script>
  		<?php
		}
?>
