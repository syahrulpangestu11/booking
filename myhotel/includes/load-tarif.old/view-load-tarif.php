<?php
	if(empty($_REQUEST['rt']) and empty($_REQUEST['sdate']) and empty($_REQUEST['edate'])){
		$start = date("d F Y");
		$end = date("d F Y",  strtotime(date("Y-m-d")." +1 month" ));
	}else{
		$start = date("d F Y", strtotime($_REQUEST['sdate']));
		$end = date("d F Y", strtotime($_REQUEST['edate']));
		$roomtype = $_REQUEST['rt'];
	}
	include("js.php");
?>
<section class="content-header">
    <h1>
        Load Tarif
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Allotments &amp; Rates</a></li>
        <li class="active">Load Tarif</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="box">
            <div class="box-body">
                <div class="form-group">
                <form method="get" action="#">
                    <label>Room Type  : </label> &nbsp;
                    <select name="rt">
                        <?php
                        $sql = "select r.roomoid, r.name from room r inner join hotel h using (hoteloid) where h.hoteloid = '".$hoteloid."' order by r.name ";
                        $query = mysql_query($sql);
                        while($row = mysql_fetch_array($query)){
							if($row['roomoid'] == $roomtype){ $selected = 'selected = "selected"';  }else{ $selected = ''; }
                        ?>
                        <option value="<?php echo $row['roomoid']; ?>" <?php echo $selected; ?>><?php echo $row['name']; ?></option>
                        <?php		
                        }
                        ?>
                    </select>
                    <br><br>
                    <label>Date Range From </label> &nbsp;
                    <input type="text" name="sdate" id="startdate" placeholder="" value="<?php echo $start; ?>"> &nbsp;&nbsp;
                    <label>To </label> &nbsp;
                    <input type="text" name="edate" id="enddate" placeholder="" value="<?php echo $end; ?>"> &nbsp;&nbsp;                    
                    <button type="button" class="pure-button find-button">Find</button>
                </form>
                </div>
			</div><!-- /.box-body -->
       </div>
    </div>
    
        <div class="row">
        <div class="box">
            <div id="data-box" class="box-body">
            </div><!-- /.box-body -->
       </div>
    </div>

</section>