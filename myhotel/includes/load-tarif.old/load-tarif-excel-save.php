<?php
if ( isset($_FILES["loadtarif"])) {
	if ($_FILES["loadtarif"]["error"] > 0) {
		$message = "Error! Return Code: " . $_FILES["loadtarif"]["error"] . "<br />";
	}else {
		
		
		$ro = explode('-',$_POST['roomoffer']);
		$roomoid = $ro[0]; $roomofferoid = $ro[1];
		
		$channellist = array();
		foreach($_POST['channel'] as $channel){
			array_push($channellist , $channel);
		}

		$currency = '1';
		
		$source = $_FILES['loadtarif']['tmp_name'];
		$target = "files/" .date("Ymd"). $_FILES["loadtarif"]["name"];
		move_uploaded_file($source, $target);
		
		include 'lib/phpexcel/PHPExcel/IOFactory.php';
		$inputFileName = $target; 
		
		try {
			$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
		} catch(Exception $e) {
			die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		}
		
		$allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
		$arrayCount = count($allDataInSheet);
				
		try {
			
			$datetarif= array();
			$extendedpolicy= array();
			$single= array();
			$double= array();
			$extrabed= array();
			$allotment= array();
			$topup= array();
			$breakfast= array();
			$minstay= array();
			$maxstay =array();
			
			for($i=4; $i<=$arrayCount; $i+=1){
				$d_date = date("Y-m-d", strtotime(trim($allDataInSheet[$i]["A"])));
				$d_extendedpolicy = trim($allDataInSheet[$i]["B"]);
				$d_single = trim($allDataInSheet[$i]["C"]);
				$d_double = trim($allDataInSheet[$i]["D"]);
				$d_extrabed = trim($allDataInSheet[$i]["E"]);
				$d_allotment = trim($allDataInSheet[$i]["F"]);
				$d_topup = trim($allDataInSheet[$i]["G"]);
				$d_breakfast = trim($allDataInSheet[$i]["H"]);
				$d_minstay = trim($allDataInSheet[$i]["I"]);
				$d_maxstay = trim($allDataInSheet[$i]["J"]);
								
				/*validation*/
				if(isset($d_date)
					and isset($d_extendedpolicy)
					and isset($d_single)
					and isset($d_double)
					and isset($d_extrabed)
					and isset($d_allotment)
					and isset($d_topup)
					and isset($d_breakfast)
					and isset($d_minstay)
					and isset($d_maxstay)
					
					and is_numeric($d_single)
					and is_numeric($d_double)
					and is_numeric($d_extrabed)
					and is_numeric($d_allotment)
					and is_numeric($d_topup)
					and is_numeric($d_minstay)
					and is_numeric($d_maxstay)
				){
					
					array_push($datetarif, $d_date);
					array_push($extendedpolicy, $d_extendedpolicy);	
					array_push($single, $d_single);	
					array_push($double, $d_double);	
					array_push($extrabed, $d_extrabed);	
					array_push($allotment, $d_allotment);	
					array_push($topup, $d_topup);	
					array_push($breakfast, $d_breakfast);	
					array_push($minstay, $d_minstay);	
					array_push($maxstay, $d_minstay);	

					//echo $d_date." | ".$d_extendedpolicy." | ".$d_single." | ".$d_double." | ".$d_extrabed." | ".$d_allotment." | ".$d_topup." | ".$d_breakfast." | ".$d_minstay." | ".$d_minstay."<br>";
						
				}else{
					
					array_push($er_date, $d_date);
					array_push($er_extendedpolicy, $d_extendedpolicy);	
					array_push($er_single, $d_single);	
					array_push($er_double, $d_double);	
					array_push($er_extrabed, $d_extrabed);	
					array_push($er_allotment, $d_allotment);	
					array_push($er_topup, $d_topup);	
					array_push($er_breakfast, $d_breakfast);	
					array_push($er_minstay, $d_minstay);	
					array_push($er_maxstay, $d_minstay);	

				}
					
			}
			
			$path = "data-xml/tarif.xml";
			include("upload-excel.php");
						
		}catch(PDOException $ex) {
			echo $ex->getMessage();
			die();
		}
	}
	//header("Location: ". $base_url ."/uploadedworkorder/?dt=". $curdate);
	
} else {
	//header("Location: ". $base_url ."/newworkorder");
}
	
?>