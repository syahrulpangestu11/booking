<?php
  include("../../conf/connection.php");
  session_start();
  $hoteloid = $_SESSION['_hotel'];
  $stmt = $db->query("select hotelcode from hotel h where h.hoteloid = '".$hoteloid."'");
  $hotel = $stmt->fetch(PDO::FETCH_ASSOC);
  $hotelcode = $hotel['hotelcode'];

  /*---------------------------------------------------------------*/
  $path_xml_hotel =  "../../data-xml/".$hotelcode.".xml";
  $xml = new DomDocument();
      $xml->preserveWhitespace = false;
      $xml->load($path_xml_hotel);
  $xpath = new DomXpath($xml);
  $xml_file =  simplexml_load_file($path_xml_hotel);
  /*---------------------------------------------------------------*/

  $startdate = $_POST['startdate'];
  $enddate = $_POST['enddate'];

  $date1 = date_create($startdate); $date2 = date_create($enddate);
  $diff = date_diff($date1,$date2);
  $night = $diff->format("%a");

  $list_date = array();
  for($i = 0; $i < $night; $i++){
    $date = date("Y-m-d",strtotime($startdate." +".$i." day"));
    array_push($list_date, $date);
  }

  $stmt = $db->query("select r.roomoid, r.name from room r inner join published p using (publishedoid) inner join hotel h using (hoteloid)  where h.hoteloid = '".$hoteloid."' and p.publishedoid not in (3)");
  foreach($stmt->fetchAll(PDO::FETCH_ASSOC) as $room){

    foreach($list_date as $date){
      // get allotment of roomtype
      $query_tag_allotment = '//hotel[@id="'.$hoteloid.'"]/masterroom[@id="'.$room['roomoid'].'"]/rate[@date="'.$date.'"]/allotment/channel[@type="1"]/text()';
      $found_allotment = false;
      foreach($xml_file->xpath($query_tag_allotment) as $tagallotment){
        $allotment = (int)$tagallotment[0];
        $found_allotment = true;
      }
      // tag allotment exist on xml
      if($found_allotment == true){
        $stmt = $db->prepare("select ro.roomofferoid, (select mr.rate from masterrate mr inner join dynamicraterule dr using (masterrateoid) inner join dynamicrate d using (dynamicrateoid) where dr.roomofferoid = ro.roomofferoid and (:a <= allocation) and d.publishedoid = '1' order by allocation asc limit 1) as rate from roomoffer ro where ro.roomoid = :b having rate IS NOT NULL");
        $stmt->execute(array(':a' => $allotment, ':b' => $room['roomoid']));
				foreach($stmt->fetchAll(PDO::FETCH_ASSOC) as $dr){
					$query_tag_rate = '//hotel[@id="'.$hoteloid.'"]//masterroom[@id="'.$room['roomoid'].'"]/rate[@date="'.$date.'"]/rateplan[@id="'.$dr['roomofferoid'].'"]/channel[@type="1"]/double';
					$check_tag_rate = $xpath->query($query_tag_rate);
					$check_tag_rate->item(0)->nodeValue = round($dr['rate']);
        }
      }
    }
  }

  $xml->formatOutput = true;
  $xml->save($path_xml_hotel) or die("Error");
?>
