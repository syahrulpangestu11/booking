<?php
if(!file_exists($path_xml_hotel)){ createXML($path_xml_hotel); }

$xml = new DomDocument();
    $xml->preserveWhitespace = false;
    $xml->load($path_xml_hotel);
$xpath = new DomXpath($xml);

$root = $xml->documentElement;

/*------------------------------------------------------------------------------------*/
$query_tag_hotel = '//hotel[@id="'.$hoteloid.'"]';

	/* create <hotel id=@>---------------------------------------------------------*/
	$check_tag_hotel = $xpath->query($query_tag_hotel);
    if($check_tag_hotel->length == 0){
	    $hotel = $xml->createElement("hotel");
	        $hotel_id = $xml->createAttribute("id");
	        $hotel_id->value = $hoteloid;
	    $hotel->appendChild($hotel_id);
	    $root->appendChild($hotel);
		$check_tag_hotel = $xpath->query($query_tag_hotel);
    }
/*------------------------------------------------------------------------------------*/

// loop for room
$channeltype = $channel;

foreach ($preparedData as $keyRoom => $valueRoom) {
	foreach ($valueRoom as $keyRate => $valueRate) {
		foreach ($valueRate as $keyDate => $valueDate) {

			/*------------------------------------------------------------------------------------*/
			$roomoid = $valueDate['masterroom'];
			$query_tag_room = $query_tag_hotel.'//masterroom[@id="'.$roomoid.'"]';

				/* create <hotel id=@><masterroom id=@>-----------------------------------------*/
				$check_tag_room = $xpath->query($query_tag_room);
				if($check_tag_room->length == 0){
					$room = $xml->createElement("masterroom");
						$room_id = $xml->createAttribute("id");
						$room_id->value = $roomoid;
					$room->appendChild($room_id);
				
					$tag_hotel = $check_tag_hotel->item(0);
					$tag_hotel->appendChild($room);
					$check_tag_room = $xpath->query($query_tag_room);
				}
			/*------------------------------------------------------------------------------------*/
			$elementName = array("extendedpolicy", "netsingle", "netdouble", "netextrabed", "single", "double", "extrabed", "currency", "topup",
								"breakfast", "minstay", "maxstay", "surcharge", "blackout", "cta", "ctd");
			
			$anstrue = array('1','Yes','True','Y','y');
			
			$valueDate['breakfast'] = in_array($valueDate['breakfast'], $anstrue) ? 'y' : 'n';
			$valueDate['surcharge'] = in_array($valueDate['surcharge'], $anstrue) ? 'y' : 'n';
			$valueDate['blackout'] = in_array($valueDate['blackout'], $anstrue) ? 'y' : 'n';
			$valueDate['cta'] = in_array($valueDate['cta'], $anstrue) ? 'y' : 'n';
			$valueDate['ctd'] = in_array($valueDate['ctd'], $anstrue) ? 'y' : 'n';
			$valueDate['closeout'] = in_array($valueDate['closeout'], $anstrue) ? 'y' : 'n';
			$valueDate['netsingle'] = empty($valueDate['netsingle']) ? "0" : $valueDate['netsingle'];
			$valueDate['netdouble'] = empty($valueDate['netdouble']) ? "0" : $valueDate['netdouble'];
			$valueDate['single'] = empty($valueDate['single']) ? "0" : $valueDate['single'];
			$valueDate['double'] = empty($valueDate['double']) ? "0" : $valueDate['double'];

			$elementNodes = array($valueDate['extendedpolicy'],
								$valueDate['netsingle'],
								$valueDate['netdouble'],
								$valueDate['netextrabed'],
								$valueDate['single'],
								$valueDate['double'],
								$valueDate['extrabed'],
								$valueDate['currency'],
								$valueDate['topup'],
								$valueDate['breakfast'],
								$valueDate['minstay'],
								$valueDate['maxstay'],
								$valueDate['surcharge'],
								$valueDate['blackout'],
								$valueDate['cta'],
								$valueDate['ctd']);
			/*------------------------------------------------------------------------------------*/

			/* create <hotel id=@><masterroom id=@><rate date=@ day=@>----------------------*/
			$date = $valueDate['ratedate'];
			$day = $valueDate['rateday'];
			$query_tag_rate = $query_tag_room.'/rate[@date="'.$date.'"]';
			$check_tag_rate = $xpath->query($query_tag_rate);
		
			if($check_tag_rate->length == 0){
				$rate = $xml->createElement("rate");
					$rate_date = $xml->createAttribute("date");
					$rate_date->value = $date;
				$rate->appendChild($rate_date);
					$rate_day = $xml->createAttribute("day");
					$rate_day->value = $day;
				$rate->appendChild($rate_day);
					
				$tag_room = $check_tag_room->item(0);
				$tag_room->appendChild($rate);
				$check_tag_rate = $xpath->query($query_tag_rate);
			}
			
			$tag_rate = $check_tag_rate->item(0);

			/* allotment */
			$query_tag_allotment = $query_tag_rate.'/allotment';
			$check_tag_allotment = $xpath->query($query_tag_allotment);
			
			if($check_tag_allotment->length == 0){
				$tag_allotment = $xml->createElement("allotment");
				$tag_rate->appendChild($tag_allotment);
				
				$check_tag_allotment = $xpath->query($query_tag_allotment);
			}
			
			$tag_allotment = $check_tag_allotment->item(0);

			$allotment = $valueDate['allotment'];
			$query_tag_channel = $query_tag_allotment.'/channel[@type="'.$channeltype.'"]';
			$check_tag_channel = $xpath->query($query_tag_channel);
			
			$channel = $xml->createElement("channel");
				$channel_type = $xml->createAttribute("type");
				$channel_type->value = $channeltype;
				$channel->appendChild($channel_type);
			$channel_allotment = $xml->createTextNode($allotment);
				$channel->appendChild($channel_allotment);
			
			if($check_tag_channel->length == 0){
				$tag_allotment->appendChild($channel);
			}else{
				$tag_element = $check_tag_channel->item(0);	
				$tag_allotment->replaceChild($channel,$tag_element);
			}

			/* closeout of room */
			$query_tag_closeout = $query_tag_rate.'/closeout';
			$check_tag_closeout = $xpath->query($query_tag_closeout);
			
			$closeout = $valueDate['closeout'];
			if($check_tag_closeout->length == 0){
				$tag_closeout = $xml->createElement("closeout", $closeout);
				$tag_rate->appendChild($tag_closeout);
			}else{
				$check_tag_closeout->item(0)->nodeValue = $closeout;
			}

			/* rateplan */
			$roomofferoid = $valueDate['rateplan'];
			$query_tag_rateplan = $query_tag_rate.'/rateplan[@id="'.$roomofferoid.'"]';
			$check_tag_rateplan = $xpath->query($query_tag_rateplan);
			
			if($check_tag_rateplan->length == 0){
				$rateplan = $xml->createElement("rateplan");
					$rateplan_id = $xml->createAttribute("id");
					$rateplan_id->value = $roomofferoid;
				$rateplan->appendChild($rateplan_id);
					
				if($check_tag_rateplan->length == 0){
					$tag_rate->appendChild($rateplan);
					$check_tag_rateplan = $xpath->query($query_tag_rateplan);
				}
			}

			$tag_rateplan = $check_tag_rateplan->item(0);

			$query_tag_rateplan_channel = $query_tag_rateplan.'/channel[@type="'.$channeltype.'"]';
			$check_tag_rateplan_channel = $xpath->query($query_tag_rateplan_channel);
					
			if($check_tag_rateplan_channel->length == 0){
				$rateplan_channel = $xml->createElement("channel");
					$rateplan_channel_type = $xml->createAttribute("type");
					$rateplan_channel_type->value = $channeltype;
				$rateplan_channel->appendChild($rateplan_channel_type);
				
				$tag_rateplan->appendChild($rateplan_channel);
				$check_tag_rateplan_channel = $xpath->query($query_tag_rateplan_channel);
			}
			
			$tag_rateplan_channel = $check_tag_rateplan_channel->item(0);
				
			foreach($elementName as $key => $value){
				$element = $value;
				$node = $elementNodes[$key];	
					
				$createElement = $xml->createElement($element);
					$createNode = $xml->createTextNode($node);
				$createElement->appendChild($createNode);
					
				$check_tag_element = $xpath->query($query_tag_rateplan_channel.'/'.$element);
					
				if($check_tag_element->length == 0){
					$tag_rateplan_channel->appendChild($createElement);
				}else{
					$tag_element = $check_tag_element->item(0);
					$tag_rateplan_channel->replaceChild($createElement,$tag_element);
				}
			}

			/*------------------------------------------------------------------------------------*/
		}
	}
}

$xml->formatOutput = true;
//echo "<xmp>". $xml->saveXML() ."</xmp>";

$xml->save($path_xml_hotel) or die("Error");
?>