<?php

$hoteloid = $_SESSION['_hotel'];



try {

    $stmt = $db->query("select hotelname from hotel where hoteloid = '".$hoteloid."'");

    $row_count = $stmt->rowCount();

    if($row_count > 0) {

        $r_hotel = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach($r_hotel as $row){

            $hotelname = $row['hotelname'];

        }

    }else{

        echo "No Result";

        die();

    }

    

    $stmt = $db->query("select * from currency where publishedoid='1'");

    $r_currency = $stmt->fetchAll(PDO::FETCH_ASSOC);

    

    $stmt = $db->query("select * from channel");

    $r_channel = $stmt->fetchAll(PDO::FETCH_ASSOC);

	

	$default_tmpl = '1';

	$default_curr = '1';

	$default_chan = '1';

    

	$stmt = $db->query("SELECT `log_import`.*, `users`.`displayname` FROM `log_import` INNER JOIN `users` USING (`useroid`) WHERE `log_import`.`hoteloid`='".$hoteloid."'");

    $r_log = $stmt->fetchAll(PDO::FETCH_ASSOC);
}catch(PDOException $ex) {

    echo "Invalid Query";

    die();

}

?>

<section class="content-header">

    <h1>

        Import External Data

    </h1>

    <ol class="breadcrumb">

        <li><a href="#"><i class="fa fa-table"></i> Settings</a></li>

        <li class="active">Import External Data</li>

    </ol>

</section>

<section class="content">

    <div class="row">

        <div class="box box-form">

        <form method="post" enctype="multipart/form-data" action="<?php echo $base_url; ?>/import-external/mapping-field">

        <table>

            <tr>

                <td style="padding-right:20px;">Hotel ID</td>

                <td>:</td>

                <td><?=$hoteloid?></td>

            </tr>

            <tr>

                <td>Hotel Name</td>

                <td>:</td>

                <td><?=$hotelname?></td>

            </tr>

            <tr>

                <td colspan="3"><hr/></td>

            </tr>

            <tr>

                <td>Data Source</td>

                <td>:</td>

                <td><select name="partner">

                    <option value="">-- Select Source --</option>

                    <option value="1" selected="selected">Agoda</option>   

                </select></td>

            </tr>

            <tr>

                <td>Rate Currency</td>

                <td>:</td>

                <td><select name="currency">

                    <option value="">-- Select Currency --</option>

                    <?php

                    foreach($r_currency as $row){

                        $currencyoid = $row['currencyoid'];

                        $currencycode = $row['currencycode'];

                        $currencytitle = $row['currencytitle'];



						if($currencyoid == $default_curr){

                        	echo '<option value="'.$currencyoid.'" selected="selected">'.$currencycode.' - '.$currencytitle.'</option>';

						}else{

							echo '<option value="'.$currencyoid.'">'.$currencycode.' - '.$currencytitle.'</option>';

						}

                    }

                    ?>

                </select></td>

            </tr>

            <tr>

                <td>Channel</td>

                <td>:</td>

                <td><select name="channel">

                    <option value="">-- Select Channel --</option>

                    <?php

                    foreach($r_channel as $row){

                        $channeloid = $row['channeloid'];

                        $channelname = $row['name'];

						

						if($channeloid == $default_chan){

                        	echo '<option value="'.$channeloid.'" selected="selected">'.$channelname.'</option>';

						}else{

							echo '<option value="'.$channeloid.'">'.$channelname.'</option>';

						}

                    }

                    ?>

                </select></td>

            </tr>

            <tr>

                <td colspan="3"><hr/></td>

            </tr>

            <tr>

                <td>File to Upload</td>

                <td>:</td>

                <td><input type="file" name="sourcefile" /></td>

            </tr>

            <tr>

                <td></td>

                <td></td>

                <td><br><button type="submit" class="small-button blue">Next</button></td>

            </tr>

        </table>

        </form>

        </div>

    </div>

    <div class="row">

    	<table class="table table-fill table-fill-centered">

        <tr>

            <td>Date</td>

            <td>Source Filename</td>

            <td>Generated Filename</td>

            <td>Import by</td>

        </tr>

        <?php

		foreach($r_log as $row){

			$sfilename = substr($row['sfilename'],12,strlen($row['sfilename'])-12);

			$gfilename = $row['gfilename'];

			$importdate = $row['importdate'];

			$displayname = $row['displayname'];



			echo '<tr><td>'.$importdate.'</td><td>'.$sfilename.'</td><td>'.$gfilename.'</td><td>'.$displayname.'</td></tr>';

		}

		?>

        </table>

    </div>

</section>