<?php
$stmt = $db->prepare("INSERT INTO `log_import`(`logoid`, `sfilename`, `gfilename`, `importdate`, `useroid`,`hoteloid`) VALUES (NULL,:a,:b,:c,:d,:e)");
$stmt->execute(array(':a' => $src_filename, ':b' => $xml_filename, ':c' => date('Y-m-d H:i:s'), ':d' => $_SESSION['_oid'], ':e' => $hoteloid));
$logimportoid = $db->lastInsertId();
?>