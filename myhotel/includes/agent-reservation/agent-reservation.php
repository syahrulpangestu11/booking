<?php include('parameter.php'); ?>
<section class="content-header">
    <h1>
        Agent Reservation
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-user-o"></i> Settings</a></li>
        <li class="active"> Agent Reservation</li>
    </ol>
</section>
<section class="content">
  <div class="row">
      <div class="box">
          <div class="box-body">
              <div class="form-group">
              <form method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/agent-reservation/checkavailability">
                  <label>Arrival</label>
                  &nbsp;
                  <input type="text" name="sdate" id="startdate" placeholder="" value="<?php echo $show_checkin; ?>">
                  &nbsp;&nbsp;
                  <label>Departure </label>
                  &nbsp;
                  <input type="text" name="edate" id="enddate" placeholder="" value="<?php echo $show_checkout; ?>">
                  &nbsp;&nbsp;
                  <label>Room(s) </label>
                  &nbsp;
                  <select name="room" style="width: 53px;">
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                  </select>
                  &nbsp;&nbsp;
                  <label>Person(s)</label>
                  &nbsp;
                  <select name="person" style="width: 60px;">
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                  </select>
                  <button type="submit" class="small-button blue"><i class="fa fa-search"></i>Check Availability</button>
              </form>
              </div>
          </div><!-- /.box-body -->
     </div>
  </div>
  <div class="row">
      <div class="box">
          <div class="box-body">
          <?php include('check-availability.php'); ?>
          </div>
      </div>
  </div>
</section>