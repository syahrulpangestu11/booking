<?php
    $hoteloid = $_SESSION['_hotel'];
	if(!isset($_SESSION['_user']) or !isset($_SESSION['_oid'])){
		echo"<script>window.location.href = '".$base_url."/login'</script>";
		die();
	}else{
		if (!isset($_SESSION['CREATED'])) {
			$_SESSION['CREATED'] = time();
		} else if (time() - $_SESSION['CREATED'] > 3600) {
			// session started more than 1 hour ago
			unset($_SESSION['_user']);
			unset($_SESSION['_initial']);
			unset($_SESSION['_oid']);
			session_destroy();
			
			echo"<script>window.location.href = '".$base_url."/login'</script>";
			die();
		}else{
			//session_regenerate_id(true);    // change session ID for the current session and invalidate old session ID
			$_SESSION['CREATED'] = time();  // update creation time
		}
	}
?>