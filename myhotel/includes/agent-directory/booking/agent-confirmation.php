<section class="content-header">
    <h1>Reservation Confirmation</h1>
    <ol class="breadcrumb">
        <tr><a href="#"><i class="fa fa-user-o"></i> Reservation</a></tr>
    </ol>
</section>
<?php
	$bookingnumber	= $_REQUEST['bookingnumber'];
	$s_master_booking	= "select b.*, cr.*  
	from booking b
	inner join bookingstatus bs using (bookingstatusoid) 
	inner join currency cr using (currencyoid) 
	where b.bookingnumber = '".$bookingnumber."' group by b.bookingoid";
	$stmt				= $db->query($s_master_booking);
	$r_master_booking	= $stmt->fetchAll(PDO::FETCH_ASSOC);
	foreach($r_master_booking as $databooked){
		$bookingoid		= $databooked['bookingoid'];
		$bookingnumber	= $databooked['bookingnumber'];
		$grandtotal		= $databooked['grandtotal'];
		$grandbalance	= $databooked['grandbalance'];
		$currency		= $databooked['currencycode'];		
		$paymentoid		= $databooked['paymentoid'];
	}
	
	$s_detail_hotel = "select DISTINCT(hotel) from bookingroom br where br.bookingoid = '".$bookingoid."' limit 1";
	$stmt			= $db->query($s_detail_hotel);
	$detail_hotel	= $stmt->fetch(PDO::FETCH_ASSOC);
		$hotelname = $detail_hotel['hotel'];
?>
<section class="content">
	<div class="box">
        <div class="row">
        	<div class="col-md-12">
				<h2>Thank you for your reservation.</h2>
				<h3>We already accept your booking at <?=$hotelname?> with bookingnumber : <?=$bookingnumber;?></h3>
	
                <div class="panel panel-warning">
                	<div class="panel-heading">Below your reservation details and information.</div>
                    <div class="panel-body">
                    <table>
                        <tr>
                            <td>Booking Number</td>
                            <td><?=$bookingnumber?></td>
                        </tr>
                        <tr>
                            <td>Grand Total </td>
                            <td><?=$currency?> <?=number_format($grandtotal)?></td>
                        </tr>
                        <tr>
                            <td>Deposit </td>
                            <td><?=$currency?> <?=number_format($deposit)?></td>
                        </tr>
                        <tr>
                            <td>Paid at hotel </td>
                            <td><?=$currency?> <?=number_format($balance)?></td>
                        </tr>
                    </table>
                    </div>
				</div>
            </div>
        </div>
    </div>
</section>
