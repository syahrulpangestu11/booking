<script type="text/javascript">
$(function(){

function showInventory(date, request){
	box = $('div#show-inventory');
	hotelcode = $('select[name=hotelcode]').find(":selected").val();
	$.ajax({
		url: "<?php echo"$base_url"; ?>/includes/chain-availability-lite/data-chain-availability.php",
		type: 'post',
		data: { lastdate: date, request: request, hotelcode : hotelcode },
		success: function(data) {
			    box.html(data);
		}
	});
}

$(document).on('change','input[name=date]', function(e){
	showInventory($(this).val(), 'today');
});

$(document).on('change','select[name=hotelcode]', function(e){
	date = $('input[name=spotdate]').val();
	showInventory(date, 'today');
});

$(document).ready(function(){
	showInventory('<?=date('Y-m-d');?>', 'today');
});

$(document).on('click','button.inv-nav', function(e){
	date = $('input[name=spotdate]').val();
	request = $(this).val();
	showInventory(date, request);
});

$(document).ready(function(){
	showInventory('<?=date('Y-m-d');?>', 'today');
});

$( "#datepicker" ).datepicker({
	changeMonth: true,
	changeYear: true,
});

/*------------------------------------------------------------*/

$(document).on('click','span.clickable', function(e){
	inputbox = $(this).next('input[type=text]');
	inputbox.show();
	$(this).hide();
});

$(document).on('change','table#inventory input[type=text]', function(e){
	var request = $(this).attr('name');
	var value = $(this).val();
	var isValid = false;

	if ($(this).hasClass("rate")) {
		var rate = parseFloat($(this).val());
		var minrate = parseFloat($(this).data("minrate"));
		if(rate < minrate){
			alert("Minimum rate for this room type are "+minrate+".");
			$(this).css( "background-color", "#ff9f9f");
			isValid = false;
		}else{
			$(this).css( "background-color", "#ffffff");
			isValid = true;
		}
	}else{
		isValid = true;
	}

	if(isValid == true){
		changeInventory($(this), request, value, 'text');
	}
});


$(document).on('click','div[node="closeout"], div[node="blackout"]', function(e){
	var request = $(this).attr('node');
	if($(this).hasClass('open')){
		var value = 'y';
	}else{
		var value = 'n';
	}
	changeInventory($(this), request, value, 'div');
});

$(document).on('click','input[type=checkbox]', function(e){
	var request = $(this).attr('name');
	if($(this).is(':checked')){
		var value = 'y';
	}else{
		var value = 'n';
	}
	changeInventory($(this), request, value, 'div');
});

function changeInventory(elem, request, value, typeElement){
	var parameter = elem.parent('td').attr('data-param');
	var existed = elem.parent('td').attr('data-existed');
	var hcode = elem.parent('td').attr('data-hcode');
	$.ajax({
		url: "<?php echo"$base_url"; ?>/includes/inventory/save.php",
		type: 'post',
		data: { hcode : hcode, parameter: parameter, existed: existed, request: request, value: value  },
		success: function(response) {
			alert(response);
			if(response == "success"){
				if(typeElement == "text"){
					elem.hide();
					elem.prev('span').html(value);
					elem.prev('span').show();
				}else if(typeElement == "div"){
					if(value == "y"){
						elem.removeClass('open'); elem.addClass('close');
					}else{
						elem.removeClass('close'); elem.addClass('open');
					}
				}
			}
		}
	});
}


});
</script>
