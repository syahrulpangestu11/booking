<?php
  session_start();
  include("../ajax-include-file.php");

  $lastdate = $_POST['lastdate'];
  $channel = 1;
  switch($_POST['request']){
  	case 'next' : $begin = 1 ; $end = 30; break;
  	case 'forward' : $begin = 10 ; $end = 39; break;
  	case 'previous' : $begin = -1 ; $end = 28; break;
  	case 'backward' : $begin = -10 ; $end = 19; break;
  	default :  $begin = 0; $end = 29; break;
  }

  $showDay = array(); $showDate = array(); $showMonth = array(); $dateArr = array();

  for($i = $begin; $i <= $end; $i++){
  	$date = date("Y-m-d",strtotime($lastdate." +".$i." day"));
  	$dateformated = date("D-d-M",strtotime($lastdate." +".$i." day"));
  	$exploDate = explode("-", $dateformated);

  	array_push($dateArr, $date);
  	array_push($showDay, $exploDate[0]);
  	array_push($showDate, $exploDate[1]);
  	array_push($showMonth, $exploDate[2]);
  }

  /*----------------------------------------------------------------------*/

  $s_hotelchain = "select h.hoteloid, h.hotelcode, h.hotelname from hotel h inner join chain c using (chainoid) where h.chainoid in (select oid from userassign where type = 'chainoid' and useroid = '".$_SESSION['_oid']."') and h.publishedoid = '1'";
  $filter = array();
  if(!empty($_POST['hotelcode'])){
    array_push($filter, "h.hotelcode = '".$_POST['hotelcode']."'");
  }
  if(count($filter) > 0){
		$combine_filter = implode(' and ',$filter);
		$s_hotelchain = $s_hotelchain.' and '.$combine_filter;
	}
  $q_hotelchain = $db->query($s_hotelchain);
  $r_hotelchain = $q_hotelchain->fetchAll(PDO::FETCH_ASSOC);
  foreach($r_hotelchain as $hotelchain){
    ${$hotelchain['hotelcode']} =  simplexml_load_file("../../data-xml/".$hotelchain['hotelcode'].".xml");
  }

  /*--------------------------------------------------------------------*/

  function getCloseOutRTXML($hotel, $hotelcode, $room, $date, $channel){
  	global $db; global ${$hotelcode};
    $closeout = 'n'; $existtag = 0;

  	$query_tag = '//hotel[@id="'.$hotel.'"]/masterroom[@id="'.$room.'"]/rate[@date="'.$date.'"]/closeout';
  	foreach(${$hotelcode}->xpath($query_tag) as $co){
  		$closeout = $co;
  		$existtag = 1;
  	}

  	return array($closeout, $existtag);
  }
  function getAllocationXML($hotel, $hotelcode, $room, $date, $channel){
  	global $db; global ${$hotelcode};
  	$allocation =0; $existtag = 0;

  	$query_tag = '//hotel[@id="'.$hotel.'"]/masterroom[@id="'.$room.'"]/rate[@date="'.$date.'"]/allotment/channel[@type="'.$channel.'"]';
  	foreach(${$hotelcode}->xpath($query_tag) as $allotment){
  		$allocation = $allotment;
  		$existtag = 1;
  	}
  	return array($allocation, $existtag);
  }

  function showDataXML($hotel, $hotelcode, $room, $date, $roomoffer, $channel){
  	global $db; global ${$hotelcode};
  	$rate=0; $closetime='';  $breakfast='n'; $cta='n'; $ctd='n'; $existtag = 0;

  	$query_tag = '//hotel[@id="'.$hotel.'"]/masterroom[@id="'.$room.'"]/rate[@date="'.$date.'"]/rateplan[@id="'.$roomoffer.'"]/channel[@type="'.$channel.'"]';
  	foreach(${$hotelcode}->xpath($query_tag) as $rowXml){
  		$rate = $rowXml->double;
  		$closetime = $rowXml->blackout;
  		$breakfast = $rowXml->breakfast;
  		$cta = $rowXml->cta;
  		$ctd = $rowXml->ctd;
  		$existtag = 1;
  	}

  	return array($rate, $closetime, $breakfast, $cta, $ctd, $existtag);
  }

  if(strtotime($dateArr[0]) >= strtotime(date('Y-m-d 00:00:00'))){
?>
  <table id="inventory" cellpadding="0" cellspacing="0">
    <thead>
      <tr>
        <th>&nbsp;</th>
        <?php
        foreach($dateArr as $key => $date){
          if($key == 0){
        ?>
        <input type="hidden" name="spotdate" value="<?=$date?>" />
        <?php
          }
        ?>
        <th class="text-center"><?php echo $showDay[$key]; ?><br /><?php echo $showDate[$key]; ?><br /><?php echo $showMonth[$key]; ?></th>
        <?php
        }
        ?>
      </tr>
    </thead>
    <tbody>
    <?php
    foreach($r_hotelchain as $hotelchain){
    ?>
      <tr class="hotel"><td colspan="31"><?php echo $hotelchain['hotelname']; ?></td></tr>
      <?php
      $s_roomtype = "select r.roomoid, r.name from room r inner join published p using (publishedoid) inner join hotel h using (hoteloid)  where h.hotelcode = '".$hotelchain['hotelcode']."' and p.publishedoid not in (3)";
      $q_roomtype = $db->query($s_roomtype);
      $r_roomtype = $q_roomtype->fetchAll(PDO::FETCH_ASSOC);
      foreach($r_roomtype as $roomtype){
      ?>
      <tr class="roomtype">
        <td><?php echo $roomtype['name']; ?></td>
        <?php
        foreach($dateArr as $date){
          $getCloseOutRT = getCloseOutRTXML($hotelchain['hoteloid'], $hotelchain['hotelcode'], $roomtype['roomoid'], $date, $channel);
          if($getCloseOutRT[0] == "y"){ $class_co = "close"; }else{ $class_co = "open"; }
        ?>
        <td data-hcode="<?=$hotelchain['hotelcode']?>" data-param="<?=$hotelchain['hoteloid'].'_'.$roomtype['roomoid'].'_'.$date.'_'.$channel?>" data-existed="<?php echo $getCloseOutRT[1]; ?>"><div node="closeout" class="closeout <?=$class_co?>">&nbsp;</div></td>
        <?php
        }
        ?>
      </tr>
      <tr>
        <td>Allocation</td>
        <?php
        foreach($dateArr as $date){
          $getAllocation = getAllocationXML($hotelchain['hoteloid'], $hotelchain['hotelcode'], $roomtype['roomoid'], $date, $channel);
        ?>
        <td <?php if($getAllocation[0]==0){echo 'class="bg-s0"';}?> data-hcode="<?=$hotelchain['hotelcode']?>" data-param="<?=$hotelchain['hoteloid'].'_'.$roomtype['roomoid'].'_'.$date.'_'.$channel?>" data-existed="<?php echo $getAllocation[1]; ?>"><span class="clickable"><?php echo $getAllocation[0]; ?></span><input type="text" name="allotment" value="<?php echo $getAllocation[0]; ?>" /></td>
        <?php
        }
        ?>
      </tr>
        <?php
        $s_rateplan = "select ro.roomofferoid, ro.offertypeoid, ro.name from roomoffer ro inner join published p using (publishedoid) inner join room r using (roomoid) inner join offertype using (offertypeoid) where r.roomoid = '".$roomtype['roomoid']."' and p.publishedoid = '1'";
        $q_rateplan = $db->query($s_rateplan);
        $r_rateplan = $q_rateplan->fetchAll(PDO::FETCH_ASSOC); $aroid = array(); $used = array();
        foreach($r_rateplan as $rateplan){
          $roid = $rateplan['roomofferoid'];
          $rate = array(); $closeout = array(); $used[$roid] = array(); $existedtag = array(); $parameter = array();
          foreach($dateArr as $key => $date){
            $getData = showDataXML($hotelchain['hoteloid'], $hotelchain['hotelcode'], $roomtype['roomoid'], $date, $rateplan['roomofferoid'], $channel);
            array_push($rate, $getData[0]);
    				array_push($closeout, $getData[1]);
            array_push($existedtag, $getData[5]);

            $s_used = "select count(bookingroomdtloid) as used from bookingroomdtl brd inner join bookingroom br using (bookingroomoid) inner join booking b using (bookingoid) where br.roomofferoid = '".$rateplan['roomofferoid']."' and brd.date = '".$date."' and b.bookingstatusoid = '4' and brd.reconciled = '0'";
            $stmt = $db->query($s_used);
            $r_used = $stmt->fetch(PDO::FETCH_ASSOC);
            array_push($used[$roid], $r_used['used']);

            array_push($parameter, $hotelchain['hoteloid'].'_'.$roomtype['roomoid'].'_'.$date.'_'.$channel.'_'.$rateplan['roomofferoid']);
          }
        ?>
      <?php /* ?>
      <tr class="roomoffer">
        <td><?php echo $rateplan['name']; ?></td>
        <?php
        foreach($closeout as $key => $value_closeout){
          if($value_closeout == "y"){ $class_co = "close"; }else{ $class_co = "open"; }
        ?>
        <td data-hcode="<?=$hotelchain['hotelcode']?>" data-param="<?=$parameter[$key]?>" data-existed="<?php echo $existedtag[$key]; ?>"><div node="blackout" class="closeout <?=$class_co?>">&nbsp;</div></td>
        <?php
        }
        ?>
      </tr>
      <tr>
        <td>Used</td>
        <?php foreach($used as $key => $value_used){ ?>
        <td><?php echo $value_used; ?></td>
        <?php } ?>
      </tr>
      <tr>
        <td>Rate</td>
        <?php foreach($rate as $key => $value_rate){ ?>
        <td data-hcode="<?=$hotelchain['hotelcode']?>" data-param="<?=$parameter[$key]?>" data-existed="<?php echo $existedtag[$key]; ?>">
          <span class="clickable"><?php echo $value_rate; ?></span>
          <input type="text" name="double" class="rate" data-minrate="<?=$minrate;?>" value="<?php echo $value_rate; ?>" />
        </td>
        <?php } ?>
      </tr>
      <?php */ ?>
    <?php
        array_push($aroid, $roid);
        }
        ?>
    
      <tr>
        <td>Used</td>
        <?php 
        foreach($dateArr as $key => $date){ 
            $usd = 0;
            foreach($aroid as $roid){
                $usd += $used[$roid][$key];
            }
            ?>
            <td <?php if($usd==0){echo 'class="bg-s0"';}?>><?php echo $usd; ?></td>
        <?php 
        }
        ?>
      </tr>
        
    <?php
      }
      ?>
    <?php
    }
    ?>
    </tbody>
  </table>
<?php
  }
?>
