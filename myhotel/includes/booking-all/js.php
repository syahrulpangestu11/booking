<script type="text/javascript">
$(function(){
	$(document).ready(function(){ 
		getLoadData();
	});
	
	function getLoadData(){
		$.ajax({
			url: "<?php echo"$base_url"; ?>/includes/booking-all/data.php",
			type: 'post',
			data: $('form#data-input').serialize(),
			success: function(data) {
				$("#data-box").html(data)
			}
		});
	}	

	$('body').on('click','tr.list', function(e) {	
		// bn = $(this).children('td').eq(0).html();
		bn = $(this).find('.open-button').attr("uo");
		$(location).attr("href", "<?php echo"$base_url"; ?>/booking-all/detail/?no="+bn);
	});
	$('body').on('click','.open-button', function(e) {
		uo = $(this).attr("uo");
		url = "<?php echo $base_url; ?>/booking-all/detail/?no="+uo;
		$(location).attr("href", url);
	});
	$('body').on('click','.confirm-button', function(e) {
		uo = $(this).attr("uo");
		url = "<?php echo $base_url; ?>/booking-all/confirmation/?no="+uo;
		$(location).attr("href", url);
	});
	
	$('body').on('click','button.opencc', function(e) {	
		window.open("<?php echo"$base_url"; ?>/paymentdetail?bookingnumber=<?=$_REQUEST['no']?>", "Payment Detail", "width=500, height=400, directories=no, titlebar=no, toolbar=no, location=no, status=no, menubar=no");
		return false;
	});

	$('body').on('click','button[act="remove-room"]', function(e) {	
		table = $(this).parent('td').parent('tr').parent('tbody').parent('table');
		tr = $(this).parent('td').parent('tr');
		
		checkout = tr.children('td').eq(0).text();
		table.find('input[name="checkout[]"]').val(checkout);
		
		tr.remove();
		
		var totalroom = 0;
		$(table.children('tbody').children('tr').children('td.nighttotal')).each(function() {
			nighttotal = parseFloat($(this).text().replace(/,/g, ''));
			totalroom = totalroom + nighttotal;
		});

		if(table.children("tbody").children("tr").size() > 3){
			table.find("tr").eq(-3).children('td').eq(3).html('<button type="button" class="btn btn-danger btn-xs" act="remove-room">Remove Room</button>');
		}
		
		table.find('input[name="totalreconcile[]"]').val(totalroom);
		table.find('input[name="totalreconcile[]"]').keyup();
	});
	
	$('body').on('change, keyup','input[name="totalreconcile[]"]', function(e) {	
		var grandtotal = 0;
		$('input[name="totalreconcile[]"]').each(function() {
			text_totalroom = $(this).val().replace(/,/g, '');
			if(text_totalroom == ''){
				totalroom = 0;
			}else{
				totalroom = parseFloat($(this).val().replace(/,/g, ''));
			}
			grandtotal = grandtotal + totalroom;
		});
		$('input[name="grandreconsile"]').val(grandtotal);
	});

	$('body').on('click','.btn-popup',function(e){
		$target = $(this).data('target');
		$($target).modal('show');
		if($target=='#cancelModal'){
			title =  $(this).data('title');
			submittext =  $(this).data('submit');
			status =  $(this).data('status');
			
			$($target).find('h4.modal-title').html(title);
			$($target).find('button.submit').text(submittext);
			$($target).find('input[name=changestatus]').val(status);
		}
		return false;
	})
		
	$('#reconcileModal').on('click','button.submit', function(e) {
		e.preventDefault();
		form = $(this).closest('form');
		modalbox = $('#NotificationModal');
		$.ajax({
			url		: '<?php echo $base_url; ?>/request/reconcile.php',
			type	: 'post',
			data	: form.serialize(),
			success	: function(response){
				console.log(response);
				$('#reconcileModal').modal('hide');
				if(response == "success"){
					response = "Booking has been reconciled. We will refresh the page.";
				}else{
					response = "Data failed to changed";
				}
				modalbox.find('.modal-body').html(response);
				modalbox.modal('show');
			}
		});
	});

	$('#cancelModal').on('click','button.submit', function(e) {
		e.preventDefault();
		form = $(this).closest('form');
		if(form.find('textarea[name=cancellationreason]').val().length <= 0 || form.find('input[name=cancellationamount]').val().length <= 0){
			modalbox = $('#AlertModal');
			modalbox.find('.modal-body').html("Please make sure you fill Cancellation Amount and Cancellation Reason");
			modalbox.modal('show');
		}else{
			modalbox = $('#NotificationModal');
			$.ajax({
				url		: '<?php echo $base_url; ?>/request/cancellation.php',
				type	: 'post',
				data	: form.serialize(),
				success	: function(response){
					console.log(response);
					$('#cancelModal').modal('hide');
					if(response == "success"){
						response = "Booking status has been changed. We will refresh the page.";
					}else{
						response = "Data failed to changed";
					}
					modalbox.find('.modal-body').html(response);
					modalbox.modal('show');
				}
			});
		}
	});

	
	$('#NotificationModal').on('hide.bs.modal', function (e) {
		 location.reload(true);
	});
	
	$('#cancelModal').on('show.bs.modal', function(e) {
		// console.log("$('#cancelModal').on('show.bs.modal'");
		// console.log(e);
		// console.log(e.relatedTarget);
		//  title = $(e.relatedTarget).data('title');
		//  submittext = $(e.relatedTarget).data('submit');
		//  status = $(e.relatedTarget).data('status');
		 
		//  $(this).find('h4.modal-title').html(title);
		//  $(this).find('button.submit').text(submittext);
		//  $(this).find('input[name=changestatus]').val(status);
	});
	
	$('tr.list-detail').click(function() {
        $(this).next('tr.list-breakdown').slideToggle(0);
		if($(this).next('tr.list-breakdown').is(":visible")){ 
			$(this).children('td').eq(0).find('i').removeClass('fa-caret-right');
			$(this).children('td').eq(0).find('i').addClass('fa-caret-down');
		}else{
			$(this).children('td').eq(0).find('i').removeClass('fa-caret-down');
			$(this).children('td').eq(0).find('i').addClass('fa-caret-right');
		}
        return false;        
    });
    
    $('#export').click(function(){
        var form = $('form#data-input');
        form.attr('action','<?php echo $base_url; ?>/includes/booking-all/export_xls.php');
        form.submit();
    });
});
</script>