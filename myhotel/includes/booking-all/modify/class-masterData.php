<?php
class masterData{
	var $bookingroomoid;
	var $roomoid;
	
	public function __construct($db){
		$this->db = $db;
	}
	
	public function setRoom($bookingroomoid){
		$this->bookingroomoid = $bookingroomoid;
		
		$stmt = $this->db->prepare("select r.roomoid from bookingroom br inner join roomoffer ro using (roomofferoid) inner join room r using (roomoid) where bookingroomoid = :a");
		$stmt->execute(array(':a' => $bookingroomoid));
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		$this->roomoid = $result['roomoid'];
		
	}
	
	public function dataRoom(){
		$stmt = $this->db->prepare("select r.roomoid, r.name AS roomname, r.roomsize, v.name as roomview, bedroom.bedconfiguration, rf.roomfacilities from room r inner join roomoffer ro using (roomoid) inner join view v using (viewoid) left join (select GROUP_CONCAT(bed.name SEPARATOR ' / ') as bedconfiguration, roomoid from bed inner join roombed using (bedoid) group by roomoid) as bedroom on bedroom.roomoid = r.roomoid left join (select GROUP_CONCAT(facilitiesoid) as roomfacilities, roomoid from roomfacilities group by roomoid) as rf on rf.roomoid = r.roomoid where r.roomoid = :a");
		$stmt->execute(array(':a' => $this->roomoid));
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		
		return $result;
	}

	public function roomMainPhoto(){
		$stmt = $this->db->prepare("select photourl from hotelphoto hp where hp.flag = 'main' AND ref_table = 'room' AND ref_id = :a");
		$stmt->execute(array(':a' => $this->roomoid));
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		
		return $result;
	}

}
?>