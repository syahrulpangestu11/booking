<?php
try {
	// include_once "../ibe/modify/include/class-masterData.php";
	// include_once "../ibe/modify/include/class-modifyBooking.php";
	include_once "class-masterData.php";
	include_once "class-modifyBooking.php";
	
	$booking		= new modifyBooking($db);	
	$booking->setBooking($bookingoid);
	$detail_booking = $booking->masterBooking();
	$detail_room	= $booking->detailBooking();
	
	$bookingdetail	= new masterData($db);

	$totalcancellation = 0;
	$n = 0;
	foreach($detail_room as $roomlist){
		$bookingdetail->setRoom($roomlist['bookingroomoid']);
		$room = $bookingdetail->dataRoom();
		
		$booking->setBookingRoomID($roomlist['bookingroomoid']);
		$cancellationfee = $booking->cancellationFee();
		
		$currency = $roomlist['currencycode'];
		
		$cancellationcost = 0;
	?>
    <div class="row box-room">
    	<div class="col-md-6">
        	<h1>Room <?=++$n?> : <?=$room['roomname']?></h1>
            <h2><?=$roomlist['promotion']?></h2>
            <label>Cancellation Policy</label>
            <?=$roomlist['cancellationname']?>
            <label>Cancellation Cost</label>
            <ul>
                <?php
                if($roomlist['termcondition'] == "non refundable"){
                    echo "<li>From ".date('d F Y', strtotime($detail_booking['bookingtime']))." : ".$currency." ".number_format($roomlist['total'])."</li>";
					$cancellationcost	=  $roomlist['total'];
                }else if($roomlist['termcondition'] == "free cancellation"){
                    
                    $select_datediff= "SELECT DATEDIFF('".$roomlist['checkin']."','".date('Y-m-d', strtotime($detail_booking['bookingtime']))."') AS DiffDate";
                    $stmt			= $db->query($select_datediff);
                    $diffdate		= $stmt->fetch(PDO::FETCH_ASSOC);
                    $before_checkin	= $diffdate['DiffDate'];
                    
                    if($before_checkin >= $roomlist['cancellationday']){
                        echo "<li>Until ".date('d F Y', strtotime($roomlist['checkin']." -".($roomlist['cancellationday']+1)."day"))." : ".$currency." 0 (free cancellation)</li>";
						$cancellationcost1	=  0;
                    }
                    
                    if($before_checkin >= $roomlist['cancellationday']){
                        $from_date_with_fee	= date('d F Y', strtotime($roomlist['checkin']." -".($roomlist['cancellationday'])."day"));
                    }else{
                        $from_date_with_fee	= date('d F Y', strtotime($detail_booking['bookingtime']));
                    }
                    
                    if($roomlist['cancellationtype'] == "full amount"){
                        echo "<li>From ".$from_date_with_fee." : ".$currency." ".number_format($roomlist['total'])."</li>";
						$cancellationcost2	=  $roomlist['total'];
                    }else{
                        $cancellationfee = 0;
                        for($i = 0; $i < $roomlist['cancellationtypenight']; $i++){
                            $date = date('Y-m-d', strtotime($roomlist['checkin'].' +'.$i.' day'));
                            
                            $q_breakdownrate	= "select total from bookingroomdtl where bookingroomoid = '".$roomlist['bookingroomoid']."' and reconciled = '0' and `date` = '".$date."'";
                            $stmt				= $db->query($q_breakdownrate);
                            $breakdowntotal		= $stmt->fetch(PDO::FETCH_ASSOC);
                            $cancellationcost	= $cancellationcost + $breakdowntotal['total'];
                        }
                        
                        echo "<li>From ".$from_date_with_fee." : ".$currency." ".number_format($cancellationcost)."</li>";
						$cancellationcost2	=  $cancellationcost;
                    }
					
                    if($before_checkin >= $roomlist['cancellationday']){
						$cancellationcost	=  $cancellationcost1;
                    }else{
						$cancellationcost	=  $cancellationcost2;
					}
                }
				$totalcancellation+=$cancellationcost;
                ?>
            </ul>
        </div>
        <div class="col-md-6">
        	<label>Breakdown Rate</label>
            <table class="table table-bordered table-striped">
                <thead><tr><th>Check in</th><th>Check Out</th><th>Rate</th></tr></thead>
                <tbody>
                <?php	
                $q_breakdownrate = "select brd.*, c.currencycode from bookingroomdtl brd left join currency c using (currencyoid) where bookingroomoid = '".$roomlist['bookingroomoid']."' and reconciled = '0'";
                $stmt = $db->query($q_breakdownrate);
                $breakdownrate = $stmt->fetchAll(PDO::FETCH_ASSOC);
                end($breakdownrate);
                $lastkey = key($breakdownrate);
                foreach($breakdownrate as $key => $brd){
                ?>
                    <tr><td><input type="hidden" name="brd[]" value="<?=$brd['bookingroomdtloid']?>"><?=date('d M Y', strtotime($brd['date']))?></td><td><?=date('d M Y', strtotime($brd['date'].' +1 day'))?></td><td class="nighttotal" style="text-align:right!important;"><?=$brd['currencycode']?> <?=number_format($brd['total'])?></td></tr>
                <?php  
                }
                ?>
                </tbody>
                <tfoot>
                	<tr><td colspan="2"><b>Total</b></td><td align="right"><?=$currency?> <?=number_format($roomlist['total'])?></td></tr>
                	<tr><td colspan="2"><b>Estimated Cancellation Cost</b></td><td class="text-right"><?=$currency?> <?=number_format($cancellationcost)?> <?php /* <input type="text" value="<?=floor($cancellationcost)?>" name="cancellationroom[<?=$roomlist['bookingroomoid']?>]" /> */ ?></td></tr>
                </td>
            </table>
        </div>
    </div>
    <?php	
	}
	?>
    <div class="row">
    	<div class="col-md-7 col-md-12 text-right">
        	<h1>Cancellation Amount</h1>
        </div>
    	<div class="col-md-5 col-md-12">
        	<input type="text" class="form-control" name="cancellationamount" value="<?=floor($totalcancellation)?>" required="required">
        </div>
    </div>
    <div class="row">
    	<div class="col-md-7 col-md-12 text-right">
        	<h1>Reason of Cancellation</h1>
        </div>
    	<div class="col-md-5 col-md-12">
        	<textarea name="cancellationreason" class="form-control" placeholder="Reason why this booking cancelled or reason to waive penalty cancellation" required="required"></textarea>
        </div>
    </div>
     <?php
}catch(Exception $ex) {
	print($ex ->getMessage());
	die();
}
?>
<style type="text/css">
.mdbook .modal-body h1, .modal-body h2{ margin:0 0 5px 0; } 
.mdbook .modal-body h1{ font-size:1.15em; font-weight:bold; }
.mdbook .modal-body h2{ font-size:1.02em; }
.mdbook .modal-body hr{ margin:5px; border-top: 1px solid #6f6f6f; }
.mdbook .modal-body label{ display:block; margin:2px 0; }
.mdbook .modal-body .table-bordered > tbody > tr:first-child > td{ background:none; padding: 1px 5px; text-align:inherit; color:inherit; }
.mdbook .modal-body .row{ margin:0 0 10px; }
.mdbook .modal-body .box-room{ border-bottom:1px dashed #99afc5; }
.mdbook .modal-body input[type=text]{
	width: 100%;
    padding: 3px 5px;
    border-radius: 0;
    text-align: right;
    border: 1px solid #a2a2a2;
    color: black;
}
</style>