<?php
	// include("../../conf/connection.php");

    $useroid = $_SESSION['_oid'];
    $user = $_SESSION['_initial'];
    $bookingpaymentproofoid = $_POST['bookingpaymentproofoid'];
    $bookingnumber = $_POST['bookingnumber'];
    $bookingoid = $_POST['bookingoid'];
    $hoteloid = $_POST['hoteloid'];
    $status = $_POST['status'];
    $remark = $_POST['remark'];
	$oldpict = $_POST['oldpicture'];
	$newpict = '';
	
	$allowedTypes = array("image/jpg", "image/jpeg", "image/gif", "image/png");
	$allowedExts = array("jpg", "jpeg", "gif", "png");


	$notError = true;
	if((!empty($_FILES["picture"]["name"]) or $_FILES["picture"]["name"]!='')){
		$fileerror = $_FILES["picture"]["error"] ;
		$filetype = strtolower($_FILES["picture"]["type"]);
		$filename_ori = strtolower($_FILES["picture"]["name"]);
		$filename_array = explode(".", $filename_ori);
		$ext = ".".end($filename_array);
		$filename = str_replace($ext, "", $filename_ori);
		$uploadedname = $bookingnumber;//"default_".$filename."-".date("Y_m_d_H_i_s");
        $imagename = $uploadedname . $ext; 
		$statusError = ($fileerror > 0 && $fileerror != 4 ? true:false);

		if($statusError  && !in_array(end($filename_array), $allowedExts)  && !in_array($filetype, $allowedTypes)){
			$notError = false;
			echo"<script>window.location.href = '".$base_url."/reservation/confirmation/?no=".$bookingnumber."&error=image'</script>";
			echo "0";
			die();
		}

        if($filename!=''){
            $source = $_FILES['picture']['tmp_name'];
            $folder = "webassets/images/paymentconfirmation/";
            $target = $folder.$imagename;
			$sts = move_uploaded_file($source, $upload_base."/" . $target);
			$newpict = ($filename!=""?str_replace('/myhotel',"",$base_url)."/".$folder.$imagename:$oldpict);
			
        }
	}

	if($notError){
		try {
			if(!empty($bookingpaymentproofoid)){
				$query = "UPDATE bookingpaymentproof SET status=:b, remark=:e, response_at=:f, response_by=:g  
							".(!empty($newpict)?' , imgproof=:h ':'')." WHERE bookingpaymentproofoid=:oid ";
				$execute=array(':b' => $status, ':e' => $remark, ':f' => date("Y-m-d H:i:s"), ':g' => $user, ':oid' => $bookingpaymentproofoid);
			}else{
				$query = "INSERT INTO bookingpaymentproof (bookingoid, status, uploaded_at, uploaded_by, remark, response_at, response_by
							".(!empty($newpict)?', imgproof':'')." ) VAlUES(:a, :b, :c, :d, :e, :f, :g ".(!empty($newpict)?', :h':'').")";
				$execute=array(':a' => $bookingoid, ':b' => $status, ':c' => date("Y-m-d H:i:s"), ':d' => $user, 
				':e' => $remark, ':f' => date("Y-m-d H:i:s"), ':g' => $user);
			}
			if(!empty($newpict)) $execute[':h']=$newpict;
			// echo"<script>console.log(".json_encode($query).");</script>";
			// echo"<script>console.log(".json_encode($execute).");</script>";

			$stmt = $db->prepare($query);
			$stmt->execute($execute);
			
			if($status=='approve'){
				//change booking status
				$stmt2 = $db->prepare("UPDATE booking set bookingstatusoid=:a where bookingnumber=:oid");
				$stmt2->execute(array(':a'=>'4', ':oid'=>$bookingnumber));

				//update xml
				$stmt = $db->prepare("SELECT `hotelcode` FROM `hotel` WHERE hoteloid = :id");
				$stmt->execute(array(':id' => $hoteloid));
				$r_ho = $stmt->fetch(PDO::FETCH_ASSOC);
				$hotelcode = $r_ho['hotelcode'];
			
			
				$stmt = $db->prepare("SELECT `roomoid`, `bookingroomoid` FROM `roomoffer` INNER JOIN `bookingroom` USING(`roomofferoid`) WHERE `bookingoid` = :a");
				$stmt->execute(array(':a' => $bookingoid));
				$r_ro = $stmt->fetchAll(PDO::FETCH_ASSOC);
				
				foreach($r_ro as $ar_ro){
					// echo "<script>console.log(".json_encode($ar_ro).")</script>";
					$vroomoid = $ar_ro['roomoid'];
					$bookingroomoid = $ar_ro['bookingroomoid'];
			
					$stmt = $db->prepare("SELECT * FROM `bookingroomdtl` WHERE bookingroomoid = :id");
					$stmt->execute(array(':id' => $bookingroomoid));
					$r_brd = $stmt->fetchAll(PDO::FETCH_ASSOC);
					
					foreach($r_brd as $a_brd){
						// echo "<script>console.log(".json_encode($a_brd).")</script>";
						$vdate = $a_brd['date'];

						
						$path_xml_hotel = "../myhotel/data-xml/".$hotelcode.".xml";
					
						$xml = new DomDocument();
						$xml->preserveWhitespace = false;
						$xml->load($path_xml_hotel);
						$xpath = new DomXpath($xml);
						$root = $xml->documentElement;
						$xml_file =  simplexml_load_file($path_xml_hotel);

						$query_tag_allotment_channel = '//hotel[@id="'.$hoteloid.'"]//masterroom[@id="'.$vroomoid.'"]/rate[@date="'.$vdate.'"]/allotment/channel[@type="1"]';
						$query_tag_allotment_channel_value = $query_tag_allotment_channel.'/text()';
			
						foreach($xml_file->xpath($query_tag_allotment_channel_value) as $tagallotment){
							$allotment = (int)$tagallotment[0];
						}
			
						$new_allotment = $allotment - 1;
						echo "<script>console.log(".json_encode($new_allotment.'|'.$allotment.' --------').")</script>";
						$check_tag_allotment_channel = $xpath->query($query_tag_allotment_channel);
						echo "<script>console.log(".json_encode($check_tag_allotment_channel).")</script>";
						$check_tag_allotment_channel->item(0)->nodeValue = $new_allotment;
						
						$xml->formatOutput = true;
						$xml->save($path_xml_hotel) or die("Error");
					}
				}
			
				
				// die();
				// if(!@include("includes/mailer/sending_mail_reservation_confirmed.php")) echo"<script>window.location.href = '".$base_url."/booking-all/confirmation/?no=".$bookingnumber."&error=saving&msg=Failed to include mailer!'</script>";
				include('includes/mailer/reservation_sending_template.php');
			}else{
				echo"<script>window.location.href = '".$base_url."/booking-all/confirmation/?no=".$bookingnumber."'</script>";
			}

		}catch(PDOException $ex) {
			// echo"<script>console.log(".json_encode($prepare).");</script>";
			// echo"<script>console.log(".json_encode($values).");</script>";
			// echo"<script>console.log(".json_encode($execute).");</script>";
			// echo"<script>console.log(".json_encode($ex).");</script>";
			// echo"<script>console.log('deleted:".json_encode($ex)."');</script>";
			echo"<script>window.location.href = '".$base_url."/booking-all/confirmation/?no=".$bookingnumber."&error=saving'</script>";
			echo "0";
			die();
		}
	}
	echo "1";
?>