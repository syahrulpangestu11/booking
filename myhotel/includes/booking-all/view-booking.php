<?php
			// echo"<script>console.log(".json_encode("hai").");</script>";
	if(empty($_REQUEST['status']) and empty($_REQUEST['startdate']) and empty($_REQUEST['enddate']) and empty($_REQUEST['ho']) and empty($_REQUEST['ag'])){
		$start = date("d F Y",  strtotime(date("Y-m-d")." -7 day" ));
		$end = date("d F Y",  strtotime(date("Y-m-d")." +1 month" ));
		$status = "-";
		$viewby = "bookingtime";
		$hoteloid = "-";
		$agentoid = "-";
	}else{
		$start = date("d F Y", strtotime($_REQUEST['startdate']));
		$end = date("d F Y", strtotime($_REQUEST['enddate']));
		$status = $_REQUEST['status'];
		$viewby = $_REQUEST['viewby'];
		$hoteloid = $_REQUEST['ho'];
		$agentoid = $_REQUEST['ag'];
    }
    echo"<script>console.log(".json_encode($hoteloid."|".$agentoid).");</script>";
	include("js.php");
?>
<style>
table tr.list{ cursor:pointer; }
</style>
<section class="content-header">
    <h1>
        Booking
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i>  Reports </a></li>
        <li class="active">Booking</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="box box-form">
            <div class="box-body">
                <div class="form-group">
                    <form method="get" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/booking-all/">
                    	<!-- <input type="hidden" name="ho" value="<?php echo $hoteloid; ?>" /> -->
                        <table>
                            <tr>
                            	<td>
                                    <label>FIlter By</label>&nbsp;&nbsp;
                                    <select name="viewby" class="input-select">
                                    	<?php if($viewby == "bookingtime"){ $selected = 'selected = "selected"';  }else{ $selected = ''; } ?>
                                    	<option value="bookingtime" <?php echo $selected;?> >Booking Date</option>
                                    	<?php if($viewby == "checkin"){ $selected = 'selected = "selected"';  }else{ $selected = ''; } ?>
                                    	<option value="checkin" <?php echo $selected;?> >Stay Date</option>
									</select>
                                    &nbsp;&nbsp;
                                    <label>from</label>&nbsp;&nbsp;
                                    <input type="text" name="startdate" id="startdate" placeholder="" value="<?php echo $start; ?>">&nbsp;&nbsp;
                                    <label>to</label>&nbsp;&nbsp;
                                    <input type="text" name="enddate" id="enddate" placeholder="" value="<?php echo $end; ?>">
                                </td> 
                            </tr>
                            <tr>
                                <td>
                                    <label>Reservation Status</label> &nbsp;
                                    <select name="status" class="input-select">
                                    <?php if($status == "all"){ $selected = 'selected = "selected"';  }else{ $selected = ''; } ?>
                                    	<option value="all" <?php echo $selected;?> >show all status</option>
                                    <?php
                                        try {
                                            $stmt = $db->query("select * from bookingstatus where publishedoid = '1'");
                                            $r_status = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                            foreach($r_status as $row){
												if($row['bookingstatusoid'] == $status and ctype_alpha($status) == false){ $selected = 'selected = "selected"';  }else{ $selected = ''; }
												echo"<option value='".$row['bookingstatusoid']."' ".$selected.">  ".$row['note']."</option>";
											}
										}catch(PDOException $ex) {
                                            echo "Invalid Query";
                                            die();
                                        }
                                    
                                    ?>   
                                    </select> 
                                    &nbsp;&nbsp;
                                    <label>Hotel</label> &nbsp;
                                    <select name="ho" class="input-select">
                                    <?php if($hoteloid == "all"){ $selected = 'selected = "selected"';  }else{ $selected = ''; } ?>
                                    <option value="all" <?=$selected;?>>show all hotel</option>
                                    <?php
                                        try {
                                            $stmt = $db->query("select * from hotel where publishedoid = '1'");
                                            $r_status = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                            foreach($r_status as $row){
												if($hoteloid == $row['hoteloid']){ $selected = 'selected = "selected"';  }else{ $selected = ''; }
												echo"<option value='".$row['hoteloid']."' ".$selected.">  ".$row['hotelname']."</option>";
											}
										}catch(PDOException $ex) {
                                            echo "Invalid Query";
                                            die();
                                        }
                                    
                                    ?>
                                    </select> &nbsp;&nbsp;
                                    <!-- <label>Agent</label> &nbsp;
                                    <select name="ag" class="input-select">
                                    <?php if($agentoid == "all"){ $selected = 'selected = "selected"';  }else{ $selected = ''; } ?>
                                    <option value="all" <?=$selected;?>>show all agent</option>
                                    <?php
                                        try {
                                            $stmt = $db->query("select * from agent where publishedoid = '1'");
                                            $r_status = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                            foreach($r_status as $row){
												if($agentoid == $row['agentoid']){ $selected = 'selected = "selected"';  }else{ $selected = ''; }
												echo"<option value='".$row['agentoid']."' ".$selected.">  ".$row['agentname']."</option>";
											}
										}catch(PDOException $ex) {
                                            echo "Invalid Query";
                                            die();
                                        }
                                    
                                    ?>   
                                    </select> &nbsp;&nbsp; -->
                                </td> 
                                <td>
                                    <button type="submit" class="small-button blue">Search</button> &nbsp;
                                    <?php if($_SESSION['_typeusr'] != '6'){?>
                                    <button type="button" class="small-button green" id="export">Export to Excel</button>
                                    <?php }?>
                                </td> 
                            </tr>
                        </table>
                    </form>
                </div>
            </div><!-- /.box-body -->
       </div>
    </div>
    
    <div class="row">
        <div class="box">
        <form method="post" enctype="multipart/form-data" action="<?php echo $base_url; ?>/rate-control/upload">
            <div id="data-box" class="box-body">
				<div class="loader">Loading...</div>
            </div><!-- /.box-body -->
		</form>
       </div>
    </div>

</section>
