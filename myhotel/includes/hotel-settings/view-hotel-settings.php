 <?php
	$d_set = array();
	try {
		$stmt = $db->query("SELECT `hoteloid`, `infantageuntil`, `childagefrom`, `childageuntil`, `minguestage`, `extrabedagefrom`, `childstayfree`, `smsbookingnotif`, `ycsbookingemail`, `unacknowledgebookinglistemail`, `checkintomorrowbookinglistemail` FROM `hotelsettings` WHERE `hoteloid`='".$hoteloid."'");
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
			$r_hotel = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_hotel as $row){
				$d_set['infantageuntil'] = $row['infantageuntil'];
				$d_set['childagefrom'] = $row['childagefrom'];
				$d_set['childageuntil'] = $row['childageuntil'];
				$d_set['minguestage'] = $row['minguestage'];
				$d_set['extrabedagefrom'] = $row['extrabedagefrom'];
				$d_set['childstayfree'] = $row['childstayfree']; 
				$d_set['smsbookingnotif'] = $row['smsbookingnotif']; 
				$d_set['ycsbookingemail'] = $row['ycsbookingemail']; 
				$d_set['unacknowledgebookinglistemail'] = $row['unacknowledgebookinglistemail'];
				$d_set['checkintomorrowbookinglistemail'] = $row['checkintomorrowbookinglistemail'];  
			}
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}
	
	include("js.php");
?>
<section class="content-header">
    <h1>
        Hotel Settings
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Settings</a></li>
        <li class="active">Hotel Settings</li>
    </ol>
</section>
<section class="content">
    <form method="post" action="#" id="data-input">
    <input type="hidden" name="hoteloid" value="<?php echo $hoteloid; ?>" />
    <div class="row">
        <div class="box box-form">
            <h1>Hotel Age Policy</h1>
            <div class="box-body">
                <div class="form-group">
                    <br>
                    <div class="form-group">
                        <label>Infant Age Until</label> &nbsp;
                        <select name="infantageuntil">
                            <?php
                            for($x=0; $x<=5; $x++){
								if($d_set['infantageuntil']==$x){
                                	echo "<option value='".$x."' selected='selected'>$x</option>";
								}else{
                                	echo "<option value='".$x."'>$x</option>";
								}
                            }
                            ?>
                        </select> &nbsp; Years Old
                    </div>
                    <div class="form-group">
                        <label>Children Age From</label> &nbsp; <span class="childagefrom"><?=$d_set['childagefrom']?></span>
                        <input type="hidden" name="childagefrom" placeholder="" value="<?=$d_set['childagefrom']?>"> &nbsp; Until &nbsp;
                        <select name="childageuntil">
                            <?php
                            for($x=0; $x<=21; $x++){
								if($d_set['childageuntil']==$x){
                                	echo "<option value='".$x."' selected='selected'>$x</option>";
								}else{
                                	echo "<option value='".$x."'>$x</option>";
								}
                            }
                            ?>
                        </select> &nbsp; Years Old
                    </div>
                    <div class="form-group">
                        <label>Minimum Guest Age For Stay (set to 0 if all ages allow)</label> &nbsp;
                        <select name="minguestage">
                            <?php
                            for($x=0; $x<=21; $x++){
								if($d_set['minguestage']==$x){
                               		echo "<option value='".$x."' selected='selected'>$x</option>";
								}else{
                                	echo "<option value='".$x."'>$x</option>";
								}
                            }
                            ?>
                        </select> &nbsp; Years Old
                    </div>
                    <div class="form-group">
                        <label>Extra Bed Required From</label> &nbsp;
                        <select name="extrabedagefrom">
                            <?php
                            for($x=0; $x<=21; $x++){
								if($d_set['extrabedagefrom']==$x){
                                	echo "<option value='".$x."' selected='selected'>$x</option>";
								}else{
                                	echo "<option value='".$x."'>$x</option>";
								}
                            }
                            ?>
                        </select> &nbsp; Years Old
                        <br><input type="checkbox" name="childstayfree" value="1" <?php if($d_set['childstayfree']=="1"){echo "checked='checked'";}?>> &nbsp; Children Stay Free on Existing Bedding
                    </div>
                </div>
            </div>
            <div class="box-footer" align="right">
				<button type="button" class="small-button blue add-button">Save</button>           
                <button type="reset" class="small-button blue">Reset</button>
            </div>
        </div>
    </div>
    <div class="row hide">
        <div class="box box-form">
            <h1>Communication Settings</h1>
            <div class="box-body">
                <div class="form-group">
                    <br>
                    <div class="form-group">
                        <label>SMS Booking Notification</label> &nbsp;
                        <select name="smsbookingnotif">
                            <option value="0" <?php if($d_set['smsbookingnotif']=="0"){echo "selected='selected'";}?>>Off</option>
                            <option value="1" <?php if($d_set['smsbookingnotif']=="1"){echo "selected='selected'";}?>>On</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>GBH Booking Email Reminder</label> &nbsp;
                        <select name="ycsbookingemail" class="hiddendiv-trigger">
                            <option value="0" <?php if($d_set['ycsbookingemail']=="0"){echo "selected='selected'";}?>>Off</option>
                            <option value="1" <?php if($d_set['ycsbookingemail']=="1"){echo "selected='selected'";}?>>On</option>
                        </select>
                        <div class="hiddendiv" <?php if($d_set['ycsbookingemail']=="1"){echo "style='display:block'";}?>>
                            <br><input type="checkbox" name="unacknowledgebookinglistemail" value="1" <?php if($d_set['unacknowledgebookinglistemail']=="1"){echo "checked='checked'";}?>> &nbsp; Unacknowledged Booking List
                            <br><input type="checkbox" name="checkintomorrowbookinglistemail" value="1" <?php if($d_set['checkintomorrowbookinglistemail']=="1"){echo "checked='checked'";}?>> &nbsp; Check-in Tomorrow Booking List
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</section>
<script>
    $(function(){
        $(".hiddendiv-trigger").change(function(){
            var val = $(this).val();
            if(val == "1"){
                $(this).parent().children(".hiddendiv").css("display","block");
            }else{
                $(this).parent().children(".hiddendiv").css("display","none");
            }
        });

        function getChildAgeFrom(){
            var val = $("select[name=infantageuntil]").val();
            if(val == "0"){
                $(".childagefrom").html(0);
				$('input[name="childagefrom"]').val(0);
            }else{ 
                var n = parseInt(val) + 1;
                $(".childagefrom").html(n);
				$('input[name="childagefrom"]').val(n);
            }
        }

        getChildAgeFrom();
        $("select[name=infantageuntil]").change(function(){
            getChildAgeFrom();
        });
    });
</script>