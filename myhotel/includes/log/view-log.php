<?php
    $month = array("January","February","March","April","May","June","July","August","September","October","November","December");
    $year = array();
    $curyear = intval(date("Y"));
    for($i=$curyear;$i>=2017;$i--){
        $year[] = $i;
    }
?>
<section class="content-header">
    <h1>Activity Log</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-cog"></i> Setting</a></li>
        <li class="active">Activity Log</li>
    </ol>
</section>
<section class="content">
  <div class="box box-default box-form">
    <div id="data-box" class="box-body">
      <form method="post" class="form-inline" style="margin-bottom:20px;" action="<?=$base_url?>/activity-log">
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label>Month : </label>
              <select class="form-control" name="month">
                  <?php
                      foreach($month as $k => $v){
                          if(($k+1) == $_POST['month']){
                            $selected = "selected";
                          }else if(empty($_POST['month']) and ($k+1) == date('n')){
                            $selected = "selected";
                          }else{
                            $selected = "";
                          }
                          echo '<option value="'.($k+1).'" '.$selected.'>'.$v.'</option>';
                      }
                  ?>
              </select>
              <label>Year : </label>
              <select class="form-control" name="year">
                  <?php
                      foreach($year as $k => $v){
                        if($v == $_POST['year']){
                          $selected = "selected";
                        }else if(empty($_POST['year']) and $v == date('Y')){
                          $selected = "selected";
                        }else{
                          $selected = "";
                        }
                        echo '<option value="'.$v.'" '.$selected.'>'.$v.'</option>';
                      }
                  ?>
              </select>
              <button type="submit" class="btn btn-primary" id="btfilter">SHOW REPORT</button>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
  <div class="box box-warning box-form">
    <div id="data-box" class="box-body">
      <div class="row">
        <div class="col-md-12">
          <table class="table table-stripped" style="font-size:0.9em">
            <thead>
              <tr><th>Log Time</th><th>Room Type</th><th>Action</th><th>Old Value</th><th>New Value</th><th>Username</th></tr>
            </thead>
            <tbody>
              <?php
              if(empty($_POST['month']) and empty($_POST['year'])){
                $monthyear = date('Ym');
              }else{
                $monthyear = date('Ym', strtotime($_POST['year']."-".$_POST['month']."-01"));
              }
              $path_url = $upload_base."/myhotel/log/".$monthyear."/apps-".$hcode."-".$monthyear.".log";
              if(file_exists($path_url)){
                $log_file = fopen($path_url,'r') or die ('File opening failed');
                while(!feof($log_file)){
                  $log_row = fgets($log_file);
                  $log = explode(" ~ ", $log_row);
              ?>

<!--  VIEW LOG - AVAILABITY  -->
          <?php if ($log[2] == "Availability") { 
                    $oldAvalog = explode(" : ", $log[5]);
                    $newAvalog = explode(" : ", $log[6]);
                  ?>
                    <tr>
                      <td><?=substr($log[1], 1, -1)?></td>  
                      <td><?=$log[2]?></td> 
                      <td><?=$log[4]?></td> 
              <?php  if ($oldAvalog[2] != $newAvalog[2]) { ?>
                      <td>| date : <?=$oldAvalog[1]?> : <marker style="background-color: yellow;"><?=$oldAvalog[2]?></marker></td>
                      <td>| date : <?=$newAvalog[1]?> : <marker style="background-color: cyan;"><?=$newAvalog[2]?></marker></td>
                    <?php } ?>
                      <td><?=$log[7]?></td>
                  </tr>
          <?php  } // echo json_encode($oldAvalog);
                ?>
<!-- END OF VIEW LOG - AVAILABILITY -->


<!-- VIEW LOG - EDIT CONTACT MANAGEMENT -->
        <?php if ($log[2] == "Edit" AND $log[4] == "Contact Management") {
                $oldValuelog = explode("~", $log[5]);
                $newValuelog = explode("~", $log[6]);
              
                if( $oldValuelog != $newValuelog ) {?>
                  <tr>
                    <td><?=substr($log[1], 1, -1)?></td>                       
                    <td><?=$log[2]?></td> 
                    <td><?=$log[4]?></td> 
                    <td> 
                    <?php $i=1;
                      while ($i <= 4){
                        $oldcon = explode("#", $oldValuelog[$i]);
                        if($oldValuelog[$i] != $newValuelog[$i]){ echo " ".$oldcon[0]." "."<marker style='background-color:yellow;'>".$oldcon[1]."</marker>"; }
                      $i=$i+1; }
                         ?> 
                    </td>
                    <td> 
                    <?php $i=1;
                      while ($i <= 4){
                        $newcon = explode("#", $newValuelog[$i]);
                        if($oldValuelog[$i] != $newValuelog[$i]){ echo " ".$newcon[0]." "."<marker style='background-color:cyan;'>".$newcon[1]."</marker>"; }
                        $i=$i+1; }
                       ?> 
                    </td>
                    <td><?=$log[7]?></td>
                  </tr>
          <?php  }       
              }   ?>
<!--  END OF VIEW LOG - EDIT CONTACT MANAGEMENT --> 


<!-- VIEW LOG - EDIT PROMOTION -->
        <?php if($log[2] == "Edit Promotion") {
                $oldPromolog = explode("~", $log[5]);
                $newPromolog = explode("~", $log[6]);

                if($oldPromolog != $newPromolog) { ?>
                  <tr>
                    <td><?=substr($log[1], 1, -1)?></td>             
                    <td><?=$log[2]?></td> 
                    <td><?=$log[4]?></td> 
                    <td> 
                    <?php $i=1;
                      while ($i <= 25) {
                        $oldprom = explode("#", $oldPromolog[$i]);
                        echo " ".$oldprom[0]." "."<marker style='background-color:yellow;'>".$oldprom[1]."</marker>";
                        $i=$i+1; } ?>
                    </td> 
                    <td> 
                    <?php $i=1;
                      while ($i <= 25) {
                        $newprom = explode("#", $newPromolog[$i]);
                        echo " ".$newprom[0]." "."<marker style='background-color:cyan;'>".$newprom[1]."</marker>";
                        $i=$i+1; } ?>
                    </td>
                    <td><?=$log[7]?></td>
                  </tr>
          <?php }
              }   ?>           
<!-- END OF VIEW LOG - EDIT PROMOTION -->


<!-- VIEW LOG - EDIT PACKAGE -->
        <?php if($log[2] == "Edit Package") {
                $oldPacklog = explode("~", $log[5]);
                $newPacklog = explode("~", $log[6]);

                if($oldPacklog != $newPacklog) { ?>
                  <tr>
                    <td><?=substr($log[1], 1, -1)?></td>             
                    <td><?=$log[2]?></td> 
                    <td><?=$log[4]?></td> 
                    <td> 
                    <?php $i=1;
                      while ($i <= 25){
                        $oldpack = explode("#", $oldPacklog[$i]);
                        echo " ".$oldpack[0]." "."<marker style='background-color:yellow;'>".$oldpack[1]."</marker>";
                        $i=$i+1; } ?>
                    </td> 
                    <td> 
                    <?php $i=1;
                      while ($i <= 25) {
                        $newpack = explode("#", $newPacklog[$i]);
                        echo " ".$newpack[0]." "."<marker style='background-color:cyan;'>".$newpack[1]."</marker>";
                        $i=$i+1; }  ?>
                    </td> 
                    <td><?=$log[7]?></td>
                  </tr>
          <?php }
              }   ?> 
<!-- END OF VIEW LOG - EDIT PACKAGE -->          


<!-- VIEW LOG - EDIT RATE PLAN -->
        <?php if ($log[2] == "Edit <br>Rate-Plan") {
                $oldRoomolog = explode("~", $log[5]);
                $newRoomolog = explode("~", $log[6]);

                if( $oldRoomolog != $newRoomolog ) { ?>
                  <tr>
                    <td><?=substr($log[1], 1, -1)?></td>                       
                    <td><?=$log[2]?></td> 
                    <td><?=$log[4]?></td> 
                    <td>
                    <?php $i=1;
                      while ($i <= 4){
                        $oldof = explode("#", $oldRoomolog[$i]);
                        if($oldRoomolog[$i] != $newRoomolog[$i]){ echo " ".$oldof[0]." "."<marker style='background-color:yellow;'>".$oldof[1]."</marker>"; }
                        $i=$i+1; } ?>  
                    </td>
                    <td> 
                    <?php $i=1;
                      while ($i <= 4){
                        $newof = explode("#", $newRoomolog[$i]);
                        if($oldRoomolog[$i] != $newRoomolog[$i]){ echo " ".$newof[0]." "."<marker style='background-color:cyan;'>".$newof[1]."</marker>"; }
                        $i=$i+1; } ?> 
                    </td>
                    <td><?=$log[7]?></td>
          <?php } 
              }   ?>       
<!--  END OF VIEW LOG - EDIT RATE PLAN -->

              <?php
                }
              }else{
              ?>
              <tr>
                <td colspan="6"><i>No activity log available</i></td>
              </tr>
              <?php
              }
              ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>
