<button type="button" class="pure-button orange category">Create New Category</button><button type="button" class="pure-button blue add"><i class="fa fa-cube"></i>Create New Extra</button> <button type="button" class="pure-button green template"><i class="fa fa-gift"></i>Create from Template</button>
<span class="status-green square">sts</span> Active &nbsp;&nbsp;&nbsp; <span class="status-black square">sts</span> Inactive &nbsp;&nbsp;&nbsp; <span class="status-red square">sts</span> Expired
</ul>

<?php

	include("../ajax-include-file.php");

	$datenow = date("Y-m-d");
	$hoteloid = $_REQUEST['hoteloid'];

	try {
		$main_query = "select h.hotelcode, e.*, c.currencycode, (case when e.publishedoid = 1 and e.endbook < '".$datenow."' then 'status-red' when e.publishedoid = 1 then 'status-green' else 'status-black' end) as status from extra e inner join currency c using (currencyoid) inner join hotel h using (hoteloid)";
		$filter = array();
		if(isset($_REQUEST['category']) and $_REQUEST['category']!=''){
			array_push($filter, 'e.category like "%'.$_REQUEST['category'].'%"');
		}
		if(isset($_REQUEST['name']) and $_REQUEST['name']!=''){
			array_push($filter, 'e.name like "%'.$_REQUEST['name'].'%"');
		}
		array_push($filter, 'h.hoteloid = "'.$hoteloid.'"');
		array_push($filter, 'e.publishedoid not in (3)');
		if(count($filter) > 0){
			$combine_filter = implode(' and ',$filter);
			$query = $main_query.' where '.$combine_filter;
		}else{
			$query = $main_query;
		}

		$stmt = $db->query($query);
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
?>
<table class="table promo-table">
	<tr>
		<td>Category</td>
		<td>Extra Name</td>
		<td>Price</td>
		<td>Show until</td>
		<td class="algn-right">&nbsp;</td>
	</tr>
<?php
			$r_promo = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_promo as $row){
				$expirydate = date("d/M/Y", strtotime($row['endbook']));
				
				$link_extra = 'https://thebuking.com/ibe/index.php?hcode='.$row['hotelcode'].'&extra='.urlencode($row['name']);
?>
    <tr>
    	<td><?php echo $row['categoryname']; ?></td>
        <td class="<?php echo $row['status']; ?>"><?php echo $row['name']; ?></td>
        <td><?php echo $row['currencycode']." ".number_format($row['price']); ?></td>
        <td><?php echo $expirydate; ?></td>
        <td class="algn-right">
        <button type="button" class="pure-button green single edit" pid="<?php echo $row['extraoid']; ?>"><i class="fa fa-pencil"></i></button>
        <button type="button" class="pure-button orange single copy-link-list" title="Copy Extra Link" link="<?=$link_extra;?>"><i class="fa fa-link"></i></button>
        <button type="button" class="pure-button red single delete" pid="<?php echo $row['extraoid']; ?>"><i class="fa fa-trash"></i></button>
        </td>
    </tr>
<?php
			}
?>
</table>
<?php
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		print($ex);
		die();
	}
?>
