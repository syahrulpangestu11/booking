<?php
	$name = (isset($_POST['name'])) ? $_POST['name'] : "";
	$published = $_POST['published'];
	$category = $_POST['category'];
	$stock = $_POST['qtystock'];
	$salefrom = date("Y-m-d", strtotime($_POST['salefrom']));
	$saleto = date("Y-m-d", strtotime($_POST['saleto']));

	$headline = $_POST['headline'];
	$description = $_POST['description'];
	
	$currency = $_POST['currency'];
	$price = $_POST['price'];
	$type = $_POST['type'];
	
	try {
		$stmt = $db->prepare("insert into extra (hoteloid, name, categoryname, stock, headline, description, startbook, endbook, currencyoid, price, type, publishedoid) values (:hoid, :a, :b, :c, :d, :e, :f, :g, :h, :i, :j, :k)");
		$stmt->execute(array(':hoid' => $hoteloid, ':a' => $name, ':b' => $category, ':c' => $stock, ':d' => $headline, ':e' => $description, ':f' => $salefrom, ':g' => $saleto, ':h' => $currency, ':i' => $price, ':j' => $type, ':k' => $published));
		$extraoid = $db->lastInsertId();
		
	$allowedExtension = array('jpg', 'jpeg', 'png');
	$tmp = explode('.', $_FILES['image']['name']);
	$extension = end($tmp);
	$image = '';	
	if(!empty($_FILES['image']['name']) and isset($_FILES['image']['name'])){
		if(in_array($extension, $allowedExtension)){
			$imagename = str_replace(' ', '_', $_FILES['image']['name']);
			$imagename = str_replace('.'.$extension, '', $imagename);
			$ext = '.'.strtolower($extension);
			$image_new_name = $hcode.substr($imagename, 0, 10).date('Ymd_His').$ext;
			$folder_destination = 'pict/extra/';
			
			move_uploaded_file($_FILES['image']['tmp_name'], $upload_base.$folder_destination.$image_new_name);
			$image = $web_url.'/'.$folder_destination.$image_new_name;
			
			try {
				$stmt = $db->prepare("update extra set picture = :a  where extraoid = :eoid");
				$stmt->execute(array(':a' => $image, ':eoid' => $extraoid));
			}catch(PDOException $ex) {
				echo "Invalid Query"; print($ex);
				die();
			}

		}else{
			echo'<script>alert("Data not saved.\\nInvalid file.")</script>';
		}
	}
		
	?>
		<script type="text/javascript">
            $(function(){ $( document ).ready(function(){ $("#dialog-success").dialog("open"); }); });
        </script>
	<?php
	}catch(PDOException $ex) {
	?>
		<script type="text/javascript">
            $(function(){ $( document ).ready(function(){ $("#dialog-error").dialog("open"); }); });
        </script>
	<?php	
	}
?>