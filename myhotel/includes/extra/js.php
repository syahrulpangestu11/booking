<script type="text/javascript">
$(function(){

	$(document).ready(function(){

		getLoadData();
	});


	$('body').on('click','button.edit', function(e) {
		pid = $(this).attr("pid");
		url = "<?php echo $base_url; ?>/extra/edit/?pid="+pid;
      	$(location).attr("href", url);
	});
	$('body').on('click','button.editcategory', function(e) {
		pid = $(this).attr("pid");
		url = "<?php echo $base_url; ?>/extra/editcategory/?pid="+pid;
      	$(location).attr("href", url);
	});	
	$('body').on('click','button.add', function(e) {
		pid = $(this).attr("pid");
		url = "<?php echo $base_url; ?>/extra/add";
      	$(location).attr("href", url);
	});
	$('body').on('click','button.addcategory', function(e) {
		pid = $(this).attr("pid");
		url = "<?php echo $base_url; ?>/extra/add-category";
      	$(location).attr("href", url);
	});
	$('body').on('click','button.category', function(e) {
		pid = $(this).attr("pid");
		url = "<?php echo $base_url; ?>/extra/category";
      	$(location).attr("href", url);
	});
	$('body').on('click','button.template', function(e) {
		url = "<?php echo $base_url; ?>/extra/template";
      	$(location).attr("href", url);
	});
	$('body').on('click','button.cancel', function(e) {
		url = "<?php echo $base_url; ?>/extra";
      	$(location).attr("href", url);
	});
	$('body').on('click','button.copy', function(e) {
		pid = $(this).attr("pid");
		url = "<?php echo $base_url; ?>/extra/add/?tid="+pid;
      	$(location).attr("href", url);
	});

   var $dialogDelete = $('<div id="dialog-del"></div>')
	.html('Are you sure you want to delete this data?')
	.dialog({
		autoOpen: false,
		title: 'Confirmation',
		buttons: {
			"Delete": function(){
				var url = $(this).data('url');
				var id = $(this).data('id');
				$(this).dialog("close");
				deleteAction(url, id);
			},
			Cancel: function(){ $( this ).dialog("close"); }
		}
	});
	var $dialogDelete1 = $('<div id="dialog-del"></div>')
	.html('Are you sure you want to delete this data?')
	.dialog({
		autoOpen: false,
		title: 'Confirmation',
		buttons: {
			"Delete": function(){
				var url = $(this).data('url');
				var id = $(this).data('id');
				$(this).dialog("close");
				deleteAction1(url, id);
			},
			Cancel: function(){ $( this ).dialog("close"); }
		}
	});

	var $dialogNotification = $('<div id="dialog-notice"></div>')
	.dialog({
		autoOpen: false,
		title: 'Notification',
		buttons: {
			"Ok": function(){
				var callback = '<?php echo $base_url; ?>/extra';
				$(this).dialog( "close" );
				$(location).attr("href", callback);
			}
		}
	});
	var $dialogNotification1 = $('<div id="dialog-notice"></div>')
	.dialog({
		autoOpen: false,
		title: 'Notification',
		buttons: {
			"Ok": function(){
				var callback = '<?php echo $base_url; ?>/extra/category';
				$(this).dialog( "close" );
				$(location).attr("href", callback);
			}
		}
	});
	function deleteAction(url, id){
		$.ajax({
			url: url,
			type: 'post',
			data: { id : id},
			success: function(data) {
				if(data == "1"){
					$dialogNotification.html("Data has been deleted");
					$dialogNotification.dialog("open");
				}else{
					$dialogNotification.html("We&rsquo;re very sorry, we can't save your data right now");
					$dialogNotification.dialog("open");
				}
			}
		});
	}

	function deleteAction1(url, id){
		$.ajax({
			url: url,
			type: 'post',
			data: { id : id},
			success: function(data) {
				if(data == "1"){
					$dialogNotification1.html("Data has been deleted");
					$dialogNotification1.dialog("open");
				}else{
					$dialogNotification1.html("We&rsquo;re very sorry, we can't save your data right now");
					$dialogNotification1.dialog("open");
				}
			}
		});
	}

	$('body').on('click','button.delete', function(e) {
		id = $(this).attr("pid");
		$dialogDelete.data("url", "<?php echo $base_url; ?>/includes/extra/delete-extra.php");
		$dialogDelete.data("id", id);
		$dialogDelete.dialog("open");
	});
	$('body').on('click','button.deletecategory', function(e) {
		id = $(this).attr("pid");
		$dialogDelete1.data("url", "<?php echo $base_url; ?>/includes/extra/delete-categoryextra.php");
		$dialogDelete1.data("id", id);
		$dialogDelete1.dialog("open");
	});

	function getLoadData(){
		$.ajax({
			url: "<?php echo"$base_url"; ?>/includes/extra/data.php",
			type: 'post',
			data: $('form#data-input').serialize(),
			success: function(data) {
				$("#data-box").html(data)
			}
		});
	}

   $("#dialog-error, #dialog-success").dialog({
	  autoOpen: false,
	  buttons: { "Ok": redirect },
	  close: function(event, ui) { redirect }
   });

   function redirect(){
	   $(location).attr("href", "<?php echo $base_url; ?>/extra");
   }

   $("#dialog-error-category, #dialog-success-category").dialog({
	  autoOpen: false,
	  buttons: { "Ok": redirect_category },
	  close: function(event, ui) { redirect_category }
   });

   function redirect_category(){
	   $(location).attr("href", "<?php echo $base_url; ?>/extra/category");
   }

   $('input[name=image]').change(function() {
			if(this.files[0].size > 200000){
			  	alert("Your image more than 200 KB, please resize your image");
			  	$(this).val("");
			  	$('#preview').attr('src',"#");
			}else{
			  	var ext = $(this).val().split('.').pop().toLowerCase();
				if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
				    alert('Invalid extension! Please upload image with JPEG, PNG, JPG, and GIF type only.');
				    $(this).val("");
			  		$('#preview').attr('src',"#");
					$('#preview').hide;
				}else{
					readURL(this);
				}
			}
   });

	function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#preview')
                    .attr('src', e.target.result)
                    .width(350)
                    .show();
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $('body').on('click','button.copy-link-list', function(e) {
		var thisLink = $(this).attr('link');
		var $temp = $("<input>");
		$("body").append($temp);
		$temp.val(thisLink).select();
		document.execCommand("copy");
		$temp.remove();
	});

	$('body').on('keyup','.utmset', function(e) {
		textarea = $('textarea[name="direct-link"]');
		var snurl = textarea.val();
		snurl = snurl + "&utm_source=" + $('input[name="utm_source"]').val() + "&utm_campaign=" + $('input[name="utm_campaign"]').val();
		textarea2 = $('textarea[name="direct2-link"]');
		textarea2.val(snurl);
	});
	
	$('body').on('click','button.copy2-link', function(e) {
		var $temp = $("<input>");
		$("body").append($temp);
		$temp.val($('textarea[name="direct2-link"]').val()).select();
		document.execCommand("copy");
		$temp.remove();
	});
});
</script>

<div id="dialog-error" title="Error Notification">
  <p>We&rsquo;re very sorry, we can't save your data right now.</p>
</div>
<div id="dialog-success" title="Confirmation Notice">
  <p>Data succesfully updated</p>
</div>
<div id="dialog-error-category" title="Error Notification">
  <p>We&rsquo;re very sorry, we can't save your data right now.</p>
</div>
<div id="dialog-success-category" title="Confirmation Notice">
  <p>Data succesfully updated</p>
</div>

<style>
img#preview{ max-width:100%; height:auto; }
</style>
