<style>
    .form-box ul li span.label{
        color: black;
        font-size: 14px;
        font-weight: normal;
        text-align: left;}
     input[type=file]{display: inline-block;}
</style>
<script type="text/javascript">
$(function() {
	$('textarea').trumbowyg({
		btns: ['viewHTML',
		  '|', 'btnGrp-design',
		  '|', 'link',
		  '|', 'btnGrp-justify',
		  '|', 'btnGrp-lists'],
		fullscreenable: false
	});
});
</script>
<section class="content-header">
    <h1>
        Create Extra
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i>  Allotments &amp; Rates </a></li>
        <li class="active">Extra</li>
    </ol>
</section>
<section class="content">
	<div class="row">
        <form class="form-box" method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/extra/add-process-category">
        <div class="box box-form">
            <h1>Category</h1>
        <ul class="inline-half">
            <li>
            <ul class="block">
                <li>
                    <span class="label">Category Name:</span>
                    <input type="text" class="long" name="name">
                </li>
            </ul>
            <li>
            <ul class="block">
                <li>
                    <span class="label">Publish Category :</span>
                    <select name="published">
                    <?php
                        try {
                            $stmt = $db->query("select * from published where showthis = 'y'");
                            $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                            foreach($r_room as $row){
                                echo"<option value='".$row['publishedoid']."'>".$row['note']."</option>";
                            }
                        }catch(PDOException $ex) {
                            echo "Invalid Query";
                            die();
                        }
                    ?>
                    </select>
                </li>
            </ul>
            </li>
        </ul>
        <button class="default-button" type="submit">Submit Extra</button>
        </div>
        <div class="box box-form">
            <h2>HEADLINE &amp; DETAIL</h2>
            <ul class="inline-half">
                <li>
                    <h3>Description</h3>
                    <textarea name="description"><?=$description?></textarea>
                </li>
            </ul>
            <div class="clear"></div>
            <div class="clear"></div>
            <button class="default-button" type="submit">Submit Extra</button>
        </div>
		</form>
    </div>
</section>
