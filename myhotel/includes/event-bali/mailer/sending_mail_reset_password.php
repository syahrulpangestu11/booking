<?php
	ob_start();
	include 'mail-content/mail-reset-password.php';
	$body = ob_get_clean();

	require_once('includes/mailer/class.phpmailer.php');
	$mail = new PHPMailer(true); // the true param means it will throw exceptions on errors, which we need to catch
	//$mail->IsSMTP(); // telling the class to use SMTP
	try {

		$mail->SetFrom($sender_email, $sender_name);
		$mail->AddReplyTo($sender_email, $sender_name);
		
		$mail->AddAddress($recipient_email, $recipient_name);
	
		
		$mail->Subject = 'IBEIND Extranet Password Reset Request';
		$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!'; // optional - MsgHTML will create an alternate automatically
		$mail->MsgHTML($body);
	
		if($mail->Send()) {
			echo"<script>document.location.href='".$base_url."/resetsuccess';</script>";
		}else{
			echo"<script>document.location.href='".$base_url."/resetfailed';</script>";
		}
		
    } catch (phpmailerException $e) { echo"<script>document.location.href='".$base_url."/resetfailed';</script>";
	} catch (Exception $e) { echo"<script>document.location.href='".$base_url."/resetfailed';</script>";
	} 
?>