<?php
	$promocodeoid = $_GET['pid'];
	try {
		$stmt = $db->query("select pc.* from promocode pc left join hotel h using (hoteloid) where promocodeoid = '".$promocodeoid."'");
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
			$pc = $stmt->fetch(PDO::FETCH_ASSOC);
			$startdate = date("d F Y", strtotime($pc['startdate']));
			$enddate = date("d F Y", strtotime($pc['enddate']));
		}else{
			echo "No Result";
			die();
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}
?>
<script type="text/javascript">
$(function() {
	$('textarea').trumbowyg({
		btns: ['viewHTML',
		  '|', 'btnGrp-design',
		  '|', 'link',
		  '|', 'btnGrp-justify',
		  '|', 'btnGrp-lists'],
		fullscreenable: false
	});
});
</script>
<section class="content-header">
    <h1>
        Edit Promo Code
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i>  Allotments &amp; Rates </a></li>
        <li class="active">Promo Code</li>
    </ol>
</section>
<section class="content">
	<form class="form-box" method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/promocode/edit-process">
	<input type="hidden" name="promocodeoid" value="<?php echo $pc['promocodeoid']; ?>"/>
	<div class="row">
        <div class="box box-form">
            <h1>Promo Code</h1>
            <ul class="inline-half colored">
				<li>
                    <ul class="block">
                        <li><h3>DETAIL PROMO CODE</h3></li>
                        <li>
                            <span class="label"><b>Promo Code:</b></span>
                            <input type="text" class="medium" name="code" required="required" value="<?php echo $pc['promocode']; ?>">
                        </li>
                        <li>
                            <span class="label">Promo Code Name:</span>
                            <input type="text" class="long" name="name" value="<?php echo $pc['name']; ?>">
                        </li>
                        <li>
                            <h3>Description :<!----></h3>
                            <textarea name="description"><?php echo $pc['description']; ?></textarea>
                            <div class="clear"></div>
                        </li>
                        <div class="clear"></div>
                        <li>
                        	<div class="clear"></div>
                            <span class="label">Publish Promo Code :</span>
                            <select name="published">
                            <?php
                                try {
                                    $stmt = $db->query("select * from published where showthis = 'y'");
                                    $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                    foreach($r_room as $row){
										if($row['publishedoid'] == $pc['publishedoid']){ $selected = "selected"; }else{ $selected=""; }
                                        echo"<option value='".$row['publishedoid']."' ".$selected.">".$row['note']."</option>";
                                    }
                                }catch(PDOException $ex) {
                                    echo "Invalid Query";
                                    die();
                                }
                            ?>
                            </select>
                        </li>
                	</ul>
                    <?php if($_SESSION['_typeusr'] == '1'){?>
                    <ul class="block">
                        <li><h3>Allow Zero Transaction</h3></li>
                        <li>
                            <span class="label">Check to allow :</span>
                            <input type="checkbox" name="allowzerotrx" value="1" <?php if($pc['allowzerotrx'] == '1') echo 'checked=checked'; ?>>
                        </li>
                    </ul>
                    <?php }?>
                </li>
								<li>
                	<ul class="block">
                        <li><h3>PERIODE OF PROMO CODE</h3></li>
                        <li>
                            <span class="label">Start Date From:</span>
                            <input type="text"class="medium" id="startdate" name="startdate" required="required" value="<?php echo $startdate; ?>" autocomplete="off">
                        </li>
                        <li>
                            <span class="label">End Date To:</span>
                            <input type="text"class="medium" id="enddate" name="enddate" required="required" value="<?php echo $enddate; ?>" autocomplete="off">
                        </li>
                        <li><h3>DISCOUNT</h3></li>
                        <li>
                            <span class="label">Discount Type:</span>
                            <select name="discounttype" class="input-select">
                                <?php
                                	$codetype = array('discount percentage', 'discount amount');
                                    foreach($codetype as $value){
										if($value == $pc['discounttype']){ $selected = "selected"; $dataval=$pc['discount']; }else{ $selected=""; $dataval=""; }
                                ?>
                                    <option value="<?php echo $value; ?>" <?php echo $selected; ?> data="<?php echo $dataval; ?>"><?php echo $value; ?></option>
                                <?php
                                    }
                                ?>
                            </select>
                        </li>
                        <li id="discount-value"></li>
                        <li><h3>COMMISSION</h3></li>
                        <li>
                            <span class="label">Commission Type:</span>
                            <select name="commissiontype" class="input-select">
                                <?php
                                	$codetype = array('commission percentage', 'commission amount');
                                    foreach($codetype as $value){
										if($value == $pc['commissiontype']){ $selected = "selected"; $dataval=$pc['commission']; }else{ $selected=""; $dataval=""; }
                                ?>
                                    <option value="<?php echo $value; ?>" <?php echo $selected; ?> data="<?php echo $dataval; ?>"><?php echo $value; ?></option>
                                <?php
                                    }
                                ?>
                            </select>
                        </li>
                        <li id="commission-value"></li>
                        <li>
                            <span class="label"><b>PIC Name:</b></span>
                            <input type="text" class="medium" name="pic_name" value="<?php echo $pc['pic_name']; ?>">
                        </li>
                        <li>
                            <span class="label"><b>PIC Contact Number:</b></span>
                            <input type="text" class="medium" name="pic_number" value="<?php echo $pc['pic_number']; ?>">
                        </li>
                    </ul>

                </li>
            </ul>
            <div class="clear"></div>
        </div>
    </div>
    <div class="row">
    	<div class="box box-form">
            <h2>Apply Promo Code to :</h2><br />
            <table class="table table-fill table-fill-centered">
                <tr>
                    <td>Select</td>
                    <td>Promotion</td>
                    <td>Apply Discount</td>
                    <td>Apply Commission</td>
                </tr>
                <tr class="list">
                    <td>
                    <?php if($pc['applybar'] == "y"){ $checked = "checked"; }else{ $checked=""; } ?>
                    <input type="checkbox" name="applybar" value="y" <?php echo $checked; ?> />
                    </td>
                    <td style="text-align:left;">Best Flexible Rate</td>
                    <td>
                    <?php if($pc['applybardiscount'] == "y"){ $checked = "checked"; }else{ $checked=""; } ?>
                    <input type="checkbox" name="applybardiscount" value="y" <?php echo $checked; ?>/>
                    </td>
                    <td>
                    <?php if($pc['applybarcommission'] == "y"){ $checked = "checked"; }else{ $checked=""; } ?>
                    <input type="checkbox" name="applybarcommission" value="y" <?php echo $checked; ?>/>
                    </td>
                </tr>
                <?php
                    try {
    $stmt = $db->query("
select 'promotion' as promotype, p.promotionoid as oid, p.name, (case when p.publishedoid = 1 and p.saleto < '".$datenow."' then 'status-red' when p.publishedoid = 1 then 'status-green' else 'status-black' end) as status from promotion p left join promotiontype pt using (promotiontypeoid) inner join discounttype dt using (discounttypeoid) inner join hotel h using (hoteloid) inner join promotionapply pa using (promotionoid) inner join roomoffer ro using (roomofferoid) inner join room r using (roomoid) inner join channel ch using (channeloid) where h.hoteloid = '".$hoteloid."' and p.publishedoid not in (3) group by p.promotionoid
union all
select 'package' as promotype, p.packageoid as oid, p.name, (case when p.publishedoid = 1 and p.saleto < '".$datenow."' then 'status-red' when p.publishedoid = 1 then 'status-green' else 'status-black' end) as status from package p inner join hotel h using (hoteloid) inner join packageapply pa using (packageoid) inner join roomoffer ro using (roomofferoid) inner join room r using (roomoid) inner join channel ch using (channeloid) where h.hoteloid = '".$hoteloid."' and p.publishedoid not in (3) group by p.packageoid
");
    $r_promo = $stmt->fetchAll(PDO::FETCH_ASSOC);
    foreach($r_promo as $rowpromo){
    	$stmt = $db->prepare("select * from promocodeapply where promocodeoid = :a and referencetable = :b and id = :c");
		$stmt->execute(array(':a' => $pc['promocodeoid'], ':b' => $rowpromo['promotype'], ':c' =>  $rowpromo['oid']));
		$row_count = $stmt->rowCount();
		if($row_count > 0){
			$rowpromo_found = $stmt->fetch(PDO::FETCH_ASSOC);
			$apply = 'y';
			$applydiscount = $rowpromo_found['applydiscount'];
			$applycommission = $rowpromo_found['applycommission'];
		}else{
			$apply = 'n'; $applydiscount = 'n'; $applycommission = 'n';
		}
    ?>
                <tr class="list">
                    <td>
                    <?php if($apply == "y"){ $checked = "checked"; }else{ $checked=""; } ?>
                    <input type="checkbox" name="apply[]" value="<?php echo $rowpromo['promotype']; ?>-<?php echo $rowpromo['oid']; ?>"  <?php echo $checked; ?>/>
                    </td>
                    <td style="text-align:left;"><?php echo $rowpromo['name']; ?></td>
                    <td>
                    <?php if($applydiscount == "y"){ $checked = "checked"; }else{ $checked=""; } ?>
                    <input type="checkbox" name="applydiscount-<?php echo $rowpromo['promotype']; ?>-<?php echo $rowpromo['oid']; ?>" value="y" <?php echo $checked; ?>/>
                    </td>
                    <td>
                    <?php if($applycommission == "y"){ $checked = "checked"; }else{ $checked=""; } ?>
                    <input type="checkbox" name="applycommission-<?php echo $rowpromo['promotype']; ?>-<?php echo $rowpromo['oid']; ?>" value="y" <?php echo $checked; ?>/>
                    </td>
                </tr>
    <?php
    }
                    }catch(PDOException $ex) {
                        echo "Invalid Query";
                        print($ex);
                        die();
                    }
                ?>
            </table>
            <div class="clear"></div>
            <button class="default-button" type="submit">Save Promo Code</button>
        </div>
    </div>
	</form>
</section>
