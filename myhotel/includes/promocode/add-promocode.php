<script type="text/javascript">
$(function() {
	$('textarea').trumbowyg({
		btns: ['viewHTML',
		  '|', 'btnGrp-design',
		  '|', 'link',
		  '|', 'btnGrp-justify',
		  '|', 'btnGrp-lists'],
		fullscreenable: false
	});
});
</script>
<section class="content-header">
    <h1>
        Create Promo Code
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i>  Allotments &amp; Rates </a></li>
        <li class="active">Promo Code</li>
    </ol>
</section>
<section class="content">
	<form class="form-box" method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/promocode/add-process">

	<div class="row">
        <div class="box box-form">
            <h1>Promo Code</h1>
            <ul class="inline-half colored">
				<li>
                    <ul class="block">
                        <li><h3>DETAIL PROMO CODE</h3></li>
                        <li>
                            <span class="label"><b>Promo Code:</b></span>
                            <input type="text" class="medium" name="code" required="required">
                        </li>
                        <li>
                            <span class="label">Promo Code Name:</span>
                            <input type="text" class="long" name="name">
                        </li>
                        <li>
                            <h3>Description :<!----></h3>
                            <textarea name="description"></textarea>
                            <div class="clear"></div>
                        </li>
                        <div class="clear"></div>
                        <li>
                        	<div class="clear"></div>
                            <span class="label">Publish Promo Code :</span>
                            <select name="published">
                            <?php
                                try {
                                    $stmt = $db->query("select * from published where showthis = 'y'");
                                    $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                    foreach($r_room as $row){
                                        echo"<option value='".$row['publishedoid']."'>".$row['note']."</option>";
                                    }
                                }catch(PDOException $ex) {
                                    echo "Invalid Query";
                                    die();
                                }
                            ?>
                            </select>
                        </li>
                	</ul>
                </li>
								<li>
                	<ul class="block">
                        <li><h3>PERIODE OF PROMO CODE</h3></li>
                        <li>
                            <span class="label">Start Date From:</span>
                            <input type="text"class="medium" id="startdate" name="startdate" required="required" autocomplete="off">
                        </li>
                        <li>
                            <span class="label">End Date To:</span>
                            <input type="text"class="medium" id="enddate" name="enddate" required="required" autocomplete="off">
                        </li>
                        <li><h3>DISCOUNT</h3></li>
                        <li>
                            <span class="label">Discount Type:</span>
                            <select name="discounttype" class="input-select">
                                <?php
                                	$codetype = array('discount percentage', 'discount amount');
                                    foreach($codetype as $value){
                                ?>
                                    <option value="<?php echo $value; ?>" data=""><?php echo $value; ?></option>
                                <?php
                                    }
                                ?>
                            </select>
                        </li>
                        <li id="discount-value"></li>
                        <li><h3>COMMISSION</h3></li>
                        <li>
                            <span class="label">Commission Type:</span>
                            <select name="commissiontype" class="input-select">
                                <?php
                                	$codetype = array('commission percentage', 'commission amount');
                                    foreach($codetype as $value){
                                ?>
                                    <option label="" value="<?php echo $value; ?>" data=""><?php echo $value; ?></option>
                                <?php
                                    }
                                ?>
                            </select>
                        </li>
                        <li id="commission-value"></li>
                        <li>
                            <span class="label"><b>PIC Name:</b></span>
                            <input type="text" class="medium" name="pic_name">
                        </li>
                        <li>
                            <span class="label"><b>PIC Contact Number:</b></span>
                            <input type="text" class="medium" name="pic_number">
                        </li>
                    </ul>

                </li>
            </ul>
            <div class="clear"></div>
        </div>
    </div>
    <div class="row">
    	<div class="box box-form">
            <h2>Apply Promo Code to :</h2><br />
            <table class="table table-fill table-fill-centered">
                <tr>
                    <td>Select</td>
                    <td>Promotion</td>
                    <td>Apply Discount</td>
                    <td>Apply Commission</td>
                </tr>
                <tr class="list">
                    <td><input type="checkbox" name="applybar" value="y" checked="checked" /></td>
                    <td style="text-align:left;">Best Flexible Rate</td>
                    <td><input type="checkbox" name="applybardiscount" value="y" checked="checked"/></td>
                    <td><input type="checkbox" name="applybarcommission" value="y" checked="checked"/></td>
                </tr>
                <?php
                    try {
    $stmt = $db->query("
select 'promotion' as promotype, p.promotionoid as oid, p.name, (case when p.publishedoid = 1 and p.saleto < '".$datenow."' then 'status-red' when p.publishedoid = 1 then 'status-green' else 'status-black' end) as status from promotion p left join promotiontype pt using (promotiontypeoid) inner join discounttype dt using (discounttypeoid) inner join hotel h using (hoteloid) inner join promotionapply pa using (promotionoid) inner join roomoffer ro using (roomofferoid) inner join room r using (roomoid) inner join channel ch using (channeloid) where h.hoteloid = '".$hoteloid."' and p.publishedoid not in (3) group by p.promotionoid
union all
select 'package' as promotype, p.packageoid as oid, p.name, (case when p.publishedoid = 1 and p.saleto < '".$datenow."' then 'status-red' when p.publishedoid = 1 then 'status-green' else 'status-black' end) as status from package p inner join hotel h using (hoteloid) inner join packageapply pa using (packageoid) inner join roomoffer ro using (roomofferoid) inner join room r using (roomoid) inner join channel ch using (channeloid) where h.hoteloid = '".$hoteloid."' and p.publishedoid not in (3) group by p.packageoid
");
    $r_promo = $stmt->fetchAll(PDO::FETCH_ASSOC);
    foreach($r_promo as $row){
    ?>
                <tr class="list">
                    <td><input type="checkbox" name="apply[]" value="<?php echo $row['promotype']; ?>-<?php echo $row['oid']; ?>" /></td>
                    <td style="text-align:left;"><?php echo $row['name']; ?></td>
                    <td><input type="checkbox" name="applydiscount-<?php echo $row['promotype']; ?>-<?php echo $row['oid']; ?>" value="y"/></td>
                    <td><input type="checkbox" name="applycommission-<?php echo $row['promotype']; ?>-<?php echo $row['oid']; ?>" value="y"/></td>
                </tr>
    <?php
    }
                    }catch(PDOException $ex) {
                        echo "Invalid Query";
                        print($ex);
                        die();
                    }
                ?>
            </table>
            <div class="clear"></div>
            <button class="default-button" type="submit">Save Promo Code</button>
        </div>
    </div>
	</form>
</section>
