<?php
	$startdate = date("Y-m-d", strtotime($_POST['startdate']));
	$enddate = date("Y-m-d", strtotime($_POST['enddate']));
	$applybar = (isset($_POST['applybar'])) ? $_POST['applybar'] : "n";	
	$applybardiscount = (isset($_POST['applybardiscount'])) ? $_POST['applybardiscount'] : "n";	
	$applybarcommission = (isset($_POST['applybarcommission'])) ? $_POST['applybarcommission'] : "n";	

	try {
		$stmt = $db->prepare("insert into promocode (hoteloid, promocode, name, description, startdate, enddate, discounttype, discount, commissiontype, commission, applybar, applybardiscount, applybarcommission, pic_name, pic_number, publishedoid) values (:hoid, :a, :b, :c, :d, :e, :f, :g, :h, :i, :j, :k, :l, :m, :n, :o)");
		$stmt->execute(array(':hoid' => $hoteloid, ':a' => $_POST['code'], ':b' => $_POST['name'], ':c' => $_POST['description'], ':d' => $startdate, ':e' => $enddate, ':f' => $_POST['discounttype'], ':g' => $_POST['discount'], ':h' => $_POST['commissiontype'], ':i' => $_POST['commission'], ':j' => $applybar, ':k' => $applybardiscount, ':l' => $applybarcommission, ':m' => $_POST['pic_name'], ':n' => $_POST['pic_number'], ':o' => $_POST['published']));
		$promocodeoid = $db->lastInsertId();
		
		if(isset($_POST['apply'])){
			foreach($_POST['apply'] as $key => $value){
				$value2 = explode('-', $value);
				$reference = $value2[0];
				$id = $value2[1];
				$applydiscount = (isset($_POST['applydiscount-'.$value])) ? $_POST['applydiscount-'.$value] : "n";	
				$applycommission = (isset($_POST['applycommission-'.$value])) ? $_POST['applycommission-'.$value] : "n";	

				$stmt = $db->prepare("insert into promocodeapply (promocodeoid, referencetable, id, applydiscount, applycommission) values (:a, :b, :c, :d, :e)");
				$stmt->execute(array(':a' => $promocodeoid, ':b' => $reference, ':c' => $id, ':d' => $applydiscount, ':e' => $applycommission));
			}
		}
		
	?>
		<script type="text/javascript">
            $(function(){ $( document ).ready(function(){ $("#dialog-success").dialog("open"); }); });
        </script>
	<?php
	}catch(PDOException $ex) {
	?>
		<script type="text/javascript">
            $(function(){ $( document ).ready(function(){ $("#dialog-error").dialog("open"); }); });
        </script>
	<?php	
	}
?>