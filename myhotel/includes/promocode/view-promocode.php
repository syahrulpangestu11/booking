 <?php
	if(empty($_REQUEST['rt']) and empty($_REQUEST['sdate']) and empty($_REQUEST['edate'])){
		$start = date("d F Y");
		$end = date("d F Y",  strtotime(date("Y-m-d")." +1 month" ));
	}else{
		$start = date("d F Y", strtotime($_REQUEST['sdate']));
		$end = date("d F Y", strtotime($_REQUEST['edate']));
	}

	include("js.php");
?>
<section class="content-header">
    <h1>
       	Promo Code
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Allotments &amp; Rates</a></li>
        <li class="active">Promo Code</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="box">
        	<form method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/promocode">
  					<input type="hidden" name="page">
            <input type="hidden" name="hoteloid" value="<?php echo $hoteloid; ?>"  />
                <div id="data-box" class="box-body">
                    <div class="loader">Loading...</div>
                </div><!-- /.box-body -->
            </form>
       </div>
    </div>

</section>
