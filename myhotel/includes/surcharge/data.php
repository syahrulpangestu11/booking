<?php
include("../ajax-include-file.php");

$daylist = array("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat");

$startdate = date("Y-m-d", strtotime($_REQUEST['sdate']));
$enddate = date("Y-m-d", strtotime($_REQUEST['edate']));
$roomoffer = $_REQUEST['rt'];
$channeloid = $_REQUEST['ch'];


		$filter_periode = "
	( 
		(startdate <= '".$startdate."' and (enddate >= '".$startdate."' and enddate <= '".$enddate."'))  
		or
		((startdate >= '".$startdate."' and startdate <= '".$enddate."') and enddate >= '".$enddate."')
		or
		((startdate >= '".$startdate."' and startdate <= '".$enddate."') and enddate <= '".$enddate."')
		or
		(startdate <= '".$startdate."' and enddate >= '".$enddate."')
		or
		(startdate >= '".$startdate."' and enddate <= '".$enddate."')
	)
		";
?>
<table class="table table-fill table-fill-centered">
    <tr>
        <td>Room</td>
        <td>Stay Date From</td>
        <td>To</td>
        <td>Applicable Day of Week</td>
        <td>Surcharge Type</td>
        <td>Surcharge Name</td>
        <td>Charge Type</td>
        <td>Is Commissionable</td>
        <td>Amount</td>
        <td>Currency</td>
        <td>&nbsp;</td>
    </tr>
<?php
	try {
		$stmt = $db->query("select s.*, r.name as roomname, ro.name as name from surcharge s inner join roomoffer ro using (roomofferoid) inner join room r using (roomoid) inner join channel ch using (channeloid) where ro.roomofferoid = '".$roomoffer."' and ch.channeloid = '".$channeloid."' and ".$filter_periode." and s.publishedoid != '3'");		
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
			$r_s = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_s as $row){
				$startdate = date("d F Y", strtotime($row['startdate']));
				$enddate = date("d F Y", strtotime($row['enddate']));
				$applyday = explode(',',$row['applyday']);
?>
    <tr class="list">
    	<td><?php echo $row['name']; ?></td>
        <td class="algn-center"><?php echo $startdate; ?></td>
        <td class="algn-center"><?php echo $enddate; ?></td>
        <td>
			<?php
                foreach($daylist as $day){
                    if(in_array($day, $applyday)){ $checked = "checked"; }else{ $checked = ""; } 
                    echo"<input type='checkbox' name='display[]' value='".$day."' ".$checked." readonly='readonly'>".$day."&nbsp;&nbsp;";
                }
            ?>
        </td>
        <td>&nbsp;</td>
        <td class="algn-center"><?php echo $row['name']; ?></td>
        <td>&nbsp;</td>
        <td class="algn-center">
			<?php if($row['commissionable'] == "y"){ $checked = "checked"; }else{ $checked = "";  } ?>
            <input type="checkbox" name="commissionable" readonly="readonly" value="y" <?php echo $checked; ?> />
        </td>
        <td class="algn-center"><?php echo number_format($row['value']); ?></td>
        <td>IDR</td>
        <td class="algn-center">
			<button type="button" class="trash delete" pid="<?php echo $row['surchargeoid']; ?>">Delete</button>
        </td>
    </tr>	
<?php				
			}
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		print($ex);
		die();
	}
?>
    <input type="hidden" name="roomofferoid" value="<?php echo $roomoffer; ?>" />
    <input type="hidden" name="channeloid" value="<?php echo $channeloid; ?>" />

