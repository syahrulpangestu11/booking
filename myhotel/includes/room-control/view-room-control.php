<?php
	if(empty($_REQUEST['rt']) and empty($_REQUEST['startdate']) and empty($_REQUEST['enddate'])){
		$start = date("d F Y");
		$end = date("d F Y",  strtotime(date("Y-m-d")." +7 day" ));
	}else{
		$start = date("d F Y", strtotime($_REQUEST['startdate']));
		$end = date("d F Y", strtotime($_REQUEST['enddate']));
		$roomoffer = $_REQUEST['rt'];
		$channeloid = $_REQUEST['ch'];
	}
	
	include("js.php");
?>
<section class="content-header">
    <h1>
        Room Control
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Allotments &amp; Rates</a></li>
        <li class="active">Room Control</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="box">
            <div class="box-body">
                <div class="form-group">
            <form method="get" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/room-control/">
                    <input type="hidden" name="ho" value="<?php echo $hoteloid; ?>" />
                    <label>Room Type  :</label> &nbsp;&nbsp;
                    <select name="rt" class="input-select">
                    <?php
                        try {
                            $stmt = $db->query("select r.roomoid, r.name, x.offernum from room r inner join hotel h using (hoteloid) left join (select count(roomofferoid) as offernum, ro.roomoid from roomoffer ro group by ro.roomoid) as x on x.roomoid = r.roomoid where h.hoteloid = '".$hoteloid."' and x.offernum > 0 and r.publishedoid = '1'");
                            $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                            foreach($r_room as $row){
                                echo"<optgroup label = '".$row['name']."'>";
                                try {
                                    $stmt = $db->query("select ro.* from roomoffer ro inner join published p using (publishedoid) inner join room r using (roomoid) inner join offertype using (offertypeoid) where r.roomoid = '".$row['roomoid']."' and p.publishedoid = '1'");
                                    $r_offer = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                    foreach($r_offer as $row1){
										if($row1['roomofferoid'] == $roomoffer){ $selected = 'selected = "selected"';  }else{ $selected = ''; }
                                       echo"<option value='".$row1['roomofferoid']."' ".$selected.">  ".$row1['name']."</option>";
                                    }
                                }catch(PDOException $ex) {
                                    echo "Invalid Query";
                                    die();
                                }     
                                echo"</optgroup>";                       
                            }
                        }catch(PDOException $ex) {
                            echo "Invalid Query";
                            die();
                        }
                    
                    ?>   
                    </select>
                    &nbsp;&nbsp;
					<label>Channel :</label> &nbsp;&nbsp;
                    <select name="ch">
                    <?php
                        try {
                            $stmt = $db->query("select * from channel");
                            $r_plan = $stmt->fetchAll(PDO::FETCH_ASSOC);
                            foreach($r_plan as $row){
								if($row['channeloid'] == $channeloid){ $selected = 'selected = "selected"';  }else{ $selected = ''; }
                                echo"<option value='".$row['channeloid']."' ".$selected.">".$row['name']."</option>";
                            }
                        }catch(PDOException $ex) {
                            echo "Invalid Query";
                            die();
                        }
                    
                    ?>        	
                    </select> &nbsp;
                    <br><br>
                    <label>Date Range From </label> &nbsp;
                    <input type="text" name="startdate" readonly autocomplete="off" id="<?php if($_SESSION['_typeusr']=='1' || $_SESSION['_typeusr']=='2'){echo "startdate";} else{ echo "startdate-2m-today";}?>" placeholder="" value="<?php echo $start; ?>"> &nbsp;&nbsp;
                    <label>To </label> &nbsp;
                    <input type="text" name="enddate" readonly autocomplete="off" id="<?php if($_SESSION['_typeusr']=='1' || $_SESSION['_typeusr']=='2'){echo "enddate";} else{ echo "enddate-2m-today";}?>" placeholder="" value="<?php echo $end; ?>"> &nbsp;&nbsp;                    
                    <button type="submit" class="small-button blue"><i class="fa fa-search"></i>Find</button>
                </form>
                </div>
            </div><!-- /.box-body -->
       </div>
    </div>
    
    <div class="row">
        <div class="box">
        	<form method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/room-control/process">
                <div id="data-box" class="box-body">
                    <div class="loader">Loading...</div>
                </div><!-- /.box-body -->
            </form>
       </div>
    </div>

</section>
