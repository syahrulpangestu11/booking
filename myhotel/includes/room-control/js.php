<script type="text/javascript">
$(function(){
	$(document).ready(function(){ getLoadData(); });
	
	function getLoadData(){
		$.ajax({
			url: "<?php echo"$base_url"; ?>/includes/room-control/data.php",
			type: 'post',
			data: $('form#data-input').serialize(),
			success: function(data) {
				$("#data-box").html(data)
			}
		});
	}
	
	$('body').on('click','input[type=checkbox]', function(e) {
		var allElem = 'input:checkbox[name='+$(this).attr('name')+']';
		if($(this).prop( "checked" ) == true){
			$(allElem).attr('checked',false);
			$(this).prop('checked',true);
		}else{
			$(allElem).attr('checked',false);
		}
	});
	
	$('body').on('click','tr.autofilled button', function(e) {
		$( "tr.autofilled select" ).each(function() {
			var elemTarget = $(this).attr("tgt");
			var value = $(this).val();
			if(value == "y"){
				$("input:checkbox[tgt='"+elemTarget+"']").prop('checked',true);
			}else if(value == "n"){
				$("input:checkbox[tgt='"+elemTarget+"']").attr('checked',false);
			}
		});
	});
	
	$('body').on('click','tr.autofilled button', function(e) {
		$( "tr.autofilled select" ).each(function() {
			var elemTarget = $(this).attr("tgt");
			var value = $(this).val();
			if(value == "y"){
				$("input:checkbox[value='"+elemTarget+"']").prop('checked',true);
			}else{
				$("input:checkbox[value='"+elemTarget+"']").attr('checked',false);
			}
		});
	});
	
	var $dialogNotice = $('<div id="dialog-notice"></div>')
    	.dialog({
    		autoOpen: false,
    		title: 'Notification',
			buttons: { 
				"Ok": function(){ $( this ).dialog( "close" ); }
			}
    	});

	var loadingBar = $('<div class="loader">Loading...</div>');

	$( "#startdate-2m-today" ).datepicker({
		defaultDate: "+1d",
		changeMonth: true, changeYear:true, minDate:0,
		onClose: function( selectedDate ) {
            $( "#enddate-2m-today" ).datepicker( "option", "minDate", selectedDate );

            var endDate = new Date(selectedDate);
            endDate.setDate(endDate.getDate()+60);
            $( "#enddate-2m-today" ).datepicker( "option", "maxDate", endDate );
		}
    });
    $("#enddate-2m-today").datepicker({
      	defaultDate: "+1d",
      	changeMonth: true, changeYear:true, minDate:0, maxDate: "+2m",
     	onClose: function( selectedDate ) {
        	$( "#startdate-2m-today" ).datepicker( "option", "maxDate", selectedDate );
      	}
    });
});
</script>