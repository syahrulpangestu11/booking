 <?php
include('includes/function-xml.php');
$path_xml_hotel = 'data-xml/'.$hcode.'.xml';

if(!file_exists($path_xml_hotel)){ createXML($path_xml_hotel_xml_hotel); }

$xml = new DomDocument();
    $xml->preserveWhitespace = false;
    $xml->load($path_xml_hotel);
$xpath = new DomXpath($xml);

$root = $xml->documentElement;

/*------------------------------------------------------------------------------------*/
$query_tag_hotel = '//hotel[@id="'.$hoteloid.'"]';
$query_tag_room = $query_tag_hotel.'//masterroom[@id="'.$roomoid.'"]';
/*------------------------------------------------------------------------------------*/
$elementName = array("blackout", "cta", "ctd");
/*------------------------------------------------------------------------------------*/

	/* create <hotel id=@>---------------------------------------------------------*/
	$check_tag_hotel = $xpath->query($query_tag_hotel);
    if($check_tag_hotel->length == 0){
	    $hotel = $xml->createElement("hotel");
	        $hotel_id = $xml->createAttribute("id");
	        $hotel_id->value = $hoteloid;
	    $hotel->appendChild($hotel_id);
	    $root->appendChild($hotel);
		$check_tag_hotel = $xpath->query($query_tag_hotel);
    }

	/* create <hotel id=@><masterroom id=@>-----------------------------------------*/
    $check_tag_room = $xpath->query($query_tag_room);
    if($check_tag_room->length == 0){
	    $room = $xml->createElement("masterroom");
	        $room_id = $xml->createAttribute("id");
	        $room_id->value = $roomoid;
	    $room->appendChild($room_id);
	
	    $tag_hotel = $check_tag_hotel->item(0);
	    $tag_hotel->appendChild($room);
		$check_tag_room = $xpath->query($query_tag_room);
    }

foreach($date as $key => $value){
	
	$exp=explode("-",$key);
	$no=$exp[1];
	
	$blackout = (isset($_POST['blackout-'.$no]) and !empty($_POST['blackout-'.$no])) ? $_POST['blackout-'.$no] : "n"; 
	$cta = (isset($_POST['cta-'.$no]) and !empty($_POST['cta-'.$no])) ? $_POST['cta-'.$no] : "n"; 
	$ctd = (isset($_POST['ctd-'.$no]) and !empty($_POST['ctd-'.$no])) ? $_POST['ctd-'.$no] : "n"; 
	
	$elementNodes = array($blackout, $cta, $ctd);

	$dateFormat = explode(" ",date("D Y-m-d",strtotime($value)));
	$day = $dateFormat[0]; 
	$date = $dateFormat[1];
	
	/* create <hotel id=@><masterroom id=@><rate date=@ day=@>----------------------*/
	$query_tag_rate = $query_tag_room.'/rate[@date="'.$date.'"]';
	$check_tag_rate = $xpath->query($query_tag_rate);
 
    if($check_tag_rate->length == 0){
		$rate = $xml->createElement("rate");
	        $rate_date = $xml->createAttribute("date");
	        $rate_date->value = $date;
		$rate->appendChild($rate_date);
	        $rate_day = $xml->createAttribute("day");
	        $rate_day->value = $day;
		$rate->appendChild($rate_day);
			
		$tag_room = $check_tag_room->item(0);
		$tag_room->appendChild($rate);
		$check_tag_rate = $xpath->query($query_tag_rate);
    }

	$query_tag_rateplan = $query_tag_rate.'/rateplan[@id="'.$roomoffer.'"]';
	$check_tag_rateplan = $xpath->query($query_tag_rateplan);
	
	if($check_tag_rateplan->length == 0){
		$rateplan = $xml->createElement("rateplan");
	        $rateplan_id = $xml->createAttribute("id");
	        $rateplan_id->value = $roomoffer;
		$rateplan->appendChild($rateplan_id);
			
		if($check_tag_rateplan->length == 0){
			$tag_rate->appendChild($rateplan);
			$check_tag_rateplan = $xpath->query($query_tag_rateplan);
		}
	}
	
	foreach($channellist as $channeltype){
		$query_tag_rateplan_channel = $query_tag_rateplan.'/channel[@type="'.$channeltype.'"]';
		$check_tag_rateplan_channel = $xpath->query($query_tag_rateplan_channel);
				
		if($check_tag_rateplan_channel->length == 0){
			$rateplan_channel = $xml->createElement("channel");
	        	$rateplan_channel_type = $xml->createAttribute("type");
	        	$rateplan_channel_type->value = $channeltype;
			$rateplan_channel->appendChild($rateplan_channel_type);
			
			$tag_rateplan->appendChild($rateplan_channel);
			$check_tag_rateplan_channel = $xpath->query($query_tag_rateplan_channel);
		}
		
		$tag_rateplan_channel = $check_tag_rateplan_channel->item(0);
			
		foreach($elementName as $key => $value){
			$element = $value;
			$node = $elementNodes[$key];	
				
			$createElement = $xml->createElement($element);
				$createNode = $xml->createTextNode($node);
			$createElement->appendChild($createNode);
				
			$check_tag_element = $xpath->query($query_tag_rateplan_channel.'/'.$element);
				
			if($check_tag_element->length == 0){
				$tag_rateplan_channel->appendChild($createElement);
			}else{
				$tag_element = $check_tag_element->item(0);
				$tag_rateplan_channel->replaceChild($createElement,$tag_element);
			}
		}
	} // for each channel
}

$xml->formatOutput = true;
$xml->save($path_xml_hotel) or die("Error");
?>