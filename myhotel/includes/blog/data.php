<button type="button" class="blue-button add">Create New Blog</button>

<?php
	include("../../conf/connection.php");
	
	$hoteloid = $_REQUEST['hoteloid'];
	$main_query = "SELECT th.*, i.* from termheadline th left join icon i using (iconoid) where termheadlineoid not in (select termheadlineoid from termheadlinehotelremove where hoteloid = '".$hoteloid."') ";
	$filter = array();
	array_push($filter, 'th.hoteloid in ("'.$hoteloid.'", "0")');
	if(isset($_REQUEST['name']) and $_REQUEST['name']!=''){
		array_push($filter, 'th.icon_title like "%'.$_REQUEST['name'].'%"');
	}
	//array_push($filter, 'p.publishedoid not in (3)');
	if(count($filter) > 0){
		$combine_filter = implode(' and ',$filter);
		$query = $main_query.' and '.$combine_filter;
	}else{
		$query = $main_query;
	}
	try {
		$stmt = $db->query($query);
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
?>
<table class="table promo-table">
    <tr>
        <th>Icon</th>
        <th>Title</th>
        <th>Description</th>
        <th></th>
    </tr>
<?php
			$r_headline = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_headline as $termheadline){
			    if($termheadline['iconoid'] == 0){
                    $icon_src = $termheadline['term_icon_src'];
                }else{
                    $icon_src = $termheadline['icon_src'];
                }
?>
    <tr>
        <td class="<?php echo $status; ?>"><?php if(!empty($icon_src)){?><img src="<?=$icon_src?>" class="icon" /><?php }else{echo 'no icon';}?></td>
        <td><?php echo $termheadline['icon_title']; ?></td>
        <td width="50%" style="text-transform:none;"><?php echo $termheadline['description']; ?></td>
        <td class="algn-right">
        <button type="button" class="pencil edit" pid="<?php echo $termheadline['termheadlineoid']; ?>">Edit</button>
        <button type="button" class="trash delete" pid="<?php echo $termheadline['termheadlineoid']; ?>">Delete</button>
        </td>
    </tr>	
<?php				
			}
?>
</table>
<?php
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		print($ex);
		die();
	}
?>
