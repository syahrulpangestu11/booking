<section class="content-header">
    <h1>
       	Headline Icon
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Headline Icon</a></li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="box">
            <div class="box-body">
                <div class="form-group">
				<form method="get" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/blog/">
            		<input type="hidden" name="hoteloid" value="<?php echo $hoteloid; ?>" />
                    <label>Find Blog Title </label> &nbsp;
                    <input type="text" name="name" class="medium" value="<?php echo $_GET['title']; ?>" />&nbsp;&nbsp;                    
                    <button type="submit" class="blue-button">Find</button>
                </form>
                </div>
            </div><!-- /.box-body -->
       </div>
    </div>
    
    <div class="row">
        <div class="box">
        	<form method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/blog">
                <div id="data-box" class="box-body">
                    <div class="loader">Loading...</div>
                </div><!-- /.box-body -->
            </form>
       </div>
    </div>

</section>
