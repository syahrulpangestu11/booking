<?php
include('includes/function-xml.php');
$path_xml_hotel = 'data-xml/'.$hcode.'.xml';

/* #### Untuk cek error ####
function exception_error_handler($errno, $errstr, $errfile, $errline ) {
    throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
}

set_error_handler("exception_error_handler");
*/

try{

/*
* create new file if doesn't exist
*/
if(!file_exists($path_xml_hotel)){ createXML($path_xml_hotel); }


$xml = new DomDocument();
    $xml->preserveWhitespace = false;
    $xml->load($path_xml_hotel);
$xpath = new DomXpath($xml);

$root = $xml->documentElement;



    $query_tag_hotel = '//hotel[@id="'.$hoteloid.'"]';
    $check_tag_hotel = $xpath->query($query_tag_hotel);
    if($check_tag_hotel->length == 0){
	    $hotel = $xml->createElement("hotel");
	        $hotel_id = $xml->createAttribute("id");
	        $hotel_id->value = $hoteloid;
	    $hotel->appendChild($hotel_id);
	    $root->appendChild($hotel);
		$check_tag_hotel = $xpath->query($query_tag_hotel);
    }

    /*
    * <masterroom>
    */
    $query_tag_room = $query_tag_hotel.'//masterroom[@id="'.$roomoid.'"]';
    $check_tag_room = $xpath->query($query_tag_room);
    if($check_tag_room->length == 0){
	    $room = $xml->createElement("masterroom");
	        $room_id = $xml->createAttribute("id");
	        $room_id->value = $roomoid;
	    $room->appendChild($room_id);

	    $tag_hotel = $check_tag_hotel->item(0);
	    $tag_hotel->appendChild($room);
		$check_tag_room = $xpath->query($query_tag_room);
    }

for($i=0;$i<=$diff;$i++){
	$dateFormat = explode(" ",date("D Y-m-d",strtotime($startdate." +".$i." day")));
	$day = $dateFormat[0];
	$date = $dateFormat[1];

	$query_tag_rate = $query_tag_room.'/rate[@date="'.$date.'"]';
	$check_tag_rate = $xpath->query($query_tag_rate);

	if(in_array($day , $selectedday)){

    	if($check_tag_rate->length == 0){
			$rate = $xml->createElement("rate");
				$rate_date = $xml->createAttribute("date");
				$rate_date->value = $date;
			$rate->appendChild($rate_date);
				$rate_day = $xml->createAttribute("day");
				$rate_day->value = $day;
			$rate->appendChild($rate_day);

			$tag_room = $check_tag_room->item(0);
			$tag_room->appendChild($rate);
			$check_tag_rate = $xpath->query($query_tag_rate);
		}

		$tag_rate = $check_tag_rate->item(0);

		/*-----------------------------------------------------------------------------*/

		if($uploadtype=="upload-allotment"){

			$query_tag_allotment = $query_tag_rate.'/allotment';
			$check_tag_allotment = $xpath->query($query_tag_allotment);


			if($check_tag_allotment->length == 0){
				$tag_allotment = $xml->createElement("allotment");
				$tag_rate->appendChild($tag_allotment);

				$check_tag_allotment = $xpath->query($query_tag_allotment);
			}

			$tag_allotment = $check_tag_allotment->item(0);

			foreach($channellist as $channeltype){
				$query_tag_channel = $query_tag_allotment.'/channel[@type="'.$channeltype.'"]';
				$check_tag_channel = $xpath->query($query_tag_channel);

				$channel = $xml->createElement("channel");
					$channel_type = $xml->createAttribute("type");
					$channel_type->value = $channeltype;
					$channel->appendChild($channel_type);
				$channel_allotment = $xml->createTextNode($allotment);
					$channel->appendChild($channel_allotment);

				if($check_tag_channel->length == 0){
					$tag_allotment->appendChild($channel);
				}else{
					$tag_element = $check_tag_channel->item(0);
					$tag_allotment->replaceChild($channel,$tag_element);
				}
			}

      $dynamicrate = false;
      /* DR Change Availabilty ================================================== */
      if((isset($_POST['activerule']) and $_POST['activerule'] == "1") ){//and $hoteloid == "1"
        $newallotment = $allotment;
        $stmt = $db->query("select ro.* from roomoffer ro inner join published p using (publishedoid) inner join room r using (roomoid) inner join hotel h using (hoteloid) inner join offertype using (offertypeoid) where r.roomoid = '".$roomoid."' and p.publishedoid = '1' and h.dynamicrate = '1'");
        $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach($r_room as $row){
          $stmt = $db->prepare("select mr.rate from masterrate mr inner join dynamicraterule dr using (masterrateoid) inner join dynamicrate d using (dynamicrateoid) where d.hoteloid = :a and dr.roomofferoid = :b and (:c <= allocation) and d.publishedoid = '1' order by allocation asc limit 1");
          $stmt->execute(array(':a' => $hoteloid, ':b' => $row['roomofferoid'], ':c' => $newallotment));
          $foundrule = $stmt->rowCount();
          if($foundrule > 0){
            $newrate = $stmt->fetch(PDO::FETCH_ASSOC);

            $query_tag_rate = '//hotel[@id="'.$hoteloid.'"]//masterroom[@id="'.$roomoid.'"]/rate[@date="'.$date.'"]/rateplan[@id="'.$row['roomofferoid'].'"]/channel[@type="1"]/double';

            $check_tag_rate = $xpath->query($query_tag_rate);
            $check_tag_rate->item(0)->nodeValue = round($newrate['rate']);
            $dynamicrate = true;
          }
        }
      }

		}

		/*-----------------------------------------------------------------------------*/

		if($uploadtype=="upload-rate"){

			$query_tag_rateplan = $query_tag_rate.'/rateplan[@id="'.$roomofferoid.'"]';
			$check_tag_rateplan = $xpath->query($query_tag_rateplan);

			if($check_tag_rateplan->length == 0){
				$rateplan = $xml->createElement("rateplan");
					$rateplan_type = $xml->createAttribute("id");
					$rateplan_type->value = $roomofferoid;
				$rateplan->appendChild($rateplan_type);

				$tag_rate->appendChild($rateplan);

				$check_tag_rateplan = $xpath->query($query_tag_rateplan);
			}


			foreach($channellist as $channeltype){

				$query_tag_channel = $query_tag_rateplan.'/channel[@type="'.$channeltype.'"]';
				$check_tag_channel = $xpath->query($query_tag_channel);

				if($check_tag_channel->length == 0){
					$channel = $xml->createElement("channel");
						$channel_type = $xml->createAttribute("type");
						$channel_type->value = $channeltype;
					$channel->appendChild($channel_type);

					$tag_channel = $check_tag_rateplan->item(0);
					$tag_channel->appendChild($channel);

					$check_tag_channel = $xpath->query($query_tag_channel);
					$tag_channel = $check_tag_channel->item(0);

					foreach($elementName as $key => $value){
						$element = $value;
						$node = $elementNodes[$key];

						$createElement = $xml->createElement($element);
						$createNode = $xml->createTextNode($node);
						$createElement->appendChild($createNode);

						$tag_channel->appendChild($createElement);
					}
				}else{
					foreach($elementName as $key => $value){
						$element = $value;
						$node = $elementNodes[$key];

						$createElement = $xml->createElement($element);
						$createNode = $xml->createTextNode($node);
						$createElement->appendChild($createNode);

						$check_tag_element = $xpath->query($query_tag_channel.'/'.$element);
						$tag_channel = $check_tag_channel->item(0);

						if($check_tag_element->length == 0){
							$tag_channel->appendChild($createElement);
						}else{
							$tag_element = $check_tag_element->item(0);
							$tag_channel->replaceChild($createElement,$tag_element);
						}
					}
				}
			} // for each channel
		}// end IF uploadtype
	}
}

$xml->formatOutput = true;
$xml->save($path_xml_hotel) /*or die("Error")*/;

}catch(Exception $ex){
  echo '<div style="display:hidden">'.$ex->getMessage().'</div>';
}
?>
