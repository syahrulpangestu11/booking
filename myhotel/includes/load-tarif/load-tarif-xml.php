<?php
	include('includes/bootstrap.php');
	$start = date("d F Y");
	$end = date("d F Y",  strtotime(date("Y-m-d")." +1 month" ));
	$daylist = array("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat");
?>
<section class="content-header">
    <h1>
        Load Rates
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i>  Allotments &amp; Rates </a></li>
        <li class="active">Load Rates</li>
    </ol>
</section>
<section class="content">
  <form method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/load-tarif/upload">

	<?php
	$stmt = $db->prepare("select dynamicrate, (select count(dynamicrateoid) as jmlrule from dynamicrate where hoteloid = hotel.hoteloid and publishedoid = '1') as jmlrule from hotel where hoteloid = :a");
	$stmt->execute(array(':a' => $hoteloid));
	$activate = $stmt->fetch(PDO::FETCH_ASSOC);
	if($activate['dynamicrate'] == "1" and $activate['jmlrule'] > 0){
	?>
	<div class="box box-danger box-form" style="background-color: #fdd8d8; padding:1px 20px 10px; text-align:center;">
		<div id="data-box" class="box-body">
			<b>PRECAUTION! <span style="color:#0000af;">Dynamic Rule Rate</span> ACTIVATED</b>, all your rates will remain change based on rules applied! Turn off the rules by click toggle button
			<label class="switch" style="margin-left: 7px;  bottom: -8px;">
				<input type="checkbox" name="activerule" value="1" <?php if($activate['dynamicrate'] == "1"){ echo "checked"; } ?>>
				<span class="slider round"></span>
			</label>
			<input type="hidden" name="forcechange" value="1">
		</div>
	</div>

	<div class="modal fade" id="notifyDR" tabindex="-1" role="dialog" aria-labelledby="notifyDR">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel"><i class="fa fa-info-circle"></i> Precaution !! Dynamic Rule Rate ACTIVATED</h4>
				</div>
				<div class="modal-body">
					Dynamic Rule's was ON, your update will force the rule.<br>Are you sure to confirm the change?<br>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel Update</button>
					<button type="button" name="btn-change" class="btn btn-primary">Yes, I confirm</button>
				</div>
			</div>
		</div>
	</div>
	<?php
	}
	?>

    <div class="box box-form">
        <div class="box-body">

			<div class="row">
                <div class="form-group col-md-3">
                    <label>Room Type</label>
                    <?php /*/ ?>
                    <select name="roomoffer" class="form-control">
                    <?php
                        try {
                            $stmt = $db->query("select r.roomoid, r.name, x.offernum from room r inner join hotel h using (hoteloid) left join (select count(roomofferoid) as offernum, ro.roomoid from roomoffer ro group by ro.roomoid) as x on x.roomoid = r.roomoid where h.hoteloid = '".$hoteloid."' and x.offernum > 0 and r.publishedoid = '1'");
                            $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                            foreach($r_room as $row){
                                echo"<optgroup label = '".$row['name']."'>";
                                try {
                                    $stmt = $db->query("select ro.* from roomoffer ro inner join published p using (publishedoid) inner join room r using (roomoid) inner join offertype using (offertypeoid) where r.roomoid = '".$row['roomoid']."' and p.publishedoid = '1'");
                                    $r_offer = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                    foreach($r_offer as $row1){
                                       echo"<option value='".$row['roomoid']."-".$row1['roomofferoid']."' minrate='".floor($row1['minrate'])."'>  ".$row1['name']."</option>";
                                    }
                                }catch(PDOException $ex) {
                                    echo "Invalid Query";
                                    die();
                                }
                                echo"</optgroup>";
                            }
                        }catch(PDOException $ex) {
                            echo "Invalid Query";
                            die();
                        }

                    ?>
                    </select>
                    <?php //*/ ?>


                    <select name="roomoid" class="form-control" id="select-roomoid">
                        <option value='' >- Select Room Type -</option>
                    <?php
                        try {
                            $stmt = $db->query("select r.roomoid, r.name, x.offernum from room r inner join hotel h using (hoteloid) left join (select count(roomofferoid) as offernum, ro.roomoid from roomoffer ro group by ro.roomoid) as x on x.roomoid = r.roomoid where h.hoteloid = '".$hoteloid."' and x.offernum > 0 and r.publishedoid = '1'");
                            $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                            foreach($r_room as $row){
                                       echo"<option value='".$row['roomoid']."' >  ".$row['name']."</option>";
                            }
                        }catch(PDOException $ex) {
                            echo "Invalid Query";
                            die();
                        }

                    ?>
                    </select>

                </div>

                <div class="form-group col-md-3">
                    <label>Rate Plan</label>
                    <select name="roomofferoid" class="form-control" id="select-roomofferoid"> </select>
                    <!--<input name="roomoffer" id="input-roomoffer" class="form-control" type="text">-->
                </div>

                <div class="form-group col-md-6" style="display:none;">
                    <label>Channel</label>
                    <select name="channeloid" class="form-control">
                    <?php
                        try {
                            $stmt = $db->query("select * from channel");
                            $r_plan = $stmt->fetchAll(PDO::FETCH_ASSOC);
                            foreach($r_plan as $row){
                                echo"<option value='".$row['channeloid']."'>".$row['name']."</option>";
                            }
                        }catch(PDOException $ex) {
                            echo "Invalid Query";
                            die();
                        }

                    ?>
                    </select> &nbsp;
                </div>
								<div class="form-group col-md-2" style="display:none">
									<label>Min Stay</label>
									<div class="input-group">
									<input type="number" name="minstay" class="form-control" placeholder="" value="1">
									<div class="input-group-addon">night</div>
									</div>
								</div>
								<div class="form-group col-md-2" style="display:none">
									<label>Max Stay</label>
									<div class="input-group">
									<input type="number" name="maxstay" class="form-control" placeholder="" value="0">
									<div class="input-group-addon">night</div>
									</div>
								</div>
								<div class="form-group col-md-4 punya-roomofferoid">
									<label>Master Rate</label>
									<div class="input-group">
										<select name="master-rate">
											<option value="">- select master rate -</option>
											<?php
											  try {
											      $stmt = $db->prepare("select mr.*, cr.currencycode from masterrate mr inner join currency cr using (currencyoid) inner join roomoffer ro using (roomofferoid) inner join room r using (roomoid) where r.hoteloid = :a");
														$stmt->execute(array(':a' => $hoteloid));
											      $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
											      foreach($r_room as $row){
											          echo"<option x='".$row['roomofferoid']."' value='".$row['currencyoid']."|".$row['rate']."' >  ".$row['bar']." - ".$row['currencycode']." ".number_format($row['rate'])."</option>";
											      }
											  }catch(PDOException $ex) {
											      echo "Invalid Query";
											      die();
											  }
											?>
										</select>
									</div>
								</div>
						 </div>
						</div>

            <div class="row">
            	<div class="form-group col-md-3">
                	<label>Start Date</label>
                    <div class="input-group">
      					<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
						<input type="text" name="startdate" readonly autocomplete="off" class="form-control" id="<?php if($_SESSION['_typeusr']=='1' || $_SESSION['_typeusr']=='2'){echo "startdate";} else{ echo "startdate-2m-today";}?>" placeholder="" value="<?php echo $start; ?>">
					</div>
                </div>
            	<div class="form-group col-md-3">
                	<label>End Date</label>
                    <div class="input-group">
      					<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                        <input type="text" name="enddate" readonly autocomplete="off" class="form-control" id="<?php if($_SESSION['_typeusr']=='1' || $_SESSION['_typeusr']=='2'){echo "enddate";} else{ echo "enddate-2m-today";}?>" placeholder="" value="<?php echo $end; ?>">
					</div>
                </div>
            	<div class="form-group col-md-6 punya-roomofferoid">
                	<label>Applicable Day of Week</label>
                    <div>
                        <?php
                            foreach($daylist as $day){
                                echo"<input type='checkbox' name='day[]' value='".$day."' checked>".$day."&nbsp;&nbsp;";
                            }
                        ?>
                    </div>
                </div>
            </div>

            <div class="row">
            	<div class="form-group col-md-2 punya-roomofferoid">
                	<label>Currency</label>
                    <select name="currency" class="form-control">
                        <?php
                        try {
                            $stmt = $db->query("select * from currency where publishedoid = '1'");
                            $r_published = $stmt->fetchAll(PDO::FETCH_ASSOC);
                            foreach($r_published as $row){
                        ?>
                            <option value="<?php echo $row['currencyoid']; ?>"><?php echo $row['currencycode']; ?></option>
                        <?php
                            }
                        }catch(PDOException $ex) {
                            echo "Invalid Query";
                            die();
                        }
                        ?>
                    </select>
                </div>
            	<div class="form-group col-md-2 punya-roomofferoid">
                	<label>Rate</label>
                    <input type="text" name="double" class="form-control" placeholder="">
                </div>
            	<div class="form-group col-md-2 punya-roomofferoid">
                	<label>Extra Bed</label>
                    <input type="text" name="extrabed" class="form-control" placeholder="">
                </div>
            	<div class="form-group col-md-2 punya-roomoid">
                	<label>Allotment</label>
                    <input type="number" name="allotment" class="form-control" placeholder="" value="99">
                </div>
            	<!--<div class="form-group col-md-2" style="display:none;">
                	<label>Allotment</label>
					<input type="number" name="topup" class="form-control" placeholder="" value="0">
                </div>-->
            	<div class="form-group col-md-2 punya-roomofferoid">
                	<label>Breakfast</label>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="breakfast" value="y"> included ( <i class="fa fa-cutlery"></i> )
                        </label>
                    </div>
                </div>
			</div>

            <div class="row">
                <div class="form-group">
                    <div class="col-sm-6">
                        <input name="uploadtype" id="upload-type" class="form-control" type="hidden">
                        <button type="button" class="btn btn-primary punya-roomoid" id="upload-allotment">Upload Allotment</button>
                        <button type="button" class="btn btn-primary punya-roomofferoid" id="upload-rate">Upload Rate</button>
                    </div>
                    <div class="col-sm-6 text-right">
                        <?php /*?><a href="<?php echo"$base_url"; ?>/import-external"><button type="button" class="btn btn-success">Import from external data</button></a><?php */?>
                    </div>
                </div>
            </div>


       </div>
    </div>

		  </form>
</section>
