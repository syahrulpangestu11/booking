<script type="text/javascript">
$(function(){	
	$('input').iCheck({
		checkboxClass: 'icheckbox_square-blue',
		radioClass: 'iradio_square-blue',
		increaseArea: '20%' // optional  
	});
	
	$('button.submit-edit').click(function(){
		currentbutton = $(this);
		forminput = $('form#data-input');
		$.ajax({
			url: '<?php echo $base_url; ?>/includes/payment-method/rqst-action.php',
			type: 'post',
			data: forminput.serialize() + '&ho=<?=$hoteloid?>',
			success: function(data) {
				if(data != "error"){
					$dialogNotice.html("Payment method has been changed");
					$dialogNotice.dialog("open");
				}else{
					$dialogNotice.html("We&rsquo;re very sorry, we can't update your data right now");
					$dialogNotice.dialog("open");
				}
			}
		});
	});

	$('button.submit-setting').click(function(){
		currentbutton = $(this);
		forminput = $('form#data-setting');
		$.ajax({
			url: '<?php echo $base_url; ?>/includes/payment-method/rqst-setting.php',
			type: 'post',
			data: forminput.serialize() + '&ho=<?=$hoteloid?>',
			success: function(data) {
				if(data != "error"){
					$dialogNotice.html("SOF/IPG setting has been changed");
					$dialogNotice.dialog("open");
				}else{
					$dialogNotice.html("We&rsquo;re very sorry, we can't update your data right now");
					$dialogNotice.dialog("open");
				}
			}
		});
	});
	
	/*
	* DIALOG
	*/

	var $dialogNotice = $('<div id="dialog-notice"></div>')
	.dialog({
		autoOpen: false,
		title: 'Notification',
		buttons: {
			"Ok": function(){
				$( this ).dialog( "close" );
				location.reload();
			}
		}
	});
});
</script>