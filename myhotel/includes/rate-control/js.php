<script type="text/javascript">
$(function(){
	function isNumeric(value){
		var numericExpression = /^(([0-9]*)|(([0-9]*)\.([0-9]*)))$/;
		if(value.match(numericExpression)){
			return true;
		}else{
			return false;
		}
	}

	var $dialogNotice = $('<div id="dialog-notice"></div>')
	.dialog({
		autoOpen: false,
		title: 'Notification',
		buttons: {
			"Ok": function(){
				$( this ).dialog( "close" );
			}
		}
	});

	$(document).ready(function(){ getLoadData(); });

	function getLoadData(){
		$.ajax({
			url: "<?php echo"$base_url"; ?>/includes/rate-control/data.php",
			type: 'GET',
			data: $('form#data-input').serialize(),
			success: function(data) {
				$("#data-box").html(data)
			},
			error : function(data) {
				alert("failed to load data");
			}
		});
	}

	$('body').on('change','#data-box input[type=checkbox]', function(e) {
		if($(this).prop( "checked" ) == true){
			$(this).next('input[type=hidden]').val('y');
		}else{
			$(this).next('input[type=hidden]').val('n');
		}
	});

	$('body').on('click','tr.autofilled button', function() {
		$( "tr.autofilled select" ).each(function() {
			var elemTarget = $(this).attr("tgt");
			var value = $(this).val();
			if(value == "y"){
				$("input:checkbox[name='"+elemTarget+"']").prop('checked',true);
				$("input:checkbox[name='"+elemTarget+"']").next('input[type=hidden]').val('y');
			}else if(value == "n"){
				$("input:checkbox[name='"+elemTarget+"']").attr('checked',false);
				$("input:checkbox[name='"+elemTarget+"']").next('input[type=hidden]').val('n');
			}
		});
		var x = 0;
		$( "tr.autofilled input" ).each(function() {
			var elemTarget = $(this).attr("tgt");
			var value = $(this).val();
			if(value != "" && isNumeric(value)){
				$("input[name='"+elemTarget+"']").val(value);
			}else{
				x++;
			}
		});
		if(x > 0){
			$dialogNotice.html("Some field hasn't been applied. Please remember that rates require number value.");
			$dialogNotice.dialog("open");
		}else{
			$dialogNotice.html("All field has been applied.");
			$dialogNotice.dialog("open");
		}
	});

	var loadingBar = $('<div class="loader">Loading...</div>');

	$('body').on('submit','#form-update', function(){
		var x = 0;
		$( "#form-update tr.list input[type='text']" ).each(function() {
			var value = $(this).val();
			if(value != "" && !isNumeric(value)){
				$(this).css("background-color", "rgb(255, 159, 159)");
    			$(this).css("border", "2px solid #A9A9A9");
				x++;
			}else{
				var minrate = parseInt($("#minrate").val());
				if($(this).attr("name")=="double[]" && value < minrate){
					$(this).css("background-color", "rgb(255, 159, 159)");
					$(this).css("border", "2px solid #A9A9A9");
					x++;
				}
			}
		});

		if(x > 0){
			$dialogNotice.html("Some field may have wrong value. Please remember that all field require number value.");
			$dialogNotice.dialog("open");
			return false;
		}else{
			return true;
		}
	});

	$('body').on('focus',"#form-update tr.list input[type='text']", function(){
		$(this).css("background-color", "rgb(255, 255, 255)");
    	$(this).css("border", "2px inset");
	});

	$('body').on('click', 'input[type="checkbox"][name=activerule]', function (event) {
		if($(this).prop('checked') == true){
			$('#form-update input[name=activerule]').val("1");
		}else{
			$('#form-update input[name=activerule]').val("0");
		}
	});

	$( "#startdate-2m-today" ).datepicker({
		defaultDate: "+1d",
		changeMonth: true, changeYear:true, minDate:0,
		onClose: function( selectedDate ) {
            $( "#enddate-2m-today" ).datepicker( "option", "minDate", selectedDate );

            var endDate = new Date(selectedDate);
            endDate.setDate(endDate.getDate()+60);
            $( "#enddate-2m-today" ).datepicker( "option", "maxDate", endDate );
		}
    });
    $("#enddate-2m-today").datepicker({
      	defaultDate: "+1d",
      	changeMonth: true, changeYear:true, minDate:0, maxDate: "+2m",
     	onClose: function( selectedDate ) {
        	$( "#startdate-2m-today" ).datepicker( "option", "maxDate", selectedDate );
      	}
    });

});
</script>
