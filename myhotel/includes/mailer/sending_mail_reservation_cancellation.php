<?php

    require_once('../includes/mailer/class.phpmailer.php');
    require_once('../includes/mailer/class.hotel-email-template.php');

    ob_start();
    include 'reservation_data_template_cancellation.php';
    
    $template = new HotelEmailTemplate($db,'cancellation','guest',$arrDataTemplate);
    $html_template = $template->getTemplate();
    echo $html_template;
    $body = ob_get_clean();

	$s_data="SELECT customer.firstname, customer.lastname, customer.email,
                #agent.email as agentemail, agent.agentname,
                booking.hoteloid,booking.bookingnumber,hotel.hotelname
                from booking 
                inner join hotel using (hoteloid) 
                #inner join agent using (agentoid) 
                inner join bookingstatus using (bookingstatusoid) 
                inner join currency using (currencyoid) 
                inner join customer using (custoid) 
                inner join country using (countryoid) 
                left join bookingpayment using (bookingoid) 
                where booking.bookingoid = '".$bookingoid."' group by booking.bookingoid";
    $q_data = $db->query($s_data);
    $data = $q_data->fetch(PDO::FETCH_ASSOC);

	// echo "<script>console.log(".json_encode($data).");</script>"; 
	// $agent_mail = $data['agentemail'];
	// $agent_name = $data['agentname'];
	$bookingnumber = $data['bookingnumber'];
	$hoteloid = $data['hoteloid'];
	$hotelname = $data['hotelname'];
	$guestname = $data['firstname']." ".$data['lastname'];
    $guestemail = $data['email'];
    
    $mail = new PHPMailer(true);
    $mail->IsSMTP(); // telling the class to use SMTP/

    // echo $body;
    try{
        $mail->SMTPDebug = 1;
        $sql_mailer = "SELECT * FROM mailercompany INNER JOIN mailertype USING (mailertypeoid) WHERE companyoid='1' and publishedoid='1'";
        $qrun_mailer = $db->query($sql_mailer);
        $run_mailer = $qrun_mailer->fetchAll(PDO::FETCH_ASSOC);
        // echo "<script>console.log(".json_encode($run_mailer).");</script>"; 
        foreach($run_mailer as $row_mailer){
            switch($row_mailer['mailertypeoid']){
                // case 1 : $mail->SetFrom('booking@basmagroups.com', $row_mailer['name']); break;
                case 2 : $mail->AddAddress($row_mailer['email'], $row_mailer['name']); break;
                case 3 : $mail->AddCC($row_mailer['email'], $row_mailer['name']); break;
                case 4 : $mail->AddBCC($row_mailer['email'], $row_mailer['name']); break;
                case 5 : $mail->AddReplyTo($row_mailer['email'], $row_mailer['name']); break;
            }
        }
        $mail->AddAddress($guestemail, $guestname);
        $mail->Subject = "Booking Cancellation for ".$hotelname." ".$bookingnumber;
        $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
        $mail->MsgHTML($body);
        if($mail->Send()){
            echo "success";
        }
    } catch (phpmailerException $e) {
        echo"<b>Php Mailer Error :</b><br>"; echo $e->errorMessage();
        // if($e->errorMessage() == "<strong>SMTP Error: Data not accepted.</strong><br />"){
        //     echo "success";
        // }else{
        //     echo"<b>Php Mailer Error :</b><br>"; echo $e->errorMessage();
        // }
    } catch (Exception $e) {
        echo"<b>All Error :</b><br>"; echo $e->getMessage();
    }

?>