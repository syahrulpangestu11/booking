<div id="email-content" style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">
	<p>You have requested a password reset for the login : <b></b></p>
    <p>To reset your password please click on this link : <a target="_blank" href="<?php echo $base_url."/newpwd/?for=".md5($user.$useroid)."&key=".$key; ?>">Reset Password</a></p>
    <p><b>Please do not reply to this email. Mail sent to this address cannot be answered. If you didn't request this password reset, please contact TheBuking support at : support@thebuking.com .</b></p>
    <br>
    <p>Thanks.</p>
</div>