<?php
class Reporting1{
	var $db;
	var $hoteloid;
	var $startdate;
	var $enddate;


	public function __construct($db){
		$this->db = $db;
	}

	public function setHotel($hoteloid){
		$this->hoteloid = $hoteloid;
	}

	public function setPeriode($startdate, $enddate){
		$this->startdate = $startdate;
		$this->enddate = $enddate;
	}

	public function materializeRoomNight(){
		$query = "select count(bookingroomdtloid) as roomnight from bookingroomdtl inner join bookingroom br using (bookingroomoid) where br.bookingoid in (select bookingoid from (select b.bookingoid, min(checkin) as min_checkin from bookingroom br inner join booking b using (bookingoid) where b.hoteloid = '".$this->hoteloid."' and b.pmsstatus = '0' group by b.bookingoid) as t1 where (min_checkin >= '".$this->startdate."' and min_checkin <= '".$this->enddate."'))";
		$stmt	= $this->db->query($query);
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		return $result['roomnight'];
	}

	public function materializeBooking(){
		$query = "select count(bookingoid) as totalbooking
from (select b.bookingoid, min(checkin) as min_checkin from bookingroom br inner join booking b using (bookingoid) where b.hoteloid = '".$this->hoteloid."' and b.pmsstatus = '0' group by b.bookingoid) as t1
where (min_checkin >= '".$this->startdate."' and min_checkin <= '".$this->enddate."')";
		$stmt	= $this->db->query($query);
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		return $result['totalbooking'];
	}

	public function materializeBookingTotal($field, $bookingstatus){
		if(is_array($bookingstatus)){
			$bookingstatus = implode("','", $bookingstatus);
		}

		$query = "select sum(".$field.") as confirmed
from (select b.".$field.", min(checkin) as min_checkin from bookingroom br inner join booking b using (bookingoid) where b.hoteloid = '".$this->hoteloid."' and b.bookingstatusoid in ('".$bookingstatus."') and b.pmsstatus = '0' group by b.bookingoid) as t1
where (min_checkin >= '".$this->startdate."' and min_checkin <= '".$this->enddate."')";
		$stmt	= $this->db->query($query);
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		return $result['confirmed'];
	}

	public function materializeBookingCommission(){
		$query = "select sum(CASE WHEN grandtotal!=grandtotalr THEN (grandtotal * gbhpercentage / 100) ELSE gbhcollect END) as commission
from (select b.grandtotal, b.grandtotalr, b.gbhpercentage, b.gbhcollect, min(checkin) as min_checkin from bookingroom br inner join booking b using (bookingoid) where b.hoteloid = '".$this->hoteloid."' and b.pmsstatus = '0' group by b.bookingoid) as t1
where (min_checkin >= '".$this->startdate."' and min_checkin <= '".$this->enddate."')";
		$stmt	= $this->db->query($query);
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		return $result['commission'];
	}

	public function listBooking(){
		$query = "select t1.*
from (select b.bookingoid, b.bookingnumber, b.bookingtime, concat(c.firstname, ' ', c.lastname) as guestname, b.grandtotal, (grandtotal * gbhpercentage / 100) as commissionr,
	b.grandtotalr, b.cancellationamount, b.gbhcollect as commission, b.bookingstatusoid, bs.note as status, min(checkin) as min_checkin 
	from booking b inner join bookingstatus bs using (bookingstatusoid) inner join customer c using (custoid) inner join bookingroom br using (bookingoid) where b.hoteloid = '".$this->hoteloid."' and b.pmsstatus = '0' group by b.bookingoid) as t1
where (min_checkin >= '".$this->startdate."' and min_checkin <= '".$this->enddate."')";
		$stmt	= $this->db->query($query);
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	public function listRoomBooking($bookingoid){
		$query = "select DATE_FORMAT(br.checkin,'%d %b %y') as checkin, DATE_FORMAT(br.checkout,'%d %b %y') as checkout, DATE_FORMAT(br.checkoutr,'%d %b %y') as checkoutr, br.room, br.promotion from bookingroom br inner join booking b using (bookingoid) where b.bookingoid = '".$bookingoid."' and b.pmsstatus = '0'";
		$stmt	= $this->db->query($query);
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}
}
?>
