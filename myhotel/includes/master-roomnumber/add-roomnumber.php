<?php
	$hoteloid = $_GET['ho'];
?>
<style>
span.items {
    background-color: #95fffb;
    padding: 5px 10px;
    border-radius: 8px;
    margin-right: 10px;
}
</style>
<section class="content-header">
    <h1>
        Create Room Number
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i>  Settings </a></li>
        <li class="active">Master Room Number</li>
    </ol>
</section>
<section class="content">
	<div class="row">
        <form class="form-box" method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/master-roomnumber/add-process">
        <input type="hidden" name="ho" value="<?=$hoteloid?>">
        <div class="box box-form">
            <h1>Master Room Number</h1>
            <ul class="inline-half">
                <li>
                <ul class="block">
                    <li>
                        <span class="label">Room Type:</span>
                        <select name="rt" class="input-select" id="sroomtype">
                            <?php
                                try {
                                    $stmt = $db->query("select r.roomoid, r.name from room r inner join hotel h using (hoteloid) where h.hoteloid = '".$hoteloid."' and r.publishedoid = '1'");
                                    $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                    foreach($r_room as $row){
                                        echo"<option value='".$row['roomoid']."' ".$selected.">  ".$row['name']."</option>";
                                    }
                                }catch(PDOException $ex) {
                                    echo "Invalid Query";
                                    die();
                                }

                            ?>
                        </select>
                    </li>
                    <li>
                        <span class="label">Room Number:</span>
                        <input type="text" class="long" name="name" required>
                    </li>
                    <!--
                    <li>
                        <span class="label">Existing Room Number:</span>
                        <div style="display:inline-block" id="existing"></div>
                    </li>\
                    -->
                    <li>
                    <img id="preview" src="#" alt="" style="display:none" />
                    </li>
                </ul>
                </li>
            </ul>
            <button class="default-button" type="submit">Save Room Number</button>
        </div>
		</form>
    </div>
</section>
<script>
    $(function(){
        $("#sroomtype").change(function(){
            var x = $(this).val();
            $.ajax({
    			url: "<?php echo"$base_url"; ?>/includes/master-roomnumber/existing-roomnumber.php",
    			type: 'post',
    			data: {'rt':x},
    			success: function(data) {
    				$("#existing").html(data)
    			}
    		});
        });
        //$("#sroomtype").change();
    });
</script>
