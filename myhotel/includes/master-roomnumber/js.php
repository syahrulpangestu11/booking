<script type="text/javascript">
$(function(){
	
	$(document).ready(function(){ 
		
		getLoadData();
	});
	
	$('body').on('click','#paging button', function(e) {
		page	= $(this).attr('page');
		form = $('form#data-input');
		form.find('input[name=page]').val(page);
		form.find('button[type=submit]').click();
	});
	
	$('form#data-input').find('select[name=rt]').change(function(){
	    $('form#data-input').find('input[name=page]').val('');
	});
	
	$('form#data-input').find('select[name="ho"]').change(function(){
	    $('form#data-input').find('select[name=rt]').val('');
	    var x = $(this).val();
	    $('form#data-input').find('select[name=rt]').children('option').each(function(){
	        if($(this).attr('dt') == x || $(this).attr('dt') == ''){
	            $(this).css('display','');
	            if($(this).attr('value') == '<?=$_REQUEST["rt"]?>'){
	                $(this).attr('selected', 'selected');
	                $('form#data-input').find('select[name=rt]').val('<?=$_REQUEST["rt"]?>');
	            }
	        }else{
	            $(this).css('display','none');
	        }
	    });
	});
	

	$('body').on('click','button.edit', function(e) {
		pid = $(this).attr("pid");
		var ho = $(this).attr("ho");
		url = "<?php echo $base_url; ?>/master-roomnumber/edit/?pid="+pid+"&ho="+ho;
      	$(location).attr("href", url);
	});

	$('body').on('click','button.add', function(e) {
		var ho = $(this).attr("ho");
		url = "<?php echo $base_url; ?>/master-roomnumber/add/?ho="+ho;
      	$(location).attr("href", url);
	});
	
   var $dialogDelete = $('<div id="dialog-del"></div>')
	.html('Are you sure you want to delete this data?')
	.dialog({
		autoOpen: false,
		title: 'Confirmation',
		buttons: { 
			"Delete": function(){ 
				var url = $(this).data('url'); 
				var id = $(this).data('id'); 
				$(this).dialog("close"); 
				deleteAction(url, id);
			},
			Cancel: function(){ $( this ).dialog("close"); }
		}
	});
	
	var $dialogNotification = $('<div id="dialog-notice"></div>')
	.dialog({
		autoOpen: false,
		title: 'Notification',
		buttons: { 
			"Ok": function(){ 
				var callback = '<?php echo $base_url; ?>/master-roomnumber'; 
				$(this).dialog( "close" );
				$(location).attr("href", callback); 
			}
		}
	});
	
	function deleteAction(url, id){
		$.ajax({
			url: url,
			type: 'post',
			data: { id : id},
			success: function(data) {
				if(data == "1"){
					$dialogNotification.html("Data has been deleted");
					$dialogNotification.dialog("open");
				}else{
					$dialogNotification.html("We&rsquo;re very sorry, we can't save your data right now");
					$dialogNotification.dialog("open");
				}
			}
		});
	}
	
	$('body').on('click','button.delete', function(e) {
		id = $(this).attr("pid");
		$dialogDelete.data("url", "<?php echo $base_url; ?>/includes/master-roomnumber/delete-roomnumber.php");
		$dialogDelete.data("id", id);
		$dialogDelete.dialog("open");
	});
		
	function getLoadData(){
		$.ajax({
			url: "<?php echo"$base_url"; ?>/includes/master-roomnumber/data.php",
			type: 'post',
			data: $('form#data-input').serialize(),
			success: function(data) {
				$("#data-box").html(data);
	            $('form#data-input').find('select[name="ho"]').change();
			}
		});
	}

   $("#dialog-error, #dialog-success").dialog({
	  autoOpen: false,
	  buttons: { "Ok": redirect },
	  close: function(event, ui) { redirect }
   });
   
   function redirect(){
	   $(location).attr("href", "<?php echo $base_url; ?>/master-roomnumber");
   }
});
</script>

<div id="dialog-error" title="Error Notification">
  <p>We&rsquo;re very sorry, we can't save your data right now.</p>
</div>
<div id="dialog-success" title="Confirmation Notice">
  <p>Data succesfully updated</p>
</div>