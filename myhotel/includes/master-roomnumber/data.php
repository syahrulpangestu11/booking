<?php
	include("../ajax-include-file.php");
	$hoteloid = $_REQUEST['ho'];
	$rt = $_REQUEST['rt'];
	if(intval($rt)>0){
	    $filter_query = "AND `roomoid`='".$rt."'";
	}else{
	    $filter_query = "";
	}
	
	$stmt = $db->prepare("SELECT hoteloid, hotelcode, hotelname FROM hotel WHERE publishedoid='1' AND hoteloid=:a ORDER BY hotelname");
    $stmt->execute(array(':a'=>$hoteloid));
    $dhotel = $stmt->fetch(PDO::FETCH_ASSOC);
    echo '<button type="button" class="pure-button blue add" ho="'.$hoteloid.'"><i class="fa fa-cube"></i>Create New Room Number</button>';
    echo " &nbsp; <b>".$dhotel['hotelname']."</b>";
	
	try {
        include("../../includes/paging/pre-paging.php");
		$stmt = $db->query("SELECT * FROM `room` r INNER JOIN `roomnumber` rn USING(`roomoid`) WHERE `hoteloid`='".$hoteloid."' AND r.publishedoid='1' AND rn.publishedoid='1' ".$filter_query." ORDER BY rn.roomoid, rn.roomnumberoid ".$paging_query);
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
?>
<table class="table promo-table">
	<tr>
		<td>Room Type</td>
		<td>Room Number</td>
		<td class="algn-right">&nbsp;</td>
	</tr>
<?php
		$r_promo = $stmt->fetchAll(PDO::FETCH_ASSOC);
		foreach($r_promo as $row){
?>
    <tr>
        <td><?php echo $row['name']; ?></td>
        <td><?php echo $row['roomnumber']; ?></td>
        <td class="algn-right">
        <button type="button" class="pure-button green single edit" pid="<?php echo $row['roomnumberoid']; ?>" ho="<?php echo $row['hoteloid']; ?>"><i class="fa fa-pencil"></i></button>
        <button type="button" class="pure-button red single delete" pid="<?php echo $row['roomnumberoid']; ?>" ho="<?php echo $row['hoteloid']; ?>"><i class="fa fa-trash"></i></button>
        </td>
    </tr>	
<?php				
		}
?>
</table>
<?php
        $stmt = $db->prepare("SELECT count(`roomnumberoid`) as jmldata FROM `room` r INNER JOIN `roomnumber` rn USING(`roomoid`) WHERE `hoteloid`='".$hoteloid."' AND r.publishedoid='1' AND rn.publishedoid='1' ".$filter_query);
        $stmt->execute();
        $result_jmldata = $stmt->fetch(PDO::FETCH_ASSOC);
        $jmldata = $result_jmldata['jmldata'];
        
        $tampildata = $row_count;
        include("../../includes/paging/post-paging.php");
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}
?>
