<?php
session_start();
error_reporting(0);
date_default_timezone_set("Asia/Makassar");
header("Content-Type: application/xls");
header("Content-Disposition: attachment; filename=AllOccupancyReport_".date("YmdHis").".xls");
header("Pragma: no-cache");
header("Expires: 0");

if(empty($_SESSION['_oid'])){
    die();
}

//------
include("../../../conf/connection.php");
include('../../../includes/reports/function/function-report.php');
include('../../../includes/class/reporting1.php');

$report = new Reporting1($db);

if(isset($_GET['year'])){
	$year = $_GET['year'];
}else{
	$year = date('Y');
}
?>

<table id="commissionReportTable" cellspacing="0" class="table table-striped">
    <thead>
        <tr>
            <th rowspan="2">Hotel</th>
            <th rowspan="2">Chain</th>
        	<?php
                for($m=1; $m<=12; $m++){
			        $periode1 = $year.'-'.$m.'-01'; 
                    echo '<th colspan="2">'.date('M Y', strtotime($periode1)).'</th>';
                }
            ?>
            <th colspan="2">Total</th>
        </tr>
        <tr>
        	<?php
                for($m=1; $m<=12; $m++){
                    echo '
                        <th>Comm</th>
                        <th>RN</th>
                    ';
                }
            ?>
            <th>Comm</th>
            <th>RN</th>
        </tr>
    </thead>
    <tbody>
    <?php
    $add = "";
    if(isset($_GET['chain']) && !empty($_GET['chain'])){
        $add .= " AND chainoid='".intval($_GET['chain'])."'";
    }
    
    $stmt = $db->prepare("SELECT hoteloid, hotelname, chain.name FROM hotel INNER JOIN chain USING(chainoid) WHERE hotel.publishedoid=1 AND hotelstatusoid=1 ".$add." 
        ORDER BY chainoid, hotelname ");
	$stmt->execute(array());
    $r_hotels = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
    $no=$num+1;
    foreach($r_hotels as $hotelx){
        $hoteloid = $hotelx['hoteloid'];
        $hotelname = $hotelx['hotelname'];
        $chainname = $hotelx['name'];
        
        $report->setHotel($hoteloid);
    ?>
        <tr link="">
            <td><?=$hotelname?></td>
            <td><?=$chainname?></td>
    	<?php
    	$stotal_commission = 0;
    	$stotal_roomnight = 0;
        for($m=1; $m<=12; $m++){
			$periode1 = $year.'-'.$m.'-01'; 
			$periode2 = date("Y-m-t",strtotime($periode1));
			
			$report->setPeriode($periode1, $periode2);
			
			//$total_confirmed = $report->materializeBookingTotal('grandtotal', 4);
			//$total_cancelled = $report->materializeBookingTotal('cancellationamount', array(5,7,8));
			//$total_revenue = $total_confirmed + $total_cancelled;
			$total_commission = $report->materializeBookingCommission();
			$total_roomnight = $report->materializeRoomNight();
			
			$stotal_commission += $total_commission;
    	    $stotal_roomnight += $total_roomnight;
		?>
                <td class="text-right"><?=number_format($total_commission/1000)?></td>
                <td class="text-right"><?=number_format($total_roomnight)?></td>
        <?php
		}
		?>
            <td class="text-right"><?=number_format($stotal_commission/1000)?></td>
            <td class="text-right"><?=number_format($stotal_roomnight)?></td>
        </tr>
    <?php
        $no++;
    }
    ?>
    </tbody>
</table>