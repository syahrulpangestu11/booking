<?php
$query_list_hotel = "SELECT h.hoteloid, h.hotelname from hotel h where h.publishedoid not in ('3') and h.hotelstatusoid IN ('1') ORDER BY h.hotelname";
$stmt = $db->prepare($query_list_hotel);
// $stmt->execute(array(':a' => $_POST['hotel']));
$stmt->execute();
$hotel = $stmt->fetchAll(PDO::FETCH_ASSOC);

include('includes/class/reporting2.php');

$report = new Reporting2($db);
$invoice = array();
echo "<pre>";
$sqlInsert = "INSERT INTO `invoice` (
  `invoiceoid`, `hoteloid`, `hotelname`, `month`, `year`, `totalbooking`, `roomnight`, `confirmedbooking`, `penaltycancelled`, `totalrevenue`, `reconcileamount`, `reconciledamount`, `invoiced`, `ppn`, `paid`, `invoicepajakoid`, `nofakturpajak`, `fakturpajak`, `invoicestatusoid`, `emailstatusoid`, `createdby`, `createdatetime`, `npwp`, `pthotel`, `attentionpic`, `contractnumber`, `invoicenumber`) VALUES ";
foreach ($hotel as $h) {
   

	$report->setHotel($h['hoteloid']);

  /*/
  ?>

<div class="row"><div class="col-md-12"><h2><?=$h['hotelname']?></h2></div></div>
<div class="row">
  <div class="col-md-12">
    <table id="commissionReportTable" cellspacing="0" class="table table-striped">
      <thead>
        <tr>
          <th>No.</th>
          <th>Month</th>
          <th>1<sup>st</sup> Stay Periode</th>
          <th>2<sup>nd</sup> Stay Periode</th>
          <th>Total Booking</th>
          <th>Room Night</th>
          <th>Confirmed Booking</th>
          <th>Penalty Cancelled</th>
          <th>Total Revenue</th>
          <th>Total Commission</th>
          <th>Reconcile</th>
          <th>Create Invoice</th>
          <th>Paid Status</th>
        </tr>
      </thead>
      <tbody>
        <?php
        //*/
        $startyear = 2017;
        for($y = $startyear; $y <= date('Y'); $y++){
          $maxMonth = ($y==date('Y')) ? date('n') : 12 ;
          for($m=1; $m<=$maxMonth; $m++){

            $periode1 = $y.'-'.$m.'-01';
            $periode2 = date("Y-m-t",strtotime($periode1));

            $report->setPeriode($periode1, $periode2);

            $total_booking = $report->materializeBookingTotal('grandtotal', array(4,5,7,8));
            $total_rn = $report->materializeRoomNight();
            $total_confirmed = $report->materializeBookingTotal('grandtotal', 4);
            $total_cancelled = $report->materializeBookingTotal('cancellationamount', array(5,7,8));
            $total_revenue = $total_confirmed + $total_cancelled;
            $total_commission = $report->materializeBookingCommission();

            // $invoice[$h['hoteloid']][$y][$m]['hoteloid'] = $h['hoteloid'];
            // $invoice[$h['hoteloid']][$y][$m]['hotelname'] = $h['hotelname'];
            // $invoice[$h['hoteloid']][$y][$m]['month'] = $m;
            // $invoice[$h['hoteloid']][$y][$m]['year'] = $year;
            // $invoice[$h['hoteloid']][$y][$m]['totalbooking'] = $total_booking;
            // $invoice[$h['hoteloid']][$y][$m]['roomnight'] = $report->materializeRoomNight();
            // $invoice[$h['hoteloid']][$y][$m]['confirmedbooking'] = $total_confirmed;
            // $invoice[$h['hoteloid']][$y][$m]['penaltycancelled'] = $total_cancelled;
            // $invoice[$h['hoteloid']][$y][$m]['totalrevenue'] = $total_revenue;
            // $invoice[$h['hoteloid']][$y][$m]['reconcileamount'] = $total_commission;
            $now = date('Y-m-d H:i:s');

            $sqlInsert .= "(NULL, ".$h['hoteloid'].", '".$h['hotelname']."', '".$m."', '".$y."', '".$total_booking."', '".$total_rn."', '".$total_confirmed."', '".$total_cancelled."', '".$total_revenue."', '".$total_commission."', 0, 0, 0, 0, 1, '', '', 1, 1, 'derrint', '".$now."', '', '', '', '', ''),";
            
            

            /*/
            ?>
            <tr>
                <td><?=$m?></td>
                <td><?=date('F', strtotime($periode1))?></td>
                <td><?=date('d M Y', strtotime($periode1))?></td>
                <td><?=date('d M Y', strtotime($periode2))?></td>
                <td class="text-right"><?=$report->materializeBooking()?></td>
                <td class="text-right"><?=$report->materializeRoomNight()?></td>
                <td class="text-right"><?=$total_confirmed?></td>
                <td class="text-right"><?=$total_cancelled?></td>
                <td class="text-right"><?=$total_revenue?></td>
                <td class="text-right"><?=$total_commission?></td>
                <td class="text-center"><a>Reconcile</a></td>
                <td><button type="button" class="btn btn-xs btn-warning">Create Invoice</button></td>
                <td class="text-center"><a>Open</a></td>
            </tr>
            <?php
            //*/
          }
        }
        /*/
        ?>
      </tbody>
    </table>
  </div>
</div>

<?php
//*/
  }
  // echo '<pre>';
  echo $sqlInsert;
  // print_r($invoice);
  echo '</pre>';
?>