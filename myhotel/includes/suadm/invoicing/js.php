<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
<script src="<?=$base_url;?>/js/jquery-lazzynumeric-master/js/autoNumeric.js"></script>
<script src="<?=$base_url;?>/js/jquery-lazzynumeric-master/js/jquery.lazzynumeric.js"></script>

<script type="text/javascript">
$(function(){
  $(".numeric").lazzynumeric();

	// halaman list hotel -----------------------------------------------------------------

	$('#selectHotel').change(function(){
    var thisElem = $(this);
    var thisVal = thisElem.val();
    if(thisVal=='all'){
      $('#selectMonth, #selectSubscribe').show();
    }else{
      $('#selectMonth, #selectSubscribe').hide();
    }
  });

	// halaman invoicing action ---------------------------------------------------------------

	$('#selectHCO').change(function(){
    var thisElem = $(this);
    if(thisElem.val()==''){
      $('#selectMTO, #pushAttn').hide();
      $('#customName, #customEmail').hide();
    }else if(thisElem.val()=='custom'){
      $('#customName, #customEmail').show();
      $('#selectMTO, #pushAttn').show();
    }else{
      $('#customName, #customEmail').hide();
      $('#selectMTO, #pushAttn').show();
    }
  });

  currentAttns = [];

  $('#pushAttn').click(function(){
    var thisElem = $(this);
    var elemHCO = thisElem.parent().children('#selectHCO');
    var elemMTO = thisElem.parent().children('#selectMTO');
    var elemAttns = thisElem.parent().children('#attns');
    var elemViewAttns = thisElem.parent().children('#viewAttns');
    var selectedMTO = elemMTO.val();
    var selectedMT = $('option:selected', elemMTO).attr('mt');
    if(elemHCO.val()=='custom'){
      var selectedEmail = thisElem.parent().children('#customEmail').val();
      var selectedName = thisElem.parent().children('#customName').val();
    }else{
      var selectedEmail = $('option:selected', elemHCO).attr('email');
      var selectedName = $('option:selected', elemHCO).attr('name');
    }
    
    if(selectedMTO==2){
      var attentionpic = $('#attentionpic');
      var attentionpicVal = attentionpic.val();
      if(attentionpicVal.length){
        attentionpic.val(attentionpicVal + ", " + selectedName);
      }else{
        attentionpic.val(selectedName);
      }
    }
    
    newAttn = '<li>'+selectedMT+': <b>'+selectedName+'</b> ('+selectedEmail+')</li>';
    elemViewAttns.append(newAttn);
    elemViewAttns.show();

    var attns = { };
    attns.email = selectedEmail;
    attns.name = selectedName;
    attns.mailertype = selectedMTO;
    currentAttns.push(attns);
    console.log(currentAttns);

    var attnsJSON = JSON.stringify(currentAttns);
    elemAttns.val(attnsJSON);
    elemAttns.change();
  });

  $('#attns').on('change paste keyup', function() {
    $("#labelViewAttns, #resetAttns").hide();
    if(this.value.length){
      $("#labelViewAttns, #resetAttns").show();
    }
  });

  $('#resetAttns').click(function(){
    $('#viewAttns').html('');
    $('#viewAttns').hide();
    $('#attns, #attentionpic').val('');
    $('#attns').change();
  });

  $('.invoiced-trigger').each(function(){
    $(this).on('change paste keyup', function(){
      var reconciledamount_temp = $('#reconciledamount_temp').val();
      var ppn = $('#ppn').val();
      var invoicedValue = parseInt(reconciledamount_temp) + (parseInt(ppn) / 100 * parseInt(reconciledamount_temp)); 
      $('#invoiced').val(invoicedValue);
      $('#invoiced').blur();
    });
  });

  $('#generatePDF').click(function(){
    var thisElem = $(this);
		var io = thisElem.attr('io');
    var pdfParams = '?io='+io;
    $('.pdf-params').each(function(){
      var pdfParam = $(this).attr('name');
      var pdfParamVal = encodeURI($(this).val());
      pdfParams += '&'+pdfParam+'='+pdfParamVal;
    });
    thisElem.attr('href', '<?=$base_url;?>/request/export/export-proforma-invoice.php'+pdfParams);

  });

  $(document).ready(function(){
    $('#selectHCO').change();
    $("#labelViewAttns, #viewAttns, #resetAttns").hide();
    $('#reconciledamount_temp').change();
  });

  $('#mailContent').summernote();




  // var $dialogNotification = $('<div id="dialog-notice"></div>')
	// .dialog({
	// 	autoOpen: false,
	// 	title: 'Notification',
	// 	buttons: { 
	// 		"Ok": function(){ 
	// 			var callback = '<?php echo $base_url; ?>/promotions'; 
	// 			$(this).dialog( "close" );
	// 			$(location).attr("href", callback); 
	// 		}
	// 	}
	// });

});
</script>








<script>
$(function(){
  

});
</script>