<?php
	$imagename = $uploadedname . $ext; // (1000 x 667)
	$imagename_t = $uploadedname . "-t" . $ext; // (160 x 107)

	function saveImage($tn, $savePath, $imageQuality="100") {
		// *** Get extension
		$extension = strrchr($savePath, '.');
		$extension = strtolower($extension);

		switch($extension) {
			case '.jpg':
			case '.jpeg':
				if (imagetypes() & IMG_JPG) {
					imagejpeg($tn, $savePath, $imageQuality);
				}
				break;
			case '.gif':
				if (imagetypes() & IMG_GIF) {
					imagegif($tn, $savePath);
				}
				break;
			case '.png':
				// *** Scale quality from 0-100 to 0-9
				$scaleQuality = round(($imageQuality/100) * 9);

				// *** Invert quality setting as 0 is best, not 9
				$invertScaleQuality = 9 - $scaleQuality;
				if (imagetypes() & IMG_PNG) {
						imagepng($tn, $savePath, $invertScaleQuality);
				}
				break;
			// ... etc
			default:
				// *** No extension - No save.
				break;
		}

		imagedestroy($tn);
	}

	if($filename!=''){

			//-------- mengupload gambar ke server
			$source = $_FILES['photos']['tmp_name'];
			$folder = "myhotel/images/destination/state/";
			$target = $folder.$imagename;
			move_uploaded_file($source, $upload_base . $target);

			// $file = $target; //This is the original file

			//-------- menghapus gambar lama
			if($oldpict!=""){
				$oldfile= str_replace($web_url.'/'.$folder,'',$oldpict);
				if(file_exists('images/'.$oldfile)){ unlink('images/'.$oldfile); }
			}

			//-------- menurunkan resolusi gambar
			// list($width, $height) = getimagesize($upload_base . $file) ;
			//
			// $save = $folder . $imagename_t; //This is the new file you saving
			// if($width < 500 or $height < 500){
			// 	$modwidth = $width * 50 /100;
			// 	$modheight = $height * 50 /100;
			// }else{
			// 	$modwidth = $width * 30 /100;
			// 	$modheight = $height * 30 /100;
			// }
			// $tn = imagecreatetruecolor($modwidth, $modheight) ;
			// switch($filetype) {
			// 	case "image/gif":	$image = imagecreatefromgif($upload_base . $file); break;
			// 	case "image/jpeg": $image = imagecreatefromjpeg($upload_base . $file); break;
			// 	case "image/png": $image = imagecreatefrompng($upload_base . $file); break;
			// }
			//
			// //prevent tranparant background turn into black
		  // if($filetype == "image/gif" or $filetype == "image/png"){
		  //   imagecolortransparent($tn, imagecolorallocatealpha($tn, 0, 0, 0, 127));
		  //   imagealphablending($tn, false);
		  //   imagesavealpha($tn, true);
		  // }
			//
			//
			// if(file_exists('images/'.$imagename)){ unlink('images/'.$imagename); } //delete original file
			// imagecopyresampled($tn, $image, 0, 0, 0, 0, $modwidth, $modheight, $width, $height) ;
			// saveImage($tn, $upload_base . $save, 75);
	}

?>
