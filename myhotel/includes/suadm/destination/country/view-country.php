<section class="content-header">
    <h1>Destination : Country</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Destination</a></li>
        <li class="active">Country</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="box">
            <div class="box-body">
                <div class="form-group">
				<form method="get" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/country/">
                    <label>Country Name  :</label> &nbsp;&nbsp;
                    <input type="text" name="name" value="<?=$_GET['name']?>"> 
                    <button type="submit" class="blue-button">Find</button>
                </form>
                </div>
            </div><!-- /.box-body -->
       </div>
    </div>
    <div class="row">
        <div class="box">
        	<form method="post" enctype="multipart/form-data" id="form-add">
                <div id="data-box" class="box-body">
                    <div class="loader">Loading...</div>
                </div><!-- /.box-body -->
            </form>
       </div>
    </div>
</section>