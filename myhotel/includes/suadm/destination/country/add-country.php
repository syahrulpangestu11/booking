<?php
	include("../../../../conf/connection.php");
	$country = $_POST['country'];
	$continent = $_POST['continent'];
	
	if(!empty($continent) and !empty($country)){
		try {
			$stmt = $db->prepare("INSERT INTO country (countryname, continentoid) VALUES (:a, :b)");
			$stmt->execute(array(':a' => $country, ':b' => $continent));
			echo "1";
		}catch(PDOException $ex) {
			echo "0 $ex";
			die();
		}
	}else{
		echo "0";
		die();
	}
?>