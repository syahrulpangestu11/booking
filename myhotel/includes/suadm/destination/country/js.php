<script type="text/javascript">
$(function(){
	$(document).ready(function(){ getLoadData(); });
	
	function getLoadData(){
		$.ajax({
			url: "<?php echo"$base_url"; ?>/includes/suadm/destination/country/data.php",
			type: 'post',
			data: $('form#data-input').serialize(),
			success: function(data) {
				$("#data-box").html(data)
			}
		});
	}
	
	$('body').on('click','button.add', function(e) {
		url = '<?php echo $base_url; ?>/includes/suadm/destination/country/add-country.php';
		forminput = $('form#form-add');
		submitData(url, forminput);
	});

	function submitData(url, forminput){
		$.ajax({
			url: url,
			type: 'post',
			data: forminput.serialize(),
			success: function(data) {
				if(data == "1"){
					$dialogNotice.html("Data succesfully updated");
					$dialogNotice.dialog("open");
				}else{
					$dialogNotice.html("We&rsquo;re very sorry, we can't save your data right now");
					$dialogNotice.dialog("open");
				}
			}
		});
	}

	var $dialogNotice = $('<div id="dialog-notice"></div>')
    	.dialog({
    		autoOpen: false,
    		title: 'Notification',
			buttons: { 
				"Ok": function(){ 
					$( this ).dialog( "close" ); 
					$(location).attr("href", "<?php echo $base_url; ?>/country"); 
				}
			}
    	});

	var loadingBar = $('<div class="loader">Loading...</div>');
});
</script>