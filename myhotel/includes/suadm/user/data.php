<button type="button" class="blue-button add-button">Create New User Role</button><br /><br />
<table class="table">
	<thead>
		<tr>
			<th>No.</th>
			<th>Name</th>
			<th>Username</th>
			<th>Role</th>
			<th>Manage</th>
			<th>&nbsp</th>
		</tr>
	</thead>
	<tbody>
<?php
try{
	error_reporting(E_ALL & ~E_NOTICE);
	include("../../../conf/connection.php");
	include("../../paging/pre-paging.php");

	$hotelname = $_REQUEST['hotelname'];
	$username = $_REQUEST['username'];
	$role = $_REQUEST['role'];

	// $main_query = "select u.*, ut.usertype, up.userpmstype FROM users u left join userstype ut using (userstypeoid) left join userpmstype up using (userpmstypeoid)";
	$main_query = "select u.*, ut.usertype FROM users u left join userstype ut using (userstypeoid) ";
		$additional_join == array();
		$filter = array();
		if(!empty($_REQUEST['username'])){
			array_push($filter, "u.username like '%".$_REQUEST['username']."%'");
		}
		if(!empty($_REQUEST['hotelname'])){
			$additional_join = " left join userassign ua on ua.useroid = u.useroid and ua.type = 'hoteloid' left join hotel h on h.hoteloid = ua.oid ";
			array_push($filter, "h.hotelname like '%".$_REQUEST['hotelname']."%'");
		}
		if(count($filter) > 0){
			$filter_query = ' where '.implode(" and ", $filter);
		}else{
			$filter_query = "";
		}
	$query = $main_query.$additional_join.$filter_query.' group by u.useroid order by u.useroid desc '.$paging_query;
	$stmt = $db->query($query);
	$result_user = $stmt->fetchAll(PDO::FETCH_ASSOC);
	foreach($result_user as $user){
		$role = array();
		if($user['ibeuser'] == "y"){ array_push($role, '[IBE] '.$user['usertype']); }
		// if($user['pmsuser'] == "y"){ array_push($role, '[PMS] '.$user['userpmstype']); }

		$stmt = $db->prepare("select case type when 'hoteloid' then '[Property]' when 'chainoid' then '[Chain]' end as type, case type when 'hoteloid' then (select hotelname from hotel where hoteloid = oid and hotel.publishedoid = '1') when 'chainoid' then (select name from chain where chainoid = oid and chain.publishedoid = '1') end as property from userassign where useroid = :id  and type != 'cityoid' order by type");
		$stmt->execute(array(':id' => $user['useroid']));
		$result_user_assigned = $stmt->fetchAll(PDO::FETCH_ASSOC);
?>
	<tr>
		<td><?=$no++?></td>
		<td><?=$user['displayname']?></td>
		<td><b><?=$user['username']?></b></td>
		<td><?=implode('<br>', $role)?></td>
		<td><?php foreach($result_user_assigned as $assigned){ echo $assigned['type']." ".$assigned['property']."<br>"; } ?></td>
		<td><button type='button' class='btn btn-primary edit-button' uo='<?=$user['username']?>'>Edit</button></td>
	</tr>
<?php
	}
}catch(PDOException $ex) {
	print($ex);
	die();
}
?>
	</tbody>
</table>
<?php
	// $main_count_query = "select count(distinct(u.useroid)) as jml FROM users u left join userstype ut using (userstypeoid) left join userpmstype up using (userpmstypeoid) ";
	$main_count_query = "select count(distinct(u.useroid)) as jml FROM users u left join userstype ut using (userstypeoid) ";
	$stmt = $db->query($main_count_query.$additional_join.$filter_query);
	$jmldata = $stmt->fetchColumn();

	$tampildata = $row_count;
	include("../../paging/post-paging.php");
?>
