<?php
	$name = $_POST['displayname'];
	$email = $_POST['email'];
	$mobile = $_POST['mobile'];
	$reservationemail = $_POST['rsvemail'];
	if(isset($_POST['opencc'])){ $open_cc = $_POST['opencc']; }else{ $open_cc = "n"; }
	if(isset($_POST['billing'])){ $billing = $_POST['billing']; }else{ $billing = "n"; }
	$username = $_POST['username'];
	if(!empty($_POST['password'])){ $password = sha1($_POST['password']); }else{ $password = ""; }
	$role = $_POST['role'];
	$status = $_POST['status'];

	if(isset($_POST['ibeuser'])){ $ibeuser = 'y'; }else{ $ibeuser = "n"; }
	// if(isset($_POST['pmsuser'])){ $pmsuser = 'y'; }else{ $pmsuser = "n"; }

	try{
		// $stmt = $db->prepare("insert into users (displayname, email, mobile, username, password, status, createdby, created, updatedby, updated, ibeuser, pmsuser) values (:a, :b, :c, :d, :e, :f, :g, :h, :i, :j, :k, :l)");
		// $stmt->execute(array(':a' => $name, ':b' => $email, ':c' => $mobile, ':d' => $username, ':e' => $password, ':f' => $status, ':g' => $_SESSION['_user'], ':h' => date('Y-m-d H:i:s'), ':i' => $_SESSION['_user'], ':j' => date('Y-m-d H:i:s'), ':k' => $ibeuser, ':l' => $pmsuser));
		$stmt = $db->prepare("insert into users (displayname, email, mobile, username, password, status, createdby, created, updatedby, updated, ibeuser) values (:a, :b, :c, :d, :e, :f, :g, :h, :i, :j, :k)");
		$stmt->execute(array(':a' => $name, ':b' => $email, ':c' => $mobile, ':d' => $username, ':e' => $password, ':f' => $status, ':g' => $_SESSION['_user'], ':h' => date('Y-m-d H:i:s'), ':i' => $_SESSION['_user'], ':j' => date('Y-m-d H:i:s'), ':k' => $ibeuser));
		$useroid = $db->lastInsertId();

		if($ibeuser == 'y'){
			$stmt = $db->prepare("update users set userstypeoid = :a where useroid = :id");
			$stmt->execute(array(':a' =>$_POST['role'], ':id' => $useroid));
		}

		// if($pmsuser == 'y'){
		// 	$stmt = $db->prepare("update users set userpmstypeoid = :a where useroid = :id");
		// 	$stmt->execute(array(':a' =>$_POST['role_pms'], ':id' => $useroid));
		// }

		if(isset($_POST['city']) and count($_POST['city']) > 0){
			foreach($_POST['city'] as $key => $cityoid){
				$stmt = $db->prepare("insert into userassign (useroid, type, oid) values (:a, :b, :c)");
				$stmt->execute(array(':a' => $useroid, ':b' => 'cityoid', ':c' => $cityoid));
			}
		}

		if(isset($_POST['chain']) and count($_POST['chain']) > 0){
			foreach($_POST['chain'] as $key => $chainoid){
				$stmt = $db->prepare("insert into userassign (useroid, type, oid) values (:a, :b, :c)");
				$stmt->execute(array(':a' => $useroid, ':b' => 'chainoid', ':c' => $chainoid));
			}
		}

		if(isset($_POST['hotel']) and count($_POST['hotel']) > 0){
			foreach($_POST['hotel'] as $key => $hoteloid){
				$stmt = $db->prepare("insert into userassign (useroid, type, oid) values (:a, :b, :c)");
				$stmt->execute(array(':a' => $useroid, ':b' => 'hoteloid', ':c' => $hoteloid));
			}
		}
	?>
		<script type="text/javascript">
        $(function(){ $( document ).ready(function(){ $("#dialog-success").dialog("open"); }); });
    </script>
	<?php
	}catch(PDOException $ex) {
		print_r($ex->getMessage());
	?>
			<script type="text/javascript">
        $(function(){ $( document ).ready(function(){ $("#dialog-error").dialog("open"); }); });
    	</script>
	<?php
	}
?>
