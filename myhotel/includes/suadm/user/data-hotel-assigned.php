
  <?php
  error_reporting(E_ALL ^ E_NOTICE);
  require_once("../../../conf/connection.php");
  include("../../paging/pre-paging.php");
  ?>
  <div class="row">
    <div class="col-md-12">
      <?php
        $main_query = "select h.hotelname, h.hotelcode, h.hoteloid, ch.name as chainname from hotel h left join chain ch using (chainoid) where h.publishedoid = '1'";

        $filter = array();
        if(!empty($_POST['hotelname'])){
          array_push($filter, 'h.hotelname like "%'.$_POST['hotelname'].'%"');
        }
        if(!empty($_POST['chain'])){
          array_push($filter, 'h.chainoid = "'.$_POST['chain'].'"');
        }
        if(count($filter) > 0){ $filter_query = ' and '.implode(" and ", $filter); }else{ $filter_query = "";  }
        $stmt = $db->query($main_query.$filter_query.' order by h.hoteloid desc '.$paging_query);
      ?>
      <table class="table table-bordered">
        <thead><tr><th class="text-center">Apply</th><th class="text-center">Hotel</th><th class="text-center">Chain</th></tr></thead>
        <tbody>
          <tr style="display:none;"><td></td></tr>
        <?php
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach($result as $hotel){
        ?>
          <tr>
            <td class="text-center"><input type="checkbox" name="choicehotel" value="<?=$hotel['hoteloid']?>" data-name="<?=$hotel['hotelname']?>"  data-chain="<?=$hotel['chainname']?>"></td>
            <td><?=$hotel['hotelname']?></td>
            <td><?=$hotel['chainname']?></td>
          </tr>
        <?php
        }
        if(empty($result)){ echo "<tr><td colspan='3'><i class='fa fa-info-circle'></i> No data found</td></tr>"; }
        ?>
        </tbody>
      </table>
      <?php
      $main_count_query = "select count(hoteloid) as jml from hotel h left join chain ch using (chainoid) where h.publishedoid = '1'";
      $stmt = $db->query($main_count_query.$filter_query);
      $jmldata = $stmt->fetchColumn();

      $tampildata = $row_count;
      include("../../paging/post-paging.php");
      ?>
    </div>
  </div>
