<?php
	$hotelname = $_REQUEST['hotelname'];
	$username = $_REQUEST['username'];
	$role = $_REQUEST['role'];

	if($_SESSION['_typeusr'] != "1"){
		$query_user_type = " where userstypeoid not in ('1')";
	}
?>
<section class="content-header">
    <h1>
       	 User
    </h1>
    <ol class="breadcrumb">
        <li class="active"><a href="#"><i class="fa fa-table"></i> List User</a></li>
    </ol>
</section>
<section class="content">

    <div class="row">
        <div class="box box-form">
            <div class="box-body">
                <div class="form-group">
                    <form method="get" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/user/">
					<input type="hidden" name="page">
                    <label>Hotel Name  :</label> &nbsp;&nbsp;
					<input type="text" name="hotelname" class="input-text" value="<?php echo $hotelname; ?>">
                    &nbsp;&nbsp;
                    <label>Username  :</label> &nbsp;&nbsp;
					<input type="text" name="username" class="input-text" value="<?php echo $username; ?>">
                    <label>Role  :</label> &nbsp;&nbsp;
                    <select name="role" class="input-select">
                    	<option value="">show all</option>
                    <?php
                    try {
                        $stmt = $db->query("select * from userstype".$query_user_type);

                        $r_usertype = $stmt->fetchAll(PDO::FETCH_ASSOC);
                        foreach($r_usertype as $row){
							if($row['userstypeoid'] == $role){ $selected = "selected"; }else{ $selected=""; }
                    ?>
                        <option value="<?php echo $row['userstypeoid']; ?>" <?php echo $selected; ?>><?php echo $row['usertype']; ?></option>
                    <?php
                        }
                    }catch(PDOException $ex) {
                        echo "Invalid Query".$ex;
                        die();
                    }
                    ?>
                    </select>
                     &nbsp;&nbsp;
                    <button type="submit" class="blue-button">Find</button>
                </form>
                </div>
            </div><!-- /.box-body -->
       </div>
    </div>

    <div class="row">
        <div class="box">
            <div id="data-box" class="box-body">
                <div class="loader">Loading...</div>
            </div><!-- /.box-body -->
       </div>
    </div>
</section>
