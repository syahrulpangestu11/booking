<?php
	$extraoid = $_GET['pid'];
	try {
		$stmt = $db->query("select e.* from extra_template e where extra_templateoid = '".$extraoid."'");
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
			$r_promo = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_promo as $row){
				$name = $row['name'];
				$salefrom = date("d F Y", strtotime($row['startbook']));
				$saleto = date("d F Y", strtotime($row['endbook']));
				$published = $row['publishedoid'];
				$headline = $row['headline'];
				$description = $row['description'];
				$picture = $row['picture'];
			}
		}else{
			echo "No Result";
			die();
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}
?>

<script type="text/javascript">
$(function() {
	$('textarea').trumbowyg({
		btns: ['viewHTML',
		  '|', 'btnGrp-design',
		  '|', 'link',
		  '|', 'btnGrp-justify',
		  '|', 'btnGrp-lists'],
		fullscreenable: false
	});
});
</script>
<section class="content-header">
    <h1>
        Edit Extra
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i>  Allotments &amp; Rates </a></li>
        <li class="active">Extra</li>
    </ol>
</section>
<section class="content">
	<div class="row">
        <form class="form-box" method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/extra-template/edit-process">
        <input type="hidden" name="extraoid" value="<?=$extraoid;?>">
        <div class="box box-form">
        <ul class="inline-half">
            <li>
            <h1>Extra</h1>
            <ul class="block">
                <li>
                    <span class="label">Extra Name:</span>
                    <input type="text" name="name" value="<?=$name;?>" style="width:100%">
                </li>
                <li>
                    <span class="label">Picture</span>
                    <input type="file" class="medium" name="image">
                </li>
                <li>
                <?php if(empty($picture)){ ?>
                <img id="preview" src="#" alt="" style="display:none" />
                <?php }else{ ?>
                <img id="preview" src="<?=$picture?>" alt=""/>
                <?php } ?>
                </li>
            </ul>
            </li>
            <li>
            <div style="text-align:right;"><button class="default-button" type="submit">Save Extra</button></div>
            <ul class="block">
                <li><span class="label"><b>Sale Date</b></span></li>
                <li>
                    <span class="label">Sale Date From:</span>
                    <input type="text"class="medium" id="startdate" name="salefrom" value="<?=$salefrom;?>">
                </li>
                <li>
                    <span class="label">Sale Date To:</span>
                    <input type="text"class="medium" id="enddate"; name="saleto" value="<?=$saleto;?>">
                </li>
                <li>
                    <span class="label">Publish Extra :</span>
                    <select name="published">
                    <?php
                        try {
                            $stmt = $db->query("select * from published where showthis = 'y'");
                            $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                            foreach($r_room as $row){
								if($row['publishedoid'] == $published){ $selected = "selected"; }else{ $selected=""; }
								echo"<option value='".$row['publishedoid']."' ".$selected.">".$row['note']."</option>";
                            }
                        }catch(PDOException $ex) {
                            echo "Invalid Query";
                            die();
                        }
                    ?>
                    </select>
                </li>
            </ul>
            </li>
        </ul>
        </div>
        <div class="box box-form">
			<h2>HEADLINE &amp; DETAIL</h2>
            <ul class="inline-half">
                <li>
                    <h3>Headline</h3>
                    <textarea name="headline"><?=$headline;?></textarea>
                </li>
				<li>
                	<h3>Description</h3>
                   	<textarea name="description"><?=$description;?></textarea>
				</li>
			</ul>
            <div class="clear"></div>
            <div class="clear"></div>
            <button class="default-button" type="submit">Save Extra</button>
   		</div>
		</form>
    </div>
</section>
