 <?php
	if(empty($_REQUEST['rt']) and empty($_REQUEST['sdate']) and empty($_REQUEST['edate'])){
		$start = date("d F Y");
		$end = date("d F Y",  strtotime(date("Y-m-d")." +1 month" ));
	}else{
		$start = date("d F Y", strtotime($_REQUEST['sdate']));
		$end = date("d F Y", strtotime($_REQUEST['edate']));
		$roomoffer = $_REQUEST['rt'];
		$channeloid = $_REQUEST['ch'];
	}

	include("js.php");
?>
<section class="content-header">
    <h1>
       	Extra Template
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Hotel</a></li>
        <li class="active">Extra Template</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="box">
            <div class="box-body">
                <div class="form-group">
                <form method="get" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/extra-template/">
                    <label>Extra Template Name </label> &nbsp;
                    <input type="text" name="name" class="medium" value="<?php echo $_GET['name']; ?>" />&nbsp;&nbsp;
                    <button type="submit" class="blue-button">Find</button>
                </form>
                </div>
            </div><!-- /.box-body -->
       </div>
    </div>

    <div class="row">
        <div class="box">
        	<form method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/bank-extra">
                <div id="data-box" class="box-body">
                    <div class="loader">Loading...</div>
                </div><!-- /.box-body -->
            </form>
       </div>
    </div>

</section>
