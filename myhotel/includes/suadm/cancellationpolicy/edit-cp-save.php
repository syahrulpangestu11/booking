<?php
	$cpoid = $_POST['cpoid'];
	$hoteloid = $_POST['hoteloid'];
	$name = (isset($_POST['name'])) ? $_POST['name'] : "";
	$description = (isset($_POST['description'])) ? $_POST['description'] : "";
	$published = (isset($_POST['published'])) ? $_POST['published'] : "";
	$termcondition = $_POST['termcondition'];	
	if($termcondition == "non refundable"){
		$cancellationday = 0;
		$typecancellation = "full amount";
		$night = 0;
	}else{
		$cancellationday = $_POST['cancellationday'];
		$typecancellation = $_POST['typecancellation'];
		if($typecancellation == "night"){
			$night = $_POST['night'];
		}else{
			$night = 0;
		}
	}
		
	try {
		$stmt = $db->prepare("update cancellationpolicy set name = :b, description = :c, publishedoid = :d, termcondition = :e, cancellationday = :f, cancellationtype = :g, night = :h  where hoteloid = :a and cancellationpolicyoid = :id");
		$stmt->execute(array(':a' => $hoteloid, ':b' => $name, ':c' => $description, ':d' => $published, ':e' => $termcondition, ':f' => $cancellationday, ':g' => $typecancellation, ':h' => $night, ':id' => $cpoid));
	}catch(PDOException $ex) {
		echo "Invalid Query";
		print($ex);
		die();
	}
	
	header("Location: ". $base_url ."/cancellationpolicy");
?>

