
<script type="text/javascript">
$(function(){
   $("#dialog-error, #dialog-success").dialog({
      autoOpen: false,
	  buttons: { "Ok": redirect },
	  close: function(event, ui) { redirect }
   });

   function redirect(){
	   $(location).attr("href", "<?php echo $base_url; ?>/admin-settings");
   }
});

function openDialog($type,$message,$warning){
  var $dialog = $("#dialog-"+$type);
  $dialog.find(".description").empty();
  if($message!=undefined){ $dialog.find(".description").text($message); }
  if($warning!=undefined){ console.log($warning); }

  $(function(){ $( document ).ready(function(){ $dialog.dialog("open"); }); });
}
</script>

<div id="dialog-error" title="Error Notification">
  <p>We&rsquo;re very sorry, we can't save your data right now.</p><p class="description"></p>
</div>
<div id="dialog-success" title="Confirmation Notice">
  <p>Data succesfully updated</p><p class="description"></p>
</div>

<?php

$limit_hotel = $_POST['limit_hotel'];
$frontend_account = $_POST['frontend_account'];

    try {
        $stmt = $db->prepare("UPDATE mastersetting SET limit_hotel = :a, updated_at = :b, updated_by = :c,frontend_account=:frontend_account WHERE masterprofileoid = :id");
        $stmt->execute(array(':a' => $limit_hotel, ':b' =>  date("Y-m-d H:i:s"), ':c' => $_SESSION['_user'],':frontend_account'=>$frontend_account, ':id' => '1'));

?>
        <script>openDialog("success"); </script>

<?php }catch(PDOException $ex) {  ?>

    <script>openDialog("error","",<?=json_encode($ex->getMessage());?>); </script>

<?php } ?>