<?php
	// include("function.php");
	try {
		$stmt = $db->query("SELECT * FROM mastersetting where masterprofileoid = '1'");
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
			$r_mastersetting = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_mastersetting as $row){
				$limit_hotel = $row['limit_hotel'];
				$frontend_account = $row['frontend_account'];
				$updated_at = $row['updated_at'];
				$updated_by = $row['updated_by'];
			}
		}else{
			echo "No Result";
			die();
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}
?>
<section class="content-header">
    <h1>
        Admin Settings
    </h1>
    <ol class="breadcrumb">
        <li class="active"><a href="#"><i class="fa fa-book"></i> Admin Settings</a></li>
    </ol>
</section>
<section class="content">
	<div class="row">
        <div class="box box-form">
            <div id="data-box" class="box-body">
            <h1>ADMIN SETTINGS</h1><hr />
            <form method="post" enctype="multipart/form-data" id="form-admin-settings" action="<?php echo $base_url; ?>/admin-settings/save">

		          <div class="row">
                        <div class="col-md-12 col-xs-12">
                                <!-- <div class="row">
                                    <div class="form-group col-md-8">
                                        <label>Name</label><input type="text" class="form-control" name="name" value="<?php //echo $name; ?>" required="required">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label>Short Code</label><input type="text" class="form-control" name="shortcode" value="<?php //echo $shortcode; ?>" required="required">
                                    </div>
                                </div> -->
                                <div class="form-group">
                                    <label>Limit Hotel</label><input type="text" class="form-control" name="limit_hotel" value="<?php echo $limit_hotel; ?>">
                                </div>
                                <div class="form-group">
                                    <label>Frontend Account</label><select class="form-control" name="frontend_account" value="<?php echo $frontend_account; ?>">
                                        <option value="agent" <?php echo ($frontend_account=='agent'?'selected':''); ?>>Agent</option>
                                        <option value="member" <?php echo ($frontend_account=='member'?'selected':''); ?>>Member</option>
                                        <option value="" <?php echo ($frontend_account==''?'selected':''); ?>>None</option>
                                    </select>
                                </div>
                        </div>
                    </div>
                    <br><br>
                    <div class="row">
                        <div class="col-md-6">
                            <?php
                            if(!empty($updated_at) and !empty($updated_by)){
                                echo "<i class='fa fa-clock-o'></i> Last updated on ".date('d/M/Y H:i', strtotime($updated_at))." <small>(GMT+8)</small> by <b>".$updated_by."</b>";
                            }
                            ?>

                        </div>
                        <div class="col-md-6">
                            <div class="form-group text-right">
                                <!-- <button type="button" class="btn btn-danger cancel">Cancel</button> -->
                                <button type="submit" class="btn btn-primary submit">Save</button>
                            </div>
                        </div>
                    </div>
            </form>
        </div>
   		</div>
    </div>
</section>
<script>

//--------- prevent enter as submit action
$('body').on('keypress','input', function(e) { if (e.keyCode == 13){ e.preventDefault(); } });


</script>
