<?php
function getCountry($country)
{

	try {
		global $db;
		$stmt = $db->query("select * from country");
		$r_country = $stmt->fetchAll(PDO::FETCH_ASSOC);
		foreach($r_country as $row){
			if($row['countryoid'] == $country){ $selected = "selected"; }else{ $selected=""; }
	?>
		<option value="<?php echo $row['countryoid']; ?>" <?php echo $selected; ?>><?php echo $row['countryname']; ?></option>
	<?php
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}

}

function getPublished($published)
{

	try {
		global $db;
		$stmt = $db->query("select * from published where showthis = 'y'");
		$r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
		foreach($r_room as $row){
			if($row['publishedoid'] == $published){ $selected = "selected"; }else{ $selected=""; }
			echo"<option value='".$row['publishedoid']."' ".$selected.">".$row['note']."</option>";
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}

}

function getStatusIBE($status)
{
	$liststatus = array(0,10,1,2);
	$liststatus_name = array('Demo (Hide IBE)', 'Demo (Show IBE)', 'Live', 'Terminated');
	if(!in_array($status, $liststatus)){ $status = 0; }
	foreach($liststatus as $key => $value){
		if($value == $status){ $selected = "selected='selected'"; }else{ $selected=""; }
		echo"<option value='".$value."' ".$selected.">".$liststatus_name[$key]."</option>";
	}
}

function getSubscribe($hotelsubscribe)
{

	try {
		global $db;
		$stmt = $db->query("select * from hotelsubscribe where publishedoid = '1'");
		$r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
		foreach($r_room as $row){
			if($row['hotelsubscribeoid'] == $hotelsubscribe){ $selected = "selected"; }else{ $selected=""; }
			echo"<option value='".$row['hotelsubscribeoid']."' ".$selected.">".$row['subscribe']."</option>";
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}

}
?>
