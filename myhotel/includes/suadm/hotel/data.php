<?php
	error_reporting(E_ALL & ~E_NOTICE);
	include("../../../conf/connection.php");
	include("../../paging/pre-paging.php");

	$usr = $_REQUEST['usr'];
	$useroid = $_REQUEST['useroid'];
	
	$q_user	= $db->query("select userstypeoid from users where useroid = '".$useroid."'");
	$user	= $q_user->fetch(PDO::FETCH_ASSOC);
	
if($user['userstypeoid'] != '4' and $user['userstypeoid'] != '3' ){
?>
<button type="button" class="small-button blue add-button">Create New Hotel</button>
<?php
}
?>
<ul class="inline-block this-inline-block">
	<li><span class="status-green square">sts</span> Active</li>
	<li><span class="status-black square">sts</span> Inactive</li>
	<li><span class="status-red square">sts</span> Expired</li>
</ul>

<?php
	/*if($_SESSION['_typeusr'] == "4"){
		$main_query =
			"SELECT c.cityname, s.statename, h.hoteloid, h.hotelname, h.stars, h.publishedoid, (case when h.publishedoid = 2 then 'status-red' when h.publishedoid = 1 then 'status-green' else 'status-black' end) as status, ht.category
			from hotel h inner join hoteltype ht using (hoteltypeoid) inner join published using (publishedoid)
			inner join city c using (cityoid) inner join state s using (stateoid) inner join country ct using (countryoid)
			inner join
		";
	}else{*/
		$main_query =
			"SELECT c.cityname, s.statename, h.hoteloid, h.hotelname, h.stars, h.publishedoid, (case when h.publishedoid = 2 then 'status-red' when h.publishedoid = 1 then 'status-green' else 'status-black' end) as status, ht.category
			from hotel h inner join hoteltype ht using (hoteltypeoid) inner join published using (publishedoid)
			inner join city c using (cityoid) inner join state s using (stateoid) inner join country ct using (countryoid)
		";
	// }

	$filter = array();
	if(isset($_REQUEST['name']) and $_REQUEST['name']!=''){
		array_push($filter, 'h.hotelname like "%'.$_REQUEST['name'].'%"');
	}
	if(isset($_REQUEST['type']) and $_REQUEST['type']!=''){
		array_push($filter, 'h.hoteltypeoid = "'.$_REQUEST['type'].'"');
	}
	if(isset($_REQUEST['star']) and $_REQUEST['star']!=''){
		array_push($filter, 'h.stars = "'.$_REQUEST['star'].'"');
	}
	if(isset($_REQUEST['country']) and $_REQUEST['country']!=''){
		array_push($filter, 'ct.countryoid = "'.$_REQUEST['country'].'"');
	}

	// array_push($filter, 'h.publishedoid not in (3) and h.hotelstatusoid IN (1,10) ');
	array_push($filter, 'h.publishedoid not in (3) ');

	//--- Filter CHAIN
	if($usr == "4"){
		$s_chain = "SELECT ua.oid FROM users u INNER JOIN userassign ua USING (useroid) WHERE ua.type = 'chainoid' AND u.useroid = '".$useroid."'";
		// echo $s_chain;
		$q_chain = $db->query($s_chain);
		$n_chain = $q_chain->rowCount();
		$r_chain = $q_chain->fetchAll(PDO::FETCH_ASSOC);
		foreach($r_chain as $row){
			array_push($filter, "h.chainoid = '".$row['oid']."'");
			// array_push($filter, 'h.hotelname LIKE "%amaz%" ');
		}
	}

	if(count($filter) > 0){
		$combine_filter = implode(' and ',$filter);
		$query = $main_query.' where '.$combine_filter;
	}else{
		$query = $main_query;
	}

	try {
		$main_query = $query." ORDER BY h.hotelstatusoid DESC, h.publishedoid DESC, h.hotelname ASC ";
		$stmt = $db->query($main_query.$paging_query);
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
?>
<table class="table promo-table">
	<tr>
			<td>Property Name</td>
			<td>Location</td>
			<td>Stars</td>
			<td>Property Type</td>
			<td class="algn-right">&nbsp;</td>
	</tr>
<?php
		$r_hotel = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_hotel as $row){
				$hoteloid = $row['hoteloid'];
				$hotelname = $row['hotelname'];
				$cityname = $row['cityname']; $statename = $row['statename'];
				$star =  $row['stars']; if(empty($star) or $star < 1){ $star = "-"; }
				$type = $row['category'];
?>
    <tr class="<?php echo $row['status']; ?>">
        <td><?php echo $hotelname; ?></td>
        <td><?php echo $statename." &rarr; ".$cityname; ?></td>
        <td>
        <?php
			for($i=1;$i<=$star;$i++) {
				//echo '<img src="images/rating/full-star.gif" width="16px" height="16px" />';
				echo '<i class="fa fa-star" style="color: #fbbc05;"></i>';
			}
		?>
        </td>
        <td><?php echo $type; ?></td>
        <td class="algn-right">
        <?php if($user['userstypeoid'] != '4' and $user['userstypeoid'] != '3' ){ ?>
        <button type="button" class="pencil edit-button" hoid="<?php echo $hoteloid; ?>">Edit</button>
        <?php } ?>
		<?php if($usr == "1"){ ?>
        <!-- <button type="button" class="trash delete-button" hoid="<?php echo $hoteloid; ?>">Delete</button> -->
        <?php } ?>
        <button type="button" class="small-button blue manage" hoid="<?php echo $hoteloid; ?>">manage</button>
        </td>
    </tr>
<?php
			}
?>
</table>
<?php
		}

		// --- PAGINATION part 2 ---
		$main_count_query = "select count(*) as jml from hotel h inner join hoteltype ht using (hoteltypeoid) inner join published using (publishedoid) inner join city c using (cityoid) inner join state s using (stateoid) inner join country ct using (countryoid)";
		if(count($filter) > 0){
			$combine_filter = implode(' and ',$filter);
			$main_count_query = $main_count_query.' where '.$combine_filter;
		}
		$stmt = $db->query($main_count_query);
		$jmldata = $stmt->fetchColumn();

		$tampildata = $row_count;
		include("../../paging/post-paging.php");

	}catch(PDOException $ex) {
		echo "Invalid Query";
		print($ex);
		die();
	}
?>
