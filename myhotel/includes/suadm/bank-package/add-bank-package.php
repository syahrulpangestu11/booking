<?php
	include('includes/bootstrap.php');
	include('includes/Package/geo-location/geo-location.php');

	$daylist = array("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat");
	$stmt = $db->query("select max(priority)+1 as maxpriority from package where hoteloid = '".$hoteloid."' and publishedoid = '1'");
	$r_max = $stmt->fetchAll(PDO::FETCH_ASSOC);
	foreach($r_max as $row){
		$maxpriority = $row['maxpriority'];
	}
?>
<style>
	span.label{ color:#333; font-size:0.9em; }
</style>
<script type="text/javascript">
$(function() {
	$('textarea#html-box').trumbowyg({
		btns: ['viewHTML',
		  '|', 'btnGrp-design',
		  '|', 'link',
		  '|', 'btnGrp-justify',
		  '|', 'btnGrp-lists'],
		fullscreenable: false
	});
});
</script>
<section class="content-header">
    <h1>
        Create Packages Template
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i>  Packages Template </a></li>
        <li class="active">Create New</li>
    </ol>
</section>
<section class="content" id="Package">
	<div class="row">
        <form class="form-box form-horizontal form-box" method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/package-template/add-process">
        <div class="box box-form">
            <div class="form-group"><div class="col-md-12"><h1>Package</h1></div></div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-md-12">Package Name</label>
                    <div class="col-md-12"><input type="text" class="form-control" name="name" required="required" value="<?=$name?>" style="width:100%"></div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">Upload New Package Image</label>
                    <div class="col-md-12"><input type="file" class="form-control" name="image"><i>recommended size : 550px x 310px</i></div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <label>Headline</label>
                        <textarea id="html-box" name="headline"></textarea>
                    </div>
                    <div class="col-md-12">
                        <label>Package Description</label>
                        <textarea id="html-box" name="description"></textarea>
                    </div>
                    <div class="col-md-12">
                        <label>Package Inclusive</label>
                        <textarea id="html-box" name="servicefacilities"></textarea>
                    </div>
                    <div class="col-md-12">
                        <label>Package Terms &amp; Condition</label>
                        <textarea id="html-box" name="termcondition"></textarea>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <ul class="inline-triple Package-triple">
                	<li style="width:100%">
                    	<h2>CONDITION</h2>
                    	<ul class="block">
                        	<li>
                            	<span>Stay:</span>
                                <input type="text" class="small" name="stay" value="1" min="1">
                            </li>
                            <li>
                                <span>Guest Booking Within:</span><br />
                                <span style="vertical-align:bottom;"><input type="number" class="small" name="min_ci" value="0" min="0" /> to <input type="number"  class="small" name="max_ci" value="0" min="0" /> days (before check in)</span>
                            </li>
                        </ul>
                    </li>
                </ul>
                <div style="padding-top:15px">
                    <div class="panel panel-danger">
                        <div class="panel-heading" style="color:#000">
                        <label>Deposit Type</label>
                        <div>
                        <?php
                            try {
                                $stmt = $db->query("select d.* from deposit d inner join published p using (publishedoid) where p.publishedoid = '1'");
                                $r_dt = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                foreach($r_dt as $row){
                                    if($row['depositoid'] == 1){ $selected = "checked"; $depositval = $depositvalue; }else{ $selected=""; $depositval = ""; }
                                    echo"<input type='radio' name='deposit' value='".$row['depositoid']."' ".$selected.">  ".$row['name'];
                                    if($row['depositoid'] == '2'){
                                        echo"<div class='depositval' style='display:none'><input type='text' class='medium'  name='depositvalue' value='".$depositval."'> Deposit for IDR Amount</div>";
                                    }else if($row['depositoid'] == '3'){
                                        echo"<div class='depositval' style='display:none'><input type='text' class='medium'  name='depositvalue' value='".$depositval."'> Deposit for %(percentage)from total</div>";
                                    }else if($row['depositoid'] == '4'){
                                        echo"<div class='depositval' style='display:none'><input type='text' class='medium'  name='depositvalue' value='".$depositval."'> night(s) amount</div>";
                                    }
                                    echo "<br>";
                                }
                            }catch(PDOException $ex) {
                                echo "Invalid Query";
                                die();
                            }
                        ?>
                        </div>
                        </div>
                    </div>
                </div>
                <ul class="inline">
                    <label>Headline Icon</label><br>
                <?php
                    try {
                        $stmt = $db->query("SELECT th.*, i.*, x.termheadlinepromooid from termheadline th left join icon i using (iconoid) left join (select termheadlinepromooid, termheadlineoid from termheadlinepromo thp where thp.id='".$promooid."' and thp.type='Package') x on x.termheadlineoid = th.termheadlineoid
                        where th.hoteloid in ('0') order by icon_title");
                        $r_headline = $stmt->fetchAll(PDO::FETCH_ASSOC);
                        foreach($r_headline as $termheadline){
                            if($termheadline['iconoid'] == 0){
                                $icon_src = $termheadline['term_icon_src'];
                            }else{
                                $icon_src = $termheadline['icon_src'];
                            }
                            ?>
                            <li class="col-md-6">
                            <input type="checkbox" name="icon[]" value="<?=$termheadline['termheadlineoid']?>" />&nbsp;&nbsp;<img src="<?=$icon_src?>" class="icon" />&nbsp;&nbsp;<?=$termheadline['icon_title']?>
                            </li>
                            <?php
                        }
                    }catch(PDOException $ex) {
                        echo "Invalid Query";
                        die();
                    }
                ?>
                </ul>
                <div class="clear"></div>
                <div style="padding-top:15px">
                    <label>Publish Package</label>
                    <select name="published" class="form-control">
                    <?php
                        try {
                            $stmt = $db->query("select * from published where showthis = 'y' ORDER BY publishedoid DESC");
                            $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                            foreach($r_room as $row){
                                echo"<option value='".$row['publishedoid']."' style='text-transform: capitalize;'>".$row['note']."</option>";
                            }
                        }catch(PDOException $ex) {
                            echo "Invalid Query";
                            die();
                        }
                    ?>
                    </select>
                </div>
            </div>
            <div class="clear"></div>
            <div class="box-footer" style="margin-top:15px">
                <div class="col-md-12 text-right">
                    <button class="btn btn-danger cancel" type="button">Cancel</button>
                    <button class="btn btn-primary" type="submit">Save Template</button>
                </div>
            </div>
		</div>
		</form>
    </div>
</section>
