<script type="text/javascript">
$(function(){
	
	$(document).ready(function(){ 
		
		getLoadData();
		
		$('input[type=radio][name=discounttype]:checked, input[type=radio][name=discountapply]:checked, input[type=radio][name=deposit]:checked, input[type=radio][name=link-style]:checked').each(function(){
			if($(this).prop("checked") == true){
				$(this).click();
			}
		});
	});
	
	$('body').on('click','input[type=radio][name=discounttype]', function(e) {
		
		if($(this).val() == '2'){
			$('li.applybox').css('display','none');
		}else{
			$('li.applybox').css('display','block');
		}
			
		if($(this).prop( "checked" ) == true){
			label = $(this).attr('label');
			$('input[type=text][name=discountvalue]').next('span.label').html(label);
		}
	});
	
	$('body').on('click','input[type="radio"][name=discountapply]', function(e) {
		allElem = $('div.applyval');
		allElem.css('display','none');
		
		if($(this).val() == '2'){
			$('input[type="checkbox"][name="applyvalue[]"]').prop('disabled',true);
			$('input[type="text"][name="applyvalue"]').prop('disabled',false);
		}else{
			$('input[type="checkbox"][name="applyvalue[]"]').prop('disabled',false);
			$('input[type="text"][name="applyvalue"]').prop('disabled',true);
		}		
		
		if($(this).val() == '2' || $(this).val() == '3' ){
			applyval = $(this).next('div.applyval');
			if($(this).prop( "checked" ) == true){
				applyval.css('display','block');
			}else{
				applyval.css('display','none');
			}
		}
	});
	
	$('body').on('click','input[type="radio"][name=deposit]', function(e) {
		allElem = $('div.depositval');
		allElem.css('display','none');
		
		
		if($(this).val() == '2' || $(this).val() == '3'  || $(this).val() == '4' ){
			applyval = $(this).next('div.depositval');
			if($(this).prop( "checked" ) == true){
				applyval.css('display','block');
			}else{
				applyval.css('display','none');
			}
		}
		
		$('input[type="text"][name="depositvalue"]').prop('disabled',true);
		$(this).next('div.depositval').children('input[type="text"][name="depositvalue"]').prop('disabled',false);
	});
	
	function readURL(input, target) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
				target.attr('src', e.target.result).show();
			};
			reader.readAsDataURL(input.files[0]);
		}
	}
	
	$('body').on('change','input[name=image]', function(e){
		box_preview_image = $('div.preview-image');
		if(this.files[0].size <= 0){
			box_preview_image.html('<div class="panel panel-warning"><div class="panel-heading">N/A Image for Promotion</div></div>');
		}else{
			var ext = $(this).val().split('.').pop().toLowerCase();
			if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
				box_preview_image.html('<div class="panel panel-warning"><div class="panel-heading">Invalid extension! Please upload image with JPEG, PNG, JPG, and GIF type only.</div></div>');
			}else{
				box_preview_image.html('<img src="#" id="preview-image" class="img-responsive" alt="Responsive image" style="width:200px;">');
				target = $('div.preview-image').children('img');
				readURL(this, target);
			}
		}
	});
	
	$('body').on('click','button.edit', function(e) {
		pid = $(this).attr("pid");
		url = "<?php echo $base_url; ?>/promotion-template/edit/?cpoid="+pid;
      	$(location).attr("href", url);
	});

	$('body').on('click','button.cancel', function(e) {
		pid = $(this).attr("pid");
		url = "<?php echo $base_url; ?>/promotion-template";
      	$(location).attr("href", url);
	});

	$('body').on('click','button.add', function(e) {
		pid = $(this).attr("pid");
		url = "<?php echo $base_url; ?>/promotion-template/add";
      	$(location).attr("href", url);
	});
	
   var $dialogDelete = $('<div id="dialog-del"></div>')
	.html('Are you sure you want to delete this data?')
	.dialog({
		autoOpen: false,
		title: 'Confirmation',
		buttons: { 
			"Delete": function(){ 
				var url = $(this).data('url'); 
				var id = $(this).data('id'); 
				$(this).dialog("close"); 
				deleteAction(url, id);
			},
			Cancel: function(){ $( this ).dialog("close"); }
		}
	});
	
	var $dialogNotification = $('<div id="dialog-notice"></div>')
	.dialog({
		autoOpen: false,
		title: 'Notification',
		buttons: { 
			"Ok": function(){ 
				var callback = '<?php echo $base_url; ?>/promotion-template'; 
				$(this).dialog( "close" );
				$(location).attr("href", callback); 
			}
		}
	});
	
	function deleteAction(url, id){
		$.ajax({
			url: url,
			type: 'post',
			data: { id : id},
			success: function(data) {
				if(data == "1"){
					$dialogNotification.html("Data has been deleted");
					$dialogNotification.dialog("open");
				}else{
					$dialogNotification.html("We&rsquo;re very sorry, we can't save your data right now");
					$dialogNotification.dialog("open");
				}
			}
		});
	}
	
	$('body').on('click','button.delete', function(e) {
		id = $(this).attr("pid");
		$dialogDelete.data("url", "<?php echo $base_url; ?>/includes/suadm/bank-promotion/delete-cp.php");
		$dialogDelete.data("id", id);
		$dialogDelete.dialog("open");
	});
		
	function getLoadData(){
		$.ajax({
			url: "<?php echo"$base_url"; ?>/includes/suadm/bank-promotion/data.php",
			type: 'post',
			data: $('form#data-input').serialize(),
			success: function(data) {
				$("#data-box").html(data)
			}
		});
	}
});
</script>