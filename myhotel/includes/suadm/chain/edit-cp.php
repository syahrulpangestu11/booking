<section class="content-header">
    <h1>
       	Hotel Chain
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Hotel Chain</a></li>
        <li class="active">Edit</li>
    </ol>
</section>
<?php
	$cpoid = $_GET['cpoid'];
	try {
		$stmt = $db->query("select cp.* from chain cp where chainoid = '".$cpoid."'");
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
			$r_promo = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_promo as $row){
				$logourl = $row['logo'];
				$name = $row['name'];
				$description = $row['description'];
				$published = $row['publishedoid'];
			}
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}
?>
<section class="content" id="basic-info">
	<div class="row">
        <div class="box box-form">
            <form method="post" enctype="multipart/form-data" id="data-input" action="#">
            <input type="hidden" name="cpoid" value="<?php echo $cpoid; ?>">
            <ul class="inline form-input">
                <li>
                    <div class="side-left"><label>Chain Name</label></div>
                    <div class="side-right"><input type="text" class="input-text" name="name" value="<?php echo $name; ?>" required="required"></div>
                    <div class="clear"></div>
                </li>
                <li>
                    <div class="side-left"><label>Chain Description</label></div>
                    <div class="side-right"><textarea name="description" style="height:100px" required="required"><?php echo $description; ?></textarea></div>
                    <div class="clear"></div>
                </li>
                <li>
                    <div class="side-left"><label>Logo URL</label></div>
                    <div class="side-right"><textarea name="logourl" style="height:50px"><?php echo $logourl; ?></textarea></div>
                    <div class="clear"></div>
                </li>
                <li>
                    <div class="side-left"><label>Publish Chain</label></div>
                    <div class="side-right">
                        <select name="published" class="input-select">
                        <?php
                            try {
                                $stmt = $db->query("select * from published where showthis = 'y'");
                                $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                foreach($r_room as $row){
                                    if($row['publishedoid'] == $published){ $selected = "selected"; }else{ $selected=""; }
                                    echo"<option value='".$row['publishedoid']."' ".$selected.">".$row['note']."</option>";
                                }
                            }catch(PDOException $ex) {
                                echo "Invalid Query";
                                die();
                            }
                        ?>        	
                        </select>
                    </div>
                    <div class="clear"></div>
                </li>
                <li>
                    <div class="side-left">&nbsp;</div>
                    <div class="side-right"><button type="button" class="submit-edit">Save</button></div>
                    <div class="clear"></div>
                </li>
                <div class="clear"></div>
            </ul>
            </form>
   		</div>
    </div>
</section>
