<?php
function saveImage($tn, $savePath, $imageQuality="100") {
	// *** Get extension
	$extension = strrchr($savePath, '.');
	$extension = strtolower($extension);

	switch($extension) {
		case '.jpg':
		case '.jpeg':
			if (imagetypes() & IMG_JPG) {
				imagejpeg($tn, $savePath, $imageQuality);
			}
			break;
		case '.gif':
			if (imagetypes() & IMG_GIF) {
				imagegif($tn, $savePath);
			}
			break;
		case '.png':
			// *** Scale quality from 0-100 to 0-9
			$scaleQuality = round(($imageQuality/100) * 9);

			// *** Invert quality setting as 0 is best, not 9
			$invertScaleQuality = 9 - $scaleQuality;

			if (imagetypes() & IMG_PNG) {
				imagepng($tn, $savePath, $invertScaleQuality);
			}
			break;
		// ... etc
		default:
			// *** No extension - No save.
			break;
	}

	imagedestroy($tn);
}
?>
