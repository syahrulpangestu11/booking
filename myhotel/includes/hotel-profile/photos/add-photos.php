<script type="text/javascript">
$(function(){

	photosOf =
		'<select name="reference[]" class="input-select">'+
			<?php
			echo '\'<option value="hotel-'.$hoteloid.'">Hotel Pictures</option>\'+';
			try {
				$stmt = $db->query('select roomoid, r.name from room r inner join hotel h using (hoteloid) where h.hoteloid = "'.$hoteloid.'"');
				$r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
				foreach($r_room as $row){
					echo '\'<option value="room-'.$row['roomoid'].'">'.$row['name'].'</option>\'+';
				}
			}catch(PDOException $ex) {
				echo 'Invalid Query';
				die();
			}
			?>
		'</select>';

	mainPicture = '<select name="main[]"><option value = "main">yes</option><option value = "unset" selected>no</option></select>';

	var $dialogNotice = $('<div id="dialog-notice"></div>')
	.dialog({
		autoOpen: false,
		title: 'Notification',
		buttons: {
			"Ok": function(){ $( this ).dialog( "close" ); }
		}
	});

	function readURL(input, previewTarget) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            previewTarget
                .attr('src', e.target.result)
                .height(100)
                .show();
        };
        reader.readAsDataURL(input.files[0]);
    }
  }

	$(document).ready(function(){ appendContent(); });

	$('body').on('click','button.add', function(e) {
		appendContent();
	});

	function appendContent(){
		tableTarget = $('table.table-fill');
		tableTarget.find('button.add').remove();
		tableTarget.append(
			"<tr>"+
				"<td>"+
					"<input type='file' class='input-text input-image' name='photos[]' accept='image/*'>"+
					"<br><img class='input-image-preview' id='preview' src='#' alt='' />"+
				"</td>"+
				"<td>"+photosOf+"</td>"+
				"<td><button type='button' class='blue-button add'>+ Add More Photo</button></td>"+
			"</tr>");

		$('.input-image').each(function(){
      $('.input-image').change(function() {
        var thisElem = $(this);
        var thisElemJS = this;
        var previewTarget = thisElem.closest('tr').find('.input-image-preview');
        var sizeKB = thisElemJS.files[0].size / 1024;
        var img = new Image();
        img.src = window.URL.createObjectURL( this.files[0] );
        img.onload = function() {
          var width = img.naturalWidth,
              height = img.naturalHeight;
          // alert(sizeKB+' ** '+width +'×'+height);

          if(sizeKB > 200){
              // alert("Your image more than 200 KB, please compress or resize your image.");
              $dialogNotice.html("Your image more than 200 KB, please compress or resize your image");
              $dialogNotice.dialog("open");
              thisElem.val("");
              previewTarget.attr('src',"#");
          }else{
              var ext = thisElem.val().split('.').pop().toLowerCase();
              if($.inArray(ext, ['jpg','jpeg', 'png', 'gif']) == -1) {
                // alert('Invalid extension! Please make sure you upload image file type (.jpg , .jpeg , .png , or .gif).');
                $dialogNotice.html("Invalid extension! Please make sure you upload image file type (.jpg , .jpeg , .png , or .gif)");
                $dialogNotice.dialog("open");
                thisElem.val("");
                previewTarget.attr('src',"#");
              }else{
              readURL(thisElemJS, previewTarget);
            }
          }
        }

      });
    });
	}
});
</script>
<section class="content-header">
    <h1>
        Add Photos
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-laptop"></i> Settings</a></li>
        <li>Hotel Profile</li>
        <li>Photo</li>
        <li class="active">Add</li>
    </ol>
</section>
<section class="content" id="photo">
	<div class="row">
        <div class="box box-form">
            <div class="box-body">
                <h1>Add New Photos</h1>
                <form method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/hotel-profile/photo/add-process">
                <input type="hidden" name="hoteloid" value="<?php echo $hoteloid; ?>">
								<input type="hidden" name="page_ref" value="<?php echo $base_url."/hotel-profile/photo"; ?>">
                <table class="table table-fill table-fill-centered">
                    <tr>
                        <td>Picture</td>
                        <td>Photos of</td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
                <br /><br />
                <button type="submit" class="small-button blue"><i class="fa fa-save"></i>Save</button>
                </form>
			</div>
   		</div>
    </div>
</section>
