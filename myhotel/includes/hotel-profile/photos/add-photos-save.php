<?php
$page_ref = $_POST['page_ref'];
 ?>

<script type="text/javascript">
$(function(){
   $("#dialog").dialog({
      autoOpen: false,
	  buttons: { "Ok": redirect },
	  close: function(event, ui) { redirect }
   });

   function redirect(){
	   $(location).attr("href", "<?php echo $page_ref; ?>");
   }
});
</script>

<?php

$hoteloid = $_POST['hoteloid'];
$photos = $_FILES['photos'];

// if(!empty($_FILES['photos'])){

	// $allowedExts = array('jpg', 'jpeg', 'gif', 'png');
	// $allowedType = array('image/jpg', 'image/jpeg', 'image/pjpeg', 'image/gif', 'image/png');

  $allowedType = array('image/gif', 'image/jpeg', 'image/pjpeg', 'image/jpg', 'image/png', 'image/x-png');
  $allowedExts = array('gif', 'jpeg', 'jpg', 'png');

  ?>

  <!-- <div>
    <p>
      <pre>
      <?php print_r($_FILES);?>
      <hr>
      <?php print_r($_POST);?>
      </pre>
    </p>
  </div> -->

  <script type="text/javascript">
  $(function(){
      $( document ).ready(function() {
          $("#dialog").dialog("open");
      });
  });
  </script>
  <?php
  	foreach($_FILES['photos']['name'] as $key => $value){
      $picture_error = $_FILES['photos']['error'][$key];
      // echo "error = ".$picture_error;
  		if($picture_error > 0){
        if($picture_error==4){
          continue; //skip jika kosong
        }
  		    ?>
              <div id="dialog" title="Notification">
                <p>We&rsquo;re very sorry, your data are succesfully saved, but we cannot save your photo(s).<br>Please make sure you upload image file type (.jpg , .jpeg , .png , or .gif), dimensions not larger than 1920 x 1080 pixels, and file size not larger than 200 KB.
                  ***PICT ERROR***
                  <pre>
                  <?php print_r($_FILES);?>
                  <hr>
                  <?php print_r($_POST);?>
                  </pre>
                </p>
              </div>
              <script type="text/javascript">
              $(function(){
                  $( document ).ready(function() {
                      $("#dialog").dialog("open");
                  });
              });
              </script>
  		<?php
  			unset($_FILES['photos']);
  			die();
  		}else{
  			$picture_name		= $_FILES['photos']['name'][$key];
  			$picture_tmp_name	= $_FILES['photos']['tmp_name'][$key];
  			$picture_type		= $_FILES['photos']['type'][$key];

  			$explode_picture_name	= explode('.', $picture_name);
  			$picture_extension		= end($explode_picture_name);
  			$ext = ".".strtolower($picture_extension);

  			if(!in_array($picture_type, $allowedType) or !in_array($picture_extension, $allowedExts)){
  			?>
  				<div id="dialog" title="Error Notification">
  				  <p>We&rsquo;re very sorry, we can't save your photo(s) right now.<br />Please make sure you upload image file type (ex: .jpg , .png , .gif)</p>
  				</div>
  				<script type="text/javascript">
  				$(function(){
  					$( document ).ready(function() {
  						$("#dialog").dialog("open");
  					});
  				});
  				</script>
  			<?php
  				unset($_FILES['photos']);
  				die();
  			}else{

  				$picture_upload_name		= $key.'-'.date("Y_m_d_H_i_s").'-'.$picture_name;
          // echo "asdasdasdas";
          include("includes/hotel-profile/photos/compress-image.php");
          // echo "qweqweqwe";
          // echo "path=".$web_url."/".$folder.$imagename;
  				$show_upload_path = $web_url."/".$folder.$imagename;
  				$show_thumbnail_path = $web_url."/".$folder.$imagename_t;

  				$reference		= explode('-' , $_POST['reference'][$key]);
  					$reftable	= $reference[0];
  					$refid		= (!empty($reference[1])) ? $reference[1] : "0";
  				$main 			= '';

  				try {
  					$stmt = $db->prepare("select hotelphotooid from hotelphoto where hoteloid = :a and ref_table = :b and ref_id = :c and flag = :d");
  					$stmt->execute(array(':a' => $hoteloid , ':b' => $reftable , ':c' => $refid , ':d' => 'main'));
  					$row_count = $stmt->rowCount();
  					if($row_count == 0) {
  						$main = 'main';
  					}
  				}catch(PDOException $ex) {
  					echo $ex->getMessage();
  					die();
  				}


  				try {
  					$stmt = $db->prepare("insert into hotelphoto (hoteloid, photourl, thumbnailurl, ref_table, ref_id, flag) values (:hoid , :a , :b , :c , :d , :e)");
  					$stmt->execute(array(':a' => $show_upload_path , ':b' => $show_thumbnail_path , ':c' => $reftable , ':d' => $refid , ':e' => $main , ':hoid' => $hoteloid));
  					$affected_rows = $stmt->rowCount();
  				}catch(PDOException $ex) {
  					echo $ex->getMessage();
  					die();
  				}
  			}
  		}
  	}
  // }
?>
<div id="dialog" title="Confirmation Notice">
  <p>Photo(s) succesfully saved</p>
  <?php //echo "uplod=".$upload_base;?>
</div>
<script type="text/javascript">
$(function(){
    $( document ).ready(function() {
        $("#dialog").dialog("open");
    });
});
</script>
