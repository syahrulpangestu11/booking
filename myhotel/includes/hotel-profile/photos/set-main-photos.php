<?php
	include("../../../conf/connection.php");
	$hotelphotooid = $_POST['pid'];
	
	try {
		$stmt = $db->prepare("select ref_table, ref_id, h.hoteloid from hotelphoto inner join hotel h using (hoteloid) where hotelphotooid =:a");
		$stmt->execute(array(':a' => $hotelphotooid));
		$r_photo = $stmt->fetchAll(PDO::FETCH_ASSOC);
		foreach($r_photo as $photo){
			$reftable = $photo['ref_table'];
			$refid = $photo['ref_id'];
			$hoteloid = $photo['hoteloid'];
		}
	}catch(PDOException $ex) {
		echo $ex;
		die();
	}
		
	if($_POST['rqst'] == "main"){
	
		try {
			$stmt = $db->prepare("UPDATE hotelphoto SET flag=:a WHERE hoteloid=:hoid and ref_table=:b and ref_id=:c");
			$stmt->execute(array(':a' => "" , ':b' => $reftable , ':c' => $refid , ':hoid' => $hoteloid));
			$affected_rows = $stmt->rowCount();
		}catch(PDOException $ex) {
			echo $ex;
			die();
		}
	
		try {
			$stmt = $db->prepare("UPDATE hotelphoto SET flag=:a WHERE hotelphotooid=:pid");
			$stmt->execute(array( ':a' => "main" , ':pid' => $hotelphotooid));
			$affected_rows = $stmt->rowCount();
		}catch(PDOException $ex) {
			echo $ex;
			die();
		}
		
	}else if($_POST['rqst'] == "flexiblerate"){
		
		try {
			$stmt = $db->prepare("UPDATE hotelphoto SET flag_flexible_rate = :a WHERE hoteloid=:hoid and ref_table=:b and ref_id=:c");
			$stmt->execute(array(':a' => "n" , ':b' => $reftable , ':c' => $refid , ':hoid' => $hoteloid));
			$affected_rows = $stmt->rowCount();
		}catch(PDOException $ex) {
			echo $ex;
			die();
		}
	
		
		try {
			$stmt = $db->prepare("UPDATE hotelphoto SET flag_flexible_rate = :a WHERE hotelphotooid=:pid");
			$stmt->execute(array( ':a' => "y" , ':pid' => $hotelphotooid));
			$affected_rows = $stmt->rowCount();
		}catch(PDOException $ex) {
			echo $ex;
			die();
		}
	
	}

	echo "1"; 
?>