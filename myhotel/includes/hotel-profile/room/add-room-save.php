<?php
	// session_start();
	include("../../../conf/connection.php");

	$hoteloid = $_POST['hoteloid'];
	$name = $_POST['name'];
	$description = $_POST['description'];
	$size = $_POST['roomsize'];
	if(isset($_POST['roomsize_inc_balcon'])){ $sizeincbalcon = $_POST['roomsize_inc_balcon']; }else{ $sizeincbalcon = "0"; }
	$view = $_POST['view'];
	$published = $_POST['published'];

	$numroom = $_POST['numroom'];
	$adult = $_POST['adult'];
	$child = $_POST['child'];
	$allotmentalert = $_POST['allotmentalert'];
	$maxextrabed = $_POST['maxextrabed'];

if(empty($_POST['rt'])){
	echo "r---";

	try {
		$stmt = $db->prepare("INSERT INTO room (hoteloid, name, description, roomsize, roomsize_inc_balcon, viewoid, publishedoid,
			numroom, adult, child, allotmentalert, maxextrabed
		) VALUES (:hoid , :a , :b , :c , :d , :e , :f, :g , :h , :i , :j , :k)");
		$stmt->execute(array(
			':a' => $name,
			':b' => $description,
			':c' => $size,
			':d' => $sizeincbalcon,
			':e' => $view ,
			':f' => $published ,

			':g' => $numroom,
			':h' => $adult,
			':i' => $child,
			':j' => $allotmentalert ,
			':k' => $maxextrabed ,

			':hoid' => $hoteloid
		));
		$affected_rows = $stmt->rowCount();

		try{
			$stmt = $db->prepare("select r.roomoid from room r inner join hotel h using (hoteloid) where r.name=:a and r.description=:b and roomsize=:c and roomsize_inc_balcon=:d and viewoid=:e and r.publishedoid=:f and h.hoteloid=:hoid  ORDER BY r.roomoid DESC LIMIT 1");
			$stmt->execute(array(':a' => $name, ':b' => $description, ':c' => $size, ':d' => $sizeincbalcon, ':e' => $view , ':f' => $published , ':hoid' => $hoteloid));
			$r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_room as $row){
				$roomoid = $row['roomoid'];
				echo $roomoid."---";
				// $_SESSION['added_roomoid'] = $roomoid;
			}
		}catch(PDOException $ex) {
			echo "0";
			echo $ex->getMessage();
			die();
		}

	}catch(PDOException $ex) {
		echo "0";
		echo $ex->getMessage();
		die();
	}

	/*****************************************************/

	if(!empty($_POST['bed'])){
		foreach ($_POST['bed'] as $bed){
			try {
				$stmt = $db->prepare("insert into roombed (`roomoid`, `bedoid`, `publishedoid`) values (:roid , :a , :b)");
				$stmt->execute(array(':roid' => $roomoid, ':a' => $bed, ':b' => '1'));
				$affected_rows = $affected_rows + $stmt->rowCount();
			}catch(PDOException $ex) {
				echo "0";
				echo $ex->getMessage();
				die();
			}
		}
	}else{
		$affected_rows = 0;
	}


	/***********************************************************/

	if(!empty($_POST['roomfcl'])){
		foreach ($_POST['roomfcl'] as $facilities){
			try {
				$stmt = $db->prepare("insert into roomfacilities (`roomoid`, `facilitiesoid`, `publishedoid`) values (:roid , :a , :b)");
				$stmt->execute(array(':roid' => $roomoid, ':a' => $facilities, ':b' => '1'));
				$affected_rows = $affected_rows + $stmt->rowCount();
			}catch(PDOException $ex) {
				echo "0";
				echo $ex->getMessage();
				die();
			}
		}
	}else{
		$affected_rows = 0;
	}

	/*

	$startdate = date("Y-m-d");
	$diff = 365;
	$selectedday = array("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat");

	$rateplanlist = array();
	$elementName = array("extendedpolicy", "netsingle", "netdouble", "netextrabed", "single", "double", "extrabed", "currency", "allotment", "topup", "breakfast", "minstay", "maxstay", "blackout", "surcharge",);
	$elementNodes = array("n", "0", "0", "0", "0", "0", "0", "1", "0", "0", "y", "1", "0", "n", "n");

	try {
		$stmt = $db->query("select * from rateplan");
		$r_plan = $stmt->fetchAll(PDO::FETCH_ASSOC);
		foreach($r_plan as $row){
			array_push($rateplanlist , $row['rateplanoid']);
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}

	try{
		$path = "../../../data-xml/tarif.xml";
		include('../../load-tarif/upload-xml.php');
	}catch(PDOException $ex) {
		echo "[ERROR]<br>".$ex;
		die();
	}

	*/

}else{
	echo "rs---";
	include("room-settings/add-room-settings-save.php");
}

echo "1";
?>
