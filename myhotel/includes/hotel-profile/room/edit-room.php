<?php
	$roomoid = $_GET['ro'];
	try {
		$stmt = $db->query("select r.* from room r inner join hotel h using (hoteloid) where r.roomoid = '".$roomoid."'");
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
			$r_hotel = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_hotel as $row){
				$roomname = $row['name'];
				$description = $row['description'];
				$size = $row['roomsize'];
				$sizeincbalcon = $row['roomsize_inc_balcon'];
				$viewoid = $row['viewoid'];
				$publishedoid = $row['publishedoid'];

				$numroom = $row['numroom'];
				$adult = $row['adult'];
				$child = $row['child'];
				$allotmentalert = $row['allotmentalert'];
				$maxextrabed = $row['maxextrabed'];
			}
		}else{
			echo "No Result";
			die();
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}

	$bedconfigure = array();
	try {
		$stmt = $db->prepare("select rb.bedoid from roombed rb inner join room r using (roomoid) inner join bed b using (bedoid) where r.roomoid=:a");
		$stmt->execute(array(':a' => $roomoid));
		$r_bed = $stmt->fetchAll(PDO::FETCH_ASSOC);
		foreach($r_bed as $row){
			array_push($bedconfigure, $row['bedoid']);
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}

	$facilitieslist = array();
	try {
		$stmt = $db->prepare("select rf.facilitiesoid from roomfacilities rf inner join facilities f using (facilitiesoid) where rf.roomoid=:a and f.facilitiestypeoid=:b and rf.publishedoid=:c and f.publishedoid =:d");
		$stmt->execute(array(':a' => $roomoid, ':b' => '3', ':c' => '1', ':d' => '1'));
		$r_facilities = $stmt->fetchAll(PDO::FETCH_ASSOC);
		foreach($r_facilities as $row){
			array_push($facilitieslist, $row['facilitiesoid']);
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}
?>

<style media="screen">
	.form-input tr td {vertical-align: top;}
	.form-input textarea.input-text {height: 70px !important;}
	.form-input input[type=text].short {width: 50px;}
</style>

<!-- <section class="content-header">
    <h1>
        Edit Room Type
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-laptop"></i> Settings</a></li>
        <li>Hotel Profile</li>
        <li>Rooms</li>
        <li class="active">Edit</li>
    </ol>
</section> -->
<!-- <section class="content" id="rooms">
	<div class="row">
        <div class="box box-form">
            <div id="data-box" class="box-body">

                <h1>Room Settings</h1> -->
                <form method="post" enctype="multipart/form-data" id="data-input" action="#">

                <div class="form-input form-wrapper">
									<input type="hidden" name="roomoid" value="<?php echo $roomoid; ?>">
	                <input type="hidden" name="bu" value="<?php echo $base_url; ?>">

								<ul class="tabs">
									<li class="tab-link" data-tab="tab-1-<?=$roomoid;?>">Basic Info</li>
									<li class="tab-link" data-tab="tab-2-<?=$roomoid;?>">Bed Configuration</li>
									<li class="tab-link" data-tab="tab-3-<?=$roomoid;?>">Room Facilities</li>
									<li class="tab-link" data-tab="tab-4-<?=$roomoid;?>">Rate Plan</li>
									<li class="tab-link" data-tab="tab-5-<?=$roomoid;?>">Photos</li>
								</ul>



								<div id="tab-1-<?=$roomoid;?>" class="tab-content">
									<table class="form-input">
                    <tr>
                        <td>Room Type</td>
                        <td><input type="text" class="input-text" name="name" value="<?php echo $roomname; ?>"></td>
                    </tr>
                    <tr>
                        <td>Room Type Description</td>
                        <td><textarea class="input-text" name="description"><?php echo $description; ?></textarea></td>
                    </tr>
                    <tr>
                        <td>Status Room</td>
                        <td>
                          <select name="published" class="input-select">
														<?php
                            try {
                                $stmt = $db->query("select * from published where showthis = 'y'");
                                $r_published = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                foreach($r_published as $row){
                                    if($row['publishedoid'] == $publishedoid){ $selected = "selected"; }else{ $selected=""; }
                            ?>
                                <option value="<?php echo $row['publishedoid']; ?>" <?php echo $selected; ?>><?php echo $row['note']; ?></option>
                            <?php
                                }
                            }catch(PDOException $ex) {
                                echo "Invalid Query";
                                die();
                            }
                            ?>
                          </select>
                        </td>
                    	</tr>
	                    <tr>
	                    	<td>Minimum Room Size</td>
	                        <td>
	                        <input type="text" class="input-text" name="roomsize" value="<?php echo $size; ?>">
	                        &nbsp;&nbsp;
	                        <?php if($sizeincbalcon == '1'){ $checked = "checked"; }else{ $checked=""; } ?>
	                        <input type="checkbox" name="roomsize_inc_balcon" <?php echo $checked; ?> value="1"> Size include balcon / terrace
	                        </td>
	                    </tr>
	                    <tr>
                        <td>View</td>
                        <td>
													<select name="view" class="input-select">
		                        <?php
		                        try {
		                            $stmt = $db->query("SELECT * FROM view ORDER BY name");
		                            $r_view = $stmt->fetchAll(PDO::FETCH_ASSOC);
		                            foreach($r_view as $row){
		                                if($row['viewoid'] == $viewoid){ $selected = "selected"; }else{ $selected=""; }
		                        ?>
		                            <option value="<?php echo $row['viewoid']; ?>" <?php echo $selected; ?>><?php echo $row['name']; ?></option>
		                        <?php
		                            }
		                        }catch(PDOException $ex) {
		                            echo "Invalid Query";
		                            die();
		                        }
		                        ?>
                          </select>

                        </td>
	                    </tr>
											<tr>
												<td>No. of Rooms</td>
												<td><input type="text" class="small short input-text"  name="numroom" value="<?php echo $numroom; ?>"></td>
											</tr>
											<tr style="display:none;">
												<td>Allotment Alert Level</td>
												<td><input type="text" class="small short input-text"  name="allotmentalert" value="<?php echo $allotmentalert; ?>"></td>
											</tr>
											<tr>
												<td>Max. Occupancy on Existing Bedding</td>
												<td><input type="text" class="small short input-text"  name="adult" value="<?php echo $adult; ?>"></td>
											</tr>
											<tr>
												<td>Max. Extra Beds</td>
												<td><input type="text" class="small short input-text"  name="maxextrabed" value="<?php echo $maxextrabed; ?>"></td>
											</tr>
											<tr>
												<td>Max. Children</td>
						            <td><input type="text" class="small short input-text"  name="child" value="<?php echo $child; ?>"></td>
											</tr>

										</table>

										<button type="button" class="submit-edit-room">Save</button>
                    <button type="button" class="cancel">Cancel</button>
								</div>


								<div id="tab-2-<?=$roomoid;?>" class="tab-content">
									<ul class="inline-triple">
                    <?php
                        try {
                            $stmt = $db->query("select * from bed");
                            $r_card = $stmt->fetchAll(PDO::FETCH_ASSOC);
                            foreach($r_card as $row){
                                if(in_array($row['bedoid'] , $bedconfigure, true)){ $checked = "checked"; }else{ $checked = ""; }
                                echo"<li><input type='checkbox' name='bed[]' value='".$row['bedoid']."' ".$checked.">&nbsp;&nbsp;<span class='capitalize'>".$row['name']."</span></li>";

                            }
                        }catch(PDOException $ex) {
                            echo "Invalid Query";
                            die();
                        }
                    ?>
                    <div class="clear"></div>
                    </ul>

										<button type="button" class="submit-edit-room">Save</button>
                    <button type="button" class="cancel">Cancel</button>
									</div>
									<div id="tab-3-<?=$roomoid;?>" class="tab-content">
                    <ul class="block this-inline-block">
                    <?php
                        try {
                            // $stmt = $db->query("select f.* from facilities f inner join facilitiestype ft using (facilitiestypeoid) where ft.facilitiestypeoid = '3' and publishedoid = '1'");
														$stmt = $db->query("select f.*, CHAR_LENGTH(f.name) AS pjg_nama from facilities f inner join facilitiestype ft using (facilitiestypeoid) where ft.facilitiestypeoid = '3' and publishedoid = '1' ORDER BY pjg_nama");
                            $r_facilities = $stmt->fetchAll(PDO::FETCH_ASSOC);
														$n_facilities = $stmt->rowCount();
                            $m=ceil($n_facilities/5);
                            $n=0;
                            foreach($r_facilities as $row){
															$n++;
                                if(in_array($row['facilitiesoid'] , $facilitieslist, true)){ $checked = "checked"; }else{ $checked = ""; }
                                echo"<li><input type='checkbox' name='roomfcl[]' value='".$row['facilitiesoid']."' ".$checked.">&nbsp;&nbsp;<span class='capitalize'>".$row['name']."</span></li>";
																if($n%$m==0){
                                  echo "</ul><ul class='block this-inline-block'>";
                                }
                            }
                        }catch(PDOException $ex) {
                            echo "Invalid Query";
                            die();
                        }
                    ?>
                    <div class="clear"></div>
                    </ul>
										<button type="button" class="submit-edit-room">Save</button>
                    <button type="button" class="cancel">Cancel</button>
									</div>
									<div id="tab-4-<?=$roomoid;?>" class="tab-content">
										<?php
										// $_GET['rt'] = $roomoid;
										// include("includes/hotel-profile/room/room-settings/js.php");
										include("includes/hotel-profile/room/room-settings/edit-room-settings.php");
										?>
										<button type="button" class="submit-edit-room">Save</button>
                    <button type="button" class="cancel">Cancel</button>
                	</div>

									<div id="tab-5-<?=$roomoid;?>" class="tab-content">
										<?php
										include("includes/hotel-profile/room/photos/add-photos.php");

										?>
                	</div>

                    <!-- <button type="button" class="submit-edit-room">Save</button>
                    <button type="button" class="cancel">Cancel</button> -->
                </div>
                </form>
                <!--
			</div>
   		</div>
    </div>
</section> -->
