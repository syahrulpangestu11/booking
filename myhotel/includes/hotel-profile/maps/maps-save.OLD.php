<?php
	include("../../../conf/connection.php");
	
	$hoteloid = $_POST['hoteloid'];
	$latitude = $_POST['latitude'];
	$longitude = $_POST['longitude'];

	try {
		$stmt = $db->prepare("UPDATE hotel SET latitude=:a , longitude=:b WHERE hoteloid=:hoid");
		$stmt->execute(array(':a' => $latitude, ':b' => $longitude, ':hoid' => $hoteloid));
		$affected_rows = $stmt->rowCount();
	}catch(PDOException $ex) {
		echo "0";
		die();
	}
	echo "1";

?>
