<style>
    .box-footer{padding-top:20px;margin-top:10px;}
    #data-commission .table td > span{display:inline-block;padding:8px 0;}
</style>
<section class="content-header">
    <h1>
        Basic Info
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-laptop"></i> Settings</a></li>
        <li>Hotel Profile</li>
        <li class="active">Basic Info</li>
    </ol>
</section>
<section class="content" id="basic-info">
	<div class="row">
        <form method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/hotel-profile/basic-info/saveinfo">
        <input type="hidden" name="hoteloid" value="<?php echo $hoteloid; ?>">
        <div class="box box-form">
	        <div class="box-header with-border">
            	<h1>Basic Hotel Information</h1>
            </div>
            <div id="data-box" class="box-body">
            <ul class="inline form-input">
                <li>
                    <div class="side-left"><label style="display:block">Property Code</label>
                    <input type="text" class="input-text" value="<?php echo $hotelcode; ?>" disabled="disabled">
                    </div>
                    <div class="side-right"><label style="display:block">Country</label>
                        <select name="country" class="input-select">
                            <?php
                                try {
                                    $stmt = $db->query("select * from country");
                                    $r_type = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                    foreach($r_type as $row){
                                        if($countryoid == $row['countryoid']){ $selected = "selected"; }else{ $selected=""; }
                                        echo"<option value='".$row['countryoid']."' ".$selected.">".$row['countryname']."</option>";
                                    }
                                }catch(PDOException $ex) {
                                    echo "Invalid Query";
                                    die();
                                }
                            ?>
                        </select>
                    </div>
                    <div class="clear"></div>
                </li>
                <li>
                    <div class="side-left"><label style="display:block">Property Name</label>
                    <input type="text" class="input-text" name="name" value="<?php echo $hotelname; ?>">
                    </div>
                    <div class="side-right"><label style="display:block">Location</label>
                        <select name="state" class="input-select">
                            <?php
                                try {
                                    $stmt = $db->query("select * from state");
                                    $r_type = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                    foreach($r_type as $row){
                                        if($stateoid == $row['stateoid']){ $selected = "selected"; }else{ $selected=""; }
                                        echo"<option value='".$row['stateoid']."' ".$selected.">".$row['statename']."</option>";
                                    }
                                }catch(PDOException $ex) {
                                    echo "Invalid Query";
                                    die();
                                }
                            ?>
                        </select>
                    </div>
                    <div class="clear"></div>
                </li>
                <li>
                    <div class="side-left"><label style="display:block">Star Rating</label>
                    <input type="text" class="input-text" name="star" value="<?php echo $star; ?>"></div>
                    <div class="side-right"><label style="display:block">Area</label>
                        <select name="city" class="input-select">
                            <?php
                                try {
                                    $stmt = $db->query("select * from city");
                                    $r_type = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                    foreach($r_type as $row){
                                        if($cityoid == $row['cityoid']){ $selected = "selected"; }else{ $selected=""; }
                                        echo"<option value='".$row['cityoid']."' ".$selected.">".$row['cityname']."</option>";
                                    }
                                }catch(PDOException $ex) {
                                    echo "Invalid Query";
                                    die();
                                }
                            ?>
                        </select>
                    </div>
                    <div class="clear"></div>
                </li>
                <li>
                    <div class="side-left"><label style="display:block">Property Type</label>
                        <select name="hoteltype" class="input-select">
                            <?php
                                try {
                                    $stmt = $db->query("select * from hoteltype");
                                    $r_type = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                    foreach($r_type as $row){
                                        if($hoteltype == $row['hoteltypeoid']){ $selected = "selected"; }else{ $selected=""; }
                                        echo"<option value='".$row['hoteltypeoid']."' ".$selected.">".$row['category']."</option>";
                                    }
                                }catch(PDOException $ex) {
                                    echo "Invalid Query";
                                    die();
                                }
                            ?>
                        </select>
                    </div>
                    <div class="side-right"><label style="display:block">Postal Code</label>
                    <input type="text" class="input-text" name="postalcode" value="<?php echo $postalcode; ?>"></div>
                    <div class="clear"></div>
                </li>
                <li>
                    <div class="side-left"><label style="display:block">Hotel Chain</label>
                    	<select name="chain" class="input-select">
                        	<option value="">- No Chain -</option>
                            <?php
                                try {
                                    $stmt = $db->query("select * from chain");
                                    $r_type = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                    foreach($r_type as $row){
                                        if($chainoid == $row['chainoid']){ $selected = "selected"; }else{ $selected=""; }
                                        echo"<option value='".$row['chainoid']."' ".$selected.">".$row['name']."</option>";
                                    }
                                }catch(PDOException $ex) {
                                    echo "Invalid Query";
                                    die();
                                }
                            ?>
                        </select>
                    </div>
                    <div class="side-right"><label style="display:block">Phone</label>
                    <input type="text" class="input-text" name="phone" value="<?php echo $hotelphone; ?>"></div>
                    <div class="clear"></div>
                </li>
                <li>
                    <div class="side-left"><label style="display:block">Street Address</label>
                    <textarea name="address"><?php echo $address; ?></textarea></div>
                    <div class="side-right"><label style="display:block">Fax</label>
                    <input type="text" class="input-text" name="fax" value="<?php echo $hotelfax; ?>"></div>
                    <div class="clear"></div>
                </li>
                <li>
                    <div class="side-left"><label style="display:block">Email</label>
                    <input type="text" class="input-text" name="email" value="<?php echo $hotelemail; ?>"></div>
                    <div class="side-right"><label style="display:block">Website</label>
                    <input type="text" class="input-text" name="website" value="<?php echo $hotelwebsite; ?>" ></div>
                    <div class="clear"></div>
                </li>
                <li>
                    <div class="side-left"><label style="display:block">Property URL</label>
                    <input type="text" class="input-text" name="htl_url" value="<?php echo $hotelhtlurl; ?>" ></div>
                    <div class="side-right"><label style="display:block">Favicon</label>
                    <input type="file" name="favicon" /></div>
                    <div class="clear"></div>
                </li>
                <li>
                	<?php if(empty($hotelbeurl)){ $hotelbeurl = 'https://thebuking.com/ibe/index.php?hcode='.$hotelcode;} ?>
                    <div class="side-left"><label style="display:block">Booking Engine URL</label>
                    <input type="text" class="input-text" name="be_url" value="<?php echo $hotelbeurl; ?>" disabled="disabled"></div>
                    <div class="side-right"><label style="display:block">Start Hotel Price</label>
                    <input type="text" class="input-text" name="starthotelprice" value="<?php echo $starthotelprice; ?>"></div>
                    <div class="clear"></div>
                </li>
                <li>
                    <div class="side-left"><label style="display:block">Property Description</label>
                    <textarea name="description"><?php echo $description; ?></textarea></div>
                    <div class="side-right"><label style="display:block">Property General Policy</label>
                    <textarea name="policy"><?php echo $policy; ?></textarea></div>
                    <div class="clear"></div>
                </li>
                <li>
                  <?php
                  // $socialmedia_url_exploded = preg_split("/\\r\\n|\\r|\\n/", $socialmedia_url);
                  // echo "<pre>";
                  // print_r($socialmedia_url_exploded);
                  // echo "</pre>";
                  ?>
                    <div class="side-left"><label style="display:block">Social Media Links</label>
                    <textarea name="socialmedia_url" style="height: 100px" placeholder="delimited by enter to separate the links"><?php echo $socialmedia_url; ?></textarea>
                    <br><i>*delimited by enter to separate the links</i>
                    </div>
                    <div class="side-right">
                      <label style="display:block">Facebook Like Script</label>
                      <textarea name="fblike_script" style="height: 100px" placeholder="paste the script here"><?php echo $fblike_script; ?></textarea></div>
                    <div class="clear"></div>
                </li>
                <div class="clear"></div>
            </ul>
        	</div>
            <div class="box-footer" align="center" style="padding-right: 10%;padding-top: 15px;">
				<button type="submit" class="small-button blue">Save</button>
                <button type="reset" class="small-button blue">Reset</button>
            </div>
   		</div>
        </form>
    </div>
    <div class="row">
		<?php
            $d_set = array();
            try {
                $stmt = $db->query("SELECT `hoteloid`, `infantageuntil`, `childagefrom`, `childageuntil`, `minguestage`, `extrabedagefrom`, `childstayfree`, `smsbookingnotif`, `ycsbookingemail`, `unacknowledgebookinglistemail`, `checkintomorrowbookinglistemail` FROM `hotelsettings` WHERE `hoteloid`='".$hoteloid."'");
                $row_count = $stmt->rowCount();
                if($row_count > 0) {
                    $r_hotel = $stmt->fetchAll(PDO::FETCH_ASSOC);
                    foreach($r_hotel as $row){
                        $d_set['infantageuntil'] = $row['infantageuntil'];
                        $d_set['childagefrom'] = $row['childagefrom'];
                        $d_set['childageuntil'] = $row['childageuntil'];
                        $d_set['minguestage'] = $row['minguestage'];
                        $d_set['extrabedagefrom'] = $row['extrabedagefrom'];
                        $d_set['childstayfree'] = $row['childstayfree'];
                        $d_set['smsbookingnotif'] = $row['smsbookingnotif'];
                        $d_set['ycsbookingemail'] = $row['ycsbookingemail'];
                        $d_set['unacknowledgebookinglistemail'] = $row['unacknowledgebookinglistemail'];
                        $d_set['checkintomorrowbookinglistemail'] = $row['checkintomorrowbookinglistemail'];
                    }
                }
            }catch(PDOException $ex) {
                echo "Invalid Query";
                die();
            }
        ?>
        <form method="post" action="#" id="data-age-policy">
        <input type="hidden" name="hoteloid" value="<?php echo $hoteloid; ?>" />
        <div class="box box-form">
            <div class="box-header with-border">
                <h1>Hotel Age Policy</h1>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <br>
                    <div class="form-group">
                        <label>Infant Age Until</label> &nbsp;
                        <select name="infantageuntil">
                            <?php
                            for($x=0; $x<=5; $x++){
								if($d_set['infantageuntil']==$x){
                                	echo "<option value='".$x."' selected='selected'>$x</option>";
								}else{
                                	echo "<option value='".$x."'>$x</option>";
								}
                            }
                            ?>
                        </select> &nbsp; Years Old
                    </div>
                    <script>
                        $(function(){
                            $("select[name='infantageuntil']").change(function(){
                                var x = parseInt($(this).val()) + 1; console.log(x);
                                $(".childagefrom").html(x);
                                $('input[name="childagefrom"]').val(x);
                            });
                        });
                    </script>
                    <div class="form-group">
                        <label>Children Age From</label> &nbsp; <span class="childagefrom"><?=$d_set['childagefrom']?></span>
                        <input type="hidden" name="childagefrom" placeholder="" value="<?=$d_set['childagefrom']?>"> &nbsp; Until &nbsp;
                        <select name="childageuntil">
                            <?php
                            for($x=0; $x<=21; $x++){
								if($d_set['childageuntil']==$x){
                                	echo "<option value='".$x."' selected='selected'>$x</option>";
								}else{
                                	echo "<option value='".$x."'>$x</option>";
								}
                            }
                            ?>
                        </select> &nbsp; Years Old
                    </div>
                    <div class="form-group">
                        <label>Minimum Guest Age For Stay (set to 0 if all ages allow)</label> &nbsp;
                        <select name="minguestage">
                            <?php
                            for($x=0; $x<=21; $x++){
								if($d_set['minguestage']==$x){
                               		echo "<option value='".$x."' selected='selected'>$x</option>";
								}else{
                                	echo "<option value='".$x."'>$x</option>";
								}
                            }
                            ?>
                        </select> &nbsp; Years Old
                    </div>
                    <div class="form-group">
                        <label>Extra Bed Required From</label> &nbsp;
                        <select name="extrabedagefrom">
                            <?php
                            for($x=0; $x<=21; $x++){
								if($d_set['extrabedagefrom']==$x){
                                	echo "<option value='".$x."' selected='selected'>$x</option>";
								}else{
                                	echo "<option value='".$x."'>$x</option>";
								}
                            }
                            ?>
                        </select> &nbsp; Years Old
                        <br><input type="checkbox" name="childstayfree" value="1" <?php if($d_set['childstayfree']=="1"){echo "checked='checked'";}?>> &nbsp; Children Stay Free on Existing Bedding
                    </div>
                </div>
            </div>
            <div class="box-footer" align="right">
				<button type="button" class="small-button blue add-button">Save</button>
                <button type="reset" class="small-button blue">Reset</button>
            </div>
        </div>
        </form>
    </div>
    <div class="row">
        <?php if($_SESSION['_typeusr']==1 or $_SESSION['_typeusr']==2){$dis = "";}else{$dis = "disabled";}?>
        <form method="post" action="#" id="data-commission" ds=<?php echo $dis;?> >
        <input type="hidden" name="hoteloid" value="<?php echo $hoteloid; ?>" />
        <div class="box box-form">
            <div class="box-header with-border">
                <h1>Hotel Commission</h1>
            </div>
            <div class="box-body">
                <div class="form-group">
                	<br />
                    <table class="table table-fill table-fill-centered">
                        <tr>
                            <td>Start Date</td>
                            <td>End Date</td>
                            <td>Commission Value</td>
                            <td>Priority</td>
                            <td>Type</td>
                            <td>Action</td>
                        </tr>
                        <tr>
                            <td><input type="text" class="medium startdate" name="newstartdate"/></td>
                            <td><input type="text" class="medium enddate" name="newenddate"/></td>
                            <td><input type="text" class="medium" name="newvalue"/></td>
                            <td><input type="text" class="medium" name="newpriority"/></td>
                            <td><select class="medium" name="newtype">
                            	<option value="override">Override</option>
                                <?php if($_SESSION['_typeusr']==1 or $_SESSION['_typeusr']==2){ ?>
                            	<option value="private_sales" <?php echo $dis;?> >Private Sales</option>
                            	<option value="base" <?php echo $dis;?> >Base Commission</option>
                                <?php } ?>
                            </select></td>
                            <td><button type="button" class="small-button blue add-email">Add</button></td>
						</tr>
                    <?php
					try {
						$stmt = $db->query("select * from hotelcommission where hoteloid = '".$hoteloid."' order by commissionoid");
						$r_mail = $stmt->fetchAll(PDO::FETCH_ASSOC);
						foreach($r_mail as $row){
							if($row['type']!='override' && $dis == "disabled") $xdis = "disabled";
								else $xdis = "";
							if(empty($row['startdate']) or $row['startdate']=="0000-00-00") $row['startdate'] = "";
								else $row['startdate'] = date('d F Y',strtotime($row['startdate']));
							if(empty($row['enddate']) or $row['enddate']=="0000-00-00") $row['enddate'] = "";
								else $row['enddate'] = date('d F Y',strtotime($row['enddate']));

                            //check type user
                            if($_SESSION['_typeusr']==1 or $_SESSION['_typeusr']==2 or $row['type']=='override'){
							echo"
							<tr x='data'>
								<td>
									<input type='hidden' name='id[]' value='".$row['commissionoid']."' ".$xdis.">
									<input type='text' class='medium startdate' name='startdate[]' value='".$row['startdate']."' ".$xdis.">
								</td>
								<td><input type='text' class='medium enddate' name='enddate[]' value='".$row['enddate']."' ".$xdis."></td>
								<td><input type='text' class='medium' name='value[]' value='".$row['value']."' ".$xdis."></td>
								<td><input type='text' class='medium' name='priority[]' value='".$row['priority']."' ".$xdis."></td>
								<td>";
							$s1='';$s2='';$s3='';
							if($row['type']=='override') $s1="selected";
							elseif($row['type']=='private_sales') $s2="selected";
							elseif($row['type']=='base') $s3="selected";
							echo'<select class="medium" name="type[]" '.$xdis.'>
									<option value="override" '.$s1.'>Override</option>';
                                    if($_SESSION['_typeusr']==1 or $_SESSION['_typeusr']==2){
									echo'<option value="private_sales" '.$s2.' '.$dis.'>Private Sales</option>
									<option value="base" '.$s3.' '.$dis.'>Base Commission</option>';
                                    }
							echo'</select>';
							echo"</td>
								<td>&nbsp;</td>
							</tr>
							";
                            }else{
                            echo"
							<tr x='data'>
								<td><span>".$row['startdate']."</span></td>
								<td><span>".$row['enddate']."</span></td>
								<td><span>".$row['value']."</span></td>
								<td><span>".$row['priority']."</span></td>";
							$ss='';
							if($row['type']=='override') $ss="Override";
							elseif($row['type']=='private_sales') $ss="Private Sales";
							elseif($row['type']=='base') $ss="Base Commission";
							echo "<td><span>".$ss."</span></td>
								<td>&nbsp;</td>
							</tr>
							";
                            }
						}
					}catch(PDOException $ex) {
						echo "Invalid Query";
						die();
					}
					?>
					</table>
                </div>
            </div>
            <div class="box-footer" align="right">
				<button type="button" class="small-button blue add-button">Save</button>
                <button type="reset" class="small-button blue">Reset</button>
            </div>
        </div>
        </form>
    </div>
</section>
