<?php
	include("../../../conf/connection.php");
	
	$hoteloid = $_POST['hoteloid'];
	$name = $_POST['name'];
	$hoteltype = $_POST['hoteltype'];
	$star = $_POST['star'];
	$address = $_POST['address'];
	$email = $_POST['email'];
	
	try {
		$stmt = $db->prepare("UPDATE hotel SET hotelname=:a , hoteltypeoid=:b , stars=:c , address=:d, email=:e WHERE hoteloid=:hoid");
		$stmt->execute(array(':a' => $name, ':b' => $hoteltype, ':c' => $star, ':d' => $address, ':e' => $email, ':hoid' => $hoteloid));
		$affected_rows = $stmt->rowCount();
	}catch(PDOException $ex) {
		echo "0";
		die();
	}
	echo "1";
?>