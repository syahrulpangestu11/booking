<link rel="stylesheet" href="<?php echo $base_url; ?>/lib/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo $base_url; ?>/lib/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<link rel="stylesheet" href="<?php echo $base_url; ?>/lib/admin-lte/css/AdminLTE.min.css">
<link href="<?php echo $base_url?>/css/style.css?v=1" rel="stylesheet" type="text/css">
<script src="<?php echo $base_url; ?>/lib/bootstrap/js/bootstrap.min.js"></script>
		<style type="text/css">
			h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 { 
				font-family: inherit; 
				font-weight: 600;
				line-height: inherit;
				color: inherit;
				margin-bottom:10px!important;
			}
			.wrapper {
				position: inherit;
				overflow: hidden!important;
			}
			.left-side {
				padding-top: inherit;
			}
			.sidebar > .sidebar-menu li > a:hover {
				background-color: rgba(72, 115, 175, 0.26);
			}
			.sidebar .sidebar-menu .treeview-menu {
				background-color: rgb(14, 26, 43);
			}
			.sidebar .sidebar-menu .treeview-menu .treeview-menu-child {
				background-color: rgba(0, 0, 0, 0.5);
			}
			.sidebar > .sidebar-menu li.active > a {
				background-color: rgba(197, 45, 47, 0.55);
			}
			.sidebar > .sidebar-menu > li.treeview.active > a {
				background-color: inherit;
			}
			.sidebar > .sidebar-menu > li.treeview.active li.treeview-child.active > a {
				background-color: inherit;
			}
			.sidebar .sidebar-menu > li > a > .fa {
				width: 28px;
				font-size: 16px;
			}
			.sidebar-menu, .main-sidebar .user-panel, .sidebar-menu>li.header{
				white-space:normal!important;
			}
			.form-group input[type=text]{
				width:100%!important;
			}
		</style>

<section class="content-header">
    <h1>Add New Loyalty Member Program</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-handshake-o"></i> Customer Relationship Management</a></li>
        <li class="active">Add New Campaign Loyalty Member Program</li>
    </ol>
</section>
<section class="content">
    <form class="form-box" method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/promotions/add-process">
    <div class="box box-form" id="step">
        <div class="row">
            <div class="col-md-12"><h1>Campaign Loyalty Member Detail</h1></div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group col-md-12">
                    <label>Campaign Name</label>
                    <input type="text" class="form-control" name="name" required>
                </div>
                <div class="form-group col-md-12">
                    <label>Description</label>
                    <textarea class="form-control" name="description"></textarea>
                </div>
                <div class="form-group col-md-6">
                    <label>Publish Campaign</label>
                    <select name="published" class="form-control">
                        <?php
                            try {
                                $stmt = $db->query("select * from published where showthis = 'y'");
                                $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                foreach($r_room as $row){
                                    if($row['publishedoid'] == '1'){ $selected = "selected"; }else{ $selected=""; }
                                    echo"<option value='".$row['publishedoid']."' ".$selected.">".$row['note']."</option>";
                                }
                            }catch(PDOException $ex) {
                                echo "Invalid Query";
                                die();
                            }
                        ?>        	
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group col-md-6">
                    <label>Start Campaign Periode</label>
                    <div class="input-group"><div class="input-group-addon"><i class="fa fa-calendar"></i></div><input type="text" class="form-control" name="startdate" required value="<?=date('d F Y')?>"></div>
                </div>
                <div class="form-group col-md-6">
                    <label>End Campaign Periode</label>
                    <div class="input-group"><div class="input-group-addon"><i class="fa fa-calendar"></i></div><input type="text" class="form-control" name="enddate" required value="<?=date('d F Y', strtotime('+3 month'))?>"></div>
                </div>
                <div class="form-group col-md-8">
                    <label>Conversion Point</label>
                    <div class="input-group"><div class="input-group-addon">IDR</div><input type="number" class="form-control" name="conversionpoint" required><div class="input-group-addon">/ 1 point</div></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-12 text-right">
                <button type="button" class="small-button blue submit-add">Save Detail &amp; Continue Assign Hotel <i class="fa fa-arrow-right"></i></button>
            </div>
        </div>
    </div>
        
    <div class="box box-form" id="step-2" style="display:none;">
        <div class="row">
            <div class="col-md-6"><h1>Apply Campaign To Hotel</h1></div>
             <div class="col-md-6 text-right"><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#hotelListModal" disabled="disabled"><i class="fa fa-plus"></i> Assign New Hotel</button></div>
        </div>
    </div>
    </form>
</section>

<!-- Modal -->
<div class="modal fade" id="hotelListModal" tabindex="-1" role="dialog" aria-labelledby="hotelListModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus-circle"></i> Add New Hotel</h4>
      </div>
      <div class="modal-body">
        <form method="post" class="form-inline" enctype="multipart/form-data" action="#">
          <div class="form-group">
            <select name="chain" class="form-control">
            <option value="">Show All Hotel Chain</option>
            <?php
            try {
              $stmt = $db->query("select * from chain where publishedoid = '1'");
              $r_chain = $stmt->fetchAll(PDO::FETCH_ASSOC);
              foreach($r_chain as $row){ echo"<option value='".$row['chainoid']."'>".$row['name']."</option>"; }
            }catch(PDOException $ex) {
              echo "Invalid Query"; die();
            }
            ?>        	
            </select>
          </div>
          <div class="form-group"><input type="text" class="form-control" name="name" placeholder="Hotel Name"></div>
          <div class="form-group"><button type="button" class="btn btn-info" id="find">Find</button></div>
        </form>
        <div class="row">
          <div id="list"></div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
