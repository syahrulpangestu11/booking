            <?php
			try {
				$stmt = $db->query("select hotelcode, chainoid from hotel where hoteloid = '".$hoteloid."'");
				$row_count = $stmt->rowCount();
				
				$r_hotel = $stmt->fetchAll(PDO::FETCH_ASSOC);
				foreach($r_hotel as $row){ $hcode = $row['hotelcode']; $hchain = $row['chainoid']; }
			}catch(PDOException $ex) {
				echo "Invalid Query"; die();
			}
			
            $s_existed_hotel_feature = "SELECT GROUP_CONCAT(featureoid) as existed from `hotelfeature` where `hoteloid` = '".$hoteloid."'";
			$stmt = $db->query($s_existed_hotel_feature);
			$r_existed_hotel_feature = $stmt->fetch(PDO::FETCH_ASSOC);
			if(!empty($r_existed_hotel_feature['existed'])){
				$subscribe_feature = explode(',', $r_existed_hotel_feature['existed']);
			}else{
				$subscribe_feature = array();
			}
			?>
           
<li <?php if($uri2=="dashboard"){ echo "class=active"; }?>>
    <a href="<?=$base_url?>/dashboard"><i class="fa fa-home"></i> <span>Dashboard</span></a>
</li>

<?php
    $activemenu = array("availability", "room-control", "rate-control", "promotions", "load-tarif", "import-external", "extra", "promocode");
	if(in_array($uri2, $activemenu)){ $active = "active"; }else{ $active = ""; }
?>
<li class="treeview <?=$active?>">
    <a href="#">
        <i class="fa fa-list"></i> <span>Allotments &amp; Rates</span>
        <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
        <li <?php if($uri2=="availability"){ echo "class=active"; }?>><a href="<?=$base_url?>/availability"><i class="fa fa-inbox"></i> Availability</a></li>
        <li <?php if($uri2=="load-tarif" or $uri2=="import-external"){ echo "class=active"; }?>><a href="<?=$base_url?>/load-tarif"><i class="fa fa-cloud-upload"></i> Load Rates</a></li>
        <li <?php if( $uri2=="room-control"){ echo "class=active"; }?>><a href="<?=$base_url?>/room-control"><i class="fa fa-circle-o"></i> Room Control</a></li>
        <li <?php if($uri2=="rate-control"){ echo "class=active"; }?>><a href="<?=$base_url?>/rate-control"><i class="fa fa-circle-o"></i> Rate Control</a></li>
        <li <?php if($uri2=="promotions"){ echo "class=active"; }?>><a href="<?=$base_url?>/promotions"><i class="fa fa-gift"></i> Promotions</a></li>
        <li <?php if($uri2=="extra"){ echo "class=active"; }?>><a href="<?=$base_url?>/extra"><i class="fa fa-cube"></i> Extras</a></li>
        <li <?php if($uri2=="promocode"){ echo "class=active"; }?>><a href="<?=$base_url?>/promocode"><i class="fa fa-barcode"></i> Promo Code</a></li>
    </ul>
</li>

<li class="treeview <?php if($uri2=="hotel-profile"){ echo "active"; }?>">
    <a href="<?=$base_url?>/hotel-profile">
        <i class="fa fa-building"></i> <span> Hotel Profile</span>
        <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
        <li <?php if($uri3=="basic-info"){ echo "class=active"; }?>><a href="<?=$base_url?>/hotel-profile/basic-info"><i class="fa fa-info-circle"></i> Basic Info</a></li>
        <li <?php if($uri3=="maps"){ echo "class=active"; }?>><a href="<?=$base_url?>/hotel-profile/maps"><i class="fa fa-map-marker"></i> Maps</a></li>
        <li <?php if($uri3=="facilities"){ echo "class=active"; }?>><a href="<?=$base_url?>/hotel-profile/facilities"><i class="fa fa-heart"></i> Facilities</a></li>
        <li <?php if($uri3=="photo"){ echo "class=active"; }?>><a href="<?=$base_url?>/hotel-profile/photo"><i class="fa fa-photo"></i> Photo</a></li>
        <li <?php if($uri3=="rooms"){ echo "class=active"; }?>><a href="<?=$base_url?>/hotel-profile/rooms"><i class="fa fa-bed"></i> Room Type</a></li>
    </ul>
</li>

<?php
    $activemenu = array("room-settings", "tax-settings", "hotel-ranking", "cancellation-policy", "surcharge", "contact-management", "booking-enginee-appearance");
	if(in_array($uri2, $activemenu)){ $active = "active"; }else{ $active = ""; }
?>
<li class="treeview <?=$active?>">
    <a href="#">
        <i class="fa fa-cog"></i> <span> Settings</span>
        <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
        <li <?php if($uri2=="booking-enginee-appearance"){ echo "class=active"; }?>><a href="<?=$base_url?>/booking-enginee-appearance"><i class="fa fa-link"></i> Booking Enginee Appearance</a></li>
        <?php /*?><li <?php if($uri2=="hotel-settings"){ echo "class=active"; }?>><a href="<?=$base_url?>/hotel-settings"><i class="fa fa-circle-o"></i> Hotel Settings</a></li><?php */?>
        <li <?php if($uri2=="room-settings"){ echo "class=active"; }?> style="display:none;"><a href="<?=$base_url?>/room-settings"><i class="fa fa-circle-o"></i> Room Settings</a></li>
        <li <?php if($uri2=="cancellation-policy"){ echo "class=active"; }?>><a href="<?=$base_url?>/cancellation-policy"><i class="fa fa-times"></i> Cancellation Policy</a></li>
        <li <?php if($uri2=="surcharge"){ echo "class=active"; }?>><a href="<?=$base_url?>/surcharge"><i class="fa fa-dollar"></i> Surcharge</a></li>
        <li <?php if($uri2=="contact-management"){ echo "class=active"; }?>><a href="<?=$base_url?>/contact-management"><i class="fa fa-user"></i> Contact Management</a></li>
        <?php /*?><li <?php if($uri2=="tax-settings"){ echo "class=active"; }?>><a href="<?=$base_url?>/tax-settings"><i class="fa fa-circle-o"></i> Tax Settings</a></li>
        <li><a href="#"><i class="fa fa-circle-o"></i> Hotel Competitor Star</a></li>
        <li <?php if($uri2=="hotel-ranking"){ echo "class=active"; }?>><a href="<?=$base_url?>/hotel-ranking"><i class="fa fa-line-chart"></i> Hotel Ranking</a></li>
        <li <?php if($uri2=="import-external"){ echo "class=active"; }?>><a href="<?=$base_url?>/import-external"><i class="fa fa-circle-o"></i> Import External Data</a></li><?php */?>
    </ul>
</li>

<?php if(in_array('1', $subscribe_feature)){ 
    $activemenu = array("loyalty-member-program", "member", "email-template");
	if(in_array($uri3, $activemenu)){ $active = "active"; }else{ $active = ""; }
?>
<li class="treeview <?=$active?>">
	<a href="#"><i class="fa fa-handshake-o"></i><span>Customer Relation</span><i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
        <li <?php if($uri3=="loyalty-member-program"){ echo "class=active"; }?>><a href="<?=$base_url?>/crm/loyalty-member-program"><i class="fa fa-circle-o"></i> Loyalty Member Program</a></li>
        <li <?php if($uri3=="member"){ echo "class=active"; }?>><a href="<?=$base_url?>/crm/member"><i class="fa fa-users"></i> Member</a></li>
        <li <?php if($uri3=="email-template"){ echo "class=active"; }?> style="display:none;"><a href="<?=$base_url?>/crm/email-template"><i class="fa fa-envelope-o"></i> Email Template</a></li>
	</ul>
</li>
<?php } ?>

<li <?php if($uri2=="agent"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/agent"><i class="fa fa-user-o"></i> Offline Agent</a></li>

<?php
    $activemenu = array("booking", "performance-reports");
	if(in_array($uri2, $activemenu)){ $active = "active"; }else{ $active = ""; }
?>
<li class="treeview <?=$active?>">
    <a href="#">
        <i class="fa fa-bar-chart-o"></i> <span>Reports</span>
        <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
        <li class="<?php if($uri2=="booking"){ echo "active"; }?>"><a href="<?php echo "$base_url"; ?>/booking"><i class="fa fa-circle-o"></i> Bookings</a></li>
        <li class="<?php if($uri2=="performance-reports"){ echo "active"; }?> treeview-child">
            <a href="#">
                <i class="fa fa-circle-o"></i> <span> Performance Reports</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu-child">
                <li <?php if($uri3=="reservation-report"){ echo "class=active"; }?>><a href="<?=$base_url?>/performance-reports/reservation-report"><i class="fa fa-circle-o"></i> Reservation Report</a></li>
                <li><a href="<?=$base_url?>/performance-reports"><i class="fa fa-circle-o"></i> By Room Night</a></li>
                <li><a href="<?=$base_url?>/performance-reports"><i class="fa fa-circle-o"></i> Promotion Revenue</a></li>
                <li><a href="<?=$base_url?>/performance-reports"><i class="fa fa-circle-o"></i> By Room Type</a></li>
                <li><a href="<?=$base_url?>/performance-reports"><i class="fa fa-circle-o"></i> Promotion Count</a></li>
            </ul>
        </li>
        <li><a href=""><i class="fa fa-circle-o"></i> System Log</a></li>
    </ul>
</li>

<li <?php if($uri2=="tracking-analytics"){ echo "class=active"; }?>><a href="<?=$base_url?>/tracking-analytics"><i class="fa fa-area-chart"></i> Tracking Analytics</a></li>

<?php if ($_SESSION['_typeusr'] != "3"){ ?>
<li><a class="manage"><i class="fa fa-dashboard"></i><span>Dashboard Super Admin</span></a> </li>
<?php } ?>


            <script type="text/javascript">
			$(function(){
				$('a.manage').click(function(){
					var loginas_hc = '0';
					var loginas_hname = '';
					$.ajax({  
						type	: 'POST', cache: false,
						url		: '<?=$base_url?>/includes/suadm/hotel/change-session.php', 
						data	: { loginas : loginas_hc, hname : loginas_hname },
						success	: function(rsp){ if(rsp == "1"){ $(location).attr("href", "<?=$base_url?>/dashboard"); } } 
					});
				});		

			});
            </script>
