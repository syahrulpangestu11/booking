<style>
#data-box .side-left label, #data-box .side-right label {
    padding-bottom: 5px;
}
</style>
<section class="content-header">
    <h1>Detail Member</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-handshake-o"></i> Customer Relationship Management</a></li>
        <li class="active">Detail Member</li>
    </ol>
</section>
<?php
$id = $_REQUEST['for'];

try {
    $main_query = "select m.*, c.countryname, mlp.accumulatepoint, mlp.point, (mlp.accumulatepoint-mlp.point) as redeem, mh.publishedoid, p.note as published_note, lpm.name as lpmname, mh.memberhoteloid 
        from member m inner join memberhotel mh using (memberoid) 
            left join country c using (countryoid)
            left join memberloyaltyprogram mlp using (memberoid)
            left join loyaltyprogram lp on (lp.loyaltyprogramoid=mlp.loyaltyprogramoid and lp.hoteloid = '".$hoteloid."')
            left join published p on (mh.publishedoid=p.publishedoid)
            left join loyaltyprogrammembership lpm using (loyaltyprogrammembershipoid)
        where mh.hoteloid = '".$hoteloid."' and mh.memberhoteloid = '".$id."' and mh.publishedoid not in (3)";

    $stmt = $db->query($main_query);
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
?>
<section class="content" id="basic-info">
    <div class="box box-form">
        <div class="box-header with-border">
            <h1>Membership Info</h1>
        </div>
        <div id="data-box" class="box-body">
        <ul class="inline form-input">
            <li>
                <div class="side-left">
                    <label style="display:block">ID Membership:</label>
                    <input type="text" class="input-text" value="<?=$row['membercode']?>" readonly>
                </div>
                <div class="side-right">
                    <label style="display:block">Email Address:</label>
                    <input type="text" class="input-text" value="<?=$row['email']?>" readonly>
                </div>
                <div class="clear"></div>
            </li>
            <li>
                <div class="side-left">
                    <label style="display:block">Title:</label>
                    <input type="text" class="input-text" value="<?=$row['title']?>" readonly>
                </div>
                <div class="side-right">
                    <label style="display:block">Phone Number:</label>
                    <input type="text" class="input-text" value="<?=$row['phone']?>" readonly>
                </div>
                <div class="clear"></div>
            </li>
            <li>
                <div class="side-left">
                    <label style="display:block">Firstname:</label>
                    <input type="text" class="input-text" value="<?=$row['firstname']?>" readonly>
                </div>
                <div class="side-right">
                    <label style="display:block">Mobile:</label>
                    <input type="text" class="input-text" value="<?=$row['mobile']?>" readonly>
                </div>
                <div class="clear"></div>
            </li>
            <li>
                <div class="side-left">
                    <label style="display:block">Lastname:</label>
                    <input type="text" class="input-text" value="<?=$row['lastname']?>" readonly>
                </div>
                <div class="side-right">
                    <label style="display:block">City:</label>
                    <input type="text" class="input-text" value="<?=$row['city']?>" readonly>
                </div>
                <div class="clear"></div>
            </li>
            <li>
                <div class="side-left">
                    <label style="display:block">Join Date:</label>
                    <input type="text" class="input-text" value="<?=date('d F Y', strtotime($row['joindate']))?>" readonly>
                </div>
                <div class="side-right">
                    <label style="display:block">Country:</label>
                    <input type="text" class="input-text" value="<?=$row['countryname']?>" readonly>
                </div>
                <div class="clear"></div>
            </li>
            <div class="clear"></div>
        </ul>
        </div>
        <div class="box-header with-border">
            <h1>Membership Point</h1>
        </div>
        <div id="data-box" class="box-body">
        <ul class="inline form-input">
            <li>
                <div class="side-left">
                    <label style="display:block">Type Membership:</label>
                    <input type="text" class="input-text" value="<?=$row['lpmname']?>" readonly>
                </div>
                <div class="side-right">
                    <label style="display:block">Total Earned:</label>
                    <input type="text" class="input-text" value="<?=$row['accumulatepoint']?>" readonly>
                </div>
                <div class="clear"></div>
            </li>
            <li>
                <div class="side-left">
                    <label style="display:block">Balance Point:</label>
                    <input type="text" class="input-text" value="<?=$row['point']?>" readonly>
                </div>
                <div class="side-right">
                    <label style="display:block">Total Redeem:</label>
                    <input type="text" class="input-text" value="<?=$row['redeem']?>" readonly>
                </div>
                <div class="clear"></div>
            </li>
        </ul>
        </div>
        <div class="box-footer" align="center" style="padding-right: 10%;padding-top: 15px; display:none">
            <button type="submit" class="small-button blue">Save</button>
            <button type="reset" class="small-button blue">Reset</button>
        </div>
    </div>
</section>
<?php
}catch(PDOException $ex) {
    echo "Invalid Query";
    die();
}
?>