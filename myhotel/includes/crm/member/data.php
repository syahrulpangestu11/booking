<?php
	error_reporting(E_ALL & ~E_NOTICE);
	include("../../../conf/connection.php");
	include("../../paging/pre-paging.php");
	
	$hoteloid = $_REQUEST['hoteloid'];
	
	$main_query = "select m.*, c.countryname, mlp.point, mh.publishedoid, p.note as published_note, lpm.name as lpmname, mh.memberhoteloid 
		from member m inner join memberhotel mh using (memberoid) 
			left join country c using (countryoid)
			left join memberloyaltyprogram mlp using (memberoid)
			left join loyaltyprogram lp on (lp.loyaltyprogramoid=mlp.loyaltyprogramoid and lp.hoteloid = '".$hoteloid."')
			left join published p on (mh.publishedoid=p.publishedoid)
			left join loyaltyprogrammembership lpm using (loyaltyprogrammembershipoid)
		where mh.hoteloid = '".$hoteloid."' and mh.publishedoid not in (3)";
	
	$filter = array();
	if(isset($_REQUEST['name']) and $_REQUEST['name']!=''){
		array_push($filter, 'm.membername like "%'.$_REQUEST['name'].'%"');
	}
	if(isset($_REQUEST['email']) and $_REQUEST['email']!=''){
		array_push($filter, 'm.email like "%'.$_REQUEST['email'].'%"');
	}
	
	if(count($filter) > 0){
		$combine_filter = implode(' and ',$filter);
		$query = $main_query.' where '.$combine_filter;
	}else{
		$query = $main_query;
	}

	try {
		$main_query = $query." order by m.joindate desc";
		$stmt = $db->query($main_query.$paging_query);
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
?>
<table class="table table-fill">
    <tr align="left">
        <td>Name</td>
        <td>E-mail</td>
        <td>Mobile</td>
        <td>Country</td>
        <td>City</td>
        <td>Point</td>
        <td>Membership</td>
        <td>Join Date</td>
        <td>&nbsp; </td>
    </tr>	
<?php
	$r_promo = $stmt->fetchAll(PDO::FETCH_ASSOC);
	foreach($r_promo as $row){
		if($row['publishedoid']=="1"){
			$published_class = " blue ";
			$published_note = $row['published_note'];
		}else{ 
			$published_class = " grey " ;
			$published_note = $row['published_note'];
		}
?>
    <tr>
        <td><?php echo $row['firstname']." ".$row['lastname']; ?></td>
        <td><?php echo $row['emailmailing']; ?></td>
        <td><?php echo (!empty($row['mobile']) ? $row['mobile'] : $row['phone']); ?></td>
        <td><?php echo $row['countryname']; ?></td>
        <td><?php echo $row['city']; ?></td>
        <td><?php echo (!empty($row['point']) ? $row['point'] : 0); ?></td>
        <td><?php echo $row['lpmname']; ?></td>
        <td><?php echo date('d/M/Y', strtotime($row['joindate'])); ?></td>
        <td class="algn-right">
			<button type="button" class="pure-button single change-published <?=$published_class;?>" id="<?=$row['memberhoteloid']?>" current-publishedoid="<?=$row['publishedoid'];?>" title="Click to Change Status"><?=$published_note;?></button>
			<button type="button" class="pure-button blue edit-member" id="<?=$row['memberhoteloid']?>" style="margin:auto 3px;"><i class="fa fa-pencil" style="margin:0"></i></button>
			<button type="button" class="pure-button red remove-member" id="<?=$row['memberhoteloid']?>" style="margin:auto 3px;"><i class="fa fa-trash" style="margin:0"></i></button>
		</td>
    </tr>	
<?php				
	}
?>
</table>
<?php
		}

		// --- PAGINATION part 2 ---
		$main_count_query = "select count(*) from member m inner join memberhotel mh using (memberoid) where mh.hoteloid = '".$hoteloid."'";
		if(count($filter) > 0){
			$combine_filter = implode(' and ',$filter);
			$main_count_query = $main_count_query.' where '.$combine_filter;
		}
		$stmt = $db->query($main_count_query);
		$jmldata = $stmt->fetchColumn();

		$tampildata = $row_count;
		include("../../paging/post-paging.php");

	}catch(PDOException $ex) {
		echo "Invalid Query";
		print($ex);
		die();
	}
?>
