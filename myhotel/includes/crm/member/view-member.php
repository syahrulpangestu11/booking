 <?php
	$name = $_POST['name'];
	$email = $_POST['email'];
	
	include("js.php");
?>
<section class="content-header">
    <h1>List Member</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-handshake-o"></i> Customer Relationship Management</a></li>
        <li class="active">List Member</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="box">
            <div class="box-body">
                <a href="<?php echo $base_url; ?>/export/member"><button type="button" class="small-button blue" style="float:right;margin:auto 5px;">Export to Excel</button></a>
                <a href=""><button type="button" class="small-button green" style="float:right;margin:auto 5px;">Import Guest</button></a>
                <div class="form-group">
            	<form method="get" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/crm/member/">
            		<input type="hidden" name="hoteloid" value="<?php echo $hoteloid; ?>" />
            		<input type="hidden" name="page" value="1" />
                    <label>Member Name  :</label> &nbsp;
					<input type="text" name="name" class="input-text" value="<?php echo $name; ?>">&nbsp;
                    <label>Member Email  :</label> &nbsp;&nbsp;
					<input type="text" name="email" class="input-text" value="<?php echo $email; ?>">&nbsp;
                    <button type="submit" class="small-button blue"><i class="fa fa-search"></i>Find</button>
                </form>
                </div>
            </div><!-- /.box-body -->
       </div>
    </div>
    
    <div class="row">
        <div class="box">
        	<form method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/room-control/process">
                <div id="data-box" class="box-body">
                    <div class="loader">Loading...</div>
                </div><!-- /.box-body -->
            </form>
       </div>
    </div>

</section>
