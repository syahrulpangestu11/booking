<!-- <nav class="navbar navbar-static-top" role="navigation">
    	<div class="navbar-left">
        	<h1 id="pagename"></h1>
            <script>
			$(function(){
				$("#pagename").html( $(".content-header h1").html() );
			});
			</script>
        </div> -->

        <script>
        $(function(){

            // $( document ).ajaxStop(function() {
            $(document).ready(function(){
                
                
                $("#child-menu").remove();
                $("section.content > div").removeClass('row');
                
                <?php
                $s_menu ="SELECT mm.* FROM menumyhotel mm WHERE mm.link = '".$uri2."'";
                $q_menu = $db->query($s_menu);
                $n_menu = $q_menu->rowCount();
                $r_menu = $q_menu->fetch(PDO::FETCH_ASSOC);
                if($n_menu>0){
                    if(!empty($r_menu['parent'])){
                        $menuBreadcrumb = '<li>'.$r_menu['parent'].'</li>';
                        $menuBreadcrumb .= '<li>'.$r_menu['menu'].'</li>';
                    }else{
                        $menuBreadcrumb = '<li>'.$r_menu['menu'].'</li>';
                    }
                    $menuDescription = strip_tags($r_menu['description']);
                    $menuDescription = trim(preg_replace('/\s+/', ' ', $menuDescription));
                    ?>

                    var pageTitle = '';
                    var breadcrumb = '<?=$menuBreadcrumb;?>';
                    var pageDescription = '<?=$menuDescription;?>';
                    
                    pageTitle = '<div class="page-title box"><ul>';
                    pageTitle += breadcrumb;
                    pageTitle += '</ul>';
                    pageTitle += '<p>'+pageDescription+'</p>';
                    pageTitle += '</div>';
                    $("section.content").first().prepend(pageTitle);
                    <?php
                }
                ?>
                console.log("<?=$uri2;?>");
            });
        });
        </script>
        <div class="navbar-right">
            <ul class="nav navbar-nav">
                <?php if($_SESSION['_typeusr']=='4' and !empty($_SESSION['_hotel']) and $_SESSION['_hotel']!='0'){
                
                $main_query =
        			"SELECT h.hoteloid, h.hotelname, h.publishedoid, (case when h.publishedoid = 2 then 'status-red' when h.publishedoid = 1 then 'status-green' else 'status-black' end) as status, ht.category
        			from hotel h inner join hoteltype ht using (hoteltypeoid) inner join published using (publishedoid)
        			inner join city c using (cityoid) inner join state s using (stateoid) inner join country ct using (countryoid)
        		";
        		
        		$filter = array();
        		
        		array_push($filter, 'h.publishedoid not in (3) ');
        		
        		$s_chain = "SELECT ua.oid FROM users u INNER JOIN userassign ua USING (useroid) WHERE ua.type = 'chainoid' AND u.useroid = '".$_SESSION['_oid']."'";
        		$q_chain = $db->query($s_chain);
        		$n_chain = $q_chain->rowCount();
        		$r_chain = $q_chain->fetchAll(PDO::FETCH_ASSOC);
        		foreach($r_chain as $row){
        			array_push($filter, "h.chainoid = '".$row['oid']."'");
        		}
        		
        		if(count($filter) > 0){
            		$combine_filter = implode(' and ',$filter);
            		$query = $main_query.' where '.$combine_filter;
            	}else{
            		$query = $main_query;
            	}
            	
            	try {
            		$main_query = $query." ORDER BY h.hoteloid DESC ";
            		$stmt = $db->query($main_query);
            		$row_count = $stmt->rowCount();
            		$myarr = array();
            		if($row_count > 0) {
            		    $r_hotel = $stmt->fetchAll(PDO::FETCH_ASSOC);
            		    foreach($r_hotel as $row){
            				$hid = $row['hoteloid'];
            				$hname = $row['hotelname'];
            				
            				$myarr[$hid] = $hname;
            			}
            		}
            	}catch(PDOException $ex) {
            		echo "Invalid Query";
            		die();
            	}
                
                ?>
                
                <?php if(!empty($_SESSION['_hotelname'])){ ?>
                    <li class="button-red"><a href="<?=$base_url?>/availability"><i class="fa fa-inbox"></i> Availability</a></li>
                    <li class="button-red"><a href="<?php echo "$base_url"; ?>/booking"><i class="fa fa-circle-o"></i> Bookings</a></li>

                    <li class="button-red dropdown user user-menu">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="<?=$base_url?>/hotel-profile/basic-info">
                        <i class="fa fa-cog"></i>
                        </a>
                    </li>
                <?php } ?>

                <li class="dropdown notifications-menu">
                    <a class="dropdown-toggle" data-toggle="dropdown" title="<?=$_SESSION['_hotelname']?>">
					<?php if(!empty($_SESSION['_hotelname'])){ ?><i class="fa fa-institution"></i> <?=$_SESSION['_hotelname']?><?php } ?> <i class="caret"></i>
                    </a>
                    <ul class="dropdown-menu">
                      <li class="header">You have <?=count($myarr)?> hotel(s)</li>
                      <li>
                        <ul class="menu">
                        <?php foreach($myarr as $key => $value){
                            $mynow = ($_SESSION['_hotel'] == $key) ? 'background-color: #e4ecf3;' : '';
                        ?>
                          <li>
                            <a class="change-hotel" hoid="<?=$key?>" style="<?=$mynow?>">
                              <i class="fa fa-institution"></i> <?=$value?>
                            </a>
                          </li>
                        <?php }?>
                        </ul>
                      </li>
                    </ul>
                </li>
                
                <script>
                    $(function(){
                        $('a.change-hotel').click(function(e) {
                    		var loginas_hc = $(this).attr('hoid');
                    		var loginas_hname = $(this).text().trim();
                    		$.ajax({
                    			type	: 'POST', cache: false,
                    			url		: '<?php echo"$base_url"; ?>/includes/suadm/hotel/change-session.php',
                    			data	: { loginas : loginas_hc, hname : loginas_hname },
                    			success	: function(rsp){ if(rsp == "1"){ document.location.href = '<?php echo"$base_url"; ?>/dashboard'; } }
                    		});
                    	});
                    });
                </script>
                
                <?php }else if($_SESSION['_typeusr']=='6'){
                    $s_hot = "SELECT ua.oid AS hoteloid, hotelname FROM users u INNER JOIN userassign ua USING (useroid) INNER JOIN hotel ON ua.oid=hotel.hoteloid WHERE ua.type = 'hoteloid' AND u.useroid = '".$_SESSION['_oid']."'";
            		$q_hot = $db->query($s_hot);
            		$r_hot = $q_hot->fetchAll(PDO::FETCH_ASSOC);
            		$myarr = array();
            		foreach($r_hot as $row){
        				$hid = $row['hoteloid'];
        				$hname = $row['hotelname'];
        				
        				$myarr[$hid] = $hname;
        			}
                ?>
                <li class="dropdown notifications-menu">
                    <a class="dropdown-toggle" data-toggle="dropdown" title="<?=$_SESSION['_hotelname']?>">
					<?php if(!empty($_SESSION['_hotelname'])){ ?><i class="fa fa-institution"></i> <?=$_SESSION['_hotelname']?><?php } ?> <i class="caret"></i>
                    </a>
                    <ul class="dropdown-menu">
                      <li class="header">You have <?=count($myarr)?> hotel(s)</li>
                      <li>
                        <ul class="menu">
                        <?php foreach($myarr as $key => $value){
                            $mynow = ($_SESSION['_hotel'] == $key) ? 'background-color: #e4ecf3;' : '';
                        ?>
                          <li>
                            <a class="change-hotel" hoid="<?=$key?>" style="<?=$mynow?>">
                              <i class="fa fa-institution"></i> <?=$value?>
                            </a>
                          </li>
                        <?php }?>
                        </ul>
                      </li>
                    </ul>
                </li>
                <script>
                    $(function(){
                        $('a.change-hotel').click(function(e) {
                    		var loginas_hc = $(this).attr('hoid');
                    		var loginas_hname = $(this).text().trim();
                    		$.ajax({
                    			type	: 'POST', cache: false,
                    			url		: '<?php echo"$base_url"; ?>/includes/suadm/hotel/change-session.php',
                    			data	: { loginas : loginas_hc, hname : loginas_hname },
                    			success	: function(rsp){ if(rsp == "1"){ document.location.href = '<?php echo"$base_url"; ?>/dashboard'; } }
                    		});
                    	});
                    });
                </script>
                
                <?php }else{?>
                    <?php if(!empty($_SESSION['_hotelname']) and $_SESSION['_typeusr'] != "6"){ ?>
                        <li class="button-red"><a href="<?=$base_url?>/availability"><i class="fa fa-inbox"></i> Availability</a></li>
                        <li class="button-red"><a href="<?php echo "$base_url"; ?>/booking"><i class="fa fa-circle-o"></i> Bookings</a></li>
                        <li class="button-red dropdown user user-menu">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="<?=$base_url?>/hotel-profile/basic-info">
                            <i class="fa fa-cog"></i>
                            </a>
                        </li>
                    <?php } ?>

                    <li class="dropdown user user-menu">
                        <a class="dropdown-toggle" data-toggle="dropdown" title="<?=$_SESSION['_hotelname']?>">
                        <?php if(!empty($_SESSION['_hotelname'])){ ?><i class="fa fa-institution "></i> <?=$_SESSION['_hotelname']?><?php } ?>
                        </a>
                    </li>
                    
                <?php }?>
                <li class="dropdown user user-menu">
                    <a class="dropdown-toggle" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-user"></i>
                        <span><?=$_SESSION['_initial']?> <i class="caret"></i></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header bg-light-blue">
                            <img src="<?php echo $_SESSION['_userpict']; ?>" class="img-circle" alt="User Image">
                            <p>
                                <?=$_SESSION['_initial']?> - <?=$_SESSION['_type']?>
                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="<?php echo"$base_url"; ?>/profile" class="btn btn-default btn-flat">Profile</a>
                                <?php
                                $tb_registered = array(1,87,88,147,149,89,152,130);
                                if(in_array($_SESSION['_oid'], $tb_registered)){
                                    $token = sha1($_SESSION['_oid'] . '_thebuking.com');
                                    echo '<form method="POST" action="https://thebuking.com/taskbook/login" target="_blank" style="display: inline-block;">
                                      <input type="hidden" name="autologin" value="'.$token.'">
                                      <button type="submit" class="btn btn-default btn-flat" style="font-size: 14px;">Taskbook</button>
                                    </form>';
                                }
                                ?>
                            </div>
                            <div class="pull-right">
                                <a href="<?php echo"$base_url"; ?>/logout" class="btn btn-default btn-flat">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    <!-- </nav> -->