<?php
	$cpoid = $_POST['cpoid'];
	$hoteloid = $_POST['hoteloid'];
	$choteloid = $_POST['choteloid'];
	$name = (isset($_POST['name'])) ? $_POST['name'] : "";
	$description = (isset($_POST['description'])) ? $_POST['description'] : "";
	
	$allowedExtension = array('jpg', 'jpeg', 'png');
	$tmp = explode('.', $_FILES['image']['name']);
	$extension = end($tmp);
	$image = '';
	if(!empty($_FILES['image']['name']) and isset($_FILES['image']['name'])){
		if(in_array($extension, $allowedExtension)){
			$imagename = str_replace(' ', '_', $_FILES['image']['name']);
			$imagename = str_replace('.'.$extension, '', $imagename);
			$ext = '.'.strtolower($extension);
			$image_new_name = $hcode.substr($imagename, 0, 10).date('Ymd_His').$ext;
			$folder_destination = 'pict/icon/';

			move_uploaded_file($_FILES['image']['tmp_name'], $upload_base.$folder_destination.$image_new_name) or die('upload failed.');
			$image = $web_url.'/'.$folder_destination.$image_new_name;

		}else{
			echo'<script>alert("Data not saved.\\nInvalid file.");</script>';
		}
	}
		
	try {
	    if(!empty($name)){
    	    if($choteloid == 0){
    	        if(empty($image)){
    	            $stmt = $db->query("SELECT th.*, i.* from termheadline th left join icon i using (iconoid) where termheadlineoid = '".$cpoid."'");
            		$row_count = $stmt->rowCount();
            		if($row_count > 0) {
            			$r_headline = $stmt->fetchAll(PDO::FETCH_ASSOC);
            			foreach($r_headline as $row){
            				if($row['iconoid'] == 0){
                                $image = $row['term_icon_src'];
                            }else{
                                $image = $row['icon_src'];
                            }
            			}
            		}
    	        }
    	        
    	        $stmt = $db->prepare("INSERT INTO `termheadline`(`termheadlineoid`, `hoteloid`, `term_icon_src`, `icon_title`, `title`, `description`) VALUES (NULL,:a,:b,:c,:d,:e)");
        		$stmt->execute(array(':a' => $hoteloid,':b' => $image, ':c' => $name, ':d' => $name, ':e' => $description));
        		$promotionoid = $db->lastInsertId();
        		
    	        $stmt = $db->prepare("INSERT INTO `termheadlinehotelremove`(`thremoveoid`, `termheadlineoid`, `hoteloid`) VALUES (NULL,:a,:b)");
        		$stmt->execute(array(':a' => $cpoid,':b' => $hoteloid));
    	    }else{
    	        if(empty($image)){
            		$stmt = $db->prepare("update termheadline set icon_title = :b, title = :c, description = :d where hoteloid = :a and termheadlineoid = :id");
            		$stmt->execute(array(':a' => $hoteloid, ':b' => $name, ':c' => $name, ':d' => $description, ':id' => $cpoid));
    	        }else{
    	            $stmt = $db->prepare("update termheadline set icon_title = :b, title = :c, description = :d, term_icon_src = :e where hoteloid = :a and termheadlineoid = :id");
            		$stmt->execute(array(':a' => $hoteloid, ':b' => $name, ':c' => $name, ':d' => $description, ':e' => $image, ':id' => $cpoid));
    	        }
    	    }
	    }else{
	        echo'<script>alert("Data not saved.\\nPlease type the title of headline icon.");</script>';
	    }
	}catch(PDOException $ex) {
		echo "Invalid Query";
		print($ex);
		die();
	}
	
	header("Location: ". $base_url ."/headline-icon");
?>

