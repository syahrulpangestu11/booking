<script type="text/javascript">
$(function(){
	
	$(document).ready(function(){ 
		
		getLoadData();
	});
	
	$('body').on('click','button.edit', function(e) {
		pid = $(this).attr("pid");
		url = "<?php echo $base_url; ?>/headline-icon/edit/?cpoid="+pid;
      	$(location).attr("href", url);
	});

	$('body').on('click','button.cancel', function(e) {
		pid = $(this).attr("pid");
		url = "<?php echo $base_url; ?>/headline-icon";
      	$(location).attr("href", url);
	});

	$('body').on('click','button.add', function(e) {
		pid = $(this).attr("pid");
		url = "<?php echo $base_url; ?>/headline-icon/add";
      	$(location).attr("href", url);
	});
	
   var $dialogDelete = $('<div id="dialog-del"></div>')
	.html('Are you sure you want to delete this data?')
	.dialog({
		autoOpen: false,
		title: 'Confirmation',
		buttons: { 
			"Delete": function(){ 
				var url = $(this).data('url'); 
				var id = $(this).data('id'); 
				$(this).dialog("close"); 
				deleteAction(url, id);
			},
			Cancel: function(){ $( this ).dialog("close"); }
		}
	});
	
	var $dialogNotification = $('<div id="dialog-notice"></div>')
	.dialog({
		autoOpen: false,
		title: 'Notification',
		buttons: { 
			"Ok": function(){ 
				var callback = '<?php echo $base_url; ?>/headline-icon'; 
				$(this).dialog( "close" );
				$(location).attr("href", callback); 
			}
		}
	});
	
	function deleteAction(url, id){
		$.ajax({
			url: url,
			type: 'post',
			data: { id : id},
			success: function(data) {
				if(data == "1"){
					$dialogNotification.html("Data has been deleted");
					$dialogNotification.dialog("open");
				}else{
					$dialogNotification.html("We&rsquo;re very sorry, we can't save your data right now");
					$dialogNotification.dialog("open");
				}
			}
		});
	}
	
	$('body').on('click','button.delete', function(e) {
		id = $(this).attr("pid");
		$dialogDelete.data("url", "<?php echo $base_url; ?>/includes/headline-icon/delete-hi.php");
		$dialogDelete.data("id", id);
		$dialogDelete.dialog("open");
	});
		
	function getLoadData(){
		$.ajax({
			url: "<?php echo"$base_url"; ?>/includes/headline-icon/data.php",
			type: 'post',
			data: $('form#data-input').serialize(),
			success: function(data) { 
				$("#data-box").html(data)
			}
		});
	}	
		
	$('body').on('change','select[name=termcondition]', function(e) {
		termcondition = $(this).val();
		cancellationday = $('input[name=cancellationday]');
		typecancellation = $('input[name=typecancellation]');
		night = $('input[name=night]');
		if(termcondition == "non refundable"){
			cancellationday.val(0);
			cancellationday.prop('disabled', true);
			typecancellation.val('full amount');
			typecancellation.prop('disabled', true);
			night.prop('disabled', true);
		}else{
			cancellationday.prop('disabled', false);
			typecancellation.prop('disabled', false);
			night.prop('disabled', false);
		}
	});
});
</script>