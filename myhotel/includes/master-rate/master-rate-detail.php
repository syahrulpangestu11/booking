<?php
  $stmt = $db->prepare("select r.roomoid, r.name as roomname, roomofferoid, ro.name as rateplan, (select bm_name as bookingmarkert from bookingmarket where bookingmarketoid = :b) as bookingmarket from roomoffer ro inner join room r using (roomoid) where ro.roomofferoid = :a");
  $stmt->execute(array(':a' => $_POST['rateplan'], ':b' => $_POST['bookingmarket']));
  $room = $stmt->fetch(PDO::FETCH_ASSOC);
?>
<style>input[type="text"]{ width:100%!important; } .ui-datepicker{ z-index:10000!important; }</style>
<section class="content-header">
    <h1>Master Rate Detail<h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-cog"></i> Setting</a></li>
        <li>Master Rate</li>
        <li class="active">Master Rate Detail</li>
    </ol>
</section>
<section class="content">
  <div class="box box-warning box-form">
    <div id="data-box" class="box-body">
      <div class="row">
        <div class="col-md-6">
          <h1><?=$room['roomname']?></h1>
          <h2>Rateplan : <?=$room['rateplan']?></h2>
          <h2>Booking Market : <?=$room['bookingmarket']?></h2>
        </div>
        <div class="col-md-6"></div>
      </div>
      <br>
      <div class="row">
        <div class="col-md-12 text-right">
          <button type="button" name="add-master-rate" class="btn btn-default" onclick="document.location.href='<?=$base_url?>/master-rate'">Back To Room List</button>
          <!-- <button type="button" name="add-master-rate" class="btn btn-primary" data-toggle="modal" data-target="#AddMasterRate">Add Master Rate</button> -->
          <button type="button" name="add-master-rate" class="btn btn-primary">Add Master Rate</button>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <table class="table">
            <thead>
              <tr><th>BAR Name</th><th>Room Type</th><th>Rate Plan</th><th>Rate</th></tr>
            </thead>
            <tbody>
              <?php
              include("includes/paging/pre-paging.php");
              $stmt = $db->prepare("select mr.*, r.name as roomname, ro.name as rateplan, cr.currencycode from masterrate mr inner join currency cr using (currencyoid) inner join roomoffer ro using (roomofferoid) inner join room r using (roomoid) where mr.roomofferoid = :a and mr.bookingmarketoid = :b ".$paging_query);
              $stmt->execute(array(':a' => $_POST['rateplan'], ':b' => $_POST['bookingmarket']));
              $row_count = $stmt->rowCount();
              $result_masterrate = $stmt->fetchAll(PDO::FETCH_ASSOC);
              foreach($result_masterrate as $mr){
              ?>
              <tr>
                <td><?=$mr['bar']?></td>
                <td><?=$mr['roomname']?></td>
                <td><?=$mr['rateplan']?></td>
                <td><?=$mr['currencycode']." ".number_format($mr['rate'])?></td>
                <td>
                  <button type="button" class="btn btn-sm btn-danger" name="remove-rate" data-id="<?=$mr['masterrateoid']?>" data-toggle="modal" data-target="#DeleteMasterRate"><i class="fa fa-trash"></i></button>
                  <button type="button" class="btn btn-sm btn-success" name="edit-rate" data-id="<?=$mr['masterrateoid']?>" data-toggle="modal" data-target="#EditMasterRate"><i class="fa fa-pencil"></i></button>
                </td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 text-center">
        <?php
      		$stmt = $db->prepare("select count(mr.masterrateoid) jmldata from masterrate mr inner join currency cr using (currencyoid) inner join roomoffer ro using (roomofferoid) inner join room r using (roomoid) where mr.roomofferoid = :a and mr.bookingmarketoid = :b ");
          $stmt->execute(array(':a' => $_POST['rateplan'], ':b' => $_POST['bookingmarket']));
          $result_jmldata = $stmt->fetch(PDO::FETCH_ASSOC);
      		$jmldata = $result_jmldata['jmldata'];

      		$tampildata = $row_count;
      		include("includes/paging/post-paging.php");
          ?>
        </div>
      </div>
    </div>
  </div>


  <div class="modal fade" id="AddMasterRate" tabindex="-1" role="dialog" aria-labelledby="AddMasterRate">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Add New Master Rate</h4>
        </div>
        <div class="modal-body">
          <form id="form-add-master-rate" class="form-horizontal" method="post">
            <input type="hidden" name="roomoffer" value="<?=$_POST['rateplan']?>">
            <input type="hidden" name="bookingmarket" value="<?=$_POST['bookingmarket']?>">
            <div class="row">
              <div class="col-md-3"><label>Room</label></div>
              <div class="col-md-9"><?=$room['roomname']?></div>
            </div>
            <div class="row">
              <div class="col-md-3"><label>Rate Plan</label></div>
              <div class="col-md-9"><?=$room['rateplan']?></div>
            </div>
            <div class="row">
              <div class="col-md-3"><label>Booking Market</label></div>
              <div class="col-md-9"><?=$room['bookingmarket']?></div>
            </div>
            <div class="form-group">
              <label class="col-md-3">BAR</label>
              <div class="col-md-7"><input type="text" class="form-control" name="bar"></div>
            </div>
            <?php  /*
            <div class="form-group">
              <label class="col-md-3">Start Date</label>
              <div class="col-md-5">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                  <input type="text" class="form-control" name="startdate">
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3">End Date</label>
              <div class="col-md-5">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                  <input type="text" class="form-control" name="enddate">
                </div>
              </div>
            </div>
            */ ?>
            <div class="form-group">
              <label class="col-md-3">Rate</label>
              <div class="col-md-3">
                <select name="currency" class="form-control">
                <?php
                $stmt = $db->prepare("select * from currency where publishedoid = '1'");
                $stmt->execute();
                $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
                foreach($result as $currency){
                ?>
                <option value="<?=$currency['currencyoid']?>" <?=$selected?>><?=$currency['currencycode']?></option>
                <?php
                }
                ?>
                </select>
              </div>
              <div class="col-md-4">
                <input type="text" class="form-control" name="rate">
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" name="btn-submit" class="btn btn-primary">Save</button>
        </div>
      </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="DeleteMasterRate" tabindex="-1" role="dialog" aria-labelledby="DeleteMasterRate">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Delete Master Rate</h4>
        </div>
        <div class="modal-body">
          <form id="form-delete-master-rate" class="form-horizontal" method="post">
            <input type="hidden" name="masterrate" value="">
            <div class="row">
              <div class="col-md-12">Are you sure you want delete this master rate?</div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" name="btn-delete" class="btn btn-danger">Delete</button>
        </div>
      </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="EditMasterRate" tabindex="-1" role="dialog" aria-labelledby="EditMasterRate">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Edit Master Rate</h4>
        </div>
        <div class="modal-body">
          please wait ...
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" name="btn-submit" class="btn btn-primary">Save</button>
        </div>
      </div>
      </div>
    </div>
  </div>

  <form id="view-detail-master-rate" method="post" action="<?=$base_url?>/master-rate/detail">
    <input type="hidden" name="rateplan" value="<?=$_POST['rateplan']?>">
    <input type="hidden" name="bookingmarket" value="<?=$_POST['bookingmarket']?>">
    <input type="hidden" name="page" value="<?=$_POST['page']?>">
  </form>
</section>
