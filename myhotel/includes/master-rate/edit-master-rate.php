<?php
  include ('../../conf/connection.php');
  $stmt = $db->prepare("select mr.*, r.roomoid, r.name as roomname, ro.name as rateplan, bm_name as bookingmarket from masterrate mr inner join bookingmarket bm using (bookingmarketoid) inner join roomoffer ro using (roomofferoid) inner join room r using (roomoid) where mr.masterrateoid = :a");
  $stmt->execute(array(':a' => $_POST['masterrate']));
  $mr = $stmt->fetch(PDO::FETCH_ASSOC);
?>
<form id="form-edit-master-rate" class="form-horizontal" method="post">
  <input type="hidden" name="masterrate" value="<?=$mr['masterrateoid']?>">
  <div class="row">
    <div class="col-md-3"><label>Room</label></div>
    <div class="col-md-9"><?=$mr['roomname']?></div>
  </div>
  <div class="row">
    <div class="col-md-3"><label>Rate Plan</label></div>
    <div class="col-md-9"><?=$mr['rateplan']?></div>
  </div>
  <div class="row">
    <div class="col-md-3"><label>Booking Market</label></div>
    <div class="col-md-9"><?=$mr['bookingmarket']?></div>
  </div>
  <div class="form-group">
    <label class="col-md-3">BAR</label>
    <div class="col-md-7"><input type="text" class="form-control" name="bar" value="<?=$mr['bar']?>"></div>
  </div>
  <?php /*
  <div class="form-group">
    <label class="col-md-3">Start Date</label>
    <div class="col-md-5">
      <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
        <input type="text" class="form-control" name="startdate" value="<?=date('d F Y', strtotime($mr['startdate']))?>">
      </div>
    </div>
  </div>
  <div class="form-group">
    <label class="col-md-3">End Date</label>
    <div class="col-md-5">
      <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
        <input type="text" class="form-control" name="enddate" value="<?=date('d F Y', strtotime($mr['enddate']))?>">
      </div>
    </div>
  </div>
  */ ?>
  <div class="form-group">
    <label class="col-md-3">Rate</label>
    <div class="col-md-3">
      <select name="currency" class="form-control">
      <?php
      $stmt = $db->prepare("select * from currency where publishedoid = '1'");
      $stmt->execute();
      $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
      foreach($result as $currency){
        if($mr['currencyoid'] == $currency['currencyoid']){ $selected = "selected"; }else{ $selected = ""; }
      ?>
      <option value="<?=$currency['currencyoid']?>" <?=$selected?>><?=$currency['currencycode']?></option>
      <?php
      }
      ?>
      </select>
    </div>
    <div class="col-md-4">
      <input type="text" class="form-control" name="rate"  value="<?=floatval($mr['rate'])?>">
    </div>
  </div>
</form>
