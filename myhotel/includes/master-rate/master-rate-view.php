<?php
  $stmt = $db->prepare("select bookingmarketoid, bm_name as bookingmarket from bookingmarket bm where publishedoid in (1)");
  $stmt->execute();
  $result_bookingmarket = $stmt->fetchAll(PDO::FETCH_ASSOC);
?>
<section class="content-header">
    <h1>Master Rate</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-cog"></i> Setting</a></li>
        <li class="active">Master Rate</li>
    </ol>
</section>
<section class="content">
  <div class="box box-warning box-form">
    <div id="data-box" class="box-body">
      <div class="row">
        <div class="col-md-12">
          <table class="table">
            <thead>
              <tr><th>Master Room Type</th><th>Rate Plan</th><th style="display:none">Market</th><th></th></tr>
            </thead>
            <tbody>
              <?php
              $stmt = $db->prepare("select roomoid, name as roomname from room r where r.hoteloid = :a and publishedoid in (0,1)");
              $stmt->execute(array(':a' => $hoteloid));
              $result_masterroom = $stmt->fetchAll(PDO::FETCH_ASSOC);
              foreach($result_masterroom as $masterroom){
              ?>
              <tr>
                <td><?=$masterroom['roomname']?></td>
                <td>
                  <?php
                  $stmt = $db->prepare("select roomofferoid, name as rateplan from roomoffer ro where ro.roomoid = :a and publishedoid in (0,1)");
                  $stmt->execute(array(':a' => $masterroom['roomoid']));
                  $count_rateplan = $stmt->rowCount();
                  if($count_rateplan > 0){
                  ?>
                    <select name="rateplan[]" class="form-control">
                  <?php
                    $result_rateplan = $stmt->fetchAll(PDO::FETCH_ASSOC);
                    foreach($result_rateplan as $rateplan){
                  ?>
                    <option value="<?=$rateplan['roomofferoid']?>"><?=$rateplan['rateplan']?></option>
                  <?php } ?>
                    </select>
                  <?php
                }else{ echo "<i>no rateplan available</i>"; }
                  ?>
                </td>
                <td style="display:none">
                  <select name="bookingmarket[]" class="form-control">
                  <?php foreach($result_bookingmarket as $bookingmarket){ ?>
                  <option value="<?=$bookingmarket['bookingmarketoid']?>"><?=$bookingmarket['bookingmarket']?></option>
                  <?php } ?>
                  </select>
                </td>
                <td>
                  <button type="button" name="view-detail" class="btn btn-success" data-id="<?=$masterroom['roomoid']?>">Manage Master Rate</button>
                </td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <form id="view-detail-master-rate" method="post" action="<?=$base_url?>/master-rate/detail">
    <input type="hidden" name="room">
    <input type="hidden" name="rateplan">
    <input type="hidden" name="bookingmarket">
  </form>

  <?php
  $stmt = $db->query("select dynamicrate from hotel where hoteloid = '".$_SESSION['_hotel']."'");
  $subscribe = $stmt->fetch(PDO::FETCH_ASSOC);
  if($subscribe['dynamicrate'] == '1'){
    include('dynamic-rate.php');
  }
  ?>
</section>
