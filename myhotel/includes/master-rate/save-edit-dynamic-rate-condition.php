<?php
try{
  session_start();
  error_reporting(E_ALL ^ E_NOTICE);
  include ('../../conf/connection.php');

  $stmt = $db->prepare("update dynamicrate set allocation = :a where dynamicrateoid = :b and hoteloid = :c");
  $stmt->execute(array(':a' => $_POST['allocation'], ':b' => $_POST['id'], ':c' => $_SESSION['_hotel']));
  $id = $_POST['id'];

  foreach($_POST['rcnt_rateplan'] as $key=> $rateplan){
    if(!empty($_POST['rcnt_masterrate'][$key])){
      $stmt = $db->prepare("update dynamicraterule set masterrateoid = :a where dynamicrateoid = :b and roomofferoid = :c");
      $stmt->execute(array(':a' => $_POST['rcnt_masterrate'][$key], ':b' => $id, ':c' => $rateplan));
    }else{
      $stmt = $db->prepare("delete from dynamicraterule where dynamicrateoid = :a and roomofferoid = :b");
      $stmt->execute(array(':a' => $id, ':b' => $rateplan));
    }
  }

  foreach($_POST['rateplan'] as $key=> $rateplan){
    if(!empty($_POST['masterrate'][$key])){
      $stmt = $db->prepare("insert into dynamicraterule (dynamicrateoid, roomofferoid, masterrateoid) value (:a, :b, :c)");
      $stmt->execute(array(':a' => $id, ':b' => $rateplan, ':c' => $_POST['masterrate'][$key]));
    }
  }

  echo "1";

}catch(Exception $e){
  echo $e->getMessage();
}
?>
