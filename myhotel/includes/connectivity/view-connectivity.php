<?php
	include('includes/bootstrap.php');
	// include('modal-cancellationpolicy.php');
	$channeloid = 1;

	$s_csp = "SELECT * FROM connectivity_sojernpixel csp WHERE csp.hoteloid = '".$hoteloid."'";	
	$stmt = $db->query($s_csp);
	$pixelsojern = $stmt->fetch(PDO::FETCH_ASSOC);
?>
<style type="text/css">
div.green{
	padding:0 7px;
	border:3px solid #fff;
	background-color:#f6fbfa;
}
div.frame-cp{
	padding: 20px;
	border: 1px solid #ddd;
}
div.roomoffer{
	border-bottom:1px solid #bcbdbd;
	padding:10px 0;
}
div.roomoffer:last-of-type{
	border-bottom:none;
}
</style>
<script type="text/javascript">
$(function() {
	$('textarea').trumbowyg({
		btns: ['btnGrp-design', 'link', 'btnGrp-justify', 'btnGrp-lists'],
		fullscreenable: false
	});
});
</script>
<section class="content-header">
    <h1>Connectivity</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-cog"></i>  Settings</a></li>
        <li class="active">Connectivity Setting</li>
    </ol>
</section>
<section class="content" id="promotion">
        <div class="box box-form">
            <div class="">
            	<div class="col-md-5 green">
                    <form class="form-box form-horizontal" method="post" enctype="multipart/form-data" id="data-input" action="#">

					<div><label style="font-size: 16px;">Pixel Sojern Configuration</label></div>
					<br>
						<h1>Booking Engine Pixel ID</h1>
						<br>
						<div class="form-group">
                            <div class="col-md-4">
                                <label>Search Results</label>
								<br>
                                <input name="pid_be_searchresult" type="text" value="<?=$pixelsojern['pid_be_searchresult']?>" class="short">
                            </div>
							<div class="col-md-4">
                                <label>Shopping Cart</label>
								<br>
                                <input name="pid_be_shoppingcart" type="text" value="<?=$pixelsojern['pid_be_shoppingcart']?>" class="short">
                            </div>
							<div class="col-md-4">
                                <label>Conversion</label>
								<br>
                                <input name="pid_be_conversion" type="text" value="<?=$pixelsojern['pid_be_conversion']?>" class="short">
                            </div>
							<div class="clearfix"></div>
                        </div>

						<h1>Website Pixel ID</h1>
						<br>
                        <div class="form-group">
                            <div class="col-md-4">
                                <label>Homepage</label>
								<br>
                                <input name="pid_w_homepage" type="text" value="<?=$pixelsojern['pid_w_homepage']?>" class="short">
                            </div>
							<div class="col-md-4">
                                <label>Product</label>
								<br>
                                <input name="pid_w_product" type="text" value="<?=$pixelsojern['pid_w_product']?>" class="short">
                            </div>
							<div class="col-md-4">
                                <label>Tracking</label>
								<br>
                                <input name="pid_w_tracking" type="text" value="<?=$pixelsojern['pid_w_tracking']?>" class="short">
                            </div>
							<div class="clearfix"></div>
                        </div>
                        <div class="form-group">
							<div class="col-md-12">
								<label>Status</label>
								<select name="publishedoid" class="form-control">
								<?php
									try {
										$stmt = $db->query("select * from published where showthis = 'y'");
										$r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
										foreach($r_room as $row){
											if($row['publishedoid'] == $pixelsojern['publishedoid']){ $selected = "selected"; }else{ $selected=""; }
											echo"<option value='".$row['publishedoid']."' ".$selected.">".$row['note']."</option>";
										}
									}catch(PDOException $ex) {
										echo "Invalid Query";
										die();
									}
								?>
								</select>
							</div>
							<div class="clear"></div>
							<br>
                            <div class="col-md-12 text-right"><button type="button" class="btn btn-primary" id="save-connectivity">Save</button></div>
                        </div>
                        <div class="clear"></div>
                   
        		</form>
                </div><!--  punya col-5 -->
				<div class="clear"></div>
			</div>
		</div>
</section>
<script type="text/javascript">
$(function(){
	$(document).ready(function(){ 
		$('input[type=radio][name=deposit]:checked').each(function(){
			if($(this).prop("checked") == true){
				$(this).click();
			}
		});
	});
	
	$('body').on('click', 'button.remove-cp', function (e){
		var cp = $(this).val();
		$.ajax({
			url		: '<?php echo $base_url; ?>/includes/bar-setting/remove-cancellationpolicy.php',
			type	: 'post',
			data	: { id : cp },
			success	: function(response){
				window.location.reload();
			}
		});
	});
	
	$('body').on('change','select[name=cancellationpolicy]', function(e) {
		desc = $('select[name=cancellationpolicy] option:selected').attr('note');
		p_desc = $('#CPModal').find('div.note-cp').html(desc);
	});
	
	$('body').on('change','input[name=choose-room]', function(e) {
		action		= $(this).val();
		elem_target	= $('#CPModal').find('input:checkbox[name="roomoffer[]"]');
		if(action == "select"){
			elem_target.prop('checked',true);
		}else if(action == "unselect"){
			elem_target.prop('checked',false);
		}
	});
	
	$('body').on('click', '#CPModal button#assign-cp', function(e){	
		var formcp = $('#CPModal').find('form#form-assign-cp');	
		$.ajax({
			url		: '<?php echo $base_url; ?>/includes/bar-setting/save-cancellationpolicy.php',
			type	: 'post',
			data	: formcp.serialize(),
			success	: function(response){
				window.location.reload();
			}
		});
		
		$('.modal').modal('hide');
	});
	
	$(document).ready(function(){ 
		$('input[type=radio][name=deposit]:checked').each(function(){
			if($(this).prop("checked") == true){
				$(this).click();
			}
		});
	});
	
	$('body').on('click','input[type="radio"][name=deposit]', function(e) {
		allElem = $('div.depositval');
		allElem.css('display','none');
		
		
		if($(this).val() == '2' || $(this).val() == '3'  || $(this).val() == '4' ){
			applyval = $(this).next('div.depositval');
			if($(this).prop( "checked" ) == true){
				applyval.css('display','block');
			}else{
				applyval.css('display','none');
			}
		}
		
		$('input[type="text"][name="depositvalue"]').prop('disabled',true);
		$(this).next('div.depositval').children('input[type="text"][name="depositvalue"]').prop('disabled',false);
	});
			
	$('body').on('click', 'button#save-connectivity', function (e){
		var form = $('form#data-input');
		$.ajax({
			url		: '<?php echo $base_url; ?>/includes/connectivity/save-connectivity.php',
			type	: 'post',
			data	: form.serialize(),
			success	: function(response){
				window.location.reload();
			}
		});
	});

	
	var dates = $("#sdate, #edate").datepicker({
		beforeShow: function() {
			setTimeout(function(){ $('.ui-datepicker').css('z-index', 9999); }, 0);
		},
		defaultDate: "+0", changeMonth: true, numberOfMonths: 1, changeYear: true,
		onSelect: function( selectedDate ) {
			var option = this.id == "sdate" ? "minDate" : "maxDate",
				instance = $( this ).data( "datepicker" );
				date = $.datepicker.parseDate( instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings ); dates.not( this ).datepicker( "option", option, date );;
			if($("#sdate").val() == $("#edate").val()) { //if value from #from same with #to or value #from more than value of #to
				var x = $("#startdate").val(); var temp = new Array(); temp = x.split(" "); var d = parseInt(temp[0]); var m = temp[1]; var y = parseInt(temp[2]);
				switch(m) {
					case "January"	: if(d==31) { d = 1; m = "February"; }else { d = d+1; } break;
					case "February"	: if(y%4 == 0) { if(d==29) { d = 1; m = "March"; }else { d = d+1; } }else { if(d==28) { d = 1; m = "March"; }else { d = d+1; } } break;
					case "March"	: if(d==31) { d = 1; m = "April"; }else { d = d+1; } break;
					case "April"	: if(d==30) { d = 1; m = "May"; }else { d = d+1; } break;
					case "May"		: if(d==31) { d = 1; m = "June"; }else { d = d+1; } break;
					case "June"		: if(d==30) { d = 1; m = "July"; }else { d = d+1; } break;
					case "July"		: if(d==31) { d = 1; m = "August"; }else { d = d+1; }break;
					case "August"	: if(d==31) { d = 1; m = "September"; }else { d = d+1; } break;
					case "September": if(d==30) { d = 1; m = "October"; }else { d = d+1; } break;
					case "October"	: if(d==31) { d = 1; m = "November"; }else { d = d+1; } break;
					case "November"	: if(d==30) { d = 1; m = "December"; }else { d = d+1; } break;
					case "December"	: if(d==31) { d = 1; m = "January"; y = y + 1; }else { d = d+1;} break;
				}$("#edate").val(d+" "+m+" "+y);
			} 
		} 
	}); 
});
</script>