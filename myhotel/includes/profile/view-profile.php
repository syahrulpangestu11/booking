<?php 
	try {
		$stmt = $db->query("select * from users where username = '".$_SESSION['_user']."'");
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
			$r_user = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_user as $row){
				$username = $row['username'];
				$displayname = $row['displayname'];

			}
		}else{
			echo "No Result";
			die();
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}
?>

<section class="content-header">
    <h1>
        Profile
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-laptop"></i> Profile</a></li>
        <li class="active">Change Setting</li>
    </ol>
</section>
<section class="content">
	<div class="row">
        <div class="box box-form">
            <div id="data-box" class="box-body">
            <h1>User Information</h1>
            <form method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/save-profile">
            <input type="hidden" name="hoteloid" value="<?php echo $hoteloid; ?>">
            <ul class="inline form-input">
                <li>
                    <div class="side-left"><label>Username</label></div>
                    <div class="side-right"><input type="text" class="input-text" readonly="readonly" name="username" value="<?php echo $username; ?>"></div>
                    <div class="clear"></div>
                </li>
                <li>
                    <div class="side-left"><label>Display Name</label></div>
                    <div class="side-right"><input type="text" class="input-text" name="displayname" value="<?php echo $displayname; ?>"></div>
                    <div class="clear"></div>
                </li>
                <li>
                    <div class="side-left"><label>Password</label></div>
                    <div class="side-right"><input type="password" class="input-text" name="password"></div>
                    <div class="clear"></div>
                </li>
                <li>
                    <div class="side-left"><label>Current Password</label></div>
                    <div class="side-right"><input type="password" class="input-text" name="currentpassword"></div>
                    <div class="clear"></div>
                </li>
                <li>
                    <div class="side-left"><label>Picture</label></div>
                    <div class="side-right"><input type="file" class="input-text" name="photos"></div>
                    <div class="clear"></div>
                </li>
                <li>
                    <div class="side-left">&nbsp;</div>
                    <div class="side-right"><button type="submit" class="submit">Save</button></div>
                    <div class="clear"></div>
                </li>
                <div class="clear"></div>
            </ul>
            </form>
        </div>
   		</div>
    </div>
</section>
