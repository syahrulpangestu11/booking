<?php
$question = $_REQUEST['q'];
$type = $_REQUEST['type'];
?>
<style>
	#form-faq button{float:right;margin:5px;}
	#form-faq .form-control{width:100%;}
</style>
<section class="content-header">
    <h1>
        FAQ
    </h1>
    <ol class="breadcrumb">
        <li class="active"><a href="#"><i class="fa fa-book"></i> FAQ</a></li>
    </ol>
</section>
<section class="content">
		    <div class="row">
		        <div class="box box-form">
		            <div class="box-body">
		                <div class="form-group">
		                    <form method="get" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/faq/">
														<input type="hidden" name="page">
		                    		<label>Question  :</label> &nbsp;&nbsp;
														<input type="text" name="q" class="input-text" value="<?=$question;?>">
				                    &nbsp;&nbsp;
				                    <label>Type  :</label> &nbsp;&nbsp;
				                    <select name="type" class="input-select">
				                    	<option value="">show all</option>
				                    <?php
				                    try {
				                        $stmt = $db->query("select distinct type from masterfaq");

				                        $r_faqtype = $stmt->fetchAll(PDO::FETCH_ASSOC);
				                        foreach($r_faqtype as $row){
																		if($row['type'] == $type){ $selected = "selected"; }else{ $selected=""; }
				                    ?>
				                        <option value="<?php echo $row['type']; ?>" <?php echo $selected; ?>><?php echo $row['type']; ?></option>
				                    <?php
				                        }
				                    }catch(PDOException $ex) {
				                        echo "Invalid Query".$ex;
				                        die();
				                    }
		                    ?>
		                    </select>
		                     &nbsp;&nbsp;
		                    <button type="submit" class="blue-button">Find</button>
		                </form>
		                </div>
		            </div><!-- /.box-body -->
		       </div>
		    </div>

		    <div class="row">
		        <div class="box">
		            <div id="data-box" class="box-body">
		                <div class="loader">Loading...</div>
		            </div><!-- /.box-body -->
		       </div>
		    </div>
</section>
