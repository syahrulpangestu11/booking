<?php
	$stmt = $db->query("select * from masterfaq where masterfaqoid = '".$_REQUEST['uo']."'");
	$row_count = $stmt->rowCount();
	if($row_count > 0) {
		$faq = $stmt->fetch(PDO::FETCH_ASSOC);
	}
?>
<style>
.form-group input[type='text'],.form-group textarea{ width:100%; }
</style>
<section class="content-header">
    <h1>
        Edit FAQ
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-laptop"></i> FAQ</a></li>
        <li class="active">Edit FAQ</li>
    </ol>
</section>
<section class="content">
<form method="post" enctype="multipart/form-data" class="form-box" id="data-input" action="<?php echo $base_url; ?>/faq/edit-save">
	<input type="hidden" name="masterfaqoid" value="<?php echo $faq['masterfaqoid']; ?>" />
      <div class="box box-form">
        <div class="box-body">
					<h3>FAQ Information</h3><hr>
          <div class="row">
						<div class="col-md-2 col-xs-12">
							<div class="form-group">
              	<label>Type</label><input type="text" class="form-control" name="type" required="required" value="<?=$faq['type']?>">
              </div>
						</div>
						<div class="col-md-10 col-xs-12">
							<div class="form-group">
								<label>Question</label><input type="text" class="form-control" name="question" value="<?=$faq['question']?>">
							</div>
						</div>
					</div><br>
          <div class="row">
						<div class="col-md-12 col-xs-12">
							<div class="form-group">
								<label>Answer</label><textarea class="form-control" name="answer" rows="5"><?=$faq['answer']?></textarea>
							</div>
						</div>
					</div><br>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<button type="button" class="btn btn-danger cancel">Cancel</button>
								<button type="submit" class="btn btn-primary submit">Save</button>
							</div>
						</div>
						<div class="col-md-6">
							<?php
							if(!empty($faq['created']) and !empty($faq['createdby'])){
								echo "<i class='fa fa-pencil-square-o'></i> Created on ".date('d/M/Y H:i', strtotime($faq['created']))." <small>(GMT+8)</small> by <b>".$faq['createdby']."</b><br>";
							}
							if(!empty($faq['updated']) and !empty($faq['updatedby'])){
								echo "<i class='fa fa-clock-o'></i> Last updated on ".date('d/M/Y H:i', strtotime($faq['updated']))." <small>(GMT+8)</small> by <b>".$faq['updatedby']."</b>";
							}
							?>

						</div>
					</div>
        </div>
   		</div>

    </form>
</section>
