<style>
.form-group input[type='text'],.form-group textarea{ width:100%; }
</style>
<section class="content-header">
    <h1>
        Edit FAQ
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-laptop"></i> FAQ</a></li>
        <li class="active">Edit FAQ</li>
    </ol>
</section>
<section class="content">
<form method="post" enctype="multipart/form-data" class="form-box" id="data-input" action="<?php echo $base_url; ?>/faq/add-save">
      <div class="box box-form">
        <div class="box-body">
					<h3>FAQ Information</h3><hr>
          <div class="row">
						<div class="col-md-2 col-xs-12">
							<div class="form-group">
              	<label>Type</label><input type="text" class="form-control" name="type" required="required">
              </div>
						</div>
						<div class="col-md-10 col-xs-12">
							<div class="form-group">
								<label>Question</label><input type="text" class="form-control" name="question">
							</div>
						</div>
					</div><br>
          <div class="row">
						<div class="col-md-12 col-xs-12">
							<div class="form-group">
								<label>Answer</label><textarea class="form-control" name="answer" rows="5"></textarea>
							</div>
							<div class="form-group">
								<button type="button" class="btn btn-danger cancel">Cancel</button>
								<button type="submit" class="btn btn-primary submit">Save</button>
							</div>
						</div>
					</div>
        </div>
   		</div>

    </form>
</section>
