<style>
@media (min-width: 768px){
.modal-dialog { width: 80%; }
}
</style>
<!-- Modal Reconcile -->
<div class="modal fade" id="reconcileModal" tabindex="-1" role="dialog" aria-labelledby="reconcileModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form method="post" action="#">
      <input type="hidden" name="bid" value="<?=$bookingoid?>" />
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Reconcile Booking</h4>
      </div>
      <div class="modal-body">
          <?php
          try {
			$q_detail = "select br.*, c.currencycode from bookingroom br inner join currency c using (currencyoid) inner join booking b using (bookingoid) where b.bookingoid = '".$bookingoid."'";
			$stmt = $db->query($q_detail);
		  
			$no = 0;
			foreach($stmt->fetchAll(PDO::FETCH_ASSOC) as $br){
          ?>
          <div class="row">
            <div class="col-md-6">
            <h4>#Room <?=++$no?></h4>
            <?=$br['room']?><br />
            <?=$br['promotion']?><br />
            <i class="fa fa-male"></i> <?=$br['adult']?>&nbsp;&nbsp;<i class="fa fa-child"></i> <?=$br['child']?><br />
            <?=$br['cancellationname']?><br />
            Term condition : <?=$br['termcondition']?><br />
            Cancellation Days : <?=$br['cancellationday']?><br />
            Type Cancellation : <?php if($br['cancellationtype'] == "night"){ echo $br['cancellationtypenight']." night"; }else{ echo $br['cancellationtype']; } ?><br />
            <?php
                $select_total = " SELECT DATEDIFF('".$br['checkin']."','".date('Y-m-d')."') AS DiffDate";
                $stmt = $db->query($select_total);
                $diffdate = $stmt->fetch(PDO::FETCH_ASSOC);
				$before_ci = $diffdate['DiffDate'];
				
				if($br['termcondition'] == "non refundable"){
					$cancellationfee = $br['total']; $show_cancel_fee = number_format($cancellationfee);
				}else if($br['termcondition'] == "free cancellation"){
					if($before_ci > $br['cancellationday']){
						$cancellationfee = 0; $show_cancel_fee = "free";
					}else{
						if($br['cancellationtype'] == "full amount"){
							$cancellationfee = $br['total'];
						}else{
							$cancellationfee = 0;
							for($i = 0; $i <= $br['cancellationtypenight']; $i++){
								$date = date('Y-m-d', strtotime($br['checkin'].' +'.$i.' day'));
								echo $q_breakdownrate;
								$q_breakdownrate = "select total from bookingroomdtl where bookingroomoid = '".$br['bookingroomoid']."' and reconciled = '0' and `date` = '".$date."'";
								$stmt = $db->query($q_breakdownrate);
								$breakdownrate = $stmt->fetch(PDO::FETCH_ASSOC);
								$cancellationfee = $cancellationfee + $breakdownrate['total'];
							}
						}
						$show_cancel_fee = number_format($cancellationfee);
					}
				}
            ?>
            <?=date('d F Y')?> : <?=$before_ci?> day before checkin (Cancellation Amount <?=$show_cancel_fee?>)<br />
            </div>
            <div class="col-md-6">
              <table class="table table-striped">
                <thead><tr><th>Check in</th><th>Check Out</th><th>Rate</th><th>&nbsp;</th></tr></thead>
                <tbody>
          <?php	
			  $q_breakdownrate = "select * from bookingroomdtl where bookingroomoid = '".$br['bookingroomoid']."' and reconciled = '0'";
			  $stmt = $db->query($q_breakdownrate);
			  $breakdownrate = $stmt->fetchAll(PDO::FETCH_ASSOC);
			  end($breakdownrate);
			  $lastkey = key($breakdownrate);
			  foreach($breakdownrate as $key => $brd){
				?>
				<tr><td><input type="hidden" name="brd[]" value="<?=$brd['bookingroomdtloid']?>"><?=date('d/M/Y', strtotime($brd['date']))?></td><td><?=date('d/M/Y', strtotime($brd['date'].' +1 day'))?></td><td class="nighttotal"><?=number_format($brd['total'])?></td><td><?php if($key == $lastkey and $key > 0){ ?><button type="button" class="btn btn-danger btn-xs" act='remove-room'>Remove Room</button><?php } ?></td></tr>
                <?php  
			  }
          ?>
				<tr><td colspan="2"><b>Total before Reconcile</b><td><?=number_format($br['total'])?></td><td>&nbsp;</td></tr>
				<tr><td colspan="2"><b>Total after Reconcile</b><td><input type="text" name="totalreconcile[]" value="<?=floor($br['totalr'])?>"><input type="hidden" name="br[]" value="<?=$br['bookingroomoid']?>"><input type="hidden" name="checkout[]" value="<?=$br['checkoutr']?>"></td><td>&nbsp;</td></tr>
                </tbody>
              </table>
            </div>
          </div>
          <?php	
			}
		  }catch(PDOException $ex) {
			echo "Invalid Query";
			print($ex ->getMessage());
			die();
		  }
          ?>
          <div class="row">
            <div class="col-md-6"><h4>Grand Total before Reconcile</h4></div>
            <div class="col-md-6"><h4><?=number_format($roomtotal_r)?></h4></div>
          </div>
          <div class="row">
            <div class="col-md-6"><h4>Grand Total after Reconcile</h4></div>
            <div class="col-md-6"><h4><input type="text" name="grandreconsile" value="<?=floor($roomtotal_rr)?>" /></h4></div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary submit">Reconcile</button>
      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="cancelModal" tabindex="-1" role="dialog" aria-labelledby="cancelModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form method="post" action="#">
      <input type="hidden" name="bid" value="<?=$bookingoid?>" />
      <input type="hidden" name="changestatus" value="" />
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Cancel Booking</h4>
      </div>
      <div class="modal-body">
          <?php
          try {
			$q_detail = "select br.*, c.currencycode from bookingroom br inner join currency c using (currencyoid) inner join booking b using (bookingoid) where b.bookingoid = '".$bookingoid."'";
			$stmt = $db->query($q_detail);
		  
			$no = 0;
			$totalcancellation = 0;
			foreach($stmt->fetchAll(PDO::FETCH_ASSOC) as $br){
          ?>
          <div class="row">
            <div class="col-md-6">
            <h4>#Room <?=++$no?></h4>
            <?=$br['room']?><br />
            <?=$br['promotion']?><br />
            <i class="fa fa-male"></i> <?=$br['adult']?>&nbsp;&nbsp;<i class="fa fa-child"></i> <?=$br['child']?><br />
            <?=$br['cancellationname']?><br />
            Term condition : <?=$br['termcondition']?><br />
            Cancellation Days : <?=$br['cancellationday']?><br />
            Type Cancellation : <?php if($br['cancellationtype'] == "night"){ echo $br['cancellationtypenight']." night"; }else{ echo $br['cancellationtype']; } ?><br />
            <?php
                $select_total = " SELECT DATEDIFF('".$br['checkin']."','".date('Y-m-d')."') AS DiffDate";
                $stmt = $db->query($select_total);
                $diffdate = $stmt->fetch(PDO::FETCH_ASSOC);
				$before_ci = $diffdate['DiffDate'];
				
				if($br['termcondition'] == "non refundable"){
					$cancellationfee = $br['total']; $show_cancel_fee = number_format($cancellationfee);
				}else if($br['termcondition'] == "free cancellation"){
					if($before_ci > $br['cancellationday']){
						$cancellationfee = 0; $show_cancel_fee = "free";
					}else{
						if($br['cancellationtype'] == "full amount"){
							$cancellationfee = $br['total'];
						}else{
							$cancellationfee = 0;
							for($i = 0; $i <= $br['cancellationtypenight']; $i++){
								$date = date('Y-m-d', strtotime($br['checkin'].' +'.$i.' day'));
								$q_breakdownrate = "select total from bookingroomdtl where bookingroomoid = '".$br['bookingroomoid']."' and reconciled = '0' and `date` = '".$date."'";
								$stmt = $db->query($q_breakdownrate);
								$breakdownrate = $stmt->fetch(PDO::FETCH_ASSOC);
								$cancellationfee = $cancellationfee + $breakdownrate['total'];
							}
						}
						$show_cancel_fee = number_format($cancellationfee);
					}
				$totalcancellation = $totalcancellation + $cancellationfee;
				}
            ?>
            <?=date('d F Y')?> : <?=$before_ci?> day before checkin (Cancellation Amount <?=$show_cancel_fee?>)<br />
            </div>
            <div class="col-md-6">
              <table class="table table-striped">
                <thead><tr><th>Check in</th><th>Check Out</th><th>Rate</th></tr></thead>
                <tbody>
          <?php	
			  $q_breakdownrate = "select * from bookingroomdtl where bookingroomoid = '".$br['bookingroomoid']."' and reconciled = '0'";
			  $stmt = $db->query($q_breakdownrate);
			  $breakdownrate = $stmt->fetchAll(PDO::FETCH_ASSOC);
			  end($breakdownrate);
			  $lastkey = key($breakdownrate);
			  foreach($breakdownrate as $key => $brd){
				?>
				<tr><td><input type="hidden" name="brd[]" value="<?=$brd['bookingroomdtloid']?>"><?=date('d/M/Y', strtotime($brd['date']))?></td><td><?=date('d/M/Y', strtotime($brd['date'].' +1 day'))?></td><td class="nighttotal"><?=number_format($brd['total'])?></td></tr>
                <?php  
			  }
          ?>
				<tr><td colspan="2"><b>Total </b></td><td><?=number_format($br['roomtotalr'])?></td></tr>
                </tbody>
              </table>
            </div>
          </div>
          <?php	
			}
		  }catch(PDOException $ex) {
			echo "Invalid Query";
			print($ex ->getMessage());
			die();
		  }
          ?>
          <div class="row"><div class="col-md-12">
          <div class="bg-danger">
            <div class="col-md-6"><h4>Cancellation Amount</h4></div>
            <div class="col-md-6"><h4><?=number_format($totalcancellation)?></h4></div>
          </div>
          </div></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary submit">Reconcile</button>
      </div>
      </form>
    </div>
  </div>
</div>


<!-- Modal No Show -->
<div class="modal fade" id="noShowModal" tabindex="-1" role="dialog" aria-labelledby="noShowModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Mark as No Show</h4>
      </div>
      <div class="modal-body">
        <form method="post" action="#">
          <div class="row">
            <div class="col-md-4"><label>Booking Number</label></div>
            <div class="col-md-8"><?=$bookingnumber?></div>
          </div>
          <div class="row">
            <div class="col-md-4"><label>Note</label></div>
            <div class="col-md-8"><textarea class="form-control" name="note"></textarea></div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">No Show</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="NotificationModal" tabindex="-1" role="dialog" aria-labelledby="NotificationModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-info-circle"></i> Notification</h4>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="paymentDetail" tabindex="-1" role="dialog" aria-labelledby="paymentDetail" data-remote="modal" data-load-url="<?php echo $base_url; ?>/paymentdetail/index.php?bookingnumber=<?=$bookingnumber?>">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-info-circle"></i> Payment Detail</h4>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>