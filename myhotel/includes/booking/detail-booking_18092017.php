<link rel="stylesheet" href="<?php echo $base_url; ?>/lib/bootstrap/css/bootstrap.min.css">
<link href="<?php echo $base_url?>/css/style.css?v=1" rel="stylesheet" type="text/css">

<style type="text/css">
	#detail-booking h1, #detail-booking h2{
		margin:0;
		font-weight:100;

	}
	#detail-booking h1{ font-size: 22px; }
	#detail-booking h2{ font-size: 17px; }
	#detail-booking span.grey{
		color:#999;
	}

	#detail-booking table { border-collapse: collapse; margin-bottom:10px; }
	#detail-booking table > tbody > tr > td { border: 1px solid #C9C9C9; padding:2px; }
	#detail-booking table.ch-table { margin-bottom:0; }
	#detail-booking table.ch-table > tbody > tr > td { border-bottom: 1px solid #434444; border-right:none; border-left:none; border-top:none; }
	#detail-booking table.ch-table > tbody > tr > td:first-of-type { padding-right:10px; }
	#detail-booking table.ch-table > tbody > tr > td:nth-of-type(2) { text-align:right; }
	#detail-booking table.ch-table > tbody > tr:last-of-type > td{ border:none; }
	
	table tr.list-detail{ cursor:pointer; }
	/* table tr.list-breakdown{ display:none; } */
	
	ul.breakdown-rate li{ margin:0 0 5px; }
	ul.breakdown-rate li > label, ul.breakdown-rate li > span{ display:block; text-align:center; padding:3px 5px; }
	ul.breakdown-rate li > label{ background-color:#ced0d2; margin:0 }
	ul.breakdown-rate li > span{ border:1px solid #ced0d2; }
</style>
<?php
	include("js.php");

	$bookingnumber = $_REQUEST['no'];

	$query = "select b.bookingnumber, b.pin, b.bookingoid, b.bookingtime, DATE_FORMAT(bookingtime, '%W, %d %M %Y at %H:%i:%s') as bookdate, b.note, b.grandtotal, b.grandtotalr, b.granddeposit, b.grandbalance, b.hotelcollect, b.gbhcollect,
	bs.note as status, bs.bookingstatusoid, cur.currencycode,
	CONCAT(firstname, ' ', lastname) as guestname, c.address, c.city, c.state, c.zipcode, c.phone, c.email,
	ctr.countryname,
	h.hotelname, b.updated, b.updatedby
	from booking b
	inner join hotel h using (hoteloid)
	inner join bookingstatus bs using (bookingstatusoid)
	inner join currency cur using (currencyoid)
	inner join customer c using (custoid)
	inner join country ctr using (countryoid)
	left join bookingpayment using (bookingoid)
	where h.hoteloid = '".$hoteloid."' and b.bookingnumber = '".$bookingnumber."'
	group by b.bookingoid";

	try {
		$stmt = $db->query($query);
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
			$r_s = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_s as $row){
				$bookingoid = $row['bookingoid'];
				$bookingnumber = $row['bookingnumber'];
				$grandtotal = $row['currencycode']." ".number_format($row['grandtotal']);
				$grandbalance = $row['currencycode']." ".number_format($row['grandbalance']);
				$granddeposit = $row['currencycode']." ".number_format($row['granddeposit']);
				$hotelcollect = $row['currencycode']." ".number_format($row['hotelcollect']);
				$gbhcollect = $row['currencycode']." ".number_format($row['gbhcollect']);
				
				$s_room		=  $db->query("select sum(roomtotal) as roomtotal, sum(roomtotalr) as roomtotalr, sum(extrabedtotal) as extrabedtotal from bookingroom where bookingoid = '".$row['bookingoid']."' group by bookingoid");
				$room		= $s_room->fetch(PDO::FETCH_ASSOC);
				$roomtotal = $row['currencycode']." ".number_format($room['roomtotal']);
				$extrabedtotal = $row['currencycode']." ".number_format($room['extrabedtotal']);
				
				
				$s_extra	=  $db->query("select sum(total) as extratotal from bookingextra where bookingoid = '".$row['bookingoid']."' group by bookingoid");
				$extra		= $s_extra->fetch(PDO::FETCH_ASSOC);
				$extratotal = $row['currencycode']." ".number_format($extra['extratotal']);
				
				$note = $row['note'];
				$bookingstatus = $row['status'];
				$bookingstatusoid = $row['bookingstatusoid'];

				$updated = $row['updated'];
				$updatedby = $row['updatedby'];

				$roomtotal_r = $row['roomtotal'];
				$roomtotal_rr = $row['roomtotalr'];
?>

<section class="content-header">
    <h1>
        Booking Number : <?php echo $bookingnumber; ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i>  Reports </a></li>
        <li class="active">Booking</li>
        <li class="active">Detail <?php echo $bookingnumber; ?></li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="box box-form">
            <div class="box-body">

                <div id="detail-booking">
                    <div class="algn-center">
                        <h1><?php echo $row['guestname']; ?></h1>
                        <span style="display:none"><?php echo $row['address']; ?>, <?php echo $row['city']; ?>, <?php echo $row['state']; ?>, <?php echo $row['zipcode']; ?>, <?php echo $row['countryname']; ?></span>
                        <span><?php echo $row['city']; ?>, <?php echo $row['countryname']; ?></span>
                        <br>
                        <span class="grey">&#9742; <?php echo $row['phone']; ?> &nbsp;&nbsp; &#9993; <?php echo $row['email']; ?></span><br>
                        <span><?php echo $row['bookdate']; ?> &nbsp;&nbsp; Booking Number : <?php echo $row['bookingnumber']; ?> &nbsp;&nbsp; <i class="fa fa-lock"></i> PIN : <?php echo $row['pin']; ?></span>
                        <br>
                        <br>
                        <h2><?php echo $row['hotelname']; ?></h2>
                    </div>
                    <div>
						<?php
						$ar_bookingroom = array();
                        $ar_roomname = array(); $ar_roomoid = array();
                        $ar_checkin = array(); $ar_checkout = array(); $ar_checkoutr = array();
                        $ar_numberroom = array();
                        $ar_child = array(); $ar_adult = array();
                        $ar_total = array(); $ar_totalr = array();
                        $ar_breakfast = array();
                        $ar_extrabed = array();
						$ar_cancellationamount = array(); $ar_cancellationpolicy = array();
						$ar_promotion = array(); $ar_promotionoid = array();

                        $q_detail = "select br.*, count(bookingroomoid) as jmlroom, sum(br.extrabed) as extrabed, c.currencycode, checkin, checkout, checkoutr
                        from bookingroom br
                        inner join currency c using (currencyoid)
                        inner join booking b using (bookingoid)
                        where b.bookingoid = '".$bookingoid."'
                        group by br.bookingroomoid";
                        try {
                            $stmt = $db->query($q_detail);
                            $row_count_room = $stmt->rowCount();
                            if($row_count_room > 0) {
                                $r_detail = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                foreach($r_detail as $rp){
									array_push($ar_bookingroom, $rp['bookingroomoid']);
                                    array_push($ar_roomname, $rp['room']);
									array_push($ar_roomoid, $rp['roomofferoid']);
									array_push($ar_promotion, $rp['promotion']);
									array_push($ar_promotionoid, $rp['promotionoid']);
                                    array_push($ar_checkin, date("d F Y", strtotime($rp['checkin'])));
									array_push($ar_checkout, date("d F Y", strtotime($rp['checkout'])));
                                    array_push($ar_checkoutr, date("d F Y", strtotime($rp['checkoutr'])));
                                    array_push($ar_numberroom, $rp['jmlroom']);
                                    array_push($ar_adult, $rp['adult']);
									array_push($ar_child, $rp['child']);
									array_push($ar_total, $rp['currencycode']." ".number_format($rp['total']));
                                    array_push($ar_totalr, $rp['currencycode']." ".number_format($rp['totalr']));
									array_push($ar_cancellationamount, $rp['currencycode']." ".number_format($rp['cancellationamount']));
									array_push($ar_cancellationpolicy, $rp['cancellationname']);
                                    array_push($ar_breakfast, $rp['breakfast']);
                                    array_push($ar_extrabed, $rp['extrabed']);
                                }
                            }
                        }catch(PDOException $ex) {
                            echo "Invalid Query";
                            print($ex);
                            die();
                        }
						if($row_count_room > 0) {
                        ?>
                    	<table class="table table-fill" id="detail-booking-table">
                        	<tr>
                        	    <td>#</td>
                                <td>Room Type</td>
                                <td>Date</td>
                                <td>Occupancy</td>
                                <td>Breakfast Included</td>
                                <td>No. of Extra Bed</td>
                                <td>Total</td>
                            </tr>
							<?php
							$r = 0;
                            foreach($ar_roomname as $key => $value){

                                echo"
                                <tr class='list-detail'>
                                    <td><i class='fa fa-caret-right'></i> ".++$r."</td>
                                    <td>".$value."<br>".$ar_promotion[$key]."</td>
                                    <td>checkin : ".$ar_checkin[$key]."<br>checkout : ".$ar_checkoutr[$key]."</td>
									<td class='center'><i class='fa fa-male'></i> ".$ar_adult[$key]." <i class='fa fa-child'></i> ".$ar_child[$key]."
									</td>
                                    <td class='center'>".$ar_breakfast[$key]."</td>
                                    <td class='center'>".$ar_extrabed[$key]."</td>
									<td class='center'>".$ar_totalr[$key]."</td>
                                </tr>
                                ";
								?>
                                <tr class="list-breakdown">
                                	<td colspan="7">
                                    	<label>Breakdown Rate Room #<?=$r?></label>
                                    	<ul class="inline-block breakdown-rate">
										<?php
                                        $q_breakdownrate = "select * from bookingroomdtl where bookingroomoid = '".$ar_bookingroom[$key]."' and reconciled = '0'";
                                        $stmt = $db->query($q_breakdownrate);
                                        $breakdownrate = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                        foreach($breakdownrate as $key1 => $brd){
                                        ?>
                                            <li><label><?=date('d/M/Y', strtotime($brd['date']))?></label><span><?=$rp['currencycode']?> <?=number_format($brd['total'])?></span></li>
                                        <?php  
                                        }
                                        ?>
                                        </ul>
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                        </table>
                        <?php
						}
						?>
                    </div>
                    <div>
						<?php
                        $ar_extraname = array();
                        $ar_qty = array();
						$ar_price = array(); $ar_total_extra = array();

                        $q_detail = "select be.*, sum(be.total) as total, sum(be.deposit) as deposit, sum(be.balance) as balance, c.currencycode, e.name as extra
                        from bookingextra be
                        inner join currency c using (currencyoid)
                        inner join booking b using (bookingoid)
						inner join extra e using (extraoid)
                        where b.bookingoid = '".$bookingoid."' group by extraoid";
                        try {
                            $stmt = $db->query($q_detail);
                            $row_count = $stmt->rowCount();
                            if($row_count > 0) {
                                $r_detail = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                foreach($r_detail as $rp){
                                    array_push($ar_extraname, $rp['extra']);
									array_push($ar_qty, $rp['qty']);
									array_push($ar_price, $rp['currencycode']." ".number_format($rp['price']));
									array_push($ar_total_extra, $rp['currencycode']." ".number_format($rp['total']));
                                }
                            }
                        }catch(PDOException $ex) {
                            echo "Invalid Query";
                            print($ex);
                            die();
                        }
						if(count($ar_extraname) > 0) {
                        ?>
                    	<table class="table table-fill">
                        	<tr>
                                <td>Extra</td>
                                <td>Qty</td>
                                <td>Price</td>
                                <td>Total</td>
                            </tr>
							<?php
                            foreach($ar_extraname as $key => $value){
                                echo"
                                <tr>
                                    <td>".$ar_extraname[$key]."</td>
                                    <td class='center'>".$ar_qty[$key]."</td>
									<td>".$ar_price[$key]."</td>
									<td>".$ar_total_extra[$key]."</td>
                                </tr>
                                ";
                            }
                            ?>
                        </table>
                        <?php
						}
						?>
                    </div>
                    <div>
                        <div class="side-left">
                            <table>
                                <tr>
                                    <td>Total Cost Confirmed with Guest</td>
                                    <td>
                                        <table class="ch-table">
                                            <tr><td>Room Rate</td><td><?php echo $roomtotal; ?></td></tr>
                                            <tr><td>Room Extra Bed Rate</td><td><?php echo $extrabedtotal; ?></td></tr>
                                            <tr><td>Extra Total</td><td><?php echo $extratotal; ?></td></tr>
                                            <tr><td>Total Confirmed to Guest</td><td><?php echo $grandtotal; ?></td></tr>
                                            <tr><td>Guest Deposit</td><td><?php echo $granddeposit; ?></td></tr>
											<tr><td>Guest Balace</td><td><?php echo $grandbalance; ?></td></tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Commission Breakdown</td>
                                    <td>
                                        <table class="ch-table">
                                            <tr><td>Commissionable Amount</td><td><?php echo $grandtotal; ?></td></tr>
                                            <tr><td>Commission</td><td><?php echo $gbhcollect; ?></td></tr>
                                            <tr><td>Total Confirmed to Hotel</td><td><?php echo $hotelcollect; ?></td></tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
														<div>
															<form method="post" action="<?=$base_url?>/booking/resend-confirmation">
                                                            	<input type="hidden" name="bookingnumber" value="<?=$bookingnumber?>">
																<b>Resend Email Confirmation to :</b><br>
																<input type="text" name="resend-email" required="required">
																<button type="submit" class="small-button blue"><i class="fa fa-send"></i> Send</button>
															</form>
														</div>
                        </div>
                        <div class="side-right">
                        	<div>
                        		<button type="button" class="small-button blue opencc"><i class="fa fa-credit-card"></i>Show Payment Detail</button>
                                <?php if($bookingstatusoid != "5" and $bookingstatusoid != "7"){ ?>
                        		<button type="button" class="small-button purple" bn="<?php echo $bookingnumber; ?>" data-toggle="modal" data-target="#reconcileModal"><i class="fa fa-balance-scale"></i>Reconcile</button>
                        		<button type="button" class="small-button red" bn="<?php echo $bookingnumber; ?>" data-toggle="modal" data-target="#cancelModal" data-title="Booking Cancellation" data-status="5" data-submit="Cancel Booking"><i class="fa fa-times-circle"></i>Booking Cancellation</button>
                        		<button type="button" class="small-button green" bn="<?php echo $bookingnumber; ?>" data-toggle="modal" data-target="#cancelModal" data-title="No Show" data-status="7" data-submit="Mark No Show"><i class="fa fa-ban"></i>Mark No Show</button>
								<?php } ?>
                        	</div>
                            <table class="table table-fill">
                                <tr><td>Guest Note</td></tr>
                                <tr><td><?php echo $note; ?></td></tr>
                            </table>
                            <table class="table table-fill">
                                <tr><td>Status : <?=$bookingstatus?></td></tr>
                                <tr><td>Last Updated  <?=date('d F Y H:i:s', strtotime($updated))?> by <?=$updatedby?></td></tr>
                            </table>
                            <?php /*
                            <table class="table table-fill">
                                <tr><td>Policies</td></tr>
                                <?php
                                try {
                                    $stmt = $db->query("select cp.description
                                    from cancellationpolicy cp
                                    inner join promotion p using (cancellationpolicyoid)
                                    inner join bookingroom br using (promotionoid)
                                    inner join booking b using (bookingoid)
                                    where b.bookingoid = '".$bookingoid."'
                                    group by cp.cancellationpolicyoid");
                                    $row_count = $stmt->rowCount();
                                    if($row_count > 0) {
                                        $r_policies = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                        foreach($r_policies as $rp){
                                            echo "<tr><td>".$rp['description']."</td></tr>";
                                        }
                                    }
                                }catch(PDOException $ex) {
                                    echo "Invalid Query";
                                    print($ex);
                                    die();
                                }
                                ?>
                            </table>
							*/?>
                        </div>
                        <div class="clear"></div>
					</div>
                <?php
                            }
                        }
                    }catch(PDOException $ex) {
                        echo "Invalid Query";
                        print($ex);
                        die();
                    }
                ?>
                </div>


    <div>
    <?php
	if($bookingstatusoid == '5' or $bookingstatusoid == '7'){
	?>
        <div style="font-size:0.9em; padding:5px;">
            <h2><i class="fa fa-chevron-right"></i> History <?=$bookingstatus?></h2>
            <table class="table table-fill" id="detail-booking-table">
                <tr valign="top"><td>#</td><td>Room Type</td><td>Date</td><td>Occupancy</td><td>Breakfast Included</td><td>No. of Extra Bed</td><td>Total</td><td>Cancellation Policy</td><td>Cancellation Amount</td></tr>
                <?php
				$r = 0;
                foreach($ar_roomname as $key => $value){
                    echo"
                        <tr valign='top'>
                            <td>".++$r."</td>
                            <td>".$value."<br>".$ar_promotion[$key]."</td>
                            <td>checkin : ".$ar_checkin[$key]."<br>checkout : ".$ar_checkoutr[$key]."</td>
                            <td><i class='fa fa-male'></i> ".$ar_adult[$key]." <i class='fa fa-child'></i> ".$ar_child[$key]."</td>
                            <td>".$ar_breakfast[$key]."</td>
                            <td>".$ar_extrabed[$key]."</td>
                            <td>".$ar_totalr[$key]."</td>
							<td>".$ar_cancellationpolicy[$key]."</td>
							<td>".$ar_cancellationamount[$key]."</td>
                        </tr>";
                }
				?>
            </table>
            <table width="100%">
                <tr valign="top"><td>
                            <table width="100%">
                                <tr valign="top">
                                    <td>Total Cost Confirmed with Guest</td>
                                    <td>
                                        <table class="ch-table">
                                            <tr><td>Room Rate</td><td><?php echo $roomtotal; ?></td></tr>
                                            <tr><td>Room Extra Bed Rate</td><td><?php echo $extrabedtotal; ?></td></tr>
                                            <tr><td>Extra Total</td><td><?php echo $extratotal; ?></td></tr>
                                            <tr><td>Total Confirmed to Guest</td><td><?php echo $grandtotal; ?></td></tr>
                                            <tr><td>Guest Deposit</td><td><?php echo $granddeposit; ?></td></tr>
											<tr><td>Guest Balace</td><td><?php echo $grandbalance; ?></td></tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                </td><td>
                            <table width="100%">
                                <tr valign="top">
                                    <td>Commission Breakdown</td>
                                    <td>
                                        <table class="ch-table">
                                            <tr><td>Commissionable Amount</td><td><?php echo $grandtotal; ?></td></tr>
                                            <tr><td>Commission</td><td><?php echo $gbhcollect; ?></td></tr>
                                            <tr><td>Total Confirmed to Hotel</td><td><?php echo $hotelcollect; ?></td></tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                </td></tr>
            </table>
        </div>
    <?php
	}
	?>
        <?php
        $query_room_reconcilled = "select count(bookingroomdtloid) as reconciled from bookingroomdtl brd inner join bookingroom br using (bookingroomoid) where reconciled = '1' and bookingoid = '".$bookingoid."'";
        try {
            $stmt = $db->query($query_room_reconcilled);
            $row_count = $stmt->rowCount();
			$countreconcile = $stmt->fetch(PDO::FETCH_ASSOC);
            if($countreconcile['reconciled']> 0){
        ?>
        <div style="font-size:0.9em">
            <h2><i class="fa fa-chevron-right"></i> History Reconcile</h2>
            <table class="table table-fill" id="detail-booking-table">
                <tr valign="top"><td>#</td><td>Room Type</td><td>Date</td><td>Occupancy</td><td>Breakfast Included</td><td>No. of Extra Bed</td><td>Total</td></tr>
                <?php
				$r = 0;
                foreach($ar_roomname as $key => $value){
                    echo"
                        <tr valign='top'>
                            <td>".++$r."</td>
                            <td>".$value."<br>".$ar_promotion[$key]."</td>
                            <td>checkin : ".$ar_checkin[$key]."<br>checkout : ".$ar_checkout[$key]."</td>
                            <td><i class='fa fa-male'></i> ".$ar_adult[$key]." <i class='fa fa-child'></i> ".$ar_child[$key]."</td>
                            <td>".$ar_breakfast[$key]."</td>
                            <td>".$ar_extrabed[$key]."</td>
                            <td>".$ar_total[$key]."</td>
                        </tr>";
                }
				?>
            </table>
            <table width="100%">
                <tr valign="top"><td>
                            <table width="100%">
                                <tr valign="top">
                                    <td>Total Cost Confirmed with Guest</td>
                                    <td>
                                        <table class="ch-table">
                                            <tr><td>Room Rate</td><td><?php echo $roomtotal; ?></td></tr>
                                            <tr><td>Room Extra Bed Rate</td><td><?php echo $extrabedtotal; ?></td></tr>
                                            <tr><td>Extra Total</td><td><?php echo $extratotal; ?></td></tr>
                                            <tr><td>Total Confirmed to Guest</td><td><?php echo $grandtotal; ?></td></tr>
                                            <tr><td>Guest Deposit</td><td><?php echo $granddeposit; ?></td></tr>
											<tr><td>Guest Balace</td><td><?php echo $grandbalance; ?></td></tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                </td><td>
                            <table width="100%">
                                <tr valign="top">
                                    <td>Commission Breakdown</td>
                                    <td>
                                        <table class="ch-table">
                                            <tr><td>Commissionable Amount</td><td><?php echo $grandtotal; ?></td></tr>
                                            <tr><td>Commission</td><td><?php echo $gbhcollect; ?></td></tr>
                                            <tr><td>Total Confirmed to Hotel</td><td><?php echo $hotelcollect; ?></td></tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                </td></tr>
            </table>
        </div>
        <?php
                }
        }catch(PDOException $ex) {
              echo "Invalid Query";
              print($ex);
              die();
		}
	?>
			</div>
       </div>
    </div>

</section>


<form method="post" id="TheForm" action="<?php echo"$base_url"; ?>/paymentdetail" target="_blank">
<input type="hidden" name="bookingnumber" value="<?=$bookingnumber?>" />
</form>
<script src="<?php echo $base_url; ?>/lib/bootstrap/js/bootstrap.min.js"></script>
<?php include('modal.php'); ?>
