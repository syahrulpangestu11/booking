<?php
$resend_email = $_POST['resend-email'];
$resend_name = $resend_email;
$bookingnumber = $_POST['bookingnumber'];

$query_booking	= "select b.* from booking b where b.bookingnumber = '".$bookingnumber."'";
$stmt           = $db->query($query_booking);
$result_booking = $stmt->fetch(PDO::FETCH_ASSOC);
$bookingoid     = $result_booking['bookingoid'];
?>
<script type="text/javascript">
	var $dialogNotice = $('<div id="dialog-notice"></div>')
	.dialog({
		autoOpen: false,
		title: 'Notification',
		buttons: {
			"Ok": function(){
				$( this ).dialog("close");
				<?php if($bookingoid != 0 or !empty($bookingoid)){ ?>
				window.location.href = "<?=$base_url;?>/booking/detail/?no=<?=$bookingnumber;?>";
				<?php }else{ ?>
				window.location.href = "<?=$base_url;?>/booking";
				<?php } ?>
			}
		}
	});
</script>
<?php
if($bookingoid != 0 or !empty($bookingoid)){
	ob_start();
	  include '../mail/email-confirm-reservation.php';
	$body = ob_get_clean();
	$subject = "Booking Confirmation for ".$hotelname." ".$bookingnumber;
	
	require_once('includes/mailer/class.phpmailer.php');
	$mail = new PHPMailer(true);
	
	$mail->IsSMTP(); // telling the class to use SMTP

	try{

	$mail->SMTPDebug = 0;
	//Ask for HTML-friendly debug output
	$mail->Debugoutput = 'html';
	//Set the hostname of the mail server
	$mail->Host = "smtp.gmail.com";
	//Set the SMTP port number - likely to be 25, 465 or 587
	$mail->SMTPSecure = 'tls';
	$mail->Port = 587;
	//Whether to use SMTP authentication
	$mail->SMTPAuth = true;
	//Username to use for SMTP authentication
	$mail->Username = "booking@thebuking.com";//"info@thebuking.com";
	//Password to use for SMTP authentication
	$mail->Password = "Res3!4nd";//"KofiEn4k";
	
	  $syntax_mail_address = "SELECT * FROM mailer INNER JOIN mailertype USING (mailertypeoid) INNER JOIN mailermail USING (mailermailoid) WHERE mailerpage = 'booking-guest' AND status = '1'";
	  $stmt	= $db->query($syntax_mail_address);
	  $result_email = $stmt->fetchAll(PDO::FETCH_ASSOC);
	  foreach($result_email as $email_list){
		switch($email_list['mailertypeoid']){
		  case 1 : $mail->SetFrom($email_list['email'], $email_list['name']); break;
		  case 2 : $mail->AddAddress($email_list['email'], $email_list['name']); break;
		  case 3 : $mail->AddCC($email_list['email'], $email_list['name']); break;
		  case 4 : $mail->AddBCC($email_list['email'], $email_list['name']); break;
		  case 5 : $mail->AddReplyTo($email_list['email'], $email_list['name']); break;
		}
	  }
	
	  $mail->AddAddress($resend_email, $resend_name);
	
	  // --- Hotel Contact ---
	  $s_hc = "SELECT * FROM hotelcontact hc WHERE hc.hoteloid = '".$hoteloid."' AND hc.publishedoid = '1'";
	  $q_hc	= $db->query($s_hc);
	  $r_hc = $q_hc->fetchAll(PDO::FETCH_ASSOC);
	  foreach($r_hc as $email_list){
		switch($email_list['mailertypeoid']){
		  case 1 : $mail->SetFrom($email_list['email'], $email_list['name']); break;
		  case 2 : $mail->AddAddress($email_list['email'], $email_list['name']); break;
		  case 3 : $mail->AddCC($email_list['email'], $email_list['name']); break;
		  case 4 : $mail->AddBCC($email_list['email'], $email_list['name']); break;
		  case 5 : $mail->AddReplyTo($email_list['email'], $email_list['name']); break;
		}
	  }
	
	
	  $mail->Subject = $subject;
	  $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
	  $mail->MsgHTML($body);
	
	  $mail->Send();
	  
	  $dialogNotice = "Email has been sent to ".$resend_email;

	}catch (phpmailerException $e) {
		$dialogNotice = "Sorry we can't re-send your email right now. Please make sure you already give right email address";
	}catch (Exception $e) {
		$dialogNotice = "Sorry we can't re-send your email right now. Please make sure you already give right email address";
	}
}else{
  $dialogNotice = "Please select your reference booking first";
}
?>

<script type="text/javascript">
$(document).ready(function(){ 
	$dialogNotice.html("<?=$dialogNotice?>");
	$dialogNotice.dialog("open");
});
</script>
