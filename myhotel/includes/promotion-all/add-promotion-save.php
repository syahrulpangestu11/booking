<?php
	//$promotype = $_POST['promotype'];
	$promotype = 1;
	$name = (isset($_POST['name'])) ? $_POST['name'] : "";

	$salefrom = date("Y-m-d", strtotime($_POST['salefrom']));
	$saleto = date("Y-m-d", strtotime($_POST['saleto']));
	$stayfrom = date("Y-m-d", strtotime($_POST['stayfrom']));
	$stayto = date("Y-m-d", strtotime($_POST['stayto']));

		$display = array();
		foreach($_POST['display'] as $key => $value){
			array_push($display , $value);
		}
	$display = implode(",", $display);

		$checkin = array();
		foreach($_POST['checkin'] as $key => $value){
			array_push($checkin , $value);
		}
	$checkin = implode(",", $checkin);

	$minstay = (isset($_POST['minstay'])) ? $_POST['minstay'] : "0";
	$maxstay = (isset($_POST['maxstay'])) ? $_POST['maxstay'] : "0";
	$timefrom = date("H:i", strtotime($_POST['timefrom']));
	$timeto = date("H:i", strtotime($_POST['timeto']));

	$discounttype = $_POST['discounttype'];
	$discountapply = $_POST['discountapply'];
	if($discountapply == '2' or $discountapply == '3'){
		if(is_array($_POST['applyvalue'])){
				$applyvalue = array();
				foreach($_POST['applyvalue'] as $key => $value){
					array_push($applyvalue , $value);
				}
			$applyvalue = implode(",", $applyvalue);
		}else{
			$applyvalue = $_POST['applyvalue'];
		}
	}else{
		$applyvalue = "";
	}
	$discountvalue = $_POST['discountvalue'];
	$minroom = $_POST['minroom'];
	$cancellationpolicy = $_POST['cancellation'];
	$priority = $_POST['priority'];

	$deal = $_POST['deal'];
	$deal_commission = $_POST['deal_commission'];

	$headline = $_POST['headline'];
	$description = $_POST['description'];
	$servicefacilities = $_POST['servicefacilities'];
	$termcondition = $_POST['termcondition'];

	$deposit = $_POST['deposit'];
	$depositvalue = (isset($_POST['depositvalue'])) ? $_POST['depositvalue'] : 0;

	$allowedExtension = array('jpg', 'jpeg', 'png');
	$tmp = explode('.', $_FILES['image']['name']);
	$extension = end($tmp);
	$image = '';
	if(!empty($_FILES['image'])){
		if(in_array($extension, $allowedExtension)){
			$imagename = str_replace(' ', '_', $_FILES['image']['name']);
			$imagename = str_replace('.'.$extension, '', $imagename);
			$ext = '.'.strtolower($extension);
			$image_new_name = substr($imagename, 0, 10).date('Ymd_His').$ext;
			$folder_destination = 'pict/promoimage/';

			move_uploaded_file($_FILES['image']['tmp_name'], $upload_base.$folder_destination.$image_new_name);
			$image = $web_url.'/'.$folder_destination.$image_new_name;
		}else{
			echo'<script>alert("Data image not saved.\\nInvalid file.")</script>';
		}
	}

	$min_ci = $_POST['min_ci'];
	$max_ci = $_POST['max_ci'];

	$popupbanner = $_POST['popupbanner'];
	$shown_no_pc = $_POST['shown_no_pc'];

	try {
		$stmt = $db->prepare("insert into promotion (hoteloid, promotiontypeoid, name, minstay, salefrom, saleto, bookfrom, bookto, displayed, checkinon, maxstay, timefrom, timeto, discounttypeoid, discountapplyoid, applyvalue, discountvalue, minroom, cancellationpolicyoid, publishedoid, priority, headline, description, servicefacilities, termcondition, depositoid, depositvalue, promoimage, min_bef_ci, max_bef_ci, popupbanner, shown_no_pc) values (:a,:b,:c,:d,:e,:f,:g,:h,:i,:j,:k,:l,:m,:n,:o,:p,:q,:r,:s,:t,:u,:v,:w,:x,:y,:z,:aa,:ab,:ac,:ad,:ae,:af)");
		$stmt->execute(array(':a' => $hoteloid, ':b' => $promotype, ':c' => $name, ':d' => $minstay, ':e' => $salefrom, ':f' => $saleto, ':g' => $stayfrom, ':h' => $stayto, ':i' => $display, ':j' => $checkin, ':k' => $maxstay, ':l' => $timefrom, ':m' => $timeto, ':n' => $discounttype, ':o' => $discountapply, ':p' => $applyvalue, ':q' => $discountvalue, ':r' => $minroom, ':s' => $cancellationpolicy, ':t' => '1' , ':u' => $priority, ':v' => $headline, ':w' => $description, ':x' => $servicefacilities, ':y' => $termcondition, ':z' => $deposit, ':aa' => $depositvalue, ':ab' => $image, ':ac' => $min_ci, ':ad' => $max_ci, ':ae' => $popupbanner, ':af' => $shown_no_pc));
		$promotionoid = $db->lastInsertId();
	}catch(PDOException $ex) {
		echo "Invalid Query";
		print($ex);
		die();
	}

	include('reorder-priority.php');
	
	if(isset($_POST['country'])){
		foreach($_POST['country'] as $key => $countryoid){
			try {
				$stmt = $db->prepare("insert into promotiongeo (promotionoid, countryoid) values (:a,:b)");
				$stmt->execute(array(':a' => $promotionoid , ':b' => $countryoid));
			}catch(PDOException $ex) {
				echo "Invalid Query";
				print($ex);
				die();
			}
		}
	}

	if(isset($_POST['noshow_country'])){
		foreach($_POST['noshow_country'] as $key => $noshow_countryoid){
			try {
				$stmt = $db->prepare("insert into promotiongeo_noshow (promotionoid, countryoid) values (:a,:b)");
				$stmt->execute(array(':a' => $promotionoid , ':b' => $noshow_countryoid));
			}catch(PDOException $ex) {
				echo "Invalid Query";
				print($ex);
				die();
			}
		}
	}


	if(isset($_POST['rateplan'])){
		foreach($_POST['rateplan'] as $key => $roomoffer){
			foreach($_POST['channel-'.$roomoffer] as $key1 => $channel){
				try {
					$stmt = $db->prepare("insert into promotionapply (promotionoid, roomofferoid, channeloid, publishedoid) values (:a,:c,:d,:e)");
					$stmt->execute(array(':a' => $promotionoid , ':c' => $roomoffer, ':d' => $channel, ':e' => '1'));
				}catch(PDOException $ex) {
					echo "Invalid Query";
					print($ex);
					die();
				}
			}
		}
	}

	if($deal == "yes" and !empty($deal)){
		try {
			$stmt = $db->prepare("insert into dealspromotion (promotionoid, commission, publishedoid) values (:a,:b,:c)");
			$stmt->execute(array(':a' => $promotionoid, ':b' => $deal_commission, ':c' => 1));
		}catch(PDOException $ex) {
			echo "Invalid Query";
			print($ex);
			die();
		}
	}

	$type = 'promotion';
	foreach ($_POST['icon'] as $key => $value) {
		$iconoid = $value;
		try {
			$stmt = $db->prepare("insert into termheadlinepromo (type, id, termheadlineoid) values (:a,:b,:c)");
			$stmt->execute(array(':a' => $type, ':b' => $promotionoid, ':c' => $iconoid));
		}catch(PDOException $ex) {
			echo "Invalid Query";
			print($ex);
			die();
		}
	}
	
	/* PROMO CODE ------------------------------------------*/
	if(isset($_POST['pc'])){
		foreach($_POST['pc'] as $key => $promocodeoid){
			$pcdiscount		= (isset($_POST['pcdiscount-'.$promocodeoid])) ? $_POST['pcdiscount-'.$promocodeoid] : "n";
			$pccommission	= (isset($_POST['pccommission-'.$promocodeoid])) ? $_POST['pccommission-'.$promocodeoid] : "n";
			
			$stmt = $db->prepare("insert into promocodeapply (promocodeoid, referencetable, id, applydiscount, applycommission) values (:a, :b, :c, :d, :e)");
			$stmt->execute(array(':a' => $promocodeoid, ':b' => 'promotion', ':c' => $promotionoid, ':d' => $pcdiscount, ':e' => $pccommission, ));
		}
	}
	/* ------------------------------------------*/

	header("Location: ". $base_url ."/promotions");
?>
