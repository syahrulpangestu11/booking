<div class="row">
  <div class="col-sm-12">
      <div class="box box-primary">
        <div class="box-header">
          <h4>Production Summary from <?=date('d F Y', strtotime($start))?> to <?=date('d F Y', strtotime($end))?></h4>
          <i>(Table View)</i>
        </div>
        <div class="box-body">

<table id="example" cellspacing="0" class="table table-striped">
  <thead>
    <tr>
        <th>Hotel Name</th>
        <th>Commission</th>
        <th>Revenue</th>
        <th>Cancelled Value</th>
        <th>Net to Hotel</th>
        <th>Visits</th>
        <th>Bookings</th>
        <th>Confirmed</th>
        <th>Cancelled</th>
        <th>R/N</th>
        <th>Conversion Rate</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $s_list_hotel_chain = "select hoteloid, hotelname from hotel h inner join chain c using (chainoid) where h.chainoid = '".$chainoid."'";
    $stmt = $db->query($s_list_hotel_chain);
    $hotel_chain = $stmt->fetchAll(PDO::FETCH_ASSOC);
    foreach($hotel_chain as $row){
      $hoteloid = $row['hoteloid'];
      $hotelname = $row['hotelname'];
      $jml_visit = 0; $jml_booking = 0; $jml_confirm = 0; $jml_cancel = 0; $jml_roomnight = 0;
      $totalreservation = 0; $totalconfirmed = 0; $totalcancelled = 0; $totalnoshow = 0;

      $jml_visit		= getVisit($hoteloid, "availability", $start, $end);
			$jml_booking	= countResult($start, $end, $hoteloid, '', array(4,5,7,8));
			$jml_confirm	= countResult($start, $end, $hoteloid, '', 4);
      $jml_cancel 	= countResult($start, $end, $hoteloid, '', 5,7,8);
			$jml_roomnight	= countRoomNight($start, $end, $hoteloid, '', '');

      $nethotel = $revenue - $commission;

      $commission = SumCommission($start, $end, $hoteloid);
      $revenue = SumRevenue($start, $end, $hoteloid);
      $cancellationamount = SumCancellationAmount($start, $end, $hoteloid);
      $nethotel = SumNetHotel($start, $end, $hoteloid);

      if($jml_visit > 0 and $jml_confirm > 0){
				$conversion = number_format($jml_confirm / $jml_visit * 100,2,",",".");
			}else{
				$conversion = 0;
			}
    ?>
    <tr>
      <td><?=$row['hotelname']?></td>
      <td><?=$commission?></td>
      <td><?=$revenue?></td>
      <td><?=$cancellationamount?></td>
      <td><?=$nethotel?></td>
      <td><?=$jml_visit?></td>
      <td><?=$jml_booking?></td>
      <td><?=$jml_confirm?></td>
      <td><?=$jml_cancel?></td>
      <td><?=$jml_roomnight?></td>
      <td><?=$conversion?> %</td>
    </tr>
    <?php
    }
    ?>
  </tbody>
</table>
      </div>
    </div>
  </div>
</div>
