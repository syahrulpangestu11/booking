<script type="text/javascript">
$(function(){
	$('#dashboard .dashboard-item').click(function(){
		linking = $(this).attr('link');
		if(linking !== ''){
			$(location).attr("href", linking);
		}
	});
});
</script>
<div id="dashboard">
    <ul class="inline-block one-column">
			<li class="dashboard-item box red one-third" link="<?php echo $base_url; ?>/hotel">
        <div class="left"><i class="fa fa-2x fa-bed"></i></div>
        <div class="right">
					<h1 class="title-1">Manage</h1>
					<h1 class="title-2">Hotel</h1>
				</div>
      </li>
      <li class="dashboard-item box blue one-third" link="<?php echo $base_url; ?>/user">
        <div class="left"><i class="fa fa-2x fa-user"></i></div>
        <div class="right">
					<h1 class="title-1">Manage</h1>
					<h1 class="title-2">User</h1>
				</div>
      </li>
    </ul>
</div>
