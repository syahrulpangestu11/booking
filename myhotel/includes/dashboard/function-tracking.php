<?php
    $path = "../ibe/tracking-analytics/log-tracking.xml";

    $xmllog = new DomDocument();
    $xmllog->preserveWhitespace = false;
    $xmllog->load($path);
    
    $xpath = new DomXpath($xmllog);
    
    
	
	$pie_color = array( "#f56954", "#00a65a", "#f39c12", "#00c0ef", "#3c8dbc", "#d2d6de");

	$browser = array();
	$query_get_browser = '//record[@hotel="'.$hoteloid.'"][@page="availability"][number(translate(@date,"-","")) >= '.$start.' and number(translate(@date,"-","")) <=  '.$end.']/browser/text()';
	foreach($xpath->query($query_get_browser) as $tag_browser){
		if(strlen($tag_browser->textContent) < 50 == true){
			if(in_array_multidimension($tag_browser->textContent, $browser)){
				$list_key_browser = array_column($browser, 'name');
				$key = array_search($tag_browser->textContent, $list_key_browser);
	
				$used = $browser[$key]['used'] + 1;
				$browser[$key]['used'] = $used;
			}else{
				array_push($browser, array('name' => $tag_browser->textContent, 'used' => 1));
	
				end($browser);
				$key = key($browser);
				$browser[$key]['color'] = $pie_color[$key];
			}
		}
	}
	
	$referal_website = getReferalSource($hoteloid, array('utm_source', 'tb_source'), 'availability', $start, $end);

?>
