<?php

function getHotelStaahConnect($hoteloid){
	global $db;
	
    $stmt = $db->prepare("SELECT `staah_connect` FROM `hotel` WHERE `hoteloid`=:a");
    $stmt->execute(array(':a' => $hoteloid));
    $r_checkexist = $stmt->fetch(PDO::FETCH_ASSOC);
    
    if($r_checkexist['staah_connect'] == '1'){
        return true;
    }else{
        return false;
    }
}

function getRoomData($hoteloid, $roomoid, $date){
	global $db;
    
    $stmt = $db->prepare("SELECT `allotment`, `closeoutroom` FROM `staah_roomtype` WHERE `hoteloid`=:a AND `roomoid`=:b AND `sdate`=:c AND `status`='1'");
    $stmt->execute(array(':a' => $hoteloid, ':b' => $roomoid, ':c' => $date));
    $r_checkexist = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
    if(count($r_checkexist) > 0){
        foreach($r_checkexist as $row){
            if(is_null($row['allotment'])){
                $allotment = "";
            }else{
                $allotment = $row['allotment'];
            }
            
            if(is_null($row['closeoutroom'])){
                $closeoutroom = "";
            }else{
                $closeoutroom = $row['closeoutroom'];
            }
        }
    }else{
        $allotment = "";
        $closeoutroom = "";
    }
    
    
    return array($allotment, $closeoutroom);
}

function getRateData($hoteloid, $roomoid, $roomofferoid, $date){
	global $db;
	
    $stmt = $db->prepare("SELECT `rate`, `closeoutrate`, `cta`, `ctd` FROM `staah_rateplan` WHERE `hoteloid`=:a AND `roomoid`=:b AND `roomofferoid`=:c AND `sdate`=:d AND `status`='1'");
    $stmt->execute(array(':a' => $hoteloid, ':b' => $roomoid, ':c' => $roomofferoid, ':d' => $date));
    $r_checkexist = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
    if(count($r_checkexist) > 0){
        foreach($r_checkexist as $row){
            if(is_null($row['rate'])){
                $rate = "";
            }else{
                $rate = $row['rate'];
            }
            
            if(is_null($row['closeoutrate'])){
                $closeoutrate = "";
            }else{
                $closeoutrate = $row['closeoutrate'];
            }
            
            if(is_null($row['cta'])){
                $cta = "";
            }else{
                $cta = $row['cta'];
            }
            
            if(is_null($row['ctd'])){
                $ctd = "";
            }else{
                $ctd = $row['ctd'];
            }
        }
    }else{
        $rate = "";
        $closeoutrate = "";
        $cta = "";
        $ctd = "";
    }
    
    
    return array($rate, $closeoutrate, $cta, $ctd);
}

?>