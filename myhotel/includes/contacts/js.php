<script type="text/javascript">
$(function(){

	function submitDataCallback(url, forminput){
		$.ajax({
			url: url,
			type: 'post',
			data: forminput.serialize(),
			success: function(data) {
				if(data == "1"){
					$dialogNotice.html("Data succesfully updated");
					$dialogNotice.dialog("open");
				}else{
					$dialogNotice.html("We&rsquo;re very sorry, we can't save your data right now"+data);
					$dialogNotice.dialog("open");
				}
			}
		});
	}


	var $dialogNotice = $('<div id="dialog-notice"></div>')
	.dialog({
		autoOpen: false,
		title: 'Notification',
		buttons: {
			"Ok": function(){
				$( this ).dialog( "close" );
				document.location.href = "<?php echo $base_url; ?>/hotel";
			}
		}
	});

	/************************************************************************************/

	$('body').on('click','button.add-button', function(e) {
		url = "<?php echo $base_url; ?>/contacts/add";
      	$(location).attr("href", url);
	});

	$('body').on('click','button.edit-button', function(e) {
		hoid = $(this).attr("hoid");
		url = "<?php echo $base_url; ?>/contacts/edit/?ho="+hoid;
		$(location).attr("href", url);
	});

	$('body').on('click','button.cancel-edit', function(e) {
		url = "<?php echo $base_url; ?>/contacts";
      	$(location).attr("href", url);
	});

	$('body').on('click','button.edit-contact', function(e) {
		href = $(this).attr("href");
		url = "<?php echo $base_url; ?>"+href;
		$(location).attr("href", url);
	});

	$('body').on('click','button.edit-hotel', function(e) {
		href = $(this).attr("href");
		url = "<?php echo $base_url; ?>"+href;
		$(location).attr("href", url);
	});

	$('button.submit-add').click(function(){
		url = '<?php echo $base_url; ?>/includes/contacts/add-contacts-save.php';
		forminput = $('form#data-input');
		submitDataCallback(url, forminput);
	});

	$('button.submit-edit').click(function(){
		url = '<?php echo $base_url; ?>/includes/contacts/edit-contacts-save.php';
		forminput = $('form#data-input');
		submitDataCallback(url, forminput);
	});

	$('body').on('click','button.manage', function(e) {
		var loginas_hc = $(this).attr('hoid');
		var loginas_hname = $(this).parent().parent().children('td').eq(0).html();
		$.ajax({
			type	: 'POST', cache: false,
			url		: '<?php echo"$base_url"; ?>/includes/contacts/change-session.php',
			data	: { loginas : loginas_hc, hname : loginas_hname },
			success	: function(rsp){ if(rsp == "1"){ document.location.href = '<?php echo"$base_url"; ?>/contact-management'; } }
		});
	});


	/************************************************************************************/
	$('body').on('click','button.delete-button', function(e) {
		id = $(this).attr("hoid");
		$dialogDelete.data("url", "<?php echo $base_url; ?>/includes/contacts/delete-contacts.php");
		$dialogDelete.data("id", id);
		$dialogDelete.dialog("open");
	});

	/************************************************************************************/
	$('body').on('click','#paging button', function(e) {
		page	= $(this).attr('page');
		pagebox = $('form#data-input').children('input[name=page]');
		pagebox.val(page);
		getLoadData();
	});


	/************************************************************************************/

	$(document).ready(function(){
		getLoadData();
		// $('select[name=country]').change();
	});

	/************************************************************************************/
	function getLoadData(){
		$.ajax({
			url: "<?=$base_url;?>/includes/contacts/data.php",
			type: 'get',
			data: $('form#data-input').serialize()+"&usr=<?=$_SESSION['_typeusr']?>&useroid=<?=$_SESSION['_oid']?>",
			success: function(data) {
				$("#data-box").html(data)
			}
		});
	}
	
	/************************************************************************************/
	function deleteAction(url, id){
		$.ajax({
			url: url,
			type: 'post',
			data: { id : id},
			success: function(data) {
				if(data == "1"){
					$dialogNotification.html("Data has been deleted");
					$dialogNotification.dialog("open");
				}else{
					$dialogNotification.html("We&rsquo;re very sorry, we can't save your data right now");
					$dialogNotification.dialog("open");
				}
			}
		});
	}


   var $dialogDelete = $('<div id="dialog-del"></div>')
	.html('Are you sure you want to delete this data?')
	.dialog({
		autoOpen: false,
		title: 'Confirmation',
		buttons: {
			"Delete": function(){
				var url = $(this).data('url');
				var id = $(this).data('id');
				$(this).dialog("close");
				deleteAction(url, id);
			},
			Cancel: function(){ $( this ).dialog("close"); }
		}
	});

	var $dialogNotification = $('<div id="dialog-notice"></div>')
	.dialog({
		autoOpen: false,
		title: 'Notification',
		buttons: {
			"Ok": function(){
				var callback = '<?php echo $base_url; ?>/hotel';
				$(this).dialog( "close" );
				$(location).attr("href", callback);
			}
		}
	});


	

});
</script>
