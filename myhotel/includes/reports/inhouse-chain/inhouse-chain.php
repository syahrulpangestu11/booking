<?php
  include('includes/bootstrap.php');
  include('script.php');
?>
<style type="text/css">
table#inventory{
	width:100%;
	border-collapse:collapse;
	font-size:0.85em;
	border-spacing:0;
}
table#inventory, table#inventory tr, table#inventory th, table#inventory td{
	border:0.2px solid #E1E1E1;
}
table#inventory thead tr th, table#inventory tbody tr td{
	padding:2px;
}
table#inventory thead tr{
	background-color:#0e1a2b;
	color:#fff;
}
table#inventory tbody tr.hotel{
	background-color:#f4f8fb;
}
table#inventory tbody tr.hotel td:first-of-type{
  padding-left: 5px;
  font-size:1.2em;
  font-weight:bold;
}
table#inventory tbody tr.roomtype{
	background-color:#f4f8fb;

}
table#inventory tbody tr.roomtype td:first-of-type{
    padding-left: 10px;
    font-size:1.1em;
  	font-weight:bold;
}
table#inventory tbody tr.roomoffer{
	color:#333;
}
table#inventory tbody tr.roomoffer td:first-of-type{
  font-size:1.1em;
	font-weight:bold;
}
table#inventory tr td:nth-child(n+2){
	text-align:center;
}
table#inventory tr:not(.roomtype) td:first-of-type{
	padding-left:40px;
}
table#inventory tr.roomoffer td:first-of-type{
	padding-left:15px;
}
table#inventory tr.roomtype td:nth-child(n+2), table#inventory tr.roomoffer td:nth-child(n+2){
	padding:0;
}
table#inventory tr td div.closeout{
	width:100%;
	height:100%;
	padding:0; margin:0;
	cursor:pointer;
}
table#inventory tr td div.closeout.open, table#inventory tr td div.closeout.close:hover{
	border: 1px solid #0C3;
	background-color:#0C6;
}
table#inventory tr td div.closeout.close, table#inventory tr td div.closeout.open:hover{
	border: 1px solid #C00;
	background-color:#F03;
}

table#inventory input[type=text]{
	display:none;
    font-size: 1em;
    padding: 2px 5px;
    max-width: 70px;
	border-radius:0;
	border: 1px solid #999;
}
input#datepicker{ width:145px; padding: 2px 5px; }

.close{
  opacity: 1;
  font-size: inherit;
  line-height: inherit;
}
</style>
<section class="content-header">
    <h1>Inhouse by Chain</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i>  Inhouse by Chain</a></li>
    </ol>
</section>
<section class="content">
  <div class="box box-form">
    <div class="box-body">
      <form class="form-inline">
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label for="exampleInputName2">Hotel Chain : </label>
              <select class="form-control" name="hotelcode">
                <option value="" selected>Show All Hotel</option>
                <?php
                  $s_hotelchain = "select h.hoteloid, h.hotelcode, h.hotelname from hotel h inner join chain c using (chainoid) where h.chainoid in (select oid from userassign where type = 'chainoid' and useroid = '".$_SESSION['_oid']."') and h.publishedoid = '1'";
                  $q_hotelchain = $db->query($s_hotelchain);
                  $r_hotelchain = $q_hotelchain->fetchAll(PDO::FETCH_ASSOC);
                  foreach($r_hotelchain as $hotelchain){
                ?>
                  <option value="<?=$hotelchain['hotelcode']?>"><?=$hotelchain['hotelname']?></option>
                <?php
                  }
                ?>
              </select>
            </div>
          </div>
          <div class="col-md-6 text-right">
            <div class="form-group">
              <button type="button" class="btn btn-primary inv-nav" value="backward"><i class="fa fa-angle-double-left"></i></button>
              <button type="button" class="btn btn-primary inv-nav" value="previous"><i class="fa fa-angle-left"></i></button>
              <input type="text"  class="form-control" name="date" id="datepicker" value="<?=date('d F Y');?>" />
              <button type="button" class="btn btn-primary inv-nav" value="next"><i class="fa fa-angle-right"></i></button>
              <button type="button" class="btn btn-primary inv-nav" value="forward"><i class="fa fa-angle-double-right"></i></button>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
  <div class="box box-form">
    <div class="box-body">
      <div class="row">
        <div class="col-md-12">
          <div id="show-inventory"></div>
        </div>
      </div>
    </div>
  </div>
</section>
