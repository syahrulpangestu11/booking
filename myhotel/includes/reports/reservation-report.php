<section class="content-header">
    <h1>Reservation Report</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-bar-chart-o"></i> Performance Report</a></li>
        <li class="active">Reservation Report</li>
    </ol>
</section>
<section class="content">
    <div id="box"><?php include('reservation-report-include.php'); ?></div>
</section>