<?php
include("../../../conf/connection.php");
include("../../php-function.php");

$base_url = $_POST['base'];

$hotelcode = $_POST['hcode'];
$stmt = $db->query("select hoteloid from hotel h where h.hotelcode = '".$hotelcode."'");
$hotel = $stmt->fetch(PDO::FETCH_ASSOC);
$hoteloid = $hotel['hoteloid'];

$lastdate = $_POST['lastdate'];
$request = $_POST['request'];
$channel = 1;

$xml_tarif =  simplexml_load_file("../../../data-xml/".$hotelcode.".xml");
$xml_external = array();

function getAllocationXML($hotel, $room, $date, $channel){
	global $db;
	global $xml_tarif;
	global $xml_external;

	$allocation =0;
	$existtag = 0;

	$query_tag = '//hotel[@id="'.$hotel.'"]/masterroom[@id="'.$room.'"]/rate[@date="'.$date.'"]/allotment/channel[@type="'.$channel.'"]';
	foreach($xml_tarif->xpath($query_tag) as $allotment){
		$allocation = $allotment;
		$existtag = 1;
	}

	return array($allocation, $existtag);
}

switch($request){
	case 'next' : $begin = 1 ; $end = 7; break;
	case 'forward' : $begin = 10 ; $end = 16; break;
	case 'previous' : $begin = -1 ; $end = 5; break;
	case 'backward' : $begin = -10 ; $end = -4; break;
	default :  $begin = 0; $end = 6; break;
}

$showDay = array(); $showDate = array(); $showMonth = array(); $dateArr = array();

for($i = $begin; $i <= $end; $i++){
	$date = date("Y-m-d",strtotime($lastdate." +".$i." day"));
	$dateformated = date("D-d-M",strtotime($lastdate." +".$i." day"));
	$exploDate = explode("-", $dateformated);

	array_push($dateArr, $date);
	array_push($showDay, $exploDate[0]);
	array_push($showDate, $exploDate[1]);
	array_push($showMonth, $exploDate[2]);
}

$roomArr = array();

$stmt = $db->query("select r.roomoid, r.name from room r inner join published p using (publishedoid) inner join hotel h using (hoteloid)  where h.hotelcode = '".$hotelcode."' and p.publishedoid not in (3)");
$dataroom = $stmt->fetchAll(PDO::FETCH_ASSOC);
foreach($dataroom as $roomtype){
	$dataChildRoom = array();
	array_push($dataChildRoom, $roomtype['roomoid'], $roomtype['name']);


	$roomallocation = array();
	$rtparam = array();
	foreach($dateArr as $date){
		$getAllocation = getAllocationXML($hoteloid, $roomtype['roomoid'], $date, $channel);
		array_push($roomallocation, $getAllocation);
		array_push($rtparam, $hoteloid.'_'.$roomtype['roomoid'].'_'.$date.'_'.$channel);
	}

	array_push($dataChildRoom, $roomallocation, $rtparam);

	$stmt = $db->query("select ro.roomofferoid, ro.offertypeoid, ro.name from roomoffer ro inner join published p using (publishedoid) inner join room r using (roomoid) inner join offertype using (offertypeoid) where r.roomoid = '".$roomtype['roomoid']."' and p.publishedoid = '1'");
	$dataroomoffer = $stmt->fetchAll(PDO::FETCH_ASSOC);
	$dataChildRoomOffer = array();
	foreach($dataroomoffer as $roomoffer){
		array_push($dataChildRoomOffer, array($roomoffer['roomofferoid'], $roomoffer['name'], $roomoffer['offertypeoid']));
	}
	array_push($dataChildRoom, $dataroomoffer);
	array_push($roomArr, $dataChildRoom);
}

//print_r($roomArr);
?>

<table id="inventory" cellpadding="0" cellspacing="0">
	<thead>
    	<tr>
    		<th>&nbsp;</th>
				<?php
				foreach($dateArr as $key => $date){
					if($key == 0){
				?>
					<input type="hidden" name="spotdate" value="<?=$date?>" />
				<?php
					}
				?>
					<th><?php echo $showDay[$key]; ?><br /><?php echo $showDate[$key]; ?><br /><?php echo $showMonth[$key]; ?></th>
				<?php
				}
				?>
        </tr>
    </thead>
    <tbody>
		<?php
		foreach($roomArr as $key => $room){
		?>
			<tr class="roomtype">
				<td colspan="8"><?php echo $room[1]; ?></td>
			</tr>
      <tr>
				<td>Availability Allocation</td>
				<?php
				foreach($room[2] as $key => $allocation){
				?>
				<td><?php echo $allocation[0]; ?></td>
				<?php
				}
				?>
			</tr>
			<?php
			foreach($room[4] as $key2 => $roomoffer){
				try {
				$stmt = $db->query("select h.hotelcode, r.roomoid, ro.minrate from room r inner join hotel h using (hoteloid) inner join roomoffer ro using (roomoid) where ro.roomofferoid = '".$roomoffer['roomofferoid']."'");
				$row_count = $stmt->rowCount();
					if($row_count > 0) {
						$r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
						foreach($r_room as $row){
							$hcode = $row['hotelcode'];
							$roomoid = $row['roomoid'];
							$minrate = $row['minrate'];
						}
					}else{
						echo "No Result";
						die();
					}
				}catch(PDOException $ex) {
					echo "Invalid Query";
					die();
				}
			?>
			<tr class="roomoffer">
		    <td><?php echo $roomoffer['name']; ?></td>
				<td colspan="7">&nbsp;</td>
			</tr>
			<tr>
				<td>Used</td>
				<?php
				foreach($dateArr as $key => $date){
					$s_used = "select count(bookingroomdtloid) as used from bookingroomdtl brd inner join bookingroom br using (bookingroomoid) inner join booking b using (bookingoid) where br.roomofferoid = '".$roomoffer['roomofferoid']."' and brd.date = '".$date."' and b.bookingstatusoid = '4' and brd.reconciled = '0' and b.pmsstatus = 0";
					$stmt = $db->query($s_used);
					$result = $stmt->fetch(PDO::FETCH_ASSOC);
				?>
				<td><?=$result['used']?></td>
				<?php
				}
				?>
			</tr>
			<tr>
				<td>Inhouse</td>
				<?php
				foreach($dateArr as $key => $date){
					$s_used = "select concat(c.firstname, ' ', c.lastname , '/', cr.currencycode, ' ', ROUND(br.roomtotalr,0), '/', br.promotion, '/', br.night, 'rn') as data, b.bookingnumber from bookingroomdtl brd inner join currency cr using (currencyoid) inner join bookingroom br using (bookingroomoid) inner join booking b using (bookingoid) inner join customer c using (custoid) where br.roomofferoid = '".$roomoffer['roomofferoid']."' and brd.date = '".$date."' and b.bookingstatusoid = '4' and brd.reconciled = '0' and b.pmsstatus = 0";
					$stmt = $db->query($s_used);
					$inhouse_count = $stmt->rowCount();
				?>
				<td style="text-align:left; vertical-align:top">
					<?php
					if($inhouse_count > 0){
						$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
						foreach($result as $rslt){
						 echo '<a href="'.$base_url.'/booking/detail/?no='.$rslt['bookingnumber'].'" target="_blank">&bull;'.$rslt['data'].'</a><br>';
						}
					}
					?>
				</td>
				<?php
				}
				?>
			</tr>
			<?php
			}
		}
	?>
    </tbody>
</table>
