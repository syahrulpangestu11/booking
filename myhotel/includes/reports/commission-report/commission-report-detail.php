<?php
	include('includes/bootstrap.php'); 
	include('includes/reports/function/function-report.php');
	include('includes/class/reporting1.php');
	
	$report = new Reporting1($db);
	$report->setHotel($hoteloid);
?>
<!-- Pagination -->
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css">
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>

<?php
    if(isset($_GET['startdate']) and isset($_GET['enddate'])){
    	$startdate = date('Y-m-d', strtotime($_GET['startdate']));
		$enddate = date('Y-m-d', strtotime($_GET['enddate']));
    }else{
    	$startdate = date('Y-m-d', strtotime(date('Y').'-'.date('m').'-01'));
		$enddate = date('Y-m-t', strtotime($startdate));
    }
?>

<section class="content-header">
    <h1>Commission Report</h1>
    <ol class="breadcrumb">
    	<li>Report</li>
        <li class="active">Commission Report - Detail</li>
    </ol>
</section>
<section class="content">
    <div id="dashboard">
    
        <div class="box">
            <div class="box-body box-form">
                <form method="GET" class="form-inline" action="<?php echo $base_url.'/commission-report/detail/'?>">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <a href="<?php echo $base_url.'/commission-report/detail/?year='.(date('Y')-1)?>"><button type="button" class="btn btn-warning">Last Month</button></a>
                        </div>
                        <div class="form-group">
                            <a href="<?php echo $base_url.'/commission-report/detail/?year='.date('Y')?>"><button type="button" class="btn btn-warning">Current Month</button></a>
                        </div>
                    </div>
                    <div class="col-md-9 text-right">
                        <div class="form-group">
                            <label>Show periode stay from</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                <input type="text" name="startdate" id="from" class="calinput form-control" value="<?php echo date('d M Y', strtotime($startdate));?>">
                            </div>                
                        </div>
                        <div class="form-group">
                            <label>until</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                <input type="text" id="to" name="enddate" class="calinput form-control" value="<?php echo date('d M Y', strtotime($enddate));?>">
                            </div>                
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Show</button>
                        </div>
                    </div>
            	</div>
                </form>
            </div>
        </div>
        
        <div class="box box-primary">
            <div class="box-body">
            	<div class="row">
                	<div class="col-md-6">
                    	<h1>Commission Report</h1>
                    </div>
                    <div class="col-md-6 text-right">
                    	<b>Running periode of <?=$year?></b>
                    </div>
                </div>
            	<div class="row">
                	<div class="col-md-12 text-right">
                    	<a href="<?=$base_url?>/request/export/export-commission-report-detail.php?hoteloid=<?=$hoteloid;?>&startdate=<?=$startdate?>&enddate=<?=$enddate?>"><button type="button" class="btn btn-primary"><i class="fa fa-file-excel-o"></i> Export to excel</button></a><br /><br />
                    </div>
                </div>
            	<div class="row">
                    <table id="commissionDetailReportTable" cellspacing="0" class="table table-striped">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Booking Number</th>
                                <th>Booking Date</th>
                                <th>Guest Name</th>
                                <th>Check in</th>
                                <th>Check out</th>
                                <th>Room Type</th>
                                <th>Promo</th>
                                <th>Confirmed Booking</th>
                                <th>Pinalty Cancelled</th>
                                <th>Commission</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                        	<?php
                            	$report->setPeriode($startdate, $enddate);
								$list_booking = $report->listBooking();
								
								if(!empty($list_booking)){
									$i = 1;
									foreach($list_booking as $key => $row){
										$list_room = $report->listRoomBooking($row['bookingoid']);
										$checkin = array();
										$checkout = array();
										$room = array();
										$promotion = array(); 
										if(!empty($list_room)){
											foreach($list_room as $key2 => $row2){
												array_push($checkin, $row2['checkin']);
												array_push($checkout, $row2['checkoutr']);
												array_push($room, $row2['room']);
												array_push($promotion, $row2['promotion']);
											}
										}
										
										if($row['bookingstatusoid'] != '4'){
											$grandtotal = 0;
										}else{
											$grandtotal = $row['grandtotal'];
                                        }

                                        if($row['grandtotal'] != $row['grandtotalr']){
                                            $row['commission'] = $row['commissionr'];
                                        }
                                        
                                        $typecomm = '';
                                        $periode1 = date('Y-m-d', strtotime($row['bookingtime']));
                                        /*base commission*/
                                        $query_base_commission = "select value as base, typecomm from hotelcommission where hoteloid = '".$hoteloid."' and ( (startdate <= '".$periode1."' and enddate >= '".$periode1."') or (startdate <= '0000-00-00' and enddate >= '0000-00-00')) and type = 'base' order by priority ASC limit 1";
                                        $stmt	= $db->query($query_base_commission);
                                        if($stmt->rowCount()){
                                            $result = $stmt->fetch(PDO::FETCH_ASSOC);
                                            $typecomm = $result['typecomm'];
                                        }
                                        /*override commission*/
                                        $query_override_commission = "select value as override, typecomm from hotelcommission where hoteloid = '".$hoteloid."' and ( (startdate <= '".$periode1."' and enddate >= '".$periode1."') or (startdate <= '0000-00-00' and enddate >= '0000-00-00')) and type = 'override' order by priority ASC limit 1";
                                        $stmt	= $db->query($query_override_commission);
                                        if($stmt->rowCount()){
                                            $result = $stmt->fetch(PDO::FETCH_ASSOC);
                                            $typecomm = $result['typecomm'];
                                        }
                                        if(empty($typecomm) or $typecomm == '2' or $typecomm == '4') $row['commission'] = 0;
							?>
                                <tr>
                                    <td><?=$i++?></td>
                                    <td><?=$row['bookingnumber']?></td>
                                    <td><?=date('d M Y H:i', strtotime($row['bookingtime']))?></td>
                                    <td><?=$row['guestname']?></td>
                                    <td><?=implode('<br>', $checkin)?></td>
                                    <td><?=implode('<br>', $checkout)?></td>
                                    <td><?=implode('<br>', $room)?></td>
                                    <td><?=implode('<br>', $promotion)?></td>
                                    <td class="text-right"><?=number_format($grandtotal)?></td>
                                    <td class="text-right"><?=number_format($row['cancellationamount'])?></td>
                                    <td class="text-right"><?=number_format($row['commission'])?></td>
                                    <td><?=$row['status']?></td>
                                </tr>
                            <?php
									}
								}
							?>
                        </tbody>
					</table>
				</div>
			</div>
		</div>
        
	</div>
</section>

<script type="text/javascript">
$( function() {
	$('#commissionDetailReportTable').DataTable({
		"aaSorting": [
		  [0, "asc"],
		],
		"lengthMenu": [[50, 100, 150, -1], [50, 100, 150, "All"]]
	});
});
</script>
<style type="text/css">
table.dataTable{ font-size:0.9em }
</style>