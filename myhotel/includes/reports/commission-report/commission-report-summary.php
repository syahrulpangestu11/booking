<?php
	include('includes/bootstrap.php'); 
	include('includes/reports/function/function-report.php');
	include('includes/class/reporting1.php');
	
	$report = new Reporting1($db);
	$report->setHotel($hoteloid);
?>
<!-- Pagination -->
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css">
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>

<?php
    if(isset($_GET['year'])){
    	$year = $_GET['year'];
    }else{
    	$year = date('Y');
    }
?>

<section class="content-header">
    <h1>Commission Report</h1>
    <ol class="breadcrumb">
    	<li>Report</li>
        <li class="active">Commission Report</li>
    </ol>
</section>
<section class="content">
    <div id="dashboard">
    
        <div class="box">
            <div class="box-body box-form">
                <form method="GET" class="form-inline" action="<?php echo $base_url.'/commission-report/'?>">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <a href="<?php echo $base_url.'/commission-report/?year='.(date('Y')-1)?>"><button type="button" class="btn btn-warning">Last Year</button></a>
                        </div>
                        <div class="form-group">
                            <a href="<?php echo $base_url.'/commission-report/?year='.date('Y')?>"><button type="button" class="btn btn-warning">Current Year</button></a>
                        </div>
                    </div>
                    <div class="col-md-9 text-right">
                        <div class="form-group">
                            <label>Show data per year for</label>
                            <select name="year" class="form-control">
                            	<?php for($i = 2017; $i <= date('Y')+1; $i++){ ?>
                                	<option value="<?=$i?>" <?php if($year==$i){ echo "selected"; } ?>><?=$i?></option>
                                <?php } ?>
                            </select>                
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Show</button>
                        </div>
                    </div>
            	</div>
                </form>
            </div>
        </div>
        
        <div class="box box-primary">
            <div class="box-body">
            	<div class="row">
                	<div class="col-md-6">
                    	<h1>Commission Report</h1>
                    </div>
                    <div class="col-md-6 text-right">
                    	<b>Running stay periode of <?=$year?></b>
                    </div>
                </div>
            	<div class="row">
                	<div class="col-md-12 text-right">
                    	<a href="<?=$base_url?>/request/export/export-commission-report-summary.php?hoteloid=<?=$hoteloid;?>&year=<?=$year?>"><button type="button" class="btn btn-primary"><i class="fa fa-file-excel-o"></i> Export to excel</button></a><br /><br />
                    </div>
                </div>
            	<div class="row">
                    <table id="commissionReportTable" cellspacing="0" class="table table-striped">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Month</th>
                                <th>1<sup>st</sup> Stay Periode</th>
                                <th>2<sup>nd</sup> Stay Periode</th>
                                <th>Total Booking</th>
                                <th>Room Night</th>
                                <th>Confirmed Booking</th>
                                <th>Penalty Cancelled</th>
                                <th>Total Revenue</th>
                                <th>Total Commission</th>
                            </tr>
                        </thead>
                        <tbody>
                        	<?php
                            for($m=1; $m<=12; $m++){
								$periode1 = $year.'-'.$m.'-01'; 
								$periode2 = date("Y-m-t",strtotime($periode1));
								
								$report->setPeriode($periode1, $periode2);
								
								$total_confirmed = $report->materializeBookingTotal('grandtotal', 4);
								$total_cancelled = $report->materializeBookingTotal('cancellationamount', array(5,7,8));
								$total_revenue = $total_confirmed + $total_cancelled;
                                $total_commission = $report->materializeBookingCommission();
                                
                                $typecomm = '';
                                /*base commission*/
                                $query_base_commission = "select value as base, typecomm from hotelcommission where hoteloid = '".$hoteloid."' and ( (startdate <= '".$periode1."' and enddate >= '".$periode1."') or (startdate <= '0000-00-00' and enddate >= '0000-00-00')) and type = 'base' order by priority ASC limit 1";
                                $stmt	= $db->query($query_base_commission);
                                if($stmt->rowCount()){
                                    $result = $stmt->fetch(PDO::FETCH_ASSOC);
                                    $typecomm = $result['typecomm'];
                                }
                                /*override commission*/
                                $query_override_commission = "select value as override, typecomm from hotelcommission where hoteloid = '".$hoteloid."' and ( (startdate <= '".$periode1."' and enddate >= '".$periode1."') or (startdate <= '0000-00-00' and enddate >= '0000-00-00')) and type = 'override' order by priority ASC limit 1";
                                $stmt	= $db->query($query_override_commission);
                                if($stmt->rowCount()){
                                    $result = $stmt->fetch(PDO::FETCH_ASSOC);
                                    $typecomm = $result['typecomm'];
                                }
                                if(empty($typecomm) or $typecomm == '2' or $typecomm == '4') $total_commission = 0;
							?>
                                <tr link="<?php echo $base_url.'/commission-report/detail/?startdate='.date('d M Y', strtotime($periode1)).'&enddate='.date('d M Y', strtotime($periode2)); ?>">
                                    <td><?=$m?></td>
                                    <td><?=date('F', strtotime($periode1))?></td>
                                    <td><?=date('d M Y', strtotime($periode1))?></td>
                                    <td><?=date('d M Y', strtotime($periode2))?></td>
                                    <td class="text-right"><?=number_format($report->materializeBooking())?></td>
                                    <td class="text-right"><?=number_format($report->materializeRoomNight())?></td>
                                    <td class="text-right"><?=number_format($total_confirmed)?></td>
                                    <td class="text-right"><?=number_format($total_cancelled)?></td>
                                    <td class="text-right"><?=number_format($total_revenue)?></td>
                                    <td class="text-right"><?=number_format($total_commission)?></td>
                                </tr>
                            <?php
							}
							?>
                        </tbody>
                        <tfoot>
                        </tfoot>
					</table>
				</div>
			</div>
		</div>
        
	</div>
</section>

<script type="text/javascript">
$( function() {
	$('#commissionReportTable').DataTable({
		"aaSorting": [
		  [0, "asc"],
		],
		"paging": false
	});
	$('#commissionReportTable tbody > tr').on('click', function(){
		window.location.href = $(this).attr('link');
	});
});
</script>
<style type="text/css">
table.dataTable{ font-size:0.9em }
table.dataTable tbody > tr:hover{ cursor:pointer; background-color:#e2ebf3 }
</style>