<section class="content-header">
    <h1>ADR Report</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-bar-chart-o"></i> Performance Report</a></li>
        <li class="active">ADR Report</li>
    </ol>
</section>
<section class="content">
    <div id="box"><?php include('adr-report-include.php'); ?></div>
</section>