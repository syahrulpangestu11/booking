<?php
function dateDifference($date_1 , $date_2 , $differenceFormat = '%a' )
{
    $datetime1 = date_create($date_1);
    $datetime2 = date_create($date_2);
    
    $interval = date_diff($datetime1, $datetime2);
    
    return $interval->format($differenceFormat);
    
}

$channeltype = 1;

if(empty($_REQUEST['from'])){
    $from = date('Y-m-d');
}else{
    $_REQUEST['from'] = str_replace("/","-",$_REQUEST['from']);
    $from = date('Y-m-d', strtotime($_REQUEST['from']));
}
if(isset($_REQUEST['to'])){ 
    $to = date('Y-m-d', strtotime($_REQUEST['to'])); 
    }else{ 
    $to = date('Y-m-d', strtotime('+10 day'));
}
$datediff = dateDifference($from , $to );

$roomRateHotel = array();

$s_h = "SELECT h.hotelcode FROM hotel h WHERE h.hoteloid = '".$hoteloid."'";
$stmt = $db->query($s_h);
$r_h = $stmt->fetch(PDO::FETCH_ASSOC);
$hcode = $r_h['hotelcode'];

$xml =  simplexml_load_file("data-xml/".$hcode.".xml");
?>

<div id="newdashboard">
    <div style="font-size:0.9em;">
        <?php
        try {
            $p = 0;
            $sb_id = "SELECT ro.roomofferoid, ro.name AS roomoffername, ot.offername AS offertypename, r.roomsize, v.name as roomview, r.roomoid, r.name as roomname, r.adult, r.child, r.maxextrabed
                    from hotel h
                    inner join room r using (hoteloid)
                    inner join view v using (viewoid)
                    inner join roomoffer ro using (roomoid)
                    inner join offertype ot using (offertypeoid)
                    where h.hoteloid = '".$hoteloid."' and r.publishedoid = '1' and ro.publishedoid = '1' and ot.publishedoid = '1' order by r.roomoid";
            $stmt	= $db->query($sb_id);
            $ab_id	= $stmt->fetchAll(PDO::FETCH_ASSOC);
            foreach($ab_id as $ab_idx){
                $roomoid = $ab_idx['roomoid'];
                $roomname = $ab_idx['roomname'];
                $rooid = $ab_idx['roomofferoid'];
                $roname = $ab_idx['roomoffername'];
                $offertypename = $ab_idx['offertypename'];

                $roomRateHotel[$rooid] = array("roomofferoid" => $rooid, "name" => $roname);
                                    
                for($n = 0; $n <= $datediff; $n++){

                    $xml_date = date("Y-m-d",strtotime($from." +".$n." day"));
                    $breakdate_name = date('D',strtotime($xml_date));
                    // $date = date("d M 'y",strtotime($xml_date." -1 day"));
                    $date = date("d M 'y",strtotime($xml_date));

                    $query_tag = '//hotel[@id="'.$hoteloid.'"]/masterroom[@id="'.$roomoid.'"]/rate[@date="'.$xml_date.'"]/rateplan[@id="'.$rooid.'"]/channel[@type="'.$channeltype.'"]';

                    foreach($xml->xpath($query_tag) as $rate) { 	
                        $roomRate = (int) $rate->netdouble;
                        $roomRateCurrency = (string) $rate->currency;
                        $roomRateRibu = round($roomRate/1000);
                    }

                    if(!empty($roomRateCurrency)){
                        $stmt = $db->query("SELECT currencycode FROM currency WHERE currencyoid = '".$roomRateCurrency."'");
                        $row_count = $stmt->rowCount();
                        if($row_count > 0) {
                            $r_cur = $stmt->fetch(PDO::FETCH_ASSOC);
                            // print_r($r_cur);
                            $rateCurrency = $r_cur['currencycode'];
                        }
                    }else{
                        $rateCurrency = "IDR";
                    }

                    $roomRateHotel[$rooid]['roomrate'][$n] = array(
                        "tanggal" => $date,
                        "rate" => $roomRate, 
                        "rateribu" => $roomRateRibu, 
                        "currency" => $rateCurrency
                        );

                }
            }
        }catch(PDOException $ex) {
            echo "Invalid Query";
            die();
        }
    

        // echo "<pre>";
        // print_r($roomRateHotel);
        // print_r($ab_id);
        // echo "</pre>";
        // print_r ($roomRateHotel['90']['roomrate'][0]);
        // echo "<hr>";
        
        ?>
    </div>

    <div class="row">
    <div class="col-md-12">
        <div class="box">
        <div class="box-header with-border">
            <!-- <h3 class="box-title">Reservation Report</h3> &nbsp; -->
            <form method="get" action="<?=$base_url?>/adr-report/"> 
            
            Range : <input type="text" name="from" id="from" class="calinput" readonly value="<?php echo date('d-m-Y', strtotime($from));?>" style="width:100px"> &nbsp;- &nbsp;
            <input type="text" id="to" name="to" class="calinput" readonly value="<?php echo date('d-m-Y', strtotime($to));?>" style="width:100px">
            <button type="submit" class="small-button blue"><i class="fa fa-search"></i>Find</button>
            </form>
            
        </div><!-- /.box-header -->
        <div class="box-body">
            <div class="row">
            	<div class="col-md-12">
                    <h4 class="title-chart">Average Daily Rates Report</h4>
                    <div class="chart">
                   		<canvas id="adr-chart"></canvas>
                    </div>
                </div>

            </div>

        </div><!-- ./box-body -->
        
        </div><!-- /.box -->
    </div><!-- /.col -->
    </div><!-- /.row -->

    <!-- Bootstrap 3.3.6 -->
    <script src="<?php echo $base_url?>/lib/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- chartjs -->
    <!-- <script src="<?php echo $base_url?>/lib/plugins/chartjs/Chart.170629.js" type="text/javascript"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.js" type="text/javascript"></script>


    <script>
   
        //---------------------------
        // ADR CHART
        //---------------------------
        
        var adr_chart = document.getElementById('adr-chart');
        adr_chart.height = 100;

        var myChart = new Chart(adr_chart, {
        type: 'line',
        data: {
            labels: [
                <?php
                foreach ($roomRateHotel as $key1 => $perRoom) {
                    foreach ($perRoom['roomrate'] as $key2 => $perN) {
                        echo '"'.$perN['tanggal'].'",';
                    }
                    break;
                }
                ?>
            ],
            datasets: [
                <?php
                foreach ($roomRateHotel as $key1 => $perRoom) {
                    $rand = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f');
                    $color = '#'.$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)];
                    ?>
                    {
                        label: "<?=$perRoom['name'];?>",
                        // yAxisID: "<?=$perRoom['roomofferoid'];?>",
                        data: [
                            <?php
                            foreach ($perRoom['roomrate'] as $key2 => $perN) {
                                echo $perN['rateribu'].",";
                            }
                            ?>
                        ],
                        backgroundColor: "rgba(255, 255, 255, 0)",
                        // borderColor: "rgba(255, 188, 0, 1)",
                        borderColor: '<?php echo $color; ?>',
                        borderWidth: 1,
                        pointRadius: 3,
                        pointBackgroundColor: '<?php echo $color; ?>',
                        pointBorderColor: '<?php echo $color; ?>',
                    },
                    <?php
                }

                ?>
            ]
        },
        
        <?php
        foreach ($roomRateHotel as $key1 => $perRoom) {
            foreach ($perRoom['roomrate'] as $key2 => $perN) {
                $globalCurrency = $perN['currency'];
                break;
            }
            break;
        }
        ?>
        
        options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true,
                            // stepSize: 500,
                            userCallback: function(value, index, values) {
                                value = value.toString();
                                value = value.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                return '<?=$globalCurrency;?> ' + value + 'K';
                            }
                        }
                    }]
                },
                tooltips: {
                    mode: 'nearest',
                    callbacks: {
                        title: function(tooltipItems, data) {
                            var datasetLabel = data.datasets[tooltipItems[0].datasetIndex].label;
                            return datasetLabel;
                        },
                        beforeBody: function(tooltipItems, data) { 
                            var label = data.labels[tooltipItems[0].index] + '\n';
                            return label;
                        },
                        label: function(tooltipItems, data) { 
                            value = tooltipItems.yLabel.toString();
                            value = value.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                            return '<?=$globalCurrency;?> ' + value + 'K';
                        }
                    }
                },
                elements: {
                    line: {
                        tension: 0
                    }
                },
                legend: {
                    display: true
                }
            }
        });	

    </script>


    <script>
    $( function() {
        var dateFormat = "dd-mm-yy",
        from = $( "#from" )
            .datepicker({
            defaultDate: "+1d",
            changeMonth: true,
			dateFormat: "dd-mm-yy",
            numberOfMonths: 1
            })
            .on( "change", function() {
            to.datepicker( "option", "minDate", getDate( this ) );
            }),
        to = $( "#to" ).datepicker({
            defaultDate: "+1d",
            changeMonth: true,
			dateFormat: "dd-mm-yy",
            numberOfMonths: 1
        })
        .on( "change", function() {
            from.datepicker( "option", "maxDate", getDate( this ) );
        });
    
        function getDate( element ) {
        var date;
        try {
            date = $.datepicker.parseDate( dateFormat, element.value );
        } catch( error ) {
            date = null;
        }
        return date;
        }
    } );
    </script>
</div>
