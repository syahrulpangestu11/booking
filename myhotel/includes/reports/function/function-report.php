<?php
	function differenceDate($start, $end){
		$date1 = new DateTime($start);
		$date2 = new DateTime($end);
		$interval = $date1->diff($date2);
		return $interval->days;
	}

	function countResult($start, $end, $hoteloid, $roomtype, $status){
		global $db;

		$main_query = "select count(DISTINCT b.bookingoid) as jmldata from booking b inner join hotel h using (hoteloid) inner join bookingroom br using (bookingoid) inner join roomoffer ro using (roomofferoid) inner join room r using (roomoid) where (date(bookingtime) >= '".$start."' and date(bookingtime) <= '".$end."') and h.hotelstatusoid = '1' and b.pmsstatus = '0'";
		$filter = array();
		if($status != ""){
			if(is_array($status)){
				$filter_status = implode("','", $status);
				array_push($filter, "b.bookingstatusoid in ('".$filter_status."')");
			}else{
				array_push($filter, "b.bookingstatusoid = '".$status."'");
			}
		}
		if(!empty($roomtype)){
			array_push($filter, "r.roomoid = '".$roomtype."'");
		}
		if(!empty($hoteloid)){
			array_push($filter, "h.hoteloid = '".$hoteloid."'");
		}

		if(count($filter) > 0){
			$combine_filter = implode(' and ',$filter);
			$query = $main_query.' and '.$combine_filter;
		}else{
			$query = $main_query;
		}

		$stmt	= $db->query($query);
		$result = $stmt->fetch(PDO::FETCH_ASSOC);

		return $result['jmldata'];
	}

	function countRoomNight($start, $end, $hoteloid, $roomtype){
		global $db;

		$main_query = "select count(DISTINCT brd.bookingroomdtloid) as jmldata from bookingroomdtl brd inner join bookingroom br using (bookingroomoid) inner join booking b using (bookingoid) inner join hotel h using (hoteloid) where (date(bookingtime) >= '".$start."' and date(bookingtime) <= '".$end."') and h.hotelstatusoid = '1' and b.pmsstatus = '0'";
		$filter = array();

		array_push($filter, "b.bookingstatusoid = '4'");
		if($roomtype != ""){
			array_push($filter, "r.roomoid = '".$roomtype."'");
		}
		if(!empty($hoteloid) and $hoteloid != 0){
			array_push($filter, "h.hoteloid = '".$hoteloid."'");
		}

		if(count($filter) > 0){
			$combine_filter = implode(' and ',$filter);
			$query = $main_query.' and '.$combine_filter;
		}else{
			$query = $main_query;
		}

		$stmt	= $db->query($query);
		$result = $stmt->fetch(PDO::FETCH_ASSOC);

		return $result['jmldata'];
	}

	function getPercentage($number1, $number2){
		if($number1 > $number2){
			$gap = $number1 - $number2;
			$note = "increase";
		}else{
			$gap = $number2 - $number1;
			$note = "decrease";
		}
		$percentage = $gap / $number2 * 100;
		return $note." ".round($percentage,0,PHP_ROUND_HALF_UP)."% from last month";
	}

	if(isset($_REQUEST['roomtype'])){ $roomtype = $_REQUEST['roomtype']; }else{ $roomtype = ""; }
	if(isset($_REQUEST['from'])){ $from = date('Y-m-d', strtotime($_REQUEST['from'])); }else{ $from = date('Y-m-d', strtotime('-10 day')); }
	if(isset($_REQUEST['to'])){ $to = date('Y-m-d', strtotime($_REQUEST['to'])); }else{ $to = date('Y-m-d'); }

	$day = differenceDate($from, $to);

	function SumRevenue($from, $to, $hoteloid){
		global $db;
		$sum_revenue 	= "select sum(grandtotal) as revenue from booking b inner join hotel h using (hoteloid) where (date(bookingtime) >= '".$from."' and date(bookingtime) <= '".$to."') and bookingstatusoid in ('4') and h.hotelstatusoid = '1' and b.pmsstatus = '0'";

		$filter = array();
		if(!empty($hoteloid) and $hoteloid != 0){
			array_push($filter, "h.hoteloid = '".$hoteloid."'");
		}

		if(count($filter) > 0){
			$combine_filter = implode(' and ',$filter);
			$sum_revenue = $sum_revenue.' and '.$combine_filter;
		}

		$stmt			= $db->query($sum_revenue);
		$arr_revenue	= $stmt->fetch(PDO::FETCH_ASSOC);
		$revenue 		= floor($arr_revenue['revenue']);

		return number_format($revenue);
	}

	function SumNetHotel($from, $to, $hoteloid){
		global $db;
		$sum_commission 	= "select sum(hotelcollect) as nethotel from booking b inner join hotel h using (hoteloid) where (date(bookingtime) >= '".$from."' and date(bookingtime) <= '".$to."') and bookingstatusoid in ('4', '5', '7', '8')  and h.hotelstatusoid = '1' and b.pmsstatus = '0'";
		$filter = array();
		if(!empty($hoteloid) and $hoteloid != 0){
			array_push($filter, "h.hoteloid = '".$hoteloid."'");
		}

		if(count($filter) > 0){
			$combine_filter = implode(' and ',$filter);
			$sum_commission = $sum_commission.' and '.$combine_filter;
		}

		$stmt				= $db->query($sum_commission);
		$arr_commission 	= $stmt->fetch(PDO::FETCH_ASSOC);
		$nethotel			= floor($arr_commission['nethotel']);

		return number_format($nethotel);
	}

	function SumCommission($from, $to, $hoteloid){
		global $db;
		$sum_commission 	= "select sum(gbhcollect) as commission from booking b inner join hotel h using (hoteloid) where (date(bookingtime) >= '".$from."' and date(bookingtime) <= '".$to."') and bookingstatusoid in ('4', '5', '7', '8')  and h.hotelstatusoid = '1' and b.pmsstatus = '0'";
		$filter = array();
		if(!empty($hoteloid) and $hoteloid != 0){
			array_push($filter, "h.hoteloid = '".$hoteloid."'");
		}

		if(count($filter) > 0){
			$combine_filter = implode(' and ',$filter);
			$sum_commission = $sum_commission.' and '.$combine_filter;
		}

		$stmt				= $db->query($sum_commission);
		$arr_commission 	= $stmt->fetch(PDO::FETCH_ASSOC);
		$commission			= floor($arr_commission['commission']);

		return number_format($commission);
	}


	function SumCancellationAmount($from, $to, $hoteloid){
		global $db;
		$sum_cancellationamount 	= "select sum(cancellationamount) as cancelled from booking b inner join hotel h using (hoteloid) where (date(bookingtime) >= '".$from."' and date(bookingtime) <= '".$to."') and h.hoteloid = '".$hoteloid."' and bookingstatusoid in ('4', '5', '7', '8') and h.hotelstatusoid = '1' and b.pmsstatus = '0'";
		$stmt						= $db->query($sum_cancellationamount);
		$arr_cancellationamount 	= $stmt->fetch(PDO::FETCH_ASSOC);
		$cancellationamount			= floor($arr_cancellationamount['commission']);

		return number_format($cancellationamount);
	}

	function countRoomNightStay($start, $end){
		global $db;

		$query = "select count(bookingroomdtloid) as roomnight from bookingroomdtl inner join bookingroom br using (bookingroomoid) where br.bookingoid in (select bookingoid from (select b.bookingoid, min(checkin) as min_checkin from bookingroom br inner join booking b using (bookingoid) inner join hotel h using (hoteloid) where  h.hotelstatusoid = '1' and b.bookingstatusoid = '4' and b.pmsstatus = '0' group by b.bookingoid) as t1 where (min_checkin >= '".$start."' and min_checkin <= '".$end."'))";
		$stmt	= $db->query($query);
		$result = $stmt->fetch(PDO::FETCH_ASSOC);

		return $result['roomnight'];
	}

	function countOccupancyBookingConfirmed($start, $end){
		global $db;

		$query = "select count(bookingoid) as totalbooking
from (select b.bookingoid, min(checkin) as min_checkin from bookingroom br inner join booking b using (bookingoid) inner join hotel h using (hoteloid) where h.hotelstatusoid = '1' and b.bookingstatusoid = '4' and b.pmsstatus = '0' group by b.bookingoid) as t1
where (min_checkin >= '".$start."' and min_checkin <= '".$end."')";
		$stmt	= $db->query($query);
		$result = $stmt->fetch(PDO::FETCH_ASSOC);

		return $result['totalbooking'];
	}

	function materializeBookingTotal($start, $end, $field, $bookingstatus){
		global $db;
		if(is_array($bookingstatus)){
			$bookingstatus = implode("','", $bookingstatus);
		}

		$query = "select sum(".$field.") as confirmed
from (select b.".$field.", min(checkin) as min_checkin from bookingroom br inner join booking b using (bookingoid) inner join hotel h using (hoteloid) where h.hotelstatusoid = '1' and b.bookingstatusoid in ('".$bookingstatus."') and b.pmsstatus = '0' group by b.bookingoid) as t1
where (min_checkin >= '".$start."' and min_checkin <= '".$end."')";
		$stmt	= $db->query($query);
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		return floatval($result['confirmed']);
	}

	function materializeBookingCommission($start, $end){
		global $db;
		$query = "select sum(gbhcollect) as commission
from (select b.gbhcollect, min(checkin) as min_checkin from bookingroom br inner join booking b using (bookingoid) inner join hotel h using (hoteloid) where h.hotelstatusoid = '1' and b.pmsstatus = '0' group by b.bookingoid) as t1
where (min_checkin >= '".$start."' and min_checkin <= '".$end."')";
		$stmt	= $db->query($query);
		$result = $stmt->fetch(PDO::FETCH_ASSOC);

		return floatval($result['commission']);
	}


?>
