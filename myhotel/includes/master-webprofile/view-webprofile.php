<?php
	include("function.php");
	try {
		$stmt = $db->query("SELECT a.* 
							,state.stateoid,state.countryoid
						FROM masterprofile a
							left join city using (cityoid)
							left join state using (stateoid) 
						where masterprofileoid = '1'");
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
			$r_masterprofile = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_masterprofile as $row){
				$inquiry = $row['inquiry_status'];
				$inquiry_wording = $row['inquiry_wording'];
				$name = $row['name'];
				$shortcode = $row['shortcode'];
				$imglogo = $row['imglogo'];
				$imgicon = $row['imgicon'];
				$imglogo2 = $row['imglogo2'];
				$imgfavicon = $row['imgfavicon'];
				$description = $row['description'];
				$email = $row['email'];
				$web = $row['web'];
				$phone = $row['phone'];
				$fax = $row['fax'];
				$whatsapp = $row['whatsapp'];
				$address = $row['address'];
				$city = $row['cityoid'];
				$state = $row['stateoid'];
				$country = $row['countryoid'];
				$socialmediaurl = $row['socialmediaurl'];
				$aboutus = $row['aboutus'];
				$privacypolicy = $row['privacypolicy'];
				$termscondition = $row['termscondition'];
				$created = $row['created'];
				$createdby = $row['createdby'];
				$updated = $row['updated'];
				$updatedby = $row['updatedby'];
			}
		}else{
			echo "No Result";
			die();
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}
?>
<style>
	#form-webprofile span{font-style: italic;color:red;font-size: 0.9em;display: none;}
	#form-webprofile button{float:right;margin:5px;}
	#form-webprofile .form-control{width:100%;}
	#form-webprofile .box-input-image img{height: 100%; max-width: 100%;}
	#form-webprofile .box-input-image label{}
	#form-webprofile .box-input-image{
		width: 100%;
		height: 150px;
		line-height: 150px;
		border: 1px solid #ccc;
		margin: 5px 0;
		text-align: center;
		padding: 0px;
		background: #f8f8f8;
	}
	#form-webprofile .bootstrap-tagsinput{display: block; height: 82px;border-radius: 0;padding:6px 12px;}
	#form-webprofile .bootstrap-tagsinput span{font-family: sans-serif; font-size: 85%;font-style: normal;}
	#form-webprofile textarea{box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075); }
</style>
<section class="content-header">
    <h1>
        Web Profile
    </h1>
    <ol class="breadcrumb">
        <li class="active"><a href="#"><i class="fa fa-book"></i> Web Profile</a></li>
    </ol>
</section>
<section class="content">
	<div class="row">
        <div class="box box-form">
            <div id="data-box" class="box-body">
            <h1>WEB PROFILE INFORMATION</h1><hr />
            <form method="post" enctype="multipart/form-data" id="form-webprofile" action="<?php echo $base_url; ?>/webprofile/save">

		          <div class="row">
								<div class="col-md-6 col-xs-12">
										<div class="row">
											<div class="form-group col-md-8">
												<label>Name</label><input type="text" class="form-control" name="name" value="<?php echo $name; ?>" required="required">
											</div>
											<div class="form-group col-md-4">
												<label>Short Code</label><input type="text" class="form-control" name="shortcode" value="<?php echo $shortcode; ?>" required="required">
											</div>
										</div>
										<div class="row">
											<div class="form-group col-md-6">
												<label>Email</label><input type="text" class="form-control" name="email" value="<?php echo $email; ?>">
												<span>*You email address has an invalid email address format.</span>
											</div>
											<div class="form-group col-md-6">
												<label>Web</label><input type="text" class="form-control" name="web" value="<?php echo $web; ?>">
												<!-- <span>*You email address has an invalid email address format.</span> -->
											</div>
										</div>
										<div class="row">
											<div class="form-group col-md-4">
													<label>Phone</label><input type="text" class="form-control only-numeric" name="phone" value="<?php echo $phone; ?>" maxlength="20">
											</div>
											<div class="form-group col-md-4">
													<label>Whatsapp</label><input type="text" class="form-control only-numeric" name="whatsapp" value="<?php echo $whatsapp; ?>" maxlength="20">
											</div>
											<div class="form-group col-md-4">
													<label>Fax</label><input type="text" class="form-control only-numeric" name="fax" value="<?php echo $fax; ?>" maxlength="20">
											</div>
										</div>
										<div class="row">
											<input type="hidden" name="dfltcountry"  value="<?=$country;?>" />
											<input type="hidden" name="dfltstate" value="<?=$state;?>" />
											<input type="hidden" name="dfltcity" value="<?=$city;?>" />
											<div class="form-group col-md-4">
												<label>Country</label><br />
												<select name="country" class="input-select" style="width:100%;">
													<?php getCountry($country); ?>
												</select>
											</div>
											<div class="form-group col-md-4 loc-state"> 
												<label>State</label><br />
												<select name="state" class="input-select" style="width:100%;">
													<?php getState($state,$country); ?>
												</select>
											</div>
											<div class="form-group col-md-4 loc-city"> 
												<label>City</label><br />
												<select name="city" class="input-select" style="width:100%;">
													<?php getCity($city,$state); ?>
												</select>
											</div>

										</div>
										<div class="form-group">
											<label>Address</label><textarea class="form-control" name="address" rows="3"><?php echo $address; ?></textarea>
										</div>
										<div class="form-group">
											<label>Social Media Url</label><input type="text" class="form-control" name="socialmediaurl" data-role="tagsinput"  data-role="tagsinput" value="<?php echo $socialmediaurl; ?>">
											<i>*delimited by enter to separate the links</i>
										</div>
								<div class="form-group">
											<label>Description</label>
											<textarea class="form-control" name="description" rows="5" ><?php echo $description; ?></textarea>
										</div>
										<div class="form-group">
											<label>Icon</label>
											<div id="preview_icon" class="box-input-image ">
												<?php if($imgicon==''){ ?>
																<label>No Picture</label>
												<?php }else{ ?>
																<img src="<?=$imgicon;?>" />
												<?php } ?>
											</div>
											<input type="hidden" name="oldicon" value="<?php echo $imgicon; ?>">
											<input type="file" class="form-control input-image" name="icon" value="<?php echo $imgicon; ?>">
										</div>
										
										<div class="form-group">
											<label>Favicon</label>
											<div id="preview_favicon" class="box-input-image ">
												<?php if($imgfavicon==''){ ?>
																<label>No Picture</label>
												<?php }else{ ?>
																<img src="<?=$imgfavicon;?>" />
												<?php } ?>
											</div>
											<input type="hidden" name="oldfavicon" value="<?php echo $imgfavicon; ?>">
											<input type="file" class="form-control input-image" name="favicon" value="<?php echo $imgfavicon; ?>">
										</div>
										
										<div class="form-group">
											<label>Wording Inquiry</label><input type="text" class="form-control" name="inquiry_wording" value="<?php echo $inquiry_wording; ?>">
										</div>
										<div class="form-group">
											<label>Inquiry</label><br>
											<select class="" name="inquiry">
												<option value="y" <?php echo ($inquiry=='y'?'selected':''); ?>>Active</option>
												<option value="n" <?php echo ($inquiry=='n'?'selected':''); ?>>Inactive</option>
											</select>
										</div>
								</div>
								<div class="col-md-6 col-xs-12">

								<div class="form-group">
											<label>About Us</label>
											<textarea class="form-control" name="aboutus" rows="9" ><?php echo $aboutus; ?></textarea>
										</div>
								<div class="form-group">
											<label>Privacy Policy</label>
											<textarea class="form-control" name="privacypolicy" rows="9" ><?php echo $privacypolicy; ?></textarea>
										</div>
										<div class="form-group">
											<label>Terms and Condition</label>
											<textarea class="form-control" name="termscondition" rows="9" ><?php echo $termscondition; ?></textarea>
										</div>
										<div class="form-group">
											<label>Logo</label>
											<div id="preview_photos" class="box-input-image">
												<?php if($imglogo==''){ ?>
																<label>No Picture</label>
												<?php }else{ ?>
																<img src="<?=$imglogo;?>" />
												<?php } ?>
											</div>
											<input type="hidden" name="oldphotos" value="<?php echo $imglogo; ?>">
											<input type="file" class="form-control input-image" name="photos" value="<?php echo $imglogo; ?>">
										</div>
										
										<div class="form-group">
											<label>Secondary Logo</label>
											<div id="preview_photos2" class="box-input-image">
												<?php if($imglogo2==''){ ?>
																<label>No Picture</label>
												<?php }else{ ?>
																<img src="<?=$imglogo2;?>" />
												<?php } ?>
											</div>
											<input type="hidden" name="oldphotos2" value="<?php echo $imglogo2; ?>">
											<input type="file" class="form-control input-image" name="photos2" value="<?php echo $imglogo2; ?>">
										</div>

								</div>
							</div>
							<br>
							<br>
							<div class="row">
								<div class="col-md-6">
									<?php
									if(!empty($updated) and !empty($updatedby)){
										echo "<i class='fa fa-clock-o'></i> Last updated on ".date('d/M/Y H:i', strtotime($updated))." <small>(GMT+8)</small> by <b>".$updatedby."</b>";
									}
									?>

								</div>
								<div class="col-md-6">
									<div class="form-group">
										<button type="submit" class="btn btn-primary submit">Save</button>
									</div>
								</div>
							</div>
            </form>
        </div>
   		</div>
    </div>
</section>

<?php include("js.php"); ?>