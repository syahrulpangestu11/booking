
<script>

$(function(){
	//--- geography 
	
	$('body').on('change','select[name=country]', function(e) {
		// console.log(000);
		$.get("<?php echo $base_url; ?>/includes/master-webprofile/function-ajax.php", {
			func: "stateRqst",
			country: $('select[name=country]').val(),
			state: $('input[type=hidden][name=dfltstate]').val()
      	}, function(response){
			//   console.log(response);
			$('.loc-state').html(unescape(response));
			$('select[name=state]').change();
      	});
	});

	$('body').on('change','select[name=state]', function(e) {
		$.get("<?php echo $base_url; ?>/includes/master-webprofile/function-ajax.php", {
			func: "cityRqst",
			state: $('select[name=state]').val(),
			city: $('input[type=hidden][name=dfltcity]').val()
      	}, function(response){
			$('.loc-city').html(unescape(response));
      	});
	});
});

//--------- validator phone number
$('.only-numeric').on('keypress', function(e){ 
	// console.log(e)
	if ((e.which < 48 && e.which != 43 ) || e.which > 57) { 
		e.preventDefault(); 
	} 
});
$(".only-numeric").bind("paste", function(e){
		e.preventDefault();
    var pastedData = e.originalEvent.clipboardData.getData('text');
		var numArray = ['+','0','1','2','3','4','5','6','7','8','9'];
		var dataArray = pastedData.split('');
		var resultData = "";
		$.each(dataArray, function(index, item) { if($.inArray(item, numArray)!=-1){resultData += item;} });
		$(this).val(resultData);
});

//--------- validator email
$('input[name=email]').change(function(){
	var $warning = $(this).parent('div').children('span');
	if(isEmail($(this).val())){
		$(this).css('border-color','#ccc');
		$('.submit').removeAttr('disabled');
		$warning.hide();
	}else{
		$(this).css('border-color','red');
		$('.submit').attr('disabled','disabled');
		$warning.show();
	}
});
function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

//--------- prevent enter as submit action
$('body').on('keypress','input', function(e) { if (e.keyCode == 13){ e.preventDefault(); } });

//--------- tagsinput setting
$('input[name=socialmediaurl]').tagsinput({ confirmKeys: [13, 32, 44] });//enter,space,comma
$('input[name=socialmediaurl]').on('itemAdded', function(event) { $('.bootstrap-tagsinput .tag').css('display','inline'); });
$('input[name=socialmediaurl]').tagsinput('add', '<?=$socialmediaurl;?>');

//--------- validation and preview image
$(".input-image").change(function() { readURL(this); });
function readURL(input) {
	var $sts = true;
	var $box = $('#preview_' + $(input).attr('name'));
	$box.empty();


	//--- validation image extension
	var $ext = input.files[0].name.split('.').pop().toLowerCase();
	if($.inArray($ext, ['gif','png','jpg','jpeg']) == -1) {
		$sts=false;
		$(input).val('');
		$box.append('<label>Uploaded file is not a valid image. Only JPG, PNG and GIF files are allowed.</label>');
		$box.find('label').css('color','red');
	}else{
		$box.find('label').css('color','black');
	}

	//--- validation image size
	if($sts){
		var size = input.files[0].size/1024/1024;
		var n = size.toFixed(2);
		if(size>1){
			$sts = false;
			$(input).val('');
			$box.append('<label>Your file size is: ' + n + 'MB, and it is too large to upload! Maximum : 1MB or less.</label>');
			$box.find('label').css('color','red');
		}else{
			$box.find('label').css('color','black');
		}
	}

	//--- preview image
	if($sts){
	  if (input.files && input.files[0]) {
	    var reader = new FileReader();
	    reader.onload = function(e) { $box.append('<img src="'+e.target.result+'" />') }
	    reader.readAsDataURL(input.files[0]);
	  }
	}
}

</script>
