<?php
	include('includes/bootstrap.php');
	include('includes/promotion/geo-location/geo-location.php');

	$daylist = array("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat");
	$stmt = $db->query("select max(priority)+1 as maxpriority from promotion where hoteloid = '".$hoteloid."' and publishedoid = '1'");
	$r_max = $stmt->fetchAll(PDO::FETCH_ASSOC);
	foreach($r_max as $row){
		$maxpriority = $row['maxpriority'];
	}

	$promooid = $_GET['pid'];
	try {
		$stmt = $db->query("select promotion.* from promotion inner join hotel h using (hoteloid) where promotionoid = '".$promooid."'");
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
			$r_promo = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_promo as $row){
				$promotype = $row['promotiontypeoid'];
				$name = $row['name'];
				$promoimage = $row['promoimage'];
				$minstay = $row['minstay'];
				$salefrom = date("d F Y", strtotime($row['salefrom']));
				$saleto = date("d F Y", strtotime($row['saleto']));
				$stayfrom = date("d F Y", strtotime($row['bookfrom']));
				$stayto = date("d F Y", strtotime($row['bookto']));
				$display = explode(',',$row['displayed']);
				$checkin = explode(',',$row['checkinon']);
				$maxstay = $row['maxstay'];
				$timefrom = date("H:i", strtotime($row['timefrom']));
				$timeto = date("H:i", strtotime($row['timeto']));
				$discounttype = $row['discounttypeoid'];
				$discountapply = $row['discountapplyoid'];
				$applyvalue = $row['applyvalue'];
				$discountvalue = floor($row['discountvalue']);
				$minroom = $row['minroom'];
				$cancellationpolicy = $row['cancellationpolicyoid'];
				$published = $row['publishedoid'];
				$priority = $row['priority'];
				$deposit = $row['depositoid'];
				$depositvalue = $row['depositvalue'];

				$min_ci = $row['min_bef_ci'];
				$max_ci = $row['max_bef_ci'];
				$max_bef_ci_today = $row['max_bef_ci_today'];

				$headline = $row['headline'];
				$description = $row['description'];
				$servicefacilities = $row['servicefacilities'];
				$termcondition = $row['termcondition'];

				$popupbanner = $row['popupbanner'];
				$shown_no_pc = $row['shown_no_pc'];
					try {
						$stmt = $db->query("select pa.*, channel.name as channelname from promotionapply pa inner join promotion using (promotionoid) inner join hotel h using (hoteloid) inner join roomoffer ro using (roomofferoid) inner join room using (roomoid) inner join channel using (channeloid) where promotionoid = '".$promooid."'");
						$row_count = $stmt->rowCount();
						if($row_count > 0) {
							$r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
							$roomoffer = array();
							foreach($r_room as $key => $row){
								$index_room = array_search($row['roomofferoid'], $roomoffer);

								if(empty($index_room)){
									array_push($roomoffer, $row['roomofferoid']);
								}
								if(empty($channel[$row['roomofferoid']]) or !in_array($row['channeloid'], $channel[$row['roomofferoid']])){
									$channel[$row['roomofferoid']][] = $row['channeloid'];
									$channelname[$row['roomofferoid']][] = $row['channelname'];
								}

							}
						}
					}catch(PDOException $ex) {
						echo "Invalid Query".$ex;
						die();
					}

			}
		}else{
			echo "No Result";
			die();
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}
?>
<script type="text/javascript">
$(function() {
	$('textarea#html-box').trumbowyg({
		btns: ['viewHTML',
		  '|', 'btnGrp-design',
		  '|', 'link',
		  '|', 'btnGrp-justify',
		  '|', 'btnGrp-lists'],
		fullscreenable: false
	});
});
</script>
<section class="content-header">
    <h1>
        Edit Promotions
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i>  Allotments &amp; Rates </a></li>
        <li class="active">Promotions</li>
    </ol>
</section>
<section class="content" id="promotion">
	<div class="row">
		<form  class="form-box form-horizontal form-box" method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/promotions/edit-process" novalidate>
        <div class="box box-form">
            <input type="hidden" name="promooid" value="<?php echo $promooid; ?>" />
            <div class="form-group"><div class="col-md-12"><h1>PROMOTION</h1></div></div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-md-12">Promotion Name</label>
                    <div class="col-md-12"><input type="text" class="form-control" name="name" required="required" value="<?=$name?>" style="width:100%"></div>
                </div>
                <div class="form-group" style="display:none">
                    <label class="col-md-12">Priority Order</label>
                    <div class="col-md-12"><input type="number" class="form-control" min="1" max="<?php echo $maxpriority; ?>" name="priority" value="<?php echo $priority; ?>"><input type="hidden" name="oldpriority" value="<?php echo $priority; ?>"></div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">Direct Booking Promotion Link</label>
                    <div class="col-md-12">
                        <div class="radio" style="display:inline-block;padding-right:10px;">
                            <label><input type="radio" name="link-style" value="primary" checked="checked"> show promotion only</label>
                        </div>
                        <div class="radio" style="display:inline-block;padding-right:10px;">
                            <label><input type="radio" name="link-style" value="all"> show promotion on top</label>
                        </div>
                    </div>
                    <div class="col-md-12"><textarea name="direct-link" class="form-control" style="width:100%"></textarea></div>
				</div>
                <div class="form-group">
                    <div class="col-md-12"><button type="button" class="btn btn-warning btn-sm copy-link" name="<?=htmlspecialchars($name)?>" night="<?php if($minstay < 2){ echo 2; }else{ echo $minstay; } ?>" min-bef-ci="<?php echo $min_ci; ?>"><i class="fa fa-link"></i> Copy Link to Clipboard</button></div>
                </div>
                <div class="form-group">
                    <label class="col-md-2">Source</label>
                    <div class="col-md-4"><input type="text" class="form-control utmset" name="utm_source" value="" style="width:100%"/></div>
                    <label class="col-md-2">Champaign</label>
                    <div class="col-md-4"><input type="text" class="form-control utmset" name="utm_campaign" value="" style="width:100%"/></div>
				</div>
                <div class="form-group">
                    <label class="col-md-2">URL Link</label>
                    <div class="col-md-10"><textarea name="direct2-link" class="form-control" style="width:100%"></textarea></div>
				</div>
                <div class="form-group">
                    <div class="col-md-12"><button type="button" class="btn btn-warning btn-sm copy2-link"><i class="fa fa-link"></i> Copy Link to Clipboard</button></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-md-12">Promotion Image</label>
                    <div class="col-md-12 preview-image">
                    <?php if(!empty($promoimage)){ ?>
                        <img src="<?=$promoimage?>" id="preview-image" class="img-responsive" alt="Responsive image" style="width:200px;">	
                    <?php }else{ echo "N/A Image for Promotion"; } ?>
                    </div>
                    <div class="col-md-12">
                        <div class="checkbox">
                            <?php if($popupbanner==1){ $checked = "checked"; }else{ $checked = ""; } ?>
                            <label><input type="checkbox" name="popupbanner" value="1" <?=$checked?>> Show image as <b>Popup Banner</b></label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">Upload New Promotion Image</label>
                    <div class="col-md-12"><input type="file" class="form-control" name="image"><i>recommended size : 550px x 310px</i></div>
                </div>
            </div>
            <div class="clear"></div>
            <div style="text-align:right"><button class="default-button" type="submit">Save Promotion</button></div>
            <ul class="inline-triple promotion-triple">
            	<li>
                	<h1>CONDITION</h1>
                	<ul class="block">
                    	<li>
                        	<span>Minimum Stay:</span>
                            <input type="text" class="small" name="minstay" value="<?php echo $minstay; ?>">
                        </li>
                        <li>
                            <span>Guest Booking Within:</span><br />
                            <span style="vertical-align:bottom;"><input type="number" class="small" name="min_ci" value="<?php echo $min_ci; ?>" min="0" /> to <input type="number"  class="small" name="max_ci" value="<?php echo $max_ci; ?>" min="0" /> days (before check in)</span>
                            <br>
                            <span><input type="checkbox" id="max_bef_ci_today" name="max_bef_ci_today" value="1" style="margin-left:20px;" <?php if(!empty($max_bef_ci_today)) echo 'checked';?>> current day only</span>
                        </li>
                    	<li><span>Sale Date</span></li>
                    	<li>
                        	<span>Sale Date From:</span>
                            <input type="text"class="medium" id="startdate" name="salefrom" value="<?php echo $salefrom; ?>">
                        </li>
                    	<li>
                        	<span>Sale Date To:</span>
                            <input type="text"class="medium" id="enddate"; name="saleto" value="<?php echo $saleto; ?>">
                        </li>
                    	<li><span>Stay Date</span></li>
                    	<li>
                        	<span>Stay Date From:</span>
                            <input type="text"class="medium" id="startcalendar" name="stayfrom" value="<?php echo $stayfrom; ?>">
                        </li>
                    	<li>
                        	<span>Stay Date To:</span>
                            <input type="text"class="medium" id="endcalendar" name="stayto" value="<?php echo $stayto; ?>">
                        </li>
                    	<li>
                        	<span>Displayed on:</span><br>
							<?php
								foreach($daylist as $day){
									if(in_array($day, $display)){ $checked = "checked"; }else{ $checked = ""; }
									echo"<input type='checkbox' name='display[]' value='".$day."' ".$checked.">".$day."&nbsp;&nbsp;";
								}
							?>
                        </li>
                    	<li>
                        	<span>Check-in on:</span><br>
							<?php
								foreach($daylist as $day){
									if(in_array($day, $checkin)){ $checked = "checked"; }else{ $checked = ""; }
									echo"<input type='checkbox' name='checkin[]' value='".$day."' ".$checked.">".$day."&nbsp;&nbsp;";
								}
							?>
                        </li>
                    	<li>
                        	<span>Maximum Stay:</span>
                            <input type="text"class="small"  name="maxstay" value="<?php echo $maxstay; ?>"> [0 = N/A]
                        </li>
                    	<li>
                        	<span>Book Time From:</span>
                            <input type="time" class="small"  name="timefrom" value="<?php echo $timefrom; ?>">
                        </li>
                    	<li>
                        	<span>Book Time To:</span>
                            <input type="time" class="small"  name="timeto" value="<?php echo $timeto; ?>">
						</li>
                    </ul>
                </li>
                <li>
                	<h1>BENEFIT</h1>
                	<ul class="block">
                    	<li>
                        	<span>Discount Type:</span>
							<?php
                                try {
                                    $stmt = $db->query("select dt.* from discounttype dt inner join published p using (publishedoid) where p.publishedoid = '1'");
                                    $r_dt = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                    foreach($r_dt as $row){
										if($row['discounttypeoid'] == $discounttype){ $selected = "checked"; }else{ $selected=""; }
                                        echo"<br><input type='radio' name='discounttype' value='".$row['discounttypeoid']."' label='".$row['labelquestion']."' ".$selected.">  ".$row['name'];
                                    }
                                }catch(PDOException $ex) {
                                    echo "Invalid Query";
                                    die();
                                }
                            ?>
                        </li>
                    	<li>
                            <span><b>Discount Value:</b></span><br>
                            <input type="text" class="small"  name="discountvalue" value="<?php echo $discountvalue; ?>">
                            <span></span>
                        </li>
                    	<li class="applybox">
                        	<span>Apply On:</span>
							<?php
                                try {
                                    $stmt = $db->query("select da.* from discountapply da inner join published p using (publishedoid) where p.publishedoid = '1'");
                                    $r_dt = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                    foreach($r_dt as $row){
										if($row['discountapplyoid'] == $discountapply){ $selected = "checked"; }else{ $selected=""; }
                                        echo"<br><input type='radio' name='discountapply' value='".$row['discountapplyoid']."' ".$selected.">  ".$row['name'];
										if($row['discountapplyoid'] == '2'){
											echo"<div class='applyval'><span class='label'>Apply on Night Number :<br></span><input type='text' class='medium'  name='applyvalue' value='".$applyvalue."'><br>
											ex: 2,3,4 for night number 2, 3 and 4 only</div>";
										}else if($row['discountapplyoid'] == '3'){
											$applyvalue = explode(',',$applyvalue);
											echo"<div class='applyval'><span class='label'>Apply on Day:</span><br>";
											foreach($daylist as $day){
												if(!empty($applyvalue)){
													if(in_array($day, $applyvalue)){ $checked = "checked"; }else{ $checked = ""; }
												}else{
													$checked = "checked";
												}
												echo"<input type='checkbox' name='applyvalue[]' value='".$day."' ".$checked.">".$day."&nbsp;&nbsp;";
											}
											echo"</div>";
										}
                                    }
                                }catch(PDOException $ex) {
                                    echo "Invalid Query";
                                    die();
                                }
                            ?>
                        </li>
                    	<li>
                        	<!-- <span>Minimum Room:</span> -->
                            <input type="hidden" class="small"  name="minroom" value="0<?php //echo $minroom; ?>">
                        </li>
					</ul>
                </li>
                <li>
                	<h1>Restriction</h1>
                	<ul class="block">
                    	<li>
                        	<span>Room Type:</span><br />
                            <input type="radio" name="select" value="all" /> select all
                            &nbsp;&nbsp;
                            <input type="radio" name="select" value="none" /> unselect all
                            <br />
                            <ul class="block roomtype">
                            <?php
                                try {
                                    $stmt = $db->query("select r.roomoid, r.name, x.offernum from room r inner join hotel h using (hoteloid) left join (select count(roomofferoid) as offernum, ro.roomoid from roomoffer ro group by ro.roomoid) as x on x.roomoid = r.roomoid where h.hoteloid = '".$hoteloid."' and x.offernum > 0 and r.publishedoid = '1'");
                                    $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                    foreach($r_room as $row){
										if(!empty($room) and in_array($row['roomoid'], $room)){ $checked = "checked"; }else{ $checked = ""; }
                                        echo"<li><span><b>".$row['name']."</b></span>";
										try {

											$stmt = $db->query("select ro.* from roomoffer ro inner join published p using (publishedoid) inner join room r using (roomoid) inner join offertype using (offertypeoid) where r.roomoid = '".$row['roomoid']."' and p.publishedoid = '1'");
											$r_plan = $stmt->fetchAll(PDO::FETCH_ASSOC);
											echo"<ul class='block roomtype-channel'>";
											foreach($r_plan as $row1){
												if(!empty($roomoffer) and in_array($row1['roomofferoid'], $roomoffer)){ $checked = "checked"; }else{ $checked = ""; }
												echo"<li><input type='checkbox' name='rateplan[]' value='".$row1['roomofferoid']."' sign='rateplan' ".$checked.">".$row1['name']."<br><dl class='dropdown'>";
												try {
													$stmt = $db->query("select * from channel");
                            						$r_channel = $stmt->fetchAll(PDO::FETCH_ASSOC);
													$row_count = $stmt->rowCount();
													if($row_count > 0){ $classnotif = "hide"; }else{ $classnotif = "";  }
													echo"<dt style='display:none'>
														<a>
														  <span class='hida ".$classnotif."'>Select Channel</span>
														  <p class='multiSel'>";
														  if(!empty($channelname[$row1['roomofferoid']])){
															foreach($channelname[$row1['roomofferoid']] as $rp){
																echo"<span title='".$rp."'>".$rp.", </span>";
															}
														  }
													echo"
														  </p>
														</a>
														</dt>";
													echo"<dd><ul class='mutliSelect'>";
													foreach($r_channel as $row2){
														if(!empty($channel[$row1['roomofferoid']]) and in_array($row2['channeloid'], $channel[$row1['roomofferoid']])){ $checked = "checked"; }else{
															if($row2['channeloid'] == '1'){ $checked = 'checked'; }else{ $checked = ''; }
														}
														echo"<li><input type='checkbox' name='channel-".$row1['roomofferoid']."[]' value='".$row2['channeloid']."' val='".$row2['name']."' ".$checked.">".$row2['name']."</li>";
													}
													echo"</ul></dd>";
												}catch(PDOException $ex2) {
													echo $ex2;
													die();
												}
												echo"</dl></li>";
											}
											echo"</ul></li>";
										}catch(PDOException $ex1) {
											echo $ex1;
											die();
										}
                                    }
                                }catch(PDOException $ex) {
                                    echo "Invalid Query";
                                    die();
                                }
                            ?>
                            </ul>
                        </li>
					</ul>
                </li>
            </ul>
            <div style="text-align:right"><button class="default-button" type="submit">Save Promotion</button></div>
		</div>
        
		<div class="box box-form" id="headline-detail">
        	<div class="row">
            	<div class="col-md-6">
                    <h2>SHOW PROMOTION WITH GEO LOCATION</h2>
                    <div class="panel panel-info">
                        <div class="panel-heading text-center">
                        	<div class="form-group">
                             	<label class="col-md-12 text-left"><i class="fa fa-map-marker"></i> Type Country :</label>
                            	<div class="col-md-12"><input type="text" name="show-geo-country" class="form-control" /></div>
                            </div>
                            <div class="list-country">
                                <ul class="inline-block">
                                <?php
                                try {
                                    $stmt = $db->query("select pg.promotiongeooid, c.countryname from promotiongeo pg inner join country c using (countryoid) where pg.promotionoid = '".$promooid."'");
                                    $row_count = $stmt->rowCount();
                                    if($row_count > 0) {
                                        $r_geo = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                        foreach($r_geo as $key => $country){
                                            echo "<li><span class='current'><input type='hidden' name='applycountry[]' value='".$country['promotiongeooid']."'>".$country['countryname']."</span></li>";
                                        }
                                    }
                                }catch(PDOException $ex) {
                                    echo "Invalid Query".$ex;
                                    die();
                                }
                                ?>                        
                                </ul>
                            </div>
                        </div>
                    </div>
            	</div>
                <div class="col-md-6">
                    <h2>NOT SHOW PROMOTION WITH GEO LOCATION</h2>
                    <div class="panel panel-danger">
                        <div class="panel-heading text-center">
                        	<div class="form-group">
                             	<label class="col-md-12 text-left"><i class="fa fa-map-marker"></i> Type Country :</label>
                            	<div class="col-md-12"><input type="text" name="noshow-geo-country" class="form-control" /></div>
                            </div>
                            <div class="list-country-noshow">
                                <ul class="inline-block">
                                <?php
                                try {
                                    $stmt = $db->query("select pgn.promotiongeo_noshowoid, c.countryname from promotiongeo_noshow pgn inner join country c using (countryoid) where pgn.promotionoid = '".$promooid."'");
                                    $row_count = $stmt->rowCount();
                                    if($row_count > 0) {
                                        $r_geo = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                        foreach($r_geo as $key => $country){
                                            echo "<li><span class='current'><input type='hidden' name='apply_noshow_country[]' value='".$country['promotiongeo_noshowoid']."'>".$country['countryname']."</span></li>";
                                        }
                                    }
                                }catch(PDOException $ex) {
                                    echo "Invalid Query".$ex;
                                    die();
                                }
                                ?>                        
                                </ul>
                            </div>
                        </div>
                    </div>
			</div>
        	</div>
        </div>
        
		<div class="box box-form">
        	<div class="row">
            	<h2>APPLY PROMOTION TO PROMO CODE (PC)</h2>
                <table class="table table-bordered table-condensed table-striped">
                	<tr class="text-center">
                    	<th>Apply</th>
                    	<th>Promo Code</th>
                        <th>Periode</th>
                        <th>Apply PC Discount</th>
                        <th>Apply PC Commission</th>
                    </tr>
                    <tr>
                    	<?php if($shown_no_pc == 'y'){ $no_pc = 'checked'; }else{ $no_pc = ''; } ?>
                    	<td><input type="checkbox" name="shown_no_pc" value="y" <?=$no_pc?>/></td>
                        <td>IBE<br />
                        <span style="font-size:0.9em; color:#de0000"><i class="fa fa-info-circle"></i> If you unselect this IBE option, it means you agreed that this promotion will not appear in IBE unless you have provided other promo code to be apply to this promotion.</span>
                        <td>N/A</td>
                        <td>0%</td>
                        <td>0%</td>
                    </tr>
                    <?php
						$s_promocode = "select pc.* from promocode pc inner join hotel h using (hoteloid) where h.hoteloid = '".$hoteloid."' order by pc.promocodeoid";
						$stmt = $db->query($s_promocode);
						$r_promocode = $stmt->fetchAll(PDO::FETCH_ASSOC);
						foreach($r_promocode as $pc){
							if($pc['discounttype'] == "discount percentage"){
								$pc_discount = $pc['discount']."%";
							}else{
								$pc_discount = number_format($pc['discount']);
							}
							if($pc['commissiontype'] == "commission percentage"){
								$pc_commission = $pc['commission']."%";
							}else{
								$pc_commission = number_format($pc['commission']);
							}
							
							$pc_apply = ''; $pc_applydiscount = ''; $pc_applycommission = '';
							
							$s_checkpc = "select pa.* from promocodeapply pa inner join promocode pc using (promocodeoid) where promocodeoid = '".$pc['promocodeoid']."' and referencetable = 'promotion' and id = '".$promooid."'";
							$stmt = $db->query($s_checkpc);
							$num_foundpc = $stmt->rowCount();
							if($num_foundpc > 0){
								$foundpc = $stmt->fetch(PDO::FETCH_ASSOC);
								$pc_apply = 'checked';
								if($foundpc['applydiscount'] == 'y'){ $pc_applydiscount = 'checked'; }
								if($foundpc['applycommission'] == 'y'){ $pc_applycommission = 'checked'; }
							}
					?>
                    <tr>
                    	<td><input type="checkbox" name="pc[]" value="<?=$pc['promocodeoid']?>" <?=$pc_apply?>/></td>
                        <td><?php echo $pc['promocode']; ?></td>
                        <td><?php echo date("d/M/Y", strtotime($pc['startdate']))." - ".date("d/M/Y", strtotime($pc['enddate'])); ?></td>
                        <td><input type="checkbox" name="pcdiscount-<?=$pc['promocodeoid']?>" value="y" <?=$pc_applydiscount?>/> <?=$pc_discount?></td>
                        <td><input type="checkbox" name="pccommission-<?=$pc['promocodeoid']?>" value="y" <?=$pc_applycommission?>/><?=$pc_commission?></td>
                    </tr>
                    <?php
						}
					?>
                </table>
            </div>
        </div>
        
        <div class="box box-form" id="headline-detail">
        	<div class="row">
                <h2>HEADLINE &amp; DETAIL</h2>
                <ul class="inline-triple">
                <?php
                    try {
                        $stmt = $db->query("SELECT th.*, i.*, x.termheadlinepromooid from termheadline th left join icon i using (iconoid) left join (select termheadlinepromooid, termheadlineoid from termheadlinepromo thp where thp.id='".$promooid."' and thp.type='promotion') x on x.termheadlineoid = th.termheadlineoid 
                        where th.termheadlineoid not in (select termheadlineoid from termheadlinehotelremove where hoteloid = '".$hoteloid."') and th.hoteloid in ('".$hoteloid."', '0') 
                        order by icon_title");
                        // where th.hoteloid='".$hoteloid."' 
                        $r_headline = $stmt->fetchAll(PDO::FETCH_ASSOC);
                        foreach($r_headline as $termheadline){
                            if($termheadline['iconoid'] == 0){
                                $icon_src = $termheadline['term_icon_src'];
                            }else{
                                $icon_src = $termheadline['icon_src'];
                            }
                            if(!empty($termheadline['termheadlinepromooid'])){ $checkapply = "checked";  }else{ $checkapply = ""; }
                            ?>
                            <li>
                            <input type="checkbox" name="icon[]" value="<?=$termheadline['termheadlineoid']?>" <?=$checkapply?>/>&nbsp;&nbsp;<img src="<?=$icon_src?>" class="icon" />&nbsp;&nbsp;<?=$termheadline['icon_title']?>
                            </li>
                            <?php
                        }
                    }catch(PDOException $ex) {
                        echo "Invalid Query";
                        die();
                    }
                ?>
                </ul>
                <div class="clear"></div>
                <div class="form-group">
                    <div class="col-md-6">
                        <label>Headline</label>
                        <textarea id="html-box" name="headline"><?=$headline?></textarea>
                    </div>
                    <div class="col-md-6">
                        <label>Promotion Description</label>
                        <textarea id="html-box" name="description"><?=$description?></textarea>
                    </div>
                    <div class="col-md-6">
                        <label>Promotion Inclusive</label>
                        <textarea id="html-box" name="servicefacilities"><?=$servicefacilities?></textarea>
                    </div>
                    <div class="col-md-6">
                        <label>Promotion Terms &amp; Condition</label>
                        <textarea id="html-box" name="termcondition"><?=$termcondition?></textarea>
                    </div>
                </div>
                <div class="form-group">
					<div class="col-md-6">
                        <label>Cancellation Policy</label>
                        <select name="cancellation" class="form-control">
                        <?php
                            try {
                                $stmt = $db->query("select cp.* from cancellationpolicy cp inner join published p using (publishedoid) where p.publishedoid = '1' and cp.hoteloid in ('0','".$hoteloid."')");
                                $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                foreach($r_room as $row){
                                    if($row['cancellationpolicyoid'] == $cancellationpolicy){ $selected = "selected"; }else{ $selected=""; }
                                    echo"<option value='".$row['cancellationpolicyoid']."' ".$selected.">".$row['name']."</option>";
                                }
                            }catch(PDOException $ex) {
                                echo "Invalid Query";
                                die();
                            }
                        ?>
                        </select>
                    </div>
                    <div class="col-md-6">
                        <label>Publish Promotion</label>
                        <select name="published" class="form-control">
                        <?php
                            try {
                                $stmt = $db->query("select * from published where showthis = 'y'");
                                $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                foreach($r_room as $row){
                                    if($row['publishedoid'] == $published){ $selected = "selected"; }else{ $selected=""; }
                                    echo"<option value='".$row['publishedoid']."' ".$selected.">".$row['note']."</option>";
                                }
                            }catch(PDOException $ex) {
                                echo "Invalid Query";
                                die();
                            }
                        ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                    <div class="panel panel-danger">
                        <div class="panel-heading" style="color:#000">
                        <label>Deposit Type</label>
                        &nbsp; <span style="font-size:0.9em; color:#de0000"><i class="fa fa-info-circle"></i> This amount will be declared as booking guarantee that charged by hotel upon booking received.</span>
                        <div>
                        <?php
                            try {
                                $stmt = $db->query("select d.* from deposit d inner join published p using (publishedoid) where p.publishedoid = '1'");
                                $r_dt = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                foreach($r_dt as $row){
                                    if($row['depositoid'] == $deposit){ $selected = "checked"; $depositval = $depositvalue; }else{ $selected=""; $depositval = ""; }
                                    echo"<input type='radio' name='deposit' value='".$row['depositoid']."' ".$selected.">  ".$row['name'];
                                    if($row['depositoid'] == '2'){
                                        echo"<div class='depositval' style='display:none'><input type='text' class='medium'  name='depositvalue' value='".$depositval."'> Deposit for IDR Amount</div>";
                                    }else if($row['depositoid'] == '3'){
                                        echo"<div class='depositval' style='display:none'><input type='text' class='medium'  name='depositvalue' value='".$depositval."'> Deposit for %(percentage)from total</div>";
                                    }else if($row['depositoid'] == '4'){
                                        echo"<div class='depositval' style='display:none'><input type='text' class='medium'  name='depositvalue' value='".$depositval."'> night(s) amount</div>";
                                    }
                                    echo "<br>";
                                }
                            }catch(PDOException $ex) {
                                echo "Invalid Query";
                                die();
                            }
                        ?>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="clear"></div>
                <div class="form-group">
                    <div class="col-md-12 text-right">
                        <button class="btn btn-danger cancel" type="button">Cancel Editing</button>
                        <button class="btn btn-primary" type="submit">Save Promotion</button>
                    </div>
                </div>
   			</div>
        </div>
    </form>
    </div>
</section>
