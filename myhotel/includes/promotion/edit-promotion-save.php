<?php
	$promotionoid = $_POST['promooid'];
	//$promotype = $_POST['promotype'];
	$promotype = 1;
	$name = (isset($_POST['name'])) ? $_POST['name'] : "";
	$priority = $_POST['priority'];
	$minstay = $_POST['minstay'];
	$salefrom = date("Y-m-d", strtotime($_POST['salefrom']));
	$saleto = date("Y-m-d", strtotime($_POST['saleto']));
	$stayfrom = date("Y-m-d", strtotime($_POST['stayfrom']));
	$stayto = date("Y-m-d", strtotime($_POST['stayto']));

		$display = array();
		foreach($_POST['display'] as $key => $value){
			array_push($display , $value);
		}
	$display = implode(",", $display);

		$checkin = array();
		foreach($_POST['checkin'] as $key => $value){
			array_push($checkin , $value);
		}
	$checkin = implode(",", $checkin);

	$maxstay = (isset($_POST['maxstay'])) ? $_POST['maxstay'] : "0";
	$timefrom = date("H:i", strtotime($_POST['timefrom']));
	$timeto = date("H:i", strtotime($_POST['timeto']));
	$max_bef_ci_today = (isset($_POST['max_bef_ci_today'])) ? $_POST['max_bef_ci_today'] : "0";

	$discounttype = $_POST['discounttype'];
	$discountapply = $_POST['discountapply'];
	if($discountapply == '2' or $discountapply == '3'){
		if(is_array($_POST['applyvalue'])){
				$applyvalue = array();
				foreach($_POST['applyvalue'] as $key => $value){
					array_push($applyvalue , $value);
				}
			$applyvalue = implode(",", $applyvalue);
		}else{
			$applyvalue = $_POST['applyvalue'];
		}
	}else{
		$applyvalue = "";
	}
	$discountvalue = $_POST['discountvalue'];
	$minroom = $_POST['minroom'];
	$cancellationpolicy = $_POST['cancellation'];
	$published = $_POST['published'];

	$deal = $_POST['deal'];
	$deal_commission = $_POST['deal_commission'];

	$headline = $_POST['headline'];
	$description = $_POST['description'];
	$servicefacilities = $_POST['servicefacilities'];
	$termcondition = $_POST['termcondition'];

	$deposit = $_POST['deposit'];
	$depositvalue = $_POST['depositvalue'];

	$allowedExtension = array('jpg', 'jpeg', 'png');
	$tmp = explode('.', $_FILES['image']['name']);
	$extension = end($tmp);
	$image = '';
	if(!empty($_FILES['image']['name'])){
		if(in_array($extension, $allowedExtension)){
			$imagename = str_replace(' ', '_', $_FILES['image']['name']);
			$imagename = str_replace('.'.$extension, '', $imagename);
			$ext = '.'.strtolower($extension);
			$image_new_name = substr($imagename, 0, 10).date('Ymd_His').$ext;
			$folder_destination = 'pict/promoimage/';

			move_uploaded_file($_FILES['image']['tmp_name'], $upload_base.$folder_destination.$image_new_name);
			$image = $web_url.'/'.$folder_destination.$image_new_name;

			try {
				$stmt = $db->prepare("update promotion set promoimage = :ac  where promotionoid = :ab");
				$stmt->execute(array(':ab' => $promotionoid, ':ac' => $image));
			}catch(PDOException $ex) {
				echo "Invalid Query"; print($ex);
				die();
			}

		}else{
			echo'<script>alert("Data not saved.\\nInvalid file.")</script>';
		}
	}

	$min_ci = $_POST['min_ci'];
	$max_ci = $_POST['max_ci'];

	$popupbanner = $_POST['popupbanner'];
	$shown_no_pc = $_POST['shown_no_pc'];

	try {
		$stmt = $db->prepare("update promotion set hoteloid = :a, promotiontypeoid = :b, name = :c, minstay = :d, salefrom = :e, saleto = :f, bookfrom = :g, bookto = :h, displayed = :i, checkinon = :j, maxstay = :k, timefrom = :l, timeto = :m, discounttypeoid = :n, discountapplyoid = :o, applyvalue = :p, discountvalue = :q, minroom = :r, cancellationpolicyoid = :s, publishedoid = :t, priority = :u, headline = :v, description =:w, servicefacilities = :x, termcondition = :y, depositoid = :z, depositvalue = :aa, min_bef_ci = :ac, max_bef_ci = :ad, popupbanner = :ae, shown_no_pc = :af, max_bef_ci_today = :ag where promotionoid = :ab");
		$stmt->execute(array(':a' => $hoteloid ,':b' => $promotype ,':c' => $name ,':d' => $minstay ,':e' => $salefrom ,':f' => $saleto ,':g' => $stayfrom ,':h' => $stayto ,':i' => $display ,':j' => $checkin ,':k' => $maxstay ,':l' => $timefrom ,':m' => $timeto ,':n' => $discounttype ,':o' => $discountapply ,':p' => $applyvalue ,':q' => $discountvalue ,':r' => $minroom ,':s' => $cancellationpolicy ,':t' => $published, ':u' => $priority, ':ab' => $promotionoid, ':v' => $headline, ':w' => $description, ':x' => $servicefacilities, ':y' => $termcondition, ':z' => $deposit, ':aa' => $depositvalue, ':ac' => $min_ci, ':ad' => $max_ci, ':ae' => $popupbanner, ':af' => $shown_no_pc, ':ag' => $max_bef_ci_today));
	}catch(PDOException $ex) {
		echo "Invalid Query";
		//print($ex);
		die();
	}

	include('reorder-priority.php');
		
	try {
		$stmt = $db->prepare("delete from promotionapply where promotionoid = :a");
		$stmt->execute(array(':a' => $promotionoid));
	}catch(PDOException $ex) {
		echo "Invalid Query"; //print($ex);
		die();
	}

	foreach($_POST['rateplan'] as $key => $roomoffer){
		foreach($_POST['channel-'.$roomoffer] as $key1 => $channel){
			try {
				$stmt = $db->prepare("insert into promotionapply (promotionoid, roomofferoid, channeloid, publishedoid) values (:a,:c,:d,:e)");
				$stmt->execute(array(':a' => $promotionoid , ':c' => $roomoffer ,':d' => $channel ,':e' => '1'));
			}catch(PDOException $ex) {
				echo "Invalid Query";
				//print($ex);
				die();
			}
		}
	}

	$stmt = $db->query("select dealspromotion.* from dealspromotion inner join promotion p using (promotionoid) where p.promotionoid = '".$promotionoid."'");
	$row_count = $stmt->rowCount();
	if($row_count == 0){
		if($deal == "yes" and !empty($deal)){
			try {
				$stmt = $db->prepare("insert into dealspromotion (promotionoid, commission, publishedoid) values (:a,:b,:c)");
				$stmt->execute(array(':a' => $promotionoid, ':b' => $deal_commission, ':c' => 1));
			}catch(PDOException $ex) {
				echo "Invalid Query";
				//print($ex);
				die();
			}
		}
	}

	$type = 'promotion';
	if(count($_POST['icon'])>0){
		if(count($_POST['icon']) == 1){
			$remaining_icon = "'".$_POST['icon']."'";
		}else{
			$remaining_icon = "'".implode("','", $_POST['icon'])."'";
		}

		$query_delete_icon = "DELETE FROM `termheadlinepromo` WHERE termheadlineoid not in (".$remaining_icon.") and `id`='".$promotionoid."' and type = '".$type."'";
		$stmt = $db->query($query_delete_icon);

		foreach ($_POST['icon'] as $key => $value){
			$iconoid = $value;
			$s_check_icon = "select * from termheadlinepromo where type = '".$type."' and id = '".$promotionoid."' and termheadlineoid = '".$iconoid."'";
			$stmt = $db->query($s_check_icon);
			$row_count = $stmt->rowCount();
			if($row_count == 0){
				try {
					$stmt = $db->prepare("insert into termheadlinepromo (type, id, termheadlineoid) values (:a,:b,:c)");
					$stmt->execute(array(':a' => $type, ':b' => $promotionoid, ':c' => $iconoid));
				}catch(PDOException $ex) {
					echo "Invalid Query";
					//print($ex);
					die();
				}
			}
		}
	}else{
		$query_delete_icon = "DELETE FROM `termheadlinepromo` WHERE `id`='".$promotionoid."' and type = '".$type."'";
		$stmt = $db->query($query_delete_icon);
	}
	
	/* GEO LOCATION COUNTRY ------------------------------------------*/
		
	if(isset($_POST['applycountry'])){
		$geo_country_not_remove = implode("','", $_POST['applycountry']);
		$query_remove_country = "delete from promotiongeo where promotionoid = '".$promotionoid."' and promotiongeooid not in ('".$geo_country_not_remove."')"; 
	}else{
		$query_remove_country = "delete from promotiongeo where promotionoid = '".$promotionoid."'"; 
	}
	$stmt = $db->query($query_remove_country);
	
	
	if(isset($_POST['country'])){
		foreach($_POST['country'] as $key => $countryoid){
			$stmt = $db->prepare("insert into promotiongeo (promotionoid, countryoid) values (:a,:b)");
			$stmt->execute(array(':a' => $promotionoid , ':b' => $countryoid));
		}
	}

	/* NO SHOW <-- GEO LOCATION COUNTRY ------------------------------------------*/
		
	if(isset($_POST['apply_noshow_country'])){
		$geo_country_not_remove = implode("','", $_POST['apply_noshow_country']);
		$query_remove_country = "delete from promotiongeo_noshow where promotionoid = '".$promotionoid."' and promotiongeo_noshowoid not in ('".$geo_country_not_remove."')"; 
	}else{
		$query_remove_country = "delete from promotiongeo_noshow where promotionoid = '".$promotionoid."'"; 
	}
	$stmt = $db->query($query_remove_country);
	
	
	if(isset($_POST['noshow_country'])){
		foreach($_POST['noshow_country'] as $key => $countryoid){
			$stmt = $db->prepare("insert into promotiongeo_noshow (promotionoid, countryoid) values (:a,:b)");
			$stmt->execute(array(':a' => $promotionoid , ':b' => $countryoid));
		}
	}
	
	/* PROMO CODE ------------------------------------------*/
		
	if(isset($_POST['pc'])){
		$pc_not_remove = implode("','", $_POST['pc']);
		$query_remove_pc = "delete from promocodeapply where referencetable = 'promotion' and id = '".$promotionoid."' and promocodeoid not in ('".$pc_not_remove."')";
	}else{
		$query_remove_pc = "delete from promocodeapply where referencetable = 'promotion' and id = '".$promotionoid."'"; 
	}
	$stmt = $db->query($query_remove_pc);


	if(isset($_POST['pc'])){
		foreach($_POST['pc'] as $key => $promocodeoid){
			$pcdiscount		= (isset($_POST['pcdiscount-'.$promocodeoid])) ? $_POST['pcdiscount-'.$promocodeoid] : "n";
			$pccommission	= (isset($_POST['pccommission-'.$promocodeoid])) ? $_POST['pccommission-'.$promocodeoid] : "n";
			
			$s_checkpc = "select pa.* from promocodeapply pa inner join promocode pc using (promocodeoid) where promocodeoid = '".$promocodeoid."' and referencetable = 'promotion' and id = '".$promotionoid."'";
			$stmt = $db->query($s_checkpc);
			$num_foundpc = $stmt->rowCount();
			if($num_foundpc > 0){
				$foundpc = $stmt->fetch(PDO::FETCH_ASSOC);
				
				$stmt = $db->prepare("update promocodeapply set applydiscount = :a, applycommission = :b where promocodeapplyoid = :c");
				$stmt->execute(array(':a' => $pcdiscount, ':b' => $pccommission, ':c' => $foundpc['promocodeapplyoid']));
			}else{
				$stmt = $db->prepare("insert into promocodeapply (promocodeoid, referencetable, id, applydiscount, applycommission) values (:a, :b, :c, :d, :e)");
				$stmt->execute(array(':a' => $promocodeoid, ':b' => 'promotion', ':c' => $promotionoid, ':d' => $pcdiscount, ':e' => $pccommission));
			}
		}
	}
	/* ------------------------------------------*/

	header("Location: ". $base_url ."/promotions");
?>
