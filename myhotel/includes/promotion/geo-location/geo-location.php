<script type="text/javascript">
$(function(){
	$(document).ready(function(){
		checkExistedGeo();
	});
	
	function generateCountry(page, assign){
		//var name = $('#geoModalCountry').find('input[name=name]').val();
		if(assign == 'show'){
			var listbox = $('#geoModalCountry').find('div#list');
		}else if(assign == 'noshow'){
			var listbox = $('#geoModalCountryNoShow').find('div#list');
		}
		
		var country_show = $('div.list-country :input').serialize();
		var country_noshow = $('div.list-country-noshow :input').serialize();
		
		$.ajax({
			url		: '<?php echo $base_url; ?>/includes/promotion/geo-location/list-geo-country.php',
			type	: 'post',
			data	: country_show + '&' + country_noshow + /*'&name=' + name + */ '&page=' + page,
			success	: function(response) {
				listbox.html(response);
			}
		});
	}
	
	$('#geoModalCountry').on('show.bs.modal', function (e) {
		generateCountry(1, 'show');
	});
	$('#geoModalCountry button#find').click(function(){
		generateCountry(1, 'show');
	});
	$('body').on('click','#geoModalCountry #paging button', function(e) {
		page	= $(this).attr('page');
		generateCountry(page, 'show'); 
	});

	$('#geoModalCountryNoShow').on('show.bs.modal', function (e) {
		generateCountry(1, 'noshow');
	});
	$('#geoModalCountryNoShow button#find').click(function(){
		generateCountry(1, 'noshow');
	});
	$('body').on('click','#geoModalCountryNoShow #paging button', function(e) {
		page	= $(this).attr('page');
		generateCountry(page, 'noshow'); 
	});

	
	$('body').on('click', '#geoModalCountry button#assign-country, #geoModalCountryNoShow button#assign-country', function (e){
		assign = $(this).val();
		
		if(assign == 'show'){
			var formcountry = $('#geoModalCountry').find('form#geo-location-country');
			var boxdestination = $('div.list-country').children('ul');
		}else if(assign == 'noshow'){
			var formcountry = $('#geoModalCountryNoShow').find('form#geo-location-country-noshow');
			var boxdestination = $('div.list-country-noshow').children('ul');
		}
		
		$.ajax({
			url		: '<?php echo $base_url; ?>/includes/promotion/geo-location/apply-country.php',
			type	: 'post',
			data	: formcountry.serialize() + '&assign=' + assign,
			success	: function(response){
				//alert(response);
				boxdestination.append(response);
				checkExistedGeo();
			}
		});
		
		$('.modal').modal('hide');
	});
	
	$('body').on('click', 'div.list-country > ul > li > span, div.list-country-noshow > ul > li > span', function (e){
		$(this).parent('li').remove();
		checkExistedGeo();
	});
	
	function checkExistedGeo(){
		existedgeo = $("div.list-country > ul > li").length;
		if(existedgeo == 0){
			if($('div.list-country > div.notification').length == 0){
				$('div.list-country').append('<div class="notification">If no country selected promotion will show in all location <b>except those specified in the no show location</b>.</div>');
			}
		}else{
			$('div.list-country > div.notification').remove();
		}

		existedgeonoshow = $("div.list-country-noshow > ul > li").length;
		if(existedgeonoshow == 0){
			if($('div.list-country-noshow > div.notification').length == 0){
				$('div.list-country-noshow').append('<div class="notification">If no country selected promotion will show in all location that have been selected.</div>');
			}
		}else{
			$('div.list-country-noshow > div.notification').remove();
		}
	}
	
	$( "input[name='show-geo-country']" ).autocomplete({
		source: function( request, response ) {
			$.ajax({
				url: "<?php echo"$base_url"; ?>/request/list-country-geo-location.php",
				data: $('.list-country :input').serialize() + '&term=' + request.term,
				success: function(data){
					response(data);
				},
				error: function(jqXHR, textStatus, errorThrown){
					alert("no country available");                        
				},
			  dataType: 'json'
			});
        },		
		minLength: 2,
		select: function( event, ui ) {
			var boxdestination = $('div.list-country').children('ul');
			boxdestination.append("<li><span class='new'><input type='hidden' name='country[]' value='"+ui.item.id+"'>"+ui.item.value+"</span></li>");
			checkExistedGeo();
			
			$( "input[name='show-geo-country']" ).val("");
			return false;
		}
    });
	
	$( "input[name='noshow-geo-country']" ).autocomplete({
		source: function( request, response ) {
			$.ajax({
				url: "<?php echo"$base_url"; ?>/request/list-country-geo-location.php",
				data: $('.list-country-noshow :input').serialize() + '&term=' + request.term,
				success: function(data){
					response(data);
				},
				error: function(jqXHR, textStatus, errorThrown){
					alert("no country available");                        
				},
			  dataType: 'json'
			});
        },		
		minLength: 2,
		select: function( event, ui ) {
			var boxdestination = $('div.list-country-noshow').children('ul');
			boxdestination.append("<li><span class='new'><input type='hidden' name='noshow_country[]' value='"+ui.item.id+"'>"+ui.item.value+"</span></li>");
			checkExistedGeo();
			$( "input[name='noshow-geo-country']" ).val("");
			return false;
			
		}
    });
});
</script>

<?php include('modal-geo-location.php'); ?>

<style type="text/css">
	div.list-country > ul > li, div.list-country-noshow > ul > li{
		margin:5px;
		padding:10px 5px;
		box-sizing:border-box;
	}
	div.list-country > ul > li > span, div.list-country-noshow > ul > li > span{
		background-color: #ffffff;
		border-radius: 5px;
		padding: 8px 10px;
		cursor:pointer;
	}
	div.list-country > ul > li > span.new, div.list-country > ul > li > span.current{
		border: 2px solid #3c8dbc;
	}
	div.list-country-noshow > ul > li > span.new, div.list-country-noshow > ul > li > span.current{
		border: 2px solid #dd4b39;
	}
	/* 
	div.list-country > ul > li > span.current{
		color: #e66b23;
		background-color: #fff9f3;
		border-radius: 5px;
		border: 1px solid #ffc8a8;
	}
	div.list-country > ul > li > span:hover:after{ text-decoration:line-through; } 
	*/
	div.list-country > ul > li > span:hover, div.list-country-noshow > ul > li > span:hover{
		text-decoration:line-through;
		color: #fff;
		background-color: #e88083;
		border-color: #ce3b4a;
	}
	div.notification{
		padding: 10px;
		font-size: 1.1em;
		font-style:italic;
	}
	
  .ui-autocomplete {
    max-height: 100px;
    overflow-y: auto;
    overflow-x: hidden;
  }
  * html .ui-autocomplete {
    height: 100px;
  }
</style>