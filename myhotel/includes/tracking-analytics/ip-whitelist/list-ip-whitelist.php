<?php
	include("includes/bootstrap.php");
?>
<section class="content-header">
    <h1>IP Whitelist</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-area-chart"></i> Tracking Analytics</a></li>
        <li><a href="#"><i class="fa"></i> IP Whitelist</a></li>
    </ol>
</section>
<section class="content">
	<form id="data-input" method="get" class="form-inline" action="">
    <input type="hidden" name="page">
    <div class="box box-form">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-search"></i></div>
                        <input type="text" class="form-control" name="keyword" placeholder="Search by IP or note">
                    </div>
                </div>
                <div class="form-group">
                    <button type="button" class="btn btn-primary" purpose="search">search</button>
                </div>
            </div>
            <div class="col-md-6 text-right">
                <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#AddIP">Add New IP</button>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-right"><i class="fa fa-info-circle"></i> All ip on this list will not be considered on tracking analytics.</div>
        </div>
        <div class="row">
            <div class="col-md-12">
            	<div id="data-box"></div>
            </div>
        </div>
    </div>
    </form>
</section>

<div class="modal fade" id="AddIP" tabindex="-1" role="dialog" aria-labelledby="AddIP">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        	<form id="add-ip" class="form-horizontal" method="post">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add new IP whitelist</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label">IP Number</label>
                    <div class="col-sm-10"><input type="text" class="form-control" name="ip"></div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Note</label>
                    <div class="col-sm-10"><textarea class="form-control" name="note"></textarea></div>
                </div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary add-ip">Save</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="EditIP" tabindex="-1" role="dialog" aria-labelledby="EditIP">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        	<form id="edit-ip" class="form-horizontal" method="post">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit IP whitelist</h4>
            </div>
            <div class="modal-body">
            loading ...
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary edit-ip">Save</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="DeleteIP" tabindex="-1" role="dialog" aria-labelledby="DeleteIP">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        	<form id="delete-ip" class="form-horizontal" method="post">
            <input type="hidden" name="ipid" />
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Delete IP</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-danger delete-ip">Delete</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="ResponseAction" tabindex="-1" role="dialog" aria-labelledby="ResponseAction">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        	<form id="delete-ip" class="form-horizontal" method="post">
            <input type="hidden" name="ipid" />
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-info-circle"></i> Notification</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-primary">OK</button>
            </div>
            </form>
        </div>
    </div>
</div>



<script type="text/javascript">
$(function(){
	$(document).ready(function(){ 
		getLoadData();
	});
	
	function getLoadData(){
		$.ajax({
			url: "<?php echo"$base_url"; ?>/includes/tracking-analytics/ip-whitelist/data.php",
			type: 'post',
			data: $('form#data-input').serialize(),
			success: function(data) {
				$("#data-box").html(data)
			}
		});
	}
	
	$('body').on('click','#paging button', function(e) {
		page	= $(this).attr('page');
		pagebox = $('form#data-input').children('input[name=page]');
		pagebox.val(page);
		getLoadData();
	});
	
	$('body').on('click','button[purpose="search"]', function(e) {
		getLoadData();
	});
	
	$('body').on('click','#ResponseAction button', function(e) {
		location.reload();
	});
	
	$('#DeleteIP').on('show.bs.modal', function(e) {
		var ipid = $(e.relatedTarget).data('ipid');
		var ip = $(e.relatedTarget).data('ip');
		$('form#delete-ip').find('input[name=ipid]').val(ipid);
		$(this).find('div.modal-body').html('Are you sure want to delete ip '+ip+' from list?');
	});
	
	$('#EditIP').on('show.bs.modal', function(e) {
		var ipid = $(e.relatedTarget).data('ipid');
		$.ajax({
			url: "<?php echo"$base_url"; ?>/includes/tracking-analytics/ip-whitelist/request-action.php",
			type: 'post',
			data: '&request=detail-ip&ipid='+ipid,
			success: function(response){
				$('#EditIP').find('div.modal-body').html(response);
			}
		});
	});
	
	$('body').on('click','button.add-ip', function(e) {
		$.ajax({
			url: "<?php echo"$base_url"; ?>/includes/tracking-analytics/ip-whitelist/request-action.php",
			type: 'post',
			data: $('form#add-ip').serialize() + '&request=add',
			success: function(response){
				$('.modal').modal('hide');
				if(response == 'success'){
					$('#ResponseAction').find('div.modal-body').html('IP already successfully added.<br>Please click OK to refresh the page.');
				}else{
					$('#ResponseAction').find('div.modal-body').html('IP failed to add, please try again.<br>Click OK to refresh the page.');
				}
				$('#ResponseAction').modal('show');
			}
		});
	});
	
	$('body').on('click','button.edit-ip', function(e) {
		$.ajax({
			url: "<?php echo"$base_url"; ?>/includes/tracking-analytics/ip-whitelist/request-action.php",
			type: 'post',
			data: $('form#edit-ip').serialize() + '&request=edit',
			success: function(response){
				$('.modal').modal('hide');
				if(response == 'success'){
					$('#ResponseAction').find('div.modal-body').html('IP was updated successfully.<br>Please click OK to refresh the page.');
				}else{
					$('#ResponseAction').find('div.modal-body').html('IP failed updating, please try again.<br>Click OK to refresh the page.');
				}
				$('#ResponseAction').modal('show');
			}
		});
	});
	
	$('body').on('click','button.delete-ip', function(e) {
		$.ajax({
			url: "<?php echo"$base_url"; ?>/includes/tracking-analytics/ip-whitelist/request-action.php",
			type: 'post',
			data: $('form#delete-ip').serialize() + '&request=delete',
			success: function(response){
				$('.modal').modal('hide');
				if(response == 'success'){
					$('#ResponseAction').find('div.modal-body').html('IP successfully deleted.<br>Please click OK to refresh the page.');
				}else{
					$('#ResponseAction').find('div.modal-body').html('IP failed to delete, please try again.<br>Click OK to refresh the page.');
				}
				$('#ResponseAction').modal('show');
			}
		});
	});

});
</script>