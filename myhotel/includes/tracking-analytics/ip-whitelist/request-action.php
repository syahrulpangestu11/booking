<?php
session_start();
include "../../../conf/connection.php";

try{
	
	switch($_POST['request']){
		case "add" :
			$stmt = $db->prepare("insert into ip_whitelist (hoteloid, ip, note) value (:a, :b, :c)");
			$stmt->execute(array(':a' => $_SESSION['_hotel'], ':b' => $_POST['ip'], ':c' => $_POST['note']));
			$ipoid = $db->lastInsertId();
			echo "success";
		break;

		case "edit" :
			$stmt = $db->prepare("update ip_whitelist set ip = :a, note = :b where ip_whitelistoid = :id");
			$stmt->execute(array(':a' => $_POST['ip'], ':b' => $_POST['note'], ':id' => $_POST['ipid']));
			echo "success";
		break;
		
		case "delete" :
			$stmt = $db->prepare("delete from ip_whitelist where ip_whitelistoid = :a");
			$stmt->execute(array(':a' => $_POST['ipid']));
			echo "success";
		break;
		
		case "detail-ip" :
			$stmt = $db->prepare("select * from ip_whitelist where ip_whitelistoid = :a");
			$stmt->execute(array(':a' => $_POST['ipid']));
			$r_ip = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_ip as $row){
			?>
                <div class="form-group">
                    <label class="col-sm-2 control-label">IP Number</label>
                    <div class="col-sm-10"><input type="text" class="form-control" name="ip" value="<?=$row['ip']?>"><input type="hidden" name="ipid" value="<?=$row['ip_whitelistoid']?>"></div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Note</label>
                    <div class="col-sm-10"><textarea class="form-control" name="note"><?=$row['note']?></textarea></div>
                </div>
            <?php
			}
		break;
	}
	
}catch(Exception $ex) {
	echo "error ".$ex->getMessage();
}
?>
