<div class="modal fade" id="CPModal" tabindex="-1" role="dialog" aria-labelledby="CPModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Cancellation Policy</h4>
      </div>
      <div class="modal-body">
        <form method="post" id="form-assign-cp" class="form-inline" enctype="multipart/form-data" action="#">
            <div class="form-group">
                <div class="col-md-6">
                	<label>Start Date</label>
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                        <input type="text" class="form-control" id="sdate" name="startdate" value="<?php echo date('d F Y');?>">
                    </div>
                </div>
                <div class="col-md-6">
                    <label>End Date</label>
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                        <input type="text" class="form-control" id="edate" name="enddate" value="<?php echo date('d F Y',strtotime('+1 year'));?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
            	<div class="col-md-12"><label>Room</label></div>
				<div class="col-md-12 text-right">
                    <div class="checkbox">
                        <label><input type="radio" name="choose-room" value="select" /> select all</label>
                        <label><input type="radio" name="choose-room" value="unselect" checked="checked" /> unselect all</label>
                    </div>
                </div>
				<?php
                    $stmt = $db->query("select ro.roomofferoid, ro.name, ot.offername from roomoffer ro inner join room r using (roomoid) inner join offertype ot using (offertypeoid) where r.hoteloid = '".$hoteloid."' and ro.publishedoid = '1' and r.publishedoid = '1'");
                    $r_roomoffer = $stmt->fetchAll(PDO::FETCH_ASSOC);
                    foreach($r_roomoffer as $roomoffer){
                    ?>
                    <div class="col-md-4">
                        <div class="checkbox">
                            <label><input type="checkbox" name="roomoffer[]" value="<?=$roomoffer['roomofferoid']?>" /> <?=$roomoffer['name']?></label>
                        </div>
                    </div>
                    <?php
                    }
                ?>   
            </div>
            <div class="form-group">
                <div class="col-md-12">
                	<label>Cancellation Policy</label>
                    <div class="input-group">
                        <select name="cancellationpolicy" class="form-control">
                        <?php
                            try {
                                $stmt = $db->query("select cp.* from cancellationpolicy cp inner join published p using (publishedoid) where p.publishedoid = '1' and cp.hoteloid in ('0','".$hoteloid."')");
                                $r_cp = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                foreach($r_cp as $row){
                                    echo"<option value='".$row['cancellationpolicyoid']."' note='".$row['description']."'>".$row['name']."</option>";
                                }
                            }catch(PDOException $ex) {
                                echo "Invalid Query";
                                die();
                            }
                        ?>        	
                        </select>
                    </div>
                </div>
			</div>
            <div class="row">
            	<div class="col-md-12 note-cp" style="padding-top:10px; font-size:0.9em;"></div>
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="assign-cp">Set Cancellation Policy</button>
      </div>
    </div>
  </div>
</div>