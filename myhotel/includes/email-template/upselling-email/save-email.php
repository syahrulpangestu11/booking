<?php
  $stmt = $db->prepare("select count(emailupsellingoid) as setupselling from emailupselling where hoteloid = :a");
  $stmt->execute(array(':a' => $_SESSION['_hotel']));
  $found = $stmt->fetch(PDO::FETCH_ASSOC);
  if($found['setupselling'] == 0){
    $stmt = $db->prepare("insert into emailupselling (hoteloid, active, temporarytime, mailingtime, subject, mailbodyheader, mailbodyfooter, mailtagline) value (:a, :b, :c, :d, :e, :f, :g, :h)");
    $stmt->execute(array(':a' => $_SESSION['_hotel'], ':b' => $_POST['upselling-email-activate'], ':c' => $_POST['temporary-time'], ':d' => $_POST['mailing-time'], ':e' => $_POST['subject'], ':f' => $_POST['header'], ':g' => $_POST['footer'], ':h' => $_POST['tagline']));
  }else{
    $stmt = $db->prepare("update emailupselling set active = :b, temporarytime = :c, mailingtime = :d, subject = :e, mailbodyheader = :f, mailbodyfooter = :g, mailtagline = :h where hoteloid = :a");
    $stmt->execute(array(':a' => $_SESSION['_hotel'], ':b' => $_POST['upselling-email-activate'], ':c' => $_POST['temporary-time'], ':d' => $_POST['mailing-time'], ':e' => $_POST['subject'], ':f' => $_POST['header'], ':g' => $_POST['footer'], ':h' => $_POST['tagline']));
  }

  header("Location: ". $base_url ."/email-template/email-upselling");
?>
