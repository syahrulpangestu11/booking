<section class="content-header">
    <h1>
       	Email Template
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-cog"></i> Setting</a></li>
        <li class="active">Email Template</li>
    </ol>
</section>
<section class="content">
  <div class="box">
    <div class="box-body">
      <table class="table">
        <thead>
          <tr>
            <th></th>
            <th>Email Template</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <?php
                $stmt = $db->prepare("select active from emailupselling where hoteloid = :a");
                $stmt->execute(array(':a' => $_SESSION['_hotel']));
                $result = $stmt->fetch(PDO::FETCH_ASSOC);
              ?>
              <label class="switch">
                <input type="checkbox" name="upselling-email-activate" value="y" <?php if($result['active'] == "y"){ echo "checked"; } ?>>
                <span class="slider round"></span>
              </label>
            </td>
            <td>Upselling Email</td>
            <td><a href="<?=$base_url?>/email-template/upselling-email"><button type="button" name="setting-upselling-email" class="btn btn-danger"><i class="fa fa-cog"></i> Setting</button></a></td>
          </tr>
          <tr style="display:none">
            <td>
              <label class="switch">
                <input type="checkbox" name="poststay-email" value="y">
                <span class="slider round"></span>
              </label>
            </td>
            <td>Post-Stay Email</td>
            <td><button type="button" name="setting-upselling-email" class="btn btn-danger"><i class="fa fa-cog"></i> Setting</button></td>
          </tr>
          <tr style="display:none">
            <td>
              <label class="switch">
                <input type="checkbox" name="prestay-email" value="y">
                <span class="slider round"></span>
              </label>
            </td>
            <td>Pre-Stay Email</td>
            <td><button type="button" name="setting-upselling-email" class="btn btn-danger"><i class="fa fa-cog"></i> Setting</button></td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</section>
