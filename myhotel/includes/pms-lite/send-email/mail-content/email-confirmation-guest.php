<?php
try{
  session_start();
	error_reporting(E_ALL ^ E_NOTICE);
  include ('conf/connection.php');
  include("includes/pms-lite/class-pms-lite.php");

  $bookingnumber = $_POST['bookingnumber'];
  $hoteloid = $_POST['hotel'];

  $pmslite = new PMSEditRSV($db);

  $pmslite->setBooking($bookingnumber);
  $datarsv = $pmslite->getBookingData();

  $dataroom = $pmslite->getBookingRoomData();
  $datacharge = $pmslite->getBookingChargeData();

  $pmslite->setPMSHotel($datarsv['hoteloid']);

  $pmslitersv = new PMSReservation($db);
  $pmslitersv->setBooking($bookingnumber);
  $stayoccupancy = $pmslitersv->summaryStayOccupancy();
  $pmslitersv->RecalculateBooking($datarsv['bookingoid']);

  if(!empty($pmslite->hoteldata['banner'])){
    $emailbanner	= $pmslite->hoteldata['banner'];
  }else{
    $emailbanner	= 'https://www.thebuking.com/ibe/image/header.jpg';
  }
?>
<body style="width:800px; height:auto; margin:0 auto;">
  <div style="width:100%; font-family:Arial, Helvetica, sans-serif; font-size:12px; margin:0;">
    <div><img src="<?=$emailbanner?>" style="height:auto; width:100%;" /></div>
    <div style="background-color:#59BBF0; text-align:center; padding:5px 10px; margin:0;"><h1 style="font-size:3em; color:#FFFFFF;">Stay excited.</h1></div>
    <div style="padding:10px 0;">
    	<h2 style="color:#59BBF0; font-size:1.5em; margin:20px 0;">Look forward to an exciting stay with us.</h2>

    	Dear <?=$datarsv['guestname'];?>,
      <p>We are delighted that you have chosen <?=$pmslite->hoteldata['hotelname']?>. <?=$pmslite->hoteldata['hotelname']?> has a lot to offer, and we have a host of tips and offers to make your visit an unforgettable one. Please find attached your reservation details and information about our hotel and the city.</p>

    	<h2 style="color:#59BBF0; font-size:1.5em; margin:20px 0;">Your Reservation - Overview</h2>

      <div style="text-align:right; margin-bottom:15px;"><b>Booking number : <?=$datarsv['bookingnumber'];?></b></div>

    <?php if(count($dataroom) > 0){ ?>
      <b>Room Details : </b><br>
      <table style="border:none; width:100%; font-family:Arial, Helvetica, sans-serif; font-size:12px; margin-bottom:20px;" cellpadding="0" cellspacing="0">
        <thead>
          <tr>
            <th style="border-bottom:2px solid #CCCCCC; padding : 5px 2px;">No.</th>
            <th style="border-bottom:2px solid #CCCCCC; padding : 5px 2px;">Room</th>
            <th style="border-bottom:2px solid #CCCCCC; padding : 5px 2px;">Check-in</th>
            <th style="border-bottom:2px solid #CCCCCC; padding : 5px 2px;">Check-out</th>
            <th style="border-bottom:2px solid #CCCCCC; padding : 5px 2px;">Extra Bed</th>
            <th style="border-bottom:2px solid #CCCCCC; padding : 5px 2px;">Room Charge</th>
            <th style="border-bottom:2px solid #CCCCCC; padding : 5px 2px;">Remark</th>
          </tr>
        </thead>
        <tbody>
          <?php
            $no = 0;
            foreach($dataroom as $dr){
              $room = $pmslite->detailRatePlan($dr['roomofferoid']);
          ?>
          <tr>
            <td><?=++$no?></td>
            <td><?=$dr['room']?><br />for <?=$dr['adult']?> adult <?=$dr['child']?> children</td>
            <td class="text-center"><?=date('d/M/Y', strtotime($dr['checkin']))?></td>
            <td class="text-center"><?=date('d/M/Y', strtotime($dr['checkoutr']))?></td>
            <td class="text-center"><?=$dr['extrabed']?></td>
            <td class="text-right"><?=$datarsv['currencycode']." ".number_format($dr['totalr'])?></td>
            <td><?=$dr['promotion']?></td>
          </tr>
          <?php
            }
          ?>
        </tbody>
      </table>
    <?php } ?>

    <?php if(count($datacharge) > 0){ ?>
      <b>Other Charges : </b><br>
      <table style="border:none; width:100%; font-family:Arial, Helvetica, sans-serif; font-size:12px; margin-bottom:20px;" cellpadding="0" cellspacing="0">
        <thead>
          <tr>
            <th style="border-bottom:2px solid #CCCCCC; padding : 5px 2px;">No.</th>
            <th style="border-bottom:2px solid #CCCCCC; padding : 5px 2px;">POS</th>
            <th style="border-bottom:2px solid #CCCCCC; padding : 5px 2px;">Product</th>
            <th style="border-bottom:2px solid #CCCCCC; padding : 5px 2px;">Price</th>
            <th style="border-bottom:2px solid #CCCCCC; padding : 5px 2px;">Qty</th>
            <th style="border-bottom:2px solid #CCCCCC; padding : 5px 2px;">Total</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach($pmslite->getBookingChargeData() as $key => $othercharges){ ?>
          <tr>
            <td><?=$key+1?></td>
            <td><?=$othercharges['pos']?></td>
            <td><?=$othercharges['product']?></td>
            <td align="right"><?=$datarsv['currencycode']." ".number_format($othercharges['price'])?></td>
            <td align="center"><?=$othercharges['qty']?></td>
            <td align="right"><?=$datarsv['currencycode']." ".number_format($othercharges['total'])?></td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    <?php } ?>

      <table style="border:none; width:100%; font-family:Arial, Helvetica, sans-serif; font-size:12px; margin-bottom:20px;" cellpadding="0" cellspacing="0">
        <tbody>
          <tr>
            <td width="25%" style="padding : 5px 2px; border-bottom:1px dotted #999999;"><b>Grand Total</b></td><td style="padding : 5px 2px; border-bottom:1px dotted #999999;"><?=$datarsv['currencycode']." ".number_format($datarsv['grandtotal'])?></td>
          </tr>
        </tbody>
      </table>

      <table border="0" width="100%">
        <tr>
          <td width="50%">
            <h3 style="font-size:1.3em; color:#59BBF0;">Guest Information</h3>
              <table style="border:none; width:auto; font-family:Arial, Helvetica, sans-serif; font-size:12px; margin-bottom:20px;" cellpadding="0" cellspacing="0">
                  <tbody>
                      <tr><td width="30%" style="padding : 5px 2px; border-bottom:1px dotted #999999;"><b>Name</b></td><td style="padding : 5px 2px; border-bottom:1px dotted #999999;"><?php echo $datarsv['guestname']; ?></td></tr>
                      <tr><td style="padding : 5px 2px; border-bottom:1px dotted #999999;"><b>Email</b></td><td style="padding : 5px 2px; border-bottom:1px dotted #999999;"><?php echo $datarsv['email']; ?></td></tr>
                      <tr><td style="padding : 5px 2px; border-bottom:1px dotted #999999;"><b>Phone</b></td><td style="padding : 5px 2px; border-bottom:1px dotted #999999;"><?php echo $datarsv['phone']; ?></td></tr>
                      <tr><td style="padding : 5px 2px; border-bottom:1px dotted #999999;"><b>City, Country</b></td><td style="padding : 5px 2px; border-bottom:1px dotted #999999;"><?php echo $datarsv['city']; ?>, <?php echo $datarsv['countryname']; ?>s</td></tr>
                  </tbody>
              </table>
          </td>
        </tr>
      </table>

      <div style="margin-bottom:10px;">
        <p>If you want to contact our team, please email to <span style="font-weight:bold; text-decoration:underline"><?=$pmslite->hoteldata['email'];?></span><br /> Please note that the currency conversions shown on the Site are for comparison purposes only.</p>
        <p>Thank you for choosing <span style="font-weight:bold; color:#59BBF0;"><?=$hotelname;?></span> and we look forward to welcoming you to our hotel.</p>
      </div>
      <div style="margin-bottom:10px;">
        <b>Powered by :</b><br />
        <img src="http://www.thebuking.com/ibe/image/thebuking-red.png" style="max-width:80px; height:auto;" /></h1>
      </div>
  </div>
</body>
<?php
}catch(Exception $e){
  echo $e->getMessage();
}
?>
