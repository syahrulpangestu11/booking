<div class="modal fade pms-lite-modal" id="assignRoom" tabindex="-1" role="dialog" aria-labelledby="assignRoom">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Tempory Room List</h4>
      </div>
      <div class="modal-body">Please wait a while <i class="fa fa-smile"></i>
      </div>
    </div>
  </div>
</div>

<div class="modal fade pms-lite-modal" id="selectAssignRoom" tabindex="-1" role="dialog" aria-labelledby="selectAssignRoom">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Select Room</h4>
      </div>
      <div class="modal-body">Please wait a while <i class="fa fa-smile"></i>
      </div>
    </div>
  </div>
</div>

<div class="modal fade pms-lite-modal" id="createReservation" tabindex="-1" role="dialog" aria-labelledby="createReservation">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Quick Reservation</h4>
      </div>
      <div class="modal-body">
        Please wait a while <i class="fa fa-smile"></i>
      </div>
    </div>
  </div>
</div>

<div class="modal fade pms-lite-modal" id="viewDetail" tabindex="-1" role="dialog" aria-labelledby="viewDetail">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Guest Detail</h4>
      </div>
      <div class="modal-body">
        Please wait a while <i class="fa fa-smile"></i>
      </div>
    </div>
  </div>
</div>

<div class="modal fade pms-lite-modal" id="informationModal" tabindex="-1" role="dialog" aria-labelledby="informationModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Notification</h4>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade pms-lite-modal" id="confirmChangeRate" tabindex="-1" role="dialog" aria-labelledby="confirmChangeRate">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Override Rate Notification</h4>
      </div>
      <div class="modal-body">This will override rate. Would you like to change rate?
      </div>
      <div class="modal-footer">
        <button type="button" name="changeoption" class="btn btn-danger" rate="override">Yes, Override Rate</button>
        <button type="button" name="changeoption" class="btn btn-primary" rate="current">No, Continue with current rate</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade pms-lite-modal" id="createOOO" tabindex="-1" role="dialog" aria-labelledby="createOOO">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Out Of Service</h4>
      </div>
      <div class="modal-body">Please wait a while <i class="fa fa-smile"></i>
      </div>
    </div>
  </div>
</div>

<div class="modal fade pms-lite-modal" id="viewOOO" tabindex="-1" role="dialog" aria-labelledby="createOOO">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Out Of Service</h4>
      </div>
      <div class="modal-body">Please wait a while <i class="fa fa-smile"></i>
      </div>
    </div>
  </div>
</div>

<div class="modal fade pms-lite-modal" id="editOOO" tabindex="-1" role="dialog" aria-labelledby="editOOO">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Out Of Service</h4>
      </div>
      <div class="modal-body">Please wait a while <i class="fa fa-smile"></i>
      </div>
    </div>
  </div>
</div>
