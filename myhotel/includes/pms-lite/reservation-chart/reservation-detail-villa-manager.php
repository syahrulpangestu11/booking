<?php
  include("includes/pms-lite/class-pms-lite.php");
  include("includes/pms-lite/reservation-chart/script-reservation-chart.php");

  $bookingoid = $_POST['bookingoid'];
  $hoteloid = $_POST['hotel'];

  $pmslite = new PMSEditRSV($db);
  $pmslite->setPMSHotel($hoteloid);
  $pmslite->setBookingbyID($bookingoid);
  $datarsv = $pmslite->getBookingData();
  $dataroom = $pmslite->getBookingRoomData();

  $pmslitersv = new PMSReservation($db);
  $pmslitersv->setBookingbyID($bookingoid);
  $stayoccupancy = $pmslitersv->summaryStayOccupancy();
  $pmslitersv->RecalculateBooking($datarsv['bookingoid']);
?>
<style>
  tr.breakdown-rate{ display: none;}
</style>
<section class="content-header">
  <h1>Reservation Chart</h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-circle"></i>  PMS Lite</a></li>
    <li><a href="#"><i class="fa fa-circle"></i>  Reservation Chart</a></li>
  </ol>
</section>
<section id="pms-lite" class="content" style="margin-left: 5px;margin-right: 5px;padding-left: 10px;padding-right: 10px;">
  <div class="box box-form box-success">
    <div class="box-body">
      <div class="row">
        <div class="col-md-6">
          <h1>Booking Number : <?php echo $datarsv['bookingnumber']; ?></h1>
        </div>
        <div class="col-md-6 text-right">
          <i class="fa fa-clock-o"></i> <?php echo $datarsv['bookdate']; ?><br>
          <i class="fa fa-lock"></i> PIN : <?php echo $datarsv['pin']; ?>
        </div>
      </div>
      <hr class="hr-wide" />
      <div class="row">
        <div class="col-md-6">
          <div class="row"><div class="col-md-12"><h3>Guest Details</h3></div></div>
          <div class="row">
            <div class="col-md-12">
              <h2><?php echo $datarsv['guestname']; ?></h2>
              <?php echo $datarsv['city']; ?>, <?php echo $datarsv['countryname']; ?><br>
              &#9742; <?php echo $datarsv['phone']; ?><br>
              &#9993; <?php echo $datarsv['email']; ?><br>
            </div>
          </div>
          <hr class="hr-dotted">
          <div class="row"><div class="col-md-12"><h3>Sales / Guest Request</h3></div></div>
          <div class="row">
            <div class="col-md-12">
              <?php if(!empty($datarsv['note'])){ echo $datarsv['note']; }else{ echo "n/a"; } ?>
            </div>
          </div>
          <hr class="hr-dotted">
          <div class="row">
            <div class="col-md-12">
              <h3>Status</h3>
              <?=$datarsv['bookingstatus']?><br>
              Last Updated  <?=date('d F Y H:i:s', strtotime($datarsv['updated']))?> by <?=$datarsv['updatedby']?>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="row">
            <div class="col-md-6">
              <h3>Stay Details</h3>
              <?php
                $q_stay_details = "select count(bookingroomoid), br.checkin, br.checkout, br.night from bookingroom br inner join booking b using (bookingoid) where b.bookingoid = '".$bookingoid."' group by br.checkin, checkout";
                $stmt = $db->query($q_stay_details);
                $r_stay_detail = $stmt->fetchAll(PDO::FETCH_ASSOC);
                foreach($r_stay_detail as $stay){
              ?>
                <div class="row"><div class="col-md-5">Check in:</div><div class="col-md-5"><?=date('d F Y', strtotime($stay['checkin']))?></div></div>
                <div class="row"><div class="col-md-5">Duration:</div><div class="col-md-5"><?=$stay['night']?> nights</div></div>
                <div class="row"><div class="col-md-5">Check out:</div><div class="col-md-5"><?=date('d F Y', strtotime($stay['checkout']))?></div></div>
              <?php
                }
              ?>
            </div>
            <div class="col-md-6">
              <div class="row"><div class="col-md-12"><h3>Arrival</h3></div></div>
              <div class="row"><div class="col-md-5">Arrival Flight:</div><div class="col-md-5"><?=$datarsv['arrivalflight']?></div></div>
              <div class="row"><div class="col-md-5">Arrival Date:</div><div class="col-md-5"><?php if(!empty($datarsv['arrivaldate'])){ echo date('d F Y', strtotime($datarsv['arrivaldate'])); } ?></div></div>
              <div class="row"><div class="col-md-5">Arrival Time:</div><div class="col-md-5"><?php if(!empty($datarsv['arrivaltime'])){ echo date('H:i', strtotime($datarsv['arrivaltime'])); } ?></div></div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <hr class="hr-dotted" />
              <h3>Room Details</h3>
              <div class="row"><div class="col-md-5">Number of Rooms:</div><div class="col-md-5"><?=$stayoccupancy['jmlroom']?> room(s)</div></div>
              <div class="row"><div class="col-md-5">Adults:</div><div class="col-md-5"><?=$stayoccupancy['adult']?></div></div>
              <div class="row"><div class="col-md-5">Children:</div><div class="col-md-5"><?=$stayoccupancy['child']?></div></div>
              <div class="row"><div class="col-md-5">Total Guest:</div><div class="col-md-5"><?=$stayoccupancy['person']?></div></div>
            </div>
          </div>
        </div>
      </div>
      <hr />
      <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-12">
              <h3>Promotion / Rate Details</h3>
              <table class="table table-bordered" id="table-detail-rsv">
                <thead>
                  <tr><th rowspan="2">No.</th><th rowspan="2">Room Type</th><th rowspan="2">Room</th><th rowspan="2">Remark</th><th rowspan="2">Guest Name</th><th colspan="2">Stay Periode</th><th colspan="2">Occupancy</th><th rowspan="2">Extra Bed</th><th rowspan="2">Status</th></tr>
                  <tr><th>Check-in</th><th>Check-out</th><th>Adult</th><th>Children</th></tr>
                </thead>
                <tbody>
                  <tr style="display:none"></tr>
                  <?php
                    $no = 0;
                    foreach($dataroom as $dr){
                      $room = $pmslite->detailRatePlan($dr['roomofferoid']);
                  ?>
                  <tr>
                    <td><?=++$no?></td>
                    <td><?=$dr['room']?></td>
                    <td><?=$pmslite->getRoomNumber($dr['bookingroomoid'])?></td>
                    <td><?=$dr['promotion']?></td>
                    <td><?=$dr['guestroom']?></td>
                    <td class="text-center"><?=date('d M Y', strtotime($dr['checkin']))?></td>
                    <td class="text-center"><?=date('d M Y', strtotime($dr['checkoutr']))?></td>
                    <td class="text-center"><?=$dr['adult']?></td>
                    <td class="text-center"><?=$dr['child']?></td>
                    <td class="text-center"><?=$dr['extrabed']?></td>
                    <td><?=$dr['pmsstatus']?></td>
                  </tr>
                  <tr class="breakdown-rate">
                    <td colspan="9" style="padding:10px;">
                      <table class="table table-bordered" id="table-detail-rsv">
                        <thead>
                          <tr><th>Guest Name</th><th>Date</th><th>Rate</th><th>Room Number</th></tr>
                        </thead>
                        <tbody>
                          <tr style="display:none"></tr>
                          <?php
                          foreach($pmslite->roomDateDetail($dr['bookingroomoid']) as $brd){
                          ?>
                          <tr>
                            <td><div class="form-group"><input type="text" class="form-control" name="guest[]" value="<?=$brd['guestroom']?>"></td>
                            <td class="text-center"><?=date('d/M/Y', strtotime($brd['date']))?></div></td>
                            <td class="text-center"><?=$brd['currencycode']." ".number_format($brd['total'],2)?></div></td>
                            <td>
                              <select name="roomnumber[]">
                                <option value="<?=$brd['roomnumberoid']?>"><?=$brd['roomnumber']?></option>
                                <?php
                                foreach($pmslite->roomDateDetail($dr['bookingroomoid']) as $availableroom){
                                ?>
                                <option value="<?=$brd['roomnumberoid']?>"><?=$brd['roomnumber']?></option>
                                <?php
                                }
                                ?>
                              </select>
                            </td>
                          </tr>
                          <?php
                          }
                          ?>
                        </tbody>
                      </table>
                    </td>
                  </tr>
                  <?php
                    }
                  ?>
                </tbody>
              </table>
            </div>
          </div>

          <hr />

          <div class="row title-manage">
            <div class="col-md-6"><h3>Other Charges</h3></div>
            <div class="col-md-6 text-right"><button type="button" name="other-charge" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#otherCharges">Add Other Charge</button></div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <table class="table table-bordered table-other-charge" id="table-detail-rsv">
                <thead>
                  <tr><th>No.</th><th>POS</th><th>Product</th><th>Price (<?=$datarsv['currencycode']?>)</th><th>Qty</th><th>Total (<?=$datarsv['currencycode']?>)</th><?php if($_SESSION['_typepmsusr'] == '4'){ ?><th>&nbsp;</th><?php } ?></tr>
                </thead>
                <tbody>
                  <tr style="display:none"></tr>
                  <?php
                  foreach($pmslite->getBookingChargeData() as $key => $othercharges){
                  ?>
                  <tr>
                    <td><?=$key+1?></td>
                    <td><?=$othercharges['pos']?></td>
                    <td><?=$othercharges['product']?></td>
                    <td class="text-right"><?=number_format($othercharges['price'],2)?></td>
                    <td class="text-center"><?=$othercharges['qty']?></td>
                    <td class="text-right"><?=number_format($othercharges['total'],2)?></td>
                    <?php if($_SESSION['_typepmsusr'] == '4'){ ?>
                    <td class="text-center">
                      <button type="button" class="btn btn-xs btn-danger" data-bcid="<?=$othercharges['bookingchargesoid']?>" data-toggle="modal" data-target="#confirmationRemoveCharges"><i class="fa fa-trash"></i></button>
                    </td>
                    <?php } ?>
                  </tr>
                  <?php
                  }

                  if(empty($othercharges)){
                    echo "<tr><td colspan='7' class='text-center'><i>no data available</i></td></tr>";
                  }
                  ?>
                </tbody>
                <tfoot>
                  <tr>
                    <th colspan="5">Total Other Charge</th>
                    <th colspan="2"><?=number_format($datarsv['totalcharge'],2)?></th>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>

      <hr />

      <div class="row">
        <div class="col-md-4">
          <div class="row"><div class="col-md-12"><h3>Source Detail</h3></div></div>
          <?php
            $source = $pmslite->BookingSource();
          ?>
          <div class="row">
            <div class="col-md-12">
              <div class="row"><div class="col-md-5">Channel:</div><div class="col-md-5"><?=$source['bookingchannel']?></div></div>
              <div class="row"><div class="col-md-5">Booking Market:</div><div class="col-md-5"><?=$source['bookingmarket']?></div></div>
            </div>
          </div>
          <?php if($datarsv['agentoid'] != '0'){ ?>
          <div class="row" style="margin-top:10px">
            <div class="col-md-12">
              <div class="row"><div class="col-md-5">Travel Agent:</div><div class="col-md-5"><?=$source['agentname']?></div></div>
              <div class="row"><div class="col-md-5">Agent Phone:</div><div class="col-md-5"><?=$source['agentphone']?></div></div>
              <div class="row"><div class="col-md-5">Agent Email:</div><div class="col-md-5"><?=$source['agentemail']?></div></div>
            </div>
          </div>
          <!--
          <div class="row">
            <div class="col-md-12">
              <button type="button" class="btn btn-default" name="email-invoice-agent">Email invoive to agent</button>
              <a href="<?=$base_url?>/pms-lite/view/invoice-agent.php?bookingnumber=<?=$datarsv['bookingnumber']?>" target="_blank"><button type="button" class="btn btn-primary" name="email-invoice-agent">Print invoice to agent</button></a>
            </div>
          </div>
          -->
          <?php } ?>
          <div class="row" style="margin-top:10px">
            <div class="col-md-12">
              <div class="row"><div class="col-md-5">UTM:</div><div class="col-md-5"><?=$datarsv['utm_source']?> -<?=$datarsv['utm_campaign']?></div></div>
            </div>
          </div>
        </div>
        <div class="col-md-8 text-right" id="actButton">
        <?php
          if($datarsv['bookingstatusoid'] != "5" and $datarsv['bookingstatusoid'] != "7"){
            if($dataroom[0]['pmsstatusoid'] == 2){
              if($dataroom[0]['checkin'] <= date('Y-m-d')){
            ?>
              <button type="button" name="pmsstatus" value="4" class="btn btn-success">Check in</button>
            <?php
              }
            }else if($dataroom[0]['pmsstatusoid'] == 4){
              if(date('Y-m-d') < $dataroom[0]['checkout']){
            ?>
              <button type="button" name="pmsstatus" value="5" class="btn btn-warning">Early Check out</button>
              <?php
              }else{
              ?>
                <button type="button" name="pmsstatus" value="5" class="btn btn-warning">Check out</button>
            <?php
              }
            }
            ?>
        <?php
          }
        ?>
            <button type="button" class="btn btn-primary" name="view-invoice">View Invoice</button>
            <a href="<?=$base_url?>/pms-lite/reservation-chart"><button type="button" class="btn btn-default" name="back"><i class="fa fa-undo"></i> Back To Reservation Chart</button></a>
        </div>
    </div>
  </div>
</section>
<form id="form-view-guest" method="post"><input type="hidden" name="bookingroom" value="<?=$dataroom[0]['bookingroomoid']?>"></form>
<form id="invoice-guest" action="<?=$base_url?>/pms-lite/view/invoice-guest.php" target="_blank" method="post">
  <input name="bookingnumber" type="hidden" value="<?=$datarsv['bookingnumber']?>">
  <input name="hotel" type="hidden" value="<?=$hoteloid?>">
</form>
<?php include('modal-edit-rsv.php'); ?>
