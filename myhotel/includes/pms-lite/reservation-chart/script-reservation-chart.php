<script type="text/javascript">
$(function(){

  Number.prototype.formatMoney = function(c, d, t){
    var n = this,
    c = isNaN(c = Math.abs(c)) ? 2 : c,
    d = d == undefined ? "." : d,
    t = t == undefined ? "," : t,
    s = n < 0 ? "-" : "",
    i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
    j = (j = i.length) > 3 ? j % 3 : 0;
   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
  };

  $('#assignRoom').on('show.bs.modal', function (event){
    var modal = $(this);
    var sourceblocked = $(event.relatedTarget); // Button that triggered the modal
    var date = sourceblocked.data('date');
    var hotel = sourceblocked.data('hotel');  // Extract info from data-* attributes
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    $.post('<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function/blocked-list-room-toassign.php', { date : date, hotel : hotel }, function(response){
      modal.find('.modal-body').html(response);
		});
  });

  $('#createReservation').on('show.bs.modal', function (event){
    var modal = $(this);
    var sourceblocked = $(event.relatedTarget);
    var date = sourceblocked.data('date');
    var room = sourceblocked.data('room');
    var roomnumber = sourceblocked.data('roomnumber');
    var hotel = sourceblocked.data('hotel');
    $.post('<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function/create-reservation.php', { date : date, room : room, roomnumber : roomnumber, hotel:hotel }, function(response){
      modal.find('.modal-body').html(response);

      $('textarea[name=note]').trumbowyg({
				btns: ['viewHTML',
				  '|', 'btnGrp-design',
				  '|', 'link',
				  '|', 'btnGrp-lists'],
				fullscreenable: false
			});

      showAvailableRoom();
      showPrice();
      $('form#form-create-reservation').validate({
        ignore: [],
    		rules: {
          title: "required",
    			firstname: { required: true, minlength: 2	},
    			lastname: { required: true, minlength: 2 },
    			email: { email: true },
    			country: "required"
        },
        messages: {
    			email: "required valid email."
        }
      });

		});
  });

  $('#createReservation, #editBookingRoom, #createOOO, #editOOO').on('focus',"#startdate-today, #enddate-today", function(){
    elem_startdate = $(this).parent().parent().parent().parent().parent().find('input#startdate-today');
    elem_enddate = $(this).parent().parent().parent().parent().parent().find('input#enddate-today');
    elem_startdate.datepicker({
      defaultDate: "+1w",
      changeMonth: true, changeYear:true, dateFormat: "dd M yy",
      onClose: function( selectedDate ) {
        var d = new Date(selectedDate);
        d.setDate(d.getDate() + 1);
        elem_enddate.datepicker( "option", "minDate", d );
      }
    });
    elem_enddate.datepicker({
        defaultDate: "+1w",
        changeMonth: true, changeYear:true, dateFormat: "dd M yy",
        onClose: function( selectedDate ) {
          elem_startdate.datepicker( "option", "maxDate", selectedDate );
        }
    });
  });

  $('#createReservation').on('keyup',"input[name=firstname], input[name=lastname]", function(){
    var guestroom = $('input[name=firstname]').val()+" "+$('input[name=lastname]').val();
    $('#createReservation input[name="guest[]"]').each(function(e){
      $(this).val(guestroom);
    });

  });

  $('#createReservation').on('change', 'input[name=checkin], input[name=checkout]', function() {
    showAvailableRoom();
  });

  $('#createReservation').on('change', 'input[name=checkin], input[name=checkout], select[name=roomofferoid], select[name=rooms]', function() {
    showPrice();
  });

  $('#createReservation').on('change', 'select[name=bookingmarket]', function() {
      bookingmarket = $(this).val();
      if(bookingmarket != 1){
        $('#confirmChangeRate').modal('show');
      }else{
        showPrice();
      }
  });

  $('#confirmChangeRate').on('click', 'button[name=changeoption]', function() {
    option = $(this).attr('rate');
    bookingmarket = $('#createReservation select[name=bookingmarket]').find(":selected").text();
    oldbookingmarket = $('input[name="oldbookingmarket"]').val();
    if(oldbookingmarket == 1 || bookingmarket == 1 || option == "override"){
      showPrice();
    }
    $('input[name="oldbookingmarket"]').val(bookingmarket);
    $('#confirmChangeRate').modal('hide');
  });


  function showAvailableRoom(){
    $.ajax({
      url: "<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function/show-available-room.php",
      type: 'post',
      data: $('form#form-create-reservation').serialize(),
      success: function(response2) {
        $('#createReservation').find('.modal-body').find('select[name=rooms]').html(response2);
      }
    });
  }

  function showPrice(){
    $.ajax({
      url: "<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function/show-price.php",
      type: 'post',
      data: $('form#form-create-reservation').serialize(),
      success: function(response2) {
        rsp = response2.split('|');
        $('#createReservation').find('.modal-body').find('div.show-detail-create-rsv').html(rsp[0]);
        $('#createReservation').find('.modal-body').find('div.button-response').html(rsp[1]);
        $("input[fnc='dailyrate']").keyup();
      }
    });
  }

  $('#editBookingRoom').on('change', 'input[name="checkin"], input[name="checkout"], select[name="roomofferoid"]', function() {
    showEditPrice();
  });

  $('#editBookingRoom').on('change', 'select[name=roomtypeoid]', function() {
    $.ajax({
      url: "<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function-edit-rsv/change-roomtype.php",
      type: 'post',
      data: $('form#form-edit-reservation').serialize(),
      success: function(response2) { 
        $('#editBookingRoom').find('.modal-body').find('select[name=roomofferoid]').html(response2);
        showEditPrice();
      }
    });
  });

  function showEditPrice(){
    $.ajax({
      url: "<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function-edit-rsv/edit-show-price.php",
      type: 'get',
      data: $('form#form-edit-reservation').serialize(),
      success: function(response2){
        rsp = response2.split('|');
        $('#editBookingRoom').find('.modal-body').find('div.show-detail-create-rsv').html(rsp[0]);
        $('#editBookingRoom').find('.modal-body').find('div.button-response').html(rsp[1]);
        editSpecialRsvTotal();
      }
    });
  }

  $('#viewDetail').on('show.bs.modal', function (event) {
    var modal = $(this);
    var sourceblocked = $(event.relatedTarget);
    var id = sourceblocked.data('id');
    var hotel = sourceblocked.data('hotel');
    $.post('<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function/view-guest.php', { id:id, hotel:hotel }, function(response){
      modal.find('.modal-body').html(response);
		});
  });

  $('#viewDetail').on('click', 'button[name="print"]', function() {
    $('form#reservation-card').submit();
    $('.modal').modal('hide');
  });

  $('body').on('click', 'button[name="view-detail"]', function() {
    $('form#view-reservation-detail').submit();
  });

  $('body').on('click', '#assignRoom button[name="assign-room"]', function(e) {
    bookingroom = $(this).attr("data-bookingroom");
    hotel = $(this).attr("data-hotel");
    $.ajax({
      url: "<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function/blocking-room-assign.php",
      type: 'post',
      data: { bookingroom : bookingroom, hotel : hotel },
      success: function(response) {
        $('#assignRoom').find('.modal-body').html(response);
      }
    });
  });

  $('#assignRoom').on('click', 'button[name="next-action"]', function() {
    pmsstatus = $(this).val();
    $.post('<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function/blocking-room-save.php',  $('form#form-edit-reservation').serialize() + '&pmsstatus='+pmsstatus, function(response){
      if(response == "1"){
        location.reload();
      }else{
        $('#systemNotification').find('.modal-body').html(response);
        $('#systemNotification').modal('show');
      }
    });
  });

  $('body').on('click', '#viewDetail button[name="pmsstatus"], #actButton button[name="pmsstatus"]', function() {
    pmsstatus = $(this).val();
    bookingroom = $('form#form-view-guest').find('input[name="bookingroom"]').val();
    $.post('<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function/save-pmsstatus-guest.php',  { bookingroom : bookingroom, pmsstatus : pmsstatus }, function(response){
      if(response == "1"){
        location.reload();
      }else if(response == "2"){
        $('#systemNotification').find('.modal-body').html("Please complete the payment first!");
        $('#systemNotification').modal('show');
      }else if(response == "3"){
        $('#systemNotification').find('.modal-body').html("Room is not ready, please make sure the room status is vacant clean!");
        $('#systemNotification').modal('show');
      }else{
        $('#systemNotification').find('.modal-body').html(response);
        $('#systemNotification').modal('show');
      }
    });
  });

  $('#createReservation').on('click', 'button[name="next-action"]', function() {
    var pmsstatus = $(this).val();
    if($('form#form-create-reservation').valid()){
      $.post('<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function/save-reservation.php',  $('form#form-create-reservation').serialize() + '&pmsstatus='+pmsstatus, function(response){

      if(response == "1"){
         location.reload();
       }else{
         $('#systemNotification').find('.modal-body').html(response);
         $('#systemNotification').modal('show');
       }
      });
    }
  });

  $('#editBookingRoom').on('click', 'button[name="next-action"]', function() {
    pmsstatus = $(this).val();
    $.post('<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function-edit-rsv/save-edit-room.php',  $('form#form-edit-reservation').serialize() + '&pmsstatus='+pmsstatus, function(response){
      if(response == "1"){
        location.reload();
      }else{
        $('#systemNotification').find('.modal-body').html(response);
        $('#systemNotification').modal('show');
      }
    });
  });

  $('#sourceDetails').on('click', 'button[name="btn-submit"]', function() {
    pmsstatus = $(this).val();
    $.post('<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function-edit-rsv/save-edit-source-details.php',  $('form#form-edit-source-details').serialize(), function(response){
      if(response == "1"){
        location.reload();
      }else{
        $('#systemNotification').find('.modal-body').html(response);
        $('#systemNotification').modal('show');
      }
    });
  });

  $('#editBookingRoom').on('click', 'button[name="save"]', function() {
    pmsstatus = $('#editBookingRoom input[name="pmsstatus"]').val();
    $.post('<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function-edit-rsv/save-edit-room.php',  $('form#form-edit-reservation').serialize() + '&pmsstatus='+pmsstatus, function(response){
      if(response == "1"){
        location.reload();
      }else{
        $('#systemNotification').find('.modal-body').html(response);
        $('#systemNotification').modal('show');
      }
    });
  });


  $('body').on('change', '#paymentDetails select[name="type"]', function() {
    var type = $(this).val();
    var boxcc = $('#paymentDetails').find('div.cc');
    var boxbt = $('#paymentDetails').find('div.bank-transfer');

    if(type == "1"){
      $(boxcc).show();
      $(boxbt).hide();
      $('#paymentDetails').find('div.bank-transfer input, div.bank-transfer select').prop("disabled", true);
      $('#paymentDetails').find('div.cc input, div.cc select').prop("disabled", false);
    }else if(type == "2"){
      $(boxcc).hide();
      $(boxbt).show();
      $('#paymentDetails').find('div.bank-transfer input, div.bank-transfer select').prop("disabled", false);
      $('#paymentDetails').find('div.cc input, div.cc select').prop("disabled", true);
    }else{
      $(boxcc).hide();
      $(boxbt).hide();
      $('#paymentDetails').find('div.bank-transfer input, div.bank-transfer select').prop("disabled", true);
      $('#paymentDetails').find('div.cc input, div.cc select').prop("disabled", true);
    }
  });

  $('body').on('click', '#otherCharges button[name="btn-submit"]', function() {
    $.post('<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function-edit-rsv/other-charges-input.php',  $('form#form-add-othercharges').serialize(), function(response){
      if(response == "1"){
        location.reload();
      }else{
        $('#systemNotification').find('.modal-body').html(response);
        $('#systemNotification').modal('show');
      }
    });
  });

  $('body').on('click', '#paymentDetails button[name="btn-submit"]', function() {
    $.post('<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function-edit-rsv/save-payment-details.php',  $('form#form-add-payment-details').serialize(), function(response){
      if(response == "1"){
        location.reload();
      }else{
        $('#systemNotification').find('.modal-body').html(response);
        $('#systemNotification').modal('show');
      }
    });
  });

  $('body').on('click', '#specialRequest button[name="btn-submit"]', function() {
    note = $('form#form-special-request').find('textarea[name=note]').val();
    bookingoid = $('form#form-special-request').find('input[name=bookingoid]').val();
    $.post('<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function-edit-rsv/save-edit-special-request.php',  { bookingoid:bookingoid, note:note}, function(response){
      if(response == "1"){
        location.reload();
      }
    });
  });

  $('body').on('click', '#arrivalDetails button[name="btn-submit"]', function() {
    $.post('<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function-edit-rsv/save-edit-arrival.php',  $('form#form-arrival').serialize(), function(response){
      if(response == "1"){
        location.reload();
      }else{
        $('#systemNotification').find('.modal-body').html(response);
        $('#systemNotification').modal('show');
      }
    });
  });

  $('body').on('click', '#ccDetails button[name="btn-submit"]', function() {
    $.post('<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function-edit-rsv/save-credit-card.php',  $('form#form-edit-cc-details').serialize(), function(response){
      if(response == "1"){
        location.reload();
      }else{
        $('#systemNotification').find('.modal-body').html(response);
        $('#systemNotification').modal('show');
      }
    });
  });

  $('button[act="breakdown"]').click(function(){
    $(this).parent().parent().next('tr.breakdown-rate').slideToggle(0);
    return false;
  });

  $('#editBookingRoom').on('show.bs.modal', function (event) {
    var modal = $(this);
    var sourceblocked = $(event.relatedTarget);
    var brid = sourceblocked.data('brid');
    $.post('<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function-edit-rsv/edit-reservation.php', { brid : brid }, function(response){
      modal.find('.modal-body').html(response);
      showAvailableRoom();
      showPrice();
    });
  });

  $('#confirmationRemoveRoom').on('show.bs.modal', function (event) {
    var modal = $(this);
    var sourceblocked = $(event.relatedTarget);
    var brid = sourceblocked.data('brid');
    $.post('<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function-edit-rsv/confirmation-remove-room.php', { brid : brid }, function(response){
      modal.find('.modal-body').html(response);
    });
  });

  $('#confirmationRemoveRoom').on('click', 'button[name="remove-room"]', function() {
    brid = $(this).attr('data-id');
    $.post('<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function-edit-rsv/save-remove-room.php',  { brid : brid }, function(response){
      if(response == "1"){
        location.reload();
      }else{
        $('#systemNotification').find('.modal-body').html(response);
        $('#systemNotification').modal('show');
      }
    });
  });

  $('#confirmationRemoveCharges').on('show.bs.modal', function (event) {
    var modal = $(this);
    var sourceblocked = $(event.relatedTarget);
    var bcid = sourceblocked.data('bcid');
    $.post('<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function-edit-rsv/confirmation-remove-charge.php', { bcid : bcid }, function(response){
      modal.find('.modal-body').html(response);
    });
  });

  $('#confirmationRemoveCharges').on('click', 'button[name="remove-charge"]', function() {
    bcid = $(this).attr('data-id');
    $.post('<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function-edit-rsv/save-remove-charge.php',  { bcid : bcid }, function(response){
      if(response == "1"){
        location.reload();
      }else{
        $('#systemNotification').find('.modal-body').html(response);
        $('#systemNotification').modal('show');
      }
    });
  });

  $('#confirmationRemovePayment').on('show.bs.modal', function (event) {
    var modal = $(this);
    var sourceblocked = $(event.relatedTarget);
    var bpid = sourceblocked.data('bpid');
    $.post('<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function-edit-rsv/confirmation-remove-payment.php', { bpid : bpid }, function(response){
      modal.find('.modal-body').html(response);
    });
  });

  $('#confirmationRemovePayment').on('click', 'button[name="remove-charge"]', function() {
    bpid = $(this).attr('data-id');
    $.post('<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function-edit-rsv/save-remove-payment.php',  { bpid : bpid }, function(response){
      if(response == "1"){
        location.reload();
      }else{
        $('#systemNotification').find('.modal-body').html(response);
        $('#systemNotification').modal('show');
      }
    });
  });

  $('#guestDetails').on('click', 'button[name="btn-submit"]', function() {
      $.post('<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function-edit-rsv/save-edit-guest.php',  $('form#form-edit-guest').serialize(), function(response){
      if(response == "1"){
        location.reload();
      }else{
        $('#systemNotification').find('.modal-body').html(response);
        $('#systemNotification').modal('show');
      }
    });
  });

  $('#createReservation').on('change', 'select[name="bookingmarket"]', function() {

    bookingmarket = $(this).val();

    elem = $('#createReservation').find('.modal-body').find('div.booking-market');
    if(bookingmarket == "1"){
      elem.css('display', 'none');
    }
  });

  $('body').on('click', 'button[name="view-invoice"]', function() {
     $('form#invoice-guest').submit();
  });
  $('body').on('click', 'button[name="print-invoice"]', function() {
     $('form#print-invoice-guest').submit();
  });
  $('body').on('click', 'button[name="reservation-edit"]', function() {
     $('form#reservation-edit').submit();
  });

  $('#createReservation').on('focus', 'input[name="firstname"]', function() {
    var firstname = $(this).val();
    var hotel = $('#createReservation').find('input[name=hotel]').val();
    $("#createReservation input[name='firstname']").autocomplete({
  		source: function( request, response ) {
  			$.ajax({
  				url: "<?php echo "$base_url"; ?>/pms-lite/request/get-guest.php",
          dataType: 'json',
  				data: { firstname : request.term, hotel:hotel },
  				success: function(data){
  					response(data);
  				},
  			});
      },
  		minLength: 2,
  		select: function( event, ui ) {
        event.preventDefault();
        $("input[name='firstname']").val(ui.item.firstname);
  			$("input[name='lastname']").val(ui.item.lastname);
        $("input[name='email']").val(ui.item.email);
        $("input[name='phone']").val(ui.item.phone);
        $("input[name='city']").val(ui.item.city);
        $("select[name='title']").val(ui.item.title);
        $("select[name='country']").val(ui.item.country);
        $("input[name='cust']").val(ui.item.id);
  		},
      change: function(event, ui) {
        if (ui.item == null) {
          $("input[name='cust']").val(0);
        }
       }
    });
  });

  $(document).ready(function() {
    DesignRsvChart()
  });


  function DesignRsvChart(){
    w_table = $('table#table-reservation-chart').width();
    w_roomnumber = $('table#table-reservation-chart tr.roomnumber').children('td').eq(0).width();
    c_td = parseFloat($('table#table-reservation-chart tr.hotelname td').attr('colspan')) - 1;
    w_td = (w_table - 150) / c_td;
    $('table#table-reservation-chart td:nth-child(n+2)').css('max-width', w_td+'px');
  }

  $('#specialRequest').on('show.bs.modal', function (event){
      $('#specialRequest textarea[name=note]').trumbowyg({
				btns: ['viewHTML',
				  '|', 'btnGrp-design',
				  '|', 'link',
				  '|', 'btnGrp-lists'],
				fullscreenable: false
			});
  });

  $("#editBookingRoom").on('click', '#changeDailyRate', function(){
      var x = confirm("Are you sure to change these rates?");
      if(x === true){
        $.post('<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function-edit-rsv/change-daily-rate.php', $('form#form-edit-reservation').serialize(), function(response){
          if(response == '1'){
              window.location.reload(true);
          }else{
            $('#systemNotification').find('.modal-body').html(response);
            $('#systemNotification').modal('show');
          }
        });
      }
  });

  $('#createReservation').on('keyup',"input[name='roomtotal[]']", function(){
    var roomtotal = parseFloat($(this).val());
    var night = parseFloat($('input[name=night]').val());
    var r = $(this).attr('for-room');
    var dailyrate = roomtotal / night;
    if(isNaN(dailyrate)) {
      var dailyrate = 0;
    }
    $("input[rate-room='"+r+"']").each(function(index){
      $(this).val(dailyrate);
    });
    specialRsvTotal();
  });

  $('#createReservation').on('keyup',"input[fnc='dailyrate']", function(){
    var r = $(this).attr('rate-room');
    var roomtotal = 0;
    $("input[rate-room='"+r+"']").each(function(index){
      var dailyrate = parseFloat($(this).val());
      if(isNaN(dailyrate)) {
        var dailyrate = 0;
      }
      roomtotal = roomtotal + dailyrate;
    });
    $("input[name='roomtotal[]'][for-room='"+r+"']").val(roomtotal);
    specialRsvTotal();
  });

  function specialRsvTotal(){
    var grandtotal = 0;
    $("#createReservation input[fnc=dailyrate").each(function(index){
      var roomtotal = parseFloat($(this).val());
      if(isNaN(roomtotal)) {
        var roomtotal = 0;
      }
      grandtotal = grandtotal + roomtotal;
    });
    $("#createReservation h2[fnc=grandtotal").html(grandtotal.toString());
  }

  $('#editBookingRoom').on('keyup',"input[name='dailyrate[]']", function(){
     editSpecialRsvTotal();
  });

  function editSpecialRsvTotal(){
    var grandtotal = 0;
    $("#editBookingRoom input[name='dailyrate[]'").each(function(index){
      var roomtotal = parseFloat($(this).val());
      if(isNaN(roomtotal)) {
        var roomtotal = 0;
      }
      grandtotal = grandtotal + roomtotal;
    });
    formatgrandtotal = (grandtotal).formatMoney(2, '.', ',');
    $("#editBookingRoom h2 span[fnc=grandtotal").html(formatgrandtotal);
  }

  $('#zarea').change(function(){
      var z = $(this).val();
      $('#zhotel').children("option").show();
      if(z != ""){
          $('#zhotel').children("option").each(function(){
              var y = $(this).attr('x');
              if(z != y && y != ""){
                  $(this).hide();
              }
          });
      }
  });

  $('#cancelModal, #noShowModal').on('show.bs.modal', function(e) {
	 title = $(e.relatedTarget).data('title');
	 submittext = $(e.relatedTarget).data('submit');
	 status = $(e.relatedTarget).data('status');

	 $(this).find('h4.modal-title').html(title);
	 $(this).find('button.submit').text(submittext);
	 $(this).find('input[name=changestatus]').val(status);
  });

  $('#cancelModal, #noShowModal').on('click','button.submit', function(e) {
	form = $(this).closest('form');
	if(form.find('textarea[name=cancellationreason]').val().length <= 0 || form.find('input[name=cancellationamount]').val().length <= 0){
		modalbox = $('#AlertModal');
		modalbox.find('.modal-body').html("Please make sure you fill Cancellation Amount and Cancellation Reason");
		modalbox.modal('show');
	}else{
		modalbox = $('#NotificationModal');
		$.ajax({
			url		: '<?php echo $base_url; ?>/request/cancellation.php',
			type	: 'post',
			data	: form.serialize(),
			success	: function(response){
				$('#cancelModal, #noShowModal').modal('hide');
				if(response == "success"){
					response = "Booking status has been changed. We will refresh the page.";
				}else{
					response = "Data failed to changed";
				}
				modalbox.find('.modal-body').html(response);
				modalbox.modal('show');
			}
		});
	}
  });

  $('#NotificationModal').on('hide.bs.modal', function (e) {
	 location.reload(true);
  });

  $("input[name='hotelname']").autocomplete({
    source: function( request, response ) {
      $.ajax({
        url: "<?php echo "$base_url"; ?>/pms-lite/request/get-hotel.php",
        dataType: 'json',
        data: { hotelname : request.term },
        success: function(data){
          response(data);
        },

      });
    },
    minLength: 2,
    select: function( event, ui ) {
      event.preventDefault();
      $("input[name='hotelname']").val(ui.item.value);
      $("input[name='zhotel']").val(ui.item.id);
    },
    change: function(event, ui) {
      if (ui.item == null) {
        $("input[name='zhotel']").val('');
      }
     }
  });

  $('#createOOO').on('show.bs.modal', function (event){
    var modal = $(this);
    var sourceblocked = $(event.relatedTarget);
    var date = sourceblocked.data('date');
    var room = sourceblocked.data('room');
    var roomnumber = sourceblocked.data('roomnumber');
    var hotel = sourceblocked.data('hotel');
    $.post('<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function/ooo-create.php', { date : date, room : room, roomnumber : roomnumber, hotel:hotel }, function(response){
      modal.find('.modal-body').html(response);
    });
  });

  $('body').on('click', '#createOOO button[name="save"]', function() {
    $.post('<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function/ooo-save.php',  $('form#form-create-ooo').serialize(), function(response){
      if(response == "1"){
        location.reload();
      }else{
        $('#systemNotification').find('.modal-body').html(response);
        $('#systemNotification').modal('show');
      }
    });
  });

  $('#viewOOO').on('show.bs.modal', function (event){
    var modal = $(this);
    var sourceblocked = $(event.relatedTarget);
    var oooid = sourceblocked.data('oooid');
    $.post('<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function/ooo-view.php', { oooid : oooid }, function(response){
      modal.find('.modal-body').html(response);
    });
  });

  $('#viewOOO').on('click', 'button[name="clear-date"]', function() {
    $.post('<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function/ooo-clear.php',  $('form#form-view-ooo').serialize(), function(response){
      if(response == "1"){
        location.reload();
      }else{
        $('#systemNotification').find('.modal-body').html(response);
        $('#systemNotification').modal('show');
      }
    });
  });

  $('#viewOOO').on('click', 'button[name="edit"]', function() {
    $.post('<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function/ooo-edit.php',  $('form#form-view-ooo').serialize(), function(response){
      $('#viewOOO').modal('toggle');
      $('#editOOO').find('.modal-body').html(response);
      $('#editOOO').modal('show');
    });
  });

  $('#editOOO').on('click', 'button[name="save"]', function() {
    $.post('<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function/ooo-edit-save.php',  $('form#form-edit-ooo').serialize(), function(response){
      if(response == "1"){
        location.reload();
      }else{
        $('#systemNotification').find('.modal-body').html(response);
        $('#systemNotification').modal('show');
      }
    });
  });

  $('#changeCurrency').on('click', 'button[name="save"]', function() {
    $.post('<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function-edit-rsv/save-edit-currency.php',  $('form#form-edit-currency').serialize(), function(response){
      if(response == "1"){
        location.reload();
      }else{
        $('#systemNotification').find('.modal-body').html(response);
        $('#systemNotification').modal('show');
      }
    });
  });

});
</script>

<div class="modal fade pms-lite-modal" id="systemNotification" tabindex="-1" role="dialog" aria-labelledby="systemNotification" style="z-index:1051">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">System Notification</h4>
      </div>
      <div class="modal-body"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
