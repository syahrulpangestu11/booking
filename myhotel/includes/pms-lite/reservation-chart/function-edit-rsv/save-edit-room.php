<?php
try{
  session_start();
	error_reporting(E_ALL ^ E_NOTICE);
	include ('../../../../conf/connection.php');
  include ('../../class-pms-lite.php');

  $hoteloid = $_SESSION['_hotel'];
  $pmslite = new PMSReservation($db);
  $pmslite->setPMSHotel($hoteloid);

  $checkin = date('Y-m-d', strtotime($_POST['checkin']));
  $checkout = date('Y-m-d', strtotime($_POST['checkout']));
  $roomoid = (isset($_POST['roomtypeoid']) && !empty($_POST['roomtypeoid'])) ? $_POST['roomtypeoid'] : $_POST['room'];
  $roomofferoid = $_POST['roomofferoid'];
  $roombooked = 1;
  $bookingroomoid = $_POST['bookingroomoid'];
  $bookingroom = $pmslite->dataBookingRoom($bookingroomoid);
  $bookingmarket = $bookingroom['bookingmarketoid'];
  $currencyoid = $bookingroom['currencyoid'];

  $c_empty = 0;
  foreach($_POST['roomnumber'] as $qkey => $qvalue){
    if(empty($qvalue)){
        $c_empty++;
    }
  }
  if(empty($_POST['adult'])){
    echo "Adult cannot be empty.";
    die();
  }  
  if(empty($_POST['roomnumber']) || $c_empty > 0){
    echo "Please assign room number on each date. If not available, try to change date or the room type.";
    die();
  }

  if($bookingmarket == 'NA'){

    $response = $pmslite->saveEditPrice($checkin, $checkout, $roomoid, $roomofferoid, '1', $roombooked, $bookingroomoid, $bookingroom['bookingmarketoid'], 0);

    if($response == true){

      $stmt = $db->prepare("UPDATE `bookingroom` SET `guestroom` = :a, `adult` = :b, `child` = :c, `pmsstatusoid` = :d, `checkin` = :e, `checkout` = :f, `checkoutr` = :g, `night` = :h, `roomofferoid` = :i WHERE bookingroomoid = :id");
      $stmt->execute(array(':id' => $bookingroomoid, ':a' => $_POST['guest'], ':b' => $_POST['adult'], ':c' => $_POST['child'], ':d' => $_POST['pmsstatus'], ':e' => $checkin, ':f' => $checkout, ':g' => $checkout, ':h' => $night, ':i' => $roomofferoid));

      $stmt = $db->prepare("UPDATE `bookingroomdtl` SET `guestroom` = :a  WHERE bookingroomoid = :id");
      $stmt->execute(array(':id' => $bookingroomoid, ':a' => $_POST['guest']));
      
        // Prepare Data
        $stmt = $db->prepare("SELECT `roomoid` FROM `roomoffer` INNER JOIN `bookingroom` USING(`roomofferoid`) WHERE `bookingroomoid` = :a");
        $stmt->execute(array(':a' => $bookingroomoid));
        $r_ro = $stmt->fetch(PDO::FETCH_ASSOC);
        $vroomoid = $r_ro['roomoid'];
        
        $stmt = $db->prepare("SELECT COUNT(`roomnumberoid`) AS jml FROM `roomnumber` WHERE `roomoid` = :a AND `publishedoid` = 1");
        $stmt->execute(array(':a' => $vroomoid));
        $r_ro = $stmt->fetch(PDO::FETCH_ASSOC);
        $vroomnumber = $r_ro['jml'];
        
        $data6 = '{
            "token": "pfUxpxmKTJNTStNhPK2Nly4UKxbcVWwNRUT1SXnPfRiWqhvmINnJ4XnefNNBuTsx",
            "hcode": "'.$pmslite->hotelcode.'",
            "roomid": "'.$vroomoid.'",
            "data": [';
        $z = 0;

      $stmt = $db->prepare("SELECT bookingroomdtloid FROM `bookingroomdtl` WHERE bookingroomoid = :id and reconciled = '0' and pmsreconciled = '0'");
      $stmt->execute(array(':id' => $bookingroomoid));
      $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
      foreach($result as $key => $brd){
        $stmt = $db->prepare("UPDATE `bookingroomdtl` SET `roomnumberoid` = :a  WHERE bookingroomdtloid = :id");
        $stmt->execute(array(':id' => $brd['bookingroomdtloid'], ':a' => $_POST['roomnumber'][$key]));
        
        // Prepare Data
        $stmt = $db->prepare("SELECT `date` FROM `bookingroomdtl` WHERE `bookingroomdtloid` = :a");
        $stmt->execute(array(':a' => $brd['bookingroomdtloid']));
        $r_rd = $stmt->fetch(PDO::FETCH_ASSOC);
        $vdate = $r_rd['date'];
        
        $stmt = $db->prepare("SELECT COUNT(`bookingroomdtloid`) jml
                FROM `bookingroomdtl` brd
                    LEFT JOIN `bookingroom` br USING(`bookingroomoid`)
                    LEFT JOIN `booking` b USING(`bookingoid`)
                    LEFT JOIN `roomoffer` ro USING(`roomofferoid`)
                WHERE b.`bookingstatusoid` IN (4) AND br.`pmsstatusoid` IN (2,4,7) 
                	AND ro.`roomoid` = :a AND brd.`date` = :b");
        $stmt->execute(array(':a' => $vroomoid, ':b' => $vdate));
        $r_ro = $stmt->fetch(PDO::FETCH_ASSOC);
        $vtxnumber = $r_ro['jml'];
        
        $vval = $vroomnumber - $vtxnumber;
        
        if($z != 0){ $data6 .=","; }else{ $z = 1; }
        $data6 .= '
            {
                "from": "'.$vdate.'",
                "to": "'.$vdate.'",
                "value": "'.$vval.'"
            }
        ';
        
      }
      
        // Prepare Data
        $data6 .= '
            ]
        }';
        
        // The data to send to the API
        $postData = (array) json_decode($data6);
        
        // Setup cURL
        $ch = curl_init('https://thebuking.com/apiv1/save/allotment_inrange/');
        curl_setopt_array($ch, array(
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_SSL_VERIFYPEER => FALSE,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
            CURLOPT_POSTFIELDS => json_encode($postData)
        ));
        
        // Send the request
        $response = curl_exec($ch);
        
        // Check for errors
        if($response === FALSE){
            die(curl_error($ch));
        }
        
        // Decode the response
        $responseData = json_decode($response, TRUE);

      $pmslite->RecalculateBooking($bookingroom['bookingoid']);
    }

  }else{
    $roomtotal = array_sum($_POST['dailyrate']);
    $stmt = $db->prepare("UPDATE `bookingroom` SET `guestroom` = :a, `adult` = :b, `child` = :c, `pmsstatusoid` = :d, `roomtotal` = :e, `roomtotalr` = :f, `total` = :g, `totalr` = :h, `checkin` = :i, `checkout` = :j, `checkoutr` = :k, `night` = :l, `roomofferoid` = :m WHERE bookingroomoid = :id");
    $stmt->execute(array(':id' => $bookingroomoid, ':a' => $_POST['guest'], ':b' => $_POST['adult'], ':c' => $_POST['child'], ':d' => $_POST['pmsstatus'], ':e' => $roomtotal, ':f' => $roomtotal, ':g' => $roomtotal, ':h' => $roomtotal, ':i' => $checkin, ':j' => $checkout, ':k' => $checkout, ':l' => $night, ':m' => $roomofferoid));

    $stmt = $db->prepare("SELECT * FROM `bookingroomdtl` WHERE bookingroomoid = :id");
    $stmt->execute(array(':id' => $bookingroomoid));
    $r_brd = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
    $stmt = $db->prepare("DELETE FROM `bookingroomdtl` WHERE bookingroomoid = :id");
    $stmt->execute(array(':id' => $bookingroomoid));
    
    //RESET-------------------------------------------------------------
    
        // Prepare Data
        $stmt = $db->prepare("SELECT `roomoid` FROM `roomoffer` INNER JOIN `bookingroom` USING(`roomofferoid`) WHERE `bookingroomoid` = :a");
        $stmt->execute(array(':a' => $bookingroomoid));
        $r_ro = $stmt->fetch(PDO::FETCH_ASSOC);
        $vroomoid = $r_ro['roomoid'];
        
        $stmt = $db->prepare("SELECT COUNT(`roomnumberoid`) AS jml FROM `roomnumber` WHERE `roomoid` = :a AND `publishedoid` = 1");
        $stmt->execute(array(':a' => $vroomoid));
        $r_ro = $stmt->fetch(PDO::FETCH_ASSOC);
        $vroomnumber = $r_ro['jml'];
        
        $data6 = '{
            "token": "pfUxpxmKTJNTStNhPK2Nly4UKxbcVWwNRUT1SXnPfRiWqhvmINnJ4XnefNNBuTsx",
            "hcode": "'.$pmslite->hotelcode.'",
            "roomid": "'.$vroomoid.'",
            "data": [';
        $z = 0;
    
    foreach($r_brd as $a_brd){
        
        // Prepare Data
        $vdate = $a_brd['date'];
        
        $stmt = $db->prepare("SELECT COUNT(`bookingroomdtloid`) jml
                FROM `bookingroomdtl` brd
                    LEFT JOIN `bookingroom` br USING(`bookingroomoid`)
                    LEFT JOIN `booking` b USING(`bookingoid`)
                    LEFT JOIN `roomoffer` ro USING(`roomofferoid`)
                WHERE b.`bookingstatusoid` IN (4) AND br.`pmsstatusoid` IN (2,4,7) 
                	AND ro.`roomoid` = :a AND brd.`date` = :b");
        $stmt->execute(array(':a' => $vroomoid, ':b' => $vdate));
        $r_ro = $stmt->fetch(PDO::FETCH_ASSOC);
        $vtxnumber = $r_ro['jml'];
        
        $vval = $vroomnumber - $vtxnumber;
        
        if($z != 0){ $data6 .=","; }else{ $z = 1; }
        $data6 .= '
            {
                "from": "'.$vdate.'",
                "to": "'.$vdate.'",
                "value": "'.$vval.'"
            }
        ';
        
    }
    
        // Prepare Data
        $data6 .= '
            ]
        }';
        
        // The data to send to the API
        $postData = (array) json_decode($data6);
        
        // Setup cURL
        $ch = curl_init('https://thebuking.com/apiv1/save/allotment_inrange/');
        curl_setopt_array($ch, array(
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_SSL_VERIFYPEER => FALSE,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
            CURLOPT_POSTFIELDS => json_encode($postData)
        ));
        
        // Send the request
        $response = curl_exec($ch);
        
        // Check for errors
        if($response === FALSE){
            die(curl_error($ch));
        }
        
        // Decode the response
        $responseData = json_decode($response, TRUE);
    
    //RESET-------------------------------------------------------------
    
        // Prepare Data
        $data6 = '{
            "token": "pfUxpxmKTJNTStNhPK2Nly4UKxbcVWwNRUT1SXnPfRiWqhvmINnJ4XnefNNBuTsx",
            "hcode": "'.$pmslite->hotelcode.'",
            "roomid": "'.$vroomoid.'",
            "data": [';
        $z = 0;

    foreach($_POST['dailyrate'] as $key2 => $dailyrate){
      $date = $_POST['date'][$key2];
      $roomnumber = $_POST['roomnumber'][$key2];
      $stmt = $db->prepare("insert into `bookingroomdtl` (`bookingroomoid`, `date`, `rate`, `discount`, `rateafter`, `extrabed`, `total`, `currencyoid`, `guestroom`, `roomnumberoid`) VALUES (:a , :b , :c , :d , :e , :f, :g, :h, :i, :j)");
      $stmt->execute(array(':a' => $bookingroomoid, ':b' => $date, ':c' => $dailyrate, ':d' => 0, ':e' => $dailyrate, ':f' => 0, ':g' => $dailyrate, ':h' => $currencyoid, ':i' =>  $_POST['guest'], ':j' => $roomnumber));

        // Prepare Data
        $vdate = $date;
        
        $stmt = $db->prepare("SELECT COUNT(`bookingroomdtloid`) jml
                FROM `bookingroomdtl` brd
                    LEFT JOIN `bookingroom` br USING(`bookingroomoid`)
                    LEFT JOIN `booking` b USING(`bookingoid`)
                    LEFT JOIN `roomoffer` ro USING(`roomofferoid`)
                WHERE b.`bookingstatusoid` IN (4) AND br.`pmsstatusoid` IN (2,4,7) 
                	AND ro.`roomoid` = :a AND brd.`date` = :b");
        $stmt->execute(array(':a' => $vroomoid, ':b' => $vdate));
        $r_ro = $stmt->fetch(PDO::FETCH_ASSOC);
        $vtxnumber = $r_ro['jml'];
        
        $vval = $vroomnumber - $vtxnumber;
        
        if($z != 0){ $data6 .=","; }else{ $z = 1; }
        $data6 .= '
            {
                "from": "'.$vdate.'",
                "to": "'.$vdate.'",
                "value": "'.$vval.'"
            }
        ';
        
    }
    
        // Prepare Data
        $data6 .= '
            ]
        }';
        
        // The data to send to the API
        $postData = (array) json_decode($data6);
        
        // Setup cURL
        $ch = curl_init('https://thebuking.com/apiv1/save/allotment_inrange/');
        curl_setopt_array($ch, array(
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_SSL_VERIFYPEER => FALSE,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
            CURLOPT_POSTFIELDS => json_encode($postData)
        ));
        
        // Send the request
        $response = curl_exec($ch);
        
        // Check for errors
        if($response === FALSE){
            die(curl_error($ch));
        }
        
        // Decode the response
        $responseData = json_decode($response, TRUE);

    $pmslite->RecalculateBooking($bookingroom['bookingoid']);
  }

  //print_r($response);
  echo "1";

}catch(Exception $e){
  echo $e->getMessage();
}
?>
