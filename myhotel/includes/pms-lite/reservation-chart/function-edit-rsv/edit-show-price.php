<?php
try{
  session_start();
	error_reporting(E_ALL ^ E_NOTICE);
	include ('../../../../conf/connection.php');
  include ('../../class-pms-lite.php');

  if(empty($hoteloid) and $_SESSION['_typeusr']=='4'){
    $stmt = $db->prepare("select hoteloid from booking INNER JOIN bookingroom USING(bookingoid) where bookingroomoid = :a");
    $stmt->execute(array(':a' => $_GET['bookingroomoid']));
    $result = $stmt->fetch(PDO::FETCH_ASSOC);
    $hoteloid = $result['hoteloid'];
  }

  $bookingroomoid = $_GET['bookingroomoid'];
  $checkin = date('Y-m-d', strtotime($_GET['checkin']));
  $checkout = date('Y-m-d', strtotime($_GET['checkout']));
  $roomoid = isset($_GET['roomtypeoid']) ? $_GET['roomtypeoid'] : $_GET['room'];
  $roomofferoid = $_GET['roomofferoid'];
  $roombooked = 1;

  $guest = $_GET['guest'];
  $adult = $_GET['adult'];
  $child = $_GET['child'];

  $pmslite = new PMSReservation($db);
  $pmslite->setRoom($roomoid);
  $room = $pmslite->roomType();
  /*--------------------------------------*/
  $bookingroom = $pmslite->dataBookingRoom($bookingroomoid);
  $bookingmarket = $bookingroom['bookingmarketoid'];
  $agenthoteloid = $bookingroom['agentoid'];

  $night = $pmslite->dateDiff($checkin, $checkout);

  if($checkout <= $checkin){
    $checkout = date('Y-m-d', strtotime($_GET['checkin'] . " +1 days"));
    $night = $pmslite->dateDiff($checkin, $checkout);
  }

  $show_list_date = array();
  $list_date = array();
  $list_roomnumber = array();
  for($n=0; $n<$night; $n++){
    $date = date('Y-m-d', strtotime($checkin. ' +'.$n.' days'));
    array_push($list_date, $date);
    array_push($show_list_date, date('d M Y', strtotime($date)));

    array_push($list_roomnumber, $pmslite->loadShowAssignRoomEdit($date, $roomoid, $bookingroomoid));
  }

  if($bookingmarket != 1){
?>
  <hr style="border-top: 2px dashed #b7d3d1;"/>
  <?php /*<div style="display:none"><?php print_r($list_roomnumber);?></div>*/?>
  <h3><i class="fa fa-key"></i> Setting Room</h3>
  <div class="row">
    <div class="col-md-12">
      <table class="table">
        <thead>
          <tr><th>Guest Room*</th><th>Adult</th><th>Child</th><th>Assign Room</th>
        </thead>
        <tbody>
          <tr>
            <td>
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-addon"><i class="fa fa-user"></i></div>
                  <input type="text" name="guest" class="form-control" value="<?=$guest?>">
                </div>
              </div>
            </td>
            <td>
              <div class="form-group">
                <select class="form-control" name="adult">
                  <?php for($i = 1; $i <= $room['adult']; $i++){ if($i == $adult){ $selected = "selected"; }else{ $selected = ""; } ?>
                    <option value="<?=$i?>" <?=$selected?>><?=$i?></option>
                  <?php } ?>
                </select>
              </div>
            </td>
            <td>
                <div class="form-group">
                  <select class="form-control" name="child">
                    <?php for($i = 0; $i <= $room['child']; $i++){ if($i == $child){ $selected = "selected"; }else{ $selected = ""; } ?>
                      <option value="<?=$i?>" <?=$selected?>><?=$i?></option>
                    <?php } ?>
                  </select>
                </div>
            </td>
            <td>
              <?php foreach($show_list_date as $key => $date){
                    $olddata= $pmslite->getDailyRateBookingbyDate($list_date[$key], $bookingroomoid);
              ?>
              <div class="row">
                <div class="col-xs-4"><?=$date?></div>
                <div class="col-xs-4">
                  <select name="roomnumber[]" class="form-control">
                    <?php /*<option value="<?=$olddata['roomnumberoid']?>" selected><?=$olddata['roomnumber']?></option>*/?>
                    <?php foreach($list_roomnumber[$key] as $key2 => $rn){
                          if($rn['roomnumberoid'] == $olddata['roomnumberoid']){ $selected = "selected"; }else{ $selected = ""; }
                    ?>
                    <option value="<?=$rn['roomnumberoid']?>" <?=$selected?>><?=$rn['roomnumber']?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="col-xs-4" align="right">
                    <input type="hidden" name="date[]" value="<?=$list_date[$key]?>">
                    <input type="text" class="form-control" name="dailyrate[]" value="<?=$olddata['total']?>" style="width:120px;background-color: #feffd3;">
                </div>
              </div>
              <?php } ?>
            </td>
          </tr>
        </tbody>
      </table>
      <small>* If guest room empty system will default get name from guest detail.</small>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12 text-right">
      <h3 style="font-size: 1.2em;">Total :<br></h3>
      <h2 style="font-size: 1.5em;margin-top: 5px;"><?=$bookingroom['currency']?> <span fnc="grandtotal"><?=$response?></span></h2>
    </div>
  </div>
<?php

  }else{

    if(empty($hoteloid)){
      $stmt = $db->prepare("select hoteloid from booking INNER JOIN bookingroom USING(bookingoid) where bookingroomoid = :a");
      $stmt->execute(array(':a' => $_GET['bookingroomoid']));
      $result = $stmt->fetch(PDO::FETCH_ASSOC);
      $hoteloid = $result['hoteloid'];
    }

    $pmslite->setPMSHotel($hoteloid);
    $pmslite->loadXML();

    $_SESSION['tokenSession'.$pmslite->hotelcode] = $pmslite->hotelcode;
    $response = $pmslite->loadShowEditPrice($checkin, $checkout, $roomoid, $roomofferoid, '1', $roombooked, $_SESSION['tokenSession'.$pmslite->hotelcode], $bookingroom['bookingmarketoid'], $agenthoteloid);

    if(strpos($response, '<failed>') !== true){


  ?>

    <hr style="border-top: 2px dashed #b7d3d1;"/>
    <h3><i class="fa fa-key"></i> Setting Room</h3>
    <div class="row">
      <div class="col-md-12">
        <table class="table">
          <thead>
            <tr><th>Guest Room*</th><th>Adult</th><th>Child</th><th>Assign Room</th>
          </thead>
          <tbody>
            <tr>
              <td>
                <div class="form-group">
                  <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-user"></i></div>
                    <input type="text" name="guest" class="form-control" value="<?=$guest?>">
                  </div>
                </div>
              </td>
              <td>
                <div class="form-group">
                  <select class="form-control" name="adult">
                    <?php for($i = 1; $i <= $room['adult']; $i++){ if($i == $adult){ $selected = "selected"; }else{ $selected = ""; } ?>
                      <option value="<?=$i?>" <?=$selected?>><?=$i?></option>
                    <?php } ?>
                  </select>
                </div>
              </td>
              <td>
                  <div class="form-group">
                    <select class="form-control" name="child">
                      <?php for($i = 0; $i <= $room['child']; $i++){ if($i == $child){ $selected = "selected"; }else{ $selected = ""; } ?>
                        <option value="<?=$i?>" <?=$selected?>><?=$i?></option>
                      <?php } ?>
                    </select>
                  </div>
              </td>
              <td>
                <?php foreach($show_list_date as $key => $date){
                      $olddata= $pmslite->getDailyRateBookingbyDate($list_date[$key], $bookingroomoid);
                ?>
                <div class="row">
                  <div class="col-xs-4"><?=$date?></div>
                  <div class="col-xs-4">
                    <select name="roomnumber[]" class="form-control">
                      <?php /*<option value="<?=$olddata['roomnumberoid']?>" selected><?=$olddata['roomnumber']?></option>*/?>
                      <?php foreach($list_roomnumber[$key] as $key2 => $rn){
                            if($rn['roomnumberoid'] == $olddata['roomnumberoid']){ $selected = "selected"; }else{ $selected = ""; }
                      ?>
                      <option value="<?=$rn['roomnumberoid']?>" <?=$selected?>><?=$rn['roomnumber']?></option>
                      <?php } ?>
                    </select>
                  </div>
                  <div class="col-xs-4" align="right">
                      <input type="hidden" name="date[]" value="<?=$list_date[$key]?>">
                      <input type="text" class="form-control" name="dailyrate[]" value="<?=$olddata['total']?>" style="width:120px;background-color: #feffd3;">
                  </div>
                </div>
                <?php } ?>
              </td>
            </tr>
          </tbody>
        </table>
        <small>* If guest room empty system will default get name from guest detail.</small>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 text-right">
        <h3 style="font-size: 1.2em;">Total :<br></h3>
        <h2 style="font-size: 1.5em;margin-top: 5px;"><?=$bookingroom['currency']?> <span fnc="grandtotal"><?=$response?></span></h2>
      </div>
    </div>
  <?php
    }else{
  ?>
  <div class="row">
    <div class="col-md-12 text-right">
      <h2 style="font-size: 1.5em;"><?=$response?></h2>
    </div>
  </div>
  |
  <button type="button" class="btn btn-sm btn-default" data-dismiss="modal" aria-label="Close">Close</button>
<?php
    }
  }
}catch(Exception $e){
  echo $e->getMessage();
}
?>
