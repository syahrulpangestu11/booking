<?php
try{
  session_start();
	error_reporting(E_ALL ^ E_NOTICE);
	include ('../../../../conf/connection.php');
  include ('../../class-pms-lite.php');

  $hoteloid = $_SESSION['_hotel'];
  $pmslite = new PMSLite($db);
  $pmslite->setPMSHotel($hoteloid);

  $bookingchargesoid = $_POST['bcid'];
  $bookingcharge = $pmslite->getBookingCharge($bookingchargesoid);

  $pmslite->removeCharge($bookingchargesoid);

  $pmslite->RecalculateBooking($bookingcharge['bookingoid']);

  echo "1";

}catch(Exception $e){
  echo $e->getMessage();
}
?>
