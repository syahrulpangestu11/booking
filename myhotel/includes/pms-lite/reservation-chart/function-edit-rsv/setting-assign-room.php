<?php
try{
  session_start();
  error_reporting(E_ALL ^ E_NOTICE);
  //include ('../../../../conf/connection.php');
  //include ('../../class-pms-lite.php');
  include ('includes/pms-lite/class-pms-lite.php');

  $pmslite = new PMSLite($db);
  $pmslite->setPMSHotel($hoteloid);
  $pmslite->setRoom($roomoid);
  $room = $pmslite->roomType();
  $rateplan = $pmslite->RatePlan();
?>
<table class="table table-bordered" id="table-reservation-chart">
  <thead>
      <tr><th>#</th><th>Room Type</th><th>Rate Plan</th><th>Assign Room</th><th>Date</th><th>Occupancy</th><th>Breakfast Included</th><th>No. of Extra Bed</th><th>Total</th></tr></thead>
  <tbody>
      <tr style="display:none"><td></td></tr>
<?php
  $q_detail = "select br.*, count(bookingroomoid) as jmlroom, sum(br.extrabed) as extrabed, c.currencycode, checkin, checkout, checkoutr, rn.roomnumber
  from bookingroom br
  inner join currency c using (currencyoid)
  inner join booking b using (bookingoid)
  left join roomnumber rn using (roomnumberoid)
  where b.bookingoid = '".$bookingoid."'
  group by br.bookingroomoid";
  $stmt = $db->query($q_detail);
  $r_detail = $stmt->fetchAll(PDO::FETCH_ASSOC);
  foreach($r_detail as $roomdtl){
?>
<tr class='list-detail'>
  <td>#</td>
  <td>
    <select name="roomoid[]" class="form-control">
      <?php
        $stmt = $db->query("select r.roomoid, r.name as roomname from room r where r.publishedoid = '1' and r.hoteloid = '".$hoteloid."'");
    		$rateplan = $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach($rateplan as $rp){
          if($rp['roomofferoid'] == $roomdtl['roomofferoid']){ $selected = "selected"; }else{ $selected = "";  }
      ?>
      <option value="<?=$rp['roomoid']?>" <?=$selected?>><?=$rp['roomname']?></option>
      <?php
        }
      ?>
    </select>
  </td>
  <td>
    <select name="roomofferoid" class="form-control">
      <?php
        $stmt = $db->query("select ro.roomofferoid, ro.name as rateplan from roomoffer ro inner join room r using (roomoid) where r.publishedoid = '1' and ro.publishedoid = '1' and r.hoteloid = '".$hoteloid."'");
    		$rateplan = $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach($rateplan as $rp){
          if($rp['roomofferoid'] == $roomdtl['roomofferoid']){ $selected = "selected"; }else{ $selected = "";  }
      ?>
      <option value="<?=$rp['roomofferoid']?>" <?=$selected?>><?=$rp['rateplan']?></option>
      <?php
        }
      ?>
    </select>
  </td>
  <td>
    <select name="roomofferoid" class="form-control">
      <?php
        $stmt = $db->query("select ro.roomofferoid, ro.name as rateplan from roomoffer ro inner join room r using (roomoid) where r.publishedoid = '1' and ro.publishedoid = '1' and r.hoteloid = '".$hoteloid."'");
        $rateplan = $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach($rateplan as $rp){
          if($rp['roomofferoid'] == $roomdtl['roomofferoid']){ $selected = "selected"; }else{ $selected = "";  }
      ?>
      <option value="<?=$rp['roomofferoid']?>" <?=$selected?>><?=$rp['rateplan']?></option>
      <?php
        }
      ?>
    </select>
  </td>
  <td>checkin : <?=$ar_checkin[$key]?><br>checkout : <?=$ar_checkoutr[$key]?></td>
  <td><i class='fa fa-male'></i> <?=$ar_adult[$key]?> <i class='fa fa-child'></i> <?=$ar_child[$key]?></td>
  <td><?=$ar_breakfast[$key]?></td>
  <td><?=$ar_extrabed[$key]?></td>
  <td><?=$ar_totalr[$key]?></td>
</tr>
<?php
  }
?>
  </tbody>
</table>
<?php
  }catch(Exception $e){
    echo $e->getMessage();
  }
?>
