<?php
try{
  session_start();
	error_reporting(E_ALL ^ E_NOTICE);
	include ('../../../../conf/connection.php');
  include ('../../class-pms-lite.php');

  $hoteloid = $_SESSION['_hotel'];
  $pmslite = new PMSLite($db);

  $bookingoid = $_POST['bookingoid'];

  if($_POST['product'] == "Other"){
    if(!empty($_POST['product1'])){ $product = $_POST['product1']; }else{ $product = $_POST['product']; }
  }else{ $product = $_POST['product']; }


  $stmt = $db->prepare("insert into bookingcharges (bookingoid, typeposoid, product, qty, currencyoid, price, total, created, createdby, updated, updatedby) value (:a, :b, :c, :d, :e, :f, :g, :h, :i, :j, :k)");
  $stmt->execute(array(':a' => $_POST['bookingoid'], ':b' => $_POST['typepos'], ':c' => $product, ':d' => $_POST['qty'], ':e' => $_POST['currency'] , ':f' => $_POST['price'] , ':g' => $_POST['total'], ':h' => date('Y-m-d H:i:s'), ':i' => $_SESSION['_user'], ':j' => date('Y-m-d H:i:s'), ':k' => $_SESSION['_user']));
  $otherchargeid = $db->lastInsertId();

  $pmslite->RecalculateBooking($bookingoid);

  echo "1";

}catch(Exception $e){
  echo '<failed>'.$e->getMessage();
}
?>
