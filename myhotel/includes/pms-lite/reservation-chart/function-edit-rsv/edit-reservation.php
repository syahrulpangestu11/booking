<?php
try{
  session_start();
	error_reporting(E_ALL ^ E_NOTICE);
	include ('../../../../conf/connection.php');
  include ('../../class-pms-lite.php');

  $hoteloid = $_SESSION['_hotel'];
  $bookingroomoid = $_POST['brid'];

  $pmslite = new PMSLite($db);
  $pmslite->setPMSHotel($hoteloid);
  $br = $pmslite->dataBookingRoom($bookingroomoid);
  $brd = $pmslite->dataBookingRoomDtl($bookingroomoid);

  $pmslite->setRoom($br['roomoid']);
  $room = $pmslite->roomType();
  $rateplan = $pmslite->RatePlan();

  $hotelroom = $pmslite->hotelRoomType($hoteloid);

  /*--------------------------------------*/
  $_SESSION['tokenSession'.$pmslite->hotelcode] = $pmslite->hotelcode;

?>
  <form id="form-edit-reservation" method="post">
    <input type="hidden" name="bookingroomoid" value="<?=$br['bookingroomoid']?>">
    <input type="hidden" name="room" value="<?=$br['roomoid']?>">
    <input type="hidden" name="pmsstatus" value="<?=$br['pmsstatusoid']?>">
    <div class="row">
      <div class="col-md-12">
        <div class="row"><div class="col-md-12"><h3>Room Type : <?=$br['room']?></h3></div></div>
        <div class="row">
          <div class="col-md-2 col-xs-6">
            <div class="form-group">
              <label>Check in</label>
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                  <input type="text" name="checkin" id="startdate-today" class="form-control" value="<?=date('d M Y', strtotime($br['checkin']))?>">
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-2 col-xs-6">
            <div class="form-group">
              <label>Check out</label>
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                  <input type="text" name="checkout" id="enddate-today" class="form-control" value="<?=date('d M Y', strtotime($br['checkout']))?>">
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-xs-8">
            <div class="form-group">
              <label>Room Type</label>
              <select name="roomtypeoid" class="form-control">
                <?php foreach($hotelroom as $rt){ 
                  if($rt['roomoid'] == $br['roomoid']){ $selected = "selected"; }else{ $selected = ""; } ?>
                  <option value="<?=$rt['roomoid']?>" <?=$selected?>><?=$rt['roomname']?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="col-md-3 col-xs-8">
            <div class="form-group">
              <label>Rate Plan</label><select name="roomofferoid" class="form-control"><?php foreach($rateplan as $rp){ if($rp['roomofferoid'] == $br['roomofferoid']){ $selected = "selected"; }else{ $selected = ""; } ?><option value="<?=$rp['roomofferoid']?>" <?=$selected?>><?=$rp['rateplan']?></option><?php } ?></select>
            </div>
          </div>
          <!--
          <div class="col-md-3 col-xs-4">
            <div class="form-group">
              <button type="button" name="recheck-price" class="btn btn-xs btn-success" style="margin-top:22px"><i class="fa fa-refresh"></i> Check New Price</button>
            </div>
          </div>
          -->
        </div>
        <div class="show-detail-create-rsv">
          <hr style="border-top: 2px dashed #b7d3d1;"/>
          <h3><i class="fa fa-key"></i> Setting Room</h3>
          <div class="row">
            <div class="col-md-12">
              <table class="table">
                <thead>
                  <tr><th>Guest Room*</th><th>Adult</th><th>Child</th><th>Assign Room</th>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      <div class="form-group">
                        <div class="input-group">
                          <div class="input-group-addon"><i class="fa fa-user"></i></div>
                          <input type="text" name="guest" class="form-control" value="<?=$br['guestroom']?>">
                        </div>
                      </div>
                    </td>
                    <td>
                      <div class="form-group">
                        <select class="form-control" name="adult">
                          <?php for($i = 1; $i <= $room['adult']; $i++){ if($i == $br['adult']){ $selected = "selected"; }else{ $selected = ""; } ?>
                            <option value="<?=$i?>" <?=$selected?>><?=$i?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </td>
                    <td>
                        <div class="form-group">
                          <select class="form-control" name="child">
                            <?php for($i = 0; $i <= $room['child']; $i++){ if($i == $br['child']){ $selected = "selected"; }else{ $selected = ""; } ?>
                              <option value="<?=$i?>" <?=$selected?>><?=$i?></option>
                            <?php } ?>
                          </select>
                        </div>
                    </td>
                    <td>
                      <?php
                        foreach($brd as $br_detail){
                          $list_roomnumber = $pmslite->loadShowAssignRoom($br_detail['date'], $br['roomoid']);
                      ?>
                      <div class="row">
                        <div class="col-xs-4"><?=date('d M Y', strtotime($br_detail['date']))?></div>
                        <div class="col-xs-4">
                          <select name="roomnumber[]" class="form-control">
                            <option value="<?=$br_detail['roomnumberoid']?>" selected><?=$br_detail['roomnumber']?></option>
                            <?php foreach($list_roomnumber as $key => $rn){ ?>
                            <option value="<?=$rn['roomnumberoid']?>"><?=$rn['roomnumber']?></option>
                            <?php } ?>
                          </select>
                        </div>
                        <div class="col-xs-4" align="right" dt="<?=$br_detail['date']."|".$bookingroomoid?>">
                            <?php
                            $olddata = $pmslite->getDailyRateBookingbyDate($br_detail['date'], $bookingroomoid);
                            ?>
                            <input type="hidden" name="date[]" value="<?=$br_detail['date']?>">
                            <input type="text" class="form-control" name="dailyrate[]" value="<?=$olddata['total']?>" style="width:120px;background-color: #feffd3;">
                        </div>
                      </div>
                      <?php
                        }
                      ?>
                    </td>
                  </tr>
                </tbody>
              </table>
              <small>* If guest room empty system will default get name from guest detail.</small>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 text-right">
              <h3 style="font-size: 1.2em;">Total :</h3>
              <h2 style="font-size: 1.5em;margin-top: 5px;" fnc="grandtotal"><?=$br['currency']?> <span fnc="grandtotal"><?=number_format($br['total'], 2)?></span></h2>
            </div>
          </div>
        </div>
      </div>
    </div>
    <hr />
    <div class="row">
      <div class="col-md-12 text-right button-response">
        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal" aria-label="Close">Close</button>
        <button type="button" name="save" value="change" class="btn btn-sm btn-primary">Save Change</button>
        <?php
        if($br['pmsstatusoid'] == 2){
          if($data['checkin'] > date('Y-m-d')){
        ?>
          <button type="button" name="next-action" value="4" class="btn btn-sm btn-success">Early Check in</button>
        <?php
          }else{
        ?>
          <button type="button" name="next-action" value="4" class="btn btn-sm btn-success">Check in</button>
        <?php
          }
        }
        ?>

        <?php
        if($br['pmsstatusoid'] == 4){
          if(date('Y-m-d') < $data['checkout']){
        ?>
          <button type="button" name="next-action" value="5" class="btn btn-sm btn-warning">Early Check out</button>
          <?php
          }else{
          ?>
            <button type="button" name="next-action" value="5" class="btn btn-sm btn-warning">Check out</button>
        <?php
          }
        }
        ?>
      </div>
    </div>
  </form>
<?php
}catch(Exception $e){
  echo $e->getMessage();
}
?>
