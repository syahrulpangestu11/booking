<?php
try{
  session_start();
  error_reporting(E_ALL ^ E_NOTICE);
  include ('../../../../conf/connection.php');

  $bookingoid = $_POST['bid'];
  $currency = $_POST['currency'];

  if(isset($_POST['room']) and $_POST['room'] == 'y'){
    $stmt = $db->prepare("update booking b inner join `hotel` h using (hoteloid) inner join `chain` ch using (chainoid) inner join `bookingroom` br using (bookingoid) inner join `bookingroomdtl` brd using (bookingroomoid)  set b.currencyoid = :a, br.currencyoid = :b, brd.currencyoid = :c where b.bookingoid = :d");
    $stmt->execute(array(':a' => $currency, ':b' => $currency, ':c' => $currency, ':d' => $bookingoid));
  }

  if(isset($_POST['charges']) and $_POST['charges'] == 'y'){
    $stmt = $db->prepare("update `bookingcharges` set `currencyoid` = :a where bookingoid = :b");
    $stmt->execute(array(':a' => $currency, ':b' => $bookingoid));
  }

  if(isset($_POST['payment']) and $_POST['payment'] == 'y'){
    $stmt = $db->prepare("update `bookingpaymentdtl` set `currencyoid` = :a where bookingoid = :b");
    $stmt->execute(array(':a' => $currency, ':b' => $bookingoid));
  }

  echo "1";

}catch(Exception $e){
  echo $e->getMessage();
}
?>
