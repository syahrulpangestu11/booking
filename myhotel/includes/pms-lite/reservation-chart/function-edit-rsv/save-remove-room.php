<?php
try{
  session_start();
	error_reporting(E_ALL ^ E_NOTICE);
	include ('../../../../conf/connection.php');
  include ('../../class-pms-lite.php');

  $hoteloid = $_SESSION['_hotel'];
  $pmslite = new PMSLite($db);
  $pmslite->setPMSHotel($hoteloid);

  $bookingroomoid = $_POST['brid'];
  $bookingroom = $pmslite->dataBookingRoom($bookingroomoid);

  $response = $pmslite->removeRoom($bookingroomoid);

  $pmslite->RecalculateBooking($bookingroom['bookingoid']);

  echo 1;

}catch(Exception $e){
  echo $e->getMessage();
}
?>
