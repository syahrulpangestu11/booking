<?php
try{
  session_start();
	error_reporting(E_ALL ^ E_NOTICE);
	include ('../../../../conf/connection.php');
  include ('../../class-pms-lite.php');

  $hoteloid = $_POST['hotel'];
  $checkin = date('d M Y', strtotime($_POST['date']));
  $checkout = date('d M Y', strtotime($checkin. ' +1 days'));
  $roomoid = $_POST['room'];
  $roomnumberoid = $_POST['roomnumber'];

  $pmslite = new PMSLite($db);
  $pmslite->setPMSHotel($hoteloid);
  $pmslite->setRoom($roomoid);
  $room = $pmslite->roomType();
  $roomnb = $pmslite->roomTypeNumber($roomnumberoid);
  /*--------------------------------------*/
?>
  <form id="form-create-ooo" method="post">
    <input type="hidden" name="hotel" value="<?=$hoteloid?>">
    <input type="hidden" name="room" value="<?=$room['roomoid']?>">
    <input type="hidden" name="roomnumber" value="<?=$roomnumberoid?>">
    <div class="row">
      <div class="col-md-12">
        <div class="row">
          <div class="col-md-6"><h3>Room Type : <?=$room['roomname']?></h3></div>
          <div class="col-md-6"><h3>Room Number : <?=$roomnb['roomnumber']?></h3></div>
        </div>
        <div class="row">
          <div class="col-md-3 col-xs-6">
            <div class="form-group">
              <label>From</label>
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                  <input type="text" name="checkin" id="startdate-today" class="form-control" value="<?=$checkin?>">
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-xs-6">
            <div class="form-group">
              <label>To</label>
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                  <input type="text" name="checkout" id="enddate-today" class="form-control" value="<?=$checkout?>">
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-xs-12">
            <div class="form-group">
              <label>Reason</label>
              <textarea name="reason" class="form-control"></textarea>
            </div>
          </div>
        </div>
        <div class="show-detail-create-rsv">
        </div>
      </div>
    </div>
    <hr/>
    <div class="row">
      <div class="col-md-12 text-right">
        <button type="button" name="save" class="btn btn-sm btn-primary">Save</button>
        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </form>
<?php
}catch(Exception $e){
  echo $e->getMessage();
}
?>
