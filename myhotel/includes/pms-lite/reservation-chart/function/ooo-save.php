<?php
try{
  session_start();
	error_reporting(E_ALL ^ E_NOTICE);
	include ('../../../../conf/connection.php');
  include ('../../class-pms-lite.php');

  $hoteloid = $_POST['hotel'];
  $pmslite = new PMSReservation($db);
  $pmslite->setPMSHotel($hoteloid);

  $roomnumber = $_POST['roomnumber'];
  $from = date('Y-m-d',strtotime($_POST['checkin']));
  $to   = date('Y-m-d',strtotime($_POST['checkout']));
  $reason = $_POST['reason'];

$stmt	= $db->prepare("insert into room_ooo (roomnumberoid, startdate, enddate, reason, status, created, createdby, updated, updatedby) value (:a, :b, :c, :d, :e, :f, :g, :h, :i)");
$stmt->execute(array(':a' => $roomnumber, ':b' => $from, ':c' => $to, ':d' => $reason, ':e' => '1', ':f' => date('Y-m-d H:i:s'), ':g' => $_SESSION['_user'], ':h' => date('Y-m-d H:i:s'), ':i' => $_SESSION['_user']));
    
    function differenceDate($arrival, $departure){
    	$date1 = new DateTime($arrival);
    	$date2 = new DateTime($departure);
    	$interval = $date1->diff($date2);
    	return $interval->days;
    }
    $night = differenceDate($from, $to);
    
    // Prepare Data
    $stmt = $db->prepare("SELECT `roomoid` FROM `roomnumber` WHERE `roomnumberoid` = :a");
    $stmt->execute(array(':a' => $roomnumber));
    $r_ro = $stmt->fetch(PDO::FETCH_ASSOC);
    $vroomoid = $r_ro['roomoid'];
    
    $stmt = $db->prepare("SELECT COUNT(`roomnumberoid`) AS jml FROM `roomnumber` WHERE `roomoid` = :a AND `publishedoid` = 1");
    $stmt->execute(array(':a' => $vroomoid));
    $r_ro = $stmt->fetch(PDO::FETCH_ASSOC);
    $vroomnumber = $r_ro['jml'];
    
    $data6 = '{
        "token": "pfUxpxmKTJNTStNhPK2Nly4UKxbcVWwNRUT1SXnPfRiWqhvmINnJ4XnefNNBuTsx",
        "hcode": "'.$pmslite->hotelcode.'",
        "roomid": "'.$vroomoid.'",
        "data": [';
    $z = 0;
    
    for($n=0; $n<=$night; $n++){
        $date = date("Y-m-d",strtotime($from." +".$n." day"));
    
        $stmt = $db->prepare("SELECT COUNT(`bookingroomdtloid`) jml
                FROM `bookingroomdtl` brd
                    LEFT JOIN `bookingroom` br USING(`bookingroomoid`)
                    LEFT JOIN `booking` b USING(`bookingoid`)
                    LEFT JOIN `roomoffer` ro USING(`roomofferoid`)
                WHERE b.`bookingstatusoid` IN (4) AND br.`pmsstatusoid` IN (2,4,7) 
                	AND ro.`roomoid` = :a AND brd.`date` = :b");
        $stmt->execute(array(':a' => $vroomoid, ':b' => $date));
        $r_rto = $stmt->fetch(PDO::FETCH_ASSOC);
        $vtxnumber = $r_rto['jml'];
        
        $vval = $vroomnumber - $vtxnumber;
        if($vval > 0){ $vval = $vval - 1; }
        
        if($z != 0){ $data6 .=","; }else{ $z = 1; }
        $data6 .= '
            {
                "from": "'.$date.'",
                "to": "'.$date.'",
                "value": "'.$vval.'"
            }
        ';
    }
    
    // Prepare Data
    $data6 .= '
        ]
    }';
    
    // The data to send to the API
    $postData = (array) json_decode($data6);
    
    // Setup cURL
    $ch = curl_init('https://thebuking.com/apiv1/save/allotment_inrange/');
    curl_setopt_array($ch, array(
        CURLOPT_POST => TRUE,
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_SSL_VERIFYPEER => FALSE,
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
        ),
        CURLOPT_POSTFIELDS => json_encode($postData)
    ));
    
    // Send the request
    $response = curl_exec($ch);
    
    // Check for errors
    if($response === FALSE){
        die(curl_error($ch));
    }
    
    // Decode the response
    $responseData = json_decode($response, TRUE);


  echo "1";

}catch(Exception $e){
  echo $e->getMessage();
}
?>
