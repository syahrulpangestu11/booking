<?php
  $currencyoid = $_POST['currency'];

  $rateplan = $pmslite->detailRatePlan($_POST['roomofferoid']);
  $checkin = date('Y-m-d', strtotime($_POST['checkin']));
  $checkout = date('Y-m-d', strtotime($_POST['checkout']));
  $night = $pmslite->dateDiff($checkin, $checkout);

  $list_date = array();

  for($n=0; $n<$night; $n++){
    $date = date('Y-m-d', strtotime($checkin. ' +'.$n.' days'));
    array_push($list_date, $date);
  }

  foreach($_POST['guest'] as $key => $guestroom){
    if(empty($guestroom)){ $guestroom = $set_defaultname; }
    $adult      = $_POST['adult'][$key];
    $child      = $_POST['child'][$key];
    $promotion  = $_POST['remark'][$key];
    $roomtotal  = $_POST['roomtotal'][$key];
    if(empty($_POST['extrabed'][$key])){ $extrabed = 'n'; }else{ $extrabed = $_POST['extrabed'][$key]; }

    $stmt = $db->prepare("INSERT INTO `bookingroom` (`bookingoid`, `roomofferoid`, `channeloid`, `promotionoid`, `packageoid`, `checkin`, `checkout`, `night`, `adult`, `child`, `roomtotal`, `extrabedtotal`, `total`, `currencyoid`, `breakfast`, `extrabed`, `hotel`, `room`, `promotion`, `roomtotalr`, `totalr`, `checkoutr`, `promocode`, `promocodecomm`, `pmsstatusoid`, `guestroom`) VALUES (:a , :b , :c , :d, :e , :f, :g, :h, :i, :j, :k, :l, :m, :n, :o, :p, :q, :r, :s, :t, :u, :v, :w, :x, :y, :z)");

    $stmt->execute(array(':a' => $pmslite->bookingoid, ':b' => $_POST['roomofferoid'], ':c' => 0, ':d' => 0, ':e' => 0, ':f' => $checkin, ':g' => $checkout, ':h' => $night, ':i' => $adult, ':j' => $child, ':k' => $roomtotal, ':l' => 0, ':m' => $roomtotal, ':n' => $currencyoid, ':o' => 'n', ':p' => $extrabed, ':q' => $pmslite->hotelname, ':r' => $rateplan['roomname'], ':s' => $promotion, ':t' => $roomtotal, ':u' => $roomtotal, ':v' => $checkout, ':w' => 'thebuking', ':x' => '0', ':y' => $_POST['pmsstatus'], ':z' => $guestroom));
    $bookingroomoid = $db->lastInsertId();
    
        // Prepare Data
        $stmt = $db->prepare("SELECT `roomoid` FROM `roomoffer` WHERE `roomofferoid` = :a");
        $stmt->execute(array(':a' => $_POST['roomofferoid']));
        $r_ro = $stmt->fetch(PDO::FETCH_ASSOC);
        $vroomoid = $r_ro['roomoid'];
        
        $stmt = $db->prepare("SELECT COUNT(`roomnumberoid`) AS jml FROM `roomnumber` WHERE `roomoid` = :a AND `publishedoid` = 1");
        $stmt->execute(array(':a' => $vroomoid));
        $r_ro = $stmt->fetch(PDO::FETCH_ASSOC);
        $vroomnumber = $r_ro['jml'];
        
        $data6 = '{
            "token": "pfUxpxmKTJNTStNhPK2Nly4UKxbcVWwNRUT1SXnPfRiWqhvmINnJ4XnefNNBuTsx",
            "hcode": "'.$pmslite->hotelcode.'",
            "roomid": "'.$vroomoid.'",
            "data": [';
        $z = 0;

    $newkey = $key+1;
    foreach($_POST['roomnumber-'.$newkey] as $key2 => $roomnumber){
      $dailyrate = $_POST['rate-'.$newkey][$key2];
      $stmt = $db->prepare("insert into `bookingroomdtl` (`bookingroomoid`, `date`, `rate`, `discount`, `rateafter`, `extrabed`, `total`, `currencyoid`, `guestroom`, `roomnumberoid`) VALUES (:a , :b , :c , :d , :e , :f, :g, :h, :i, :j)");
      $stmt->execute(array(':a' => $bookingroomoid, ':b' => $list_date[$key2], ':c' => $dailyrate, ':d' => 0, ':e' => $dailyrate, ':f' => 0, ':g' => $dailyrate, ':h' => $currencyoid, ':i' =>  $guestroom, ':j' => $roomnumber));
    
        // Prepare Data
        $stmt = $db->prepare("SELECT COUNT(`bookingroomdtloid`) jml
                FROM `bookingroomdtl` brd
                    LEFT JOIN `bookingroom` br USING(`bookingroomoid`)
                    LEFT JOIN `booking` b USING(`bookingoid`)
                    LEFT JOIN `roomoffer` ro USING(`roomofferoid`)
                WHERE b.`bookingstatusoid` IN (4) AND br.`pmsstatusoid` IN (2,4,7) 
                	AND ro.`roomoid` = :a AND brd.`date` = :b");
        $stmt->execute(array(':a' => $vroomoid, ':b' => $list_date[$key2]));
        $r_ro = $stmt->fetch(PDO::FETCH_ASSOC);
        $vtxnumber = $r_ro['jml'];
        
        $vval = $vroomnumber - $vtxnumber;
        
        if($z != 0){ $data6 .=","; }else{ $z = 1; }
        $data6 .= '
            {
                "from": "'.$list_date[$key2].'",
                "to": "'.$list_date[$key2].'",
                "value": "'.$vval.'"
            }
        ';
        
    }
    
        // Prepare Data
        $data6 .= '
            ]
        }';
    
        if($_POST['pmsstatus'] == '2' || $_POST['pmsstatus'] == '4' || $_POST['pmsstatus'] == '7'){
            // The data to send to the API
            $postData = (array) json_decode($data6);
            
            // Setup cURL
            $ch = curl_init('https://thebuking.com/apiv1/save/allotment_inrange/');
            curl_setopt_array($ch, array(
                CURLOPT_POST => TRUE,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_SSL_VERIFYPEER => FALSE,
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json'
                ),
                CURLOPT_POSTFIELDS => json_encode($postData)
            ));
            
            // Send the request
            $response = curl_exec($ch);
            
            // Check for errors
            if($response === FALSE){
                die(curl_error($ch));
            }
            
            // Decode the response
            $responseData = json_decode($response, TRUE);
        }
  }
?>
