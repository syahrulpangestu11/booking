<?php
try{
  session_start();
	error_reporting(E_ALL ^ E_NOTICE);
	include ('../../../../conf/connection.php');
  include ('../../class-pms-lite.php');

  $hoteloid = $_POST['hotel'];;
  $checkin = date('Y-m-d', strtotime($_POST['checkin']));
  $checkout = date('Y-m-d', strtotime($_POST['checkout']));
  $roomoid = $_POST['room'];
  $roomofferoid = $_POST['roomofferoid'];
  $roombooked = (isset($_POST['rooms']) or !empty($_POST['rooms']))  ? $_POST['rooms'] : 1;

  $pmslite = new PMSLite($db);
  $pmslite->setPMSHotel($hoteloid);
  $pmslite->loadXML();
  $pmslite->setRoom($roomoid);
  /*--------------------------------------*/

  $night = $pmslite->dateDiff($checkin, $checkout);

  $_SESSION['tokenSession'.$pmslite->hotelcode] = $pmslite->hotelcode;
  $availableroom = $pmslite->loadShowAvailableRoom($checkin, $night, $roomoid);

  if($availableroom > 0){
    for($r = 1; $r<=$availableroom; $r++){
      echo "<option aoa='".$checkin."' value='".$r."'>".$r."</option>";
    }
  }else{
    echo "<option aoa='".$checkin."' value='0'>0</option>";
  }
?>
<?php
}catch(Exception $e){
  echo $e->getMessage();
}
?>
