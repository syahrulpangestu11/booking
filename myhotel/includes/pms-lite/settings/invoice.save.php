<?php
	$invnolabel = $_POST['invnolabel'];
	$notefax = $_POST['notefax'];
	$noteemail = $_POST['noteemail'];
	
	try {
		$stmt = $db->prepare("UPDATE `pmsinvoicesetting` SET `invnolabel`=:b,`notefax`=:c,`noteemail`=:d WHERE `hoteloid`=:a");
		$stmt->execute(array(':a' => $hoteloid, ':b' => $invnolabel, ':c' => $notefax, ':d' => $noteemail));
		$affected_rows = $stmt->rowCount();
		
		echo '<script>alert("Data is saved"); location.href = "'.$base_url.'/pms-lite/settings";</script>';
	}catch(PDOException $ex) {
		echo '<script>alert("Failed to save the data"); location.href = "'.$base_url.'/pms-lite/settings";</script>';
	}
?>