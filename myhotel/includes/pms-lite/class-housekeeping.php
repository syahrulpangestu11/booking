<?php
class PMSHousekeeping extends PMSLite{

  function HousekeepingToday(){

    //$stmt = $this->db->prepare("INSERT INTO housekeepinglog (logtime, roomnumberoid, fromstatus, tostatus, note, created, createdby) SELECT '".date('Y-m-d 00:00:00')."', rn.roomnumberoid, rn.housekeepingstatusoid, '1', '',  '".date('Y-m-d 00:00:00')."', 'System' FROM roomnumber rn inner join room r using (roomoid) left join housekeepingstatus hks using (housekeepingstatusoid) left join (select max(logtime) as lastupdate, roomnumberoid from housekeepinglog group by roomnumberoid) as hl on hl.roomnumberoid = rn.roomnumberoid where r.hoteloid = :a and rn.publishedoid = '1' and r.publishedoid = '1' and (date(hl.lastupdate) <  '".date('Y-m-d')."' or hl.lastupdate IS NULL)");
    $stmt = $this->db->prepare("INSERT INTO housekeepinglog (logtime, roomnumberoid, fromstatus, tostatus, note, created, createdby) SELECT '".date('Y-m-d 00:00:00')."', rn.roomnumberoid, rn.housekeepingstatusoid, CASE WHEN occupied > 0 THEN '6' ELSE '1' END, '',  '".date('Y-m-d 00:00:00')."', 'System' FROM roomnumber rn inner join room r using (roomoid) left join housekeepingstatus hks using (housekeepingstatusoid) left join (select max(logtime) as lastupdate, roomnumberoid from housekeepinglog group by roomnumberoid) as hl on hl.roomnumberoid = rn.roomnumberoid left join (select count(brd.bookingroomdtloid) as occupied, rn.roomnumberoid from bookingroomdtl brd inner join roomnumber rn using (roomnumberoid) inner join bookingroom br using (bookingroomoid) inner join booking b using (bookingoid) where b.hoteloid = :a and brd.date = '".date('Y-m-d')."' and b.bookingstatusoid = '4' and br.pmsstatusoid in ('4') and brd.reconciled = '0' group by rn.roomnumberoid) oc on oc.roomnumberoid = rn.roomnumberoid  where r.hoteloid = :b and rn.publishedoid = '1' and r.publishedoid = '1' and (date(hl.lastupdate) <  '".date('Y-m-d')."' or hl.lastupdate IS NULL)");
		$stmt->execute(array(':a' => $this->hoteloid, ':b' => $this->hoteloid));
    $count_affected_insert = $stmt->rowCount();
    if($count_affected_insert > 0){
      $stmt = $this->db->prepare("UPDATE roomnumber rn inner join room r using (roomoid) inner join (select roomnumberoid, tostatus, note, logtime as lastupdate from housekeepinglog where housekeepinglogoid in (SELECT max(housekeepinglogoid) from housekeepingstatus group by roomnumberoid)) as orn on orn.roomnumberoid = rn.roomnumberoid SET rn.housekeepingstatusoid = tostatus, rn.housekeepingnote = note WHERE r.hoteloid = :a");
  		$stmt->execute(array(':a' => $this->hoteloid));
    }

    $stmt = $this->db->prepare("SELECT rn.roomnumber, rn.roomnumberoid, r.name as roomtype, hks.status as housekeepingstatus, rn.housekeepingstatusoid, rn.housekeepingnote as remark, hl.lastupdate FROM roomnumber rn inner join room r using (roomoid) left join housekeepingstatus hks using (housekeepingstatusoid) left join (select max(logtime) as lastupdate, roomnumberoid from housekeepinglog group by roomnumberoid) as hl on hl.roomnumberoid = rn.roomnumberoid WHERE r.hoteloid  = :a and rn.publishedoid = '1' and r.publishedoid = '1'");
		$stmt->execute(array(':a' => $this->hoteloid));
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    return $result;
  }

  function HousekeepingRoom($roomnumberoid, $date){
    $stmt = $this->db->prepare("SELECT rn.roomnumber, rn.roomnumberoid, r.name as roomtype, hks.status as housekeepingstatus, rn.housekeepingstatusoid, rn.housekeepingnote as remark FROM roomnumber rn inner join room r using (roomoid) left join housekeepingstatus hks using (housekeepingstatusoid) WHERE rn.roomnumberoid  = :a");
		$stmt->execute(array(':a' => $roomnumberoid));
		$result = $stmt->fetch(PDO::FETCH_ASSOC);

    return $result;
  }

  function RoomStatusHousekeeping($roomnumberoid, $date){
    $data = array();

    $stmt = $this->db->prepare("SELECT count(brd.bookingroomdtloid) as jmldata, br.pmsstatusoid from bookingroomdtl brd inner join bookingroom br using (bookingroomoid) inner join booking b using (bookingoid) inner join customer c using (custoid) where brd.roomnumberoid = :a and  brd.date = :b and b.bookingstatusoid = '4' and br.pmsstatusoid in ('4') and brd.reconciled = '0'");
		$stmt->execute(array(':a' => $roomnumberoid, ':b' => $date));
		$result = $stmt->fetch(PDO::FETCH_ASSOC);

    if($result['jmldata'] > 0){
      if($result['pmsstatusoid'] == '5' or $result['pmsstatusoid'] == '6'){
        $data['availability'] = "Check Out";
        $data['class'] = "checkout";
      }else{
        $data['availability'] = "Occupied";
        $data['class'] = "occupied";
      }
    }else{
      $data['availability'] = "Available";
      $data['class'] = "available";
    }

    return $data;
  }
}
?>
