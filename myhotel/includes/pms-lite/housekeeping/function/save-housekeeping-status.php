<?php
try{
  session_start();
  error_reporting(E_ALL ^ E_NOTICE);
  include ('../../../../conf/connection.php');

  $roomnumberoid = $_POST['roomnumberoid'];
  $datetime = date('Y-m-d H:i:s');
  $fromstatus = $_POST['fromstatus'];
  $tostatus = $_POST['tostatus'];
  $note = $_POST['note'];

  $stmt	= $db->prepare("INSERT INTO housekeepinglog (logtime, roomnumberoid, fromstatus, tostatus, note, created, createdby) VALUES (:a, :b, :c, :d, :e, :f, :g)");
  $stmt->execute(array(':a' => $datetime, ':b' => $roomnumberoid, ':c' => $fromstatus, ':d' => $tostatus, ':e' => $note, ':f' => $datetime, ':g' => $_SESSION['_user']));

  $stmt	= $db->prepare("UPDATE roomnumber SET housekeepingstatusoid = :a, housekeepingnote = :b WHERE roomnumberoid = :id");
  $stmt->execute(array(':a' => $tostatus, ':b' => $note, ':id' => $roomnumberoid));

  echo "1";

}catch(Exception $e){
  echo $e->getMessage();
}
?>
