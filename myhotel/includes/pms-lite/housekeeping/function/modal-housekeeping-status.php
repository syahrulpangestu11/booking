<?php
try{
  session_start();
	error_reporting(E_ALL ^ E_NOTICE);
	include ('../../../../conf/connection.php');
  include ('../../class-pms-lite.php');
  include ('../../class-housekeeping.php');

  $hoteloid = $_POST['hotel'];
  $date = date('Y-m-d', strtotime($_POST['date']));
  $roomnumberoid = $_POST['roomnumber'];

  $housekeeping = new PMSHousekeeping($db);

  $hk = $housekeeping->HousekeepingRoom($roomnumberoid, $date);
  $statusroom = $housekeeping->RoomStatusHousekeeping($hk['roomnumberoid'], $date);


?>
  <form id="form-change-housekeepingstatus" method="post">
    <input type="hidden" name="roomnumberoid" value="<?=$hk['roomnumberoid']?>">
    <input type="hidden" name="fromstatus" value="<?=$hk['housekeepingstatusoid']?>">
    <input type="hidden" name="date" value="<?=$date?>">
    <div class="row">
      <div class="col-md-6"></div>
      <div class="col-md-6"></div>
    </div>
    <div class="row">
      <div class="col-md-2">
        <div class="form-group">
          <label for="inputEmail3">Current Status</label><input type="text" readonly class="form-control" name="status" value="<?=$hk['housekeepingstatus']?>">
        </div>
      </div>
      <div class="col-md-2">
        <div class="form-group">
          <label for="inputEmail3">Change Status</label>
          <select name="tostatus" class="form-control input-sm">
          <?php
          $stmt = $db->prepare("select * from housekeepingstatus where publishedoid = '1'");
          $stmt->execute();
          $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
          foreach($result as $status){
            if($hk['housekeepingstatusoid'] == $status['housekeepingstatusoid']){ $selected = "selected"; }else{ $selected = ""; }
          ?>
          <option value="<?=$status['housekeepingstatusoid']?>" <?=$selected?>><?=$status['status']?></option>
          <?php
          }
          ?>
          </select>
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label for="inputEmail3">Remark / Note</label><input type="text" class="form-control" name="note" value="<?=$hk['remark']?>">
        </div>
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="col-md-12 text-right">
        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal" aria-label="Close">Close</button>
        <button type="button" name="btn-submit" class="btn btn-sm btn-primary">Save Change</button>
      </div>
    </div>
  </form>
<?php
}catch(Exception $e){
  echo $e->getMessage();
}
?>
