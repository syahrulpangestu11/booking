<?php
  include("includes/pms-lite/housekeeping/script-housekeeping.php");
  include("includes/pms-lite/class-pms-lite.php");
  include("includes/pms-lite/class-housekeeping.php");

  $housekeeping = new PMSHousekeeping($db);

  if(!isset($_POST['startdate']) and !isset($_POST['enddate'])){
    $_POST['startdate'] = date('d F Y', strtotime(date('d F Y'). ' -1 days'));
    $_POST['enddate'] = date('d F Y', strtotime($_POST['startdate']. ' +30 days'));
  }
?>
<section class="content-header">
  <h1>Housekeeping</h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-circle"></i>  PMS Lite</a></li>
    <li><a href="#"><i class="fa fa-circle"></i>  Housekeeping</a></li>
  </ol>
</section>
<section id="pms-lite" class="content">
  <form class="form-inline" method="post" action="<?=$base_url?>/pms-lite/housekeeping/">
  <div class="box box-form box-warning">
    <div class="box-body">
      <div class="row">
        <?php if(empty($_SESSION['_hotel'])){ ?>
          <div class="col-md-3 col-xs-6">
            <select class="form-control" name="zarea" id="zarea" style="width:100%">
              <option value="" selected>Show All Area</option>
              <?php
                $s_hotelcity = "select ct.cityoid, ct.cityname, h.hotelname from hotel h inner join city ct using (cityoid) inner join chain c using (chainoid) where ((h.chainoid in (select oid from userassign where type = 'chainoid' and useroid = '".$_SESSION['_oid']."')) or (h.hoteloid in (select oid from userassign where type = 'hoteloid' and useroid = '".$_SESSION['_oid']."'))) and h.publishedoid = '1' group by ct.cityoid";
                $q_hotelcity = $db->query($s_hotelcity);
                $r_hotelcity = $q_hotelcity->fetchAll(PDO::FETCH_ASSOC);
                foreach($r_hotelcity as $hotelcity){
                  if($hotelcity['cityoid'] == $_POST['zarea']){ $selected = "selected"; }else{ $selected = ""; }
              ?>
                <option value="<?=$hotelcity['cityoid']?>" <?=$selected?>><?=$hotelcity['cityname']?></option>
              <?php
                }
              ?>
            </select>
          </div>
          <div class="col-md-3 col-xs-6">
              <input type="text" class="form-control" name="hotelname" style="width:100%" placeholder="Hotel, villa, residence name">
              <input type="hidden" name="zhotel">
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-primary">CHECK</button>
          </div>
        <?php } ?>
      </div>
      <br>
      <div class="row">
        <div class="col-md-12">
  <?php
  if(empty($_SESSION['_hotel'])){
    $filter = array();
    if(!empty($_POST['zarea']) and empty($_POST['hotelname'])){ array_push($filter, 'h.cityoid = "'.$_REQUEST['zarea'].'"'); }
    if(!empty($_POST['zhotel'])){ array_push($filter, 'h.hotelcode = "'.$_POST['zhotel'].'"'); }
    if(!empty($_POST['hotelname'])){ array_push($filter, 'h.hotelname like "%'.$_POST['hotelname'].'%"'); }

    if(count($filter) > 0){ $combine_filter = ' and '.implode(' and ',$filter); }else{ $combine_filter = ""; }

    $s_hotelchain = "select h.hoteloid, h.hotelcode, h.hotelname from hotel h inner join chain c using (chainoid) where (h.chainoid in (select oid from userassign where type = 'chainoid' and useroid = '".$_SESSION['_oid']."' ".$combine_filter.") or h.hoteloid in (select oid from userassign where type = 'hoteloid' and useroid = '".$_SESSION['_oid']."' ".$combine_filter.")) and h.publishedoid = '1'";
  }else{
    $s_hotelchain = "select h.hoteloid, h.hotelcode, h.hotelname from hotel h inner join chain c using (chainoid) where  h.hoteloid = '".$_SESSION['_hotel']."' and h.publishedoid = '1'";
  }

  $q_hotelchain = $db->query($s_hotelchain);
  $r_hotelchain = $q_hotelchain->fetchAll(PDO::FETCH_ASSOC);
  foreach($r_hotelchain as $hotelchain){
    $housekeeping->setPMSHotel($hotelchain['hoteloid']);
  ?>
  <table class="table table-bordered table-striped" id="housekeepingtable">
    <thead>
      <tr><th colspan="5"><h2><?=$hotelchain['hotelname']?></h2></th></tr>
      <tr><th>Room</th><th>Room Type</th><th>Status</th><th>Availability</th><th>Remarks</th></tr>
    </thead>
    <tbody>
      <tr style="display:none"></tr>
      <?php
        $date = date('Y-m-d');
        foreach($housekeeping->HousekeepingToday() as $hk){
          $statusroom = $housekeeping->RoomStatusHousekeeping($hk['roomnumberoid'], $date);
      ?>
      <tr>
        <td><?=$hk['roomnumber']?></td>
        <td><?=$hk['roomtype']?></td>
        <td class="hk-<?=$hk['housekeepingstatusoid']?>"><a data-toggle="modal" data-target="#changeHKStatus" data-rn="<?=$hk['roomnumberoid']?>" data-date="<?=$date?>" data-hotel="<?=$hotelchain['hoteloid']?>" style="text-decoration:underline;"><?=$hk['housekeepingstatus']?></a></td>
        <td class="<?=$statusroom['class']?>"><?=$statusroom['availability']?></td>
        <td><a data-toggle="modal" data-target="#changeHKStatus" data-rn="<?=$hk['roomnumberoid']?>" data-date="<?=$date?>" data-hotel="<?=$hotelchain['hoteloid']?>"><?=$hk['remark']?></a></td>
      </tr>
      <?php
        }
      ?>
    </tbody>
  </table>
  <?php
  }
  ?>
        </div>
      </div>
    </div>
  </div>
  </form>
</section>

<div class="modal fade pms-lite-modal" id="changeHKStatus" tabindex="-1" role="dialog" aria-labelledby="changeHKStatus">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Change Housekeeping Status</h4>
      </div>
      <div class="modal-body">Please wait a while <i class="fa fa-smile"></i>
      </div>
    </div>
  </div>
</div>
