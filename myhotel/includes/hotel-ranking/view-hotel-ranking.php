<?php	
	try {
		$stmt = $db->query("select hc.*, c.cityname, h.hotelname, s.stateoid from hotel h inner join city c using (cityoid) inner join state s using (stateoid) left join hotelcommission hc using (hoteloid) where h.hoteloid = '".$hoteloid."'");
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
			$r_hotel = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_hotel as $row){
				$hotelname = $row['hotelname'];
				$commission = $row['value'];
				$city = $row['cityname'];
				$stateoid = $row['stateoid'];

				$stmt = $db->query("select h.hoteloid, rank.ranking from hotel h left join 
				(select @rank:=IFNULL(@rank,0)+1 AS ranking, h.hoteloid 
					from hotel h inner join city c using (cityoid) 
					inner join state s using (stateoid) 
					left join hotelcommission hc using (hoteloid) 
					where s.stateoid = '".$stateoid."' order by hc.value desc) 
				as rank using (hoteloid) 
				where h.hoteloid = '".$hoteloid."'");
				$r_hotel = $stmt->fetchAll(PDO::FETCH_ASSOC);
				foreach($r_hotel as $row){
					$ranking = $row['ranking'];	
				}
			}
		}else{
			echo "No Result";
			die();
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}
?>
<section class="content-header">
    <h1>
       	Hotel Ranking
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Settings</a></li>
        <li class="active">Hotel Ranking</li>
    </ol>
</section>
<section class="content">
    <form class="form-box" method="post" enctype="multipart/form-data" id="data-input" action="#">
    <input type="hidden" name="ho" value="<?php echo $hoteloid; ?>" />
    <div class="row">
        <div class="box box-form">
            <div class="box-body">
                	<h1><?php echo $roomname; ?></h1>
					<table class="table table-fill table-fill-centered">
                        <tr class="center">
                            <td><label>City</label></td>
                            <td><label>Current Commission</label></td> 
                            <td><label>Current Ranking</label></td>  
                            <td><label>Proposed Commission</label></td>  
                            <td style="display:none;"><label>Ranking with Proposed Commission</label></td>  
                        </tr>
                        <tr class="center">
                            <td><?php echo $city; ?></td>
                            <td><?php echo $commission; ?></td>
							<td><?php echo $ranking; ?></td>
							<td><input type="number" class="small" step="any"  name="commission" value="<?php echo $commission; ?>"></td>
							<td style="display:none;">&nbsp;</td>
                        </tr>
                    </table>
                    <button type="button" class="small-button blue save">Save</button>
            </div><!-- /.box-body -->
       </div>
    </div>
    </form>

    <div class="row">
        <div class="box box-form">
            <div class="box-body">

	<table class="table table-fill">
        <tr class="center">
            <td>Rank</td>
            <td>Hotel Name</td>
        </tr>
	<?php
        try {
            $stmt = $db->query("select @ranking:=IFNULL(@ranking,0)+1 AS ranking, hc.*, c.cityname, h.hotelname from hotel h inner join city c using (cityoid) inner join state s using (stateoid) left join hotelcommission hc using (hoteloid) where s.stateoid = '".$stateoid."' order by hc.value desc");
            $row_count = $stmt->rowCount();
            if($row_count > 0) {
                $r_hotel = $stmt->fetchAll(PDO::FETCH_ASSOC);
                foreach($r_hotel as $row){
                    $hotelname = $row['hotelname'];
                    $ranking = $row['ranking'];
				?>
                    <tr>
                    	<td class="center"><?php echo $ranking; ?></td>
                        <td><?php echo $hotelname; ?></td>
                    </tr>
                <?php
                }
            }else{
                echo "No Result";
                die();
            }
        }catch(PDOException $ex) {
            echo "Invalid Query";
            die();
        }
    
    ?>
	</table>
            </div><!-- /.box-body -->
       </div>
    </div>
</section>