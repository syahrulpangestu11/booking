<script type="text/javascript">
$(function(){	
	var $dialogNotice = $('<div id="dialog-notice"></div>')
	.dialog({
		autoOpen: false,
		title: 'Notification',
		buttons: { 
			"Ok": function(){ 
				$( this ).dialog( "close" );
				$(location).attr("href", "<?php echo $base_url; ?>/hotel-ranking"); 
			}
		}
	});
	
	$('button.save').click(function(){
		url = '<?php echo $base_url; ?>/includes/hotel-ranking/edit-hotel-ranking-save.php';
		forminput = $('form#data-input');
		textsuccess = 'Data succesfully saved';
		submitData(url, forminput);
	});
	
	function submitData(url, forminput){
		$.ajax({
			url: url,
			type: 'post',
			data: forminput.serialize(),
			success: function(data) {
				if(data == "1"){
					$dialogNotice.html("Data succesfully updated");
					$dialogNotice.dialog("open");
				}else{
					$dialogNotice.html("We&rsquo;re very sorry, we can't save your data right now");
					$dialogNotice.dialog("open");
				}
			}
		});
	}

});
</script>