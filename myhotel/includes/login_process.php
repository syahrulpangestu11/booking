<?php
$username = isset($_POST['user']) ? $_POST['user'] : $_COOKIE['_u'];
$password = isset($_POST['pass']) ? $_POST['pass'] : $_COOKIE['_p'];
$remember = isset($_POST['remember']) ? $_POST['remember'] : '';

setcookie("_u",$username);
setcookie("_p",$password);
$_SESSION['_remember'] = $remember;

try {
	$stmt = $db->prepare("SELECT u.*, ut.usertype, up.userpmstype FROM users u left join userstype ut using (userstypeoid) left join userpmstype up using (userpmstypeoid) WHERE username = :usr AND password = SHA1(:pwd) AND `u`.`status` = '1' limit 1");
	$stmt->execute(array(':usr' => $username, ':pwd' => $password));
	$founduser = $stmt->rowCount();

	// check if IBE or PMS account
	if($founduser > 0){
		$user = $stmt->fetch(PDO::FETCH_ASSOC);

		$_SESSION['_user'] = $user['username'];
		$_SESSION['_initial'] = $user['displayname'];
		$_SESSION['_oid'] = $user['useroid'];

		if(empty($user['picture'])){
			$_SESSION['_userpict'] = $base_url."/images/avatar5.png";
		}else{
			$_SESSION['_userpict'] = $user['picture'];
		}

		/*-------------------------------------------------------------------------------------------------------------*/

		if($user['ibeuser'] == "y"){
			$_SESSION['_type'] = $user['usertype'];
			$_SESSION['_typeusr'] = $user['userstypeoid'];

			// if user manage IBE unit only
			if($user['userstypeoid'] == 3 or $user['userstypeoid'] == 6 or $user['userstypeoid'] == 7){
				$stmt = $db->prepare("SELECT h.hoteloid, h.hotelname FROM userassign ua inner join hotel h on h.hoteloid = ua.oid  WHERE useroid = :user and type='hoteloid'");
				$stmt->execute(array(':user' => $user['useroid']));
				$hotel = $stmt->fetch(PDO::FETCH_ASSOC);

				$_SESSION['_hotel'] = $hotel['hoteloid'];
				$_SESSION['_hotelname'] = $hotel['hotelname'];
			}
		}

		if($user['pmsuser'] == "y"){
			$_SESSION['_typepms'] = $user['userpmstype'];
			$_SESSION['_typepmsusr'] = $user['userpmstypeoid'];
		}

		if($_SESSION['_typepmsusr'] == '2' or $_SESSION['_typepmsusr'] == '4' or $_SESSION['_typepmsusr'] == '5'){
			echo"<script>window.location.href = '".$base_url."/pms-lite/reservation-chart'</script>";
		}else{
			echo"<script>window.location.href = '".$base_url."/dashboard'</script>";
		}


	// check if AGENT account
	}else{
		$stmt = $db->prepare("SELECT * FROM agent WHERE email = :usr AND password = :pwd AND publishedoid = '1'");
		$stmt->execute(array(':usr' => $username, ':pwd' => sha1($password)));
		$foundagent = $stmt->rowCount();

		if($foundagent > 0){
			$agent = $stmt->fetch(PDO::FETCH_ASSOC);

			$_SESSION['_user'] = $agent['agentoid'];
			$_SESSION['_initial'] = $agent['agentname'];
			$_SESSION['_oid'] = $agent['agentoid'];
			$_SESSION['_type'] = 'agent';
			$_SESSION['_typeusr'] = 'agent';
			$_SESSION['_hotel'] = 0;

			if(empty($user['picture'])){
				$_SESSION['_userpict'] = $base_url."/images/avatar5.png";
			}else{
				$_SESSION['_userpict'] = $user['picture'];
			}

			echo"<script>window.location.href = '".$base_url."/dashboard'</script>";

		// USER DOESN'T EXIST
		}else{
			echo"<script>window.location.href = '".$base_url."/loginerror'</script>";
		}
	}


}catch(PDOException $ex) {
	echo "Invalid Query";
	die();
}
?>
