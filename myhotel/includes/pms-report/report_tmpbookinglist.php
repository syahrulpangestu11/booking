<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        $('#example').DataTable();
        
        $('html, body, .content-side').css('min-height','auto');
        
        $('body').on('click','.act', function(e) {
            var idr = $(this).parent().children('.input-select').val();
            var id = $(this).attr('id');
    		$.ajax({
    			url: "<?php echo"$base_url"; ?>/includes/pms-report/ajax/report_tmpbookinglist.php",
    			type: 'post',
    			data: {'id':id, 'idr':idr},
    			success: function(data) { 
    			    if(data=='1'){
    				    location.reload();
    			    }else{
    			        alert("invalid request");
    			    }
    			}
    		});
    	});
    });
</script>
<style>
    .act.gray{background-color:#d8d8d8;}
</style>
<section class="content-header">
    <h1>
       	Report PMS - Temporary Booking List
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Report PMS</a></li>
    </ol>
</section>
<section class="content">
<?php
	if(empty($_REQUEST['rt']) and empty($_REQUEST['startdate']) and empty($_REQUEST['enddate'])){
		$start = date("d F Y");
		$end = date("d F Y",  strtotime(date("Y-m-d")." +7 day" ));
	}else{
		$start = date("d F Y", strtotime($_REQUEST['startdate']));
		$end = date("d F Y", strtotime($_REQUEST['enddate']));
		$roomoffer = $_REQUEST['rt'];
		$channeloid = $_REQUEST['ch'];
	}
?>
<div class="row">
    <div class="box box-form">
        <div class="box-body" style="font-size: 12px;">
        <form method="get" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/room-control/">
            <input type="hidden" name="ho" value="<?php echo $hoteloid; ?>" />
            <div class="form-group" style="display:inline-block">
                <label for="" class="col-sm-2 control-label">Select Room Type:</label>
                <div class="col-sm-10">
                    <select name="rt" class="input-select">
                    <?php
                        try {
                            $stmt = $db->query("select r.roomoid, r.name, x.offernum from room r inner join hotel h using (hoteloid) left join (select count(roomofferoid) as offernum, ro.roomoid from roomoffer ro group by ro.roomoid) as x on x.roomoid = r.roomoid where h.hoteloid = '".$hoteloid."' and x.offernum > 0 and r.publishedoid = '1'");
                            $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                            foreach($r_room as $row){
                                echo"<optgroup label = '".$row['name']."'>";
                                try {
                                    $stmt = $db->query("select ro.* from roomoffer ro inner join published p using (publishedoid) inner join room r using (roomoid) inner join offertype using (offertypeoid) where r.roomoid = '".$row['roomoid']."' and p.publishedoid = '1'");
                                    $r_offer = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                    foreach($r_offer as $row1){
                    					if($row1['roomofferoid'] == $roomoffer){ $selected = 'selected = "selected"';  }else{ $selected = ''; }
                                       echo"<option value='".$row1['roomofferoid']."' ".$selected.">  ".$row1['name']."</option>";
                                    }
                                }catch(PDOException $ex) {
                                    echo "Invalid Query";
                                    die();
                                }     
                                echo"</optgroup>";                       
                            }
                        }catch(PDOException $ex) {
                            echo "Invalid Query";
                            die();
                        }
                    
                    ?>
                    </select>
                </div>
            </div>
            <div class="form-group" style="display:inline-block">
                <label for="" class="col-sm-2 control-label">Between:</label>
                <div class="col-sm-5">
                    <input type="text" name="startdate" id="startdate" placeholder="" value="<?php echo $start; ?>">
                </div>
            </div>
            <div class="form-group" style="display:inline-block">
                <label for="" class="col-sm-2 control-label">And:</label>
                <div class="col-sm-5">
                    <input type="text" name="startdate" id="enddate" placeholder="" value="<?php echo $end; ?>">
                </div>
            </div>
        </form>
        </div>
    </div>
</div>
<div class="row">
    <div class="box box-form">
    <div class="box-body" style="font-size: 12px;overflow-x:scroll;">
        <table id="example" class="display" cellspacing="0" width="100%">
            <thead>
                <tr>
                   <th>#</th>
                   <th>Res ID</th>
                   <th>Guest Name</th>
                   <td>Phone/Mobile</td>
                   <td>Email</td>
                   <td>Stay Duration</td>
                   <td>Room# / Type</td>
                   <th>Pax</th>
                   <th>Amount</th>
                   <th>Notes</th>
                   <th style="width:200px">Check in Card</th>
                </tr>
            </thead>
            <tbody>
            <?php
                try {
                    $stmt = $db->query("SELECT br.bookingroomoid, bookingnumber, title, firstname, lastname, phone, mobilephone, email, 
                            checkin, checkout, DATEDIFF(checkout, checkin) AS diff, br.room, br.adult, br.child, currencycode, br.total, r.roomoid
                        FROM `bookingroom` br
                            INNER JOIN `booking` b USING(`bookingoid`)
                            INNER JOIN `customer` c USING(`custoid`)
                            LEFT JOIN `currency` cr ON cr.`currencyoid` = br.`currencyoid`
                            INNER JOIN `roomoffer` ro ON ro.`roomofferoid` = br.`roomofferoid`
                            INNER JOIN `room` r ON r.`roomoid` = ro.`roomoid`
                        WHERE br.`pmsstatusoid` in (0,1) AND `bookingstatusoid` in (4) AND b.hoteloid='".$hoteloid."'
                        ORDER BY `bookingoid` DESC");
                    $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC); $n=0;
                    foreach($r_room as $row){
                        $title = ($row['title']=='1')?"Mrs.":($row['title']=='2')?"Miss":"Mr.";
                        $phone = !empty($row['phone'])?$row['phone']:$row['mobilephone'];
                        $date = date('d M', strtotime($row['checkin']))." - ".date('d M', strtotime($row['checkout']))." (".$row['diff'].")";
                        $pax = $row['adult']."(A) ".$row['child']."(C) ";
                        $amount = number_format($row['total'], 2, '.', ',');
                        ?>
                        <tr class="gradeX" room="<?=$row['roomoid']?>">
                            <td><?=++$n?></td>
                            <td><?=$row['bookingnumber']?></td>	
                            <td><?=$title." ".$row['firstname']." ".$row['lastname']?></td>
                            <td><?=$phone?></td>
                            <td><?=$row['email']?></td>
                            <td><?=$date?></td>
                            <td><?=$row['room']?></td>
                            <td><?=$pax?></td>
                            <td><?=$amount?></td>
                            <td>&nbsp;</td>
                            <td>
                                <select class="input-select">
                                    <option value="">Assign</option>
                                    <?php 
                                        $stmt = $db->query("SELECT *
                                            FROM `roomnumber` 
                                            WHERE `roomoid` = ".$row['roomoid']." AND `publishedoid` = 1 
                                                AND `roomnumberoid` NOT IN (SELECT `roomnumberoid` FROM `bookingroom` br INNER JOIN `booking` b USING(`bookingoid`)
                                                    WHERE br.`pmsstatusoid` in (2,4) AND `bookingstatusoid` in (4) AND b.hoteloid='".$hoteloid."' 
                                                    AND checkin <= '".$row['checkout']."' AND checkout >= '".$row['checkin']."')");
                                        $r_roomnum = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                        foreach($r_roomnum as $rownum){
                                            echo '<option value="'.$rownum['roomnumberoid'].'">'.$rownum['roomnumber'].'</option>';
                                        }
                                    ?>
                                </select>&nbsp;
                                <input type="button" class="blue-button act" id="<?=$row['bookingroomoid']?>" value="reserve">&nbsp;
                                <a href="#"><i class="fa fa-lg fa-envelope-o"></i></a>
                            </td>
                        </tr>
                        <?php
                    }
                }catch(PDOException $ex) {
                    echo "Invalid Query";
                    die();
                }
            
            ?>
            </tbody>
        </table>
    </div>
    </div>
</div>
</section>