<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    });
</script>
<section class="content-header">
    <h1>
       	Report PMS - Market
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Report PMS</a></li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="box box-form"><div class="box-body" style="overflow-x: scroll;font-size: 12px;">
            <table id="example" class="display" cellspacing="0" width="100%">
            <thead>
                <tr>
                   <th>Market</th>
                   <th>Total Reservations</th>
                   <th>Total Room Nights</th>
                   <th class="sort-numeric">Amount (inc. Tax)</th>
                   <th class="sort-alpha">Amount (nett Hotel inc. Tax)</th>
                </tr>
             </thead>
             <tbody>
                <tr class="gradeX">
                   <td>Direct Booking</td>
                   <td>3</td>
                   <td>7</td>
                   <td>3.509.800</td>
                   <td>2.900.661</td>
                </tr>
                <tr class="gradeC">
                   <td>Direct Website</td>
                   <td>18</td>
                   <td>45</td>
                   <td>67.500.000</td>
                   <td>55.785.123</td>
                </tr>
                <tr class="gradeA">
                   <td>Travel Agent</td>
                   <td>15</td>
                   <td>25</td>
                   <td>30.500.000</td>
                   <td>25.206.611</td>
                </tr>
                <tr class="gradeA">
                   <td>Corporate</td>
                   <td>3</td>
                   <td>8</td>
                   <td>18.400.000</td>
                   <td>15.206.611</td>
                </tr>
                <tr class="gradeA">
                   <td>Wedding & Event</td>
                   <td>3</td>
                   <td>50</td>
                   <td>105.000.000</td>
                   <td>86.776.859</td>
                </tr>
                <tr class="gradeA">
                   <td>Online Travel Agent (OTA)</td>
                   <td>72</td>
                   <td>167</td>
                   <td>192.050.000</td>
                   <td>158.719.008</td>
                </tr>
                <tr class="gradeA">
                   <td>Vacation Rental</td>
                   <td>26</td>
                   <td>56</td>
                   <td>68.880.000</td>
                   <td>56.925.619</td>
                </tr>
                <tr class="gradeA">
                   <td>Affiliate</td>
                   <td>3</td>
                   <td>7</td>
                   <td>10.500.000</td>
                   <td>8.677.685</td>
                </tr>
             </tbody>
            </table>
        </div></div>
    </div>
</section>