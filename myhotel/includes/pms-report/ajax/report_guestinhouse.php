<?php
try{
    session_start();
	error_reporting(0);
	include ('../../../conf/connection.php');

    $checkin = $_POST['a'];
    $stay = $_POST['b'];
    $hoteloid = $_POST['c'];

    $stmt = $db->prepare("SELECT hoteloid, hotelcode, hotelname FROM hotel WHERE hoteloid=:a AND publishedoid='1' ORDER BY hotelname");
    $stmt->execute(array(':a' => $hoteloid));
    $hotel = $stmt->fetch(PDO::FETCH_ASSOC);

    include("../function_report.php");

}catch(PDOException $e){
    echo "Failed to load the data!"; //. $e->getMessage();
    die();
}
?>
<div align="center">
    <h2 style="text-transform:uppercase"><?=$hotel['hotelname']?></h2>
    <h2>GUEST DEPOSIT REPORT</h2>
</div>
<br><br>
<div style="margin-bottom:10px">
    <div style="float:left"></div>
    <div style="float:right">
        <button type="button" class="btn btn-primary" id="btexport" style="margin-bottom:5px">Export to Excel</button>
        <form id="exporttoexcel" action="<?=$base_hotel?>/myhotel/pms-lite/view/rpginhouse_xls.php" method="post" target="_blank">
            <input name="a" type="hidden" value="<?=$checkin?>">
            <input name="b" type="hidden" value="<?=$stay?>">
            <input name="c" type="hidden" value="<?=$hoteloid?>">
        </form>
        </div>
    </div>
    <div style="clear:both"></div>
</div>
<table id="example" class="display" cellspacing="0" width="100%">
<thead>
    <tr>
        <th>Reservation ID</th>
        <th>Firstname</th>
        <th>Lastname</th>
        <th>Arrival Date</th>
        <th>Departure Date</th>
        <th>Nights</th>
        <th>Num of Adult/Child</th>
        <th>Rooms</th>
        <th>Status</th>
        <th>Currency</th>
        <th>Room Total</th>
        <th>Charges Total</th>
        <th>Paid</th>
        <th>Remaining Balance</th>
    </tr>

</thead>
<tbody>
    <?php
    $data = getReservationDataInHouse($db, $hoteloid, $checkin, $stay);
    foreach($data as $row){
        $bookingoid = $row['bookingoid'];
        $bookingnumber = $row['bookingnumber'];
        $firstname = $row['firstname'];
        $lastname = $row['lastname'];
        $checkin = date("d-M-Y", strtotime($row['checkin']));
        $checkout = date("d-M-Y", strtotime($row['checkout']));
        $diff = $row['diff'];
        $adult = empty($row['adult']) ? "0" : $row['adult'];
        $child = empty($row['child']) ? "0" : $row['child'];
        $roomnumber = $row['roomnumber'];
        $currcode = empty($row['currencycode']) ? "IDR" : $row['currencycode'];

        if($row['bookingstatusoid'] == "5"){
            $status = "Cancelled";
        }else if($row['bookingstatusoid'] == "7"){
            $status = "No Show";
        }else{
            $status = $row['status'];
        }

        echo '<tr>
            <td>'.$bookingnumber.'</td>
            <td>'.$firstname.'</td>
            <td>'.$lastname.'</td>
            <td align="center">'.$checkin.'</td>
            <td align="center">'.$checkout.'</td>
            <td align="center">'.$diff.'</td>
            <td align="center">'.$adult.'/'.$child.'</td>
            <td>'.$roomnumber.'</td>
            <td>'.$status.'</td>
            <td style="padding-left:10px">'.$currcode.'</td>
            <td align="right">'.number_format($row['totalroom'], 2, '.', ',').'</td>
            <td align="right">'.number_format($row['totalcharge'], 2, '.', ',').'</td>
            <td align="right">'.number_format($row['paid'], 2, '.', ',').'</td>
            <td align="right">'.number_format($row['paid_balance'], 2, '.', ',').'</td>
        </tr>';
    }
    ?>
</tbody>
</table>
