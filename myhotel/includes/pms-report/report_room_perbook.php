<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous"> -->

<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    });
</script>
<section class="content-header">
    <h1>
       	Report PMS - Rooms On Book Report</h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-table"></i> Report PMS</a></li>
    </ol>
</section>
<section class="content">
    


<div class="row">
<div class="box box-form"><div class="box-body" style="font-size: 12px;">

<div class="form-group">
<label>Between:</label> &nbsp;&nbsp;
<input type='text' class="form-control" value="2017-10-01"/>
<label>And:</label> &nbsp;&nbsp;
<input type='text' class="form-control" value="2017-10-31"/>
</div>
        </div></div>
</div>




    <div class="row">
        <div class="box box-form"><div class="box-body" style="font-size: 12px;">
        
            <table id="example" class="display table promo-table" cellspacing="0" width="100%">
            <thead>
              <tr class="table-head">
                <td><strong>Impact</strong></td>
                <td><strong>Revenue</strong></td>
                <td><strong>Room Nights</strong></td>
                <td><strong>AGR</strong></td>
                <td colspan="9"><strong>RPD For Last 10 Days</strong></td>
                <td><strong>Nights</strong></td>
                <td><strong>RPD</strong></td>
                <td><strong>%</strong></td>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><strong>Reservation</strong></td>
                <td>516910</td>
                <td>1</td>
                <td>516910</td>
                <td>Mon</td>
                <td>02-Oct</td>
                <td>4</td>
                <td>11%</td>
                <td>Sat</td>
                <td>07-Oct</td>
                <td>2</td>
                <td>14%</td>
                <td>Next 10 days</td>
                <td>18</td>
                <td>2</td>
                <td>13%</td>
              </tr>
              <tr>
                <td><strong>Cancellations</strong></td>
                <td>-461601</td>
                <td>-1</td>
                <td>461601</td>
                <td>Tue</td>
                <td>03-Oct</td>
                <td>4</td>
                <td>29%</td>
                <td>Sun</td>
                <td>08-Oct</td>
                <td>0</td>
                <td>0%</td>
                <td>Next 10 - 20 days</td>
                <td>0</td>
                <td>0</td>
                <td>0%</td>
              </tr>
              <tr>
                <td><strong>Amendments</strong></td>
                <td>9304380</td>
                <td>9</td>
                <td>1033820</td>
                <td>Wed</td>
                <td>04-Oct</td>
                <td>4</td>
                <td>29%</td>
                <td>Mon</td>
                <td>09-Oct</td>
                <td>1</td>
                <td>7%</td>
                <td>Next 21 - 30 days</td>
                <td>0</td>
                <td>0</td>
                <td>0%</td>
              </tr>
              <tr>
                <td><strong>No shows</strong></td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>Thu</td>
                <td>05-Oct</td>
                <td>4</td>
                <td>29%</td>
                <td>Tue</td>
                <td>10-Oct</td>
                <td>1</td>
                <td>7%</td>
                <td>Next 30 days</td>
                <td>18</td>
                <td>1</td>
                <td>4%</td>
              </tr>
              <tr>
                <td><strong>Net</strong></td>
                <td>9359689</td>
                <td>9</td>
                <td>1039965</td>
                <td>Fri</td>
                <td>06-Oct</td>
                <td>2</td>
                <td>14%</td>
                <td>Wed</td>
                <td>11-Oct</td>
                <td>3</td>
                <td>21%</td>
                <td>Next 31 to 60 days</td>
                <td>0</td>
                <td>0</td>
                <td>0%</td>
              </tr>
            </tbody>
            </table>
            <BR /><BR />
            <table id="example" class="display table promo-table" cellspacing="0" width="100%">
            <thead>
              <tr class="table-head">
                <td><strong>Day</strong></td>
                <td><strong>Date</strong></td>
                <td><strong>Room In House</strong></td>
                <td><strong>Arrivals</strong></td>
                <td><strong>Departures</strong></td>
                <td><strong>Groups</strong></td>
                <td><strong>ROB</strong></td>
                <td><strong>ARR</strong></td>
                <td><strong>Day</strong></td>
                <td><strong>Date</strong></td>
                <td><strong>Room In House</strong></td>
                <td><strong>Arrivals</strong></td>
                <td><strong>Departures</strong></td>
                <td><strong>Groups</strong></td>
                <td><strong>ROB</strong></td>
                <td><strong>ARR</strong></td>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><strong>Thu</strong></td>
                <td><strong>12-Oct</strong></td>
                <td>3</td>
                <td>0</td>
                <td>0</td>
                <td>1</td>
                <td>3</td>
                <td>1033820</td>
                <td><strong>Fri</strong></td>
                <td><strong>27-Oct</strong></td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
              </tr>
              <tr>
                <td><strong>Fri</strong></td>
                <td><strong>13-Oct</strong></td>
                <td>3</td>
                <td>0</td>
                <td>0</td>
                <td>1</td>
                <td>3</td>
                <td>1033820</td>
                <td><strong>Sat</strong></td>
                <td><strong>28-Oct</strong></td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
              </tr>
              <tr>
                <td><strong>Sat</strong></td>
                <td><strong>14-Oct</strong></td>
                <td>3</td>
                <td>0</td>
                <td>0</td>
                <td>1</td>
                <td>3</td>
                <td>1033820</td>
                <td><strong>Sun</strong></td>
                <td><strong>29-Oct</strong></td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
              </tr>
              <tr>
                <td><strong>Sun</strong></td>
                <td><strong>15-Oct</strong></td>
                <td>3</td>
                <td>0</td>
                <td>0</td>
                <td>1</td>
                <td>3</td>
                <td>1033820</td>
                <td><strong>Mon</strong></td>
                <td><strong>30-Oct</strong></td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
              </tr>
              <tr>
                <td><strong>Mon</strong></td>
                <td><strong>16-Oct</strong></td>
                <td>3</td>
                <td>0</td>
                <td>0</td>
                <td>1</td>
                <td>3</td>
                <td>1033820</td>
                <td><strong>Tue</strong></td>
                <td><strong>31-Oct</strong></td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
              </tr>
              <tr>
                <td><strong>Tue</strong></td>
                <td><strong>17-Oct</strong></td>
                <td>3</td>
                <td>0</td>
                <td>0</td>
                <td>1</td>
                <td>3</td>
                <td>1033820</td>
                <td><strong>Wed</strong></td>
                <td><strong>01-Nov</strong></td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
              </tr>
              <tr>
                <td><strong>Wed</strong></td>
                <td><strong>18-Oct</strong></td>
                <td>3</td>
                <td>0</td>
                <td>3</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td><strong>Thu</strong></td>
                <td><strong>02-Nov</strong></td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
              </tr>
              <tr>
                <td><strong>Thu</strong></td>
                <td><strong>19-Oct</strong></td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td><strong>Fri</strong></td>
                <td><strong>03-Nov</strong></td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
              </tr>
              <tr>
                <td><strong>Fri</strong></td>
                <td><strong>20-Oct</strong></td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td><strong>Sat</strong></td>
                <td><strong>04-Nov</strong></td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
              </tr>
              <tr>
                <td><strong>Sat</strong></td>
                <td><strong>21-Oct</strong></td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td><strong>Sun</strong></td>
                <td><strong>05-Nov</strong></td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
              </tr>
              <tr>
                <td><strong>Sun</strong></td>
                <td><strong>22-Oct</strong></td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td><strong>Mon</strong></td>
                <td><strong>06-Nov</strong></td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
              </tr>
              <tr>
                <td><strong>Mon</strong></td>
                <td><strong>23-Oct</strong></td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td><strong>Tue</strong></td>
                <td><strong>07-Nov</strong></td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
              </tr>
              <tr>
                <td><strong>Tue</strong></td>
                <td><strong>24-Oct</strong></td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td><strong>Wed</strong></td>
                <td><strong>08-Nov</strong></td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
              </tr>
              <tr>
                <td><strong>Wed</strong></td>
                <td><strong>25-Oct</strong></td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td><strong>Thu</strong></td>
                <td><strong>09-Nov</strong></td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
              </tr>
              <tr>
                <td><strong>Thu</strong></td>
                <td><strong>26-Oct</strong></td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td><strong>Fri</strong></td>
                <td><strong>10-Nov</strong></td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
              </tr>
            </tbody>
          </table>
            <BR /><BR />
            <table id="example" class="display table promo-table" cellspacing="0" width="100%">
            <thead>
              <tr>
                <td colspan="13"><strong> Amendments made on &nbsp; &nbsp;11/10/2017</strong></td>
              </tr>
              <tr class="table-head">
                <td><strong>No</strong></td>
                <td><strong>Reason</strong></td>
                <td><strong>Room</strong></td>
                <td><strong>Name</strong></td>
                <td><strong>Original    C/I</strong></td>
                <td><strong>Amended    C/I</strong></td>
                <td><strong>Original    C/O</strong></td>
                <td><strong>Amended    C/O</strong></td>
                <td><strong>Room(s)</strong></td>
                <td><strong>Night(s)</strong></td>
                <td><strong>Rate</strong></td>
                <td><strong>Revenue</strong></td>
                <td><strong>Remarks</strong></td>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>1</td>
                <td>C/I date amended</td>
                <td>131</td>
                <td>Kelly Bullock</td>
                <td>14-10-17</td>
                <td>11-10-17</td>
                <td>18-10-17</td>
                <td>18-10-17</td>
                <td>1</td>
                <td>3</td>
                <td>1033820</td>
                <td>3101460</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>2</td>
                <td>C/I date amended</td>
                <td>132</td>
                <td>Kelly Bullock</td>
                <td>14-10-17</td>
                <td>11-10-17</td>
                <td>18-10-17</td>
                <td>18-10-17</td>
                <td>1</td>
                <td>3</td>
                <td>1033820</td>
                <td>3101460</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>3</td>
                <td>C/I date amended</td>
                <td>135</td>
                <td>Kelly Bullock</td>
                <td>14-10-17</td>
                <td>11-10-17</td>
                <td>18-10-17</td>
                <td>18-10-17</td>
                <td>1</td>
                <td>3</td>
                <td>1033820</td>
                <td>3101460</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td colspan="8"><strong>LOS</strong></td>
                <td><strong>3</strong></td>
                <td><strong>9</strong></td>
                <td><strong>1033820</strong></td>
                <td><strong>9304380</strong></td>
                <td>&nbsp;</td>
              </tr>
            </tbody>
          </table>
            <BR /><BR />
            <table id="example" class="display table promo-table" cellspacing="0" width="100%">
            <thead>
              <tr>
                <td colspan="11"><strong>Cancellations made on &nbsp; &nbsp;11/10/2017</strong></td>
              </tr>
              <tr class="table-head">
                <td><strong>No</strong></td>
                <td><strong>Reason</strong></td>
                <td><strong>Time</strong></td>
                <td><strong>Name</strong></td>
                <td><strong>Check-In</strong></td>
                <td><strong>Check-Out</strong></td>
                <td><strong>Room(s)</strong></td>
                <td><strong>Night(s)</strong></td>
                <td><strong>Rate</strong></td>
                <td><strong>Revenue</strong></td>
                <td><strong>Remarks</strong></td>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>1</td>
                <td>Natural Disaster</td>
                <td>22:08:40</td>
                <td>Rinrin Defi</td>
                <td>13-10-17</td>
                <td>14-10-17</td>
                <td>1</td>
                <td>1</td>
                <td>461601</td>
                <td>461601</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td colspan="6"><strong>LOS</strong></td>
                <td><strong>1</strong></td>
                <td><strong>1</strong></td>
                <td><strong>461601</strong></td>
                <td><strong>461601</strong></td>
                <td>&nbsp;</td>
              </tr>
            </tbody>
          </table> <BR /><BR />
            <table width="100%" cellspacing="0" class="display table promo-table" id="example2">
              <thead>
                <tr>
                  <td colspan="12"><strong>No show(s) on &nbsp; &nbsp;11/10/2017</strong></td>
                </tr>
                <tr class="table-head">
                  <td><strong>No</strong></td>
                  <td><strong>Name</strong></td>
                  <td><strong>Room</strong></td>
                  <td><strong>Check-In</strong></td>
                  <td><strong>Check-Out</strong></td>
                  <td><strong>Room(s)</strong></td>
                  <td><strong>Night(s)</strong></td>
                  <td><strong>Rate</strong></td>
                  <td><strong>Revenue</strong></td>
                  <td><strong>Booked    By</strong></td>
                  <td><strong>Billing</strong></td>
                  <td><strong>Remarks</strong></td>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td colspan="12">No Result    found.</td>
                </tr>
              
              </tbody>
            </table><BR /><BR />
            <table width="100%" cellspacing="0" class="display table promo-table" id="example3">
              <thead>
                <tr>
                  <td colspan="12"><strong>Reservations on &nbsp; &nbsp;11/10/2017</strong></td>
                </tr>
                <tr class="table-head">
                  <td><strong>No</strong></td>
                  <td><strong>Name</strong></td>
                  <td><strong>Room</strong></td>
                  <td><strong>Check-In</strong></td>
                  <td><strong>Check-Out</strong></td>
                  <td><strong>Room(s)</strong></td>
                  <td><strong>Night(s)</strong></td>
                  <td><strong>Rate</strong></td>
                  <td><strong>Revenue</strong></td>
                  <td><strong>Booked    By</strong></td>
                  <td><strong>Billing</strong></td>
                  <td><strong>Remarks</strong></td>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td colspan="12">No Result    found.</td>
                </tr>
              
              </tbody>
            </table><Br /><BR />
            <table width="100%" cellspacing="0" class="display table promo-table" id="example4">
              <thead>
                <tr>
                  <td colspan="12"><strong>Tentative on &nbsp; &nbsp;11/10/2017</strong></td>
                </tr>
                <tr class="table-head">
                  <td><strong>No</strong></td>
                  <td><strong>Name</strong></td>
                  <td><strong>Room</strong></td>
                  <td><strong>Check-In</strong></td>
                  <td><strong>Check-Out</strong></td>
                  <td><strong>Room(s)</strong></td>
                  <td><strong>Night(s)</strong></td>
                  <td><strong>Rate</strong></td>
                  <td><strong>Revenue</strong></td>
                  <td><strong>Booked    By</strong></td>
                  <td><strong>Billing</strong></td>
                  <td><strong>Remarks</strong></td>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td colspan="12">No Result    found.</td>
                </tr>
              
              </tbody>
            </table>
            <p>&nbsp;</p>
        </div></div>
    </div>
</section>