<?php
    include("function_report.php");
?>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
<script>
    $(document).ready(function() {
        //$('#xload').css('display', 'none');
        
        $('#btfilter').click(function(){
            //$('#xload').css('display', '');
            
            $.post('<?php echo $base_url; ?>/includes/pms-report/ajax/report_rsvlist.php', $("#formFilter").serialize(), function(response){
                $('#loaddata').html(response);
                
                var table = $('#example').DataTable( {
                    dom: 'B lfrtip',
                    columnDefs: [
                        {
                            targets: 1,
                            className: 'noVis'
                        }
                    ],
                    buttons: [
                        {
                            extend: 'colvis', text: 'Column Visibility',
                            columns: ':not(.noVis)'
                        }
                    ]
                } );
                
                $('#example_filter input').unbind();
                $('#example_filter input').bind('keyup', function(e) {
                    if(e.keyCode == 13) {
                        table.search(this.value).draw();
                    }
                });   
                
                //$('#xload').css('display', 'none');
            });
        });
        $('#btfilter').trigger('click');
        
        $( "#startdate1" ).datepicker({
    		defaultDate: "0",
    		dateFormat: "dd M y",
    		changeMonth: true, changeYear:true,
    		onClose: function( selectedDate ) {
                $( "#enddate1" ).datepicker( "option", "minDate", selectedDate );
    		},
    		onSelect: function(dateText) {}
        });
        $( "#enddate1" ).datepicker({
          	defaultDate: "0",
    		dateFormat: "dd M y",
          	changeMonth: true, changeYear:true,
         	onClose: function( selectedDate ) {
            	$( "#startdate1" ).datepicker( "option", "maxDate", selectedDate );
          	},
    		onSelect: function(dateText) {}
        });
        
        $( "#startdate2" ).datepicker({
    		defaultDate: "0",
    		dateFormat: "dd M y",
    		changeMonth: true, changeYear:true,
    		onClose: function( selectedDate ) {
                $( "#enddate2" ).datepicker( "option", "minDate", selectedDate );
    		},
    		onSelect: function(dateText) {}
        });
        $( "#enddate2" ).datepicker({
          	defaultDate: "0",
    		dateFormat: "dd M y",
          	changeMonth: true, changeYear:true,
         	onClose: function( selectedDate ) {
            	$( "#startdate2" ).datepicker( "option", "maxDate", selectedDate );
          	},
    		onSelect: function(dateText) {}
        });
        
        $(document).on('click', 'a.print', function() {
            $(this).parent().children('form.reservation-card').submit();
        });
        $(document).on('click', 'a.detail', function() { 
            $(this).parent().parent().find('form.view-reservation-detail').submit();
        });
        $(document).on('click', '#btexport', function(){
            $('#exporttoexcel').submit();
        });
        $('input[name="bdall"]').change(function(){
            var n = $(this).val();
            if(n == '1'){
                $('#bdall').css('display', 'inline-block');
            }else{
                $('#bdall').css('display', 'none');
            }
        });
        $('input[name="ciall"]').change(function(){
            var n = $(this).val();
            if(n == '1'){
                $('#ciall').css('display', 'inline-block');
            }else{
                $('#ciall').css('display', 'none');
            }
        });
    });
</script>
<style>
    .content .form-group label {
        padding-right:10px;
    }
    .content .form-group label, .content .form-group select, .content .form-group input {
        margin-bottom:5px;
    }
    a.detail {
        color: #1200ff;
    }
    a.detail:hover {
        text-decoration: underline;
    }
    .btn {
        border-radius: 3px;
        -webkit-box-shadow: none;
        box-shadow: none;
        border: 1px solid transparent;
    }
    .btn-primary {
        color: #fff;
        background-color: #337ab7;
        border-color: #2e6da4;
    }
    .btn-primary {
        background-color: #3c8dbc;
        border-color: #367fa9;
    }
</style>
<section class="content-header">
    <h1>
       	Report PMS - Reservation List
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Report PMS</a></li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="box box-form">
            <form id="formFilter" method="POST">
            <input type="hidden" name="hoteloid" value="<?=$_SESSION['_hotel']?>">
            <div class="box-body" style="font-size:12px;">
                <div class="form-group">
                    <label>Filter:</label>
                    <select name="rt" id="selrt" class="form-control" style="max-width:200px">
                        <option value="">All Room Type</option>
                        <?php
                        if(!empty($hoteloid)){
                            $data = getHotelRoom($db, $hoteloid);
                            foreach($data as $row){
                                echo '<option value="'.$row['roomoid'].'">'.$row['name'].'</option>';
                            }
                        }else{
                            $s_chain = "SELECT ua.oid, ua.type FROM users u INNER JOIN userassign ua USING (useroid) WHERE ua.type IN ('chainoid', 'hoteloid') AND u.useroid = '".$_SESSION['_oid']."'";
                    		$q_chain = $db->query($s_chain);
                    		$r_chain = $q_chain->fetchAll(PDO::FETCH_ASSOC);
                    		$rxoid = array(); $rxtype = array();
                    		foreach($r_chain as $row){
                    			$rxoid[] = $row['oid'];
                    			$rxtype[] = $row['type'];
                    		}
                    		
                    		$rxchain = array(); $rxhotel = array();
                            foreach($rxtype as $k => $v){
                                if($v == 'chainoid'){
                                    $rxchain[] = $rxoid[$k];
                                }else if($v == 'hoteloid'){
                                    $rxhotel[] = $rxoid[$k];
                                }
                            }
                            
                            $msg = "";
                            if(count($rxchain) > 0){
                                $imp = implode(",", $rxchain);
                                $msg .= "AND chainoid IN (".$imp.")";
                            }
                            if(count($rxhotel) > 0){
                                $imp = implode(",", $rxhotel);
                                $msg .= "AND hoteloid IN (".$imp.")";
                            }
                    		
                    		if($msg != ""){
                                $stmt = $db->prepare("SELECT hoteloid, hotelcode, hotelname FROM hotel WHERE publishedoid='1' ".$msg." ORDER BY hotelname");
            	                $stmt->execute(array());
                                $data1 = $stmt->fetchAll(PDO::FETCH_ASSOC);
                    		}else{
                    		    $data1 = array();
                    		}
                    		
                    		foreach($data1 as $dt){
                    		    $data = getHotelRoom($db, $dt['hoteloid']);
                    		    echo '<optgroup label="'.$dt['hotelname'].'">';
                                foreach($data as $row){
                                    echo '<option value="'.$row['roomoid'].'">'.$row['name'].'</option>';
                                }
                    		}
                        }
                        ?>
                    </select>
                    <select name="status" id="selstat" class="form-control">
                        <option value="">All Status</option>
                        <?php
                            $data = getPMSStatus($db, array('0'));
                            foreach($data as $row){
                                echo '<option value="'.$row['pmsstatusoid'].'">'.$row['status'].'</option>';
                            }
                        ?>
                    </select>
                    <select name="market" id="selmark" class="form-control">
                        <option value="">All Market</option>
                        <?php
                            $data = getBookingMarket($db);
                            foreach($data as $row){
                                echo '<option value="'.$row['bookingmarketoid'].'">'.$row['bm_name'].'</option>';
                            }
                        ?>
                    </select>
                    <select name="agent" id="selagent" class="form-control" style="max-width:110px;">
                        <option value="">All Agent</option>
                        <?php
                            if($hoteloid > 0){
                                $data = getAgent($db, $hoteloid);
                            }else{
                                if($_SESSION['_typeusr'] == "4"){
                            		$s_chain = "SELECT ua.oid FROM users u INNER JOIN userassign ua USING (useroid) WHERE ua.type = 'chainoid' AND u.useroid = '".$_SESSION['_oid']."'";
                            		$q_chain = $db->query($s_chain);
                            		$r_chain = $q_chain->fetchAll(PDO::FETCH_ASSOC);
                            		foreach($r_chain as $row){
                            			$chainoid = $row['oid'];
                            		}
                            		$data = getAgentChain($db, $chainoid); 
                            	}else{
                                    $data = array();
                            	}
                            }
                            foreach($data as $row){
                                echo '<option value="'.$row['agenttypeoid'].'">'.$row['typename'].'</option>';
                            }
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label>Reservation Date:</label>
                    <input type="radio" name="bdall" value="0" checked>All &nbsp;
                    <input type="radio" name="bdall" value="1">Range &nbsp;
                    <div id="bdall" style="display:none;">
                        <label>from:</label>
                        <input type="text" class="form-control" name="bdstartdate" id="startdate1" readonly="readonly" value="<?=date("d M y")?>" style="width:75px">
                        <label>to:</label>
                        <input type="text" class="form-control" name="bdenddate" id="enddate1" readonly="readonly" value="<?=date("d M y", strtotime("+1 Month"))?>" style="width:75px">
                    </div>
                </div>
                <div class="form-group">
                    <label>Check-in Date:</label>
                    <input type="radio" name="ciall" value="0">All &nbsp;
                    <input type="radio" name="ciall" value="1" checked>Range &nbsp;
                    <div id="ciall" style="display:inline-block;">
                        <label>from:</label>
                        <input type="text" class="form-control" name="cistartdate" id="startdate2" readonly="readonly" value="<?=date("d M y")?>" style="width:75px">
                        <label>to:</label>
                        <input type="text" class="form-control" name="cienddate" id="enddate2" readonly="readonly" value="<?=date("d M y", strtotime("+1 Week"))?>" style="width:75px">
                    </div>
                     &nbsp; 
                    <button type="button" class="btn btn-primary" id="btfilter">FILTER</button>
                </div>
            </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="box box-form">
            <div class="box-body" style="font-size: 12px;">
                <!--<div id="xload" align="center"><img src="../images/loading.gif" style="width:10%"></div>-->
                
                <div id="loaddata"></div>
            </div>
        </div>
    </div>
</section>