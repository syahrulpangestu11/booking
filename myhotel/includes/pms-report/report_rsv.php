<?php
	include("includes/paging/pre-paging.php"); 
?>
<style>
    .label{display:inline;padding:.2em .6em .3em;font-size:75%;font-weight:700;line-height:1;color:#fff;text-align:center;white-space:nowrap;vertical-align:baseline;border-radius:.25em}
    .label-primary{background-color:#5d9cec}
    .label-success{background-color:#27c24c}
    .label-info{background-color:#23b7e5}
    .label-danger{background-color:#f05050}
    .label-warning{background-color:#ff902b}
    .gotodetail{cursor:pointer;}
    table#dataTable tr:nth-child(n+2):hover td{ cursor:pointer; background-color:#feffeb;  }
</style>
<script>
    $(function(){
        $(document).on('click',".gotodetail",function(){
            var x = $(this).attr("bn");
            window.open("<?=$base_url?>/pms-lite/view-detail-reservation/?bookingnumber="+x);
        });
        $('body').on('click','#paging button', function(e) {
    		page	= $(this).attr('page');
    		location.href = "<?=$base_url?>/pms-report/rsv/?page="+page;
    	});
    });
</script>
<section class="content-header">
    <h1>
       	Report PMS - Reservation
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Report PMS</a></li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="box box-form">
            <div class="box-body" style="font-size: 12px;">
                <div class="form-group">
                    <label>Filter:</label>
                    &nbsp;&nbsp;
                    <select name="status" class="form-control">
                        <option value="volvo">All Status</option>
                        <option value="saab">Check In</option>
                        <option value="fiat">Check Out</option>
                        <option value="audi">Reserve</option>
                        <option value="volvo">Temporary Reserve</option>
                        <option value="volvo">Guest In-House</option>
                    </select>
                    <select name="market" class="form-control">
                        <option value="volvo">All Market</option>
                        <option value="volvo">Direct website</option>
                        <option value="saab">direct booking</option>
                        <option value="fiat">Travel agent</option>
                        <option value="audi">Corporate</option>
                        <option value="volvo">Online Travel Agent (OTA)</option>
                        <option value="saab">Vacation Rental</option>
                        <option value="fiat">Wedding & event</option>
                        <option value="audi">Affiliate</option>
                    </select>
                    <label>Between:</label> &nbsp;&nbsp;
                    <input type='text' class="form-control" value="2017-10-01"/>
                    <label>And:</label> &nbsp;&nbsp;
                    <input type='text' class="form-control" value="2017-10-31"/>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="box box-form">
            <div class="box-body" style="font-size: 12px;">
            <table id="dataTable" class="table promo-table">
            <thead>
                <tr>
                   <th>#</th>
                   <th>Res#</th>
                   <th>Status</th>
                   <th>Guest Name</th>
                   <th>Email</th>
                   <th>Phone</th>
                   <th>Room Type</th>
                   <th>Room</th>
                   <th>Check In</th>
                   <th>Check Out</th>
                   <th>R/N</th>                                   
                   <th>Logding</th>
                   <th>Logding Tax</th>
                   <th>Other Charges</th>
                   <th>Other Tax</th>
                   <th>Paid</th>
                   <th>Balance</th>
                   <th>Country</th>
                   <th>Source</th>
                </tr>
             </thead>
             <tbody>
            <?php
                try {
                    $stmt = $db->query("SELECT bookingoid, bookingnumber, title, firstname, lastname, phone, mobilephone, email, 
                            checkin, checkout, DATEDIFF(checkout, checkin) AS diff, br.room, currencycode, b.grandtotal, b.bookingtime, rn.roomnumber, 
                            b.bm_name, bm.bm_name as bm_name1, countryname, pm.status, pm.`pmsstatusoid`
                        FROM `booking` b
                            LEFT JOIN `bookingroom` br USING(`bookingoid`)
                            LEFT JOIN `bookingroomdtl` brd USING(`bookingroomoid`)
                            INNER JOIN `customer` c USING(`custoid`)
                            LEFT JOIN `currency` cr ON cr.`currencyoid` = b.`currencyoid`
                            LEFT JOIN `roomoffer` ro ON ro.`roomofferoid` = br.`roomofferoid`
                            LEFT JOIN `room` r ON r.`roomoid` = ro.`roomoid`
                            LEFT JOIN `roomnumber` rn ON brd.`roomnumberoid` = rn.`roomnumberoid`
                            LEFT JOIN `bookingmarket` bm ON bm.`bookingmarketoid` = b.`bookingmarketoid`
                            LEFT JOIN `country` cn ON cn.`countryoid` = c.`countryoid`
                            LEFT JOIN `pmsstatus` pm ON pm.`pmsstatusoid` = b.`pmsstatus`
                        WHERE `bookingstatusoid` in (4) AND b.hoteloid='".$hoteloid."'
                        GROUP BY `bookingoid` DESC".$paging_query);
                    $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC); $n=0;
                    foreach($r_room as $row){
                        $title = ($row['title']=='1')?"Mrs.":($row['title']=='2')?"Miss":"Mr.";
                        $phone = !empty($row['phone'])?$row['phone']:$row['mobilephone'];
                        $amount = number_format($row['grandtotal'], 0, '.', ',');
                        $roomnumber = empty($row['roomnumber'])?"-":$row['roomnumber'];
                        $bm_name = empty($row['bm_name'])?(empty($row['bm_name1'])?"-":$row['bm_name1']):$row['bm_name'];
                        $labelarr = array("label-danger");
                        ?>
                        <tr class="gradeX gotodetail" bn="<?=$row['bookingnumber']?>">
                            <td><?=++$n?></td>
                            <td><?=$row['bookingnumber']?></td>
                            <td><span class="label <?=(!isset($labelarr[$row['pmsstatusoid']])?"label-primary":$labelarr[$row['pmsstatusoid']]);?>"><?=$row['status']?></span></td>
                            <td><span style="width:80px;word-wrap:break-word;display:inline-block;"><?=$title." ".$row['firstname']." ".$row['lastname']?></span></td>
                            <td><span style="width:80px;word-wrap:break-word;display:inline-block;"><?=$row['email']?></span></td>
                            <td><?=$phone?></td>
                            <td style="width:100px;word-wrap:break-word;display:inline-block;"><?=$row['room']?></td>
                            <td><?=$roomnumber?></td>
                            <td><?=date('d M Y', strtotime($row['checkin']))?></td>
                            <td><?=date('d M Y', strtotime($row['checkout']))?></td>
                            <td><?=$row['diff']?></td>
                            <td><?=$amount?></td>
                            <td>-</td>
                            <td>100.000</td>
                            <td>-</td>
                            <td>110.000</td>
                            <td>4.406.800</td>
                            <td><?=$row['countryname']?></td>
                            <td><?=$bm_name?></td>
                        </tr>
                        <?php
                    }
                }catch(PDOException $ex) {
                    echo "Invalid Query";
                    die();
                }
            
            ?>
            </tbody>
            </table>
            <?php
            	// --- PAGINATION part 2 ---
            	$main_query = "SELECT count(distinct(bookingoid)) as jmldata
                        FROM `booking` b
                            LEFT JOIN `bookingroom` br USING(`bookingoid`)
                            LEFT JOIN `bookingroomdtl` brd USING(`bookingroomoid`)
                            INNER JOIN `customer` c USING(`custoid`)
                            LEFT JOIN `currency` cr ON cr.`currencyoid` = b.`currencyoid`
                            LEFT JOIN `roomoffer` ro ON ro.`roomofferoid` = br.`roomofferoid`
                            LEFT JOIN `room` r ON r.`roomoid` = ro.`roomoid`
                            LEFT JOIN `roomnumber` rn ON brd.`roomnumberoid` = rn.`roomnumberoid`
                            LEFT JOIN `bookingmarket` bm ON bm.`bookingmarketoid` = b.`bookingmarketoid`
                            LEFT JOIN `country` cn ON cn.`countryoid` = c.`countryoid`
                            LEFT JOIN `pmsstatus` pm ON pm.`pmsstatusoid` = b.`pmsstatus`
                        WHERE `bookingstatusoid` in (4) AND b.hoteloid='".$hoteloid."'";
                $query = $main_query;
            	$stmt = $db->query($query);
            	$row = $stmt->fetch(PDO::FETCH_ASSOC);
            	$jmldata = $row['jmldata'];
            
            	$tampildata = $row_count;
            	include("includes/paging/post-paging.php");
            ?>
            </div>
        </div>
    </div>
</section>