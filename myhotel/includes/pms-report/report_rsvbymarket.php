<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    });
</script>
<section class="content-header">
    <h1>
       	Report PMS - Reservation by Market
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Report PMS</a></li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="box box-form"><div class="box-body" style="overflow-x: scroll;font-size: 12px;">
            <table id="example" class="display" cellspacing="0" width="100%">
            <thead>
                <tr>
                   <th>#</th>
                   <th>Res#</th>
                   <th>Guest Name</th>
                   <th>Room Type</th>
                   <th>Room</th>
                   <th>Created On</th>
                   <th>Check In</th>
                   <th>Check Out</th>
                   <th>Night(s)</th>
                   <th>Nation</th>
                   <th>Source</th>
                   <th>Amount(Incl. Tax)</th>
                </tr>
             </thead>
             <tbody>
                <tr class="gradeX">
                   <td>1</td>
                   <td>179</td>
                   <td>Ika Dewi</td>
                   <td>Superior</td>
                   <td>103</td>
                   <td>3 Sept 2017</td>
                   <td>3 Sept 2017</td>
                   <td>6 Sept 2017</td>
                   <td>3</td>
                   <td>Indonesian</td>
                   <td>Direct Website</td>
                   <td>3.509.800</td>
                </tr>
                <tr class="gradeX">
                   <td>2</td>
                   <td>182</td>
                   <td>Adi Nugraha</td>
                   <td>Superior</td>
                   <td>104</td>
                   <td>3 Sept 2017</td>
                   <td>4 Sept 2017</td>
                   <td>9 Sept 2017</td>
                   <td>4</td>
                   <td>Australia</td>
                   <td>Direct Website</td>
                   <td>4.406.800</td>
                </tr>
                 <tr class="gradeX">
                   <td>3</td>
                   <td>189</td>
                   <td>Adhe Setya</td>
                   <td>Deluxe</td>
                   <td>203</td>
                   <td>4 Sept 2017</td>
                   <td>7 Sept 2017</td>
                   <td>8 Sept 2017</td>
                   <td>1</td>
                   <td>Indonesian</td>
                   <td>Direct Booking</td>
                   <td>1.509.800</td>
                </tr>
                 <tr class="gradeX">
                   <td>4</td>
                   <td>172</td>
                   <td>Adi Nugraha</td>
                   <td>Deluxe</td>
                   <td>201</td>
                   <td>1 Sept 2017</td>
                   <td>16 Sept 2017</td>
                   <td>17 Sept 2017</td>
                   <td>1</td>
                   <td>Indonesian</td>
                   <td>Travel Agent</td>
                   <td>1.509.800</td>
                </tr>
                 <tr class="gradeX">
                   <td>1</td>
                   <td>139</td>
                   <td>Vina Guntary</td>
                   <td>Superior</td>
                   <td>101</td>
                   <td>1 Sept 2017</td>
                   <td>3 Sept 2017</td>
                   <td>10 Sept 2017</td>
                   <td>7</td>
                   <td>Indonesian</td>
                   <td>Vacation Rental</td>
                   <td>5.509.800</td>
                </tr>  
                   <tr class="gradeX">
                   <td>1</td>
                   <td>139</td>
                   <td>Vina Guntary</td>
                   <td>Superior</td>
                   <td>102</td>
                   <td>1 Sept 2017</td>
                   <td>3 Sept 2017</td>
                   <td>10 Sept 2017</td>
                   <td>7</td>
                   <td>Indonesian</td>
                   <td>Vacation Rental</td>
                   <td>5.509.800</td>
                </tr>
             </tbody>
            </table>
        </div></div>
    </div>
</section>