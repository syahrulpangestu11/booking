<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous"> -->

<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    });
</script>
<section class="content-header">
    <h1>
       	Report PMS - Flash Manager Report
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Report PMS</a></li>
    </ol>
</section>
<section class="content">
    


<div class="row">
<div class="box box-form"><div class="box-body" style="font-size: 12px;">

<div class="form-group">
    <!-- Select Room Type:   
    <input type="checkbox" id="myChec1">Superior    
    <input type="checkbox" id="myCheck">Deluxe 
    <input type="checkbox" id="myCheck">Suites
    &nbsp;&nbsp;&nbsp;&nbsp;
    <select name="status" class="form-control">
        <option value="volvo">All Status</option>
        <option value="saab">Check In</option>
        <option value="fiat">Check Out</option>
        <option value="audi">Reserve</option>
        <option value="volvo">Temporary Reserve</option>
    </select>
        
    <select name="market" class="form-control">
        <option value="volvo">All Market</option>
        <option value="volvo">Direct website</option>
        <option value="saab">direct booking</option>
        <option value="fiat">Travel agent</option>
        <option value="audi">Corporate</option>
        <option value="volvo">Online Travel Agent (OTA)</option>
        <option value="saab">Vacation Rental</option>
        <option value="fiat">Wedding & event</option>
        <option value="audi">Affiliate</option>
    </select>
    
    <label>Between:</label> &nbsp;&nbsp;
    <input type='text' class="form-control" value="2017-10-01"/>
    <label>And:</label> &nbsp;&nbsp;
    <input type='text' class="form-control" value="2017-10-31"/> -->
    <label>Date:</label> &nbsp;&nbsp;
    <input type='text' class="form-control" value="2017-10-11"/>
</div>
        </div></div>
</div>




    <div class="row">
        <div class="box box-form"><div class="box-body" style="font-size: 12px;">
            <table id="example" class="display table promo-table" cellspacing="0" width="100%">
            <thead>
            <tr>
               <th>Description</th>
               <td>2017</td>
               <td>2017</td>
               <td>2017</td>
               <td>2016</td>
               <td>2016</td>
               <td>2016</td>
              </tr>
         </thead>
         <tbody>
            <tr>
              <td>&nbsp;</td>
              <td>DAY</td>
              <td>MONTH</td>
              <td>YEAR</td>
              <td>DAY</td>
              <td>MONTH</td>
              <td>YEAR</td>
            </tr>
            <tr>
              <td>Total Rooms in Hotel (Incl. DNR)</td>
              <td align="right">14</td>
              <td align="right">196</td>
              <td align="right">9751</td>
              <td align="right">35</td>
              <td align="right">385</td>
              <td align="right">9975</td>
            </tr>
            <tr>
              <td>Rooms Occupied</td>
              <td align="right">3</td>
              <td align="right">29</td>
              <td align="right">35</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
            </tr>
            <tr>
              <td>Total Rooms in Hotel minus OOO Rooms</td>
              <td align="right">14</td>
              <td align="right">196</td>
              <td align="right">9751</td>
              <td align="right">35</td>
              <td align="right">385</td>
              <td align="right">9975</td>
            </tr>
            <tr>
              <td>Available Rooms</td>
              <td align="right">11</td>
              <td align="right">167</td>
              <td align="right">9716</td>
              <td align="right">35</td>
              <td align="right">385</td>
              <td align="right">9975</td>
            </tr>
            <tr>
              <td>Available Rooms minus OOO Rooms</td>
              <td align="right">11</td>
              <td align="right">167</td>
              <td align="right">9716</td>
              <td align="right">35</td>
              <td align="right">385</td>
              <td align="right">9975</td>
            </tr>
            <tr>
              <td>Complimentary Rooms</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
            </tr>
            <tr>
              <td>Total Rooms Use in Hotel</td>
              <td align="right">14</td>
              <td align="right">196</td>
              <td align="right">9751</td>
              <td align="right">35</td>
              <td align="right">385</td>
              <td align="right">9975</td>
            </tr>
            <tr>
              <td>Rooms Occupied minus Comp And House Use</td>
              <td align="right">3</td>
              <td align="right">29</td>
              <td align="right">35</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
            </tr>
            <tr>
              <td>Rooms Occupied minus House Use</td>
              <td align="right">3</td>
              <td align="right">29</td>
              <td align="right">35</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
            </tr>
            <tr>
              <td>Rooms Occupied minus Comp</td>
              <td align="right">3</td>
              <td align="right">29</td>
              <td align="right">35</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
            </tr>
            <tr>
              <td>Day Use Rooms</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
            </tr>
            <tr>
              <td>Out of Order Rooms (DNR)</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
            </tr>
            <tr>
              <td>In-House Adults</td>
              <td align="right">4</td>
              <td align="right">17</td>
              <td align="right">18</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
            </tr>
            <tr>
              <td>In-House Children</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
            </tr>
            <tr>
              <td>Total In-House Persons</td>
              <td align="right">4</td>
              <td align="right">17</td>
              <td align="right">18</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
            </tr>
            <tr>
              <td>Individual Persons In-House</td>
              <td align="right">0</td>
              <td align="right">8</td>
              <td align="right">9</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
            </tr>
            <tr>
              <td>Block Persons In-House</td>
              <td align="right">4</td>
              <td align="right">9</td>
              <td align="right">9</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
            </tr>
            <tr>
              <td>VIP Persons In-House</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
            </tr>
            <tr>
              <td>Individual Rooms In-House</td>
              <td align="right">0</td>
              <td align="right">21</td>
              <td align="right">26</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
            </tr>
            <tr>
              <td>Block Rooms In-House</td>
              <td align="right">3</td>
              <td align="right">9</td>
              <td align="right">10</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
            </tr>
            <tr>
              <td>Source Rooms In-House</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
            </tr>
            <tr>
              <td>Company Rooms In-House</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
            </tr>
            <tr>
              <td>Travel Agent Rooms In-House</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
            </tr>
            <tr>
              <td>Blocks In-House</td>
              <td align="right">3</td>
              <td align="right">7</td>
              <td align="right">7</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
            </tr>
            <tr>
              <td>Birthdays In-House</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
            </tr>
            <tr>
              <td>% Rooms Occupied minus OOO</td>
              <td align="right">21.43</td>
              <td align="right">14.8</td>
              <td align="right">0.36</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
            </tr>
            <tr>
              <td>% Rooms Occupied minus Comp and House</td>
              <td align="right">21.43</td>
              <td align="right">14.8</td>
              <td align="right">0.36</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
            </tr>
            <tr>
              <td>Arrival Rooms</td>
              <td align="right">3</td>
              <td align="right">10</td>
              <td align="right">15</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
            </tr>
            <tr>
              <td>Arrival Persons</td>
              <td align="right">4</td>
              <td align="right">12</td>
              <td align="right">18</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
            </tr>
            <tr>
              <td>Frontdesk Rooms</td>
              <td align="right">3</td>
              <td align="right">10</td>
              <td align="right">15</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
            </tr>
            <tr>
              <td>Walk-in Rooms</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
            </tr>
            <tr>
              <td>Frontdesk Persons</td>
              <td align="right">4</td>
              <td align="right">12</td>
              <td align="right">18</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
            </tr>
            <tr>
              <td>Walk-in Persons</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
            </tr>
            <tr>
              <td>Departure Rooms</td>
              <td align="right">1</td>
              <td align="right">12</td>
              <td align="right">12</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
            </tr>
            <tr>
              <td>Departure Persons</td>
              <td align="right">1</td>
              <td align="right">14</td>
              <td align="right">14</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
            </tr>
            <tr>
              <td>No Show Rooms</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
            </tr>
            <tr>
              <td>No Show Persons</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
            </tr>
            <tr>
              <td>Reservation Cancellations made Today</td>
              <td align="right">0</td>
              <td align="right">2</td>
              <td align="right">2</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
            </tr>
            <tr>
              <td>Reservations Made Today</td>
              <td align="right">0</td>
              <td align="right">15</td>
              <td align="right">15</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
            </tr>
            <tr>
              <td>Cancelled Reservations for Today</td>
              <td align="right">0</td>
              <td align="right">2</td>
              <td align="right">2</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
            </tr>
            <tr>
              <td>Room Nights Reserved Today</td>
              <td align="right">0</td>
              <td align="right">30</td>
              <td align="right">36</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
            </tr>
            <tr>
              <td>ADR</td>
              <td>Rp 1,052,256.46</td>
              <td>Rp 747,592.28</td>
              <td>Rp 787,734.27</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
            </tr>
            <tr>
              <td>ADR minus Comp</td>
              <td>Rp 1,052,256.46</td>
              <td>Rp 747,592.28</td>
              <td>Rp 787,734.27</td>
              <td align="right">0</td>
              <td align="right">0</td>
              <td align="right">0</td>
            </tr>
            <tr>
              <td>Room Revenue</td>
              <td>Rp 3,156,769.37</td>
              <td>Rp 21,680,176.22</td>
              <td>Rp 27,570,699.35</td>
              <td>--</td>
              <td>--</td>
              <td>--</td>
            </tr>
            <tr>
              <td>Total Revenue</td>
              <td>Rp 3,156,769.37</td>
              <td>Rp 21,681,249.22</td>
              <td>Rp 27,572,163.85</td>
              <td>--</td>
              <td>--</td>
              <td>--</td>
            </tr>
            <tr>
              <td>Arrival Rooms for Tomorrow</td>
              <td align="right">0</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td align="right">0</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>Departure Rooms for Tomorrow</td>
              <td align="right">0</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td align="right">0</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>% Rooms Occupied for the Next 7 Days</td>
              <td align="right">18.37</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td align="right">0</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>% Rooms Occupied for the Next 31 Days</td>
              <td align="right">4.15</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td align="right">0</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>% Rooms Occupied for the Next 365 Days</td>
              <td align="right">0.35</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td align="right">0.28</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td colspan="7">POS Revenue</td>
            </tr>
            <tr height="21">
              <td height="21">Hlx_SPA</td>
              <td>--</td>
              <td>Rp 290.00</td>
              <td>Rp 290.00</td>
              <td>--</td>
              <td>--</td>
              <td>--</td>
            </tr>
            </tbody>
            </table>
      </div></div>
    </div>
</section>