<?php
    $month = array("January","February","March","April","May","June","July","August","September","October","November","December");
    $year = array();
    $curyear = intval(date("Y"));
    for($i=$curyear;$i>=2017;$i--){
        $year[] = $i;
    }
?>
<script>
    $(document).ready(function() {
        $('#xload').css('display', 'none');

        $('#btfilter').click(function(){
            $('#xload').css('display', '');
            $('#loaddata').html("");

            setTimeout(function (){
                var a = $("#zmonth").val();
                var b = $("#zyear").val();
                var c = $("#zhotel").val();

                if(a.trim()!="" && b.trim()!="" && c.trim()!=""){
                    $.post('<?php echo $base_url; ?>/includes/pms-report/ajax/report_occupancy.php', { 'a':a, 'b':b, 'c':c }, function(response){
                        $('#loaddata').html(response);
                        $('#xload').css('display', 'none');
                    });
                }else{
                    alert("All field can't be empty!");
                    $('#xload').css('display', 'none');
                }
            }, 2000);
        });

        $(document).on('click','#btexport',function(){
            $('#exporttoexcel').submit();
        });
    });
</script>
<style>
    .content .form-group label {
        padding-right:10px;
    }
    .content .form-group label, .content .form-group select, .content .form-group input {
        margin-bottom:5px;
    }
    a.detail {
        color: #1200ff;
    }
    a.detail:hover {
        text-decoration: underline;
    }
    .btn {
        border-radius: 3px;
        -webkit-box-shadow: none;
        box-shadow: none;
        border: 1px solid transparent;
    }
    .btn-primary {
        color: #fff;
        background-color: #337ab7;
        border-color: #2e6da4;
    }
    .btn-primary {
        background-color: #3c8dbc;
        border-color: #367fa9;
    }
    #example {
        border: 1px solid #999;
    }
    #example tbody td, #example tfoot th {
        border-top: 1px solid #999;
    }
    #example tbody td, #example thead th, #example tfoot th {
        border-left: 1px dashed #ddd;
        padding: 2px 2px;
    }
    #example tbody tr td:first-child, #example thead tr th:first-child, #example tfoot tr th:first-child {
        border-left: 0;
    }
    .bleft {border-left: 1px dashed #ddd !important;}
    .bnoleft {border-left: 0 !important;}
    #example thead, #example tfoot {
        background-color: #d4ebec;
    }
</style>
<section class="content-header">
    <h1>
       	Report PMS - Occupancy Report
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Report PMS</a></li>
    </ol>
</section>
<section class="content">
<div class="row">
    <div class="box box-form">
        <div class="box-body" style="font-size:12px;">
            <div class="form-group">
                <label>Month : </label>
                <select class="form-control" id="zmonth">
                    <option value="">Select Month</option>
                    <?php
                        foreach($month as $k => $v){
                            echo '<option value="'.$v.'">'.$v.'</option>';
                        }
                    ?>
                </select>
                <label>Year : </label>
                <select class="form-control" id="zyear">
                    <option value="">Select Year</option>
                    <?php
                        foreach($year as $k => $v){
                            echo '<option value="'.$v.'">'.$v.'</option>';
                        }
                    ?>
                </select>
                <label>Property : </label>
                <?php
                if($hoteloid > 0){
                    $stmt = $db->prepare("SELECT hoteloid, hotelcode, hotelname FROM hotel WHERE hoteloid=:a AND publishedoid='1' ORDER BY hotelname");
	                $stmt->execute(array(':a' => $hoteloid));
                    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
                }else{
                    $s_chain = "SELECT ua.oid, ua.type FROM users u INNER JOIN userassign ua USING (useroid) WHERE ua.type IN ('chainoid', 'hoteloid') AND u.useroid = '".$_SESSION['_oid']."'";
            		$q_chain = $db->query($s_chain);
            		$r_chain = $q_chain->fetchAll(PDO::FETCH_ASSOC);
            		$rxoid = array(); $rxtype = array();
            		foreach($r_chain as $row){
            			$rxoid[] = $row['oid'];
            			$rxtype[] = $row['type'];
            		}

            		$rxchain = array(); $rxhotel = array();
                    foreach($rxtype as $k => $v){
                        if($v == 'chainoid'){
                            $rxchain[] = $rxoid[$k];
                        }else if($v == 'hoteloid'){
                            $rxhotel[] = $rxoid[$k];
                        }
                    }

                    $msg = "";
                    if(count($rxchain) > 0){
                        $imp = implode(",", $rxchain);
                        $msg .= "AND chainoid IN (".$imp.")";
                    }
                    if(count($rxhotel) > 0){
                        $imp = implode(",", $rxhotel);
                        $msg .= "AND hoteloid IN (".$imp.")";
                    }

            		if($msg != ""){
                        $stmt = $db->prepare("SELECT hoteloid, hotelcode, hotelname FROM hotel WHERE publishedoid='1' ".$msg." ORDER BY hotelname");
    	                $stmt->execute(array());
                        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
            		}else{
            		    $data = array();
            		}
                }
                ?>
                <select class="form-control" id="zhotel">
                    <option value="">Select Hotel</option>
                    <?php
                        foreach($data as $vals){
                            echo '<option value="'.$vals['hoteloid'].'">'.$vals['hotelname'].'</option>';
                        }
                    ?>
                </select>
                <button type="button" class="btn btn-primary" id="btfilter">SHOW REPORT</button>
                
                <?php
                    $check_user_access = "select count(h.hoteloid) as foundnagisa from hotel h inner join city ct using (cityoid) inner join chain c using (chainoid) where ((h.chainoid in (select oid from userassign where type = 'chainoid' and useroid = '".$_SESSION['_oid']."')) or (h.hoteloid in (select oid from userassign where type = 'hoteloid' and useroid = '".$_SESSION['_oid']."'))) and h.publishedoid = '1'and h.chainoid = '64'";
                    $q_user_access = $db->query($check_user_access);
                    $r_user_access = $q_user_access->fetch(PDO::FETCH_ASSOC);
                    if($r_user_access['foundnagisa'] > 0 or $_SESSION['_hotel'] == "1"){
                ?>
                    <?php if(in_array($_SESSION['_typeusr'], array('1','2','4'))){?>
                        <a href="<?php echo $base_url; ?>/pms-report/occupancypervilla" style="float:right"><button type="button" class="btn btn-primary" id="btfilter">Occupancy per Villa</button></a>
                    <?php }?>
                <?php 
                    }
                ?>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="box box-form">
        <div class="box-body" style="font-size: 12px;">
            <div id="xload" align="center"><img src="../images/loading.gif" style="width:10%"></div>

            <div id="loaddata"></div>
        </div>
    </div>
</div>
</section>
