<?php
    session_start();
    error_reporting(0);
    include("../../../conf/connection.php");

    function dateDiff($start, $end){
		$startdateStamp = date_create($start);
		$enddateStamp = date_create($end);
		$interval = date_diff($startdateStamp , $enddateStamp);
		return $interval->format('%a');
	}
    
    $startdate = date('Y-m-d', strtotime($_POST['startdate']));
    $enddate = date('Y-m-d', strtotime($_POST['enddate']));
    
    $s_hotelchain = "select h.hoteloid, h.hotelcode, h.hotelname from hotel h inner join chain c using (chainoid) where h.chainoid in (select oid from userassign where type = 'chainoid' and useroid = '".$_SESSION['_oid']."') and h.publishedoid = '1'";
    $filter = array();
    if(!empty($_POST['hotelcode'])){
    $s_hotelchain = "select h.hoteloid, h.hotelcode, h.hotelname from hotel h inner join chain c using (chainoid) where h.publishedoid = '1'";
    array_push($filter, "h.hotelcode = '".$_POST['hotelcode']."'");
    }
    if(count($filter) > 0){
    	$combine_filter = implode(' and ',$filter);
    	$s_hotelchain = $s_hotelchain.' and '.$combine_filter;
    }
    $q_hotelchain = $db->query($s_hotelchain);
    $r_hotelchain = $q_hotelchain->fetchAll(PDO::FETCH_ASSOC);
    foreach($r_hotelchain as $hotelchain){
        $msg = "";
        if(!empty($startdate)){
            $msg .= "AND ((br.`checkin`<='".$startdate."' AND br.`checkout`>='".$startdate."') 
                OR (br.`checkin`>='".$startdate."' AND br.`checkout`<='".$enddate."') 
                OR (br.`checkin`<='".$enddate."' AND br.`checkout`>='".$enddate."')) ";
        }
        
        $stmt = $db->prepare("SELECT bookingoid, bookingnumber, bookingtime, title, firstname, lastname, c.phone, c.mobilephone, c.email, r.name as roomname, b.totalcharge,
            checkin, checkout, DATEDIFF(checkout, checkin) AS diff, br.room, br.adult, br.child, br.total as roomtotal, currencycode, b.grandtotal, b.paid, b.paid_balance, b.bookingtime, rn.roomnumber,
            b.bm_name, bm.bm_name as bm_name1, countryname, pm.status, pm.`pmsstatusoid`, brd.bookingroomdtloid, b.agentoid, g.agentname, g.agenttypeoid, bookingstatusoid
        FROM `booking` b
            INNER JOIN `bookingroom` br USING(`bookingoid`)
            INNER JOIN `bookingroomdtl` brd USING(`bookingroomoid`)
            INNER JOIN `customer` c USING(`custoid`)
            LEFT JOIN `currency` cr ON cr.`currencyoid` = b.`currencyoid`
            LEFT JOIN `roomoffer` ro ON ro.`roomofferoid` = br.`roomofferoid`
            LEFT JOIN `room` r ON r.`roomoid` = ro.`roomoid`
            LEFT JOIN `roomnumber` rn ON brd.`roomnumberoid` = rn.`roomnumberoid`
            LEFT JOIN `bookingmarket` bm ON bm.`bookingmarketoid` = b.`bookingmarketoid`
            LEFT JOIN `country` cn ON cn.`countryoid` = c.`countryoid`
            LEFT JOIN `pmsstatus` pm ON pm.`pmsstatusoid` = br.`pmsstatusoid`
            LEFT JOIN `agent` g ON g.`agentoid`=b.`agentoid`
        WHERE `bookingstatusoid` in (4) ".$msg." AND br.pmsstatusoid in (2,4,5,6) AND brd.reconciled = '0' AND b.hoteloid = :a
        GROUP BY `bookingroomoid` ORDER BY r.roomoid, br.checkin ASC");
        $stmt->execute(array(':a'=>$hotelchain['hoteloid']));
        $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
        
?>
<table id="inventory" cellpadding="0" cellspacing="0" style="margin-bottom:20px; font-size:0.83em;">
    <thead>
        <tr>
            <th style="min-width:90px">Room</th>
            <th style="min-width:145px">Guest</th>
            <th style="min-width:60px">CheckIn</th>
            <th style="min-width:60px">CheckOut</th>
            <th>Ref#</th>
            <th>Adult</th>
            <th>Child</th>
            <th colspan="2">Rate/Night</th>
            <th colspan="2">Room Total</th>
            <th colspan="2">Other Charge</th>
            <th colspan="2">Grand Total</th>
            <th colspan="2">Payment Total</th>
            <th colspan="2">Balance Total</th>
            <th style="min-width:70px">Status</th>
            <th style="min-width:240px">Payment Notes</th>
            <th style="min-width:240px">Other Charges Notes</th>
        <tr>
    </thead>
    <tbody>
        <?php
        echo '<tr><td colspan="22">&nbsp;</td></tr>';

        $night = dateDiff($startdate, $enddate);
        for($i=0; $i<=$night; $i++){
            $cdate = date('Y-m-d', strtotime($startdate . " +" . $i ." day"));

            echo '<tr><td colspan="22" style="font-weight:600;background-color:#eaeaea;">'.date('d M Y', strtotime($cdate)).'</td></tr>';
            $sdroomtotal = 0;
            foreach($r_room as $row){
                $bookingoid = $row['bookingoid'];
                $bookingnum = $row['bookingnumber'];
                $guestname = $row['firstname']." ".$row['lastname'];
                $checkin = date("d M y", strtotime($row['checkin']));
                $checkout = date("d M y", strtotime($row['checkout']));

                $sci = date('Y-m-d', strtotime($checkin));
                $sco = date('Y-m-d', strtotime($checkout));
                $ngt = dateDiff($sci, $sco); $found = 0;
                for($j=0; $j<$ngt; $j++){
                    $sdt = date('Y-m-d', strtotime($sci . " +" . $j ." day"));
                    if($sdt == $cdate){
                        $found = 1;
                    }
                }
                if($found == 0){ continue; }

                $room = $row['room'];
                $roomname = empty($room)?$row['roomname']:$room;
                if($row['bookingstatusoid'] == "5"){
                    $status = "Cancelled";
                }else if($row['bookingstatusoid'] == "7"){
                    $status = "No Show";
                }else{
                    $status = $row['status']; 
                }
                $adult = $row['adult'];
                $child = $row['child'];
                $currcode = empty($row['currencycode']) ? "IDR" : $row['currencycode'];
                $droomtotal = number_format($row['roomtotal']/$ngt, 2, '.', ','); $sdroomtotal += ($row['roomtotal']/$ngt);
                $roomtotal = number_format($row['roomtotal'], 2, '.', ',');
                $grandtotal = number_format($row['grandtotal'], 2, '.', ',');
                $grandbalance = number_format($row['paid_balance'], 2, '.', ',');
                $grandpaid = number_format($row['paid'], 2, '.', ',');
                $grandcharge = number_format($row['totalcharge'], 2, '.', ',');
                
                $stmt = $db->prepare("SELECT `method`, `currencycode`, `total`, `paymentdate`
                    FROM `bookingpaymentdtl` INNER JOIN `currency` USING(`currencyoid`) LEFT JOIN `pmspaymenttype` USING(`pmspaymenttypeoid`) WHERE `bookingoid`=:a");
                $stmt->execute(array(':a' => $bookingoid));
                $payment = $stmt->fetchAll(PDO::FETCH_ASSOC);
                
                $stmt = $db->prepare("SELECT `product`, `currencycode`, `total`
                    FROM `bookingcharges` INNER JOIN `currency` USING(`currencyoid`) WHERE `bookingoid`=:a");
                $stmt->execute(array(':a' => $bookingoid));
                $charges = $stmt->fetchAll(PDO::FETCH_ASSOC);
                
                
                echo '<tr valign="top">
                    <td>'.$roomname.'</td>
                    <td>'.$guestname.'</td>
                    <td>'.$checkin.'</td>
                    <td>'.$checkout.'</td>
                    <td><a class="detail" title="View Detail">'.$bookingnum.'</a></td>
                    <td align="center">'.$adult.'</td>
                    <td align="center">'.$child.'</td>
                    <td>'.$currcode.'</td>
                    <td align="right">'.$droomtotal.'</td>
                    <td>'.$currcode.'</td>
                    <td align="right">'.$roomtotal.'</td>
                    <td>'.$currcode.'</td>
                    <td align="right">'.$grandcharge.'</td>
                    <td>'.$currcode.'</td>
                    <td align="right">'.$grandtotal.'</td>
                    <td>'.$currcode.'</td>
                    <td align="right">'.$grandpaid.'</td>
                    <td>'.$currcode.'</td>
                    <td align="right">'.$grandbalance.'</td>
                    <td>'.$status.'</td>
                    <td>
                    <table class="subtable">
                        <tbody>';
                        
                        foreach($payment as $pay){
                            echo '<tr><td>'.$pay['method'].'('.date('d/m/y', strtotime($pay['paymentdate'])).')</td><td>'.$pay['currencycode'].'</td><td>'.number_format($pay['total'], 2, '.', ',').'</td></tr>';
                        }
                            
                echo '  </tbody>
                    </table>
                    </td>
                    <td>
                    <table class="subtable">
                        <tbody>';
                        
                        foreach($charges as $charge){
                            echo '<tr><td>'.$charge['product'].'</td><td>'.$charge['currencycode'].'</td><td>'.number_format($charge['total'], 2, '.', ',').'</td></tr>';
                        }
                            
                echo '  </tbody>
                    </table>
                    <form class="view-reservation-detail" action="'.$base_url.'/myhotel/pms-lite/reservation-detail" method="post" target="_blank">
                        <input name="bookingoid" type="hidden" value="'.$bookingoid.'">
                        <input name="hotel" type="hidden" value="'.$hotelchain['hoteloid'].'">
                    </form>
                    </td>
                <tr>';
            }
            echo '<tr style="background-color:#f8f8f8;"><td colspan="7">Total</td><td>'.$currcode.'</td><td align="right">'.number_format($sdroomtotal, 2, '.', ',').'</td><td colspan="13">&nbsp;</td></tr>';
        }
        echo '<tr><td colspan="22">&nbsp;</td></tr>';
        ?>
    </tbody>
</table>
<?php
    }
?>