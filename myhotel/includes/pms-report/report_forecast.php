<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous"> -->

<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    });
</script>
<section class="content-header">
    <h1>
       	Report PMS - Forecast Report
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Report PMS</a></li>
    </ol>
</section>
<section class="content">
    


<div class="row">
<div class="box box-form"><div class="box-body" style="font-size: 12px;">

<div class="form-group">
    <!-- Select Room Type:   
    <input type="checkbox" id="myChec1">Superior    
    <input type="checkbox" id="myCheck">Deluxe 
    <input type="checkbox" id="myCheck">Suites
    &nbsp;&nbsp;&nbsp;&nbsp;
    <select name="status" class="form-control">
        <option value="volvo">All Status</option>
        <option value="saab">Check In</option>
        <option value="fiat">Check Out</option>
        <option value="audi">Reserve</option>
        <option value="volvo">Temporary Reserve</option>
    </select>
        
    <select name="market" class="form-control">
        <option value="volvo">All Market</option>
        <option value="volvo">Direct website</option>
        <option value="saab">direct booking</option>
        <option value="fiat">Travel agent</option>
        <option value="audi">Corporate</option>
        <option value="volvo">Online Travel Agent (OTA)</option>
        <option value="saab">Vacation Rental</option>
        <option value="fiat">Wedding & event</option>
        <option value="audi">Affiliate</option>
    </select> -->
    
    <label>Between:</label> &nbsp;&nbsp;
    <input type='text' class="form-control" value="2017-10-01"/>
    <label>And:</label> &nbsp;&nbsp;
    <input type='text' class="form-control" value="2017-10-31"/>
</div>
        </div></div>
</div>




    <div class="row">
        <div class="box box-form"><div class="box-body" style="font-size: 12px;">
            <table id="example" class="display table promo-table" cellspacing="0" width="100%">
            <thead>
              <tr>
                <td>Date</td>
                <td>Day</td>
                <td>Adl</td>
                <td>Chl</td>
                <td>Total Guests</td>
                <td>Arr.Rms. Deducted</td>
                <td>Dep.Rms. Deducted</td>
                <td>Occ.Rms. Deducted</td>
                <td>Occ.Rms. Deducted%</td>
                <td>DNR/House Use Rooms</td>
                <td>Blk-Rooms Deducted</td>
                <td>Blk-Rooms N.Deducted</td>
                <td>RoomRevenue Deducted</td>
                <td>Avg.Room Deducted</td>
                <td>Other Revenue</td>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td colspan="15">Individual    Reservations</td>
              </tr>
              <tr>
                <td>12-10-17</td>
                <td>Thu</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0.00%</td>
                <td>0</td>
                <td>0</td>
                <td>3</td>
                <td>--</td>
                <td>--</td>
                <td>--</td>
              </tr>
              <tr>
                <td colspan="2">Total</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0.00%</td>
                <td>0</td>
                <td>0</td>
                <td>3</td>
                <td>--</td>
                <td>--</td>
                <td>--</td>
              </tr>
              <tr>
                <td colspan="15">Block    Reservations</td>
              </tr>
              <tr>
                <td>12-10-17</td>
                <td>Thu</td>
                <td>4</td>
                <td>0</td>
                <td>4</td>
                <td>0</td>
                <td>0</td>
                <td>3</td>
                <td>21.43%</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>Rp 3,101,460.00</td>
                <td>Rp 1,033,820.00</td>
                <td>--</td>
              </tr>
              <tr>
                <td colspan="2">Total</td>
                <td>4</td>
                <td>0</td>
                <td>4</td>
                <td>0</td>
                <td>0</td>
                <td>3</td>
                <td>21.43%</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>Rp 3,101,460.00</td>
                <td>--</td>
                <td>--</td>
              </tr>
              <tr>
                <td colspan="15">Block Rooms Not    Picked Up</td>
              </tr>
              <tr>
                <td>12-10-17</td>
                <td>Thu</td>
                <td>4</td>
                <td>1</td>
                <td>5</td>
                <td>0</td>
                <td>0</td>
                <td>3</td>
                <td>21.43%</td>
                <td>0</td>
                <td>0</td>
                <td>NA</td>
                <td>Rp 2,946,390.00</td>
                <td>Rp 982,130.00</td>
                <td>--</td>
              </tr>
              <tr>
                <td colspan="2">Total</td>
                <td>4</td>
                <td>1</td>
                <td>5</td>
                <td>0</td>
                <td>0</td>
                <td>3</td>
                <td>21.43%</td>
                <td>0</td>
                <td>0</td>
                <td>NA</td>
                <td>Rp 2,946,390.00</td>
                <td>Rp 982,130.00</td>
                <td>--</td>
              </tr>
              <tr>
                <td colspan="15">All    Reservations And Block Rooms Not Picked Up Combined</td>
              </tr>
              <tr>
                <td>12-10-17</td>
                <td>Thu</td>
                <td>8</td>
                <td>1</td>
                <td>9</td>
                <td>0</td>
                <td>0</td>
                <td>6</td>
                <td>14.29%</td>
                <td>0</td>
                <td>0</td>
                <td>3</td>
                <td>Rp 6,047,850.00</td>
                <td>Rp 1,007,975.00</td>
                <td>--</td>
              </tr>
              <tr>
                <td colspan="2">Total</td>
                <td>8</td>
                <td>1</td>
                <td>9</td>
                <td>0</td>
                <td>0</td>
                <td>6</td>
                <td>14.29%</td>
                <td>0</td>
                <td>0</td>
                <td>3</td>
                <td>Rp 6,047,850.00</td>
                <td>Rp 1,007,975.00</td>
                <td>--</td>
              </tr>
            
            </tbody>
            </table>
      </div></div>
    </div>
</section>