<?php
    if(!empty($_GET['ho']) and $_GET['ref']=='contacts'){
        $hoteloid = $_GET['ho'];
        $hotelname = " - ".$_GET['hn'];
    }

  $s_mt = "SELECT * from mailertype WHERE mailertypeoid NOT IN (1)";
  $q_mt = $db->query($s_mt);
  $reservationemail = $q_mt->fetchAll(PDO::FETCH_ASSOC);
  // $reservationemail = array('to', 'cc', 'bcc');
  include_once("js.php");
 // $icon = array('y' => '<i class="fa fa-check"></i>', 'n' => '<i class="fa fa-times"></i>');
?>
<section class="content-header">
   <h1>
       Contact Management
   </h1>
   <ol class="breadcrumb">
       <li><a href="#"><i class="fa fa-table"></i> Settings</a></li>
       <li class="active">Contact Management</li>
   </ol>
</section>


<section class="content">
    <form method="post" action="#" id="data-input">
    <input type="hidden" name="hoteloid" value="<?php echo $hoteloid; ?>" />
    <div class="row">
        <div class="box box-form">
            <h1>Email Settings : Receiver <?=$hotelname;?></h1>
            <div class="box-body">
                <div class="form-group">
                    <table class="table table-fill table-fill-centered">
                        <tr>
                            <td>Name</td>
                            <td>Email</td>
                            <td>Phone</td>
                            <td>Reservation Email</td>
                            <td colspan="2">Action</td>
                        </tr>
                        <tr>
                            <td><input type="text" class="medium" name="name"/></td>
                            <td><input type="text" class="medium" name="email"/></td>
                            <td><input type="text" class="medium" name="phone"/></td>
                            <!-- <td>
                              <?php foreach ($reservationemail as $key => $value) {
                                echo '<input type="radio" name="reservationemail" value="'.$value.'"/>'.$value;
                              }
                              ?>
                            </td> -->
                            <td>
                              <select name="reservationemail">
  							                <?php
                                foreach ($reservationemail as $value) {
                                    echo"<option value='".$value['mailertypeoid']."'> ".$value['mailertype']."</option>";
                                }

                              ?>
                              </select>
                            </td>
                            <td><button type="button" class="small-button blue add-hc">Add</button></td>
                            <td>&nbsp;</td>
						</tr>
                    <?php
					try {
						$stmt = $db->query("select * from hotelcontact where hoteloid = '".$hoteloid."' AND publishedoid = '1' order by email");
						$r_mail = $stmt->fetchAll(PDO::FETCH_ASSOC);
						foreach($r_mail as $row){
							echo"
							<tr>
								<td>
									<input type = 'hidden' name = 'ro_id[]' value = '".$row['hotelcontactoid']."'>
									<input type = 'text' class='medium' name = 'ro_name[]' value = '".$row['name']."'>
								</td>
								<td><input type = 'text' class='medium' name = 'ro_email[]' value = '".$row['email']."'></td>
                <td><input type = 'text' class='medium' name = 'ro_phone[]' value = '".$row['phone']."'></td>
                <td>
                  <select name = 'ro_reservationemail[]'>";

                // foreach ($reservationemail as $key => $value) {
                //   $checked = ($row['reservationemail']==$value) ? ' checked="checked" ' : '';
                //   echo '<input type="radio" name="reservationemail['.$row['hotelcontactoid'].'][]" value="'.$value.'" '.$checked.'/>'.$value;
                // }
                    foreach ($reservationemail as $value) {
                      if($row['mailertypeoid'] == $value['mailertypeoid']){ $selected = "selected"; }else{ $selected = ""; }
                      echo"<option value='".$value['mailertypeoid']."' ".$selected.">  ".$value['mailertype']."</option>";
                    }
                    echo "
                  </select>
                </td>
								<td>&nbsp;</td>
                                <td><button type='button' data-id = '".$row['hotelcontactoid']."' class='small-button red del'>Delete</button></td>
							</tr>
							";
						}
					}catch(PDOException $ex) {
						echo "Invalid Query";
						die();
					}
					?>
					</table>
                </div>
            </div>
            <br>
            <div class="box-footer" align="right">
                <?php
                if(!empty($_GET['ho']) and $_GET['ref']=='contacts'){
                    ?>
                    <a type="button" class="btn btn-default" href="<?=$base_url;?>/contacts"><i class="fa fa-arrow-left"></i> Back</a>
                    <?php
                } ?>
				<button type="button" class="small-button blue submit-edit-hc">Save</button>
                <button type="reset" class="small-button blue">Reset</button>
            </div>
        </div>
    </div>
    </form>
</section>
