<?php
session_start();
date_default_timezone_set("Asia/Makassar");
header("Content-Type: application/xls");
header("Content-Disposition: attachment; filename=OtherChargeReport_".date("YmdHis").".xls");
header("Pragma: no-cache");
header("Expires: 0");

if(empty($_SESSION['_oid'])){
    die();
}

include("../../conf/connection.php");
include("../../includes/pms-report/function_report.php");

$month = $_POST['a'];
$year = $_POST['b'];
$hoteloid = $_POST['c'];

$stmt = $db->prepare("SELECT hoteloid, hotelcode, hotelname FROM hotel WHERE hoteloid=:a AND publishedoid='1' ORDER BY hotelname");
$stmt->execute(array(':a' => $hoteloid));
$hotel = $stmt->fetch(PDO::FETCH_ASSOC);

?>
<table id="example" class="display" cellspacing="0" width="100%">
<thead>
    <tr>
        <th>Check In</th>
        <th>Check Out</th>
        <th>Guest Name</th>
        <th>POS</th>
        <th>Product</th>
        <th colspan="2">Price</th>
        <th>Qty</th>
        <th colspan="4">Total</th>
        <th>Created Date</th>
        <th>Created By</th>
    </tr>
</thead>
<tbody>
    <?php
    $armonth = array("January"=>1,"February"=>2,"March"=>3,"April"=>4,"May"=>5,"June"=>6,"July"=>7,"August"=>8,"September"=>9,"October"=>10,"November"=>11,"December"=>12);
    $data = getReservationChargesRange($db, $hoteloid, $month, $year);
    $titles = getCustTitle($db);
    
    $gtotalusd = 0;$gtotalidr = 0;
    foreach($data as $row){
        if(isset($titles[$row['title']])) $title = $titles[$row['title']]; else $title = $row['title'];
        $guestname = $title." ".$row['firstname']." ".$row['lastname'];
        $pos = $row['pos'];
        $bookingoid = $row['bookingoid'];
        $product = $row['product'];
        $qty = $row['qty'];
        $price = $row['price'];
        $total = $row['total'];
        $created = date("d/m", strtotime($row['created']));
        $createdby = $row['createdby'];
        $displayname = empty($row['displayname']) ? "-" : $row['displayname'];
        $currencycode = $row['currencycode'];
        $checkin = date("d/m", strtotime($row['checkin']));
        $checkout = date("d/m", strtotime($row['checkout']));
        
        if($currencycode == 'USD'){
            $gtotalusd += $total;
            $totalusd = $currencycode.'</td><td align="right" class="bnoleft">'.number_format($total, 2, '.', ',');
            $totalidr = '</td><td align="right" class="bnoleft">';
        }else{
            $gtotalidr += $total;
            $totalidr = $currencycode.'</td><td align="right" class="bnoleft">'.number_format($total, 2, '.', ',');
            $totalusd = '</td><td align="right" class="bnoleft">';
        }
        
        echo '<tr>
            <td align="center">'.$checkin.'</td>
            <td align="center">'.$checkout.'</td>
            <td>'.$guestname.'</td>
            <td>'.$pos.'</td>
            <td>'.$product.'</td>
            <td>'.$currencycode.'</td><td align="right" class="bnoleft">'.number_format($price, 2, '.', ',').'</td>
            <td align="center">'.$qty.'</td>
            <td>'.$totalusd.'</td>
            <td>'.$totalidr.'</td>
            <td align="center">'.$created.'</td>
            <td>'.$createdby.' ('.$displayname.')</td>
        </tr>';
    
    }
    ?>
</tbody>
<tfoot>
    <tr>
        <th rowspan="2" colspan="8" align="center">TOTAL</th>
        <th>USD</th>
        <th align="right" style="width:75px" class="bnoleft"><?=number_format($gtotalusd, 2, '.', ',')?></th>
        <th>IDR</th>
        <th align="right" style="width:75px" class="bnoleft"><?=number_format($gtotalidr, 2, '.', ',')?></th>
        <th></th>
        <th></th>
    </tr>
</tfoot>
</table>