<?php
try{
  session_start();
	error_reporting(E_ALL ^ E_NOTICE);
  include ('../../conf/connection.php');
  include("../../includes/pms-lite/class-pms-lite.php");

  $hoteloid = $_SESSION['_hotel'];
  $bookingnumber = $_REQUEST['bookingnumber'];

  $pmslite = new PMSReservation($db);
  $pmslite->setBooking($bookingnumber);
  $datarsv = $pmslite->getBookingData();
  $pmslite->setPMSHotel($datarsv['hoteloid']);

  $pmslitedata = new PMSEditRSV($db);
  $pmslitedata->setBooking($bookingnumber);
  $dataroom = $pmslitedata->getBookingRoomData();

  if(in_array($hoteloid,$avoid_adjust)){
    $adjust = 1;
  }


  /*--------------------------------------*/
  $fileVersion = date("Y.m.d.H.i.s");
  
  /*--------------------------------------*/
  
  if(empty($datarsv['invnumofhotel']) or $datarsv['invnumofhotel']=='0'){
      $newdate = date('Y-m-d H:i:s');
      
      $stmt = $db->prepare("SELECT MAX(`invnumofhotel`) as jml FROM `booking` WHERE `hoteloid`=:a AND LEFT(`dateofinvoice`,4)=:b AND RIGHT(LEFT(`dateofinvoice`,7),2)=:c");
      $stmt->execute(array(':a' => $datarsv['hoteloid'], ':b' => date('Y', strtotime($newdate)), ':c' => date('m', strtotime($newdate))));
      $numinv = $stmt->fetch(PDO::FETCH_ASSOC);
      
      if(empty($numinv['jml'])){
          $newjml = 1;
      }else{
          $newjml = intval($numinv['jml']) + 1;
      }
      
      $stmt = $db->prepare("SELECT MAX(`invnumofchain`) as jml FROM `booking` LEFT JOIN `hotel` USING(`hoteloid`) WHERE `chainoid`=(SELECT `chainoid` FROM `hotel` WHERE `hoteloid`=:a) AND LEFT(`dateofinvoice`,4)=:b AND RIGHT(LEFT(`dateofinvoice`,7),2)=:c");
      $stmt->execute(array(':a' => $datarsv['hoteloid'], ':b' => date('Y', strtotime($newdate)), ':c' => date('m', strtotime($newdate))));
      $anuminv = $stmt->fetch(PDO::FETCH_ASSOC);
      
      if(empty($anuminv['jml'])){
          $anewjml = 1;
      }else{
          $anewjml = intval($anuminv['jml']) + 1;
      }
      
      $stmt = $db->prepare("UPDATE `booking` SET  `invnumofhotel`=:a, `invnumofchain`=:b, `dateofinvoice`=:c WHERE `bookingnumber`=:d;");
      $stmt->execute(array(':a' => $newjml, ':b' => $anewjml, ':c' => $newdate, ':d' => $bookingnumber));
      
      $idnuminv = $newjml;
      $aidnuminv = $anewjml;
      $invrawdate = $newdate;
  }else{
      $idnuminv = $datarsv['invnumofhotel'];
      $aidnuminv = $datarsv['invnumofchain'];
      $invrawdate = $datarsv['dateofinvoice'];
  }
  
  switch(strlen($idnuminv)){
      case 1: $idnuminv='000'.$idnuminv; break;
      case 2: $idnuminv='00'.$idnuminv; break;
      case 3: $idnuminv='0'.$idnuminv; break;
  }
  
  switch(strlen($aidnuminv)){
      case 1: $aidnuminv='000'.$aidnuminv; break;
      case 2: $aidnuminv='00'.$aidnuminv; break;
      case 3: $aidnuminv='0'.$aidnuminv; break;
  }
  
  if(empty($invrawdate) or $invrawdate=='0000-00-00' or $invrawdate=='1970-01-01'){
      $invrawdate = date('Y-m-d');
  }
  $invdate = date('d-M-Y', strtotime($invrawdate));
  $invmonth = date('m', strtotime($invrawdate));
  $invyear = date('Y', strtotime($invrawdate));
  
  $stmt = $db->prepare("SELECT `invnolabel`, `notefax`, `noteemail` FROM `pmsinvoicesetting` WHERE `hoteloid`=:a");
  $stmt->execute(array(':a' => $hoteloid));
  $invset = $stmt->fetch(PDO::FETCH_ASSOC);
  $invnolabel = $invset['invnolabel'];
?>
<link rel="stylesheet" href="../../lib/bootstrap/css/bootstrap.min.css?v=<?=$fileVersion;?>">
<link rel="stylesheet" href="../style-print.css?v=<?=$fileVersion;?>">
<script type="text/javascript" src="../..//js/jquery-1.11.0.min.js"></script>
<?php  include("../component/script-view-pmslite.php"); ?>
<div id="container">
  <div id="print">
    <div class="row">
      <div class="col-md-offset-6 col-md-6">
        <h2><?=$pmslite->hoteldata['hotelname']?></h2>
        <?=$pmslite->hoteldata['address']?><br>
        Phone : <?=$pmslite->hoteldata['phone']?><br>
        E-mail : <?=$pmslite->hoteldata['email']?><br>
        Website : <?=$pmslite->hoteldata['website']?>
      </div>
    </div>
    <div class="row">
      <div class="col-md-8">
        <div class="box">
          <div class="row" style="margin-top:0">
              <div class="col-xs-6">Invoice No.</div>
              <div class="col-xs-6">: <?=$idnuminv?>/<?=$invnolabel?>-<?=$aidnuminv?>/<?=$invmonth?>/<?=$invyear?></div>
          </div>
          <div class="row" style="margin-top:0">
              <div class="col-xs-6">Date</div>
              <div class="col-xs-6">: <?=$invdate?></div>
          </div>
          <h3>BOOKING DETAILS #<?=$bookingnumber?></h3>
          <h3>Guest : <?=$datarsv['guestname']?></h3>
          <div class="row">
            <div class="col-md-6"><b>Address :</b><br><?=$datarsv['address']?></div>
            <div class="col-md-6">
              <b>Phone :</b> <?=$datarsv['phone']?><br>
              <b>E-mail :</b> <?=$datarsv['email']?>
            </div>
          </div>
          <div class="row">
            <div class="col-md-2"><b>Created on :</b><br><?=date('d M Y', strtotime($datarsv['bookingtime']))?></div>
            <div class="col-md-6">
              <table>
                <thead><tr><th>Stay Details</th><th>Room(s) / Person(s)</th></tr></thead>
                <tbody>
                    <tr>
                      <td>
                        <?php
                          foreach($pmslite->detailStay() as $staydtl){
                            echo $staydtl['room']." (".$staydtl['roomnumber'].")<br>".date('d M', strtotime($staydtl['checkin']))." - ".date('d M', strtotime($staydtl['checkout']))." (".$staydtl['night']." night)<br>";
                          }
                        ?>
                      </td>
                      <td>
                        <?php
                        $stayoccupancy = $pmslite->summaryStayOccupancy();
                        echo $stayoccupancy['jmlroom']." room(s) / ". $stayoccupancy['person']." (". $stayoccupancy['adult']." adult / ".$stayoccupancy['child']." children)";
                        ?>
                      </td>
                    </tr>
                </tbody>
              </table>
            </div>
            <div class="col-md-4">
              <b>Amount :</b>
              <br>
              <?=$datarsv['currencycode']." ".number_format($datarsv['grandtotal'] * $adjust,2)?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row"><div class="col-md-12 text-center"><h1>ACCOUNT STATEMENT</h1></div></div>
    <div class="row">
      <div class="col-md-12">
        <table class="table table-bordered">
          <thead>
            <tr><th></th><th>Date</th><th>Description-References</th><th>Folio#</th><th>Disc/Allow</th><th>Charges</th><th>Tax</th><th>Payment</th></tr>
          </thead>
          <tbody>
            <?php $no = 0; $grtotal = 0;?>
            <?php if(!($_SESSION['_typepmsusr'] == '2' || $_SESSION['_typepmsusr'] == '4')){ //VM-OM?>
            <?php
              $grtotal += $datarsv['totalroom']+$datarsv['totalextra'];
              foreach($dataroom as $dr){
            ?>
            <tr>
              <td><?=++$no?></td>
              <td><?=date('d M Y', strtotime($datarsv['bookingtime']))?></td>
              <td><?=$dr['promotion']?> <?=$dr['room']?> / <?=$pmslitedata->getRoomNumber($dr['bookingroomoid'])?></td>
              <td></td>
              <td></td>
              <td><?=$datarsv['currencycode']." ".number_format($dr['totalr'] * $adjust,2)?></td>
              <td></td>
              <td></td>
            </tr>
            <?php
              }
            ?>

            <?php
              foreach($pmslitedata->getBookingExtraData() as $key => $otherextras){
            ?>
              <tr>
                <td><?=++$no?></td>
                <td><?=date('d M Y', strtotime($datarsv['bookingtime']))?></td>
                <td>Qty <?=$otherextras['qty']?> <?=$otherextras['name']?> - Extras</td>
                <td></td>
                <td></td>
                <td><?=$datarsv['currencycode']." ".number_format($otherextras['total'] * $adjust,2)?></td>
                <td></td>
                <td></td>
              </tr>
            <?php
              }
            ?>
            <?php } //VM-OM?>
            <?php
              foreach($pmslitedata->getBookingChargeData() as $key => $othercharges){ $grtotal += $othercharges['total'];
            ?>
              <tr>
                <td><?=++$no?></td>
                <td><?=date('d M Y', strtotime($othercharges['created']))?></td>
                <td>Qty <?=$othercharges['qty']?> <?=$othercharges['product']?> - <?=$othercharges['pos']?></td>
                <td></td>
                <td></td>
                <td><?=$datarsv['currencycode']." ".number_format($othercharges['total'] * $adjust,2)?></td>
                <td></td>
                <td></td>
              </tr>
            <?php
              }
            ?>
            <?php if(!($_SESSION['_typepmsusr'] == '2' || $_SESSION['_typepmsusr'] == '4')){ //VM-OM?>
            <?php
              foreach($pmslite->paymentList() as $payment){
            ?>
              <tr>
                <td><?=++$no?></td>
                <td><?=date('d M Y', strtotime($payment['paymentdate']))?></td>
                <td><?=$payment['description']?></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td><?=$datarsv['currencycode']." ".number_format($payment['amount'] * $adjust,2)?></td>
              </tr>
            <?php
              }
            ?>
            <?php } //VM-OM?>
          </tbody>
          <tfoot>
            <tr><th colspan="3">Total</th><th></th><th></th><th><?=$datarsv['currencycode']." ".number_format($grtotal * $adjust,2)?></th><th></th>
            <th>
                <?php if(!($_SESSION['_typepmsusr'] == '2' || $_SESSION['_typepmsusr'] == '4')){ //VM-OM?>
                <?=$datarsv['currencycode']." ".number_format($datarsv['paid'] * $adjust,2)?>
                <?php } //VM-OM?>
            </th></tr>
          </tfoot>
        </table>
      </div>
    </div>
    <?php if(!($_SESSION['_typepmsusr'] == '2' || $_SESSION['_typepmsusr'] == '4')){ //VM-OM?>
    <div class="row"><div class="col-md-8 col-xs-6 text-right"><b>Booking Total</b></div><div class="col-md-4 col-xs-6"><?=$datarsv['currencycode']." ".number_format(($datarsv['totalroom']+$datarsv['totalextra']) * $adjust,2)?></div></div>
    <?php } //VM-OM?>
    <div class="row"><div class="col-md-8 col-xs-6 text-right"><b>Other Charges</b></div><div class="col-md-4 col-xs-6"><?=$datarsv['currencycode']." ".number_format($datarsv['totalcharge'] * $adjust,2)?></div></div>
    <?php if(!($_SESSION['_typepmsusr'] == '2' || $_SESSION['_typepmsusr'] == '4')){ //VM-OM?>
    <div class="row"><div class="col-md-8 col-xs-6 text-right"><b>Total Tax</b></div><div class="col-md-4 col-xs-6"></div></div>
    <div class="row"><div class="col-md-8 col-xs-6 text-right"><b>Total Disc/Allow</b></div><div class="col-md-4 col-xs-6"></div></div>
    <div class="row"><div class="col-md-8 col-xs-6 text-right"><b>Total Paid</b></div><div class="col-md-4 col-xs-6"><?=$datarsv['currencycode']." ".number_format($datarsv['paid'] * $adjust,2)?></div></div>
    <div class="row"><div class="col-md-8 col-xs-6 text-right"><b>Balance</b></div><div class="col-md-4 col-xs-6"><?=$datarsv['currencycode']." ".number_format($datarsv['paid_balance'] * $adjust,2)?></div></div>
    <?php } //VM-OM?>
  </div>
  <div class="row">
      <div class="col-xs-12">
          <h2>Print Options</h2>
          <div style="border:dashed #ccc;border-width:1px 0;padding:5px 0">
              <div class="row">
                  <div class="col-sm-2">
                      <b>BANK ACCOUNT</b>
                      <?php
                      if($_SESSION['_typeusr'] == '4' || $_SESSION['_typeusr'] == '2'){ 
                        echo '<br><a class="newbank" style="cursor:pointer">new account</a>';
                      }
                      ?>
                  </div>
                  <div class="col-sm-10">
                      <div class="row">
                    <?php
                    $stmt = $db->prepare("SELECT hoteloid, chainoid FROM `hotel` WHERE hoteloid = :a");
                    $stmt->execute(array(':a' => $datarsv['hoteloid']));
                    $xmna = $stmt->fetch(PDO::FETCH_ASSOC);
                    
                    $stmt = $db->prepare("SELECT pb.* FROM `pmsbankacc` pb WHERE (chainoid=:a or hoteloid=:b) and publishedoid='1'");
                    $stmt->execute(array(':a' => $xmna['chainoid'], ':b' => $xmna['hoteloid']));
                    $bankacc = $stmt->fetchAll(PDO::FETCH_ASSOC);
                    
                    foreach($bankacc as $xmndt){
                        echo '
                            <div class="col-sm-4" style="padding-bottom:10px">
                                <input type="checkbox" name="bacc" value="'.$xmndt['pmsbankaccoid'].'"> &nbsp; <b>Account Name: </b><span>'.$xmndt['accountname'].'</span><br>
                                <b>Bank Name: </b><span>'.$xmndt['bankname'].'</span><br>
                                <b>Account Number: </b><span>'.$xmndt['accountnumber'].'</span><br>
                                <b>Swift Code: </b><span>'.$xmndt['swiftcode'].'</span>';
                                
                        if($_SESSION['_typeusr'] == '4'){ 
                            echo '&nbsp; <a class="rembank" style="cursor:pointer" to="'.$xmndt['pmsbankaccoid'].'">remove</a>';
                        }
                                
                        echo '
                            </div>
                        ';
                    }
                    ?>
                      <div class="col-sm-4" style="padding-bottom:10px;display:none;" id="newbankc">
                        <b>Account Name: </b><span><input type="text" class="form-control input-sm" id="inaccnm"></span><br>
                        <b>Bank Name: </b><span><input type="text" class="form-control input-sm" id="inbanknm"></span><br>
                        <b>Account Number: </b><span><input type="text" class="form-control input-sm" id="inaccnbr"></span><br>
                        <b>Swift Code: </b><span><input type="text" class="form-control input-sm" id="inswcode"></span><br>
                        <?php if($_SESSION['_typeusr'] == '4' and empty($_SESSION['_hotel'])){ ?>
                        <span><input type="checkbox" id="inchain" checked> Save for All Hotel</span><br>
                        <?php } ?>
                        <input type="button" id="savebank" class="btn-xs btn btn-primary" value="Save"> &nbsp; <a class="calbank" style="cursor:pointer">cancel</a>
                      </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <div id="action-button" class="row">
    <div class="col-xs-12 text-center">
      <button type="button" name="close" class="btn btn-sm btn-default">Close</button> &nbsp; | &nbsp;
      <select id="printtype" class="form-control input-sm" style="width:200px;display:inline-block">
        <?php if($_SESSION['_typepmsusr'] == '2' || $_SESSION['_typepmsusr'] == '4'){?>
          <option value="3">View Other Charges Only</option>
        <?php }else{?>
          <option value="1">View All Booking Item</option>
          <option value="2">View Room Only</option>
          <option value="3">View Other Charges Only</option>
          <option value="4">View All Booking Item with Payment</option>
          <?php if($datarsv['bookingstatusoid']=='5' || $datarsv['bookingstatusoid']=='7'){?>
          <option value="5">View Cancellation Amount</option>
          <?php }?>
        <?php }?>
      </select>
      <button type="button" name="print-invoice-guest" class="btn btn-sm btn-primary">Print</button>
    </div>
  </div>
</div>
<form id="print-invoice-guest" action="../print/pinvoice-guest.php?new=1&nopay=1" target="_blank" method="post">
  <input name="bookingnumber" type="hidden" value="<?=$datarsv['bookingnumber']?>">
</form>
<script>
    $(function(){
        $("#printtype").change(function(){
            var x = $(this).val();
            if(x==1){
                $("#print-invoice-guest").attr("action","../print/pinvoice-guest.php?new=1&nopay=1");
            }else if(x==2){
                $("#print-invoice-guest").attr("action","../print/pinvoice-guest.php?new=1&nopay=1&nocharge=1");
            }else if(x==3){
                $("#print-invoice-guest").attr("action","../print/pinvoice-guest.php?new=1&nopay=1&noroom=1&noextra=1");
            }else if(x==4){
                $("#print-invoice-guest").attr("action","../print/pinvoice-guest.php?new=1");
            }else if(x==5){
                $("#print-invoice-guest").attr("action","../print/pinvoice-guest.php?new=2");
            }
        });
        $("#printtype").change();
        
        $(document).on('click', ".newbank", function(){
            $("#newbankc").show();
        });
        
        $(document).on('click', ".calbank", function(){
            $("#newbankc").hide();
            $("#inaccnm").val("");
            $("#inbanknm").val("");
            $("#inaccnbr").val("");
            $("#inswcode").val("");
        });
        
        $(document).on('click', "#savebank", function(){
            var a = $("#inaccnm").val();
            var b = $("#inbanknm").val();
            var c = $("#inaccnbr").val();
            var d = $("#inswcode").val();
            
            if ($('#inchain').length > 0 && $('#inchain').is(':checked')) {
                var e = '<?=$xmna['chainoid']?>';
                var f = '0';
            }else{
                var e = '0';
                var f = '<?=$xmna['hoteloid']?>';
            }
            
            if(a.trim()!="" && b.trim()!="" && c.trim()!="" && d.trim()!=""){
                $.post('<?php echo $base_url; ?>/myhotel/pms-lite/view/bank/save.php', { 'a':a, 'b':b, 'c':c, 'd':d, 'e':e, 'f':f }, function(response){
                    alert(response);
                    window.location.reload(true);
                });
            }else{
                alert("All field can't be empty!");
            }
        });
        
        $(document).on('click', ".rembank", function(){
            if (confirm("Are you sure want to remove?")){
            
            var a = $(this).attr('to');
            
            if(a.trim()!=""){
                $.post('<?php echo $base_url; ?>/myhotel/pms-lite/view/bank/delete.php', { 'a':a }, function(response){
                    alert(response);
                    window.location.reload(true);
                });
            }else{
                alert("Failed to remove data!");
            }
            
            }
        });
        
        $('input[type="checkbox"][name="bacc"]').change(function(){
            var zz = $(this).val();
            if(this.checked){
                $("#print-invoice-guest").append('<input class="xx" name="inbacc[]" type="hidden" value="'+(zz)+'">');
            }else{
                $("#print-invoice-guest").find(".xx").each(function(){
                    if($(this).val() == zz){
                        $(this).remove();
                    }
                });
            }
        });
        
    });
</script>
<?php
}catch(Exception $e){
  echo $e->getMessage();
}
?>
