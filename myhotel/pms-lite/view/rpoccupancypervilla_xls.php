<?php
session_start();
error_reporting(0);
date_default_timezone_set("Asia/Makassar");
header("Content-Type: application/xls");
header("Pragma: no-cache");
header("Expires: 0");
header("Content-Disposition: attachment; filename=ReportOccPerVilla_".date("YmdHis").".xls");

if(empty($_SESSION['_oid'])){
    die();
}

try{
    include("../../conf/connection.php");
    $year = $_POST['b'];
    
    $s_chain = "SELECT ua.oid, ua.type FROM users u INNER JOIN userassign ua USING (useroid) WHERE ua.type IN ('chainoid', 'hoteloid') AND u.useroid = '".$_SESSION['_oid']."'";
    $q_chain = $db->query($s_chain);
    $r_chain = $q_chain->fetchAll(PDO::FETCH_ASSOC);
    $rxoid = array(); $rxtype = array();
    foreach($r_chain as $row){
        $rxoid[] = $row['oid'];
        $rxtype[] = $row['type'];
    }

    $rxchain = array(); $rxhotel = array();
    foreach($rxtype as $k => $v){
        if($v == 'chainoid'){
            $rxchain[] = $rxoid[$k];
        }else if($v == 'hoteloid'){
            $rxhotel[] = $rxoid[$k];
        }
    }

    $msg = "";
    if(count($rxchain) > 0){
        $imp = implode(",", $rxchain);
        $msg .= "AND chainoid IN (".$imp.")";
    }
    if(count($rxhotel) > 0){
        $imp = implode(",", $rxhotel);
        $msg .= "AND hoteloid IN (".$imp.")";
    }

    if($msg != ""){
        $stmt = $db->prepare("SELECT hoteloid, hotelcode, hotelname FROM hotel WHERE publishedoid='1' ".$msg." ORDER BY hotelname");
        $stmt->execute(array());
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $qx = array();
        foreach($data as $vals){
            $qx[] = $vals['hoteloid'];
        }
        $in = implode(",",$qx);
        $query .= "AND r.hoteloid IN (".$in.")";
    }else{
        die();
    }

    $stmt = $db->prepare("SELECT hoteloid, hotelname, count(`roomnumberoid`) AS numofroom
        FROM hotel h
            LEFT JOIN `room` r USING(`hoteloid`)
            LEFT JOIN `roomnumber` rn USING(`roomoid`)
        WHERE h.`publishedoid` not in (3) AND r.`publishedoid` not in (3) AND rn.`publishedoid` not in (3) ".$query." 
        GROUP BY hoteloid, hotelname");
    $stmt->execute();
    $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
    $data_room_n = array(); $data_room_s = array(); $data_room_m = array();
    foreach($r_room as $room){
        $data_room_n[$room['hoteloid']] = $room['numofroom'];
        $data_room_m[$room['hoteloid']] = $room['hotelname'];
    }

    $stmt = $db->prepare("SELECT r.hoteloid, MONTH(bdt.date) as bulan, COUNT(bdt.`bookingroomdtloid`) as roomsold 
        FROM `bookingroom` br INNER JOIN `booking` b USING(`bookingoid`) INNER JOIN `hotel` r USING(`hoteloid`) INNER JOIN `bookingroomdtl` bdt USING(`bookingroomoid`) 
        WHERE br.`pmsstatusoid`!=0 AND `bookingstatusoid` in (4) AND br.pmsstatusoid in (2,4,5,6) ".$query." AND YEAR(bdt.date)='".$year."'
        GROUP BY r.hoteloid, bulan");
    $stmt->execute();
    $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $data_room_v = array();
    foreach($r_room as $room){
        $data_room_v[$room['hoteloid']][$room['bulan']] = $room['roomsold'];
    }

    $armonth = array("Jan"=>1,"Feb"=>2,"Mar"=>3,"Apr"=>4,"May"=>5,"Jun"=>6,"Jul"=>7,"Aug"=>8,"Sep"=>9,"Oct"=>10,"Nov"=>11,"Dec"=>12);

}catch(PDOException $ex){
    echo "Failed to load the data!"; //. $e->getMessage();
    die();
}
?>
<table>
    <tr>
        <th><b>OCCUPANCY PER VILLA REPORT</b></th>
    </tr>
    <tr>
        <th>YEAR : <?=$year?></th>
    </tr>
    <tr>
        <th></th>
    </tr>
</table>
<table id="example" class="display" cellspacing="0" width="100%">
<thead>
    <tr>
        <th>#</th>
        <th>Villa</th>
        <?php 
        foreach($armonth as $k => $v){
            echo '<th>'.$k.'</th>';
        }
        ?>
        <th>YTD</th>
    </tr>
</thead>
<tbody>
    <?php
    $no = 0; $month_oc = array(); $month_nm = array();
    foreach($data_room_m as $mk => $mv){
        $no++;
        $hotelname = $mv;
        $ytd = 0;

        echo '
            <tr>
                <td align="center">'.$no.'</td>
                <td>'.$hotelname.'</td>';

            $values = array();
            
            foreach($armonth as $k => $v){
                $roomsold = empty($data_room_v[$mk][$v]) ? 0 : intval($data_room_v[$mk][$v]);
                $numofroom = $data_room_n[$mk];
                $dofm = cal_days_in_month(CAL_GREGORIAN, $v, $year);
                $occ = round(($roomsold*100)/($dofm*$numofroom),2);
                echo '<td align="center">'.$occ.'%</td>';
                $values[] = $occ;

                $epl = explode(" ", $hotelname);
                $sepl = (count($epl)<=1) ? $epl[0] : $epl[0]." ".$epl[1];
                $month_oc[$v][] = $occ;
                $month_nm[$v][] = $sepl;
            }
            
            $ytd = round(array_sum($values)/count($values),2);

            $epl = explode(" ", $hotelname);
            $sepl = (count($epl)<=1) ? $epl[0] : $epl[0]." ".$epl[1];
            $month_oc[13][] = $ytd;
            $month_nm[13][] = $sepl;

        echo '  
                <td align="center">'.$ytd.'%</td>
            </tr>
        ';

    }
    ?>
</tbody>
</table>