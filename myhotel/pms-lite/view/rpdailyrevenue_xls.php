<?php
session_start();
error_reporting(0);
date_default_timezone_set("Asia/Makassar");
header("Content-Type: application/xls");
header("Content-Disposition: attachment; filename=ReportDailyRevenue_".date("YmdHis").".xls");
header("Pragma: no-cache");
header("Expires: 0");

if(empty($_SESSION['_oid'])){
    die();
}

  include("../../conf/connection.php");
  include("../../includes/php-function.php");
  include("../../includes/pms-lite/class-pms-lite.php");
  $pmslite = new PMSLite($db);

  $lastdate = $_POST['lastdate'];
  $channel = 1;
  switch($_POST['request']){
    case 'next' : $begin = 1 ; $end = 7; break;
    case 'forward' : $begin = 7 ; $end = 13; break;
    case 'previous' : $begin = -1 ; $end = 5; break;
    case 'backward' : $begin = -7 ; $end = -1; break;
    default :  $begin = 0; $end = 6; break;
  }

  $showDay = array(); $showDate = array(); $showMonth = array(); $dateArr = array();

  for($i = $begin; $i <= $end; $i++){
    $date = date("Y-m-d",strtotime($lastdate." +".$i." day"));
    $dateformated = date("D-d-M",strtotime($lastdate." +".$i." day"));
    $exploDate = explode("-", $dateformated);

    array_push($dateArr, $date);
    array_push($showDay, $exploDate[0]);
    array_push($showDate, $exploDate[1]);
    array_push($showMonth, $exploDate[2]);
  }

  $pmslite->setPeriodePMS(date('Y-m-d', strtotime($lastdate.' +'.$begin." day")), date('Y-m-d', strtotime($lastdate.' +'.$end." day")));

  /*----------------------------------------------------------------------*/

  $s_hotelchain = "select h.hoteloid, h.hotelcode, h.hotelname from hotel h inner join chain c using (chainoid) where h.chainoid in (select oid from userassign where type = 'chainoid' and useroid = '".$_SESSION['_oid']."') and h.publishedoid = '1'";
  $filter = array();
  if(!empty($_POST['hotelcode'])){
    array_push($filter, "h.hotelcode = '".$_POST['hotelcode']."'");
  }
  if(count($filter) > 0){
    $combine_filter = implode(' and ',$filter);
    $s_hotelchain = $s_hotelchain.' and '.$combine_filter;
  }
  $q_hotelchain = $db->query($s_hotelchain);
  $r_hotelchain = $q_hotelchain->fetchAll(PDO::FETCH_ASSOC);
  ?>
  <table id="inventory" cellpadding="0" cellspacing="0" border="1">
    <tbody>
      <?php
      /*---------------------------------------------------------------------*/
      foreach($r_hotelchain as $hotelchain){
        $pmslite->setPMSHotel($hotelchain['hoteloid']);
        $roomTypeTable = $pmslite->roomTypeTable();
      /*---------------------------------------------------------------------*/
      ?>
      <tr class="hotel"><td colspan="8" style="text-transform: uppercase;"><?php echo $hotelchain['hotelname']; ?></td></tr>
      <tr class="date">
        <th>&nbsp;</th>
        <?php
        foreach($dateArr as $key => $date){
          if($key == 0){
        ?>
        <input type="hidden" name="spotdate" value="<?=$date?>" />
        <?php
          }
        ?>
        <th class="text-center"><?php echo $showDay[$key]; ?><br /><?php echo $showDate[$key]; ?><br /><?php echo $showMonth[$key]; ?></th>
        <?php
        }
        ?>
      </tr>
      <?php
      /*---------------------------------------------------------------------*/
      foreach($roomTypeTable as $roomtypeRow){
        $roomAllocation = $pmslite->roomAllocation($roomtypeRow['id']);
        $dataBookedRoom = $pmslite->dataBookedRoom($roomtypeRow['id'], "booked");
        $dataInHouseRoom = $pmslite->dataBookedRoom($roomtypeRow['id'], "inhouse");
        $roomRevenue = $pmslite->dataInhouseRevenue($roomtypeRow['id']);
      /*---------------------------------------------------------------------*/
      ?>
      <tr class="roomtype"><td colspan="8"><?=$roomtypeRow['name']?></td></tr>
      <tr>
        <td>Allocation</td>
        <?php foreach($dateArr as $key){ echo '<td>'.$roomAllocation.'</td>'; } ?>
      </tr>
      <tr>
        <td>Booked</td>
        <?php foreach($dataBookedRoom as $booked){ echo '<td>'.$booked.'</td>'; } ?>
      </tr>
      <tr>
        <td>Available</td>
        <?php foreach($dataBookedRoom as $booked){ echo '<td>'.($roomAllocation-$booked).'</td>'; } ?>
      </tr>
      <!--
      <tr>
        <td>In-House</td>
        <?php foreach($dataInHouseRoom as $inhouse){ echo '<td>'.$inhouse.'</td>'; } ?>
      </tr>
      -->
      <tr>
        <td><b>Revenue</b></td>
        <?php foreach($roomRevenue as $revenue){ echo '<td style="vertical-align:top">'.$revenue.'</td>'; } ?>
      </tr>
    <?php
      }
    ?>
    <tr class="hotel">
      <td>Total Revenue <?=$hotelchain['hotelname']?></td>
      <?php
        $hotelRevenue = $pmslite->dataInhouseHotelRevenue();
        foreach($hotelRevenue as $revenue){ echo $revenue; }
      ?>
    </tr>
    <tr><td colspan="8">&nbsp;</td></tr>
    <?php
    }
    /*----------------------------------------------------*/
    if(empty($_POST['hotelcode'])){
    ?>

    <tr class="hotel">
      <td>Total Revenue All Hotel</td>
      <?php
        $chainRevenue = $pmslite->dataInhouseChainRevenue($_SESSION['_oid']);
        foreach($chainRevenue as $revenue){ echo $revenue; }
      ?>
    </tr>
    <?php
    }
    ?>
    </tbody>
  </table>
