<?php
session_start();
date_default_timezone_set("Asia/Makassar");
header("Content-Type: application/xls");
header("Content-Disposition: attachment; filename=IncomeReport_".date("YmdHis").".xls");
header("Pragma: no-cache");
header("Expires: 0");

if(empty($_SESSION['_oid'])){
    die();
}

include("../../conf/connection.php");
include("../../includes/pms-report/function_report.php");

$month = $_POST['a'];
$year = $_POST['b'];
$hoteloid = $_POST['c'];

$stmt = $db->prepare("SELECT hoteloid, hotelcode, hotelname FROM hotel WHERE hoteloid=:a AND publishedoid='1' ORDER BY hotelname");
$stmt->execute(array(':a' => $hoteloid));
$hotel = $stmt->fetch(PDO::FETCH_ASSOC);

?>
<table id="example" class="display" cellspacing="0" width="100%">
<thead>
    <tr>
        <th rowspan="2">No</th>
        <th rowspan="2" style="width: 32px;">Total Room</th>
        <th rowspan="2" style="width: 120px;">Guest Name</th>
        <th rowspan="2">In</th>
        <th rowspan="2">Out</th>
        <th rowspan="2">Total R/N</th>
        <th rowspan="2" colspan="2">Rate/Night</th>
        <th rowspan="2">Total Rate</th>
        <th colspan="4">Distribution Rate</th>
        <th rowspan="2">Payment Received</th>
        <th rowspan="2">Booked by</th>
        <th rowspan="2">Payment</th>
        <th rowspan="2">Remarks</th>
    </tr>
    <tr>
        <th class="bleft">Tax-10%</th>
        <th>SC-5%</th>
        <th>CC Charge-3%</th>
        <th>Owner Rev</th>
    </tr>
</thead>
<tbody>
    <?php
    $armonth = array("January"=>1,"February"=>2,"March"=>3,"April"=>4,"May"=>5,"June"=>6,"July"=>7,"August"=>8,"September"=>9,"October"=>10,"November"=>11,"December"=>12);
    $data = getReservationDataIncomeRange($db, $hoteloid, $month, $year);
    $titles = getCustTitle($db);

    $no=0; $tdiff=0; $tconv=0;
    $tx=$sc=$nb=$rv=$gt=array('idr'=>0, 'usd'=>0);

    foreach($data as $row){
        $no++;
        $totalroom = 1;
        if(isset($titles[$row['title']])) $title = $titles[$row['title']]; else $title = $row['title'];
        $guestname = $title." ".$row['firstname']." ".$row['lastname'];
        $checkin = date("d/m/Y", strtotime($row['checkin']));
        $checkout = date("d/m/Y", strtotime($row['checkout']));
        $diff = $row['diff'];
        $ratepernight = number_format($row['total']/$diff, 2, '.', ',');
        $currcode = empty($row['currencycode']) ? "IDR" : $row['currencycode'];
        $grandtotal = number_format($row['total'], 2, '.', ',');
        $tax10 = $row['total'] * 10.5/115.5;
        $sc5 = $row['total'] * 5/115.5;
        if($row['agentoid'] == '92'){ $cc3 = $row['total']*0.03; }else{ $cc3 = 0; }

        if($currcode == "IDR"){
          $tax10 = round($tax10);
          $sc5  = round($sc5);
          $cc3 = round($cc3);
        }
        $rev = $row['total'] - $tax10 - $sc5 - $cc3;
        $ngsreceive = $row['total'] - $cc3;

        if($currcode == 'USD'){
            $gt['usd'] += $row['total'];
            $tx['usd'] += $tax10;
            $sc['usd'] += $sc5;
            $cc['usd'] += $cc3;
            $rv['usd'] += $rev;
            $nr['usd'] += $ngsreceive;
        }else{
            $gt['idr'] += $row['total'];
            $tx['idr'] += $tax10;
            $sc['idr'] += $sc5;
            $cc['idr'] += $cc3;
            $rv['idr'] += $rev;
            $nr['idr'] += $ngsreceive;
        }

        $tax10 = number_format($tax10, 2, '.', ',');
        $sc5 = number_format($sc5, 2, '.', ',');
        $cc3 = number_format($cc3, 2, '.', ',');
        $rev = number_format($rev, 2, '.', ',');
        $ngsreceive = number_format($ngsreceive, 2, '.', ',');

        $curexc = '13,000';
        if($currcode == "USD"){
            $totalconv = number_format(($row['total'] - ($row['total']*0.25)) * 13000, 2, '.', ',');
        }else{
            $totalconv = $rev;
        }
        $agentname = empty($row['agentname']) ? "-" : $row['agentname'];
        if($row['bookingstatusoid'] == "5"){
            $status = "Cancelled";
        }else if($row['bookingstatusoid'] == "7"){
            $status = "No Show";
        }else{
            $status = $row['status'];
        }
        $bookingoid = $row['bookingoid'];

        $stmt = $db->prepare("SELECT `bookingoid`, `currencycode`, SUM(`total`) AS total
            FROM `bookingpaymentdtl` LEFT JOIN `currency` USING(`currencyoid`) WHERE `bookingoid`=:a GROUP BY `bookingoid`, `currencycode`");
        $stmt->execute(array(':a' => $bookingoid));
        $payment = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $totalpay = 0;
        foreach($payment as $pay){
            if($currcode == 'USD'){
                $totalpay += ($pay['total'] * 13000);
            }else{
                $totalpay += $pay['total'];
            }
        }
        if($totalpay >= $row['total']){
            $mtotalpay = "Paid";
        }else if($totalpay > 0){
            $mtotalpay = "Partially";
        }else{
            $mtotalpay = "-";
        }

        echo '
            <tr>
                <td align="center">'.$no.'</td>
                <td align="center">'.$totalroom.'</td>
                <td>'.$guestname.'</td>
                <td>'.$checkin.'</td>
                <td>'.$checkout.'</td>
                <td align="center">'.$diff.'</td>
                <td>'.$currcode.'</td>
                <td class="bnoleft" align="right">'.$ratepernight.'</td>
                <td align="right">'.$grandtotal.'</td>
                <td align="right">'.$tax10.'</td>
                <td align="right">'.$sc5.'</td>
                <td align="right">'.$cc3.'</td>
                <td align="right">'.$rev.'</td>
                <td align="right">'.$ngsreceive.'</td>
                <td>'.$agentname.'</td>
                <td align="center">'.$mtotalpay.'</td>
                <td>'.$row['roomnumber']." - ".$status.'</td>
            </tr>
        ';

        $tdiff += $diff;
    }
    ?>
</tbody>
<tfoot>
    <tr>
        <th colspan="5">GRAND TOTAL</th>
        <th><?=$tdiff?></th>
        <th>IDR</th>
        <th></th>
        <th align="right"><?=number_format($gt['idr'], 2, '.', ',')?></th>
        <th align="right"><?=number_format($tx['idr'], 2, '.', ',')?></th>
        <th align="right"><?=number_format($sc['idr'], 2, '.', ',')?></th>
        <th align="right"><?=number_format($cc['idr'], 2, '.', ',')?></th>
        <th align="right"><?=number_format($rv['idr'], 2, '.', ',')?></th>
        <th align="right"><?=number_format($nr['idr'], 2, '.', ',')?></th>
        <th></th>
        <th></th>
        <th></th>
    </tr>
    <tr>
        <th colspan="5"></th>
        <th></th>
        <th>USD</th>
        <th></th>
        <th align="right"><?=number_format($gt['usd'], 2, '.', ',')?></th>
        <th align="right"><?=number_format($tx['usd'], 2, '.', ',')?></th>
        <th align="right"><?=number_format($sc['usd'], 2, '.', ',')?></th>
        <th align="right"><?=number_format($cc['usd'], 2, '.', ',')?></th>
        <th align="right"><?=number_format($rv['usd'], 2, '.', ',')?></th>
        <th align="right"><?=number_format($nr['usd'], 2, '.', ',')?></th>
        <th></th>
        <th></th>
        <th></th>
    </tr>
</tfoot>
</table>
