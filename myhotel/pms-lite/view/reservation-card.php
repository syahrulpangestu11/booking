<?php
try{
  session_start();
	error_reporting(E_ALL ^ E_NOTICE);
  include ('../../conf/connection.php');
  include("../../includes/pms-lite/class-pms-lite.php");

  $hoteloid = $_POST['hotel'];
  $bookingroomdtloid = $_POST['bookingroomdtl'];

  $pmslite = new PMSLite($db);
  $pmslite->setPMSHotel($hoteloid);
  $data = $pmslite->getBookingRoom($bookingroomdtloid);
  /*--------------------------------------*/
  $fileVersion = date("Y.m.d.H.i.s");
?>
<link rel="stylesheet" href="../../lib/bootstrap/css/bootstrap.min.css?v=<?=$fileVersion;?>">
<link rel="stylesheet" href="../style-print.css?v=<?=$fileVersion;?>">
<script type="text/javascript" src="../..//js/jquery-1.11.0.min.js"></script>
<?php  include("../component/script-view-pmslite.php"); ?>
<div id="container">
  <div id="print">
    <div class="row">
      <div class="col-md-offset-6 col-md-6">
        <h2><?=$pmslite->hoteldata['hotelname']?></h2>
        <?=$pmslite->hoteldata['address']?><br>
        Phone : <?=$pmslite->hoteldata['phone']?><br>
        E-mail : <?=$pmslite->hoteldata['email']?><br>
        Website : <?=$pmslite->hoteldata['website']?>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6"><h2>Check-in Card</h2></div>
      <div class="col-md-6"><h2>Reservation Number # <?=$data['bookingnumber']?></h2></div>
    </div>
    <div class="box">
      <div class="row">
        <div class="col-md-6"><h3>Room Type : <?=(empty($data['room'])?$data['roomname']:$data['room'])?></h3></div>
        <div class="col-md-6"><h3>Room Number : <?=$data['roomnumber']?></h3></div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <table class="table table-bordered">
          <tr valign="top">
            <td width="50%">
              Guest : <b><?=$data['ownername']?></b>
              <br>
              <?php
              if(!empty($data['guestname'])){
                echo "Guest Room Detail : <b>".$data['guestname']."</b>";
              }else{
                echo "<b>Guest detail not filled.</b>";
              }
              ?>
            </td>
            <td width="50%">
              <table width="100%">
                <tr><td>Date(s)</td><td>: <b><?=date('d M Y', strtotime($data['checkin']))?> - <?=date('d M Y', strtotime($data['checkout']))?></b></td></tr>
                <tr><td>Night(s)</td><td>: <b><?=$data['night']?></b></td></tr>
                <tr><td>Person(s)</td><td>: <b><?=$data['adult']?> adult &amp; <?=$data['child']?> child</b></td></tr>
                <tr>
                  <td colspan="2">
                    <div class="box">
                    <?php if($_POST['show'] == "hide-price"){ ?>
                      <div class="row">
                        <div class="col-xs-12 text-center"><h2>Guest Booking Price</h2></div>
                      </div>
                    <?php }else{ ?>
                      <div class="row">
                        <div class="col-xs-6">Booking Charges<br><i>(Seasonal Rate)</i></div>
                        <div class="col-xs-6 text-right"><b><?=$data['currency']?> <?=number_format($data['total'])?></b></div>
                      </div>
                      <div class="row">
                        <div class="col-xs-6">Total Tax</div>
                        <div class="col-xs-6 text-right"><b>-</b></div>
                      </div>
                      <div class="row">
                        <div class="col-xs-6"><b>Total Amount</b></div>
                        <div class="col-xs-6 text-right"><b><?=$data['currency']?> <?=number_format($data['total'])?></b></div>
                      </div>
                      <div class="row">
                        <div class="col-xs-6"><b>Amount Paid<b></div>
                        <div class="col-xs-6 text-right"><b><?=$data['currency']?> <?=number_format($data['deposit'])?></b></div>
                      </div>
                      <div class="row">
                        <div class="col-xs-6"><b>Balance<b></div>
                        <div class="col-xs-6 text-right"><b><?=$data['currency']?> <?=number_format($data['balance'])?></b></div>
                      </div>
                    <?php } ?>
                    </div>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-6">
        <b>Date: </b><?=date('d M Y')?>
      </div>
      <div class="col-md-offset-3 col-md-3 text-center">
        <b>Signature</b>
        <br><br><br><br><br>
      </div>
    </div>
  </div>
  <div id="action-button" class="row">
    <div class="col-xs-12 text-center">
      <button type="button" name="close" class="btn btn-sm btn-default">Close</button>
      <button type="button" name="print-reservation-card" class="btn btn-sm btn-primary">Print</button>
      <?php if($_POST['show'] == "hide-price"){ ?>
        <button type="button" name="show-reservation-card" value="show-price" class="btn btn-sm btn-warning">Show Price</button>
      <?php }else{ ?>
        <button type="button" name="show-reservation-card" value="hide-price" class="btn btn-sm btn-warning">Hide Price</button>
      <?php } ?>
    </div>
  </div>
</div>
<form id="reservation-card" action="reservation-card.php" method="post">
  <input name="bookingroomdtl" type="hidden" value="<?=$data['bookingroomdtloid']?>">
  <input name="hotel" type="hidden" value="<?=$hoteloid?>">
  <input type="hidden" name="show" value="<?=$_POST['show']?>">
</form>
<form id="print-reservation-card" action="../print/preservation-card.php" target="_blank" method="post">
  <input name="bookingroomdtl" type="hidden" value="<?=$data['bookingroomdtloid']?>">
  <input name="hotel" type="hidden" value="<?=$hoteloid?>">
  <input type="hidden" name="show" value="<?=$_POST['show']?>">
</form>
<?php
}catch(Exception $e){
  echo $e->getMessage();
}
?>
