<?php
session_start();
error_reporting(0);
date_default_timezone_set("Asia/Makassar");
header("Content-Type: application/xls");
header("Pragma: no-cache");
header("Expires: 0");

if(empty($_SESSION['_oid'])){
    die();
}

  include("../../conf/connection.php");

  $filter = array();

  if(!empty($_POST['hotel'])){
    $hoteloid = $_POST['hotel'];
    $stmt = $db->prepare("SELECT hoteloid, hotelcode, hotelname FROM hotel WHERE hoteloid=:a AND publishedoid='1' ORDER BY hotelname");
    $stmt->execute(array(':a' => $hoteloid));
    $hotel = $stmt->fetch(PDO::FETCH_ASSOC);
    array_push($filter, "b.hoteloid = '".$hoteloid."'");
    header("Content-Disposition: attachment; filename=ReportProductionAgent_".$hotel['hotelname']."_".date("YmdHis").".xls");
  }else{
    $chainoid = $_POST['chain'];
    $stmt = $db->prepare("SELECT chainoid, name as chainname FROM chain WHERE chainoid=:a");
    $stmt->execute(array(':a' => $chainoid));
    $chain = $stmt->fetch(PDO::FETCH_ASSOC);
    array_push($filter, "h.chainoid = '".$chainoid."'");
    header("Content-Disposition: attachment; filename=ReportProductionAgent_".$chain['chainname']."_".date("YmdHis").".xls");
  }



  if($_POST['bdall'] == 1){
    $start_book = date('Y-m-d',strtotime($_POST['bstartdate']));
    $end_book = date('Y-m-d',strtotime($_POST['benddate']));
    array_push($filter, "(DATE(b.bookingtime) >= '".$start_book."' and DATE(b.bookingtime) <= '".$end_book."')");
  }
  if($_POST['ciall'] == 1){
    $start_arrival = date('Y-m-d',strtotime($_POST['cstartdate']));
    $end_arrival = date('Y-m-d',strtotime($_POST['cenddate']));
    array_push($filter, "(brd.date between '".$start_arrival."' and '".$end_arrival."') and brd.pmsreconciled = '0' and brd.reconciled = '0'");
  }

  if(count($filter)){ $query_filter = ' and '.implode(' and ', $filter); }else{  $query_filter = ''; }

  $q_agent_report = "select bm.bm_name as market, IFNULL(a.agentname, '-') as agentname, count(distinct(b.bookingoid)) as jmlbooking, count(brd.bookingroomdtloid) as roomnight, SUM(CASE WHEN br.currencyoid = 1 THEN brd.total ELSE 0 END) AS total_idr, SUM(CASE WHEN br.currencyoid = 2 THEN brd.total ELSE 0 END) AS total_usd from booking b left join bookingmarket bm using (bookingmarketoid) inner join hotel h using (hoteloid) left join agent a using (agentoid) inner join bookingroom br using (bookingoid) inner join bookingroomdtl brd using (bookingroomoid) where b.bookingstatusoid = '4' and br.pmsstatusoid in ('2', '4','5','6') ".$query_filter." group by bm.bookingmarketoid, a.agentoid";

  $stmt = $db->prepare($q_agent_report);
  $stmt->execute();
  $r_agent_report = $stmt->fetchAll(PDO::FETCH_ASSOC);

?>
<table id="example" class="display" cellspacing="0" width="100%" border="0">
  <thead>
    <tr><th colspan="6">Production by Agent Report</th></tr>
    <tr>
      <th colspan="6"><?php if(!empty($_POST['hotel'])){ echo $hotel['hotelname']; }else{ echo $chain['chainname']; } ?></th>
    </tr>
    <tr>
      <th colspan="6">
        <?php if($_POST['bdall'] == 1){ ?>Booking Periode: <?=date('d F Y',strtotime($_POST['bstartdate']))?> - <?=date('d F Y',strtotime($_POST['benddate']))?><br><?php } ?>
        <?php if($_POST['ciall'] == 1){ ?>Stay Periode : <?=date('d F Y',strtotime($_POST['cstartdate']))?> - <?=date('d F Y',strtotime($_POST['cenddate']))?><?php } ?>
      </th>
    </th>
    <tr><th colspan="6"></th></tr>
  </thead>
</table>

<table id="example" class="display" cellspacing="0" width="100%" border="1">
<thead>
  <tr>
      <th rowspan="2">Market</th>
      <th rowspan="2">Agent</th>
      <th rowspan="2">Total Reservation</th>
      <th rowspan="2">Total Room Night</th>
      <th colspan="2">Amount</th>
  </tr>
  <tr>
      <th>IDR</th>
      <th>USD</th>
  </tr>
</thead>
<tbody>
    <tr>
        <td>&nbsp;</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <?php
    foreach($r_agent_report as $ar){
      $a1+=$ar['jmlbooking'];
      $a2+=$ar['roomnight'];
      $a3+=$ar['total_idr'];
      $a4+=$ar['total_usd'];
    ?>
      <tr>
        <td><?=$ar['market']?></td>
        <td><?=$ar['agentname']?></td>
        <td align="right"><?=$ar['jmlbooking']?></td>
        <td align="right"><?=$ar['roomnight']?></td>
        <td align="right"><?php echo number_format($ar['total_idr'], 2, ',', '.'); ?></td>
        <td align="right"><?php echo number_format($ar['total_usd'], 2, ',', '.'); ?></td>
      </tr>
    <?php
    }
    ?>
    <tr>
        <td>&nbsp;</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
</tbody>
<tfoot>
  <tr>
    <th colspan="2">TOTAL</th>
    <th align="right"><?=$a1?></th>
    <th align="right"><?=$a2?></th>
    <th align="right"><?=number_format($a3, 2, ',', '.')?></th>
    <th align="right"><?=number_format($a4, 2, ',', '.')?></th>
  </tr>
</tfoot>
</table>
