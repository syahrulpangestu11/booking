<script type="text/javascript">
$(function(){

  $('body').on('click', 'button[name="close"]', function() {
    window.top.close();
  });

  // -RESERVATION CARD

  $('body').on('click', 'button[name="show-reservation-card"]', function() {
    form = $('form#reservation-card');
    form.find('input[name=show]').val($(this).val());
    form.submit();
  });

  $('body').on('click', 'button[name="print-reservation-card"]', function() {
    form = $('form#print-reservation-card');
    show = $('form#reservation-card').find('input[name=show]').val();
    form.find('input[name=show]').val(show);
    form.submit();
  });

  // -INVOICE Agent
  $('body').on('click', 'button[name="print-invoice-agent"]', function() {
    form = $('form#print-invoice-agent');
    form.find('input[name=show]').val($(this).val());
    form.submit();
  });

  // -INVOICE Guest
  $('body').on('click', 'button[name="print-invoice-guest"]', function() {
    form = $('form#print-invoice-guest');
    form.find('input[name=show]').val($(this).val());
    form.submit();
  });

});
</script>
