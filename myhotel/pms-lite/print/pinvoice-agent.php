<?php
session_start();
ob_start();
require_once('../../lib/tcpdf/tcpdf.php');
include('../../conf/connection.php');
include("../../includes/pms-lite/class-pms-lite.php");

$hoteloid = $_SESSION['_hotel'];
$bookingnumber = $_POST['bookingnumber'];

$pmslite = new PMSReservation($db);
$pmslite->setBooking($bookingnumber);
$datarsv = $pmslite->getBookingData();
$pmslite->setPMSHotel($datarsv['hoteloid']);

$pmslitedata = new PMSEditRSV($db);
$pmslitedata->setBooking($bookingnumber);
$dataroom = $pmslitedata->getBookingRoomData();

$agent = $pmslite->agentDetail($datarsv['agentoid']);
/*--------------------------------------*/
$fileVersion = date("Y.m.d.H.i.s");

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('PMS Lite Powered by TheBuking');
$pdf->SetTitle('Invoce to Agent '.$datarsv['bookingnumber']);
$pdf->SetSubject('Invoce to Agent '.$datarsv['bookingnumber']);
$pdf->SetKeywords('Invoce to Agent', 'invoice', $datarsv['bookingnumber']);

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set margins
$pdf->SetMargins(8, 10, 8, true);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

$pdf->SetFont('dejavusans', '', 8);

$pdf->AddPage();

$html = '
    <table width="100%" border="0">
        <tr>
            <td>&nbsp;</td>
            <td>
              <h2>'.$pmslite->hoteldata['hotelname'].'</h2>
              '.$pmslite->hoteldata['address'].'<br>Phone : '.$pmslite->hoteldata['phone'].'<br>E-mail : '.$pmslite->hoteldata['email'].'<br>Website : '.$pmslite->hoteldata['website'].'
            </td>
        </tr>
    </table><br><br>
    <table style="border:1px solid #000; padding:3px;">
        <tr><td colspan="4"><h2>BOOKING DETAILS</h2></td></tr>
        <tr><td colspan="4"><b>Agent Detail : </b><br><b>'.$agent['agentname'].' - '.$agent['phone'].' / '.$agent['email'].'</b></td></tr>
        <tr>
          <td colspan="2"><b>Guest Name :</b>'.$datarsv['guestname'].'<br><b>Address :</b><br>'.$datarsv['address'].'</td>
          <td colspan="2"><b>Phone :</b>'.$datarsv['phone'].'<br><b>E-mail :</b> '.$datarsv['email'].'</td>
        </tr>
        <tr>
          <td><b>Created on</b><br>'.date('d M Y', strtotime($datarsv['bookingtime'])).'</td>
          <td><b>Stay Details</b><br>';
          foreach($pmslite->detailStay() as $staydtl){
            $html.= $staydtl['room']." (".$staydtl['roomnumber'].")<br>".date('d M', strtotime($staydtl['checkin']))." - ".date('d M', strtotime($staydtl['checkout']))." (".$staydtl['night']." night)<br>";
          }
    $html.= '</td><td><b>Room(s) / Person(s)</b><br>';
          $stayoccupancy = $pmslite->summaryStayOccupancy();
          $html.= $stayoccupancy['jmlroom']." room(s) / ". $stayoccupancy['person']." (". $stayoccupancy['adult']." adult / ".$stayoccupancy['child']." children)";
    $html.= '
          </td>
          <td><b>Amount :</b><br>'.$datarsv['currencycode'].' '.number_format($datarsv['grandtotal']).'</td>
        </tr>
    </table><br>
    <h1 style="text-align:center">ACCOUNT STATEMENT</h1>
    <table width="100%" border="1" style="border:1px solid #000; padding:3px;">
      <thead>
        <tr style="text-align:center; font-weight:bold; background-color:gray;"><th></th><th>Date</th><th>Description-References</th><th>Folio#</th><th>Disc/Allow</th><th>Charges</th><th>Tax</th><th>Payment</th></tr>
      </thead>
      <tbody>';
      $no = 0;

      foreach($dataroom as $dr){
        $html.= '
        <tr>
          <td>'.++$no.'</td>
          <td>'.date('d M Y', strtotime($datarsv['bookingtime'])).'</td>
          <td>'.$dr['promotion'].' '.$dr['room'].' / '.$pmslitedata->getRoomNumber($dr['bookingroomoid']).'</td>
          <td></td>
          <td></td>
          <td>'.$datarsv['currencycode'].' '.number_format($dr['totalr']).'</td>
          <td></td>
          <td></td>
        </tr>';
      }

      foreach($pmslitedata->getBookingChargeData() as $key => $othercharges){
        $html.= '
        <tr>
          <td>'.++$no.'</td>
          <td>'.date('d M Y', strtotime($othercharges['created'])).'</td>
          <td> Qty'.$othercharges['qty']." ".$othercharges['product']." - ".$othercharges['pos'].'</td>
          <td></td>
          <td></td>
          <td>'.$datarsv['currencycode']." ".number_format($othercharges['total']).'</td>
          <td></td>
          <td></td>
        </tr>';
      }

      foreach($pmslite->paymentList() as $payment){
        $html.= '
        <tr>
          <td>'.++$no.'</td>
          <td>'.date('d M Y', strtotime($payment['paymentdate'])).'</td>
          <td>'.$payment['note'].'</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td>'.$datarsv['currencycode'].' '.number_format($payment['amount']).'</td>
        </tr>';
      }
    $html.= '
      </tbody>
      <tfoot>
        <tr><th colspan="3" align="right"><b>Total</b></th><th></th><th></th><th></th><th></th><th>'.$datarsv['currencycode'].' '.number_format($datarsv['paid']).'</th></tr>
      </tfoot>
    </table>
    <br><br>
    <table>
      <tr>
        <td>&nbsp;</td>
        <td>
          <table>
            <tr><td><b>Booking Total</b></td><td>'.$datarsv['currencycode'].' '.number_format($datarsv['totalroom']+$datarsv['totalextra']).'</td></tr>
            <tr><td><b>Other Charges</b></td><td>'.$datarsv['currencycode'].' '.number_format($datarsv['totalcharge']).'</td></tr>
            <tr><td><b>Total Tax</b></td><td></td></tr>
            <tr><td><b>Total Disc/Allow</b></td><td></td></tr>
            <tr><td><b>Total Paid</b></td><td>'.$datarsv['currencycode'].' '.number_format($datarsv['paid']).'</td></tr>
            <tr><td><b>Balance</b></td><td>'.$datarsv['currencycode'].' '.number_format($datarsv['paid_balance']).'</td></tr>
          </table>
        </td>
      </tr>
    </table>';
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->lastPage();
// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('ReservationCard_'.$datarsv['bookingnumber'].'.pdf', 'I');
?>
