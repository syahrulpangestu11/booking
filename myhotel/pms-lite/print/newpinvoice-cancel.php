<?php
error_reporting(0);
function terbilang ($angka) {
    $angka = (float)$angka;
    $bilangan = array('','Satu','Dua','Tiga','Empat','Lima','Enam','Tujuh','Delapan','Sembilan','Sepuluh','Sebelas');
    if ($angka < 12) {
        return $bilangan[$angka];
    } else if ($angka < 20) {
        return $bilangan[$angka - 10] . ' Belas';
    } else if ($angka < 100) {
        $hasil_bagi = (int)($angka / 10);
        $hasil_mod = $angka % 10;
        return trim(sprintf('%s Puluh %s', $bilangan[$hasil_bagi], $bilangan[$hasil_mod]));
    } else if ($angka < 200) { return sprintf('Seratus %s', terbilang($angka - 100));
    } else if ($angka < 1000) { $hasil_bagi = (int)($angka / 100); $hasil_mod = $angka % 100; return trim(sprintf('%s Ratus %s', $bilangan[$hasil_bagi], terbilang($hasil_mod)));
    } else if ($angka < 2000) { return trim(sprintf('Seribu %s', terbilang($angka - 1000)));
    } else if ($angka < 1000000) { $hasil_bagi = (int)($angka / 1000); $hasil_mod = $angka % 1000; return sprintf('%s Ribu %s', terbilang($hasil_bagi), terbilang($hasil_mod));
    } else if ($angka < 1000000000) { $hasil_bagi = (int)($angka / 1000000); $hasil_mod = $angka % 1000000; return trim(sprintf('%s Juta %s', terbilang($hasil_bagi), terbilang($hasil_mod)));
    } else if ($angka < 1000000000000) { $hasil_bagi = (int)($angka / 1000000000); $hasil_mod = fmod($angka, 1000000000); return trim(sprintf('%s Milyar %s', terbilang($hasil_bagi), terbilang($hasil_mod)));
    } else if ($angka < 1000000000000000) { $hasil_bagi = $angka / 1000000000000; $hasil_mod = fmod($angka, 1000000000000); return trim(sprintf('%s Triliun %s', terbilang($hasil_bagi), terbilang($hasil_mod)));
    } else {
        return '';
    }
}

function getIndianCurrency($number)
{
    $decimal = round($number - ($no = floor($number)), 2) * 100;
    $hundred = null;
    $digits_length = strlen($no);
    $i = 0;
    $str = array();
    $words = array(0 => '', 1 => 'One', 2 => 'Two',
        3 => 'Three', 4 => 'Four', 5 => 'Five', 6 => 'Six',
        7 => 'Seven', 8 => 'Eight', 9 => 'Nine',
        10 => 'Ten', 11 => 'Eleven', 12 => 'Twelve',
        13 => 'Thirteen', 14 => 'Fourteen', 15 => 'Fifteen',
        16 => 'Sixteen', 17 => 'Seventeen', 18 => 'Eighteen',
        19 => 'Nineteen', 20 => 'Twenty', 30 => 'Thirty',
        40 => 'Forty', 50 => 'Fifty', 60 => 'Sixty',
        70 => 'Seventy', 80 => 'Eighty', 90 => 'Ninety');
    $digits = array('', 'Hundred','Thousand','Million', 'Billion');
    while( $i < $digits_length ) {
        $divider = ($i == 2) ? 10 : 100;
        $number = floor($no % $divider);
        $no = floor($no / $divider);
        $i += $divider == 10 ? 1 : 2;
        if ($number) {
            $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
            $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
            $str [] = ($number < 21) ? $words[$number].' '. $digits[$counter]. $plural.' '.$hundred:$words[floor($number / 10) * 10].' '.$words[$number % 10]. ' '.$digits[$counter].$plural.' '.$hundred;
        } else $str[] = null;
    }
    $Rupees = implode('', array_reverse($str));
    $paise = ($decimal) ? " point " . ($words[$decimal / 10] . " " . $words[$decimal % 10]) . '' : '';
    return ($Rupees ? $Rupees . '' : '') . $paise;
}

?>
<?php
  //$hoteloid = $_SESSION['_hotel'];
  $bookingnumber = $_REQUEST['bookingnumber'];

  $pmslite = new PMSReservation($db);
  $pmslite->setBooking($bookingnumber);
  $datarsv = $pmslite->getBookingData();
  $pmslite->setPMSHotel($datarsv['hoteloid']);
  $hoteloid = $datarsv['hoteloid'];

  $pmslitedata = new PMSEditRSV($db);
  $pmslitedata->setBooking($bookingnumber);
  $dataroom = $pmslitedata->getBookingRoomData();

  if(in_array($hoteloid,$avoid_adjust)){
    $adjust = 1;
  }


  /*--------------------------------------*/
  $fileVersion = date("Y.m.d.H.i.s");
  
  /*--------------------------------------*/
  $idnuminv = $datarsv['invnumofhotel'];
  switch(strlen($idnuminv)){
      case 1: $idnuminv='000'.$idnuminv; break;
      case 2: $idnuminv='00'.$idnuminv; break;
      case 3: $idnuminv='0'.$idnuminv; break;
  }
  
  $aidnuminv = $datarsv['invnumofchain'];
  switch(strlen($aidnuminv)){
      case 1: $aidnuminv='000'.$aidnuminv; break;
      case 2: $aidnuminv='00'.$aidnuminv; break;
      case 3: $aidnuminv='0'.$aidnuminv; break;
  }
  
  $invrawdate = $datarsv['dateofinvoice'];
  if(empty($invrawdate) or $invrawdate=='0000-00-00' or $invrawdate=='1970-01-01'){
      $invrawdate = date('Y-m-d');
  }
  $invdate = date('d-M-Y', strtotime($invrawdate));
  $invmonth = date('m', strtotime($invrawdate));
  $invyear = date('Y', strtotime($invrawdate));
  
  $datasource = $pmslitedata->BookingSource();
  
  $stmt = $db->prepare("SELECT `invnolabel`, `notefax`, `noteemail` FROM `pmsinvoicesetting` WHERE `hoteloid`=:a");
  $stmt->execute(array(':a' => $hoteloid));
  $invset = $stmt->fetch(PDO::FETCH_ASSOC);
  $invnolabel = $invset['invnolabel'];
  $invnotefax = $invset['notefax'];
  $invnoteemail = $invset['noteemail'];
  
  
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('PMS Lite Powered by TheBuking');
$pdf->SetTitle('Invoice to Guest '.$datarsv['bookingnumber']);
$pdf->SetSubject('Invoice to Guest '.$datarsv['bookingnumber']);
$pdf->SetKeywords('Invoice to Guest', 'invoice', $datarsv['bookingnumber']);

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set margins
$pdf->SetMargins(10, 10, 10, true);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

$pdf->SetFont('dejavusans', '', 8);

$pdf->AddPage();

$html = '
<table>
    <tr>
        <td><br><br><b style="font-size:12px">'.$pmslite->hotelname.'</b><br>'.$pmslite->hoteldata['address'].'<br>Ph. '.$pmslite->hoteldata['phone'].' Fax. '.$pmslite->hoteldata['fax'].'<br>Website: '.$pmslite->hoteldata['website'].'</td>
        <td align="right"><br><br><br><br><br><br>
        Exclusively managed by:
        <img src="nagisa.png" style="width:160px"/></td>
    </tr>
    <tr>
        <td colspan="2">
        <hr>
        <table><tr><td align="center" style="font-size:12px">CANCELLATION INVOICE</td></tr></table>
        <hr>
        </td>
    </tr>
    <tr>
        <td>
        <table>
        <tr><td height="14" style="width:90px">Name of Guest</td><td style="width:230px">: &nbsp;'.$datarsv['guestname'].'</td></tr>
        <tr><td height="14">Agent</td><td>: &nbsp;'.$datasource['agentname'].'</td></tr>
        <tr><td height="14">Address</td><td>: &nbsp;'.$datarsv['address'].' '.$datarsv['city'].'</td></tr>
        <tr><td height="14">Email</td><td>: &nbsp;'.$datarsv['email'].'</td></tr>
        <tr><td height="14">Telephone</td><td>: &nbsp;'.$datarsv['phone'].'</td></tr>
        <tr><td height="14">Fax</td><td>: &nbsp;'.$datarsv['fax'].'</td></tr>
        </table>
        </td>
        <td>
        <table>
        <tr><td height="14" style="width:120px;color:#ff0000">Invoice No.</td><td style="width:200px">: &nbsp;'.$idnuminv.'/'.$invnolabel.'-'.$aidnuminv.'/'.$invmonth.'/'.$invyear.'</td></tr>
        <tr><td height="14">Date</td><td>: &nbsp;'.$invdate.'</td></tr>
        <tr><td height="14">Confirmation No.</td><td>: &nbsp;</td></tr>
        <tr><td height="14">Date of Confirmation</td><td>: &nbsp;</td></tr>
        </table>
        </td>
    </tr>
    <tr>
        <td colspan="2"><br><hr></td>
    </tr>
    <tr>
        <td colspan="2"><br></td>
    </tr>
    <tr>
        <td colspan="2">
        <table border="1" cellpadding="5">
        <tr align="center"><th style="width:30px">No</th><th style="width:500px">Description</th><th style="width:130px">Cancellation Fee</th></tr>
    ';
    
$grtotal = 0;
$no = 0; 

if(!isset($_GET['noroom']) or (isset($_GET['noroom']) and $_GET['noroom']!='1')){
$grtotal += $datarsv['cancellationamount'];

    $html .= '<tr>
                <td align="center">'.(++$no).'</td>
                <td>Cancellation for Ref# : '.$datarsv['bookingnumber'].'';

foreach($dataroom as $dr){
    $date1 = new DateTime($dr['checkin']);
    $date2 = new DateTime($dr['checkout']);
    $diff = $date2->diff($date1)->format("%a");
    
    $rateplandata = $pmslitedata->detailRatePlan($dr['roomofferoid']);
    
            $html .= '<br>&bull; '.$dr['promotion'].' '.$dr['room'].' / '.$pmslitedata->getRoomNumber($dr['bookingroomoid']).'
                    <br>&nbsp; &nbsp; - Stay : '.date("d-M-Y", strtotime($dr['checkin'])).' / '.date("d-M-Y", strtotime($dr['checkout'])).'
                    <br>&nbsp; &nbsp; - Rate Plan : '.$rateplandata['roomoffer'].'
                    <br>&nbsp; &nbsp; - Amount : '.$datarsv['currencycode']." ".number_format($dr['totalr'] * $adjust,2);
}
                
    $html .= '  <br>
                <br>Cancellation Reason:
                <br>&nbsp; '.$datarsv['cancellationreason'].'</td>
                <td align="right">'.$datarsv['currencycode']." ".number_format($datarsv['cancellationamount'] * $adjust,2).'</td>
            </tr>';
}

$html .= '
        <tr><td></td><td align="center">Total</td><td align="right">'.$datarsv['currencycode']." ".number_format($grtotal * $adjust,2).'</td></tr>
        ';
        
    if($grtotal > 0){
        switch($datarsv['currencyoid']){
            case '1': $word = terbilang($grtotal * $adjust).' Rupiah'; break;
            case '2': $word = getIndianCurrency($grtotal * $adjust). ' US Dollar'; break;
            default : $word = '';
        }
    }else{
        $word = '';
    }
        
$html .= '
        </table>
        </td>
    </tr>
    <tr>';
if($invnolabel == "NBPM"){
    if(!empty($datarsv['note'])){
        $html .= '<td><br><br>Guest Note:<br>'.$datarsv['note'].'</td>';
    }else{
        $html .= '<td></td>';
    }
}else{
    $html .= '<td></td>';
}
$html .= '<td><br><br>In Words: <i># '.$word.'</i> #</td>
    </tr>
    <tr>
        <td colspan="2">
        <br><br><br>
        <table>
        <tr><td colspan="2"><b>Bank Detail</b></td></tr>';

  $inbacc = $_POST['inbacc'];
  if(is_array($inbacc)){
    $ar = implode(",", $inbacc);
    
    $stmt = $db->prepare("SELECT pb.* FROM `pmsbankacc` pb WHERE pmsbankaccoid IN (".$ar.") and publishedoid='1'");
    $stmt->execute(array());
    $bankacc = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
    foreach($bankacc as $xmndt){
        $html .= '
            <tr><td colspan="2">&nbsp;</td></tr>
            <tr><td style="width:100px">Account Name</td><td>: &nbsp;<b>'.$xmndt['accountname'].'</b></td></tr>
            <tr><td>Bank Name</td><td>: &nbsp;<b>'.$xmndt['bankname'].'</b></td></tr>
            <tr><td>Account Number</td><td>: &nbsp;<b>'.$xmndt['accountnumber'].'</b></td></tr>
            <tr><td>Swift Code</td><td>: &nbsp;<b>'.$xmndt['swiftcode'].'</b></td></tr>';
    }
        
  }else{
    $html .= '
        <tr><td colspan="2">&nbsp;</td></tr>';
      
  }

        
$html .= '
        </table>
        </td>
    </tr>
    <tr>
        <td colspan="2">
        <br><br>
        <table>
        <tr><td style="width:100px"><u>Note:</u></td><td colspan="5">1. Please settle payment in full invoice amount</td></tr>
        <tr><td></td><td colspan="5">2. All bank charges will be borne by costumer in the case of any charge of Bank correspondence for USD Transfer after received will be inform and bourne by customer</td></tr>
        <tr><td></td><td colspan="5">3. All Payment by Indonesian Rupiah must be based on our floating rate</td></tr>
        <tr><td></td><td colspan="5">4. Please send us copy of the bank transfer for our internal file, Fax No. '.$invnotefax.' or email : '.$invnoteemail.'</td></tr>
        <tr><td></td><td colspan="5">5. Floating rate only valid for 7 days after invoice sending date</td></tr>
        </table>
        </td>
    </tr>
    <tr>
        <td colspan="2"><br><br><br></td>
    </tr>
    <tr align="center">
        <td>Issued by,</td>
        <td>Received by,</td>
    </tr>
    <tr>
        <td colspan="2"><br><br><br></td>
    </tr>
    <tr align="center">
        <td>_______________________</td>
        <td>_______________________</td>
    </tr>
</table>
';

$pdf->writeHTML($html, true, false, true, false, '');

$pdf->lastPage();
// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('CancellationInv_'.$datarsv['bookingnumber'].'.pdf', 'I');
?>