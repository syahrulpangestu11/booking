<?php
error_reporting(E_ALL ^ E_NOTICE);
include ('../configuration/connection.php');
	$stmt = $db->prepare("select aa.*, a.* from affiliatehotel_agreement aa inner join affiliatehotel ah using (affiliatehoteloid) inner join affiliate a using (affiliateoid) where affiliatehoteloid = :a");
	$stmt->execute(array(':a' => $_GET['id']));
	$agreement = $stmt->fetch(PDO::FETCH_ASSOC);
?>
<div class="box dark box-detail-status" id="box-status-5">
    <header>
        <div class="icons"><i class="fa fa-handshake-o"></i></div>
        <h5>Request Agreement</h5>
        <!-- .toolbar -->
        <div class="toolbar">
          <nav style="padding: 8px;">
              <a href="javascript:;" class="btn btn-default btn-xs collapse-box">
                  <i class="fa fa-minus"></i>
              </a>
              <a href="javascript:;" class="btn btn-default btn-xs full-box">
                  <i class="fa fa-expand"></i>
              </a>
          </nav>
        </div>            <!-- /.toolbar -->
    </header>
    <div id="div-1" class="body collapse in" aria-expanded="true">
        <div class="form-group">
            <label for="text1" class="control-label col-lg-4">Agreement Date</label>
            <div class="col-lg-6">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    <input type="text" name="agreementdate" class="form-control" value="<?php echo date("d F Y", strtotime($agreement['agreementdate'])); ?>">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="text1" class="control-label col-lg-4">Length of Contract</label>
            <div class="col-lg-6">
                <div class="input-group">
                <input class="form-control" type="number" name="length_of_contract" min="0" value="<?php echo $agreement['length_of_contract']; ?>">
                <span class="input-group-addon">year</span>
                </div>
            </div>
        </div>
		<hr />
        
		<div class="form-group">
            <label for="text1" class="control-label col-lg-4">Benefit</label>
            <div class="col-lg-8">
                <div class="checkbox">
                    <label><input class="uniform" type="radio" name="benefit" value="no website"> No website</label>
                    <?php 
					if($agreement['benefit_website'] == "free website"){ 
					 	$base_website_commission = $agreement['base_benefit_freewebsite'];
						$markup_website_commission = $agreement['markup_benefit_freewebsite'];			 			 
					}else{ 
					 	$base_website_commission = $agreement['base_freewebsite_commission'];
						$markup_website_commission = $agreement['markup_freewebsite_commission'];			 			 
					}
					?>    
                    <label><input class="uniform" type="radio" name="benefit" value="free website" href="#freeWebsite"> Free website</label>
                    <label><input class="uniform" type="radio" name="benefit" value="paid website" href="#paidWebsite"> Paid website</label>
                </div>
            </div>
        </div>   
         
    <div id="freeWebsite" class="benefit">
        <div class="form-group">
            <label for="text1" class="control-label col-lg-4">Base Commission</label>
            <div class="col-lg-4">
                <div class="input-group">
                <input class="form-control" name="base_website_commission" for="markup" type="number" readonly="readonly" value="<?php echo $base_website_commission; ?>">
                <span class="input-group-addon">%</span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="text1" class="control-label col-lg-4">Affiliate Commission Markup</label>
            <div class="col-lg-4">
                <div class="input-group">
                <input class="form-control" name="markup_website_commission" type="number" readonly="readonly" value="<?php echo $markup_website_commission; ?>">
                <span class="input-group-addon">%</span>
                </div>
            </div>
        </div>
    </div>        
    <div id="paidWebsite" class="benefit">
        <div class="form-group">
            <label for="text1" class="control-label col-lg-4">Paid Website Amount</label>
            <div class="col-lg-6">
                <input type="number" name="paidwebsite" class="form-control" value="<?php echo $agreement['benefit_website_amount']; ?>">
            </div>
        </div>
    </div>
       <hr /> 
        <div class="form-group">
            <label for="text1" class="control-label col-lg-4">Commission Type</label>
            <div class="col-lg-8">
                <div class="checkbox">
                    <label><input class="uniform" type="radio" name="commissiontype" value="flat fee" href="#flatFee" > Flat Fee</label>
                    <label><input class="uniform" type="radio" name="commissiontype" value="commission percent" href="#commissionPercentage" > Commission by percentage (%)</label>
                    <label><input class="uniform" type="radio" name="commissiontype" value="commission markup" href="#commissionMarkUp" > Commission by mark-up</label>
                </div>
            </div>
        </div>
        
    <div id="flatFee" class="commtype">
                    <?php 
					if($agreement['commissiontype'] == "flat fee"){ 
					    $flatfee_commission = $agreement['flatfee_commission']; 
					}else{ 
					    $flatfee_commission = $agreement['flatfeecommission']; 
					}
					?>
        <div class="form-group">
            <label for="text1" class="control-label col-lg-4">Flat Fee Billing Periode</label>
            <div class="col-lg-6">
                <select name="flatfeebillingtype" class="form-control chzn-select" tabindex="7">
                <?php if($agreement['flatfee_billingtype'] == "monthly"){ $selected = 'selected="selected"'; }else{ $selected = ''; } ?>
                <option value="monthly" <?php echo $selected; ?>>Monthly</option>
                <?php if($agreement['flatfee_billingtype'] == "annual"){ $selected = 'selected="selected"'; }else{ $selected = ''; } ?>
                <option value="annual" <?php echo $selected; ?>>Annual</option>
                <?php if($agreement['flatfee_billingtype'] == "pertransaction"){ $selected = 'selected="selected"'; }else{ $selected = ''; } ?>
                <option value="pertransaction" <?php echo $selected; ?>>Amount Per Transaction</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="text1" class="control-label col-lg-4">Flat Fee Amount</label>
            <div class="col-lg-6">
                <input type="number" name="flatfee_amount" class="form-control" value="<?php echo $agreement['flatfee_amount']; ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="text1" class="control-label col-lg-4">Affiliate Commission</label>
            <div class="col-lg-4">
                <div class="input-group">
                    <input class="form-control" name="flatfee_commission" type="number" readonly="readonly" value="<?php echo $flatfee_commission; ?>">
                    <span class="input-group-addon">%</span>
                </div>
			</div>
            <label for="text1" class="control-label col-lg-4 text-left">*from flat fee amount</label>
        </div>
    </div>
    <div id="commissionPercentage" class="commtype">
                    <?php 
					if($agreement['commissiontype'] == "commission percent"){ 
						$thebuking_commission = $agreement['thebuking_commission'];
						$affiliate_commission = $agreement['affiliate_commission'];
						$min_guarantee = $agreement['min_guarantee_commission']; 
					}else{ 
						$thebuking_commission = '';
						$affiliate_commission = $agreement['commission'];
						$min_guarantee = ''; 
					}
					?>
        <div class="form-group">
            <label for="text1" class="control-label col-lg-4">The Buking Commission</label>
            <div class="col-lg-4">
                <div class="input-group">
                <input class="form-control" type="number" name="thebuking_commission" for="percentage" value="<?php echo $thebuking_commission; ?>">
                <span class="input-group-addon">%</span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="text1" class="control-label col-lg-4">Affiliate Commission</label>
            <div class="col-lg-4">
                <div class="input-group">
                <input class="form-control" type="number" name="affiliate_commission" for="percentage" readonly="readonly" value="<?php echo $affiliate_commission; ?>">
                <span class="input-group-addon">%</span>
                </div>
            </div>
            <label for="text1" class="control-label col-lg-4 text-left">*from agreement</label>
        </div>
        <div class="form-group">
            <label for="text1" class="control-label col-lg-4">Minimum Guarantee Commission</label>
            <div class="col-lg-6">
                <input type="number" name="minguarantee" for="percentage" class="form-control" value="<?php echo $min_guarantee; ?>">
            </div>
        </div>
    </div>
    <div id="commissionMarkUp" class="commtype">
                    <?php 
					if($agreement['commissiontype'] == "commission markup"){ 
						$base_commission = $agreement['base_commission']; 
						$thebuking_commission = $agreement['thebuking_commission']; 
						$affiliate_commission_markup = $agreement['affiliate_commission_markup']; 
						$min_guarantee = $agreement['min_guarantee_commission']; 
					}else{ 
						$base_commission = $agreement['base_standard_commission']; 
						$thebuking_commission = $agreement['base_standard_commission']+$agreement['markup_standard_commission']; 
						$affiliate_commission_markup = $agreement['markup_standard_commission']; 
						$min_guarantee = ''; 
					}
					?>
        <div class="form-group">
            <label for="text1" class="control-label col-lg-4">Base Commission</label>
            <div class="col-lg-4">
                <div class="input-group">
                <input class="form-control" name="base_commission" for="markup" type="number" readonly="readonly" value="<?php echo $base_commission; ?>">
                <span class="input-group-addon">%</span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="text1" class="control-label col-lg-4">Affiliate Commission Markup</label>
            <div class="col-lg-4">
                <div class="input-group">
                <input class="form-control" type="number" name="markup_commission" for="markup" readonly="readonly" value="<?php echo $affiliate_commission_markup; ?>">
                <span class="input-group-addon">%</span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="text1" class="control-label col-lg-4">Agreement Commission</label>
            <div class="col-lg-4">
                <div class="input-group">
                <input class="form-control" name="thebuking_commission" for="markup" type="number" readonly="readonly" value="<?php echo $thebuking_commission; ?>">
                <span class="input-group-addon">%</span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="text1" class="control-label col-lg-4">Minimum Guarantee Commission</label>
            <div class="col-lg-6">
                <input type="number" name="minguarantee" for="markup" class="form-control" value="<?php echo $min_guarantee; ?>">
            </div>
        </div>
    </div>

		<hr />
        <div class="form-group">
            <label for="text1" class="control-label col-lg-4">Connectivity :</label>
        </div>
        <div class="form-group">
            <label for="text1" class="control-label col-lg-4">Siteminder</label>
            <div class="col-lg-2">
                <?php if($agreement['siteminder'] == "y"){ $checked = 'checked="checked"'; }else{ $checked = ''; } ?>
                <input type="radio" name="siteminder" value="y" <?php echo $checked; ?>/> yes
            </div>
            <div class="col-lg-2">
                <?php if($agreement['siteminder'] == "n"){ $checked = 'checked="checked"'; }else{ $checked = ''; } ?>
                <input type="radio" name="siteminder" value="n" <?php echo $checked; ?>/> no
            </div>
        </div>
        <div class="form-group">
            <label for="text1" class="control-label col-lg-4">Siteminder Name</label>
            <div class="col-lg-8">
                <input type="text" name="siteminder_name" class="form-control" required="required"  value="<?php echo $agreement['siteminder_name']; ?>"/> 
            </div>
        </div>
        <div class="form-group">
            <label for="text1" class="control-label col-lg-4">Property Management System</label>
            <div class="col-lg-2">
                <?php if($agreement['pms'] == "y"){ $checked = 'checked="checked"'; }else{ $checked = ''; } ?>
                <input type="radio" name="pms" value="y" <?php echo $checked; ?>/> yes
            </div>
            <div class="col-lg-2">
                <?php if($agreement['pms'] == "n"){ $checked = 'checked="checked"'; }else{ $checked = ''; } ?>
                <input type="radio" name="pms" value="n" <?php echo $checked; ?>/> no
            </div>
        </div>
        <div class="form-group">
            <label for="text1" class="control-label col-lg-4">Property Management System Name</label>
            <div class="col-lg-8">
                <input type="text" name="pms_name" class="form-control" required="required"  value="<?php echo $agreement['pms_name']; ?>"/> 
            </div>
        </div>
        <div class="form-group">
            <label for="text1" class="control-label col-lg-4">Internet Payment Gateway</label>
            <div class="col-lg-2">
                <?php if($agreement['ipg'] == "y"){ $checked = 'checked="checked"'; }else{ $checked = ''; } ?>
                <input type="radio" name="ipg" value="y" <?php echo $checked; ?>/> yes
            </div>
            <div class="col-lg-2">
                <?php if($agreement['ipg'] == "n"){ $checked = 'checked="checked"'; }else{ $checked = ''; } ?>
                <input type="radio" name="ipg" value="n" <?php echo $checked; ?>/> no
            </div>
        </div>
        <div class="form-group">
            <label for="text1" class="control-label col-lg-4">Internet Payment Gateway Name</label>
            <div class="col-lg-8">
                <input type="text" name="ipg_name" class="form-control" required="required"  value="<?php echo $agreement['ipg_name']; ?>"/> 
            </div>
        </div>
		<hr />
        <div class="form-group">
            <label for="text1" class="control-label col-lg-4">Company Name</label>
            <div class="col-lg-8">
                <input type="text" name="companyname" class="form-control" value="<?php echo $agreement['company_name']; ?>" required="required">
            </div>
        </div>
        <div class="form-group">
            <label for="text1" class="control-label col-lg-4">Company NPWP</label>
            <div class="col-lg-8">
                <input type="text" name="companynpwp" class="form-control" value="<?php echo $agreement['company_npwp']; ?>" required="required">
            </div>
        </div>
        <div class="form-group">
            <label for="text1" class="control-label col-lg-4">Company Address</label>
            <div class="col-lg-8">
                <textarea id="companyaddress" name="companyaddress" class="form-control" required="required"><?php echo $agreement['company_address']; ?></textarea>
            </div>
        </div>
        <div class="form-group">
            <label for="text1" class="control-label col-lg-4">PIC Name</label>
            <div class="col-lg-8">
                <input type="text" name="picname" class="form-control" value="<?php echo $agreement['pic_name']; ?>" required="required">
            </div>
        </div>
        <div class="form-group">
            <label for="text1" class="control-label col-lg-4">PIC Title</label>
            <div class="col-lg-8">
                <input type="text" name="pictitle" class="form-control"  value="<?php echo $agreement['pic_title']; ?>" required="required">
            </div>
        </div>
        <div class="form-group">
            <label for="text1" class="control-label col-lg-4">PIC ID Number</label>
            <div class="col-lg-8">
                <input type="text" name="picidnumber" class="form-control" value="<?php echo $agreement['pic_id_number']; ?>" required="required">
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

</script>