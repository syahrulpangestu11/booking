<?php
$stmt	= $db->prepare("select h.hotelname, h.hoteloid, h.createdby, h.affiliatehotelstatusoid, h.affibestatusoid, h.affwdmstatusoid from hotel as `h` where h.hoteloid = :a");
$stmt->execute(array(':a' => $_POST['hoteloid']));
$hotel_implementation	= $stmt->fetch(PDO::FETCH_ASSOC);

// stop implementation IBE
if($hotel_implementation['affibestatusoid'] != 0){
  $stmt = $db->prepare("select affiliatelogoid, al.startdate, to_type, to_status from affiliatelog al inner join hotel h using (hoteloid) where to_type = :a and to_status = :b and al.hoteloid = :c order by al.startdate desc limit 1");
  $stmt->execute(array(':a' => 'ibe', ':b' => $hotel_implementation['affibestatusoid'], ':c' => $hotel_implementation['hoteloid']));
  $log		= $stmt->fetch(PDO::FETCH_ASSOC);
  $lastlog	= $log['affiliatelogoid'];
  $last_type = $log['to_type'];
  $last_status = $log['to_status'];
  $lasttime	= $log['startdate'];
  $idletime	= knowIdleTime($lasttime, $logtime);

  $stmt = $db->prepare("insert into affiliatelog (hoteloid, startdate, from_type, from_status, to_type, to_status, note, useroid, displayname) values (:a, :b, :c, :d, :e, :f, :g, :h, :i)");
  $stmt->execute(array(':a' => $hotel_implementation['hoteloid'], ':b' => $logtime, ':c' => $last_type, ':d' => $last_status, ':e' => 'ibe', ':f' => '6', ':g' => $_POST['note'], ':h' => $_SESSION['_oid'], ':i' => $_SESSION['_initial']));
  $affiliatelogid = $db->lastInsertId();

  $stmt = $db->prepare("update hotel set affibestatusoid = :a where hoteloid = :b");
  $stmt->execute(array(':a' => '6', ':b' => $hotel_implementation['hoteloid']));

  $stmt = $db->prepare("update affiliatelog set enddate = :a, idletime = :b where affiliatelogoid = :c");
  $stmt->execute(array(':a' => $logtime, ':b' => $idletime, ':c' => $lastlog));
}

// stop implementation WDM
if($hotel_implementation['affwdmstatusoid'] != 0){
  $stmt = $db->prepare("select affiliatelogoid, al.startdate, to_type, to_status from affiliatelog al inner join hotel h using (hoteloid) where to_type = :a and to_status = :b and al.hoteloid = :c order by al.startdate desc limit 1");
  $stmt->execute(array(':a' => 'wdm', ':b' => $hotel_implementation['affwdmstatusoid'], ':c' => $hotel_implementation['hoteloid']));
  $log		= $stmt->fetch(PDO::FETCH_ASSOC);
  $lastlog	= $log['affiliatelogoid'];
  $last_type = $log['to_type'];
  $last_status = $log['to_status'];
  $lasttime	= $log['startdate'];
  $idletime	= knowIdleTime($lasttime, $logtime);

  $stmt = $db->prepare("insert into affiliatelog (hoteloid, startdate, from_type, from_status, to_type, to_status, note, useroid, displayname) values (:a, :b, :c, :d, :e, :f, :g, :h, :i)");
  $stmt->execute(array(':a' => $hotel_implementation['hoteloid'], ':b' => $logtime, ':c' => $last_type, ':d' => $last_status, ':e' => 'wdm', ':f' => '8', ':g' => $_POST['note'], ':h' => $_SESSION['_oid'], ':i' => $_SESSION['_initial']));
  $affiliatelogid = $db->lastInsertId();

  $stmt = $db->prepare("update hotel set affwdmstatusoid = :a where hoteloid = :b");
  $stmt->execute(array(':a' => '8', ':b' => $hotel_implementation['hoteloid']));

  $stmt = $db->prepare("update affiliatelog set enddate = :a, idletime = :b where affiliatelogoid = :c");
  $stmt->execute(array(':a' => $logtime, ':b' => $idletime, ':c' => $lastlog));
}
?>
