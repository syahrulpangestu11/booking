<?php
if(!empty($_POST['affoid'])){
	include ('../class/affiliate.php');
	$affiliate = new Affiliate($db);
	$affiliate->setAffiliate($_POST['affoid']);
	$profile = $affiliate->profileAffiliate($affiliate->affiliateoid);
}

	$stmt = $db->prepare("INSERT INTO affiliatehotel_agreementlog (affiliatehoteloid, affiliatelogoid, commissiontype, benefit_website, benefit_website_amount, agreementdate, length_of_contract, siteminder, pms, ipg, company_name, company_address, company_npwp, pic_name, pic_id_number, created, createdby, updated, updatedby) VALUES (:a, :b, :c, :d, :e, :f, :g, :h, :j, :l, :n, :o, :p, :q, :r, :s, :t, :u, :v)");
	$stmt->execute(array(':a' => $_POST['affhotel'], ':b' => $affiliatelogid, ':c' => $_POST['commissiontype'], ':d' => $_POST['benefit'], ':e' => $_POST['paidwebsite'], ':f' => date('Y-m-d', strtotime($_POST['agreementdate'])), ':g' => $_POST['length_of_contract'], ':h' => $_POST['siteminder'], ':j' => $_POST['pms'], ':l' => $_POST['ipg'], ':n' => $_POST['companyname'], ':o' => $_POST['companyaddress'], ':p' => $_POST['companynpwp'], ':q' => $_POST['picname'], ':r' => $_POST['picidnumber'], ':s' => date('Y-m-d H:i:s'), ':t' => $_POST['usname'], ':u' => date('Y-m-d H:i:s'), ':v' => $_POST['usname']));
	$aggreementlogid = $db->lastInsertId();
	
	if($_POST['commissiontype'] == 'flatfee'){
		$stmt = $db->prepare("UPDATE affiliatehotel_agreementlog SET flatfee_billingtype = :a, flatfee_amount = :b, flatfee_commission = :c WHERE affiliatehotel_agreementlogoid = :id");
		$stmt->execute(array(':a' => $_POST['flatfeebillingtype'], ':b' => $_POST['flatfee_amount'], ':c' => $profile['flatfeecommission'], ':id' => $aggreementlogid));
	}else if($_POST['commissiontype'] == 'commissionpercent'){
		$stmt = $db->prepare("UPDATE affiliatehotel_agreementlog SET thebuking_commission = :a, affiliate_commission = :b, min_guarantee_commission = :c WHERE affiliatehotel_agreementlogoid = :id");
		$stmt->execute(array(':a' => $_POST['thebuking_commission'], ':b' => $profile['commission'], ':c' => $_POST['minguarantee'], ':id' => $aggreementlogid));
	}else if($_POST['commissiontype'] == 'commissionmarkup'){
		$stmt = $db->prepare("UPDATE affiliatehotel_agreementlog SET base_commission = :a, thebuking_commission = :b, 	affiliate_commission_markup = :c, min_guarantee_commission = :d WHERE affiliatehotel_agreementlogoid = :id");
		$stmt->execute(array(':a' => $profile['basecommission'], ':b' => $profile['basecommission']+$_POST['affiliate_commission_markup'], ':c' => $_POST['affiliate_commission_markup'], ':d' => $_POST['minguarantee'], ':id' => $aggreementlogid));
	}

	$stmt = $db->prepare("select count(affiliatehotel_agreementoid) existagreement from affiliatehotel_agreement where affiliatehoteloid = :a");
	$stmt->execute(array(':a' => $_POST['affhotel']));
	$count = $stmt->fetch(PDO::FETCH_ASSOC);
	
	if($count['existagreement'] == 0){
		$stmt = $db->prepare("INSERT INTO affiliatehotel_agreement (affiliatehoteloid, affiliatelogoid, commissiontype, benefit_website, benefit_website_amount, agreementdate, length_of_contract, siteminder, pms, ipg, company_name, company_address, company_npwp, pic_name, pic_id_number, created, createdby, updated, updatedby) VALUES (:a, :b, :c, :d, :e, :f, :g, :h, :j, :l, :n, :o, :p, :q, :r, :s, :t, :u, :v)");
		$stmt->execute(array(':a' => $_POST['affhotel'], ':b' => $affiliatelogid, ':c' => $_POST['commissiontype'], ':d' => $_POST['benefit'], ':e' => $_POST['paidwebsite'], ':f' => date('Y-m-d', strtotime($_POST['agreementdate'])), ':g' => $_POST['length_of_contract'], ':h' => $_POST['siteminder'], ':j' => $_POST['pms'], ':l' => $_POST['ipg'], ':n' => $_POST['companyname'], ':o' => $_POST['companyaddress'], ':p' => $_POST['companynpwp'], ':q' => $_POST['picname'], ':r' => $_POST['picidnumber'], ':s' => date('Y-m-d H:i:s'), ':t' => $_POST['usname'], ':u' => date('Y-m-d H:i:s'), ':v' => $_POST['usname']));
		$aggreementid = $db->lastInsertId();
	
		if($_POST['commissiontype'] == 'flatfee'){
			$stmt = $db->prepare("UPDATE affiliatehotel_agreement SET flatfee_billingtype = :a, flatfee_amount = :b, flatfee_commission = :c WHERE affiliatehotel_agreementoid = :id");
			$stmt->execute(array(':a' => $_POST['flatfeebillingtype'], ':b' => $_POST['flatfee_amount'], ':c' => $profile['flatfeecommission'], ':id' => $aggreementid));
		}else if($_POST['commissiontype'] == 'commissionpercent'){
			$stmt = $db->prepare("UPDATE affiliatehotel_agreement SET thebuking_commission = :a, affiliate_commission = :b, min_guarantee_commission = :c WHERE affiliatehotel_agreementoid = :id");
			$stmt->execute(array(':a' => $_POST['thebuking_commission'], ':b' => $profile['commission'], ':c' => $_POST['minguarantee'], ':id' => $aggreementid));
		}else if($_POST['commissiontype'] == 'commissionmarkup'){
			$stmt = $db->prepare("UPDATE affiliatehotel_agreement SET base_commission = :a, thebuking_commission = :b, 	affiliate_commission_markup = :c, min_guarantee_commission = :d WHERE affiliatehotel_agreementoid = :id");
			$stmt->execute(array(':a' => $profile['basecommission'], ':b' => $profile['basecommission']+$_POST['affiliate_commission_markup'], ':c' => $_POST['affiliate_commission_markup'], ':d' => $_POST['minguarantee'], ':id' => $aggreementid));
		}
	}else{
		$stmt = $db->prepare("UPDATE affiliatehotel_agreement SET commissiontype = :a, benefit_website = :b, benefit_website_amount = :c, agreementdate = :d, length_of_contract = :e, siteminder = :f, pms = :g, ipg = :h, company_name = :i, company_address = :j, company_npwp = :k, pic_name = :l, pic_id_number = :m , updated = :n, updatedby = :o, flatfee_amount = :p, flatfee_commission = :q, thebuking_commission = :r, affiliate_commission = :s, affiliate_commission_markup = :t, min_guarantee_commission = :u, base_commission = :v  where affiliatehoteloid = :id");
		$stmt->execute(array(':a' => $_POST['commissiontype'], ':b' => $_POST['benefit'], ':c' => $_POST['paidwebsite'], ':d' => date('Y-m-d', strtotime($_POST['agreementdate'])), ':e' => $_POST['length_of_contract'], ':f' => $_POST['siteminder'], ':g' => $_POST['pms'], ':h' => $_POST['ipg'], ':i' => $_POST['companyname'], ':j' => $_POST['companyaddress'], ':k' => $_POST['companynpwp'], ':l' => $_POST['picname'], ':m' => $_POST['picidnumber'], ':n' => date('Y-m-d H:i:s'), ':o' => $_POST['usname'], ':p' => 0, ':q' => 0, ':r' => 0, ':s' => 0, ':t' => 0, ':u' => 0, ':v' => 0, ':id' => $_POST['affhotel']));
		if($_POST['commissiontype'] == 'flatfee'){
			$stmt = $db->prepare("UPDATE affiliatehotel_agreement SET flatfee_billingtype = :a, flatfee_amount = :b, flatfee_commission = :c WHERE affiliatehoteloid = :id");
			$stmt->execute(array(':a' => $_POST['flatfeebillingtype'], ':b' => $_POST['flatfee_amount'], ':c' => $profile['flatfeecommission'], ':id' => $_POST['affhotel']));
		}else if($_POST['commissiontype'] == 'commissionpercent'){
			$stmt = $db->prepare("UPDATE affiliatehotel_agreement SET thebuking_commission = :a, affiliate_commission = :b, min_guarantee_commission = :c WHERE affiliatehoteloid = :id");
			$stmt->execute(array(':a' => $_POST['thebuking_commission'], ':b' => $profile['commission'], ':c' => $_POST['minguarantee'], ':id' => $_POST['affhotel']));
		}else if($_POST['commissiontype'] == 'commissionmarkup'){
			$stmt = $db->prepare("UPDATE affiliatehotel_agreement SET base_commission = :a, thebuking_commission = :b, 	affiliate_commission_markup = :c, min_guarantee_commission = :d WHERE affiliatehoteloid = :id");
			$stmt->execute(array(':a' => $profile['basecommission'], ':b' => $profile['basecommission']+$_POST['affiliate_commission_markup'], ':c' => $_POST['affiliate_commission_markup'], ':d' => $_POST['minguarantee'], ':id' => $_POST['affhotel']));
		}
	}

?>