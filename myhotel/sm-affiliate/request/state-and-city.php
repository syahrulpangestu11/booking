<?php
error_reporting(E_ALL ^ E_NOTICE);
include ('../configuration/connection.php');

if(isset($_GET['func'])) { 
	if($_GET['func'] == "stateRqst"){
		getState($_GET['country'],$_GET['state']); 
	}else if($_GET['func'] == "cityRqst"){
		getCity($_GET['state'],$_GET['city']); 
	}else if($_GET['func'] == "brandRqst"){
		getBrand($_GET['chain'], $_GET['brand']); 
	}
	
}


function getState($country,$state)
{
	echo"<select name='state' class='form-control'> ";
	try {
		global $db;
		$stmt = $db->query("select s.stateoid, s.statename from state s inner join country c using (countryoid) where c.countryoid = '".$country."'");
		$r_state = $stmt->fetchAll(PDO::FETCH_ASSOC);
		foreach($r_state as $row){
			if($row['stateoid'] == $state){ $selected = "selected"; }else{ $selected=""; }
			echo"<option value='".$row['stateoid']."' ".$selected.">".$row['statename']."</option>";
		}
		echo"<option value='0'>Other</option>";
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}
	echo"</select><input type='text' name='statename' class='form-control other' placeholder='Type State Name' />
";
}


function getCity($state,$city)
{
	echo"<select name='city' class='form-control'> ";
	try {
		global $db;
		$stmt = $db->query("select ct.cityoid, ct.cityname from city ct inner join state s using (stateoid) inner join country c using (countryoid) where s.stateoid = '".$state."' order by ct.cityname");
		$r_state = $stmt->fetchAll(PDO::FETCH_ASSOC);
		foreach($r_state as $row){
			if($row['cityoid'] == $city){ $selected = "selected"; }else{ $selected=""; }
			echo"<option value='".$row['cityoid']."' ".$selected.">".$row['cityname']."</option>";
		}
		echo"<option value='0'>Other</option>";
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}
	echo"</select><input type='text' name='cityname' class='form-control other' placeholder='Type City Name' />
";
}
function getBrand($chain, $brand)
{
	echo"<select name='brand' class='form-control'> ";
	try {
		global $db;
		$stmt = $db->query("select b.brandoid, b.name from brand b inner join chain c using (chainoid) where c.chainoid = '".$chain."' order by b.name");
		$r_state = $stmt->fetchAll(PDO::FETCH_ASSOC);
		foreach($r_state as $row){
			if($row['brandoid'] == $brand){ $selected = "selected"; }else{ $selected=""; }
			echo"<option value='".$row['brandoid']."' ".$selected.">".$row['name']."</option>";
		}
		echo"<option value='0'>Other</option>";
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}
	echo"</select><input type='text' name='brandname' class='form-control other' placeholder='Type Brand Name' />
";
}
?>