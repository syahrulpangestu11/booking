<?php
class affiliate{
	var $db;
	var $affiliateoid;
	var $firstname;
	var $lastname;
	var $email;
	
	public function __construct($db){
		$this->db = $db;
	}
		
	public function registerAffiliate($firstname, $lastname, $email, $phone, $password){
		$stmt = $this->db->prepare("insert into affiliate (firstname, lastname, email, phone, password, created, updated, publishedoid) values (:a, :b, :c, :d, sha1(:e), :f, :g, :h)");
		$stmt->execute(array(':a' => $firstname, ':b' => $lastname, ':c' => $email, ':d' => $phone, ':e' => $password, ':f' => date('Y-m-d H:i:s'), ':g' => date('Y-m-d H:i:s'), ':h' => 1));
		$this->affiliateoid = $this->db->lastInsertId();
	}

	public function setAffiliate($id){
		$stmt = $this->db->prepare("select count(affiliateoid) as foundaffliate, affiliateoid, firstname, lastname, email from affiliate where affiliateoid = :a");
		$stmt->execute(array(':a' => $id));
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		if($result['foundaffliate'] > 0){
			$this->affiliateoid = $result['affiliateoid'];
			$this->firstname = $result['firstname'];
			$this->lastname = $result['lastname'];
			$this->email = $result['email'];
		}
	}

	public function validateAffiliate($email, $password){
		$stmt = $this->db->prepare("select count(affiliateoid) as foundaffliate, affiliateoid from affiliate where email = :a and password = :b and publishedoid = :c");
		$stmt->execute(array(':a' => $email, ':b' => sha1($password), ':c' => 1));
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		if($result['foundaffliate'] > 0){
			$this->affiliateoid = $result['affiliateoid'];
		}
		return $result['foundaffliate'];
	}
	
	public function setSessionAffiliate($id){
		$stmt = $this->db->prepare("select count(affiliateoid) as foundaffliate, affiliateoid, firstname, lastname, email, managed_all from affiliate where affiliateoid = :a and publishedoid = :b");
		$stmt->execute(array(':a' => $id, ':b' => 1));
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		if($result['foundaffliate'] > 0){
			$_SESSION['tbcaff'] = 'affiliate';
			$_SESSION['tbcaffnoidf'] = $result['affiliateoid'];
			$_SESSION['tbcaffname'] = $result['firstname']." ".$result['lastname'];
			$_SESSION['tbcafflmail'] = $result['email'];
			$_SESSION['tbcafflmngd'] = $result['managed_all'];
		}
	}

	public function checkExistedEmail($email){
		$stmt = $this->db->prepare("select count(affiliateoid) as foundaffliate, email from affiliate where email = :a");
		$stmt->execute(array(':a' => $email));
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		return $result['foundaffliate'];
	}

	public function checkChangedEmail($email, $id){
		$stmt = $this->db->prepare("select count(affiliateoid) as foundaffliate, email from affiliate where email = :a and affiliateoid != :b");
		$stmt->execute(array(':a' => $email, ':b' => $id));
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		return $result['foundaffliate'];
	}


	public function profileAffiliate($id){
		$stmt = $this->db->prepare("select * from affiliate where affiliateoid = :a");
		$stmt->execute(array(':a' => $id));
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		return $result;
	}
	
	public function generalData(){
		$stmt = $this->db->query("select * from affiliategeneraldata");
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		return $result;
	}
}
?>