    <div class="add-training-schedule">
        <div class="form-group">
            <label class="control-label col-md-3">Date</label>
            <div class="col-md-6">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    <input type="text" name="detail-date" id="detail-date" class="form-control" value="<?php echo date("d F Y"); ?>" required="required">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3">Time</label>
            <div class="col-md-6">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                    <input type="time" name="detail-time" id="training-time" class="form-control" value="12:00">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3">Note</label>
            <div class="col-md-8">
                <textarea id="note_ibe" name="detail-note" class="form-control"></textarea>
            </div>
        </div>
    </div>
