<div id="div-1" class="body collapse in" aria-expanded="true">
    <div class="form-group">
        <label class="control-label col-lg-3">WDM Template</label>
        <div class="col-lg-8">
            <select name="wdmtemplate" class="form-control" tabindex="7">
                <?php 
					$stmt = $db->query("SELECT `wdmtemplateoid`, `template` FROM `wdmtemplate` as `wt` where `wt`.`publishedoid` = '1'");
					$wdmtemplate = $stmt->fetchAll(PDO::FETCH_ASSOC);
                    foreach($wdmtemplate as $template){
                ?>
                <option value="<?=$template['wdmtemplateoid'];?>"><?=$template['template'];?></option>
                <?php
                    }
                ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-lg-3">Note</label>
        <div class="col-lg-8">
            <textarea id="note_wdm" name="note_wdm" class="form-control"></textarea>
        </div>
    </div>
</div>
