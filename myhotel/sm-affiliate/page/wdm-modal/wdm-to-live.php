<div class="row">
    <div class="col-md-6">
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <label class="control-label col-lg-4 text-left">Current Status</label>
                    <div class="col-lg-6">
                        <input type="text" name="currentstatus" class="form-control" readonly value="<?=$affwdm['wdmstatus'];?>"><input type="hidden" name="wdmfrom" value="<?=$affwdm['affwdmstatusoid'];?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-4 text-left">Change Status to</label>
                    <div class="col-lg-6">
                        <select name="wdmto" class="form-control chzn-select" tabindex="7">
                            <?php 
								$statusto = array(7);
								$status = implode(',', $statusto);
								$stmt = $db->query("SELECT `affwdmstatusoid`, `wdmstatus` FROM `affwdmstatus` as `aw` where `aw`.`affwdmstatusoid` in (".$status.") and `aw`.`publishedoid` = '1'");
								$list_wdm = $stmt->fetchAll(PDO::FETCH_ASSOC);
								foreach($list_wdm as $wdm){
							?>
							<option value="<?=$wdm['affwdmstatusoid'];?>"><?=$wdm['wdmstatus'];?></option>
                            <?php
								}
							?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
			<?php 
				foreach($list_wdm as $wdm){
			?>
            <div class="box dark box-detail-wdm" id="box-wdm-<?=$wdm['affwdmstatusoid'];?>">
                <header>
                    <h5><?=$wdm['wdmstatus'];?></h5>
                    <!-- .toolbar -->
                    <div class="toolbar">
                      <nav style="padding: 8px;">
                          <a href="javascript:;" class="btn btn-default btn-xs collapse-box">
                              <i class="fa fa-minus"></i>
                          </a>
                          <a href="javascript:;" class="btn btn-default btn-xs full-box">
                              <i class="fa fa-expand"></i>
                          </a>
                      </nav>
                    </div>            <!-- /.toolbar -->
                </header>
                <?php include ('form/form-wdm-'.$status.'.php'); ?>
			</div>               
			<?php
			}
            ?>
        </div>
    </div>
    <div class="col-md-6">
        <?php include('email/email-internal.php'); ?>
    </div>
</div>