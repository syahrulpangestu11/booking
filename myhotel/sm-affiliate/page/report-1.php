<div class="row">
	<div class="col-lg-12 col-xs-12">
        <div class="box dark">
            <header>
                <div class="icons"><i class="fa fa-print"></i></div>
                <h5>Paid Commission Report</h5>
                <!-- .toolbar -->
                <div class="toolbar">
                  <nav style="padding: 8px;">
                      <a href="javascript:;" class="btn btn-default btn-xs collapse-box">
                          <i class="fa fa-minus"></i>
                      </a>
                      <a href="javascript:;" class="btn btn-default btn-xs full-box">
                          <i class="fa fa-expand"></i>
                      </a>
                  </nav>
                </div>            <!-- /.toolbar -->
            </header>
            <div id="div-1" class="body collapse in" aria-expanded="true">
                <form class="form-horizontal">
                    <div class="form-group">
                        <label for="text1" class="control-label col-lg-4">Periode</label>
                        <div class="col-lg-4">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input type="text" name="startdate" id="startdate" class="form-control" value="<?php echo date("d F Y"); ?>">
                            </div>
						</div>
                        <div class="col-lg-4">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input type="text" name="enddate" id="enddate" class="form-control" value="<?php echo date("d F Y"); ?>">
                            </div>
						</div>
                    </div>
                    <div class="form-group">
                        <label for="text1" class="control-label col-lg-4">Select Property</label>
                        <div class="col-lg-8">
                            <div class="checkbox"><label><input class="uniform" name="property[]" type="checkbox" value="option1" checked=""> All Property</label></div>
                            <div class="checkbox"><label><input class="uniform" name="property[]" type="checkbox" value="option1"> My Hotel 1</label></div>
                            <div class="checkbox"><label><input class="uniform" name="property[]" type="checkbox" value="option1"> My Hotel 2</label></div>
						</div>
                    </div>
                    <div class="form-group">
                        <label for="text1" class="control-label col-lg-4">Commission Status</label>
                        <div class="col-lg-8">
                            <div class="checkbox"><label><input class="uniform" name="status[]" type="checkbox" value="option1" checked=""> All Status</label></div>
                            <div class="checkbox"><label><input class="uniform" name="status[]" type="checkbox" value="option1"> Commission Request</label></div>
                            <div class="checkbox"><label><input class="uniform" name="status[]" type="checkbox" value="option1"> Not Collectable</label></div>
                            <div class="checkbox"><label><input class="uniform" name="status[]" type="checkbox" value="option1"> Commission Paid</label></div>
                            <div class="checkbox"><label><input class="uniform" name="status[]" type="checkbox" value="option1"> No Commission</label></div>
						</div>
                    </div>
                    <div class="form-group">
                        <label for="text1" class="control-label col-lg-4">&nbsp;</label>
                        <div class="col-lg-8">
                        <button type="submit" class="btn btn-primary">Generate Report</button>
                        </div>
					</div>
                </form>
            </div>
   		</div>
    </div>
</div>