<div id="div-1" class="body collapse in" aria-expanded="true">
    <div class="form-group">
        <label for="text1" class="control-label col-md-4">Subscribe Form Date</label>
        <div class="col-md-6">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                <input type="text" name="agreementdate" id="agreementdate" class="form-control" value="<?php echo date("d F Y"); ?>" required="required">
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="text1" class="control-label col-md-4">Duration of Contract</label>
        <div class="col-md-4">
            <div class="input-group">
            <input class="form-control" type="number" name="length_of_contract" min="0" required="required">
            <span class="input-group-addon">year</span>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="text1" class="control-label col-md-4">Upload Subscribe Form</label>
        <div class="col-md-6">
			<input type="file" name="filesf" id="filesf" class="form-control">
        </div>
    </div>
    <hr />
    <div class="form-group">
        <label for="text1" class="control-label col-md-4">Benefit</label>
        <div class="col-md-8">
            <div class="checkbox">
                <label><input class="uniform" type="radio" name="benefit" value="no website" checked="checked"> No website</label>
                <label><input class="uniform" type="radio" name="benefit" value="free website"> Free website</label>
                <label><input class="uniform" type="radio" name="benefit" value="paid website" href="#paidWebsite"> Paid website</label>
            </div>
        </div>
    </div> 
    <div id="paidWebsite" class="benefit" style="display:none;">
        <div class="form-group">
            <label for="text1" class="control-label col-md-4">Paid Website Amount</label>
            <div class="col-md-6">
                <input type="number" name="paidwebsite" class="form-control">
            </div>
        </div>
    </div>
    <hr />
    <div class="form-group">
        <label for="text1" class="control-label col-md-4">Commission Type</label>
        <div class="col-md-8">
            <div class="checkbox">
                <label><input class="uniform" type="radio" name="commissiontype" value="commission percent" href="#commissionPercentage" checked="checked"> Commission by percentage (%)</label>
                <label><input class="uniform" type="radio" name="commissiontype" value="flat fee" href="#flatFee"> Flat Fee</label>
            </div>
        </div>
    </div>
    <div id="flatFee" class="commtype" style="display:none;">
        <div class="form-group">
            <label for="text1" class="control-label col-md-4">Flat Fee Billing Periode</label>
            <div class="col-md-6">
                <select name="flatfeebillingtype" class="form-control chzn-select" tabindex="7">
                <option value="monthly">Monthly</option>
                <option value="annual">Annual</option>
                <option value="pertransaction">Amount Per Transaction</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="text1" class="control-label col-md-4">Flat Fee Amount</label>
            <div class="col-md-6">
                <input type="number" name="flatfee_amount" class="form-control">
            </div>
        </div>
    </div>
    <div id="commissionPercentage" class="commtype">
        <div class="form-group">
            <label for="text1" class="control-label col-md-4">The Buking Commission</label>
            <div class="col-md-4">
                <div class="input-group">
                <input class="form-control" type="number" name="thebuking_commission">
                <span class="input-group-addon">%</span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="text1" class="control-label col-md-4">Minimum Guarantee Commission</label>
            <div class="col-md-6">
                <input type="number" name="min_guarantee" class="form-control">
            </div>
        </div>
    </div>
    
    <hr />

    <div class="form-group">
        <label for="text1" class="control-label col-md-4">Connectivity :</label>
    </div>
    <div class="form-group">
        <label for="text1" class="control-label col-md-4">Channel Manager</label>
        <div class="col-md-2">
            <input type="radio" name="siteminder" value="y"/> yes
        </div>
        <div class="col-md-2">
            <input type="radio" name="siteminder" value="n" checked="checked" /> no
        </div>
    </div>
    <div class="form-group" style="display:none;">
        <label for="text1" class="control-label col-md-4">Channel Manager Name</label>
        <div class="col-md-8">
            <input type="text" name="siteminder_name" class="form-control"/> 
        </div>
    </div>
    <div class="form-group">
        <label for="text1" class="control-label col-md-4">Property Management System</label>
        <div class="col-md-2">
            <input type="radio" name="pms" value="y"/> yes
        </div>
        <div class="col-md-2">
            <input type="radio" name="pms" value="n" checked="checked" /> no
        </div>
    </div>
    <div class="form-group" style="display:none;">
        <label for="text1" class="control-label col-md-4">Property Management System Name</label>
        <div class="col-md-8">
            <input type="text" name="pms_name" class="form-control"/> 
        </div>
    </div>
    <div class="form-group">
        <label for="text1" class="control-label col-md-4">Internet Payment Gateway</label>
        <div class="col-md-2">
            <input type="radio" name="ipg" value="y"/> yes
        </div>
        <div class="col-md-2">
            <input type="radio" name="ipg" value="n" checked="checked" /> no
        </div>
    </div>
    <div class="form-group" style="display:none;">
        <label for="text1" class="control-label col-md-4">Internet Payment Gateway Name</label>
        <div class="col-md-8">
            <input type="text" name="ipg_name" class="form-control"/> 
        </div>
    </div>
    <hr />
    <div class="form-group">
        <label for="text1" class="control-label col-md-4">Company Name</label>
        <div class="col-md-8">
            <input type="text" name="companyname" class="form-control" required="required">
        </div>
    </div>
    <div class="form-group">
        <label for="text1" class="control-label col-md-4">Company NPWP</label>
        <div class="col-md-8">
            <input type="text" name="companynpwp" class="form-control" required="required">
        </div>
    </div>
    <div class="form-group">
        <label for="text1" class="control-label col-md-4">Company Address</label>
        <div class="col-md-8">
            <textarea id="companyaddress" name="companyaddress" class="form-control" required="required"></textarea>
        </div>
    </div>
    <div class="form-group">
        <label for="text1" class="control-label col-md-4">PIC Name</label>
        <div class="col-md-8">
            <input type="text" name="picname" class="form-control" required="required">
        </div>
    </div>
    <div class="form-group">
        <label for="text1" class="control-label col-md-4">PIC Title</label>
        <div class="col-md-8">
            <input type="text" name="pictitle" class="form-control" required="required">
        </div>
    </div>
    <div class="form-group">
        <label for="text1" class="control-label col-md-4">PIC ID Number</label>
        <div class="col-md-8">
            <input type="text" name="picidnumber" class="form-control" required="required">
        </div>
    </div>
    <hr />
    <div class="form-group">
        <label class="control-label col-md-3">Note</label>
        <div class="col-md-8">
            <textarea id="note" name="note" class="form-control"></textarea>
        </div>
    </div>
</div>