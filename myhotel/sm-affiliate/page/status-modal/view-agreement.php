<div class="modal fade" id="viewAgreementModal" tabindex="-1" role="dialog" aria-labelledby="viewAgreementModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">View Agreement Modal</h4>
            </div>
            <form class="form-horizontal">
            <div class="modal-body">
				<div class="row">
                    <div class="col-lg-12 col-xs-12" id="view-agreement-content">
					</div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="generate-pdf-by-view">Generate PDF</button>
            </div>
			</form>
        </div>
    </div>
</div>