<div class="modal fade statusmodal" id="sm-Modal" tabindex="-1" role="dialog" aria-labelledby="sm-Modal">
    <div class="modal-dialog" role="document">
        <form class="form-horizontal" method="post" action="<?=$base_url."/".$uri2?>/stage-process-updating" enctype="multipart/form-data">
            <div class="modal-content">
            </div>
        </form>
    </div>
</div>
<div class="modal fade wdmmodal" id="wdm-Modal" tabindex="-1" role="dialog" aria-labelledby="wdm-Modal">
    <div class="modal-dialog" role="document">
        <form class="form-horizontal" method="post" action="<?=$base_url."/".$uri2?>/stage-process-updating" enctype="multipart/form-data">
            <div class="modal-content">
            </div>
        </form>
    </div>
</div>
<div class="modal fade ibemodal" id="ibe-Modal" tabindex="-1" role="dialog" aria-labelledby="ibe-Modal">
    <div class="modal-dialog" role="document">
        <form class="form-horizontal" method="post" action="<?=$base_url."/".$uri2?>/stage-process-updating" enctype="multipart/form-data">
            <div class="modal-content">
            </div>
        </form>
    </div>
</div>
<div class="modal fade abortedmodal" id="aborted-Modal" tabindex="-1" role="dialog" aria-labelledby="ibe-Modal">
    <div class="modal-dialog" role="document">
        <form class="form-horizontal" method="post" action="<?=$base_url."/".$uri2?>/stage-process-updating" enctype="multipart/form-data">
            <div class="modal-content">
            </div>
        </form>
    </div>
</div>

<div class="modal fade" id="changeStatusConfirmationModal" tabindex="-1" role="dialog" aria-labelledby="changeStatusConfirmationModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-info-circle"></i> Notification</h4>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>
