<section class="content-header">
    <h1>Stage Process</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-bullhorn"></i>  Sales &amp; Marketing</a></li>
        <li class="active">Stage Process</li>
    </ol>
</section>
<section class="content">
  <div id="sm-content">
    <div id="dashboard" class="row">
      <div class="row">
      <?php
			$num = 0;
			$stmt = $db->query("SELECT * FROM `affiliatehotelstatus` as `as` where `as`.affiliatehotelstatusoid not in ('1', '2', '4', '8', '10','11', '12', '13') and  `as`.`publishedoid` = '1'");
			$list_affhotelstatus = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($list_affhotelstatus as $affhotelstatus){
				$num++;
				if($num % 3 == 1){ echo'</div><div class="row">'; }
				if($affhotelstatus['affiliatehotelstatusoid'] == 6){
					$liststatus = array(6, 14);
				}else if($affhotelstatus['affiliatehotelstatusoid'] == 7){
					$liststatus = array(7, 14, 15);
				}else if($affhotelstatus['affiliatehotelstatusoid'] == 8){
					$liststatus = array(8, 15);
				}else{
					$liststatus = $affhotelstatus['affiliatehotelstatusoid'];
				}

				if(is_array($liststatus)){
					$searchstatus = implode(',',$liststatus);
				}else{
					$searchstatus = $liststatus;
				}
			?>
            <div class="col-lg-4 col-xs-12">
                <div class="box inverse status-<?=$affhotelstatus['affiliatehotelstatusoid'];?>">
                    <header>
                        <h5><?=$affhotelstatus['status'];?></h5>
                        <!-- .toolbar -->
                        <div class="toolbar">
                          <nav style="padding: 8px;">
                          <input type="text" name="keyword" />
                          <button type="button" class="btn btn-xs btn-default" name="searchhotelstage" value="<?php echo $searchstatus; ?>"><i class="fa fa-search"></i></button>
                          </nav>
                        </div>
                        <!-- /.toolbar -->
                    </header>
                    <div id="div-2" class="body collapse in">
                        <?php showDataDashboard($affhotelstatus['affiliatehotelstatusoid'], $liststatus, '#sm-Modal'); ?>
                    </div>
                </div>
            </div>
            <?php
			}
			?>

      <?php
      /*-- ibe RFC box --*/
      $stmt = $db->query("SELECT * FROM `affibestatus` as `ai` where `ai`.`publishedoid` = '1' and ai.affibestatusoid in ('5')");
      $list_affhotelstatus = $stmt->fetchAll(PDO::FETCH_ASSOC);
      foreach($list_affhotelstatus as $affhotelstatus){
        $num++;
        if($num % 3 == 1){ echo'</div><div class="row">'; }
      ?>
        <div class="col-md-4 col-xs-12">
            <div class="box inverse status-<?=$affhotelstatus['affibestatusoid'];?>">
                <header>
                    <div class="icons"><i class="fa fa-th-large"></i></div>
                    <h5><?=$affhotelstatus['ibestatus'];?></h5>
                    <!-- .toolbar -->
                    <div class="toolbar">
                      <nav style="padding: 8px;">
                      <input type="text" name="keyword" />
                      <button type="button" class="btn btn-xs btn-default" name="searchibestage" type-status="ibe-status" value="<?=$affhotelstatus['affibestatusoid'];?>"><i class="fa fa-search"></i></button>
                      </nav>
                    </div>
                    <!-- /.toolbar -->
                </header>
                <div id="div-2" class="body collapse in">
                    <?php showDataIBE($affhotelstatus['affibestatusoid'], '#ibe-Modal'); ?>
                </div>
            </div>
        </div>
      <?php
      }
      ?>

      <?php
      /*-- WDM to Live --*/
      $stmt = $db->query("SELECT * FROM `affwdmstatus` as `aw` where `aw`.`publishedoid` = '1' and aw.affwdmstatusoid in ('6')");
      $list_affwdmstatus = $stmt->fetchAll(PDO::FETCH_ASSOC);
      foreach($list_affwdmstatus as $affwdm){
        $num++;
        if($num % 3 == 1){ echo'</div><div class="row">'; }
      ?>
        <div class="col-md-4 col-xs-12">
            <div class="box inverse status-<?=$affwdm['affwdmstatusoid'];?>">
                <header>
                    <div class="icons"><i class="fa fa-th-large"></i></div>
                    <h5><?=$affwdm['wdmstatus'];?></h5>
                    <!-- .toolbar -->
                    <div class="toolbar">
                      <nav style="padding: 8px;">
                      <input type="text" name="keyword" />
                      <button type="button" class="btn btn-xs btn-default" name="searchwdmstage" type-status="wdm-status" value="<?=$affwdm['affwdmstatusoid'];?>"><i class="fa fa-search"></i></button>
                      </nav>
                    </div>
                    <!-- /.toolbar -->
                </header>
                <div id="div-2" class="body collapse in">
                    <?php showDataWDM($affwdm['affwdmstatusoid'], '#wdm-Modal'); ?>
                </div>
            </div>
        </div>
      <?php
      }
      ?>

      <?php
      /*-- commerce box --*/
			$stmt = $db->query("SELECT * FROM `affiliatehotelstatus` as `as` where `as`.affiliatehotelstatusoid in ('8') and  `as`.`publishedoid` = '1'");
			$list_affhotelstatus = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($list_affhotelstatus as $affhotelstatus){
				$num++;
				if($num % 3 == 1){ echo'</div><div class="row">'; }
				if($affhotelstatus['affiliatehotelstatusoid'] == 6){
					$liststatus = array(6, 14, 15);
				}else if($affhotelstatus['affiliatehotelstatusoid'] == 7){
					$liststatus = array(7, 14);
				}else if($affhotelstatus['affiliatehotelstatusoid'] == 8){
					$liststatus = array(8, 15);
				}else{
					$liststatus = $affhotelstatus['affiliatehotelstatusoid'];
				}

				if(is_array($liststatus)){
					$searchstatus = implode(',',$liststatus);
				}else{
					$searchstatus = $liststatus;
				}
			?>
            <div class="col-lg-4 col-xs-12">
                <div class="box inverse status-<?=$affhotelstatus['affiliatehotelstatusoid'];?>">
                    <header>
                        <h5><?=$affhotelstatus['status'];?></h5>
                        <!-- .toolbar -->
                        <div class="toolbar">
                          <nav style="padding: 8px;">
                          <input type="text" name="keyword" />
                          <button type="button" class="btn btn-xs btn-default" name="searchhotelstage" value="<?php echo $searchstatus; ?>"><i class="fa fa-search"></i></button>
                          </nav>
                        </div>
                        <!-- /.toolbar -->
                    </header>
                    <div id="div-2" class="body collapse in">
                        <?php showDataDashboard($affhotelstatus['affiliatehotelstatusoid'], $liststatus, '#sm-Modal'); ?>
                    </div>
                </div>
            </div>
            <?php
			}
			?>
    	</div>
    </div>
  </div>
</section>
