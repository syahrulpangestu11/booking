<section class="content-header">
    <h1>Hotel Assessment</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-bullhorn"></i>  Sales &amp; Marketing</a></li>
        <li class="active">Hotel Assessment</li>
    </ol>
</section>
<section class="content">
  <div id="sm-content">
    <div id="dashboard" class="row">
      <div class="row">
      <?php
			$num = 0;
			$stmt = $db->query("SELECT * FROM `affiliatehotelstatus` as `as` where `as`.affiliatehotelstatusoid in ('1', '2') and  `as`.`publishedoid` = '1'");
			$list_affhotelstatus = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($list_affhotelstatus as $affhotelstatus){
				$num++;
				if($num % 2 == 1){ echo'</div><div class="row">'; }
				if($affhotelstatus['affiliatehotelstatusoid'] == 6){
					$liststatus = array(6, 14, 15);
				}else if($affhotelstatus['affiliatehotelstatusoid'] == 7){
					$liststatus = array(7, 14);
				}else if($affhotelstatus['affiliatehotelstatusoid'] == 8){
					$liststatus = array(8, 15);
				}else{
					$liststatus = $affhotelstatus['affiliatehotelstatusoid'];
				}

				if(is_array($liststatus)){
					$searchstatus = implode(',',$liststatus);
				}else{
					$searchstatus = $liststatus;
				}
			?>
            <div class="col-lg-6 col-xs-12">
                <div class="box inverse status-<?=$affhotelstatus['affiliatehotelstatusoid'];?>">
                    <header>
                        <h5><?=$affhotelstatus['status'];?></h5>
                        <!-- .toolbar -->
                        <div class="toolbar">
                          <nav style="padding: 8px;">
                          <input type="text" name="keyword" />
                          <button type="button" class="btn btn-xs btn-default" name="searchhotelstage" value="<?php echo $searchstatus; ?>"><i class="fa fa-search"></i></button>
                          </nav>
                        </div>
                        <!-- /.toolbar -->
                    </header>
                    <input type="hidden" name="page">
                    <input type="hidden" name="status" value ="<?=$affhotelstatus['affiliatehotelstatusoid'];?>">
                    <div id="div-2" class="body collapse in">
                        <?php showDataDashboard($affhotelstatus['affiliatehotelstatusoid'], $liststatus, '#sm-Modal', '0', ''); ?>
                    </div>
                </div>
            </div>
            <?php
			}
			?>
    	</div>
    </div>
  </div>
</section>
