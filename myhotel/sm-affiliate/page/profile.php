<?php
$profile = $affiliate->profileAffiliate($affiliate->affiliateoid);

$base_standard = 5;
$base_free_website = 7;
?>

<div class="row">
   <div class="col-lg-3 col-xs-12">
      <ul class="list-inline menu-tab">
      <li class="active"><a class="text-muted" href="#tab-profile" data-toggle="tab"><i class="fa fa-user-circle-o"></i> Profile</a></li>
      <li><a class="text-muted" href="#tab-bank-account" data-toggle="tab"><i class="fa fa-bank"></i> Bank Account</a></li>
      <li><a class="text-muted" href="#tab-change-password" data-toggle="tab"><i class="fa fa-lock"></i> Change Password</a></li>
      <li><a class="text-muted" href="#tab-commission" data-toggle="tab"><i class="fa fa-line-chart"></i> Affiliate Commission</a></li>
      </ul>
   </div>
   <div class="col-lg-9 col-xs-12">
      
<div class="tab-content">
    <div id="tab-profile" class="tab-pane active">
        <div class="box dark">
            <header>
                <div class="icons"><i class="fa fa-user-circle-o"></i></div>
                <h5>Profile</h5>
                <!-- .toolbar -->
                <div class="toolbar">
                  <nav style="padding: 8px;">
                      <a href="javascript:;" class="btn btn-default btn-xs collapse-box"><i class="fa fa-minus"></i></a>
                      <a href="javascript:;" class="btn btn-default btn-xs full-box"><i class="fa fa-expand"></i></a>
                  </nav>
                </div>            <!-- /.toolbar -->
            </header>
            <div id="div-1" class="body collapse in" aria-expanded="true">
                <form class="form-horizontal" id="">
                    <div class="form-group">
                        <label class="control-label col-lg-4">First Name</label>
                        <div class="col-lg-6">
                            <input type="text" name="firstname" placeholder="First Name" value="<?php echo $profile['firstname']; ?>" class="form-control" required="required">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-4">Last Name</label>
                        <div class="col-lg-6">
                            <input type="text" name="lastname" placeholder="Last Name" value="<?php echo $profile['lastname']; ?>" class="form-control" required="required">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-4">Email</label>
                        <div class="col-lg-6">
                            <input type="email" name="email" placeholder="Email" value="<?php echo $profile['email']; ?>" class="form-control" required="required">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-4">Phone</label>
                        <div class="col-lg-6">
                            <input type="email" name="phone" placeholder="Phone" value="<?php echo $profile['phone']; ?>" class="form-control" required="required">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-4">&nbsp;</label>
                        <div class="col-lg-6">
                            <button type="button" name="submit-account" value="profile" class="btn btn-primary">Save Change</button>
                        </div>
                    </div>
                </form>
            </div>
   		</div>
    </div>
    <div id="tab-bank-account" class="tab-pane">
        <div class="box dark">
            <header>
                <div class="icons"><i class="fa fa-bank"></i></div>
                <h5>Bank Account</h5>
                <!-- .toolbar -->
                <div class="toolbar">
                  <nav style="padding: 8px;">
                      <a href="javascript:;" class="btn btn-default btn-xs collapse-box">
                          <i class="fa fa-minus"></i>
                      </a>
                      <a href="javascript:;" class="btn btn-default btn-xs full-box">
                          <i class="fa fa-expand"></i>
                      </a>
                  </nav>
                </div>            <!-- /.toolbar -->
            </header>
            <div id="div-1" class="body collapse in" aria-expanded="true">
                <form class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-lg-4">Bank Name</label>
                        <div class="col-lg-6">
                            <input type="text" name="bank" placeholder="Bank Name" value="<?php echo $profile['bankname']; ?>" class="form-control" required="required">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-4">Branch</label>
                        <div class="col-lg-6">
                            <input type="text" name="branch" placeholder="Branch" value="<?php echo $profile['bankbranch']; ?>" class="form-control" required="required">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-4">Account Number</label>
                        <div class="col-lg-6">
                            <input type="text" name="accountnumber" placeholder="Account Number" value="<?php echo $profile['accountnumber']; ?>" class="form-control" required="required">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-4">Account Name</label>
                        <div class="col-lg-6">
                            <input type="text" name="accountname" placeholder="Account Name" value="<?php echo $profile['accountname']; ?>" class="form-control" required="required">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-4">&nbsp;</label>
                        <div class="col-lg-6">
                            <button type="button" name="submit-account" value="bank-account" class="btn btn-primary">Save Change</button>
                        </div>
                    </div>
                </form>
            </div>
   		</div>
    </div>
    <div id="tab-change-password" class="tab-pane">
        <div class="box dark">
            <header>
                <div class="icons"><i class="fa fa-lock"></i></div>
                <h5>Change Password</h5>
                <!-- .toolbar -->
                <div class="toolbar">
                  <nav style="padding: 8px;">
                      <a href="javascript:;" class="btn btn-default btn-xs collapse-box"><i class="fa fa-minus"></i></a>
                      <a href="javascript:;" class="btn btn-default btn-xs full-box"><i class="fa fa-expand"></i></a>
                  </nav>
                </div>            <!-- /.toolbar -->
            </header>
            <div id="div-1" class="body collapse in" aria-expanded="true">
                <form class="form-horizontal" id="">
                    <div class="form-group">
                        <label class="control-label col-lg-4">Password</label>
                        <div class="col-lg-6">
                            <input type="password" name="password" placeholder="Password" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-4">Confirm Password</label>
                        <div class="col-lg-6">
                            <input type="password" name="confirmpassword" placeholder="Confirm Passworde" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-4">&nbsp;</label>
                        <div class="col-lg-6">
                            <button type="button" name="submit-account" value="change-password" class="btn btn-primary">Save Change</button>
                        </div>
                    </div>
                </form>
            </div>
   		</div>
    </div>
    <div id="tab-commission" class="tab-pane">
        <div class="box dark">
            <header>
                <div class="icons"><i class="fa fa-line-chart"></i></div>
                <h5>Affiliate Commission</h5>
                <!-- .toolbar -->
                <div class="toolbar">
                  <nav style="padding: 8px;">
                      <a href="javascript:;" class="btn btn-default btn-xs collapse-box">
                          <i class="fa fa-minus"></i>
                      </a>
                      <a href="javascript:;" class="btn btn-default btn-xs full-box">
                          <i class="fa fa-expand"></i>
                      </a>
                  </nav>
                </div>            <!-- /.toolbar -->
            </header>
            <div id="div-1" class="body collapse in" aria-expanded="true">
                <form class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-lg-4">Affiliate Commission :</label>
                        <div class="col-lg-2">
                           <div class="input-group">
                               <input class="form-control" name="flatfeecommission" type="number" value="<?php echo $profile['flatfeecommission']; ?>">
                               <span class="input-group-addon">%</span>
                            </div>
			            </div>
                        <label class="control-label col-lg-5 text-left">from flat fee</label>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-4">Affiliate Commission :</label>
                        <div class="col-lg-2">
                           <div class="input-group">
                               <input class="form-control" name="commission" type="number" value="<?php echo $profile['commission']; ?>">
                               <span class="input-group-addon">%</span>
                            </div>
			            </div>
                        <label class="control-label col-lg-5 text-left">from agreement (percentage commission)</label>
                    </div>
                    <hr />
                    <h4>Standard</h4>
                    <div class="form-group">
                        <label class="control-label col-lg-4">Base Commission :</label>
                        <div class="col-lg-2">
                           <div class="input-group">
                               <input class="form-control" name="base_standard_commission" type="number" value="<?php echo $generaldata['base_standard_commission']; ?>" readonly="readonly">
                               <span class="input-group-addon">%</span>
                            </div>
			            </div>
                        <label class="control-label col-lg-5 text-left">The B&uuml;king</label>
                    </div>
                    <div class="form-group">
                        <label for="text1" class="control-label col-lg-4">Affiliate Commission Markup</label>
                        <div class="col-lg-4">
                            <div class="input-group">
                                <select class="form-control" name="markup_standard_commission" for="markup">
                                <?php
                                for($i = 1; $i <= $generaldata['limit_standard_commission']; $i++){
				                    if($i == $profile['markup_standard_commission']){ $selected = 'selected="selected"';  }else{ $selected =''; }
				                    echo "<option value='".$i."' ".$selected.">".$i."</option>";
                                }
                                ?>
                                </select>
                                <span class="input-group-addon">%</span>
                            </div>
                        </div>
                    </div>
                    <hr />
                    <h4>Free Website</h4>
                    <div class="form-group">
                        <label class="control-label col-lg-4">Base Commission :</label>
                        <div class="col-lg-2">
                           <div class="input-group">
                               <input class="form-control" name="base_freewebsite_commission" type="number" value="<?php echo $generaldata['base_freewebsite_commission']; ?>" readonly="readonly">
                               <span class="input-group-addon">%</span>
                            </div>
			            </div>
                        <label class="control-label col-lg-5 text-left">The B&uuml;king</label>
                    </div>
                    <div class="form-group">
                        <label for="text1" class="control-label col-lg-4">Affiliate Commission Markup</label>
                        <div class="col-lg-4">
                            <div class="input-group">
                                <select class="form-control" name="markup_freewebsite_commission" for="markup">
                                <?php
                                for($i = 1; $i <= $generaldata['limit_freewebsite_commission']; $i++){
				                    if($i == $profile['markup_freewebsite_commission']){ $selected = 'selected="selected"';  }else{ $selected =''; }
				                    echo "<option value='".$i."' ".$selected.">".$i."</option>";
                                }
                                ?>
                                </select>
                                <span class="input-group-addon">%</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-4">&nbsp;</label>
                        <div class="col-lg-6">
                            <button type="button" name="submit-account" value="aff-commission" class="btn btn-primary">Save Change</button>
                        </div>
                    </div>
                </form>
            </div>
   		</div>
    </div>
</div>

    </div>
</div>