<?php
$hoteloid = $uri4;
$stmt = $db->prepare("select h.*, c.name as chainname from hotel h left join chain c using (chainoid) where h.hoteloid = :a");
$stmt->execute(array(':a' => $hoteloid));
$hotel = $stmt->fetch(PDO::FETCH_ASSOC);
?>
<script type="text/javascript">
$(function(){
  $("button[name='savecontact']").click(function() {
    var name = $("input[name='cname']").val();
    var phone = $("input[name='cphone']").val();
    var email = $("input[name='cemail']").val();

    $.ajax({
      url: "<?php echo $base_url; ?>/sm-affiliate/page/hotel-detail/save-contact.php",
      type: 'post',
      data: { id : <?=$hoteloid?>, name : name, phone : phone, email : email },
      success: function(data){
        $("table#table-contact tbody").append("<tr><td></td><td>"+name+"</td><td>"+phone+"</td><td>"+email+"</td><td></td></tr>");
        $("table#table-contact input[type='text']").val('');
      }
    });
  });
});
</script>
<section class="content-header">
    <h1>Detail Property</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-bullhorn"></i> Sales &amp; Marketing</a></li>
        <li class="active"><a href="#"><i class="fa fa-building"></i> Detail Property</a></li>
    </ol>
</section>
<section class="content">
	<form class="form-horizontal" method="post" action="<?=$base_url."/".$uri2?>/stage-process-updating" enctype="multipart/form-data">
    <div class="row detail-hotel">
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="box dark">
                        <h3><?php echo $hotel['hotelname']; ?></h3>
                        Chain : <?php echo $hotel['chainname']; ?><br />
                        Type : Hotel<br />
                        Star Rating : <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><br />
                        <i class="fa fa-globe"></i> <a href="<?php echo $hotel['website']; ?>" target="_blank"><?php echo $hotel['website']; ?></a><br />
                        <i class="fa fa-map-marker"></i> <?php echo $hotel['address']; ?><br />
                        <i class="fa fa-phone"></i> <?php echo $hotel['phone']; ?> | <i class="fa fa-fax"></i> <?php echo $hotel['fax']; ?><br />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="box dark">
                        <header>
                            <div class="icons"><i class="fa fa-user-circle-o"></i></div>
                            <h5>Detail Subscribe Form</h5>
                            <div class="toolbar">
                              <nav style="padding: 8px;">
                                  <a href="javascript:;" class="btn btn-default btn-xs collapse-box"><i class="fa fa-minus"></i></a>
                                  <a href="javascript:;" class="btn btn-default btn-xs full-box"><i class="fa fa-expand"></i></a>
                              </nav>
                            </div>
                        </header>
                        <div id="div-1" class="body collapse in" aria-expanded="true">
                        <?php include('detail/log-subscribe-form.php'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="box dark">
                        <header>
                            <div class="icons"><i class="fa fa-address-book-o"></i></div>
                            <h5>Hotel Contact Management</h5>
                            <div class="toolbar">
                              <nav style="padding: 8px;">
                                  <a href="javascript:;" class="btn btn-default btn-xs collapse-box"><i class="fa fa-minus"></i></a>
                                  <a href="javascript:;" class="btn btn-default btn-xs full-box"><i class="fa fa-expand"></i></a>
                              </nav>
                            </div>
                        </header>
                        <div id="div-1" class="body collapse in" aria-expanded="true">
                        <table id="table-contact" class="table responsive-table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="input-contact">
                                  <td>#</td>
                                  <td><input type="text" class="form-control" name="cname"></td>
                                  <td><input type="text" class="form-control" name="cemail"></td>
                                  <td><input type="text" class="form-control" name="cphone"></td>
                                  <td><button type="button" class="btn btn-sm btn-primary" name="savecontact">Save</button></td>
                                </tr>
                                <?php
                                $no = 0;
                                $stmt = $db->query("select * from hotelcontact where hoteloid = '".$hoteloid."' AND publishedoid = '1' order by email");
                                $r_mail = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                foreach($r_mail as $row){
                                ?>
                                <tr>
                                    <td><?=++$no?></td>
                                    <td><?=$row['name']?></td>
                                    <td><?=$row['email']?></td>
                                    <td><?=$row['phone']?></td>
                                </tr>
                                <?php
                                }
                                ?>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="box dark">
                        <header>
                            <div class="icons"><i class="fa fa-folder-open"></i></div>
                            <h5>Hotel Bussiness Review</h5>
                            <div class="toolbar">
                              <nav style="padding: 8px;">
                                  <a href="javascript:;" class="btn btn-default btn-xs collapse-box"><i class="fa fa-minus"></i></a>
                                  <a href="javascript:;" class="btn btn-default btn-xs full-box"><i class="fa fa-expand"></i></a>
                              </nav>
                            </div>
                        </header>
                        <?php include('bussiness-review.php'); ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="box dark">
                        <header>
                            <div class="icons"><i class="fa fa-folder-open"></i></div>
                            <h5>Current Booking Enginee Information</h5>
                            <div class="toolbar">
                              <nav style="padding: 8px;">
                                  <a href="javascript:;" class="btn btn-default btn-xs collapse-box"><i class="fa fa-minus"></i></a>
                                  <a href="javascript:;" class="btn btn-default btn-xs full-box"><i class="fa fa-expand"></i></a>
                              </nav>
                            </div>
                        </header>
                        <div id="div-1" class="body collapse in" aria-expanded="true">
                            <div class="form-group">
                                <label for="text1" class="control-label col-md-5">Current Booking Enginee</label>
                                <div class="col-md-6">
                                    <input type="text" name="current_be_name" class="form-control" value="<?=$br['current_be_name']?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="text1" class="control-label col-md-5">End of contract</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                    	<span class="input-group-addon">month</span>
                                        <input type="text" name="end_of_contract_month" class="form-control" value="<?=$br['end_of_contract_month']?>">
                                        <span class="input-group-addon">year</span>
                                        <input type="text" name="end_of_contract_year" class="form-control" value="<?=$br['end_of_contract_year']?>">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="text1" class="control-label col-md-5">Penalty if broken the current contract</label>
                                <div class="col-md-7">
                                    <textarea class="form-control" name="penalty_if_broken_contract"><?=$br['penalty_if_broken_contract']?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
              <div class="col-md-12 col-xs-12">
                <?php if(!in_array($hotel['affiliatehotelstatusoid'], array('4', '13'))){ ?>
                <button type="button" class="btn btn-danger" name="aborted" data-toggle="modal" data-target="#aborted-Modal" data-id="<?=$hoteloid?>" data-status="<?=$hotel['affiliatehotelstatusoid']?>"><i class="fa fa-warning"></i> Aborted</button>
                <?php } ?>
              </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="box dark">
                        <header>
                            <div class="icons"><i class="fa fa-info-circle"></i></div>
                            <h5>Log Status</h5>
                            <div class="toolbar">
                              <nav style="padding: 8px;">
                                  <a href="javascript:;" class="btn btn-default btn-xs collapse-box"><i class="fa fa-minus"></i></a>
                                  <a href="javascript:;" class="btn btn-default btn-xs full-box"><i class="fa fa-expand"></i></a>
                              </nav>
                            </div>
                        </header>
                        <div id="div-1" class="body collapse in" aria-expanded="true">
                            <?php include('detail/sm-stage.php'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="box dark">
                        <header>
                            <div class="icons"><i class="fa fa-info-circle"></i></div>
                            <h5>Implementation Stage - WDM</h5>
                            <div class="toolbar">
                              <nav style="padding: 8px;">
                                  <a href="javascript:;" class="btn btn-default btn-xs collapse-box"><i class="fa fa-minus"></i></a>
                                  <a href="javascript:;" class="btn btn-default btn-xs full-box"><i class="fa fa-expand"></i></a>
                              </nav>
                            </div>
                        </header>
                        <div id="div-1" class="body collapse in" aria-expanded="true">
                          <?php include('detail/implementation-stage-wdm.php'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="box dark">
                        <header>
                            <div class="icons"><i class="fa fa-info-circle"></i></div>
                            <h5>Implementation Stage - IBE</h5>
                            <div class="toolbar">
                              <nav style="padding: 8px;">
                                  <a href="javascript:;" class="btn btn-default btn-xs collapse-box"><i class="fa fa-minus"></i></a>
                                  <a href="javascript:;" class="btn btn-default btn-xs full-box"><i class="fa fa-expand"></i></a>
                              </nav>
                            </div>
                        </header>
                        <div id="div-1" class="body collapse in" aria-expanded="true">
                          <?php include('detail/implementation-stage-ibe.php'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</section>
