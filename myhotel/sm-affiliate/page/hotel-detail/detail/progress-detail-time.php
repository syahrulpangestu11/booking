<?php
$stmt			= $db->prepare("select * from afflogdetail where affiliatelogoid = :a order by detail_date desc");
$stmt->execute(array(':a' => $row['affiliatelogoid']));
$count_progress	= $stmt->rowCount();

if($count_progress > 0){
?>
	<tr class="detail"><td></td><td colspan='7'>
	<?php
	foreach($stmt->fetchAll(PDO::FETCH_ASSOC) as $logdtl){
	?>
  &bull; <?=date('dmY', strtotime($logdtl['detail_date']))?> <?=date('H:i', strtotime($logdtl['detail_time']))?> - <?=$logdtl['detail_note']?><br>
	<?php
	}
	?>
	</td></tr>
<?php
}
?>
