<table id="log-status" class="table table-bordered responsive-table">
  <thead>
      <tr>
          <th>No.</th>
          <th>Date</th>
          <th>From</th>
          <th>To</th>
          <th>By</th>
          <th>Note</th>
          <th>Duration</th>
      </tr>
  </thead>
  <tbody>
  <?php
      $no = 0;
      $stmt = $db->prepare("select al.*, CASE WHEN x1.status IS NULL THEN x2.status ELSE x1.status END as statusfrom, y.status as statusto  from affiliatelog al
left join (select ahs.status, ahs.affiliatehotelstatusoid from affiliatehotelstatus ahs) x1 on x1.affiliatehotelstatusoid = al.from_status  and al.from_type = 'sm'
left join (select ai.ibestatus as status, ai.affibestatusoid from affibestatus ai) x2 on x2.affibestatusoid = al.from_status  and al.from_type = 'ibe'
inner join (select ai.ibestatus as status, ai.affibestatusoid from affibestatus ai) y on y.affibestatusoid = al.to_status
where al.hoteloid = :a and al.to_type = 'ibe' order by al.startdate desc");
      $stmt->execute(array(':a' => $hoteloid));
      foreach($stmt->fetchAll(PDO::FETCH_ASSOC) as $row){
  ?>
      <tr>
          <td><?php echo ++$no; ?></td>
          <td><?php echo date('d M y H:i', strtotime($row['startdate'])); ?></td>
          <td><?php echo $row['statusfrom']; ?></td>
          <td><?php echo $row['statusto']; ?></td>
          <td><?php echo $row['displayname']; ?></td>
          <td><?php echo $row['note']; ?></td>
          <td><?php if(!empty($row['idletime'])){ echo $row['idletime']; }else{ echo knowIdleTime($row['startdate'], date('Y-m-d H:i:s')); } ?></td>
      </tr>
      <?php
          if(in_array($row['to_status'], array(2, 3))){ include('progress-detail-time.php'); }
      }
  ?>
  </tbody>
</table>
