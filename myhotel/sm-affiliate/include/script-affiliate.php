<script type="text/javascript">
$(function(){

	$('body').on('click','#sm-content #paging button', function(e) {
		page	= $(this).attr('page');
		status = $(this).parent().parent().parent().parent().children('input[name=status]').val();
		pagebox = $(this).parent().parent().parent().parent().children('input[name=page]');
		keyword	= $(this).parent().parent().parent().parent().find('input[name=keyword]').val();
		box = $(this).parent().parent().parent();
		pagebox.val(page);
		$.post('<?php echo $base_url; ?>/sm-affiliate/include/load-stage-process.php', { page: page, status: status, keyword : keyword }, function(response){
			box.html(response);
		});
	});

	$('body').on('click', 'tr.direct-detail-hotel > td:not(:last-child)', function (e) {
		window.open('<?php echo $base_url; ?>/<?=$uri2?>/detail-sm-hotel/'+$(this).parent('tr').attr('data-id'),'_blank');
	});

	$('.statusmodal, .wdmmodal, .ibemodal').on('change', 'select[name=statusto]', function (e) {
		valelem = $(this).val();
		parent = $(this).closest('div.modal-body');
		parent.find('.box-detail-status').not('#box-status-'+valelem).hide();
		parent.find('.box-detail-status').not('#box-status-'+valelem).find('input, textarea, button').prop('disabled',true);
		parent.find('#box-status-'+valelem).show();
		parent.find('#box-status-'+valelem).find('input, textarea, button').prop('disabled',false);
		/*------------------------------------------------------------*/
		internal_mail_box	= $('.statusmodal').find('div#internal-mail');
		selectedstage		= $('select[name=statusto] option:selected').text();
		/*------------------------------------------------------------*/
		subject = internal_mail_box.find('input[name="subject-email"]');

		textsubject = "Change Status "+$('.statusmodal').find('h5#hotelname').html()+" from "+$('.statusmodal').find('input[name=currentstatus]').val()+" to "+selectedstage;
		subject.val(textsubject);
		/*------------------------------------------------------------*/
		editor = internal_mail_box.find('.trumbowyg-editor');

		target_next_stage = editor.find('span#writed-next-stage');
		target_next_stage.html(selectedstage);
		target_note = editor.find('span#writed-note');
		target_note.html('');
	});


	$('.statusmodal, .wdmmodal, .ibemodal').on('change', 'select[name=wdmto]', function (e) {
		valelem = $(this).val();
		parent = $(this).closest('div.modal-body');
		parent.find('.box-detail-wdm').not('#box-wdm-'+valelem).hide();
		parent.find('.box-detail-wdm').not('#box-wdm-'+valelem).find('input, textarea, button').prop('disabled',true);
		parent.find('#box-wdm-'+valelem).show();
		parent.find('#box-wdm-'+valelem).find('input, textarea, button').prop('disabled',false);
		/*------------------------------------------------------------*/
		internal_mail_box	= $('.wdmmodal').find('div#internal-mail');
		selectedstage		= $('select[name=wdmto] option:selected').text();
		/*------------------------------------------------------------*/
		subject = internal_mail_box.find('input[name="subject-email"]');

		textsubject = "Change Status "+$('.wdmmodal').find('h5#hotelname').html()+" from "+$('.wdmmodal').find('input[name=currentstatus]').val()+" to "+selectedstage;
		subject.val(textsubject);
		/*------------------------------------------------------------*/
		editor = internal_mail_box.find('.trumbowyg-editor');

		target_next_stage = editor.find('span#writed-next-stage');
		target_next_stage.html(selectedstage);
		target_note = editor.find('span#writed-note');
		target_note.html('');
	});

	$('.ibemodal').on('change', 'select[name=ibeto]', function (e) {
		valelem = $(this).val();
		parent = $(this).closest('div.modal-body');
		parent.find('.box-detail-ibe').not('#box-ibe-'+valelem).hide();
		parent.find('.box-detail-ibe').not('#box-ibe-'+valelem).find('input, textarea, button').prop('disabled',true);
		parent.find('#box-ibe-'+valelem).show();
		parent.find('#box-ibe-'+valelem).find('input, textarea, button').prop('disabled',false);
		/*------------------------------------------------------------*/
		internal_mail_box	= $('.ibemodal').find('div#internal-mail');
		selectedstage		= $('select[name=ibeto] option:selected').text();
		/*------------------------------------------------------------*/
		subject = internal_mail_box.find('input[name="subject-email"]');

		textsubject = "Change Status "+$('.ibemodal').find('h5#hotelname').html()+" from "+$('.ibemodal').find('input[name=currentstatus]').val()+" to "+selectedstage;
		subject.val(textsubject);
		/*------------------------------------------------------------*/
		editor = internal_mail_box.find('.trumbowyg-editor');

		target_next_stage = editor.find('span#writed-next-stage');
		target_next_stage.html(selectedstage);
		target_note = editor.find('span#writed-note');
		target_note.html('');
	});


	$('.statusmodal').on('change', 'input[name=commissiontype]', function (e) {
		if($(this).is(':checked') ){
			tabdiv = $(this).attr('href');
			parent = $(this).closest('div.modal-body');
			parent.find('.commtype').not(tabdiv).hide();
			parent.find('.commtype').not(tabdiv).find('input, textarea, button').prop('disabled',true);
			parent.find(tabdiv).show();
			parent.find(tabdiv).find('input, textarea, button').prop('disabled',false);
		}
	});

	$('.statusmodal').on('change', 'input[name=benefit]', function (e) {
		if($(this).is(':checked') ){
			tabdiv = $(this).attr('href');
			parent = $(this).closest('div.modal-body');
			parent.find('.benefit').not(tabdiv).hide();
			parent.find(tabdiv).show();
		}
	});

	$('body').on('change', 'input[name=markup_commission]', function (e) {
		aff_comm = parseFloat($(this).val());
		base_comm = parseFloat($('input[name="base_commission"][for="markup"]').val());
		tb_comm = aff_comm + base_comm;
		$('input[name="thebuking_commission"][for="markup"]').val(tb_comm);
	});

	$('body').on('click', 'input[name=siteminder], input[name=ipg], input[name=pms]', function (e) {
		nextdiv = $(this).parent().parent().next('div.form-group');
		if($(this).val() == "y"){
			nextdiv.show();
			nextdiv.find('input, textarea, button').prop('disabled',false);
		}else{
			nextdiv.hide();
			nextdiv.find('input, textarea, button').prop('disabled',true);
		}
	});


	$('body').on('focus','input[name=agreementdate]', function(e) {
		$(this).datepicker({
			beforeShow: function() {
				setTimeout(function(){ $('.ui-datepicker').css('z-index', 9999); }, 0);
			},
			defaultDate: "+0"
		});
	});
	$('body').on('focus','input[name=detail-date], input[name=dateoverride]', function(e) {
		$(this).datepicker({
			beforeShow: function() {
				setTimeout(function(){ $('.ui-datepicker').css('z-index', 9999); }, 0);
			},
			defaultDate: "+0"
		});
	});

	$('#viewAgreementModal').on('show.bs.modal', function (e) {
		$('.statusmodal').modal('hide');
		var modal = $(this);
		var id = $(e.relatedTarget).data('agreement');
		$.get('<?php echo $base_url; ?>/request/view-agreement.php', {
			id: id
      	}, function(response){
			modal.find('div#view-agreement-content').html(response);
		});
	});


	/*-----------------------------------------------------------------------------*/
	$('.statusmodal').on('show.bs.modal', function (e) {
		modal			= $(this);
		modal_content	= modal.find('.modal-content');
		var id = $(e.relatedTarget).data('id');
		var status = $(e.relatedTarget).data('status');
		$.get('<?php echo $base_url; ?>/sm-affiliate/request/detail-sm-status.php', {
			id: id, status:status
      	}, function(response){
			modal_content.html(response);
			$('textarea#html-box').trumbowyg({
				btns: ['viewHTML',
				  '|', 'btnGrp-design',
				  '|', 'link',
				  '|', 'btnGrp-justify',
				  '|', 'btnGrp-lists'],
				fullscreenable: false
			});

			modal.find('select[name=statusto]').change();
			modal.find('select[name=ibeto]').change();
			modal.find('select[name=wdmto]').change();
			modal.find('textarea[name=note]').keyup();
		});
	});
	$('.wdmmodal').on('show.bs.modal', function (e) {
		modal			= $(this);
		modal_content	= modal.find('.modal-content');
		var id = $(e.relatedTarget).data('id');
		$.get('<?php echo $base_url; ?>/sm-affiliate/request/detail-wdm-status.php', {
			id: id
      	}, function(response){
			modal_content.html(response);
			$('textarea#html-box').trumbowyg({
				btns: ['viewHTML',
				  '|', 'btnGrp-design',
				  '|', 'link',
				  '|', 'btnGrp-justify',
				  '|', 'btnGrp-lists'],
				fullscreenable: false
			});

			modal.find('select[name=statusto]').change();
			modal.find('select[name=ibeto]').change();
			modal.find('select[name=wdmto]').change();
			modal.find('textarea[name=note_wdm]').keyup();
		});
	});
	$('.ibemodal').on('show.bs.modal', function (e) {
		modal			= $(this);
		modal_content	= modal.find('.modal-content');
		var id = $(e.relatedTarget).data('id');
		$.get('<?php echo $base_url; ?>/sm-affiliate/request/detail-ibe-status.php', {
			id: id
      	}, function(response){
			modal_content.html(response);
			modal_content.html(response);
			$('textarea#html-box').trumbowyg({
				btns: ['viewHTML',
				  '|', 'btnGrp-design',
				  '|', 'link',
				  '|', 'btnGrp-justify',
				  '|', 'btnGrp-lists'],
				fullscreenable: false
			});

			modal.find('select[name=statusto]').change();
			modal.find('select[name=ibeto]').change();
			modal.find('select[name=wdmto]').change();
			modal.find('textarea[name=note_ibe]').keyup();
		});
	});
	$('.abortedmodal').on('show.bs.modal', function (e) {
		modal			= $(this);
		modal_content	= modal.find('.modal-content');
		var id = $(e.relatedTarget).data('id');
		var status = $(e.relatedTarget).data('status');
		$.get('<?php echo $base_url; ?>/sm-affiliate/request/detail-aborted.php', {
			id: id, status:status
      	}, function(response){
			modal_content.html(response);
			$('textarea#html-box').trumbowyg({
				btns: ['viewHTML',
				  '|', 'btnGrp-design',
				  '|', 'link',
				  '|', 'btnGrp-justify',
				  '|', 'btnGrp-lists'],
				fullscreenable: false
			});

			modal.find('select[name=statusto]').change();
			modal.find('textarea[name=note]').keyup();
		});
	});


	$('.statusmodal, .wdmmodal, .ibemodal').on('click', 'input:checkbox[name=training], input:checkbox[name="training-ibe"]', function (e) {
		$( "div.add-training-schedule" ).toggle( "slow", function() {
		});
	});

	$('body').on('click', 'button[name=searchhotelstage]', function (e) {
		status	= $(this).val();
		keyword	= $(this).parent().find('input[name=keyword]').val();
		target	= $(this).parent().parent().parent().parent().find('#div-2');
		$.post('<?php echo $base_url; ?>/sm-affiliate/request/search-hotel-stage.php', {
			status: status, keyword : keyword
      	}, function(response){
			target.html(response);
		});
	});

	$('body').on('click', 'button[name=searchwdmstage], button[name=searchibestage]', function (e) {
		status	= $(this).val();
		type	= $(this).attr("type-status");
		keyword	= $(this).parent().find('input[name=keyword]').val();
		target	= $(this).parent().parent().parent().parent().find('table').children('tbody');
		$.post('<?php echo $base_url; ?>/sm-affiliate/request/search-wdm-ibe-stage.php', {
			status: status, keyword : keyword, type : type
      	}, function(response){
			target.html(response);
		});
	});


	/*-------------------------------------------------------------------------*/

	$('.statusmodal').on('keyup', 'textarea[name=note], textarea[name=note_agreement], textarea[name=detail-note]', function (e) {
		textnote	= $(this).val();
		editor		= $('.statusmodal').find('div#internal-mail').find('.trumbowyg-editor');
		target_note = editor.find('span#writed-note');
		target_note.html(textnote);
		textbox = $('.statusmodal').find('div#internal-mail').find('textarea[name="email-internal"]');
		textbox.val(editor.html());
	});

	$('.wdmmodal').on('keyup', 'textarea[name=note_wdm], textarea[name=detail-note]', function (e) {
		textnote	= $(this).val();
		editor		= $('.wdmmodal').find('div#internal-mail').find('.trumbowyg-editor');
		target_note = editor.find('span#writed-note');
		target_note.html(textnote);
		textbox = $('.wdmmodal').find('div#internal-mail').find('textarea[name="email-internal"]');
		textbox.val(editor.html());
	});

	$('.ibemodal').on('keyup', 'textarea[name=note_ibe], textarea[name=detail-note]', function (e) {
		textnote	= $(this).val();
		editor		= $('.ibemodal').find('div#internal-mail').find('.trumbowyg-editor');
		target_note = editor.find('span#writed-note');
		target_note.html(textnote);
		textbox = $('.ibemodal').find('div#internal-mail').find('textarea[name="email-internal"]');
		textbox.val(editor.html());
	});

	$('.abortedmodal').on('keyup', 'textarea[name=note]', function (e) {
		textnote	= $(this).val();
		editor		= $('.abortedmodal').find('div#internal-mail').find('.trumbowyg-editor');
		target_note = editor.find('span#writed-note');
		target_note.html(textnote);
		textbox = $('.abortedmodal').find('div#internal-mail').find('textarea[name="email-internal"]');
		textbox.val(editor.html());
	});

	/*-------------------------------------------------------------------------*/

	$('body').on('click', 'button[name=delete]', function (e) {
		$(this).parent().parent().remove();
	});

	$('body').on('click', 'button[name=add-to-email]', function (e) {
		$('<div class="form-group"><div class="col-xs-10"><input type="text" name="to-email[]" class="form-control" value=""></div><div class="col-xs-2"><button type="button" name="delete" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button></div></div>').insertBefore(this);
	});

	$('body').on('click', 'button[name=add-cc-email]', function (e) {
		$('<div class="form-group"><div class="col-xs-10"><input type="text" name="cc-email[]" class="form-control" value=""></div><div class="col-xs-2"><button type="button" name="delete" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button></div></div>').insertBefore(this);
	});

});
</script>
