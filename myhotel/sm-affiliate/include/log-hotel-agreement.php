<?php
$stmt = $db->prepare("select * from affiliatehotel_agreementlog where affiliatelogoid = :a");
$stmt->execute(array(':a' => $row['affiliatelogoid']));
$agreement = $stmt->fetch(PDO::FETCH_ASSOC);
?>
<br>
<b>Agreement Date : </b><?php echo date('d F Y', strtotime($agreement['agreementdate'])); ?></br>
<b>Length of Contract : </b><?php echo $agreement['length_of_contract']; ?> year</br>
<hr />
<b>Website Benefit :</b> <?php echo $agreement['benefit_website']; ?><br />
<?php if($agreement['benefit_website'] == "free website"){ ?>
<b>Base Commission :</b> <?php echo $agreement['base_benefit_freewebsite']; ?>%<br />
<b>Affiliate Commission Markup :</b> <?php echo $agreement['markup_benefit_freewebsite']; ?>%<br />
<?php }else if($agreement['commissiontype'] == "paid website"){ ?>
<b>Paid Amount :</b> <?php echo number_format($agreement['benefit_website_amount']); ?>
<?php } ?>
<hr />
<b>Commission Type :</b> <?php echo $agreement['commissiontype']; ?><br />
<?php if($agreement['commissiontype'] == "flat fee") { ?>
<b>Flat Fee Billing Periode :</b> <?php echo $agreement['flatfee_billingtype']; ?><br />
<b>Flat Fee Amount :</b> <?php echo number_format($agreement['flatfee_amount']); ?><br />
<b>Affiliate Commission :</b> <?php echo $agreement['flatfee_commission']; ?>% from flat fee amount<br />
<?php }else if($agreement['commissiontype'] == "commission percent"){ ?>
<b>The B&uuml;king Commission :</b> <?php echo $agreement['thebuking_commission']; ?>%<br />
<b>Affiliate Commission :</b> <?php echo $agreement['affiliate_commission']; ?>% from agreement<br />
<b>Minimum Guarantee Commission :</b> <?php echo number_format($agreement['min_guarantee_commission']); ?>
<?php }else if($agreement['commissiontype'] == "commission markup"){ ?>
<b>Base Commission :</b> <?php echo $agreement['base_commission']; ?>%<br />
<b>Affiliate Commission Markup :</b> <?php echo $agreement['affiliate_commission_markup']; ?>%<br />
<b>Agreement Commission :</b> <?php echo $agreement['thebuking_commission']; ?>%<br />
<b>Minimum Guarantee Commission :</b> <?php echo number_format($agreement['min_guarantee_commission']); ?>
<?php } ?>
<hr />
<b>Siteminder : </b><?php echo $agreement['siteminder']; ?></br>
<b>Property Management System : </b><?php echo $agreement['pms']; ?></br>
<b>Internet Payment Gateway : </b><?php echo $agreement['ipg']; ?></br>
<hr />
<b>Company Name : </b><?php echo $agreement['company_name']; ?></br>
<b>Company Address : </b><?php echo $agreement['company_address']; ?></br>
<b>Company NPWP: </b><?php echo $agreement['company_npwp']; ?></br>
<hr />
<b>PIC Name : </b><?php echo $agreement['pic_name']; ?></br>
<b>PIC Title : </b><?php echo $agreement['pic_title']; ?></br>
<b>PIC ID Number : </b><?php echo $agreement['pic_id_number']; ?></br>


