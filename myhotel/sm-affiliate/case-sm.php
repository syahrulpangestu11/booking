    <!-- Metis core stylesheet -->
    <link rel="stylesheet" href="<?php echo $base_url; ?>/sm-affiliate/assets/css/main.css?v=<?=date('YmdHis')?>">
    <!-- metisMenu stylesheet -->
    <link rel="stylesheet" href="<?php echo $base_url; ?>/sm-affiliate/assets/lib/metismenu/metisMenu.css">
    <!-- animate.css stylesheet -->
    <link rel="stylesheet" href="<?php echo $base_url; ?>/sm-affiliate/assets/lib/animate.css/animate.css">
    <link rel="stylesheet/less" type="text/css" href="<?php echo $base_url; ?>/sm-affiliate/assets/less/theme.less">
    <script src="<?php echo $base_url; ?>/sm-affiliate/assets/less/less.js"></script>
    <link rel="stylesheet" href="<?php echo $base_url; ?>/sm-affiliate/assets/css/reset.css?v=<?=date('YmdHis')?>">

<?php
	include ('includes/bootstrap.php');
	include ('include/script-affiliate.php');
	include ('include/function-affiliate.php');

	switch($uri3){
		case 'stage-process'			: include ('page/stage-process.php'); break;
		case 'stage-process-updating'	: include ('request/change-log-status.php'); break;
    case 'hotel-assessment' : include ("page/hotel-assessment.php"); break;
    case 'wdm-stage' : include ("page/wdm-stage.php"); break;
    case 'ibe-stage' : include ("page/ibe-stage.php"); break;
    case 'other-stage' : include ("page/other-stage.php"); break;
		case 'management-summary'	: include ("includes/management/dashboard/management-dashboard.php"); break;
		case 'production-detail'	: include ("includes/management/dashboard/transaction-details-production.php"); break;
		case 'occupancy-detail'		: include ("includes/management/dashboard/transaction-details-occupancy.php"); break;
		case 'bispro-report'		: include ('page/bispro-report.php'); break;
		case 'register-hotel' : include ('page/hotel-add.php'); break;
		case 'register-process-hotel' : include ('page/include/register-hotel.php'); break;
		case 'list-hotel' : include ('page/hotel-list/hotel-list.php'); break;
		case 'detail-sm-hotel' : include ('page/hotel-detail/hotel-detail.php'); break;
    case 'kpi' : include ('page/kpi/view-kpi.php'); break;
    case 'daily-update-sm' : include ('page/daily-update-sm.php'); break;
	}

	include('page/status-modal.php');
?>
