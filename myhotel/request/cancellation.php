<?php
session_start();
include "../conf/connection.php";
// include "../../ibe/modify/include/class-modifyBooking.php";
include "class-modifyBooking.php";

try {
	$bookingoid			= $_POST['bid'];
	$bookingstatusoid	= $_POST['changestatus'];
	$cancellationamount	= $_POST['cancellationamount'];
	$cancellationreason	= $_POST['cancellationreason'];

	$booking		= new modifyBooking($db);	
	$booking->setBooking($bookingoid);
	$detail_booking = $booking->masterBooking();
	$hoteloid		= $detail_booking['hoteloid'];
	
	$booking->setBookingStatus($bookingstatusoid);
	
	$finalrevenue		= $cancellationamount;
	$finalcommission	= $finalrevenue * $booking->commission / 100;
	$nethotel			= $finalrevenue - $finalcommission;
	
	$stmt = $db->prepare("update booking set hotelcollect = :b, gbhcollect = :c, cancellationamount= :d, bookingstatusoid = :e, cancellationreason = :f, updated = :g, updatedby = :h where bookingoid = :id");
	$stmt->execute(array(':b' => $nethotel, ':c' => $finalcommission, ':d' => $cancellationamount, ':e' => $booking->bookingstatus, ':f' => $cancellationreason, ':g' => date('Y-m-d H:i:s'), ':h' => $_SESSION['_user'], ':id' => $booking->bookingoid));
	
	//------------------------------------------------
	
	if(isset($_POST['pms']) && $_POST['pms'] == '1'){
	    
	    // // Prepare Data
        // $stmt = $db->prepare("SELECT `roomoid`, `bookingroomoid` FROM `roomoffer` INNER JOIN `bookingroom` USING(`roomofferoid`) WHERE `bookingoid` = :a");
        // $stmt->execute(array(':a' => $bookingoid));
        // $r_ro = $stmt->fetch(PDO::FETCH_ASSOC);
        // $vroomoid = $r_ro['roomoid'];
        // $bookingroomoid = $r_ro['bookingroomoid'];
        
        // $stmt = $db->prepare("SELECT COUNT(`roomnumberoid`) AS jml FROM `roomnumber` WHERE `roomoid` = :a AND `publishedoid` = 1");
        // $stmt->execute(array(':a' => $vroomoid));
        // $r_ro = $stmt->fetch(PDO::FETCH_ASSOC);
        // $vroomnumber = $r_ro['jml'];
        
        // $stmt = $db->prepare("SELECT `hotelcode` FROM `hotel` WHERE hoteloid = :id");
        // $stmt->execute(array(':id' => $hoteloid));
        // $r_ho = $stmt->fetch(PDO::FETCH_ASSOC);
        
        // $data6 = '{
        //     "token": "pfUxpxmKTJNTStNhPK2Nly4UKxbcVWwNRUT1SXnPfRiWqhvmINnJ4XnefNNBuTsx",
        //     "hcode": "'.$r_ho['hotelcode'].'",
        //     "roomid": "'.$vroomoid.'",
        //     "data": [';
        // $z = 0;
        
        // $stmt = $db->prepare("SELECT * FROM `bookingroomdtl` WHERE bookingroomoid = :id");
        // $stmt->execute(array(':id' => $bookingroomoid));
        // $r_brd = $stmt->fetchAll(PDO::FETCH_ASSOC);
        
        // foreach($r_brd as $a_brd){
            
        //     // Prepare Data
        //     $vdate = $a_brd['date'];
            
        //     $stmt = $db->prepare("SELECT COUNT(`bookingroomdtloid`) jml
        //             FROM `bookingroomdtl` brd
        //                 LEFT JOIN `bookingroom` br USING(`bookingroomoid`)
        //                 LEFT JOIN `booking` b USING(`bookingoid`)
        //                 LEFT JOIN `roomoffer` ro USING(`roomofferoid`)
        //             WHERE b.`bookingstatusoid` IN (4) AND br.`pmsstatusoid` IN (2,4,7) 
        //             	AND ro.`roomoid` = :a AND brd.`date` = :b");
        //     $stmt->execute(array(':a' => $vroomoid, ':b' => $vdate));
        //     $r_ro = $stmt->fetch(PDO::FETCH_ASSOC);
        //     $vtxnumber = $r_ro['jml'];
            
        //     $vval = $vroomnumber - $vtxnumber;
            
        //     if($z != 0){ $data6 .=","; }else{ $z = 1; }
        //     $data6 .= '
        //         {
        //             "from": "'.$vdate.'",
        //             "to": "'.$vdate.'",
        //             "value": "'.$vval.'"
        //         }
        //     ';
            
        // }
        
        // // Prepare Data
        // $data6 .= '
        //     ]
        // }';
        
        // // The data to send to the API
        // $postData = (array) json_decode($data6);
        
        // // Setup cURL
        // $ch = curl_init('https://thebuking.com/apiv1/save/allotment_inrange/');
        // curl_setopt_array($ch, array(
        //     CURLOPT_POST => TRUE,
        //     CURLOPT_RETURNTRANSFER => TRUE,
        //     CURLOPT_SSL_VERIFYPEER => FALSE,
        //     CURLOPT_HTTPHEADER => array(
        //         'Content-Type: application/json'
        //     ),
        //     CURLOPT_POSTFIELDS => json_encode($postData)
        // ));
        
        // // Send the request
        // $response = curl_exec($ch);
        
        // // Check for errors
        // if($response === FALSE){
        //     die(curl_error($ch));
        // }
        
        // // Decode the response
        // $responseData = json_decode($response, TRUE);
	}else{

        // Allotment IBE Cancellation
        $stmt = $db->prepare("SELECT `hotelcode` FROM `hotel` WHERE hoteloid = :id");
        $stmt->execute(array(':id' => $hoteloid));
        $r_ho = $stmt->fetch(PDO::FETCH_ASSOC);
        $hotelcode = $r_ho['hotelcode'];

        $stmt = $db->prepare("SELECT `roomoid`, `bookingroomoid` FROM `roomoffer` INNER JOIN `bookingroom` USING(`roomofferoid`) WHERE `bookingoid` = :a");
        $stmt->execute(array(':a' => $bookingoid));
        $r_ro = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach($r_ro as $ar_ro){
            $vroomoid = $ar_ro['roomoid'];
            $bookingroomoid = $ar_ro['bookingroomoid'];

            $stmt = $db->prepare("SELECT * FROM `bookingroomdtl` WHERE bookingroomoid = :id");
            $stmt->execute(array(':id' => $bookingroomoid));
            $r_brd = $stmt->fetchAll(PDO::FETCH_ASSOC);
            
            foreach($r_brd as $a_brd){
                $vdate = $a_brd['date'];

                $path_xml_hotel = "../data-xml/".$hotelcode.".xml";
                $xml = new DomDocument();
                $xml->preserveWhitespace = false;
                $xml->load($path_xml_hotel);
                $xpath = new DomXpath($xml);
                $root = $xml->documentElement;
                $xml_file =  simplexml_load_file($path_xml_hotel);

                $query_tag_allotment_channel = '//hotel[@id="'.$hoteloid.'"]//masterroom[@id="'.$vroomoid.'"]/rate[@date="'.$vdate.'"]/allotment/channel[@type="1"]';
                $query_tag_allotment_channel_value = $query_tag_allotment_channel.'/text()';

                $allotment = 0;
                foreach($xml_file->xpath($query_tag_allotment_channel_value) as $tagallotment){
                    $allotment = (int)$tagallotment[0];
                }

                $new_allotment = $allotment + 1;

                $check_tag_allotment_channel = $xpath->query($query_tag_allotment_channel);
                $check_tag_allotment_channel->item(0)->nodeValue = $new_allotment;

                $xml->formatOutput = true;
                $xml->save($path_xml_hotel) or die("Error");
            }
        }


        // //-- PatchStaah
        // include("../../ibe/component/patch_staah.php");
        // $staah_connect = getHotelStaahConnect($hoteloid);
        // //-- END PatchStaah
        
        // //STAAH
        // if($staah_connect){
        //     $bookingnumber = $detail_booking['bookingnumber'];
        //     include("../../ibe/function/send_staah_cancel.php");
        // }
    }
	
	//------------------------------------------------
	
// 	ob_start();
// 		include '../../mail/email-cancellation-reservation.php';
// 	$body = ob_get_clean();
// 	$subject = "Booking Cancellation for ".$hotelname." ".$bookingnumber;
	
// 	require_once('../includes/mailer/class.phpmailer.php');
// 	$mail = new PHPMailer(true);
	
// 	try{
		
// 	    //PMS
//     	if(isset($_POST['pms']) && $_POST['pms'] == '1'){
//     	    require_once('../../mail/mail-smtp/default-setting-mail-pms.php');
    	    
//     	    $subject = "PMS Cancellation for ".$hotelname." ".$bookingnumber;
    	    
//     	    if(isset($_POST['sendtoguest']) && $_POST['sendtoguest']=='1'){
//     	        $mail->AddAddress($email, $name);
//     	    }else{
//     	        $mail->AddAddress("pms@thebuking.com", "TheBuking PMS");
//     	    }
//     	}else{

// 		    require_once('../../mail/mail-smtp/default-setting-mail.php');
				
// 		    $mail->AddAddress($email, $name);
    	    
//     	}
		
// 		$mail->Subject = $subject;
// 		$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
// 		$mail->MsgHTML($body);

// 		$mail->Send();
		
		
// 		echo "success";


	// } catch (phpmailerException $e) {
	//     if($e->errorMessage() == "<strong>SMTP Error: Data not accepted.</strong><br />"){
	// 	    echo "success";
	//     }else{
	//         echo"<b>Php Mailer Error :</b><br>"; echo $e->errorMessage();
	//     }
	// } catch (Exception $e) {
	// 	echo"<b>All Error :</b><br>"; echo $e->getMessage();
    // }
    include '../includes/mailer/sending_mail_reservation_cancellation.php';
}catch(Exception $ex) {
	print($ex ->getMessage());
	die();
}

?>