<?php
	include "../conf/connection.php";

	$keyword = trim($_REQUEST['term']);
	if(isset($_REQUEST['country'])){
		$showncountry = "'".implode("','", $_REQUEST['country'])."'";
	}else if(isset($_REQUEST['noshow_country'])){
		$showncountry = "'".implode("','", $_REQUEST['noshow_country'])."'";
	}else{
		$showncountry = "''";
	}

	
	$query = "select countryoid, countryname from country c where countryname like '%".$keyword."%' and countryoid not in (".$showncountry.")";
	$stmt = $db->query($query);
	$fetch = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	$found = 0;
	
	foreach($fetch as $row){
		$found++;
		
		$result['value']	= htmlentities($row['countryname']);
		$result['id']		= $row['countryoid'];
		$row_set[]			= $result;
	}
	
	echo json_encode($row_set);
?>