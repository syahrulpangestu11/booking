<?php

// set API Endpoint and access key (and any options of your choice)
$endpoint = 'live';
$access_key = 'e26a85ed7a5fa7def4c07c751cbb3635';

// Initialize CURL:
$ch = curl_init('http://apilayer.net/api/'.$endpoint.'?access_key='.$access_key.'');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

// Store the data:
$json = curl_exec($ch);
curl_close($ch);

// Decode JSON response:
$exchangeRates = json_decode($json, true);

include("../myhotel/conf/connection.php");

$i = 0; $j = 0;
foreach ($exchangeRates['quotes'] as $key => $value) {
    // Access the exchange rate values, e.g. GBP:
    
    $sourcekey = substr($key, 0, 3);
    $resultkey = substr($key, 3, 3);

    $sourcenom = 1;
    $resultnom = $value;

    $dateupdate = date("Y-m-d H:i:s");

    $stmt = $db->prepare("SELECT `converteroid` FROM `currencyconverter` WHERE `sourcecurrency`=:a AND `resultcurrency`=:b");
    $stmt->execute(array(
        ':a' => $sourcekey,
        ':b' => $resultkey));

    $r_check = $stmt->fetchAll(PDO::FETCH_ASSOC);

    if (count($r_check) < 1) {
        # no data in db
        $stmt = $db->prepare("INSERT INTO `currencyconverter`(`converteroid`, `sourcecurrency`, `resultcurrency`, `sourcenominal`, `resultnominal`, `dateupdate`, `publishedoid`) 
            VALUES (NULL,:a,:b,:c,:d,:e,'1')");
        $stmt->execute(array(
            ':a' => $sourcekey,
            ':b' => $resultkey,
            ':c' => $sourcenom,
            ':d' => $resultnom,
            ':e' => $dateupdate));
        $i++;
    }else{
        # have been inserted before
        foreach($r_check as $a_check){
            $converteroid = $a_check['converteroid'];
        }
        $stmt = $db->prepare("UPDATE `currencyconverter` SET `sourcenominal`=:a, `resultnominal`=:b, `dateupdate`=:c WHERE `converteroid`=:d");
        $stmt->execute(array(
            ':a' => $sourcenom,
            ':b' => $resultnom,
            ':c' => $dateupdate,
            ':d' => $converteroid));
        $j++;
    }
}

echo "Completed on ".($i+$j)." series of data. ".$i." data has been inserted, ".$j." data has been updated.";