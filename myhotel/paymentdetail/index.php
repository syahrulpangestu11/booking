<?php
session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>The B&uuml;king Verify Login - Payment Detail</title>

	<style>
		@font-face{
			font-family:'Open Sans';
			src:url(../css/fonts/open-sans/OpenSans-Regular.eot) format('truetype');
		}
		body{
			font-family: "Open Sans", "Lucida Grande", Tahoma, Helvetica, Arial, sans-serif;
			font-size:12px;
			letter-spacing:1px;
		}
		ul.block{
			list-style-type: none;
			margin:0;
			padding:0;
		}
		ul.block > li, ul.block > li > label, ul.block > li > span{
			display: block;
		}
		ul.block > li{
			padding-bottom:15px;
		}
		#verifyForm{
			display:table;
			border: 1px solid #E2F0F7;
			background-color: #F3F8F9;
			padding:10px 50px;
			margin:17% auto;
		}
		label{
			font-weight:bold;
			padding-bottom:5px;
		}
		input[type=text], input[type=password]{
			border:1px solid #09C;
			padding:5px 8px;
		}
		input[type=submit], input[type=button]{
			padding:10px;
			width:100%;
			background-color:#46B8F4;
			border:1px solid #09C;
			color:#FFF;
			font-weight:bold;
		}
	</style>
</head>

<body>
	<div id="verifyForm">
	<?php
		include "../conf/connection.php";
		if(isset($_POST['verify']) and  $_POST['verify'] == "VERIFY LOGIN" and !empty($_SESSION['_oid'])){
			if((isset($_POST['username']) and isset($_POST['password'])) and (!empty($_POST['username']) and !empty($_POST['password']))){
				
				try{
					$stmt = $db->prepare("SELECT * FROM users inner join userstype using (userstypeoid) WHERE username = :usr AND password = SHA1(:pwd) AND status = :status AND useroid = :a");
					$stmt->execute(array(':usr' => $_POST['username'], ':pwd' => $_POST['password'], ':status' => 1, ':a' => $_SESSION['_oid']));
					$row_count = $stmt->rowCount();
						
					if($row_count == 1) {	
						$syntax_payment	= "select bookingpayment.*, creditcard.* 
							from bookingpayment 
								left join creditcard using (cardoid) 
								inner join booking b using (bookingoid) 
								inner join bookingroom br using (bookingoid)
							where b.bookingnumber = :as and b.hoteloid = :ax and (stcounter is null or stcounter < 3) 
								and b.bookingstatusoid = 4 and br.checkin >= :ae ";
						$stmt = $db->prepare($syntax_payment);
						$stmt->execute(array(':as' => $_POST['bookingnumber'], ':ax' => $_SESSION['_hotel'], ':ae' => date('Y-m-d')));
						$result_payment	= $stmt->fetchAll(PDO::FETCH_ASSOC);
						if(count($result_payment) > 0){
							foreach($result_payment as $payment){
								$paymentoid = $payment['paymentoid'];
								$typepayment = $payment['type'];
								$cc_type = $payment['cardname'];
								$cc_number = $payment['cardnumber'];
								$cc_holder = $payment['cardholder'];
								$cc_exp_month = $payment['expmonth'];
								$cc_exp_year = $payment['expyear'];
								$cc_csc = $payment['cvc'];
								$stcounter = $payment['stcounter'];
							}

							$stcounter = $stcounter + 1;

							$stmt = $db->prepare("UPDATE `bookingpayment` SET `stcounter`=:a, `stdate`=:b WHERE `paymentoid`=:c");
							$stmt->execute(array(':a' => $stcounter, ':b' => date('Y-m-d H:i:s'), ':c' => $paymentoid));

						}else{
							echo 'Please contact administrator account to access this content.';
							die();
						}
			?>
					<table>
						<tr><td colspan="3"><h3>PAYMENT DETAIL</h3></td></tr>
						<tr><td>Card Type</td><td>:</td><td><?=$cc_type?></td></tr>
						<tr><td>Card Number</td><td>:</td><td><?=$cc_number?></td></tr>
						<tr><td>CVC</td><td>:</td><td><?=$cc_csc?></td></tr>
						<tr><td>Card Holder</td><td>:</td><td><?=$cc_holder?></td></tr>
						<tr><td>Card Expiry (month / year)</td><td>:</td><td><?=$cc_exp_month?> / <?=$cc_exp_year?></td></tr>
					</table>
			<?php
					}else{
						$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
						echo '<script>location.href = "'.$actual_link.'"</script>';
						die();
					}
					
					unset($_POST['verify']);
					unset($_POST['username']);
					unset($_POST['password']);
					
					$_POST['verify'] = "";
					$_POST['username'] = "";
					$_POST['password'] = "";

				}catch(PDOException $ex){
					//echo '';
				}
				
			}
		}else{
    ?>
	<form method="post">
    	<ul class="block">
        	<li>
        		<label>Username :</label>
                <span><input type="text" name="username" /></span>
            </li>
        	<li>
        		<label>Password :</label>
                <span><input type="password" name="password" /></span>
            </li>
        	<li>
                <span><input type="submit" name="verify" value="VERIFY LOGIN" /></span>
            </li>
        </ul>
        <input type="hidden" name="bookingnumber" value="<?=$_GET['bookingnumber']?>" />
    </form>
    <?php 
		} 
	?>
    </div>
</body>
</html>
