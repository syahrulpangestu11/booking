<?php
	ob_start();
  session_start();
	error_reporting(E_ALL ^ E_NOTICE);
  include "conf/baseurl.php";
  include "conf/connection.php";
	include "includes/php-function.php";

  $fileVersion = date("Y.m.d.H.i.s");
  
  if($uri2=="export"){
      include("active_login_check.php");
      switch($uri3){
          case 'member':
              include('includes/crm/member/export_xls.php');
              die();
              break;
      }
  }
?>
<!DOCTYPE html>
<html>
<head>
<title>The B&uuml;king Hotels Extranet</title>
<?php
include("lib/Mobile-Detect-2.8.26/Mobile_Detect.php");
$detect = new Mobile_Detect;
if(($detect->isMobile() or $detect->isTablet()) and (!isset($_SESSION['_dekstopversionview']) or $_SESSION['_dekstopversionview'] == false)){
	$_SESSION['_dekstopversionview'] = false;
?>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<?php
}else{
	$_SESSION['_dekstopversionview'] = true;
}
?>

<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" href="<?php echo"$base_url"; ?>/images/favicon.png" type="images/x-icon">
<link rel="stylesheet" href="<?php echo"$base_url"; ?>/css/font-awesome-4.7.0/css/font-awesome.min.css" type="text/css">
<link rel="stylesheet" href="<?php echo"$base_url"; ?>/css/style.css?v=<?=$fileVersion;?>" type="text/css">

<script type="text/javascript" src="<?php echo"$base_url"; ?>/js/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="<?php echo"$base_url"; ?>/js/validation/jquery-validate.js"></script>
<script  type="text/javascript" src="<?php echo"$base_url"; ?>/js/ui-1.11.2/jquery-ui.js"></script>
<link rel="stylesheet" href="<?php echo"$base_url"; ?>/js/ui-1.11.2/jquery-ui.css">
<link rel="stylesheet" href="<?php echo"$base_url"; ?>/js/iCheck/skins/square/blue.css">
<script type="text/javascript" src="<?php echo"$base_url"; ?>/js/iCheck/icheck.js"></script>
<script type="text/javascript" src="<?php echo"$base_url"; ?>/js/script.js?v=<?=$fileVersion;?>"></script>

<script type="text/javascript" src="<?php echo $base_url; ?>/js/trumbowyg/trumbowyg.min.js"></script>
<link rel="stylesheet" href="<?php echo $base_url; ?>/js/trumbowyg/ui/trumbowyg.css?v=<?=$fileVersion;?>">

<script type="text/javascript" src="<?php echo $base_url; ?>/js/bootstrap-tagsinput/dist/bootstrap-tagsinput.js"></script>
<link rel="stylesheet" href="<?php echo $base_url; ?>/js/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
</head>

<?php
    include "includes/case.php";
?>

</html>
