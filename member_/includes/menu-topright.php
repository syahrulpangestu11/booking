
<style>
    .menu-products #menu-topright .navbar-right .navbar-nav > li.button-red a {
    color: #fff;
    background: #288626 !important;
}
</style>
        <script>
        $(function(){

            $(document).ready(function(){
                
                
                $("#child-menu").remove();
                $("section.content > div").removeClass('row');
                
                <?php
                $s_menu ="SELECT mm.* FROM menumyhotel mm WHERE mm.link = '".$uri2."'";
                $q_menu = $db->query($s_menu);
                $n_menu = $q_menu->rowCount();
                $r_menu = $q_menu->fetch(PDO::FETCH_ASSOC);
                if($n_menu>0){
                    if(!empty($r_menu['parent'])){
                        $menuBreadcrumb = '<li>'.$r_menu['parent'].'</li>';
                        $menuBreadcrumb .= '<li>'.$r_menu['menu'].'</li>';
                    }else{
                        $menuBreadcrumb = '<li>'.$r_menu['menu'].'</li>';
                    }
                    $menuDescription = strip_tags($r_menu['description']);
                    $menuDescription = trim(preg_replace('/\s+/', ' ', $menuDescription));
                    ?>

                    var pageTitle = '';
                    var breadcrumb = '<?=$menuBreadcrumb;?>';
                    var pageDescription = '<?=$menuDescription;?>';
                    
                    pageTitle = '<div class="page-title box"><ul>';
                    pageTitle += breadcrumb;
                    pageTitle += '</ul>';
                    pageTitle += '<p>'+pageDescription+'</p>';
                    pageTitle += '</div>';
                    $("section.content").first().prepend(pageTitle);
                    <?php
                }
                ?>
            });
        });
        </script>
        <div class="navbar-right">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a class="dropdown-toggle" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-user"></i>
                        <span><?=$_SESSION['_member_initial']?> <i class="caret"></i></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header bg-light-blue">
                            <img src="<?php echo $_SESSION['_member_userpict']; ?>" class="img-circle" alt="User Image">
                            <p>
                                <?=$_SESSION['_member_initial']?>
                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="<?php echo"$base_url"; ?>/profile" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a href="<?php echo"$base_url"; ?>/logout" class="btn btn-default btn-flat">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    <!-- </nav> -->