<?php
    function getCountry($country)
    {
        if($country==''){
            echo '<option value="" selected>Choose Country</option>';
        }


        try {
            global $db;
            $stmt = $db->query("select * from country order by countryname asc");
            $r_country = $stmt->fetchAll(PDO::FETCH_ASSOC);
            foreach($r_country as $row){
                if($row['countryoid'] == $country){ $selected = "selected"; }else{ $selected=""; }
        ?>
            <option value="<?php echo $row['countryoid']; ?>" <?php echo $selected; ?>><?php echo $row['countryname']; ?></option>
        <?php
            }
        }catch(PDOException $ex) {
            echo "Invalid Query";
            die();
        }

    }
    
    function getState($state,$country)
    {
        if($state==''){
            echo '<option value="" selected>-</option>';
        }

        try {
            global $db;
            $stmt = $db->query("select * from state where countryoid='$country' order by statename asc");
            $r_state = $stmt->fetchAll(PDO::FETCH_ASSOC);
            foreach($r_state as $row){
                if($row['stateoid'] == $state){ $selected = "selected"; }else{ $selected=""; }
        ?>
            <option value="<?php echo $row['stateoid']; ?>" <?php echo $selected; ?>><?php echo $row['statename']; ?></option>
        <?php
            }
        }catch(PDOException $ex) {
            echo "Invalid Query";
            die();
        }

    }
    function getCity($city,$state)
    {
        if($city==''){
            echo '<option value="" selected>-</option>';
        }

        try {
            global $db;
            $stmt = $db->query("select * from city where stateoid='$state' order by cityname asc");
            $r_city = $stmt->fetchAll(PDO::FETCH_ASSOC);
            foreach($r_city as $row){
                if($row['cityoid'] == $city){ $selected = "selected"; }else{ $selected=""; }
        ?>
            <option value="<?php echo $row['cityoid']; ?>" <?php echo $selected; ?>><?php echo $row['cityname']; ?></option>
        <?php
            }
        }catch(PDOException $ex) {
            echo "Invalid Query";
            die();
        }

    }

?>