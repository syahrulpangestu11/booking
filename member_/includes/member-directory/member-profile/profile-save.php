<?php
	include("../../../../conf/connection.php");

	$memberoid = $_SESSION['_member_oid'];
	$firstname = $_POST['firstname'];
	$lastname = $_POST['lastname'];
	$address = $_POST['address'];
	$mobile = $_POST['mobile'];
	$phone = $_POST['phone'];
	$email = $_POST['email'];
	$city = $_POST['city'];

	$password = $_POST['password'];
	$oldpict = $_POST['oldpicture'];;
	
	$allowedTypes = array("image/jpg", "image/jpeg", "image/gif", "image/png");
	$allowedExts = array("jpg", "jpeg", "gif", "png");

	$prepare = "firstname=:a, address=:b, phone=:c, email=:e, cityoid=:g, lastname=:h, mobile=:i";
	$execute = array(':a' => $firstname, ':b' => $address, ':c' => $phone, ':e' => $email, ':g' => $city, 
					':h' => $lastname, ':i' => $mobile, ':aoid' => $memberoid);

	$notError = true;
	if((!empty($_FILES["picture"]["name"]) or $_FILES["picture"]["name"]!='')){
		$fileerror = $_FILES["picture"]["error"] ;
		$filetype = strtolower($_FILES["picture"]["type"]);
		$filename_ori = strtolower($_FILES["picture"]["name"]);
		$filename_array = explode(".", $filename_ori);
		$ext = ".".end($filename_array);
		$filename = str_replace($ext, "", $filename_ori);
		$uploadedname = "member_".$memberoid;//"default_".$filename."-".date("Y_m_d_H_i_s");
		$statusError = ($fileerror > 0 && $fileerror != 4 ? true:false);

		if($statusError  && !in_array(end($filename_array), $allowedExts)  && !in_array($filetype, $allowedTypes)){
			$notError = false;
			echo"<script>window.location.href = '".$base_url."/profile/?error=image'</script>";
			echo "0";
			die();
		}

		include("compress-image.php"); //function for upload and compress image
		$prepare .= ", picture=:k";
		$execute[':k']= ($filename!=""?str_replace('/member',"",$base_url)."/".$folder.$imagename_t:$oldpict);
	}

	if(!empty($password)){
		$prepare .= ", password=:j";
		$execute[':j']= sha1($password);
	}
	
	// echo"<script>console.log(".json_encode($execute).");</script>";

	if($notError){
		try {
			$stmt = $db->prepare("UPDATE member SET ".$prepare." WHERE memberoid=:aoid");
			$stmt->execute($execute);

			echo"<script>window.location.href = '".$base_url."/profile'</script>";
		}catch(PDOException $ex) {
			// echo"<script>console.log(".json_encode($ex).");</script>";
			echo"<script>window.location.href = '".$base_url."/profile/?error=saving'</script>";
			echo "0";
			die();
		}
	}
	echo "1";
?>