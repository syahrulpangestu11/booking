 <?php
	function randomString($length) {
		$characters = '0123456789';
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, strlen($characters) - 1)];
		}
		return $randomString;
	}
	
	function differenceDate($arrival, $departure){
		$date1 = new DateTime($arrival);
		$date2 = new DateTime($departure);
		$interval = $date1->diff($date2);
		return $interval->days;
	}
	
	function rateReffCode($currentrate, $discount){
		$discount = $currentrate * $discount / 100;
		$rate = $currentrate - $discount;
		return $rate;
	}
	
	function matchday($subject, $key){
		$pattern = '/'.$key.'/i';
		preg_match($pattern, $subject, $matches);
		if(count($matches) > 0){
			return true;
		}else{
			return false;
		}
	}
	function matchnight($subject, $key){
		$ar = explode(',', $subject);
		$c = count($ar);
		$res = false;
		for($i=0;$i<$c;$i++){
			if($key == $ar[$i]){
				$res = true;
				break;
			}
		}
		return $res;
	}
	
	function promocodediscount($applydiscount, $discounttype, $discount, $total){
		if($applydiscount == 'y' and $total > 0){
			if($discounttype == "discount percentage"){
				$total = $total - ($discount/100 * $total);
			}else if($discounttype == "discount amount"){
				$total = $total - $discount;
			}
		}
		
		return $total;
	}
?>