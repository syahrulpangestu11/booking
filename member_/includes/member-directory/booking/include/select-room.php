<?php
	session_start();
	error_reporting(E_ALL & ~E_NOTICE);
	include("../../../../conf/connection.php");
	include("function-ibeind.php");
	
	$prm	= explode(" ",$_POST['parameter']);
	$agenthoteloid = $_SESSION['_oid'];
	
	$startdate = $prm[0];
	$enddate = $prm[1];
	$night = differenceDate($startdate, $enddate);
	
	$hoteloid = $prm[2];
	$roomoid = $prm[3];
	$roomofferoid = $prm[4];

	$s_detail_room = "select ro.roomofferoid, ro.name, r.roomoid, r.name as masterroom, r.hoteloid, r.adult, r.child, h.hotelcode, h.hotelname from roomoffer ro inner join published p using (publishedoid) inner join room r using (roomoid) inner join hotel h using (hoteloid) inner join offertype using (offertypeoid) where ro.roomofferoid = '".$roomofferoid."' and ro.publishedoid = '1'";
	$stmt = $db->query($s_detail_room);
	$room = $stmt->fetch(PDO::FETCH_ASSOC);
		$roomname = $room['name'];
		$hoteloid = $room['hoteloid'];
		$hotelname = $room['hotelname'];
		$hcode = $room['hotelcode'];
		$maxadult = $room['adult'];
		$maxchild = $room['child'];
	
	$xml =  simplexml_load_file("../../../../data-xml/".$hcode.".xml");
	
	$promoname = "Best Flexible Rate";
	$promocode = "thebuking";
	$applycommission = "n";
	$breakfast = "n";
	$with_extrabed = "n";
	$flag = 0;
	$channeloid = 1;
	$currencyoid = 1;
		$s_currency = "select currencycode from currency where currencyoid = '".$currencyoid."'";
		$stmt	= $db->query($s_currency);
		$r_currency = $stmt->fetch(PDO::FETCH_ASSOC);
	$currency = $r_currency['currencycode'];
	
	
	$bookingDetail = array();
	$available = true;
	for($n=0; $n<$night; $n++){
		$allotment = 0;
		$rate = 0; $surcharge = 0;
		
		$date = date("Y-m-d",strtotime($startdate." +".$n." day"));
		$query_tag = '//hotel[@id="'.$hoteloid.'"]/masterroom[@id="'.$roomoid.'"]/rate[@date="'.$date.'"]/allotment/channel[@type="'.$channeloid.'"]/text()';
		foreach($xml->xpath($query_tag) as $tagallotment){
			$allotment = (int)$tagallotment[0];
		}
		
		if($allotment > 0){
			$syntax_bar = "select rate, surcharge from agenthotelrate where agenthoteloid = '".$agenthoteloid."' and roomofferoid = '".$room['roomofferoid']."' and '".$date."' >= startdate and '".$date."' <= enddate order by priority limit 1";
			$stmt = $db->query($syntax_bar);
			$row_bar = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			
			foreach($row_bar as $row){
				$rate = $row['rate'];
				$surcharge = $row['surcharge'];
			}
			if($rate > 0){
				$totalrate = $rate + $surcharge;
				$totalperroom = $totalperroom + $totalrate;

				$bookingDetail[$n]['date'] = $date;
				$bookingDetail[$n]['rate'] = $rate;
				$bookingDetail[$n]['currencyoid'] = $currencyoid;
				$bookingDetail[$n]['totalpernight'] = $totalrate;
				$bookingDetail[$n]['disc_note'] = "";
			}else{
				$available = false;
				$break;
			}
		}else{
			$available = false;
			break;
		}
	}
	
	if($available == true){
		$totalbalance = 0;
		$totaldeposit = $totalperroom;
		
		$stmt = $db->prepare("INSERT INTO `bookingtemp` (`submittime`, `session_id`, `roomofferoid`, `channeloid`, `promotionoid`, `checkin`, `checkout`, `night`, `adult`, `child`, `flag`, `breakfast`, `extrabed`, `roomtotal`, `total`, `deposit`, `balance`, `currencyoid`, `hotel`, `room`, `promotion`, `promocode`, `pc_commission`)  VALUES (:a , :b , :c , :d , :e , :f, :g, :h, :i, :j, :k, :l, :m, :n, :o, :p, :q, :r, :s, :t, :u, :v, :w)");
		$stmt->execute(array(':a' => date('Y-m-d H:i:s'), ':b' => $_SESSION['tokenSession'], ':c' => $roomofferoid, ':d' => $channeloid, ':e' => 0, ':f' => $startdate, ':g' => $enddate, ':h' => $night, ':i' => 0, ':j' => 0, ':k' => $flag, ':l' => $breakfast, ':m' => $with_extrabed, ':n' => $totalperroom, ':o' => $totalperroom, ':p' => $totaldeposit, ':q' => $totalbalance, ':r' => $currencyoid, ':s' => $hotelname, ':t' => $roomname, ':u' => $promoname, ':v' => $promocode, ':w' => $applycommission));
		$tempoid = $db->lastInsertId();
		foreach($bookingDetail as $j => $value2){
			$stmt = $db->prepare("INSERT INTO `bookingtempdtl` (`bookingtempoid`, `date`, `rate`, `discount`, `rateafter`, `extrabed`, `total`, `currencyoid`)  VALUES (:a , :b , :c , :d , :e , :f, :g, :h)");
			$stmt->execute(array(':a' => $tempoid, ':b' => $value2['date'], ':c' => $value2['rate'], ':d' => $value2['disc_note'], ':e' => $value2['totalpernight'], ':f' => $with_extrabed, ':g' => $value2['totalpernight'], ':h' => $value2['currencyoid']));
		}
?>
    <li numbering="<?=$sorts;?>">
    	<div class="row">
        	<div class="col-xs-6">
                <input type="hidden" name="bttmpfrbkngibnd[]" value="<?=$tempoid;?>" />
                <b>Room <?=$sorts;?></b>
            </div>
            <div class="col-xs-6" class="text-right"><a class="remove" prm="<?=$tempoid?>"><span><i class="fa fa-trash"></i> Remove Room</span></a></div>
        </div>
        <div class="row">
        	<div class="col-md-12">
                <b><?=$roomname?></b><br /><?=$promoname?>
            </div>
        </div>
    	<div class="row">
        	<div class="col-xs-6"><b>Room Occupancy</b></div>
        	<div class="col-xs-6">
            	<div class="row">
                	<div class="col-xs-6">Adult</div>
                	<div class="col-xs-6">Child</div>
                </div>
            	<div class="row">
                	<div class="col-xs-6">
                        <select name="adult[]" class="form-control" style="width:auto;">
                            <?php for($a = 1; $a <= $maxadult; $a++){ ?>
                            <option value="<?=$a?>"><?=$a?></option>
                            <?php } ?>
                        </select>
                    </div>
                	<div class="col-xs-6">
                        <select name="child[]" class="form-control" style="width:auto;">
                            <?php for($a = 0; $a <= $maxchild; $a++){ ?>
                            <option value="<?=$a?>"><?=$a?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    	<div class="row">
        	<div class="col-xs-6"><b>Check - in</b></div>
            <div class="col-xs-6"><?=date('d F Y', strtotime($startdate));?></div>
        </div>
    	<div class="row">
        	<div class="col-xs-6"><b>Check - out</b></div>
            <div class="col-xs-6"><?=date('d F Y', strtotime($enddate));?></div>
        </div>
    	<div class="row">
        	<div class="col-xs-6"><b>Room Total</b></div>
            <div class="col-xs-6"><?=$currency?> <?=number_format($totalperroom)?></div>
        </div>
    </li>
<?php	
	}
?>