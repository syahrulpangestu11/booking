<?php
	session_start();
	error_reporting(E_ALL & ~E_NOTICE);
	include("../../../../conf/connection.php");
		
	$session_transaction = "select sum(total) as total, sum(deposit) as deposit, sum(balance) as balance, format(sum(total),0) as formattotal, format(sum(deposit),0) as formatdeposit, format(sum(balance),0) as formatbalance, c.currencycode from bookingtemp left join currency c using (currencyoid) where session_id = '".$_SESSION['tokenSession']."'";
	$stmt	= $db->query($session_transaction);
	$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	foreach($result as $row){
		if(empty($row['currencycode'])){ $row['currencycode']=$main_currency; }
		echo"
		<xml>
			<total>".$row['currencycode']." ".$row['formattotal']."</total>
			<deposit>".$row['currencycode']." ".$row['formatdeposit']."</deposit>
			<balance>".$row['currencycode']." ".$row['formatbalance']."</balance>
			<amounttotal>".$row['total']."</amounttotal>
			<amountdeposit>".$row['deposit']."</amountdeposit>
			<amountbalance>".$row['balance']."</amountbalance>
		</xml>
		";
	}
?>