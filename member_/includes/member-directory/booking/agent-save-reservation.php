<?php
	$secret		= '6LdhNw4TAAAAAN438wwI9B5pYbSt4rirP3co0-Kf';
	$captcha	= $_POST['g-recaptcha-response'];
	$response	= file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret."&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']);
	$json 		= json_decode($response, true);
	$verification	= $json['success'];
	
	$verification = true;

if($verification == false){
	?>
	<script type="text/javascript">
    $(function(){
        $("#dialog").html('A valid chaptcha is required! We direct you to check guest and payment page');
		$("#dialog").dialog({
			autoOpen: true,
			title: 'Notification',
			buttons: {
				"Ok": function(){
					$(this).dialog( "close" );
					<?php
						if(count($_POST) == 0 or empty($_POST['g-recaptcha-response'])){
							$url = 'index.php?hcode='.$hotelcode;
						}else{
							$url = 'index.php?page=payment&hcode='.$hotelcode;
						}
					?>
					document.location.href = '<?=$url?>';
				}
			}
		});
    });
    </script>
    <?php
}else{
	try{
		unset($_SESSION['captcha_id']);

		include ("include/save-booking.php");

		ob_start();
		include '../../../../mail/email-confirm-reservation.php';
		$body = ob_get_clean();
		$subject = "Booking Confirmation for ".$hotelname." ".$bookingnumber;

		require_once('function/mailer/class.phpmailer.php');
		$mail = new PHPMailer(true);

		$mail->IsSMTP(); // telling the class to use SMTP

        try{

        $mail->SMTPDebug = 0;
        //Ask for HTML-friendly debug output
        $mail->Debugoutput = 'html';
        //Set the hostname of the mail server
        $mail->Host = "smtp.gmail.com";
        //Set the SMTP port number - likely to be 25, 465 or 587
        $mail->SMTPSecure = 'tls';
        $mail->Port = 587;
        //Whether to use SMTP authentication
        $mail->SMTPAuth = true;
        //Username to use for SMTP authentication
        $mail->Username = "booking@thebuking.com";//"info@thebuking.com";
        //Password to use for SMTP authentication
        $mail->Password = "Res3!4nd";//"KofiEn4k";

			$syntax_mail_address = "SELECT * FROM mailer INNER JOIN mailertype USING (mailertypeoid) INNER JOIN mailermail USING (mailermailoid) WHERE mailerpage = 'booking-guest' AND status = '1'";
			$stmt	= $db->query($syntax_mail_address);
			$result_email = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($result_email as $email_list){
				switch($email_list['mailertypeoid']){
					case 1 : $mail->SetFrom($email_list['email'], $email_list['name']); break;
					case 2 : $mail->AddAddress($email_list['email'], $email_list['name']); break;
					case 3 : $mail->AddCC($email_list['email'], $email_list['name']); break;
					case 4 : $mail->AddBCC($email_list['email'], $email_list['name']); break;
					case 5 : $mail->AddReplyTo($email_list['email'], $email_list['name']); break;
				}
			}
			
			$mail->AddAddress($guest_email, $guest_name);
			$mail->AddCC($agent_email, $agent_name);

			// --- Hotel Contact ---
			$s_hc = "SELECT * FROM hotelcontact hc WHERE hc.hoteloid = '".$hoteloid."' AND hc.publishedoid = '1'";
			$q_hc	= $db->query($s_hc);
			$r_hc = $q_hc->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_hc as $email_list){
				switch($email_list['mailertypeoid']){
					case 1 : $mail->SetFrom($email_list['email'], $email_list['name']); break;
					case 2 : $mail->AddAddress($email_list['email'], $email_list['name']); break;
					case 3 : $mail->AddCC($email_list['email'], $email_list['name']); break;
					case 4 : $mail->AddBCC($email_list['email'], $email_list['name']); break;
					case 5 : $mail->AddReplyTo($email_list['email'], $email_list['name']); break;
				}
			}

			$mail->Subject = $subject;
			$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
			$mail->MsgHTML($body);

			//$mail->Send();

			unset($_SESSION);
			echo'<script>window.location.href="'.$base_url.'/booking/confirmation/?bookingnumber='.$bookingnumber.'"</script>';
		} catch (phpmailerException $e) {
			echo"<b>Php Mailer Error :</b><br>"; echo $e->errorMessage();
		} catch (Exception $e) {
			echo"<b>All Error :</b><br>"; echo $e->getMessage();
		}

	}catch(PDOException $ex) {
		echo"<b>All Error :</b><br>"; echo $ex;
	}
}
?>
