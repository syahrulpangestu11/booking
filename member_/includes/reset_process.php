<?php
$user = $_POST['user'];
$email = $_POST['email'];

function randomString($length) {
    $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz_-';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $randomString;
}

if(!empty($user) and !empty($email)){
	try {
		$stmt = $db->prepare("SELECT * FROM users inner join userstype using (userstypeoid) WHERE username = :usr AND email = :mail AND status = :status");
		$stmt->execute(array(':usr' => $user, ':mail' => $email, ':status' => 1));
		$row_count = $stmt->rowCount();
			
		if($row_count == 0) {
			echo"<script>document.location.href='".$base_url."/resetfailed';</script>";
		}else{
			$key = randomString(64);
			
			$r_users = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_users as $users){
				$useroid = $users['useroid'];
			}
			
			$stmt = $db->prepare("INSERT INTO `userpwdreset`(`useroid`, `keywords`, `status`, `requestdate`) VALUES (:a,:b,:c,:d)");
			$stmt->execute(array(':a' => $useroid, ':b' => $key, ':c' => '1', ':d' => date('Y-m-d H:i:s')));
			
			$sender_name = "The Buking Support Account";
			$sender_email = "support@thebuking.com";
			
			$recipient_name = $user;
			$recipient_email = $email;
			
			include('mailer/sending_mail_reset_password.php');
		}
	}catch(PDOException $ex) {
		echo "Invalid Query".$ex;
		die();
	}
}else{
	echo"<script>document.location.href='".$base_url."/resetfailed';</script>";
}
?>