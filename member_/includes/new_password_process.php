<?php
$for = $_POST['for'];
$key = $_POST['key'];
$newpwd = $_POST['newpwd'];
$repeatpwd = $_POST['repeatpwd'];

if(!empty($for) and !empty($key) and !empty($newpwd) and !empty($repeatpwd)){
	try {
		$stmt = $db->prepare("SELECT * FROM userpwdreset inner join users using (useroid) WHERE MD5(CONCAT(username,userpwdreset.useroid)) = :usr AND keywords = :key AND userpwdreset.status = :status");
		$stmt->execute(array(':usr' => $for, ':key' => $key, ':status' => 1));
		$row_count = $stmt->rowCount();
			
		if($row_count == 0) {
			echo"<script>document.location.href='".$base_url."/newpwdfailed';</script>";
		}else{
			$r_reset = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_reset as $reset){
				$resetoid = $reset['resetoid'];
			}
			
			$stmt = $db->prepare("UPDATE `users` SET `password` = SHA1(:b) WHERE MD5(CONCAT(username,useroid)) = :a");
			$stmt->execute(array(':a' => $for, ':b' => $newpwd));
			
			$stmt = $db->prepare("UPDATE `userpwdreset` SET `status` = :b, `resetdate` = :c WHERE `resetoid` = :a");
			$stmt->execute(array(':a' => $resetoid, ':b' => '0', ':c' => date('Y-m-d H:i:s')));
			
			echo"<script>document.location.href='".$base_url."/newpwdsuccess';</script>";
		}
	}catch(PDOException $ex) {
		echo "Invalid Query".$ex;
		die();
	}
}else{
	echo"<script>document.location.href='".$base_url."/newpwdfailed';</script>";
}
?>