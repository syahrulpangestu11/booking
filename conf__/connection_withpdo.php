<?php
	$db_hostname='localhost';
	$charset='utf8';
	
    $db_username="demoaccess"; 	
    $db_password="F0ot3!B4LLn"; 
    
    // $db_username="root"; 	
    // $db_password=""; 
    // $dbname="o2o_2";
    $dbname="demo_o2o";

	// try {
	// 	$db = new PDO('mysql:host='.$db_hostname.';dbname='.$dbname.';charset=utf8', $db_username, $db_password);
	// 	$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	// 	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	// 	$db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
		

	// }catch(PDOException $e) {
	// 	echo "Invalid Connection";
	// 	die();

	// }

	class DbConnection extends PDO
	{
	
		public function __construct($dsn, $username = NULL, $password = NULL, $options = array())
		{
			$default_options = array(
				PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
				PDO::ATTR_EMULATE_PREPARES => false,
				PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
			);
			$options = $options + $default_options;
			parent::__construct($dsn, $username, $password, $options);
		}

		public function run($sql, $args = NULL)
		{
			$query = $sql;
			if(is_array($args))
			if (count($args)>0){
				foreach($args as $key => $value){ 
					if(is_int($key)){
						$query = preg_replace('/[?]/', "'".$value."'", $query, 1);
					}else{
						$query = str_replace($key, "'".$value."'", $query); 
					}
				}
			}
			// Func::logJS($query);
			// Func::logJS($sql);
			// Func::logJS($args);

			if (!$args)
			{
				return $this->query($sql);
			}
			$stmt = $this->prepare($sql);
			$stmt->execute($args);
			return $stmt;
		}
		
        /*
            // Getting single column from the single row
            $id = $db->run("SELECT id FROM users WHERE email=?", [$email])->fetchColumn();

            // Getting single row
            $user = $db->run("SELECT * FROM users WHERE email=?", [$email])->fetch();

            // Getting array of rows
            $users = $db->run("SELECT * FROM users LIMIT ?,?", [$offset, $limit])->fetchAll();

			// Getting array of rows
			$all = $db->run("SELECT name, id FROM pdowrapper")->fetchAll(PDO::FETCH_KEY_PAIR);

            // Getting the number of affected rows
            $updated = $db->run("UPDATE users SET balance = ? WHERE id =? ", [$balance, $id])->rowCount();
         */
	}

	$db = new DbConnection("mysql:host=$db_hostname;dbname=$dbname;charset=$charset", $db_username, $db_password);

?>
