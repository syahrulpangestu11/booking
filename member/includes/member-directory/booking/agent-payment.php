<section class="content-header">
    <h1>Payment</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-user-o"></i> Reservation</a></li>
    </ol>
</section>
<section class="content">
	<form class="form-horizontal" method="post" action="<?=$base_url?>/booking/process">
    <input type="hidden" name="hoteloid" value="<?=$_POST['hoteloid']?>" />
    <div class="box">
    	<div class="row">
        	<div class="col-sm-7">
				<h4><i class="fa fa-user-o"></i> Guest Detail</h4>
                <div class="form-group">
                	<label class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-10"><input type="email" name="email" required="required" class="form-control" placeholder="E-mail address example@example.com"></div>
                </div>
                <div class="form-group">
					<label class="col-sm-2 control-label">Title</label>
                    <div class="col-sm-10">
                        <?php
                            $title = array('Mr.', 'Mrs.', 'Miss');
                            foreach($title as $key => $value){
                               if($key == 0){ $checked = 'checked="checked"'; }
                        ?>
                        <input type="radio" name="title" value="<?=$value?>" <?=$checked?>> <?=$value?> &nbsp;&nbsp;
                        <?php
                            }
                        ?>
                    </div>
                </div>
                <div class="form-group">
					<label class="col-sm-2 control-label">First Name</label>
                    <div class="col-sm-10"><input type="text" name="firstname" class="form-control" placeholder="First name"></div>
                </div>
                <div class="form-group">
					<label class="col-sm-2 control-label">Last Name</label>
                    <div class="col-sm-10"><input type="text" name="lastname" class="form-control" placeholder="Last name"></div>
                </div>
                <div class="form-group">
					<label class="col-sm-2 control-label">Phone</label>
                    <div class="col-sm-10"><input type="text" name="phone" required="required" class="form-control" placeholder="+62 361 123456"></div>
                </div>
                <div class="form-group">
					<label class="col-sm-2 control-label">City</label>
                    <div class="col-sm-10"><input type="text" name="city" required="required" class="form-control" placeholder="City"></div>
                </div>
                <div class="form-group">
					<label class="col-sm-2 control-label">Country</label>
                    <div class="col-sm-10">
                        <select name="country" class="form-control" required>
                            <option selected="selected">Select Your Country</option>
                        <?php
                        $syntax_country = "select countryoid, countryname from country";
                        $stmt	= $db->query($syntax_country);
                        $result_country = $stmt->fetchAll(PDO::FETCH_ASSOC);
                        foreach($result_country as $country){
                            $selected = '';
							if(strtolower($country['countryname']) == "indonesia"){
								$selected = 'selected';
							}
                        ?>
                            <option value="<?=$country['countryoid']?>" <?=$selected?>><?=$country['countryname']?></option> 
                        <?php
                        }
                        ?>
                        </select>
                    </div>
                </div>
                
				<h4><i class="fa fa-credit-card"></i> Payment</h4>
                <div class="row">
                    <div class="col-md-12">
                    	<?php
							$btempoid = "'".implode("','", $_REQUEST['bttmpfrbkngibnd'])."'";
							$s_total = "select sum(total) as grandtotal, c.currencycode from bookingtemp bt inner join currency c using (currencyoid) where (session_id = '".$_SESSION['tokenSession']."' and session_id <> '') and bt.bookingtempoid in (".$btempoid.")";
							$stmt	= $db->query($s_total);
							$ttl	= $stmt->fetch(PDO::FETCH_ASSOC);
							
							$s_balance = "select h.hotelname, ah.creditfacility, ah.agenthoteloid, h.hoteloid from hotel h left join agenthotel ah using (hoteloid) where h.hoteloid = '".$_POST['hoteloid']."' and ah.agentoid = '".$_SESSION['_oid']."'";
							$stmt	= $db->query($s_balance);
							$balance = $stmt->fetch(PDO::FETCH_ASSOC);
							
							if($balance['creditfacility'] >= $ttl['grandtotal']){
								$cf_data_toogle = 'data-toggle="tab"';
								$cf_role_note = '';
								$cf_active = 'active';
								$cc_active = '';
								?>
                                <script type="text/javascript">
								$( document ).ready(function() {
									$('div#credit-card input, div#credit-card select, div#credit-card textarea').prop('disabled', true);
								});
								</script>
                                <?php
							}else{
								$cf_data_toogle = '';
								$cf_role_note = '<small>(not enough balance)</small>';
								$cf_active = '';
								$cc_active = 'active';
								$cc_pay_disabled = '';
							}
						?>
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="<?=$cf_active?>"><a href="#credit-facility" aria-controls="credit-facility" role="tab" <?=$cf_data_toogle?>>Credit Facility <?=$cf_role_note?></a></li>
                            <li role="presentation" class="<?=$cc_active?>"><a href="#credit-card" aria-controls="credit-card" role="tab" data-toggle="tab">Credit Card</a></li>
                        </ul>
                        <div id="payment-option" class="tab-content">
                            <div role="tabpanel" class="tab-pane <?=$cf_active?>" id="credit-facility">
                                <input type="hidden" name="payment_type" value="creditfacility" />
                                <div class="form-group">
                                    <label class="col-sm-12">Your Current Credit Facility Balance at <?=$balance['hotelname']?></label>
                                    <div class="col-sm-6"><input type="text" class="form-control" readonly value="<?=number_format($balance['creditfacility'])?>"></div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane <?=$cc_active?>" id="credit-card">
                            	<input type="hidden" name="payment_type" value="cc" />
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Type</label>
                                    <div class="col-sm-6">
                                        <select name="cardtype" class="form-control" required>
                                        <option selected="selected">Select Your Card</option>
                                        <?php
                                        $syntax_card = "select * from creditcard";
                                        $stmt	= $db->query($syntax_card);
                                        $result_card = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                        foreach($result_card as $card){
                                        ?>
                                            <option value="<?=$card['cardcode']?>"><?=$card['cardname']?></option>
                                        <?php
                                        }
                                        ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Credit Card Number</label>
                                    <div class="col-sm-6"><input type="text" name="cardnumber" class="form-control" placeholder="Credit card number" required="required"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Card Holder</label>
                                    <div class="col-sm-6"><input type="text" name="cardholder" class="form-control" placeholder="Name on card" required="required"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Expired of Card</label>
                                    <div class="col-sm-4">
                                        <select name="expmonth" class="form-control" required>
                                        <option selected="selected">Exipiry Month</option>
                                        <?php
                                            $monthNumber	= array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');
                                            $monthName		= array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
                                            foreach($monthNumber as $key => $value){
												if($monthName[$key] == date('F')){ $selected = 'selected'; }
                                        ?>
                                            <option value="<?=$monthNumber[$key]?>" <?=$selected?>><?=$monthName[$key]?></option> 
                                        <?php
                                            }
                                        ?>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <select name="expyear" class="form-control" required>
                                        <option selected="selected">Exipiry Year</option>
                                        <?php
                                            for($y = date('Y'); $y <= date('Y')+10; $y++){
                                                if($y == date('Y')){ $selected = 'selected'; }
                                        ?>
                                            <option value="<?=$y?>" <?=$selected?>><?=$y?></option> 
                                        <?php
                                            }
                                        ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">CVC</label>
                                    <div class="col-sm-3"><input type="text" name="csc" class="form-control"  placeholder="Security code"></div>
                                    <div class="col-sm-5">The 3 or 4 digit number in the back of your credit card</div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12 text-right"><img src="https://www.thebuking.com/v2/image/comodo_secure_seal_100x85_transp.png"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div id="captchaimage">
                            <script src="https://www.google.com/recaptcha/api.js" async defer></script>
                            <div class="g-recaptcha" data-sitekey="6LdhNw4TAAAAAL0aVrGG5VnnP2UyQ4eSEDbFYVx3"></div>
                            <input type="hidden" class="hiddenRecaptcha" name="hiddenRecaptcha" id="hiddenRecaptcha">
                        </div>
                        <style>
                            #captchaimage{ margin:10px 0 5px; text-align:right; }
                            div.g-recaptcha { display:inline-block; }
                            a.blue{ color:#00F; }
							button[type=submit]{ font-size:1.1em; padding:10px 25px; }
                        </style>
                    </div>
                </div>
                <div class="form-group" style="text-align:right;">
                    <div class="col-sm-12">
                        <button type="submit" name="submit" class="btn btn-success btn-lg">Submit Reservation</button>
                    </div>
                </div>
            </div>
            <div class="col-sm-5">
                <div id="box-summary-reservation">
                    <div class="row" id="title"><div class="col-sm-12"><h4>Reservation Summary</h4></div></div>
                    <div class="row">
                    	<div class="col-sm-12">
                    		<ul class="block" id="breakdown-total">
                            <?php
							$totalroom = 0; $numberroom = 0;
							$query_sumary = "select bt.*, c.currencycode  
							from bookingtemp bt
							inner join roomoffer ro using (roomofferoid)
							inner join offertype ot using (offertypeoid) 
							inner join channel ch using (channeloid)
							inner join currency c using (currencyoid)
							left join promotion p using (promotionoid)
							inner join room r using (roomoid) 
							where (session_id = '".$_SESSION['tokenSession']."' and session_id <> '') and bt.bookingtempoid in (".$btempoid.")";
							$stmt	= $db->query($query_sumary);
							$result_summary = $stmt->fetchAll(PDO::FETCH_ASSOC);
							foreach($result_summary as $key => $summary){
								$totalroom += $summary['total'];
								$numberroom +=1;
							?>
                            <li>
                                <div class="row"><div class="col-sm-12"><input type="hidden" name="bttmpfrbkngibnd[]" value="<?=$tempoid;?>" /><b>Room # <?=$numberroom?></b></div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <b><?=$summary['room']?></b><br /><?=$summary['promotion']?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6"><b>Room Occupancy</b></div>
                                    <div class="col-xs-6">
                                        <div class="row">
                                            <div class="col-xs-6">Adult : <?=$_REQUEST['adult'][$key]?></div>
                                            <div class="col-xs-6">Child : <?=$_REQUEST['child'][$key]?></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6"><b>Check - in</b></div><div class="col-xs-6"><?=date('d F Y', strtotime($summary['checkin']));?></div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6"><b>Check - out</b></div><div class="col-xs-6"><?=date('d F Y', strtotime($summary['checkout']));?></div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6"><b>Room Total</b></div><div class="col-xs-6"><?=$summary['currencycode']?> <?=number_format($summary['total'])?></div>
                                </div>
                            </li>
                            <?php
								$s_update_temp	= "update bookingtemp set adult = '".$_REQUEST['adult'][$key]."', child = '".$_REQUEST['child'][$key ]."' where bookingtempoid = '".$summary['bookingtempoid']."'";
								$stmt_update	= $db->query($s_update_temp);
							}
							?>
                            </ul>
						</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-xs-6"><h5>Grand Total</h5></div>
                                <div class="col-xs-6"><h5 id="grandtotal"><?=$summary['currencycode']?> <?=number_format($totalroom)?></h5></div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6"><h5>Number of Room</h5></div>
                                <div class="col-xs-6"><h5 id="numberroom"><?=$numberroom?></h5></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</section>
<script type="text/javascript">
	$('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
		var last_selected = $(e.relatedTarget).attr("href");
			$(last_selected+' input, '+last_selected+' select, '+last_selected+' textarea').prop('disabled', true);
		var current_selected = $(e.target).attr("href");
			$(current_selected+' input, '+current_selected+' select, '+current_selected+' textarea').prop('disabled', false);
  	})	
</script>