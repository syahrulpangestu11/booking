<!-- <button type="button" class="blue-button add-button">Create New FAQ</button><br /><br /> -->
<table class="table">
	<thead>
		<tr>
			<th>Booking Number</th>
			<th>Status</th>
			<th>Hotel</th>
			<th>Guest</th>
			<th>Book Date</th>
			<th>Check In</th>
			<th>Check Out</th>
			<th>Grand Total</th>
			<th>Manage</th>
			<th>&nbsp</th>
		</tr>
	</thead>
	<tbody>
<?php
try{
	error_reporting(E_ALL & ~E_NOTICE);
	include("../../../conf/connection.php");
    include("../../ajax-include-file.php");
	include("../../paging/pre-paging.php");

    $startdate = date("Y-m-d", strtotime($_REQUEST['startdate']));
    $enddate = date("Y-m-d", strtotime($_REQUEST['enddate']));
    $status = $_REQUEST['status'];
    $viewby = $_REQUEST['viewby'];

    $main_query = "SELECT booking.*, hotel.hotelname,
                        CONCAT(title.name,' ',customer.firstname,' ',customer.lastname) as guestname,
                        bookingstatus.note as bookingstatus, currency.currencycode
                    from booking 
                    inner join currency using (currencyoid)
                    INNER JOIN hotel using(hoteloid) 
                    INNER JOIN customer using(custoid) 
                    INNER JOIN title on customer.title=title.titleoid  
                    INNER JOIN bookingstatus using(bookingstatusoid) 
                    left join bookingpayment using (bookingoid)
                    left join bookingroom using (bookingoid) ";
		$additional_join = "";
        $filter = array();
		if(!empty($_REQUEST['agentoid'])){
            array_push($filter,"agentoid='".$_REQUEST['agentoid']."'");
        }
		if(!empty($_REQUEST['q'])){
			array_push($filter, "bookingnumber like '%".$_REQUEST['q']."%'");
		}
		if(!empty($_REQUEST['viewby'])){
			array_push($filter, "(DATE(".$viewby.") >= '".$startdate."' and DATE(".$viewby.") <= '".$enddate."')");
		}
		if(!empty($_REQUEST['status'])){
			if($_REQUEST['status']!='all') array_push($filter, "bookingstatusoid = '".$status."'");
		}
		if(!empty($_REQUEST['hotelname'])){
			// $additional_join = " left join userassign ua on ua.useroid = u.useroid and ua.type = 'hoteloid' left join hotel h on h.hoteloid = ua.oid ";
			// array_push($filter, "h.hotelname like '%".$_REQUEST['hotelname']."%'");
		}
		if(count($filter) > 0){
			$filter_query = ' where '.implode(" and ", $filter);
		}else{
			$filter_query = "";
        }
    $query = $main_query.$additional_join.$filter_query.' group by bookingoid order by bookingtime desc '.$paging_query;
    // echo "<script>console.log(".json_encode($query).");</script>";
	$stmt = $db->query($query);
	$result_rsv = $stmt->fetchAll(PDO::FETCH_ASSOC);
	foreach($result_rsv as $row){
    // echo "<script>console.log(".json_encode($row).");</script>";
				$bookdate = date("d F Y", strtotime($row['bookingtime']));

				unset($checkin); unset($checkout);
				$checkin = array(); $checkout = array();

				try {
					$stmt = $db->query("select checkin, checkout from bookingroom where bookingoid = '".$row['bookingoid']."' group by checkin, checkout");
					$r_cico = $stmt->fetchAll(PDO::FETCH_ASSOC);
					foreach($r_cico as $arr){
						array_push($checkin , date("d F Y", strtotime($arr['checkin'])));
						array_push($checkout , date("d F Y", strtotime($arr['checkout'])));
					}
				}catch(PDOException $ex) {
					echo "Invalid Query";
					print($ex);
					die();
				}
?>
	<tr>
		<td><b><?=$row['bookingnumber']?></b></td>
		<td style="text-align:left"><?=$row['bookingstatus']?></td>
		<td><?=$row['hotelname']?></td>
		<td><?=$row['guestname']?></td>
        <td style="text-align:left"><?php echo $bookdate; ?></td>
        <td style="text-align:left"><?php foreach($checkin as $value){ echo $value."<br>"; } ?></td>
        <td style="text-align:left"><?php foreach($checkout as $value){ echo $value."<br>"; } ?></td>
        <td style="text-align:right"><?php echo $row['currencycode']." ".number_format($row['grandtotalr']); ?></td>
		<td>
			<button type='button' class='btn btn-primary open-button' uo='<?=$row['bookingnumber']?>'>Open</button>
			<!-- <button type='button' class='btn btn-warning confirm-button' uo='<?=$row['bookingnumber']?>'>Confirmation</button> -->
			<!-- <button type='button' class='btn btn-danger delete-button' uo='<?=$row['bookingnumber']?>'>Delete</button> -->
		</td>
	</tr>
<?php
	}
}catch(PDOException $ex) {
	print($ex);
	die();
}
?>
	</tbody>
</table>
<?php
	$main_count_query = "select count(bookingoid) as jml FROM booking ";
	$stmt = $db->query($main_count_query.$additional_join.$filter_query);
	$jmldata = $stmt->fetchColumn();

	$tampildata = $row_count;
	include("../../paging/post-paging.php");
?>
