<script type="text/javascript">
$(function(){

	$("#dialog-error, #dialog-success").dialog({
      autoOpen: false,
	  buttons: { "Ok": redirect },
	  close: function(event, ui) { redirect }
   });
   
   function redirect(){
	   $(location).attr("href", "<?php echo $base_url; ?>/profile");
   }
   

	/************************************************************************************/
	$('body').on('change','select[name=country]', function(e) {
		$.get("<?php echo $base_url; ?>/includes/member-directory/member-profile/function-ajax.php", {
			func: "stateRqst",
			country: $('select[name=country]').val(),
			state: $('input[type=hidden][name=dfltstate]').val()
      	}, function(response){
			$('.loc-state').html(unescape(response));
			$('select[name=state]').change();
      	});
	});

	$('body').on('change','select[name=state]', function(e) {
		$.get("<?php echo $base_url; ?>/includes/member-directory/member-profile/function-ajax.php", {
			func: "cityRqst",
			state: $('select[name=state]').val(),
			city: $('input[type=hidden][name=dfltcity]').val()
      	}, function(response){
			$('.loc-city').html(unescape(response));
      	});
	});


	//--------- validation and preview image
	$(".input-image").change(function() { readURL(this); });

});

function readURL(input) {
	var $sts = true;
	var $box = $('#preview_' + $(input).attr('name'));
	$box.empty();

	//--- validation image extension
	var $ext = input.files[0].name.split('.').pop().toLowerCase();
	if($.inArray($ext, ['gif','png','jpg','jpeg']) == -1) {
		$sts=false;
		$(input).val('');
		$box.append('<label>Uploaded file is not a valid image. Only JPG, PNG and GIF files are allowed.</label>');
		$box.find('label').css('color','red');
	}else{
		$box.find('label').css('color','black');
	}

	//--- validation image size
	if($sts){
		var size = input.files[0].size/1024/1024;
		var n = size.toFixed(2);
		if(size>1){
			$sts = false;
			$(input).val('');
			$box.append('<label>Your file size is: ' + n + 'MB, and it is too large to upload! Maximum : 1MB or less.</label>');
			$box.find('label').css('color','red');
		}else{
			$box.find('label').css('color','black');
		}
	}

	//--- preview image
	if($sts){
	  if (input.files && input.files[0]) {
	    var reader = new FileReader();
	    reader.onload = function(e) { $box.append('<img src="'+e.target.result+'" />') }
	    reader.readAsDataURL(input.files[0]);
	  }
	}
}


function openDialog($type,$message,$warning){
		var $dialog = $("#dialog-"+$type);
		$dialog.find(".description").empty();
		if($message!=undefined){ $dialog.find(".description").text($message); }
		if($warning!=undefined){ console.log($warning); }

		$(function(){ $( document ).ready(function(){ $dialog.dialog("open"); }); });
	}
</script>