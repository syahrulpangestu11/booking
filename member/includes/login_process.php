<?php
$username = isset($_POST['user']) ? $_POST['user'] : $_COOKIE['_member_u'];
$password = isset($_POST['pass']) ? $_POST['pass'] : $_COOKIE['_member_p'];
$remember = isset($_POST['remember']) ? $_POST['remember'] : '';

setcookie("_member_u",$username);
setcookie("_member_p",$password);
$_SESSION['_member_remember'] = $remember;

try {

	$stmt = $db->prepare("SELECT * FROM member WHERE email = :usr AND password = :pwd AND publishedoid = '1'");
	$stmt->execute(array(':usr' => $username, ':pwd' => sha1($password)));
	$foundmember = $stmt->rowCount();

	if($foundmember > 0){
		$member = $stmt->fetch(PDO::FETCH_ASSOC);

		$_SESSION['_member_initial'] = $member['firstname'].' '.$member['lastname'];
		$_SESSION['_member_oid'] = $member['memberoid'];

		if(empty($member['picture'])){
			$_SESSION['_member_userpict'] = $base_url."/images/avatar5.png";
		}else{
			$_SESSION['_member_userpict'] = $member['picture'];
		}

		// echo"<script>window.location.href = '".$base_url."/dashboard'</script>";
		echo"<script>window.location.href = '".str_replace('/member', '', $base_url)."/'</script>";

	// USER DOESN'T EXIST
	}else{
		echo"<script>window.location.href = '".$base_url."/loginerror'</script>";
	}


}catch(PDOException $ex) {
	echo "Invalid Query";
	die();
}
?>
