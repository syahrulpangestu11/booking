<?php
$db_hostname='localhost'; 
// $db_username='support-tb_bsmsu'; 
// $db_password='9pnMxKViKn'; 
$db_username='root'; 
$db_password=''; 
$dbname='o2o_2';

try {
	$db = new PDO('mysql:host='.$db_hostname.';dbname='.$dbname.';charset=utf8', $db_username, $db_password);
	$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
}catch(PDOException $e) {
	echo "Invalid Connection";
	die();
	
}

date_default_timezone_set('Asia/Brunei');
$todaydate=(date("Y-m-d") >= "2015-01-01") ? date("Y-m-d") : "2015-01-01" ;
$_document_root= $_SERVER['DOCUMENT_ROOT']."";

//Price Adjustment for Owner
include_once('price_adjustment.php');


$_profile = $db->query("SELECT masterprofileoid, inquiry_status, inquiry_wording, shortcode, name, imglogo, imgicon, imgfavicon, imglogo2, 
								description, email, phone, whatsapp, address, socialmediaurl, fax, web,
								cityname as city, statename as state, countryname as country,
								mastersetting.limit_hotel, mastersetting.frontend_account        
							FROM masterprofile f 
							INNER JOIN city USING(cityoid)
							INNER JOIN state USING(stateoid)
							INNER JOIN country USING(countryoid)
							INNER JOIN mastersetting USING(masterprofileoid)
							WHERE masterprofileoid='1'  limit 1")
			->fetch();
?>