<?php
$adjust = 1;
$avoid_adjust = array();
$hide_other_charges = 0;

if(isset($_SESSION['_user']) && $_SESSION['_user']=='randymikayla'){
    $adjust = 0.95;
}

$stmt = $db->prepare("SELECT useroid FROM users INNER JOIN userassign USING(useroid) WHERE userpmstypeoid = 5 AND username = :a AND ((`type` = 'hoteloid' AND oid IN (SELECT hoteloid FROM hotel WHERE chainoid = '64')) OR (`type` = 'chainoid' AND oid = '64'))");
$stmt->execute(array(':a' => $_SESSION['_user']));
$radj = $stmt->fetchAll(PDO::FETCH_ASSOC);
if(count($radj) > 0){
    $adjust = 0.95;
    $hide_other_charges = 1;
    //$avoid_adjust[] = '1412';
}

?>