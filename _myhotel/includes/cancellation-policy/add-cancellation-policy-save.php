<?php
	include("../../conf/connection.php");

	$roomofferoid = $_POST['roomofferoid'];
	$channeloid = $_POST['channeloid'];
	$startdate = date("Y-m-d", strtotime($_POST['startdate']));
	$enddate = date("Y-m-d", strtotime($_POST['enddate']));
	$cancellationpolicyoid = $_POST['cancellation'];
	$published = 1;
	
	try {
		$stmt = $db->prepare("INSERT INTO cancellationpolicyapply (roomofferoid, channeloid, startdate, enddate, cancellationpolicyoid, publishedoid) VALUES (:b , :c , :d , :e , :f , :g)");
		$stmt->execute(array(':b' => $roomofferoid, ':c' => $channeloid, ':d' => $startdate, ':e' => $enddate , ':f' => $cancellationpolicyoid , ':g' => $published));
		echo "1";
	}catch(PDOException $ex) {
		echo "0";
		die();
	}
?>