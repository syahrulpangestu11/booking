<script type="text/javascript">
$(function(){
			
	$('body').on('click','button.add', function(e) {
		tr = $(this).parent('td').parent('tr');
		var note = tr.find('input[name=note]').val();
		var startdate = tr.find('input[name=startdate]').val();
		var enddate  = tr.find('input[name=enddate]').val();
		var rate = tr.find('input[name=rate]').val();
		var surcharge = tr.find('input[name=surcharge]').val();
		var priority = tr.find('input[name=priority]').val();
		var ro = $(this).attr('ro');
		var ah = $(this).attr('ah');
		$.ajax({
			url: '<?php echo $base_url; ?>/includes/agent-rate/save.php',
			type: 'post',
			data: { note : note, startdate : startdate, enddate : enddate, rate : rate, surcharge : surcharge, priority : priority, ro : ro, ah : ah, request : "save" },
			success: function(data) {
				if(data == "success"){
					$dialogNotification.html("Data has been added");
					$dialogNotification.dialog("open");
				}else{
					$dialogNotification.html("We&rsquo;re very sorry, we can't save your data right now");
					$dialogNotification.dialog("open");
				}
			}
		});		
	});
	
	$('body').on('click','button.edit', function(e) {
		tr = $(this).parent('td').parent('tr');
		var note = tr.find('input[name=note]').val();
		var startdate = tr.find('input[name=startdate]').val();
		var enddate  = tr.find('input[name=enddate]').val();
		var rate = tr.find('input[name=rate]').val();
		var surcharge = tr.find('input[name=surcharge]').val();
		var priority = tr.find('input[name=priority]').val();
		ahr = $(this).attr('ahr');
		$.ajax({
			url: '<?php echo $base_url; ?>/includes/agent-rate/save.php',
			type: 'post',
			data: { note : note, startdate : startdate, enddate : enddate, rate : rate, surcharge : surcharge, priority : priority, ahr : ahr, request : "update" },
			success: function(data) {
				if(data == "success"){
					$dialogNotification.html("Data has been updated");
					$dialogNotification.dialog("open");
				}else{
					$dialogNotification.html("We&rsquo;re very sorry, we can't save your data right now");
					$dialogNotification.dialog("open");
				}
			}
		});		
	});

   var $dialogDelete = $('<div id="dialog-del"></div>')
	.html('Are you sure you want to delete this data?')
	.dialog({
		autoOpen: false,
		title: 'Confirmation',
		buttons: { 
			"Delete": function(){ 
				var elem = $(this).data('elem'); 
				$(this).dialog("close"); 
				deleteElem(elem);
			},
			Cancel: function(){ $( this ).dialog("close"); }
		}
	});
	
	var $dialogNotification = $('<div id="dialog-notice"></div>')
	.dialog({
		autoOpen: false,
		title: 'Notification',
		buttons: { 
			"Ok": function(){ 
				var callback = '<?php echo $base_url; ?>/agent-rate/edit/?aho=<?=$_GET['aho']?>'; 
				$(this).dialog( "close" );
				$(location).attr("href", callback); 
			}
		}
	});
	
	function deleteAction(url, id){
		$.ajax({
			url: url,
			type: 'post',
			data: { id : id},
			success: function(data) {
				if(data == "1"){
					$dialogNotification.html("Data has been deleted");
					$dialogNotification.dialog("open");
				}else{
					$dialogNotification.html("We&rsquo;re very sorry, we can't save your data right now");
					$dialogNotification.dialog("open");
				}
			}
		});
	}
	
	function deleteElem(elem){
		elem.parent().parent().remove();
	}
		
	$('body').on('click','button.delete', function(e) {
		$dialogDelete.data("elem", $(this));
		$dialogDelete.dialog("open");
	});
			
	$('.startdate').each(function(){
    	$(this).datepicker();
	});
	$('.enddate').each(function(){
    	$(this).datepicker();
	});
});
</script>