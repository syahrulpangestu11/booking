<link rel="stylesheet" href="<?php echo $base_url; ?>/lib/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo $base_url; ?>/lib/admin-lte/css/AdminLTE.min.css">
<link href="<?php echo $base_url?>/css/style.css?v=1" rel="stylesheet" type="text/css">
<script src="<?php echo $base_url; ?>/lib/bootstrap/js/bootstrap.min.js"></script>
		<style type="text/css">
			h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 { 
				font-family: inherit; 
				font-weight: 600;
				line-height: inherit;
				color: inherit;
				margin-bottom:10px!important;
			}
			.wrapper {
				position: inherit;
				overflow: hidden!important;
			}
			.left-side {
				padding-top: inherit;
			}
			.sidebar > .sidebar-menu li > a:hover {
				background-color: rgba(72, 115, 175, 0.26);
			}
			.sidebar .sidebar-menu .treeview-menu {
				background-color: rgb(14, 26, 43);
			}
			.sidebar .sidebar-menu .treeview-menu .treeview-menu-child {
				background-color: rgba(0, 0, 0, 0.5);
			}
			.sidebar > .sidebar-menu li.active > a {
				background-color: rgba(197, 45, 47, 0.55);
			}
			.sidebar > .sidebar-menu > li.treeview.active > a {
				background-color: inherit;
			}
			.sidebar > .sidebar-menu > li.treeview.active li.treeview-child.active > a {
				background-color: inherit;
			}
			.sidebar .sidebar-menu > li > a > .fa {
				width: 28px;
				font-size: 16px;
			}
			.sidebar-menu, .main-sidebar .user-panel, .sidebar-menu>li.header{
				white-space:normal!important;
			}
			.form-group input[type=text]{
				width:100%!important;
			}
</style>
<section class="content-header">
    <h1>Payment Method</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-cog"></i> Settings</a></li>
        <li class="active">Payment Method</li>
    </ol>
</section>
<section class="content">
    <form method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/promotions/add-process">
    <?php
		$stmt = $db->query("select paymentmethodoid from hotel where hoteloid = '".$hoteloid."'");
		$r_paymenthotel = $stmt->fetch(PDO::FETCH_ASSOC);
		$paymentmethodhotel = $r_paymenthotel['paymentmethodoid'];
	?>
    <div class="box box-form">
    	<div class="row">
            <div class="col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">Note</div>
                    <div class="panel-body">
					<?php
                        $stmt = $db->query("select * from paymentmethod where publishedoid = '1'");
                        $r_method = $stmt->fetchAll(PDO::FETCH_ASSOC);
                        foreach($r_method as $method){
							echo "<div style='margin-bottom:5px;'><b>&bull; ".$method['method']."</b><br>".$method['description'].'</div>';
						}
                    ?>
                    </div>
                </div>
            </div>
        	<div class="col-md-6">
                <div class="form-group"><label>Select Your Hotel Payment Method :</label></div>
                <?php
                    foreach($r_method as $method){
						if($method['paymentmethodoid'] == $paymentmethodhotel){ $checked = 'checked = "checked"'; }else{ $checked = ""; }
                ?>
                <div class="checkbox">
                    <label>
                        <input type="radio" name="method" value="<?=$method['paymentmethodoid']?>" aria-label="<?=$method['method']?>" <?=$checked?>> <?=$method['method']?>
                    </label>
                </div>
                <?php
                    }
                ?>
                <div class="form-group"><button type="button" class="small-button blue submit-edit">Save Change</button></div>
    		</div>
    	</div>        
    </div>
	</form>
</section>