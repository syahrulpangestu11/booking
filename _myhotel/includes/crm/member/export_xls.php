<?php

if(isset($_SESSION['_hotel']) and !empty($_SESSION['_hotel'])){
    $hoteloid = $_SESSION['_hotel'];
    $main_query = "select m.* from member m inner join memberhotel mh using (memberoid) where mh.hoteloid = '".$hoteloid."' order by m.joindate desc";
    
    try {
		$stmt = $db->query($main_query);
		$r_promo = $stmt->fetchAll(PDO::FETCH_ASSOC);
		if(count($r_promo) > 0) {
		    $data = array();
	        foreach($r_promo as $row){
	            $data[] = array(
	                    "Name" => $row['firstname']." ".$row['lastname'],
	                    "E-Mail" => $row['emailmailing'],
	                    "Point" => $row['point'],
	                    "Join Date" => date('d/M/Y', strtotime($row['joindate']))
	                );
	        }
	        
	        $filename = "hotelmember_".date('Ymdhis').".xls";		 
            header("Content-Type: application/vnd.ms-excel");
			header("Content-Disposition: attachment; filename=\"$filename\"");
			
			$heading = false;
			foreach($data as $row) {
                if(!$heading) {
                  echo implode("\t", array_keys($row)) . "\n";
                  $heading = true;
                }
                echo implode("\t", array_values($row)) . "\n";
            }
            
		}else{
		    echo '<script>alert("Empty Data");document.location.href="'.$base_url.'/crm/member";</script>';
		}
    }catch(PDOException $ex) {
		echo '<script>alert("Invalid Request");document.location.href="'.$base_url.'/crm/member";</script>';
	}
}else{
    echo '<script>alert("Invalid Request");document.location.href="'.$base_url.'/crm/member";</script>';
}