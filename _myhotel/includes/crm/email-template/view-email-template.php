 <?php
	$name = $_POST['name'];
	$email = $_POST['email'];
?>
<section class="content-header">
    <h1>Email Template</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-handshake-o"></i> Customer Relationship Management</a></li>
        <li class="active">Email Template</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="box">
            <div class="box-body">
                <div class="form-group">
            	<form method="get" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/crm/member/">
            		<input type="hidden" name="hoteloid" value="<?php echo $hoteloid; ?>" />
                    <label>Email Type  :</label> &nbsp;
					<select name="type-email"  class="input-text">
                    	<option value="booking">booking</option>
                        <option value="occasional">occasional</option>
                    </select>
                    &nbsp;
                    <label>Name  :</label> &nbsp;&nbsp;
					<input type="text" name="name" class="input-text" value="<?php echo $name; ?>">&nbsp;
                    <button type="submit" class="small-button blue"><i class="fa fa-search"></i>Find</button>
                </form>
                </div>
            </div><!-- /.box-body -->
       </div>
    </div>
    
    <div class="row">
        <div class="box">
        	<form method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/room-control/process">
                <div id="data-box" class="box-body">
                    <div class="loader">Loading...</div>
                </div><!-- /.box-body -->
            </form>
       </div>
    </div>

</section>
