<link rel="stylesheet" href="<?php echo $base_url; ?>/lib/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo $base_url; ?>/lib/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<link rel="stylesheet" href="<?php echo $base_url; ?>/lib/admin-lte/css/AdminLTE.min.css">
<link href="<?php echo $base_url?>/css/style.css?v=1" rel="stylesheet" type="text/css">
<script src="<?php echo $base_url; ?>/lib/bootstrap/js/bootstrap.min.js"></script>
		<style type="text/css">
			h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 { 
				font-family: inherit; 
				font-weight: 600;
				line-height: inherit;
				color: inherit;
				margin-bottom:10px!important;
			}
			.wrapper {
				position: inherit;
				overflow: hidden!important;
			}
			.left-side {
				padding-top: inherit;
			}
			.sidebar > .sidebar-menu li > a:hover {
				background-color: rgba(72, 115, 175, 0.26);
			}
			.sidebar .sidebar-menu .treeview-menu {
				background-color: rgb(14, 26, 43);
			}
			.sidebar .sidebar-menu .treeview-menu .treeview-menu-child {
				background-color: rgba(0, 0, 0, 0.5);
			}
			.sidebar > .sidebar-menu li.active > a {
				background-color: rgba(197, 45, 47, 0.55);
			}
			.sidebar > .sidebar-menu > li.treeview.active > a {
				background-color: inherit;
			}
			.sidebar > .sidebar-menu > li.treeview.active li.treeview-child.active > a {
				background-color: inherit;
			}
			.sidebar .sidebar-menu > li > a > .fa {
				width: 28px;
				font-size: 16px;
			}
			.sidebar-menu, .main-sidebar .user-panel, .sidebar-menu>li.header{
				white-space:normal!important;
			}
			.form-group input[type=text]{
				width:100%!important;
			}
		</style>
<section class="content-header">
    <h1>Email Template</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-handshake-o"></i> Customer Relationship Management</a></li>
        <li class="active">Add New Campaign Loyalty Member Program</li>
    </ol>
</section>
<section class="content">
    <form class="form-box" method="post" enctype="multipart/form-data" id="data-input" action="#">
    <div class="box box-form">
        <div class="row">
            <div class="col-md-12"><h1>Detail Email Template</h1></div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group col-md-12">
                    <label>Name</label>
                    <input type="text" class="form-control" name="name" required>
                </div>
                <div class="form-group col-md-12">
                    <label>Description</label>
                    <textarea class="form-control" name="description"></textarea>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group col-md-6">
                    <label>Type Email</label>
                    <select name="type-email" class="form-control">
                        <option value="booking" selected>booking</option> 
                        <option value="occasional">occasional</option>       	
                    </select>
                </div>
                <div class="email-booking">
                    <div class="form-group col-md-12">
                        <label>Sending</label>
                        <div class="radio">
                          <label>
                            <input type="radio" name="sending-email" id="optionsRadios1" value="before" checked>
                            before
                          </label>
                        </div>
                        <div class="radio">
                          <label>
                            <input type="radio" name="sending-email" id="optionsRadios2" value="during">
                            during
                          </label>
                        </div>
                        <div class="radio disabled">
                          <label>
                            <input type="radio" name="sending-email" id="optionsRadios3" value="after" disabled>
                            after
                          </label>
                        </div>
					</div>
                    <div class="form-group col-md-8">
                        <div class="input-group"><input type="text" class="form-control" name="days" required value="3"><div class="input-group-addon">day before / after checkin</div></div>
                    </div>
                </div>
                <div class="email-occasional" style="display:none;">
                    <div class="form-group col-md-12">
                        <label>Sending</label>
                        <div class="radio">
                          <label>
                            <input type="radio" name="sending-email" id="optionsRadios1" value="birthday" checked>
                            birthday
                          </label>
                        </div>
					</div>
                </div>
 			</div>               
        </div>
    </div>
    <div class="box box-form">
        <div class="row">
            <div class="col-md-12"><h1>Email Content</h1></div>
        </div>
        <div class="row">
            <div class="form-group col-md-12">
                <label>Subject</label>
                <input type="text" class="form-control" name="subject" required>
            </div>
            <div class="form-group col-md-12">
                <label>Email Content</label>
                <textarea class="email-body" name="email-body" style="width:100%"></textarea>
                <div class="clear"></div><div class="clear"></div>
            </div>
            <div class="clear"></div>
            <div class="clear"></div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <button class="default-button" type="submit">Save</button>
                <button class="pure-button red cancel" type="button">Cancel</button>
            </div>
        </div>
    </div>
    </form>
</section>