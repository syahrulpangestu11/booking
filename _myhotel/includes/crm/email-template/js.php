<script type="text/javascript">
$(function(){
	
	$(document).ready(function(){ 
		getLoadData();
	});
	
	$('body').on('click','#paging button', function(e) {
		page	= $(this).attr('page');
		pagebox = $('form#data-input').children('input[name=page]');
		pagebox.val(page);
		getLoadData(); 
	});
	
	function getLoadData(){
		$.ajax({
			url: "<?php echo"$base_url"; ?>/includes/crm/email-template/data.php",
			type: 'post',
			data: $('form#data-input').serialize(),
			success: function(data) {
				$("#data-box").html(data)
			}
		});
	}
	
	$('body').on('click','button.add-button', function(e) {
		url = "<?php echo $base_url; ?>/crm/email-template/add";
      	$(location).attr("href", url);
	});
	
	$('textarea.email-body').trumbowyg({
		btns: ['viewHTML',
		  '|', 'btnGrp-design',
		  '|', 'link',
		  '|', 'btnGrp-justify',
		  '|', 'btnGrp-lists'],
		fullscreenable: false
	});
	
	$('body').on('change','select[name=type-email]', function(e) {
		if($(this).val() == "booking"){
			$('div.email-booking').show();
			$('div.email-occasional').hide();
		}else{
			$('div.email-booking').hide();
			$('div.email-occasional').show();
		}
	});
});
</script>