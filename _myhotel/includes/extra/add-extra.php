<?php
if(isset($_GET['tid'])){
		$templateoid = $_GET['tid'];
		try {
			$stmt = $db->query("select * from extra_template where extra_templateoid = '".$templateoid."'");
			$row_count = $stmt->rowCount();
			if($row_count > 0) {
				$r_promo = $stmt->fetchAll(PDO::FETCH_ASSOC);
				foreach($r_promo as $row){
					$name = $row['name'];
					$salefrom = date("d F Y", strtotime($row['startbook']));
					$saleto = date("d F Y", strtotime($row['endbook']));
					$published = $row['publishedoid'];
					$headline = $row['headline'];
					$description = $row['description'];
					$picture = $row['picture'];
				}
			}else{
				echo "No Result";
				die();
			}
		}catch(PDOException $ex) {
			echo "Invalid Query";
			die();
		}
}
?>

<style>
    .form-box ul li span.label{
        color: black;
        font-size: 14px;
        font-weight: normal;
        text-align: left;}
     input[type=file]{display: inline-block;}
</style>
<script type="text/javascript">
$(function() {
	$('textarea').trumbowyg({
		btns: ['viewHTML',
		  '|', 'btnGrp-design',
		  '|', 'link',
		  '|', 'btnGrp-justify',
		  '|', 'btnGrp-lists'],
		fullscreenable: false
	});
});
</script>
<section class="content-header">
    <h1>
        Create Extra
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i>  Allotments &amp; Rates </a></li>
        <li class="active">Extra</li>
    </ol>
</section>
<section class="content">
	<div class="row">
        <form class="form-box" method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/extra/add-process">
        <div class="box box-form">
            <h1>Extra</h1>
        <ul class="inline-half">
            <li>
            <ul class="block">
                <li>
                    <span class="label">Extra Name:</span>
                    <input type="text" class="long" name="name" value="<?=$name?>">
                </li>
                <li>
                    <span class="label">Picture</span>
                    <input type="file" class="medium" name="image">
                </li>
                <li>
                <img id="preview" src="<?php if(!empty($picture)){ echo $picture; }else{ echo "#"; } ?>" alt="" style="<?php if(empty($picture)){ echo "display:none"; } ?>" />
                </li>
            </ul>
            <li>
            <ul class="block">
                <li><span class="label"><b>Sale Date</b></span></li>
                <li>
                    <span class="label">Sale Date From:</span>
                    <input type="text"class="medium" id="startdate" name="salefrom" value="<?=$salefrom?>">
                </li>
                <li>
                    <span class="label">Sale Date To:</span>
                    <input type="text"class="medium" id="enddate"; name="saleto" value="<?=$saleto?>">
                </li>
                <li>
                    <span class="label"><b>Price:</b></span>
                    <select name="currency" class="input-select">
                        <?php
                        try {
                            $stmt = $db->query("select * from currency where publishedoid = '1'");
                            $r_published = $stmt->fetchAll(PDO::FETCH_ASSOC);
                            foreach($r_published as $row){
                        ?>
                            <option value="<?php echo $row['currencyoid']; ?>"><?php echo $row['currencycode']; ?></option>
                        <?php
                            }
                        }catch(PDOException $ex) {
                            echo "Invalid Query";
                            die();
                        }
                        ?>
                    </select>
                    <input type="text" class="medium" name="price">
                </li>
                <li>
                    <span class="label">Publish Extra :</span>
                    <select name="published">
                    <?php
                        try {
                            $stmt = $db->query("select * from published where showthis = 'y'");
                            $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                            foreach($r_room as $row){
                                echo"<option value='".$row['publishedoid']."'>".$row['note']."</option>";
                            }
                        }catch(PDOException $ex) {
                            echo "Invalid Query";
                            die();
                        }
                    ?>
                    </select>
                </li>
            </ul>
            </li>
        </ul>
        </div>
        <div class="box box-form">
			<h2>HEADLINE &amp; DETAIL</h2>
            <ul class="inline-half">
                <li>
                    <h3>Headline</h3>
                    <textarea name="headline"><?=$headline?></textarea>
                </li>
				<li>
                	<h3>Description</h3>
                   	<textarea name="description"><?=$description?></textarea>
				</li>
			</ul>
            <div class="clear"></div>
            <div class="clear"></div>
            <button class="default-button" type="submit">Submit Extra</button>
   		</div>
		</form>
    </div>
</section>
