<?php
	$categoryextrasoid = $_POST['categoryextrasoid'];
	$name = (isset($_POST['name'])) ? $_POST['name'] : "";
	$published = $_POST['published'];
	$description = $_POST['description'];
	
	try {
		$stmt = $db->prepare("update categoryextras set categoryname = :a, description = :b, publishedoid = :c where categoryextrasoid = :eoid");
		$stmt->execute(array(':a' => $name, ':b' => $description, ':c' => $published, ':eoid' => $categoryextrasoid));
	?>
		<script type="text/javascript">
            $(function(){ $( document ).ready(function(){ $("#dialog-success-category").dialog("open"); }); });
        </script>
	<?php
	}catch(PDOException $ex) {
	?>
		<script type="text/javascript">
            $(function(){ $( document ).ready(function(){ $("#dialog-error-category").dialog("open"); }); });
        </script>
	<?php	
	}
?>