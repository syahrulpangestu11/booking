<?php
	$name = (isset($_POST['name'])) ? $_POST['name'] : "";
	$published = $_POST['published'];
	$description = $_POST['description'];
	
	try {
		$stmt = $db->prepare("insert into categoryextras (categoryname, description, publishedoid) values (:a, :b, :c)");
		$stmt->execute(array(':a' => $name, ':b' => $description, ':c' => $published));
		$extraoid = $db->lastInsertId();
		
	?>
		<script type="text/javascript">
            $(function(){ $( document ).ready(function(){ $("#dialog-success-category").dialog("open"); }); });
        </script>
	<?php
	}catch(PDOException $ex) {
	?>
		<script type="text/javascript">
            $(function(){ $( document ).ready(function(){ $("#dialog-error-category").dialog("open"); }); });
        </script>
	<?php	
	}
?>