<?php
	function saveImage($tn, $savePath, $imageQuality="100") {
		// *** Get extension
		$extension = strrchr($savePath, '.');
		$extension = strtolower($extension);

		switch($extension) {
			case '.jpg':
			case '.jpeg':
				if (imagetypes() & IMG_JPG) {
					imagejpeg($tn, $savePath, $imageQuality);
				}
				break;
			case '.gif':
				if (imagetypes() & IMG_GIF) {
					imagegif($tn, $savePath);
				}
				break;
			case '.png':
				// *** Scale quality from 0-100 to 0-9
				$scaleQuality = round(($imageQuality/100) * 9);

				// *** Invert quality setting as 0 is best, not 9
				$invertScaleQuality = 9 - $scaleQuality;

				if (imagetypes() & IMG_PNG) {
					imagepng($tn, $savePath, $invertScaleQuality);
				}
				break;
			// ... etc
			default:
				// *** No extension - No save.
				break;
		}

		imagedestroy($tn);
	}

	$allowedExts = array('jpg', 'jpeg', 'gif', 'png');
	$allowedType = array('image/jpeg', 'image/pjpeg', 'image/gif', 'image/png');

	$picture_error = $_FILES['photos']['error'];
	if($picture_error > 0){
	?>
		<div id="dialog" title="Error Notification">
		  <p>We&rsquo;re very sorry, we can't save your data right now.<br />Please make sure you upload image file type (ex: .jpg , .png , .gif)</p>
		</div>
		<script type="text/javascript">
		$(function(){
			$( document ).ready(function() {
				$("#dialog").dialog("open");
			});
		});
		</script>
	<?php
		unset($_FILES['photos']);
		die();
	}else{
		$picture_name		= $_FILES['photos']['name'];
		$picture_tmp_name	= $_FILES['photos']['tmp_name'];
		$picture_type		= $_FILES['photos']['type'];

		$explode_picture_name	= explode('.', $picture_name);
		$picture_extension		= end($explode_picture_name);
		$ext = ".".strtolower($picture_extension);

		if(!in_array($picture_type, $allowedType) or !in_array($picture_extension, $allowedExts)){
		?>
			<div id="dialog" title="Error Notification">
			  <p>We&rsquo;re very sorry, we can't save your data right now.<br />Please make sure you upload image file type (ex: .jpg , .png , .gif)</p>
			</div>
			<script type="text/javascript">
			$(function(){
				$( document ).ready(function() {
					$("#dialog").dialog("open");
				});
			});
			</script>
		<?php
			unset($_FILES['photos']);
			die();
		}else{

			$picture_upload_name		= $key.'-'.date("Y_m_d_H_i_s").'-'.$picture_name;

			include"compress-image.php";

			$show_upload_path = $web_url."/".$folder.$imagename;
			$show_thumbnail_path = $web_url."/".$folder.$imagename_t;

			$reference		= explode('-' , $_POST['reference']);
				$reftable	= $reference[0];
				$refid		= (!empty($reference[1])) ? $reference[1] : "0";
			$main 			= '';


			$stmt = $db->prepare("update extra set picture = :a where extraoid = :eoid");
			$stmt->execute(array(':a' => $show_upload_path, ':eoid' => $extraoid));
			$affected_rows = $stmt->rowCount();
		}
	}
?>
