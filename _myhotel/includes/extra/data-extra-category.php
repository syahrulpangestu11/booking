<button type="button" class="pure-button green cancel"><< Back to Extra</button>
<button type="button" class="pure-button blue addcategory">Create New Category</button>
<span class="status-green square">sts</span> Active &nbsp;&nbsp;&nbsp; <span class="status-black square">sts</span> Inactive &nbsp;&nbsp;&nbsp;
<?php
	include("../../conf/connection.php");
	$datenow = date("Y-m-d");

	try {

		$main_query = "select e.* from categoryextras e where publishedoid != 3";
		$stmt = $db->query($main_query);
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
?>
<table class="table promo-table">
	<tr>
		<td>Category Name</td>
	</tr>
<?php
		if($row_count > 0) {
			$r_promo = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_promo as $row){
				$published = ($row['publishedoid']==1) ? "status-green" : "status-black";
				// echo $published;
?>
    <tr>
        <td class="<?php echo $published; ?>"><?php echo $row['categoryname']; ?></td>
        <td class="algn-right">
        <button type="button" class="pure-button green single editcategory" pid="<?php echo $row['categoryextrasoid']; ?>"><i class="fa fa-pencil"></i></button>
        <button type="button" class="pure-button red single deletecategory" pid="<?php echo $row['categoryextrasoid']; ?>"><i class="fa fa-trash"></i></button>
        </td>
    </tr>
<?php
			}
		}
?>
</table>
<?php
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		print($ex);
		die();
	}
?>
