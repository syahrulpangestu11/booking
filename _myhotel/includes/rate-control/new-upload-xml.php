<?php
include('includes/function-xml.php');
$path_xml_hotel = 'data-xml/'.$hcode.'.xml';

if(!file_exists($path_xml_hotel)){ createXML($path_xml_hotel_xml_hotel); }

$xml = new DomDocument();
    $xml->preserveWhitespace = false;
    $xml->load($path_xml_hotel);
$xpath = new DomXpath($xml);

$root = $xml->documentElement;

/*------------------------------------------------------------------------------------*/
$query_tag_hotel = '//hotel[@id="'.$hoteloid.'"]';
$query_tag_room = $query_tag_hotel.'//masterroom[@id="'.$roomoid.'"]';
/*------------------------------------------------------------------------------------*/
$elementName = array("netsingle", "netdouble", "single", "double", "extrabed", "breakfast", "minstay", "maxstay", "surcharge");
/*------------------------------------------------------------------------------------*/

	/* create <hotel id=@>---------------------------------------------------------*/
	$check_tag_hotel = $xpath->query($query_tag_hotel);
    if($check_tag_hotel->length == 0){
	    $hotel = $xml->createElement("hotel");
	        $hotel_id = $xml->createAttribute("id");
	        $hotel_id->value = $hoteloid;
	    $hotel->appendChild($hotel_id);
	    $root->appendChild($hotel);
		$check_tag_hotel = $xpath->query($query_tag_hotel);
    }

	/* create <hotel id=@><masterroom id=@>-----------------------------------------*/
    $check_tag_room = $xpath->query($query_tag_room);
    if($check_tag_room->length == 0){
	    $room = $xml->createElement("masterroom");
	        $room_id = $xml->createAttribute("id");
	        $room_id->value = $roomoid;
	    $room->appendChild($room_id);

	    $tag_hotel = $check_tag_hotel->item(0);
	    $tag_hotel->appendChild($room);
		$check_tag_room = $xpath->query($query_tag_room);
    }

foreach($_POST['date'] as $key => $value){

	$value_double = (isset($_POST['double'][$key]) and !empty($_POST['double'][$key])) ? $_POST['double'][$key] : 0;
	$value_extrabed = (isset($_POST['extrabed'][$key]) and !empty($_POST['extrabed'][$key])) ? $_POST['extrabed'][$key] : 0;
	$value_breakfast = (isset($_POST['breakfast'][$key]) and !empty($_POST['breakfast'][$key])) ? $_POST['breakfast'][$key] : "n";
	$value_minstay = (isset($_POST['minstay'][$key]) and !empty($_POST['minstay'][$key])) ? $_POST['minstay'][$key] : 1;
	$value_maxstay = (isset($_POST['maxstay'][$key]) and !empty($_POST['maxstay'][$key])) ? $_POST['maxstay'][$key] : 1;
	$value_surcharge = (isset($_POST['surcharge'][$key]) and !empty($_POST['surcharge'][$key])) ? $_POST['surcharge'][$key] : "n";

	$value_allotment = (isset($_POST['allotment'][$key]) and !empty($_POST['allotment'][$key])) ? $_POST['allotment'][$key] : 0;



	$elementNodes = array($value_double, $value_double, $value_double, $value_double, $value_extrabed, $value_breakfast, $value_minstay, $value_maxstay, $value_surcharge);

	$dateFormat = explode(" ",date("D Y-m-d",strtotime($value)));
	$day = $dateFormat[0];
	$date = $dateFormat[1];

	/* create <hotel id=@><masterroom id=@><rate date=@ day=@>----------------------*/
	$query_tag_rate = $query_tag_room.'/rate[@date="'.$date.'"]';
	$check_tag_rate = $xpath->query($query_tag_rate);

    if($check_tag_rate->length == 0){
		$rate = $xml->createElement("rate");
	        $rate_date = $xml->createAttribute("date");
	        $rate_date->value = $date;
		$rate->appendChild($rate_date);
	        $rate_day = $xml->createAttribute("day");
	        $rate_day->value = $day;
		$rate->appendChild($rate_day);

		$tag_room = $check_tag_room->item(0);
		$tag_room->appendChild($rate);
		$check_tag_rate = $xpath->query($query_tag_rate);
    }

    /* allotment 20170605 */
    $query_tag_allotment = $query_tag_rate.'/allotment';
	$check_tag_allotment = $xpath->query($query_tag_allotment);
	if($check_tag_allotment->length == 0){
	    $allotment = $xml->createElement("allotment");

	    $tag_rate = $check_tag_rate->item(0);
	    $tag_rate->appendChild($allotment);
	    $check_tag_allotment = $xpath->query($query_tag_allotment);
	}

	foreach($channellist as $channeltype){
		$query_tag_allotment_channel = $query_tag_allotment.'/channel[@type="'.$channeltype.'"]';
		$check_tag_allotment_channel = $xpath->query($query_tag_allotment_channel);

	    $allotment_channel = $xml->createElement("channel");
			$allotment_channel_type = $xml->createAttribute("type");
			$allotment_channel_type->value = $channeltype;
		$allotment_channel->appendChild($allotment_channel_type);
            $allotment_channel_value = $xml->createTextNode($value_allotment);
	    $allotment_channel->appendChild($allotment_channel_value);

		$tag_allotment = $check_tag_allotment->item(0);

	    if($check_tag_allotment_channel->length == 0){
			$tag_allotment->appendChild($allotment_channel);
			$check_tag_allotment_channel = $xpath->query($query_tag_allotment_channel);
		}else{
		    $tag_allotment_channel = $check_tag_allotment_channel->item(0);
			$tag_allotment->replaceChild($allotment_channel, $tag_allotment_channel);
		}

	}


	$query_tag_rateplan = $query_tag_rate.'/rateplan[@id="'.$roomoffer.'"]';
	$check_tag_rateplan = $xpath->query($query_tag_rateplan);

	if($check_tag_rateplan->length == 0){
		$rateplan = $xml->createElement("rateplan");
	        $rateplan_id = $xml->createAttribute("id");
	        $rateplan_id->value = $roomoffer;
		$rateplan->appendChild($rateplan_id);

		$tag_rate = $check_tag_rate->item(0);
		$tag_rate->appendChild($rateplan);
		$check_tag_rateplan = $xpath->query($query_tag_rateplan);
	}

	foreach($channellist as $channeltype){
		$query_tag_rateplan_channel = $query_tag_rateplan.'/channel[@type="'.$channeltype.'"]';
		$check_tag_rateplan_channel = $xpath->query($query_tag_rateplan_channel);


		if($check_tag_rateplan_channel->length == 0){
			$rateplan_channel = $xml->createElement("channel");
				$rateplan_channel_type = $xml->createAttribute("type");
				$rateplan_channel_type->value = $channeltype;
			$rateplan_channel->appendChild($rateplan_channel_type);

			$tag_rateplan = $check_tag_rateplan->item(0);
			$tag_rateplan->appendChild($rateplan_channel);
			$check_tag_rateplan_channel = $xpath->query($query_tag_rateplan_channel);
		}

		$tag_rateplan_channel = $check_tag_rateplan_channel->item(0);

		foreach($elementName as $key => $value){
			$element = $value;
			$node = $elementNodes[$key];

			$createElement = $xml->createElement($element);
				$createNode = $xml->createTextNode($node);
			$createElement->appendChild($createNode);

			$check_tag_element = $xpath->query($query_tag_rateplan_channel.'/'.$element);

			if($check_tag_element->length == 0){
				$tag_rateplan_channel->appendChild($createElement);
			}else{
				$tag_element = $check_tag_element->item(0);
				$tag_rateplan_channel->replaceChild($createElement,$tag_element);
			}
		}
	} // for each channel

  $dynamicrate = false;
  /* DR Change Availabilty ================================================== */
  if($_POST['activerule'] == "1" ){//and $hoteloid == "1"
    $newallotment = $value_allotment;
    $stmt = $db->query("select ro.* from roomoffer ro inner join published p using (publishedoid) inner join room r using (roomoid) inner join hotel h using (hoteloid) inner join offertype using (offertypeoid) where r.roomoid = '".$roomoid."' and p.publishedoid = '1' and h.dynamicrate = '1'");
    $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
    foreach($r_room as $row){
      $stmt = $db->prepare("select mr.rate from masterrate mr inner join dynamicraterule dr using (masterrateoid) inner join dynamicrate d using (dynamicrateoid) where d.hoteloid = :a and dr.roomofferoid = :b and (:c <= allocation) and d.publishedoid = '1' order by allocation asc limit 1");
      $stmt->execute(array(':a' => $hoteloid, ':b' => $row['roomofferoid'], ':c' => $newallotment));
      $foundrule = $stmt->rowCount();
      if($foundrule > 0){
        $newrate = $stmt->fetch(PDO::FETCH_ASSOC);

        $query_tag_rate = '//hotel[@id="'.$hoteloid.'"]//masterroom[@id="'.$roomoid.'"]/rate[@date="'.$date.'"]/rateplan[@id="'.$row['roomofferoid'].'"]/channel[@type="1"]/double';

        $check_tag_rate = $xpath->query($query_tag_rate);
        $check_tag_rate->item(0)->nodeValue = round($newrate['rate']);
        $dynamicrate = true;
      }
    }
  }

}

$xml->formatOutput = true;
//echo "<xmp>". $xml->saveXML() ."</xmp>";

$xml->save($path_xml_hotel) or die("Error");


?>
