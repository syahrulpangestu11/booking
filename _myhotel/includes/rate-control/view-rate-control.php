<?php
	if(empty($_REQUEST['rt']) and empty($_REQUEST['startdate']) and empty($_REQUEST['enddate'])){
		$start = date("d F Y");
		$end = date("d F Y",  strtotime(date("Y-m-d")." +7 day" ));
	}else{
		$start = date("d F Y", strtotime($_REQUEST['startdate']));
		$end = date("d F Y", strtotime($_REQUEST['enddate']));
		$roomoffer = $_REQUEST['rt'];
		$channeltype = $_REQUEST['channel'];
	}

	include("js.php");
?>
<section class="content-header">
    <h1>
        Rate Control
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i>  Allotments &amp; Rates </a></li>
        <li class="active">Rate Control</li>
    </ol>
</section>
<section class="content">

		<form method="get" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/rate-control/">

		<?php
		$stmt = $db->prepare("select dynamicrate, (select count(dynamicrateoid) as jmlrule from dynamicrate where hoteloid = hotel.hoteloid and publishedoid = '1') as jmlrule from hotel where hoteloid = :a");
		$stmt->execute(array(':a' => $hoteloid));
		$activate = $stmt->fetch(PDO::FETCH_ASSOC);
		if($activate['dynamicrate'] == "1" and $activate['jmlrule'] > 0){
		?>
		<div class="row">
			<div class="box box-danger box-form" style="background-color: #fdd8d8; padding:1px 20px 10px; text-align:center;">
				<div class="box-body">
					<b>PRECAUTION! <span style="color:#0000af;">Dynamic Rule Rate</span> ACTIVATED</b>, all your rates will remain change based on rules applied! Turn off the rules by click toggle button
					<label class="switch" style="margin-left: 7px;  bottom: -8px;">
						<input type="checkbox" name="activerule" <?php if($activate['dynamicrate'] == "1"){ echo "checked"; } ?> value="1">
						<span class="slider round"></span>
					</label>
					<input type="hidden" name="forcechange" value="1">
				</div>
			</div>
		</div>
		<?php
		}
		?>

    <div class="row">
        <div class="box box-form">
            <div class="box-body">
                <div class="form-group">
                    	<input type="hidden" name="ho" value="<?php echo $hoteloid; ?>" />
                        <table>
                            <tr>
                                <td>
                                    <label>Room Type</label> &nbsp;
                                    <select name="rt" class="input-select">
                                    <?php
                                        try {
                                            $stmt = $db->query("select r.roomoid, r.name, x.offernum from room r inner join hotel h using (hoteloid) left join (select count(roomofferoid) as offernum, ro.roomoid from roomoffer ro group by ro.roomoid) as x on x.roomoid = r.roomoid where h.hoteloid = '".$hoteloid."' and x.offernum > 0 and r.publishedoid = '1'");
                                            $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                            foreach($r_room as $row){
                                                echo"<optgroup label = '".$row['name']."'>";
                                                try {
                                                    $stmt = $db->query("select ro.* from roomoffer ro inner join published p using (publishedoid) inner join room r using (roomoid) inner join offertype using (offertypeoid) where r.roomoid = '".$row['roomoid']."' and p.publishedoid = '1'");
                                                    $r_offer = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                                    foreach($r_offer as $row1){
                                                        if($row1['roomofferoid'] == $roomoffer){ $selected = 'selected = "selected"';  }else{ $selected = ''; }
                                                       echo"<option value='".$row1['roomofferoid']."' ".$selected.">  ".$row1['name']."</option>";
                                                    }
                                                }catch(PDOException $ex) {
                                                    echo "Invalid Query";
                                                    die();
                                                }
                                                echo"</optgroup>";
                                            }
                                        }catch(PDOException $ex) {
                                            echo "Invalid Query";
                                            die();
                                        }

                                    ?>
                                    </select>
                                    &nbsp;&nbsp;
                                    <input type="hidden" name="channel" value="1" />
<!--                                    <label>Channel</label> &nbsp;&nbsp;
                                    <select name="channel">
                                    <?php
/*                                        try {
                                            $stmt = $db->query("select * from channel");
                                            $r_plan = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                            foreach($r_plan as $row){
												if($row['channeloid'] == $channeltype){ $selected = 'selected = "selected"';  }else{ $selected = ''; }
												echo"<option value='".$row['channeloid']."' ".$selected.">".$row['name']."</option>";
                                            }
                                        }catch(PDOException $ex) {
                                            echo "Invalid Query";
                                            die();
                                        }
*/
                                    ?>
                                    </select> &nbsp;
-->
                                    <label>Stay Date from</label> &nbsp;&nbsp;
                                    <input type="text" name="startdate" readonly autocomplete="off" id="<?php if($_SESSION['_typeusr']=='1' || $_SESSION['_typeusr']=='2'){echo "startdate";} else{ echo "startdate-2m-today";}?>" placeholder="" value="<?php echo $start; ?>"> &nbsp;&nbsp;
                                    <label>to</label> &nbsp;&nbsp;
                                    <input type="text" name="enddate" readonly autocomplete="off" id="<?php if($_SESSION['_typeusr']=='1' || $_SESSION['_typeusr']=='2'){echo "enddate";} else{ echo "enddate-2m-today";}?>" placeholder="" value="<?php echo $end; ?>"> &nbsp;&nbsp;
                                    <button type="submit" class="small-button blue"><i class="fa fa-search"></i> Search</button>
                                </td>
                            </tr>
                        </table>
                </div>
            </div><!-- /.box-body -->
       </div>
    </div>
		</form>

    <div class="row">
        <div class="box">
        <form method="post" id="form-update" enctype="multipart/form-data" action="<?php echo $base_url; ?>/rate-control/upload">
						<input type="hidden" name="activerule" <?php if($activate['dynamicrate'] == "1"){ echo "value='1'"; } ?>>
            <div id="data-box" class="box-body">
							<div class="loader">Loading...</div>
            </div><!-- /.box-body -->
				</form>
       </div>
    </div>

</section>
