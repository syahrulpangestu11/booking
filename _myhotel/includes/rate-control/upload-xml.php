 <?php
include('includes/function-xml.php');
$path_xml_hotel = 'data-xml/'.$hcode.'.xml';

if(!file_exists($path_xml_hotel)){ createXML($path_xml_hotel_xml_hotel); }

// load XML, create XPath object
$xml = new DomDocument();
$xml->preserveWhitespace = false;
$xml->load($path_xml_hotel);

$xpath = new DomXpath($xml);

// Get the root element "data"
$root = $xml->documentElement;

$query_tag_hotel = '//hotel[@id="'.$hoteloid.'"]';
$query_tag_room = $query_tag_hotel.'//masterroom[@id="'.$roomoid.'"]';

$check_tag_hotel = $xpath->query($query_tag_hotel);
if($check_tag_hotel->length == 0){
	$hotel = $xml->createElement("hotel");
	$hotel_id = $xml->createAttribute("id");
	$hotel_id->value = $hoteloid;
	$hotel->appendChild($hotel_id);
	
	$root->appendChild($hotel);
	$check_tag_hotel = $xpath->query($query_tag_hotel);
}

$check_tag_room = $xpath->query($query_tag_room);
if($check_tag_room->length == 0){
	$room = $xml->createElement("masterroom");
	$room_id = $xml->createAttribute("id");
	$room_id->value = $roomoid;
	$room->appendChild($room_id);
	
	$tag_hotel = $check_tag_hotel->item(0);
	$tag_hotel->appendChild($room);
	$check_tag_room = $xpath->query($query_tag_room);
}

foreach($_POST['date'] as $key => $value){
	
	$elementNodes = array($_POST['single'][$key], $_POST['double'][$key], $_POST['extrabed'][$key], $_POST['breakfast'][$key], $_POST['minstay'][$key], $_POST['maxstay'][$key], $_POST['blackout'][$key], $_POST['surcharge'][$key]);

	$dateFormat = explode(" ",date("D Y-m-d",strtotime($value)));
	$day = $dateFormat[0]; 
	$date = $dateFormat[1];
	
	$query_tag_rate = $query_tag_room.'/rate[@date="'.$date.'"]';

	$check_tag_rate = $xpath->query($query_tag_rate);
 
	if($check_tag_rate->length == 0){
		
		$rate = $xml->createElement("rate");
		$rate_date = $xml->createAttribute("date");
		$rate_date->value = $date;
		$rate->appendChild($rate_date);
		$rate_day = $xml->createAttribute("day");
		$rate_day->value = $day;
		$rate->appendChild($rate_day);
		
		$tag_room = $check_tag_room->item(0);
		$tag_room->appendChild($rate);
		
		$check_tag_rate = $xpath->query($query_tag_rate);
	} 

	$query_tag_rateplan = $query_tag_rate.'/rateplan[@id="'.$roomoffer.'"]';
		
	$check_tag_rateplan = $xpath->query($query_tag_rateplan);
	
	if($check_tag_rateplan->length == 0){
		$rateplan = $xml->createElement("rateplan");
		$rateplan_type = $xml->createAttribute("id");
		$rateplan_type->value = $roomoffer;
		$rateplan->appendChild($rateplan_type);
		
		$tag_rate = $check_tag_rate->item(0);
		$tag_rate->appendChild($rateplan);
		
		$check_tag_rateplan = $xpath->query($query_tag_rateplan);
	}

	
	foreach($channellist as $channeltype){
		
		$query_tag_channel = $query_tag_rateplan.'/channel[@type="'.$channeltype.'"]';
		
		$check_tag_channel = $xpath->query($query_tag_channel);
		
		if($check_tag_channel->length == 0){
			$channel = $xml->createElement("channel");
			$channel_type = $xml->createAttribute("type");
			$channel_type->value = $channeltype;
			$channel->appendChild($channel_type);
			
			$tag_channel = $check_tag_rateplan->item(0);
			$tag_channel->appendChild($channel);
			
			$check_tag_channel = $xpath->query($query_tag_channel);
			$tag_channel = $check_tag_channel->item(0);
			
			foreach($elementName as $key => $value){
				$element = $value;
				$node = $elementNodes[$key];	
				
				$createElement = $xml->createElement($element);
				$createNode = $xml->createTextNode($node);
				$createElement->appendChild($createNode);
				
				$tag_channel->appendChild($createElement);
			}
		}else{	
			foreach($elementName as $key => $value){
				$element = $value;
				$node = $elementNodes[$key];	
				
				$createElement = $xml->createElement($element);
				$createNode = $xml->createTextNode($node);
				$createElement->appendChild($createNode);
				
				$check_tag_element = $xpath->query($query_tag_channel.'/'.$element);
				$tag_channel = $check_tag_channel->item(0);
				
				if($check_tag_element->length == 0){
					$tag_channel->appendChild($createElement);
				}else{
					$tag_element = $check_tag_element->item(0);
					$tag_channel->replaceChild($createElement,$tag_element);
				}
			}
		}
	} // for each channel
}

$xml->formatOutput = true;
//echo "<xmp>". $xml->saveXML() ."</xmp>";

$xml->save($path_xml_hotel) or die("Error");


?>