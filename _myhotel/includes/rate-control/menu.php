<aside class="left-side">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?php echo"$base_url"; ?>/images/avatar5.png" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>Hello, <?=$_SESSION['_initial']?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <ul class="sidebar-menu">
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span> 
                    <i class="fa fa-angle-left pull-right"></i>        
                </a>
                <ul class="treeview-menu">
                    <li><a href=""><i class="fa fa-angle-right"></i> Availability</a></li>
                    <li><a href=""><i class="fa fa-angle-right"></i> Best Rate Guarantee</a></li>
                    <li><a href=""><i class="fa fa-angle-right"></i> Ranking &amp; Conversion</a></li>
                    <li><a href=""><i class="fa fa-angle-right"></i> Review &amp; Content Scores</a></li>
                    <li><a href=""><i class="fa fa-angle-right"></i> Promotion</a></li>
                    <li><a href=""><i class="fa fa-angle-right"></i> Booking Performances</a></li>
                    <li><a href=""><i class="fa fa-angle-right"></i> Average Daily Rate (ADR) Performance</a></li>
                    <li><a href=""><i class="fa fa-angle-right"></i> Length of Stay (LOS) &amp; Lead Time</a></li>
                    <li><a href=""><i class="fa fa-angle-right"></i> Update hotel profile</a></li>
                </ul>
            </li>
            <li class="treeview <?php if($uri2=="room-control" or $uri2=="rate-control" or $uri2=="promotions" or $uri2=="load-tarif" or $uri2=="cancellation-policy" or $uri2=="surcharge"){ echo "active"; }?>">
                <a href="#">
                    <i class="fa fa-table"></i> <span>Allotments &amp; Rates</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li <?php if( $uri2=="room-control"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/room-control"><i class="fa fa-angle-right"></i> Room Control</a></li>
                    <li <?php if($uri2=="rate-control"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/rate-control"><i class="fa fa-angle-right"></i> Rate Control</a></li>
                    <li <?php if($uri2=="promotions"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/promotions"><i class="fa fa-angle-right"></i> Promotions</a></li>
                    <li <?php if($uri2=="load-tarif"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/load-tarif"><i class="fa fa-angle-right"></i> Load Tariff</a></li>
                    <li <?php if($uri2=="cancellation-policy"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/cancellation-policy"><i class="fa fa-angle-right"></i> Cancellation Policy</a></li>
                    <li <?php if($uri2=="surcharge"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/surcharge"><i class="fa fa-angle-right"></i> Surcharge</a></li>
                </ul>
            </li>
            <li class="treeview <?php if($uri2=="booking"){ echo "active"; }?>">
                <a href="#">
                    <i class="fa fa-bar-chart-o"></i> <span>Reports</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu"> 
                    <li><a href=""><i class="fa fa-angle-right"></i> Dashboard</a></li>
                    <li <?php if($uri2=="booking"){ echo "class=active"; }?>><a href="3"><i class="fa fa-angle-right"></i> Booking</a></li>
                    <li><a href=""><i class="fa fa-angle-right"></i> Performance Reports</a></li>
                    <li><a href=""><i class="fa fa-angle-right"></i> System Log</a></li>
                </ul>
            </li>
            <li class="treeview <?php if($uri2=="hotel-settings" or $uri2=="room-settings" or $uri2=="hotel-profile" or $uri2=="tax-settings" or $uri2=="hotel-ranking"){ echo "active"; }?>">
                <a href="#">
                    <i class="fa fa-laptop"></i> <span> Settings</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li <?php if($uri2=="hotel-settings"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/hotel-settings"><i class="fa fa-angle-right"></i> Hotel Settings</a></li>
                    <li <?php if($uri2=="room-settings"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/room-settings"><i class="fa fa-angle-right"></i> Room Settings</a></li>
                    <li <?php if($uri2=="tax-settings"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/tax-settings"><i class="fa fa-angle-right"></i> Tax Settings</a></li>
                    <li><a href="#"><i class="fa fa-angle-right"></i> Hotel Competitor Star</a></li>
                    <li <?php if($uri2=="hotel-ranking"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/hotel-ranking"><i class="fa fa-angle-right"></i> Hotel Ranking</a></li>
                    <li class="treeview-child <?php if($uri2=="hotel-profile"){ echo "active"; }?>">
                    	<a href="<?php echo"$base_url"; ?>/hotel-profile">
                        	<i class="fa fa-angle-right"></i> Hotel Profile
                        </a>
                    	<ul class="treeview-menu-child">
                        	<li <?php if($uri3=="basic-info"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/hotel-profile/basic-info"><i class="fa fa-angle-double-right"></i> Basic Info</a></li>
                        	<li <?php if($uri3=="maps"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/hotel-profile/maps"><i class="fa fa-angle-double-right"></i> Maps</a></li>
                        	<li <?php if($uri3=="facilities"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/hotel-profile/facilities"><i class="fa fa-angle-double-right"></i> Facilities</a></li>
                        	<li <?php if($uri3=="photo"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/hotel-profile/photo"><i class="fa fa-angle-double-right"></i> Photo</a></li>
                        	<li <?php if($uri3=="rooms"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/hotel-profile/rooms"><i class="fa fa-angle-double-right"></i> Rooms</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>