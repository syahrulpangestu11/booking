<?php
try{
  session_start();
	error_reporting(E_ALL ^ E_NOTICE);
	include ('../../../../conf/connection.php');
  include ('../../../../conf/baseurl.php');
  include ('../../class-pms-lite.php');

  $hoteloid = $_POST['hotel'];
  $bookingroomdtloid = $_POST['id'];

  $pmslite = new PMSLite($db);
  $pmslite->setPMSHotel($hoteloid);
  $data = $pmslite->getBookingRoom($bookingroomdtloid);
  /*--------------------------------------*/

?>
  <form id="form-view-guest" method="post">
    <input type="hidden" name="bookingroom" value="<?=$data['bookingroomoid']?>">
		<div class="row">
			<div class="col-md-6"><h3>Single Reservation</h3></div>
			<div class="col-md-6"><h3>Reservation Number # <?=$data['bookingnumber']?></h3></div>
		</div>
    <hr />
    <div class="row">
			<div class="col-md-6"><h3><?=$data['guestname']?></h3></div>
			<div class="col-md-4"><h3>Room Type : <?=$data['roomname']?></h3></div>
      <div class="col-md-2"><h3>Room Number : <?=$data['roomnumber']?></h3></div>
		</div>
    <hr />
    <div class="row">
      <div class="col-md-6">
        <div class="row"><div class="col-md-12"><h4><span class="blue">Guest Details :</span></h4></div></div>
        <div class="row"><div class="col-md-12"><?=$data['guestname']?></div></div>
        <div class="row"><div class="col-md-12"><b>Phone :</b> <?=$data['phone']?></div></div>
        <div class="row"><div class="col-md-12"><b>Email :</b> <?=$data['email']?></div></div>
        <div class="row"><div class="col-md-12"><h4><span class="blue">Rate Details :</span> <?=$data['rateplan']?></h4></div></div>
        <?php if(in_array($_SESSION['_typepmsusr'], array('1', '3', '6'))){ ?>
        <div class="row"><div class="col-md-12" style="margin-top:5px;"><b>Total Amount :</b> <?=$data['currency']?> <?=number_format($data['total'], 2)?></div></div>
        <div class="row"><div class="col-md-12"><b>Paid :</b> <?=$data['currency']?> <?=number_format($data['deposit'], 2)?></div></div>
        <div class="row"><div class="col-md-12"><b>Balance :</b> <?=$data['currency']?> <?=number_format($data['balance'], 2)?></div></div>
        <?php } ?>
      </div>
      <div class="col-md-6">
        <div class="row"><div class="col-md-12"><h4><span class="blue">Stay Details :</span></h4></div></div>
        <div class="row"><div class="col-md-12"><b>Check in date :</b> <?=date('d M Y', strtotime($data['checkin']))?></div></div>
        <div class="row"><div class="col-md-12"><b>Check out date :</b> <?=date('d M Y', strtotime($data['checkout']))?></div></div>
        <div class="row"><div class="col-md-12"><b>Room Nights :</b> <?php if(empty($data['night'])){ echo $data['nights'];}else{ echo $data['night'];}?></div></div>
        <div class="row"><div class="col-md-12"><b>Occupancy :</b><?=$data['adult']?> Adults <?=$data['child']?> Children</div></div>
        <div class="row"><div class="col-md-12"><h4><span class="blue">Sales / Guest Request :</span></h4></div></div>
        <div class="row"><div class="col-md-12"><?=$data['note']?></div></div>
      </div>
    </div>
    <hr />
    <div class="row">
      <?php if(in_array($_SESSION['_typepmsusr'], array('5','6'))){ ?>
      <div class="col-md-12 text-right">
        <button type="button" name="view-detail" class="btn btn-sm btn-primary">View</button>
        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
      </div>
      <?php }else{ ?>
      <div class="col-md-6">
        <?php
        if($data['pmsstatusoid'] == 2){
          if($data['checkin'] > date('Y-m-d')){
        ?>
          <button type="button" name="pmsstatus" value="4" class="btn btn-sm btn-success">Early Check in</button>
        <?php
          }else{
        ?>
          <button type="button" name="pmsstatus" value="4" class="btn btn-sm btn-success">Check in</button>
        <?php
          }
        }else if($data['pmsstatusoid'] == 4){
          if(date('Y-m-d') < $data['checkout']){
        ?>
          <button type="button" name="pmsstatus" value="5" class="btn btn-sm btn-warning">Early Check out</button>
          <?php
          }else{
          ?>
            <button type="button" name="pmsstatus" value="5" class="btn btn-sm btn-warning">Check out</button>
        <?php
          }
        }else if($data['pmsstatusoid'] == 7){
        ?>
          <button type="button" name="pmsstatus" value="2" class="btn btn-sm btn-danger">Reserve</button>
        <?php
          if($data['checkin'] > date('Y-m-d')){
        ?>
          <button type="button" name="pmsstatus" value="4" class="btn btn-sm btn-success">Early Check in</button>
        <?php
          }else{
        ?>
          <button type="button" name="pmsstatus" value="4" class="btn btn-sm btn-success">Check in</button>
        <?php
          }
          if(date('Y-m-d') < $data['checkout']){
        ?>
          <button type="button" name="pmsstatus" value="5" class="btn btn-sm btn-warning">Early Check out</button>
          <?php
          }else{
          ?>
            <button type="button" name="pmsstatus" value="5" class="btn btn-sm btn-warning">Check out</button>
        <?php
          }
        }
        ?>
      </div>
      <div class="col-md-6 text-right">
        <button type="button" name="print" class="btn btn-sm btn-primary">Print Reservation Card</button>
        <button type="button" name="send-invoice" class="btn btn-sm btn-primary" style="display:none;">Send Email</button>
        <button type="button" name="view-detail" class="btn btn-sm btn-primary">View</button>
        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
      </div>
      <?php } ?>
    </div>
  </form>
  <form id="reservation-card" action="https://thebuking.com/myhotel/pms-lite/view/reservation-card.php" method="post" target="_blank">
    <input name="bookingroomdtl" type="hidden" value="<?=$data['bookingroomdtloid']?>">
    <input name="hotel" type="hidden" value="<?=$hoteloid?>">
  </form>
  <form id="view-reservation-detail" action="https://thebuking.com/myhotel/pms-lite/reservation-detail" method="post">
    <input name="bookingoid" type="hidden" value="<?=$data['bookingoid']?>">
    <input name="hotel" type="hidden" value="<?=$hoteloid?>">
  </form>
<?php
}catch(Exception $e){
  echo $e->getMessage();
}
?>
