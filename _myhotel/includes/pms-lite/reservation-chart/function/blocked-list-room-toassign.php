<?php
try{
  session_start();
	error_reporting(E_ALL ^ E_NOTICE);
	include ('../../../../conf/connection.php');
  include ('../../class-pms-lite.php');

  $hoteloid = $_POST['hotel'];
  $date = $_POST['date'];

  $pmslite = new PMSLite($db);
  $pmslite->setPMSHotel($hoteloid);
  $list_unassign_room = $pmslite->unassignedRoom($date);
  if(count($list_unassign_room) == 0){
?>
  No Data Reservation
  <div class="row">
    <div class="col-md-12 text-right">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
  </div>
<?php
  }else{
?>
<form id="asign-room" method="post">
  <table id="table-data" class="table table-striped">
    <thead>
      <tr>
        <th>#</th>
        <th>Booking Number</th>
        <th>Guest</th>
        <th>Email</th>
        <th>Phone</th>
        <th>Stay Duration</th>
        <th>Room # / Type</th>
        <th>Pax</th>
        <th>Amount</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
<?php
    $n = 1;
    foreach($list_unassign_room as $list){
?>
    <tr>
      <td><?=$n++?></td>
      <td><?=$list['bookingnumber']?><input type="hidden" name="bookingroom[]" value="<?=$list['bookingroomoid']?>"></td>
      <td><?=$list['guestname']?></td>
      <td><?=$list['email']?></td>
      <td><?=$list['phone']?></td>
      <td><?=date('d M Y', strtotime($list['checkin']))?> - <?=date('d M Y', strtotime($list['checkout']))?> (<?=$list['night']?>)</td>
      <td><?=$list['roomname']?></td>
      <td><?=$list['adult']?> (A) <?=$list['child']?> (C)</td>
      <td><?=$list['currencycode']?> <?=number_format($list['total'])?></td>
      <td>
        <button type="button" class="btn btn-xs btn-primary" name="assign-room" data-bookingroom="<?=$list['bookingroomoid']?>" data-hotel="<?=$hoteloid?>">Assign Room</button>
      </td>
    </tr>
<?php
    }
?>
    </tbody>
  </table>
  <hr>
  <div class="row">
    <div class="col-md-12 text-right">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
  </div>
</form>
<?php
  }
?>
<?php
}catch(Exception $e){
  echo $e->getMessage();
}
?>
