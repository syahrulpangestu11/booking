<?php
try{
  session_start();
  error_reporting(E_ALL ^ E_NOTICE);
  include ('../../../../conf/connection.php');
  include ('../../class-pms-lite.php');
  include ('../../class-housekeeping.php');

  $hoteloid = $_SESSION['_hotel'];

  $pmslite = new PMSLite($db);
  $pmslite->setPMSHotel($hoteloid);

  $bookingroomoid = $_POST['bookingroom'];
  $pmsstatusoid = $_POST['pmsstatus'];
  
  $stmt = $db->prepare("SELECT `chainoid` FROM `hotel` WHERE `hoteloid`=:a");
  $stmt->execute(array(':a' => $hoteloid));
  $chain = $stmt->fetch(PDO::FETCH_ASSOC);
  
  //-- NDBV Custom
  if($hoteloid == '10254' || $hoteloid == '1' || $chain == '64'){
      if($pmsstatusoid == '5' || $pmsstatusoid == '6'){
        if($hoteloid == '10254' || $hoteloid == '1'){
            //Payment
            $stmt = $db->prepare("SELECT `bookingoid`, `currencycode`, SUM(`total`) AS total 
                FROM `bookingpaymentdtl` LEFT JOIN `currency` USING(`currencyoid`) WHERE `bookingoid`=(SELECT bookingoid FROM bookingroom WHERE bookingroomoid=:a) 
                GROUP BY `bookingoid`, `currencycode`");
            $stmt->execute(array(':a' => $bookingroomoid));
            $payment = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $totalpayusd = 0;
            $totalpayidr = 0;
            foreach($payment as $pay){
                if($currcode == 'USD'){
                    $totalpayusd += $pay['total'];
                }else{
                    $totalpayidr += $pay['total'];
                }
            }
            //Total Room
            $stmt = $db->prepare("SELECT sum(br.total) AS roomtotal, sum(br.deposit) AS roomdeposit, sum(br.balance) AS roombalance, c.currencycode from bookingroom br INNER JOIN booking b USING(bookingoid) INNER JOIN currency c ON c.currencyoid=b.currencyoid where bookingroomoid = :a");
    		$stmt->execute(array(':a' => $bookingroomoid));
    		$result = $stmt->fetch(PDO::FETCH_ASSOC);
    		$roomtotal = $result['roomtotal'];
    		$roomtotalcc = $result['currencycode'];
            
            if($roomtotalcc == 'USD'){
                if(($totalpayusd + ($totalpayidr / 13000)) < $roomtotal){
                    echo "2"; die();
                }
            }else{
                if(($totalpayidr + ($totalpayusd * 13000)) < $roomtotal){
                    echo "2"; die();
                }
            }
        }
        
        //UPDATE HK
        $housekeeping = new PMSHousekeeping($db);  
        $datetime = date("Y-m-d H:i:s");
        
        $stmt = $db->prepare("SELECT `date`, `roomnumberoid` FROM `bookingroomdtl` WHERE `bookingroomoid`=:a ORDER BY `date` LIMIT 1");
        $stmt->execute(array(':a' => $bookingroomoid));
        $checkhk = $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach($checkhk as $chk){
            $hk = $housekeeping->HousekeepingRoom($chk['roomnumberoid'], date("Y-m-d"));
            
            $roomnumberoid = $chk['roomnumberoid'];
            $fromstatus = $hk['housekeepingstatusoid'];
            $tostatus = '1';
            $note = "guest checkout";
        }
        
        $stmt	= $db->prepare("INSERT INTO housekeepinglog (logtime, roomnumberoid, fromstatus, tostatus, note, created, createdby) VALUES (:a, :b, :c, :d, :e, :f, :g)");
        $stmt->execute(array(':a' => $datetime, ':b' => $roomnumberoid, ':c' => $fromstatus, ':d' => $tostatus, ':e' => $note, ':f' => $datetime, ':g' => $_SESSION['_user']));
        
        $stmt	= $db->prepare("UPDATE roomnumber SET housekeepingstatusoid = :a, housekeepingnote = :b WHERE roomnumberoid = :id");
        $stmt->execute(array(':a' => $tostatus, ':b' => $note, ':id' => $roomnumberoid));
      }
      
      if($pmsstatusoid == '4'){
        $housekeeping = new PMSHousekeeping($db);  
        
        $stmt = $db->prepare("SELECT `date`, `roomnumberoid` FROM `bookingroomdtl` WHERE `bookingroomoid`=:a ORDER BY `date` LIMIT 1");
        $stmt->execute(array(':a' => $bookingroomoid));
        $checkhk = $stmt->fetchAll(PDO::FETCH_ASSOC);
        
        foreach($checkhk as $chk){
            $hk = $housekeeping->HousekeepingRoom($chk['roomnumberoid'], date("Y-m-d"));
            
            if($hk['housekeepingstatusoid'] != '3'){
                echo "3"; die();
            }
        }
      }
  }
  //-- NDBV Custom End
  
  $pmslite->changePMSStatus($bookingroomoid, $pmsstatusoid);

    // Prepare Data
    $stmt = $db->prepare("SELECT `roomoid` FROM `roomoffer` INNER JOIN `bookingroom` USING(`roomofferoid`) WHERE `bookingroomoid` = :a");
    $stmt->execute(array(':a' => $bookingroomoid));
    $r_ro = $stmt->fetch(PDO::FETCH_ASSOC);
    $vroomoid = $r_ro['roomoid'];
    
    $stmt = $db->prepare("SELECT COUNT(`roomnumberoid`) AS jml FROM `roomnumber` WHERE `roomoid` = :a AND `publishedoid` = 1");
    $stmt->execute(array(':a' => $vroomoid));
    $r_ro = $stmt->fetch(PDO::FETCH_ASSOC);
    $vroomnumber = $r_ro['jml'];
    
    $data6 = '{
        "token": "pfUxpxmKTJNTStNhPK2Nly4UKxbcVWwNRUT1SXnPfRiWqhvmINnJ4XnefNNBuTsx",
        "hcode": "'.$pmslite->hotelcode.'",
        "roomid": "'.$vroomoid.'",
        "data": [';
    $z = 0;
    
    $stmt = $db->prepare("SELECT * FROM `bookingroomdtl` WHERE bookingroomoid = :id");
    $stmt->execute(array(':id' => $bookingroomoid));
    $r_brd = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
    foreach($r_brd as $a_brd){
        
        // Prepare Data
        $vdate = $a_brd['date'];
        
        $stmt = $db->prepare("SELECT COUNT(`bookingroomdtloid`) jml
                FROM `bookingroomdtl` brd
                    LEFT JOIN `bookingroom` br USING(`bookingroomoid`)
                    LEFT JOIN `booking` b USING(`bookingoid`)
                    LEFT JOIN `roomoffer` ro USING(`roomofferoid`)
                WHERE b.`bookingstatusoid` IN (4) AND br.`pmsstatusoid` IN (2,4,7) 
                	AND ro.`roomoid` = :a AND brd.`date` = :b");
        $stmt->execute(array(':a' => $vroomoid, ':b' => $vdate));
        $r_ro = $stmt->fetch(PDO::FETCH_ASSOC);
        $vtxnumber = $r_ro['jml'];
        
        $vval = $vroomnumber - $vtxnumber;
        
        if($z != 0){ $data6 .=","; }else{ $z = 1; }
        $data6 .= '
            {
                "from": "'.$vdate.'",
                "to": "'.$vdate.'",
                "value": "'.$vval.'"
            }
        ';
        
    }
    
    // Prepare Data
    $data6 .= '
        ]
    }';
    
    // The data to send to the API
    $postData = (array) json_decode($data6);
    
    // Setup cURL
    $ch = curl_init('https://thebuking.com/apiv1/save/allotment_inrange/');
    curl_setopt_array($ch, array(
        CURLOPT_POST => TRUE,
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_SSL_VERIFYPEER => FALSE,
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
        ),
        CURLOPT_POSTFIELDS => json_encode($postData)
    ));
    
    // Send the request
    $response = curl_exec($ch);
    
    // Check for errors
    if($response === FALSE){
        die(curl_error($ch));
    }
    
    // Decode the response
    $responseData = json_decode($response, TRUE);


  echo "1";

}catch(Exception $e){
  print_r($_POST);
  echo $e->getMessage();
}
?>
