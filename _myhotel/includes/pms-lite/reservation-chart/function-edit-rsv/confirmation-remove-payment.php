<?php
try{
  session_start();
	error_reporting(E_ALL ^ E_NOTICE);
	include ('../../../../conf/connection.php');
  include ('../../../../conf/baseurl.php');
  include ('../../class-pms-lite.php');

  $paymentoid = $_POST['bpid'];

  $pmslite = new PMSReservation($db);
  $data = $pmslite->paymentListDetail($paymentoid);
  /*--------------------------------------*/

?>
  <form id="form-remove-charge" method="post">
    <input type="hidden" name="paymentoid" value="<?=$data['bookingpaymentdtloid']?>">
    <div class="row">
      <div class="col-md-6">
        <div class="row"><div class="col-md-12"><h4><span class="blue">Payment Date : </span><?=date('d M Y', strtotime($data['paymentdate']))?></h4></div></div>
        <div class="row"><div class="col-md-12"><h4><span class="blue">Type : </span> <?=$data['description']?></h4></div></div>
        <div class="row"><div class="col-md-12"><b>Receipt # :</b> <?=$data['receipt']?></div></div>
      </div>
      <div class="col-md-6">
        <div class="row"><div class="col-md-12"><h4><span class="blue">Details :</span></h4></div></div>
        <div class="row"><div class="col-md-12"><b>Amount :</b> <?=$data['currencycode']?> <?=number_format($data['amount'],2)?></div></div>
        <div class="row"><div class="col-md-12" style="margin-top:5px;"><b>Administration Fee :</b> <?=$data['currencycode']?> <?=number_format($data['admnistrationfee'],2)?></div></div>
        <div class="row"><div class="col-md-12" style="margin-top:5px;"><b>Total Payment :</b> <?=$data['currencycode']?> <?=number_format($data['total'],2)?></div></div>
      </div>
    </div>
    <hr />
    <div class="row">
      <div class="col-md-6"><b>Are you sure you want to remove this charge?</b></div>
      <div class="col-md-6 text-right">
        <button type="button" name="remove-charge" class="btn btn-sm btn-danger" data-id="<?=$data['bookingpaymentdtloid']?>">Remove</button>
        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </form>
<?php
}catch(Exception $e){
  echo $e->getMessage();
}
?>
