<?php
try{
    session_start();
    error_reporting(E_ALL ^ E_NOTICE);
    include ('../../../../conf/connection.php');
    
    $bookingroomoid = $_POST['bookingroomoid'];
    
    $prevratetotal = 0;
    $nextratetotal = 0;
    foreach($_POST['dailyratedate'] as $key => $date){
        $rate = $_POST['dailyrate'][$key];
        
        $stmt = $db->prepare("SELECT `total` FROM `bookingroomdtl` WHERE `bookingroomoid`=:a AND `date`=:b");
        $stmt->execute(array(':a' => $bookingroomoid, ':b' => $date));
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        $prevrate = $result['total'];
        
        $stmt = $db->prepare("UPDATE `bookingroomdtl` SET `total`=:a WHERE `bookingroomoid`=:b AND `date`=:c");
        $stmt->execute(array(':a' => $rate, ':b' => $bookingroomoid, ':c' => $date));
        
        $prevratetotal += $prevrate;
        $nextratetotal += $rate;
    }
    
    $stmt = $db->prepare("SELECT `bookingoid`, `roomtotal`, `roomtotalr`, `extrabedtotal`, `total`, `totalr`, `deposit`, `balance` FROM `bookingroom` WHERE `bookingroomoid`=:a");
    $stmt->execute(array(':a' => $bookingroomoid));
    $result = $stmt->fetch(PDO::FETCH_ASSOC);
    $bookingoid = $result['bookingoid'];
    $roomtotal = $result['roomtotal'];
    $roomtotalr = $result['roomtotalr'];
    $extrabedtotal = $result['extrabedtotal'];
    $total = $result['total'];
    $totalr = $result['totalr'];
    $deposit = $result['deposit'];
    $balance = $result['balance'];
    
    $nroomtotal = $roomtotal - $prevratetotal + $nextratetotal;
    $nroomtotalr = $roomtotalr - $prevratetotal + $nextratetotal;
    $ntotal = $nroomtotal + $extrabedtotal;
    $ntotalr = $nroomtotalr + $extrabedtotal;
    $nbalance = $ntotal - $deposit;
    
    $stmt = $db->prepare("UPDATE `bookingroom` SET `roomtotal`=:a, `roomtotalr`=:b, `total`=:c, `totalr`=:d, `balance`=:e WHERE `bookingroomoid`=:f");
    $stmt->execute(array(':a' => $nroomtotal, ':b' => $nroomtotalr, ':c' => $ntotal, ':d' => $ntotalr, ':e' => $nbalance, ':f' => $bookingroomoid));
    
    $stmt = $db->prepare("SELECT `grandtotal`, `grandtotalr`, `grandbalance`, `granddeposit` FROM `booking` WHERE `bookingoid`=:a");
    $stmt->execute(array(':a' => $bookingoid));
    $result = $stmt->fetch(PDO::FETCH_ASSOC);
    $grandtotal = $result['grandtotal'];
    $grandtotalr = $result['grandtotalr'];
    $grandbalance = $result['grandbalance'];
    $granddeposit = $result['granddeposit'];
    
    $ngrandtotal = $grandtotal - $total + $ntotal;
    $ngrandtotalr = $grandtotalr - $totalr + $ntotalr;
    $ngrandbalance = $ngrandtotal - $granddeposit;
    
    $stmt = $db->prepare("UPDATE `booking` SET `grandtotal`=:a, `grandtotalr`=:b, `grandbalance`=:c WHERE `bookingoid`=:f");
    $stmt->execute(array(':a' => $ngrandtotal, ':b' => $ngrandtotalr, ':c' => $ngrandbalance, ':f' => $bookingroomoid));

    echo "1";

}catch(Exception $e){
    echo "Error";
}
?>