<?php
try{
  session_start();
	error_reporting(E_ALL ^ E_NOTICE);
	include ('../../../../conf/connection.php');
  include ('../../class-pms-lite.php');

  $pmslite = new PMSReservation($db);

  $paymentoid = $_POST['bpid'];
  $bookingpayment = $pmslite->paymentListDetail($paymentoid);

  $pmslite->removePayment($paymentoid);

  $pmslite->RecalculateBooking($bookingpayment['bookingoid']);

  echo "1";

}catch(Exception $e){
  echo $e->getMessage();
}
?>
