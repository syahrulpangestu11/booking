<script type="text/javascript">
$(function(){
  $('#assignRoom').on('show.bs.modal', function (event){
    var modal = $(this);
    var sourceblocked = $(event.relatedTarget); // Button that triggered the modal
    var date = sourceblocked.data('date');
    var hotel = sourceblocked.data('hotel');  // Extract info from data-* attributes
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    $.post('<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function/blocked-list-room-toassign.php', { date : date, hotel : hotel }, function(response){
      modal.find('.modal-body').html(response);
		});
  });

  $('#createReservation').on('show.bs.modal', function (event){
    var modal = $(this);
    var sourceblocked = $(event.relatedTarget);
    var date = sourceblocked.data('date');
    var room = sourceblocked.data('room');
    var roomnumber = sourceblocked.data('roomnumber');
    var hotel = sourceblocked.data('hotel');
    $.post('<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function/create-reservation.php', { date : date, room : room, roomnumber : roomnumber, hotel:hotel }, function(response){
      modal.find('.modal-body').html(response);

      $('textarea[name=note]').trumbowyg({
				btns: ['viewHTML',
				  '|', 'btnGrp-design',
				  '|', 'link',
				  '|', 'btnGrp-lists'],
				fullscreenable: false
			});

      showAvailableRoom();
      showPrice();
      $('form#form-create-reservation').validate({
        ignore: [],
    		rules: {
          title: "required",
    			firstname: { required: true, minlength: 2	},
    			lastname: { required: true, minlength: 2 },
    			email: { required: true, email: true },
    			phone: { required: true, minlength: 5 },
    			city: { required: true, minlength: 2 },
    			country: "required"
        },
        messages: {
    			email: "required valid email."
        }
      });

		});
  });

  $('#createReservation, #editBookingRoom').on('focus',"#startdate-today, #enddate-today", function(){
    elem_startdate = $(this).parent().parent().parent().parent().parent().find('input#startdate-today');
    elem_enddate = $(this).parent().parent().parent().parent().parent().find('input#enddate-today');
    elem_startdate.datepicker({
    defaultDate: "+1w",
    changeMonth: true, changeYear:true, minDate:-1, dateFormat: "dd M yy",
    onClose: function( selectedDate ) {
      elem_enddate.datepicker( "option", "minDate", selectedDate );
    }
    });
    elem_enddate.datepicker({
        defaultDate: "+1w",
        changeMonth: true, changeYear:true, minDate:0, dateFormat: "dd M yy",
        onClose: function( selectedDate ) {
          elem_startdate.datepicker( "option", "maxDate", selectedDate );
        }
    });
  });

  $('#createReservation').on('keyup',"input[name=firstname], input[name=lastname]", function(){
    var guestroom = $('input[name=firstname]').val()+" "+$('input[name=lastname]').val();
    $('#createReservation input[name="guest[]"]').each(function(e){
      $(this).val(guestroom);
    });

  });

  $('#createReservation').on('change', 'input[name=checkin], input[name=checkout]', function() {
    showAvailableRoom();
  });

  $('#createReservation').on('change', 'input[name=checkin], input[name=checkout], select[name=roomofferoid], select[name=rooms], select[name=bookingmarket], select[name=agent]', function() {
    showPrice();
  });

  function showAvailableRoom(){
    $.ajax({
      url: "<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function/show-available-room.php",
      type: 'post',
      data: $('form#form-create-reservation').serialize(),
      success: function(response2) {
        $('#createReservation').find('.modal-body').find('select[name=rooms]').html(response2);
      }
    });
  }

  function showPrice(){
    $.ajax({
      url: "<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function/show-price.php",
      type: 'post',
      data: $('form#form-create-reservation').serialize(),
      success: function(response2) {
        rsp = response2.split('|');
        $('#createReservation').find('.modal-body').find('div.show-detail-create-rsv').html(rsp[0]);
        $('#createReservation').find('.modal-body').find('div.button-response').html(rsp[1]);
      }
    });
  }

  $('#editBookingRoom').on('click', 'button[name="recheck-price"]', function() {
    showEditPrice();
  });

  function showEditPrice(){
    $.ajax({
      url: "<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function-edit-rsv/edit-show-price.php",
      type: 'get',
      data: $('form#form-edit-reservation').serialize(),
      success: function(response2){
        rsp = response2.split('|');
        $('#editBookingRoom').find('.modal-body').find('div.show-detail-create-rsv').html(rsp[0]);
        $('#editBookingRoom').find('.modal-body').find('div.button-response').html(rsp[1]);
      }
    });
  }

  $('#viewDetail').on('show.bs.modal', function (event) {
    var modal = $(this);
    var sourceblocked = $(event.relatedTarget);
    var id = sourceblocked.data('id');
    var hotel = sourceblocked.data('hotel');
    $.post('<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function/view-guest.php', { id:id, hotel:hotel }, function(response){
      modal.find('.modal-body').html(response);
		});
  });

  $('#viewDetail').on('click', 'button[name="print"]', function() {
    $('form#reservation-card').submit();
    $('.modal').modal('hide');
  });

  $('body').on('click', 'button[name="view-detail"]', function() {
    $('form#view-reservation-detail').submit();
  });

  $('body').on('click', '#assignRoom button[name="assign-room"]', function(e) {
    bookingroom = $(this).attr("data-bookingroom");
    hotel = $(this).attr("data-hotel");
    e.preventDefault();
    $.ajax({
      url: "<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function/save-assign-room.php",
      type: 'post',
      data: { bookingroom : bookingroom, hotel : hotel },
      success: function(response) {
        if(response == "1"){
          location.reload();
        }else if(response == "<notavailable>"){
          $('.modal').modal('hide');
          $('#informationModal').find('.modal-body').html("Reservation Not Possible. No room available.");
          $('#informationModal').modal('show');
        }
      }
    });
  });

  $('body').on('click', '#viewDetail button[name="pmsstatus"]', function() {
    pmsstatus = $(this).val();
    bookingroom = $('form#form-view-guest').find('input[name="bookingroom"]').val();
    $.post('<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function/save-pmsstatus-guest.php',  { bookingroom : bookingroom, pmsstatus : pmsstatus }, function(response){
      if(response == "1"){
        location.reload();
      }
    });
  });

  $('#createReservation').on('click', 'button[name="next-action"]', function() {
    var pmsstatus = $(this).val();
    if($('form#form-create-reservation').valid()){
      $.post('<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function/save-reservation.php',  $('form#form-create-reservation').serialize() + '&pmsstatus='+pmsstatus, function(response){
        if(response == "1"){
         location.reload();
        }
      });
    }
  });




  $('#editBookingRoom').on('click', 'button[name="next-action"]', function() {
    pmsstatus = $(this).val();
    $.post('<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function-edit-rsv/save-edit-room.php',  $('form#form-edit-reservation').serialize() + '&pmsstatus='+pmsstatus, function(response){
      if(response == "1"){
        location.reload();
      }
    });
  });


  $('body').on('change', '#paymentDetails select[name="type"]', function() {
    var type = $(this).val();
    var boxcc = $('#paymentDetails').find('div.cc');
    var boxbt = $('#paymentDetails').find('div.bank-transfer');

    if(type == "1"){
      $(boxcc).show();
      $(boxbt).hide();
      $('#paymentDetails').find('div.bank-transfer input, div.bank-transfer select').prop("disabled", true);
      $('#paymentDetails').find('div.cc input, div.cc select').prop("disabled", false);
    }else if(type == "2"){
      $(boxcc).hide();
      $(boxbt).show();
      $('#paymentDetails').find('div.bank-transfer input, div.bank-transfer select').prop("disabled", false);
      $('#paymentDetails').find('div.cc input, div.cc select').prop("disabled", true);
    }else{
      $(boxcc).hide();
      $(boxbt).hide();
      $('#paymentDetails').find('div.bank-transfer input, div.bank-transfer select').prop("disabled", true);
      $('#paymentDetails').find('div.cc input, div.cc select').prop("disabled", true);
    }
  });

  $('body').on('click', '#otherCharges button[name="btn-submit"]', function() {
    $.post('<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function-edit-rsv/other-charges-input.php',  $('form#form-add-othercharges').serialize(), function(response){
      if(response == "1"){
        location.reload();
      }
    });
  });

  $('body').on('click', '#paymentDetails button[name="btn-submit"]', function() {
    $.post('<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function-edit-rsv/save-payment-details.php',  $('form#form-add-payment-details').serialize(), function(response){
      if(response == "1"){
        location.reload();
      }
    });
  });

  $('body').on('click', '#specialRequest button[name="btn-submit"]', function() {
    note = $('form#form-special-request').find('textarea[name=note]').val();
    bookingoid = $('form#form-special-request').find('input[name=bookingoid]').val();
    $.post('<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function-edit-rsv/save-edit-special-request.php',  { bookingoid:bookingoid, note:note}, function(response){
      alert(response);
      if(response == "1"){
        //location.reload();
      }
    });
  });

  $('body').on('click', '#arrivalDetails button[name="btn-submit"]', function() {
    $.post('<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function-edit-rsv/save-edit-arrival.php',  $('form#form-arrival').serialize(), function(response){
      if(response == "1"){
        location.reload();
      }
    });
  });

  $('body').on('click', '#ccDetails button[name="btn-submit"]', function() {
    $.post('<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function-edit-rsv/save-credit-card.php',  $('form#form-edit-cc-details').serialize(), function(response){
      if(response == "1"){
        location.reload();
      }
    });
  });

  $('button[act="breakdown"]').click(function(){
    $(this).parent().parent().next('tr.breakdown-rate').slideToggle(0);
    return false;
  });

  $('#editBookingRoom').on('show.bs.modal', function (event) {
    var modal = $(this);
    var sourceblocked = $(event.relatedTarget);
    var brid = sourceblocked.data('brid');
    $.post('<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function-edit-rsv/edit-reservation.php', { brid : brid }, function(response){
      modal.find('.modal-body').html(response);
      showAvailableRoom();
      showPrice();
    });
  });

  $('#confirmationRemoveRoom').on('show.bs.modal', function (event) {
    var modal = $(this);
    var sourceblocked = $(event.relatedTarget);
    var brid = sourceblocked.data('brid');
    $.post('<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function-edit-rsv/confirmation-remove-room.php', { brid : brid }, function(response){
      modal.find('.modal-body').html(response);
    });
  });

  $('#confirmationRemoveRoom').on('click', 'button[name="remove-room"]', function() {
    brid = $(this).attr('data-id');
    $.post('<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function-edit-rsv/save-remove-room.php',  { brid : brid }, function(response){
      if(response == "1"){
        location.reload();
      }
    });
  });

  $('#confirmationRemoveCharges').on('show.bs.modal', function (event) {
    var modal = $(this);
    var sourceblocked = $(event.relatedTarget);
    var bcid = sourceblocked.data('bcid');
    $.post('<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function-edit-rsv/confirmation-remove-charge.php', { bcid : bcid }, function(response){
      modal.find('.modal-body').html(response);
    });
  });

  $('#confirmationRemoveCharges').on('click', 'button[name="remove-charge"]', function() {
    bcid = $(this).attr('data-id');
    $.post('<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function-edit-rsv/save-remove-charge.php',  { bcid : bcid }, function(response){
      if(response == "1"){
        location.reload();
      }
    });
  });

  $('#guestDetails').on('click', 'button[name="btn-submit"]', function() {
      $.post('<?php echo $base_url; ?>/includes/pms-lite/reservation-chart/function-edit-rsv/save-edit-guest.php',  $('form#form-edit-guest').serialize(), function(response){
      if(response == "1"){
        location.reload();
      }
    });
  });

  $('#createReservation').on('change', 'select[name="bookingmarket"]', function() {

    bookingmarket = $(this).val();

    elem = $('#createReservation').find('.modal-body').find('div.booking-market');
    if(bookingmarket == "3"){
      elem.css('display', 'block');
    }else{
      elem.css('display', 'none');
    }
  });

  $('body').on('click', 'button[name="view-invoice"]', function() {
     $('form#invoice-guest').submit();
  });
  $('body').on('click', 'button[name="print-invoice"]', function() {
     $('form#print-invoice-guest').submit();
  });
  $('body').on('click', 'button[name="reservation-edit"]', function() {
     $('form#reservation-edit').submit();
  });

  $('#createReservation').on('keyup, change', 'input[name="firstname"]', function() {
    var firstname = $(this).val();
    var hotel = $('#createReservation').find('input[name=hotel]').val();
    $("input[name='firstname']").autocomplete({
  		source: function( request, response ) {
  			$.ajax({
  				url: "<?php echo "$base_url"; ?>/pms-lite/request/get-guest.php",
  				data: { firstname : firstname, hotel : hotel },
  				success: function(data){
  					response(data);
  				},
  				/*error: function(jqXHR, textStatus, errorThrown){
  					alert(errorThrown);
  				},*/
  			  dataType: 'json'
  			});
      },
  		minLength: 2,
  		select: function( event, ui ) {
        event.preventDefault();
        $("input[name='firstname']").val(ui.item.firstname);
  			$("input[name='lastname']").val(ui.item.lastname);
        $("input[name='email']").val(ui.item.email);
        $("input[name='phone']").val(ui.item.phone);
        $("input[name='city']").val(ui.item.city);
        $("select[name='title']").val(ui.item.title);
        $("select[name='country']").val(ui.item.country);
        $("input[name='cust']").val(ui.item.id);
  		},
      change: function(event, ui) {
        if (ui.item == null) {
          $("input[name='cust']").val(0);
        }
       }
    });
  });

  $(document).ready(function() {
    DesignRsvChart()
  });


  function DesignRsvChart(){
    w_table = $('table#table-reservation-chart').width();
    w_roomnumber = $('table#table-reservation-chart tr.roomnumber').children('td').eq(0).width();
    c_td = parseFloat($('table#table-reservation-chart tr.hotelname td').attr('colspan')) - 1;
    w_td = (w_table - 150) / c_td;
    $('table#table-reservation-chart td:nth-child(n+2)').css('max-width', w_td+'px');
  }

  $('#specialRequest').on('show.bs.modal', function (event){
      $('#specialRequest textarea[name=note]').trumbowyg({
				btns: ['viewHTML',
				  '|', 'btnGrp-design',
				  '|', 'link',
				  '|', 'btnGrp-lists'],
				fullscreenable: false
			});
  });

});
</script>
