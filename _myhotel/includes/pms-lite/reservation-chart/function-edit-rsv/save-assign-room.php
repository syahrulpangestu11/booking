<?php
try{
  session_start();
  error_reporting(E_ALL ^ E_NOTICE);
  include ('../../../../conf/connection.php');
  include ('../../class-pms-lite.php');

  $hoteloid = $_POST['hotel'];
  $bookingroomoid = $_POST['bookingroom'];

  $pmslite = new PMSReservation($db);
  $pmslite->setPMSHotel($hoteloid);
  $data = $pmslite->dataBookingRoom($bookingroomoid);

  $show_list_date = array();
  $list_date = array();
  $list_roomnumber = array();
  $list_roomnumber_daily = array();

  for($n=0; $n<$data['night']; $n++){
    $date = date('Y-m-d', strtotime($data['checkin']. ' +'.$n.' days'));
    array_push($list_date, $date);
    array_push($show_list_date, date('d M Y', strtotime($date)));
    $available_room = $pmslite->loadShowAssignRoom($date, $data['roomoid']);
    if(empty($available_room)){
      echo "<notavailable>";
      die();
    }
    array_push($list_roomnumber,  $available_room);
    foreach($available_room as $ar){
      array_push($list_roomnumber_daily,  $ar['roomnumberoid']);
    }
  }

  $order_room = $pmslite->reorderRoom($list_roomnumber_daily);

  foreach($show_list_date as $key => $date){
      $roomnumberoid = 0;
    foreach($list_roomnumber[$key] as $key2 => $rn){
      if($rn['roomnumberoid'] == $order_room[0]){
        $roomnumberoid = $rn['roomnumberoid'];
        break;
      }
    }
    if(empty($roomnumberoid)){
      $roomnumberoid = $rn['roomnumberoid'];
    }

    $pmslite->assignRoom($bookingroomoid, $roomnumberoid);
  }

  $pmsstatusoid = 2;
  $pmslite->changePMSStatus($bookingroomoid, $pmsstatusoid);

  echo "1";

}catch(Exception $e){
  echo $e->getMessage();
}
?>
