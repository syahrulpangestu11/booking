<div class="modal fade pms-lite-modal" id="otherCharges" tabindex="-1" role="dialog" aria-labelledby="otherCharges">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add other Charges</h4>
      </div>
      <div class="modal-body">
        <form id="form-add-othercharges" method="post">
          <input type="hidden" name="bookingoid" value="<?=$datarsv['bookingoid']?>">
          <div class="row">
            <div class="col-md-3">
              <div class="form-group">
                <label for="inputEmail3">POS</label>
                <select class="form-control" name="typepos">
                  <?php
                  $stmt = $db->prepare("select * from typepos where publishedoid = '1'");
                  $stmt->execute();
                  $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
                  foreach($result as $pos){
                  ?>
                  <option value="<?=$pos['typeposoid']?>" <?=$selected?>><?=$pos['pos']?></option>
                  <?php
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="inputEmail3">Product</label>
                <select name="product" class="form-control">
                  <option value="Transport">Transport</option>
                  <option value="Function Fee">Function Fee</option>
                  <option value="Banjar Fee">Banjar Fee</option>
                  <option value="Organizer Fee">Organizer Fee</option>
                  <option value="Extra Bed">Extra Bed</option>
                  <option value="Baby Cot">Baby Cot</option>
                  <option value="Extra Person">Extra Person</option>
                  <option value="Pool Fence">Pool Fence</option>
                  <option value="Other">Other (will be input manually)</option>
                </select>
                <script>
                $(function(){
                  $('body').on('change', '#otherCharges select[name="product"]', function() {
                    value = $(this).val();
                    if(value == "Other"){
                      $('#otherCharges .manual-product').css('display', 'inline-block');
                    }else{
                      $('#otherCharges .manual-product').css('display', 'none');
                    }
                  });
                  /*
                  $('body').on('change, keyup', '#otherCharges input[name="qty"], #otherCharges input[name="price"]', function() {

                    qty = parseFLoat($('#otherCharges input[name="qty"]').val());alert(qty);
                    price = parseFLoat($('#otherCharges input[name="price"]').val());
                    total = qty * price;

                    ('#otherCharges input[name="total"]').val(total);
                  });
                  */
                });
                </script>
              </div>
            </div>
            <div class="col-md-4 manual-product" style="display:none;">
              <div class="form-group">
                <label for="inputEmail3">Manually Input Product</label>
                <input type="text" class="form-control" name="product1">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-1">
              <div class="form-group">
                <label for="inputEmail3">Qty</label><input type="text" class="form-control" name="qty" value="1">
              </div>
            </div>
            <div class="col-md-1">
              <div class="form-group">
                <label for="inputEmail3">Currency</label>
                <select name="currency" class="form-control input-sm">
                <?php
                $stmt = $db->prepare("select * from currency where publishedoid = '1'");
                $stmt->execute();
                $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
                foreach($result as $currency){
                ?>
                <option value="<?=$currency['currencyoid']?>" <?=$selected?>><?=$currency['currencycode']?></option>
                <?php
                }
                ?>
                </select>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label for="inputEmail3">Price</label><input type="text" class="form-control" name="price">
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label for="inputEmail3">Total</label><input type="text" class="form-control" name="total">
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" name="btn-submit" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade pms-lite-modal" id="ccDetails" tabindex="-1" role="dialog" aria-labelledby="ccDetails">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Credit Card Details</h4>
      </div>
      <div class="modal-body">
        <form id="form-edit-cc-details" method="post">
          <input type="hidden" name="bookingpayment" value="<?=$datarsv['paymentoid']?>">
          <input type="hidden" name="bookingoid" value="<?=$datarsv['bookingoid']?>">

          <div class="row">
            <div class="col-md-2">
              <div class="form-group">
                <label for="inputEmail3">Credit Card</label>
                <select name="card" class="form-control input-sm">
                <?php
                $stmt = $db->prepare("select * from creditcard where publishedoid = '1'");
                $stmt->execute();
                $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
                foreach($result as $currency){
                ?>
                <option value="<?=$currency['cardoid']?>" <?=$selected?>><?=$currency['cardname']?></option>
                <?php
                }
                ?>
                </select>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label for="inputEmail3">Card Number</label><input type="text" class="form-control" name="cardnumber">
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label for="inputEmail3">Card Holder</label><input type="text" class="form-control" name="cardholder">
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label for="inputEmail3">Expired Month</label>
                <select name="expmonth" class="form-control">
                  <?php
                      $monthNumber	= array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');
                      $monthName		= array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
                      foreach($monthNumber as $key => $value){
                          $selected = '';
                          if($monthName[$key] == date('F')){ $selected = 'selected'; }
                  ?>
                      <option value="<?=$monthNumber[$key]?>" <?=$selected?>><?=$monthName[$key]?></option>
                  <?php
                      }
                  ?>
                  </select>
                </div>
              </div>
              <div class="col-md-1">
                <div class="form-group">
                  <label for="inputEmail3">Expired Year</label>
                  <select name="expyear" class="form-control">
                  <?php for($y = date('Y'); $y <= date('Y')+10; $y++){ ?>
                  <option value="<?=$y?>"><?=$y?></option>
                  <?php } ?>
                  </select>
                </div>
              </div>
              <div class="col-md-1">
                <div class="form-group">
                  <label for="inputEmail3">CVC</label><input type="text" class="form-control" name="cvc">
                </div>
              </div>
          </div>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" name="btn-submit" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade pms-lite-modal" id="editBookingRoom" tabindex="-1" role="dialog" aria-labelledby="editRoom">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Room Detail</h4>
      </div>
      <div class="modal-body">
        please wait ...
      </div>
    </div>
  </div>
</div>

<div class="modal fade pms-lite-modal" id="confirmationRemoveRoom" tabindex="-1" role="dialog" aria-labelledby="confirmationRemoveRoom">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Remove Room Detail</h4>
      </div>
      <div class="modal-body">
        please wait ...
      </div>
    </div>
  </div>
</div>

<div class="modal fade pms-lite-modal" id="confirmationRemoveCharges" tabindex="-1" role="dialog" aria-labelledby="confirmationRemoveCharges">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Remove Item Charges</h4>
      </div>
      <div class="modal-body">
        please wait ...
      </div>
    </div>
  </div>
</div>

<div class="modal fade pms-lite-modal" id="confirmationRemovePayment" tabindex="-1" role="dialog" aria-labelledby="confirmationRemovePayment">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Remove Payment</h4>
      </div>
      <div class="modal-body">
        please wait ...
      </div>
    </div>
  </div>
</div>

<div class="modal fade pms-lite-modal" id="guestDetails" tabindex="-1" role="dialog" aria-labelledby="guestDetails">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Guest Details</h4>
      </div>
      <div class="modal-body">
        <form id="form-edit-guest" class="form-horizontal" method="post">
          <input type="hidden" name="custoid" value="<?=$datarsv['custoid']?>">
          <div class="form-group">
            <label for="" class="col-sm-2 control-label">Title</label>
            <div class="col-sm-1">
                <select name="title" class="form-control input-sm">
                <?php
                $stmt = $db->prepare("select * from title");
                $stmt->execute();
                $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
                foreach($result as $title){
                  if($title['titleoid'] == $datarsv['title']){ $selected = "selected"; }else{ $selected = "";  }
                ?>
                <option value="<?=$title['titleoid']?>" <?=$selected?>><?=$title['name']?></option>
                <?php
                }
                ?>
                </select>
            </div>
          </div>
          <div class="form-group">
            <label for="" class="col-sm-2 control-label">First Name <span class="mandatory">*</span></label>
            <div class="col-sm-4">
                <input type="text" name="firstname" class="form-control input-sm" value="<?=$datarsv['firstname']?>"  placeholder="Firstname">
            </div>
            <label for="" class="col-sm-2 control-label">Last Name <span class="mandatory">*</span></label>
            <div class="col-sm-4">
                <input type="text" name="lastname" class="form-control input-sm" value="<?=$datarsv['lastname']?>"  placeholder="Lastname">
            </div>
          </div>
          <div class="form-group">
            <label for="" class="col-sm-2 control-label">Phone <span class="mandatory">*</span></label>
            <div class="col-sm-4">
              <input type="text" name="phone" class="form-control input-sm" placeholder="" value="<?=$datarsv['phone']?>">
            </div>
            <label for="" class="col-sm-2 control-label">Fax</label>
            <div class="col-sm-4">
              <input type="text" name="fax"  class="form-control input-sm" placeholder="" value="<?=$datarsv['fax']?>">
            </div>
          </div>
          <div class="form-group">
            <label for="" class="col-sm-2 control-label">Mobile</label>
            <div class="col-sm-4">
              <input type="text" name="mobile" class="form-control input-sm" placeholder=""  value="<?=$datarsv['mobilephone']?>">
            </div>
            <label for="" class="col-sm-2 control-label">Email <span class="mandatory">*</span></label>
            <div class="col-sm-4">
              <input type="text" name="email" class="form-control input-sm" placeholder="" value="<?=$datarsv['email']?>">
            </div>
          </div>
          <div class="form-group">
            <label for="" class="col-sm-2 control-label">Address</label>
            <div class="col-sm-4">
              <textarea name="address" class="form-control input-sm"><?=$datarsv['address']?></textarea>
            </div>
          </div>
          <div class="form-group">
            <label for="" class="col-sm-2 control-label">Country</label>
            <div class="col-sm-4">
              <select name="country" class="form-control">
                <option>Select</option>
                  <?php
            $syntax_country = "select countryoid, countryname from country";
            $stmt	= $db->query($syntax_country);
            $result_country = $stmt->fetchAll(PDO::FETCH_ASSOC);
            foreach($result_country as $country){
              if($country['countryoid'] == $datarsv['countryoid']){ $selected = "selected"; }else{ $selected = ""; }
            ?>
              <option value="<?=$country['countryoid']?>" <?=$selected?>><?=$country['countryname']?></option>
            <?php
            }
            ?>
              </select>
            </div>
            <label for="" class="col-sm-2 control-label">State</label>
            <div class="col-sm-4">
              <input type="text" name="state" class="form-control input-sm" placeholder=""  value="<?=$datarsv['state']?>">
            </div>
          </div>
          <div class="form-group">
            <label for="" class="col-sm-2 control-label">City <span class="mandatory">*</span></label>
            <div class="col-sm-4">
              <input type="text" name="city" class="form-control input-sm" placeholder=""  value="<?=$datarsv['city']?>">
            </div>
            <label for="" class="col-sm-2 control-label">Zip Code</label>
            <div class="col-sm-4">
              <input type="text" name="zipcode" class="form-control input-sm" placeholder="" value="<?=$datarsv['zipcode']?>">
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" name="btn-submit" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade pms-lite-modal" id="emailInvoiceGuest" tabindex="-1" role="dialog" aria-labelledby="emailInvoiceGuest">
  <div class="modal-dialog" role="document">
    <form id="form-send-invoice-guest" method="post" action="<?=$base_url?>/pms-lite/send-invoice-guest">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Send Invoice Guest</h4>
        </div>
        <div class="modal-body">
          <input type="hidden" name="bookingnumber" value="<?=$datarsv['bookingnumber']?>">
          <div class="form-group">
            <div class="row">
              <label class="col-xs-3 text-right">Send Invoice to </label>
              <div class="col-xs-5">
                <input type="text" name="email" value="<?=$datarsv['email']?>" class="form-control col-xs-6" required>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" name="btn-submit" class="btn btn-primary">Send Email</button>
        </div>
      </div>
    </form>
  </div>
</div>

<div class="modal fade pms-lite-modal" id="emailInvoiceAgent" tabindex="-1" role="dialog" aria-labelledby="emailInvoiceAgent">
  <div class="modal-dialog" role="document">
    <form id="form-send-invoice-agent" method="post" action="<?=$base_url?>/pms-lite/send-invoice-agent">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Send Invoice Guest</h4>
        </div>
        <div class="modal-body">
          <input type="hidden" name="bookingnumber" value="<?=$datarsv['bookingnumber']?>">
          <div class="form-group">
            <div class="row">
              <label class="col-xs-3 text-right">Send Invoice to </label>
              <div class="col-xs-5">
                <input type="text" name="email" value="<?=$datarsv['email']?>" class="form-control col-xs-6" required>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" name="btn-submit" class="btn btn-primary">Send Email</button>
        </div>
      </div>
    </form>
  </div>
</div>

<div class="modal fade pms-lite-modal" id="emailConfirmation" tabindex="-1" role="dialog" aria-labelledby="emailConfirmation">
  <div class="modal-dialog" role="document">
    <form id="form-send-confirmation-guest" method="post" action="<?=$base_url?>/pms-lite/send-confirmation-guest">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Send Confirmation Email to Guest</h4>
        </div>
        <div class="modal-body">
          <input type="hidden" name="bookingnumber" value="<?=$datarsv['bookingnumber']?>">
          <div class="form-group">
            <div class="row">
              <label class="col-xs-3 text-right">Send Email to </label>
              <div class="col-xs-5">
                <input type="text" name="email" value="<?=$datarsv['email']?>" class="form-control col-xs-6" required>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" name="btn-submit" class="btn btn-primary">Send Email</button>
        </div>
      </div>
    </form>
  </div>
</div>

<div class="modal fade pms-lite-modal" id="specialRequest" tabindex="-1" role="dialog" aria-labelledby="specialRequest">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Sales / Guest Request</h4>
      </div>
      <div class="modal-body">
        <form id="form-special-request" method="post">
          <input type="hidden" name="bookingoid" value="<?=$datarsv['bookingoid']?>">
          <div class="row">
            <div class="col-md-12">
              <textarea name="note"><?=$datarsv['note']?></textarea>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" name="btn-submit" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade pms-lite-modal" id="specialRequest" tabindex="-1" role="dialog" aria-labelledby="specialRequest">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Sales / Guest Request</h4>
      </div>
      <div class="modal-body">
        <form id="form-special-request" method="post">
          <input type="hidden" name="bookingoid" value="<?=$datarsv['bookingoid']?>">
          <div class="row">
            <div class="col-md-12">
              <textarea name="note"><?=$datarsv['note']?></textarea>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" name="btn-submit" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade pms-lite-modal" id="arrivalDetails" tabindex="-1" role="dialog" aria-labelledby="arrivalDetails">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Arrival Details</h4>
      </div>
      <div class="modal-body">
        <form id="form-arrival" method="post">
          <input type="hidden" name="bookingoid" value="<?=$datarsv['bookingoid']?>">
          <div class="row">
            <div class="col-md-2">
              <div class="form-group">
                <label for="inputEmail3">Arrival Flight</label><input type="text" class="form-control" name="arrivalflight">
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label for="inputEmail3">Arrival Date</label><input type="text" class="form-control" name="arrivaldate" id="startdate">
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label for="inputEmail3">Arrival Time</label><input type="time" class="form-control" name="arrivaltime">
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" name="btn-submit" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<?php include('includes/pms-lite/reservation-chart/function-edit-rsv/modal/modal-payment.php'); ?>

<div class="modal fade pms-lite-modal" id="sourceDetails" tabindex="-1" role="dialog" aria-labelledby="sourceDetails">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Source Details</h4>
      </div>
      <div class="modal-body">
        <form id="form-edit-source-details" class="form-horizontal" method="post">
          <input type="hidden" name="bookingoid" value="<?=$datarsv['bookingoid']?>">
          <div class="form-group">
            <div class="col-sm-4">
              <label for="" class="control-label">Booking Channel</label>
              <select name="bookingchannel" class="form-control">
                  <?php
                  $stmt = $db->prepare("select * from bookingchannel where publishedoid = '1'");
                  $stmt->execute();
                  $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
                  foreach($result as $bc){
                      if($bc['bookingchanneloid'] == $datarsv['bookingchanneloid']){ $selected = "selected"; }else{ $selected = "";  }
                  ?>
                  <option value="<?=$bc['bookingchanneloid']?>" <?=$selected?>><?=$bc['bc_name']?></option>
                  <?php
                  }
                  ?>
              </select>
            </div>
            <div class="col-sm-4">
              <label for="" class="control-label">Booking Channel</label>
              <select name="bookingmarket" class="form-control">
                <?php
                $stmt = $db->prepare("select * from bookingmarket where publishedoid = '1'");
                $stmt->execute();
                $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
                foreach($result as $bm){
                  if($bm['bookingmarketoid'] == $datarsv['bookingmarketoid']){ $selected = "selected"; }else{ $selected=""; }
                ?>
                <option value="<?=$bm['bookingmarketoid']?>" <?=$selected?>><?=$bm['bm_name']?></option>
                <?php
                }
                ?>
              </select>
            </div>
            <div class="col-sm-4">
              <label for="" class="control-label">Agent</label>
              <select name="agent" class="form-control">
                <option value="0">No Agent</option>
                <?php
                $stmt = $db->prepare("select a.agentoid, a.agentname from agent a where a.publishedoid = 1 order by a.agentname");
                $stmt->execute();
                $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
                foreach($result as $agent){
                    if($agent['agentoid'] == $datarsv['agentoid']){ $selected = "selected"; }else{ $selected=""; }
                ?>
                <option value="<?=$agent['agentoid']?>" <?=$selected?>><?=$agent['agentname']?></option>
                <?php
                }
                ?>
              </select>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" name="btn-submit" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
