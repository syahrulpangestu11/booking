<?php
    $stmt = $db->prepare("SELECT `invnolabel`, `notefax`, `noteemail` FROM `pmsinvoicesetting` WHERE `hoteloid`=:a");
    $stmt->execute(array(':a' => $hoteloid));
    $invset = $stmt->fetch(PDO::FETCH_ASSOC);
    $invnolabel = $invset['invnolabel'];
    $notefax = $invset['notefax'];
    $noteemail = $invset['noteemail'];
?>
<section class="content-header">
    <h1>
        PMS Settings
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-laptop"></i> PMS Lite</a></li>
        <li class="active">PMS Settings</li>
    </ol>
</section>
<section class="content" id="basic-info">
	<div class="row">
        <form method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/pms-lite/settings/saveinvoice">
        <input type="hidden" name="hoteloid" value="<?php echo $hoteloid; ?>">
        <div class="box box-form">
	        <div class="box-header with-border">
            	<h1>Invoice Settings</h1>
            </div>
            <div id="data-box" class="box-body">
            <ul class="inline form-input">
                <li>
                    <div class="side-left">
                        <label style="display:block">Invoice Number Label</label>
                        <input type="text" name="invnolabel" class="input-text" value="<?php echo $invnolabel; ?>">
                        <br><i style="font-size:80%">Exp. 0001/####-0001/04/2018, This label is placed in ####</i>
                    </div>
                    <div class="side-right"></div>
                    <div class="clear"></div>
                </li>
                <li>
                    <div class="side-left">
                        <label style="display:block">Note Fax</label>
                        <input type="text" name="notefax" class="input-text" value="<?php echo $notefax; ?>">
                    </div>
                    <div class="side-right"></div>
                    <div class="clear"></div>
                </li>
                <li>
                    <div class="side-left">
                        <label style="display:block">Note Email</label>
                        <input type="text" name="noteemail" class="input-text" value="<?php echo $noteemail; ?>">
                    </div>
                    <div class="side-right"></div>
                    <div class="clear"></div>
                </li>
            </ul>
            </div>
            <div class="box-footer" align="left">
				<button type="submit" class="small-button blue add-button">Save</button>
                <button type="reset" class="small-button blue">Reset</button>
            </div>
        </div>
        </form>
    </div>
</section>