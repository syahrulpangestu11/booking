<?php
	ob_start();
	  include 'mail-content/invoice-guest.php';
	$body = ob_get_clean();
	$subject = "Invoice ".$hotelname." - ".$bookingnumber;

	require_once('includes/mailer/class.phpmailer.php');
	$mail = new PHPMailer(true);

	$mail->IsSMTP(); // telling the class to use SMTP

	try{

	$mail->SMTPDebug = 0;
	//Ask for HTML-friendly debug output
	$mail->Debugoutput = 'html';
	//Set the hostname of the mail server
	$mail->Host = "smtp.gmail.com";
	//Set the SMTP port number - likely to be 25, 465 or 587
	$mail->SMTPSecure = 'tls';
	$mail->Port = 587;
	//Whether to use SMTP authentication
	$mail->SMTPAuth = true;
	//Username to use for SMTP authentication
	$mail->Username = "pms@thebuking.com";//"info@thebuking.com";
	//Password to use for SMTP authentication
	$mail->Password = "Res3!4nd";//"KofiEn4k";

    $syntax_mail_address = "SELECT * FROM mailer INNER JOIN mailertype USING (mailertypeoid) INNER JOIN mailermail USING (mailermailoid) WHERE mailerpage = 'booking-guest' AND status = '1'";
    $stmt	= $db->query($syntax_mail_address);
    $result_email = $stmt->fetchAll(PDO::FETCH_ASSOC);
    foreach($result_email as $email_list){
  		switch($email_list['mailertypeoid']){
  		  case 1 : $mail->SetFrom($email_list['email'], $email_list['name']); break;
  		  case 2 : $mail->AddAddress($email_list['email'], $email_list['name']); break;
  		  case 3 : $mail->AddCC($email_list['email'], $email_list['name']); break;
  		  case 4 : $mail->AddBCC($email_list['email'], $email_list['name']); break;
  		  case 5 : $mail->AddReplyTo($email_list['email'], $email_list['name']); break;
  		}
    }
    /*
    foreach($_POST['mailingtype'] as $key => $type){
      switch($type){
  		  case 'to'   : $mail->AddAddress($_POST['email'][$key], $_POST['name'][$key]); break;
  		  case 'cc'   : $mail->AddCC($_POST['email'][$key], $_POST['name'][$key]); break;
  		  case 'bcc'  : $mail->AddBCC($_POST['email'][$key], $_POST['name'][$key]); break;
  		}
    }
    */
    $mail->AddAddress($_POST['email'], $_POST['email']);

	  $mail->Subject = $subject;
	  $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
	  $mail->MsgHTML($body);

	  $mail->Send();

?>
		<script type="text/javascript">
			$(document).ready(function(e) {
				$('#notificationModal').find('.modal-body').html('Email has been sent.');
				$('#notificationModal').modal('show');
			});
		</script>

<?php

	}catch (phpmailerException $e) {

		?>
				<script type="text/javascript">
					$(document).ready(function(e) {
						$('#notificationModal').find('.modal-body').html('Sorry we cant re-send your email right now. Please make sure you already give right email address.');
						$('#notificationModal').modal('show');
					});
				</script>

		<?php

	}catch (Exception $e) {

		?>
				<script type="text/javascript">
					$(document).ready(function(e) {
						$('#notificationModal').find('.modal-body').html('Sorry we cant re-send your email right now. Please make sure you already give right email address.');
						$('#notificationModal').modal('show');
					});
				</script>

		<?php

	}

?>
<div class="modal fade" id="notificationModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="fa fa-info-circle"></i> Notifiction</h4>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
        <button type="button" name="back" class="btn btn-primary"  data-dismiss="modal">OK</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<form id="reservation-edit" action="<?=$base_url?>/pms-lite/reservation-detail" method="post">
  <input name="bookingnumber" type="hidden" value="<?=$datarsv['bookingnumber']?>">
  <input name="hotel" type="hidden" value="<?=$hoteloid?>">
</form>

<script type="text/javascript">
	$(document).ready(function(e) {
		$('#notificationModal').on('hidden.bs.modal', function (e) {
		  $('form#reservation-edit').submit();
		})
	});
</script>
