<script type="text/javascript">
$(function(){
  $('#changeHKStatus').on('show.bs.modal', function (event){
    var modal = $(this);
    var sourceblocked = $(event.relatedTarget); // Button that triggered the modal
    var roomnumber = sourceblocked.data('rn');

    var date = sourceblocked.data('date');
    var hotel = sourceblocked.data('hotel');  // Extract info from data-* attributes
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    $.post('<?php echo $base_url; ?>/includes/pms-lite/housekeeping/function/modal-housekeeping-status.php', { date : date, hotel : hotel, roomnumber : roomnumber  }, function (response){
      modal.find('.modal-body').html(response);
		});
  });

  $('body').on('click', '#changeHKStatus button[name="btn-submit"]', function() {
    $.post('<?php echo $base_url; ?>/includes/pms-lite/housekeeping/function/save-housekeeping-status.php',  $('form#form-change-housekeepingstatus').serialize(), function(response){
      if(response == "1"){
        location.reload();
      }
    });
  });

  $("input[name='hotelname']").autocomplete({
    source: function( request, response ) {
      $.ajax({
        url: "<?php echo "$base_url"; ?>/pms-lite/request/get-hotel.php",
        dataType: 'json',
        data: { hotelname : request.term },
        success: function(data){
          response(data);
        },

      });
    },
    minLength: 2,
    select: function( event, ui ) {
      event.preventDefault();
      $("input[name='hotelname']").val(ui.item.value);
      $("input[name='zhotel']").val(ui.item.id);
    },
    change: function(event, ui) {
      if (ui.item == null) {
        $("input[name='zhotel']").val('');
      }
     }
  });

});
</script>
