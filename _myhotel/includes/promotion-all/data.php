<!-- <button type="button" class="pure-button blue add"><i class="fa fa-gift"></i>Create New Promotion</button> <button type="button" class="pure-button green template"><i class="fa fa-gift"></i>Create from Template</button> -->
&nbsp;&nbsp;&nbsp;
<span class="status-green square">sts</span> Active &nbsp;&nbsp;&nbsp; <span class="status-black square">sts</span> Inactive &nbsp;&nbsp;&nbsp; <span class="status-red square">sts</span> Expired

<?php

	include("../ajax-include-file.php");
	
	$datenow = date("Y-m-d");
	$range = $_REQUEST['range'];
	$hoteloid = $_REQUEST['hoteloid'];
	$hcode = $_REQUEST['hcode'];
	$roomoffer = $_REQUEST['rt'];
	$channeloid = $_REQUEST['ch'];
	$startdate = date("Y-m-d", strtotime($_REQUEST['sdate']));
	$enddate = date("Y-m-d", strtotime($_REQUEST['edate']));

	function generateDirectPromotion($name, $minstay, $min_bef_ci, $hcode){
		// global $hcode;
		$link = 'https://thebuking.com/ibe/index.php?hcode='.$hcode.'&night='.$minstay.'&promotion='.$name.'&requirementcheckin='.$min_bef_ci.'&show=promo_only';
		return $link;
	}
	if($hoteloid!=0){
		$filter_hotel = " AND h.hoteloid = '".$hoteloid."' ";
	}else{
		$filter_hotel = "";
	}
	
	if(!empty($_REQUEST['sdate']) and empty($_REQUEST['edate'])){
		$startdate = date("Y-m-d", strtotime($_REQUEST['sdate']));
		
		if($range == "sale"){
			$filter_periode = "and (salefrom <= '".$startdate."' and saleto >= '".$startdate."')";
		}else{
			$filter_periode = "and (bookfrom <= '".$startdate."' and bookto >= '".$startdate."')";
		}
	}else if(empty($_REQUEST['sdate']) and !empty($_REQUEST['edate'])){
		$startdate = date("Y-m-d", strtotime($_REQUEST['sdate']));
		
		if($range == "sale"){
			$filter_periode = "and (salefrom <= '".$enddate."' and saleto >= '".$enddate."')";
		}else{
			$filter_periode = "and (bookfrom <= '".$enddate."' and bookto >= '".$enddate."')";
		}
	}else if(!empty($_REQUEST['sdate']) and !empty($_REQUEST['edate'])){
		if($range == "sale"){
			$filter_periode = "and 
		( 
			(salefrom <= '".$startdate."' and (saleto >= '".$startdate."' and saleto <= '".$enddate."'))  
			or
			((salefrom >= '".$startdate."' and salefrom <= '".$enddate."') and saleto >= '".$enddate."')
			or
			((salefrom >= '".$startdate."' and salefrom <= '".$enddate."') and saleto <= '".$enddate."')
			or
			(salefrom <= '".$startdate."' and saleto >= '".$enddate."')
			or
			(salefrom >= '".$startdate."' and saleto <= '".$enddate."')
		)
			";
		}else{
			$filter_periode = " and 
		( 
			(bookfrom <= '".$startdate."' and (bookto >= '".$startdate."' and bookto <= '".$enddate."'))  
			or
			((bookfrom >= '".$startdate."' and bookfrom <= '".$enddate."') and bookto >= '".$enddate."')
			or
			((bookfrom >= '".$startdate."' and bookfrom <= '".$enddate."') and bookto <= '".$enddate."')
			or
			(bookfrom <= '".$startdate."' and bookto >= '".$enddate."')
			or
			(bookfrom >= '".$startdate."' and bookto <= '".$enddate."')
		)
			";
		}
	}else{
		$filter_periode = "";
	}
	
	$filter_channel = "";

	if(!empty($roomoffer)){
		$filter_roomoffer = "and pa.roomofferoid = '".$roomoffer."'";
		$join_room = "inner join roomoffer ro using (roomofferoid) inner join room r using (roomoid) inner join channel ch using (channeloid)";
		if(!empty($channeloid)){
			$filter_channel = "and ch.channeloid = '".$channeloid."'";
		}
	}else{
		$filter_roomoffer = "";
		$join_room = "";
	}

	try {
		$sql_promo = "
		SELECT 'promotion' as promotype, p.promotionoid, p.name, p.salefrom, p.saleto, p.discountvalue, p.priority, p.promoimage, p.description, p.servicefacilities, pt.type, dt.labelquestion, p.minstay, p.min_bef_ci, (case when p.publishedoid = 1 and p.saleto < '".$datenow."' then 'status-red' when p.publishedoid = 1 then 'status-green' else 'status-black' end) as status, p.publishedoid, pb.note AS published_note, h.hotelcode, h.hotelname from promotion p left join promotiontype pt using (promotiontypeoid) inner join discounttype dt using (discounttypeoid) inner join hotel h using (hoteloid) left join promotionapply pa using (promotionoid) ".$join_room." left join published pb ON (p.publishedoid = pb.publishedoid) where p.publishedoid not in (3) ".$filter_hotel."  ".$filter_roomoffer." ".$filter_channel." ".$filter_periode." group by p.promotionoid
		ORDER BY p.salefrom DESC, p.publishedoid DESC, p.promotionoid DESC
		";
		// echo "<pre>".$sql_promo."</pre>";
		$stmt = $db->query($sql_promo);
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
?>
<table class="table promo-table">
    <tr align="left">
		<td>No</td>
		<td>Picture</td>
		<td>Hotel</td>
		<td>Promotion Name</td>
		<td>Sale Date</td>
        <td>Description</td>
        <td>Inclusive</td>
		<td>URL</td>
    </tr>	
<?php
			$r_promo = $stmt->fetchAll(PDO::FETCH_ASSOC);
			$nomer = 0;
			foreach($r_promo as $row){
				$nomer++;
				$hcode = $row['hotelcode'];
				$salefrom = date("d/M/Y", strtotime($row['salefrom']));
				$expirydate = date("d/M/Y", strtotime($row['saleto']));
				// $promo_name = htmlspecialchars($row['name']);
				$promo_name = urlencode($row['name']);
				$promo_night = ($row['minstay'] < 2) ? 2 : $row['minstay'] ;
				if($row['status']=="status-green"){
					$published_class = " blue ";
					$published_note = $row['published_note'];
				}else if($row['status']=="status-red"){
					$published_class = " red ";
					$published_note = "expired";
				}else{ 
					$published_class = " grey " ;
					$published_note = $row['published_note'];
				}

				if(!empty($row['description'])){
					$description = "<br><b>Description : </b><br>".$row['description'];
				}else{
					$description = "";
				}
?>
    <tr class="<?php echo $row['status']; ?>">
		<td class="<?php echo $row['status']; ?>"><?=$nomer;?></td>
		<td><img src="<?=$row['promoimage'];?>" width="70" alt=""></td>
		<td><?=$row['hotelname'];?></td>
        <td><?=$row['name'];?></td>
		<td><?=$salefrom." - ".$expirydate;?></td>
        <td>
		<b>Discount : </b>
		<br>
		<?php echo number_format($row['discountvalue'])." ".$row['labelquestion']; ?>
		<br>
		<b>Min. Stay :</b> <?php echo $row['minstay']; ?> night
		<?=$description;?></td>
		<td><?=($row['servicefacilities']);?></td>
        <td class="algn-right">
		<?php /*/ ?>
        <button type="button" class="pure-button green single edit" pid="<?php echo $row['promotionoid']; ?>"><i class="fa fa-pencil"></i></button>
        <button type="button" class="pure-button red single delete" pid="<?php echo $row['promotionoid']; ?>"><i class="fa fa-trash"></i></button>
		<?php //*/ ?>
		<button type="button" class="pure-button single change-published <?=$published_class;?>" pid="<?=$row['promotionoid']; ?>" current-publishedoid="<?=$row['publishedoid'];?>" title="Click to Change Status">
		<?=$published_note;?></button>
		<button type="button" class="pure-button orange single copy-link-list" name="<?=$promo_name;?>" night="<?=$promo_night;?>" title="Copy Promotion Link" min_bef_ci="<?=$row['min_bef_ci']?>" link="<?=generateDirectPromotion($promo_name, $promo_night, $row['min_bef_ci'], $hcode);?>"><i class="fa fa-link"></i></button>
        </td>
    </tr>	
<?php				
			}
?>
</table>
<?php
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		print($ex);
		die();
	}
?>