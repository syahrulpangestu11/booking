<?php
    include("function_report.php");
?>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
<script>
    $(document).ready(function() {
        $('#example').children('tbody').css('display', '');
        
        var table = $('#example').DataTable( {
            dom: 'B lfrtip',
            columnDefs: [
                {
                    targets: 1,
                    className: 'noVis'
                }
            ],
            buttons: [
                {
                    extend: 'colvis', text: 'Column Visibility',
                    columns: ':not(.noVis)'
                }
            ]
        } );
        
        $('html, body, .content-side').css('min-height', '');
        $('#xload').css('display', 'none');
        
        var bdall = $('input[name="bdall"]:checked').val();
        var ciall = $('input[name="ciall"]:checked').val();
        
        $.fn.dataTable.ext.search.push(
            function( settings, data, dataIndex ) {
                var rt = $("#selrt option:selected").text();
                var rtv = $("#selrt option:selected").val();
                var room = data[7] || "/";
                
                var stat = $("#selstat option:selected").text();
                var statv = $("#selstat option:selected").val();
                var sts = data[9] || "";
                
                var mark = $("#selmark option:selected").text();
                var markv = $("#selmark option:selected").val();
                var mrk = data[11] || "";
                
                var agt = $("#selagent option:selected").text();
                var agtv = $("#selagent option:selected").val();
                var agtn = data[12] || "";
                
                var from = new Date($("#startdate1").val());
                var to = new Date($("#enddate1").val());
                var current = new Date(data[2]) || "";
                
                var cifrom = new Date($("#startdate2").val());
                var cito = new Date($("#enddate2").val());
                var cicurrent = new Date(table.row(dataIndex).nodes().to$().find('td.acin').attr('x')) || "";
                
                var agty = table.row(dataIndex).nodes().to$().find('td.agt').attr('x');
                
                var x = room.split("/");
                if ((rt === x[1] || rtv === "") && (stat === sts || statv === "") && (mark === mrk || markv === "") && (agty == agtv || agtv === "")) {
                    if(bdall == '1'){
                        if(ciall == '1'){
                            if((current >= from && current <= to) && (cicurrent >= cifrom && cicurrent <= cito)){
                                return true;
                            }
                        }else{
                            if((current >= from && current <= to)){
                                return true;
                            }
                        }
                    }else{
                        if(ciall == '1'){
                            if((cicurrent >= cifrom && cicurrent <= cito)){
                                return true;
                            }
                        }else{
                            return true;
                        }
                    }
                }
                return false;
            }
        );
        
        /*$('#selrt, #selstat, #selmark, #selagent').change(function() {
            table.draw();
        });*/
        
        $('#btfilter').click(function(){
            bdall = $('input[name="bdall"]:checked').val();
            ciall = $('input[name="ciall"]:checked').val(); console.log(bdall+"-"+ciall);
            
            $('#example').children('tbody').css('display', 'none');
            $('#xload').css('display', '');
            setTimeout(function(){ 
                table.draw();
                $('#example').children('tbody').css('display', '');
                $('#xload').css('display', 'none');
            }, 3000);
        });
        
        $( "#startdate1" ).datepicker({
    		defaultDate: "0",
    		dateFormat: "dd M y",
    		changeMonth: true, changeYear:true,
    		onClose: function( selectedDate ) {
                $( "#enddate1" ).datepicker( "option", "minDate", selectedDate );
    		},
    		onSelect: function(dateText) {
                //table.draw();
    		}
        });
        $( "#enddate1" ).datepicker({
          	defaultDate: "0",
    		dateFormat: "dd M y",
          	changeMonth: true, changeYear:true,
         	onClose: function( selectedDate ) {
            	$( "#startdate1" ).datepicker( "option", "maxDate", selectedDate );
          	},
    		onSelect: function(dateText) {
                //table.draw();
    		}
        });
        
        $( "#startdate2" ).datepicker({
    		defaultDate: "0",
    		dateFormat: "dd M y",
    		changeMonth: true, changeYear:true,
    		onClose: function( selectedDate ) {
                $( "#enddate2" ).datepicker( "option", "minDate", selectedDate );
    		},
    		onSelect: function(dateText) {
                //table.draw();
    		}
        });
        $( "#enddate2" ).datepicker({
          	defaultDate: "0",
    		dateFormat: "dd M y",
          	changeMonth: true, changeYear:true,
         	onClose: function( selectedDate ) {
            	$( "#startdate2" ).datepicker( "option", "maxDate", selectedDate );
          	},
    		onSelect: function(dateText) {
                //table.draw();
    		}
        });
        
        $('#example').on('click', 'a.print', function() {
            $(this).parent().children('form.reservation-card').submit();
        });
        $('#example').on('click', 'a.detail', function() {
            $(this).parent().parent().find('form.view-reservation-detail').submit();
        });
        $('#btexport').click(function(){
            $('#exporttoexcel').submit();
        });
        $('input[name="bdall"]').change(function(){
            var n = $(this).val();
            if(n == '1'){
                $('#bdall').css('display', 'inline-block');
            }else{
                $('#bdall').css('display', 'none');
            }
        });
        $('input[name="ciall"]').change(function(){
            var n = $(this).val();
            if(n == '1'){
                $('#ciall').css('display', 'inline-block');
            }else{
                $('#ciall').css('display', 'none');
            }
        });
    });
</script>
<style>
    .content .form-group label {
        padding-right:10px;
    }
    .content .form-group label, .content .form-group select, .content .form-group input {
        margin-bottom:5px;
    }
    a.detail {
        color: #1200ff;
    }
    a.detail:hover {
        text-decoration: underline;
    }
    .btn {
        border-radius: 3px;
        -webkit-box-shadow: none;
        box-shadow: none;
        border: 1px solid transparent;
    }
    .btn-primary {
        color: #fff;
        background-color: #337ab7;
        border-color: #2e6da4;
    }
    .btn-primary {
        background-color: #3c8dbc;
        border-color: #367fa9;
    }
</style>
<section class="content-header">
    <h1>
       	Report PMS - Reservation List
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Report PMS</a></li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="box box-form">
            <div class="box-body" style="font-size:12px;">
                <div class="form-group">
                    <label>Filter:</label>
                    <select name="rt" id="selrt" class="form-control">
                        <option value="">All Room Type</option>
                        <?php
                            $data = getHotelRoom($db, $hoteloid);
                            foreach($data as $row){
                                echo '<option value="'.$row['roomoid'].'">'.$row['name'].'</option>';
                            }
                        ?>
                    </select>
                    <select name="status" id="selstat" class="form-control">
                        <option value="">All Status</option>
                        <?php
                            $data = getPMSStatus($db, array('0'));
                            foreach($data as $row){
                                echo '<option value="'.$row['pmsstatusoid'].'">'.$row['status'].'</option>';
                            }
                        ?>
                    </select>
                    <select name="market" id="selmark" class="form-control">
                        <option value="">All Market</option>
                        <?php
                            $data = getBookingMarket($db);
                            foreach($data as $row){
                                echo '<option value="'.$row['bookingmarketoid'].'">'.$row['bm_name'].'</option>';
                            }
                        ?>
                    </select>
                    <select name="agent" id="selagent" class="form-control" style="max-width:110px;">
                        <option value="">All Agent</option>
                        <?php
                            if($hoteloid > 0){
                                $data = getAgent($db, $hoteloid);
                            }else{
                                if($_SESSION['_typeusr'] == "4"){
                            		$s_chain = "SELECT ua.oid FROM users u INNER JOIN userassign ua USING (useroid) WHERE ua.type = 'chainoid' AND u.useroid = '".$_SESSION['_oid']."'";
                            		$q_chain = $db->query($s_chain);
                            		$r_chain = $q_chain->fetchAll(PDO::FETCH_ASSOC);
                            		foreach($r_chain as $row){
                            			$chainoid = $row['oid'];
                            		}
                            		$data = getAgentChain($db, $chainoid); 
                            	}else{
                                    $data = array();
                            	}
                            }
                            foreach($data as $row){
                                echo '<option value="'.$row['agenttypeoid'].'">'.$row['typename'].'</option>';
                            }
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label>Reservation Date:</label>
                    <input type="radio" name="bdall" value="0" checked>All &nbsp;
                    <input type="radio" name="bdall" value="1">Range &nbsp;
                    <div id="bdall" style="display:none;">
                        <label>from:</label>
                        <input type="text" class="form-control" name="startdate" id="startdate1" readonly="readonly" value="<?=date("d M y")?>" style="width:75px">
                        <label>to:</label>
                        <input type="text" class="form-control" name="enddate" id="enddate1" readonly="readonly" value="<?=date("d M y", strtotime("+1 Month"))?>" style="width:75px">
                    </div>
                </div>
                <div class="form-group">
                    <label>Check-in Date:</label>
                    <input type="radio" name="ciall" value="0" checked>All &nbsp;
                    <input type="radio" name="ciall" value="1">Range &nbsp;
                    <div id="ciall" style="display:none;">
                        <label>from:</label>
                        <input type="text" class="form-control" name="startdate" id="startdate2" readonly="readonly" value="<?=date("d M y")?>" style="width:75px">
                        <label>to:</label>
                        <input type="text" class="form-control" name="enddate" id="enddate2" readonly="readonly" value="<?=date("d M y", strtotime("+1 Month"))?>" style="width:75px">
                    </div>
                     &nbsp; 
                    <button type="button" class="btn btn-primary" id="btfilter">FILTER</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="box box-form">
            <div class="box-body" style="font-size:12px;overflow:auto">
            <div id="xload" align="center"><img src="../images/loading.gif" style="width:10%"></div>
            <table id="example" class="display" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Res ID</th>
                        <th>Date</th>
                        <th>Guest Name</th>
                        <th>C/I</th>
                        <th>C/O</th>
                        <th>Stay</th>
                        <th>Room# / Hotel Name</th>
                        <th>Pax</th>
                        <th>Status</th>
                        <th>Amount</th>
                        <th>Booking Market</th>
                        <th>Agent</th>
                        <th>Check in Card</th>
                    </tr>
                </thead>
                <tbody style="display:none">
                    <?php
                        $titles = getCustTitle($db);
                        if($hoteloid > 0){
                            $data = getReservationData($db, $hoteloid); 
                        }else{
                    		$s_chain = "SELECT ua.oid, ua.type FROM users u INNER JOIN userassign ua USING (useroid) WHERE ua.type IN ('chainoid', 'hoteloid') AND u.useroid = '".$_SESSION['_oid']."'";
                    		$q_chain = $db->query($s_chain);
                    		$r_chain = $q_chain->fetchAll(PDO::FETCH_ASSOC);
                    		$rxoid = array(); $rxtype = array();
                    		foreach($r_chain as $row){
                    			$rxoid[] = $row['oid'];
                    			$rxtype[] = $row['type'];
                    		}
                    		$data = getReservationDataAllArray($db, $rxtype, $rxoid); 
                        }
                        $no=0;
                        foreach($data as $row){
                            $no++;
                            $bookingoid = $row['bookingoid'];
                            $bookingnum = $row['bookingnumber'];
                            if(isset($titles[$row['title']])) $title = $titles[$row['title']]; else $title = $row['title'];
                            $guestname = $title." ".$row['firstname']." ".$row['lastname'];
                            $checkin = date("d M y", strtotime($row['checkin']));
                            $checkout = date("d M y", strtotime($row['checkout']));
                            $diff = $row['diff'];
                            $roomnumber = empty($row['roomnumber']) ? "-" : $row['roomnumber'];
                            $room = $row['room'];
                            $hotelname = $row['hotelname'];
                            $adult = $row['adult'];
                            $child = $row['child'];
                            if($row['bookingstatusoid'] == "5"){
                                $status = "Cancelled";
                            }else if($row['bookingstatusoid'] == "7"){
                                $status = "No Show";
                            }else{
                                $status = $row['status']; 
                            }
                            $grandtotal = number_format($row['grandtotal'], 2, '.', ',');
                            $currcode = empty($row['currencycode']) ? "IDR" : $row['currencycode'];
                            $market = empty($row['bm_name']) ? empty($row['bm_name1']) ? "-" : $row['bm_name1'] : $row['bm_name'];
                            $bookingdate = date("d M y", strtotime($row['bookingtime']));
                            $agentname = empty($row['agentname']) ? "-" : $row['agentname'];
                            $agenttypeoid = empty($row['agenttypeoid']) ? "0" : $row['agenttypeoid'];
                    ?>
                    <tr class="gradeX">
                        <td><?=$no?></td>
                        <td><a class="detail" title="View Detail"><?=$bookingnum?></a></td>	
                        <td><?=$bookingdate?></td>
                        <td><?=$guestname?></td>
                        <td class="acin" x="<?=$checkin?>"><?=$checkin?></td>
                        <td><?=$checkout?></td>
                        <td><?=$diff." night"?></td>
                        <td><?=$roomnumber." / ".$hotelname?></td>
                        <td><?=$adult?>(A) <?=$child?>(C)</td>
                        <td><?=$status?></td>
                        <td><?=$currcode." ".$grandtotal?></td>
                        <td><?=$market?></td>
                        <td class="agt" x="<?=$agenttypeoid?>"><?=$agentname?></td>
                        <td align="center">
                            <a class="print" title="Print Reservation Card"><i class="fa fa-lg fa-print"></i></a>
                            <form class="reservation-card" action="<?=$base_url?>/pms-lite/view/reservation-card.php" method="post" target="_blank">
                                <input name="bookingroomdtl" type="hidden" value="<?=$row['bookingroomdtloid']?>">
                                <input name="hotel" type="hidden" value="<?=$hoteloid?>">
                            </form>
                            <form class="view-reservation-detail" action="<?=$base_url?>/pms-lite/reservation-detail" method="post" target="_blank">
                                <input name="bookingoid" type="hidden" value="<?=$bookingoid?>">
                                <input name="hotel" type="hidden" value="<?=$hoteloid?>">
                            </form>
                        </td>
                    </tr>
                    <?php
                        }
                    ?>
                </tbody>
            </table>
            </div>
            <div class="box-footer" align="right">
            <button type="button" class="btn btn-primary" id="btexport">Export to Excel</button>
            <form id="exporttoexcel" action="<?=$base_url?>/pms-lite/view/rsvlist_xls.php" method="post" target="_blank">
                <input name="hoteloid" type="hidden" value="<?=$hoteloid?>">
            </form>
            </div>
        </div>
    </div>
</section>