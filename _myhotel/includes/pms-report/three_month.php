<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<style>
    .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default {
        background: #fff !important;
    }
    .ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active {
        background: #FCF8E3 !important;
        color: #000 !important;
    }
    .ui-state-default.red, .ui-widget-content .ui-state-default.red, .ui-widget-header .ui-state-default.red {
        background: #DC2A2A !important;
    }
</style>
<script>
$( function() {
    $( "#datepicker" ).datepicker({
        numberOfMonths: 3
    });
    
    <?php
        $sql = "SELECT title, firstname, lastname, `date`, roomnumberoid FROM `bookingroomdtl` INNER JOIN `bookingroom` br USING(`bookingroomoid`) INNER JOIN `booking` USING(`bookingoid`) INNER JOIN `customer` USING(`custoid`) WHERE br.`pmsstatusoid` IN (2,4) AND `roomnumberoid` != 0 AND `hoteloid` = '".$hoteloid."'";
    	$stmt = $db->query($sql);
    	$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    	foreach($result as $data){
    	    $exp = explode('-',$data['date']);
    	    echo '$("td[data-handler=\'selectDay\'][data-month=\''.intval($exp[1]).'\'][data-year=\''.intval($exp[0]).'\']").each(function(){ var tx = $(this).children("a").text(); if(tx == "'.intval($exp[2]).'") $(this).children("a").addClass("red"); });';
    	}
    ?>
} );
</script>
<section class="content-header">
  <h1>3 Month View</h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-circle"></i>  PMS Lite</a></li>
    <li><a href="#"><i class="fa fa-circle"></i>  3 Month View</a></li>
  </ol>
</section>
<section id="pms-lite" class="content">
  <div class="box box-form">
    <div class="box-body">
      <div class="row">
        <div class="col-sm-12">
          <h1>3 Month View</h1>
        </div>
      </div>
    <div class="box-body">
        <div id="datepicker" align="center"></div>
    </div>
  </div>
</section>