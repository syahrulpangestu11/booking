<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous"> -->

<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    });
</script>
<section class="content-header">
    <h1>
       	Report PMS - Housekeeping Report</h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-table"></i> Report PMS</a></li>
    </ol>
</section>
<section class="content">
    


<div class="row">
<div class="box box-form"><div class="box-body" style="font-size: 12px;">

<div class="form-group">
<label>Between:</label> &nbsp;&nbsp;
<input type='text' class="form-control" value="2017-10-01"/>
<label>And:</label> &nbsp;&nbsp;
<input type='text' class="form-control" value="2017-10-31"/>
</div>
        </div></div>
</div>




    <div class="row">
        <div class="box box-form"><div class="box-body" style="font-size: 12px;">
        
            <table id="example" class="display housekeeping table promo-table" cellspacing="0" width="100%">
            <thead>
              <tr class="table-head">
                <td width="15"><strong>#</strong></td>
                <td><strong>Room</strong></td>
                <td><strong>Room Type</strong></td>
                <td><strong>Res#</strong></td>
                <td><strong>Guest Name</strong></td>
                <td><strong>Check In</strong></td>
                <td><strong>Check Out</strong></td>
                <td><strong>Nights</strong></td>
                <td><strong>Room Notes</strong></td>
                <td><strong>Status</strong></td>
                <td><strong>House Keeper</strong></td>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>&nbsp;</td>
                <td>101</td>
                <td>Standard</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -&nbsp;&nbsp; </td>
                <td>&nbsp;</td>
                <td>TouchUp</td>
                <td>Not Assigned</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>102</td>
                <td>Standard</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -&nbsp;&nbsp; </td>
                <td>&nbsp;</td>
                <td>TouchUp</td>
                <td>Not Assigned</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>103</td>
                <td>Standard</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -&nbsp;&nbsp; </td>
                <td>&nbsp;</td>
                <td>TouchUp</td>
                <td>Not Assigned</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>104</td>
                <td>Standard</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -&nbsp;&nbsp; </td>
                <td>&nbsp;</td>
                <td>TouchUp</td>
                <td>Not Assigned</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>105</td>
                <td>Standard</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -&nbsp;&nbsp; </td>
                <td>&nbsp;</td>
                <td>TouchUp</td>
                <td>Not Assigned</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>111</td>
                <td>Deluxe</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -&nbsp;&nbsp; </td>
                <td>&nbsp;</td>
                <td>TouchUp</td>
                <td>Not Assigned</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>112</td>
                <td>Deluxe</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -&nbsp;&nbsp; </td>
                <td>&nbsp;</td>
                <td>TouchUp</td>
                <td>Not Assigned</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>113</td>
                <td>Deluxe</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -&nbsp;&nbsp; </td>
                <td>&nbsp;</td>
                <td>TouchUp</td>
                <td>Not Assigned</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>114</td>
                <td>Deluxe</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -&nbsp;&nbsp; </td>
                <td>&nbsp;</td>
                <td>TouchUp</td>
                <td>Not Assigned</td>
              </tr>
              <tr resrvid="9854718" rsvrmid="9838683" tabcolor="109617" tabname="Austin Holden" onclick="showSingleResDesc(event,this);">
                <td>&nbsp;</td>
                <td>131</td>
                <td>Luxury</td>
                <td>100311</td>
                <td>Austin Holden</td>
                <td>11-Oct-17</td>
                <td>18-Oct-17</td>
                <td>7</td>
                <td>&nbsp;</td>
                <td>Dirty</td>
                <td>Not Assigned</td>
              </tr>
              <tr resrvid="9854719" rsvrmid="9838684" tabcolor="109617" tabname="Neil Armstrong" onclick="showSingleResDesc(event,this);">
                <td>&nbsp;</td>
                <td>132</td>
                <td>Luxury</td>
                <td>100312</td>
                <td>Neil Armstrong</td>
                <td>11-Oct-17</td>
                <td>18-Oct-17</td>
                <td>7</td>
                <td>&nbsp;</td>
                <td>Dirty</td>
                <td>Not Assigned</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>133</td>
                <td>Luxury</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -&nbsp;&nbsp; </td>
                <td>&nbsp;</td>
                <td>TouchUp</td>
                <td>Not Assigned</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>134</td>
                <td>Luxury</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -&nbsp;&nbsp; </td>
                <td>&nbsp;</td>
                <td>TouchUp</td>
                <td>Not Assigned</td>
              </tr>
              <tr resrvid="9854720" rsvrmid="9838685" tabcolor="109617" tabname="April Monique" onclick="showSingleResDesc(event,this);">
                <td>&nbsp;</td>
                <td>135</td>
                <td>Luxury</td>
                <td>100313</td>
                <td>April Monique</td>
                <td>11-Oct-17</td>
                <td>18-Oct-17</td>
                <td>7</td>
                <td>---</td>
                <td>Dirty</td>
                <td>Not Assigned </td>
              </tr>
            </tbody>
            </table>
<BR /><BR />
            <p>&nbsp;</p>
      </div></div>
    </div>
</section>