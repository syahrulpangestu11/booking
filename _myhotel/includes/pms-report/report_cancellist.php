<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous"> -->

<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    });
</script>
<section class="content-header">
    <h1>
       	Report PMS - Cancellation List
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Report PMS</a></li>
    </ol>
</section>
<section class="content">
    


<div class="row">
<div class="box box-form"><div class="box-body" style="font-size: 12px;">

<div class="form-group">
    Select Room Type:   
    <input type="checkbox" id="myChec1">Superior    
    <input type="checkbox" id="myCheck">Deluxe 
    <input type="checkbox" id="myCheck">Suites
    &nbsp;&nbsp;&nbsp;&nbsp;
    <!-- <select name="status" class="form-control">
        <option value="volvo">All Status</option>
        <option value="saab">Check In</option>
        <option value="fiat">Check Out</option>
        <option value="audi">Reserve</option>
        <option value="volvo">Temporary Reserve</option>
    </select>
        
    <select name="market" class="form-control">
        <option value="volvo">All Market</option>
        <option value="volvo">Direct website</option>
        <option value="saab">direct booking</option>
        <option value="fiat">Travel agent</option>
        <option value="audi">Corporate</option>
        <option value="volvo">Online Travel Agent (OTA)</option>
        <option value="saab">Vacation Rental</option>
        <option value="fiat">Wedding & event</option>
        <option value="audi">Affiliate</option>
    </select> -->
    
    <label>Between:</label> &nbsp;&nbsp;
    <input type='text' class="form-control" value="2017-10-01"/>
    <label>And:</label> &nbsp;&nbsp;
    <input type='text' class="form-control" value="2017-10-31"/>
</div>
        </div></div>
</div>




    <div class="row">
        <div class="box box-form"><div class="box-body" style="font-size: 12px;">
            <table id="example" class="display" cellspacing="0" width="100%">
            <thead>
            <tr>
               <th>#</th>
               <th>Res ID</th>
               <th>Guest Name</th>
               <td>Phone/Mobile</td>
               <td>Email</td>
               <td>Stay Duration</td>
               <td>Room Type</td>
             <th>Pax</th>
               <th>Amount</th>
             <th>Charges</th>
             <td>Reason</td>
               <th>Action</th>
               
            </tr>
         </thead>
         <tbody>
           
            <tr class="gradeX">
               <td>1</td>
                <td>100312</td>	
                <td>Jeane Julia</td>
                <td>jeane.juliag@gmail.com</td>
                <td>0834358466</td>
                <td>13 Oct - 18 Oct (7)</td>
                <td>Superior</td>
                <td>2(A) 0(C)</td>
                <td>2.481.548</td>
                <td>481.548 <br>Balance 0</td>
                <td>Cxl by Agoda</td>
                <td><input type="button" value="reinstate">&nbsp;<a href="#"><i class="fa fa-lg fa-envelope-o"></i> </a></td>
            </tr>
             <tr class="gradeX">
               <td>2</td>
                <td>100313</td>	
                <td>Nugraha Adi</td>
                <td>nugrahaadi@gmail.com</td>
                <td>08784545566</td>
                <td>13 Oct - 12 Oct (7)</td>
                <td>Luxury</td>
                <td>1(A) 0(C)</td>
                <td>1.381.559</td>
             <td>181.548 <br>Balance 181.548</td>
                <td>Cxl tru webrsv</td>
                <td><input type="button" value="reinstate">&nbsp;<a href="#"><i class="fa fa-lg fa-envelope-o"></i> </a></td>
            </tr>
             <tr class="gradeX">
               <td>3</td>
                  <td>100211</td>	
                <td>Zesfi Surya</td>
                <td>siryasurya@gmail.com</td>
                <td>0878734346</td>
                <td>13 Oct - 18 Oct (7)</td>
                <td>Deluxe</td>
                <td>2(A) 0(C)</td>
                <td>7.381.4810</td>
                 <td>381.481 <br>Balance 381.481</td>
                <td>Disaster</td>
                <td><input type="button" value="reinstate">&nbsp;<a href="#"><i class="fa fa-lg fa-envelope-o"></i> </a></td>
            </tr> <!--
             <tr class="gradeX">
               <td>5</td>
                <td>100211</td>	
                <td>Hanny K.</td>
                <td>hanny@gmail.com</td>
                <td>0845446466</td>
                <td>131 Oct - 14 Oct (7)</td>
                <td>111/Luxury</td>
                <td>1(A) 1(C)</td>
                <td>Rp 3,381,481.80</td>
                <td>&nbsp;</td>
                <td><a href="#"><img src="printer.png"></a>&nbsp;<a href="#"><i class="fa fa-lg fa-envelope-o"></i> </a></td>
            </tr>  
               <tr class="gradeX">
                 <td>6</td>
                <td>100311</td>	
                <td>Lorensio D.</td>
                <td>lorensio@gmail.com</td>
                <td>08785558466</td>
                <td>13 Oct - 15 Oct (7)</td>
                <td>101/Luxury</td>
                <td>2(A) 0(C)</td>
                <td>Rp 7,586,481.40</td>
                <td>&nbsp;</td>
                <td><a href="#"><img src="printer.png"></a>&nbsp;<a href="#"><i class="fa fa-lg fa-envelope-o"></i> </a></td>
            </tr>                                  
            <tr class="gradeX">
                 <td>7</td>
                <td>100311</td>	
                <td>Lorensio D.</td>
                <td>lorensio@gmail.com</td>
                <td>08785558466</td>
                <td>13 Oct - 15 Oct (7)</td>
                <td>Luxury</td>
                <td>2(A) 0(C)</td>
                <td>Rp 7,586,481.40</td>
                <td>&nbsp;</td>
                <td><a href="#"><img src="printer.png"></a>&nbsp;<a href="#"><i class="fa fa-lg fa-envelope-o"></i> </a></td>
            </tr>    
            <tr class="gradeX">
                 <td>8</td>
                <td>100311</td>	
                <td>Jeanny Dorce</td>
                <td>lorensio@gmail.com</td>
                <td>08785558466</td>
                <td>13 Oct - 15 Oct (7)</td>
                <td>Luxury</td>
                <td>2(A) 0(C)</td>
                <td>Rp 7,586,481.40</td>
                <td>&nbsp;</td>
                <td><a href="#"><img src="printer.png"></a>&nbsp;<a href="#"><i class="fa fa-lg fa-envelope-o"></i> </a></td>
            </tr>            <!-->
         </tbody>
            </table>
        </div></div>
    </div>
</section>