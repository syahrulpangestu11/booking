<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.6.1/fullcalendar.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.6.1/fullcalendar.print.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.6.1/fullcalendar.min.js"></script>
<style>
    #calendar {
		max-width: 900px;
		margin: 0 auto;
	}
	.fc-button-group, .fc button{
	    display:block;
	}
	.fc-view {
        border-left: 1px solid #ccc;
        border-right: 1px solid #ccc;
    }
    .fc-unthemed td.fc-today {
        background: #fcf8e3 !important;
    }
    .fc-event, .fc-event-dot {
        color: #fff !important;
    }
    .fc-event[style="background-color:#257e4a;border-color:#257e4a"] {
        background-color:#257e4a !important;
        border-color:#257e4a !important;
    }
    .fc-event[style="background-color:#DC2A2A;border-color:#DC2A2A"] {
        background-color:#DC2A2A !important;
        border-color:#DC2A2A !important;
    }
</style>
<script>

	$(document).ready(function() {

		$('#calendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,basicWeek,basicDay'
			},
			defaultDate: '<?=date('Y-m-d')?>',
			navLinks: true, // can click day/week names to navigate views
			editable: true,
			eventLimit: true, // allow "more" link when too many events
			events: [
			    <?php
                    $sql = "SELECT title, firstname, lastname, `date`, brd.roomnumberoid, roomnumber, room, br.pmsstatusoid FROM `bookingroomdtl` brd INNER JOIN `roomnumber` USING(`roomnumberoid`) INNER JOIN `bookingroom` br USING(`bookingroomoid`) INNER JOIN `booking` USING(`bookingoid`) INNER JOIN `customer` USING(`custoid`) WHERE br.`pmsstatusoid` IN (2,4) AND brd.`roomnumberoid` != 0 AND `hoteloid` = '".$hoteloid."' group by date, roomnumberoid";
                	$stmt = $db->query($sql);
                	$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
                	foreach($result as $data){
                	    if($data['pmsstatusoid']=='4'){ $color = ", color: '#257e4a'"; }else{ $color = ", color: '#DC2A2A'"; }
                	    echo "{ title: '".$data['firstname']." ".$data['lastname']." (".$data['roomnumber'].")', start: '".$data['date']."'".$color." },";
                	}
                ?>
			]
		});

	});

</script>
<section class="content-header">
  <h1>Calendar View</h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-circle"></i>  PMS Lite</a></li>
    <li><a href="#"><i class="fa fa-circle"></i>  Calendar View</a></li>
  </ol>
</section>
<section id="pms-lite" class="content">
  <div class="box box-form">
    <div class="box-body">
      <div class="row">
        <div class="col-sm-12">
          <h1>Calendar View</h1>
        </div>
      </div>
    <div class="box-body">
        <div id='calendar'></div>
    </div>
  </div>
</section>
