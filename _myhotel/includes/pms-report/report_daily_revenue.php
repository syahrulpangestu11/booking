<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous"> -->

<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    });
</script>
<section class="content-header">
    <h1>
       	Report PMS - Daily Revenue Report&nbsp;</h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-table"></i> Report PMS</a></li>
    </ol>
</section>
<section class="content">
    


<div class="row">
<div class="box box-form"><div class="box-body" style="font-size: 12px;">

<div class="form-group">
    <label>Between:</label> &nbsp;&nbsp;
    <input type='text' class="form-control" value="2017-10-01"/>
    <label>And:</label> &nbsp;&nbsp;
    <input type='text' class="form-control" value="2017-10-31"/>
</div>
        </div></div>
</div>




    <div class="row">
        <div class="box box-form"><div class="box-body" style="font-size: 12px;">
        
            <table id="example" class="display table promo-table" cellspacing="0" width="100%">
            <thead>
              <tr class="table-head">
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td colspan="3"><strong>2017</strong></td>
                <td colspan="3"><strong>2016</strong></td>
              </tr>
              <tr class="table-head">
                <td>&nbsp;</td>
                <td><strong>&nbsp;Gross&nbsp;</strong></td>
                <td><strong>&nbsp;Adj&nbsp;</strong></td>
                <td><strong>&nbsp;Today&nbsp;</strong></td>
                <td><strong>&nbsp;MTD&nbsp;</strong></td>
                <td><strong>&nbsp;YTD&nbsp;</strong></td>
                <td><strong>&nbsp;Today&nbsp;</strong></td>
                <td><strong>&nbsp;MTD&nbsp;</strong></td>
                <td><strong>&nbsp;YTD&nbsp;</strong></td>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td colspan="9"><strong>Room</strong></td>
              </tr>
              <tr>
                <td><strong>Room Charge</strong></td>
                <td>   3,101,460 </td>
                <td> - </td>
                <td> 3,101,460 </td>
                <td> 21,348,320 </td>
                <td> 26,207,260 </td>
                <td> - </td>
                <td> - </td>
                <td> - </td>
              </tr>
              <tr>
                <td><strong>Late Check-out</strong></td>
                <td>&nbsp;-</td>
                <td> - </td>
                <td>-</td>
                <td>- </td>
                <td>   - </td>
                <td> - </td>
                <td> - </td>
                <td> - </td>
              </tr>
              <tr>
                <td><strong>Early Check - In</strong></td>
                <td>&nbsp;-</td>
                <td> - </td>
                <td>-</td>
                <td>- </td>
                <td>   - </td>
                <td> - </td>
                <td> - </td>
                <td> - </td>
              </tr>
              <tr>
                <td><strong>Early Check - Out</strong></td>
                <td>&nbsp;-</td>
                <td> - </td>
                <td>55,309 </td>
                <td>- </td>
                <td>   - </td>
                <td> - </td>
                <td> - </td>
                <td> - </td>
              </tr>
              <tr>
                <td><strong>Room Charge(TAX Exempt)</strong></td>
                <td>&nbsp;-</td>
                <td> - </td>
                <td>-</td>
                <td>- </td>
                <td>   - </td>
                <td> - </td>
                <td> - </td>
                <td> - </td>
              </tr>
              <tr>
                <td><strong>Extra Bed</strong></td>
                <td>&nbsp;-</td>
                <td> - </td>
                <td>-</td>
                <td>- </td>
                <td>   - </td>
                <td> - </td>
                <td> - </td>
                <td> - </td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>   3,101,460 </td>
                <td> - </td>
                <td> 3,156,769 </td>
                <td> 21,348,320 </td>
                <td> 26,207,260 </td>
                <td> - </td>
                <td> - </td>
                <td> - </td>
              </tr>
              <tr>
                <td colspan="9"><strong>Room Others</strong></td>
              </tr>
              <tr>
                <td><strong>Cancellations</strong></td>
                <td>    55,309 </td>
                <td> - </td>
                <td>55,309 </td>
                <td>331,856 </td>
                <td> 1,363,439 </td>
                <td> - </td>
                <td> - </td>
                <td> - </td>
              </tr>
              <tr>
                <td><strong>No Show</strong></td>
                <td>&nbsp;-</td>
                <td> - </td>
                <td>-</td>
                <td>- </td>
                <td>   - </td>
                <td> - </td>
                <td> - </td>
                <td> - </td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td><strong>    55,309 </strong></td>
                <td><strong> - </strong></td>
                <td><strong>55,309 </strong></td>
                <td><strong>331,856 </strong></td>
                <td><strong> 1,363,439 </strong></td>
                <td><strong> - </strong></td>
                <td><strong> - </strong></td>
                <td><strong> - </strong></td>
              </tr>
              <tr>
                <td colspan="9"><strong>Hlx_Restaurant</strong></td>
              </tr>
              <tr>
                <td><strong>Hlx_Buffet</strong></td>
                <td>&nbsp;-</td>
                <td> - </td>
                <td>-</td>
                <td> 783 </td>
                <td>1,175 </td>
                <td> - </td>
                <td> - </td>
                <td> - </td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td><strong>&nbsp;-</strong></td>
                <td><strong> - </strong></td>
                <td><strong>-</strong></td>
                <td><strong> 783 </strong></td>
                <td><strong>1,175 </strong></td>
                <td><strong> - </strong></td>
                <td><strong> - </strong></td>
                <td><strong> - </strong></td>
              </tr>
              <tr>
                <td colspan="9"><strong>Hlx_SPA</strong></td>
              </tr>
              <tr>
                <td><strong>Hlx_Body Massage</strong></td>
                <td>&nbsp;-</td>
                <td> - </td>
                <td>-</td>
                <td> 205 </td>
                <td> 205 </td>
                <td> - </td>
                <td> - </td>
                <td> - </td>
              </tr>
              <tr>
                <td><strong>Hlx_Beauty Treatm</strong></td>
                <td>&nbsp;-</td>
                <td> - </td>
                <td>-</td>
                <td> 85 </td>
                <td> 85 </td>
                <td> - </td>
                <td> - </td>
                <td> - </td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td><strong>&nbsp;-</strong></td>
                <td><strong> - </strong></td>
                <td><strong>-</strong></td>
                <td><strong> 290 </strong></td>
                <td><strong> 290 </strong></td>
                <td><strong> - </strong></td>
                <td><strong> - </strong></td>
                <td><strong> - </strong></td>
              </tr>
              <tr>
                <td><strong>Total Revenue</strong></td>
                <td><strong>   3,156,769 </strong></td>
                <td><strong> - </strong></td>
                <td><strong> 3,212,079 </strong></td>
                <td><strong> 21,681,249 </strong></td>
                <td><strong> 27,572,164 </strong></td>
                <td><strong> - </strong></td>
                <td><strong> - </strong></td>
                <td><strong> - </strong></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td colspan="9"><strong>Payable</strong></td>
              </tr>
              <tr>
                <td><strong>Hlx_Room Tax</strong></td>
                <td>&nbsp;-</td>
                <td> - </td>
                <td>-</td>
                <td>651,302 </td>
                <td>752,616 </td>
                <td> - </td>
                <td> - </td>
                <td> - </td>
              </tr>
              <tr>
                <td><strong>Hlx_Service Tax</strong></td>
                <td>&nbsp;-</td>
                <td> - </td>
                <td>-</td>
                <td>757,270 </td>
                <td>896,835 </td>
                <td> - </td>
                <td> - </td>
                <td> - </td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td><strong>&nbsp;-</strong></td>
                <td><strong> - </strong></td>
                <td><strong>-</strong></td>
                <td><strong> 1,408,572 </strong></td>
                <td><strong> 1,649,451 </strong></td>
                <td><strong> - </strong></td>
                <td><strong> - </strong></td>
                <td><strong> - </strong></td>
              </tr>
              <tr>
                <td><strong>Total Payable</strong></td>
                <td><strong>&nbsp;-</strong></td>
                <td><strong> - </strong></td>
                <td><strong>-</strong></td>
                <td><strong> 1,408,572 </strong></td>
                <td><strong> 1,649,451 </strong></td>
                <td><strong> - </strong></td>
                <td><strong> - </strong></td>
                <td><strong> - </strong></td>
              </tr>
              
            </tbody>
            </table>
<BR /><BR />
            <p>&nbsp;</p>
      </div></div>
    </div>
</section>