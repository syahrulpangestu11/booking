<?php
    include('includes/bootstrap.php');

    $year = array();
    $curyear = intval(date("Y"));
    for($i=$curyear;$i>=2017;$i--){
        $year[] = $i;
    }
?>
<link rel="stylesheet" href="<?php echo $base_url?>/js/tablesorter/themes/blue/style.css?v=1">
<style>
    .content .form-group label {
        padding-right:10px;
    }
    .content .form-group label, .content .form-group select, .content .form-group input {
        margin-bottom:5px;
    }
    a.detail {
        color: #1200ff;
    }
    a.detail:hover {
        text-decoration: underline;
    }
    a.linkmodal {
        color: #2680e1;
    }
    a.linkmodal:hover {
        text-decoration: underline;
    }
    .btn {
        border-radius: 3px;
        -webkit-box-shadow: none;
        box-shadow: none;
        border: 1px solid transparent;
    }
    .btn-primary {
        color: #fff;
        background-color: #337ab7;
        border-color: #2e6da4;
    }
    .btn-primary {
        background-color: #3c8dbc;
        border-color: #367fa9;
    }
    #example {
        border: 1px solid #999;
    }
    #example tbody td, #example tfoot th {
        border-top: 1px solid #999;
    }
    #example tbody td, #example thead th, #example tfoot th {
        border-left: 1px dashed #ddd;
        padding: 2px 2px;
    }
    #example tbody tr td:first-child, #example thead tr th:first-child, #example tfoot tr th:first-child {
        border-left: 0;
    }
    #example thead th, #example tfoot th {
        text-align: center;
    }
    #example thead th {
        cursor: pointer;
    }
    .bright {border-right: 1px dashed #ddd !important;}
    #example thead, #example tfoot {
        background-color: #d4ebec;
    }
    #loaddata h2{
        font-weight: bold;
    }
</style>
<section class="content-header">
    <h1>
       	Report PMS - Occupancy per Villa
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Report PMS</a></li>
    </ol>
</section>
<section class="content">
<div class="row">
    <div class="col-lg-12">
    <div class="box box-form">
        <div class="box-body" style="font-size:12px;">
            <div class="form-group">
                <label>Year : </label>
                <select class="form-control inline input-sm" id="zyear">
                    <option value="">Select Year</option>
                    <?php
                        foreach($year as $k => $v){
                            echo '<option value="'.$v.'">'.$v.'</option>';
                        }
                    ?>
                </select>
                <button type="button" class="btn btn-primary btn-sm" id="btfilter">SHOW REPORT</button>
            </div>
        </div>
    </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
    <div class="box box-form">
        <div class="box-body" style="font-size: 12px;">
            <div id="xload" align="center"><img src="../images/loading.gif" style="width:10%"></div>

            <div id="loaddata"></div>
        </div>
    </div>
    </div>
</div>
</section>
<!-- chartjs -->
<script src="<?php echo $base_url?>/lib/plugins/chartjs/Chart.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.4/Chart.bundle.min.js" type="text/javascript"></script>
<script src="<?php echo $base_url?>/js/tablesorter/jquery.tablesorter.js" type="text/javascript"></script> 
<script>
    $(document).ready(function() {
        $('#xload').css('display', 'none');

        $('#btfilter').click(function(){
            $('#xload').css('display', '');
            $('#loaddata').html("");

            setTimeout(function (){
                var b = $("#zyear").val();

                if(b.trim()!=""){
                    $.post('<?php echo $base_url; ?>/includes/pms-report/occupancypervilla/ajax-occupancypervilla.php', { 'b':b }, function(response){
                        $('#loaddata').html(response);
                        $('#xload').css('display', 'none');
                        $("#example").tablesorter();
                    });
                }else{
                    alert("All field can't be empty!");
                    $('#xload').css('display', 'none');
                }
            }, 2000);
        });

        $(document).on('click','#btexport',function(){
            $('#exporttoexcel').submit();
        });
    });
</script>
<script type="text/javascript">
$(function(){
	$(document).on('click',"a.linkmodal",function(){
        $('#xx').html('<canvas id="myChart"></canvas>');

        var ss = $(this).attr('ss');
        var sm = $(this).attr('sm');

        //Prepare for Chart Booking
        var y = $("#data-name-"+ss).text();
        var z = $("#data-occ-"+ss).text();
        var ay = y.split(";");
        var az = z.split(";");
        var bgc = [];
        var bc = [];
        var bgcx = [];
        for(var i=0; i<ay.length; i++){
            bgc[i] = 'rgba(0, 136, 208, 0.7)';
            bc[i] = 'rgba(7, 102, 150, 1)';
            bgcx[i] = 'rgba(76, 206, 104, 0.7)';
        }
        
        var xdata = [];
        xdata[0] = {
                label: sm,
                data: az,
                backgroundColor: bgc,
                borderColor: bc,
                borderWidth: 1
            };

        var ctx = document.getElementById("myChart").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ay,
                datasets: xdata
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        });
        //End of Chart Booking
    });
});
</script>

<!-- Default bootstrap modal example -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" style="width:1080px">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Occupancy per Villa</h4>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-lg-12" id="xx">
                <canvas id="myChart"></canvas>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
