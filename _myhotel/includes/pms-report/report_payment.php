<?php
  include('includes/bootstrap.php');

  $month = array("January","February","March","April","May","June","July","August","September","October","November","December");
  $year = array();
  $curyear = intval(date("Y"));
  for($i=$curyear;$i>=2017;$i--){
      $year[] = $i;
  }
?>
<script>
    $(document).ready(function() {
        $(document).ready(function(){
            $('#btfilter').click();
        });

        $('#xload').css('display', 'none');

        $('#btfilter1').click(function(){
            $('#xload').css('display', '');
            $('#loaddata').html("");

            $.ajax({
              url: "<?php echo $base_url; ?>/includes/pms-report/ajax/report_payment.php",
              type: 'post',
              data: $('form#form-report-payment1').serialize(),
              success: function(response) {
                $('#loaddata').html(response);
                $('#xload').css('display', 'none');
              }
            });
        });

        $(document).on('click','#btexport',function(){
            $('#exporttoexcel').submit();
        });
        
        $( "#startdate1" ).datepicker({
    		defaultDate: "0",
    		dateFormat: "dd M y",
    		changeMonth: true, changeYear:true,
    		onClose: function( selectedDate ) {
                $( "#enddate1" ).datepicker( "option", "minDate", selectedDate );
    		},
    		onSelect: function(dateText) {}
        });
        $( "#enddate1" ).datepicker({
          	defaultDate: "0",
    		dateFormat: "dd M y",
          	changeMonth: true, changeYear:true,
         	onClose: function( selectedDate ) {
            	$( "#startdate1" ).datepicker( "option", "maxDate", selectedDate );
          	},
    		onSelect: function(dateText) {}
        });
        
        $( "#startdate2" ).datepicker({
    		defaultDate: "0",
    		dateFormat: "dd M y",
    		changeMonth: true, changeYear:true,
    		onClose: function( selectedDate ) {
                $( "#enddate2" ).datepicker( "option", "minDate", selectedDate );
    		},
    		onSelect: function(dateText) {}
        });
        $( "#enddate2" ).datepicker({
          	defaultDate: "0",
    		dateFormat: "dd M y",
          	changeMonth: true, changeYear:true,
         	onClose: function( selectedDate ) {
            	$( "#startdate2" ).datepicker( "option", "maxDate", selectedDate );
          	},
    		onSelect: function(dateText) {}
        });

        $('input[name="bdall"]').change(function(){
            var n = $(this).val();
            if(n == '1'){
                $('#bdall').css('display', 'inline-block');
            }else{
                $('#bdall').css('display', 'none');
            }
        });
        $('input[name="ciall"]').change(function(){
            var n = $(this).val();
            if(n == '1'){
                $('#ciall').css('display', 'inline-block');
            }else{
                $('#ciall').css('display', 'none');
            }
        });
    });
</script>
<style>
#example {
    border: 1px solid #999;
}
#example tbody td, #example tfoot th {
    border-top: 1px solid #999;
}
#example tbody td, #example thead th, #example tfoot th {
    border-left: 1px dashed #ddd;
    padding: 2px 2px;
}
#example tbody tr td:first-child, #example thead tr th:first-child, #example tfoot tr th:first-child {
    border-left: 0;
}
#example thead, #example tfoot {
    background-color: #d4ebec;
}
#example thead th {
    text-align: center;
}
</style>
<section class="content-header">
    <h1>Report PMS - Payment Report</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i>  Payment Report</a></li>
    </ol>
</section>
<section class="content">

<?php /*
  <div class="box box-danger box-form" style="background-color: #fdd8d8; padding:1px 20px 10px; text-align:center;">
    <div id="data-box" class="box-body">
      <b><i class="fa fa-info-circle"></i> This page is still under development.</b>
    </div>
  </div>
*/?>

  <div class="row">
    <div class="col-lg-12">
    <form id="form-report-payment1">
      <div class="box box-form form-inline">
          <div class="box-body" style="font-size:13px;padding:0;">
              <div class="form-group" style="display:block;margin-bottom:15px;">
                  <label style="padding-right:10px">Property : </label>
                  <?php
                  if($hoteloid > 0){
                      $stmt = $db->prepare("SELECT hoteloid, hotelcode, hotelname FROM hotel WHERE hoteloid=:a AND publishedoid='1' ORDER BY hotelname");
                    $stmt->execute(array(':a' => $hoteloid));
                      $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
                  }else{
                      $s_chain = "SELECT ua.oid, ua.type FROM users u INNER JOIN userassign ua USING (useroid) WHERE ua.type IN ('chainoid', 'hoteloid') AND u.useroid = '".$_SESSION['_oid']."'";
                  $q_chain = $db->query($s_chain);
                  $r_chain = $q_chain->fetchAll(PDO::FETCH_ASSOC);
                  $rxoid = array(); $rxtype = array();
                  foreach($r_chain as $row){
                    $rxoid[] = $row['oid'];
                    $rxtype[] = $row['type'];
                  }
                  
                  $rxchain = array(); $rxhotel = array();
                      foreach($rxtype as $k => $v){
                          if($v == 'chainoid'){
                              $rxchain[] = $rxoid[$k];
                          }else if($v == 'hoteloid'){
                              $rxhotel[] = $rxoid[$k];
                          }
                      }
                      
                      $msg = "";
                      if(count($rxchain) > 0){
                          $imp = implode(",", $rxchain);
                          $msg .= "AND chainoid IN (".$imp.")";
                      }
                      if(count($rxhotel) > 0){
                          $imp = implode(",", $rxhotel);
                          $msg .= "AND hoteloid IN (".$imp.")";
                      }
                  
                  if($msg != ""){
                          $stmt = $db->prepare("SELECT hoteloid, hotelcode, hotelname FROM hotel WHERE publishedoid='1' ".$msg." ORDER BY hotelname");
                        $stmt->execute(array());
                          $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
                  }else{
                      $data = array();
                  }
                  }
                  ?>
                  <select name="hotelcode" class="form-control" id="zhotel">
                      <option value="">- Select Hotel -</option>
                      <?php
                          if(count($data) > 1){
                              echo '<option value="0">All Hotels</option>';
                          }
                          foreach($data as $vals){
                              echo '<option value="'.$vals['hoteloid'].'">'.$vals['hotelname'].'</option>';
                          }
                      ?>
                  </select>
              </div>
              <div class="form-group" style="display:block;margin-bottom:15px;">
                    <label style="padding-right:10px;">Invoice Date:</label>
                    <input type="radio" name="bdall" value="0" checked> All &nbsp;
                    <input type="radio" name="bdall" value="1"> Range &nbsp;
                    <div id="bdall" style="display:none;">
                        <label>from: </label>
                        <input type="text" class="form-control" name="bdstartdate" id="startdate1" readonly="readonly" value="<?=date("d M y", strtotime("-1 Month"))?>" style="width:90px">
                        <label>to: </label>
                        <input type="text" class="form-control" name="bdenddate" id="enddate1" readonly="readonly" value="<?=date("d M y")?>" style="width:90px">
                    </div>
                </div>
                <div class="form-group" style="display:block;margin-bottom:15px;">
                    <label style="padding-right:10px;">Check-in Date:</label>
                    <input type="radio" name="ciall" value="0"> All &nbsp;
                    <input type="radio" name="ciall" value="1" checked> Range &nbsp;
                    <div id="ciall" style="display:inline-block;">
                        <label>from: </label>
                        <input type="text" class="form-control" name="cistartdate" id="startdate2" readonly="readonly" value="<?=date("d M y", strtotime("-1 Month"))?>" style="width:90px">
                        <label>to: </label>
                        <input type="text" class="form-control" name="cienddate" id="enddate2" readonly="readonly" value="<?=date("d M y")?>" style="width:90px">
                    </div>
                     &nbsp; 
                    <button type="button" class="btn btn-primary" id="btfilter1">FILTER</button>
                </div>
          </div>
      </div>
    </form>
    </div>
  </div>
  <div class="box box-form">
    <div class="box-body" style="font-size: 0.85em;">
        <div id="xload" align="center"><img src="../images/loading.gif" style="width:10%"></div>
        <div id="loaddata"></div>
    </div>
  </div>
</section>
