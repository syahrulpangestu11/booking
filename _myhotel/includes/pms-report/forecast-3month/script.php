<script type="text/javascript">
$(function(){

$('input[name="startperiode"]').datepicker( {
    changeMonth: true,
    changeYear: true,
    showButtonPanel: true,
    dateFormat: 'MM yy',
    onClose: function(dateText, inst) {
        $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
    }
});

function showInventory(){
	box = $('div#show-inventory');
	box.html("loading ...");
	$.ajax({
		url: "<?php echo"$base_url"; ?>/includes/pms-report/forecast-3month/data_daily_3-month_forecast.php",
		type: 'post',
		data: $('form#daily-revenue').serialize(),
		success: function(data) {
			box.html(data);
		}
	});
}

$(document).on('click','#btexport',function(){
		$('#exporttoexcel').submit();
});

$(document).on('click','form#daily-revenue button', function(e){
	showInventory();
});

$(document).ready(function(){
	showInventory();
});

$( "#datepicker" ).datepicker({
	changeMonth: true,
	changeYear: true,
});

});
</script>
