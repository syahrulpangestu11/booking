<?php
    include('includes/bootstrap.php');
    //$hoteloid = 1;
    
    function getStartAndEndDate($week, $year) {
        $dto = new DateTime();
        $dto->setISODate($year, $week);
        $ret['week_start'] = $dto->format('Y-m-d');
        $dto->modify('+6 days');
        $ret['week_end'] = $dto->format('Y-m-d');
        return $ret;
    }
?>
<link rel="stylesheet" href="<?php echo $base_url?>/js/tablesorter/themes/blue/style.css?v=1">
<style>
    .box-footer{border:1;}
    #data-input label{display:inline;font-size:13px;font-weight:normal;}
    .fade.show {opacity: 1;}
    .nav-link {display: block;padding: .5rem 1rem;}
    .nav-tabs .nav-link {border: 1px solid transparent;border-top-left-radius: .25rem;border-top-right-radius: .25rem;}
    .nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {color: #495057;background-color: #fff;border-color: #dee2e6 #dee2e6 #fff;}
    .modal-dialog {width: 100%;}
    div#myModal {padding-left: 15px;padding-right: 15px;}
    .tab-container {padding: 10px;font-size: 13px;}
    #data-input select {border-radius: 0;padding-left: 3px;padding-right: 3px;}
    thead {background-color: #c2c2c2;color: #fdfdfd;}
</style>
<section class="content-header">
    <h1>
       	Chain Occupancy Report
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Chain Occupancy Report</a></li>
    </ol>
</section>
<section class="content">
<div class="row">
    <div class="col-lg-12">
        <form method="post" enctype="multipart/form-data" id="data-input" action="">
        <div class="box box-form">
	        <div class="box-header with-border">
            	<h1>Filter Search</h1>
            </div>
            <div id="data-box" class="box-body">
                <div class="form-row">
                    <div class="form-check col-md-2">
                        <input type="radio" class="form-check-input" name="filtersearch" value="stay" checked>
                        <label class="form-check-label">Stay Date</label>
                    </div>
                    <div class="form-check col-md-2">
                        <input type="radio" class="form-check-input" name="filtersearch" value="booking">
                        <label class="form-check-label">Booking Date</label>
                    </div>
                    <div class="form-check col-md-2">
                        <input type="radio" class="form-check-input" name="filtersearch" value="checkin">
                        <label class="form-check-label">Check-In Date</label>
                    </div>
                </div>
        	</div>
	        <div class="box-header with-border">
            	<h1>Property</h1>
            </div>
            <div id="data-box" class="box-body" style="margin-left:15px;margin-right:15px;">
                <div class="row">
                    <div class="form-check col-md-2">
                        <input type="checkbox" class="form-check-input rty all" name="property[]" value="0" checked>
                        <label class="form-check-label">All Property</label>
                    </div>
                    <?php 
                    if($hoteloid > 0){
                        $stmt = $db->prepare("SELECT hoteloid, hotelcode, hotelname FROM hotel WHERE hoteloid=:a AND publishedoid='1' ORDER BY hotelname");
                        $stmt->execute(array(':a' => $hoteloid));
                        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
                    }else{
                        $s_chain = "SELECT ua.oid, ua.type FROM users u INNER JOIN userassign ua USING (useroid) WHERE ua.type IN ('chainoid', 'hoteloid') AND u.useroid = '".$_SESSION['_oid']."'";
                        $q_chain = $db->query($s_chain);
                        $r_chain = $q_chain->fetchAll(PDO::FETCH_ASSOC);
                        $rxoid = array(); $rxtype = array();
                        foreach($r_chain as $row){
                            $rxoid[] = $row['oid'];
                            $rxtype[] = $row['type'];
                        }

                        $rxchain = array(); $rxhotel = array();
                        foreach($rxtype as $k => $v){
                            if($v == 'chainoid'){
                                $rxchain[] = $rxoid[$k];
                            }else if($v == 'hoteloid'){
                                $rxhotel[] = $rxoid[$k];
                            }
                        }

                        $msg = "";
                        if(count($rxchain) > 0){
                            $imp = implode(",", $rxchain);
                            $msg .= "AND chainoid IN (".$imp.")";
                        }
                        if(count($rxhotel) > 0){
                            $imp = implode(",", $rxhotel);
                            $msg .= "AND hoteloid IN (".$imp.")";
                        }

                        if($msg != ""){
                            $stmt = $db->prepare("SELECT hoteloid, hotelcode, hotelname FROM hotel WHERE publishedoid='1' ".$msg." ORDER BY hotelname");
                            $stmt->execute(array());
                            $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
                        }else{
                            $data = array();
                        }
                    }
                    $x=2;
                    foreach($data as $vals){
                        echo '
                        <div class="form-check col-md-2">
                            <input type="checkbox" class="form-check-input rty noall" name="property[]" value="'.$vals['hoteloid'].'">
                            <label class="form-check-label">'.$vals['hotelname'].'</label>
                        </div>
                        ';
                        if($x%5==0){
                            echo '
                            </div>
                            <div class="row">
                            ';
                        }
                        $x++;
                    }
                    ?>
                </div>
        	</div>
	        <div class="box-header with-border">
            	<h1>Period</h1>
            </div>
            <div id="data-box" class="box-body">
                <div class="form-row">
                    <div class="form-check col-md-2">
                        <input type="radio" class="form-check-input" name="period" value="annual" checked>
                        <label class="form-check-label">Annually</label>
                        <br>
                        <select name="annually">
                            <?php
                            for($i=2017;$i<=intval(date("Y"));$i++){
                                echo '<option value="'.$i.'">'.$i.'</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-check col-md-2">
                        <input type="radio" class="form-check-input" name="period" value="month">
                        <label class="form-check-label">Monthly</label>
                        <br>
                        <select name="monthly_year">
                            <?php
                            for($i=2017;$i<=intval(date("Y"));$i++){
                                echo '<option value="'.$i.'">'.$i.'</option>';
                            }
                            ?>
                        </select>
                        <select name="monthly_month">
                            <?php
                            for($i=1;$i<=12;$i++){
                                echo '<option value="'.date("m", strtotime("2017-".$i."-1")).'">'.date("F", strtotime("2017-".$i."-1")).'</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-check col-md-2">
                        <input type="radio" class="form-check-input" name="period" value="week">
                        <label class="form-check-label">Weekly</label>
                        <br>
                        <select name="weekly_year" class="yweek">
                            <?php
                            for($i=2017;$i<=intval(date("Y"));$i++){
                                echo '<option value="'.$i.'">'.$i.'</option>';
                            }
                            ?>
                        </select>
                        <select name="weekly_week" class="wweek">
                            <?php
                            for($j=2017;$j<=intval(date("Y"));$j++){
                                $woy = date("W", strtotime($j."-12-28"));
                                if($j > 2017){ $n='style="display:none"'; }
                                for($i=1;$i<=$woy;$i++){
                                    $week_array = getStartAndEndDate($i,$j);
                                    $val = date("Y-m-d",strtotime($week_array['week_start'])).'|'.date("Y-m-d",strtotime($week_array['week_end']));
                                    echo '<option value="'.$val.'" x="'.$j.'" '.$n.'>Week '.$i.'</option>';
                                }
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-check col-md-2">
                        <input type="radio" class="form-check-input" name="period" value="range">
                        <label class="form-check-label">Range Date</label>
                        <br>
                        <div class="input-group">
          					<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
    						<input type="text" name="range_start" class="form-control" id="startdate" placeholder="" value="<?php echo date("j F Y"); ?>">
    					</div>
    					<div class="input-group">
          					<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                            <input type="text" name="range_end" class="form-control" id="enddate" placeholder="" value="<?php echo date("j F Y", strtotime("+1 Month")); ?>">
    					</div>
                    </div>
                </div>
            </div>
            <div class="box-header with-border">
            	<h1>Compare</h1>
            </div>
            <div id="data-box" class="box-body">
                <div class="row">
                    <div class="form-row col-md-12">
                        <div class="form-check col-md-3">
                            <input type="checkbox" class="form-check-input" name="compare" value="1">
                            <label class="form-check-label">Variance</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-row col-md-12">
                        <div class="form-check col-md-2">
                            <select name="var_annually">
                                <?php
                                for($i=2017;$i<=intval(date("Y"));$i++){
                                    echo '<option value="'.$i.'">'.$i.'</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-check col-md-2">
                            <select name="var_monthly_year">
                                <?php
                                for($i=2017;$i<=intval(date("Y"));$i++){
                                    echo '<option value="'.$i.'">'.$i.'</option>';
                                }
                                ?>
                            </select>
                            <select name="var_monthly_month">
                                <?php
                                for($i=1;$i<=12;$i++){
                                    echo '<option value="'.date("m", strtotime("2017-".$i."-1")).'">'.date("F", strtotime("2017-".$i."-1")).'</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-check col-md-2">
                            <select name="var_weekly_year" class="yweek">
                                <?php
                                for($i=2017;$i<=intval(date("Y"));$i++){
                                    echo '<option value="'.$i.'">'.$i.'</option>';
                                }
                                ?>
                            </select>
                            <select name="var_weekly_week" class="wweek">
                                <?php
                                for($j=2017;$j<=intval(date("Y"));$j++){
                                    $woy = date("W", strtotime($j."-12-28"));
                                    if($j > 2017){ $n='style="display:none"'; }
                                    for($i=1;$i<=$woy;$i++){
                                        $week_array = getStartAndEndDate($i,$j);
                                        $val = date("Y-m-d",strtotime($week_array['week_start'])).'|'.date("Y-m-d",strtotime($week_array['week_end']));
                                        echo '<option value="'.$val.'" x="'.$j.'" '.$n.'>Week '.$i.'</option>';
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-check col-md-2">
                            <div class="input-group">
              					<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
        						<input type="text" name="var_range_start" class="form-control" id="startdate1" placeholder="" value="<?php echo date("j F Y"); ?>">
        					</div>
        					<div class="input-group">
              					<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                <input type="text" name="var_range_end" class="form-control" id="enddate1" placeholder="" value="<?php echo date("j F Y", strtotime("+1 Month")); ?>">
        					</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer" align="center">
				<button type="button" id="view" class="small-button blue" data-remote="false" data-toggle="modal" data-target="#myModal">View</button>
            </div>
   		</div>
        </form>
    </div>
</div>
</section>
<!-- chartjs -->
<script src="<?php echo $base_url?>/lib/plugins/chartjs/Chart.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.4/Chart.bundle.min.js" type="text/javascript"></script>
<script src="<?php echo $base_url?>/js/tablesorter/jquery.tablesorter.js" type="text/javascript"></script> 
<script type="text/javascript">
$(function(){
	$("#view").click(function(){
        $.post("<?=$base_url;?>/includes/pms-report/chain_occupancy/ajax-occupancy-report.php", $("#data-input").serialize(), function(result){
            $("#myModal .modal-body").html(result);
            $("#myTable").tablesorter();

            //Prepare for Chart Booking
            var y = $("#data-name").text();
            var z = $("#data-bid").text();
            var x = $("#data-com").text();
            var ay = y.split(";");
            var az = z.split(";");
            var ax = x.split(";");
            var bgc = [];
            var bc = [];
            var bgcx = [];
            var bcx = [];
            for(var i=0; i<ay.length; i++){
                bgc[i] = 'rgba(0, 136, 208, 0.7)';
                bc[i] = 'rgba(7, 102, 150, 1)';
                bgcx[i] = 'rgba(76, 206, 104, 0.7)';
                bcx[i] = 'rgba(38, 127, 174, 1)';
            }
            
            var xdata = [];
            xdata[0] = {
                    label: 'Data',
                    data: az,
                    backgroundColor: bgc,
                    borderColor: bc,
                    borderWidth: 1
                };
            if(x != ";;;"){
                xdata[1] = {
                    label: 'Compare',
                    data: ax,
                    backgroundColor: bgcx,
                    borderColor: bcx,
                    borderWidth: 1
                };
            }

            var ctx = document.getElementById("myChart").getContext('2d');
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: ay,
                    datasets: xdata
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            }
                        }]
                    }
                }
            });
            //End of Chart Booking
        });
    });
    
    $(".rty").change(function(){
        if(this.checked){
            var x = $(this).val();
            if(x == "0"){
                $(".rty.noall").prop('checked', false);
            }else{
                $(".rty.all").prop('checked', false);
            }
        }
    });
    
    
    $(".yweek").change(function(){
        var n = $(this).val();
        $(this).parent().find(".wweek").children("option").each(function(){
            var z = $(this).attr("x");
            if(z == n){
                $(this).show();
            }else{
                $(this).hide();
            }
        });
    });
    
    $( "#startdate1" ).datepicker({
		defaultDate: "+1w",
		changeMonth: true, changeYear:true,
		onClose: function( selectedDate ) {
            $( "#enddate1" ).datepicker( "option", "minDate", selectedDate );
		}
    });
    $( "#enddate1" ).datepicker({
      	defaultDate: "+1w",
      	changeMonth: true, changeYear:true,
     	onClose: function( selectedDate ) {
        	$( "#startdate1" ).datepicker( "option", "maxDate", selectedDate );
      	}
    });
});
</script>

<!-- Default bootstrap modal example -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Chain Occupancy Report</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>