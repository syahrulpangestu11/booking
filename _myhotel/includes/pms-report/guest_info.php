<style>
.form-group input[type=text] { width: 100%; }</style>
<section class="content-header">
    <h1>
       	Guest Information
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Guest Information</a></li>
    </ol>
</section>
<section class="content">
    <form class="form-horizontal">
    <div class="row">
        <div class="col-md-6">
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Guest Details</h3>
            </div>
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Guest ID</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control input-sm" value="P17" disabled>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Name</label>
                  <div class="col-sm-10">
                    <div class="row">
                        <div class="col-sm-3">
                            <select class="form-control input-sm">
                            <option>Title</option>
                            <option>Mr.</option>
                            <option>Mrs.</option>
                            <option>Ms.</option>
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <input type="text" class="form-control input-sm" placeholder="Firstname">
                        </div>
                        <div class="col-sm-5">
                            <input type="text" class="form-control input-sm" placeholder="Lastname">
                        </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Gender</label>
                  <div class="col-sm-10">
                    <div class="row">
                        <div class="col-sm-3">
                            <select class="form-control input-sm">
                            <option>Male</option>
                            <option>Female</option>
                            </select>
                        </div>
                        <div class="col-sm-9">
                            <div class="form-group">
                                <label for="" class="col-sm-1 control-label">ID</label>
                                <div class="col-sm-4">
                                    <select class="form-control input-sm">
                                    <option>Passport</option>
                                    <option>KTP</option>>
                                    </select>
                                </div>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control input-sm" placeholder="#">
                                </div>
                            </div>
                        </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Upload ID</label>
                  <div class="col-sm-10">
                    <button type="button" class="btn btn-default btn-sm">Upload</button>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">More Info</label>
                  <div class="col-sm-10">
                      <div class="row">
                          <div class="col-sm-3">
                            <button type="button" class="btn btn-default btn-sm">Upload</button>
                          </div>
                          <div class="col-sm-9">
                            <input type="text" class="form-control input-sm" placeholder="Note">
                          </div>
                      </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Nationality</label>
                  <div class="col-sm-10">
                    <select name="country" class="form-control">
                    	<option>Select</option>
                        <?php
            			$syntax_country = "select countryoid, countryname from country";
            			$stmt	= $db->query($syntax_country);
            			$result_country = $stmt->fetchAll(PDO::FETCH_ASSOC);
            			foreach($result_country as $country){
            			?>
            				<option value="<?=$country['countryoid']?>" <?=$selected?>><?=$country['countryname']?></option> 
            			<?php
            			}
            			?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Phone #</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control input-sm" placeholder="">
                  </div>
                  <label for="" class="col-sm-2 control-label">Fax #</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control input-sm" placeholder="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Mobile #</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control input-sm" placeholder="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">E-Mail</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control input-sm" placeholder="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Mark as</label>
                  <div class="col-sm-10">
                      <div class="radio">
                        <label>
                          <input type="radio" name="mark" value="" checked="">
                          VIP
                        </label>
                      </div>
                      <div class="radio">
                        <label>
                          <input type="radio" name="mark" value="">
                          Black List
                        </label>
                      </div>
                  </div>
                </div>
              </div>
          </div>
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Home Address</h3>
            </div>
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Address</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control input-sm" placeholder="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Country</label>
                  <div class="col-sm-4">
                    <select name="country" class="form-control">
                    	<option>Select</option>
                        <?php
            			$syntax_country = "select countryoid, countryname from country";
            			$stmt	= $db->query($syntax_country);
            			$result_country = $stmt->fetchAll(PDO::FETCH_ASSOC);
            			foreach($result_country as $country){
            			?>
            				<option value="<?=$country['countryoid']?>" <?=$selected?>><?=$country['countryname']?></option> 
            			<?php
            			}
            			?>
                    </select>
                  </div>
                  <label for="" class="col-sm-2 control-label">State</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control input-sm" placeholder="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">City</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control input-sm" placeholder="">
                  </div>
                  <label for="" class="col-sm-2 control-label">Zip Code</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control input-sm" placeholder="">
                  </div>
                </div>
              </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Work Details</h3>
            </div>
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Organization</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control input-sm" placeholder="">
                  </div>
                  <label for="" class="col-sm-2 control-label">Designation</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control input-sm" placeholder="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Address</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control input-sm" placeholder="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Country</label>
                  <div class="col-sm-4">
                    <select name="country" class="form-control">
                    	<option>Select</option>
                        <?php
            			$syntax_country = "select countryoid, countryname from country";
            			$stmt	= $db->query($syntax_country);
            			$result_country = $stmt->fetchAll(PDO::FETCH_ASSOC);
            			foreach($result_country as $country){
            			?>
            				<option value="<?=$country['countryoid']?>" <?=$selected?>><?=$country['countryname']?></option> 
            			<?php
            			}
            			?>
                    </select>
                  </div>
                  <label for="" class="col-sm-2 control-label">State</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control input-sm" placeholder="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">City</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control input-sm" placeholder="">
                  </div>
                  <label for="" class="col-sm-2 control-label">Zip Code</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control input-sm" placeholder="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Phone 1#</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control input-sm" placeholder="">
                  </div>
                  <label for="" class="col-sm-2 control-label">Phone 2#</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control input-sm" placeholder="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Fax #</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control input-sm" placeholder="">
                  </div>
                </div>
              </div>
          </div>
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Guest Preferences</h3>
            </div>
              <div class="box-body with-border">
                <div class="form-group">
                  <div class="col-sm-12">
                    <textarea class="form-control" rows="3" placeholder=""></textarea>
                  </div>
                </div>
              </div>
            <div class="box-header with-border">
              <h3 class="box-title">Other Details</h3>
            </div>
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Spouse</label>
                  <div class="col-sm-10">
                    <div class="row">
                        <div class="col-sm-3">
                            <select class="form-control input-sm">
                            <option>Title</option>
                            <option>Mr.</option>
                            <option>Mrs.</option>
                            <option>Ms.</option>
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <input type="text" class="form-control input-sm" placeholder="Firstname">
                        </div>
                        <div class="col-sm-5">
                            <input type="text" class="form-control input-sm" placeholder="Lastname">
                        </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Birthday</label>
                  <div class="col-sm-10">
                    <div class="row">
                        <div class="col-sm-3">
                            <select class="form-control input-sm">
                            <option>Month</option>
                            <option>January</option>
                            <option>February</option>
                            <option>March</option>
                            <option>April</option>
                            <option>May</option>
                            <option>June</option>
                            <option>July</option>
                            <option>August</option>
                            <option>September</option>
                            <option>October</option>
                            <option>November</option>
                            <option>December</option>
                            </select>
                        </div>
                        <div class="col-sm-3">
                            <select class="form-control input-sm">
                            <option>Day</option>
                                <?php 
                                for($i=1; $i<=31; $i++){
                                    echo '<option>'.$i.'</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-sm-3">
                            <select class="form-control input-sm">
                            <option>Year</option>
                                <?php 
                                for($i=2002; $i>=1970; $i--){
                                    echo '<option>'.$i.'</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Anniversary</label>
                  <div class="col-sm-10">
                    <div class="row">
                        <div class="col-sm-3">
                            <select class="form-control input-sm">
                            <option>Month</option>
                            <option>January</option>
                            <option>February</option>
                            <option>March</option>
                            <option>April</option>
                            <option>May</option>
                            <option>June</option>
                            <option>July</option>
                            <option>August</option>
                            <option>September</option>
                            <option>October</option>
                            <option>November</option>
                            <option>December</option>
                            </select>
                        </div>
                        <div class="col-sm-3">
                            <select class="form-control input-sm">
                            <option>Day</option>
                                <?php 
                                for($i=1; $i<=31; $i++){
                                    echo '<option>'.$i.'</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-sm-3">
                            <select class="form-control input-sm">
                            <option>Year</option>
                                <?php 
                                for($i=2002; $i>=1970; $i--){
                                    echo '<option>'.$i.'</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
          </div>
          <div class="box">
              <div class="box-body">
                <button type="submit" class="btn btn-primary">Save</button>
                <button type="reset" class="btn btn-default">Cancel</button>
                <button type="button" class="btn btn-default">Trip Advisor Review</button>
              </div>
          </div>
        </div>
    </div>
    </form>
</section>