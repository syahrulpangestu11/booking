<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous"> -->

<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    });
</script>
<section class="content-header">
    <h1>
       	Report PMS - Daily Report</h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-table"></i> Report PMS</a></li>
    </ol>
</section>
<section class="content">
    


<div class="row">
<div class="box box-form"><div class="box-body" style="font-size: 12px;">

<div class="form-group">
    <!-- Select Room Type:   
    <input type="checkbox" id="myChec1">Superior    
    <input type="checkbox" id="myCheck">Deluxe 
    <input type="checkbox" id="myCheck">Suites
    &nbsp;&nbsp;&nbsp;&nbsp;
    <select name="status" class="form-control">
        <option value="volvo">All Status</option>
        <option value="saab">Check In</option>
        <option value="fiat">Check Out</option>
        <option value="audi">Reserve</option>
        <option value="volvo">Temporary Reserve</option>
    </select>
        
    <select name="market" class="form-control">
        <option value="volvo">All Market</option>
        <option value="volvo">Direct website</option>
        <option value="saab">direct booking</option>
        <option value="fiat">Travel agent</option>
        <option value="audi">Corporate</option>
        <option value="volvo">Online Travel Agent (OTA)</option>
        <option value="saab">Vacation Rental</option>
        <option value="fiat">Wedding & event</option>
        <option value="audi">Affiliate</option>
    </select> -->
    
    <label>Between:</label> &nbsp;&nbsp;
    <input type='text' class="form-control" value="2017-10-01"/>
    <label>And:</label> &nbsp;&nbsp;
    <input type='text' class="form-control" value="2017-10-31"/>
</div>
        </div></div>
</div>




    <div class="row">
        <div class="box box-form"><div class="box-body" style="font-size: 12px;">
            <table id="example" class="display table promo-table" cellspacing="0" width="100%">
            <!-- <thead>
            </thead> -->
            <tbody>
              <tr class="table-head">
                <td>&nbsp;</td>
                <td colspan="3"><strong>11</strong></td>
                <td colspan="3"><strong>Oct</strong></td>
                <td colspan="3"><strong>Wed</strong></td>
              </tr>
            
              <tr class="table-head">
                <td>&nbsp;</td>
                <td colspan="3"><strong>FTD</strong></td>
                <td colspan="3"><strong>MTD</strong></td>
                <td colspan="3"><strong>YTD</strong></td>
              </tr>
              <tr>
                <td><strong>No of letable rooms</strong></td>
                <td colspan="3">14</td>
                <td colspan="3">18</td>
                <td colspan="3">34</td>
              </tr>
              <tr>
                <td><strong>RPD</strong></td>
                <td colspan="3">3</td>
                <td colspan="3">3</td>
                <td colspan="3">0</td>
              </tr>
              <tr>
                <td><strong>Complimentary rooms</strong></td>
                <td colspan="3">0</td>
                <td colspan="3">0</td>
                <td colspan="3">0</td>
              </tr>
              <tr>
                <td><strong>Occupancy %</strong></td>
                <td colspan="3">21</td>
                <td colspan="3">15</td>
                <td colspan="3">0</td>
              </tr>
              <tr>
                <td><strong>House use</strong></td>
                <td colspan="3">0</td>
                <td colspan="3">0</td>
                <td colspan="3">0</td>
              </tr>
              <tr>
                <td><strong>ARR</strong></td>
                <td colspan="3">1052256</td>
                <td colspan="3">747592</td>
                <td colspan="3">787734</td>
              </tr>
              <tr>
                <td><strong>REVPAR</strong></td>
                <td colspan="3">225484</td>
                <td colspan="3">110613</td>
                <td colspan="3">2827</td>
              </tr>
              <tr>
                <td><strong>HOTREVPAR</strong></td>
                <td colspan="3">225484</td>
                <td colspan="3">110619</td>
                <td colspan="3">2828</td>
              </tr>
              <tr class="table-head">
                <td><strong>Income    heads</strong></td>
                <td colspan="3"><strong>FTD</strong></td>
                <td colspan="3"><strong>MTD</strong></td>
                <td colspan="3"><strong>YTD</strong></td>
              </tr>
              <tr class="table-head">
                <td>&nbsp;</td>
                <td><strong>Actual</strong></td>
                <td><strong>Budget</strong></td>
                <td><strong>Variance</strong></td>
                <td><strong>Actual</strong></td>
                <td><strong>Budget</strong></td>
                <td><strong>Variance</strong></td>
                <td><strong>Actual</strong></td>
                <td><strong>Budget</strong></td>
                <td><strong>Variance</strong></td>
              </tr>
              <tr>
                <td><strong>Total income per # sold</strong></td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    1,052,256 </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -&nbsp;&nbsp; </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    1,052,256 </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    747,629 </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -&nbsp;&nbsp; </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    747,629 </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    787,776 </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -&nbsp;&nbsp; </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    787,776 </td>
              </tr>
              <tr>
                <td><strong>Room</strong></td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    3,156,769 </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -&nbsp;&nbsp; </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    3,156,769 </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    21,680,176 </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -&nbsp;&nbsp; </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    21,680,176 </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    27,570,699 </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -&nbsp;&nbsp; </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    27,570,699 </td>
              </tr>
              <tr>
                <td><strong>Total</strong></td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    3,156,769 </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -&nbsp;&nbsp; </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    3,156,769 </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    21,680,466 </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -&nbsp;&nbsp; </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    21,680,466 </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    27,570,989 </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -&nbsp;&nbsp; </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    27,570,989 </td>
              </tr>
              
            </tbody>
            </table>
      </div></div>
    </div>
</section>