<button type="button" class="small-button blue add-button">Add New Agent</button>
<ul class="inline-block this-inline-block">
	<li><span class="status-green square">sts</span> Active</li>
	<li><span class="status-black square">sts</span> Inactive</li>
	<!-- <li><span class="status-red square">sts</span> Expired</li> -->
</ul>

<?php
	error_reporting(E_ALL & ~E_NOTICE);
	include("../../conf/connection.php");

	$usr = $_REQUEST['usr'];
	$hoid = $_REQUEST['hoid'];

	// $main_query = "select c.cityname, s.statename, a.agentoid, a.agentname, a.publishedoid, (case when a.publishedoid = 2 then 'status-red' when a.publishedoid = 1 then 'status-green' else 'status-black' end) AS status
	// FROM
	// agenthotel ah
	// INNER JOIN published p ON ah.publishedoid = p.publishedoid
	// RIGHT JOIN agent a USING (agentoid)
	// INNER JOIN city c using (cityoid) inner join state s using (stateoid) inner join country ct using (countryoid)";

	$main_query = "select ah.agenthoteloid, c.cityname, s.statename, a.agentoid, at.typename, a.agentname, a.email, a.phone, ah.publishedoid, (case when ah.publishedoid = 2 then 'status-red' when ah.publishedoid = 1 then 'status-green' else 'status-black' end) AS status, creditfacility, creditbalance
	FROM
	agenthotel ah
	INNER JOIN published p ON ah.publishedoid = p.publishedoid
	RIGHT JOIN agent a USING (agentoid)
	LEFT JOIN agenttype at USING (agenttypeoid)
	INNER JOIN city c using (cityoid) inner join state s using (stateoid) inner join country ct using (countryoid)";

	$filter = array();
	if(isset($_REQUEST['name']) and $_REQUEST['name']!=''){
		array_push($filter, 'a.agentname like "%'.$_REQUEST['name'].'%"');
	}
	if(isset($_REQUEST['type']) and $_REQUEST['type']!=''){
		array_push($filter, 'a.agenttypeoid = "'.$_REQUEST['type'].'"');
	}
	if(isset($_REQUEST['country']) and $_REQUEST['country']!=''){
		array_push($filter, 'ct.countryoid = "'.$_REQUEST['country'].'"');
	}

	//array_push($filter, 'a.agentoid IN (SELECT ah.agentoid FROM agenthotel ah WHERE hoteloid = )');
	array_push($filter, "ah.hoteloid = '".$hoid."'");
	array_push($filter, 'ah.publishedoid not in (3)');

	if(count($filter) > 0){
		$combine_filter = implode(' and ',$filter);
		$query = $main_query.' where '.$combine_filter;
	}else{
		$query = $main_query;
	}
	//echo "<pre>".$query."</pre>";
	try {
		$stmt = $db->query($query." order by a.agenttypeoid asc, a.agentoid asc");
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
?>
<table class="table promo-table">
	<tr>
			<td>Agent Name</td>
			<td>Agent Type</td>
			<td>Email</td>
			<td>Phone</td>
            <td>Credit Facility</td>
            <td>Credit Balance</td>
			<td>&nbsp;</td>
	</tr>
<?php
		$r_agent = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_agent as $row){
				$agentoid = $row['agentoid'];
				$agenthoteloid = $row['agenthoteloid'];
				$agentname = $row['agentname'];
				$cityname = $row['cityname']; $statename = $row['statename'];
				$email = $row['email'];
				$phone = $row['phone'];
				$status = $row['status'];
				$creditfacility = $row['creditfacility'];
				$creditbalance = $row['creditbalance'];
				$agenttype = $row['typename'];
?>
    <tr class="<?php echo $status; ?>">
        <td><?php echo $agentname; ?></td>
        <td><?php echo $agenttype; ?></td>
		<td class="lowercase"><?php echo $email;?></td>
		<td class="lowercase"><?php echo $phone;?></td>
        <td><?php echo floor($creditfacility); ?></td>
        <td><?php echo floor($creditbalance); ?></td>
        <td class="algn-right">
        <button type="button" class="pencil edit-button" agenthoteloid="<?php echo $agenthoteloid; ?>">Edit</button>
        <button type="button" class="trash delete-button" agenthoteloid="<?php echo $agenthoteloid; ?>">Delete</button>
        <button type="button" class="small-button blue manage" agenthoteloid="<?php echo $agenthoteloid; ?>">Manage Rate</button>
        <button type="button" class="small-button blue creditfacility" agenthoteloid="<?php echo $agenthoteloid; ?>">Credit Facility</button>
        </td>
    </tr>
<?php
			}
?>
</table>
<?php
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		print($ex);
		die();
	}
?>
