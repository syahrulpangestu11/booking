<?php
	$daylist = array("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat");
	$stmt = $db->query("select max(priority)+1 as maxpriority from promotion where hoteloid = '".$hoteloid."' and publishedoid = '1'");
	$r_max = $stmt->fetchAll(PDO::FETCH_ASSOC);
	foreach($r_max as $row){
		$maxpriority = $row['maxpriority'];
	}
	
	$promooid = $_GET['pid'];
	try {
		$stmt = $db->query("select promotion.* from promotion inner join hotel h using (hoteloid) where promotionoid = '".$promooid."'");
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
			$r_promo = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_promo as $row){
				$promotype = $row['promotiontypeoid'];
				$name = $row['name'];
				$minstay = $row['minstay'];
				$salefrom = date("d F Y", strtotime($row['salefrom']));
				$saleto = date("d F Y", strtotime($row['saleto']));
				$stayfrom = date("d F Y", strtotime($row['bookfrom']));
				$stayto = date("d F Y", strtotime($row['bookto']));
				$display = explode(',',$row['displayed']);
				$checkin = explode(',',$row['checkinon']);
				$maxstay = $row['maxstay'];
				$timefrom = date("H:i", strtotime($row['timefrom']));
				$timeto = date("H:i", strtotime($row['timeto']));
				$discounttype = $row['discounttypeoid'];
				$discountapply = $row['discountapplyoid'];
				$applyvalue = $row['applyvalue'];
				$discountvalue = $row['discountvalue'];
				$minroom = $row['minroom'];
				$cancellationpolicy = $row['cancellationpolicyoid'];
				$published = $row['publishedoid'];
				$priority = $row['priority'];
				$deposit = $row['depositoid'];
				$depositvalue = $row['depositvalue'];
				
				$headline = $row['headline'];
				$description = $row['description'];
				$servicefacilities = $row['servicefacilities'];
				$termcondition = $row['termcondition'];
				
					try {
						$stmt = $db->query("select pa.*, channel.name as channelname from promotionapply pa inner join promotion using (promotionoid) inner join hotel h using (hoteloid) inner join roomoffer ro using (roomofferoid) inner join room using (roomoid) inner join channel using (channeloid) where promotionoid = '".$promooid."'");
						$row_count = $stmt->rowCount();
						if($row_count > 0) {
							$r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
							$roomoffer = array();
							foreach($r_room as $key => $row){
								$index_room = array_search($row['roomofferoid'], $roomoffer);
								
								if(empty($index_room)){ 
									array_push($roomoffer, $row['roomofferoid']); 
								}
								if(empty($channel[$row['roomofferoid']]) or !in_array($row['channeloid'], $channel[$row['roomofferoid']])){
									$channel[$row['roomofferoid']][] = $row['channeloid'];
									$channelname[$row['roomofferoid']][] = $row['channelname'];
								}
								
							}
						}
					}catch(PDOException $ex) {
						echo "Invalid Query".$ex; 
						die();
					}

			}
		}else{
			echo "No Result";
			die();
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}
?>
<script type="text/javascript">
$(function() {
	$('textarea').trumbowyg({
		btns: ['viewHTML',
		  '|', 'btnGrp-design',
		  '|', 'link',
		  '|', 'btnGrp-justify',
		  '|', 'btnGrp-lists'],
		fullscreenable: false
	});
});
</script>
<section class="content-header">
    <h1>
        Edit Promotions
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i>  Allotments &amp; Rates </a></li>
        <li class="active">Promotions</li>
    </ol>
</section>
<section class="content">
	<div class="row">
		<form class="form-box" method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/promotions/edit-process">
        <div class="box box-form">
            <input type="hidden" name="promooid" value="<?php echo $promooid; ?>" />
            <h1>PROMOTION</h1>
            <ul class="block">
                <li>
                    <span class="label">Promotion Name:</span>
                    <input type="text" class="long"  name="name" value="<?php echo $name; ?>">
                </li>
                <li>
                    <span class="label">Promotion Image:</span>
                    <input type="file" class="long" name="image">
                    <br /><i>recommended size : 275px x 155px</i>
                </li>
                <li>
                    <span class="label">Priority Order:</span>
                    <input type="number" class="small" min="1" max="<?php echo $maxpriority; ?>"  name="priority" value="<?php echo $priority; ?>">
                    <input type="hidden" name="oldpriority" value="<?php echo $priority; ?>">
                </li>
            </ul>
            <div class="clear"></div>
            <ul class="inline-triple">
            	<li>
                	<h1>CONDITION</h1>
                	<ul class="block">
                    	<li>
                        	<span class="label">Minimum Stay:</span>
                            <input type="text" class="small" name="minstay" value="<?php echo $minstay; ?>">
                        </li>
                    	<li><span class="label">Sale Date</span></li>
                    	<li>
                        	<span class="label">Sale Date From:</span>
                            <input type="text"class="medium" id="startdate" name="salefrom" value="<?php echo $salefrom; ?>">
                        </li>
                    	<li>
                        	<span class="label">Sale Date To:</span>
                            <input type="text"class="medium" id="enddate"; name="saleto" value="<?php echo $saleto; ?>">
                        </li>
                    	<li><span class="label">Stay Date</span></li>
                    	<li>
                        	<span class="label">Stay Date From:</span>
                            <input type="text"class="medium" id="startcalendar" name="stayfrom" value="<?php echo $stayfrom; ?>">
                        </li>
                    	<li>
                        	<span class="label">Stay Date To:</span>
                            <input type="text"class="medium" id="endcalendar" name="stayto" value="<?php echo $stayto; ?>">
                        </li>
                    	<li>
                        	<span class="label">Displayed on:</span><br>
							<?php
								foreach($daylist as $day){
									if(in_array($day, $display)){ $checked = "checked"; }else{ $checked = ""; } 
									echo"<input type='checkbox' name='display[]' value='".$day."' ".$checked.">".$day."&nbsp;&nbsp;";
								}
							?>
                        </li>
                    	<li>
                        	<span class="label">Check-in on:</span><br>
							<?php
								foreach($daylist as $day){
									if(in_array($day, $checkin)){ $checked = "checked"; }else{ $checked = ""; } 
									echo"<input type='checkbox' name='checkin[]' value='".$day."' ".$checked.">".$day."&nbsp;&nbsp;";
								}
							?>
                        </li>
                    	<li>
                        	<span class="label">Maximum Stay:</span>
                            <input type="text"class="small"  name="maxstay" value="<?php echo $maxstay; ?>"> [0 = N/A]
                        </li>
                    	<li>
                        	<span class="label">Book Time From:</span>
                            <input type="time" class="small"  name="timefrom" value="<?php echo $timefrom; ?>">
                        </li>
                    	<li>
                        	<span class="label">Book Time To:</span>
                            <input type="time" class="small"  name="timeto" value="<?php echo $timeto; ?>">
						</li>
                    </ul>
                </li>
                <li>
                	<h1>BENEFIT</h1>
                	<ul class="block">
                    	<li>
                        	<span class="label">Discount Type:</span>
							<?php
                                try {
                                    $stmt = $db->query("select dt.* from discounttype dt inner join published p using (publishedoid) where p.publishedoid = '1'");
                                    $r_dt = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                    foreach($r_dt as $row){
										if($row['discounttypeoid'] == $discounttype){ $selected = "checked"; }else{ $selected=""; }
                                        echo"<br><input type='radio' name='discounttype' value='".$row['discounttypeoid']."' label='".$row['labelquestion']."' ".$selected.">  ".$row['name'];
                                    }
                                }catch(PDOException $ex) {
                                    echo "Invalid Query";
                                    die();
                                }                            
                            ?>        	
                        </li>
                    	<li>
                            <span class="label"><b>Discount Value:</b></span><br>
                            <input type="text" class="small"  name="discountvalue" value="<?php echo $discountvalue; ?>">
                            <span class="label"></span>
                        </li>
                    	<li class="applybox">
                        	<span class="label">Apply On:</span>
							<?php
                                try {
                                    $stmt = $db->query("select da.* from discountapply da inner join published p using (publishedoid) where p.publishedoid = '1'");
                                    $r_dt = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                    foreach($r_dt as $row){
										if($row['discountapplyoid'] == $discountapply){ $selected = "checked"; }else{ $selected=""; }
                                        echo"<br><input type='radio' name='discountapply' value='".$row['discountapplyoid']."' ".$selected.">  ".$row['name'];
										if($row['discountapplyoid'] == '2'){
											echo"<div class='applyval'><span class='label'>Apply on Night Number :<br></span><input type='text' class='medium'  name='applyvalue' value='".$applyvalue."'><br>
											ex: 2,3,4 for night number 2, 3 and 4 only</div>";
										}else if($row['discountapplyoid'] == '3'){
											$applyvalue = explode(',',$applyvalue);
											echo"<div class='applyval'><span class='label'>Apply on Day:</span><br>";
											foreach($daylist as $day){
												if(!empty($applyvalue)){
													if(in_array($day, $applyvalue)){ $checked = "checked"; }else{ $checked = ""; }
												}else{
													$checked = "checked";
												}
												echo"<input type='checkbox' name='applyvalue[]' value='".$day."' ".$checked.">".$day."&nbsp;&nbsp;";
											}
											echo"</div>";
										}
                                    }
                                }catch(PDOException $ex) {
                                    echo "Invalid Query";
                                    die();
                                }                            
                            ?>        	
                        </li>
                    	<li>
                        	<span class="label">Minimum Room:</span>
                            <input type="text"class="small"  name="minroom" value="<?php echo $minroom; ?>">
                        </li>
					</ul>
                </li>
                <li>
                	<h1>Restriction</h1>
                	<ul class="block">
                    	<li>
                        	<span class="label">Room Type:</span><br />
                            <input type="radio" name="select" value="all" /> select all
                            &nbsp;&nbsp;
                            <input type="radio" name="select" value="none" /> unselect all
                            <br />
                            <ul class="block roomtype">
                            <?php
                                try {
                                    $stmt = $db->query("select r.roomoid, r.name, x.offernum from room r inner join hotel h using (hoteloid) left join (select count(roomofferoid) as offernum, ro.roomoid from roomoffer ro group by ro.roomoid) as x on x.roomoid = r.roomoid where h.hoteloid = '".$hoteloid."' and x.offernum > 0 and r.publishedoid = '1'");
                                    $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                    foreach($r_room as $row){
										if(!empty($room) and in_array($row['roomoid'], $room)){ $checked = "checked"; }else{ $checked = ""; }                                        
                                        echo"<li><span><b>".$row['name']."</b></span>";
										try {
											
											$stmt = $db->query("select ro.* from roomoffer ro inner join published p using (publishedoid) inner join room r using (roomoid) inner join offertype using (offertypeoid) where r.roomoid = '".$row['roomoid']."' and p.publishedoid = '1'");
											$r_plan = $stmt->fetchAll(PDO::FETCH_ASSOC);
											echo"<ul class='block roomtype-channel'>";
											foreach($r_plan as $row1){
												if(!empty($roomoffer) and in_array($row1['roomofferoid'], $roomoffer)){ $checked = "checked"; }else{ $checked = ""; }                                        
												echo"<li><input type='checkbox' name='rateplan[]' value='".$row1['roomofferoid']."' sign='rateplan' ".$checked.">".$row1['name']."<br><dl class='dropdown'>";
												try {
													$stmt = $db->query("select * from channel");
                            						$r_channel = $stmt->fetchAll(PDO::FETCH_ASSOC);
													$row_count = $stmt->rowCount();
													if($row_count > 0){ $classnotif = "hide"; }else{ $classnotif = "";  }
													echo"<dt>
														<a>
														  <span class='hida ".$classnotif."'>Select Channel</span>    
														  <p class='multiSel'>";
														  if(!empty($channelname[$row1['roomofferoid']])){
															foreach($channelname[$row1['roomofferoid']] as $rp){	  
																echo"<span title='".$rp."'>".$rp.", </span>";
															}
														  }
													echo"	  
														  </p>  
														</a>
														</dt>";					
													echo"<dd><ul class='mutliSelect'>";
													foreach($r_channel as $row2){
														if(!empty($channel[$row1['roomofferoid']]) and in_array($row2['channeloid'], $channel[$row1['roomofferoid']])){ $checked = "checked"; }else{ $checked = ""; }
														echo"<li><input type='checkbox' name='channel-".$row1['roomofferoid']."[]' value='".$row2['channeloid']."' val='".$row2['name']."' ".$checked.">".$row2['name']."</li>";
													}
													echo"</ul></dd>";
												}catch(PDOException $ex2) {
													echo $ex2;
													die();
												}
												echo"</dl></li>";
											}
											echo"</ul></li>";
										}catch(PDOException $ex1) {
											echo $ex1;
											die();
										}
                                    }
                                }catch(PDOException $ex) {
                                    echo "Invalid Query";
                                    die();
                                }
                            ?> 
                            </ul> 
                        </li>
                    	<li>
                            <span class="label">Cancellation Policy:</span>
                            <select name="cancellation">
                            <?php
                                try {
                                    $stmt = $db->query("select * from cancellationpolicy inner join published p using (publishedoid) where p.publishedoid = '1'");
                                    $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                    foreach($r_room as $row){
										if($row['cancellationpolicyoid'] == $cancellationpolicy){ $selected = "checked"; }else{ $selected=""; }
                                        echo"<option value='".$row['cancellationpolicyoid']."' ".$selected.">".$row['name']."</option>";
                                    }
                                }catch(PDOException $ex) {
                                    echo "Invalid Query";
                                    die();
                                }
                            ?>        	
                            </select>
                        </li>
                    	<li>
                            <span class="label">Publish Promotion :</span>
                            <select name="published">
                            <?php
                                try {
                                    $stmt = $db->query("select * from published where showthis = 'y'");
                                    $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                    foreach($r_room as $row){
										if($row['publishedoid'] == $published){ $selected = "selected"; }else{ $selected=""; }
                                        echo"<option value='".$row['publishedoid']."' ".$selected.">".$row['note']."</option>";
                                    }
                                }catch(PDOException $ex) {
                                    echo "Invalid Query";
                                    die();
                                }
                            ?>        	
                            </select>
                        </li>
					</ul>
                </li>
            </ul>
		</div>
        <div class="box box-form">
			<h2>HEADLINE &amp; DETAIL</h2>
            <ul class="inline-triple">
            <?php
                try {
                    $stmt = $db->query("select th.*, i.*, x.termheadlinepromooid from termheadline th left join icon i using (iconoid) left join (select termheadlinepromooid, termheadlineoid from termheadlinepromo thp where thp.id='".$promooid."' and thp.type='promotion') x on x.termheadlineoid = th.termheadlineoid where th.hoteloid='".$hoteloid."' order by icon_title");
                    $r_headline = $stmt->fetchAll(PDO::FETCH_ASSOC);
                    foreach($r_headline as $termheadline){
                        if($termheadline['iconoid'] == 0){
                            $icon_src = $termheadline['term_icon_src'];
                        }else{
                            $icon_src = $termheadline['icon_src'];
                        }
						if(!empty($termheadline['termheadlinepromooid'])){ $checkapply = "checked";  }else{ $checkapply = ""; }
                        ?>
                        <li>
                        <input type="checkbox" name="icon[]" value="<?=$termheadline['termheadlineoid']?>" <?=$checkapply?>/>&nbsp;&nbsp;<img src="<?=$icon_src?>" class="icon" />&nbsp;&nbsp;<?=$termheadline['icon_title']?>
                        </li>
                        <?php                        
                    }
                }catch(PDOException $ex) {
                    echo "Invalid Query";
                    die();
                }
            ?>
			</ul>
			<div class="clear"></div>            
            <ul class="inline-half">
            	<li>
                	<ul class="block">
                        <li>
                            <h3>Headline</h3>
                            <textarea name="headline"><?=$headline?></textarea>
                        </li>
                        <li>
                            <h3>Promotion Description</h3>
                            <textarea name="description"><?=$description?></textarea>
                        </li>
                        <li>
                            <h3>Promotion Services &amp; Facilities</h3>
                            <textarea name="servicefacilities"><?=$servicefacilities?></textarea>
                        </li>
                    </ul>
				</li>
                <li>
                	<ul class="block">
                    	<li>
                        	<h3>Deposit Type:</h3>
							<?php
                                try {
                                    $stmt = $db->query("select d.* from deposit d inner join published p using (publishedoid) where p.publishedoid = '1'");
                                    $r_dt = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                    foreach($r_dt as $row){
										if($row['depositoid'] == $deposit){ $selected = "checked"; $depositval = $depositvalue; }else{ $selected=""; $depositval = ""; }
                                        echo"<input type='radio' name='deposit' value='".$row['depositoid']."' ".$selected.">  ".$row['name'];
										if($row['depositoid'] == '2'){
											echo"<div class='depositval' style='display:none'><input type='text' class='medium'  name='depositvalue' value='".$depositval."'> Deposit for IDR Amount</div>";
										}else if($row['depositoid'] == '3'){
											echo"<div class='depositval' style='display:none'><input type='text' class='medium'  name='depositvalue' value='".$depositval."'> Deposit for %(percentage)from total</div>";
										}else if($row['depositoid'] == '4'){
											echo"<div class='depositval' style='display:none'><input type='text' class='medium'  name='depositvalue' value='".$depositval."'> night(s) amount</div>";
										}
										echo "<br>";
                                    }
                                }catch(PDOException $ex) {
                                    echo "Invalid Query";
                                    die();
                                }                            
                            ?>        	
                        </li>
                        <li>
                            <h3>Promotion Terms &amp; Condition</h3>
                            <textarea name="termcondition"><?=$termcondition?></textarea>
                        </li>
                    </ul>
				</li>
			</ul>
            <div class="clear"></div>   
            <div class="clear"></div>            
            <button class="default-button" type="submit">Edit Promotion</button>
   		</div>
    </form>
    </div>
</section>
