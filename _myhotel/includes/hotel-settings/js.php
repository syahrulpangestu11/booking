<script type="text/javascript">
$(function(){	
	var $dialogNotice = $('<div id="dialog-notice"></div>')
	.dialog({
		autoOpen: false,
		title: 'Notification',
		buttons: { 
			"Ok": function(){ 
				$( this ).dialog( "close" );
				$(location).attr("href", "<?php echo $base_url; ?>/hotel-settings"); 
			}
		}
	});
	
	$('button.add-button').click(function(){
		url = '<?php echo $base_url; ?>/includes/hotel-settings/update-hotel-settings.php';
		forminput = $('form#data-input');
		submitData(url, forminput);
	});
	
	function submitData(url, forminput){
		$.ajax({
			url: url,
			type: 'post',
			data: forminput.serialize(),
			success: function(data) {
				if(data == "1"){
					$dialogNotice.html("Data succesfully updated");
					$dialogNotice.dialog("open");
				}else{
					$dialogNotice.html("We&rsquo;re very sorry, we can't save your data right now");
					$dialogNotice.dialog("open");
				}
			}
		});
	}
});
</script>