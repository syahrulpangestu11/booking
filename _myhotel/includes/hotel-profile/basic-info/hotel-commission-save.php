<?php
	session_start();
	include("../../../conf/connection.php");
	$hoteloid = $_POST['hoteloid'];
	
	$id = array(); $startdate = array(); $enddate = array(); $value = array(); $priority = array(); $type = array(); 
	$new_startdate = array(); $new_enddate = array(); $new_value = array(); $new_priority = array(); $new_type = array();

	if(isset($_POST['id'])){
		foreach($_POST['id'] as $key => $val){ array_push($id, $val); }
	}
	if(isset($_POST['startdate'])){
		foreach($_POST['startdate'] as $key => $val){ array_push($startdate, $val); }
	}
	if(isset($_POST['enddate'])){
		foreach($_POST['enddate'] as $val){ array_push($enddate, $val); }
	}
	if(isset($_POST['value'])){
		foreach($_POST['value'] as $val){ array_push($value, $val); }
	}
	if(isset($_POST['priority'])){
		foreach($_POST['priority'] as $val){ array_push($priority, $val); }
	}
	if(isset($_POST['type'])){
		foreach($_POST['type'] as $val){ array_push($type, $val); }
	}
	if(isset($_POST['new_startdate'])){
		foreach($_POST['new_startdate'] as $val){ array_push($new_startdate, $val); }
	}
	if(isset($_POST['new_enddate'])){
		foreach($_POST['new_enddate'] as $val){ array_push($new_enddate, $val); }
	}
	if(isset($_POST['new_value'])){
		foreach($_POST['new_value'] as $val){ array_push($new_value, $val); }
	}
	if(isset($_POST['new_priority'])){
		foreach($_POST['new_priority'] as $val){ array_push($new_priority, $val); }
	}
	if(isset($_POST['new_type'])){
		foreach($_POST['new_type'] as $val){ array_push($new_type, $val); }
	}
	
	$tarr = array('base','private_sales','override');

	if(count($id)>0 and count($startdate)>0 and count($enddate)>0 and count($value)>0 and count($priority)>0 and count($type)>0){
		foreach($id as $key => $val){
			if(in_array($type[$key], $tarr)){
				if(!($_SESSION['_typeusr']==1 or $_SESSION['_typeusr']==2) and $type[$key]!='override'){
					echo "Invalid Value";
					exit();
				}else{
					try {
						$stmt = $db->prepare("UPDATE `hotelcommission` SET `startdate`=:b, `enddate`=:c, `value`=:d, `priority`=:e, `type`=:f WHERE `commissionoid` = :a");
						$stmt->execute(array(':a' => $val, ':b' => date('Y-m-d',strtotime($startdate[$key])), ':c' => date('Y-m-d',strtotime($enddate[$key])), ':d' => $value[$key], ':e' => $priority[$key], ':f' => $type[$key]));
					}catch(PDOException $ex){
						echo "Invalid Query";
						//print($ex);
						die();
					}	
				}
			}else{
				echo "Invalid Value";
				exit();
			}
		}
	}
	
	if(count($new_startdate)>0 and count($new_enddate)>0 and count($new_value)>0 and count($new_priority)>0 and count($new_type)>0){
		foreach($new_startdate as $key => $val){
			if(in_array($new_type[$key], $tarr)){
				if(!($_SESSION['_typeusr']==1 or $_SESSION['_typeusr']==2) and $new_type[$key]!='override'){
					echo "Invalid Value";
					exit();
				}else{
					try {
						$stmt = $db->prepare("INSERT INTO `hotelcommission`(`hoteloid`, `startdate`, `enddate`, `value`, `priority`, `type`) VALUES (:a,:b,:c,:d,:e,:f)");
						$stmt->execute(array(':a' => $hoteloid, ':b' => date('Y-m-d',strtotime($new_startdate[$key])), ':c' => date('Y-m-d',strtotime($new_enddate[$key])), ':d' => $new_value[$key], ':e' => $new_priority[$key], ':f' => $new_type[$key]));
					}catch(PDOException $ex) {
						echo "Invalid Query";
						//print($ex);
						die();
					}	
				}
			}else{
				echo "Invalid Value";
				exit();
			}
		}
	}
	
	echo "1";
?>