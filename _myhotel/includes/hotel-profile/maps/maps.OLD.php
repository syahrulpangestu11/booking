<section class="content-header">
    <h1>
        Maps
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-laptop"></i> Settings</a></li>
        <li>Hotel Profile</li>
        <li class="active">Maps</li>
    </ol>
</section>
<section class="content" id="maps">
	<div class="row">
        <div class="box box-form">
            <div id="data-box" class="box-body">
                <h1>Maps</h1>
                <form method="post" enctype="multipart/form-data" id="data-input" action="#">
                <input type="hidden" name="hoteloid" value="<?php echo $hoteloid; ?>">
                <div class="form-input">
                    <div class="side-left">
                        <label>Latitude</label>
                        <input type="text" class="input-text" name="latitude" value="<?php echo $latitude; ?>">
                        <label>Longitude</label>
                        <input type="text" class="input-text" name="longitude" value="<?php echo $longitude; ?>">
                        <button type="button" class="submit">Save</button>
                    </div>
                    <div class="side-right"><div id="location" style="width: 500px; height: 400px;"></div>	</div>
                    <div class="clear"></div>
                </div>
                </form>
        </div>
   		</div>
    </div>
</section>

<script type="text/javascript">
$(function(){

	$('#location').locationpicker({
		location: {latitude: <?php echo $latitude; ?>, longitude:  <?php echo $longitude; ?>},	
		radius: 0,
		inputBinding: {
			latitudeInput: $('input[name=latitude]'),
			longitudeInput: $('input[name=longitude]')
		}
	});
	var marker = new google.maps.Marker({
    position: myLatlng,
    title:"Hello World!"
});

google.maps.event.addListener(map, "idle", function(){
    marker.setMap(map);
});
	/*$('#maps').on('.ui-tabs-active', function(){ 
		$('#location').locationpicker('autosize');
	});*/
});
</script>