<form method="post" enctype="multipart/form-data" id="data-input" action="#">
    <input type="hidden" name="hoteloid" value="<?php echo $hoteloid; ?>">
    <ul class="inline-half form-input">
        <li>
            <div class="side-left"><label>Airport Transfer Fee</label></div>
            <div class="side-right"><input type="text" class="input-text" name="airport_transfer_fee" value="<?php echo $airport_transfer_fee; ?>"></div>
            <div class="clear"></div>
        </li>
        <li>
            <div class="side-left"><label>Distance from City Center</label></div>
            <div class="side-right"><input type="text" class="input-text" name="distance_from_city" value="<?php echo $distance_from_city; ?>"></div>
            <div class="clear"></div>
        </li>
        <li>
            <div class="side-left"><label>Distance from Airport</label></div>
            <div class="side-right"><input type="text" class="input-text" name="distance_from_airport" value="<?php echo $distance_from_airport; ?>"></div>
            <div class="clear"></div>
        </li>
        <li>
            <div class="side-left"><label>Time to Airport (Minutes)</label></div>
            <div class="side-right"><input type="text" class="input-text" name="time_to_airport" value="<?php echo $time_to_airport; ?>"></div>
            <div class="clear"></div>
        </li>
        <li>
            <div class="side-left"><label>Room Voltage</label></div>
            <div class="side-right"><input type="text" class="input-text" name="roomvoltage" value="<?php echo $roomvoltage; ?>"></div>
            <div class="clear"></div>
        </li>
        <li>
            <div class="side-left"><label>Check-In From</label></div>
            <div class="side-right"><input type="text" class="input-text" name="checkin_from" value="<?php echo $checkin_from; ?>"></div>
            <div class="clear"></div>
        </li>
        <li>
            <div class="side-left"><label>Check-In Until</label></div>
            <div class="side-right"><input type="text" class="input-text" name="checkin_until" value="<?php echo $checkin_until; ?>"></div>
            <div class="clear"></div>
        </li>
        <li>
            <div class="side-left"><label>Check-Out From</label></div>
            <div class="side-right"><input type="text" class="input-text" name="checkout_from" value="<?php echo $checkout_from; ?>"></div>
            <div class="clear"></div>
        </li>
        <li>
            <div class="side-left"><label>Check-Out Until</label></div>
            <div class="side-right"><input type="text" class="input-text" name="checkout_until" value="<?php echo $checkout_until; ?>"></div>
            <div class="clear"></div>
        </li>
        <li>
            <div class="side-left"><label>Internet Usage Fee</label></div>
            <div class="side-right"><input type="text" class="input-text" name="internet_fee" value="<?php echo $internet_fee; ?>"></div>
            <div class="clear"></div>
        </li>
        <li>
            <div class="side-left"><label>Non-Smoking Rooms / Floors</label></div>
            <div class="side-right"><input type="text" class="input-text" name="non_smoking_room" value="<?php echo $non_smoking_room; ?>"></div>
            <div class="clear"></div>
        </li>
        <li>
            <div class="side-left"><label>Breakfast Charge (when not included in room rate)</label></div>
            <div class="side-right"><input type="text" class="input-text" name="breakfast_charge" value="<?php echo $breakfast_charge; ?>"></div>
            <div class="clear"></div>
        </li>
        <li>
            <div class="side-left"><label>Number of Bars</label></div>
            <div class="side-right"><input type="text" class="input-text" name="number_of_bars" value="<?php echo $number_of_bars; ?>"></div>
            <div class="clear"></div>
        </li>
        <li>
            <div class="side-left"><label>Number of Floors</label></div>
            <div class="side-right"><input type="text" class="input-text" name="number_of_floors" value="<?php echo $number_of_floors; ?>"></div>
            <div class="clear"></div>
        </li>
        <li>
            <div class="side-left"><label>Number of restaurants</label></div>
            <div class="side-right"><input type="text" class="input-text" name="number_of_restaurants" value="<?php echo $number_of_restaurants; ?>"></div>
            <div class="clear"></div>
        </li>
        <li>
            <div class="side-left"><label>Number of Rooms</label></div>
            <div class="side-right"><input type="text" class="input-text" name="number_of_rooms" value="<?php echo $number_of_rooms; ?>"></div>
            <div class="clear"></div>
        </li>
        <li>
            <div class="side-left"><label>Parking Fee (per day)</label></div>
            <div class="side-right"><input type="text" class="input-text" name="parking_fee" value="<?php echo $parking_fee; ?>"></div>
            <div class="clear"></div>
        </li>
        <li>
            <div class="side-left"><label>Reception Open Until</label></div>
            <div class="side-right"><input type="text" class="input-text" name="reception_open_until" value="<?php echo $reception_open_until; ?>"></div>
            <div class="clear"></div>
        </li>
        <li>
            <div class="side-left"><label>Year Hotel Built</label></div>
            <div class="side-right"><input type="text" class="input-text" name="hotel_built" value="<?php echo $hotel_built; ?>"></div>
            <div class="clear"></div>
        </li>
        <li>
            <div class="side-left"><label>Year Hotel Renovated</label></div>
            <div class="side-right"><input type="text" class="input-text" name="hotel_renovated" value="<?php echo $hotel_renovated; ?>"></div>
            <div class="clear"></div>
        </li>
        <li>
            <div class="side-left">&nbsp;</div>
            <div class="side-right"><button type="button" class="submit">Save</button></div>
            <div class="clear"></div>
        </li>
        <div class="clear"></div>
    </ul>
</form>
