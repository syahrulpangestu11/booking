<?php
	include("../../../conf/connection.php");
	
	$hoteloid = $_POST['hoteloid'];
	$airport_transfer_fee	=	$_POST['airport_transfer_fee'];
	$distance_from_city	=	$_POST['distance_from_city'];
	$distance_from_airport	=	$_POST['distance_from_airport'];
	$time_to_airport	=	$_POST['time_to_airport'];
	$roomvoltage	=	$_POST['roomvoltage'];
	$checkin_from	=	$_POST['checkin_from'];
	$checkin_until	=	$_POST['checkin_until'];
	$checkout_from	=	$_POST['checkout_from'];
	$checkout_until	=	$_POST['checkout_until'];
	$internet_fee	=	$_POST['internet_fee'];
	$non_smoking_room	=	$_POST['non_smoking_room'];
	$breakfast_charge	=	$_POST['breakfast_charge'];
	$number_of_bars	=	$_POST['number_of_bars'];
	$number_of_floors	=	$_POST['number_of_floors'];
	$number_of_restaurants	=	$_POST['number_of_restaurants'];
	$number_of_rooms	=	$_POST['number_of_rooms'];
	$parking_fee	=	$_POST['parking_fee'];
	$reception_open_until	=	$_POST['reception_open_until'];
	$hotel_built	=	$_POST['hotel_built'];
	$hotel_renovated	=	$_POST['hotel_renovated'];
	
	try {
		$stmt = $db->prepare("UPDATE `hotel` SET `airport_transfer_fee`=:a , `distance_from_city`=:b , `distance_from_airport`=:c , `time_to_airport`=:d , `roomvoltage`=:e , `checkin_from`=:f , `checkin_until`=:g , `checkout_from`=:h , `checkout_until`=:i , `internet_fee`=:j , `non_smoking_room`=:k , `breakfast_charge`=:l , `number_of_bars`=:m , `number_of_floors`=:n , `number_of_restaurants`=:o , `number_of_rooms`=:p , `parking_fee`=:q , `reception_open_until`=:r , `hotel_built`=:s , `hotel_renovated`=:t WHERE `hotel`.`hoteloid` =:hoid;");
		$stmt->execute(array(':a' => $airport_transfer_fee , ':b' => $distance_from_city , ':c' => $distance_from_airport , ':d' => $time_to_airport , ':e' => $roomvoltage , ':f' => $checkin_from , ':g' => $checkin_until , ':h' => $checkout_from , ':i' => $checkout_until , ':j' => $internet_fee , ':k' => $non_smoking_room , ':l' => $breakfast_charge , ':m' => $number_of_bars , ':n' => $number_of_floors , ':o' => $number_of_restaurants , ':p' => $number_of_rooms , ':q' => $parking_fee , ':r' => $reception_open_until , ':s' => $hotel_built , ':t' => $hotel_renovated , ':hoid' => $hoteloid));
		$affected_rows = $stmt->rowCount();
	}catch(PDOException $ex) {
		echo "0";
		die();
	}
	echo "1";

	
?>