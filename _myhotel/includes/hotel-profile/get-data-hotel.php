<?php
	try {
		$stmt = $db->query("select h.*, ht.category, c.name as chainname, city.stateoid, state.countryoid from hotel h inner join hoteltype ht using (hoteltypeoid) left join chain c using (chainoid) left join city using(cityoid) left join state using(stateoid) where hoteloid = '".$hoteloid."'");
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
			$r_hotel = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_hotel as $row){
				$hotelname = $row['hotelname'];
				$hotelcode = $row['hotelcode'];
				$chainoid = $row['chainoid'];
				$chainname = $row['chainname'];
				$hoteltype = $row['hoteltypeoid'];
				$star = $row['stars'];
				$address = $row['address'];
				$latitude = $row['latitude'];
				$longitude = $row['longitude'];
				$cityoid = $row['cityoid'];
				$stateoid = $row['stateoid'];
				$countryoid = $row['countryoid'];
				$hotelemail = $row['email'];
				$hotelphone = $row['phone'];
				$hotelfax = $row['fax'];
				$hotelwebsite = $row['website'];
				$hotelhtlurl = $row['htl_url'];
				$hotelbeurl = $row['be_url'];
				$starthotelprice = $row['starthotelprice'];

				$socialmedia_url = $row['socialmedia_url'];
				$fblike_script = $row['fblike_script'];

				$description = $row['description'];
				$policy = $row['policy'];
				$postalcode = $row['postalcode'];

				$airport_transfer_fee	=	$row['airport_transfer_fee'];
				$distance_from_city	=	$row['distance_from_city'];
				$distance_from_airport	=	$row['distance_from_airport'];
				$time_to_airport	=	$row['time_to_airport'];
				$roomvoltage	=	$row['roomvoltage'];
				$checkin_from	=	$row['checkin_from'];
				$checkin_until	=	$row['checkin_until'];
				$checkout_from	=	$row['checkout_from'];
				$checkout_until	=	$row['checkout_until'];
				$internet_fee	=	$row['internet_fee'];
				$non_smoking_room	=	$row['non_smoking_room'];
				$breakfast_charge	=	$row['breakfast_charge'];
				$number_of_bars	=	$row['number_of_bars'];
				$number_of_floors	=	$row['number_of_floors'];
				$number_of_restaurants	=	$row['number_of_restaurants'];
				$number_of_rooms	=	$row['number_of_rooms'];
				$parking_fee	=	$row['parking_fee'];
				$reception_open_until	=	$row['reception_open_until'];
				$hotel_built	=	$row['hotel_built'];
				$hotel_renovated	=	$row['hotel_renovated'];
			}
		}else{
			echo "No Result";
			die();
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}
?>
