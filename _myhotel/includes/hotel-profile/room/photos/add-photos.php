
            <div class="box-body" id="room-photo">
                <!-- <h1>Add New Photos</h1> -->
                <form method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/hotel-profile/photo/add-process">
                <input type="hidden" name="hoteloid" value="<?php echo $hoteloid; ?>">
                <input type="hidden" name="page_ref" value="<?php echo $base_url."/hotel-profile/rooms"; ?>">

                <table class="table table-fill table-fill-centered table-photos">
                    <tr>
                        <td>Picture</td>
                        <td>Photos of</td>
                        <td>Action</td>
                    </tr>
                    <?php
                    if($uri3=="rooms" and $uri4 != "add"){
                      include("includes/hotel-profile/room/photos/view-photos.php");
                    }
                    ?>
                </table>
                <br /><br />
                <style media="screen">
                  div.photo-upload-notice{
                    font-size: 12px;
                    font-style: italic;
                  }
                </style>
                <div class="photo-upload-notice">*) Please make sure you upload image file type (ex: .jpg , .png , .gif), with image dimensions not larger than 1920 x 1080 pixels, and file size not larger than 200 KB.</div>
                <button type="submit" class="small-button blue">Save</button>
                </form>
			</div>
