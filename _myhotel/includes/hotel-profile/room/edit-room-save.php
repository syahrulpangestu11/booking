<?php
try {
	include("../../../conf/connection.php");

	$roomoid = $_POST['roomoid'];
	$name = $_POST['name'];
	$description = $_POST['description'];
	$size = $_POST['roomsize'];
	if(isset($_POST['roomsize_inc_balcon'])){ $sizeincbalcon = $_POST['roomsize_inc_balcon']; }else{ $sizeincbalcon = "0"; }
	$view = $_POST['view'];
	$published = $_POST['published'];

	// echo "<pre>";
	// print_r($_POST);
	// echo "</pre>";

	$stmt = $db->prepare("UPDATE room SET name=:a , description=:b , roomsize=:c , roomsize_inc_balcon=:d, viewoid=:e , publishedoid=:f WHERE roomoid=:roid");
	$stmt->execute(array(':a' => $name, ':b' => $description, ':c' => $size, ':d' => $sizeincbalcon, ':e' => $view , ':f' => $published , ':roid' => $roomoid));
	$affected_rows = $stmt->rowCount();

	/*****************************************************/
	if(!empty($_POST['bed'])){
		if(is_array($_POST['bed'])){
			$list_roombed = implode("','", $_POST['bed']);
		}else{
			$list_roombed = $_POST['bed'];
		}

		$stmt = $db->prepare("delete from roombed where roomoid = :roid and bedoid not in ('".$list_roombed."')");
		$stmt->execute(array(':roid' => $roomoid));

		foreach($_POST['bed'] as $key => $bed){
			$stmt = $db->prepare("select count(roomoid) as foundbed from roombed where roomoid = :roid and bedoid = :a");
			$stmt->execute(array(':roid' => $roomoid, ':a' => $bed));
			$data_bed = $stmt->fetch(PDO::FETCH_ASSOC);

			if($data_bed['foundbed'] == 0){
				$stmt = $db->prepare("insert into roombed (`roomoid`, `bedoid`, `publishedoid`) values (:roid , :a , :b)");
				$stmt->execute(array(':roid' => $roomoid, ':a' => $bed, ':b' => '1'));
			}
		}
	}else{
		$stmt = $db->prepare("delete from roombed where roomoid = :roid");
		$stmt->execute(array(':roid' => $roomoid));
	}

	/*****************************************************/
	$fcltype = '3';
	if(!empty($_POST['roomfcl'])){
		if(is_array($_POST['roomfcl'])){
			$list_roomfcl = implode("','", $_POST['roomfcl']);
		}else{
			$list_roomfcl = $_POST['roomfcl'];
		}

		$stmt = $db->prepare("delete from roomfacilities where roomoid = :roid and facilitiesoid not in ('".$list_roomfcl."')");
		$stmt->execute(array(':roid' => $roomoid));

		foreach ($_POST['roomfcl'] as $key => $facilities){
			$stmt = $db->prepare("select count(roomfacilitiesoid) as foundfcl from roomfacilities where roomoid = :roid and facilitiesoid = :a");
			$stmt->execute(array(':roid' => $roomoid, ':a' => $facilities));
			$data_fcl = $stmt->fetch(PDO::FETCH_ASSOC);

			if($data_fcl['foundfcl'] == 0){
				$stmt = $db->prepare("insert into roomfacilities (`roomoid`, `facilitiesoid`, `publishedoid`) values (:roid , :a , :b)");
				$stmt->execute(array(':roid' => $roomoid, ':a' => $facilities, ':b' => '1'));
			}
		}
	}else{
		$stmt = $db->prepare("delete rf from roomfacilities rf inner join facilities using (facilitiesoid) where roomoid=:roid");
		$stmt->execute(array(':roid' => $roomoid));
	}


	// echo "---".$_POST['rt']."---";
	//$_GET['rt'] = $roomoid;
	include("room-settings/edit-room-settings-save.php");

	echo "1";

}catch(Exception $ex) {
	echo $ex->getMessage();
	die();
}
?>
