<?php
	if(isset($_POST['tabca'])){
		try {
			$tabca = (isset($_POST['tabca'])) ? $_POST['tabca'] : "n";
			$show = (isset($_POST['show'])) ? $_POST['show'] : array();

			$stmt = $db->prepare("update hotel set show_bar = :a where hoteloid = :id");
			$stmt->execute(array(':a' => $tabca, ':id' => $_SESSION['_hotel']));

			foreach ($show as $key => $value) {
				$stmt = $db->prepare("update roomoffer, room set show_bar = :a where roomoffer.roomoid=room.roomoid and hoteloid = :id and roomofferoid = :idro");
				$stmt->execute(array(':a' => $value, ':id' => $_SESSION['_hotel'], ':idro' => $key));
			}
		}catch(PDOException $ex) {
			echo "Invalid Query"; //print($ex);
			die();
		}
	}

	header("Location: ". $base_url ."/booking-enginee-appearance");

?>
