<?php
	$hotelname = $_REQUEST['hotelname'];
	$chainoid = $_REQUEST['chainoid'];
	$country = $_REQUEST['country'];
	$state = $_REQUEST['state'];
	$city = $_REQUEST['city'];
?>
<section class="content-header">
    <h1>
       	Contact Management All
    </h1>
    <ol class="breadcrumb">
        <li class="active"><a href="#"><i class="fa fa-table"></i> Hotel</a></li>
    </ol>
</section>
<section class="content">

    <div class="row">
        <div class="box box-form">
            <div class="box-body">
                <div class="form-group">
            <form method="get" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/contact-management-all/">
							<input type="hidden" name="dfltstate" value="0">
            	<input type="hidden" name="dfltcity" value="0">
							<input type="hidden" name="page">
                    <label>Property Name  :</label> &nbsp;&nbsp;
										<input type="text" name="hotelname" class="input-text" value="<?php echo $hotelname; ?>">
                    <label>Chain  :</label> &nbsp;&nbsp;
                    <select name="chainoid" class="input-select">
                    	<option value="">show all</option>
                    <?php
                    try {
                        $stmt = $db->query("SELECT * from chain ORDER BY name");
                        $r_hoteltype = $stmt->fetchAll(PDO::FETCH_ASSOC);
                        foreach($r_hoteltype as $row){
                            $chain_selected = ($chainoid == $row['chainoid']) ? ' selected="selected" ' : '' ;
                    ?>
                        <option value="<?php echo $row['chainoid']; ?>" <?=$chain_selected;?>><?php echo $row['name']; ?></option>
                    <?php
                        }
                    }catch(PDOException $ex) {
                        echo "Invalid Query";
                        die();
                    }
                    ?>
                    </select>
                    <!-- <label>Star  :</label> &nbsp;&nbsp;
					<input type="text" name="star" class="input-text small" value="<?php echo $star; ?>"> -->
					<!-- <br /><br />     -->
                    <!--
                    <label>Country  :</label> &nbsp;&nbsp;
                    <select name="country" class="input-select">
                    	<option value="">show all</option>
                        <?php //getCountry($country); ?>
                    </select>
                    <span class="loc-state"></span>
                    <span class="loc-city"></span>
                    -->
                    <!-- <br /><br />    -->
                    <button type="submit" class="small-button blue"><i class="fa fa-search"></i> Find</button>
                </form>
                </div>
            </div><!-- /.box-body -->
       </div>
    </div>

    <div class="row">
        <div class="box">
        	<form method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/room-control/process">
                <div id="data-box" class="box-body">
                    <div class="loader">Loading...</div>
                </div><!-- /.box-body -->
            </form>
       </div>
    </div>
</section>
