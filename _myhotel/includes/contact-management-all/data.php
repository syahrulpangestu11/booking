<?php
	error_reporting(E_ALL & ~E_NOTICE);
	include("../../conf/connection.php");
	include("../paging/pre-paging.php");

	$usr = $_REQUEST['usr'];
	$useroid = $_REQUEST['useroid'];
	
	$q_user	= $db->query("select userstypeoid from users where useroid = '".$useroid."'");
	$user	= $q_user->fetch(PDO::FETCH_ASSOC);
	


	/*if($_SESSION['_typeusr'] == "4"){
		$main_query =
			"SELECT c.cityname, s.statename, h.hoteloid, h.hotelname, h.stars, h.publishedoid, (case when h.publishedoid = 2 then 'status-red' when h.publishedoid = 1 then 'status-green' else 'status-black' end) as status, ht.category
			from hotel h inner join hoteltype ht using (hoteltypeoid) inner join published using (publishedoid)
			inner join city c using (cityoid) inner join state s using (stateoid) inner join country ct using (countryoid)
			inner join
		";
	}else{*/
		$main_query =
		"SELECT DISTINCT hc.name, hc.phone, hc.email, h.hotelname, mt.mailertype from hotelcontact hc INNER JOIN hotel h USING (hoteloid) inner join chain USING (chainoid) inner join mailertype mt USING (mailertypeoid)
		";
	// }

	$filter = array();
	if(isset($_REQUEST['hotelname']) and $_REQUEST['hotelname']!=''){
		array_push($filter, 'h.hotelname like "%'.$_REQUEST['hotelname'].'%"');
	}
	if(isset($_REQUEST['chainoid']) and $_REQUEST['chainoid']!=''){
		array_push($filter, 'h.chainoid = "'.$_REQUEST['chainoid'].'"');
	}
	if(isset($_REQUEST['star']) and $_REQUEST['star']!=''){
		array_push($filter, 'h.stars = "'.$_REQUEST['star'].'"');
	}
	if(isset($_REQUEST['country']) and $_REQUEST['country']!=''){
		array_push($filter, 'ct.countryoid = "'.$_REQUEST['country'].'"');
	}

	// array_push($filter, 'h.publishedoid not in (3) and h.hotelstatusoid IN (1,10) ');
	array_push($filter, 'hc.publishedoid in (1) ');

	//--- Filter CHAIN
	/*/
	if($usr == "4"){
		$s_chain = "SELECT ua.oid FROM users u INNER JOIN userassign ua USING (useroid) WHERE ua.type = 'chainoid' AND u.useroid = '".$useroid."'";
		// echo $s_chain;
		$q_chain = $db->query($s_chain);
		$n_chain = $q_chain->rowCount();
		$r_chain = $q_chain->fetchAll(PDO::FETCH_ASSOC);
		foreach($r_chain as $row){
			array_push($filter, "h.chainoid = '".$row['oid']."'");
			// array_push($filter, 'h.hotelname LIKE "%amaz%" ');
		}
	}
	//*/

	if(count($filter) > 0){
		$combine_filter = implode(' and ',$filter);
		$query = $main_query.' where '.$combine_filter;
	}else{
		$query = $main_query;
	}

	try {
		$main_query = $query." ORDER BY h.hotelname ASC ";
		$stmt = $db->query($main_query.$paging_query);
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
			// echo $main_query;
?>
<table class="table promo-table">
	<tr>
			<td>Property Name</td>
			<td>Name</td>
			<td>Email</td>
			<td>Phone Number</td>
	</tr>
<?php
		$r_hotel = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_hotel as $row){
				$hoteloid = $row['hoteloid'];
?>
    <tr class="<?php echo $row['status']; ?>">
        <td><?php echo $row['hotelname']; ?></td>
		<td><?php echo $row['name'];?></td>
        <td style="text-transform: lowercase;"><?php echo $row['mailertype']." : ".$row['email'];?></td>
		<td><?php echo $row['phone']; ?></td>
		<?php /*/ ?>
		<td class="algn-right">
        <?php if($user['userstypeoid'] != '4' and $user['userstypeoid'] != '3' ){ ?>
        <button type="button" class="pencil edit-button" hoid="<?php echo $hoteloid; ?>">Edit</button>
        <?php } ?>
		<?php if($usr == "1"){ ?>
        <!-- <button type="button" class="trash delete-button" hoid="<?php echo $hoteloid; ?>">Delete</button> -->
        <?php } ?>
        <button type="button" class="small-button blue manage" hoid="<?php echo $hoteloid; ?>">manage</button>
		</td>
		<?php //*/ ?>
    </tr>
<?php
			}
?>
</table>
<?php
		}

		// --- PAGINATION part 2 ---
		$main_count_query =
		"SELECT COUNT(DISTINCT hc.name, hc.phone, hc.email, h.hotelname, mt.mailertype) as jml from hotelcontact hc INNER JOIN hotel h USING (hoteloid) inner join chain USING (chainoid) inner join mailertype mt USING (mailertypeoid)
		";
		if(count($filter) > 0){
			$combine_filter = implode(' and ',$filter);
			$main_count_query = $main_count_query.' where '.$combine_filter;
		}
		$stmt = $db->query($main_count_query);
		$jmldata = $stmt->fetchColumn();

		$tampildata = $row_count;
		include("../paging/post-paging.php");

	}catch(PDOException $ex) {
		echo "Invalid Query";
		print($ex);
		die();
	}
?>
