<?php
include_once("includes/bootstrap.php");

	$name = $_REQUEST['name'];
	// $star = $_REQUEST['star'];
	$type = $_REQUEST['type'];
	// $country = $_REQUEST['country'];
	// $state = $_REQUEST['state'];
    // $city = $_REQUEST['city'];
    
    $categoryList = array(
		"agent" => "Agent",
		"hotel" => "Hotel",
		// "vendor" => "Vendor",
	);
?>
<section class="content-header">
    <h1>
       	Contacts
    </h1>
    <ol class="breadcrumb">
        <li class="active"><a href="#"><i class="fa fa-table"></i> Contacts</a></li>
    </ol>
</section>
<section class="content">
    <!-- <div class="row">
        <div class="box">
        asd
        </div>
    </div> -->

    <div class="row">
        <div class="box box-form">
            <div class="box-body">
                <div class="form-group">
            <form method="get" id="data-input" action="<?php echo $base_url; ?>/contacts/">
							<input type="hidden" name="dfltstate" value="0">
            	            <input type="hidden" name="dfltcity" value="0">
							<input type="hidden" name="page">
                    <label>Name  :</label> &nbsp;&nbsp;
                    <input type="text" name="name" class="input-text" value="<?php echo $name; ?>">
                    <label>Category  :</label> &nbsp;&nbsp;
                    <select name="category" class="input-select">
                    	<option value="">show all</option>
                        <?php 
                        foreach ($categoryList as $key => $value) {
                            $selected = ($_REQUEST['category']==$key) ? ' selected="selected" ' : '' ;
                            echo '<option value="'.$key.'" '.$selected.'>'.$value.'</option>';
                        }
                        ?>
                    </select>
                    <label>Property Type  :</label> &nbsp;&nbsp;
                    <select name="type" class="input-select">
                    	<option value="">show all</option>
                        <?php
                        try {
                            $stmt = $db->query("select * from hoteltype");
                            $r_hoteltype = $stmt->fetchAll(PDO::FETCH_ASSOC);
                            foreach($r_hoteltype as $row){
                                $selected = ($row['hoteltypeoid']==$_REQUEST['type']) ? ' selected="selected" ' : '' ;
                                ?>
                                <option value="<?php echo $row['hoteltypeoid']; ?>" <?=$selected;?> ><?php echo $row['category']; ?></option>
                                <?php
                            }
                        }catch(PDOException $ex) {
                            echo "Invalid Query";
                            die();
                        }
                        ?>
                    </select>
                    <!-- <label>Star  :</label> &nbsp;&nbsp;
					<input type="text" name="star" class="input-text small" value="<?php echo $star; ?>"> -->

                    <label>SM Status  :</label> &nbsp;&nbsp;
                    <select name="ahs" class="input-select">
                    	<option value="">show all</option>
                        <?php
                        try {
                            $stmt = $db->query("select * from affiliatehotelstatus");
                            $r_ahs = $stmt->fetchAll(PDO::FETCH_ASSOC);
                            foreach($r_ahs as $row){
                                $selected = ($row['affiliatehotelstatusoid']==$_REQUEST['ahs']) ? ' selected="selected" ' : '' ;
                                ?>
                                <option value="<?php echo $row['affiliatehotelstatusoid']; ?>" <?=$selected;?> ><?php echo $row['status']; ?></option>
                                <?php
                            }
                        }catch(PDOException $ex) {
                            echo "Invalid Query";
                            die();
                        }
                        ?>
                    </select>
                    <button type="submit" class="small-button blue"><i class="fa fa-search"></i> Find</button>
                </form>
                </div>
            </div><!-- /.box-body -->
       </div>
    </div>

    <div class="row">
        <div class="box">
        	<form method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/room-control/process">
                <div id="data-box" class="box-body">
                    <div class="loader">Loading...</div>
                </div><!-- /.box-body -->
            </form>
       </div>
    </div>
</section>
