<?php
	error_reporting(E_ALL & ~E_NOTICE);
	include("../../conf/connection.php");
	include("../paging/pre-paging.php");

	$usr = $_REQUEST['usr'];
	$useroid = $_REQUEST['useroid'];
	// echo "<pre>";
	// print_r($_REQUEST);
	// echo "</pre>";
	$q_user	= $db->query("select userstypeoid from users where useroid = '".$useroid."'");
	$user	= $q_user->fetch(PDO::FETCH_ASSOC);
	

// if($user['userstypeoid'] != '4' and $user['userstypeoid'] != '3' ){
?>
<!-- <button type="button" class="small-button blue add-button">Add Vendor Contact</button> -->
<!-- <br><br> -->
<?php
// }
/*/
?>
<ul class="inline-block this-inline-block" style="
    margin-left: 15px;
    line-height: 34px;
">
	<li><span class="status-green square">sts</span> Active</li>
	<li><span class="status-black square">sts</span> Inactive</li>
	<li><span class="status-red square">sts</span> Expired</li>
</ul>

<?php
//*/

	/*if($_SESSION['_typeusr'] == "4"){
		$main_query =
			"SELECT c.cityname, s.statename, h.hoteloid, h.hotelname, h.stars, h.publishedoid, (case when h.publishedoid = 2 then 'status-red' when h.publishedoid = 1 then 'status-green' else 'status-black' end) as status, ht.category
			from hotel h inner join hoteltype ht using (hoteltypeoid) inner join published using (publishedoid)
			inner join city c using (cityoid) inner join state s using (stateoid) inner join country ct using (countryoid)
			inner join
		";
	}else{*/

		$s_hotel =
			"SELECT h.hoteloid AS id, h.hotelname AS name, 'hotel' AS category, h.address, h.email, h.phone, hs.status AS hotelstatus, ahs.status AS smstatus, c.name AS ownedby
			from hotel h 
			LEFT JOIN chain c USING (chainoid)
			LEFT join hoteltype ht using (hoteltypeoid) 
			LEFT join hotelstatus hs USING (hotelstatusoid) 
			LEFT JOIN affiliatehotelstatus ahs USING (affiliatehotelstatusoid)
		";
		$s_agent = "SELECT a.agentoid AS id, a.agentname AS name, 'agent' AS category, a.address, a.email, a.phone, '' AS hotelstatus, '' AS smstatus, 
			-- h.hotelname AS ownedby
			'' AS ownedby
			FROM agent a 
			LEFT JOIN agenthotel ah USING (agentoid)
			LEFT JOIN hotel h ON (ah.hoteloid = h.hoteloid)
		";
	// }

	$filterA = array();
	$filterH = array();
	$filterV = array();
	if(isset($_REQUEST['name']) and $_REQUEST['name']!=''){
		array_push($filterH, 'h.hotelname like "%'.$_REQUEST['name'].'%"');
		array_push($filterA, 'a.agentname like "%'.$_REQUEST['name'].'%"');
	}
	if(isset($_REQUEST['type']) and $_REQUEST['type']!=''){
		array_push($filterH, 'h.hoteltypeoid = "'.$_REQUEST['type'].'"');
	}
	// if(isset($_REQUEST['star']) and $_REQUEST['star']!=''){
	// 	array_push($filter, 'h.stars = "'.$_REQUEST['star'].'"');
	// }
	if(isset($_REQUEST['ahs']) and $_REQUEST['ahs']!=''){
		array_push($filterH, 'h.affiliatehotelstatusoid = "'.$_REQUEST['ahs'].'"');
	}

	array_push($filterH, 'h.publishedoid not in (3) ');
	array_push($filterA, 'a.publishedoid not in (3) ');
	

	if(count($filterH) > 0 or count($filterA) > 0 ){
		$combine_filterH = implode(' and ',$filterH);
		$s_hotel .= " where ".$combine_filterH;
	
		$combine_filterA = implode(' and ',$filterA);
		$s_agent .= " where ".$combine_filterA;
	}

	
	if($_REQUEST['category']=='hotel'){
		$query = $s_hotel;
	}else if($_REQUEST['category']=='agent'){
		$query = $s_agent;
	}else if($_REQUEST['category']=='vendor'){
		$query = $s_vendor;
	}else{
		$query = $s_hotel." UNION ".$s_agent;
	}
	

	try {
		$main_query = $query." ORDER BY name ASC "; //h.hotelstatusoid DESC, h.publishedoid DESC,
		// echo "<pre>";
		// print_r($main_query);
		// echo "</pre>";
		$stmt = $db->query($main_query.$paging_query);
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
?>
<!-- <h1><b>Contacts List</b></h1> -->
<table class="table promo-table" style="margin-top: 0; font-size: 13px;">
	<tr>
			<td style="width: 15%;">Company Name</td>
			<td>Category</td>
			<td style="width: 15%;">Address</td>
			<td>Email</td>
			<td>Phone</td>
			<td>Owned By</td>
			<td>Statuses</td>
			<td style="width: 8%;" class="algn-right">&nbsp;</td>
	</tr>
<?php
		$r_main = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_main as $row){
				$statuses = ($row['category']=='hotel') ? "Status: <b>".$row['hotelstatus']."</b><br> SM Status: <b>".$row['smstatus']."</b>" : "" ;
				if($row['category']=='hotel'){
					$category = '<span class="label label-danger">'.$row['category'].'</span>';
					$sContact =
					"SELECT hc.name, hc.phone, hc.email, mt.mailertype from hotelcontact hc INNER JOIN hotel h USING (hoteloid) inner join mailertype mt USING (mailertypeoid) WHERE hc.hoteloid = '".$row['id']."' AND hc.publishedoid in (1) 
					";
					$stmt2 = $db->query($sContact);
					$row_count2 = $stmt2->rowCount();
					$email = "";
					$phone = "";
					if($row_count2 > 0) {
						$r_hotel2 = $stmt2->fetchAll(PDO::FETCH_ASSOC);
						foreach($r_hotel2 as $row2)	{
							$textEmail = !empty($row2['email']) ? $row2['name'].' - '.$row2['mailertype']." : <span style='text-transform: lowercase;'>".$row2['email']."</span><br>" : "" ;
							$textPhone = !empty($row2['phone']) ? $row2['phone']."<br>" : "" ;
							$email .= $textEmail;
							$phone .= $textPhone;
						}
					}
					$editURL = "/contact-management/?ho=".$row['id']."&hn=".$row['name']."&ref=contacts";
					$editHotelURL = "/hotel/edit/?ho=".$row['id']."&ref=contacts";
					$ownedby = $row['ownedby'];
				}else if($row['category']=='agent'){
					$category = '<span class="label label-success">'.$row['category'].'</span>';
					$email =  "<span style='text-transform: lowercase;'>".$row['email']."</span>";
					$phone = $row['phone'];

					$sAgentHotel = "SELECT ah.agenthoteloid, h.hotelname FROM agenthotel ah LEFT JOIN hotel h using (hoteloid) LEFT JOIN agent a USING (agentoid) WHERE ah.agentoid = '".$row['id']."' ORDER BY hotelname";
					$stmt2 = $db->query($sAgentHotel);
					$row_count2 = $stmt2->rowCount();
					if($row_count2 > 0) {
						$r_hotel2 = $stmt2->fetchAll(PDO::FETCH_ASSOC);
						$hotelname = array();
						foreach($r_hotel2 as $row2)	{
							array_push($hotelname, $row2['hotelname']);
							$aho = $row2['agenthoteloid'];
						}
						$ownedby = implode(", ",$hotelname);
					}
					$editURL = "/agent/edit/?aho=".$aho;
				}
				
?>
    <tr class="<?=$row['status']; ?>">
        <td><?=$row['name'];?></td>
		<td><?=$category;?></td>
		<td><?=$row['address'];?></td>
		<td nowrap><?=$email;?></td>
		<td nowrap><?=$phone;?></td>
		<td><?=$ownedby;?></td>
		<td style="font-size: 12px;"><?=$statuses;?></td>
        <td class="algn-right">
			<!-- <a class="btn btn-primary edit-contact" href="<?=$editURL;?>" title="Edit Contact"><i class="fa fa-pencil"></i></button> -->
			<!-- <button type="button" class="btn btn-primary edit-contact" href="<?=$editURL;?>" title="Edit Contact"><i class="fa fa-pencil"></i></button> -->
			<?php
			//*/
			if($row['category']=='hotel'){
				?>
				<button type="button" class="btn btn-warning edit-contact" href="<?=$editURL;?>" title="Edit Contact"><i class="fa fa-user"></i></button>
				<button type="button" class="btn btn-primary edit-hotel" href="<?=$editHotelURL;?>" title="Edit Hotel"><i class="fa fa-h-square"></i></button>
				<?php
			}else if($row['category']=='agent'){
				?>
				<!-- <button type="button" class="btn btn-warning edit-contact" hoid="<?=$hoteloid;?>" hname="<?=$hotelname;?>" title="Edit Agent"><i class="fa fa-pencil"></i></button> -->
				<?php
			}else if($row['category']=='vendor'){
				?>
				<!-- <button type="button" class="btn btn-danger edit-contact" hoid="<?=$hoteloid;?>" hname="<?=$hotelname;?>" title="Edit Agent"><i class="fa fa-pencil"></i></button> -->
				<?php
			} //*/ ?>
        </td>
    </tr>
<?php
			}
?>
</table>
<?php
		}

		// --- PAGINATION part 2 ---
		$main_count_query = "SELECT count(*) as jml from ($main_query) allrows";
		// echo $main_count_query;
		if(count($filter) > 0){
			$combine_filter = implode(' and ',$filter);
			$main_count_query = $main_count_query.' where '.$combine_filter;
		}
		$stmt = $db->query($main_count_query);
		$jmldata = $stmt->fetchColumn();

		$tampildata = $row_count;
		include("../paging/post-paging.php");

	}catch(PDOException $ex) {
		echo "Invalid Query";
		print($ex);
		die();
	}
?>
