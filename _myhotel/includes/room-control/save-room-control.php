<script type="text/javascript">
$(function(){
   $("#dialog").dialog({
      autoOpen: false,
	  buttons: { "Ok": redirect },
	  close: function(event, ui) { redirect }
   });
   
   function redirect(){
	   document.location.href = '<?=$base_url?>/room-control/?rt=<?=$_POST['roomoffer']?>&channel=<?=$_POST['channeloid']?>&startdate=<?=$_POST['startdate']?>&enddate=<?=$_POST['enddate']?>';
   }
});
</script>

<?php
	$roomoid = $_POST['roomoid'];
	$roomoffer = $_POST['roomoffer'];
	$channellist = array($_POST['channeloid']);

	$date = array();
	foreach($_POST as $key => $value){
		if(substr($key,0,5) == 'date-'){ $date[$key] = $value; }
	}
	
	try{
		include('new-upload-xml.php');
?>
    <div id="dialog" title="Confirmation Notice">
      <p>Data succesfully added</p>
    </div>
    <script type="text/javascript">
    $(function(){
        $( document ).ready(function() {
            $("#dialog").dialog("open");
        });
    });
    </script>
    <?php
	}catch(PDOException $ex) {
		echo "[ERROR]<br>".$ex;
	?>
    <div id="dialog" title="Error Notification">
      <p>We&rsquo;re very sorry, we can't save your data right now.</p>
    </div>
    <script type="text/javascript">
    $(function(){
        $( document ).ready(function() {
            $("#dialog").dialog("open");
        });
    });
    </script>
<?php
	}
?>