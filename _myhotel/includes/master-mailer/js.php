<script type="text/javascript">
$(function(){
  // AJAX submitDataCallback
  function submitDataCallback(url, forminput){
    // alert("fi="+forminput.serialize());
    $.ajax({
      url: url,
      type: 'post',
      data: forminput.serialize(),
      success: function(data) {
        if(data == "1"){
          $dialogNotice.html("Data succesfully updated");
          $dialogNotice.dialog("open");
        }else{
          $dialogNotice.html("We&rsquo;re very sorry, we can't save your data right now. Error = "+data);
          // $dialogNotice.html("We&rsquo;re very sorry, we can't save your data right now.");
          $dialogNotice.dialog("open");
        }
      }
    });
  }

  var $dialogNotice = $('<div id="dialog-notice"></div>')
	.dialog({
		autoOpen: false,
		title: 'Notification',
		buttons: {
			"Ok": function(){
				$( this ).dialog( "close" );
			}
		}
	});

  $('button.submit-edit-hc').click(function(){
		url = '<?php echo $base_url; ?>/includes/master-mailer/edit-mailer.php';
		forminput = $(this).closest('form#data-input');
		submitDataCallback(url, forminput);
	});


  // /*
  // TOMBOL ADD RATE PLAN
  $('body').on('click','button.add-hc', function(e) {
		tableTarget = $(this).closest('table.table-fill');
    // parentTable = $(this).closest("table.table-fill");

		name = tableTarget.find('input[name=name]').val();
		email = tableTarget.find('input[name=email]').val();
		phone = tableTarget.find('input[name=phone]').val();
		// reservationemail = tableTarget.find('input[name=reservationemail]:checked').val();
    reservationemail = tableTarget.find('select[name=reservationemail]').val();
		reservationemail_text = tableTarget.find('select[name=reservationemail] option:selected').text();

		if(name.length !== 0){

      tableTarget.append(
        "<tr>"+
          "<td><input type = 'text' class = 'medium' name = 'new_name[]' value = '"+name+"'></td>"+
          "<td><input type = 'text' class = 'medium' name = 'new_email[]' value = '"+email+"'></td>"+
          "<td><input type = 'text' class = 'medium' name = 'new_phone[]' value = '"+phone+"'></td>"+
          "<td class='select_re'></td>"+
          "<td>&nbsp;</td>"+
          "<td><button type='button' class='small-button red del'>Delete</button></td>"+
        "</tr>");

			appendDestination = tableTarget.find('.select_re').last();
			tableTarget.find('select[name=reservationemail]').clone().attr('name','new_reservationemail[]').appendTo(appendDestination);
			newSelect = tableTarget.find('select[name="new_reservationemail[]"]').last();
			newSelect.val(reservationemail);

			$(this).closest('tr').find('input').val('');
			$(this).closest('tr').find('select').prop('seletedIndex', 0);
		}
  });


    // TOMBOL DELETE RATE PLAN
       var $dialogDelete = $('<div id="dialog-del"></div>')
    	.html('Are you sure you want to delete this data?')
    	.dialog({
    		autoOpen: false,
    		title: 'Confirmation',
    		buttons: {
    			"Delete": function(){
    				var elem = $(this).data('elem');
    				$(this).dialog("close");
    				deleteElem(elem);
    			},
    			Cancel: function(){ $( this ).dialog("close"); }
    		}
    	});

    	var $dialogNotification = $('<div id="dialog-notice"></div>')
    	.dialog({
    		autoOpen: false,
    		title: 'Notification',
    		buttons: {
    			"Ok": function(){
    				$(this).dialog( "close" );
    			}
    		}
    	});


    	function deleteAction(url, id){
    		$.ajax({
    			url: url,
    			type: 'post',
    			data: { id : id},
    			success: function(data) {
    				if(data == "1"){
    					$dialogNotification.html("Data has been deleted");
    					$dialogNotification.dialog("open");
    				}else{
    					$dialogNotification.html("We&rsquo;re very sorry, we can't save your data right now");
    					$dialogNotification.dialog("open");
    				}
    			}
    		});
    	}

    	function deleteElem(elem){
    		elem.parent().parent().remove();
				deleteAction('<?php echo $base_url; ?>/includes/master-mailer/delete-mailer.php', elem.attr('data-id'));
    	}

    	$('body').on('click','.del', function(e) {
        $dialogDelete.data("elem", $(this));
    		$dialogDelete.dialog("open");
    	});
});
</script>
