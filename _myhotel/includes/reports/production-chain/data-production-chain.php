<?php
  session_start();
  include("../../../conf/connection.php");
  include("../../php-function.php");

  $lastdate = $_POST['lastdate'];
  $channel = 1;
  switch($_POST['request']){
  	case 'next' : $begin = 1 ; $end = 14; break;
  	case 'forward' : $begin = 10 ; $end = 23; break;
  	case 'previous' : $begin = -1 ; $end = 12; break;
  	case 'backward' : $begin = -10 ; $end = 3; break;
  	default :  $begin = 0; $end = 13; break;
  }

  $showDay = array(); $showDate = array(); $showMonth = array(); $dateArr = array();

  for($i = $begin; $i <= $end; $i++){
  	$date = date("Y-m-d",strtotime($lastdate." +".$i." day"));
  	$dateformated = date("D-d-M",strtotime($lastdate." +".$i." day"));
  	$exploDate = explode("-", $dateformated);

  	array_push($dateArr, $date);
  	array_push($showDay, $exploDate[0]);
  	array_push($showDate, $exploDate[1]);
  	array_push($showMonth, $exploDate[2]);
  }

  /*----------------------------------------------------------------------*/

  $s_hotelchain = "select h.hoteloid, h.hotelcode, h.hotelname from hotel h inner join chain c using (chainoid) where h.chainoid in (select oid from userassign where type = 'chainoid' and useroid = '".$_SESSION['_oid']."') and h.publishedoid = '1'";
  $filter = array();
  if(!empty($_POST['hotelcode'])){
    array_push($filter, "h.hotelcode = '".$_POST['hotelcode']."'");
  }
  if(count($filter) > 0){
		$combine_filter = implode(' and ',$filter);
		$s_hotelchain = $s_hotelchain.' and '.$combine_filter;
	}
  $q_hotelchain = $db->query($s_hotelchain);
  $r_hotelchain = $q_hotelchain->fetchAll(PDO::FETCH_ASSOC);
  foreach($r_hotelchain as $hotelchain){
    ${$hotelchain['hotelcode']} =  simplexml_load_file("../../../data-xml/".$hotelchain['hotelcode'].".xml");
  }

  /*--------------------------------------------------------------------*/

  function getCloseOutRTXML($hotel, $hotelcode, $room, $date, $channel){
  	global $db; global ${$hotelcode};
    $closeout = 'n'; $existtag = 0;

  	$query_tag = '//hotel[@id="'.$hotel.'"]/masterroom[@id="'.$room.'"]/rate[@date="'.$date.'"]/closeout';
  	foreach(${$hotelcode}->xpath($query_tag) as $co){
  		$closeout = $co;
  		$existtag = 1;
  	}

  	return array($closeout, $existtag);
  }
  function getAllocationXML($hotel, $hotelcode, $room, $date, $channel){
  	global $db; global ${$hotelcode};
  	$allocation =0; $existtag = 0;

  	$query_tag = '//hotel[@id="'.$hotel.'"]/masterroom[@id="'.$room.'"]/rate[@date="'.$date.'"]/allotment/channel[@type="'.$channel.'"]';
  	foreach(${$hotelcode}->xpath($query_tag) as $allotment){
  		$allocation = $allotment;
  		$existtag = 1;
  	}
  	return array($allocation, $existtag);
  }

  function showDataXML($hotel, $hotelcode, $room, $date, $roomoffer, $channel){
  	global $db; global ${$hotelcode};
  	$rate=0; $closetime='';  $breakfast='n'; $cta='n'; $ctd='n'; $existtag = 0;

  	$query_tag = '//hotel[@id="'.$hotel.'"]/masterroom[@id="'.$room.'"]/rate[@date="'.$date.'"]/rateplan[@id="'.$roomoffer.'"]/channel[@type="'.$channel.'"]';
  	foreach(${$hotelcode}->xpath($query_tag) as $rowXml){
  		$rate = $rowXml->double;
  		$closetime = $rowXml->blackout;
  		$breakfast = $rowXml->breakfast;
  		$cta = $rowXml->cta;
  		$ctd = $rowXml->ctd;
  		$existtag = 1;
  	}

  	return array($rate, $closetime, $breakfast, $cta, $ctd, $existtag);
  }

?>
  <table id="inventory" cellpadding="0" cellspacing="0">
    <thead>
      <tr>
        <th>&nbsp;</th>
        <?php
        foreach($dateArr as $key => $date){
          if($key == 0){
        ?>
        <input type="hidden" name="spotdate" value="<?=$date?>" />
        <?php
          }
        ?>
        <th class="text-center"><?php echo $showDay[$key]; ?><br /><?php echo $showDate[$key]; ?><br /><?php echo $showMonth[$key]; ?></th>
        <?php
        }
        ?>
      </tr>
    </thead>
    <tbody>
    <?php
    $totalusedhotel = array();
    foreach($r_hotelchain as $hotelchain){
    ?>
      <tr class="hotel"><td colspan="15"><?php echo $hotelchain['hotelname']; ?></td></tr>
      <?php
      $s_roomtype = "select r.roomoid, r.name from room r inner join published p using (publishedoid) inner join hotel h using (hoteloid)  where h.hotelcode = '".$hotelchain['hotelcode']."' and p.publishedoid not in (3)";
      $q_roomtype = $db->query($s_roomtype);
      $r_roomtype = $q_roomtype->fetchAll(PDO::FETCH_ASSOC); $totalusedroomtype = array();
      foreach($r_roomtype as $roomtype){
      ?>
      <tr class="roomtype">
        <td colspan="15"><?php echo $roomtype['name']; ?></td>
      </tr>
      <tr>
        <td>Allocation</td>
        <?php
        foreach($dateArr as $date){
          $getAllocation = getAllocationXML($hotelchain['hoteloid'], $hotelchain['hotelcode'], $roomtype['roomoid'], $date, $channel);
        ?>
        <td><?php echo $getAllocation[0]; ?></td>
        <?php
        }
        ?>
      </tr>
        <?php
        $s_rateplan = "select ro.roomofferoid, ro.offertypeoid, ro.name from roomoffer ro inner join published p using (publishedoid) inner join room r using (roomoid) inner join offertype using (offertypeoid) where r.roomoid = '".$roomtype['roomoid']."' and p.publishedoid = '1'";
        $q_rateplan = $db->query($s_rateplan);
        $r_rateplan = $q_rateplan->fetchAll(PDO::FETCH_ASSOC); $totalusedrateplan = array();
        foreach($r_rateplan as $rateplan){
          $rate = array(); $closeout = array(); $used = array(); $existedtag = array(); $parameter = array(); $revenue_rateplan = array();
          foreach($dateArr as $key => $date){
            /*
            $getData = showDataXML($hotelchain['hoteloid'], $hotelchain['hotelcode'], $roomtype['roomoid'], $date, $rateplan['roomofferoid'], $channel);
            array_push($rate, $getData[0]);
    				array_push($closeout, $getData[1]);
            array_push($existedtag, $getData[5]);
            */

            $s_used = "select count(bookingroomdtloid) as used, cr.currencycode, sum(brd.total) as revenue from bookingroomdtl brd inner join currency cr using (currencyoid) inner join bookingroom br using (bookingroomoid) inner join booking b using (bookingoid) where br.roomofferoid = '".$rateplan['roomofferoid']."' and brd.date = '".$date."' and b.bookingstatusoid = '4' and brd.reconciled = '0' and b.pmsstatus = '0'";
            $stmt = $db->query($s_used);
            $r_used = $stmt->fetch(PDO::FETCH_ASSOC);
            array_push($used, $r_used['used']);
            array_push($revenue_rateplan, $r_used['currencycode']." ".number_format($r_used['revenue']));

            //array_push($parameter, $hotelchain['hoteloid'].'_'.$roomtype['roomoid'].'_'.$date.'_'.$channel.'_'.$rateplan['roomofferoid']);
          }
        ?>
      <tr class="roomoffer">
        <td colspan="15"><?php echo $rateplan['name']; ?></td>
      </tr>
      <tr>
        <td>Used</td>
        <?php foreach($used as $key => $value_used){ ?>
        <td><?php echo $value_used; ?></td>
        <?php } ?>
      </tr>
    <?php
        array_push($totalusedrateplan, $used);
        }
    ?>
    <tr class="roomoffer">
      <td>Total Used <?=$roomtype['name']?></td>
      <?php
      $tmp = array();
      foreach($totalusedrateplan[0] as $key => $date){
        $total = 0;
        for($i=0; $i<count($totalusedrateplan); $i++){
            $total += $totalusedrateplan[$i][$key];
        }
      ?>
      <td><?=$total?></td>
      <?php
      array_push($tmp, $total);
      }
      ?>
    </tr>
    <?php
      array_push($totalusedroomtype, $tmp);
      }
    ?>
    <tr class="hotel">
      <td>Total Used <?=$hotelchain['hotelname']?></td>
      <?php
      $tmp = array();
      foreach($totalusedroomtype[0] as $key => $date){
        $total = 0;
        for($i=0; $i<count($totalusedroomtype); $i++){
            $total += $totalusedroomtype[$i][$key];
        }
      ?>
      <td><?=$total?></td>
      <?php
      array_push($tmp, $total);
      }
      ?>
    </tr>
    <tr><td colspan="15">&nbsp;</td></tr>
    <?php
    array_push($totalusedhotel, $tmp);
    }
    ?>
    <tr class="hotel">
      <td>Total Used All Hotel</td>
      <?php
      foreach($totalusedhotel[0] as $key => $date){
        $total = 0;
        for($i=0; $i<count($totalusedhotel); $i++){
            $total += $totalusedhotel[$i][$key];
        }
      ?>
      <td><?=$total?></td>
      <?php
      }
      ?>
    </tr>
    </tbody>
  </table>
