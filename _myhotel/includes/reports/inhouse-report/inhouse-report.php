<section class="content-header">
    <h1>Inhouse Report</h1>
    <ol class="breadcrumb">
        <li><a href="#">Report</a></li>
        <li class="active">Inhouse Report</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="box box-form">
            <div class="box-body">

<style type="text/css">
table#inventory{
	width:100%;
	border-collapse:collapse;
	font-size:0.85em;
	border-spacing:0;
}
table#inventory, table#inventory tr, table#inventory th, table#inventory td{
	border:0.2px solid #E1E1E1;
}
table#inventory thead tr th, table#inventory tbody tr td{
	padding:2px;
}
table#inventory thead tr{
	background-color:#0e1a2b;
	color:#fff;
}
table#inventory tbody tr.roomtype{
	background-color:#f4f8fb;
	font-size:1.15em;
	font-weight:bold;
}
table#inventory tbody tr.roomoffer{
	font-size:1.1em;
	font-weight:bold;
	color:#333;
}
table#inventory tr td:nth-child(n+2){
	text-align:center;
}
table#inventory tr:not(.roomtype) td:first-of-type{
	padding-left:40px;
}
table#inventory tr.roomoffer td:first-of-type{
	padding-left:15px;
}
table#inventory tr.roomtype td:nth-child(n+2), table#inventory tr.roomoffer td:nth-child(n+2){
	padding:0;
}
table#inventory tr td a{
	text-decoration: none;
  color: inherit;
}
table#inventory tr td a:hover{
	text-decoration: underline;
  color: inherit;
}
table#inventory tr td div.closeout{
	width:100%;
	height:100%;
	padding:0; margin:0;
	cursor:pointer;
}
table#inventory tr td div.closeout.open, table#inventory tr td div.closeout.close:hover{
	border: 1px solid #0C3;
	background-color:#0C6;
}
table#inventory tr td div.closeout.close, table#inventory tr td div.closeout.open:hover{
	border: 1px solid #C00;
	background-color:#F03;
}

table#inventory input[type=text]{
	display:none;
    font-size: 1em;
    padding: 2px 5px;
    max-width: 70px;
	border-radius:0;
	border: 1px solid #999;
}
input#datepicker{ width:125px; padding: 2px 5px; }
</style>
<?php include('script.php'); ?>

            	<div style="margin-bottom:15px;">
                <table width="100%">
                <tr>
                <td width="33%" align="center">
            		<button type="button" class="inv-nav" value="backward"><i class="fa fa-angle-double-left"></i></button>
            		<button type="button" class="inv-nav" value="previous"><i class="fa fa-angle-left"></i></button>
                <input type="text" name="date" id="datepicker" value="<?=date('d F Y');?>" />
            		<button type="button" class="inv-nav" value="next"><i class="fa fa-angle-right"></i></button>
            		<button type="button" class="inv-nav" value="forward"><i class="fa fa-angle-double-right"></i></button>
                </td>
                </tr>
                </table>
                </div>
                <div id="show-inventory"></div>

            </div><!-- /.box-body -->
       </div>
    </div>
</section>
