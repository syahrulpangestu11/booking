<div id="newdashboard">
    <link href="<?php echo $base_url?>/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $base_url?>/lib/admin-lte/css/AdminLTE.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $base_url?>/lib/admin-lte/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $base_url?>/css/style.css?v=<?=$fileVersion;?>" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <style type="text/css">
        /* h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
            font-family: inherit;
            font-weight: 600;
            line-height: inherit;
            color: inherit;
        }
        .wrapper {
            position: inherit;
            overflow: hidden!important;
        }
        .left-side {
            padding-top: inherit;
        }
        .sidebar > .sidebar-menu li > a:hover {
            background-color: rgba(72, 115, 175, 0.26);
        }
        .sidebar .sidebar-menu .treeview-menu {
            background-color: rgb(14, 26, 43);
        }
        .sidebar .sidebar-menu .treeview-menu .treeview-menu-child {
            background-color: rgba(0, 0, 0, 0.5);
        }
        .sidebar > .sidebar-menu li.active > a {
            background-color: rgba(197, 45, 47, 0.55);
        }
        .sidebar > .sidebar-menu > li.treeview.active > a {
            background-color: inherit;
        }
        .sidebar > .sidebar-menu > li.treeview.active li.treeview-child.active > a {
            background-color: inherit;
        }
        .sidebar .sidebar-menu > li > a > .fa {
            width: 28px;
            font-size: 16px;
        }
        .sidebar-menu li>a>.fa-angle-left {
            position: relative;
            margin-right: inherit;
        }
        .sidebar .sidebar-menu > li > a > .fa.pull-right {
            right: 0;
            text-align: center;
        }
        .box {
            border-radius: inherit;
            border-top: inherit;
        }
        .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default {
            background-color: #ededed !important;
        } */
    </style>

    <?php
        function differenceDate($start, $end){
            $date1 = new DateTime($start);
            $date2 = new DateTime($end);
            $interval = $date1->diff($date2);
            return $interval->days;
        }

        function countResult($start, $end, $hoteloid, $roomtype, $status){
            global $db;

            $main_query = "select count(DISTINCT b.bookingoid) as jmldata from booking b inner join hotel h using (hoteloid) inner join bookingroom br using (bookingoid) inner join roomoffer ro using (roomofferoid) inner join room r using (roomoid) where (date(bookingtime) >= '".$start."' and date(bookingtime) <= '".$end."') and h.hoteloid = '".$hoteloid."' and b.pmsstatus = '0'";
            $filter = array();
            if($status != ""){
                array_push($filter, "b.bookingstatusoid = '".$status."'");
            }
            if($roomtype != ""){
                array_push($filter, "r.roomoid = '".$roomtype."'");
            }

            if(count($filter) > 0){
                $combine_filter = implode(' and ',$filter);
                $query = $main_query.' and '.$combine_filter;
            }else{
                $query = $main_query;
            }

            $stmt	= $db->query($query);
            $result = $stmt->fetch(PDO::FETCH_ASSOC);

            return $result['jmldata'];
        }

        if(isset($_REQUEST['roomtype'])){ $roomtype = $_REQUEST['roomtype']; }else{ $roomtype = ""; }
        if(isset($_REQUEST['from'])){ $from = date('Y-m-d', strtotime($_REQUEST['from'])); }else{ $from = date('Y-m-d', strtotime('-10 day')); }
        if(isset($_REQUEST['to'])){ $to = date('Y-m-d', strtotime($_REQUEST['to'])); }else{ $to = date('Y-m-d'); }

        $day = differenceDate($from, $to);

        /*
        * For Data Chart and Below Chart
        */

        $show_date = array();
        $point_reservation = array();
        $point_confirmed = array();
        $point_cancelled = array();
        $point_noshow = array();
        for($i = 0; $i <= $day; $i++){
            $date	= date('Y-m-d', strtotime($from. '+'.$i.' day'));
            array_push($show_date, date('d M', strtotime($date)));
            array_push($point_reservation, countResult($date, $date, $hoteloid, $roomtype, ""));
            array_push($point_confirmed, countResult($date, $date, $hoteloid, $roomtype, 4));
            array_push($point_cancelled, countResult($date, $date, $hoteloid, $roomtype, 5));
            array_push($point_noshow, countResult($date, $date, $hoteloid, $roomtype, 7));
        }

        $sum_revenue 	= "select sum(grandtotal) as revenue, count(bookingoid) as confirmed from booking inner join hotel h using (hoteloid) where (date(bookingtime) >= '".$from."' and date(bookingtime) <= '".$to."') and h.hoteloid = '".$hoteloid."'  and bookingstatusoid in ('4') and pmsstatus = '0'";
        $stmt			= $db->query($sum_revenue);
        $arr_revenue	= $stmt->fetch(PDO::FETCH_ASSOC);
		$revenue 		= floor($arr_revenue['revenue']);

        $sum_commission	= "select sum(gbhcollect) as commission from booking inner join hotel h using (hoteloid) where (date(bookingtime) >= '".$from."' and date(bookingtime) <= '".$to."') and h.hoteloid = '".$hoteloid."' and bookingstatusoid in ('4') and pmsstatus = '0'";
        $stmt			= $db->query($sum_commission);
        $arr_commission = $stmt->fetch(PDO::FETCH_ASSOC);
		$commission		= floor($arr_commission['commission']);

        $sum_cancellationamount = "select sum(cancellationamount) as cancelled from booking inner join hotel h using (hoteloid) where (date(bookingtime) >= '".$from."' and date(bookingtime) <= '".$to."') and h.hoteloid = '".$hoteloid."' and bookingstatusoid in ('5','7') and pmsstatus = '0'";
        $stmt					= $db->query($sum_cancellationamount);
        $arr_cancellationamount = $stmt->fetch(PDO::FETCH_ASSOC);
		$cancellationamount		= floor($arr_cancellationamount['cancelled']);

		$nethotel = $revenue - $commission;
    ?>

    <div class="row">
    <div class="col-md-12">
        <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Reservation Report</h3> &nbsp;
            <form method="get" action="<?=$base_url?>/performance-reports/reservation-report/">
            <select name="roomtype" class="form-control" style="display:inline-block;width:auto;margin-left:5px;">
            <?php if($roomtype == ""){ $selected = 'selected = "selected"';  }else{ $selected = ''; } ?>
                <option value="" <?=$selected?>>All room type</option>
            <?php
                try {
                    $stmt = $db->query("select r.roomoid, r.name from room r inner join hotel h using (hoteloid) where h.hoteloid = '".$hoteloid."' and r.publishedoid = '1'");
                    $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                    foreach($r_room as $row){
                        if($row['roomoid'] == $roomtype){ $selected = 'selected = "selected"';  }else{ $selected = ''; }
                        echo"<option value='".$row['roomoid']."' ".$selected.">  ".$row['name']."</option>";
                    }
                }catch(PDOException $ex) {
                    echo "Invalid Query";
                    die();
                }

            ?>
            </select> &nbsp;
            Range : <input type="text" name="from" id="from" class="calinput" readonly value="<?php echo date('d-m-Y', strtotime($from));?>" style="width:100px"> &nbsp;- &nbsp;
            <input type="text" id="to" name="to" class="calinput" readonly value="<?php echo date('d-m-Y', strtotime($to));?>" style="width:100px">
            <button type="submit" class="small-button blue"><i class="fa fa-search"></i>Find</button>
            </form>
            <div class="box-tools pull-right">
            <a href="<?php echo $base_url?>/booking" class="btn btn-sm bg-blue btn-flat pull-left">View Reservation</a> &nbsp;
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>

        </div><!-- /.box-header -->
        <div class="box-body">
            <div class="row">
            <div class="col-md-8">
                <p class="text-center">
                <strong>Sales: <?php echo date('d M, Y', strtotime($from)); ?> - <?php echo date('d M, Y', strtotime($to)); ?></strong>
                </p>
                <div class="chart">
                <!-- Sales Chart Canvas -->
                <canvas id="salesChart" style="height: 180px;"></canvas>
                </div><!-- /.chart-responsive -->
            </div><!-- /.col -->
            <div class="col-md-4">
                <p class="text-center">
                <strong>Summary</strong>
                </p>
                <div class="progress-group">
                <span class="progress-text">Total Reservation</span>
                <span class="progress-number">
                    <?php $totalreservation = countResult($from, $to, $hoteloid, $roomtype, ""); ?>
                    <b><?=$totalreservation?></b>
                </span>
                <div class="progress sm">
                    <div class="progress-bar progress-bar-aqua" style="width: 100%"></div>
                </div>
                </div><!-- /.progress-group -->
                <div class="progress-group">
                <span class="progress-text">Total Confirmed Booking</span>
                <span class="progress-number">
                    <?php
                        $totalconfirmed = countResult($from, $to, $hoteloid, $roomtype, 4);
                        if($totalreservation > 0){
                            $barconfirmed = floor($totalconfirmed / $totalreservation * 100);
                        }else{
                            $barconfirmed = 0;
                        }
                    ?>
                    <b><?=$totalconfirmed?></b> / <?=$totalreservation?>
                </span>
                <div class="progress sm">
                    <div class="progress-bar progress-bar-green" style="width: <?=$barconfirmed?>%"></div>
                </div>
                </div><!-- /.progress-group -->
                <div class="progress-group">
                <span class="progress-text">Total Cancelled Booking</span>
                <span class="progress-number">
                    <?php
                        $totalcancelled = countResult($from, $to, $hoteloid, $roomtype, 5);
                        if($totalreservation > 0){
                            $barcancelled = floor($totalcancelled / $totalreservation * 100);
                        }else{
                            $barcancelled = 0;
                        }
                    ?>
                    <b><?=$totalcancelled?></b> / <?=$totalreservation?>
                </span>
                <div class="progress sm">
                    <div class="progress-bar progress-bar-red" style="width: <?=$barcancelled?>%"></div>
                </div>
                </div><!-- /.progress-group -->
                <div class="progress-group">
                <span class="progress-text">Total No Show</span>
                <span class="progress-number">
                    <?php
                        $totalnoshow = countResult($from, $to, $hoteloid, $roomtype, 7);
                        if($totalreservation > 0){
                            $barnoshow = floor($totalnoshow / $totalreservation * 100);
                        }else{
                            $barnoshow = 0;
                        }
                    ?>
                    <b><?=$totalnoshow?></b> / <?=$totalreservation?>
                </span>
                <div class="progress sm">
                    <div class="progress-bar progress-bar-yellow" style="width: <?=$barnoshow?>%"></div>
                </div>
                </div><!-- /.progress-group -->
            </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- ./box-body -->
        <div class="box-footer">
            <div class="row">
            <div class="col-sm-3 col-xs-6">
                <div class="description-block border-right">
                <h5 class="description-header">IDR <?=number_format($revenue)?></h5>
                <span class="description-text">TOTAL REVENUE</span>
                </div><!-- /.description-block -->
            </div><!-- /.col -->
            <div class="col-sm-3 col-xs-6">
                <div class="description-block">
                <h5 class="description-header"><?=number_format($cancellationamount)?></h5>
                <span class="description-text">CANCEL / NO SHOW</span>
                </div><!-- /.description-block -->
            </div>
            <div class="col-sm-3 col-xs-6">
                <div class="description-block border-right">
                <h5 class="description-header">IDR <?=number_format($commission)?></h5>
                <span class="description-text">TOTAL COMMISSION</span>
                </div><!-- /.description-block -->
            </div><!-- /.col -->
            <div class="col-sm-3 col-xs-6">
                <div class="description-block border-right">
                <h5 class="description-header">IDR <?=number_format($nethotel)?></h5>
                <span class="description-text">NET TO HOTEL</span>
                </div><!-- /.description-block -->
            </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.box-footer -->
        </div><!-- /.box -->
    </div><!-- /.col -->
    </div><!-- /.row -->

    <!-- Bootstrap 3.3.6 -->
    <script src="<?php echo $base_url?>/lib/bootstrap/js/bootstrap.min.js"></script>
    <!-- chartjs -->
    <script src="<?php echo $base_url?>/lib/plugins/chartjs/Chart.min.js"></script>
    <script>
        $(function () {
            'use strict';

            /* ChartJS
            * -------
            * Here we will create a few charts using ChartJS
            */

            //-----------------------
            //- MONTHLY SALES CHART -
            //-----------------------

            // Get context with jQuery - using jQuery's .get() method.
            var salesChartCanvas = $("#salesChart").get(0).getContext("2d");
            // This will get the first returned node in the jQuery collection.
            var salesChart = new Chart(salesChartCanvas);

            var salesChartData = {
                labels: [ <?php echo '"'.implode('","', $show_date).'"'; ?> ],
                datasets: [
                {
                    label: "Reservation",
                    fillColor: "rgba(0, 192, 239, 0.5)",
                    strokeColor: "rgba(0, 192, 239, 0.5)",
                    pointColor: "rgba(0, 192, 239, 0.5)",
                    pointStrokeColor: "#c1c7d1",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgb(220,220,220)",
                    data: [ <?php echo implode(',', $point_reservation); ?> ]
                },
                {
                    label: "Confirmed Booking",
                    fillColor: "rgba(0, 166, 90, 0.5)",
                    strokeColor: "rgba(0, 166, 90, 0.5)",
                    pointColor: "rgba(0, 166, 90, 0.5)",
                    pointStrokeColor: "rgba(60,141,188,1)",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(60,141,188,1)",
                    data: [ <?php echo implode(',', $point_confirmed); ?> ]
                },
                {
                    label: "Cancelled Booking",
                    fillColor: "rgba(221, 75, 57, 0.5)",
                    strokeColor: "rgba(221, 75, 57, 0.5)",
                    pointColor: "rgba(221, 75, 57, 0.5)",
                    pointStrokeColor: "rgba(60,141,188,1)",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(60,141,188,1)",
                    data: [ <?php echo implode(',', $point_cancelled); ?> ]
                },
                {
                    label: "No Show",
                    fillColor: "rgba(243, 156, 18, 0.5)",
                    strokeColor: "rgba(243, 156, 18, 0.5)",
                    pointColor: "rgba(243, 156, 18, 0.5)",
                    pointStrokeColor: "rgba(60,141,188,1)",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(60,141,188,1)",
                    data: [ <?php echo implode(',', $point_noshow); ?> ]
                }
                ]
            };

            var salesChartOptions = {
                //Boolean - If we should show the scale at all
                showScale: true,
                //Boolean - Whether grid lines are shown across the chart
                scaleShowGridLines: false,
                //String - Colour of the grid lines
                scaleGridLineColor: "rgba(0,0,0,.05)",
                //Number - Width of the grid lines
                scaleGridLineWidth: 1,
                //Boolean - Whether to show horizontal lines (except X axis)
                scaleShowHorizontalLines: true,
                //Boolean - Whether to show vertical lines (except Y axis)
                scaleShowVerticalLines: true,
                //Boolean - Whether the line is curved between points
                bezierCurve: true,
                //Number - Tension of the bezier curve between points
                bezierCurveTension: 0.3,
                //Boolean - Whether to show a dot for each point
                pointDot: false,
                //Number - Radius of each point dot in pixels
                pointDotRadius: 4,
                //Number - Pixel width of point dot stroke
                pointDotStrokeWidth: 1,
                //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
                pointHitDetectionRadius: 20,
                //Boolean - Whether to show a stroke for datasets
                datasetStroke: true,
                //Number - Pixel width of dataset stroke
                datasetStrokeWidth: 2,
                //Boolean - Whether to fill the dataset with a color
                datasetFill: true,
                //String - A legend template
                legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%=datasets[i].label%></li><%}%></ul>",
                //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                maintainAspectRatio: true,
                //Boolean - whether to make the chart responsive to window resizing
                responsive: true
            };

            //Create the line chart
            salesChart.Line(salesChartData, salesChartOptions);

            //---------------------------
            //- END MONTHLY SALES CHART -
            //---------------------------
        });
    </script>
    <script>
    $( function() {
        var dateFormat = "dd-mm-yy",
        from = $( "#from" )
            .datepicker({
            defaultDate: "+1d",
            changeMonth: true,
			dateFormat: "dd-mm-yy",
            numberOfMonths: 1
            })
            .on( "change", function() {
            to.datepicker( "option", "minDate", getDate( this ) );
            }),
        to = $( "#to" ).datepicker({
            defaultDate: "+1d",
            changeMonth: true,
			dateFormat: "dd-mm-yy",
            numberOfMonths: 1
        })
        .on( "change", function() {
            from.datepicker( "option", "maxDate", getDate( this ) );
        });

        function getDate( element ) {
        var date;
        try {
            date = $.datepicker.parseDate( dateFormat, element.value );
        } catch( error ) {
            date = null;
        }
        return date;
        }
    } );
    </script>
</div>
