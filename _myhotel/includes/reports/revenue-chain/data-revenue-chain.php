<?php
  session_start();
  include("../../../conf/connection.php");
  include("../../php-function.php");

  $lastdate = $_POST['lastdate'];
  $channel = 1;
  switch($_POST['request']){
  	case 'next' : $begin = 1 ; $end = 7; break;
  	case 'forward' : $begin = 7 ; $end = 13; break;
  	case 'previous' : $begin = -1 ; $end = 5; break;
  	case 'backward' : $begin = -7 ; $end = -1; break;
  	default :  $begin = 0; $end = 6; break;
  }

  $showDay = array(); $showDate = array(); $showMonth = array(); $dateArr = array();

  for($i = $begin; $i <= $end; $i++){
  	$date = date("Y-m-d",strtotime($lastdate." +".$i." day"));
  	$dateformated = date("D-d-M",strtotime($lastdate." +".$i." day"));
  	$exploDate = explode("-", $dateformated);

  	array_push($dateArr, $date);
  	array_push($showDay, $exploDate[0]);
  	array_push($showDate, $exploDate[1]);
  	array_push($showMonth, $exploDate[2]);
  }

  /*----------------------------------------------------------------------*/

  $s_hotelchain = "select h.hoteloid, h.hotelcode, h.hotelname from hotel h inner join chain c using (chainoid) where h.chainoid in (select oid from userassign where type = 'chainoid' and useroid = '".$_SESSION['_oid']."') and h.publishedoid = '1'";
  $filter = array();
  if(!empty($_POST['hotelcode'])){
    array_push($filter, "h.hotelcode = '".$_POST['hotelcode']."'");
  }
  if(count($filter) > 0){
		$combine_filter = implode(' and ',$filter);
		$s_hotelchain = $s_hotelchain.' and '.$combine_filter;
	}
  $q_hotelchain = $db->query($s_hotelchain);
  $r_hotelchain = $q_hotelchain->fetchAll(PDO::FETCH_ASSOC);
  foreach($r_hotelchain as $hotelchain){
    //${$hotelchain['hotelcode']} =  simplexml_load_file("../../../data-xml/".$hotelchain['hotelcode'].".xml");
  }

  /*--------------------------------------------------------------------*/


?>
  <table id="inventory" cellpadding="0" cellspacing="0">
    <thead>
      <tr>
        <th>&nbsp;</th>
        <?php
        foreach($dateArr as $key => $date){
          if($key == 0){
        ?>
        <input type="hidden" name="spotdate" value="<?=$date?>" />
        <?php
          }
        ?>
        <th class="text-center"><?php echo $showDay[$key]; ?><br /><?php echo $showDate[$key]; ?><br /><?php echo $showMonth[$key]; ?></th>
        <?php
        }
        ?>
      </tr>
    </thead>
    <tbody>
    <?php
    foreach($r_hotelchain as $hotelchain){
    ?>
      <tr class="hotel"><td colspan="15"><?php echo $hotelchain['hotelname']; ?></td></tr>
      <?php
      $s_roomtype = "select r.roomoid, r.name from room r inner join published p using (publishedoid) inner join hotel h using (hoteloid)  where h.hotelcode = '".$hotelchain['hotelcode']."' and p.publishedoid not in (3)";
      $q_roomtype = $db->query($s_roomtype);
      $r_roomtype = $q_roomtype->fetchAll(PDO::FETCH_ASSOC);
      foreach($r_roomtype as $roomtype){
      ?>
      <tr class="roomtype">
        <td colspan="15"><?php echo $roomtype['name']; ?></td>
      </tr>
      <tr>
        <td>Sold</td>
        <?php
        foreach($dateArr as $date){
          $s_usedroomtype = "select count(bookingroomdtloid) as used from bookingroomdtl brd inner join currency cr using (currencyoid) inner join bookingroom br using (bookingroomoid) inner join roomoffer ro using (roomofferoid) inner join room r using (roomoid) inner join booking b using (bookingoid) where r.roomoid = '".$roomtype['roomoid']."' and brd.date = '".$date."' and b.bookingstatusoid = '4' and brd.reconciled = '0' and b.pmsstatus = '0'";
          $stmt = $db->query($s_usedroomtype);
          $usedroomtype = $stmt->fetch(PDO::FETCH_ASSOC);
        ?>
        <td><?php echo $usedroomtype['used']; ?></td>
        <?php
        }
        ?>
      </tr>
        <?php
        $s_rateplan = "select ro.roomofferoid, ro.offertypeoid, ro.name from roomoffer ro inner join published p using (publishedoid) inner join room r using (roomoid) inner join offertype using (offertypeoid) where r.roomoid = '".$roomtype['roomoid']."' and p.publishedoid = '1'";
        $q_rateplan = $db->query($s_rateplan);
        $r_rateplan = $q_rateplan->fetchAll(PDO::FETCH_ASSOC);
        foreach($r_rateplan as $rateplan){
          $rate = array(); $closeout = array(); $used = array(); $existedtag = array(); $parameter = array(); $revenue_rateplan = array();
          foreach($dateArr as $key => $date){
            $s_used = "select count(bookingroomdtloid) as used, cr.currencycode, sum(brd.total) as revenue from bookingroomdtl brd inner join currency cr using (currencyoid) inner join bookingroom br using (bookingroomoid) inner join booking b using (bookingoid) where br.roomofferoid = '".$rateplan['roomofferoid']."' and brd.date = '".$date."' and b.bookingstatusoid = '4' and brd.reconciled = '0' and b.pmsstatus = '0' and b.pmsstatus = '0' group by brd.currencyoid";
            $stmt = $db->query($s_used);
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

              $date_used = array(); $date_revenue = array();
              foreach($result as $r_used){
                array_push($date_used, $r_used['used']);
                array_push($date_revenue, $r_used['currencycode']." ".number_format($r_used['revenue'], 2));
              }

            array_push($used, array_sum($date_used));
            array_push($revenue_rateplan, $date_revenue);
          }

          //print_r($revenue_rateplan);
        ?>
      <tr class="roomoffer">
        <td colspan="15"><?php echo $rateplan['name']; ?></td>
      </tr>
      <tr>
        <td>Sold</td>
        <?php foreach($used as $key => $value_used){ ?>
        <td><?php echo $value_used; ?></td>
        <?php } ?>
      </tr>
      <tr>
        <td>Revenue</td>
        <?php foreach($revenue_rateplan as $key => $value_revenue_rateplan){ ?>
        <td>
          <?php
            if(is_array($value_revenue_rateplan)){
              foreach($value_revenue_rateplan as $revenue_rateplan){
                echo $revenue_rateplan."<br>";
              }
            }
          ?>
        </td>
        <?php } ?>
      </tr>
    <?php
        }
    ?>
    <tr class="roomoffer">
      <td>Total Revenue <?=$roomtype['name']?></td>
      <?php
      $revenue_rateplan = array();
      foreach($dateArr as $key => $date){
        $s_used = "select cr.currencycode, sum(brd.total) as revenue from bookingroomdtl brd inner join currency cr using (currencyoid) inner join bookingroom br using (bookingroomoid) inner join roomoffer ro using (roomofferoid) inner join room r using (roomoid) inner join booking b using (bookingoid) where r.roomoid = '".$roomtype['roomoid']."' and brd.date = '".$date."' and b.bookingstatusoid = '4' and brd.reconciled = '0' and b.pmsstatus = '0' group by brd.currencyoid";
        $stmt = $db->query($s_used);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
      ?>
      <td>
        <?php
          foreach($result as $revenuehotel){
            echo $revenuehotel['currencycode']." ".number_format($revenuehotel['revenue'], 2)."<br>";
          }
        ?>
      </td>
      <?php
      }
      ?>
    </tr>
    <?php
      }
    ?>
    <tr class="hotel">
      <td>Total Revenue <?=$hotelchain['hotelname']?></td>
      <?php
      $revenue_rateplan = array();
      foreach($dateArr as $key => $date){
        $s_revenuehotel = "select cr.currencycode, sum(brd.total) as revenue from bookingroomdtl brd inner join currency cr using (currencyoid) inner join bookingroom br using (bookingroomoid) inner join booking b using (bookingoid) where b.hoteloid = '".$hotelchain['hoteloid']."' and brd.date = '".$date."' and b.bookingstatusoid = '4' and brd.reconciled = '0' and b.pmsstatus = '0' group by brd.currencyoid";
        $stmt = $db->query($s_revenuehotel);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
      ?>
      <td>
        <?php
          foreach($result as $revenuehotel){
            echo $revenuehotel['currencycode']." ".number_format($revenuehotel['revenue'], 2)."<br>";
          }
        ?>
      </td>
      <?php
      }
      ?>
    </tr>
    <tr><td colspan="15">&nbsp;</td></tr>
    <?php
    }

    $used_allhotel = array(); $revenue_allhotel = array();
    foreach($dateArr as $key => $date){
      $s_revenuechain = "select count(bookingroomdtloid) as used, cr.currencycode, sum(brd.total) as revenue from bookingroomdtl brd inner join currency cr using (currencyoid) inner join bookingroom br using (bookingroomoid) inner join booking b using (bookingoid) inner join hotel h using (hoteloid) inner join chain c using (chainoid) where c.chainoid in (select oid from userassign where type = 'chainoid' and useroid = '".$_SESSION['_oid']."') and brd.date = '".$date."' and b.bookingstatusoid = '4' and brd.reconciled = '0' and b.pmsstatus = '0' group by brd.chainoid group by brd.currencyoid";
      $stmt = $db->query($s_revenuechain);
      $revenuechain = $stmt->fetchAll(PDO::FETCH_ASSOC);

      $all_used = array(); $all_revenue = array();
      foreach($revenuechain as $chain){
        array_push($all_used, $chain['used']);
        array_push($all_revenue, $chain['currencycode']." ".number_format($chain['revenue'], 2));
      }
      array_push($used_allhotel, array_sum($all_used));
      array_push($revenue_allhotel, $all_revenue);
    }
    ?>
    <tr class="hotel">
      <td>Total Sold Room All Hotel</td>
      <?php foreach($used_allhotel as $key => $value_used){ ?>
      <td><?php echo $value_used; ?></td>
      <?php } ?>
    </tr>
    <tr class="hotel">
      <td>Total Revenue All Hotel</td>
      <?php foreach($revenue_allhotel as $key => $value_revenue){ ?>
      <td>
        <?php
          if(is_array($value_revenue)){
            foreach($value_revenue as $revenue_allhotel){
              echo $revenue_allhotel."<br>";
            }
          }
        ?>
      </td>
      <?php } ?>
    </tr>
    </tbody>
  </table>
