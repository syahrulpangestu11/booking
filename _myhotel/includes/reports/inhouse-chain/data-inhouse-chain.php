<?php
  session_start();
  include("../../../conf/connection.php");
  include("../../php-function.php");

  $lastdate = $_POST['lastdate'];
  $channel = 1;
  switch($_POST['request']){
    case 'next' : $begin = 1 ; $end = 7; break;
  	case 'forward' : $begin = 7 ; $end = 13; break;
  	case 'previous' : $begin = -1 ; $end = 5; break;
  	case 'backward' : $begin = -7 ; $end = -1; break;
  	default :  $begin = 0; $end = 6; break;
  }

  $showDay = array(); $showDate = array(); $showMonth = array(); $dateArr = array();

  for($i = $begin; $i <= $end; $i++){
  	$date = date("Y-m-d",strtotime($lastdate." +".$i." day"));
  	$dateformated = date("D-d-M",strtotime($lastdate." +".$i." day"));
  	$exploDate = explode("-", $dateformated);

  	array_push($dateArr, $date);
  	array_push($showDay, $exploDate[0]);
  	array_push($showDate, $exploDate[1]);
  	array_push($showMonth, $exploDate[2]);
  }

  /*----------------------------------------------------------------------*/

  $s_hotelchain = "select h.hoteloid, h.hotelcode, h.hotelname from hotel h inner join chain c using (chainoid) where h.chainoid in (select oid from userassign where type = 'chainoid' and useroid = '".$_SESSION['_oid']."') and h.publishedoid = '1'";
  $filter = array();
  if(!empty($_POST['hotelcode'])){
    array_push($filter, "h.hotelcode = '".$_POST['hotelcode']."'");
  }
  if(count($filter) > 0){
		$combine_filter = implode(' and ',$filter);
		$s_hotelchain = $s_hotelchain.' and '.$combine_filter;
	}
  $q_hotelchain = $db->query($s_hotelchain);
  $r_hotelchain = $q_hotelchain->fetchAll(PDO::FETCH_ASSOC);
  foreach($r_hotelchain as $hotelchain){
    ${$hotelchain['hotelcode']} =  simplexml_load_file("../../../data-xml/".$hotelchain['hotelcode'].".xml");
  }

  /*--------------------------------------------------------------------*/

  function getCloseOutRTXML($hotel, $hotelcode, $room, $date, $channel){
  	global $db; global ${$hotelcode};
    $closeout = 'n'; $existtag = 0;

  	$query_tag = '//hotel[@id="'.$hotel.'"]/masterroom[@id="'.$room.'"]/rate[@date="'.$date.'"]/closeout';
  	foreach(${$hotelcode}->xpath($query_tag) as $co){
  		$closeout = $co;
  		$existtag = 1;
  	}

  	return array($closeout, $existtag);
  }
  function getAllocationXML($hotel, $hotelcode, $room, $date, $channel){
  	global $db; global ${$hotelcode};
  	$allocation =0; $existtag = 0;

  	$query_tag = '//hotel[@id="'.$hotel.'"]/masterroom[@id="'.$room.'"]/rate[@date="'.$date.'"]/allotment/channel[@type="'.$channel.'"]';
  	foreach(${$hotelcode}->xpath($query_tag) as $allotment){
  		$allocation = $allotment;
  		$existtag = 1;
  	}
  	return array($allocation, $existtag);
  }

  function showDataXML($hotel, $hotelcode, $room, $date, $roomoffer, $channel){
  	global $db; global ${$hotelcode};
  	$rate=0; $closetime='';  $breakfast='n'; $cta='n'; $ctd='n'; $existtag = 0;

  	$query_tag = '//hotel[@id="'.$hotel.'"]/masterroom[@id="'.$room.'"]/rate[@date="'.$date.'"]/rateplan[@id="'.$roomoffer.'"]/channel[@type="'.$channel.'"]';
  	foreach(${$hotelcode}->xpath($query_tag) as $rowXml){
  		$rate = $rowXml->double;
  		$closetime = $rowXml->blackout;
  		$breakfast = $rowXml->breakfast;
  		$cta = $rowXml->cta;
  		$ctd = $rowXml->ctd;
  		$existtag = 1;
  	}

  	return array($rate, $closetime, $breakfast, $cta, $ctd, $existtag);
  }

?>
  <table id="inventory" cellpadding="0" cellspacing="0">
    <thead>
      <tr>
        <th>&nbsp;</th>
        <?php
        foreach($dateArr as $key => $date){
          if($key == 0){
        ?>
        <input type="hidden" name="spotdate" value="<?=$date?>" />
        <?php
          }
        ?>
        <th class="text-center"><?php echo $showDay[$key]; ?><br /><?php echo $showDate[$key]; ?><br /><?php echo $showMonth[$key]; ?></th>
        <?php
        }
        ?>
      </tr>
    </thead>
    <tbody>
    <?php
    foreach($r_hotelchain as $hotelchain){
    ?>
      <tr class="hotel"><td colspan="8"><?php echo $hotelchain['hotelname']; ?></td></tr>
      <?php
      $s_roomtype = "select r.roomoid, r.name from room r inner join published p using (publishedoid) inner join hotel h using (hoteloid)  where h.hotelcode = '".$hotelchain['hotelcode']."' and p.publishedoid not in (3)";
      $q_roomtype = $db->query($s_roomtype);
      $r_roomtype = $q_roomtype->fetchAll(PDO::FETCH_ASSOC);
      foreach($r_roomtype as $roomtype){
      ?>
      <tr class="roomtype">
        <td colspan="8"><?php echo $roomtype['name']; ?></td>
      </tr>
      <tr>
        <td>Allocation</td>
        <?php
        foreach($dateArr as $date){
          $getAllocation = getAllocationXML($hotelchain['hoteloid'], $hotelchain['hotelcode'], $roomtype['roomoid'], $date, $channel);
        ?>
        <td><?php echo $getAllocation[0]; ?></td>
        <?php
        }
        ?>
      </tr>
        <?php
        $s_rateplan = "select ro.roomofferoid, ro.offertypeoid, ro.name from roomoffer ro inner join published p using (publishedoid) inner join room r using (roomoid) inner join offertype using (offertypeoid) where r.roomoid = '".$roomtype['roomoid']."' and p.publishedoid = '1'";
        $q_rateplan = $db->query($s_rateplan);
        $r_rateplan = $q_rateplan->fetchAll(PDO::FETCH_ASSOC);
        foreach($r_rateplan as $rateplan){
          $rate = array(); $closeout = array(); $used = array(); $existedtag = array(); $parameter = array(); $revenue_rateplan = array();
          foreach($dateArr as $key => $date){
            /*
            $getData = showDataXML($hotelchain['hoteloid'], $hotelchain['hotelcode'], $roomtype['roomoid'], $date, $rateplan['roomofferoid'], $channel);
            array_push($rate, $getData[0]);
    				array_push($closeout, $getData[1]);
            array_push($existedtag, $getData[5]);
            */

            $s_used = "select count(bookingroomdtloid) as used, cr.currencycode, sum(brd.total) as revenue from bookingroomdtl brd inner join currency cr using (currencyoid) inner join bookingroom br using (bookingroomoid) inner join booking b using (bookingoid) where br.roomofferoid = '".$rateplan['roomofferoid']."' and brd.date = '".$date."' and b.bookingstatusoid = '4' and brd.reconciled = '0' and b.pmsstatus = '0'";
            $stmt = $db->query($s_used);
            $r_used = $stmt->fetch(PDO::FETCH_ASSOC);
            array_push($used, $r_used['used']);
            array_push($revenue_rateplan, $r_used['currencycode']." ".number_format($r_used['revenue']));

            //array_push($parameter, $hotelchain['hoteloid'].'_'.$roomtype['roomoid'].'_'.$date.'_'.$channel.'_'.$rateplan['roomofferoid']);
          }
        ?>
      <tr class="roomoffer">
        <td colspan="8"><?php echo $rateplan['name']; ?></td>
      </tr>
      <tr>
        <td>Used</td>
        <?php foreach($used as $key => $value_used){ ?>
        <td><?php echo $value_used; ?></td>
        <?php } ?>
      </tr>
      <tr>
        <td>Inhouse</td>
        <?php
				foreach($dateArr as $key => $date){
					$s_used = "select concat(c.firstname, ' ', c.lastname , '/', cr.currencycode, ' ', ROUND(brd.total,0), '/', br.promotion, '/', br.night, 'rn') as data, b.bookingnumber from bookingroomdtl brd inner join currency cr using (currencyoid) inner join bookingroom br using (bookingroomoid) inner join booking b using (bookingoid) inner join customer c using (custoid) where br.roomofferoid = '".$rateplan['roomofferoid']."' and brd.date = '".$date."' and b.bookingstatusoid = '4' and brd.reconciled = '0' and b.pmsstatus = '0'";
					$stmt = $db->query($s_used);
					$inhouse_count = $stmt->rowCount();
				?>
				<td style="text-align:left; vertical-align:top">
					<?php
					if($inhouse_count > 0){
						$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
						foreach($result as $rslt){
						 echo '&bull; '.$rslt['data'].'<br>';
						}
					}
					?>
				</td>
				<?php
				}
				?>
      </tr>
      <tr>
        <td>Revenue</td>
        <?php foreach($revenue_rateplan as $key => $value_revenue_rateplan){ ?>
        <td><?php echo $value_revenue_rateplan; ?></td>
        <?php } ?>
      </tr>
    <?php
        }
    ?>
    <tr class="roomoffer">
      <td>Total Revenue <?=$roomtype['name']?></td>
      <?php
      $revenue_rateplan = array();
      foreach($dateArr as $key => $date){
        $s_used = "select cr.currencycode, sum(brd.total) as revenue from bookingroomdtl brd inner join currency cr using (currencyoid) inner join bookingroom br using (bookingroomoid) inner join roomoffer ro using (roomofferoid) inner join room r using (roomoid) inner join booking b using (bookingoid) where r.roomoid = '".$roomtype['roomoid']."' and brd.date = '".$date."' and b.bookingstatusoid = '4' and brd.reconciled = '0' and b.pmsstatus = '0'";
        $stmt = $db->query($s_used);
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
      ?>
      <td><?=$result['currencycode']." ".number_format($result['revenue'])?></td>
      <?php
      }
      ?>
    </tr>
    <?php
      }
    ?>
    <tr class="hotel">
      <td>Total Revenue <?=$hotelchain['hotelname']?></td>
      <?php
      $revenue_rateplan = array();
      foreach($dateArr as $key => $date){
        $s_revenuehotel = "select cr.currencycode, sum(brd.total) as revenue from bookingroomdtl brd inner join currency cr using (currencyoid) inner join bookingroom br using (bookingroomoid) inner join booking b using (bookingoid) where b.hoteloid = '".$hotelchain['hoteloid']."' and brd.date = '".$date."' and b.bookingstatusoid = '4' and brd.reconciled = '0' and b.pmsstatus = '0'";
        $stmt = $db->query($s_revenuehotel);
        $revenuehotel = $stmt->fetch(PDO::FETCH_ASSOC);
      ?>
      <td><?=$revenuehotel['currencycode']." ".number_format($revenuehotel['revenue'])?></td>
      <?php
      }
      ?>
    </tr>
    <tr><td colspan="8">&nbsp;</td></tr>
    <?php
    }
    ?>
    <tr class="hotel">
      <td>Total Revenue All Hotel</td>
      <?php
      $revenue_rateplan = array();
      foreach($dateArr as $key => $date){
        $s_revenuechain = "select cr.currencycode, sum(brd.total) as revenue from bookingroomdtl brd inner join currency cr using (currencyoid) inner join bookingroom br using (bookingroomoid) inner join booking b using (bookingoid) inner join hotel h using (hoteloid) inner join chain c using (chainoid) where c.chainoid in (select oid from userassign where type = 'chainoid' and useroid = '".$_SESSION['_oid']."') and brd.date = '".$date."' and b.bookingstatusoid = '4' and brd.reconciled = '0' and b.pmsstatus = '0'";
        $stmt = $db->query($s_revenuechain);
        $revenuechain = $stmt->fetch(PDO::FETCH_ASSOC);
      ?>
      <td><?=$revenuechain['currencycode']." ".number_format($revenuechain['revenue'])?></td>
      <?php
      }
      ?>
    </tr>
    </tbody>
  </table>
