<?php
function createXML($path_xml_hotel){
    $doc = new DOMDocument('1.0');
	$doc->formatOutput = true;
	
	$data = $doc->createElement('data');
	
	$attributeName = array('hotel', 'source');
	$attributeValue = array($hoteloid, 'internal');
	
	foreach($attributeName as $key => $attr_name)
	{
		$attr_value = $attributeValue[$key];		

		$createAttr = $doc->createAttribute($attr_name);
		$createAttr->value = $attr_value;
		$data->appendChild($createAttr);
	}
	
	$doc->appendChild($data);
	$doc->save($path_xml_hotel);
}
?>