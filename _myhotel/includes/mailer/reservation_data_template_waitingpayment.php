<?php 
        echo "<script>console.log(".json_encode('waiting payment').");</script>"; 
    $s_master_booking	= "select b.*, b.note as guestrequest, c.*, cr.*, bs.note as status, bp.*, ct.countryname, cc.cardname, 
    h.banner, h.hotelname, b.bookingoid, h.email as hotelemail , h.address as hoteladdress 
	from booking b
	inner join hotel h using (hoteloid) 
	inner join bookingstatus bs using (bookingstatusoid) 
	inner join currency cr using (currencyoid) 
	inner join customer c using (custoid) 
	inner join country ct using (countryoid) 
	left join bookingpayment bp using (bookingoid) 
	left join creditcard cc using (cardoid)
    where b.bookingoid = '".$bookingoid."' group by b.bookingoid";
    
    // $r_master_booking = mysqli_query($conn, $s_master_booking);
    // while($databooked=mysqli_fetch_array($r_master_booking)){

        
    $r_master_booking = $db->query($s_master_booking);
    $databooked_data = $r_master_booking->fetch(PDO::FETCH_ASSOC);
    $databooked = $databooked_data;
    // echo "<script>console.log(".json_encode($databooked_data).");</script>"; 

    // foreach($databooked_data as $databooked){
        // echo "<script>console.log(".json_encode($databooked).");</script>"; 

		$hotelname 		= $databooked['hotelname'];
		$hotelemail 	= $databooked['hotelemail'];
		$hoteladdress 	= $databooked['hoteladdress'];
		$email			= $databooked['email'];
		$firstname		= $databooked['firstname'];  
		$lastname		= $databooked['lastname'];  
		$name			= $databooked['firstname']." ".$databooked['lastname'];  
		// $phone			= $databooked['phone']; 
		$city			= $databooked['city']; 
		$country		= $databooked['countryname'];
		$guestrequest	= $databooked['guestrequest'];
		if(!empty($databooked['banner'])){
			$emailbanner	= $databooked['banner'];
		}else{
			$emailbanner	= 'https://www.thebuking.com/ibe/image/header.jpg';
		}
		
		$bookingoid		= $databooked['bookingoid'];
		$bookingnumber	= $databooked['bookingnumber'];
        $grandtotal		= $databooked['grandtotal'];
        
		$grandcancellation = $databooked['cancellationamount'];
		$cancellationreason			= $databooked['cancellationreason'];
        
		$user_ip		= $databooked['user_ip'];
		$user_browser	= $databooked['user_browser'];
		
		$granddeposit	= $databooked['granddeposit'];
		$grandbalance	= $databooked['grandbalance'];
		$currency		= $databooked['currencycode'];		
		$paymentoid		= $databooked['paymentoid'];
		
		$memberoid		= $databooked['memberoid'];
		$point			= $databooked['point'];
	// }
	
	$query_detail_rsv = "select br.*, c.currencycode from bookingroom br inner join currency c using (currencyoid) where br.bookingoid = '".$bookingoid."'";
    
    // $result_detail = mysqli_query($conn, $query_detail_rsv);
    
    $qresult_detail = $db->query($query_detail_rsv);
    $result_detail = $qresult_detail->fetchAll(PDO::FETCH_ASSOC);
    // echo "<script>console.log(".json_encode($query_detail_rsv).");</script>"; 
    // echo "<script>console.log(".json_encode($result_detail).");</script>"; 
	
	$query_detail_extra = "select be.*, e.name, c.currencycode from bookingextra be inner join currency c using (currencyoid) inner join booking b using (bookingoid) inner join extra e using (extraoid) where b.bookingoid = '".$bookingoid."'";
    // $result_extra = mysqli_query($conn, $query_detail_extra);
    // $foundextra		= mysqli_num_rows($result_extra);
    
    
    $qresult_extra = $db->query($query_detail_extra);
    $foundextra = $qresult_extra->rowCount();
    $result_extra = $qresult_extra->fetchAll(PDO::FETCH_ASSOC);
    // echo "<script>console.log(".json_encode($result_extra).");</script>"; 

    $query_term_condition = "select br.* from bookingroom br where br.bookingoid = '".$bookingoid."' group by promotionoid";
    // $result_term_condition = mysqli_query($conn, $query_term_condition);
    // while($termcondition=mysqli_fetch_array($result_term_condition)){


    $qresult_term_condition = $db->query($query_term_condition);
    $result_term_condition = $qresult_term_condition->fetch(PDO::FETCH_ASSOC);
    // echo "<script>console.log(".json_encode($result_term_condition).");</script>"; 

    $rate_termcondition='';
    $rate_cancellationpolicy='';
    foreach($result_term_condition as $termcondition){
        $rate_termcondition=$termcondition['rate_termcondition'];
        $rate_cancellationpolicy=$termcondition['rate_cancellationpolicy'];
        if(empty($termcondition['rate_termcondition'])){ $rate_termcondition = "-"; }
        if(empty($termcondition['rate_cancellationpolicy'])){ $rate_cancellationpolicy = "-"; }
    }

    
    // ------------------------------------------


        $arrDataBooking = array();
        $arrDataBooking['booking_cancellation'] = $rate_cancellationpolicy;//
        $arrDataBooking['booking_term_condition'] = $rate_termcondition;//new
        $arrDataBooking['booking_min_checkin'] = '';
        $arrDataBooking['booking_max_checkout'] = '';
        $arrDataBooking['booking_number'] = $bookingnumber;//
        $arrDataBooking['booking_email_date'] = date("d F Y");//
        $arrDataBooking['booking_total_room_rate'] = $roomtotal;
        $arrDataBooking['booking_total_extrabed_date'] = '';
        $arrDataBooking['booking_total_extrabed_rate'] = '';
        $arrDataBooking['booking_grand_total'] = $grandtotal;//
        $arrDataBooking['booking_currency'] = $currency;//
        $arrDataBooking['booking_extra'] = $html_extra;//new
        $arrDataBooking['booking_grand_deposit'] = $currency." ".number_format($granddeposit);//new
        $arrDataBooking['booking_grand_balance'] = $currency." ".number_format($grandbalance);//new
        $arrDataBooking['booking_grand_cancellation'] = $currency." ".number_format($grandcancellation);//new
        $arrDataBooking['booking_cancellation_reason'] = $cancellationreason;//new

        $arrDataUser = array();
        $arrDataUser['user_ip'] = $user_ip;//new
        $arrDataUser['user_browser'] = $user_browser;//new
        
        $arrDataHotel = array();
        $arrDataHotel['hotel_banner'] = $emailbanner;//new
        $arrDataHotel['hotel_name'] = $hotelname;//
        $arrDataHotel['hotel_email'] = $hotelemail;//
        $arrDataHotel['hotel_address'] = $hoteladdress;//
        $arrDataHotel['hotel_city'] = $city;//

        $arrDataGuest = array();
        $arrDataGuest['guest_email'] = $email;//new
        $arrDataGuest['guest_phone'] = $phone;//new
        $arrDataGuest['guest_firstname'] = $firstname;//
        $arrDataGuest['guest_lastname'] = $lastname;//
        $arrDataGuest['guest_country'] = $country;//

        $arrDataRoom = array();
		$num=0;
        foreach($result_detail as $detail){
            $tmp = array();$num++;
            $tmp['no'] = $num;//
            $tmp['country'] = $country;//
            $tmp['occupancy'] = '';//
            $tmp['adult'] = $detail['adult'];//
            $tmp['child'] = $detail['child'];//
            $tmp['checkin'] = date("d M Y", strtotime($detail['checkin']));//
            $tmp['checkout'] = date("d M Y", strtotime($detail['checkout']));//
            $tmp['room'] = $detail['room'];//
            $tmp['note'] = $guestrequest;//
            $tmp['extrabed'] = $detail['extrabed'];//
            $tmp['inclusion'] = '';
            $tmp['number_room'] = '';
            $tmp['breakfast'] = $detail['breakfast'];//
            $tmp['room_total'] = $detail['currencycode']." ".number_format($detail['roomtotal']);//
            $tmp['total'] = $detail['currencycode']." ".number_format($detail['total']);//
            array_push($arrDataRoom, $tmp);
        }
        
        $arrDataExtra = array(); $num=0;
        foreach($result_extra as $extra){
            $tmp = array(); $num++;
            $tmp['no'] = $num;//
            $tmp['name'] = $extra['name'];//
            $tmp['qty'] = $extra['qty'];//
            $tmp['rate'] = $extra['currencycode']." ".number_format($extra['price']);//
            $tmp['total'] = $extra['currencycode']." ".number_format($extra['total']);//
            array_push($arrDataExtra, $tmp);
        }
	
    
        $arrDataTemplate = array();
        $arrDataTemplate['company'] = $_profile;
        $arrDataTemplate['booking'] = $arrDataBooking;
        $arrDataTemplate['user'] = $arrDataUser;
        $arrDataTemplate['hotel'] = $arrDataHotel;
        $arrDataTemplate['guest'] = $arrDataGuest;
        $arrDataTemplate['_detail'] = $arrDataRoom;
        $arrDataTemplate['_detail_extra'] = $arrDataExtra;

?>