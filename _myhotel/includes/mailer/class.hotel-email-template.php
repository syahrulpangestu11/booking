<?php
class HotelEmailTemplate{
    private $_template;
    private $_template_with_data;
    private $_email_type;
    private $_email_to;
    private $_data;
    private $_db;
    
    /* ---- VARIABLE FOR TEMPLATE----

        $booking_number;$booking_min_checkin;$booking_max_checkout;$booking_currency;$booking_extra;$booking_grand_deposit;$booking_grand_balance;$booking_grand_cancellation;
        $booking_cancellation_reason;$booking_total_room_rate;$booking_total_extrabed_rate;$booking_grandtotal;$booking_email_date;
        
        $hotel_name;$hotel_address;$hotel_city;$hotel_email;$hotel_banner;
        
        $user_ip;$user_browser;
        
        $guest_firstname;$guest_lastname;$guest_country;
        
        $company_imglogo;$company_name;$company_address;$company_city;$company_state;$company_country;$company_phone;$company_mobile;$company_email;$company_web;
        
        $_start_detail;
        $detail_no;$detail_adult;$detail_child;$detail_room_total;$detail_country;$detail_occupancy;$detail_checkin;$detail_checkout;$detail_room;
        $detail_extrabed;$detail_note;$detail_inclusion;$detail_number_room;$detail_breakfast;$detail_total;
        $_end_detail;
        
        $_start_detail_extra;
        $detail_extra_no;$detail_extra_name;$detail_extra_qty;$detail_extra_rate;$detail_extra_total;
        $_end_detail_extra;
    */

    public function __construct($db, $email_type, $email_to, $data){
        $this->_db=$db;
        $this->_email_type=$email_type;
        $this->_email_to=$email_to;
        $this->_data=$data;
    }
  
    function getTemplate(){ 
        $query = $this->_db->query("SELECT * FROM mastertemplate_email 
                                        WHERE masterprofileoid = 1 
                                                AND publishedoid = 1 
                                                AND email_type = '$this->_email_type' 
                                                AND email_to = '$this->_email_to'");

        $result = $query->fetch(PDO::FETCH_ASSOC);
        $_template = $result['email_body'];
        $this->_template = $_template;

        $this->getTemplateWithData(); 

        return $this->_template_with_data;
    }

    private function getTemplateWithData(){
        $_template = $this->_template;

        foreach($this->_data as $key => $dataArray){
            $isLoop = preg_match('/^[_](.*)$/', $key, $dataLoop);
            $dataName = ( $isLoop ? $dataLoop[1] : $key );

            if($isLoop){
                $_template = $this->replaceDataLoop($dataName,$dataArray,$_template);
            }else{
                $_template = $this->replaceDataArray($dataName,$dataArray,$_template);
            }

        }
        
        $this->_template_with_data = $_template;
    }

    private function replaceDataArray($dataName,$dataArray,$_template){

        //if company then remove prefix
        $regex = ( $dataName == 'company' ? '/[\A$]'.$dataName.'_(\w+)[;\z]/' : '/[\A$]('.$dataName.'_\w+)[;\z]/' );

        preg_match_all($regex, $_template, $_template_match);
        
        foreach($_template_match[0] as $key => $value){ 
            $_template = str_replace($value,$dataArray[$_template_match[1][$key]],$_template); 
        }

        return $_template;
    }

    private function replaceDataLoop($dataName,$dataArray,$_template){

        preg_match_all('/[$]_start_'.$dataName.';(.*?)[$]_end_'.$dataName.';/sm', $_template, $_template_match_loop);
        preg_match_all('/[\A$]'.$dataName.'_(\w+)[;\z]/', $_template_match_loop[1][0], $_template_match_loop_item);

        $_template = preg_replace('/[$]_start_'.$dataName.';(.*?)[$]_end_'.$dataName.';/sm', '$_loop_'.$dataName.'_here;', $_template); 
        $html_loop = ''; 

        foreach($dataArray as $data){
            $template_loop = $_template_match_loop[1][0];

            foreach($_template_match_loop_item[0] as $key => $value){ 
                $template_loop = str_replace($value,$data[$_template_match_loop_item[1][$key]],$template_loop);  
            }

            $html_loop .=  $template_loop; 
        }
        
        $_template = str_replace('$_loop_'.$dataName.'_here;', $html_loop, $_template);

        return $_template;
    }

}
?>