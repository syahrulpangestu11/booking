<?php
	$s_master_booking	= "select b.*, b.note as guestrequest, c.*, cr.*, bs.note as status, bp.*, ct.countryname, cc.cardname, h.banner, h.hotelname, h.email as hotelemail
	,h.address as hoteladdress
	from booking b
	inner join hotel h using (hoteloid)
	inner join bookingstatus bs using (bookingstatusoid)
	inner join currency cr using (currencyoid)
	inner join customer c using (custoid)
	inner join country ct using (countryoid)
	left join bookingpayment bp using (bookingoid)
	left join creditcard cc using (cardoid)
	where b.bookingoid = '".$bookingoid."' group by b.bookingoid";
	$stmt				= $db->query($s_master_booking);
	$r_master_booking	= $stmt->fetchAll(PDO::FETCH_ASSOC);
	foreach($r_master_booking as $databooked){
		$hotelname 		= $databooked['hotelname'];
		$hotelemail		= $databooked['hotelemail'];
		$hoteladdress	= $databooked['hoteladdress'];
		$email			= $databooked['email'];
		$name			= $databooked['firstname']." ".$databooked['lastname'];
		$firstname			= $databooked['firstname'];
		$lastname			= $databooked['lastname'];
		$phone			= $databooked['phone'];
		$city			= $databooked['city'];
		$country		= $databooked['countryname'];
		$guestrequest	= $databooked['guestrequest'];
		if(!empty($databooked['banner'])){
			$emailbanner	= $databooked['banner'];
		}else{
			$emailbanner	= 'https://www.thebuking.com/ibe/image/header.jpg';
			// $emailbanner	= '';
		}

		$user_ip		= $databooked['user_ip'];
		$user_browser	= $databooked['user_browser'];

		//$bookingoid		= $databooked['bookingoid'];
		$bookingnumber	= $databooked['bookingnumber'];
		$grandtotal		= $databooked['grandtotal'];
		$grandbalance	= $databooked['grandbalance'];

		$grandcancellation = $databooked['cancellationamount'];
		$currency		= $databooked['currencycode'];
		$paymentoid		= $databooked['paymentoid'];

		$memberoid		= $databooked['memberoid'];
		$point			= $databooked['point'];
		$cancellationreason			= $databooked['cancellationreason'];
	}

	$query_detail_rsv = "select br.*, c.currencycode from bookingroom br left join currency c using (currencyoid) where br.bookingoid = '".$bookingoid."'";
	$stmt			= $db->query($query_detail_rsv);
	$result_detail	= $stmt->fetchAll(PDO::FETCH_ASSOC);

	$query_detail_extra = "select be.*, e.name, c.currencycode from bookingextra be left join currency c using (currencyoid) inner join booking b using (bookingoid) inner join extra e using (extraoid) where b.bookingoid = '".$bookingoid."'";
	$stmt			= $db->query($query_detail_extra);
	$foundextra		= $stmt->rowCount();
	$result_extra	= $stmt->fetchAll(PDO::FETCH_ASSOC);

	
    $html_extra='';
    if($foundextra > 0){
        $num = 0;
        // while($extra=mysqli_fetch_array($result_extra)){
		foreach($result_extra as $extra){
			$html_extra.=' <p><b>'.$extra['name'].'<br>Qty '.$extra['qty'].'</td> &nbsp;-&nbsp; '.$extra['currencycode'].' '.number_format($extra['total']).'</p>';
			/*$html_extra.='<tr style="vertical-align:top;">
			<td style="padding : 5px 2px; border-bottom:1px solid #d0d0d0; text-align:right"><?=++$num?>.</td>
			<td style="padding : 5px 2px; border-bottom:1px solid #d0d0d0;">'.$extra['name'].'</td>
			<td style="padding : 5px 2px; border-bottom:1px solid #d0d0d0; text-align:center">'.$extra['name'].'</td>
			<td style="padding : 5px 2px; border-bottom:1px solid #d0d0d0;">'.$extra['currencycode'].' '.number_format($extra['price']).'</td>
			<td style="padding : 5px 2px; border-bottom:1px solid #d0d0d0;">'.$extra['currencycode'].' '.number_format($extra['total']).'</td></tr>';
			*/
        }
	}
	
	
	$rate_cancellationpolicy=''; $rate_termcondition='';$rate_promotion='';
	$query_term_condition = "select br.* from bookingroom br where br.bookingoid = '".$bookingoid."' group by promotionoid";
	$stmt	= $db->query($query_term_condition);
	$result_term_condition = $stmt->fetchAll(PDO::FETCH_ASSOC);
	foreach($result_term_condition as $key => $termcondition){
		$rate_cancellationpolicy = $termcondition['rate_cancellationpolicy'];
		$rate_termcondition = $termcondition['rate_termcondition'];
		$rate_promotion = $termcondition['promotion'];
	} 
    // ------------------------------------------

        $arrDataBooking = array();
        $arrDataBooking['booking_cancellation'] = $rate_cancellationpolicy;//
        $arrDataBooking['booking_term_condition'] = $rate_termcondition;//
        $arrDataBooking['booking_promotion'] = $rate_promotion;//new
        $arrDataBooking['booking_min_checkin'] = '';
        $arrDataBooking['booking_max_checkout'] = '';
        $arrDataBooking['booking_number'] = $bookingnumber;//
        $arrDataBooking['booking_email_date'] = date("d F Y");//
        $arrDataBooking['booking_total_room_rate'] = '';
        $arrDataBooking['booking_total_extrabed_date'] = '';
        $arrDataBooking['booking_total_extrabed_rate'] = '';
        $arrDataBooking['booking_grand_total'] = $currency." ".number_format($grandtotal);//
        $arrDataBooking['booking_currency'] = $currency;//
        $arrDataBooking['booking_extra'] = $html_extra;//
        $arrDataBooking['booking_grand_deposit'] = '';
        $arrDataBooking['booking_grand_balance'] = '';
        $arrDataBooking['booking_grand_cancellation'] = $currency." ".number_format($grandcancellation);//new
        $arrDataBooking['booking_cancellation_reason'] = $cancellationreason;//new

        $arrDataUser = array();
        $arrDataUser['user_ip'] = $user_ip;//
        $arrDataUser['user_browser'] = $user_browser;//
        
        $arrDataHotel = array();
        $arrDataHotel['hotel_banner'] = $emailbanner;//new
        $arrDataHotel['hotel_name'] = $hotelname;//
        $arrDataHotel['hotel_email'] = $hotelemail;//
        $arrDataHotel['hotel_address'] = $hoteladdress;//
        $arrDataHotel['hotel_city'] = $city;//

        $arrDataGuest = array();
        $arrDataGuest['guest_email'] = $email;//new
        $arrDataGuest['guest_phone'] = $phone;//new
        $arrDataGuest['guest_firstname'] = $firstname;//
        $arrDataGuest['guest_lastname'] = $lastname;//
        $arrDataGuest['guest_country'] = $country;//

        $arrDataRoom = array();

		$num=0;
        foreach($result_detail as $detail){
            $tmp = array();$num++;
            $tmp['no'] = $num;//
            $tmp['country'] = $country;
            $tmp['occupancy'] = '';
            $tmp['adult'] = $detail['adult'];//
            $tmp['child'] = $detail['child'];//
            $tmp['checkin'] = date("d M Y", strtotime($detail['checkin']));//
            $tmp['checkout'] = date("d M Y", strtotime($detail['checkout']));//
            $tmp['room'] = $detail['room'];//
            $tmp['note'] = $detail['promotion'].'<br>'.$guestrequest;//
            $tmp['extrabed'] = ($detail['extrabed']=='y'?'Yes<br>('.$detail['currencycode']." ".number_format($detail['extrabedtotal']).')':'No');//
            $tmp['inclusion'] = '';
            $tmp['number_room'] = '';
            $tmp['breakfast'] = $detail['breakfast'];
            $tmp['room_total'] = $detail['currencycode']." ".number_format($detail['roomtotal']);//
            $tmp['total'] = $detail['currencycode']." ".number_format($detail['total']);//
            array_push($arrDataRoom, $tmp);
        }
        
        $arrDataExtra = array(); $num=0;
        foreach($result_extra as $extra){
            $tmp = array(); $num++;
            $tmp['no'] = $num;//
            $tmp['name'] = $extra['name'];//
            $tmp['qty'] = $extra['qty'];//
            $tmp['rate'] = $extra['currencycode']." ".number_format($extra['price']);//
            $tmp['total'] = $extra['currencycode']." ".number_format($extra['total']);//
            array_push($arrDataExtra, $tmp);
        }
	
        $arrDataTemplate = array();
        $arrDataTemplate['company'] = $_profile;
        $arrDataTemplate['booking'] = $arrDataBooking;
        $arrDataTemplate['user'] = $arrDataUser;
        $arrDataTemplate['hotel'] = $arrDataHotel;
        $arrDataTemplate['guest'] = $arrDataGuest;
        $arrDataTemplate['_detail'] = $arrDataRoom;
		$arrDataTemplate['_detail_extra'] = $arrDataExtra;
		
?>