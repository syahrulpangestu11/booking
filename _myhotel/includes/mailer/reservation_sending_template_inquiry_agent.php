<?php

    $mail = new PHPMailer(true); // the true param means it will throw exceptions on errors, which we need to catch
    $mail->IsSMTP(); // telling the class to use SMTP
    try {
        $sql_mailer = "SELECT * FROM mailercompany INNER JOIN mailertype USING (mailertypeoid) WHERE companyoid='1' and publishedoid='1'";
        $qrun_mailer = $db->query($sql_mailer);
        $run_mailer = $qrun_mailer->fetchAll(PDO::FETCH_ASSOC);
        foreach($run_mailer as $row_mailer){
            switch($row_mailer['mailertypeoid']){
                case 1 : $mail->SetFrom($_profile['SMTP_Username'], $row_mailer['name']); break;
                case 2 : $mail->AddAddress($row_mailer['email'], $row_mailer['name']); break;
                case 3 : $mail->AddCC($row_mailer['email'], $row_mailer['name']); break;
                case 4 : $mail->AddBCC($row_mailer['email'], $row_mailer['name']); break;
                case 5 : $mail->AddReplyTo($row_mailer['email'], $row_mailer['name']); break;
            }
        }
        
        $mail->AddAddress($agent_mail,$agent_mail);
        $mail->Subject = $_profile['name']." [G] Booking Inquiry for [".$hotelname."] - [".$bookingnumber."]";
        $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!'; // optional - MsgHTML will create an alternate automatically
        $mail->MsgHTML($body_inquiry);
        
        unset($_SESSION['tokenSession']);
        unset($bookingoid);
        if($mail->Send()) {
            echo"<script>window.location.href = '".$base_url."/booking-all/detail/?no=".$bookingnumber."'</script>";
        }
        
    } catch (phpmailerException $e) { echo"<b>Php Mailer Error [guest] :</b><br>"; echo $e->errorMessage();//Pretty error messages from PHPMailer
    } catch (Exception $e) { echo"<b>All Error :</b><br>"; echo $e->getMessage(); //Boring error messages from anything else!
    } 

?>