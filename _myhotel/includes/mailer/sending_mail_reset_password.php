<?php
	ob_start();
	include 'mail-content/mail-reset-password.php';
	$body = ob_get_clean();

	require_once('includes/mailer/class.phpmailer.php');
	$mail = new PHPMailer(true); // the true param means it will throw exceptions on errors, which we need to catch
	$mail->IsSMTP(); // telling the class to use SMTP
	try {
	    
	    $mail->SMTPDebug = 0;
		//Ask for HTML-friendly debug output
		$mail->Debugoutput = 'html';
		//Set the hostname of the mail server
		$mail->Host = "smtp.gmail.com";
		//Set the SMTP port number - likely to be 25, 465 or 587
		$mail->SMTPSecure = 'tls';
		$mail->Port = 587;
		//Whether to use SMTP authentication
		$mail->SMTPAuth = true;
		//Username to use for SMTP authentication
		$mail->Username = "support@thebuking.com";//"info@thebuking.com";
		//Password to use for SMTP authentication
		$mail->Password = "TH3buk!n9J4Y4";//"KofiEn4k";

		$mail->SetFrom($sender_email, $sender_name);
		$mail->AddReplyTo($sender_email, $sender_name);
		
		$mail->AddAddress($recipient_email, $recipient_name);
	
		
		$mail->Subject = 'The Buking Extranet Password Reset Request';
		$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!'; // optional - MsgHTML will create an alternate automatically
		$mail->MsgHTML($body);
	
		if($mail->Send()) {
			echo"<script>document.location.href='".$base_url."/resetsuccess';</script>";
		}else{
			echo"<script>document.location.href='".$base_url."/resetfailed';</script>";
		}
		
    } catch (phpmailerException $e) { echo"<script>document.location.href='".$base_url."/resetfailed';</script>";
	} catch (Exception $e) { echo"<script>document.location.href='".$base_url."/resetfailed';</script>";
	} 
?>