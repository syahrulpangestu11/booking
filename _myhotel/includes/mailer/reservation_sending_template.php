<?php

    // require_once('conf/connection_withpdo.php');
	echo "<script>console.log(".json_encode('template').");</script>"; 
    require_once('includes/mailer/class.phpmailer.php');
    require_once('includes/mailer/class.hotel-email-template.php');

	$main_query="SELECT customer.firstname, customer.lastname, customer.email, 
                    #agent.email as agentemail, agent.agentname,
                    booking.hoteloid,booking.bookingnumber,booking.bookingstatusoid,hotel.hotelname
                from booking 
                inner join hotel using (hoteloid) 
                #inner join agent using (agentoid) 
                inner join bookingstatus using (bookingstatusoid) 
                inner join currency using (currencyoid) 
                inner join customer using (custoid) 
                inner join country using (countryoid) 
                left join bookingpayment using (bookingoid) 
                where booking.bookingoid = '".$bookingoid."' group by booking.bookingoid";
    $main_stmt = $db->query($main_query);
    $main_data = $main_stmt->fetch(PDO::FETCH_ASSOC);

	echo "<script>console.log(".json_encode($main_query).");</script>"; 
    echo "<script>console.log(".json_encode($main_data).");</script>"; 
    
    $bookingstatusoid = $main_data['bookingstatusoid'];//
    $inquiry_status = ($bookingstatusoid==1?true:false);
	$bookingnumber = $main_data['bookingnumber'];
	$hotelname = $main_data['hotelname'];
    $hoteloid = $main_data['hoteloid'];
    
	$guestname = $main_data['firstname']." ".$main_data['lastname'];
	$guestemail = $main_data['email'];
    $agent_name = $guestname;
	$agent_mail = $guestemail;
	// $agent_mail = $main_data['agentemail'];
	// $agent_name = $main_data['agentname'];
	// $agent_mail = '';
    // $agent_name = '';
    
    switch($bookingstatusoid){
        case 1: //inquiry
            include 'reservation_data_template_inquiry.php';

            ob_start();
            $template = new HotelEmailTemplate($db,'inquiry','agent',$arrDataTemplate);
            $html_template = $template->getTemplate();
            echo $html_template;
            $body_inquiry = ob_get_clean();

            include('reservation_sending_template_inquiry_agent.php');
            break;

        case 3: //waitingpayment
            include 'reservation_data_template_waitingpayment.php';

            ob_start();
            $template = new HotelEmailTemplate($db,'waiting payment','agent',$arrDataTemplate);
            $html_template = $template->getTemplate();
            echo $html_template;
            $body_waiting_payment = ob_get_clean();
            
            include('reservation_sending_template_waitingpayment_agent.php');
            break;

        case 4: //confirmation
            include 'reservation_data_template_confirmation.php';
            echo "<script>console.log(".json_encode($arrDataTemplate).");</script>"; 

            ob_start();
            $template = new HotelEmailTemplate($db,'confirmation','guest',$arrDataTemplate);
            $html_template = $template->getTemplate();
            echo $html_template;
            $body = ob_get_clean();
            
            ob_start();
            $template = new HotelEmailTemplate($db,'confirmation','agent',$arrDataTemplate);
            $html_template = $template->getTemplate();
            echo $html_template;
            $body_send_to_agent = ob_get_clean();

            ob_start();
            $template = new HotelEmailTemplate($db,'confirmation','hotel',$arrDataTemplate);
            $html_template = $template->getTemplate();
            echo $html_template;
            $body_send_to_hotel = ob_get_clean();
            
            // die();
            
            include('reservation_sending_template_confirmation_guest.php');
            break;
    }


?>