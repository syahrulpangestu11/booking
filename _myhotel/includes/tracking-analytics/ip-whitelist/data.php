<?php
	session_start();
	error_reporting(E_ALL & ~E_NOTICE);
	include("../../../conf/connection.php");
	include("../../paging/pre-paging.php");
	
	$main_query = "select * from ip_whitelist";
	
	$filter = array();
		array_push($filter, 'hoteloid = "'.$_SESSION['_hotel'].'"');
	if(isset($_REQUEST['keyword']) and $_REQUEST['keyword']!=''){
		array_push($filter, '(ip like "%'.$_REQUEST['keyword'].'%" or note like "%'.$_REQUEST['keyword'].'%")');
	}
	
	if(count($filter) > 0){
		$combine_filter = implode(' and ',$filter);
		$query = $main_query.' where '.$combine_filter;
	}else{
		$query = $main_query;
	}
?>
<table class="table table-striped">
    <thead>
        <tr>
            <th>No.</th>
            <th>IP Number</th>
            <th>Note</th>
            <th></th>
        </tr>
    </thead> 
    <tbody>
    <?php
		$main_query = $query." order by ip DESC ";
		$stmt = $db->query($query.$paging_query);
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
			$r_ip = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_ip as $row){
	?>
    	<tr>
        	<td><?=++$num?></td>
            <td><?=$row['ip'];?></td>
            <td><?=$row['note'];?></td>
            <td class="text-right">
            	<button type="button" class="btn btn-sm btn-primary" data-ipid="<?=$row['ip_whitelistoid'];?>" data-toggle="modal" data-target="#EditIP"><i class="fa fa-pencil"></i></button>
            	<button type="button" class="btn btn-sm btn-danger" data-ipid="<?=$row['ip_whitelistoid'];?>"  data-ip="<?=$row['ip'];?>" data-toggle="modal" data-target="#DeleteIP"><i class="fa fa-trash"></i></button>
			</td> 
        </tr>
    <?php
			}
		}
	?>
    </tbody>
</table>
<?php
	$main_count_query = "select count(*) from ip_whitelist";
	if(count($filter) > 0){
		$combine_filter = implode(' and ',$filter);
		$main_count_query = $main_count_query.' where '.$combine_filter;
	}
	$stmt = $db->query($main_count_query);
	$jmldata = $stmt->fetchColumn();

	$tampildata = $row_count;
	include("../../paging/post-paging.php");
?>

