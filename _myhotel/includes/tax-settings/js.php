<script type="text/javascript">
$(function(){	
	var $dialogNotice = $('<div id="dialog-notice"></div>')
	.dialog({
		autoOpen: false,
		title: 'Notification',
		buttons: { 
			"Ok": function(){ 
				$( this ).dialog( "close" );
				$(location).attr("href", "<?php echo $base_url; ?>/tax-settings"); 
			}
		}
	});

	$('body').on('click','button.add-button', function(e) {
		url = "<?php echo $base_url; ?>/tax-settings/add";
      	$(location).attr("href", url);
	});
	
	$('body').on('click','button.edit-button', function(e) {
		to = $(this).attr("to");
		url = "<?php echo $base_url; ?>/tax-settings/edit/?to="+to;
      	$(location).attr("href", url);
	});
	
	$('button.submit-add').click(function(){
		url = '<?php echo $base_url; ?>/includes/tax-settings/add-tax-settings-save.php';
		forminput = $('form#data-input');
		textsuccess = 'Data succesfully saved';
		submitData(url, forminput);
	});
	
	$('button.submit-edit').click(function(){
		url = '<?php echo $base_url; ?>/includes/tax-settings/edit-tax-settings-save.php';
		forminput = $('form#data-input');
		textsuccess = 'Data succesfully saved';
		submitData(url, forminput);
	});
	
	function submitData(url, forminput){
		$.ajax({
			url: url,
			type: 'post',
			data: forminput.serialize(),
			success: function(data) {
				//alert(data);
				if(data == "1"){
					$dialogNotice.html("Data succesfully updated");
					$dialogNotice.dialog("open");
				}else{
					$dialogNotice.html("We&rsquo;re very sorry, we can't save your data right now");
					$dialogNotice.dialog("open");
				}
			}
		});
	}
	
   var $dialogDel = $('<div id="dialog-del"></div>')
	.html('Are you sure you want to delete this data?')
	.dialog({
		autoOpen: false,
		title: 'Confirmation',
		buttons: { 
			"Delete": function(){ 
				var to = $(this).data('to'); 
				deleteProcess(to);
				$( this ).dialog( "close" ); 
			},
			Cancel: function(){ $( this ).dialog( "close" ); }
		}
	});

	
	$('body').on('click','button.delete-button', function(e) {
		to = $(this).attr("to");
		$dialogDel.data("to", to);
		$dialogDel.dialog("open");
	});

	function deleteProcess(to){
		$.ajax({
			url: '<?php echo $base_url; ?>/includes/tax-settings/delete-tax-settings.php',
			type: 'post',
			data: { to : to},
			success: function(data) {
				if(data == "1"){
					$dialogNotice.html("Data has been deleted");
					$dialogNotice.dialog("open");
				}else{
					$dialogNotice.html("We&rsquo;re very sorry, we can't save your data right now");
					$dialogNotice.dialog("open");
				}
			}
		});
	}

});
</script>