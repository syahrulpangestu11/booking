<section class="content-header">
    <h1>
        Add Tax Settings
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Settings</a></li>
        <li><a href="#"><i class="fa"></i> Tax Settings</a></li>
        <li class="active">Add</li>
    </ol>
</section>
<section class="content">
   
    <div class="row">
        <div class="box box-form">
            <h1>Add new tax settings</h1>
            <form method="post" enctype="multipart/form-data" id="data-input" action="#">
            <input type="hidden" name="hoteloid" value="<?php echo $hoteloid; ?>" />
            <div class="side-left">
                <div class="form-input">
                    <label>Start Date</label>
                    <input type="text" class="input-text" name="startdate" id="startdate">
                    <label>End Date</label>
                    <input type="text" class="input-text" name="enddate" id="enddate">
                    <label>Tax Type</label> 
                    <select name="taxtype" class="input-select">
                        <?php
                        try {
                            $stmt = $db->query("select * from taxtype");
                            $r_tax = $stmt->fetchAll(PDO::FETCH_ASSOC);
                            foreach($r_tax as $row){
                        ?>
                            <option value="<?php echo $row['taxtypeoid']; ?>"><?php echo $row['name']; ?></option>
                        <?php	
                            }
                        }catch(PDOException $ex) {
                            echo "Invalid Query";
                            die();
                        }
                        ?>
                    </select>
					<label>Charge Type</label> 
                    <select name="chargetype" class="input-select">
                        <?php
                        try {
                            $stmt = $db->query("select * from chargetype");
                            $r_tax = $stmt->fetchAll(PDO::FETCH_ASSOC);
                            foreach($r_tax as $row){
                        ?>
                            <option value="<?php echo $row['chargetypeoid']; ?>"><?php echo $row['name']; ?></option>
                        <?php	
                            }
                        }catch(PDOException $ex) {
                            echo "Invalid Query";
                            die();
                        }
                        ?>
                    </select>
					&nbsp;&nbsp;<input type="checkbox" name='fee' value="y"> Fee
                </div>
            </div>
            <div class="side-right">
                <div class="form-input">
                    <label>Tax Value (%)</label>
                    <input type="text" class="input-text" name="taxpercent">
                    <label>Publish this tax?</label> 
                    <select name="published" class="input-select">
                        <?php
                        try {
                            $stmt = $db->query("select * from published where showthis = 'y'");
                            $r_published = $stmt->fetchAll(PDO::FETCH_ASSOC);
                            foreach($r_published as $row){
                        ?>
                            <option value="<?php echo $row['publishedoid']; ?>"><?php echo $row['note']; ?></option>
                        <?php	
                            }
                        }catch(PDOException $ex) {
                            echo "Invalid Query";
                            die();
                        }
                        ?>
                    </select>
                    <br>
                    <button type="button" class="submit-add">Submit new tax</button>
                </div>
            </div>
            </form>
            <div class="clear"></div>
       	</div>
    </div>
    
</section>