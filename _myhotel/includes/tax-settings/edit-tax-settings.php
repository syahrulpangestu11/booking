<?php
	$taxoid = $_GET['to'];
	try {
		$stmt = $db->query("select tax.* from tax inner join hotel h using (hoteloid) where taxoid = '".$taxoid."'");
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
			$r_tax = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_tax as $row){
				$startdate = date("d F Y", strtotime($row['startdate']));
				$enddate = date("d F Y", strtotime($row['enddate']));
				$taxtype = $row['taxtypeoid'];
				$chargetype = $row['chargetypeoid'];
				$fee = $row['fee'];
				$taxpercent = $row['taxpercent'];
				$publishedoid = $row['publishedoid'];
			}
		}else{
			echo "No Result";
			die();
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}
?>
<section class="content-header">
    <h1>
        Edit Tax Settings
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Settings</a></li>
        <li><a href="#"><i class="fa"></i> Tax Settings</a></li>
        <li class="active">Edit</li>
    </ol>
</section>
<section class="content">
   
    <div class="row">
        <div class="box box-form">
            <h1>Edit tax settings</h1>
            <form method="post" enctype="multipart/form-data" id="data-input" action="#">
            <input type="hidden" name="taxoid" value="<?php echo $taxoid; ?>" />
            <div class="side-left">
                <div class="form-input">
                    <label>Start Date</label>
                    <input type="text" class="input-text" name="startdate" id="startdate" value="<?php echo $startdate; ?>">
                    <label>End Date</label>
                    <input type="text" class="input-text" name="enddate" id="enddate" value="<?php echo $enddate; ?>">
                    <label>Tax Type</label> 
                    <select name="taxtype" class="input-select">
                        <?php
                        try {
                            $stmt = $db->query("select * from taxtype");
                            $r_tax = $stmt->fetchAll(PDO::FETCH_ASSOC);
                            foreach($r_tax as $row){
								if($row['taxtypeoid'] == $taxtype){ $selected = "selected"; }else{ $selected=""; }

                        ?>
                            <option value="<?php echo $row['taxtypeoid']; ?>" <?php echo $selected; ?>><?php echo $row['name']; ?></option>
                        <?php	
                            }
                        }catch(PDOException $ex) {
                            echo "Invalid Query";
                            die();
                        }
                        ?>
                    </select>
					<label>Charge Type</label> 
                    <select name="chargetype" class="input-select">
                        <?php
                        try {
                            $stmt = $db->query("select * from chargetype");
                            $r_tax = $stmt->fetchAll(PDO::FETCH_ASSOC);
                            foreach($r_tax as $row){
								if($row['chargetypeoid'] == $chargetype){ $selected = "selected"; }else{ $selected=""; }
                        ?>
                            <option value="<?php echo $row['chargetypeoid']; ?>" <?php echo $selected; ?>><?php echo $row['name']; ?></option>
                        <?php	
                            }
                        }catch(PDOException $ex) {
                            echo "Invalid Query";
                            die();
                        }
                        ?>
                    </select>
					<?php if($fee == 'y'){ $checked = "checked"; }else{ $checked=""; } ?>
					<input type="checkbox" name="fee" <?php echo $checked; ?> value="y"> Fee
                </div>
            </div>
            <div class="side-right">
                <div class="form-input">
                    <label>Tax Value (%)</label>
                    <input type="text" class="input-text" name="taxpercent" value="<?php echo $taxpercent; ?>">
                    <label>Publish this tax?</label> 
                    <select name="published" class="input-select">
                        <?php
                        try {
                            $stmt = $db->query("select * from published where showthis = 'y'");
                            $r_published = $stmt->fetchAll(PDO::FETCH_ASSOC);
                            foreach($r_published as $row){
								if($row['publishedoid'] == $publishedoid){ $selected = "selected"; }else{ $selected=""; }
                        ?>
                            <option value="<?php echo $row['publishedoid']; ?>" <?php echo $selected; ?>><?php echo $row['note']; ?></option>
                        <?php	
                            }
                        }catch(PDOException $ex) {
                            echo "Invalid Query";
                            die();
                        }
                        ?>
                    </select>
                    <br>
                    <button type="button" class="submit-edit">Edit Tax</button>
                </div>
            </div>
            </form>
            <div class="clear"></div>
       	</div>
    </div>
    
</section>