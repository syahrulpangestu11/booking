<script type="text/javascript">
$(function(){
	
	$(document).ready(function(){ 
	});
	
	$('body').on('click','button.edit', function(e) {
		rt = $(this).attr("rt");
		url = "<?php echo $base_url; ?>/room-settings/edit/?rt="+rt;
      	$(location).attr("href", url);
	});
	
	$('body').on('click','button.add', function(e) {
		tableTarget = $('table.table-fill');
		parentTable = $(this).parent().parent().parent();
		
		name = parentTable.find('input[name=offername]').val();
		offertype = parentTable.find('select[name=offertype]').val();
		type = parentTable.find('select[name=offertype] option:selected').text();
		minrate = parentTable.find('input[name=offerminrate]').val();
		
		if(name.length !== 0){
		
			tableTarget.append("<tr><td></td><td><input type = 'text' class = 'medium' name = 'new_name[]' value = '"+name+"'></td><td class='select_type'></td><td><input type = 'text' class = 'medium' name = 'new_minrate[]' value = '"+minrate+"'></td><td>&nbsp;</td><td><button class='trash delete'>delete</button></td></tr>");
			
			appendDestination = $('.select_type').last();
			$('select[name=offertype]').clone().attr('name','new_type[]').appendTo(appendDestination);
			newSelect = $('select[name="new_type[]"]').last();
			newSelect.val(offertype);
		
			$(this).parent().parent().find('input').val('');
			$(this).parent().parent().find('select').prop('seletedIndex', 0);
		}
		
		
		
	});

   var $dialogDelete = $('<div id="dialog-del"></div>')
	.html('Are you sure you want to delete this data?')
	.dialog({
		autoOpen: false,
		title: 'Confirmation',
		buttons: { 
			"Delete": function(){ 
				var elem = $(this).data('elem'); 
				$(this).dialog("close"); 
				deleteElem(elem);
			},
			Cancel: function(){ $( this ).dialog("close"); }
		}
	});
	
	var $dialogNotification = $('<div id="dialog-notice"></div>')
	.dialog({
		autoOpen: false,
		title: 'Notification',
		buttons: { 
			"Ok": function(){ 
				var callback = '<?php echo $base_url; ?>/promotions'; 
				$(this).dialog( "close" );
				$(location).attr("href", callback); 
			}
		}
	});
	
	function deleteAction(url, id){
		$.ajax({
			url: url,
			type: 'post',
			data: { id : id},
			success: function(data) {
				if(data == "1"){
					$dialogNotification.html("Data has been deleted");
					$dialogNotification.dialog("open");
				}else{
					$dialogNotification.html("We&rsquo;re very sorry, we can't save your data right now");
					$dialogNotification.dialog("open");
				}
			}
		});
	}
	
	function deleteElem(elem){
		elem.parent().parent().remove();
	}
	
	$('body').on('click','button.delete', function(e) {
		id = $(this).attr("pid");
		$dialogDelete.data("url", "<?php echo $base_url; ?>/includes/promotion/delete-promotion.php");
		$dialogDelete.data("id", id);
		$dialogDelete.dialog("open");
	});
	
	$('body').on('click','button.delete', function(e) {
		$dialogDelete.data("elem", $(this));
		$dialogDelete.dialog("open");
	});
			

});
</script>