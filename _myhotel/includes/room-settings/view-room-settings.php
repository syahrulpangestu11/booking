<?php
	include("js.php");
?>
<section class="content-header">
    <h1>
       	Room Settings
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Settings</a></li>
        <li class="active">Room Settings</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="box box-form">
            <div class="box-header with-border">
                <h1>List of Rooms</h1>
            </div>
			<div class="box-body">
				<?php
                    try {
                        $stmt = $db->query("select r.roomoid, r.name, r.priority, count(roomofferoid) as numoffer from room r inner join published p using (publishedoid) inner join hotel h using (hoteloid) left join roomoffer ro on r.roomoid = ro.roomoid where h.hoteloid = '".$hoteloid."' and p.publishedoid not in (3) group by r.roomoid");
                        $row_count = $stmt->rowCount();
                       
                ?>
                <table class="table promo-table">
                <?php
                            $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                            foreach($r_room as $row){
								$stmt_roomphoto = $db->query("SELECT count(photourl) as numphoto FROM hotelphoto hp WHERE hp.hoteloid = '".$hoteloid."' AND ref_table = 'room' AND ref_id = '".$row['roomoid']."'");
								$r_roomphoto = $stmt_roomphoto->fetchAll(PDO::FETCH_ASSOC);
								foreach($r_roomphoto as $roomphoto){
									$jml_roomphoto = $roomphoto['numphoto'];
								}
                ?>
                    <tr>
                        <td><?php echo $row['name']; ?></td>
                        <td>Total No. of Room Offers : <?php echo $row['numoffer']; ?></td>
                        <td>Total No. of Room Photos : <?php echo $jml_roomphoto; ?></td>
                        <td>Room Order : <?php echo $row['priority']; ?></td>
                        <td class="algn-right">
                        <button type="button" class="pencil edit" rt="<?php echo $row['roomoid']; ?>">Edit</button>
                        <button type="button" class="trash delete" rt="<?php echo $row['roomoid']; ?>">Delete</button>
                        </td>
                    </tr>	
                <?php				
                            }
                ?>
                </table>
                <?php
                    }catch(PDOException $ex) {
                        echo "Invalid Query";
                        print($ex);
                        die();
                    }
                ?>
			</div><!-- /.box-body -->
            <div class="box-footer" style="padding-top:20px;margin-top:10px;">
                <a href="<?php echo $base_url; ?>/room-control"><input type="button" value="Room Control" class="small-button blue" /></a>
                <a href="<?php echo $base_url; ?>/rate-control"><input type="button" value="Rate Control" class="small-button blue" /></a>
                <a href="<?php echo $base_url; ?>/hotel-profile/photo"><input type="button" value="Upload Photo" class="small-button blue" /></a>
            </div>
       </div>
    </div>
</section>
