<button type="button" class="small-button blue add-button">Add New Agent</button>
<ul class="inline-block this-inline-block">
	<li><span class="status-green square">sts</span> Active</li>
	<li><span class="status-black square">sts</span> Inactive</li>
	<!-- <li><span class="status-red square">sts</span> Expired</li> -->
</ul>
<style>
.subinfo{font-size: 95%;color: #808080;text-transform: none;}
.subdetail{background-color:#fafafa;}
.subtable {width:90%; margin:5px 10px;}
.subtable td, .subtable th{padding: 10px 7px !important;} 
.subtable td {background-color:#f9f9f9;border-top:1px solid #dcdcdc;}
.hide{display:none;}
</style>
<?php
	error_reporting(E_ALL & ~E_NOTICE);
	include("../../conf/connection.php");

	$usr = $_REQUEST['usr'];
	$hoid = $_REQUEST['hoid'];

	$main_query = "select a.agentoid, a.hotelcreatoroid, a.datecreated, h.hotelname, a.useroid, u.username, c.cityname, s.statename, a.agentname, a.email, a.phone, a.publishedoid, (case when a.publishedoid = 2 then 'status-red' when a.publishedoid = 1 then 'status-green' else 'status-black' end) AS status
	FROM agent a
	INNER JOIN published p ON a.publishedoid = p.publishedoid
	LEFT JOIN hotel h ON a.hotelcreatoroid = h.hoteloid
	LEFT JOIN users u ON a.useroid = u.useroid
	INNER JOIN city c ON a.cityoid = c.cityoid inner join state s using (stateoid) inner join country ct using (countryoid)";

	$filter = array();
	if(isset($_REQUEST['name']) and $_REQUEST['name']!=''){
		array_push($filter, 'a.agentname like "%'.$_REQUEST['name'].'%"');
	}
	if(isset($_REQUEST['country']) and $_REQUEST['country']!=''){
		array_push($filter, 'ct.countryoid = "'.$_REQUEST['country'].'"');
	}

	//array_push($filter, "ah.hoteloid = '".$hoid."'");
	array_push($filter, 'a.publishedoid not in (3)');

	if(count($filter) > 0){
		$combine_filter = implode(' and ',$filter);
		$query = $main_query.' where '.$combine_filter;
	}else{
		$query = $main_query;
	}
	//echo "<pre>".$query."</pre>";
	try {
		$stmt = $db->query($query." order by a.agentoid asc");
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
?>
<table class="table promo-table">
	<tr>
			<td>Agent Name</td>
			<td>City</td>
			<td>Email</td>
			<td>Phone</td>
			<td>Created By</td>
			<td>&nbsp;</td>
	</tr>
<?php
		$r_agent = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_agent as $row){
				$agentoid = $row['agentoid'];
				$agentname = $row['agentname'];
				$cityname = $row['cityname']; $statename = $row['statename'];
				$email = $row['email'];
				$phone = $row['phone'];
				$status = $row['status'];
				$hotelcreator = empty($row['hotelname']) ? "-" : $row['hotelname'];
				$usercreator = $row['username'];
				$datecreated = date('d/m/Y',strtotime($row['datecreated']));
?>
    <tr class="<?php echo $status; ?>">
        <td><?php echo $agentname; ?></td>
        <td><?php echo $statename." &rarr; ".$cityname; ?></td>
		<td class="lowercase"><?php echo $email;?></td>
		<td class="lowercase"><?php echo $phone;?></td>
		<td><?php echo $hotelcreator." (".$usercreator.")<br/><span class='subinfo'>Join at: ".$datecreated."</span>"; ?></td>
        <td class="algn-right">
        <button type="button" class="pencil edit-button" agenthoteloid="<?php echo $agentoid; ?>">Edit</button>
        <?php //if($usr == "1"){ ?>
        <button type="button" class="trash delete-button" agenthoteloid="<?php echo $agentoid; ?>">Delete</button>
        <?php //} ?>
		<button type="button" class="small-button blue view_hotel" agenthoteloid="<?php echo $agentoid; ?>">View Hotel</button>
        </td>
    </tr>
	<tr class="hide detail" dtl="<?php echo $agentoid;?>">
	<?php
		$query1 = "SELECT ah.`agenthoteloid`, ah.`hoteloid`, h.hotelname, ah.`agentoid`, ah.`publishedoid`, ah.`creditfacility`, ah.`creditbalance`, 
		(case when ah.publishedoid = 2 then 'status-red' when ah.publishedoid = 1 then 'status-green' else 'status-black' end) AS status
		FROM `agenthotel` ah 
		LEFT JOIN hotel h ON ah.hoteloid = h.hoteloid 
		WHERE ah.`publishedoid` not in (3) and ah.`agentoid`=:a";
		$stmt1 = $db->prepare($query1);
		$stmt1->execute(array(':a'=>$agentoid));
		$row_count1 = $stmt1->rowCount();
		
		echo '<td colspan="6" class="subdetail">
		<table class="subtable" cellspacing="0">
			<tr>
				<th>No.</th>
				<th>Hotel Connect</th>
				<th>Credit Facility</th>
				<th>Credit Balance</th>
				<th>Status</th>
				<th><button type="button" class="small-button blue manage" agentoid="'.$agentoid.'">Manage Agent</button></th>
			</tr>';
		
		if($row_count1 > 0) {
			$r_agent1 = $stmt1->fetchAll(PDO::FETCH_ASSOC);
			$n1 = 0;
			foreach($r_agent1 as $row1){
				$agenthoteloid1 = $row1['agenthoteloid'];
				$hotelname1 = $row1['hotelname'];
				$creditfacility = $row1['creditfacility'];
				$creditbalance = $row1['creditbalance'];
				$status1 = $row1['status'];
				$n1++;
				echo '<tr align="center">
						<td>'.$n1.'</td>
						<td>'.$hotelname1.'</td>
						<td>'.$creditfacility.'</td>
						<td>'.$creditbalance.'</td>
						<td><span class="'.$status1.' square">sts</span></td>
						<td>
						<button type="button" class="trash unassign" agenthoteloid="'.$agenthoteloid1.'">Remove</button>
						</td>
					</tr>';
			}
		}else{
			echo '<td colspan="6" align="center" style="text-transform:none">There are not any hotel assigned to this agent.</td>';
		}

		echo '</table></td>';
	?>
	</tr>
<?php
			}
?>
</table>
<?php
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		print($ex);
		die();
	}
?>
