<?php
	include("../../conf/connection.php");

	$agentoid = $_POST['agentoid'];
	$name = $_POST['name'];
	$address = $_POST['address'];
	$phone = $_POST['phone'];
	$website = $_POST['website'];
	$email = $_POST['email'];
	$published = $_POST['published'];

	$city = $_POST['city'];

	try {
		$stmt = $db->prepare("UPDATE agent SET agentname=:a, address=:b, phone=:c, website=:d, email=:e, publishedoid=:f, cityoid=:g WHERE agentoid=:aoid");
		$stmt->execute(array(':a' => $name, ':b' => $address, ':c' => $phone, ':d' => $website, ':e' => $email, ':f' => $published, ':g' => $city, ':aoid' => $agentoid));

		$affected_rows = $stmt->rowCount();
	}catch(PDOException $ex) {
		echo "0";
		die();
	}
	echo "1";
?>
