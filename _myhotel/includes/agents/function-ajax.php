<?php
include("../../conf/connection.php");

if(isset($_GET['func'])) {
	if($_GET['func'] == "stateRqst"){
		getState($_GET['country'],$_GET['state']);
	}else if($_GET['func'] == "cityRqst"){
		getCity($_GET['state'],$_GET['city']);
	}
}


function getState($country,$state)
{
	echo"State <br><select name='state' class='input-select'> ";
	try {
		global $db;
		$stmt = $db->query("select s.stateoid, s.statename from state s inner join country c using (countryoid) where c.countryoid = '".$country."'");
		$r_state = $stmt->fetchAll(PDO::FETCH_ASSOC);
		foreach($r_state as $row){
			if($row['stateoid'] == $state){ $selected = "selected"; }else{ $selected=""; }
			echo"<option value='".$row['stateoid']."' ".$selected.">".$row['statename']."</option>";
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}
	echo"</select>";
}


function getCity($state,$city)
{
	echo"City <br><select name='city' class='input-select'> ";
	try {
		global $db;
		$stmt = $db->query("select ct.cityoid, ct.cityname from city ct inner join state s using (stateoid) inner join country c using (countryoid) where s.stateoid = '".$state."' order by ct.cityname");
		$r_state = $stmt->fetchAll(PDO::FETCH_ASSOC);
		foreach($r_state as $row){
			if($row['cityoid'] == $city){ $selected = "selected"; }else{ $selected=""; }
			echo"<option value='".$row['cityoid']."' ".$selected.">".$row['cityname']."</option>";
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}
	echo"</select>";
}

?>
