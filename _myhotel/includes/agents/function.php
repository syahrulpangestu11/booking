<?php
function getCountry($country)
{

	try {
		global $db;
		$stmt = $db->query("select * from country");
		$r_country = $stmt->fetchAll(PDO::FETCH_ASSOC);
		foreach($r_country as $row){
			if($row['countryoid'] == $country){ $selected = "selected"; }else{ $selected=""; }
	?>
		<option value="<?php echo $row['countryoid']; ?>" <?php echo $selected; ?>><?php echo $row['countryname']; ?></option>
	<?php	
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}

}

function getPublished($published)
{

	try {
		global $db;
		$stmt = $db->query("select * from published where showthis = 'y'");
		$r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
		foreach($r_room as $row){
			if($row['publishedoid'] == $published){ $selected = "selected"; }else{ $selected=""; }
			echo"<option value='".$row['publishedoid']."' ".$selected.">".$row['note']."</option>";
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}

}
?>