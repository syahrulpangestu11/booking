<section class="content-header">
    <h1>
        Load Tarif
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i>  Allotments &amp; Rates </a></li>
        <li>Load Tarif</li>
        <li class="active">Import from excel</li>
    </ol>
</section>
<section class="content">
   
    <div class="row">
        <div class="box box-form">
            <h1>Import Load Tarif</h1>
            <form method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/load-tarif/upload-excel">
            <input type="hidden" name="hoteloid" value="<?php echo $hoteloid; ?>" />
            <div class="side-left">
                <div class="form-input">
                    <label>Room Type</label>
                    <select name="roomoffer" class="input-select">
                    <?php
                        try {
                            $stmt = $db->query("select r.roomoid, r.name, x.offernum from room r inner join hotel h using (hoteloid) left join (select count(roomofferoid) as offernum, ro.roomoid from roomoffer ro group by ro.roomoid) as x on x.roomoid = r.roomoid where h.hoteloid = '".$hoteloid."' and x.offernum > 0 and r.publishedoid = '1'");
                            $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                            foreach($r_room as $row){
                                echo"<optgroup label = '".$row['name']."'>";
                                try {
                                    $stmt = $db->query("select ro.* from roomoffer ro inner join published p using (publishedoid) inner join room r using (roomoid) inner join offertype using (offertypeoid) where r.roomoid = '".$row['roomoid']."' and p.publishedoid = '1'");
                                    $r_offer = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                    foreach($r_offer as $row1){
                                       echo"<option value='".$row['roomoid']."-".$row1['roomofferoid']."'>  ".$row1['name']."</option>";
                                    }
                                }catch(PDOException $ex) {
                                    echo "Invalid Query";
                                    die();
                                }     
                                echo"</optgroup>";                       
                            }
                        }catch(PDOException $ex) {
                            echo "Invalid Query";
                            die();
                        }
                    
                    ?>   
                    </select>
                    <label>Channel</label>
					<?php
                        try {
                            $stmt = $db->query("select * from channel");
                            $r_plan = $stmt->fetchAll(PDO::FETCH_ASSOC);
                            foreach($r_plan as $row){
                                echo"<input type='checkbox' name='channel[]' value='".$row['channeloid']."'>".$row['name']."&nbsp;";
                            }
                        }catch(PDOException $ex) {
                            echo "Invalid Query";
                            die();
                        }
                    
                    ?>        	
                    <label>File</label> 
                    <input type="file" class="input-text" name="loadtarif">
                    <br> 
                    <button type="submit" class="submit-add">Import</button>
                </div>
            </div>
            </form>
            <div class="clear"></div>
       	</div>
    </div>
    
</section>