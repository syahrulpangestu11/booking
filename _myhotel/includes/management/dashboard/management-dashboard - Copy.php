<style>
table#table-dashboard-management tr:first-of-type, table#table-dashboard-management tr:nth-of-type(2) {
	background-color: #e0e0e0;
}
table#table-dashboard-management tr > th {
	text-align:center;
}
span.trend{ font-size:1.2em; }
.green{ color:#063; }
.red{ color:#900; }
h4.title-chart{ text-align:center; font-weight:bold; font-size:1.05em; }
</style>
<?php
	include ('includes/bootstrap.php');
	include('includes/reports/function/function-report.php');
	include('includes/management/management-dashboard/function-management-dashboard.php');
	
	$_POST['year_1'] = date('Y');
	$_POST['month_1'] = date('m');
	$year = $_POST['year_1'];
	$month = $_POST['month_1'];
	
	$from_year = $year."-01-01";
	$to_year = date('Y-m-d');
	
	$from	= $year."-".$month."-01";
	$to		= $year."-".$month."-".date('t', strtotime($from));
	
	if($month == 1){
		$month_before	= 12;
		$year_before	= $year - 1;
	}else{
		$month_before	= $month - 1;
		$year_before	= $year;
	}
	$from_before	= $year_before."-".$month_before."-01";
	$to_before		= $year_before."-".$month_before."-".date('t', strtotime($from_before));

	$tm_booking = countResult($from, $to, '', '', 4);
	$lm_booking = countResult($from_before, $to_before, '', '', 4);
	$booking_ytd = countResult($from_year, $to_year, '', '', 4);
	$yt_booking = "N/A";
	$ytp_booking = "N/A";
	
	$tm_roomnight = countRoomNight($from, $to, '', '');
	$lm_roomnight = countRoomNight($from_before, $to_before, '', '');
	$roomnight_ytd = countRoomNight($from_year, $to_year, '', '');
	$yt_roomnight = "N/A";
	$ytp_roomnight = "N/A";
	
	$tm_sales = SumRevenue($from, $to, '');
	$lm_sales = SumRevenue($from_before, $to_before,'');
	$sales_ytd = SumRevenue($from_year, $to_year, '');
	$yt_sales = "N/A";
	$ytp_sales = "N/A";

	$tm_comm = SumCommission($from, $to, '');
	$lm_comm = SumCommission($from_before, $to_before,'');
	$comm_ytd = SumCommission($from_year, $to_year, '');
	$yt_comm = "N/A";
	$ytp_comm = "N/A";
	
	$tmp_booking	= productionBooking($from, $to, 4);
	$lmp_booking	= productionBooking($from_before, $to_before, 4);
	$bookingp_ytd	= productionBooking($from_year, $to_year, 4);
	
	$tmp_roomnight	= productionRoomNight($from, $to, 4);
	$lmp_roomnight	= productionRoomNight($from_before, $to_before, 4);
	$roomnightp_ytd	= productionRoomNight($from_year, $to_year, 4);

	$tmp_sales 		= productionRevenue($from, $to, 4);
	$lmp_sales 		= productionRevenue($from_before, $to_before, 4);
	$salesp_ytd 	= productionRevenue($from_year, $to_year, 4);
	
	$tmp_comm		= productionCommission($from, $to);
	$lmp_comm		= productionCommission($from_before, $to_before);
	$commp_ytd 		= productionCommission($from_year, $to_year);
	
	$show_date = array(); $point_reservation = array(); $point_confirmed = array(); $point_cancelled = array();
	$point_noshow = array();
	$point_roomnight = array();
	$point_commission = array(); $point_revenue = array();
	
	$show_date_occupancy = array();
	$point_roomnight_occupancy = array(); $point_confirmed_occupancy = array();
	$point_revenue_occupancy = array(); $point_commission_occupancy = array();
	
	for($i = 1; $i <= date('m'); $i++){
		$chart_start_date	= $year.'-'.$i.'-01';
		$chart_to_date	= date('Y-m-t', strtotime($chart_start_date));
		array_push($show_date, date('M', strtotime($chart_start_date)));
		array_push($point_reservation, productionBooking($chart_start_date, $chart_to_date, array(4, 5, 7, 8)));
		array_push($point_confirmed, countResult($chart_start_date, $chart_to_date, $hoteloid, "", 4));
		array_push($point_cancelled, countResult($chart_start_date, $chart_to_date, $hoteloid, $roomtype, array(5,7,8)));
		/*
		array_push($point_noshow, countResult($chart_start_date, $chart_to_date, $hoteloid, $roomtype, 7));
		*/
		array_push($point_roomnight, countRoomNight($chart_start_date, $chart_to_date, $hoteloid, ''));
		
		$commission_legend = floatval(str_replace(',', '', SumCommission($chart_start_date, $chart_to_date, '')));
		if($commission_legend > 0){
			$commission_legend = $commission_legend / 1000;
		}
		$revenue_legend = floatval(str_replace(',', '', SumRevenue($chart_start_date, $chart_to_date, '')));
		if($revenue_legend > 0){
			$revenue_legend = $revenue_legend / 1000000;
		}
		
		array_push($point_commission, $commission_legend);
		array_push($point_revenue, $revenue_legend);
	}
	
	for($i = 1; $i <= 12; $i++){
		$chart_start_date	= $year.'-'.$i.'-01';
		$chart_to_date	= date('Y-m-t', strtotime($chart_start_date));
		
		array_push($show_date_occupancy, date('M', strtotime($chart_start_date)));
		array_push($point_roomnight_occupancy, countRoomNightStay($chart_start_date, $chart_to_date));		
		array_push($point_confirmed_occupancy, countOccupancyBookingConfirmed($chart_start_date, $chart_to_date));
		
		$commission_legend = materializeBookingCommission($chart_start_date, $chart_to_date);
		if($commission_legend > 0){
			$commission_legend = $commission_legend / 1000;
		}
		$revenue_legend = materializeBookingTotal($chart_start_date, $chart_to_date, "grandtotal", array(4, 5, 7, 8));
		if($revenue_legend > 0){
			$revenue_legend = $revenue_legend / 1000000;
		}

		array_push($point_revenue_occupancy, $revenue_legend);		
		array_push($point_commission_occupancy, $commission_legend);		
	}
?>
<div id="newdashboard">
	<div class="box">
        <div class="box-body">
        	<div class="row">
            	<div class="col-md-12 text-center"><h1>DASHBOARD MANAGEMENT</h1></div>
            </div>
            <div class="row">
            	<div class="col-md-12">
                    <h3 style="text-transform:uppercase;">Hotel Production</h3>
				</div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table id="table-dashboard-management" class="table table-striped table-bordered">
                        <tr>
                            <th rowspan="2">&nbsp;</th>
                            <th rowspan="2">This Month</th>
                            <th rowspan="2">Last Month</th>
                            <th rowspan="2">Trend</th>
                            <th rowspan="2">YTD</th>
                            <th colspan="2">Year Target</th>
                        </tr>
                        <tr>
                            <th>%</th>
                            <th>IDR</th>
                        </tr>
                        <tr>
                        	<td>Booking</td>
                            <td><?=$tm_booking?></td>
                            <td><?=$lm_booking?></td>
                            <td><?=trend($tm_booking, $lm_booking)?></td>
                            <td><?=$booking_ytd?></td>
                            <td><?=$yt_booking?></td>
                            <td><?=$ytp_booking?></td>
                        </tr>
                        <tr>
                        	<td>Room Night</td>
                            <td><?=$tm_roomnight?></td>
                            <td><?=$lm_roomnight?></td>
                            <td><?=trend(str_replace(',', '', $tm_roomnight), str_replace(',', '', $lm_roomnight))?></td>
                            <td><?=$roomnight_ytd?></td>
                            <td><?=$yt_roomnight?></td>
                            <td><?=$ytp_roomnight?></td>
                        </tr>
                        <tr>
                        	<td>Sales</td>
                            <td><?=$tm_sales?></td>
                            <td><?=$lm_sales?></td>
                            <td><?=trend(str_replace(',', '', $tm_sales), str_replace(',', '', $lm_sales))?></td>
                            <td><?=$sales_ytd?></td>
                            <td><?=$yt_sales?></td>
                            <td><?=$ytp_sales?></td>
                        </tr>
                        <tr>
                        	<td>Commission</td>
                            <td><?=$tm_comm?></td>
                            <td><?=$lm_comm?></td>
                            <td><?=trend(str_replace(',', '', $tm_comm), str_replace(',', '', $lm_comm))?></td>
                            <td><?=$comm_ytd?></td>
                            <td><?=$yt_comm?></td>
                            <td><?=$ytp_comm?></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="row">
            	<div class="col-md-6">
                    <div class="chart">
                   		<canvas id="number-chart"></canvas>
                    </div>
                    <h4 class="title-chart">Booking &amp; Room Night <?php echo $year; ?></h4>
                </div>
            	<div class="col-md-6">
                    <div class="chart">
                   		<canvas id="revenue-chart"></canvas>
                    </div>
                    <h4 class="title-chart">Revenue &amp; Commission <?php echo $year; ?></h4>
                </div>
            </div>
            <div class="row">
            	<div class="col-md-12">
                    <h3 style="text-transform:uppercase;">Report Occupancy</h3>
				</div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table id="table-dashboard-management" class="table table-striped table-bordered">
                        <tr>
                            <th rowspan="2">&nbsp;</th>
                            <th rowspan="2">This Month</th>
                            <th rowspan="2">Last Month</th>
                            <th rowspan="2">Trend</th>
                            <th rowspan="2">YTD</th>
                            <th colspan="2">Year Target</th>
                        </tr>
                        <tr>
                            <th>%</th>
                            <th>IDR</th>
                        </tr>
                        <tr>
                        	<td>Booking</td>
                            <td><?=$tmp_booking?></td>
                            <td><?=$lmp_booking?></td>
                            <td><?=trend($tmp_booking, $lmp_booking)?></td>
                            <td><?=$bookingp_ytd?></td>
                            <td><?=$yt_booking?></td>
                            <td><?=$ytp_booking?></td>
                        </tr>
                        <tr>
                        	<td>Room Night</td>
                            <td><?=$tmp_roomnight?></td>
                            <td><?=$lmp_roomnight?></td>
                            <td><?=trend($tmp_roomnight, $lmp_roomnight)?></td>
                            <td><?=$roomnightp_ytd?></td>
                            <td><?=$yt_roomnight?></td>
                            <td><?=$ytp_roomnight?></td>
                        </tr>
                        <tr>
                        	<td>Sales</td>
                            <td><?=$tmp_sales?></td>
                            <td><?=$lmp_sales?></td>
                            <td><?=trend(str_replace(',', '', $tmp_sales), str_replace(',', '', $lmp_sales))?></td>
                            <td><?=$salesp_ytd?></td>
                            <td><?=$yt_sales?></td>
                            <td><?=$ytp_sales?></td>
                        </tr>
                        <tr>
                        	<td>Commission</td>
                            <td><?=$tmp_comm?></td>
                            <td><?=$lmp_comm?></td>
                            <td><?=trend(str_replace(',', '', $tmp_comm), str_replace(',', '', $lmp_comm))?></td>
                            <td><?=$commp_ytd?></td>
                            <td><?=$yt_comm?></td>
                            <td><?=$ytp_comm?></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="row">
            	<div class="col-md-6">
                    <div class="chart">
                   		<canvas id="occupancy-chart"></canvas>
                    </div>
                    <h4 class="title-chart">Confirmed Booking &amp; Room Night <?php echo $year; ?></h4>
                </div>
            	<div class="col-md-6">
                    <div class="chart">
                   		<canvas id="occupancy-revenue-chart"></canvas>
                    </div>
                    <h4 class="title-chart">Revenue &amp; Commission <?php echo $year; ?></h4>
                </div>
            </div>
            <div class="row">
            	<div class="col-md-12">
                	<a href="<?=$base_url;?>/transaction-details"><button type="button" class="btn btn-primary">View Transaction Detail</button></a>
                </div>
            </div>
    	</div>
    </div>
</div>


<script src="<?php echo $base_url?>/lib/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<!-- chartjs -->
<script src="<?php echo $base_url?>/lib/plugins/chartjs/Chart.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(function () {
        'use strict';

        /* ChartJS
        * -------
        * Here we will create a few charts using ChartJS
        */

        //-----------------------
        //- MONTHLY SALES CHART -
        //-----------------------

        // Get context with jQuery - using jQuery's .get() method.
        var salesChartCanvas = $("#number-chart").get(0).getContext("2d");
        // This will get the first returned node in the jQuery collection.
        var salesChart = new Chart(salesChartCanvas);

        var salesChartData = {
            labels: [ <?php echo '"'.implode('","', $show_date).'"'; ?> ],
            datasets: [
                {
                    label: "Booking",
                    fillColor: "rgba(255, 255, 255, 0)",
                    strokeColor: "rgba(165, 145, 230, 1)",
                    pointColor: "rgba(134, 108, 220, 1)",
                    pointStrokeColor: "rgba(134, 108, 220,1)",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(165, 145, 230, 1)",
                    data: [ <?php echo implode(',', $point_reservation); ?> ]
                },
                {
                    label: "Confirmed Booking",
                    fillColor: "rgba(255, 255, 255, 0)",
                    strokeColor: "rgba(0, 230, 81, 1)",
                    pointColor: "rgba(23, 187, 81, 1)",
                    pointStrokeColor: "rgba(23, 187, 81, 1)",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(0, 230, 81, 1)",
                    data: [ <?php echo implode(',', $point_confirmed); ?> ]
                },
                {
                    label: "Cancelled Booking",
                    fillColor: "rgba(255, 255, 255, 0)",
                    strokeColor: "rgba(210, 33, 51, 1)",
                    pointColor: "rgba(173, 10, 27, 1)",
                    pointStrokeColor: "rgba(173, 10, 27, 1)",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(210, 33, 51, 1)",
                    data: [ <?php echo implode(',', $point_cancelled); ?> ]
                },
                {
                    label: "Room Night",
                    fillColor: "rgba(255, 255, 255, 0)",
                    strokeColor: "rgba(255, 200, 0, 1)",
                    pointColor: "rgba(255, 179, 85, 1)",
                    pointStrokeColor: "rgba(255, 179, 85, 1)",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(255, 200, 0, 1)",
                    data: [ <?php echo implode(',', $point_roomnight); ?> ]
                },
            ]
        };

        var salesChartOptions = {
            //Boolean - If we should show the scale at all
            showScale: true,
            //Boolean - Whether grid lines are shown across the chart
            scaleShowGridLines: true,
            //String - Colour of the grid lines
            scaleGridLineColor: "rgba(0,0,0,.05)",
            //Number - Width of the grid lines
            scaleGridLineWidth: 1,
            //Boolean - Whether to show horizontal lines (except X axis)
            scaleShowHorizontalLines: true,
            //Boolean - Whether to show vertical lines (except Y axis)
            scaleShowVerticalLines: true,
            //Boolean - Whether the line is curved between points
            bezierCurve: true,
            //Number - Tension of the bezier curve between points
            bezierCurveTension: 0.3,
            //Boolean - Whether to show a dot for each point
            pointDot: true,
            //Number - Radius of each point dot in pixels
            pointDotRadius: 4,
            //Number - Pixel width of point dot stroke
            pointDotStrokeWidth: 1,
            //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
            pointHitDetectionRadius: 20,
            //Boolean - Whether to show a stroke for datasets
            datasetStroke: true,
            //Number - Pixel width of dataset stroke
            datasetStrokeWidth: 2,
            //Boolean - Whether to fill the dataset with a color
            datasetFill: true,
            //String - A legend template
            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%=datasets[i].label%></li><%}%></ul>",
            //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
            maintainAspectRatio: true,
            //Boolean - whether to make the chart responsive to window resizing
            responsive: true
        };

        //Create the line chart
        salesChart.Line(salesChartData, salesChartOptions);

        //---------------------------
        // REVENUE CHART
        //---------------------------
		
		var canvas = document.getElementById('revenue-chart');
		new Chart(canvas, {
		  type: 'line',
		  data: {
			labels: [<?php echo '"'.implode('","', $show_date).'"'; ?>],
			datasets: [{
			  label: 'Revenue',
			  yAxisID: 'Revenue',
			  data: [<?php echo implode(',', $point_revenue); ?>]
			}, {
			  label: 'Commission',
			  yAxisID: 'Commission',
			  data: [<?php echo implode(',', $point_commission); ?>]
			}]
		  },
		  options: {
			scales: {
			  yAxes: [{
				id: 'Revenue',
				type: 'linear',
				position: 'left',
			  }, {
				id: 'Commission',
				type: 'linear',
				position: 'right',
				ticks: {
				  max: 1,
				  min: 0
				}
			  }]
			}
		  }
		});	
		
		/*	
        // Get context with jQuery - using jQuery's .get() method.
        var revenueChartCanvas = $("#revenue-chart").get(0).getContext("2d");
        // This will get the first returned node in the jQuery collection.
        var revenueChart = new Chart(revenueChartCanvas);

        var revenueChartData = {
            labels: [ <?php //echo '"'.implode('","', $show_date).'"'; ?> ],
            datasets: [
                {
                    label: "Total Revenue",
                    fillColor: "rgba(255, 255, 255, 0)",
                    strokeColor: "rgba(35, 198, 206, 1)",
                    pointColor: "rgba(31, 178, 185, 1)",
                    pointStrokeColor: "rgba(31, 178, 185, 1)",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(35, 198, 206,1)",
                    data: [ <?php //echo implode(',', $point_revenue); ?> ]
                },
                {
                    label: "Total Commission",
                    fillColor: "rgba(255, 255, 255, 0)",
                    strokeColor: "rgba(78, 197, 49, 1)",
                    pointColor: "rgba(51, 175, 20, 1)",
                    pointStrokeColor: "rgba(51, 175, 20, 1)",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(78, 197, 49, 1)",
                    data: [ <?php //echo implode(',', $point_commission); ?> ]
                },
            ]
        };

        var revenueChartOptions = {
            //Boolean - If we should show the scale at all
            showScale: true,
            //Boolean - Whether grid lines are shown across the chart
            scaleShowGridLines: true,
            //String - Colour of the grid lines
            scaleGridLineColor: "rgba(0,0,0,.05)",
            //Number - Width of the grid lines
            scaleGridLineWidth: 1,
            //Boolean - Whether to show horizontal lines (except X axis)
            scaleShowHorizontalLines: true,
            //Boolean - Whether to show vertical lines (except Y axis)
            scaleShowVerticalLines: true,
            //Boolean - Whether the line is curved between points
            bezierCurve: true,
            //Number - Tension of the bezier curve between points
            bezierCurveTension: 0.3,
            //Boolean - Whether to show a dot for each point
            pointDot: true,
            //Number - Radius of each point dot in pixels
            pointDotRadius: 4,
            //Number - Pixel width of point dot stroke
            pointDotStrokeWidth: 1,
            //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
            pointHitDetectionRadius: 20,
            //Boolean - Whether to show a stroke for datasets
            datasetStroke: true,
            //Number - Pixel width of dataset stroke
            datasetStrokeWidth: 2,
            //Boolean - Whether to fill the dataset with a color
            datasetFill: true,
            //String - A legend template
            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%=datasets[i].label%></li><%}%></ul>",
            //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
            maintainAspectRatio: true,
            //Boolean - whether to make the chart responsive to window resizing
            responsive: true
        };

        //Create the line chart
        revenueChart.Line(revenueChartData, revenueChartOptions);
		*/
		
        //-----------------------
        //- occupancy CHART -
        //-----------------------

        // Get context with jQuery - using jQuery's .get() method.
        var occupancyChartCanvas = $("#occupancy-chart").get(0).getContext("2d");
        // This will get the first returned node in the jQuery collection.
        var occupancyChart = new Chart(occupancyChartCanvas);

        var occupancyChartData = {
            labels: [ <?php echo '"'.implode('","', $show_date_occupancy).'"'; ?> ],
            datasets: [
                {
                    label: "Room Night Stay",
                    fillColor: "rgba(255, 255, 255, 0)",
                    strokeColor: "rgba(0, 230, 81, 1)",
                    pointColor: "rgba(23, 187, 81, 1)",
                    pointStrokeColor: "rgba(23, 187, 81, 1)",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(0, 230, 81, 1)",
                    data: [ <?php echo implode(',', $point_roomnight_occupancy); ?> ]
                },
                {
                    label: "Confirmed Booking",
                    fillColor: "rgba(255, 255, 255, 0)",
                    strokeColor: "rgba(255, 200, 0, 1)",
                    pointColor: "rgba(255, 179, 85, 1)",
                    pointStrokeColor: "rgba(255, 179, 85, 1)",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(255, 200, 0, 1)",
                    data: [ <?php echo implode(',', $point_confirmed_occupancy); ?> ]
                },
            ]
        };

        var occupancyChartOptions = {
            //Boolean - If we should show the scale at all
            showScale: true,
            //Boolean - Whether grid lines are shown across the chart
            scaleShowGridLines: true,
            //String - Colour of the grid lines
            scaleGridLineColor: "rgba(0,0,0,.05)",
            //Number - Width of the grid lines
            scaleGridLineWidth: 1,
            //Boolean - Whether to show horizontal lines (except X axis)
            scaleShowHorizontalLines: true,
            //Boolean - Whether to show vertical lines (except Y axis)
            scaleShowVerticalLines: true,
            //Boolean - Whether the line is curved between points
            bezierCurve: true,
            //Number - Tension of the bezier curve between points
            bezierCurveTension: 0.3,
            //Boolean - Whether to show a dot for each point
            pointDot: true,
            //Number - Radius of each point dot in pixels
            pointDotRadius: 4,
            //Number - Pixel width of point dot stroke
            pointDotStrokeWidth: 1,
            //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
            pointHitDetectionRadius: 20,
            //Boolean - Whether to show a stroke for datasets
            datasetStroke: true,
            //Number - Pixel width of dataset stroke
            datasetStrokeWidth: 2,
            //Boolean - Whether to fill the dataset with a color
            datasetFill: true,
            //String - A legend template
            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%=datasets[i].label%></li><%}%></ul>",
            //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
            maintainAspectRatio: true,
            //Boolean - whether to make the chart responsive to window resizing
            responsive: true
        };

        //Create the line chart
        occupancyChart.Line(occupancyChartData, occupancyChartOptions);
		
        //-----------------------
        //- occupancy Revenue CHART -
        //-----------------------

        // Get context with jQuery - using jQuery's .get() method.
        var occupancyRevenueChartCanvas = $("#occupancy-revenue-chart").get(0).getContext("2d");
        // This will get the first returned node in the jQuery collection.
        var occupancyRevenueChart = new Chart(occupancyRevenueChartCanvas);

        var occupancyRevenueChartData = {
            labels: [ <?php echo '"'.implode('","', $show_date_occupancy).'"'; ?> ],
            datasets: [
                {
                    label: "Revenue",
                    fillColor: "rgba(255, 255, 255, 0)",
                    strokeColor: "rgba(35, 198, 206, 1)",
                    pointColor: "rgba(31, 178, 185, 1)",
                    pointStrokeColor: "rgba(31, 178, 185, 1)",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(35, 198, 206,1)",
                    data: [ <?php echo implode(',', $point_revenue_occupancy); ?> ]
                },
                {
                    label: "Commission",
                    fillColor: "rgba(255, 255, 255, 0)",
                    strokeColor: "rgba(78, 197, 49, 1)",
                    pointColor: "rgba(51, 175, 20, 1)",
                    pointStrokeColor: "rgba(51, 175, 20, 1)",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(78, 197, 49, 1)",
                    data: [ <?php echo implode(',', $point_commission_occupancy); ?> ]
                },
            ]
        };

        var occupancyRevenueChartOptions = {
            //Boolean - If we should show the scale at all
            showScale: true,
            //Boolean - Whether grid lines are shown across the chart
            scaleShowGridLines: true,
            //String - Colour of the grid lines
            scaleGridLineColor: "rgba(0,0,0,.05)",
            //Number - Width of the grid lines
            scaleGridLineWidth: 1,
            //Boolean - Whether to show horizontal lines (except X axis)
            scaleShowHorizontalLines: true,
            //Boolean - Whether to show vertical lines (except Y axis)
            scaleShowVerticalLines: true,
            //Boolean - Whether the line is curved between points
            bezierCurve: true,
            //Number - Tension of the bezier curve between points
            bezierCurveTension: 0.3,
            //Boolean - Whether to show a dot for each point
            pointDot: true,
            //Number - Radius of each point dot in pixels
            pointDotRadius: 4,
            //Number - Pixel width of point dot stroke
            pointDotStrokeWidth: 1,
            //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
            pointHitDetectionRadius: 20,
            //Boolean - Whether to show a stroke for datasets
            datasetStroke: true,
            //Number - Pixel width of dataset stroke
            datasetStrokeWidth: 2,
            //Boolean - Whether to fill the dataset with a color
            datasetFill: true,
            //String - A legend template
            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%=datasets[i].label%></li><%}%></ul>",
            //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
            maintainAspectRatio: true,
            //Boolean - whether to make the chart responsive to window resizing
            responsive: true
        };

        //Create the line chart
        occupancyRevenueChart.Line(occupancyRevenueChartData, occupancyRevenueChartOptions);
    });
</script>
