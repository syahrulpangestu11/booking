<?php
	include("includes/bootstrap.php");
	include('includes/management/management-dashboard/function-management-dashboard.php');
	include('includes/function-tracking.php');
?>
<!-- JQUERY UI -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- Pagination -->
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css">
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>
<style type="text/css">

	.ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default {
		background-color: #ededed !important;
	}
	#example_wrapper .col-sm-12 {
        overflow-x: scroll;
    }
    #example > thead {
        background-color: #eaeaea;
    }
	#dashboard h1{ font-size:1.5em; }
	#dashboard h2{ font-size:1.2em; }
	table#example tr:hover td{ cursor:pointer; background-color:#ecf4fb;  }
	/*table#example tr td a{ color:inherit; }*/
	table#example tr td a:hover{ text-decoration:underline; }
</style>

<?php
    if(isset($_GET['startdate'])){
    	$startdate = date('Y-m-d', strtotime($_GET['startdate']));
    }else{
    	$startdate = date("Y-m-d",strtotime("first day of this month"));//date('Y-m-01');
    }

    $show_startdate = date('d F Y', strtotime($startdate));
    $start = date('Ymd', strtotime($startdate));

    if(isset($_GET['enddate'])){
    	$enddate = date('Y-m-d', strtotime($_GET['enddate']));
    }else{
    	$enddate = date("Y-m-d",strtotime('last day of this month'));//date('Y-m-d');
    }

    $show_enddate = date('d F Y', strtotime($enddate));
    $end = date('Ymd', strtotime($enddate));

    $lastmonthstart = date("d-m-Y",strtotime("first day of last month"));
    $lastmonthend = date("d-m-Y",strtotime("last day of last month"));
    $currentmonthstart = date("d-m-Y",strtotime("first day of this month"));
    $currentmonthend = date("d-m-Y");
?>
<section class="content-header">
    <h1>Occupancy Details</h1>
    <ol class="breadcrumb">
        <li class="active">Occupancy Details</li>
    </ol>
</section>
<section class="content">
    <div id="dashboard">

        <div class="box">
            <div class="box-body box-form">
                <form method="GET" class="form-inline" action="<?php echo $base_url.'/'.$uri2.'/occupancy-detail/'?>">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <a href="<?php echo $base_url.'/'.$uri2.'/occupancy-detail/?startdate='.$lastmonthstart.'&enddate='.$lastmonthend?>"><button type="button" class="btn btn-warning">Last Month</button></a>
                        </div>
                        <div class="form-group">
                            <a href="<?php echo $base_url.'/'.$uri2.'/occupancy-detail/?startdate='.$currentmonthstart.'&enddate='.$currentmonthend?>"><button type="button" class="btn btn-warning">Current Month</button></a>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <label>Show periode start from</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                <input type="text" name="startdate" id="from" class="calinput form-control" value="<?php echo date('d-m-Y', strtotime($start));?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label>until</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                <input type="text" id="to" name="enddate" class="calinput form-control" value="<?php echo date('d-m-Y', strtotime($end));?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Show</button>
                        </div>
                    </div>
            	</div>
                </form>
            </div>
        </div>

				<div class="box box-success">
					<div class="box-body">
							<div class="row">
									<div class="col-md-12"><?php include('detail/grafik-occupancy.php'); ?></div>
							</div>
					</div>
				</div>

        <div class="box box-primary">
            <div class="box-body">
            	<div class="row">
                	<div class="col-md-6">
                    	<h2>theB&uuml;king</h2>
                    </div>
                    <div class="col-md-6 text-right">
                    	<b>Running periode of <?=date('d / M / Y', strtotime($startdate))?> until <?=date('d / M / Y', strtotime($enddate))?></b>
                    </div>
                	<div class="col-md-12">
                        <h1>Occupancy Details</h1>
                    </div>
                </div>
            	<div class="row">
                	<div class="col-md-12 text-right">* dalam ribuan rupiah</div>
                    <table id="example" cellspacing="0" class="table table-striped">
                        <thead>
                            <tr>
                                <th>Hotel</th>
																<th>Chain</th>
                                <th>Location</th>
                                <th>Visit</th>
                                <th>Booking</th>
                                <th>Confirm</th>
                                <th>RN</th>
                                <th>%Conv</th>
                                <th>Sales*</th>
                                <th>Commission*</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!---------------------------------------------------------------->
<?php
	$main_query = "SELECT c.cityname, s.statename, h.hoteloid, h.hotelname, h.stars, ht.category, h.website, case when ch.name = 'No Chain' then '' else ch.name end as chainname
	from hotel h inner join chain ch using (chainoid) inner join hoteltype ht using (hoteltypeoid) inner join city c using (cityoid) inner join state s using (stateoid) inner join country ct using (countryoid)";

	$filter = array();
	/* show LIVE hotel only ---------------------*/
	array_push($filter, "h.publishedoid not in ('3') and h.hotelstatusoid IN ('1')");

	if(count($filter) > 0){
		$combine_filter = implode(' and ',$filter);
		$query_list_hotel = $main_query.' where '.$combine_filter;
	}else{
		$query_list_hotel = $main_query;
	}

	$sum_visit = array(); $sum_booking = array(); $sum_confirm = array(); $sum_roomnight = array(); $sum_total_sales = array(); $sum_total_commission = array();

	$stmt = $db->query($query_list_hotel);
	$row_count = $stmt->rowCount();
	if($row_count > 0) {
		$no = 0;
		$result_list_hotel = $stmt->fetchAll(PDO::FETCH_ASSOC);
		foreach($result_list_hotel as $list_hotel){
			$hoteloid = $list_hotel['hoteloid'];

			$jml_visit		= getVisit($hoteloid, "availability", $start, $end);
			$jml_booking	= occupancyBooking($startdate, $enddate, $hoteloid, array(4,5,7,8));
			$jml_confirm	= occupancyBooking($startdate, $enddate, $hoteloid, 4);
			$jml_roomnight	= occupancyRoomNight($startdate, $enddate, $hoteloid, 4);
			$total_sales		= (int)str_replace( ',', '', occupancyRevenue($startdate, $enddate, $hoteloid, 4)) / 1000;
			$total_commission	= (int)str_replace( ',', '', occupancyCommission($startdate, $enddate, $hoteloid)) / 1000;

			if($jml_visit > 0 and $jml_confirm > 0){
				$conversion = number_format($jml_confirm / $jml_visit * 100,2,",",".");
			}else{
				$conversion = 0;
			}

			array_push($sum_visit, $jml_visit);
			array_push($sum_booking, $jml_booking);
			array_push($sum_confirm, $jml_confirm);
			array_push($sum_roomnight, $jml_roomnight);
			array_push($sum_total_sales, $total_sales);
			array_push($sum_total_commission, $total_commission);

			$weburl = true;
			if(!empty($list_hotel['website'])){
				if (strpos($list_hotel['website'], 'http') === false) {
					$list_hotel['website'] = 'http://'.$list_hotel['website'];
					$weburl = true;
				}
			}else{
				$weburl = false;
			}
?>
            <tr hoid="<?=$hoteloid?>">
                <td>
                <?php if($weburl == true){ ?>
                <a href="<?=$list_hotel['website'];?>" target="_blank"><?=$list_hotel['hotelname'];?></a>
                <?php }else{ ?>
                <a href="#"><?=$list_hotel['hotelname'];?></a>
                <?php } ?>
                </td>
								<td><?=$list_hotel['chainname'];?></td>
                <td><?=$list_hotel['cityname'];?></td>
                <td class="text-right"><?=$jml_visit?></td>
                <td class="text-right"><?=$jml_booking?></td>
                <td class="text-right"><?=$jml_confirm?></td>
                <td class="text-right"><?=$jml_roomnight?></td>
                <td class="text-center"><?=$conversion?> %</td>
                <td class="text-right"><?=number_format($total_sales)?></td>
                <td class="text-right"><?=number_format($total_commission)?></td>
            </tr>
<?php
		}
	}

	$monthly_visit		= array_sum($sum_visit);
	$monthly_confirm	= array_sum($sum_confirm);

	if($monthly_visit > 0 and $monthly_confirm > 0){
		$monthly_conversion = number_format($monthly_confirm / $monthly_visit * 100,2,",",".");
	}else{
		$monthly_conversion = 0;
	}
?>
                        <!---------------------------------------------------------------->
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="3" class="text-right">Periode Running Total</th>
                                <th class="text-right"><?=$monthly_visit?></th>
                                <th class="text-right"><?=array_sum($sum_booking)?></th>
                                <th class="text-right"><?=$monthly_confirm?></th>
                                <th class="text-right"><?=array_sum($sum_roomnight)?></th>
                                <th class="text-center"><?=$monthly_conversion?> %</th>
                                <th class="text-right"><?=number_format(array_sum($sum_total_sales))?></th>
                                <th class="text-right"><?=number_format(array_sum($sum_total_commission))?></th>
                            </tr>
                        </tfoot>
                    </table>
              </div>
            </div>
		</div>

    </div>
</section>

<script type="text/javascript">
$( function() {
	$('body').on('click','table#example tr td:nth-child(n+2)', function(e) {
		var loginas_hc = $(this).closest('tr').attr('hoid');
		var loginas_hname = $(this).closest('tr').children('td').eq(0).html();
		$.ajax({
			type	: 'POST', cache: false,
			url		: '<?php echo"$base_url"; ?>/includes/suadm/hotel/change-session.php',
			data	: { loginas : loginas_hc, hname : loginas_hname },
			success	: function(rsp){ if(rsp == "1"){ document.location.href = '<?php echo"$base_url"; ?>/dashboard'; } }
		});
	});

	var dateFormat = "dd-mm-yy",
	from = $( "#from" )
		.datepicker({
		defaultDate: "+1d",
		changeMonth: true,
		dateFormat: "dd-mm-yy",
		numberOfMonths: 1
		})
		.on( "change", function() {
		to.datepicker( "option", "minDate", getDate( this ) );
		}),
	to = $( "#to" ).datepicker({
		defaultDate: "+1d",
		changeMonth: true,
		dateFormat: "dd-mm-yy",
		numberOfMonths: 1
	})
	.on( "change", function() {
		from.datepicker( "option", "maxDate", getDate( this ) );
	});

	function getDate( element ) {
		var date;
		try {
			date = $.datepicker.parseDate( dateFormat, element.value );
		} catch( error ) {
			date = null;
		}
		return date;
	}

	$('body').on('click','button.manage', function(e) {
		var loginas_hc = $(this).attr('hoid');
		var loginas_hname = $(this).parent().parent().children('div').eq(0).children('h3').html();
		$.ajax({
			type	: 'POST', cache: false,
			url		: '<?php echo"$base_url"; ?>/includes/suadm/hotel/change-session.php',
			data	: { loginas : loginas_hc, hname : loginas_hname },
			success	: function(rsp){ if(rsp == "1"){ document.location.href = '<?php echo"$base_url"; ?>/dashboard'; } }
		});
	});

	$('#example').DataTable({
		"aaSorting": [
		  [7, "dsc"],
		],
		"lengthMenu": [[25, 50, 75, -1], [25, 50, 75, "All"]]
	});
});
</script>
