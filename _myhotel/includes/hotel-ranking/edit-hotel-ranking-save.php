<?php
	include("../../conf/connection.php");

	$hoteloid = $_POST['ho'];
	$commission = $_POST['commission'];
	
	try {
		$stmt = $db->prepare("select * from hotelcommission WHERE hoteloid=:a");
		$stmt->execute(array(':a' => 4));
		$row_count = $stmt->rowCount();
	}catch(PDOException $ex) {
		echo "0".$ex;
		die();
	}

	if($row_count > 0){	
		try {
			$stmt = $db->prepare("UPDATE hotelcommission SET value=:a WHERE hoteloid=:b");
			$stmt->execute(array(':a' => $commission, ':b' => $hoteloid));
		}catch(PDOException $ex) {
			echo "0".$ex;
			die();
		}
	}else{
		try {
			$stmt = $db->prepare("insert into hotelcommission (hoteloid, value) values (:a,:b)");
			$stmt->execute(array(':a' => $hoteloid, ':b' => $commission));
		}catch(PDOException $ex) {
			echo "0".$ex;
			die();
		}
	}
	
	echo "1";
?>