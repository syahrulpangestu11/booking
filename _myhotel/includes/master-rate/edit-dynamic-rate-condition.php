<?php
try{
  session_start();
  error_reporting(E_ALL ^ E_NOTICE);
  include ('../../conf/connection.php');
  $hoteloid = $_SESSION['_hotel'];

  $stmt = $db->prepare("select * from dynamicrate where dynamicrateoid = :a");
  $stmt->execute(array(':a' => $_POST['id']));
  $rule = $stmt->fetch(PDO::FETCH_ASSOC);

  $stmt = $db->prepare("select dr.*, ro.* from dynamicraterule dr inner join roomoffer ro using (roomofferoid) where dr.dynamicrateoid = :a");
  $q_plan = $stmt->execute(array(':a' => $rule['dynamicrateoid']));
  foreach($stmt->fetchAll(PDO::FETCH_ASSOC) as $r_plan){
    $rateplan[] = $r_plan['roomofferoid'];
    $masterrate[] = $r_plan['masterrateoid'];
  }
?>
  <form>
    <input type="hidden" name="id" value="<?=$rule['dynamicrateoid']?>">
    <label># Step 1 : Add Condition</label>
    <div class="row">
      <div class="form-group">
        <label class="col-xs-4">If allocation &le;</label>
        <div class="col-xs-3"><input type="text" class="form-control" name="allocation" value="<?=$rule['allocation']?>"></div>
      </div>
    </div>
    <hr />
    <label># Step 2 : Select room and rate plan to applied on Dynamic Rate</label>
    <table class="table table-striped">
      <?php
        $stmt = $db->query("select r.roomoid, r.name, x.offernum from room r inner join hotel h using (hoteloid) left join (select count(roomofferoid) as offernum, ro.roomoid from roomoffer ro group by ro.roomoid) as x on x.roomoid = r.roomoid where h.hoteloid = '".$hoteloid."' and x.offernum > 0 and r.publishedoid = '1'");
        $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach($r_room as $row){
      ?>
        <tr><th colspan="2"><?=$row['name']?></th></tr>
        <tr><th>Room &amp; Rate Plan</th><th>Update Rate to</th></tr>
        <?php
          $stmt = $db->query("select ro.* from roomoffer ro inner join published p using (publishedoid) inner join room r using (roomoid) inner join offertype using (offertypeoid) where r.roomoid = '".$row['roomoid']."' and p.publishedoid = '1'");
          $r_plan = $stmt->fetchAll(PDO::FETCH_ASSOC);
          foreach($r_plan as $row1){
            if(in_array($row1['roomofferoid'], $rateplan)){
              $input_rp = "rcnt_rateplan[]";
              $input_mr = "rcnt_masterrate[]";
            }else{
              $input_rp = "rateplan[]";
              $input_mr = "masterrate[]";
            }
        ?>
        <tr>
          <td><input type="hidden" name="<?=$input_rp?>" value="<?=$row1['roomofferoid']?>"><?=$row1['name']?></td>
          <td>
            <select name="<?=$input_mr?>">
              <option value="">- select rate -</option>
              <?php
                $stmt = $db->prepare("select mr.*, cr.currencycode from masterrate mr inner join currency cr using (currencyoid) where mr.roomofferoid = :a");
                $stmt->execute(array(':a' => $row1['roomofferoid']));
                $row_count = $stmt->rowCount();
                $result_masterrate = $stmt->fetchAll(PDO::FETCH_ASSOC);
                foreach($result_masterrate as $mr){
              ?>
                <option value="<?=$mr['masterrateoid']?>" <?php if(in_array($mr['masterrateoid'], $masterrate)){ echo "selected"; } ?>><?=$mr['bar']." - ".$mr['currencycode']." ".number_format($mr['rate'])?></option>
              <?php
                }
              ?>
            </select>
          </td>
        </tr>
      <?php
          }
      ?>
        <tr><td colspan="2">&nbsp;</td></tr>
      <?php
        }
      ?>
    </table>
  </form>
<?php
}catch(Exception $e){
  echo $e->getMessage();
}
?>
