
<script type="text/javascript">
$(function(){

	$('body').on('click','button[name=add-master-rate]', function(e) {
			e.preventDefault();
			$('#AddMasterRate').modal('show');
	});
	
	$('body').on('click','button[name="view-detail"]', function(e) {
		elem = $(this).parent('td').parent('tr');
		forms = $('form#view-detail-master-rate');
		forms.find('input[name="room"]').val($(this).attr('data-id'));
		forms.find('input[name="rateplan"]').val(elem.find('select[name="rateplan[]"]').val());
		forms.find('input[name="bookingmarket"]').val(elem.find('select[name="bookingmarket[]"]').val());
		forms.submit();
	});

	$('body').on('click','#paging button', function(e) {
		page	= $(this).attr('page');
		form = $('form#view-detail-master-rate')
		form.find('input[name=page]').val(page);
		form.submit();
	});

	$('body').on('click', '#AddMasterRate button[name="btn-submit"]', function() {
    $.ajax({
      url: "<?php echo $base_url; ?>/includes/master-rate/save-master-rate.php",
      type: 'post',
      data: $('form#form-add-master-rate').serialize(),
      success: function(response) {
        if(response == "1"){
          location.reload();
        }
      }
    });
  });

	$('#DeleteMasterRate').on('show.bs.modal', function (event) {
		var modal = $(this);
		var sourceblocked = $(event.relatedTarget);
		var masterrate = sourceblocked.data('id');
		modal.find('input[name=masterrate]').val(masterrate);
	});

	$('body').on('click', '#DeleteMasterRate button[name="btn-delete"]', function() {
    $.ajax({
      url: "<?php echo $base_url; ?>/includes/master-rate/delete-master-rate.php",
      type: 'post',
      data: $('form#form-delete-master-rate').serialize(),
      success: function(response) {
        if(response == "1"){
          location.reload();
        }
      }
    });
  });

	$('#EditMasterRate').on('show.bs.modal', function (event) {
		var modal = $(this);
		var sourceblocked = $(event.relatedTarget);
		var masterrate = sourceblocked.data('id');
		$.ajax({
      url: "<?php echo $base_url; ?>/includes/master-rate/edit-master-rate.php",
      type: 'post',
      data: { masterrate:masterrate },
      success: function(response) {
				modal.find('.modal-body').html(response);
      }
    });
	});

	$('#AddMasterRate, #EditMasterRate').on('focus',"input[name=startdate], input[name=enddate]", function(){
    elem_startdate = $(this).parent().parent().parent().parent().find('input[name=startdate]');
    elem_enddate = $(this).parent().parent().parent().parent().find('input[name=enddate]');
    elem_startdate.datepicker({
    defaultDate: "+1w",
    changeMonth: true, changeYear:true, dateFormat: "dd MM yy",
    onClose: function( selectedDate ) {
      elem_enddate.datepicker( "option", "minDate", selectedDate );
    }
    });
    elem_enddate.datepicker({
        defaultDate: "+1w",
        changeMonth: true, changeYear:true, dateFormat: "dd MM yy",
        onClose: function( selectedDate ) {
          elem_startdate.datepicker( "option", "maxDate", selectedDate );
        }
    });
  });

	$('body').on('click', '#EditMasterRate button[name="btn-submit"]', function() {
    $.ajax({
      url: "<?php echo $base_url; ?>/includes/master-rate/save-edit-master-rate.php",
      type: 'post',
      data: $('form#form-edit-master-rate').serialize(),
      success: function(response) {
        if(response == "1"){
          location.reload();
        }
      }
    });
  });

/*
	$('body').on('click','button[name="add-condition"]', function(e) {
		$.ajax({
      url: "<?php echo $base_url; ?>/includes/master-rate/add-dynamic-rate-condition.php",
      type: 'post',
      data: $('form#dynamic-rateplan').serialize(),
			global: false,
      success: function(response) {
        $('div.list-condition').html(response);
      }
    });
	});
*/

	$(document).ready(function(){

		$('input[name="roomtype[]"]').each(function( index ) {
			rateplan = $(this).parent().find('ul.list-rateplan');
			rateplanchoice = $(this).parent().find('ul.list-rateplan li input[type=checkbox]');
			if($(this).prop('checked') == true){
				rateplan.show();
				rateplanchoice.attr('disabled', false);
			}else{
				rateplan.hide();
				rateplanchoice.attr('disabled', true);
			}
		});
	});

	$('body').on('click', 'input[name="roomtype[]"]', function (event) {
		rateplan = $(this).parent().find('ul.list-rateplan');
		rateplanchoice = $(this).parent().find('ul.list-rateplan li input[type=checkbox]');
		if($(this).prop('checked') == true){
			rateplan.show();
			rateplanchoice.attr('disabled', false);
		}else{
			rateplan.hide();
			rateplanchoice.attr('disabled', true);
		}
	});

	$('body').on('click', 'input[name=select]', function (event) {
		if($(this).val() == 'all'){
			$('ul.list-rateplan').show();
			$('ul.list-rateplan li input[type=checkbox]').attr('disabled', false);
			$('input[name="roomtype[]"]').prop('checked', true);
		}else{
			$('ul.list-rateplan').hide();
			$('ul.list-rateplan li input[type=checkbox]').attr('disabled', true);
			$('input[name="roomtype[]"]').prop('checked', false);
		}

		$('ul.list-rateplan li input[type=checkbox]').prop('checked', true);
	});

	$('#addCondition').on('show.bs.modal', function (event){
    var modal = $(this);
    $.post('<?php echo $base_url; ?>/includes/master-rate/add-dynamic-rate-condition.php', $('form#dynamic-rateplan').serialize(), function(response){
      modal.find('.modal-body').html(response);
		});
  });

	$('#addCondition').on('click', 'button[name=save]', function (event) {
		$.ajax({
      url: "<?php echo $base_url; ?>/includes/master-rate/save-add-dynamic-rate-condition.php",
      type: 'post',
      data: $('#addCondition form').serialize(),
      success: function(response) {
        if(response == "1"){
          location.reload();
        }
      }
    });
	});

	$('body').on('click', 'input[name=activerule]', function (event) {
		if($(this).prop('checked') == true){ var active = 1; }else{ var active = 0; }
		$.ajax({
      url: "<?php echo $base_url; ?>/includes/master-rate/activation-dynamic-rate.php",
      type: 'post',
      data: { activate : active},
			global: false,
      success: function(response) { /*alert(response);*/ }
    });
	});

	$('#removeCondition').on('show.bs.modal', function (event) {
		var modal = $(this);
		var sourceblocked = $(event.relatedTarget);
		var dynamicrule = sourceblocked.data('id');
		modal.find('input[name=dynamicrule]').val(dynamicrule);
	});

	$('#removeCondition').on('click', 'button[name=delete]', function (event) {
		var id = $('#removeCondition input[name=dynamicrule]').val();
		$.ajax({
      url: "<?php echo $base_url; ?>/includes/master-rate/delete-dynamic-rate-condition.php",
      type: 'post',
      data: { id : id },
      success: function(response) { if(response == "1"){ location.reload(); }else{ alert(response); } }
    });
	});

	$('#updateCondition').on('show.bs.modal', function (event) {
		var modal = $(this);
		var sourceblocked = $(event.relatedTarget);
		var dynamicrule = sourceblocked.data('id');
		$.ajax({
      url: "<?php echo $base_url; ?>/includes/master-rate/edit-dynamic-rate-condition.php",
      type: 'post',
      data: { id : dynamicrule },
      success: function(response) {
				modal.find('.modal-body').html(response);
      }
    });
	});

	$('#updateCondition').on('click', 'button[name=save]', function (event) {
		$.ajax({
			url: "<?php echo $base_url; ?>/includes/master-rate/save-edit-dynamic-rate-condition.php",
			type: 'post',
			data: $('#updateCondition form').serialize(),
			success: function(response) {
				if(response == "1"){
					location.reload();
				}
			}
		});
	});

});
</script>
