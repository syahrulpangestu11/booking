<?php
try{
  session_start();
  error_reporting(E_ALL ^ E_NOTICE);
  include ('../../conf/connection.php');

  $stmt = $db->prepare("insert into dynamicrate (hoteloid, allocation, publishedoid) value (:a, :b, :c)");
  $stmt->execute(array(':a' => $_SESSION['_hotel'], ':b' => $_POST['allocation'], ':c' => 1));
  $id = $db->lastInsertId();

  foreach($_POST['rateplan'] as $key=> $rateplan){
    if(!empty($_POST['masterrate'][$key])){
      $stmt = $db->prepare("insert into dynamicraterule (dynamicrateoid, roomofferoid, masterrateoid) value (:a, :b, :c)");
      $stmt->execute(array(':a' => $id, ':b' => $rateplan, ':c' => $_POST['masterrate'][$key]));
    }
  }

  echo "1";

}catch(Exception $e){
  echo $e->getMessage();
}
?>
