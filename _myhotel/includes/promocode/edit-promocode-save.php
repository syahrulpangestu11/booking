<?php
	$promocodeoid = $_POST['promocodeoid'];
	$startdate = date("Y-m-d", strtotime($_POST['startdate']));
	$enddate = date("Y-m-d", strtotime($_POST['enddate']));
	$applybar = (isset($_POST['applybar'])) ? $_POST['applybar'] : "n";	
	$applybardiscount = (isset($_POST['applybardiscount'])) ? $_POST['applybardiscount'] : "n";	
	$applybarcommission = (isset($_POST['applybarcommission'])) ? $_POST['applybarcommission'] : "n";	
	$allowzerotrx = (isset($_POST['allowzerotrx']) and $_POST['allowzerotrx'] == '1') ? '1' : '0';	
	
	try {
		$stmt = $db->prepare("update promocode set promocode=:a, name=:b, description=:c, startdate=:d, enddate=:e, discounttype=:f, discount=:g, commissiontype=:h, commission=:i, applybar=:j, applybardiscount=:k, applybarcommission=:l, pic_name=:m, pic_number=:n, publishedoid=:o, allowzerotrx=:p where promocodeoid=:poid");
		$stmt->execute(array(':poid' => $promocodeoid, ':a' => $_POST['code'], ':b' => $_POST['name'], ':c' => $_POST['description'], ':d' => $startdate, ':e' => $enddate, ':f' => $_POST['discounttype'], ':g' => $_POST['discount'], ':h' => $_POST['commissiontype'], ':i' => $_POST['commission'], ':j' => $applybar, ':k' => $applybardiscount, ':l' => $applybarcommission, ':m' => $_POST['pic_name'], ':n' => $_POST['pic_number'], ':o' => $_POST['published'], ':p' => $allowzerotrx));
		
		if(isset($_POST['apply'])){
			$existedapply = array();
			foreach($_POST['apply'] as $key => $value){
				$value2 = explode('-', $value);
				$reference = $value2[0];
				$id = $value2[1];
				$applydiscount = (isset($_POST['applydiscount-'.$value])) ? $_POST['applydiscount-'.$value] : "n";	
				$applycommission = (isset($_POST['applycommission-'.$value])) ? $_POST['applycommission-'.$value] : "n";	
				$stmt = $db->prepare("select promocodeapplyoid from promocodeapply where promocodeoid = :a and referencetable = :b and id = :c");
				$stmt->execute(array(':a' => $promocodeoid, ':b' => $reference, ':c' =>  $id));
				$row_count = $stmt->rowCount();
				if($row_count > 0){
					$rowpromo_found = $stmt->fetch(PDO::FETCH_ASSOC);
					$stmt = $db->prepare("update promocodeapply set applydiscount=:a, applycommission=:b where promocodeapplyoid=:c");
					$stmt->execute(array(':a' => $applydiscount, ':b' => $applycommission, ':c' => $rowpromo_found['promocodeapplyoid']));
					array_push($existedapply, $rowpromo_found['promocodeapplyoid']);
				}else{
					$stmt = $db->prepare("insert into promocodeapply (promocodeoid, referencetable, id, applydiscount, applycommission) values (:a, :b, :c, :d, :e)");
					$stmt->execute(array(':a' => $promocodeoid, ':b' => $reference, ':c' => $id, ':d' => $applydiscount, ':e' => $applycommission));
					array_push($existedapply, $db->lastInsertId());
				}
			}
			
			$existedapply = implode("','", $existedapply);
			$stmt = $db->query("delete from promocodeapply where promocodeoid = '".$promocodeoid."' and promocodeapplyoid not in ('".$existedapply."')");
		}else{
			$stmt = $db->prepare("delete from promocodeapply where promocodeoid=:a");
			$stmt->execute(array(':a' => $promocodeoid));
		}
	?>
		<script type="text/javascript">
           $(function(){ $( document ).ready(function(){ $("#dialog-success").dialog("open"); }); });
        </script>
	<?php
	}catch(PDOException $ex) {
		//print_r($ex);
	?>
		<script type="text/javascript">
           $(function(){ $( document ).ready(function(){ $("#dialog-error").dialog("open"); }); });
        </script>
	<?php	
	}
?>