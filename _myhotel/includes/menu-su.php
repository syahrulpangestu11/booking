<li <?php if($uri2=="dashboard"){ echo "class=active"; }?>>
    <a href="<?php echo"$base_url"; ?>/dashboard"><i class="fa fa-dashboard"></i><span>Dashboard</span></a>
</li>

<li class="treeview  <?php if($uri2=="hotel" or $uri2=="promotion-template" or $uri2=="package-template"  or $uri2=="extra-template" or $uri2=="poi" or $uri2=="contact-management-all" or $uri2=="promotion-all"){ echo "active"; }?>">
    <a href="<?php echo"$base_url"; ?>/hotel"><i class="fa fa-home"></i><span>Hotel</span></a>
    <ul class="treeview-menu">
        <li <?php if( $uri2=="hotel"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/hotel"><i class="fa fa-circle-o"></i> Hotels</a></li>
        <?php /*?>
        <li <?php if( $uri2=="promotion-template"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/promotion-template"><i class="fa fa-circle-o"></i> Promotion Template</a></li>
        <li <?php if( $uri2=="package-template"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/package-template"><i class="fa fa-circle-o"></i> Package Template</a></li>
        <li <?php if( $uri2=="extra-template"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/extra-template"><i class="fa fa-circle-o"></i> Extra Template</a></li>
        <li <?php if( $uri2=="poi"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/poi"><i class="fa fa-circle-o"></i> POI Data</a></li>
        <li <?php if( $uri2=="promotion-all"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/promotion-all"><i class="fa fa-circle-o"></i> Promotion All</a></li>
        <?php */?>
        <li <?php if( $uri2=="contact-management-all"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/contact-management-all"><i class="fa fa-circle-o"></i> Contact Management All</a></li>
        <li <?php if( $uri2=="booking-all"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/booking-all"><i class="fa fa-circle-o"></i> Booking All</a></li>
    </ul>
</li>

<li <?php if($uri2=="chain"){ echo "class=active"; }?>>
    <a href="<?php echo"$base_url"; ?>/chain"><i class="fa fa-chain"></i><span>Chain Hotel</span></a>
</li>

<li style="display:none">
    <a href="<?php echo"$base_url"; ?>/activities"><i class="fa fa-home"></i><span>Activities</span></a>
</li>

<li class="treeview <?php if($uri2=="country" or $uri2=="state" or $uri2=="city"){ echo "active"; }?>">
    <a href="#">
        <i class="fa fa-map-marker"></i><span>Destination</span>
        <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
        <li <?php if( $uri2=="country"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/country"><i class="fa fa-circle-o"></i> Country</a></li>
        <li <?php if($uri2=="state"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/state"><i class="fa fa-circle-o"></i> State</a></li>
        <li <?php if($uri2=="city"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/city"><i class="fa fa-circle-o"></i> City</a></li>
    </ul>
</li>

<li <?php if($uri2=="user"){ echo "class=active"; }?>>
    <a href="<?php echo"$base_url"; ?>/user"><i class="fa fa-user-o"></i><span>User</span></a>
</li>
<!-- <li <?php if($uri2=="agent"){ echo "class=active"; }?>>
    <a href="<?php echo"$base_url"; ?>/all-agents"><i class="fa fa-users"></i><span>Agent</span></a>
</li> -->

<!-- <li <?php if($uri2=="cancellationpolicy"){ echo "class=active"; }?>>
    <a href="<?php echo"$base_url"; ?>/cancellationpolicy"><i class="fa fa-ban"></i><span>Cancellation Policy</span></a>
</li> -->
<li class="treeview <?php if($uri2=="webprofile" or $uri2=="faq"){ echo "active";}?>">
    <a href="<?php echo"$base_url"; ?>/webprofile">
      <i class="fa fa-cog"></i><span>General Settings</span>
      <i class="fa fa-angle-left pull-right"></i>
    </a>

    <ul class="treeview-menu">
        <li <?php if( $uri2=="webprofile"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/webprofile"><i class="fa fa-circle-o"></i> Web Profile</a></li>
        <li <?php if($uri2=="faq"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/faq"><i class="fa fa-circle-o"></i> FAQ</a></li>
        <li <?php if( $uri2=="contact-management-web"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/contact-management-web"><i class="fa fa-circle-o"></i> Contact Management</a></li>
        <li <?php if( $uri2=="payment-method"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/payment-method"><i class="fa fa-circle-o"></i> Payment Method</a></li>
        <?php if($_SESSION['_typeusr']=='1'){ ?>
        <li <?php if( $uri2=="admin-settings"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/admin-settings"><i class="fa fa-circle-o"></i> Admin Settings</a></li>
        <?php } ?>
        <?php if($_SESSION['_typeusr']=='1'||$_SESSION['_typeusr']=='2'){ ?>
        <li <?php if( $uri2=="email-template"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/email-template"><i class="fa fa-circle-o"></i> Email Template</a></li>
        <?php } ?>
    </ul>
</li>
<?php /*/ ?>
<li <?php if($uri2=="all-agents"){ echo "class=active"; }?> style="display:none;"><a href="<?php echo"$base_url"; ?>/all-agents"><i class="fa fa-user-o"></i> Offline Agent</a></li>

<li class="treeview <?php if($uri2=="web-event" or $uri2=="web-contact" or $uri2=="web-trial" or $uri2=="web-demo"){ echo "active"; }?>">
    <a href="#">
        <i class="fa fa-user-circle"></i><span>Web Interaction</span>
        <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
        <li <?php if($uri2=="web-event" and $uri3=="bali"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/web-event/bali"><i class="fa fa-circle-o"></i> Event Bali</a></li>
        <li <?php if($uri2=="web-event" and $uri3=="jogja"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/web-event/jogja"><i class="fa fa-circle-o"></i> Event Jogja</a></li>
        <li <?php if($uri2=="web-contact"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/web-contact"><i class="fa fa-circle-o"></i> Contact Us</a></li>
        <li <?php if($uri2=="web-trial"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/web-trial"><i class="fa fa-circle-o"></i> Trial Request</a></li>
        <li <?php if($uri2=="web-demo"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/web-demo"><i class="fa fa-circle-o"></i> IBE Demo Request</a></li>
    </ul>
</li>


<li class="treeview <?php if($uri2=="sales-marketing"){ echo "active"; }?>">
    <a href="#">
        <i class="fa fa-bullhorn"></i><span>Sales &amp; Marketing</span>
        <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
        <li <?php if($uri3=="hotel-assessment"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/sales-marketing/hotel-assessment"><i class="fa fa-circle-o"></i> Hotel Assessment</a></li>
        <li <?php if($uri3=="stage-process"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/sales-marketing/stage-process"><i class="fa fa-circle-o"></i> Stage Process</a></li>
        <li <?php if($uri3=="wdm-stage"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/sales-marketing/wdm-stage"><i class="fa fa-circle-o"></i> WDM</a></li>
        <li <?php if($uri3=="ibe-stage"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/sales-marketing/ibe-stage"><i class="fa fa-circle-o"></i> IBE</a></li>
        <li <?php if($uri3=="other-stage"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/sales-marketing/other-stage"><i class="fa fa-circle-o"></i> Other Stage</a></li>
        <li <?php if($uri3=="bispro-report"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/sales-marketing/bispro-report"><i class="fa fa-circle-o"></i> Bispro Report</a></li>
        <li <?php if($uri3=="kpi"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/sales-marketing/kpi"><i class="fa fa-circle-o"></i> KPI</a></li>
        <li <?php if($uri3=="daily-update-sm"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/sales-marketing/daily-update-sm"><i class="fa fa-circle-o"></i> Daily Update SM</a></li>
        <li <?php if($uri3=="management-summary"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/sales-marketing/management-summary"><i class="fa fa-circle-o"></i> Management Summary</a></li>
        <li <?php if($uri3=="production-detail"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/sales-marketing/production-detail"><i class="fa fa-circle-o"></i> Production Dtl</a></li>
        <li <?php if($uri3=="occupancy-detail"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/sales-marketing/occupancy-detail"><i class="fa fa-circle-o"></i> Distributed Dtl</a></li>
        <li <?php if($uri2=="alloccupancyreport"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/alloccupancyreport"><i class="fa fa-circle-o"></i> All Occupancy Report</a></li>
        <li <?php if($uri2=="contacts"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/contacts"><i class="fa fa-circle-o"></i> Contacts</a></li>
        <li <?php if($uri2=="invoicing"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/invoicing"><i class="fa fa-circle-o"></i> Invoicing</a></li>
    </ul>
</li>
<?php //*/ ?>