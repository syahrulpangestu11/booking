<script type="text/javascript">
$(function(){
	$('#dashboard .dashboard-item').click(function(){
		linking = $(this).attr('link');
		if(linking !== ''){
			$(location).attr("href", linking);
		}
	});
});
</script>
<?php
$today = date("Y-m-d");
$today_plus_7 = date("Y-m-d",strtotime($today." +7 day"));

$this_week_firstdate = date("Y-m-d",strtotime("this week"));
$this_week_lastdate = date("Y-m-d",strtotime("this week +6 days"));

$this_month_firstdate = date("Y-m-d",strtotime("first day of this month"));
$this_month_lastdate = date("Y-m-d",strtotime("last day of this month"));

$query_form = "
	from booking b
	inner join hotel h using (hoteloid)
	inner join bookingstatus bs using (bookingstatusoid)
	inner join customer c using (custoid)
	inner join bookingroom br using (bookingoid)
	inner join roomoffer ro using (roomofferoid)
	inner join room r using (roomoid)
	left join bookingpayment using (bookingoid)
	where h.hoteloid = '".$hoteloid."' ";

$query_group_bookingoid = " group by b.bookingoid";

$query_form_detail = "
	from booking b
	inner join hotel h using (hoteloid)
	inner join bookingstatus bs using (bookingstatusoid)
	inner join customer c using (custoid)
	inner join bookingroom br using (bookingoid)
	inner join roomoffer ro using (roomofferoid)
	inner join room r using (roomoid)
	inner join bookingroomdtl brd using (bookingroomoid)
	left join bookingpayment using (bookingoid)
	where h.hoteloid = '".$hoteloid."' ";

	include('includes/function-tracking.php');
	// echo "<pre>";
	// print_r(TAPeriodeDate($start, $end));
	// echo "<hr>";
	// print_r($_SESSION);
	// echo "</pre>";

	$startdate = date('Y-m-01');
	$enddate = date('Y-m-d');
	$allvisit = getVisit($hoteloid, "availability", $start, $end);

	if($allvisit > 0){
		$averagevisit = floor($allvisit / date('t'));
	}else{
		$averagevisit = 0;
	}

	$sum_allbooking 	= "select count(bookingoid) as booking from booking inner join hotel h using (hoteloid) where (date(bookingtime) >= '".$startdate."' and date(bookingtime) <= '".$enddate."') and h.hoteloid = '".$hoteloid."' and booking.bookingstatusoid = '4'";
	$stmt			= $db->query($sum_allbooking);
	$arr_allbooking	= $stmt->fetch(PDO::FETCH_ASSOC);
	$countbooking 	= $arr_allbooking['booking'];

	$sum_allroomnight 	= "select count(bookingroomdtloid) as roomnight from bookingroomdtl inner join bookingroom using (bookingroomoid) inner join booking using (bookingoid) inner join hotel h using (hoteloid) where (date(bookingtime) >= '".$startdate."' and date(bookingtime) <= '".$enddate."') and h.hoteloid = '".$hoteloid."' and booking.bookingstatusoid = '4'";
	
	$stmt				= $db->query($sum_allroomnight);
	$arr_allroomnight	= $stmt->fetch(PDO::FETCH_ASSOC);
	$countroomnight		= $arr_allroomnight['roomnight'];

	if($countbooking > 0){
		$connversionrate_booking = number_format($countbooking / $allvisit * 100,2,",",".");
	}else{
		$connversionrate_booking = 0;
	}

	if($countroomnight > 0){
		$connversionrate_roomnight = number_format($countroomnight / $allvisit * 100,2,",",".");
	}else{
		$connversionrate_roomnight = 0;
	}
?>
<div class="box box-form">
    <h2>Hotel : <?php echo $_SESSION['_hotelname']; ?></h2>
</div>
<div id="dashboard">

    <ul class="mybox">
			<!-- <h1 class="box">Manage Your Business</h1> -->
      <li class="dashboard-item box red one-third" link="<?php echo $base_url; ?>/availability">
        <div class="left"><i class="fa fa-2x fa-bed"></i></div>
				<div class="right">
					<h2 class="title-1">Manage</h2>
					<h2 class="title-2">Availability</h2>
				</div>
      </li>
      <li class="dashboard-item box red one-third" link="<?php echo $base_url; ?>/rate-control">
				<div class="left"><i class="fa fa-2x fa-money"></i></div>
				<div class="right">
					<h2 class="title-1">Manage</h2>
					<h2 class="title-2">Rates</h2>
				</div>
      </li>
			<!-- <li class="dashboard-item box red one-third" link="<?php echo $base_url; ?>/agent">
				<div class="left"><i class="fa fa-2x fa-user"></i></div>
				<div class="right">
					<h2 class="title-1">Manage</h2>
					<h2 class="title-2">Agents</h2>
				</div>
      </li> -->
      <li class="dashboard-item box red one-third" link="mailto:sales@thebuking.com">
				<div class="left"><i class="fa fa-2x fa-percent"></i></div>
				<div class="right">
					<h2 class="title-1">Commission</h2>
					<h2 class="title-2">subscribe our promotion portal</h2>
				</div>
      </li>
			<!-- <h1>Your Statistics</h1> -->
			<!-- <li class="dashboard-item box blue one-third" link="#">
        <div class="left"><i class="fa fa-2x fa-battery"></i></div>
				<div class="right">
					<h2 class="title-1">Content Score</h2>
					<h2 class="title-2">97%</h2>
				</div>
      </li> -->
      <li class="dashboard-item box blue one-third" link="<?php echo $base_url; ?>/tracking-analytics">
				<div class="left"><i class="fa fa-2x fa-users"></i></div>
				<div class="right">
					<h2 class="title-1">Average Daily Visitors</h2>
					<h2 class="title-2"><?=$averagevisit?></h2>
				</div>
      </li>
      <li class="dashboard-item box blue one-third" link="<?php echo $base_url; ?>/performance-reports/reservation-report">
				<div class="left"><i class="fa fa-2x fa-shopping-cart"></i></div>
				<div class="right">
					<h2 class="title-1">Conversion Rate</h2>
					<h2 class="title-2">Booking : <?=$connversionrate_booking?> %</h2>
                    <h2 class="title-2">Room Night : <?=$connversionrate_roomnight?> %</h2>
				</div>
      </li>
        <li class="dashboard-item box blue one-third" link="<?php echo $base_url; ?>/booking">
            <div class="left"><i class="fa fa-2x fa-key"></i></div>
            <div class="right">
                <?php
                $queryOccupancy = "select count(bookingroomdtloid) as numberroom ".$query_form_detail." and (DATE(br.checkin) >= '".$this_month_firstdate."' and DATE(br.checkin) <= '".$this_month_lastdate."') AND bookingstatusoid IN (4)";
				$queryProduction = "select count(bookingroomdtloid) as numberroom ".$query_form_detail." and (DATE(bookingtime) >= '".$this_month_firstdate."' and DATE(bookingtime) <= '".$this_month_lastdate."') AND bookingstatusoid IN (4) ";
				$query = "SELECT (".$queryOccupancy.") AS occup, (".$queryProduction.") AS prod";
				// echo $queryOccupancy;
                try {
                    $stmt = $db->query($query);
                    $r_arrival = $stmt->fetchAll(PDO::FETCH_ASSOC);
                    foreach($r_arrival as $row){
                        $numOccup = $row['occup'];
						$numProd = $row['prod'];
                    }
                }catch(PDOException $ex) {
                    echo "Invalid Query";
                    print($ex);
                    die();
                }
                ?>
				<h2 class="title-1">Room nights sold this month</h2>
				<h2 class="title-2">Production : <?php echo $numProd; ?></h2>
				<h2 class="title-2">Occupancy : <?php echo $numOccup; ?></h2>
            </div>
      </li>
			<!-- <h1>Happening Now</h1> -->
			<li class="dashboard-item box purple one-third" link="#">
				<div class="left"><i class="fa fa-2x fa-suitcase"></i></div>
				<div class="right">
					<?php
					$query = "SELECT count(b.bookingoid) as numberci from booking b INNER JOIN bookingroom br USING (bookingoid) where hoteloid = '".$_SESSION['_hotel']."' and (DATE(br.checkin) >= '".$this_week_firstdate."' and DATE(br.checkin) <= '".$this_week_lastdate."')";
					// echo $query;
					try {
						$stmt = $db->query($query);
						$r_arrival = $stmt->fetchAll(PDO::FETCH_ASSOC);
						foreach($r_arrival as $row){
							$numofci = $row['numberci'];
						}
					}catch(PDOException $ex) {
						echo "Invalid Query";
						print($ex);
						die();
					}
					?>
					<h2 class="title-1">Arrivals This Week</h2>
					<h2 class="title-2"><?php echo $numofci; ?></h2>
                    <div style="font-size:0.9em;">
						<?php
						// $day = date('w') - 1;
						// $day = 169;
						// $this_week_firstdate = date('Y-m-d', strtotime('-'.$day.' days'));
						// $this_week_lastdate = date('Y-m-d', strtotime('+'.(6-$day).' days'));

						$query = "select b.bookingoid, br.checkin, CONCAT(firstname, ' ', lastname) as guestname ".$query_form." and (DATE(checkin) >= '".$this_week_firstdate."' and DATE(checkin) <= '".$this_week_lastdate."') ".$query_group_bookingoid." ORDER BY b.bookingoid DESC LIMIT 2 ";
						// echo "------------".$query."------------";
						try {
							$stmt = $db->query($query);
							$r_arrival = $stmt->fetchAll(PDO::FETCH_ASSOC);
							foreach($r_arrival as $row){
								echo $row['guestname']."<br>";
								try {
									$stmt = $db->query("select count(bookingroomoid)as jmlroom, checkin, checkout from bookingroom where bookingoid = '".$row['bookingoid']."' group by checkin, checkout");
									$r_cico = $stmt->fetchAll(PDO::FETCH_ASSOC);
									foreach($r_cico as $row1){
										echo $row1['jmlroom']." room &bull; ci/co ".$row1['checkin']."/".$row1['checkout']."<br>";
									}
								}catch(PDOException $ex) {
									echo "Invalid Query";
									print($ex);
									die();
								}

							}
						}catch(PDOException $ex) {
							echo "Invalid Query";
							print($ex);
							die();
						}
						?>
					</div>
				</div>
      </li>


			<li class="dashboard-item box purple one-third" link="#">
				<div class="left"><i class="fa fa-2x fa-calendar-check-o"></i></div>
				<div class="right">
					<?php
					$query = "select count(b.bookingoid) as numberrsv from booking b where hoteloid = '".$_SESSION['_hotel']."' and (DATE(bookingtime) >= '".$this_week_firstdate."' and DATE(bookingtime) <= '".$this_week_lastdate."')";
					try {
						$stmt = $db->query($query);
						$r_arrival = $stmt->fetchAll(PDO::FETCH_ASSOC);
						foreach($r_arrival as $row){
							$numofrsv = $row['numberrsv'];
						}
					}catch(PDOException $ex) {
						echo "Invalid Query";
						print($ex);
						die();
					}
					?>
					<h2 class="title-1">Reservation this week</h2>
					<h2 class="title-2"><?php echo $numofrsv; ?></h2>
                    <div style="font-size:0.9em;">
					<?php
						$startdate = date("Y-m-d");
						$enddate = date("Y-m-d");
											$query = "SELECT b.bookingoid, CONCAT(firstname, ' ', lastname) as guestname ".$query_form." and (DATE(bookingtime) >= '".$this_week_firstdate."' and DATE(bookingtime) <= '".$this_week_lastdate."') ".$query_group_bookingoid." ORDER BY b.bookingoid DESC LIMIT 2";
						try {
							$stmt = $db->query($query);
							$r_arrival = $stmt->fetchAll(PDO::FETCH_ASSOC);
							foreach($r_arrival as $row){
								echo $row['guestname']."<br>";
								try {
									$stmt = $db->query("select count(bookingroomoid)as jmlroom, checkin, checkout from bookingroom where bookingoid = '".$row['bookingoid']."' group by checkin, checkout");
									$r_cico = $stmt->fetchAll(PDO::FETCH_ASSOC);
									foreach($r_cico as $row1){
										echo $row1['jmlroom']." room &bull; ci/co ".$row1['checkin']."/".$row1['checkout']."<br>";
									}
								}catch(PDOException $ex) {
									echo "Invalid Query";
									print($ex);
									die();
								}

							}
						}catch(PDOException $ex) {
							echo "Invalid Query";
							print($ex);
							die();
						}
					?>
					</div>
				</div>
      </li>

			<li class="dashboard-item box purple one-third" link="<?php echo $base_url; ?>/adr-report">
				<div class="left"><i class="fa fa-2x fa-calendar"></i></div>
				<div class="right">
					<h2 class="title-1">Average Daily Rates Report</h2>
					<!-- <h2 class="title-2">1</h2> -->
					<div style="font-size:0.9em;">
					<?php
						$startdate = date("Y-m-d");
						$enddate = date("Y-m-d");
						$roomRateHotel = array();
						

							try {
								$stmt = $db->query("select r.roomoid, r.name, x.offernum from room r inner join hotel h using (hoteloid) left join (select count(roomofferoid) as offernum, ro.roomoid from roomoffer ro group by ro.roomoid) as x on x.roomoid = r.roomoid where h.hoteloid = '".$hoteloid."' and x.offernum > 0 and r.publishedoid = '1'");
								$r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
								// echo "<pre>";
								// print_r($r_room);
								foreach($r_room as $row){
									try {
										$stmt = $db->query("select ro.* from roomoffer ro inner join published p using (publishedoid) inner join room r using (roomoid) inner join offertype using (offertypeoid) where r.roomoid = '".$row['roomoid']."' and p.publishedoid = '1'");
										$r_offer = $stmt->fetchAll(PDO::FETCH_ASSOC);
										foreach($r_offer as $row1){

											$roomoffer = $row1['roomofferoid'];
											$roomoffername = $row1['name'];

											try {
												$stmt = $db->query("select h.hotelcode, r.roomoid, ro.minrate from room r inner join hotel h using (hoteloid) inner join roomoffer ro using (roomoid) where ro.roomofferoid = '".$roomoffer."'");
												$row_count = $stmt->rowCount();
												if($row_count > 0) {
													$r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
													foreach($r_room as $row){
														$hcode = $row['hotelcode'];
														$roomoid = $row['roomoid'];
														$minrate = $row['minrate'];

														$date = date("Y-m-d");
														$channeltype = 1;

														$xml =  simplexml_load_file("data-xml/".$hcode.".xml");
														$query_tag = '//hotel[@id="'.$hoteloid.'"]/masterroom[@id="'.$roomoid.'"]/rate[@date="'.$date.'"]/rateplan[@id="'.$roomoffer.'"]/channel[@type="'.$channeltype.'"]';
														foreach($xml->xpath($query_tag) as $rate) { 	
															list($extendedpolicy, $netsingle, $single, $netdouble, $double, $netextrabed, $extrabed, $currency, $topup, $breakfast, $minstay, $maxstay, $blackout, $surcharge) = array($rate->extendedpolicy , $rate->netsingle , $rate->single , $rate->netdouble , $rate->double , $rate->netextrabed , $rate->extrabed , $rate->currency, $rate->topup , $rate->breakfast , $rate->minstay , $rate->maxstay ,  $rate->blackout , $rate->surcharge);
														} 

														$roomRate = (string) $rate->netdouble;
														$roomRateCurrency = (string) $rate->currency;

														if(!empty($roomRateCurrency)){
															$stmt = $db->query("SELECT currencycode FROM currency WHERE currencyoid = '".$roomRateCurrency."'");
															$row_count = $stmt->rowCount();
															if($row_count > 0) {
																$r_cur = $stmt->fetch(PDO::FETCH_ASSOC);
																// print_r($r_cur);
																$rateCurrency = $r_cur['currencycode'];
															}
														}else{
															$rateCurrency = "IDR";
														}
														

														$roomRateHotel[] = array("name" => $roomoffername, "rate" => $roomRate, "currency" => $rateCurrency);
													}
												}else{
													echo "No Result";
													die();
												}
											}catch(PDOException $ex) {
												echo "Invalid Query";
												die();
											}


										}
									}catch(PDOException $ex) {
										echo "Invalid Query";
										die();
									}     
								}
							}catch(PDOException $ex) {
								echo "Invalid Query";
								die();
							}
							// print_r($roomRateHotel);
							
							$prices = array_column($roomRateHotel, 'rate');
							$min_array = $roomRateHotel[array_search(min($prices), $prices)];
							$max_array = $roomRateHotel[array_search(max($prices), $prices)];
							$avgRate = $min_array['currency']." ".number_format(array_sum($prices) / count($prices));
							
							// print_r ($max_array);
							// echo "</pre>";

							$minRate = $min_array['currency']." ".number_format($min_array['rate']);
							$maxRate = $max_array['currency']." ".number_format($max_array['rate']);
							echo "<b>Lowest rate</b> : <i>".$min_array['name']."</i> - ".$minRate;
							echo "<br>";
							echo "<b>Highest rate</b> : <i>".$max_array['name']."</i> - ".$maxRate;
							echo "<br>";
							echo "<b>Today's average</b> : ".$avgRate;

?>
					</div>
				</div>
      </li>

    </ul>

<style>
	/* #dashboard ul > li.dashboard-item {overflow: visible !important; display: block;} */
</style>

	<?php include('includes/reports/reservation-report-include.php'); ?>
</div>
