<section class="content-header">
    <h1>
       	Reference Code
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Settings</a></li>
        <li class="active">Reference Code</li>
    </ol>
</section>
<script type="text/javascript" src="<?php echo $base_url;?>/js/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
tinyMCE.init({
	theme : "advanced",
	theme_advanced_buttons1 : "bold,italic,underline,separator,bullist,numlist,separator,undo,redo,separator,hr,removeformat,visualaid,separator,sub,sup,separator,formatselect",
	mode : "textareas",
    editor_selector : "mceEditor"
});
</script>
<section class="content">
    <div class="row">
        <div class="box box-form">
			<div class="box-body">
				<?php
                $id=$_GET['id'];
                include("includes/js.datepicker.php");
                
				try {
					$stmt = $db->query("SELECT * FROM `referencecode` WHERE `referencecodeoid`='".$id."'");
					$r_refcode = $stmt->fetchAll(PDO::FETCH_ASSOC);
					foreach($r_refcode as $refcode){
						$startdate = date("d F Y",strtotime($refcode['startdate']));
               			$enddate = date("d F Y",strtotime($refcode['enddate']));
						?>
						
                <form name="refcode" method="post" action="index.php?page=refcodeedit_process">
                <input type="hidden" name="referencecodeoid" value="<?php echo"$refcode[referencecodeoid]"; ?>" />
                <table>
                    <tr><td>Code</td><td><input type="text" class="input" name="code" value="<?php echo"$refcode[referencecode]"; ?>" readonly="readonly" /></td></tr>
                    <tr><td>Title</td><td><input type="text" class="input" name="name" value="<?php echo"$refcode[name]"; ?>" /></td></tr>
                    <tr valign="top"><td>Description</td><td><textarea name="description" id="description" class="mceEditor" cols="110" rows="8"><?php echo"$refcode[description]"; ?></textarea></td></tr>
                    <tr valign="top"><td>Type</td><td>
                        <ul style="list-style:none; margin:0; padding:0;">
                            <li><input type="radio" name="type" value="d" <?php if($refcode['type']=='d'){ echo'checked="checked"'; }?> /> &nbsp;Discount
                                <div style="margin:10px;">
                                    <input type="text" class="input" name="d0" size="3" value="<?php if($refcode['type']=='d'){ echo"$refcode[discount]"; }?>" /> &nbsp; %
                                </div></li>
                            <li><input type="radio" name="type" value="c" <?php if($refcode['type']=='c'){ echo'checked="checked"'; }?> /> &nbsp;Commision
                                <div style="margin:10px;">
                                    <input type="text" class="input" name="c0" size="3" value="<?php if($refcode['type']=='c'){ echo"$refcode[commision]"; }?>" /> &nbsp; %
                                </div></li>
                            <li><input type="radio" name="type" value="dc" <?php if($refcode['type']=='dc'){ echo'checked="checked"'; }?> /> &nbsp;Both
                                <div style="margin:10px;">
                                    Discount : <input type="text" class="input" name="d1" size="3" value="<?php if($refcode['type']=='dc'){ echo"$refcode[discount]"; }?>" /> &nbsp; %
                                     &nbsp; &nbsp;
                                    Commision : <input type="text" class="input" name="c1" size="3" value="<?php if($refcode['type']=='dc'){ echo"$refcode[commision]"; }?>" /> &nbsp; %
                                </div></li>
                        </ul>
                    </td></tr>
                    <tr><td>Start Date</td><td><input type="text" name="startdate" id="startdate" value="<?php echo $startdate;?>"></td></tr>
                    <tr><td>End Date</td><td><input type="text" name="enddate" id="enddate" value="<?php echo $enddate;?>"></td></tr>
                    <tr><td>PIC Name</td><td><input type="text" class="input" name="pic" value="<?php echo"$refcode[pic]"; ?>" /></td></tr>
                    <tr><td>PIC Phone</td><td><input type="text" class="input" name="picnumber" value="<?php echo"$refcode[picnumber]"; ?>" /></td></tr>
                    <tr><td>Status</td><td><select name="status"><option value="1" <?php if($refcode['publishedoid']=='1'){ echo'selected="selected"'; }?> >active</option><option value="0" <?php if($refcode['publishedoid']=='0'){ echo'selected="selected"'; }?> >not active</option></select></td></tr>
                    <tr>
                        
						<?php
					}
				}catch(PDOException $ex) {
					echo "Invalid Query";
					print($ex);
					die();
				}
                ?>
                
                    <td valign="top"><br>Apply to </td>
                    <td>
                        <br>
                        <?php
						try {
							$stmt = $db->query("SELECT * FROM `referenceoffer` WHERE `referencecode`='".$refcode['referencecode']."'");
                            $tmpoffer = array(); $tmppackage = array();
                            $tmpofferdisc = array(); $tmppackagedisc = array();
							$r_refdets = $stmt->fetchAll(PDO::FETCH_ASSOC);
							foreach($r_refdets as $r_refdet){
								if($r_refdet['type'] == 'offer'){
                                    $tmpoffer[] = $r_refdet['id']; 
                                    $tmpofferdesc[$r_refdet['id']] = $r_refdet['applydiscount']; 
                                }
							}
						}catch(PDOException $ex) {
							echo "Error on loading data";
							print($ex);
							die();
						}
                        ?>
                        <input type="checkbox" name="applytobar" value="1" <?php if($refcode['applytobar']=='1'){ echo'checked="checked"'; }?> /> &nbsp; Bar Rate
                        <br><br>
                        <b>Promotion</b>
                        <ul style="list-style:none; margin:10px; padding:0; max-height:300px; overflow:scroll">
                            <?php
							$datenow = date("Y-m-d");
							try {
								$stmt = $db->query("
								select 'promotion' as promotype, p.promotionoid, p.name, p.saleto, p.discountvalue, p.priority, pt.type, dt.labelquestion, (case when p.publishedoid = 1 and p.saleto < '".$datenow."' then 'status-red' when p.publishedoid = 1 then 'status-green' else 'status-black' end) as status from promotion p left join promotiontype pt using (promotiontypeoid) inner join discounttype dt using (discounttypeoid) inner join hotel h using (hoteloid) inner join promotionapply pa using (promotionoid) inner join roomoffer ro using (roomofferoid) inner join room r using (roomoid) inner join channel ch using (channeloid) where h.hoteloid = '".$hoteloid."' and p.publishedoid not in (3) group by p.promotionoid
								union all
								select 'package' as promotype, p.packageoid, p.name, p.saleto, 0 as discountvalue, p.priority, '' as type, '' as labelquestion, (case when p.publishedoid = 1 and p.saleto < '".$datenow."' then 'status-red' when p.publishedoid = 1 then 'status-green' else 'status-black' end) as status from package p inner join hotel h using (hoteloid) inner join packageapply pa using (packageoid) inner join roomoffer ro using (roomofferoid) inner join room r using (roomoid) inner join channel ch using (channeloid) where h.hoteloid = '".$hoteloid."' and p.publishedoid not in (3) group by p.packageoid
								");
								$r_offers = $stmt->fetchAll(PDO::FETCH_ASSOC);
								foreach($r_offers as $r_offer){
                            ?>
                            <li>
                                <input type="checkbox" name="offer-<?=$r_offer['promotionoid']?>" value="<?=$r_offer['promotionoid']?>" <?php if(in_array($r_offer['promotionoid'], $tmpoffer)){ echo'checked="checked"'; }?> /> 
                                &nbsp; <?=$r_offer['name']?> 
                                &nbsp; <span class="subcheck">
                                    <input type="checkbox" name="offerdiscount-<?=$r_offer['promotionoid']?>" value="1" <?php if(in_array($r_offer['promotionoid'], $tmpoffer) and $tmpofferdesc[$r_offer['promotionoid']]=='1'){ echo'checked="checked"'; }?> /> 
                                    &nbsp; Apply Discount
                                </span>
                            </li>
                            <?php	
								}
							}catch(PDOException $ex) {
								echo "Error on loading data";
								print($ex);
								die();
							}
                            ?>
                        </ul>
                    </td></tr>
                    <tr align="right"><td colspan="2">
                    <input type="button" name="back" value="CANCEL" onclick="document.location.href='index.php?page=refcode&hc=<?php echo"$hc"; ?>'" />
                    <input type="submit" name="continue" value="SUBMIT" /><br />
                    </td></tr>
                </table>
                </form>
			</div><!-- /.box-body -->
       </div>
    </div>
</section>