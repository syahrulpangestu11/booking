<section class="content-header">
    <h1>
       	Reference Code
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Settings</a></li>
        <li class="active">Reference Code</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="box box-form">
			<div class="box-body">
            	<input type="button" name="button" value="Add Reference Code" class="small-button blue" onclick="document.location.href='<?php echo"$base_url"; ?>/refcode/refcodeadd"><br><br />
                <table class="table promo-table table-fill">
                  <tr align="center">    
                    <td>Code</td>
                    <td>Title</td>
                    <td>Start Date</td>
                    <td>End Date</td>
                    <td>Discount</td>
                    <td>Commision</td>
                    <td>Status</td>
                    <td>Edit</td>
                  </tr>
            	<?php
                    try {
                        $stmt = $db->query("select * from referencecode left join hotel using (hoteloid) where referencecode.hoteloid='".$hoteloid."' order by hoteloid");
                        $row_count = $stmt->rowCount();
						$r_refcode = $stmt->fetchAll(PDO::FETCH_ASSOC);
						foreach($r_refcode as $row){							
							if($data%2==1){ $class="tr-1"; }else{ $class="tr-2";} $data++;
							?>
                              <tr class="<?php echo"$class"; ?>">
                                <td align="center"><?php echo"$row[referencecode]"; ?></td>
                                <td><?php echo"$row[name]"; ?></td>
                                <td align="center"><?php echo"$row[startdate]"; ?></td>
                                <td align="center"><?php echo"$row[enddate]"; ?></td>
                                <td align="center"><?php if(strpos($row['type'], 'd') === FALSE){ echo"-"; }else{ echo"$row[discount]"; } ?></td>
                                <td align="center"><?php if(strpos($row['type'], 'c') === FALSE){ echo"-"; }else{ echo"$row[commision]"; } ?></td>
                                <td align="center"><?php if($row['publishedoid']==1){ echo"active"; }else{ echo"not active"; } ?></td>
                                <td align="center"><a href="<?php echo"$base_url"; ?>/refcode/refcodeedit/?id=<?php echo"$row[referencecodeoid]"; ?>"><button type="button" class="pencil edit-button">Edit</button></a></td></tr>
                                <?php
						}
                    }catch(PDOException $ex) {
                        echo "Invalid Query";
                        print($ex);
                        die();
                    }
                ?>
				</table>
			</div><!-- /.box-body -->
       </div>
    </div>
</section>