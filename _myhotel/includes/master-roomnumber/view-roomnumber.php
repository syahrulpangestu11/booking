<?php
    $roomoffer = $_REQUEST['rt'];
    $ho = $_REQUEST['ho'];
    $page = $_REQUEST['page'];

	include("js.php");
?>
<section class="content-header">
    <h1>
       	Master Room Number
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Settings</a></li>
        <li class="active">Master Room Number</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="box">
            <div class="box-body">
                <div class="form-group">
                <form method="GET" id="data-input" action="<?php echo $base_url; ?>/master-roomnumber/">
                    <input type="hidden" name="page" value="<?php echo $page; ?>">
                    <label>Property : </label>
                    <?php
                    if($hoteloid > 0){
                        $stmt = $db->prepare("SELECT hoteloid, hotelcode, hotelname FROM hotel WHERE hoteloid=:a AND publishedoid='1' ORDER BY hotelname");
    	                $stmt->execute(array(':a' => $hoteloid));
                        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
                    }else{
                        $s_chain = "SELECT ua.oid, ua.type FROM users u INNER JOIN userassign ua USING (useroid) WHERE ua.type IN ('chainoid', 'hoteloid') AND u.useroid = '".$_SESSION['_oid']."'";
                		$q_chain = $db->query($s_chain);
                		$r_chain = $q_chain->fetchAll(PDO::FETCH_ASSOC);
                		$rxoid = array(); $rxtype = array();
                		foreach($r_chain as $row){
                			$rxoid[] = $row['oid'];
                			$rxtype[] = $row['type'];
                		}
                		
                		$rxchain = array(); $rxhotel = array();
                        foreach($rxtype as $k => $v){
                            if($v == 'chainoid'){
                                $rxchain[] = $rxoid[$k];
                            }else if($v == 'hoteloid'){
                                $rxhotel[] = $rxoid[$k];
                            }
                        }
                        
                        $msg = "";
                        if(count($rxchain) > 0){
                            $imp = implode(",", $rxchain);
                            $msg .= "AND chainoid IN (".$imp.")";
                        }
                        if(count($rxhotel) > 0){
                            $imp = implode(",", $rxhotel);
                            $msg .= "AND hoteloid IN (".$imp.")";
                        }
                		
                		if($msg != ""){
                            $stmt = $db->prepare("SELECT hoteloid, hotelcode, hotelname FROM hotel WHERE publishedoid='1' ".$msg." ORDER BY hotelname");
        	                $stmt->execute(array());
                            $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
                		}else{
                		    $data = array();
                		}
                    }
                    ?>
                    <select class="form-control" name="ho">
                        <?php
                            foreach($data as $vals){
                                if($vals['hoteloid'] == $ho){ $selected = 'selected = "selected"';  }else{ $selected = ''; }
                                echo '<option value="'.$vals['hoteloid'].'" '.$selected.'>'.$vals['hotelname'].'</option>';
                            }
                        ?>
                    </select> &nbsp;&nbsp;
                    <label>Room Type  :</label> &nbsp;&nbsp;
                    <select name="rt" class="input-select" style="max-width:200px">
                        <option dt='' value=''>-- View All --</option>
                    <?php
                        try {
                            foreach($data as $vals){
                                $xhoteloid = $vals['hoteloid'];
                                $stmt = $db->query("select r.roomoid, r.name from room r inner join hotel h using (hoteloid) where h.hoteloid = '".$xhoteloid."' and r.publishedoid = '1'");
                                $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                foreach($r_room as $row){
                                    if($row['roomoid'] == $roomoffer && $xhoteloid == $ho){ $selected = 'selected = "selected"';  }else{ $selected = ''; }
                                    echo"<option dt='".$xhoteloid."' value='".$row['roomoid']."' ".$selected." style='display:none'>".$row['name']."</option>";
                                }
                            }
                        }catch(PDOException $ex) {
                            echo "Invalid Query";
                            die();
                        }
                    
                    ?>   
                    </select>                
                    <button type="submit" class="small-button blue"><i class="fa fa-search"></i>Filter</button>
                </form>
                </div>
            </div><!-- /.box-body -->
       </div>
    </div>
    <div class="row">
        <div class="box">
        	<form method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/master-roomnumber">
            <input type="hidden" name="hoteloid" value="<?php echo $hoteloid; ?>"  />
                <div id="data-box" class="box-body">
                    <div class="loader">Loading...</div>
                </div><!-- /.box-body -->
            </form>
       </div>
    </div>

</section>
