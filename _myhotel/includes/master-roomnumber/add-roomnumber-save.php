<?php
	$hoteloid = (isset($_POST['ho'])) ? intval($_POST['ho']) : "";
	$name = (isset($_POST['name'])) ? $_POST['name'] : "";
	$rt = (isset($_POST['rt'])) ? intval($_POST['rt']) : "";
	$published = '1';
	
	if(empty(trim($name)) or empty(trim($rt))){
	    ?>
	    <script type="text/javascript">
            alert("Room Number should not be empty!"); location.href="<?=$base_url?>/master-roomnumber/add";
        </script>
	    <?php
	    die();
	}
	
	try {
	    $stmt = $db->query("select r.roomoid from room r inner join hotel h using (hoteloid) where h.hoteloid = '".$hoteloid."' and r.publishedoid = '1' and r.roomoid='".$rt."'");
        $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if(count($r_room) == 0){
    	    ?>
    	    <script type="text/javascript">
                alert("Invalid Room Type!"); location.href="<?=$base_url?>/master-roomnumber/add";
            </script>
    	    <?php
    	    die();
    	}
    	
	    $stmt = $db->query("select rn.roomnumberoid from roomnumber rn inner join room r using(roomoid) inner join hotel h using (hoteloid) where h.hoteloid = '".$hoteloid."' and r.publishedoid = '1' and r.roomoid='".$rt."' and rn.roomnumber='".$name."'");
        $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if(count($r_room) > 0){
    	    ?>
    	    <script type="text/javascript">
                alert("Room Number already exist!"); location.href="<?=$base_url?>/master-roomnumber/add";
            </script>
    	    <?php
    	    die();
    	}
	    
		$stmt = $db->prepare("INSERT INTO `roomnumber`(`roomnumberoid`, `roomoid`, `roomnumber`, `publishedoid`) VALUES (NULL,:a,:b,:c)");
		$stmt->execute(array(':a' => $rt, ':b' => $name, ':c' => $published));
	    ?>
		<script type="text/javascript">
            $(function(){ $( document ).ready(function(){ $("#dialog-success").dialog("open"); }); });
        </script>
	    <?php
	}catch(PDOException $ex) {
	    ?>
		<script type="text/javascript">
            $(function(){ $( document ).ready(function(){ $("#dialog-error").dialog("open"); }); });
        </script>
    	<?php	
	}
?>