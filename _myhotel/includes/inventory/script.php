<script type="text/javascript">
$(function(){

	var recentbox;

$(document).on('click','span.clickable', function(e){
	inputbox = $(this).next('input[type=text]');
	inputbox.show();
	$(this).hide();
});

$(document).on('change','table#inventory input[type=text]', function(e){

	var request = $(this).attr('name');
	var value = $(this).val();
	var isValid = false;

	if ($(this).hasClass("rate")) {
		var rate = parseFloat($(this).val());
		var minrate = parseFloat($(this).data("minrate"));
		if($('input[name=activerule]').prop('checked') == true && $('input[name=forcechange]').val() == "1"){
			isValid = false;
			recentbox = $(this);
			$("#notifyDR").modal();
			isValid = false;
		}else if(rate < minrate){
			alert("Minimum rate for this room type are "+minrate+".");
			$(this).css( "background-color", "#ff9f9f");
			isValid = false;
		}else{
			$(this).css( "background-color", "#ffffff");
			isValid = true;
		}
	}else{
		isValid = true;
	}

	if(isValid == true){
		changeInventory($(this), request, value, 'text');
	}
});

$('body').on('click', '#notifyDR button[name="btn-change"]', function() {
	$('input[name=forcechange]').val('0');
	recentbox.change();
	$('#notifyDR').modal('hide');
});

$('#notifyDR').on('hidden.bs.modal', function () {
	recentbox.hide();
	recentbox.prev('span').show();
})

$(document).on('click','div[node="closeout"], div[node="blackout"]', function(e){
	var request = $(this).attr('node');
	if($(this).hasClass('open')){
		var value = 'y';
	}else{
		var value = 'n';
	}
	changeInventory($(this), request, value, 'div');
});

$(document).on('click','table#inventory input[type=checkbox]', function(e){
	var request = $(this).attr('name');
	if($(this).is(':checked')){
		var value = 'y';
	}else{
		var value = 'n';
	}
	changeInventory($(this), request, value, 'div');
});

function changeInventory(elem, request, value, typeElement){
	if($('input[name=activerule]').prop('checked') == true){ var activedr = 1; }else{ var activedr = 0; }

	var parameter = elem.parent('td').attr('data-param');
	var existed = elem.parent('td').attr('data-existed');
	$.ajax({
		url: "<?php echo"$base_url"; ?>/includes/inventory/save.php",
		type: 'post',
		data: { hcode : <?=$hcode?>, parameter: parameter, existed: existed, request: request, value: value, activedr : activedr  },
		success: function(response) {
			if(response == "success"){
				if(typeElement == "text"){
					elem.hide();
					elem.prev('span').html(value);
					elem.prev('span').show();
					$('input[name=forcechange]').val('1');
				}else if(typeElement == "div"){
					if(value == "y"){
						elem.removeClass('open'); elem.addClass('close');
					}else{
						elem.removeClass('close'); elem.addClass('open');
					}
				}
				elem.parent('td').attr('data-existed', '1');
			}else if(response == "success-dynamic"){
				showInventory($('input#datepicker').val(), 'today');
			}else{
				alert(response);
			}
		}
	});
}

function showInventory(date, request){
	box = $('div#show-inventory')
	$.ajax({
		url: "<?php echo"$base_url"; ?>/includes/inventory/data.php",
		type: 'post',
		data: { lastdate: date, request: request, hcode : <?=$hcode?> },
		success: function(data) {
		    if(data.trim() != ""){
			    box.html(data);
		    }else{
		        alert("You can only look in today and after today data.");
		    }
		}
	});
}

$(document).on('change','input#datepicker', function(e){
	showInventory($(this).val(), 'today');
});


$(document).ready(function(){
	showInventory('<?=date('Y-m-d');?>', 'today');
});

$(document).on('click','button.inv-nav', function(e){
	date = $('input[name=spotdate]').val();
	request = $(this).val();
	showInventory(date, request);
});

$(document).ready(function(){
	showInventory('<?=date('Y-m-d');?>', 'today');
});

$( "#datepicker" ).datepicker({
	changeMonth: true,
	changeYear: true,
	minDate: 0
});

	$('input[name="startdate"]').datepicker({
		defaultDate: "+1w",
		changeMonth: true, changeYear:true, dateFormat: "dd MM yy",
		onClose: function( selectedDate ) {
			$('input[name="enddate"]').datepicker( "option", "minDate", selectedDate );
		}
	});
	$('input[name="enddate"]').datepicker({
			defaultDate: "+1w",
			changeMonth: true, changeYear:true, dateFormat: "dd MM yy",
			onClose: function( selectedDate ) {
				$('input[name="startdate"]').datepicker( "option", "maxDate", selectedDate );
			}
	});

	$('#recalculateDRModal').on('click','button[name="btn-change"]', function(e){
		if($('input[name="understand-changed-dr"]').prop('checked') == true){
			$.ajax({
				url: "<?php echo"$base_url"; ?>/includes/inventory/save-recalculate-dynamic-rate.php",
				type: 'post',
				data: $('#recalculateDRModal form').serialize(),
				success: function(response) {
					location.reload();
				}
			});
		}else{
			$('#recalculateDRModal .notification-changed-dr label').html('<i class="fa fa-info-circle"></i> Please click checkbox first to allow change.');
		}
	});

});
</script>

<style>
.ui-datepicker{ z-index: 1050!important; }
</style>
