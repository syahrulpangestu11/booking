<?php include('includes/bootstrap.php'); ?>
<section class="content-header">
    <h1>
        Availability
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i>  Allotments &amp; Rates </a></li>
        <li class="active">Availability</li>
    </ol>
</section>
<section class="content">
        <?php
        $stmt = $db->prepare("select dynamicrate, (select count(dynamicrateoid) as jmlrule from dynamicrate where hoteloid = hotel.hoteloid and publishedoid = '1') as jmlrule from hotel where hoteloid = :a");
        $stmt->execute(array(':a' => $hoteloid));
        $activate = $stmt->fetch(PDO::FETCH_ASSOC);
        if($activate['dynamicrate'] == "1" and $activate['jmlrule'] > 0){
        ?>
        <div class="row">
          <div class="box box-danger box-form" style="background-color: #fdd8d8; padding:1px 20px 10px; text-align:center;">
            <div id="data-box" class="box-body">
              <b>PRECAUTION! <span style="color:#0000af;">Dynamic Rule Rate</span> ACTIVATED</b>, all your rates will remain change based on rules applied! To turn off the rules temporary upon updating by click toggle button
              <label class="switch" style="margin-left: 7px;  bottom: -8px;">
                <input type="checkbox" name="activerule" <?php if($activate['dynamicrate'] == "1"){ echo "checked"; } ?>>
                <span class="slider round"></span>
              </label>
              <input type="hidden" name="forcechange" value="1">
            </div>
          </div>
        </div>

        <div class="modal fade" id="notifyDR" tabindex="-1" role="dialog" aria-labelledby="notifyDR">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-info-circle"></i> Precaution !! Dynamic Rule Rate ACTIVATED</h4>
              </div>
              <div class="modal-body">
                Dynamic Rule's was ON, your update will force the rule.<br>Are you sure to confirm the change?<br>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel Update</button>
                <button type="button" name="btn-change" class="btn btn-primary">Yes, I confirm</button>
              </div>
            </div>
          </div>
        </div>
        <?php
        }
        ?>

    <div class="row">
        <div class="box box-form">
            <div class="box-body">

<style type="text/css">
table#inventory{
	width:100%;
	border-collapse:collapse;
	font-size:0.85em;
	border-spacing:0;
}
table#inventory, table#inventory tr, table#inventory th, table#inventory td{
	border:0.2px solid #E1E1E1;
}
table#inventory thead tr th{ text-align: center;}
table#inventory thead tr th, table#inventory tbody tr td{
	padding:2px;
}
table#inventory thead tr{
	background-color:#0e1a2b;
	color:#fff;
}
table#inventory tbody tr.roomtype{
	background-color:#f4f8fb;
	font-size:1.15em;
	font-weight:bold;
}
table#inventory tbody tr.roomoffer{
	font-size:1.1em;
	font-weight:bold;
	color:#333;
}
table#inventory tr td:nth-child(n+2){
	text-align:center;
}
table#inventory tr:not(.roomtype) td:first-of-type{
	padding-left:40px;
}
table#inventory tr.roomoffer td:first-of-type{
	padding-left:15px;
}
table#inventory tr.roomtype td:nth-child(n+2), table#inventory tr.roomoffer td:nth-child(n+2){
	padding:0;
}
table#inventory tr td div.closeout{
	width:100%;
	height:100%;
	padding:0; margin:0;
	cursor:pointer;
}
table#inventory tr td div.closeout.open, table#inventory tr td div.closeout.close:hover{
	border: 1px solid #0C3;
	background-color:#0C6;
  opacity : 1!important;
}
table#inventory tr td div.closeout.close, table#inventory tr td div.closeout.open:hover{
	border: 1px solid #C00;
	background-color:#F03;
  opacity : 1!important;
}

table#inventory input[type=text]{
	display:none;
    font-size: 1em;
    padding: 2px 5px;
    max-width: 70px;
	border-radius:0;
	border: 1px solid #999;
}
input#datepicker{ width:125px; padding: 2px 5px; }

.bg-s0{
    background-color: #fdd8d8;
}
</style>
<?php include('script.php'); ?>
            	<div style="margin-bottom:15px;">
                <table width="100%">
                <tr>
                <td width="33%">
            		<?php if ($_SESSION['_typeusr'] != "6"){?>
            		<button type="button" class="inv-nav" value="bulk update" onclick="window.open('<?=$base_url?>/load-tarif', 'Bulk Update', 'width=1200, height=500, directories=no, titlebar=no, toolbar=no, location=no, status=no, menubar=no')">Bulk Update</button>
            		<?php
                  }
                  if($activate['dynamicrate'] == "1" and $activate['jmlrule'] > 0){
                ?>
                <button type="button" value="dynamicrule" data-toggle="modal" data-target="#recalculateDRModal">Recalculate Dynamic Rule</button>
                <?php
                  }
                ?>
                </td>
                <td width="33%" align="center">
            		<button type="button" class="inv-nav" value="backward"><i class="fa fa-angle-double-left"></i></button>
            		<button type="button" class="inv-nav" value="previous"><i class="fa fa-angle-left"></i></button>
                <input type="text" name="date" id="datepicker" value="<?=date('d F Y');?>" />
            		<button type="button" class="inv-nav" value="next"><i class="fa fa-angle-right"></i></button>
            		<button type="button" class="inv-nav" value="forward"><i class="fa fa-angle-double-right"></i></button>
                </td>
                <td width="33%" align="right">
            		<?php if ($_SESSION['_typeusr'] != "6"){?>
                    <a href="<?=$base_url?>/rate-control"><button type="button" class="inv-nav">Classic Rate Control</button></a>
                    <a href="<?=$base_url?>/room-control"><button type="button" class="inv-nav">Classic Room Control</button></a>
                    <?php }?>
            	  </td>
                </tr>
                </table>
                </div>
                <div id="show-inventory"></div>

            </div><!-- /.box-body -->
       </div>
    </div>
</section>

<div class="modal fade" id="recalculateDRModal" tabindex="-1" role="dialog" aria-labelledby="recalculateDRModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Recalculate Rate Baed on Dynamic Rule</h4>
      </div>
      <div class="modal-body">
        <form id="form-recalculate" class="form-horizontal" method="post">
          <input type="hidden" name="hoteloid" value="<?=$hoteloid?>">
          <div class="form-group">
            <label class="col-md-3">Start Date</label>
            <div class="col-md-6">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                <input type="text" class="form-control" name="startdate" value="<?=date('d F Y')?>">
              </div>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-3">End Date</label>
            <div class="col-md-6">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                <input type="text" class="form-control" name="enddate" value="<?=date('d F Y', strtotime("+1 month"))?>">
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-12"><label><input type="checkbox" name="understand-changed-dr" value="y"> I understand all rates will be changed based on Rules</label></div>
            <div class="col-md-12 notification-changed-dr"><label style="color:red"></label></div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" name="btn-change" class="btn btn-primary">Update Rates</button>
      </div>
    </div>
  </div>
</div>
