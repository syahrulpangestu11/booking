<button type="button" class="blue-button add">Create New Cancellation Policy</button>
<ul class="block">
	<li><span class="status-green square">sts</span> Active &nbsp;&nbsp;&nbsp; <span class="status-black square">sts</span> Inactive </li>
</ul>

<?php
	include("../../../conf/connection.php");
	
	$hoteloid = $_REQUEST['hoteloid'];
	$main_query = "select cp.*, (case when cp.publishedoid = 0 then 'status-red' when cp.publishedoid = 1 then 'status-green' else 'status-black' end) as status from cancellationpolicy cp inner join published p using (publishedoid) ";
	$filter = array();
	array_push($filter, 'cp.hoteloid = "'.$hoteloid.'"');
	if(isset($_REQUEST['name']) and $_REQUEST['name']!=''){
		array_push($filter, 'cp.name like "%'.$_REQUEST['name'].'%"');
	}
	if(isset($_REQUEST['hoteloid']) and $_REQUEST['hoteloid']!=''){
		if(empty($_REQUEST['hoteloid'])){
			$_REQUEST['hoteloid'] = 0;
		}
		array_push($filter, 'cp.hoteloid = "'.$_REQUEST['hoteloid'].'"');
	}
	array_push($filter, 'p.publishedoid not in (3)');
	if(count($filter) > 0){
		$combine_filter = implode(' and ',$filter);
		$query = $main_query.' where '.$combine_filter;
	}else{
		$query = $main_query;
	}
	try {
		$stmt = $db->query($query);
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
?>
<table class="table promo-table">
	<tr>
		<td>Name</td>
		<td>Description</td>
		<td></td>
	</tr>
<?php
			$r_promo = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_promo as $row){
?>
    <tr>
        <td class="<?php echo $row['status']; ?>"><?php echo $row['name']; ?></td>
        <td width="50%" style="text-transform:none;"><?php echo $row['description']; ?></td>
        <td class="algn-right">
        <button type="button" class="pencil edit" pid="<?php echo $row['cancellationpolicyoid']; ?>">Edit</button>
        <button type="button" class="trash delete" pid="<?php echo $row['cancellationpolicyoid']; ?>">Delete</button>
        </td>
    </tr>	
<?php				
			}
?>
</table>
<?php
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		print($ex);
		die();
	}
?>
