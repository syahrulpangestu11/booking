<?php
	$name = (isset($_POST['name'])) ? $_POST['name'] : "";
	$description = (isset($_POST['description'])) ? $_POST['description'] : "";
	$published = (isset($_POST['published'])) ? $_POST['published'] : "";
	$termcondition = $_POST['termcondition'];	
	if($termcondition == "non refundable"){
		$cancellationday = 0;
		$typecancellation = "full amount";
		$night = 0;
	}else{
		$cancellationday = $_POST['cancellationday'];
		$typecancellation = $_POST['typecancellation'];
		if($typecancellation == "night"){
			$night = $_POST['night'];
		}else{
			$night = 0;
		}
	}
		
	try {
		$stmt = $db->prepare("insert into cancellationpolicy (hoteloid, name, description, publishedoid, termcondition, cancellationday, cancellationtype, night) values (:a, :b, :c, :d, :e, :f, :g, :h)");
		$stmt->execute(array(':a' => $hoteloid,':b' => $name, ':c' => $description, ':d' => $published, ':e' => $termcondition, ':f' => $cancellationday, ':g' => $typecancellation, ':h' => $night));
		$promotionoid = $db->lastInsertId();
	}catch(PDOException $ex) {
		echo "Invalid Query";
		print($ex);
		die();
	}
	
	header("Location: ". $base_url ."/cancellationpolicy");
?>

