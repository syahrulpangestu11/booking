<?php include('includes/bootstrap.php'); ?>
<script type="text/javascript">
$(function(){

	function submitDataCallback(url, forminput){
		$.ajax({
			url: url,
			type: 'post',
			data: forminput.serialize(),
			success: function(data) {
				if(data == "1"){
					$dialogNotice.html("Data succesfully updated");
					$dialogNotice.dialog("open");
				}else{
					$dialogNotice.html("We&rsquo;re very sorry, we can't save your data right now"+data);
					$dialogNotice.dialog("open");
				}
			}
		});
	}


	var $dialogNotice = $('<div id="dialog-notice"></div>')
	.dialog({
		autoOpen: false,
		title: 'Notification',
		buttons: {
			"Ok": function(){
				$( this ).dialog( "close" );
				$(location).attr("href", "<?php echo $base_url; ?>/user");
			}
		}
	});

	var $dialogNotif = $('<div id="dialog-notif"></div>')
	.dialog({
		autoOpen: false,
		title: 'Notification',
		buttons: {
			"Ok": function(){
				$( this ).dialog("close");
			}
		}
	});

	/************************************************************************************/
	$('body').on('click','button.add-button', function(e) {
		url = "<?php echo $base_url; ?>/user/add";
      	$(location).attr("href", url);
	});

	$('body').on('click','.edit-button', function(e) {
		uo = $(this).attr("uo");
		url = "<?php echo $base_url; ?>/user/edit/?uo="+uo;
		$(location).attr("href", url);
	});

	$('body').on('click','button.cancel', function(e) {
		url = "<?php echo $base_url; ?>/user";
      	$(location).attr("href", url);
	});

	$(document).ready(function(){
		<?php if($uri2 == "user" and ($uri3 != "add" and $uri3 != "edit")){ ?>
		getLoadData();
		<?php }else if($uri2 == "user" and $uri3 == "add"){ ?>
			$('input[name="ibeuser"]').change();
			$('input[name="pmsuser"]').change();
			$('select[name="role"]').change();
			$('select[name="role_pms"]').change();
		<?php } ?>

	});

	$('body').on('click','#paging button', function(e) {
		page	= $(this).attr('page');
		pagebox = $('form#data-input').children('input[name=page]');
		pagebox.val(page);
		getLoadData();
	});

	/************************************************************************************/
	function getLoadData(){
		$.ajax({
			url: "<?php echo"$base_url"; ?>/includes/suadm/user/data.php",
			type: 'post',
			data: $('form#data-input').serialize()+"&usr=<?=$_SESSION['_typeusr']?>&useroid=<?=$_SESSION['_oid']?>",
			success: function(data) {
				$("#data-box").html(data)
			}
		});
	}

	$('body').on('change','select[name="role"]', function(e) {
		var role = $(this).val();
		if(role == '2' || role == '3' || role == '4'){
			if(role == '2'){
				url = '<?php echo $base_url; ?>/includes/suadm/user/load-city-hotel.php';
			}else if(role == '3' || role == '4'){
				url = '<?php echo $base_url; ?>/includes/suadm/user/load-chain-hotel.php';
			}
			$('div#role-box').css('display', 'block');
			$('div#role-box').html('loading ...');

			uoid = $('input[name="useroid"]');
			if(uoid.size() > 0){
				var uoid = uoid.val();
			}else{
				var uoid = "0";
			}

			$.ajax({
				url: url,
				type: 'post',
				data: { role : role, uoid : uoid, usr : '<?=$_SESSION['_typeusr']?>', useroid : '<?=$_SESSION['_oid']?>'},
				success: function(data){
					$('div#role-box').html(data);
					$('#addproperty').click();
				}
			});
		}else{
			$('div#role-box').html('<div class="text-center"><strong>Super Admin can manage all property </strong></div>');
		}
	});

	$(function(){
	   $("#dialog-error, #dialog-success").dialog({
		  autoOpen: false,
		  buttons: { "Ok": redirect },
		  close: function(event, ui) { redirect }
	   });

	   function redirect(){
		   $(location).attr("href", "<?php echo $base_url; ?>/user");
	   }
	});

	/************************************************************************************/
	$('body').on('keyup','#searchkey', function(e) {
		var key = $(this).val().toLowerCase();
		$("#listproperty li").hide();
		$("#listproperty li").each(function(index, element) {
            var str = $(this).text();
			if (str.toLowerCase().indexOf(key) >= 0)
				$(this).css("display","inline-block");
        });
	});

	$("body").on('click','#addproperty',function(e){
		$(this).hide();
		$("#saveproperty").css("display","inline-block");
		$("#searchkey").css("display","inline-block");
		$("#listproperty li").css("display","inline-block");
	});

	$("body").on('click','#saveproperty',function(e){
		$(this).hide();
		$("#addproperty").css("display","inline-block");
		$("#searchkey").val('').hide();
		$("#listproperty li").each(function(index, element) {
            var li = $(this);
			var checkbox = li.children("input");
			if(checkbox.is(':checked')){
				li.appendTo("#chosenproperty");
			}
        });
		$("#listproperty li").hide();
	});



	$.validator.addMethod('require-one', function(value) {
	    return $('.require-one:checked').size() > 0;
	}, 'Please check at least one box.');

	// var checkboxes = $('.require-one');
	// var checkbox_names = $.map(checkboxes, function(e, i) {
	//     return $(e).attr("name")
	// }).join(" ");

	$("#data-input").validate({
	    // groups: {
	    //     checks: checkbox_names
	    // },
	    errorPlacement: function(error, element) {
        // if (element.attr("type") == "checkbox") error.insertAfter(checkboxes.last());
        // else error.insertAfter(element);
				// alert(error);
				$dialogNotif.html("Please check at least one box");
				$dialogNotif.dialog("open");
	    }
	});

	/*-------------------------------------------------------------------------------------------*/

	$('input[name="ibeuser"], input[name="pmsuser"]').change(function(){
		target = $(this).attr("data-target");
		if($(this).is(':checked')){
			$('div.'+target).css('display', 'block');
			$('div.'+target+' select').prop('disabled', false);
		}else{
			$('div.'+target).css('display', 'none');
			$('div.'+target+' select').prop('disabled', 'disabled');
		}
	});

	$('select[name="role"]').change(function(){
		role = $(this).val();
		table = $('table#list-applied-property');
		btn = $('button[name="select-property"]');
		if(role == 1 || role == 2 || role == 5){
			table.find('thead').html('<tr><th>User will manage all property</th></tr>');
			btn.css('display', 'none');
		}else if(role == 3){
			table.find('thead').html('<tr><th>No</th><th>Hotel</th><th>Chain</th><th></th></tr>');
			btn.css('display', 'block');
		}else if(role == 4){
			table.find('thead').html('<tr><th>No</th><th>Chain</th><th></th></tr>');
			btn.css('display', 'block');
		}
		table.find('tbody').html('<tr style="display:none;"></tr>');
	});

	$('select[name="role_pms"]').change(function(){
		role = $(this).val();
		if($('input[name="ibeuser"]:checked').length == 0){
			table = $('table#list-applied-property');
			btn = $('button[name="select-property"]');
			if(role == 2 || role == 4 || role == 5){
				table.find('thead').html('<tr><th>No</th><th>Hotel</th><th>Chain</th><th></th></tr>');
				btn.css('display', 'block');
			}else if(role == 1 || role == 3){
				table.find('thead').html('<tr><th>No</th><th>Chain</th><th></th></tr>');
				btn.css('display', 'block');
			}
			table.find('tbody').html('<tr style="display:none;"></tr>');
		}
	});


	$('input[name="pmsuser"]').change(function(){
		if($('input[name="ibeuser"]:checked').length == 0){
			$('select[name="role_pms"]').change();
		}else{
			$('select[name="role"]').change();
		}
	});

	$('input[name="ibeuser"]').change(function(){
		if($('input[name="ibeuser"]:checked').length > 0){
			$('select[name="role"]').change();
		}else{
			$('select[name="role_pms"]').change();
		}
	});

	/*-------------------------------------------------------------------------------------------*/

	$('#selectPropertyModal').on('show.bs.modal', function (event){
		modal = $(this);
    $.post('<?php echo $base_url; ?>/includes/suadm/user/data-assigned-property.php', $('form#data-input').serialize(), function(response){
      modal.find('.modal-body').html(response);
		});
  });

	$('body').on('click','#selectPropertyModal #paging button', function(e) {
		page	= $(this).attr('page');
		$('#selectPropertyModal').find('input[name=page]').val(page);
		loadDataHotel();
	});

	$('body').on('keyup','#selectPropertyModal input[name="hotelname"]', function(e) {
		$('#selectPropertyModal').find('input[name=page]').val("1");
		loadDataHotel();
	});

	$('body').on('change','#selectPropertyModal select[name="chain"]', function(e) {
		$('#selectPropertyModal').find('input[name=page]').val("1");
		loadDataHotel();
	});

	$('body').on('keyup','#selectPropertyModal input[name="chainname"]', function(e) {
		$('#selectPropertyModal').find('input[name=page]').val("1");
		loadDataHotel();
	});

	function loadDataHotel(){
		modal.find('.data-response').html('<i class="fa fa-hourglass"></i> loading ..');
		if($('#selectPropertyModal').find('input[name=type]').val() == "hotel" ){
			$.post('<?php echo $base_url; ?>/includes/suadm/user/data-hotel-assigned.php', $('form#form-select-property').serialize(), function(response){
	      modal.find('.data-response').html(response);
			});
		}else{
			$.post('<?php echo $base_url; ?>/includes/suadm/user/data-chain-assigned.php', $('form#form-select-property').serialize(), function(response){
	      modal.find('.data-response').html(response);
			});
		}

	}

	/*-------------------------------------------------------------------------------------------*/

	$('body').on('change','#selectPropertyModal input[name="choicehotel"], #selectPropertyModal input[name="choicechain"]', function(e) {
		selected_box = $('#selectPropertyModal').find('#selected-box');
		values = $(this).val();
		name = $(this).attr('data-name');
		name_elem = $(this).attr('name');
    if(this.checked) {
			if(name_elem == "choicehotel"){
				chain = $(this).attr('data-chain');
				selected_box.append('<div class="applied" data-id="'+values+'"><input type="hidden" name="hotel[]" value="'+values+'" data-name="'+name+'"  data-chain="'+chain+'">'+name+' <i class="fa fa-window-close"></i></div>');
			}else if(name_elem == "choicechain"){
				selected_box.append('<div class="applied" data-id="'+values+'"><input type="hidden" name="chain[]" value="'+values+'" data-name="'+name+'">'+name+' <i class="fa fa-window-close"></i></div>');
			}
    }else{
      selected_box.find('div.applied[data-id="'+values+'"]').remove();
    }
		var n = $("div.applied").length;
		if(n == 0){
			selected_box.html('<span id="no-data"><i>No data applied<i></span>');
		}else{
			selected_box.find('span#no-data').remove();
		}
	});

	$('body').on('click','#selectPropertyModal div.applied', function(e) {
		values = $(this).attr('data-id');
		name_elem = $(this).find('input').attr('name');
		$(this).remove();
		if(name_elem == "hotel[]"){
			$('#selectPropertyModal').find('input[name="choicehotel"][value="'+values+'"]').prop('checked', false);
		}else if(name_elem == "chain[]"){
			$('#selectPropertyModal').find('input[name="choicechain"][value="'+values+'"]').prop('checked', false);
		}
	});

	/*-------------------------------------------------------------------------------------------*/

	$('body').on('click','#selectPropertyModal button[name="add-to-list"]', function(e){
		table = $('table#list-applied-property').find('tbody');
		$("#selectPropertyModal input[name='hotel[]']").each(function(index, element) {
			table.append('<tr><td><i>new<i></td><td><input type="hidden" name="hotel[]" value="'+$(this).val()+'">'+$(this).attr('data-name')+'</td><td>'+$(this).attr('data-chain')+'</td><td class="text-center"><a href="#" id="remove" style="color:#ec5757"><i class="fa fa-trash"></i></a></td></tr>');
    });

		$("#selectPropertyModal input[name='chain[]']").each(function(index, element) {
			table.append('<tr><td><i>new<i></td><td><input type="hidden" name="chain[]" value="'+$(this).val()+'">'+$(this).attr('data-name')+'</td><td class="text-center"><a href="#" id="remove" style="color:#ec5757"><i class="fa fa-trash"></i></a></td></tr>');
    });

		$('.modal').modal('hide');
	});

	$('body').on('click','a#remove', function(e){
		$(this).parent().parent().remove();
	});

	/*-------------------------------------------------------------------------------------------*/

});
</script>

<div style="display:none">
	<div id="dialog-error" title="Error Notification">
	  <p>We&rsquo;re very sorry, we can't save your data right now.</p>
	</div>
	<div id="dialog-success" title="Confirmation Notice">
	  <p>Data succesfully updated</p>
	</div>
</div>

<div class="modal fade" id="selectPropertyModal" tabindex="-1" role="dialog" aria-labelledby="selectPropertyModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Select Property to Managed</h4>
      </div>
      <div class="modal-body">
        loading ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" name="add-to-list">Add to list</button>
      </div>
    </div>
  </div>
</div>
