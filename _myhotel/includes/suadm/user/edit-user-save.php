<?php
	$useroid = $_POST['useroid'];
	$name = $_POST['displayname'];
	$email = $_POST['email'];
	$mobile = $_POST['mobile'];
	$reservationemail = $_POST['rsvemail'];
	if(isset($_POST['opencc'])){ $open_cc = $_POST['opencc']; }else{ $open_cc = "n"; }
	if(isset($_POST['billing'])){ $billing = $_POST['billing']; }else{ $billing = "n"; }
	$username = $_POST['username'];
	if(!empty($_POST['password'])){ $password = sha1($_POST['password']); }else{ $password = ""; }
	$role = $_POST['role'];
	$status = $_POST['status'];

	if(isset($_POST['ibeuser'])){ $ibeuser = 'y'; $role = $_POST['role']; }else{ $ibeuser = "n"; $role = 0; }
	// if(isset($_POST['pmsuser'])){ $pmsuser = 'y'; $rolepms = $_POST['role_pms']; }else{ $pmsuser = "n"; $rolepms = 0; }

	try{
		// $stmt = $db->prepare("update users set displayname =:a, email = :b, mobile =:c, status = :d, ibeuser = :e, userstypeoid = :f, pmsuser = :g, userpmstypeoid = :h, updatedby =:i, updated = :j where useroid = :id");
		// $stmt->execute(array(':a' => $name, ':b' => $email, ':c' => $mobile, ':d' => $status, ':e' => $ibeuser, ':f' => $role, ':g' => $pmsuser, ':h' => $rolepms, ':i' => $_SESSION['_user'], ':j' => date('Y-m-d H:i:s'), ':id' => $useroid));
		$stmt = $db->prepare("update users set displayname =:a, email = :b, mobile =:c, status = :d, ibeuser = :e, userstypeoid = :f, updatedby =:i, updated = :j where useroid = :id");
		$stmt->execute(array(':a' => $name, ':b' => $email, ':c' => $mobile, ':d' => $status, ':e' => $ibeuser, ':f' => $role, ':i' => $_SESSION['_user'], ':j' => date('Y-m-d H:i:s'), ':id' => $useroid));

		if(!empty($password)){
			$stmt = $db->prepare("update users set password = :a where useroid = :uoid");
			$stmt->execute(array(':a' => $password, ':uoid' => $useroid));
		}

		/*-----------------------------------------------------------------------*/
		if(isset($_POST['slccity']) and count($_POST['slccity']) > 0){
			$remaincity = "'".implode("','", $_POST['slccity'])."'";
			$stmt = $db->query("delete from userassign where useroid = '".$useroid."' and type = 'cityoid' and oid not in (".$remaincity.")");
		}else{
			$stmt = $db->query("delete from userassign where useroid = '".$useroid."' and type = 'cityoid'");
		}


		if(isset($_POST['city']) and count($_POST['city']) > 0){
			foreach($_POST['city'] as $key => $cityoid){
				$stmt = $db->prepare("insert into userassign (useroid, type, oid) values (:a, :b, :c)");
				$stmt->execute(array(':a' => $useroid, ':b' => 'cityoid', ':c' => $cityoid));
			}
		}
		/*-----------------------------------------------------------------------*/
		if(isset($_POST['slcchain']) and count($_POST['slcchain']) > 0){
			$remainchain = "'".implode("','", $_POST['slcchain'])."'";
			$stmt = $db->query("delete from userassign where useroid = '".$useroid."' and type = 'chainoid' and oid not in (".$remainchain.")");
		}else{
			$stmt = $db->query("delete from userassign where useroid = '".$useroid."' and type = 'chainoid'");
		}

		if(isset($_POST['chain']) and count($_POST['chain']) > 0){
			foreach($_POST['chain'] as $key => $chainoid){
				$stmt = $db->prepare("insert into userassign (useroid, type, oid) values (:a, :b, :c)");
				$stmt->execute(array(':a' => $useroid, ':b' => 'chainoid', ':c' => $chainoid));
			}
		}
		/*-----------------------------------------------------------------------*/
		if(isset($_POST['slchotel']) and count($_POST['slchotel']) > 0){
			$remainhotel = "'".implode("','", $_POST['slchotel'])."'";
			$stmt = $db->query("delete from userassign where useroid = '".$useroid."' and type = 'hoteloid' and oid not in (".$remainhotel.")");
		}else{
			$stmt = $db->query("delete from userassign where useroid = '".$useroid."' and type = 'hoteloid'");
		}

		if(isset($_POST['hotel']) and count($_POST['hotel']) > 0){
			foreach($_POST['hotel'] as $key => $hoteloid){
				$stmt = $db->prepare("insert into userassign (useroid, type, oid) values (:a, :b, :c)");
				$stmt->execute(array(':a' => $useroid, ':b' => 'hoteloid', ':c' => $hoteloid));
			}
		}

	?>
		<script type="text/javascript">
	    $(function(){ $( document ).ready(function(){ $("#dialog-success").dialog("open"); }); });
    </script>
	<?php
	}catch(PDOException $ex) {
	?>
		<script type="text/javascript">
      $(function(){ $( document ).ready(function(){ $("#dialog-error").dialog("open"); }); });
    </script>
	<?php
	}
?>
