<form id="form-select-property">
<input type="hidden" name="page">
<?php
  error_reporting(E_ALL ^ E_NOTICE);
  require_once("../../../conf/connection.php");



  if($_POST['role'] == '4' or (empty($_POST['role']) and ($_POST['role_pms'] == '1' or $_POST['role_pms'] == '3'))){
    ?>
  <div class="row">
    <div class="col-xs-6">
      <div class="form-group">
        <label for="exampleInputName2">Chain</label>
        <input type="text" class="form-control" name="chainname" placeholder="Chain Name"><input type="hidden" name="type" value="chain">
      </div>
    </div>
  </div>
  <div class="data-response">
<?php
    include('data-chain-assigned.php');
?>
  </div>
<?php
  }else{
?>
  <div class="row">
    <div class="col-xs-6">
      <div class="form-group">
        <label for="exampleInputName2">Hotel</label>
        <input type="text" class="form-control" name="hotelname" placeholder="Hotel Name"><input type="hidden" name="type" value="hotel">
      </div>
    </div>
    <div class="col-xs-6">
      <div class="form-group">
        <label for="exampleInputEmail2">Chain</label>
        <select class="form-control" name="chain">
          <option value="">- Show all chain -</option>
          <?php
          try {
          $stmt = $db->query("select * from chain where publishedoid = '1' order by name");
          $r_chain = $stmt->fetchAll(PDO::FETCH_ASSOC);
          foreach($r_chain as $row){ echo"<option value='".$row['chainoid']."'>".$row['name']."</option>"; }
          }catch(PDOException $ex) {
          echo "Invalid Query"; die();
          }
          ?>
        </select>
      </div>
    </div>
  </div>
  <div class="data-response">
<?php
    include('data-hotel-assigned.php');
?>
  </div>
<?php
  }
?>
  <hr>
  <div class="row">
    <div class="col-md-12"><h5>Selected property to be applied :</h5></div>
    <div class="col-md-12" id="selected-box">
      <span id="no-data"><i>No data applied<i></span>
    </div>
  </div>
</form>
<style>
#selectPropertyModal .table>tbody>tr>td, #selectPropertyModal .table>tbody>tr>th, #selectPropertyModal  .table>thead>tr>td, #selectPropertyModal  .table>thead>tr>th{ padding: 3px!important; font-size: 0.9em;}
#selectPropertyModal .row{ margin-bottom: 10px;}
#selectPropertyModal .form-group input, #selectPropertyModal .form-group select{ font-size: 0.9em; padding: 2px 5px; height: auto;}
#selectPropertyModal .form-group select{ padding: 3px 5px; }
#selectPropertyModal .applied{
  display: inline-block;
  font-size: 0.87em;
  margin: 0 3px 3px 0;
  padding: 2px 5px;
  background-color: #ffeac3;
  border: 1px solid #ffe0a6;
  letter-spacing: 1px;
  color: #b10000;
  cursor: pointer;
}
#selectPropertyModal .applied:hover{
  background-color: #fff;
}
</style>
