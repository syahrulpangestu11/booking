<?php
	error_reporting(E_ALL ^ E_NOTICE);
	include("../../../conf/connection.php");
	$role = $_POST['role'];
	$usr = $_POST['usr'];
	$xuseroid = $_POST['useroid'];

	if(isset($_POST['uoid']) and !empty($_POST['uoid'])){
		$useroid = $_POST['uoid'];
		$s_user_chain = "select oid from userassign where useroid = '".$useroid."' and type = 'chainoid'";
		$stmt = $db->query($s_user_chain);
		$countchain = $stmt->rowCount();
		if($countchain > 0){
			$user_chain = $stmt->fetchAll(PDO::FETCH_COLUMN);
		}else{
			$user_chain = array();
		}

		$s_user_hotel = "select oid from userassign where useroid = '".$useroid."' and type = 'hoteloid'";
		$stmt = $db->query($s_user_hotel);
		$counthotel = $stmt->rowCount();
		if($counthotel > 0){
			$user_hotel = $stmt->fetchAll(PDO::FETCH_COLUMN);
		}else{
			$user_hotel = array();
		}
	}

	if($role == "4"){
		try {
			$s_chain = "select c.*,  (case when p.publishedoid = 0 then 'status-red' when p.publishedoid = 1 then 'status-green' else 'status-black' end) as status  from chain c inner join published p using (publishedoid) where p.publishedoid != '3' order by c.name";
			$stmt = $db->query($s_chain);
			$row_count = $stmt->rowCount();
			if($row_count > 0){
				echo "<h3>Apply to chain :</h3>";
				echo "<ul class='inline-triple'>";
					$r_chain = $stmt->fetchAll(PDO::FETCH_ASSOC);
					foreach($r_chain as $rowc){
						if(count($user_chain) > 0 and in_array($rowc['chainoid'], $user_chain) == true){
							$nameinput = "slcchain[]";
							$checked = "checked = 'checked'";
						}else{
							$nameinput = "chain[]";
							$checked = "";
						}
						echo "<li><input type='checkbox' name='".$nameinput."' class='require-one' value='".$rowc['chainoid']."' ".$checked.">".$rowc['name']."</li>";
					}
				echo "</ul>";

			}
		}catch(PDOException $ex) {
			echo "Invalid Query";
			print($ex);
			die();
		}
	}else if($role == "3"){

	// echo "<br>";
		try {
    	    $filter_main = array();
    	
    	    if ($usr == "4"){
        	    $s_chain = "SELECT ua.oid FROM users u INNER JOIN userassign ua USING (useroid) WHERE ua.type = 'chainoid' AND u.useroid = '".$xuseroid."'";
        		// echo $s_chain;
        		$q_chain = $db->query($s_chain);
        		$n_chain = $q_chain->rowCount();
        		$r_chain = $q_chain->fetchAll(PDO::FETCH_ASSOC);
        		foreach($r_chain as $row){
        			array_push($filter_main, "h.chainoid = '".$row['oid']."'");
        		}
        	}
        	
        	if(count($filter_main) > 0){
        		$filter_main_query = "and ".implode(" and ", $filter_main);
        	}else{
        		$filter_main_query = "";
        	}
	

			$s_chain = "select h.hoteloid, h.hotelname, h.stars from hotel h inner join hoteltype ht using (hoteltypeoid) inner join published using (publishedoid) inner join city c using (cityoid) inner join state s using (stateoid) inner join country ct using (countryoid) where h.publishedoid not in (3) ".$filter_main_query." order by h.hotelname";
			$stmt = $db->query($s_chain);
			$row_count = $stmt->rowCount();
			if($row_count > 0){
				echo "<h3>Apply to property :</h3><div style='display:none'>$usr - $useroid</div>";
				echo "<ul class='inline-triple' id='chosenproperty'>";
					$r_hotel = $stmt->fetchAll(PDO::FETCH_ASSOC);
					foreach($r_hotel as $rowc){
						if(count($user_hotel) > 0 and in_array($rowc['hoteloid'], $user_hotel) == true){
							$nameinput = "slchotel[]";
							$checked = "checked = 'checked'";
							echo "
								<li>
									<input type='checkbox' name='".$nameinput."' value='".$rowc['hoteloid']."' class='require-one' ".$checked.">
									".$rowc['hotelname']."
								</li>
							";
						}else{
							$nameinput = "hotel[]";
							$checked = "";
						}
					}
				echo "</ul>";
				echo '<br/><input type="button" id="addproperty" value="More Property"> &nbsp; <input type="button" id="saveproperty" value="Add This Property" style="display:none;"> &nbsp; <input type="text" id="searchkey" placeholder="Find by hotel name..." style="width:200px;display:none;"/>';
				echo '<div id="listproperty">';
				echo "<ul class='inline-triple'>";
					foreach($r_hotel as $rowc){
						if(count($user_hotel) > 0 and in_array($rowc['hoteloid'], $user_hotel) == true){
							$nameinput = "slchotel[]";
							$checked = "checked = 'checked'";
						}else{
							$nameinput = "hotel[]";
							$checked = "";
							echo "
								<li style='display:none'>
									<input type='checkbox' name='".$nameinput."' value='".$rowc['hoteloid']."' ".$checked." class='require-one'>
									".$rowc['hotelname']."
									</li>
								";
						}
					}
				echo "</ul>";
				echo '</div>';

			}
		}catch(PDOException $ex) {
			echo "Invalid Query";
			//print($ex);
			die();
		}
	}
?>
