<?php
	session_start();
	include("../../../conf/connection.php");

	$hoteloid = $_POST['ho'];
	$name = $_POST['name'];
	$chain = $_POST['chain'];
	$star = $_POST['star'];
	$type = $_POST['type'];
	$totalroom = $_POST['totalroom'];
	$address = $_POST['address'];
	$latitude = $_POST['latitude'];
	$longitude = $_POST['longitude'];
	$phone = $_POST['phone'];
	$website = $_POST['website'];
	$email = $_POST['email'];
	$published = $_POST['published'];
	$hotelstatus = $_POST['hotelstatus'];
	$city = $_POST['city'];
	$hotelsubscribe = (empty($_POST['hotelsubscribe'])?'':$_POST['hotelsubscribe']);

	try {
		$stmt = $db->prepare("UPDATE hotel SET hotelname=:a , stars=:b , hoteltypeoid=:c , totalroom=:d ,  address=:e,  latitude=:f, longitude=:g, phone=:h, website=:i, email=:j, publishedoid=:k, cityoid=:l, chainoid = :m, hotelstatusoid = :n, updatedby =:a1, updated = :a2, hotelsubscribeoid = :a3, 
			pthotel = :pthotel,
			npwp = :npwp,
			contractnumber = :contractnumber,
			show_reminder = :show_reminder
			WHERE hoteloid=:hoid");
		$stmt->execute(array(':a' => $name, ':b' => $star, ':c' => $type, ':d' => $totalroom, ':e' => $address, ':f' => $latitude, ':g' => $longitude, ':h' => $phone, ':i' => $website, ':j' => $email, ':k' => $published, ':l' => $city, ':hoid' => $hoteloid, ':m' => $chain, ':n' => $hotelstatus, ':a1' => $_SESSION['_user'], ':a2' => date('Y-m-d H:i:s'), ':a3' => $hotelsubscribe,
		':pthotel' => $_POST['pthotel'],
		':npwp' => $_POST['npwp'],
		':contractnumber' => $_POST['contractnumber'],
		':show_reminder' => $_POST['show_reminder']
		));
		$affected_rows = $stmt->rowCount();

		/*
		* commission
		*/

		$tarr = array('base','private_sales','override');

		if(isset($_POST['comm_id']) and count($_POST['comm_id'])>0){
			foreach($_POST['comm_id'] as $key => $val){
				if(in_array($_POST['comm_type'][$key], $tarr)){
					$stmt = $db->prepare("UPDATE `hotelcommission` SET `startdate`=:b, `enddate`=:c, `value`=:d, `priority`=:e, `type`=:f, `typecomm` = :g WHERE `commissionoid` = :a");
					$stmt->execute(array(':a' => $val, ':b' => date('Y-m-d',strtotime($_POST['comm_startdate'][$key])), ':c' => date('Y-m-d',strtotime($_POST['comm_enddate'][$key])), ':d' => $_POST['comm_value'][$key], ':e' => $_POST['comm_priority'][$key], ':f' => $_POST['comm_type'][$key], ':g' => $_POST['comm_typecomm'][$key]));
				}
			}
		}

		if(isset($_POST['comm_new_startdate']) and count($_POST['comm_new_startdate'])>0){
			foreach($_POST['comm_new_startdate'] as $key => $val){
				if(in_array($_POST['comm_new_type'][$key], $tarr)){
					$stmt = $db->prepare("INSERT INTO `hotelcommission`(`hoteloid`, `startdate`, `enddate`, `value`, `priority`, `type`, `typecomm`) VALUES (:a, :b, :c, :d, :e, :f, :g)");
					$stmt->execute(array(':a' => $hoteloid, ':b' => date('Y-m-d',strtotime($_POST['comm_new_startdate'][$key])), ':c' => date('Y-m-d',strtotime($_POST['comm_new_enddate'][$key])), ':d' => $_POST['comm_new_value'][$key], ':e' => $_POST['comm_new_priority'][$key], ':f' => $_POST['comm_new_type'][$key], ':g' => $_POST['typecomm'][$key]));
				}
			}
		}



		/*
		* hotel feature
		*/
		if(isset($_POST['foid']) and count($_POST['foid']) > 0){
			$new_feature = "'".implode("','", $_POST['foid'])."'";
			$stmt = $db->prepare("delete from hotelfeature where featureoid not in (:a) and hoteloid = :b");
			$stmt->execute(array(':a' => $new_feature, ':b' => $hoteloid));

            $s_existed_hotel_feature = "SELECT GROUP_CONCAT(featureoid) as existed from `hotelfeature` where `hoteloid` = '".$hoteloid."'";
			$stmt = $db->query($s_existed_hotel_feature);
			$r_existed_hotel_feature = $stmt->fetch(PDO::FETCH_ASSOC);
			if(!empty($r_existed_hotel_feature['existed'])){
				$exist_feature = explode(',', $r_existed_hotel_feature['existed']);
			}else{
				$exist_feature = array();
			}

			foreach($_POST['foid'] as $key =>  $value){

				$featureoid = $value;
				$commission = $_POST['comm-'.$featureoid];

				if(in_array($featureoid, $exist_feature)){
					$stmt = $db->prepare("UPDATE hotelfeature SET commission = :a where hoteloid = :b and featureoid = :c");
					$stmt->execute(array(':a' => $commission, ':b' => $hoteloid, ':c' => $featureoid));
				}else{
					$stmt = $db->prepare("INSERT INTO hotelfeature (hoteloid, featureoid, commission) VALUES (:a, :b, :c)");
					$stmt->execute(array(':a' => $hoteloid, ':b' => $featureoid, ':c' => $commission));
				}
			}
		}else{
			$stmt = $db->prepare("delete from hotelfeature where hoteloid = :a");
			$stmt->execute(array(':a' => $hoteloid));
		}

		/*
		* hotel payment method
		*/
		$stmt = $db->query("select * from hotelpaymentmethod where hoteloid = '".$hoteloid."'");
		$r_methods = $stmt->fetchAll(PDO::FETCH_ASSOC); $arr_method = array();
		foreach($r_methods as $methods){
			$arr_method[] = $methods['paymentmethodoid'];
		}

		if(!isset($_POST['method'])){
			$_POST['method'] = array();
		}

		if(!isset($_POST['dokustaging'])){ $_POST['dokustaging'] = 0; }
		if(!isset($_POST['ipaystaging'])){ $_POST['ipaystaging'] = 0; }

		$stmt = $db->query("select * from paymentmethod where publishedoid = '1' and paymentmethodoid != '1'");
		$r_method = $stmt->fetchAll(PDO::FETCH_ASSOC);
		foreach($r_method as $method){
			if(in_array($method['paymentmethodoid'], $arr_method)){
				if(in_array($method['paymentmethodoid'], $_POST['method'])){
					if($method['paymentmethodoid'] == '4'){
						$stmt = $db->prepare("update hotelpaymentmethod set publishedoid='1', staging = :c, mallid = :d, sharedkey = :e, chainmerchant = :f where hoteloid = :a and paymentmethodoid = :b");
						$stmt->execute(array(':a' => $hoteloid, ':b' => $method['paymentmethodoid'], ':c' => $_POST['dokustaging'], ':d' => $_POST['mallid'], ':e' => $_POST['sharedkey'], ':f' => $_POST['chainmerchant']));
					}else if($method['paymentmethodoid'] == '5'){
						$stmt = $db->prepare("update hotelpaymentmethod set publishedoid='1', staging = :c, merchantcode = :d, merchantkey = :e where hoteloid = :a and paymentmethodoid = :b");
						$stmt->execute(array(':a' => $hoteloid, ':b' => $method['paymentmethodoid'], ':c' => $_POST['ipaystaging'], ':d' => $_POST['merchantcode'], ':e' => $_POST['merchantkey']));
					}else{
						$stmt = $db->prepare("update hotelpaymentmethod set publishedoid='1' where hoteloid = :a and paymentmethodoid = :b");
						$stmt->execute(array(':a' => $hoteloid, ':b' => $method['paymentmethodoid']));
					}
				}else{
					$stmt = $db->prepare("update hotelpaymentmethod set publishedoid='0' where hoteloid = :a and paymentmethodoid = :b");
					$stmt->execute(array(':a' => $hoteloid, ':b' => $method['paymentmethodoid']));
				}
			}else{
				if(in_array($method['paymentmethodoid'], $_POST['method'])){
					if($method['paymentmethodoid'] == '4'){
						$stmt = $db->prepare("insert into `hotelpaymentmethod`(`hoteloid`, `paymentmethodoid`, `staging`, `mallid`, `sharedkey`, `chainmerchant`, `publishedoid`) values(:a, :b, :c, :d, :e, :f, '1')");
						$stmt->execute(array(':a' => $hoteloid, ':b' => $method['paymentmethodoid'], ':c' => $_POST['dokustaging'], ':d' => $_POST['mallid'], ':e' => $_POST['sharedkey'], ':f' => $_POST['chainmerchant']));
					}else if($method['paymentmethodoid'] == '5'){
						$stmt = $db->prepare("insert into `hotelpaymentmethod`(`hoteloid`, `paymentmethodoid`, `staging`, `merchantcode`, `merchantkey`, `publishedoid`) values(:a, :b, :c, :d, :e, '1')");
						$stmt->execute(array(':a' => $hoteloid, ':b' => $method['paymentmethodoid'], ':c' => $_POST['ipaystaging'], ':d' => $_POST['merchantcode'], ':e' => $_POST['merchantkey']));
					}else{
						$stmt = $db->prepare("insert into `hotelpaymentmethod`(`hoteloid`, `paymentmethodoid`, `publishedoid`) values(:a, :b, '1')");
						$stmt->execute(array(':a' => $hoteloid, ':b' => $method['paymentmethodoid']));
					}
				}
			}
		}

	}catch(PDOException $ex) {
		echo $ex->getMessage();
		die();
	}
	echo "1";
?>
