<?php
	$hoteloid = $_GET['ho'];
	try {
		$stmt = $db->query("select * from hotel h inner join city ci using (cityoid) inner join state st using (stateoid) inner join country c using (countryoid) where h.hoteloid = '".$hoteloid."'");
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
			$r_hotel = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_hotel as $row){
				$hotelname = $row['hotelname'];
				$hoteltype = $row['hoteltypeoid'];
				$star = $row['stars'];
				$totalroom = $row['totalroom'];
				$address = $row['address'];
				$latitude = $row['latitude'];
				$longitude = $row['longitude'];
				$website = $row['website'];
				$phone = $row['phone'];
				$email = $row['email'];
				$published = $row['publishedoid'];
				$country = $row['countryoid'];
				$state = $row['stateoid'];
				$city = $row['cityoid'];
				$chain = $row['chainoid'];
				$hotelstatus = $row['hotelstatusoid'];
				$hotelsubscribe = $row['hotelsubscribeoid'];
				$show_reminder = $row['show_reminder'];

                $pthotel = $row['pthotel'];
                $npwp = $row['npwp'];
                $contractnumber = $row['contractnumber'];

				$created = $row['created'];
				$createdby = $row['createdby'];
				$updated = $row['updated'];
				$updatedby = $row['updatedby'];
			}
		}else{
			echo "No Result";
			die();
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}


?>

<style>
    span.label{color:#333;font-size:100%;}
    .form-box select.long { width: 350px; }
</style>
<section class="content-header">
    <h1>
        Hotel
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-laptop"></i> Hotel</a></li>
        <li><?php echo $hotelname; ?></li>
        <li class="active">Edit</li>
    </ol>
</section>
<section class="content">
    <form class="form-box" method="post" enctype="multipart/form-data" id="data-input" action="#">

	<div class="row">
        <div class="box box-form">
            <input type="hidden" name="ho" value="<?php echo $hoteloid; ?>">
            <input type="hidden" name="dfltstate" value="<?php echo $state; ?>">
            <input type="hidden" name="dfltcity" value="<?php echo $city; ?>">
            <h3><i class="fa fa-building-o"></i> HOTEL PROFILE</h3>
            <ul class="block">
                <li>
                    <span class="label">Property Name</span>
                    <input type="text" class="long" name="name" value="<?php echo $hotelname; ?>">
                </li>
                <li>
                    <span class="label">Chain</span>
                    <select name="chain" class="long">
                    <?php
                    try {
                    $stmt = $db->query("select * from chain where publishedoid = '1' order by name");
                    $r_chain = $stmt->fetchAll(PDO::FETCH_ASSOC);
                        foreach($r_chain as $row){
					        if($row['chainoid'] == $chain){ $selected = "selected"; }else{ $selected=""; }
					        echo"<option value='".$row['chainoid']."' ".$selected.">".$row['name']."</option>";
					    }
                    }catch(PDOException $ex) {
                    echo "Invalid Query"; die();
                    }
                    ?>
                    </select>
				</li>
                <li>
                    <span class="label">Star Rating</span>
                    <input type="number" class="small" name="star" min="0" max="7" value="<?php echo $star; ?>">
                </li>
                <li>
                    <span class="label">Property Type</span>
                    <select name="type" class="input-select">
                    <?php
                    try {
                        $stmt = $db->query("select * from hoteltype");
                        $r_hoteltype = $stmt->fetchAll(PDO::FETCH_ASSOC);
                        foreach($r_hoteltype as $row){
                            if($row['hoteltypeoid'] == $hoteltype){ $selected = "selected"; }else{ $selected=""; }
                    ?>
                        <option value="<?php echo $row['hoteltypeoid']; ?>" <?php echo $selected; ?>><?php echo $row['category']; ?></option>
                    <?php
                        }
                    }catch(PDOException $ex) {
                        echo "Invalid Query";
                        die();
                    }
                    ?>
                    </select>
                </li>
                <li>
                    <span class="label">Number of Rooms</span>
                    <input type="number" class="small" name="totalroom" min="0" value="<?php echo $totalroom; ?>">
                </li>
                <li>
                    <span class="label">PT Name</span>
                    <input type="text" class="long" name="pthotel" value="<?=$pthotel;?>">
                </li>
                <li>
                    <span class="label">N.P.W.P.</span>
                    <input type="text" class="long" name="npwp" value="<?=$npwp;?>">
                </li>
                <li>
                    <span class="label">Contract Number</span>
                    <input type="text" class="long" name="contractnumber" value="<?=$contractnumber;?>">
				</li>
            </ul>

            <h3><i class="fa fa-map-marker"></i>  HOTEL LOCATION</h3>
            <ul class="block">
                <li>
                    <span class="label">Street Address</span>
                    <textarea name="address"><?php echo $address; ?></textarea>
				</li>
                <li>
                    <span class="label float-left" style="vertical-align:middle">&nbsp;</span>
                    <div class="side-left">
                        <ul class="inline-block">
                            <li>
                            	Country<br />
                                <select name="country" class="input-select">
									<?php getCountry($country); ?>
                                </select>
                            </li>
                            <li class="loc-state"></li>
                            <li class="loc-city"></li>
                        </ul>
                    </div>
                    <div class="clear"></div>
                </li>
                <li>
                    <span class="label float-left" style="vertical-align:middle">Maps  Location</span>
                    <div class="side-left">
                        <ul class="inline-block">
                            <li>Latitude<br /><input type="text" class="medium" name="latitude" value="<?php echo $latitude; ?>"></li>
                            <li>Longitude<br /><input type="text" class="medium" name="longitude" value="<?php echo $longitude; ?>"></li>
                        </ul>
                    </div>
                    <div class="clear"></div>
				</li>
            </ul>

            <h3><i class="fa fa-phone"></i> HOTEL CONTACT</h3>
            <ul class="block">
                <li>
                    <span class="label">Main Phone Number</span>
                    <input type="text" class="medium" name="phone" value="<?php echo $phone; ?>">
				</li>
                <li>
                    <span class="label">Property Website</span>
                    <input type="text" class="long" name="website" value="<?php echo $website; ?>">
				</li>
                <li>
                    <span class="label">Property Email</span>
                    <input type="text" class="long" name="email" value="<?php echo $email; ?>">
				</li>
            </ul>

            <h3><i class="fa fa-cog"></i>  HOTEL STATUS</h3>
            <ul class="block">
                <li>
                    <span class="label"><b>IBE Status</b></span>
                    <select name="hotelstatus">
                    <?php getStatusIBE($hotelstatus); ?>
                    </select>
                </li>
                <li>
                    <span class="label"><b>Publish Property</b></span>
                    <select name="published">
                    <?php getPublished($published); ?>
                    </select>
                </li>
				<!-- <li>
                    <span class="label"><b>Subscribe Type</b></span>
                    <select name="hotelsubscribe">
                    <?php //getSubscribe($hotelsubscribe); ?>
                    </select>
                </li> -->
				<li>
                    <span class="label"><b>Show Reminder</b></span>
                    <select name="show_reminder">
                        <option value="0" <?php if($show_reminder != '1') echo 'selected';?>>Not Active</option>
                        <option value="1" <?php if($show_reminder == '1') echo 'selected';?>>Active</option>
                    </select>
                </li>
            </ul>

            <!-- <h3>HOTEL COMMISSION SCHEME</h3>
            <table class="table table-fill" id="data-commission">
            <tbody>
            <tr>
								<td>Scheme</td>
                <td>Start Date</td>
                <td>End Date</td>
                <td>Commission</td>
                <td>Priority</td>
                <td>Commission Type</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
								<td>
									<select name="newtypecomm">
										<?php //foreach($typecomm as $value){ ?>
										<option value="<?php //echo $value[0]?>"><?php //echo $value[1]?></option>
										<?php //} ?>
									</select>
								</td>
                <td><input type="text" class="medium startdate" name="newstartdate"/></td>
                <td><input type="text" class="medium enddate" name="newenddate"/></td>
                <td><input type="text" class="medium" name="newvalue"/></td>
                <td><input type="text" class="medium" name="newpriority"/></td>
                <td><select class="medium" name="newtype">
                    <option value="override">Override</option>
                    <option value="base" <?php echo $dis;?> >Base Commission</option>
                </select></td>
                <td><button type="button" class="small-button blue add-commission">Add</button></td>
            </tr>
			<?php
            // try {
            //     $stmt = $db->query("select * from hotelcommission where hoteloid = '".$hoteloid."' order by commissionoid");
            //     $r_mail = $stmt->fetchAll(PDO::FETCH_ASSOC);
            //     foreach($r_mail as $row){
            //         if($row['type']!='override' && $dis == "disabled") $xdis = "disabled";
            //             else $xdis = "";
            //         if(empty($row['startdate']) or $row['startdate']=="0000-00-00") $row['startdate'] = "";
            //             else $row['startdate'] = date('d F Y',strtotime($row['startdate']));
            //         if(empty($row['enddate']) or $row['enddate']=="0000-00-00") $row['enddate'] = "";
            //             else $row['enddate'] = date('d F Y',strtotime($row['enddate']));

            //         echo"
            //         <tr x='data'>
			// 									<td>
			// 										<select name='comm_typecomm[]'>";
			// 											foreach($typecomm as $value){
			// 												if($value[0] == $row['typecomm']){ $selected = "selected"; }else{ $selected = ""; }
			// 											echo "<option value='.$value[0].' ".$selected.">".$value[1]."</option>";
			// 											}
			// 							echo"
			// 										</select>
			// 									</td>
            //             <td>
            //                 <input type='hidden' name='comm_id[]' value='".$row['commissionoid']."' ".$xdis.">
            //                 <input type='text' class='medium startdate' name='comm_startdate[]' value='".$row['startdate']."' ".$xdis.">
            //             </td>
            //             <td><input type='text' class='medium enddate' name='comm_enddate[]' value='".$row['enddate']."' ".$xdis."></td>
            //             <td><input type='text' class='medium' name='comm_value[]' value='".$row['value']."' ".$xdis."></td>
            //             <td><input type='text' class='medium' name='comm_priority[]' value='".$row['priority']."' ".$xdis."></td>
            //             <td>";
            //         $s1='';$s2='';$s3='';
            //         if($row['type']=='override') $s1="selected";
            //         elseif($row['type']=='private_sales') $s2="selected";
            //         elseif($row['type']=='base') $s3="selected";
            //         echo'<select class="medium" name="comm_type[]" '.$xdis.'>
            //                 <option value="override" '.$s1.'>Override</option>';
            //                 if($_SESSION['_typeusr']==1 or $_SESSION['_typeusr']==2){
            //                 echo'<option value="private_sales" '.$s2.' '.$dis.'>Private Sales</option>
            //                 <option value="base" '.$s3.' '.$dis.'>Base Commission</option>';
            //                 }
            //         echo'</select>';
            //         echo"</td>
            //             <td>&nbsp;</td>
            //         </tr>
            //         ";
            //     }
            // }catch(PDOException $ex) {
            //     echo "Invalid Query";
            //     die();
            // }
            ?>
            </tbody>
            </table>

            <h3><i class="fa fa-check-square-o"></i> HOTEL FEATURE</h3>
            <table class="table table-fill">
            <tbody>
            <tr>
                <td>Subscribe</td>
                <td>Feature</td>
                <td>Commission (%)</td>
            </tr>
            <?php
            $s_existed_hotel_feature = "SELECT GROUP_CONCAT(featureoid) as existed from `hotelfeature` where `hoteloid` = '".$hoteloid."'";
			$stmt = $db->query($s_existed_hotel_feature);
			$r_existed_hotel_feature = $stmt->fetch(PDO::FETCH_ASSOC);
			if(!empty($r_existed_hotel_feature['existed'])){
				$exist_feature = explode(',', $r_existed_hotel_feature['existed']);
			}else{
				$exist_feature = array();
			}

            $s_hotel_feature = "SELECT f.* from `feature` f inner join `published` p using (`publishedoid`) where f.`publishedoid` = '1'";
			$stmt = $db->query($s_hotel_feature);
			$r_hotel_feature = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_hotel_feature as $key => $row){
				if(in_array($row['featureoid'], $exist_feature)){
					$s_commission_feature = "SELECT commission from `hotelfeature` where `hoteloid` = '".$hoteloid."' and featureoid = '".$row['featureoid']."'";
					$stmt = $db->query($s_commission_feature);
					$r_commission_feature = $stmt->fetch(PDO::FETCH_ASSOC);
					$commission = $r_commission_feature['commission'];
					$checked = 'checked = "checked"';
				}else{
					$commission = 5;
					$checked = '';
				}
			?>
            <tr class="list">
                <td><input type="checkbox" name="foid[]" value="<?=$row['featureoid']?>" <?=$checked?>/></td>
                <td><?=$row['name']?></td>
                <td><input type="number" name="comm-<?=$row['featureoid']?>" value="<?=$commission?>" min="0" /></td>
            </tr>
            <?php
			}
			?>
            </tbody>
            </table> -->

            <h3><i class="fa fa-setting"></i>  AVAILABLE PAYMENT METHODS</h3>
            <ul class="block" id="paymethod">
                <?php
                    $stmt = $db->query("select * from hotelpaymentmethod where hoteloid = '".$hoteloid."'");
                    $r_methods = $stmt->fetchAll(PDO::FETCH_ASSOC); $arr_method = array(); $methodinfo = array();
                    foreach($r_methods as $methods){
                        if($methods['publishedoid'] == '1'){ $arr_method[] = $methods['paymentmethodoid']; }
                        $methodinfo[$methods['paymentmethodoid']]['staging'] = $methods['staging'];
                        $methodinfo[$methods['paymentmethodoid']]['mallid'] = $methods['mallid'];
                        $methodinfo[$methods['paymentmethodoid']]['sharedkey'] = $methods['sharedkey'];
                        $methodinfo[$methods['paymentmethodoid']]['chainmerchant'] = $methods['chainmerchant'];
                        $methodinfo[$methods['paymentmethodoid']]['merchantcode'] = $methods['merchantcode'];
                        $methodinfo[$methods['paymentmethodoid']]['merchantkey'] = $methods['merchantkey'];
                    }

                    $stmt = $db->query("select * from paymentmethod where publishedoid = '1'");
                    $r_method = $stmt->fetchAll(PDO::FETCH_ASSOC);
                    foreach($r_method as $method){
                        if($method['paymentmethodoid'] == '1'){
                        ?>
                        <li class="checkbox">
                            &nbsp; &nbsp; <input type="checkbox" value="<?=$method['paymentmethodoid']?>" aria-label="<?=$method['method']?>" checked disabled> <?=$method['method']?> <i>*default</i>
                        </li>
                        <?php
                        }else if(in_array($method['paymentmethodoid'], $arr_method)){
                        ?>
                        <li class="checkbox">
                            &nbsp; &nbsp; <input type="checkbox" name="method[]" value="<?=$method['paymentmethodoid']?>" aria-label="<?=$method['method']?>" checked> <?=$method['method']?>
                        </li>
                        <?php
                        }else{
                        ?>
                        <li class="checkbox">
                            &nbsp; &nbsp; <input type="checkbox" name="method[]" value="<?=$method['paymentmethodoid']?>" aria-label="<?=$method['method']?>"> <?=$method['method']?>
                        </li>
                        <?php
                        }

                        if($method['paymentmethodoid'] == '4'){
                        ?>
                        <li code="<?=$method['paymentmethodoid']?>" class="div <?php if(!in_array($method['paymentmethodoid'], $arr_method)){echo 'hide';}?>">
                            <table class="table table-fill">
                            <tbody>
                            <tr>
                                <td style="width:20%">IPG</td>
                                <td style="width:20%">MallId</td>
                                <td style="width:20%">SharedKey</td>
                                <td style="width:20%">ChainMerchant</td>
                                <td style="width:20%">Production?</td>
                            </tr>
                            <tr class="list">
                                <td><?=$method['method']?></td>
                                <td><input type="text" name="mallid" class="medium" value="<?=$methodinfo[$method['paymentmethodoid']]['mallid']?>"/></td>
                                <td><input type="text" name="sharedkey" class="medium" value="<?=$methodinfo[$method['paymentmethodoid']]['sharedkey']?>"/></td>
                                <td><input type="text" name="chainmerchant" class="medium" value="<?=$methodinfo[$method['paymentmethodoid']]['chainmerchant']?>"/></td>
                                <td><input type="checkbox" name="dokustaging" value="1" <?php if($methodinfo[$method['paymentmethodoid']]['staging'] == '1'){echo 'checked';}?>/></td>
                            </tr>
                            </tbody>
                            </table>
                        </li>
                        <?php
                        }
                        if($method['paymentmethodoid'] == '5'){
                        ?>
                        <li code="<?=$method['paymentmethodoid']?>" class="div <?php if(!in_array($method['paymentmethodoid'], $arr_method)){echo 'hide';}?>">
                            <table class="table table-fill">
                            <tbody>
                            <tr>
                                <td style="width:20%">IPG</td>
                                <td style="width:20%">MerchantCode</td>
                                <td style="width:20%">MerchantKey</td>
                                <td style="width:40%">Production?</td>
                            </tr>
                            <tr class="list">
                                <td><?=$method['method']?></td>
                                <td><input type="text" name="merchantcode" class="medium" value="<?=$methodinfo[$method['paymentmethodoid']]['merchantcode']?>"/></td>
                                <td><input type="text" name="merchantkey" class="medium" value="<?=$methodinfo[$method['paymentmethodoid']]['merchantkey']?>"/></td>
                                <td><input type="checkbox" name="ipaystaging" value="1" <?php if($methodinfo[$method['paymentmethodoid']]['staging'] == '1'){echo 'checked';}?>/></td>
                            </tr>
                            </tbody>
                            </table>
                        </li>
                        <?php
                        }
                    }
                ?>
            </ul>

            <br>
            <div class="form-input">
                <button type="button" class="pure-button red cancel-edit">Cancel</button>
                <button type="button" class="submit-edit">Save</button>
            </div>
            <div class="form-input">
                <?php
                    if(!empty($created) and !empty($createdby)){
                        echo "<i class='fa fa-pencil-square-o'></i> Created on <b>".date('d/M/Y H:i', strtotime($created))."</b> <small>(GMT+8)</small> by <b>".$createdby."</b><br>";
                    }
                    if(!empty($updated) and !empty($updatedby)){
                        echo "<i class='fa fa-clock-o'></i> Last updated on <b>".date('d/M/Y H:i', strtotime($updated))."</b> <small>(GMT+8)</small> by <b>".$updatedby."</b>";
                    }
                ?>
            </div>
        </div>
    </div>

    </form>
</section>
<script>
    $(function(){
        $("#paymethod").children('.checkbox').children('input[type="checkbox"]').change(function(){
            var x = $(this).val();
            if(this.checked){
                $("#paymethod").children('.div[code="'+x+'"]').removeClass('hide');
            }else{
                $("#paymethod").children('.div[code="'+x+'"]').addClass('hide');
            }
        });
    });
</script>
