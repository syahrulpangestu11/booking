<script type="text/javascript">
$(function(){

	function submitDataCallback(url, forminput){
		$.ajax({
			url: url,
			type: 'post',
			data: forminput.serialize(),
			success: function(data) {
				if(data == "1"){
					$dialogNotice.html("Data succesfully updated");
					$dialogNotice.dialog("open");
				}else{
					$dialogNotice.html("We&rsquo;re very sorry, we can't save your data right now"+data);
					$dialogNotice.dialog("open");
				}
			}
		});
	}


	var $dialogNotice = $('<div id="dialog-notice"></div>')
	.dialog({
		autoOpen: false,
		title: 'Notification',
		buttons: {
			"Ok": function(){
				$( this ).dialog( "close" );
				<?php 
				if($_REQUEST['ref']=="contacts"){
					?>
					document.location.href = "<?php echo $base_url; ?>/contacts";					
					<?php
				}else{
					?>
					document.location.href = "<?php echo $base_url; ?>/hotel";
					<?php
				} ?>
			}
		}
	});

	/************************************************************************************/

	$('body').on('click','button.add-button', function(e) {
		url = "<?php echo $base_url; ?>/hotel/add";
      	$(location).attr("href", url);
	});

	$('body').on('click','button.edit-button', function(e) {
		hoid = $(this).attr("hoid");
		url = "<?php echo $base_url; ?>/hotel/edit/?ho="+hoid;
		$(location).attr("href", url);
	});

	$('body').on('click','button.cancel-edit', function(e) {
		url = "<?php echo $base_url; ?>/hotel";
      	$(location).attr("href", url);
	});


	$('button.submit-add').click(function(){
		url = '<?php echo $base_url; ?>/includes/suadm/hotel/add-hotel-save.php';
		forminput = $('form#data-input');
		submitDataCallback(url, forminput);
	});

	$('button.submit-edit').click(function(){
		url = '<?php echo $base_url; ?>/includes/suadm/hotel/edit-hotel-save.php';
		forminput = $('form#data-input');
		submitDataCallback(url, forminput);
	});

	$('body').on('click','button.manage', function(e) {
		var loginas_hc = $(this).attr('hoid');
		var loginas_hname = $(this).parent().parent().children('td').eq(0).html();
		$.ajax({
			type	: 'POST', cache: false,
			url		: '<?php echo"$base_url"; ?>/includes/suadm/hotel/change-session.php',
			data	: { loginas : loginas_hc, hname : loginas_hname },
			success	: function(rsp){ if(rsp == "1"){ document.location.href = '<?php echo"$base_url"; ?>/dashboard'; } }
		});
	});

	/************************************************************************************/
	$('body').on('change','select[name=country]', function(e) {
		$.get("<?php echo $base_url; ?>/includes/suadm/hotel/function-ajax.php", {
			func: "stateRqst",
			country: $('select[name=country]').val(),
			state: $('input[type=hidden][name=dfltstate]').val()
      	}, function(response){
			$('.loc-state').html(unescape(response));
			$('select[name=state]').change();
      	});
	});

	$('body').on('change','select[name=state]', function(e) {
		$.get("<?php echo $base_url; ?>/includes/suadm/hotel/function-ajax.php", {
			func: "cityRqst",
			state: $('select[name=state]').val(),
			city: $('input[type=hidden][name=dfltcity]').val()
      	}, function(response){
			$('.loc-city').html(unescape(response));
      	});
	});

	/************************************************************************************/
	$('body').on('click','button.delete-button', function(e) {
		id = $(this).attr("hoid");
		$dialogDelete.data("url", "<?php echo $base_url; ?>/includes/suadm/hotel/delete-hotel.php");
		$dialogDelete.data("id", id);
		$dialogDelete.dialog("open");
	});

	/************************************************************************************/
	$('body').on('click','#paging button', function(e) {
		page	= $(this).attr('page');
		pagebox = $('form#data-input').children('input[name=page]');
		pagebox.val(page);
		getLoadData();
	});

	/************************************************************************************/
	$('body').on('change','input[name="foid[]"]', function(e) {
		if($(this).is(':checked')){
			$(this).parent().parent().find('input[type=number]').prop('disabled', false);
		}else{
			$(this).parent().parent().find('input[type=number]').prop('disabled', true);
		}
	});

	/************************************************************************************/

	$(document).ready(function(){
		getLoadData();
		$('select[name=country]').change();
	});

	/************************************************************************************/
	function getLoadData(){
		$.ajax({
			url: "<?php echo"$base_url"; ?>/includes/suadm/hotel/data.php",
			type: 'post',
			data: $('form#data-input').serialize()+"&usr=<?=$_SESSION['_typeusr']?>&useroid=<?=$_SESSION['_oid']?>",
			success: function(data) {
				$("#data-box").html(data)
			}
		});
	}
	/************************************************************************************/
	function deleteAction(url, id){
		$.ajax({
			url: url,
			type: 'post',
			data: { id : id},
			success: function(data) {
				if(data == "1"){
					$dialogNotification.html("Data has been deleted");
					$dialogNotification.dialog("open");
				}else{
					$dialogNotification.html("We&rsquo;re very sorry, we can't save your data right now");
					$dialogNotification.dialog("open");
				}
			}
		});
	}


   var $dialogDelete = $('<div id="dialog-del"></div>')
	.html('Are you sure you want to delete this data?')
	.dialog({
		autoOpen: false,
		title: 'Confirmation',
		buttons: {
			"Delete": function(){
				var url = $(this).data('url');
				var id = $(this).data('id');
				$(this).dialog("close");
				deleteAction(url, id);
			},
			Cancel: function(){ $( this ).dialog("close"); }
		}
	});

	var $dialogNotification = $('<div id="dialog-notice"></div>')
	.dialog({
		autoOpen: false,
		title: 'Notification',
		buttons: {
			"Ok": function(){
				var callback = '<?php echo $base_url; ?>/hotel';
				$(this).dialog( "close" );
				$(location).attr("href", callback);
			}
		}
	});

	/*------------------------------------------------------------------------------*/
	$(document).on('focus',"#data-commission .startdate", function(){
		$(this).datepicker({
			//defaultDate: "+1w",
			changeMonth: true, changeYear:true,
			onClose: function( selectedDate ) {
				$( this ).parent().parent().children("td").eq(1).children("input.enddate").datepicker( "option", "minDate", selectedDate );
			}
		});
	});
	$(document).on('focus',"#data-commission .enddate", function(){
		$(this).datepicker({
			//defaultDate: "+1w",
			changeMonth: true, changeYear:true,
			onClose: function( selectedDate ) {
				$( this ).parent().parent().children("td").eq(0).children("input.startdate").datepicker( "option", "maxDate", selectedDate );
			}
		});
	});

	$('body').on('click','button.add-commission', function(e) {

		tableTarget = $('table#data-commission');
		parentTable = $(this).parent().parent().parent();

		typecomm = parentTable.find('select[name=newtypecomm]').val();
		startdate = parentTable.find('input[name=newstartdate]').val();
		enddate = parentTable.find('input[name=newenddate]').val();
		value = parentTable.find('input[name=newvalue]').val();
		priority = parentTable.find('input[name=newpriority]').val();
		type = parentTable.find('select[name=newtype]').val();
		s1='';s2='';s3='';
		if(type=='override') s1='selected';
		if(type=='private_sales') s2='selected';
		if(type=='base') s3='selected';

		tc1='';tc2='';tc3='';tc4='';
		if(typecomm=='1') tc1='selected';
		if(typecomm=='2') tc2='selected';
		if(typecomm=='3') tc3='selected';
		if(typecomm=='4') tc4='selected';

		if(startdate.trim().length !== 0 && enddate.trim().length !== 0 && value.trim().length !== 0 && priority.trim().length !== 0 && type.trim().length !== 0){

			if($('#data-commission').attr('ds')=='disabled'){external = "<option value='private_sales' "+s2+" disabled>Private Sales</option><option value='base' "+s3+" disabled>Base Commission</option>";}else{external = "<option value='private_sales' "+s2+">Private Sales</option><option value='base' "+s3+">Base Commission</option>";}

			tableTarget.append("<tr x='data'><td><select name='typecomm[]'><option value='1' "+tc1+">Commission % per materialize booking</option><option value='2' "+tc2+">Monthly Subscribe Fee</option><option value='3' "+tc3+">Commission amount per materialize booking</option><option value='4' "+tc4+">Annual Subscribe Fee</option></select></td></td><td><input type='text' class='medium startdate' name='comm_new_startdate[]' value='"+startdate+"'></td><td><input type='text' class='medium enddate' name='comm_new_enddate[]' value='"+enddate+"'></td><td><input type='text' class='medium' name='comm_new_value[]' value='"+value+"'></td><td><input type='text' class='medium' name='comm_new_priority[]' value='"+priority+"'></td><td><select class='medium' name='comm_new_type[]'><option value='override' "+s1+">Override</option>"+external+"</select></td><td>&nbsp;</td></tr>");

			$(this).parent().parent().find('input').val('');
		}
	});

});
</script>
<?php
	$typecomm = array(array(1, "Commission % per materialize booking"), array(2, "Monthly Subscribe Fee"), array(3, "Commission amount per materialize booking"), array(4, "Annual Subscribe Fee"));
?>
