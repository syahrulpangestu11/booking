<?php
	error_reporting(E_ALL & ~E_NOTICE);
	include("../../../../conf/connection.php");
?>
<table class="table table-fill table-fill-centered">
    <tr>
        <td>No.</td>
        <td>Continent Name</td>
        <td>Country Name</td>
        <td>Number of State</td>
        <td>Number of City</td>
        <td>&nbsp;</td>
    </tr>
    <tr class="autofilled">
     	<td>&nbsp;</td>
        <td>
		<select name="continent">
		<?php
            try {
                $stmt = $db->query("select * from continent");
                $r_continent = $stmt->fetchAll(PDO::FETCH_ASSOC);
                foreach($r_continent as $row){
                    echo"<option value='".$row['continentoid']."'>".$row['continentname']."</option>";
                }
            }catch(PDOException $ex) {
                echo "Invalid Query";
                die();
            }
        
        ?>        	
        </select>
        </td>
        <td><input type="text" name="country" class="medium" placeholder="Create new country"></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td><button type="button" class="blue-button add">Add</button></td>
    </tr>
<?php			
	$name = $_REQUEST['name'];
	
	$main_query = "select c.*, cnt.continentname, scount.jmlstate, ccount.jmlcity from country c inner join continent cnt using (continentoid) left join (select s.countryoid, s.stateoid, count(stateoid) as jmlstate from state s inner join country using (countryoid) group by country.countryoid) as scount on scount.countryoid = c.countryoid left join (select country.countryoid, count(cityoid) as jmlcity from city ct inner join state using (stateoid) inner join country using (countryoid) group by country.countryoid) as ccount on ccount.countryoid = c.countryoid";
	$filter = array();
	if(isset($_REQUEST['name']) and $_REQUEST['name']!=''){
		array_push($filter, 'c.countryname like "%'.$_REQUEST['name'].'%"');
	}
	if(count($filter) > 0){
		$combine_filter = implode(' and ',$filter);
		$query = $main_query.' where '.$combine_filter;
	}else{
		$query = $main_query;
	}
	try {
		$stmt = $db->query($query." order by c.countryname");
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
		$r_country = $stmt->fetchAll(PDO::FETCH_ASSOC);
			$i = 0;
			foreach($r_country as $row){
				$i++;
			?>
            <tr class="list">
            	<td><?=$i?></td>
                <td style="text-align:left"><?=$row['continentname']?></td>
                <td style="text-align:left"><?=$row['countryname']?></td>
                <td><?=(empty($row['jmlstate']))?"-":$row['jmlstate']?></td>
                <td><?=(empty($row['jmlcity']))?"-":$row['jmlcity']?></td>
                <td>&nbsp;</td>
            </tr>
            <?php
			}
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		print($ex);
		die();
	}
?>
</table>