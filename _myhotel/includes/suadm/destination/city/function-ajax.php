<?php
error_reporting(E_ALL & ~E_NOTICE);
include("../../../../conf/connection.php");

if(isset($_GET['func'])) { 
	if($_GET['func'] == "stateRqst"){
		getState($_GET['country']); 
	}else if($_GET['func'] == "cityRqst"){
		getCity($_GET['state'],$_GET['city']); 
	}
}

function getCountry($country)
{

	try {
		global $db;
		$stmt = $db->query("select * from country");
		$r_country = $stmt->fetchAll(PDO::FETCH_ASSOC);
		foreach($r_country as $row){
			if($row['countryoid'] == $country){ $selected = "selected"; }else{ $selected=""; }
	?>
		<option value="<?php echo $row['countryoid']; ?>" <?php echo $selected; ?>><?php echo $row['countryname']; ?></option>
	<?php	
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}

}


function getState($country)
{
	echo"<select name='state' class='input-select'> ";
	try {
		global $db;
		$stmt = $db->query("select s.stateoid, s.statename from state s inner join country c using (countryoid) where c.countryoid = '".$country."'");
		$r_state = $stmt->fetchAll(PDO::FETCH_ASSOC);
		foreach($r_state as $row){
			echo"<option value='".$row['stateoid']."'>".$row['statename']."</option>";
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}
	echo"</select>";
}
?>
