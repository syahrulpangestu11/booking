<button type="button" class="blue-button add">Create New POI</button>

<?php
	include("../../../conf/connection.php");
	
	$main_query = "select * FROM `poi` ";
	$filter = array();
	if(isset($_REQUEST['name']) and $_REQUEST['name']!=''){
		array_push($filter, 'title like "%'.$_REQUEST['name'].'%"');
	}
	array_push($filter, 'publishedoid not in (3)');
	if(count($filter) > 0){
		$combine_filter = implode(' and ',$filter);
		$query = $main_query.' where '.$combine_filter;
	}else{
		$query = $main_query;
	}
	try {
		$stmt = $db->query($query);
		$row_count = $stmt->rowCount();
?>
<table class="table promo-table">
    <tr>
        <th align="left" style="padding:7px;">Image</th>
        <th align="left" style="padding:7px;">POI Title</th>
        <th align="left" width="50%" style="padding:7px;text-transform:none;">Headline</th>
        <th align="left" style="padding:7px;">Status</th>
        <th class="algn-right">&nbsp;</th>
    </tr>
<?php
		if($row_count > 0) {
			$r_promo = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_promo as $row){
				$published = ($row['publishedoid']==1) ? "Active" : "Inactive" ;
				if(!empty($row['photo'])){
				    $img = '<img src="'.$row['photo'].'" style="height:75px">';
				}else{
				    $img = '-';
				}
?>
    <tr>
        <td><?php echo $img; ?></td>
        <td><?php echo $row['title']; ?></td>
        <td><?php echo $row['headline']; ?></td>
        <td><?php echo $published; ?></td>
        <td class="algn-right">
        <button type="button" class="pencil edit" pid="<?php echo $row['poioid']; ?>">Edit</button>
        <button type="button" class="trash delete" pid="<?php echo $row['poioid']; ?>">Delete</button>
        </td>
    </tr>	
<?php				
			}
		}
?>
</table>
<?php
	}catch(PDOException $ex) {
		echo "Invalid Query";
		print($ex);
		die();
	}
?>
