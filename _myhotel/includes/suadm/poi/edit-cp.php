<?php
	include('includes/bootstrap.php');
	
	$poioid = $_GET['cpoid'];
	try {
		$stmt = $db->query("select * from poi where poioid = '".$poioid."'");
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
			$r_promo = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_promo as $row){
			    $title = $row['title'];
			    $photo = $row['photo'];
			    $headline = $row['headline'];
			    $article = $row['article'];
			    $meta_title = $row['meta_title'];
			    $meta_description = $row['meta_description'];
			    $meta_keyword = $row['meta_keyword'];
			    $poicategoryoid = $row['poicategoryoid'];
			    $publishedoid = $row['publishedoid'];

			}
		}else{
			echo "No Result";
			die();
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}
?>
<style>
	span.label{ color:#333; font-size:0.9em; } 
</style>
<script type="text/javascript">
$(function() {
	$('textarea#html-box').trumbowyg({
		btns: ['viewHTML',
		  '|', 'btnGrp-design',
		  '|', 'link',
		  '|', 'btnGrp-justify',
		  '|', 'btnGrp-lists'],
		fullscreenable: false
	});
});
</script>
<section class="content-header">
    <h1>
        Edit POI Data
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i>  POI Data </a></li>
        <li class="active">Edit</li>
    </ol>
</section>
<section class="content" id="promotion">
	<div class="row">
        <form class="form-box form-horizontal form-box" method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/poi/edit-process">
        <input type="hidden" name="cpoid" value="<?=$poioid?>">
        <div class="box box-form">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-md-12">POI Title</label>
                    <div class="col-md-12"><input type="text" class="form-control" name="title" required="required" style="width:100%" value="<?=$title?>"></div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">Upload New Promotion Image</label>
                    <div class="col-md-12"><input type="file" class="form-control" name="image"><i>recommended size : 550px x 310px</i></div>
                    <div class="col-md-12 preview-image">
                    <?php if(!empty($photo)){ ?>
                        <img src="<?=$photo?>" id="preview-image" class="img-responsive" alt="Responsive image" style="width:200px;">	
                    <?php }else{ echo "N/A Image for Promotion"; } ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <label>Headline</label>
                        <textarea id="html-box" name="headline"><?=$headline?></textarea><br>
                    </div>
                    <div class="col-md-12">
                        <label>Article</label>
                        <textarea id="html-box" name="article"><?=$article?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <label>POI Category</label>
                        <select name="category" class="form-control">
                        <?php
                            try {
                                $stmt = $db->query("select * from poicategory");
                                $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                foreach($r_room as $row){
                                    if($row['poicategoryoid'] == $poicategoryoid){ $selected = "selected"; }else{ $selected=""; }
                                    echo"<option value='".$row['poicategoryoid']."' $selected>".$row['categoryname']."</option>";
                                }
                            }catch(PDOException $ex) {
                                echo "Invalid Query";
                                die();
                            }
                        ?>
                        </select><br>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="col-md-12">
                        <label>Meta Title</label><br>
                        <textarea name="metatitle" class="form-control" style="width:100%" rows="3"><?=$meta_title?></textarea><br>
                    </div>
                    <div class="col-md-12">
                        <label>Meta Description</label><br>
                        <textarea name="metadescription" class="form-control" style="width:100%" rows="3"><?=$meta_description?></textarea><br>
                    </div>
                    <div class="col-md-12">
                        <label>Meta Keyword</label><br>
                        <textarea name="metakeyword" class="form-control" style="width:100%" rows="3"><?=$meta_keyword?></textarea><br>
                    </div>
                    <div class="col-md-12">
                        <label>Area</label>
                        <select name="area[]" class="form-control" multiple rows="4">
                        <?php
                            try {
                                $area = array();
                                $stmt = $db->query("select areaoid from poiarea where poioid = '".$poioid."'");
                                $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                foreach($r_room as $row){
                                    array_push($area, $row['areaoid']);
                                }
                                
                                if(in_array('0', $area)){
                                    echo "<option value='0' selected>- All Area -</option>";
                                }else{
                                    echo "<option value='0'>- All Area -</option>";
                                }
                                
                                $stmt = $db->query("select * from state");
                                $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                foreach($r_room as $row){
                                    if(in_array($row['stateoid'], $area)){ $selected = "selected"; }else{ $selected=""; }
                                    echo"<option value='".$row['stateoid']."' $selected>".$row['statename']."</option>";
                                }
                            }catch(PDOException $ex) {
                                echo "Invalid Query";
                                die();
                            }
                        ?>
                        </select><br>
                    </div>
                    <div class="col-md-12">
                        <label>City</label>
                        <select name="city[]" class="form-control" multiple rows="4">
                        <?php
                            try {
                                $area = array();
                                $stmt = $db->query("select cityoid from poicity where poioid = '".$poioid."'");
                                $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                foreach($r_room as $row){
                                    array_push($area, $row['cityoid']);
                                }
                                
                                if(in_array('0', $area)){
                                    echo "<option value='0' selected>- All City -</option>";
                                }else{
                                    echo "<option value='0'>- All City -</option>";
                                }
                                
                                $stmt = $db->query("select * from city");
                                $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                foreach($r_room as $row){
                                    if(in_array($row['cityoid'], $area)){ $selected = "selected"; }else{ $selected=""; }
                                    echo"<option value='".$row['cityoid']."' $selected>".$row['cityname']."</option>";
                                }
                            }catch(PDOException $ex) {
                                echo "Invalid Query";
                                die();
                            }
                        ?>
                        </select><br>
                    </div>
                    <div class="col-md-12">
                        <label>Publish POI</label>
                        <select name="published" class="form-control">
                        <?php
                            try {
                                $stmt = $db->query("select * from published where showthis = 'y' ORDER BY publishedoid DESC");
                                $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                foreach($r_room as $row){
                                    if($row['publishedoid'] == $publishedoid){ $selected = "selected"; }else{ $selected=""; }
                                    echo"<option value='".$row['publishedoid']."' style='text-transform: capitalize;' $selected>".$row['note']."</option>";
                                }
                            }catch(PDOException $ex) {
                                echo "Invalid Query";
                                die();
                            }
                        ?>
                        </select><br>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
            <div class="box-footer" style="margin-top:15px">
                <div class="col-md-12 text-right">
                    <button class="btn btn-danger cancel" type="button">Cancel</button>
                    <button class="btn btn-primary" type="submit">Save Data</button>
                </div>
            </div>
		</div>
		</form>
    </div>
</section>