<?php
	$title = (isset($_POST['title'])) ? $_POST['title'] : "";
	$headline = $_POST['headline'];
	$article = $_POST['article'];
	
	$metatitle = $_POST['metatitle'];
	$metadescription = $_POST['metadescription'];
	$metakeyword = $_POST['metakeyword'];

	$allowedExtension = array('jpg', 'jpeg', 'png');
	$tmp = explode('.', $_FILES['image']['name']);
	$extension = end($tmp);
	$image = '';
	if(!empty($_FILES['image'])){
		if(in_array($extension, $allowedExtension)){
			$imagename = str_replace(' ', '_', $_FILES['image']['name']);
			$imagename = str_replace('.'.$extension, '', $imagename);
			$ext = '.'.strtolower($extension);
			$image_new_name = substr($imagename, 0, 10).date('Ymd_His').$ext;
			$folder_destination = 'pict/poi/';

			move_uploaded_file($_FILES['image']['tmp_name'], $upload_base.$folder_destination.$image_new_name);
			$image = $web_url.'/'.$folder_destination.$image_new_name;
		}else{
			echo'<script>alert("Data image not saved.\\nInvalid file.")</script>';
		}
	}
	
	$published = $_POST['published'];
	$category = $_POST['category'];

	try {
		$stmt = $db->prepare("INSERT INTO `poi`(`poioid`, `title`, `photo`, `headline`, `article`, `meta_title`, `meta_description`, `meta_keyword`, `poicategoryoid`, `publishedoid`) VALUES (NULL,:a,:b,:c,:d,:e,:f,:g,:h,:i)");
		$stmt->execute(array(
		    ':a' => $title,
		    ':b' => $image,
		    ':c' => $headline,
		    ':d' => $article,
		    ':e' => $metatitle,
		    ':f' => $metadescription,
		    ':g' => $metakeyword,
		    ':h' => $category,
		    ':i' => $published
		    ));
		$poioid = $db->lastInsertId();
	}catch(PDOException $ex) {
		echo "Invalid Query";
		print($ex);
		die();
	}
	
	foreach ($_POST['area'] as $key => $value) {
		try {
			$stmt = $db->prepare("insert into poiarea (`poioid`, `areaoid`) values (:a,:b)");
			$stmt->execute(array(':a' => $poioid, ':b' => $value));
		}catch(PDOException $ex) {
			echo "Invalid Query";
			print($ex);
			die();
		}
	    if($value == '0'){ break; }
	}
	
	foreach ($_POST['city'] as $key => $value) {
		try {
			$stmt = $db->prepare("insert into poicity (`poioid`, `cityoid`) values (:a,:b)");
			$stmt->execute(array(':a' => $poioid, ':b' => $value));
		}catch(PDOException $ex) {
			echo "Invalid Query";
			print($ex);
			die();
		}
	    if($value == '0'){ break; }
	}

	header("Location: ". $base_url ."/poi");
?>

