<?php
	include('includes/bootstrap.php');
	include('includes/promotion/geo-location/geo-location.php');

	$daylist = array("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat");
	$stmt = $db->query("select max(priority)+1 as maxpriority from promotion where hoteloid = '".$hoteloid."' and publishedoid = '1'");
	$r_max = $stmt->fetchAll(PDO::FETCH_ASSOC);
	foreach($r_max as $row){
		$maxpriority = $row['maxpriority'];
	}

	$promooid = $_GET['cpoid'];
	try {
		$stmt = $db->query("select * from promotion_template where templateoid = '".$promooid."'");
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
			$r_promo = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_promo as $row){
				$name = $row['name'];
				$promoimage = $row['promoimage'];
				$minstay = $row['minstay'];
				$maxstay = $row['maxstay'];
				$discounttype = $row['discounttypeoid'];
				$discountapply = $row['discountapplyoid'];
				$applyvalue = $row['applyvalue'];
				$discountvalue = floor($row['discountvalue']);
				$minroom = $row['minroom'];
				$published = $row['publishedoid'];
				$deposit = $row['depositoid'];
				$depositvalue = $row['depositvalue'];

				$min_ci = $row['min_bef_ci'];
				$max_ci = $row['max_bef_ci'];

				$headline = $row['headline'];
				$description = $row['description'];
				$servicefacilities = $row['servicefacilities'];
				$termcondition = $row['termcondition'];

			}
		}else{
			echo "No Result";
			die();
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}
?>
<style>
	span.label{ color:#333; font-size:0.9em; } 
</style>
<script type="text/javascript">
$(function() {
	$('textarea#html-box').trumbowyg({
		btns: ['viewHTML',
		  '|', 'btnGrp-design',
		  '|', 'link',
		  '|', 'btnGrp-justify',
		  '|', 'btnGrp-lists'],
		fullscreenable: false
	});
});
</script>
<section class="content-header">
    <h1>
        Edit Promotions Template
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i>  Promotions Template </a></li>
        <li class="active">Edit</li>
    </ol>
</section>
<section class="content" id="promotion">
	<div class="row">
        <form class="form-box form-horizontal form-box" method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/promotion-template/edit-process">
        <div class="box box-form">
            <input type="hidden" name="promooid" value="<?php echo $promooid; ?>" />
            <div class="form-group"><div class="col-md-12"><h1>PROMOTION</h1></div></div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-md-12">Promotion Name</label>
                    <div class="col-md-12"><input type="text" class="form-control" name="name" required="required" value="<?=$name?>" style="width:100%"></div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">Upload New Promotion Image</label>
                    <div class="col-md-12"><input type="file" class="form-control" name="image"><i>recommended size : 550px x 310px</i></div>
                    <div class="col-md-12 preview-image">
                    <?php if(!empty($promoimage)){ ?>
                        <img src="<?=$promoimage?>" id="preview-image" class="img-responsive" alt="Responsive image" style="width:200px;">	
                    <?php }else{ echo "N/A Image for Promotion"; } ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <label>Headline</label>
                        <textarea id="html-box" name="headline"><?=$headline?></textarea>
                    </div>
                    <div class="col-md-12">
                        <label>Promotion Description</label>
                        <textarea id="html-box" name="description"><?=$description?></textarea>
                    </div>
                    <div class="col-md-12">
                        <label>Promotion Inclusive</label>
                        <textarea id="html-box" name="servicefacilities"><?=$servicefacilities?></textarea>
                    </div>
                    <div class="col-md-12">
                        <label>Promotion Terms &amp; Condition</label>
                        <textarea id="html-box" name="termcondition"><?=$termcondition?></textarea>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <ul class="inline-triple promotion-triple">
                	<li style="width:100%">
                    	<h2>CONDITION</h2>
                    	<ul class="block">
                        	<li>
                            	<span>Minimum Stay:</span>
                                <input type="text" class="small" name="minstay" value="<?php echo $minstay; ?>">
                            </li>
                            <li>
                                <span>Guest Booking Within:</span><br />
                                <span style="vertical-align:bottom;"><input type="number" class="small" name="min_ci" value="<?php echo $min_ci; ?>" min="0" /> to <input type="number"  class="small" name="max_ci" value="<?php echo $max_ci; ?>" min="0" /> days (before check in)</span>
                            </li>
                        	<li>
                            	<span>Maximum Stay:</span>
                                <input type="text"class="small"  name="maxstay" value="<?php echo $maxstay; ?>"> [0 = N/A]
                            </li>
                        </ul>
                    </li>
                    <li style="width:100%">
                    	<h2>BENEFIT</h2>
                    	<ul class="block">
                        	<li>
                            	<span>Discount Type:</span>
    							<?php
                                    try {
                                        $stmt = $db->query("select dt.* from discounttype dt inner join published p using (publishedoid) where p.publishedoid = '1'");
                                        $r_dt = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                        foreach($r_dt as $row){
    										if($row['discounttypeoid'] == $discounttype){ $selected = "checked"; }else{ $selected=""; }
                                            echo"<br><input type='radio' name='discounttype' value='".$row['discounttypeoid']."' label='".$row['labelquestion']."' ".$selected.">  ".$row['name'];
                                        }
                                    }catch(PDOException $ex) {
                                        echo "Invalid Query";
                                        die();
                                    }
                                ?>
                            </li>
                        	<li>
                                <span><b>Discount Value:</b></span><br>
                                <input type="text" class="small"  name="discountvalue" value="<?php echo $discountvalue; ?>">
                                <span></span>
                            </li>
                        	<li class="applybox">
                            	<span>Apply On:</span>
    							<?php
                                    try {
                                        $stmt = $db->query("select da.* from discountapply da inner join published p using (publishedoid) where p.publishedoid = '1'");
                                        $r_dt = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                        foreach($r_dt as $row){
    										if($row['discountapplyoid'] == $discountapply){ $selected = "checked"; }else{ $selected=""; }
                                            echo"<br><input type='radio' name='discountapply' value='".$row['discountapplyoid']."' ".$selected.">  ".$row['name'];
    										if($row['discountapplyoid'] == '2'){
    											echo"<div class='applyval'><span class='label'>Apply on Night Number :<br></span><input type='text' class='medium'  name='applyvalue' value='".$applyvalue."'><br>
    											ex: 2,3,4 for night number 2, 3 and 4 only</div>";
    										}else if($row['discountapplyoid'] == '3'){
    											$applyvalue = explode(',',$applyvalue);
    											echo"<div class='applyval'><span class='label'>Apply on Day:</span><br>";
    											foreach($daylist as $day){
    												if(!empty($applyvalue)){
    													if(in_array($day, $applyvalue)){ $checked = "checked"; }else{ $checked = ""; }
    												}else{
    													$checked = "checked";
    												}
    												echo"<input type='checkbox' name='applyvalue[]' value='".$day."' ".$checked.">".$day."&nbsp;&nbsp;";
    											}
    											echo"</div>";
    										}
                                        }
                                    }catch(PDOException $ex) {
                                        echo "Invalid Query";
                                        die();
                                    }
                                ?>
                            </li>
                        	<li>
                            	<span>Minimum Room:</span>
                                <input type="text"class="small"  name="minroom" value="<?php echo $minroom; ?>">
                            </li>
    					</ul>
                    </li>
                </ul>
                <div style="padding-top:15px">
                    <div class="panel panel-danger">
                        <div class="panel-heading" style="color:#000">
                        <label>Deposit Type</label>
                        <div>
                        <?php
                            try {
                                $stmt = $db->query("select d.* from deposit d inner join published p using (publishedoid) where p.publishedoid = '1'");
                                $r_dt = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                foreach($r_dt as $row){
                                    if($row['depositoid'] == $deposit){ $selected = "checked"; $depositval = $depositvalue; }else{ $selected=""; $depositval = ""; }
                                    echo"<input type='radio' name='deposit' value='".$row['depositoid']."' ".$selected.">  ".$row['name'];
                                    if($row['depositoid'] == '2'){
                                        echo"<div class='depositval' style='display:none'><input type='text' class='medium'  name='depositvalue' value='".$depositval."'> Deposit for IDR Amount</div>";
                                    }else if($row['depositoid'] == '3'){
                                        echo"<div class='depositval' style='display:none'><input type='text' class='medium'  name='depositvalue' value='".$depositval."'> Deposit for %(percentage)from total</div>";
                                    }else if($row['depositoid'] == '4'){
                                        echo"<div class='depositval' style='display:none'><input type='text' class='medium'  name='depositvalue' value='".$depositval."'> night(s) amount</div>";
                                    }
                                    echo "<br>";
                                }
                            }catch(PDOException $ex) {
                                echo "Invalid Query";
                                die();
                            }
                        ?>
                        </div>
                        </div>
                    </div>
                </div>
                <ul class="inline">
                    <label>Headline Icon</label><br>
                <?php
                    try {
                        $stmt = $db->query("SELECT th.*, i.*, x.termheadlinepromooid from termheadline th left join icon i using (iconoid) left join (select termheadlinepromooid, termheadlineoid from termheadlinepromo_template thp where thp.id='".$promooid."' and thp.type='promotion') x on x.termheadlineoid = th.termheadlineoid 
                        where th.hoteloid in ('0') order by icon_title");
                        $r_headline = $stmt->fetchAll(PDO::FETCH_ASSOC);
                        foreach($r_headline as $termheadline){
                            if($termheadline['iconoid'] == 0){
                                $icon_src = $termheadline['term_icon_src'];
                            }else{
                                $icon_src = $termheadline['icon_src'];
                            }
                            if(!empty($termheadline['termheadlinepromooid'])){ $checkapply = "checked";  }else{ $checkapply = ""; }
                            ?>
                            <li class="col-md-6">
                            <input type="checkbox" name="icon[]" value="<?=$termheadline['termheadlineoid']?>" <?=$checkapply?>/>&nbsp;&nbsp;<img src="<?=$icon_src?>" class="icon" />&nbsp;&nbsp;<?=$termheadline['icon_title']?>
                            </li>
                            <?php
                        }
                    }catch(PDOException $ex) {
                        echo "Invalid Query";
                        die();
                    }
                ?>
                </ul>
                <div class="clear"></div>
                <div style="padding-top:15px">
                    <label>Publish Promotion</label>
                    <select name="published" class="form-control">
                    <?php
                        try {
                            $stmt = $db->query("select * from published where showthis = 'y'");
                            $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                            foreach($r_room as $row){
                                if($row['publishedoid'] == $published){ $selected = "selected"; }else{ $selected=""; }
                                echo"<option value='".$row['publishedoid']."' ".$selected.">".$row['note']."</option>";
                            }
                        }catch(PDOException $ex) {
                            echo "Invalid Query";
                            die();
                        }
                    ?>
                    </select>
                </div>
            </div>
            <div class="clear"></div>
            <div class="box-footer" style="margin-top:15px">
                <div class="col-md-12 text-right">
                    <button class="btn btn-danger cancel" type="button">Cancel</button>
                    <button class="btn btn-primary" type="submit">Save Template</button>
                </div>
            </div>
		</div>
		</form>
    </div>
</section>