<style>
.border-bottom{
  margin-bottom: 5px;
  padding-bottom: 5px;
  border-bottom: 1px solid #ddd;
}
#inputs-wrapper .row {padding-bottom: 10px;}
#inputs-wrapper .row > div:first-of-type {line-height: 34px;}

.inline-block{
  display: inline-block !important;
  /* vertical-align: bottom; */
}
</style>

<?php
  // echo '<pre>';
  // print_r($_SESSION);
  // echo '</pre>';
  include('includes/bootstrap.php');
  $sInv = "SELECT i.*, ist.status FROM invoice i INNER JOIN invoicestatus ist USING (invoicestatusoid) WHERE i.invoiceoid = :a";
  $stmt = $db->prepare($sInv);
  $stmt->execute(array(':a' => $_GET['io']));
  $inv = $stmt->fetch(PDO::FETCH_ASSOC);
  $monthyear = date('F', mktime(0, 0, 0, $inv['month'], 10))." ".$inv['year'];
  $periode1 = date("Y-m-d", strtotime($inv['year'].'-'.$inv['month'].'-01'));
  $periode2 = date("Y-m-t",strtotime($periode1));
  $newInvoiceOID = ($inv['invoicestatusoid']==1) ? $inv['invoicestatusoid']+2 : $inv['invoicestatusoid']+1 ;

  $sHotel = "SELECT h.pthotel, h.npwp, h.contractnumber FROM hotel h WHERE h.hoteloid = :a";
  $stmt = $db->prepare($sHotel);
  $stmt->execute(array(':a' => $inv['hoteloid']));
  $hotel = $stmt->fetch(PDO::FETCH_ASSOC);
  $pthotel = !empty($inv['pthotel']) ? $inv['pthotel'] : $hotel['pthotel'];
  $npwp = !empty($inv['npwp']) ? $inv['npwp'] : $hotel['npwp'];
  $contractnumber = !empty($inv['contractnumber']) ? $inv['contractnumber'] : $hotel['contractnumber'];
  $ppn = !empty($inv['ppn']) ? $inv['ppn'] : 10;
  $reconciledamount = !empty($inv['reconciledamount']) ? round($inv['reconciledamount']) : round($inv['reconcileamount']);
  $paid = !empty($inv['paid']) ? round($inv['paid']) : round($inv['invoiced']) ;

  $suffixINV = '/TB/'.date('m', mktime(0, 0, 0, $inv['month'], 10)).'/'.$inv['year'];
  $sInvThisMonth = "SELECT invoiceoid FROM invoice i WHERE i.invoicenumber LIKE '%".$suffixINV."%' ";
  $stmt = $db->prepare($sInvThisMonth);
  $stmt->execute();
  $jml = $stmt->rowCount();
  $nomorUrut = $jml+1;
	$nomorUrutINV = str_pad($nomorUrut, 3, "0", STR_PAD_LEFT);
  $invoicenumber = !empty($inv['invoicenumber']) ? $inv['invoicenumber'] : $nomorUrutINV.$suffixINV;

  $sUser = "SELECT u.* FROM users u WHERE u.useroid = :a";
  $stmt = $db->prepare($sUser);
  $stmt->execute(array(':a' => $_SESSION['_oid']));
  $user = $stmt->fetch(PDO::FETCH_ASSOC);

  function showAtStatus($iso, $current_iso){
    if(is_array($iso)){
      if(!in_array($current_iso,$iso)){
        return "hidden";
      }else{
        return "";
      }
    }else{
      if($current_iso != $iso){
        return "hidden";
      }else{
        return "";
      }
    }
  }
  switch ($inv['invoicestatusoid']) {
    case 1:
      $mailContent = '
        Dear  '.$inv['hotelname'].',
        <br><br>
        Warmest Greetings from The Buking Team.
        <br><br>
        Herewith I attached '.$monthyear.' commission report for us to reconcile. 
        <br>
        Please confirm back to me until 3 days after received this email, whether the amount is correct or need any amendment. 
        <br>
        Once it is confirmed or no confirmation between us at the specified time, we can proceed to the invoice and payment. 
        <br><br>
        Should you need any assistance, please do not hesitate to contact me directly. Thank you for your assistance and cooperation.  
        <br><br><br>
        Best Regards, 
        <br><br>
        '.$user['displayname'].'<br>
        The Büking Sales & Account Executive<br>
        M: '.$user['mobile'].'
        <br><br>
        PT Wesolve Solusi Indonesia<br>
        Cervino Village 27th Floor, Jl. KH. Abdullah Syafi’ie Kav.27<br>
        Casablanca – Jakarta 12810, Indonesia<br>
        phone: +62 21 8068 3025 / +62 822 4495 8558<br>
        <br><br>
        <i style="color: #aaa;">
        This e-mail and any information contained are confidential and legally privileged. It is intended solely for the use of the individual or entity to whom it is addressed and others authorized to receive it. If you are not the intended recipient, you are hereby notified that any disclosure, copying, distribution or taking any action in reliance on the contents of this e-mail is strictly prohibited and may be unlawful. If you have received this e-mail in error, please notify us immediately by responding to this e-mail or by telephone to PT Wesolve Solusi Indonesia +62 21 8068 3025 then delete this email including any attachment(s) from your system. PT Wesolve Solusi Indonesia does not accept liability for damage caused by any of the foregoing. This e-mail is PT Wesolve Solusi Indonesia, having Registered Address at Cervino Village 27 Floor, Jl. KH. Abdullah Syafi’ie Kav.27, Casablanca – Jakarta 12810, Indonesia
        </i>
        ';
      break;
    case 4:
      $mailContent = '
        Dear  '.$inv['hotelname'].',
        <br><br>
        Warmest Greeting from The Buking…
        <br><br>
        Herewith I attached '.$monthyear.' proforma invoice of '.$inv['hotelname'].'. 
        <br>
        Please confirm back to us if your property has been paid and please attached the payment received
        <br>
        Should you need any assistance, please do not hesitate to contact me directly.
        <br><br>
        Thank you for your attention and corporation, Should you have any further question, please do not hesitate to contact us back. Have a great day.
        <br><br><br>
        Best Regards, 
        <br><br>
        '.$user['displayname'].'<br>
        The Büking Sales & Account Executive<br>
        M: '.$user['mobile'].'
        <br><br>
        PT Wesolve Solusi Indonesia<br>
        Cervino Village 27th Floor, Jl. KH. Abdullah Syafi’ie Kav.27<br>
        Casablanca – Jakarta 12810, Indonesia<br>
        phone: +62 21 8068 3025 / +62 822 4495 8558<br>
        <br><br>
        <i style="color: #aaa;">
        This e-mail and any information contained are confidential and legally privileged. It is intended solely for the use of the individual or entity to whom it is addressed and others authorized to receive it. If you are not the intended recipient, you are hereby notified that any disclosure, copying, distribution or taking any action in reliance on the contents of this e-mail is strictly prohibited and may be unlawful. If you have received this e-mail in error, please notify us immediately by responding to this e-mail or by telephone to PT Wesolve Solusi Indonesia +62 21 8068 3025 then delete this email including any attachment(s) from your system. PT Wesolve Solusi Indonesia does not accept liability for damage caused by any of the foregoing. This e-mail is PT Wesolve Solusi Indonesia, having Registered Address at Cervino Village 27 Floor, Jl. KH. Abdullah Syafi’ie Kav.27, Casablanca – Jakarta 12810, Indonesia
        </i>
        ';
      break;
      case 6:
      $mailContent = '
        Dear  '.$inv['hotelname'].',
        <br><br>
        Warmest Greeting from The Buking…
        <br><br>
        Herewith I attached '.$monthyear.' invoice & tax invoice of '.$inv['hotelname'].'. 
        <br>
        Should you need any assistance, please do not hesitate to contact me directly.
        <br><br>
        Thank you for your attention and corporation, Should you have any further question, please do not hesitate to contact us back. Have a great day.
        <br><br><br>
        Best Regards, 
        <br><br>
        '.$user['displayname'].'<br>
        The Büking Sales & Account Executive<br>
        M: '.$user['mobile'].'
        <br><br>
        PT Wesolve Solusi Indonesia<br>
        Cervino Village 27th Floor, Jl. KH. Abdullah Syafi’ie Kav.27<br>
        Casablanca – Jakarta 12810, Indonesia<br>
        phone: +62 21 8068 3025 / +62 822 4495 8558<br>
        <br><br>
        <i style="color: #aaa;">
        This e-mail and any information contained are confidential and legally privileged. It is intended solely for the use of the individual or entity to whom it is addressed and others authorized to receive it. If you are not the intended recipient, you are hereby notified that any disclosure, copying, distribution or taking any action in reliance on the contents of this e-mail is strictly prohibited and may be unlawful. If you have received this e-mail in error, please notify us immediately by responding to this e-mail or by telephone to PT Wesolve Solusi Indonesia +62 21 8068 3025 then delete this email including any attachment(s) from your system. PT Wesolve Solusi Indonesia does not accept liability for damage caused by any of the foregoing. This e-mail is PT Wesolve Solusi Indonesia, having Registered Address at Cervino Village 27 Floor, Jl. KH. Abdullah Syafi’ie Kav.27, Casablanca – Jakarta 12810, Indonesia
        </i>
        ';
      break;
    default:
      $mailContent = '';
      break;
  }
?>
<section class="content-header">
  <h1>Invoicing Action</h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-table"></i> Invoicing</a></li>
    <li class="active">Action</li>
  </ol>
</section>
<section class="content">
  <form method="post" enctype="multipart/form-data" class="form-inline" action="<?=$base_url;?>/invoicing/action-save">
  <div class="box box-warning">
    <div class="box-body">
      <div class="text-center">
        <h1><?=$inv['hotelname'];?></h1>
        <b><?=$monthyear;?></b>
      </div>
      <br>
      <div class="col-md-3">
        <div class="text-center">
          <b>This Month Summary</b>
        </div>
        <br>
        <div class="border-bottom">
          <div class="col-md-7">Total Booking</div>
          <div class="col-md-5 text-right"><?=number_format($inv['totalbooking']);?></div>
          <div class="clear"></div>
        </div>
        <div class="border-bottom">
          <div class="col-md-7">Room Night</div>
          <div class="col-md-5 text-right"><?=number_format($inv['roomnight']);?></div>
          <div class="clear"></div>
        </div>
        <div class="border-bottom">
          <div class="col-md-7">Confirmed Booking</div>
          <div class="col-md-5 text-right"><?=number_format($inv['confirmedbooking']);?></div>
          <div class="clear"></div>
        </div>
        <div class="border-bottom">
          <div class="col-md-7">Penalty Cancelled</div>
          <div class="col-md-5 text-right"><?=number_format($inv['penaltycancelled']);?></div>
          <div class="clear"></div>
        </div>
        <div class="border-bottom">
          <div class="col-md-7">Total Revenue</div>
          <div class="col-md-5 text-right"><?=number_format($inv['totalrevenue']);?></div>
          <div class="clear"></div>
        </div>
        <div class="">
          <div class="col-md-7">Reconcile Amount</div>
          <div class="col-md-5 text-right"><?=number_format($inv['reconcileamount']);?></div>
          <div class="clear"></div>
        </div>
      </div>
      <div class="col-md-9" id="inputs-wrapper">
        <input type="hidden" name="invoiceoid" value="<?=$inv['invoiceoid'];?>">
        <input type="hidden" name="newinvoicestatusoid" value="<?=$newInvoiceOID;?>">
        <div class="row">
          <div class="col-md-2">Status</div>
          <div class="col-md-10" style="line-height: 34px; text-transform: capitalize;"> : <b><?=$inv['status'];?></b></div>
          <div class="clear"></div>
        </div>

        <!-- Attention PIC -->
        <div class="row <?=showAtStatus([1,4,6],$inv['invoicestatusoid']);?>">
          <div class="col-md-2">Attn PIC</div>
          <div class="col-md-10"> :
            <?php
            $s_mt = "SELECT * from mailertype WHERE mailertypeoid NOT IN (1,6)";
            $q_mt = $db->query($s_mt);
            $reservationemail = $q_mt->fetchAll(PDO::FETCH_ASSOC);
            ?>

            <select name="hotelcontactoid" id="selectHCO" class="form-control" style="width: 200px">
              <option value="">- Please Select -</option>
              <?php 
              $sHC = "SELECT hc.* from hotelcontact hc where hc.publishedoid not in ('3') and hc.hoteloid = :a ORDER BY hc.name";
              $stmt = $db->prepare($sHC);
              $stmt->execute(array(':a' => $inv['hoteloid']));
              foreach($stmt->fetchAll(PDO::FETCH_ASSOC) as $rHC){
                // if($rHC['hotelcontactoid'] == $inv['hotelcontactoid']){ $selected = "selected"; }else{ $selected = ""; }
                echo '<option value="'.$rHC['hotelcontactoid'].'" email="'.$rHC['email'].'" name="'.$rHC['name'].'" '.$selected.'>';
                echo $rHC['name'].' - '.$rHC['email'];
                echo '</option>';
              }
              ?>
              <option value="custom"> + Add Recipient</option>
            </select>
            <input type="text" name="custom_name" id="customName" placeholder="Type Name">
            <input type="email" name="custom_email" id="customEmail" placeholder="Type Email">
            <select name="reservationemail" id="selectMTO" class="form-control">
              <?php
              foreach ($reservationemail as $value) {
                  echo"<option value='".$value['mailertypeoid']."' mt='".$value['mailertype']."' > ".$value['mailertype']."</option>";
              }

            ?>
            </select>
            <button type="button" id="pushAttn" class="btn btn-warning">
              <i class="fa fa-plus"></i>
            </button>
            <textarea name="attns" id="attns" cols="30" rows="10" class="hidden"></textarea>
            <input type="text" name="attentionpic" class="pdf-params hidden" id="attentionpic" value="">
            <div id="labelViewAttns"><br>&nbsp;&nbsp;<b>Current Recipients :</b></div>
            <ul id="viewAttns" style="">
            </ul>
            &nbsp;&nbsp;<a id="resetAttns" class="btn btn-danger">Reset</a>
          </div>
        </div>
        <!-- <div class="row <?=showAtStatus(0,$inv['invoicestatusoid']);?>">
          <div class="col-md-2">Attention PIC</div>
          <div class="col-md-10"> : <input type="text" name="attentionpic" class="pdf-params" id="attentionpic" value="<?=$inv['attentionpic'];?>"></div>
          <div class="clear"></div>
        </div>
        <div class="row <?=showAtStatus(0,$inv['invoicestatusoid']);?>">
          <div class="col-md-2">Attention PIC Email</div>
          <div class="col-md-10"> : <input type="text" name="attentionpicemail" id="attentionpicemail" value="<?=$inv['attentionpicemail'];?>"></div>
          <div class="clear"></div>
        </div> -->

        <!-- Stage 1 -->
        <div class="row <?=showAtStatus(1,$inv['invoicestatusoid']);?>">
          <div class="col-md-2">Commission Report</div>
          <div class="col-md-10"> : <a class="btn btn-success" href="<?=$base_url;?>/request/export/export-commission-report-detail.php?hoteloid=<?=$inv['hoteloid'];?>&startdate=<?=$periode1;?>&enddate=<?=$periode2;?>"><i class="fa fa-file-excel-o"></i> Generate XLS</a> 
          <input type="file" name="filecommissionreportdetail" class="inline-block"  id="filecommissionreportdetail" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"></div>
          <div class="clear"></div>
        </div>

        <!-- Stage 2 -->
        <div class="row <?=showAtStatus(3,$inv['invoicestatusoid']);?>">
          <div class="col-md-2">Reconciled Hotel</div>
          <div class="col-md-10"> : <input type="text" name="reconciledamount" value="<?=$reconciledamount;?>" class="numeric" ></div>
          <div class="clear"></div>
        </div>

        <!-- Stage 3 -->
        <div class="row <?=showAtStatus(4,$inv['invoicestatusoid']);?>">
          <div class="col-md-2">PT Hotel</div>
          <div class="col-md-10"> : <input type="text" name="pthotel" class="pdf-params" value="<?=$pthotel;?>"></div>
          <div class="clear"></div>
        </div>
        <div class="row <?=showAtStatus(4,$inv['invoicestatusoid']);?>">
          <div class="col-md-2">NPWP</div>
          <div class="col-md-10"> : <input type="text" name="npwp" class="pdf-params" value="<?=$npwp;?>"></div>
          <div class="clear"></div>
        </div>
        <div class="row <?=showAtStatus(4,$inv['invoicestatusoid']);?>">
          <div class="col-md-2">Invoice No.</div>
          <div class="col-md-10"> : <input type="text" name="invoicenumber" class="pdf-params" value="<?=$invoicenumber;?>"></div>
          <div class="clear"></div>
        </div>
        <div class="row <?=showAtStatus(4,$inv['invoicestatusoid']);?>">
          <div class="col-md-2">Contract No.</div>
          <div class="col-md-10"> : <input type="text" name="contractnumber" class="pdf-params" value="<?=$contractnumber;?>"></div>
          <div class="clear"></div>
        </div>
        <div class="row <?=showAtStatus(4,$inv['invoicestatusoid']);?>">
          <div class="col-md-2">Invoiced Amount</div>
          <div class="col-md-10"> : <!-- &nbsp; <b><?=number_format($inv['reconciledamount']);?></b> --> <input type="number" id="reconciledamount_temp" class="invoiced-trigger pdf-params" name="reconciledamount_temp" value="<?=$inv['reconciledamount'];?>"> &nbsp; + &nbsp; PPN  &nbsp; <input type="number" id="ppn" class="invoiced-trigger pdf-params" name="ppn" style="width: 10%;" value="<?=$ppn;?>"> &nbsp; %  &nbsp; =  &nbsp; <input type="text" id="invoiced" name="invoiced" class="pdf-params numeric" value="<?=$inv['invoiced'];?>"></div>
          <div class="clear"></div>
        </div>
        <div class="row <?=showAtStatus(4,$inv['invoicestatusoid']);?>">
          <div class="col-md-2">Description</div>
          <div class="col-md-10"> : 
            <!-- <textarea name="invoicedescription" value="<?=$inv['invoicedescription'];?>"></textarea> -->
            <input type="text" style="width: 62%;" name="invoicedescription" class="pdf-params" value="<?=$inv['invoicedescription'];?>">
          </div>
          <div class="clear"></div>
        </div>
        <div class="row <?=showAtStatus(4,$inv['invoicestatusoid']);?>">
          <div class="col-md-2">Proforma Invoice</div>
          <div class="col-md-10"> : <a class="btn btn-danger" id="generatePDF" io="<?=$inv['invoiceoid'];?>" target="_blank"><i class="fa fa-file-pdf-o"></i> Generate Invoice</a> 
          <input type="file" name="fileproformainvoice" class="inline-block"  id="fileproformainvoice" accept=".pdf"></div>
          <div class="clear"></div>
        </div>

        <!-- Stage 4 -->
        <div class="row <?=showAtStatus(5,$inv['invoicestatusoid']);?>">
          <div class="col-md-2">Paid</div>
          <div class="col-md-10"> : <input type="text" name="paid" class="numeric" value="<?=$paid;?>"></div>
          <div class="clear"></div>
        </div>

        <!-- Stage 5 -->
        <div class="row <?=showAtStatus(6,$inv['invoicestatusoid']);?>">
          <div class="col-md-2">No Faktur Pajak</div>
          <div class="col-md-10"> : <input type="text" name="nofakturpajak" value="<?=$inv['nofakturpajak'];?>"></div>
          <div class="clear"></div>
        </div>
        <div class="row <?=showAtStatus(6,$inv['invoicestatusoid']);?>">
          <div class="col-md-2">Faktur Pajak</div>
          <div class="col-md-10"> : <input type="file" name="fakturpajak" class="inline-block" id="fakturpajak" accept=".pdf, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel, image/*"></div>
          <div class="clear"></div>
        </div>
        <div class="row <?=showAtStatus(6,$inv['invoicestatusoid']);?>">
          <div class="col-md-2">Invoice</div>
          <div class="col-md-10"> : <input type="file" name="fileinvoice" class="inline-block" id="fileinvoice" accept=".pdf, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel, image/*"></div>
          <div class="clear"></div>
        </div>
        
        <div class=" <?=showAtStatus([1,4,6],$inv['invoicestatusoid']);?>">
          <br>
          <textarea name="mailcontent" id="mailContent" class="full-width"><?=$mailContent;?></textarea>
        </div>
        <br>
        <button type="submit" class="small-button purple <?=showAtStatus(1,$inv['invoicestatusoid']);?>"><i class="fa fa-envelope"></i>Send Reconciliation</button>
        <button type="submit" class="small-button blue <?=showAtStatus([3,5],$inv['invoicestatusoid']);?>"><i class="fa fa-save"></i>Update</button>
        <button type="submit" class="small-button purple <?=showAtStatus([4,6],$inv['invoicestatusoid']);?>"><i class="fa fa-envelope"></i>Send Invoice</button>
        <button type="submit" class="small-button green <?=showAtStatus([7],$inv['invoicestatusoid']);?>"><i class="fa fa-check"></i>Close Invoice</button>
        
        <!-- <button type="button" class="small-button red" bn="2018052400011" data-toggle="modal" data-target="#cancelModal" data-title="Booking Cancellation" data-status="5" data-submit="Cancel Booking"><i class="fa fa-times-circle"></i>Booking Cancellation</button>
        <button type="button" class="small-button green" bn="2018052400011" data-toggle="modal" data-target="#cancelModal" data-title="No Show" data-status="7" data-submit="Mark No Show"><i class="fa fa-ban"></i>Mark No Show</button> -->
      </div>
    </div>
  </div>

  </form>
</section>