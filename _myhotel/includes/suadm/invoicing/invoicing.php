<?php
	include('includes/bootstrap.php');
?>
<style>
#invoicing-table tr > td,
#invoicing-table tr > th {
  vertical-align: middle;
}
</style>
<section class="content-header">
  <h1>Invoicing</h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-table"></i> Invoicing</a></li>
    <li class="active">Invoicing</li>
  </ol>
</section>
<section class="content">
  <form method="post" class="form-inline">
  <div class="box box-warning">
    <div class="box-body">
      <div class="form-group">
        <label>Hotel</label>
        <select name="hotel" id="selectHotel" class="form-control">
          <option value="all">All</option>
          <?php
          $sHotel = "SELECT h.hoteloid, h.hotelname from hotel h inner join invoice i using (hoteloid) WHERE reconcileamount > 0  GROUP BY h.hoteloid ORDER BY h.hotelname";
          $stmt = $db->query($sHotel);
          if(empty($_POST['hotel'])){ $_POST['hotel'] = "all"; }
          foreach($stmt->fetchAll(PDO::FETCH_ASSOC) as $h){
            if($h['hoteloid'] == $_POST['hotel']){ $selected = "selected"; }else{ $selected = ""; }
            ?>
            <option value="<?=$h['hoteloid']?>" <?=$selected?>><?=$h['hotelname']?></option>
            <?php
          }
          ?>
        </select>
      </div>
      <div class="form-group">
        <label>Periode</label>
        <?php
        // if($_POST['hotel'] == "all"){
          ?>
          <select name="month" id="selectMonth" class="form-control">
            <?php 
            if(empty($_POST['month'])){ 
              $month = date('n'); 
              $newdate = strtotime('-1 month', mktime(0, 0, 0, $month, 10));
              $month = date('n', $newdate); 
            }else{ 
              $month = $_POST['month'];
            }
            for($m=1; $m <=12; $m++){ if($m == $month){ $selected = "selected"; }else{ $selected = ""; } ?>
            <option value="<?=$m?>" <?=$selected?>><?=date('F', mktime(0, 0, 0, $m, 10));?></option>
            <?php } ?>
          </select>
          <?php //} ?>

        <select name="year" class="form-control">
          <?php
            $sYear = "SELECT DISTINCT(i.year) as year FROM invoice i ORDER BY i.year";
            $stmt = $db->query($sYear);
            $jml = $stmt->rowCount();
            $y = $stmt->fetch(PDO::FETCH_ASSOC);
            $startyear = ($jml>0) ? $y['year'] : date('Y');
            if(empty($_POST['year'])){ $year = date('Y'); }else{ $year=$_POST['year'];}
            for($i = $startyear; $i <= date('Y'); $i++){
              if($i == $year){ $selected = "selected"; }else{ $selected = ""; }
              ?>
              <option value="<?=$i?>" <?=$selected?>><?=$i?></option>
              <?php
            }
          ?>
        </select>       
      </div>

      <div class="form-group">
        <label>Subscribe Type</label>
        <select name="hotelsubscribeoid" id="selectSubscribe" class="form-control">
          <option value="all">All</option>
          <?php
          $sSubscribe = "SELECT hs.* from hotelsubscribe hs INNER JOIN hotel h USING (hotelsubscribeoid) 
          inner join invoice i using (hoteloid) GROUP BY hs.hotelsubscribeoid ORDER BY hs.subscribe";
          $stmt = $db->query($sSubscribe);
          if(empty($_POST['hotelsubscribeoid'])){ $_POST['hotelsubscribeoid'] = 'all'; }
          foreach($stmt->fetchAll(PDO::FETCH_ASSOC) as $hs){
            if($hs['hotelsubscribeoid'] == $_POST['hotelsubscribeoid']){ $selected = "selected"; }else{ $selected = ""; }
            ?>
            <option value="<?=$hs['hotelsubscribeoid']?>" <?=$selected?>><?=$hs['subscribe']?></option>
            <?php
          }
          ?>
        </select>
        <?php echo $bala; ?>
      </div>

      <button type="submit" class="btn btn-primary">View</button>   

    </div>
  </div>

  <?php
    if(isset($_POST)){
  ?>
  <div class="box box-success">
    <div class="box-body">
      
      <?php
      $m = date("n", strtotime("first day of previous month"));
      $y = date("Y", strtotime("first day of previous month"));
      $sGenerate = "SELECT h.hoteloid, h.hotelname from hotel h inner join invoice i using (hoteloid) WHERE i.month = :a AND i.year = :b GROUP BY h.hoteloid ORDER BY h.hotelname";
      $stmt = $db->prepare($sGenerate);
      $stmt->execute(array(':a' => $m, ':b' => $y));
      $jml = $stmt->rowCount();
      if($jml==0){
        ?>
        <a href="<?=$base_url;?>/invoicing/generate" class="btn btn-warning" href><i class="fa fa-refresh"></i> Generate <b><?=date("F Y", strtotime("first day of previous month"));?></b> Invoices</a>
        <br><br>
        <?php 
      } ?>
      
      <!-- <div class="row"><div class="col-md-12"><h1>Commission Report Table</h1></div></div> -->
      <?php
      if($_POST['hotel'] == "all"){
        include('all-hotel.php');
      }else{
        include('per-hotel.php');
      }
      ?>
    </div>
  </div>
  <?php } ?>
  </form>
</section>