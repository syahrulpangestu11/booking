<?php
	$packageoid = $_POST['packageoid'];
	$name = (isset($_POST['name'])) ? $_POST['name'] : "";
	$stay = $_POST['stay'];

	$published = $_POST['published'];

	$headline = $_POST['headline'];
	$description = $_POST['description'];
	$servicefacilities = $_POST['servicefacilities'];
	$termcondition = $_POST['termcondition'];

	$deposit = $_POST['deposit'];
	$depositvalue = $_POST['depositvalue'];

	$allowedExtension = array('jpg', 'jpeg', 'png');
	$tmp = explode('.', $_FILES['image']['name']);
	$extension = end($tmp);
	$image = '';
	if(!empty($_FILES['image']['name'])){
		if(in_array($extension, $allowedExtension)){
			$imagename = str_replace(' ', '_', $_FILES['image']['name']);
			$imagename = str_replace('.'.$extension, '', $imagename);
			$ext = '.'.strtolower($extension);
			$image_new_name = substr($imagename, 0, 10).date('Ymd_His').$ext;
			$folder_destination = 'pict/packageimage/';

			move_uploaded_file($_FILES['image']['tmp_name'], $upload_base.$folder_destination.$image_new_name);
			$image = $web_url.'/'.$folder_destination.$image_new_name;

			try {
				$stmt = $db->prepare("update package_template set packageimage = :ac  where package_templateoid = :ab");
				$stmt->execute(array(':ab' => $packageoid, ':ac' => $image));
			}catch(PDOException $ex) {
				echo "Invalid Query"; print($ex);
				die();
			}

		}else{
			echo'<script>alert("Image not saved.\\nInvalid file.")</script>';
		}
	}

	$min_ci = $_POST['min_ci'];
	$max_ci = $_POST['max_ci'];

	try {
		$stmt = $db->prepare("update package_template set name = :c, stay = :d, publishedoid = :t, headline = :v, description =:w, servicefacilities = :x, termcondition = :y, depositoid = :z, depositvalue = :aa, min_bef_ci = :ac, max_bef_ci = :ad where package_templateoid = :ab");
		$stmt->execute(array(':c' => $name ,':d' => $stay,':t' => $published, ':ab' => $packageoid, ':v' => $headline, ':w' => $description, ':x' => $servicefacilities, ':y' => $termcondition, ':z' => $deposit, ':aa' => $depositvalue, ':ac' => $min_ci, ':ad' => $max_ci));
	}catch(PDOException $ex) {
		echo "Invalid Query";
		print($ex);
		die();
	}

	$type = 'package';
	if(count($_POST['icon'])>0){
		if(count($_POST['icon']) == 1){
			$remaining_icon = "'".$_POST['icon']."'";
		}else{
			$remaining_icon = "'".implode("','", $_POST['icon'])."'";
		}

		$query_delete_icon = "DELETE FROM `termheadlinepackage_template` WHERE termheadlineoid not in (".$remaining_icon.") and `id`='".$packageoid."' and type = '".$type."'";
		$stmt = $db->query($query_delete_icon);

		foreach ($_POST['icon'] as $key => $value){
			$iconoid = $value;
			$s_check_icon = "select * from termheadlinepackage_template where type = '".$type."' and id = '".$packageoid."' and termheadlineoid = '".$iconoid."'";
			$stmt = $db->query($s_check_icon);
			$row_count = $stmt->rowCount();
			if($row_count == 0){
				try {
					$stmt = $db->prepare("insert into termheadlinepackage_template (type, id, termheadlineoid) values (:a,:b,:c)");
					$stmt->execute(array(':a' => $type, ':b' => $packageoid, ':c' => $iconoid));
				}catch(PDOException $ex) {
					echo "Invalid Query";
					print($ex);
					die();
				}
			}
		}
	}else{
		$query_delete_icon = "DELETE FROM `termheadlinepackage_template` WHERE `id`='".$packageoid."' and type = '".$type."'";
		$stmt = $db->query($query_delete_icon);
	}

	header("Location: ". $base_url ."/package-template");
?>
