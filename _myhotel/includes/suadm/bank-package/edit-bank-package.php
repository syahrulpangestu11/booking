<?php
	include('includes/bootstrap.php');

	$packageoid = $_GET['pid'];
	try {
		$stmt = $db->query("select * from package_template where package_templateoid = '".$packageoid."'");
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
			$r_package = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_package as $row){
				$name = $row['name'];
				$packageimage = $row['packageimage'];
				$stay = $row['stay'];
				$published = $row['publishedoid'];
				$deposit = $row['depositoid'];
				$depositvalue = $row['depositvalue'];

				$min_ci = $row['min_bef_ci'];
				$max_ci = $row['max_bef_ci'];

				$headline = $row['headline'];
				$description = $row['description'];
				$servicefacilities = $row['servicefacilities'];
				$termcondition = $row['termcondition'];

			}
		}else{
			echo "No Result";
			die();
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}
?>
<style>
	span.label{ color:#333; font-size:0.9em; }
</style>
<script type="text/javascript">
$(function() {
	$('textarea#html-box').trumbowyg({
		btns: ['viewHTML',
		  '|', 'btnGrp-design',
		  '|', 'link',
		  '|', 'btnGrp-justify',
		  '|', 'btnGrp-lists'],
		fullscreenable: false
	});
});
</script>
<section class="content-header">
    <h1>
        Edit packages Template
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i>  packages Template </a></li>
        <li class="active">Edit</li>
    </ol>
</section>
<section class="content" id="package">
	<div class="row">
        <form class="form-box form-horizontal form-box" method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/package-template/edit-process">
        <div class="box box-form">
            <input type="hidden" name="packageoid" value="<?php echo $packageoid; ?>" />
            <div class="form-group"><div class="col-md-12"><h1>PACKAGE</h1></div></div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-md-12">Package Name</label>
                    <div class="col-md-12"><input type="text" class="form-control" name="name" required="required" value="<?=$name?>" style="width:100%"></div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">Upload New Package Image</label>
                    <div class="col-md-12"><input type="file" class="form-control" name="image"><i>recommended size : 550px x 310px</i></div>
                    <div class="col-md-12 preview-image">
                    <?php if(!empty($packageimage)){ ?>
                        <img src="<?=$packageimage?>" id="preview-image" class="img-responsive" alt="Responsive image" style="width:200px;">
                    <?php }else{ echo "N/A Image for package"; } ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <label>Headline</label>
                        <textarea id="html-box" name="headline"><?=$headline?></textarea>
                    </div>
                    <div class="col-md-12">
                        <label>package Description</label>
                        <textarea id="html-box" name="description"><?=$description?></textarea>
                    </div>
                    <div class="col-md-12">
                        <label>package Inclusive</label>
                        <textarea id="html-box" name="servicefacilities"><?=$servicefacilities?></textarea>
                    </div>
                    <div class="col-md-12">
                        <label>package Terms &amp; Condition</label>
                        <textarea id="html-box" name="termcondition"><?=$termcondition?></textarea>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <ul class="inline-triple package-triple">
                	<li style="width:100%">
                    	<h2>CONDITION</h2>
                    	<ul class="block">
                        	<li>
                            	<span>Stay:</span>
                                <input type="text" class="small" name="stay" value="<?php echo $stay; ?>">
                            </li>
                            <li>
                                <span>Guest Booking Within:</span><br />
                                <span style="vertical-align:bottom;"><input type="number" class="small" name="min_ci" value="<?php echo $min_ci; ?>" min="0" /> to <input type="number"  class="small" name="max_ci" value="<?php echo $max_ci; ?>" min="0" /> days (before check in)</span>
                            </li>
                        </ul>
                    </li>
                </ul>
                <div style="padding-top:15px">
                    <div class="panel panel-danger">
                        <div class="panel-heading" style="color:#000">
                        <label>Deposit Type</label>
                        <div>
                        <?php
                            try {
                                $stmt = $db->query("select d.* from deposit d inner join published p using (publishedoid) where p.publishedoid = '1'");
                                $r_dt = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                foreach($r_dt as $row){
                                    if($row['depositoid'] == $deposit){ $selected = "checked"; $depositval = $depositvalue; }else{ $selected=""; $depositval = ""; }
                                    echo"<input type='radio' name='deposit' value='".$row['depositoid']."' ".$selected.">  ".$row['name'];
                                    if($row['depositoid'] == '2'){
                                        echo"<div class='depositval' style='display:none'><input type='text' class='medium'  name='depositvalue' value='".$depositval."'> Deposit for IDR Amount</div>";
                                    }else if($row['depositoid'] == '3'){
                                        echo"<div class='depositval' style='display:none'><input type='text' class='medium'  name='depositvalue' value='".$depositval."'> Deposit for %(percentage)from total</div>";
                                    }else if($row['depositoid'] == '4'){
                                        echo"<div class='depositval' style='display:none'><input type='text' class='medium'  name='depositvalue' value='".$depositval."'> night(s) amount</div>";
                                    }
                                    echo "<br>";
                                }
                            }catch(PDOException $ex) {
                                echo "Invalid Query";
                                die();
                            }
                        ?>
                        </div>
                        </div>
                    </div>
                </div>
                <ul class="inline">
                    <label>Headline Icon</label><br>
                <?php
                    try {
                        $stmt = $db->query("SELECT th.*, i.*, x.termheadlinepackage_templateoid from termheadline th left join icon i using (iconoid) left join (select termheadlinepackage_templateoid, termheadlineoid from termheadlinepackage_template thp where thp.id='".$packageoid."' and thp.type='package') x on x.termheadlineoid = th.termheadlineoid
                        where th.hoteloid in ('0') order by icon_title");
                        $r_headline = $stmt->fetchAll(PDO::FETCH_ASSOC);
                        foreach($r_headline as $termheadline){
                            if($termheadline['iconoid'] == 0){
                                $icon_src = $termheadline['term_icon_src'];
                            }else{
                                $icon_src = $termheadline['icon_src'];
                            }
                            if(!empty($termheadline['termheadlinepackage_templateoid'])){ $checkapply = "checked";  }else{ $checkapply = ""; }
                            ?>
                            <li class="col-md-6">
                            <input type="checkbox" name="icon[]" value="<?=$termheadline['termheadlineoid']?>" <?=$checkapply?>/>&nbsp;&nbsp;<img src="<?=$icon_src?>" class="icon" />&nbsp;&nbsp;<?=$termheadline['icon_title']?>
                            </li>
                            <?php
                        }
                    }catch(PDOException $ex) {
                        echo "Invalid Query";
                        die();
                    }
                ?>
                </ul>
                <div class="clear"></div>
                <div style="padding-top:15px">
                    <label>Publish package</label>
                    <select name="published" class="form-control">
                    <?php
                        try {
                            $stmt = $db->query("select * from published where showthis = 'y'");
                            $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                            foreach($r_room as $row){
                                if($row['publishedoid'] == $published){ $selected = "selected"; }else{ $selected=""; }
                                echo"<option value='".$row['publishedoid']."' ".$selected.">".$row['note']."</option>";
                            }
                        }catch(PDOException $ex) {
                            echo "Invalid Query";
                            die();
                        }
                    ?>
                    </select>
                </div>
            </div>
            <div class="clear"></div>
            <div class="box-footer" style="margin-top:15px">
                <div class="col-md-12 text-right">
                    <button class="btn btn-danger cancel" type="button">Cancel</button>
                    <button class="btn btn-primary" type="submit">Save Template</button>
                </div>
            </div>
		</div>
		</form>
    </div>
</section>
