<script type="text/javascript">
$(function() {
	$('textarea').trumbowyg({
		btns: ['viewHTML',
		  '|', 'btnGrp-design',
		  '|', 'link',
		  '|', 'btnGrp-justify',
		  '|', 'btnGrp-lists'],
		fullscreenable: false
	});
});
</script>
<section class="content-header">
    <h1>
        Create Extra
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i>  Allotments &amp; Rates </a></li>
        <li class="active">Extra</li>
    </ol>
</section>
<section class="content">
	<div class="row">
        <form class="form-box" method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/extra-template/add-process">
        <div class="box box-form">
            <h1>Extra</h1>
        <ul class="inline-half">
            <li>
            <ul class="block">
                <li>
                    <span class="label">Extra Name:</span>
                    <input type="text" class="long" name="name">
                </li>
                <li>
                    <span class="label">Picture</span>
                    <input type="file" class="medium" name="image">
                </li>
                <li>
                <img id="preview" src="#" alt="" style="display:none" />
                </li>
            </ul>
            <li>
            <ul class="block">
                <li><span class="label"><b>Sale Date</b></span></li>
                <li>
                    <span class="label">Sale Date From:</span>
                    <input type="text"class="medium" id="startdate" name="salefrom">
                </li>
                <li>
                    <span class="label">Sale Date To:</span>
                    <input type="text"class="medium" id="enddate"; name="saleto">
                </li>
								<li>
                    <span class="label">Publish Extra :</span>
                    <select name="published">
                    <?php
                        try {
                            $stmt = $db->query("select * from published where showthis = 'y'");
                            $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                            foreach($r_room as $row){
                                echo"<option value='".$row['publishedoid']."'>".$row['note']."</option>";
                            }
                        }catch(PDOException $ex) {
                            echo "Invalid Query";
                            die();
                        }
                    ?>
                    </select>
                </li>
            </ul>
            </li>
        </ul>
        </div>
        <div class="box box-form">
			<h2>HEADLINE &amp; DETAIL</h2>
            <ul class="inline-half">
                <li>
                    <h3>Headline</h3>
                    <textarea name="headline"></textarea>
                </li>
				<li>
                	<h3>Description</h3>
                   	<textarea name="description"></textarea>
				</li>
			</ul>
            <div class="clear"></div>
            <div class="clear"></div>
            <button class="default-button" type="submit">Submit Extra</button>
   		</div>
		</form>
    </div>
</section>
