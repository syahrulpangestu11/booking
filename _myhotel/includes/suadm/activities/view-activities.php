<?php
	$name = $_REQUEST['name'];
	$country = $_REQUEST['country'];
	$state = $_REQUEST['state'];
	$city = $_REQUEST['city'];
?>
<section class="content-header">
    <h1>
       	Hotel
    </h1>
    <ol class="breadcrumb">
        <li class="active"><a href="#"><i class="fa fa-table"></i> Activities</a></li>
    </ol>
</section>
<section class="content">

    <div class="row">
        <div class="box box-form">
            <div class="box-body">
                <div class="form-group">
            <form method="get" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/hotel/">
			<input type="hidden" name="dfltstate" value="0">
            <input type="hidden" name="dfltcity" value="0">

                    <label>Activities  :</label> &nbsp;&nbsp;
					<input type="text" name="name" class="input-text" value="<?php echo $name; ?>">
					<br /><br />    
                    <!--
                    <label>Country  :</label> &nbsp;&nbsp;                  
                    <select name="country" class="input-select">
                    	<option value="">show all</option>
                        <?php //getCountry($country); ?>
                    </select>
                    <span class="loc-state"></span>
                    <span class="loc-city"></span>
                    -->
                    <br /><br />   
                    <button type="submit" class="small-button blue"><i class="fa fa-search"></i>Find</button>
                </form>
                </div>
            </div><!-- /.box-body -->
       </div>
    </div>
    
    <div class="row">
        <div class="box">
        	<form method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>">
                <div id="data-box" class="box-body">
                    <div class="loader">Loading...</div>
                </div><!-- /.box-body -->
            </form>
       </div>
    </div>
</section>
