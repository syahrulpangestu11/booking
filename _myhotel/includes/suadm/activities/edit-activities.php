<?php
	$itemoid = $_GET['ho'];
	try {
		$stmt = $db->query("select * from item i inner join published using (publishedoid) inner join city c using (cityoid) inner join state s using (stateoid) inner join country ct using (countryoid) where i.itemoid = '".$itemoid."'");
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
			$r_hotel = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_hotel as $row){
				$itemname	= $row['itemname'];
				$minqty		= $row['minquantity'];
				$company	= $row['company'];
				$headline	= $row['headline'];
				$description= $row['description'];
				$published	= $row['publishedoid'];
				$country	= $row['countryoid'];
				$state		= $row['stateoid'];
				$city		= $row['cityoid'];
				$multiprice	= $row['multipricetypeoid'];
			}
		}else{
			echo "No Result";
			die();
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}


?>
<section class="content-header">
    <h1>
        Activities
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-laptop"></i> Activities</a></li>
        <li><?php echo $hotelname; ?></li>
        <li class="active">Edit</li>
    </ol>
</section>
<section class="content">
	<div class="row">
        <div class="box box-form">
        	<form class="form-box" method="post" enctype="multipart/form-data" id="data-input" action="#">
            <input type="hidden" name="ho" value="<?php echo $itemoid; ?>">
            <input type="hidden" name="dfltstate" value="<?php echo $state; ?>">
            <input type="hidden" name="dfltcity" value="<?php echo $city; ?>">
            <h3>ACTIVITIES DETAIL</h3>
            <ul class="block">
                <li>
                    <span class="label">Name</span>
                    <input type="text" class="long" name="name" value="<?php echo $itemname; ?>">
				</li>
                <li>
                    <span class="label float-left" style="vertical-align:middle">Destination</span>
                    <div class="side-left">
                        <ul class="inline-block">
                            <li>
                            	Country<br />
                                <select name="country" class="input-select">
									<?php getCountry($country); ?>
                                </select>
                            </li>
                            <li class="loc-state"></li>
                            <li class="loc-city"></li>
                        </ul>
                    </div>
                    <div class="clear"></div>
                </li>
                <li>
                    <span class="label">Minimum Qty or Pax</span>
                    <input type="number" class="small" name="minqty" min="1" value="<?php echo $minqty; ?>">
                </li>
                <li>
                    <span class="label">Company Supplier</span>
                    <input type="text" class="long" name="company" value="<?php echo $company; ?>">
				</li>
                <li>
                    <span class="label"><b>Publish Activities</b></span>
                    <select name="published">
                    <?php getPublished($published); ?>
                    </select>
                </li>
            </ul>
                        
            <h3>ACTIVITIES INQUIRY</h3>
            <ul class="inline-triple">
            	<li>
                	<span class="label">Headline</span>
                    <textarea name="headline"><?php echo $headline; ?></textarea>
                </li>
            	<li>
                	<span class="label">Description</span>
                    <textarea name="description"><?php echo $description; ?></textarea>
                </li>
            </ul>
            
            <h3>Activities Price</h3>
            <ul class="block">
                <li>
                    <span class="label float-left" style="vertical-align:middle">Pricing Type</span>
                    <div class="side-left">
					<?php
						try {
							$stmt = $db->query("select * from multipricetype");
							$row_count = $stmt->rowCount();
							if($row_count > 0) {
								$r_mp = $stmt->fetchAll(PDO::FETCH_ASSOC);
								foreach($r_mp as $mp){
									if($mp['multipricetypeoid'] == $multiprice){ $checked = 'checked'; }else{ $checked = ''; } 
								?>
									<input type="radio" name="multiprice" io="<?php echo $itemoid; ?>" value="<?php echo $mp['multipricetypeoid']; ?>" <?php echo $checked; ?> id="typepriceedit" /><?php echo $mp['type']; ?><br />											
								<?php
								}
							}else{
								echo "No Result";
								die();
							}
						}catch(PDOException $ex) {
							echo "Invalid Query";
							die();
						}
					?>
                    </div>
                    <div class="clear"></div>
				</li>
                <li>
                    <span class="label">Currency</span>
                    <select name="currency">
                        <?php
                        try {
                            $stmt = $db->query("select * from currency");
                            $r_published = $stmt->fetchAll(PDO::FETCH_ASSOC);
                            foreach($r_published as $row){
                                if($row['currencyoid'] == $currency){ $selected = 'selected'; }else{ $selected = ''; }
                        ?>
                            <option value="<?php echo $row['currencyoid']; ?>" <?php echo $selected; ?>><?php echo $row['currencycode']; ?></option>
                        <?php	
                            }
                        }catch(PDOException $ex) {
                            echo "Invalid Query";
                            die();
                        }
                        ?>
                    </select>
                </li>
                <li>
                </li>
			</ul>
                
            <ul class="block">
                <li>
                    <span class="label float-left" style="vertical-align:middle">&nbsp;</span>
                    <div class="side-left">
                        <div class="form-input">
							<button type="button" class="submit-edit">Submit</button>
                        </div>
                    </div>
                    <div class="clear"></div>
				</li>
            </ul>
		</form>
   		</div>
    </div>
</section>
