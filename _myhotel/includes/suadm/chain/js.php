<script type="text/javascript">
$(function(){
	
	function submitDataCallback(url, forminput){
		$.ajax({
			url: url,
			type: 'post',
			data: forminput.serialize(),
			success: function(response) {
				if(response == 1){
					$dialogNotice.html("Data succesfully updated");
					$dialogNotice.dialog("open");
				}else{
					$dialogNotice.html("We&rsquo;re very sorry, we can't save your data right now");
					$dialogNotice.dialog("open");
				}
			}
		});
	}


	var $dialogNotice = $('<div id="dialog-notice"></div>')
	.dialog({
		autoOpen: false,
		title: 'Notification',
		buttons: {
			"Ok": function(){
				$( this ).dialog( "close" );
				document.location.href = "<?php echo $base_url; ?>/chain";
			}
		}
	});
	
	/*
	*
	*/
	
	$('button.submit-add').click(function(){
		url = '<?php echo $base_url; ?>/includes/suadm/chain/add-cp-save.php';
		forminput = $('form#data-input');
		submitDataCallback(url, forminput);
	});

	$('button.submit-edit').click(function(){
		url = '<?php echo $base_url; ?>/includes/suadm/chain/edit-cp-save.php';
		forminput = $('form#data-input');
		submitDataCallback(url, forminput);
	});

	/*
	*
	*/
	
	$(document).ready(function(){ 
		getLoadData();
	});
	
	$('body').on('click','#paging button', function(e) {
		page	= $(this).attr('page');
		pagebox = $('form#data-input').children('input[name=page]');
		pagebox.val(page);
		getLoadData(); 
	});
	
	$('body').on('click','button.edit', function(e) {
		pid = $(this).attr("pid");
		url = "<?php echo $base_url; ?>/chain/edit/?cpoid="+pid;
      	$(location).attr("href", url);
	});

	$('body').on('click','button.add', function(e) {
		pid = $(this).attr("pid");
		url = "<?php echo $base_url; ?>/chain/add";
      	$(location).attr("href", url);
	});
	
   var $dialogDelete = $('<div id="dialog-del"></div>')
	.html('Are you sure you want to delete this data?')
	.dialog({
		autoOpen: false,
		title: 'Confirmation',
		buttons: { 
			"Delete": function(){ 
				var url = $(this).data('url'); 
				var id = $(this).data('id'); 
				$(this).dialog("close"); 
				deleteAction(url, id);
			},
			Cancel: function(){ $( this ).dialog("close"); }
		}
	});
	
	var $dialogNotification = $('<div id="dialog-notice"></div>')
	.dialog({
		autoOpen: false,
		title: 'Notification',
		buttons: { 
			"Ok": function(){ 
				var callback = '<?php echo $base_url; ?>/chain'; 
				$(this).dialog( "close" );
				$(location).attr("href", callback); 
			}
		}
	});
	
	function deleteAction(url, id){
		$.ajax({
			url: url,
			type: 'post',
			data: { id : id},
			success: function(data) {
				if(data == "1"){
					$dialogNotification.html("Data has been deleted");
					$dialogNotification.dialog("open");
				}else{
					$dialogNotification.html("We&rsquo;re very sorry, we can't save your data right now");
					$dialogNotification.dialog("open");
				}
			}
		});
	}
	
	$('body').on('click','button.delete', function(e) {
		id = $(this).attr("pid");
		$dialogDelete.data("url", "<?php echo $base_url; ?>/includes/suadm/chain/delete-cp.php");
		$dialogDelete.data("id", id);
		$dialogDelete.dialog("open");
	});
		
	function getLoadData(){
		$.ajax({
			url: "<?php echo"$base_url"; ?>/includes/suadm/chain/data.php",
			type: 'post',
			data: $('form#data-input').serialize(),
			success: function(data) {
				$("#data-box").html(data)
			}
		});
	}	
		

});
</script>