<?php

$block_role_pms = array(1, 2, 3, 4, 5, 6);


if(in_array($_SESSION['_typepmsusr'], $block_role_pms) and $next == true){
    echo '
	<script>
	    setTimeout(function(){
           window.location.reload(1);
        }, 900000);
	</script>';

	$next = false;
	if($uri2=="pms-lite"){
		include('includes/bootstrap.php');
?>
		<link rel="stylesheet" href="<?php echo"$base_url"; ?>/includes/pms-lite/style-pms-lite.css?v=<?=$fileVersion;?>" type="text/css">
<?php
		if($uri3=="reservation-chart"){

			include("pms-lite/reservation-chart/reservation-chart.php");
		}else if($uri3=="reservation-detail"){
			switch($_SESSION['_typepmsusr']){
				case '2' : include("pms-lite/reservation-chart/reservation-detail-villa-manager.php"); break;
				case '4' : include("pms-lite/reservation-chart/reservation-detail-villa-manager.php"); break;
				case '5' : include("pms-lite/reservation-chart/reservation-detail-owner.php"); break;
				case '6' : include("pms-lite/reservation-chart/reservation-detail-no-edit.php"); break;
				default : include("pms-lite/reservation-chart/reservation-detail.php"); break;
			}
		}else if($uri3=="reservation-detail-edit"){
			  include("pms-lite/reservation-chart/reservation-detail-edit.php");
		}else if($uri3=="send-invoice-guest"){
			  include("pms-lite/send-email/send-invoice-guest.php");
		}else if($uri3=="send-invoice-agent"){
			  include("pms-lite/send-email/send-invoice-agent.php");
		}else if($uri3=="send-confirmation-guest"){
			  include("pms-lite/send-email/send-email-confirmation-guest.php");
		}else if($uri3=="calendar-view"){
		    include("pms-report/calendar_view.php");
		}else if($uri3=="three-month"){
				include("pms-report/three_month.php");
		}else if($uri3=="housekeeping"){
				include("pms-lite/housekeeping/housekeeping.php");
		}else if($uri3=="settings"){
		    if($uri4=="saveinvoice"){
		        include("pms-lite/settings/invoice.save.php");
		    }else{
				include("pms-lite/settings/invoice.php");
		    }
		}
	}else if($uri2=="pms-report"){
		if($uri3=="rsv"){
			include("pms-report/report_rsv.php");
		}else if($uri3=="rsvbymarket"){
			include("pms-report/report_rsvbymarket.php");
		}else if($uri3=="market"){
			include("pms-report/report_market.php");
		}else if($uri3=="rsvlist"){
			include("pms-report/report_rsvlist1.php");
		}else if($uri3=="checkinlist"){
			include("pms-report/report_checkinlist.php");
		}else if($uri3=="checkoutlist"){
			include("pms-report/report_checkoutlist.php");
		}else if($uri3=="noshowlist"){
			include("pms-report/report_noshowlist.php");
		}else if($uri3=="tmpbookinglist"){
			include("pms-report/report_tmpbookinglist.php");
		}else if($uri3=="cancellist"){
			include("pms-report/report_cancellist.php");
		}else if($uri3=="guestinhouselist"){
			include("pms-report/report_guestinhouselist.php");
		}else if($uri3=="flashmgr"){
			include("pms-report/report_flash_manager.php");
		}else if($uri3=="forecast"){
			include("pms-report/report_forecast.php");
		}else if($uri3=="businessanalyst"){
			include("pms-report/report_business_analyst.php");
		}else if($uri3=="occupancyanalysis"){
			include("pms-report/report_occupancy_analysis.php");
		}else if($uri3=="dailyreport"){
			include("pms-report/report_daily_report.php");
		}else if($uri3=="roomperbook"){
			include("pms-report/report_room_perbook.php");
		}else if($uri3=="mtdytd"){
			include("pms-report/report_mtd_ytd.php");
		}else if($uri3=="housekeeping"){
			include("pms-report/report_housekeeping.php");
		}else if($uri3=="dailyrevenue"){
			include("pms-report/report_daily_revenue.php");
		}else if($uri3=="incomereport"){
			include("pms-report/report_income.php");
		}else if($uri3=="arledgerreport"){
			include("pms-report/report_arledger.php");
		}else if($uri3=="otherchargereport"){
			include("pms-report/report_othercharge.php");
        }else if($uri3=="agentreport"){
			include("pms-report/report_agent.php");
        }else if($uri3=="countryreport"){
			include("pms-report/report_country.php");
        }else if($uri3=="dailyrevenue-report"){
            include("pms-report/daily-revenue/report_daily_revenue.php");
        }else if($uri3=="forecast-report"){
			include("pms-report/forecast/report_forecast.php");
        }else if($uri3=="forecast-3month-report"){
			include("pms-report/forecast-3month/report_3month_forecast.php");
		}else if($uri3=="dailytransaction"){
			include("pms-report/dailytransaction/report_dailytransaction.php");
    	}else if($uri3=="paymentreport"){
  			include("pms-report/report_payment.php");
		}else if($uri3=="chainoccupancy"){
			include("pms-report/chain_occupancy/view-occupancy-report.php");
	  	}else if($uri3=="occupancypervilla"){
			include("pms-report/occupancypervilla/view-occupancypervilla.php");
	  	}else{
			$next = true;
		}
	}else{
		$next = true;
	}
}
?>
