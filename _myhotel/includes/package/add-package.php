<?php
	include('includes/bootstrap.php');
	include('includes/package/modal/modal-package.php');
	include('includes/package/geo-location/geo-location.php');

	$daylist = array("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat");
	$stay = 0;
	$min_ci = 0;
	$max_ci = 0;

	if(isset($_GET['tid'])){
    	$packageoid = $_GET['tid'];
    	try {
    		$stmt = $db->query("select * from package_template where package_templateoid = '".$packageoid."'");
    		$row_count = $stmt->rowCount();
    		if($row_count > 0) {
    			$r_promo = $stmt->fetchAll(PDO::FETCH_ASSOC);
    			foreach($r_promo as $row){
    				$name = $row['name'];
    				$packageimage = $row['packageimage'];
    				$stay = $row['stay'];
    				$published = $row['publishedoid'];
    				$deposit = $row['depositoid'];
    				$depositvalue = $row['depositvalue'];

    				$min_ci = $row['min_bef_ci'];
    				$max_ci = $row['max_bef_ci'];

    				$headlinepackage = $row['headline'];
    				$description = $row['description'];
    				$servicefacilities = $row['servicefacilities'];
    				$termcondition = $row['termcondition'];

    			}
    		}else{
    			echo "No Result";
    			die();
    		}
    	}catch(PDOException $ex) {
    		echo "Invalid Query";
    		die();
    	}
	}
?>
<script type="text/javascript">
$(function() {
	$('textarea.html-box').trumbowyg({
		btns: ['viewHTML',
		  '|', 'btnGrp-design',
		  '|', 'link',
		  '|', 'btnGrp-justify',
		  '|', 'btnGrp-lists'],
		fullscreenable: false
	});
});
</script>
<section class="content-header">
    <h1>
        Create Package
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i>  Allotments &amp; Rates </a></li>
        <li class="active">Package</li>
    </ol>
</section>
<section class="content" id="promotion">
    <form class="form-box form-horizontal" method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/package/add-process">
        <div class="box box-form">
            <div class="form-group"><div class="col-md-12"><h1>PACKAGE</h1></div></div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-md-12">Package Name</label>
                            <div class="col-md-12"><input type="text" class="form-control" name="name" required="required" value="<?=$name?>" style="width:100%"></div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">Direct Booking Package Link</label>
                            <div class="col-md-12">
                                <div class="radio" style="display:inline-block;padding-right:10px;">
                                    <label><input type="radio" name="link-style" value="primary" checked="checked"> show package only</label>
                                </div>
                                <div class="radio" style="display:inline-block;padding-right:10px;">
                                    <label><input type="radio" name="link-style" value="all"> show package on top</label>
                                </div>
                            </div>
                            <div class="col-md-12"><textarea name="direct-link" class="form-control"></textarea></div>
        				</div>
        				<div class="form-group">
                            <div class="col-md-12"><button type="button" class="btn btn-warning btn-sm copy-link" name="<?=htmlspecialchars($name)?>" night="<?php if($stay < 2){ echo 2; }else{ echo $stay; } ?>" min-bef-ci="<?php echo $min_ci; ?>"><i class="fa fa-link"></i> Copy Link to Clipboard</button></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-md-12">Package Image</label>
                            <div class="col-md-12 preview-image">
                            <?php if(!empty($packageimage)){ ?>
                                <img src="<?=$packageimage?>" id="preview-image" class="img-responsive" alt="Responsive image" style="width:200px;">
                            <?php }else{ echo "N/A Image for Package"; } ?>
                            </div>
                            <div class="col-md-12">
                                <div class="checkbox">
                                    <label><input type="checkbox" name="popupbanner" value="1"> Show as <b>Popup Banner</b></label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">Upload New Package Image</label>
                            <div class="col-md-12"><input type="file" class="form-control" name="image"></div>
                            <div class="col-md-12"><i>recommended size : 550px x 310px</i></div>
                        </div>
                    </div>
                </div>
                <div class="form-group"><div class="col-md-12 text-right"><button class="btn btn-primary" type="submit">Save Package</button></div></div>
            </div>
            <div class="clear"></div>
            <br>

            <div class="row">
								<div class="col-md-4 triple" style="padding:10px;">
									<h2>PACKAGE TYPE</h2>
									<div class="form-group">
										<div class="col-md-12">
											<select name="typepackage" class="form-control">
											<?php foreach($listpackagetype as $key => $value){ ?>
												<option value="<?=$key?>" checked="checked"> <?=$value?></option>
											<?php } ?>
											</select>
										</div>
									</div>
                	<h2>CONDITION</h2>
										<div class="form-group">
												<label class="col-md-5">Stay:</label>
												<div class="col-md-4"><input type="number" class="form-control" name="stay" min="1" value="<?=$stay?>"></div>
										</div>
										<div class="form-group long-stay-feature" style="display:none;">
											<div class="checkbox col-md-12">
											  <label>
											    <input type="checkbox" name="overbook" value="y">
											    Allow over booking
											  </label>
											</div>
											<div class="checkbox col-md-12">
											  <label>
											    <input type="checkbox" name="ignorenight" value="y">
											    Allow this package show in all night criteria
											  </label>
											</div>
										</div>
                    <div class="form-group">
                        <label class="col-md-12">Guest Booking Within:</label>
                        <div class="col-md-4"><input type="number" class="form-control" name="min_ci" min="0"  value="<?=$min_ci?>"/></div>
                        <div class="col-md-1 text-center">to</div>
                        <div class="col-md-4"><input type="number"  class="form-control" name="max_ci" min="0" value="<?=$max_ci?>" /></div>
                        <div class="col-md-12">days (before check in)</div>
                    </div>
                    <div class="form-group"><label class="col-md-12">Sale Date</label></div>
                    <div class="form-group">
                        <label class="col-md-5">Sale Date From:</label>
                        <div class="col-md-7">
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                <input type="text" class="form-control" id="startdate" name="salefrom" value="<?php echo date('d F Y');?>">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5">Sale Date To:</label>
                        <div class="col-md-7">
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                <input type="text" class="form-control" id="enddate" name="saleto">
                            </div>
                        </div>
                    </div>
                    <div class="form-group"><label class="col-md-3">Stay Date</label></div>
                    <div class="form-group">
                        <label class="col-md-5">Stay Date From:</label>
                        <div class="col-md-7">
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                <input type="text" class="form-control" id="startcalendar" name="stayfrom" value="<?php echo date('d F Y');?>">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5">Stay Date To:</label>
                        <div class="col-md-7">
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                <input type="text" class="form-control" id="endcalendar" name="stayto">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Displayed on:</label><br>
                        <div class="col-md-12">
                        <?php foreach($daylist as $day){ echo"<input type='checkbox' name='display[]' value='".$day."' checked>".$day."&nbsp;&nbsp;"; } ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Check-in on:</label><br>
                        <div class="col-md-12">
                        <?php foreach($daylist as $day){ echo"<input type='checkbox' name='checkin[]' value='".$day."' checked>".$day."&nbsp;&nbsp;"; } ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5">Book Time From:</label>
                        <div class="col-md-7">
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                <input type="time" class="form-control" name="timefrom" value="00:00">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5">Book Time To:</label>
                        <div class="col-md-7">
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                <input type="time" class="form-control" name="timeto" value="23:59">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 triple" style="display:none;">
                	<h2>BENEFIT</h2>
                    <div class="form-group">
                        <label class="col-md-12">Discount Type</label>
                        <?php
                            try {
                                $stmt = $db->query("select dt.* from discounttype dt inner join published p using (publishedoid) where p.publishedoid = '1'");
                                $r_dt = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                foreach($r_dt as $row){
                                    if($row['discounttypeoid'] == '1'){ $selected = "checked"; }else{ $selected=""; }
                                    echo "<div class='checkbox'><label><input type='radio' name='discounttype' value='".$row['discounttypeoid']."' label='".$row['labelquestion']."' ".$selected.">  ".$row['name']."</label></div>";
                                }
                            }catch(PDOException $ex) {
                                echo "Invalid Query";
                                die();
                            }
                        ?>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Discount Value</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control"  name="discountvalue">
                            <span></span>
                        </div>
                    </div>
                    <div class="form-group applybox">
                        <label class="col-md-12">Apply On</label>
                        <?php
                            try {
                                $stmt = $db->query("select da.* from discountapply da inner join published p using (publishedoid) where p.publishedoid = '1'");
                                $r_dt = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                foreach($r_dt as $row){
                                    if($row['discountapplyoid'] == '1'){ $selected = "checked"; }else{ $selected=""; }
                                    echo"<div class='col-md-12'><input type='radio' name='discountapply' value='".$row['discountapplyoid']."' ".$selected.">  ".$row['name']."";
                                    if($row['discountapplyoid'] == '2'){
                                        echo"<div class='applyval'><label>Apply on Night Number</label><input type='text' class='form-control' name='applyvalue'><br>
                                        ex: 2,3,4 for night number 2, 3 and 4 only</div>";
                                    }else if($row['discountapplyoid'] == '3'){
                                        echo"<div class='applyval'><label>Apply on Day</label><br>";
                                        foreach($daylist as $day){
                                            echo"<input type='checkbox' name='applyvalue[]' value='".$day."' checked>".$day."&nbsp;&nbsp;";
                                        }
                                        echo"</div>";
                                    }
									echo "</div>";
                                }
                            }catch(PDOException $ex) {
                                echo "Invalid Query";
                                die();
                            }
                        ?>
                    </div>
                </div>

                <style>
                    #step-3 {
                        padding: 20px;
                        border: 1px solid #ddd;
                    }
                </style>

                <div class="col-md-8">
                	<div class="" id="step-3">
                        <div class="row">
                            <div class="col-md-6"><h1><i class="fa fa-bed"></i> Set Room &amp; Rate</h1></div>
                            <div class="col-md-6 text-right"><button type="button" class="btn btn-success" data-toggle="modal" data-target="#RatePlanModal">Add New Room and Rate</button></div>
                        </div>
												<div class="row">
													<div class="col-md-12">
														<label>
															<i class="fa fa-info-circle"></i> Multiples apply for package purchases? &nbsp;&nbsp;&nbsp;
															<input type="radio" name="multiply" value="y"> Y
															<input type="radio" name="multiply" checked value="n"> N
														</label>
													</div>
												</div><div class="row">
													<div class="col-md-12">
														<label>
															<i class="fa fa-info-circle"></i> Apply multiplier &nbsp;&nbsp;&nbsp;
															<input type="radio" name="multiplier" value="plus"> +
															<input type="radio" name="multiplier" checked value="minus"> -
															<input type="text" name="multiplier_val" value="0"> %
														</label>
													</div>
												</div>
                        <div class="row" id="assigned-rateplan">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group"><div class="col-md-12 text-right"><button class="btn btn-primary" type="submit">Save Package</button></div></div>
        </div>

		<?php if(!in_array($_SESSION['_hoteltemplate'], array(4))){ ?>
		<div class="box box-form" id="headline-detail">
        	<div class="row">
            	<div class="col-md-6">
                    <h1><i class="fa fa-eye"></i> Show Package with Geo Location</h1>
                    <div class="panel panel-info">
                        <div class="panel-heading text-center">
                            <button type="button" class="btn btn-primary btn-lg"  data-toggle="modal" data-target="#geoModalCountry"><i class="fa fa-map-marker"></i> Add Location</button>
                            <div class="list-country"><ul class="inline-block"></ul></div>
                        </div>
                    </div>
                </div>
            	<div class="col-md-6">
                    <h1><i class="fa fa-eye-slash"></i> Hide Package with Geo Location</h1>
                    <div class="panel panel-danger">
                        <div class="panel-heading text-center">
                            <button type="button" class="btn btn-danger btn-lg"  data-toggle="modal" data-target="#geoModalCountryNoShow"><i class="fa fa-map-marker"></i> Add Location</button>
                            <div class="list-country-noshow"><ul class="inline-block"></ul></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<?php } ?>

		<div class="box box-form" id="headline-detail">
			<h2>HEADLINE &amp; DETAIL</h2>
            <div class="row">
            <?php
                try {
										if(isset($_GET['tid'])){
											$stmt = $db->query("SELECT th.*, i.*, x.termheadlinepackage_templateoid from termheadline th left join icon i using (iconoid) left join (select termheadlinepackage_templateoid, termheadlineoid from termheadlinepackage_template thp where thp.id='".$packageoid."' and thp.type='package') x on x.termheadlineoid = th.termheadlineoid
											where th.termheadlineoid not in (select termheadlineoid from termheadlinehotelremove where hoteloid = '".$hoteloid."') and th.hoteloid in ('".$hoteloid."', '0')
											order by icon_title");
										}else{
											$stmt = $db->query("select * from termheadline th left join icon using (iconoid) where th.termheadlineoid not in (select termheadlineoid from termheadlinehotelremove where hoteloid = '".$hoteloid."') and th.hoteloid in ('".$hoteloid."', '0') order by icon_title");
										}
                    //$stmt = $db->query("select * from termheadline th left join icon using (iconoid) where th.termheadlineoid not in (select termheadlineoid from termheadlinehotelremove where hoteloid = '".$hoteloid."') and th.hoteloid in ('".$hoteloid."', '0') order by icon_title");
                    $r_headline = $stmt->fetchAll(PDO::FETCH_ASSOC);
                    foreach($r_headline as $headline){
                        if($headline['iconoid'] == 0){
                            $icon_src = $headline['term_icon_src'];
                        }else{
                            $icon_src = $headline['icon_src'];
                        }
												if(!empty($headline['termheadlinepackage_templateoid'])){ $checkapply = "checked='checked'";  }else{ $checkapply = ""; }
                        ?>
                        <div class="col-md-3">
                        <input type="checkbox" name="icon[]" value="<?=$headline['termheadlineoid']?>" <?=$checkapply?>/>&nbsp;&nbsp;<img src="<?=$icon_src?>" class="icon" />&nbsp;&nbsp;<?=$headline['icon_title']?>
                        </div>
                        <?php
                    }
                }catch(PDOException $ex) {
                    echo "Invalid Query";
                    die();
                }
            ?>
			</div>
            <div class="form-group">
                <div class="col-md-6">
                    <br>
                    <label>Headline</label>
                    <textarea name="headline" class="html-box"><?=$headlinepackage?></textarea>
                </div>
                <div class="col-md-6">
                    <br>
                    <label>Package Description</label>
                    <textarea name="description" class="html-box"><?=$description?></textarea>
                </div>
                <div class="col-md-6">
                    <br>
                    <label>Package Inclusive</label>
                    <textarea name="servicefacilities" class="html-box"><?=$servicefacilities?></textarea>
                </div>
                <div class="col-md-6">
                    <br>
                    <label>Package Terms &amp; Condition</label>
                    <textarea name="termcondition" class="html-box"><?=$termcondition?></textarea>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <br>
                        <label class="col-md-12">Cancellation Policy:</label>
                        <div class="col-md-12">
                            <select name="cancellation" class="form-control">
                            <?php
                                try {
                                    $stmt = $db->query("select cp.* from cancellationpolicy cp inner join published p using (publishedoid) where p.publishedoid = '1' and cp.hoteloid in ('0','".$hoteloid."')");
                                    $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                    foreach($r_room as $row){
                                        echo"<option value='".$row['cancellationpolicyoid']."'>".$row['name']."</option>";
                                    }
                                }catch(PDOException $ex) {
                                    echo "Invalid Query";
                                    die();
                                }
                            ?>
                            </select>
                    	</div>
                    </div>
										<div class="panel panel-danger">
											<div class="panel-heading" style="color:#000">
                    		<div class="form-group">
													<div class="col-md-12">
                            <label>Deposit Type</label>
                            <br>
                            <span style="font-size:0.9em; color:#de0000"><i class="fa fa-info-circle"></i> This amount will be declared as booking guarantee that charged by hotel upon booking received.</span>
                        	</div>
                        	<div class="col-md-12">
													<?php
	                            try {
	                                $stmt = $db->query("select d.* from deposit d inner join published p using (publishedoid) where p.publishedoid = '1'");
	                                $r_dt = $stmt->fetchAll(PDO::FETCH_ASSOC);
	                                foreach($r_dt as $row){
	                                    if($row['depositoid'] == $deposit){ $selected = "checked"; $depositval = $depositvalue; }else{ $selected=""; $depositval = ""; }
	                                    echo"<input type='radio' name='deposit' value='".$row['depositoid']."' ".$selected.">  ".$row['name'];
	                                    if($row['depositoid'] == '2'){
	                                        echo"<div class='depositval' style='display:none'><input type='text' class='medium'  name='depositvalue' value='".$depositval."'> Deposit for IDR Amount</div>";
	                                    }else if($row['depositoid'] == '3'){
	                                        echo"<div class='depositval' style='display:none'><input type='text' class='medium'  name='depositvalue' value='".$depositval."'> Deposit for %(percentage)from total</div>";
	                                    }else if($row['depositoid'] == '4'){
	                                        echo"<div class='depositval' style='display:none'><input type='text' class='medium'  name='depositvalue' value='".$depositval."'> night(s) amount</div>";
	                                    }
	                                    echo "<br>";
	                                }
	                            }catch(PDOException $ex) {
	                                echo "Invalid Query";
	                                die();
	                            }
	                        ?>
                        	</div>
												</div>
											</div>
										</div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <br>
                        <label class="col-md-12">Publish Package :</label>
                        <div class="col-md-12">
                            <select name="published" class="form-control">
                            <?php
                                try {
                                    $stmt = $db->query("select * from published where showthis = 'y'");
                                    $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                    foreach($r_room as $row){
                                        // if($row['publishedoid'] == $published){ $selected = "selected"; }else{ $selected=""; }
                                        echo"<option value='".$row['publishedoid']."' >".$row['note']."</option>";
                                    }
                                }catch(PDOException $ex) {
                                    echo "Invalid Query";
                                    die();
                                }
                            ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
            	<div class="col-md-12 text-right">
                    <button class="btn btn-danger cancel" type="button">Cancel Editing</button>
                    <button class="btn btn-primary" type="submit">Save Package</button>
            	</div>
            </div>
   		</div>
    </form>
</section>
