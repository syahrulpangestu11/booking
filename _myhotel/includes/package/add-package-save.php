<?php
try {

	$channel = 1;
	$name = (isset($_POST['name'])) ? $_POST['name'] : "";

	$stay = (isset($_POST['stay'])) ? $_POST['stay'] : "0";
	$salefrom = date("Y-m-d", strtotime($_POST['salefrom']));
	$saleto = date("Y-m-d", strtotime($_POST['saleto']));
	$stayfrom = date("Y-m-d", strtotime($_POST['stayfrom']));
	$stayto = date("Y-m-d", strtotime($_POST['stayto']));
		$display = array();
		foreach($_POST['display'] as $key => $value){
			array_push($display , $value);
		}
	$display = implode(",", $display);
		$checkin = array();
		foreach($_POST['checkin'] as $key => $value){
			array_push($checkin , $value);
		}
	$checkin = implode(",", $checkin);
	$timefrom = date("H:i", strtotime($_POST['timefrom']));
	$timeto = date("H:i", strtotime($_POST['timeto']));
	$min_ci = $_POST['min_ci'];
	$max_ci = $_POST['max_ci'];

	$discounttype = $_POST['discounttype'];
	$discountapply = $_POST['discountapply'];
	if($discountapply == '2' or $discountapply == '3'){
		if(is_array($_POST['applyvalue'])){
				$applyvalue = array();
				foreach($_POST['applyvalue'] as $key => $value){
					array_push($applyvalue , $value);
				}
			$applyvalue = implode(",", $applyvalue);
		}else{
			$applyvalue = $_POST['applyvalue'];
		}
	}else{
		$applyvalue = "";
	}
	$discountvalue = $_POST['discountvalue'];

	$cancellationpolicy = $_POST['cancellation'];
	$deposit = $_POST['deposit'];
	$depositvalue = (isset($_POST['depositvalue'])) ? $_POST['depositvalue'] : 0;

	$headline = $_POST['headline'];
	$description = $_POST['description'];
	$servicefacilities = $_POST['servicefacilities'];
	$termcondition = $_POST['termcondition'];
	$published = $_POST['published'];

	$allowedExtension = array('jpg', 'jpeg', 'png');
	$tmp = explode('.', $_FILES['image']['name']);
	$extension = end($tmp);
	$image = '';
	if(!empty($_FILES['image'])){
		if(in_array($extension, $allowedExtension)){
			$imagename = str_replace(' ', '_', $_FILES['image']['name']);
			$imagename = str_replace('.'.$extension, '', $imagename);
			$ext = '.'.strtolower($extension);
			$image_new_name = substr($imagename, 0, 10).date('Ymd_His').$ext;
			$folder_destination = 'pict/packageimage/';

			move_uploaded_file($_FILES['image']['tmp_name'], $upload_base.$folder_destination.$image_new_name);
			$image = $web_url.'/'.$folder_destination.$image_new_name;
		}else{
			echo'<script>alert("Data image not saved.\\nInvalid file.")</script>';
		}
	}
	$popupbanner = $_POST['popupbanner'];

	if($_POST['typepackage'] != "2"){
		$allowoverbooking = 'n';
		$alwaysshown = 'n';
	}else{
		$allowoverbooking = (isset($_POST['overbook'])) ? $_POST['overbook'] : 'n';
		$alwaysshown = (isset($_POST['ignorenight'])) ? $_POST['ignorenight'] : 'n';
	}

		$stmt = $db->prepare("insert into package (hoteloid, name, stay, salefrom, saleto, bookfrom, bookto, displayed, checkinon, timefrom, timeto, discounttypeoid, discountapplyoid, applyvalue, discountvalue, cancellationpolicyoid, publishedoid, headline, description, servicefacilities, termcondition, depositoid, depositvalue, packageimage, min_bef_ci, max_bef_ci, popupbanner, `multiply`, typepackage, overbooking, alwaysshown, multiplier, multiplier_val) values (:a, :b, :c, :d, :e, :f, :g, :h, :i, :j, :k, :l, :m, :n, :o, :p, :q, :r, :s, :t, :u, :v, :w, :x, :y, :z, :aa, :ab, :ac, :ad, :ae, :af,:ag)");
		$stmt->execute(array(':a' => $hoteloid, ':b' => $name, ':c' => $stay, ':d' => $salefrom, ':e' => $saleto, ':f' => $stayfrom, ':g' => $stayto, ':h' => $display, ':i' => $checkin, ':j' => $timefrom, ':k' => $timeto, ':l' => $discounttype, ':m' => $discountapply, ':n' => $applyvalue, ':o' => $discountvalue, ':p' => $cancellationpolicy, ':q' => $published , ':r' => $headline, ':s' => $description, ':t' => $servicefacilities, ':u' => $termcondition, ':v' => $deposit, ':w' => $depositvalue, ':x' => $image, ':y' => $min_ci, ':z' => $max_ci, ':aa' => $popupbanne, ':ab' => $_POST['multiply'], ':ac' => $_POST['typepackage'], ':ad' => $allowoverbooking, ':ae' => $alwaysshown, ':af' => $_POST['multiplier'], ':ag' => $_POST['multiplier_val']));
		$packageoid = $db->lastInsertId();

	/* GEO LOCATION SHOW / NOW SHOW ---------------------------------------*/

	if(isset($_POST['country'])){
		foreach($_POST['country'] as $key => $countryoid){
			$stmt = $db->prepare("insert into packagegeo (packageoid, countryoid) values (:a, :b)");
			$stmt->execute(array(':a' => $packageoid , ':b' => $countryoid));
		}
	}

	if(isset($_POST['noshow_country'])){
		foreach($_POST['noshow_country'] as $key => $noshow_countryoid){
			$stmt = $db->prepare("insert into packagegeo_noshow (packageoid, countryoid) values (:a, :b)");
			$stmt->execute(array(':a' => $packageoid , ':b' => $noshow_countryoid));
		}
	}

	/* RATE PLAN ---------------------------------------*/

	if(isset($_POST['rateplan'])){
		foreach($_POST['rateplan'] as $key => $rateplan){
			$stmt = $db->prepare("insert into packageapply (packageoid, roomofferoid, channeloid, publishedoid) values (:a, :b, :c, :d)");
			$stmt->execute(array(':a' => $packageoid , ':b' => $rateplan, ':c' => $channel, ':d' => '1'));
			$packageapplyoid = $db->lastInsertId();

			foreach($_POST['from-'.$rateplan] as $key => $value){
				$startperiode = date('Y-m-d', strtotime($_POST['from-'.$rateplan][$key]));
				$endperiode = date('Y-m-d', strtotime($_POST['to-'.$rateplan][$key]));
				$rate = $_POST['rate-'.$rateplan][$key];
				$priority = $_POST['priority-'.$rateplan][$key];

				$stmt = $db->prepare("insert into packagerate (packageapplyoid, startdate, enddate, rate, priority) values (:a, :b, :c, :d, :e)");
				$stmt->execute(array(':a' => $packageapplyoid , ':b' => $startperiode, ':c' => $endperiode, ':d' => $rate, ':e' => $priority));
			}
		}
	}

	/* TERM & HEADLINE ICON ---------------------------------------*/

	$type = 'package';
	foreach ($_POST['icon'] as $key => $value) {
		$iconoid = $value;
		try {
			$stmt = $db->prepare("insert into termheadlinepromo (type, id, termheadlineoid) values (:a, :b, :c)");
			$stmt->execute(array(':a' => $type, ':b' => $packageoid, ':c' => $iconoid));
		}catch(PDOException $ex) {
			echo "Invalid Query";
			print($ex);
			die();
		}
	}

	header("Location: ". $base_url ."/package");

}catch(Exception $ex) {
	print($ex->getMessage());
	die();
}
?>
