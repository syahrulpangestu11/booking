<?php
try {
	$packageoid = $_POST['packageoid'];
	$channel = 1;
	$name = (isset($_POST['name'])) ? $_POST['name'] : "";

	$stay = (isset($_POST['stay'])) ? $_POST['stay'] : "0";
	$salefrom = date("Y-m-d", strtotime($_POST['salefrom']));
	$saleto = date("Y-m-d", strtotime($_POST['saleto']));
	$stayfrom = date("Y-m-d", strtotime($_POST['stayfrom']));
	$stayto = date("Y-m-d", strtotime($_POST['stayto']));
		$display = array();
		foreach($_POST['display'] as $key => $value){
			array_push($display , $value);
		}
	$display = implode(",", $display);
		$checkin = array();
		foreach($_POST['checkin'] as $key => $value){
			array_push($checkin , $value);
		}
	$checkin = implode(",", $checkin);
	$timefrom = date("H:i", strtotime($_POST['timefrom']));
	$timeto = date("H:i", strtotime($_POST['timeto']));
	$min_ci = $_POST['min_ci'];
	$max_ci = $_POST['max_ci'];

	$discounttype = $_POST['discounttype'];
	$discountapply = $_POST['discountapply'];
	if($discountapply == '2' or $discountapply == '3'){
		if(is_array($_POST['applyvalue'])){
				$applyvalue = array();
				foreach($_POST['applyvalue'] as $key => $value){
					array_push($applyvalue , $value);
				}
			$applyvalue = implode(",", $applyvalue);
		}else{
			$applyvalue = $_POST['applyvalue'];
		}
	}else{
		$applyvalue = "";
	}
	$discountvalue = $_POST['discountvalue'];

	$cancellationpolicy = $_POST['cancellation'];
	$deposit = $_POST['deposit'];
	$depositvalue = (isset($_POST['depositvalue'])) ? $_POST['depositvalue'] : 0;

	$headline = $_POST['headline'];
	$description = $_POST['description'];
	$servicefacilities = $_POST['servicefacilities'];
	$termcondition = $_POST['termcondition'];
	$published = $_POST['published'];

	$allowedExtension = array('jpg', 'jpeg', 'png');
	$tmp = explode('.', $_FILES['image']['name']);
	$extension = end($tmp);
	$image = '';
	if(!empty($_FILES['image']['name']) and isset($_FILES['image']['name'])){
		if(in_array($extension, $allowedExtension)){
			$imagename = str_replace(' ', '_', $_FILES['image']['name']);
			$imagename = str_replace('.'.$extension, '', $imagename);
			$ext = '.'.strtolower($extension);
			$image_new_name = substr($imagename, 0, 10).date('Ymd_His').$ext;
			$folder_destination = 'pict/packageimage/';

			move_uploaded_file($_FILES['image']['tmp_name'], $upload_base.$folder_destination.$image_new_name);
			$image = $web_url.'/'.$folder_destination.$image_new_name;

			try {
				$stmt = $db->prepare("update package set packageimage = :a  where packageoid = :b");
				$stmt->execute(array(':a' => $image, ':b' => $packageoid));
			}catch(PDOException $ex) {
				echo "Invalid Query"; print($ex);
				die();
			}

		}else{
			echo'<script>alert("Data not saved.\\nInvalid file.")</script>';
		}
	}

	$min_ci = $_POST['min_ci'];
	$max_ci = $_POST['max_ci'];

	$popupbanner = $_POST['popupbanner'];

	if($_POST['typepackage'] != "2"){
		$allowoverbooking = 'n';
		$alwaysshown = 'n';
	}else{
		$allowoverbooking = (isset($_POST['overbook'])) ? $_POST['overbook'] : 'n';
		$alwaysshown = (isset($_POST['ignorenight'])) ? $_POST['ignorenight'] : 'n';
	}

	$stmt = $db->prepare("update package set hoteloid = :a, name = :b, stay = :c, salefrom = :d, saleto = :e, bookfrom = :f, bookto = :g, displayed = :h, checkinon = :i, timefrom = :j, timeto = :k, discounttypeoid = :l, discountapplyoid = :m, applyvalue = :n, discountvalue = :o, cancellationpolicyoid = :p, publishedoid = :q, headline = :r, description = :s, servicefacilities = :t, termcondition = :u, depositoid = :v, depositvalue = :w, min_bef_ci = :y, max_bef_ci = :z, popupbanner = :aa, `multiply` = :ab, typepackage = :ac, overbooking = :ad, alwaysshown = :ae, multiplier = :af, multiplier_val = :ag where packageoid = :id");
	$stmt->execute(array(':a' => $hoteloid, ':b' => $name, ':c' => $stay, ':d' => $salefrom, ':e' => $saleto, ':f' => $stayfrom, ':g' => $stayto, ':h' => $display, ':i' => $checkin, ':j' => $timefrom, ':k' => $timeto, ':l' => $discounttype, ':m' => $discountapply, ':n' => $applyvalue, ':o' => $discountvalue, ':p' => $cancellationpolicy, ':q' => $published , ':r' => $headline, ':s' => $description, ':t' => $servicefacilities, ':u' => $termcondition, ':v' => $deposit, ':w' => $depositvalue, ':y' => $min_ci, ':z' => $max_ci, ':aa' => $popupbanner, ':ab' => $_POST['multiply'], ':ac' => $_POST['typepackage'], ':ad' => $allowoverbooking, ':ae' => $alwaysshown, ':af' => $_POST['multiplier'], ':ag' => $_POST['multiplier_val'], ':id' => $packageoid));

	/* TERM & HEADLINE ICON -----------------------------------------*/

	$type = 'package';
	if(count($_POST['icon'])>0){
		if(count($_POST['icon']) == 1){
			$remaining_icon = "'".$_POST['icon']."'";
		}else{
			$remaining_icon = "'".implode("','", $_POST['icon'])."'";
		}

		$query_delete_icon = "DELETE FROM `termheadlinepackage` WHERE termheadlineoid not in (".$remaining_icon.") and `id`='".$packageoid."' and type = '".$type."'";
		$stmt = $db->query($query_delete_icon);

		foreach ($_POST['icon'] as $key => $value){
			$iconoid = $value;
			$s_check_icon = "select * from termheadlinepackage where type = '".$type."' and id = '".$packageoid."' and termheadlineoid = '".$iconoid."'";
			$stmt = $db->query($s_check_icon);
			$row_count = $stmt->rowCount();
			if($row_count == 0){
				try {
					$stmt = $db->prepare("insert into termheadlinepackage (type, id, termheadlineoid) values (:a,:b,:c)");
					$stmt->execute(array(':a' => $type, ':b' => $packageoid, ':c' => $iconoid));
				}catch(PDOException $ex) {
					echo "Invalid Query";
					print($ex);
					die();
				}
			}
		}
	}else{
		$query_delete_icon = "DELETE FROM `termheadlinepackage` WHERE `id`='".$packageoid."' and type = '".$type."'";
		$stmt = $db->query($query_delete_icon);
	}

	/* GEO LOCATION COUNTRY ------------------------------------------*/

	if(isset($_POST['applycountry'])){
		$geo_country_not_remove = implode("','", $_POST['applycountry']);
		$query_remove_country = "delete from packagegeo where packageoid = '".$packageoid."' and packagegeooid not in ('".$geo_country_not_remove."')";
	}else{
		$query_remove_country = "delete from packagegeo where packageoid = '".$packageoid."'";
	}
	$stmt = $db->query($query_remove_country);


	if(isset($_POST['country'])){
		foreach($_POST['country'] as $key => $countryoid){
			$stmt = $db->prepare("insert into packagegeo (packageoid, countryoid) values (:a,:b)");
			$stmt->execute(array(':a' => $packageoid , ':b' => $countryoid));
		}
	}

	/* NO SHOW <-- GEO LOCATION COUNTRY ------------------------------------------*/

	if(isset($_POST['apply_noshow_country'])){
		$geo_country_not_remove = implode("','", $_POST['apply_noshow_country']);
		$query_remove_country = "delete from packagegeo_noshow where packageoid = '".$packageoid."' and packagegeo_noshowoid not in ('".$geo_country_not_remove."')";
	}else{
		$query_remove_country = "delete from packagegeo_noshow where packageoid = '".$packageoid."'";
	}
	$stmt = $db->query($query_remove_country);


	if(isset($_POST['noshow_country'])){
		foreach($_POST['noshow_country'] as $key => $countryoid){
			$stmt = $db->prepare("insert into packagegeo_noshow (packageoid, countryoid) values (:a,:b)");
			$stmt->execute(array(':a' => $packageoid , ':b' => $countryoid));
		}
	}

	/* RATE PLAN ---------------------------------------*/
	if(isset($_POST['rateplan'])){
		$rateplan_not_remove = implode("','", $_POST['rateplan']);
		$query_remove_rate = "delete packagerate from packagerate inner join packageapply using (packageapplyoid) where packageoid = '".$packageoid."' and roomofferoid not in ('".$rateplan_not_remove."')";
		$query_remove_rateplan = "delete from packageapply where packageoid = '".$packageoid."' and roomofferoid not in ('".$rateplan_not_remove."')";
	}else{
		$query_remove_rate = "delete packagerate from packagerate inner join packageapply using (packageapplyoid) where packageoid = '".$packageoid."'";
		$query_remove_rateplan = "delete from packageapply where packageoid = '".$packageoid."'";
	}
	$stmt = $db->query($query_remove_rate);
	$stmt = $db->query($query_remove_rateplan);


	if(isset($_POST['rateplan'])){
		foreach($_POST['rateplan'] as $key => $rateplan){

			$stmt = $db->query("select count(packageapplyoid) as found, packageapplyoid from packageapply where packageoid = '".$packageoid."' and  packageoid = '".$packageoid."' and roomofferoid = '".$rateplan."'");
			$existrateplan = $stmt->fetch(PDO::FETCH_ASSOC);

			if($existrateplan['found'] == 0){
				$stmt = $db->prepare("insert into packageapply (packageoid, roomofferoid, channeloid, publishedoid) values (:a, :b, :c, :d)");
				$stmt->execute(array(':a' => $packageoid , ':b' => $rateplan, ':c' => $channel, ':d' => '1'));
				$packageapplyoid = $db->lastInsertId();
			}else{
				$packageapplyoid = $existrateplan['packageapplyoid'];
			}

			if(isset($_POST['c_pr-'.$rateplan])){
				$rate_not_remove = implode("','", $_POST['c_pr-'.$rateplan]);
				$query_remove_rate = "delete from packagerate where packageapplyoid = '".$packageapplyoid."' and packagerateoid not in ('".$rate_not_remove."')";
			}else{
				$query_remove_rate = "delete from packagerate where packageapplyoid = '".$packageapplyoid."'";
			}
			$stmt = $db->query($query_remove_rate);

			if(isset($_POST['c_pr-'.$rateplan])){
				foreach($_POST['c_pr-'.$rateplan] as $key => $value){
					$startperiode = date('Y-m-d', strtotime($_POST['c_from-'.$rateplan][$key]));
					$endperiode = date('Y-m-d', strtotime($_POST['c_to-'.$rateplan][$key]));
					$rate = $_POST['c_rate-'.$rateplan][$key];
					$priority = $_POST['c_priority-'.$rateplan][$key];
					$packagerateoid = $_POST['c_pr-'.$rateplan][$key];

					$stmt = $db->prepare("update packagerate set startdate = :a, enddate = :b, rate =:c, priority = :d where packagerateoid = :id");
					$stmt->execute(array(':a' => $startperiode, ':b' => $endperiode, ':c' => $rate, ':d' => $priority, ':id' => $packagerateoid));
				}
			}

			if(isset($_POST['from-'.$rateplan])){
				foreach($_POST['from-'.$rateplan] as $key => $value){
					$startperiode = date('Y-m-d', strtotime($_POST['from-'.$rateplan][$key]));
					$endperiode = date('Y-m-d', strtotime($_POST['to-'.$rateplan][$key]));
					$rate = $_POST['rate-'.$rateplan][$key];
					$priority = $_POST['priority-'.$rateplan][$key];

					$stmt = $db->prepare("insert into packagerate (packageapplyoid, startdate, enddate, rate, priority) values (:a, :b, :c, :d, :e)");
					$stmt->execute(array(':a' => $packageapplyoid , ':b' => $startperiode, ':c' => $endperiode, ':d' => $rate, ':e' => $priority));
				}
			}
		}
	}

	header("Location: ". $base_url ."/package");

}catch(Exception $ex) {
	print($ex->getMessage());
	die();
}
?>
