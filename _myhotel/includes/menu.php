<div class="menu-side">
    <section class="parent">
        <ul class="parent-menu">
        <?php
        switch($_SESSION['_type'] ){
          case 'agent' :
            include('menu-agent.php');
            break;
          default :
            // if super admin and marketing manager (not managed hotel yet)
            if(in_array($_SESSION['_typeusr'], array(1,2) ) and empty($_SESSION['_hotel'])){
              include('menu-su.php');
            // if chain manager (not managed hotel yet)
            }else if(in_array($_SESSION['_typeusr'], array(4)) and empty($_SESSION['_hotel'])){
              include('menu-chain.php');
            // marketing manager user
            }else if(in_array($_SESSION['_typeusr'], array(5)) and empty($_SESSION['_hotel'])){
              include('menu-management.php');
            // terminated user
            }else if($_SESSION['_typeusr'] == "7"){
              include('menu-terminated.php');
            // availability user
            }else if($_SESSION['_typeusr'] == "6"){
              include('menu-availability.php');
            // unit / property user
            }else if($_SESSION['_typeusr'] == "3" or ($_SESSION['_typeusr'] != "3" and !empty($_SESSION['_hotel']))){
              include('menu-hotel.php');
            }
            break;
        }

        /* PMS MENU ================================================================================*/
        // check if hotel has feature pms.
        /*/
        if(isset($_SESSION['_hotel']) and !empty($_SESSION['_hotel'])){
          $q_feature_pms = "SELECT count(hotelfeatureoid) as `allowpms` from `hotelfeature` where `hoteloid` = '".$_SESSION['_hotel']."' and `featureoid` = '2'";
        }else{
          $q_feature_pms = "select count(hotelfeatureoid) as `allowpms` from hotelfeature inner join hotel h using (hoteloid) inner join chain c using (chainoid) where (h.chainoid in (select oid from userassign where type = 'chainoid' and useroid = '".$_SESSION['_oid']."') or h.hoteloid in (select oid from userassign where type = 'hoteloid' and useroid = '".$_SESSION['_oid']."')) and h.publishedoid = '1' and featureoid = '2'";
        }

        $stmt = $db->query($q_feature_pms);
        $feature_pms = $stmt->fetch(PDO::FETCH_ASSOC);
        if($feature_pms['allowpms'] > 0){ $allowpms = true; }else{ $allowpms = false; }

        if($allowpms == true and !empty($_SESSION['_typepmsusr'])){
          switch($_SESSION['_typepmsusr']){
            case 1 :
              include('menu-pms-am.php');
              break;
            case 2 :
              include('menu-pms-vm.php');
              break;
            case 3 :
              include('menu-pms-am.php');
              break;
            case 4 :
              include('menu-pms-vm.php');
              break;
            case 5 :
              include('menu-pms-owner.php');
              break;
            case 6 :
              include('menu-pms-acc.php');
              break;
          }
        }//*/

        if((isset($_SESSION['_typeusr']) and ($_SESSION['_typeusr'] != "3" and $_SESSION['_typeusr'] != "7")) and !empty($_SESSION['_hotel'])){
        ?>
          <li><a class="manage"><i class="fa fa-dashboard"></i><span>Dashboard <?=$_SESSION['_type']?></span></a> </li>
        <?php
        }
        ?>
        </ul>
    </section>
    <!-- /.parent -->
</div>
