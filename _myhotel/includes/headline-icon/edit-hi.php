<section class="content-header">
    <h1>
       	Headline Icon
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?=$base_url?>/headline-icon"><i class="fa fa-table"></i> Headline Icon</a></li>
        <li class="active">Edit</li>
    </ol>
</section>
<script type="text/javascript">
$(function() {
	$('textarea').trumbowyg({
		btns: ['viewHTML',
		  '|', 'btnGrp-design',
		  '|', 'link',
		  '|', 'btnGrp-justify',
		  '|', 'btnGrp-lists'],
		fullscreenable: false
	});
});
</script>
<style type="text/css">.trumbowyg-box{ padding-top:50px; }</style>
<?php
	$cpoid = $_GET['cpoid'];
	try {
		$stmt = $db->query("SELECT th.*, i.* from termheadline th left join icon i using (iconoid) where termheadlineoid = '".$cpoid."'");
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
			$r_headline = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_headline as $row){
				$choteloid = $row['hoteloid'];
				$name = $row['icon_title'];
				$description = $row['description'];
				
				if($row['iconoid'] == 0){
                    $icon_src = $row['term_icon_src'];
                }else{
                    $icon_src = $row['icon_src'];
                }
			}
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}
?>
<section class="content" id="basic-info">
	<div class="row">
        <div class="box box-form">
            <form method="post" enctype="multipart/form-data" id="data-input" class="form-box" action="<?=$base_url?>/headline-icon/edit-process">
            <input type="hidden" name="hoteloid" value="<?php echo $hoteloid; ?>">
            <input type="hidden" name="choteloid" value="<?php echo $choteloid; ?>">
            <input type="hidden" name="cpoid" value="<?php echo $cpoid; ?>">
            <ul class="inline form-input">
                <li>
                    <div class="side-left"><label>Headline Icon Title</label></div>
                    <div class="side-right"><input type="text" class="input-text" name="name" value="<?php echo $name;?>"></div>
                    <div class="clear"></div>
                </li>
                <li>
                    <div class="side-left"><label>Headline Icon Description</label></div>
                    <div class="side-right"><textarea name="description" style="height:100px"><?php echo $description;?></textarea></div>
                    <div class="clear"></div>
                </li>
                <li>
                    <div class="side-left"><label>Headline Icon Image</label></div>
                    <div class="side-right">
                        <input type='file' class='input-text input-image' name='image' accept='image/*'><br>
                        <span>Preview: </span><br>
                        <img class='input-image-preview' id='preview' src='<?php echo $icon_src;?>' alt="" style="max-height:100px"/><br><br>
                    </div>
                    <div class="clear"></div>
                </li>
                <li>
                    <div class="side-left">&nbsp;</div>
                    <div class="side-right"><button type="submit" class="submit">Save</button>&nbsp;<button class="pure-button red cancel" type="button">Cancel</button></div>
                    <div class="clear"></div>
                </li>
                <div class="clear"></div>
            </ul>
            </form>
   		</div>
    </div>
</section>
<script>
  function readURL(input, previewTarget) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            previewTarget
                .attr('src', e.target.result)
                .show();
        };
        reader.readAsDataURL(input.files[0]);
    }
  }

  $(document).ready(function(){
    <?php if(empty($icon_src)){?>
    $('.input-image-preview').hide();
    <?php }?>
  });

  $('.input-image').each(function(){
    $('.input-image').change(function() {
      var thisElem = $(this);
      var thisElemJS = this;
      var previewTarget = thisElem.parent().find('.input-image-preview');
      var sizeKB = thisElemJS.files[0].size / 1024;
      var img = new Image();
      img.src = window.URL.createObjectURL( this.files[0] );
      img.onload = function() {
        var width = img.naturalWidth,
            height = img.naturalHeight;

        if(sizeKB > 200){
            // alert("Your image more than 200 KB, please compress or resize your image.");
            $dialogNotice.html("Your image more than 200 KB, please compress or resize your image");
            $dialogNotice.dialog("open");
            thisElem.val("");
            previewTarget.attr('src',"#");
        }else{
            var ext = thisElem.val().split('.').pop().toLowerCase();
            if($.inArray(ext, ['jpg','jpeg', 'png']) == -1) {
              // alert('Invalid extension! Please make sure you upload image file type (.jpg , .jpeg , .png).');
              $dialogNotice.html("Invalid extension! Please make sure you upload image file type (.jpg , .jpeg , .png)");
              $dialogNotice.dialog("open");
              thisElem.val("");
              previewTarget.attr('src',"#");
            }else{
              readURL(thisElemJS, previewTarget);
              previewTarget.show();
            }
        }
      }

    });
  });
  
  var $dialogNotice = $('<div id="dialog-notice"></div>')
	.dialog({
		autoOpen: false,
		title: 'Notification',
		buttons: {
			"Ok": function(){
				$(this).dialog( "close" );
			}
		}
	});
</script>
