<?php
	$roomoid = $_REQUEST['rt'];
	
	$sdata = "select r.name from room r where roomoid='".$roomoid."'";
	$qdata = mysql_query($sdata);
   	$rd = mysql_fetch_array($qdata);
	
	$roomname = $rd['name'];
?>
<section class="content-header">
    <h1>
        Add Room Allotment
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Allotments &amp; Rates</a></li>
        <li><a href="#"><i class="fa"></i> Room Allotment</a></li>
        <li class="active">Add</li>
    </ol>
</section>
<section class="content">
   
    <div class="row">
        <div class="box box-form">
            <h1>Add new allotment</h1>
            <form method="post" enctype="multipart/form-data" action="<?php echo $base_url; ?>/<?php echo $uri2; ?>/addprocess">
            <div class="side-left">
                <div class="form-input">
                    <input type="hidden" name="roomoid" value="<?php echo $roomoid; ?>"  />
                	<label>Room Type</label>
                    <select name="roomoid" class="input-select">
                        <?php
                        $sql = "select * from room r inner join hotel h using (hoteloid) where h.hoteloid = '".$hoteloid."'";
                        $query = mysql_query($sql);
                        while($row = mysql_fetch_array($query)){
							if($row['roomoid'] == $roomoid){ $selected = "selected"; }else{ $selected=""; }
                        ?>
                        <option value="<?php echo $row['roomoid']; ?>" <?php echo $selected; ?>><?php echo $row['name']; ?></option>
                        <?php		
                        }
                        ?>
                    </select>
                    <label>Start Date</label>
                    <input type="text" class="input-text" name="startdate" id="startdate">
                    <label>End Date</label>
                    <input type="text" class="input-text" name="enddate" id="enddate">
                    <label>Allotment</label> 
                    <input type="text" class="input-text" name="allotment">
                </div>
            </div>
            <div class="side-right">
                <div class="form-input">
                    <label>Priority</label> 
                    <select name="priority" class="input-select">
                        <?php
                        for($i=10;$i>=1;$i--){
                        ?>
                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                        <?php		
                        }
                        ?>
                    </select>
                    <br>
                    <label>Publish this allotment?</label> 
                    <select name="published" class="input-select">
                        <?php
                        $sql = "select * from published";
                        $query = mysql_query($sql);
                        while($row = mysql_fetch_array($query)){
                        ?>
                        <option value="<?php echo $row['publishedoid']; ?>"><?php echo $row['note']; ?></option>
                        <?php		
                        }
                        ?>
                    </select>
                    <br>
                    <button type="submit">Submit</button>
                </div>
            </div>
            </form>
            <div class="clear"></div>
       	</div>
    </div>
    
</section>