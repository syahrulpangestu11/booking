<script type="text/javascript">
$(function(){

  $(document).ready(function(){
    $('#select-roomoid').change();
  });

	$('body').on('click','button[fnc=fromexcel]', function(){
      	document.location.href = "<?php echo $base_url; ?>/load-tarif/excel-import";
	});

	$('body').on('click','button#upload-rate', function(){
    // minrate	= parseFloat($('select[name="roomoffer"] option:selected').attr('minrate'));
    minrate	= parseFloat($('#select-roomofferoid option:selected').attr('minrate'));
		double 	= parseFloat($('input[name=double]').val());
		if(double < minrate || isNaN(double)){
			$("div.dialog-notification").html('Your input rate less than room minimum rate.<br/>Please change your rate more than '+minrate+'.')
			dialog.dialog("open");
		}else{
      $('#upload-type').val('upload-rate');
			$('form#data-input').submit();
		}
	});


  $('body').on('click','button#upload-allotment', function(){
    $('#upload-type').val('upload-allotment');
		$('form#data-input').submit();
	});


    var dialog = $("<div class='dialog-notification' style='height:auto'></div>").dialog({
      autoOpen: false,
      modal: true,
	  title: 'Notification',
      buttons: {
        'Ok': function() {
          dialog.dialog( "close" );
        }
      },
      close: function() {
        dialog.dialog( "close" );
      }
    });


    $('#select-roomoid').change(function(){
      var thisValue = $(this).val();
      var input_roomoffer = thisValue + '-' + $('#select-roomofferoid').val();

      if(thisValue!=''){
        $('.punya-roomoid').show();
        $('.punya-roomofferoid').hide();
        $('#input-roomoffer').val(input_roomoffer);
      }else{
        $('.punya-roomoid').hide();
        $('.punya-roomofferoid').hide();
      }

      $.ajax({
        url: '<?php echo $base_url; ?>/includes/load-tarif/ajax-rateplan.php',
        type: 'post',
        data: { pid : thisValue},
        success: function(data) {
          $('#select-roomofferoid').html(data);
        }
      });
    });

    $('#select-roomofferoid').change(function(){
      var thisValue = $(this).val();
      var input_roomoffer = $('#select-roomoid').val() + '-' + thisValue;

      if(thisValue!=''){
        $('.punya-roomoid').hide();
        $('.punya-roomofferoid').show();
        $('#input-roomoffer').val(input_roomoffer);

        $('select[name="master-rate"]').children("option").each(function(){
            var y = $(this).attr('x');
            if(y == undefined){
              $(this).show();
            }else if(thisValue != y){
              $(this).hide();
            }else{
              $(this).show();
            }
        });
      }else{
        $('.punya-roomoid').show();
        $('.punya-roomofferoid').hide();
      }
    });

    $('select[name=master-rate]').change(function(){
      var thisValue = $(this).val();
      valinput = thisValue.split("|");

      $('select[name=currency] option[value='+valinput[0]+']').attr('selected',true);
      $('input[name=double]').val(valinput[1]);
    });

    $( "#startdate-2m-today" ).datepicker({
		defaultDate: "+1d",
		changeMonth: true, changeYear:true, minDate:0,
		onClose: function( selectedDate ) {
            $( "#enddate-2m-today" ).datepicker( "option", "minDate", selectedDate );

            var endDate = new Date(selectedDate);
            endDate.setDate(endDate.getDate()+180);
            $( "#enddate-2m-today" ).datepicker( "option", "maxDate", endDate );
		}
    });
    $("#enddate-2m-today").datepicker({
      	defaultDate: "+1d",
      	changeMonth: true, changeYear:true, minDate:0, maxDate: "+6m",
     	onClose: function( selectedDate ) {
        	$( "#startdate-2m-today" ).datepicker( "option", "maxDate", selectedDate );
      	}
    });
});
</script>
<style>
.ui-widget-overlay {
    background: #000000;
    opacity: .5;
    filter: Alpha(Opacity=30);
}
</style>
