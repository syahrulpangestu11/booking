<?php
	$roomoid = $_REQUEST['rt'];
	$allotmentoid = $_REQUEST['ra'];
	
	$sdata = "select *, r.name from roomallotment ra inner join room r using (roomoid) where allotmentoid='".$allotmentoid."'";
	$qdata = mysql_query($sdata);
   	$rd = mysql_fetch_array($qdata);
	
	$roomname = $rd['name'];
	
	$startdate = date("d F Y", strtotime($rd['startdate']));
	$enddate = date("d F Y", strtotime($rd['enddate']));
	$allotment = $rd['allotment'];
?>
<section class="content-header">
    <h1>
        Edit Room Allotment
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Allotments &amp; Rates</a></li>
        <li><a href="#"><i class="fa"></i> Room Allotment</a></li>
        <li class="active">Edit</li>
    </ol>
</section>
<section class="content">
   
    <div class="row">
        <div class="box box-form">
            <h1>Edit allotment</h1>
            <form method="post" enctype="multipart/form-data" action="<?php echo $base_url; ?>/<?php echo $uri2; ?>/editprocess">
            <div class="side-left">
                <div class="form-input">
                    <input type="hidden" name="allotmentoid" value="<?php echo $allotmentoid; ?>"  />
                	<label>Room Type</label>
                    <select name="roomoid" class="input-select">
                        <?php
                        $sql = "select * from room r inner join hotel h using (hoteloid) where h.hoteloid = '".$hoteloid."'";
                        $query = mysql_query($sql);
                        while($row = mysql_fetch_array($query)){
							if($row['roomoid'] == $rd['roomoid']){ $selected = "selected"; }else{ $selected=""; }
                        ?>
                        <option value="<?php echo $row['roomoid']; ?>" <?php echo $selected; ?>><?php echo $row['name']; ?></option>
                        <?php		
                        }
                        ?>
                    </select>
                    <label>Start Date</label>
                    <input type="text" class="input-text" name="startdate" id="startdate" value="<?php echo $startdate; ?>">
                    <label>End Date</label>
                    <input type="text" class="input-text" name="enddate" id="enddate" value="<?php echo $enddate; ?>">
                    <label>Allotment</label> 
                    <input type="text" class="input-text" name="allotment" value="<?php echo $allotment; ?>">
                </div>
            </div>
            <div class="side-right">
                <div class="form-input">
                    <label>Priority</label> 
                    <select name="priority" class="input-select">
                        <?php
                        for($i=10;$i>=1;$i--){
							if($i == $rd['priority']){ $selected = "selected"; }else{ $selected=""; }
                        ?>
                        <option value="<?php echo $i; ?>" <?php echo $selected; ?>><?php echo $i; ?></option>
                        <?php		
                        }
                        ?>
                    </select>
                    <br>
                    <label>Publish this allotment?</label> 
                    <select name="published" class="input-select">
                        <?php
                        $sql = "select * from published";
                        $query = mysql_query($sql);
                        while($row = mysql_fetch_array($query)){
							if($row['publishedoid'] == $rd['publishedoid']){ $selected = "selected"; }else{ $selected=""; }
                        ?>
                        <option value="<?php echo $row['publishedoid']; ?>" <?php echo $selected; ?>><?php echo $row['note']; ?></option>
                        <?php		
                        }
                        ?>
                    </select>
                    <br>
                    <button type="submit">Update</button>
                </div>
            </div>
            </form>
            <div class="clear"></div>
       	</div>
    </div>
    
</section>