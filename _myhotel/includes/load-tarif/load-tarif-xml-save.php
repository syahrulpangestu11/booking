<script type="text/javascript">
$(function(){
   $("#dialog").dialog({
      autoOpen: false,
	  buttons: { "Ok": redirect },
	  close: function(event, ui) { redirect }
   });
   
   function redirect(){
	   $(location).attr("href", "<?php echo $base_url; ?>/load-tarif");
   }
});
</script>
<?php
	// echo "<pre>";
	// print_r($_POST);
	// echo "<pre>";
	// die;

	// $ro = explode('-',$_POST['roomoffer']);
	// $roomoid = $ro[0]; $roomofferoid = $ro[1];

	$uploadtype = $_POST['uploadtype'];

	$roomoid = $_POST['roomoid'];
	$roomofferoid = $_POST['roomofferoid'];
	
	$selectedday = array();
	foreach($_POST['day'] as $day){
		array_push($selectedday , $day);
	}
	$channellist = array();
	array_push($channellist , $_POST['channeloid']);
	
	$startdate = date("Y-m-d", strtotime($_POST['startdate']));
	$enddate = date("Y-m-d", strtotime($_POST['enddate']));
	
	try {	
		$stmt = $db->query("SELECT DATEDIFF('".$enddate."','".$startdate."') AS DiffDate");
		$r_datediff = $stmt->fetchAll(PDO::FETCH_ASSOC);
		foreach($r_datediff as $row){
			$diff = $row['DiffDate'];
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}
	
	$selectedday = array();
	foreach($_POST['day'] as $day){
		array_push($selectedday , $day);
	}
	if(isset($_POST['extendedpolicy'])){ $extendedpolicy = $_POST['extendedpolicy']; }else{ $fee = "n"; }
	$single = $_POST['single'];
	$double = $_POST['double'];
	$extrabed = $_POST['extrabed'];
	$currency = $_POST['currency'];
	$allotment = $_POST['allotment'];
	$topup = $_POST['topup'];
	if(isset($_POST['breakfast'])){ $breakfast = $_POST['breakfast']; }else{ $fee = "n"; }
	$minstay = $_POST['minstay'];
	$maxstay = $_POST['maxstay'];
	
	$elementName = array("extendedpolicy", "netsingle", "netdouble", "netextrabed", "single", "double", "extrabed", "currency", "topup", "breakfast", "minstay", "maxstay", "blackout", "surcharge",);
	$elementNodes = array($extendedpolicy, $single, $double, $extrabed, $single, $double, $extrabed, $currency, $topup, $breakfast, $minstay, $maxstay, "n", "n");
	
	try{
		$path = "data-xml/tarif.xml";
		include('new_load_tarif.php');
?>
    <div id="dialog" title="Confirmation Notice">
      <p>Data succesfully added</p>
    </div>
    <script type="text/javascript">
    $(function(){
        $( document ).ready(function() {
            $("#dialog").dialog("open");
        });
    });
    </script>
    <?php
	}catch(PDOException $ex) {
		echo "[ERROR]<br>".$ex;
	?>
    <div id="dialog" title="Error Notification">
      <p>We&rsquo;re very sorry, we can't save your data right now.</p>
    </div>
    <script type="text/javascript">
    $(function(){
        $( document ).ready(function() {
            $("#dialog").dialog("open");
        });
    });
    </script>
<?php
	}
?>