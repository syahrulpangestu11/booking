<h1>Add new allotment</h1>
<div class="side-left">
    <div class="form-input">
        <input type="hidden" name="roomoid"  />
        <label>Start Date</label>
        <input type="text" class="input-text" name="startdate" id="startdate">
        <label>End Date</label>
        <input type="text" class="input-text" name="enddate" id="enddate">
    </div>
</div>
<div class="side-right">
    <div class="form-input">
        <label>Tax</label> 
        <input type="text" class="input-text" name="allotment">
        <label>Priority</label> 
        <select name="priority" class="input-select">
			<?php
            for($i=10;$i>=1;$i--){
            ?>
            <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
            <?php		
            }
            ?>
        </select>
		<br>
        <button type="submit">Submit new allotment</button>
    </div>
</div>
<div class="clear"></div>
