<section class="content-header">
    <h1>
        Dashboard
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>
<?php
	if (empty($_SESSION['_hotel']) and $_SESSION['_typeusr'] == "4"){
		echo "<section class='content'>";
		include('dashboard/view-dashboard-chain.php');
		echo "</section>";
	}else if ($_SESSION['_typeusr'] == "5" and $_SESSION['_hotel'] == "0"){
		include('management/dashboard/management-dashboard.php');
	}elseif (empty($_SESSION['_hotel']) and $_SESSION['_typeusr'] != "3"){
		echo "<section class='content'>";
		include('dashboard/view-dashboard-su.php');
		echo "</section>";
	}else{
		echo "<section class='content'>";
		include('dashboard/view-dashboard-hotel.php');
		echo "</section>";
	}
?>

