<?php include('includes/bootstrap.php'); ?>
<script type="text/javascript">
$(function(){

	function submitDataCallback(url, forminput){
		$.ajax({
			url: url,
			type: 'post',
			data: forminput.serialize(),
			success: function(data) {
				if(data == "1"){
					$dialogNotice.html("Data succesfully updated");
					$dialogNotice.dialog("open");
				}else{
					$dialogNotice.html("We&rsquo;re very sorry, we can't save your data right now"+data);
					$dialogNotice.dialog("open");
				}
			}
		});
	}


	var $dialogNotice = $('<div id="dialog-notice"></div>')
	.dialog({
		autoOpen: false,
		title: 'Notification',
		buttons: {
			"Ok": function(){
				$( this ).dialog( "close" );
				$(location).attr("href", "<?php echo $base_url; ?>/user");
			}
		}
	});

	var $dialogNotif = $('<div id="dialog-notif"></div>')
	.dialog({
		autoOpen: false,
		title: 'Notification',
		buttons: {
			"Ok": function(){
				$( this ).dialog("close");
			}
		}
	});

	/************************************************************************************/
	$('body').on('click','button.add-button', function(e) {
		url = "<?php echo $base_url; ?>/faq/add";
      	$(location).attr("href", url);
	});

	$('body').on('click','.edit-button', function(e) {
		uo = $(this).attr("uo");
		url = "<?php echo $base_url; ?>/faq/edit/?uo="+uo;
		$(location).attr("href", url);
	});

	$('body').on('click','.delete-button', function(e) {
		uo = $(this).attr("uo");
		url = "<?php echo $base_url; ?>/faq/delete/?uo="+uo;
		$(location).attr("href", url);
	});

	$('body').on('click','button.cancel', function(e) {
		url = "<?php echo $base_url; ?>/faq";
      	$(location).attr("href", url);
	});

	$(document).ready(function(){
		<?php if($uri2 == "faq" and ($uri3 != "add" and $uri3 != "edit")){ ?>
		getLoadData();
		<?php }else if($uri2 == "faq" and $uri3 == "add"){ ?>
			// $('input[name="ibeuser"]').change();
			// $('input[name="pmsuser"]').change();
			// $('select[name="role"]').change();
			// $('select[name="role_pms"]').change();
		<?php } ?>

	});

	$('body').on('click','#paging button', function(e) {
		page	= $(this).attr('page');
		pagebox = $('form#data-input').children('input[name=page]');
		pagebox.val(page);
		getLoadData();
	});

	/************************************************************************************/
	function getLoadData(){
		$.ajax({
			url: "<?php echo"$base_url"; ?>/includes/master-faq/data.php",
			type: 'post',
			data: $('form#data-input').serialize()+"&usr=<?=$_SESSION['_typeusr']?>&useroid=<?=$_SESSION['_oid']?>",
			success: function(data) {
				console.log('getLoadData success');
				$("#data-box").html(data)
			}
		});
	}


	$(function(){
	   $("#dialog-error, #dialog-success").dialog({
		  autoOpen: false,
		  buttons: { "Ok": redirect },
		  close: function(event, ui) { redirect }
	   });

	   function redirect(){
		   $(location).attr("href", "<?php echo $base_url; ?>/faq");
	   }
	});

	/************************************************************************************/
	$.validator.addMethod('require-one', function(value) {
	    return $('.require-one:checked').size() > 0;
	}, 'Please check at least one box.');

	// var checkboxes = $('.require-one');
	// var checkbox_names = $.map(checkboxes, function(e, i) {
	//     return $(e).attr("name")
	// }).join(" ");

	$("#data-input").validate({
	    // groups: {
	    //     checks: checkbox_names
	    // },
	    errorPlacement: function(error, element) {
        // if (element.attr("type") == "checkbox") error.insertAfter(checkboxes.last());
        // else error.insertAfter(element);
				// alert(error);
				$dialogNotif.html("Please check at least one box");
				$dialogNotif.dialog("open");
	    }
	});


	$('body').on('click','a#remove', function(e){
		$(this).parent().parent().remove();
	});

	/*-------------------------------------------------------------------------------------------*/

});
</script>

<div style="display:none">
	<div id="dialog-error" title="Error Notification">
	  <p>We&rsquo;re very sorry, we can't save your data right now.</p>
	</div>
	<div id="dialog-success" title="Confirmation Notice">
	  <p>Data succesfully updated</p>
	</div>
</div>
