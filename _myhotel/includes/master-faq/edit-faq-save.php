<?php
	$masterfaqoid = $_POST['masterfaqoid'];
	$type = $_POST['type'];
	$question = $_POST['question'];
	$answer = $_POST['answer'];
	try{
		$stmt = $db->prepare("update masterfaq set type = :a, question= :b, answer= :c, updatedby= :d, updated= :e where masterfaqoid = :id");
		$stmt->execute(array(':a' => $type, ':b' => $question, ':c' => $answer, ':d' => $_SESSION['_user'], ':e' => date('Y-m-d H:i:s'), ':id' => $masterfaqoid));

	?>
		<script type="text/javascript">
	    $(function(){ $( document ).ready(function(){ $("#dialog-success").dialog("open"); }); });
    </script>
	<?php
	}catch(PDOException $ex) {
	?>
		<script type="text/javascript">
      $(function(){ $( document ).ready(function(){ $("#dialog-error").dialog("open"); }); });
    </script>
	<?php
	}
?>
