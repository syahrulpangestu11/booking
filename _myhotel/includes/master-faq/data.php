<button type="button" class="blue-button add-button">Create New FAQ</button><br /><br />
<table class="table">
	<thead>
		<tr>
			<th>No.</th>
			<th>Type</th>
			<th>Question</th>
			<th>Answer</th>
			<th>Manage</th>
			<th>&nbsp</th>
		</tr>
	</thead>
	<tbody>
<?php
try{
	error_reporting(E_ALL & ~E_NOTICE);
	include("../../conf/connection.php");
	include("../paging/pre-paging.php");

	$question = $_REQUEST['q'];
	$type = $_REQUEST['type'];

	$main_query = "select * from masterfaq";
		$additional_join == array();
		$filter = array();
		if(!empty($_REQUEST['q'])){
			array_push($filter, "question like '%".$_REQUEST['q']."%'");
		}
		if(!empty($_REQUEST['type'])){
			array_push($filter, "type like '%".$_REQUEST['type']."%'");
		}
		if(count($filter) > 0){
			$filter_query = ' where '.implode(" and ", $filter);
		}else{
			$filter_query = "";
		}
	$query = $main_query.$additional_join.$filter_query.' order by updated desc '.$paging_query;
	$stmt = $db->query($query);
	$result_faq = $stmt->fetchAll(PDO::FETCH_ASSOC);
	foreach($result_faq as $faq){
?>
	<tr>
		<td><?=$no++?></td>
		<td><b><?=$faq['type']?></b></td>
		<td><?=$faq['question']?></td>
		<td><?=$faq['answer']?></td>
		<td><?='Last updated at '.$faq['updated'].' by '.$faq['updatedby']?></td>
		<td>
			<button type='button' class='btn btn-primary edit-button' uo='<?=$faq['masterfaqoid']?>'>Edit</button>
			<button type='button' class='btn btn-danger delete-button' uo='<?=$faq['masterfaqoid']?>'>Delete</button>
		</td>
	</tr>
<?php
	}
}catch(PDOException $ex) {
	print($ex);
	die();
}
?>
	</tbody>
</table>
<?php
	$main_count_query = "select count(masterfaqoid) as jml FROM masterfaq ";
	$stmt = $db->query($main_count_query.$additional_join.$filter_query);
	$jmldata = $stmt->fetchColumn();

	$tampildata = $row_count;
	include("../paging/post-paging.php");
?>
