<?php
	$promocodeoid = $_POST['promocodeoid'];
	$startdate = date("Y-m-d", strtotime($_POST['startdate']));
	$enddate = date("Y-m-d", strtotime($_POST['enddate']));
	$applybar = (isset($_POST['applybar'])) ? $_POST['applybar'] : "n";	
	$applybardiscount = (isset($_POST['applybardiscount'])) ? $_POST['applybardiscount'] : "n";	
	$applybarcommission = (isset($_POST['applybarcommission'])) ? $_POST['applybarcommission'] : "n";	
	$allowzerotrx = (isset($_POST['allowzerotrx']) and $_POST['allowzerotrx'] == '1') ? '1' : '0';	
	try {
	// echo"<script>console.log(".json_encode($_POST['']).");</script>";
		$stmt = $db->prepare("update promocodeall set promocode=:a, name=:b, description=:c, startdate=:d, enddate=:e, discounttype=:f, discount=:g, commissiontype=:h, commission=:i, pic_name=:m, pic_number=:n, publishedoid=:o, allowzerotrx=:p where promocodealloid=:poid");
		$stmt->execute(array(':poid' => $promocodeoid, ':a' => $_POST['code'], ':b' => $_POST['name'], ':c' => $_POST['description'], ':d' => $startdate, ':e' => $enddate, ':f' => $_POST['discounttype'], ':g' => $_POST['discount'], ':h' => $_POST['commissiontype'], ':i' => $_POST['commission'], ':m' => $_POST['pic_name'], ':n' => $_POST['pic_number'], ':o' => $_POST['published'], ':p' => $allowzerotrx));
		// die();
		
		if(isset($_POST['apply'])){
			$existedapply = array();
			foreach($_POST['apply'] as $key => $value){
				$value2 = explode('-', $value);
				$hoteloid = $value2[0];
				// echo $hoteloid;
				$id = $value2[1];
				$applydiscount = (isset($_POST['applydiscount'])) ? $_POST['applydiscount'] : "n";	
				$applycommission = (isset($_POST['applycommission'])) ? $_POST['applycommission'] : "n";	
				$stmt = $db->prepare("select promocodeapplyalloid from promocodeapplyall where promocodealloid = :a and hoteloid = :b");
				$stmt->execute(array(':a' => $promocodeoid, ':b' => $hoteloid));
				$row_count = $stmt->rowCount();
				if($row_count > 0){
					$rowpromo_found = $stmt->fetch(PDO::FETCH_ASSOC);
					$stmt = $db->prepare("update promocodeapplyall set applydiscount=:a, applycommission=:b where promocodeapplyalloid=:c");
					$stmt->execute(array(':a' => $applydiscount, ':b' => $applycommission, ':c' => $rowpromo_found['promocodeapplyoid']));
					array_push($existedapply, $rowpromo_found['promocodeapplyalloid']);
				}else{
					$stmt = $db->prepare("insert into promocodeapplyall (promocodealloid, hoteloid, applydiscount, applycommission) values (:a, :b, :d, :e)");
					$stmt->execute(array(':a' => $promocodeoid, ':b' => $hoteloid, ':d' => $applydiscount, ':e' => $applycommission));
					array_push($existedapply, $db->lastInsertId());
				}
			}
			
			$existedapply = implode("','", $existedapply);
			$stmt = $db->query("delete from promocodeapplyall where promocodealloid = '".$promocodeoid."' and promocodeapplyalloid not in ('".$existedapply."')");
		}else{
			$stmt = $db->prepare("delete from promocodeapplyall where promocodealloid=:a");
			$stmt->execute(array(':a' => $promocodeoid));
		}
	?>
		<script type="text/javascript">
           $(function(){ $( document ).ready(function(){ $("#dialog-success").dialog("open"); }); });
        </script>
	<?php
	}catch(PDOException $ex) {
		//print_r($ex);
	?>
		<script type="text/javascript">
           $(function(){ $( document ).ready(function(){ $("#dialog-error").dialog("open"); }); });
        </script>
	<?php	
	}
?>