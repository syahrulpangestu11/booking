<button type="button" class="pure-button blue add"><i class="fa fa-barcode"></i>Create New Promo Code</button>

<?php

	include("../ajax-include-file.php");
	include("../paging/pre-paging.php");
	
	$datenow = date("Y-m-d");
	$hoteloid = $_REQUEST['hoteloid'];
	// echo $datenow;
	
	$main_query = "select pc.*, (case when pc.publishedoid = 1 and pc.enddate < '".$datenow."' then 'status-red' when pc.publishedoid = 1 then 'status-green' else 'status-black' end) as status from promocode pc where ";
	
	$filter = array();
	
	if(isset($_REQUEST['code']) and $_REQUEST['code']!=''){
		array_push($filter, 'pc.promocode like "%'.$_REQUEST['code'].'%"');
	}
	
	if((isset($_REQUEST['sdate']) and $_REQUEST['sdate']!='') or (isset($_REQUEST['edate']) and $_REQUEST['edate']!='')){
		$startdate = date("Y-m-d", strtotime($_REQUEST['sdate']));
		$enddate = date("Y-m-d", strtotime($_REQUEST['edate']));

		array_push($filter, "( 
		(startdate <= '".$startdate."' and (enddate >= '".$startdate."' and enddate <= '".$enddate."'))  
		or
		((startdate >= '".$startdate."' and startdate <= '".$enddate."') and enddate >= '".$enddate."')
		or
		((startdate >= '".$startdate."' and startdate <= '".$enddate."') and enddate <= '".$enddate."')
		or
		(startdate <= '".$startdate."' and enddate >= '".$enddate."')
		or
		(startdate >= '".$startdate."' and enddate <= '".$enddate."')
		)");
	}
	
	array_push($filter, 'pc.publishedoid not in (3)');
	
	if(count($filter) > 0){
		$combine_filter = implode($filter);
		$query = $main_query.$combine_filter;
	}else{
		$query = $main_query;
	}
	
	try {
		$main_query = $query." order by pc.promocodeoid";
		// echo $main_query;
		$stmt = $db->query($main_query.$paging_query);
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
?>
<table class="table promo-table">
    <tr>
        <td>Promo Code</td>
        <td>Periode</td>
        <td>&nbsp;</td>
    </tr>	
<?php
			$r_promo = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_promo as $row){
				$startperiode = date("d F Y", strtotime($row['startdate']));
				$endperiode = date("d F Y", strtotime($row['enddate']));
?>
    <tr>
        <td class="<?php echo $row['status']; ?>"><?php echo $row['promocode']; ?></td>
        <td>Periode : <?php echo $startperiode; ?> - <?php echo $endperiode; ?></td>
        <td class="algn-right">
        <button type="button" class="pure-button green single edit" pid="<?php echo $row['promocodeoid']; ?>"><i class="fa fa-pencil"></i></button>
       
       <button type="button" class="pure-button red single delete" pid="<?php echo $row['promocodeoid']; ?>"><i class="fa fa-trash"></i></button>
		</td>
    </tr>	
<?php				
			}
?>
</table>
<?php
		}
		
		$main_count_query = "select count(*) as jml from promocode pc";
		if(count($filter) > 0){
			$combine_filter = implode(' where ',$filter);
			$main_count_query = $main_count_query.' where '.$combine_filter;
		}
		// echo($main_count_query);
		$stmt = $db->query($main_count_query);
		$jmldata = $stmt->fetchColumn();	
	
		$tampildata = $row_count;
		include("../paging/post-paging.php");	

	}catch(PDOException $ex) {
		echo "Invalid Query";
		print($ex);
		die();
	}
?>
