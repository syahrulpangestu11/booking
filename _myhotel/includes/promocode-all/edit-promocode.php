<?php
	$promocodeoid = $_GET['pid'];
	try {
		$stmt = $db->query("select pc.* from promocode pc where promocodeoid = '".$promocodeoid."'");
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
			$pc = $stmt->fetch(PDO::FETCH_ASSOC);
			$startdate = date("d F Y", strtotime($pc['startdate']));
			$enddate = date("d F Y", strtotime($pc['enddate']));
		}else{
			echo "No Result";
			die();
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}
?>
<script type="text/javascript">
$(function() {
	$('textarea').trumbowyg({
		btns: ['viewHTML',
		  '|', 'btnGrp-design',
		  '|', 'link',
		  '|', 'btnGrp-justify',
		  '|', 'btnGrp-lists'],
		fullscreenable: false
	});
});
</script>
<section class="content-header">
    <h1>
        Edit Promo Code
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i>  Allotments &amp; Rates </a></li>
        <li class="active">Promo Code</li>
    </ol>
</section>
<section class="content">
	<form class="form-box" method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/promocode-all/edit-process">
	<input type="text" name="promocodeoid" value="<?php echo $pc['promocodeoid']; ?>"/>
	<div class="row">
        <div class="box box-form">
            <h1>Promo Code</h1>
            <ul class="inline-half colored">
				<li>
                    <ul class="block">
                        <li><h3>DETAIL PROMO CODE</h3></li>
                        <li>
                            <span class="label"><b>Promo Code:</b></span>
                            <input type="text" class="medium" name="code" required="required" value="<?php echo $pc['promocode']; ?>">
                        </li>
                        <li>
                            <span class="label">Promo Code Name:</span>
                            <input type="text" class="long" name="name" value="<?php echo $pc['name']; ?>">
                        </li>
                        <li>
                            <h3>Description :<!----></h3>
                            <textarea name="description"><?php echo $pc['description']; ?></textarea>
                            <div class="clear"></div>
                        </li>
                        <div class="clear"></div>
                        <li>
                        	<div class="clear"></div>
                            <span class="label">Publish Promo Code :</span>
                            <select name="published">
                            <?php
                                try {
                                    $stmt = $db->query("select * from published where showthis = 'y'");
                                    $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                    foreach($r_room as $row){
										if($row['publishedoid'] == $pc['publishedoid']){ $selected = "selected"; }else{ $selected=""; }
                                        echo"<option value='".$row['publishedoid']."' ".$selected.">".$row['note']."</option>";
                                    }
                                }catch(PDOException $ex) {
                                    echo "Invalid Query";
                                    die();
                                }
                            ?>
                            </select>
                        </li>
                	</ul>
                    <?php if($_SESSION['_typeusr'] == '1'){?>
                    <ul class="block">
                        <li><h3>Allow Zero Transaction</h3></li>
                        <li>
                            <span class="label">Check to allow :</span>
                            <input type="checkbox" name="allowzerotrx" value="1" <?php if($pc['allowzerotrx'] == '1') echo 'checked=checked'; ?>>
                        </li>
                    </ul>
                    <?php }?>
                </li>
								<li>
                	<ul class="block">
                        <li><h3>PERIODE OF PROMO CODE</h3></li>
                        <li>
                            <span class="label">Start Date From:</span>
                            <input type="text"class="medium" id="startdate" name="startdate" required="required" value="<?php echo $startdate; ?>" autocomplete="off">
                        </li>
                        <li>
                            <span class="label">End Date To:</span>
                            <input type="text"class="medium" id="enddate" name="enddate" required="required" value="<?php echo $enddate; ?>" autocomplete="off">
                        </li>
                        <li><h3>DISCOUNT</h3></li>
                        <li>
                            <span class="label">Discount Type:</span>
                            <select name="discounttype" class="input-select">
                                <?php
                                	$codetype = array('discount percentage', 'discount amount');
                                    foreach($codetype as $value){
										if($value == $pc['discounttype']){ $selected = "selected"; $dataval=$pc['discount']; }else{ $selected=""; $dataval=""; }
                                ?>
                                    <option value="<?php echo $value; ?>" <?php echo $selected; ?> data="<?php echo $dataval; ?>"><?php echo $value; ?></option>
                                <?php
                                    }
                                ?>
                            </select>
                        </li>
                        <li id="discount-value"></li>
                        <li><h3>COMMISSION</h3></li>
                        <li>
                            <span class="label">Commission Type:</span>
                            <select name="commissiontype" class="input-select">
                                <?php
                                	$codetype = array('commission percentage', 'commission amount');
                                    foreach($codetype as $value){
										if($value == $pc['commissiontype']){ $selected = "selected"; $dataval=$pc['commission']; }else{ $selected=""; $dataval=""; }
                                ?>
                                    <option value="<?php echo $value; ?>" <?php echo $selected; ?> data="<?php echo $dataval; ?>"><?php echo $value; ?></option>
                                <?php
                                    }
                                ?>
                            </select>
                        </li>
                        <li id="commission-value"></li>
                        <li>
                            <span class="label"><b>PIC Name:</b></span>
                            <input type="text" class="medium" name="pic_name" value="<?php echo $pc['pic_name']; ?>">
                        </li>
                        <li>
                            <span class="label"><b>PIC Contact Number:</b></span>
                            <input type="text" class="medium" name="pic_number" value="<?php echo $pc['pic_number']; ?>">
                        </li>
                    </ul>

                </li>
            </ul>
            <div class="clear"></div>
        </div>
    </div>
    <div class="row">
    	<div class="box box-form" id="step-3">
        <div class="row">
            <div class="col-md-6"><h1><i class="fa fa-building"></i> Apply Campaign To Hotel</h1></div>
            <div class="col-md-6 text-right"><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#hotelListModal">Assign New Hotel</button></div>
        </div>
        <div class="row" id="assigned-hotel">
            <ul id="accordion">
            <?php
                $stmt = $db->prepare("select p.hoteloid, h.hotelname from promocode p inner join hotel h using (hoteloid) where p.promocodeoid = :a");
                $stmt->execute(array(':a' => $promocodeoid));
                $r_lp_hotel = $stmt->fetchAll(PDO::FETCH_ASSOC);
                foreach($r_lp_hotel as $hotel){
            ?>
                <li lph="<?=$hotel['loyaltyprogramhoteloid']?>">
                    <div class="header">
                        <div class="row">
                            <div class="col-md-6"><?=$hotel['hotelname']?></div>
                            <div class="col-md-6 text-right">
                                <?php if($campaign['chainoid'] != 0){ ?>
                                <button type="button" class="btn btn-warning btn-sm" data-lph="<?=$hotel['loyaltyprogramhoteloid']?>"><i class="fa fa-trash-o"></i> Remove Hotel</button>
                                <?php } ?>
                                <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#promotionListModal" data-hotel="<?=$hotel['hoteloid']?>"  data-lph="<?=$hotel['loyaltyprogramhoteloid']?>">Select Promotion</button>
                            </div>
                        </div>
                    </div>
                    <div class="content" id="assigned-promotion">
                        <?php
                            $stmt = $db->prepare("select p.name from promotion p inner join promocodeapply on p.promotionoid = promocodeapply.id where p.hoteloid = :a and promocodeoid = :b");
                            $stmt->execute(array(':a' => $hotel['hoteloid'], ':b' => $promocodeoid));
                            $r_lp_promotion = $stmt->fetchAll(PDO::FETCH_ASSOC);
                            foreach($r_lp_promotion as $promotion){
                        ?>
                            <div class="col-md-6"><button type="button" class="btn btn-danger btn-xs" id="remove-promotion" data-lpp="<?=$promotion['loyaltyprogrampromotionoid']?>" data-toggle="modal" data-target="#confirmDelPromotion" ><i class="fa fa-close"></i></button> <?=$promotion['name']?></div>
                        <?php
                            }
                        ?>
                    </div>
                </li>
            <?php
                }
            ?>
            </ul>
        </div>
                <br>
                <div class="row">
                        <div class="form-group col-md-12 text-right">
                                <button type="button" class="small-button red cancel">Cancel</button>
                                <button type="button" class="small-button blue submit-edit">Save Change</button>
                        </div>
                </div>
    </div>
    </div>
	</form>
</section>
