<?php
	error_reporting(E_ALL ^ E_NOTICE);
	include("../../conf/connection.php");

	$hoteloid = $_POST['ho'];
	$loyaltyprogramhoteloid = $_POST['lph'];
    $datenow = date("Y-m-d");
    // echo $hoteloid;    
// 	$s_existed_promotion = "select 'promotion' as promotype, p.promotionoid as oid, p.name, (case when p.publishedoid = 1 and p.saleto < '".$datenow."' then 'status-red' when p.publishedoid = 1 then 'status-green' else 'status-black' end) as status from promotion p left join promotiontype pt using (promotiontypeoid) inner join discounttype dt using (discounttypeoid) inner join hotel h using (hoteloid) inner join promotionapply pa using (promotionoid) inner join roomoffer ro using (roomofferoid) inner join room r using (roomoid) inner join channel ch using (channeloid) where h.hoteloid = '".$hoteloid."' and p.publishedoid not in (3) group by p.promotionoid
// union all
// select 'package' as promotype, p.packageoid as oid, p.name, (case when p.publishedoid = 1 and p.saleto < '".$datenow."' then 'status-red' when p.publishedoid = 1 then 'status-green' else 'status-black' end) as status from package p inner join hotel h using (hoteloid) inner join packageapply pa using (packageoid) inner join roomoffer ro using (roomofferoid) inner join room r using (roomoid) inner join channel ch using (channeloid) where h.hoteloid = '".$hoteloid."' and p.publishedoid not in (3) group by p.packageoid";
// echo $s_existed_promotion;
	// $stmt = $db->query($s_existed_promotion);
	// $r_existed_promotion = $stmt->fetch(PDO::FETCH_ASSOC);
	// if(!empty($r_existed_promotion['existed'])){
	// 	$exist_promotion = $r_existed_promotion['existed'];
	// }else{
	// 	$exist_promotion = '';
	// }
?>

<div class="box-body">
    <table class="table table-striped">
        <thead>
            <tr><th>No.</th>
            <th>Promotion</th>
            <th>Select</th>
            <th>Apply Discount</th>
            <th>Apply Commission</th>
        </tr></thead>
        <tbody>

        <?php
            // main query
            $batas=10;
			$halaman=isset($_REQUEST['page']) ? $_REQUEST['page'] : 0 ;
			if(empty($halaman)){
				$posisi=0; $halaman=1;
			}else{
				$posisi = ($halaman-1) * $batas;
			}
			$no=$posisi+1;
			$j = $halaman + ($halaman - 1) * ($batas - 1);

			$paging_query = " limit ".$posisi.", ".$batas;
			$num = $posisi;

            $main_query_promotion ="select 'promotion' as promotype, saleto, salefrom, promotionoid as oid, name, hoteloid from promotion";
            $main_query_package = " select 'package' as promotype, saleto, salefrom, packageoid as oid, name, hoteloid from package";    
            // query tambahan jika ada filter
            $filter = array();
            if(isset($_REQUEST['name']) and $_REQUEST['name']!=''){
                array_push($filter, 'p.promotionname like "%'.$_REQUEST['keyword'].'%"');
            }
			array_push($filter, 'p.hoteloid = "'.$hoteloid.'"');
			array_push($filter, 'p.publishedoid <> "3"');
			array_push($filter, 'p.publishedoid = "1"');

			if(!empty($exist_promotion)){
				array_push($filter, 'p.promotionoid not in ('.$exist_promotion.')');
			}
			// menggabungkan query utama dengan query filter
            // if(count($filter) > 0){
            //     $combine_filter = implode(' and ',$filter);
            //     $syntax_list = $main_query.' where '.$combine_filter;
            // }else{
                $syntax_list_promotion = $main_query_promotion;
                $syntax_list_package = $main_query_package;
                $syntax_list = $syntax_list_promotion." union".$syntax_list_package;
            // }
            $syntax_list_last = " where hoteloid = '".$hoteloid."' and publishedoid not in (3) and saleto >= '".$datenow."' and salefrom <= '".$datenow."'";
            $syntax_list_paging = $syntax_list_promotion.$syntax_list_last." union".$syntax_list_package.$syntax_list_last." order by salefrom, saleto ".$paging_query;
            // echo($syntax_list_paging);
            $stmt = $db->query($syntax_list_paging);
			$result_list = $stmt->fetchAll(PDO::FETCH_ASSOC);?>
           <!--  <tr>
                <td>1</td>
                <td>Best Flexible Rate</td>
                <td><?php echo date('d/M/Y', strtotime($list['salefrom']))." - ".date('d/M/Y', strtotime($list['saleto'])); ?></td>
                <td class="text-center"><input type="checkbox" name="promotion-<?= $list['promotype']?>[]" value="<?=$list['oid']?> "><input type="hidden" name="promotype" value="<?= $list['promotype']?>"></td>
            </tr> -->
            <tr>
                <td>1</td>
                <td>Best Flexible Rate<input type="hidden" name="flexiblerate" value="Best Flexible Rate"></td><input type="hidden" name="applybar" value="y">
                <td class="text-center"><input type="checkbox" name="promotion-flexiblerate" value="y"><input type="hidden" name="promotype" value="<?= $list['promotype']?>"></td>
                <td class="text-center"><input type="checkbox" name="discountapply-flexiblerate" value="y"></td>
                <td class="text-center"><input type="checkbox" name="commissionapply-flexiblerate" value="y"></td>
            </tr>  
            <?php foreach($result_list as $list){
                $num++;
        ?>  
            
            <tr> <input type="text" name="hoteloid-<?= $list['promotype']?>" value="<?=$list['hoteloid']?>">
                <td><?php echo $num; ?></td>
                <td><?php echo $list['name']; ?></td>
                <td class="text-center"><input type="checkbox" name="promotion-<?= $list['promotype']?>[]" value="<?=$list['oid']?> "><input type="hidden" name="promotype-<?= $list['promotype']?>" value="<?= $list['promotype']?>"></td>
                <td class="text-center"><input type="checkbox" name="discountapply-<?= $list['promotype']?>" value="y"></td>
                <td class="text-center"><input type="checkbox" name="commissionapply-<?= $list['promotype']?>" value="y"></td>
            </tr>
        <?php
            }
        ?>
        </tbody>
    </table>
    <?php require_once('paging/post-paging.php'); ?>
</div>
