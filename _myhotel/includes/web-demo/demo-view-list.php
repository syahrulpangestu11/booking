<style media="screen">
.pure-button > i.fa, .small-button > i.fa {
	margin-right: 0px;
}
.color-green {color:#5fbeaa;}
.color-orange {color: #F59226;}
.color-red {color:#E02726;}
.color-purple {color:#b198dc;}

.dialog-detail {font-size: 12px;}
.dialog-detail table tr td {vertical-align: top;}
</style>

<?php
include('conf/connection-thebukingcom.php');
include("includes/paging/pre-paging.php");

	$stmt = $dbTB->query("SELECT count(formdemooid) as registered from formdemo");
	$registered = $stmt->fetch(PDO::FETCH_ASSOC);
	$stmt = $dbTB->query("SELECT count(formdemooid) as emailed from formdemo where sendemail = '1'");
	$emailed = $stmt->fetch(PDO::FETCH_ASSOC);
?>

<section class="content-header">
    <h1>
       	Demo Request
    </h1>
    <ol class="breadcrumb">
        <li class="active"><a href="#"><i class="fa fa-table"></i> Demo Request</a></li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="box">
            <div id="data-box" class="box-body">
            <form method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/web-demo/send-email">
            <input type="hidden" name="id" value="" />
<h1>Demo Request</h1>
<h2><i class="fa fa-user"></i> Registered : <?php echo $registered['registered']; ?> &nbsp;&nbsp;/&nbsp;&nbsp; <i class="fa fa-envelope-o"></i> Emailed : <?php echo $emailed['emailed']; ?></h2>
<table class="table promo-table">
	<tr>
		<td>No.</td>
		<td>Registration Date</td>
		<td>Name</td>
		<td>Email</td>
		<td>Property</td>
		<td>Rooms</td>
    <td>Mobile</td>
		<td>Country</td>
    <td>Action</td>
	</tr>
	<?php
	$nomor = $no-1;
	$main_query = "SELECT * from formdemo order by submitdate desc";
	// $stmt = $dbTB->query("SELECT * from formdemo order by submitdate desc");
	$stmt = $dbTB->query($main_query.$paging_query);
	$row_count = $stmt->rowCount();
	if($row_count > 0) {
		$r_registered = $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach($r_registered as $data){
					$submitdate = date('d/M/Y', strtotime($data['submitdate']));
					$submitdate_1 = date('d F Y, H:i:s', strtotime($data['submitdate']));
					$name = $data['firstname']." ".$data['lastname'];
					switch ($data['sendemail']) {
						case 1: $status = '<b class="color-green"><i class="fa fa-check"></i> Emailed</b>'; break;
						default: $status = ""; break;
					}
	?>
	<tr>
		<td><?php echo ++$nomor; ?></td>
		<td><?php echo $submitdate; ?></td>
		<td><?php echo $name; ?></td>
		<td style="text-transform:lowercase"><?php echo $data['email']; ?></td>
		<td><?php echo $data['propertyname']; ?></td>
		<td><?php echo $data['numberofrooms']; ?></td>
    <td><?php echo $data['phone']; ?></td>
		<td><?php echo $data['country']; ?></td>
    <td>
		<button type="button" class="detail-button small-button blue" id="<?php echo $data['formdemooid']; ?>"><i class="fa fa-list"></i></button>
    <?php if($data['sendemail'] == 0){ ?>
    <button type="button" class="send-button small-button green" id="<?php echo $data['formdemooid']; ?>"><i class="fa fa-envelope"></i></button>
    <?php }else{ ?>
		<b class="color-green"><i class="fa fa-check"></i> Emailed</b>
		<?php } ?>


		<!-- Pop Up Detail -->
		<div id="dialog-detail-<?=$data['formdemooid'];?>" class="dialog-detail" title="Demo Request Detail" style="display: none;">
			Submit Date : <b><?=$submitdate_1;?></b><br><br>
			<table>
				<tr>
					<td colspan="3"><b><u>User Information</u></b></td>
				</tr>
				<tr>
					<td style="width: 110px;">Name</td>
					<td>:</td>
					<td><?=$name;?></td>
				</tr>
				<tr>
					<td>Email</td>
					<td>:</td>
					<td><?=$data['email'];?></td>
				</tr>
				<tr>
					<td>Phone</td>
					<td>:</td>
					<td><?=$data['phone'];?></td>
				</tr>
				<tr>
					<td>Property Name</td>
					<td>:</td>
					<td><?=$data['propertyname'];?></td>
				</tr>
				<tr>
					<td>Number of Rooms</td>
					<td>:</td>
					<td><?=$data['numberofrooms'];?></td>
				</tr>
				<tr>
					<td>Country</td>
					<td>:</td>
					<td><?=$data['country'];?></td>
				</tr>
				<tr>
					<td>Post/Zip Code</td>
					<td>:</td>
					<td><?=$data['postcode'];?></td>
				</tr>
				<tr>
					<td>Comments</td>
					<td>:</td>
					<td><?=$data['comments'];?></td>
				</tr>
			</table>
		</div>


    </td>
	</tr>
	<?php
        }
	}
	?>
</table>
            </form>
            </div><!-- /.box-body -->

						<?php
						// --- PAGINATION part 2 ---
						$main_count_query = "SELECT count(formdemooid) as jml from formdemo";
						$stmt = $dbTB->query($main_count_query);
						$jmldata = $stmt->fetchColumn();

						$tampildata = $row_count;
						include("includes/paging/post-paging.php");
						?>
       </div>
    </div>
</section>

<script type="text/javascript">
$(function(){
	$(".dialog-detail").dialog({
		 autoOpen: false,
		 minWidth: 600,
		 buttons: [
			 {
			 text: "Ok",
	      click: function() {
	        $( this ).dialog( "close" );
	      }
		 }
	 ]
	 //close: function(event, ui) { redirect }
	});

	$('body').on('click','button.send-button', function(e) {
		$('input[name=id]').val($(this).attr('id'));
		$('form#data-input').submit();
	});

	$('body').on('click','button.detail-button', function(e) {
		// $('input[name=id]').val($(this).attr('id'));
		id = $(this).attr("id");
		// url = "<?php echo $base_url; ?>/web-event/detail/?id="+id;
		$("#dialog-detail-"+id).dialog("open");
		//$(location).attr("href", url);
	});

	$('body').on('click','#paging button', function(e) {
		page	= $(this).attr('page');
		url = "<?php echo $base_url; ?>/web-demo/?page="+page;
		$(location).attr("href", url);
	});
});
</script>
