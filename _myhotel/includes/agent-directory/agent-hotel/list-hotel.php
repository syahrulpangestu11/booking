<section class="content-header">
    <h1>Agent Hotel</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-user-o"></i> Agent Hotel</a></li>
    </ol>
</section>
<section class="content">
    <div class="box">
	<table class="table promo-table">
		<tr>
				<td>Hotel</td>
				<td>Credit Facility Balance</td>
		</tr>
<?php

	try {
		$main_query = "select h.hotelname, ah.creditfacility from agenthotel ah inner join hotel h using (hoteloid) inner join agent a using (agentoid) where a.agentoid = '".$_SESSION['_oid']."' order by h.hotelname";
		$stmt = $db->query($main_query);
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
	?>
	<?php
		$r_agent = $stmt->fetchAll(PDO::FETCH_ASSOC);
		foreach($r_agent as $row){
	?>
		<tr>
			<td><?php echo $row['hotelname']; ?></td>
			<td><?php echo number_format($row['creditfacility']); ?></td>
		</tr>
	<?php
		}
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		print($ex);
		die();
	}
	?>
	</table>
    </div>
</section>