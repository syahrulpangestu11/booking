<script type="text/javascript">
$(function(){
	$('button[name="select-room"]').click(function(){
		var parameter = $(this).attr('prm');
		var detail = $('ul#breakdown-total');
		$.ajax({
			url		: '<?=$base_url?>/includes/agent-directory/booking/include/select-room.php',
			type	: 'post',
			data	: { parameter : parameter},
			success	: function(response){
				detail.append(response);
				rsvCalculating();
			}
		});
	});
	
	$('body').on('click','a.remove', function(e){
		var elem = $(this);
		var parameter = elem.attr('prm');
		$.ajax({
			url		: '<?=$base_url?>/includes/agent-directory/booking/include/remove-room.php',
			type	: 'post',
			data	: { parameter : parameter },
			success	: function(response){
				if(response == "0"){
					elem.parent().parent().parent('li').remove();
					rsvCalculating();
				}
			}
		});
	});
	
	function rsvCalculating(){
		$.ajax({
			url		: '<?=$base_url?>/includes/agent-directory/booking/include/detail-total.php',
			success	: function(response){
				xmlDoc = $.parseXML(response),
				$xml = $(xmlDoc ),
				$formattotal = $xml.find("total").text();
				$formatdeposit = $xml.find("deposit").text();
				$formatbalance = $xml.find("balance").text();
				$total = $xml.find("amounttotal").text();
				
				$('#box-summary-reservation').find('h5#grandtotal').html($formattotal);
				
				if(parseFloat($total) > 0){
					$('#box-summary-reservation').find('button[type=submit]').removeAttr('disabled');	
				}else{
					$('#box-summary-reservation').find('button[type=submit]').attr('disabled','disabled');	
				}
			}
		});

		numberroom =  $('ul#breakdown-total > li').size();
		$('h5#numberroom').html(numberroom + ' room(s)');
	}
});
</script>