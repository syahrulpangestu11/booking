<?php
try {
	session_start();
	error_reporting(E_ALL ^ E_NOTICE);
	include("../../conf/connection.php");
	print_r($_POST);
	$hoteloid			= $_SESSION['_hotel'];
	$name				= $_POST['name'];
	$headline			= $_POST['headline'];
	$servicefacilities	= $_POST['servicefacilities'];
	$deposit			= $_POST['deposit'];
	$depositval 		= (isset($_POST['depositvalue'])) ? $_POST['depositvalue'] : 0;
	
	$stmt = $db->query("select barsettingoid from barsetting where hoteloid = '".$hoteloid."'");
	$founddata = $stmt->rowCount();
	if($founddata == 0){
		$stmt = $db->prepare("INSERT INTO barsetting (hoteloid, headline, servicefacilities, depositoid, depositvalue, name) VALUES (:a , :b , :c , :d , :e, :f)");
		$stmt->execute(array(':a' => $hoteloid, ':b' => $headline, ':c' => $servicefacilities, ':d' => $deposit , ':e' => $depositval, ':f' => $name));
	}else{
		$hotelbar = $stmt->fetch(PDO::FETCH_ASSOC);
		$stmt = $db->prepare("UPDATE barsetting SET headline = :b, servicefacilities = :c, depositoid = :d, depositvalue= :e, name= :f where hoteloid = :a");
		$stmt->execute(array(':a' => $hoteloid, ':b' => $headline, ':c' => $servicefacilities, ':d' => $deposit , ':e' => $depositval, ':f' => $name));
	}
	
}catch(PDOException $ex) {
	echo "0";
	echo $ex->getMessage();
	die();
}
?>