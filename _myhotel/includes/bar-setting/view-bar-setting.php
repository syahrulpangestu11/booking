<?php
	include('includes/bootstrap.php');
	include('modal-cancellationpolicy.php');
	$channeloid = 1;
	
	$stmt = $db->query("select * from barsetting where hoteloid = '".$hoteloid."'");
	$hotelbar = $stmt->fetch(PDO::FETCH_ASSOC);
	
	$deposit = (!empty($hotelbar['depositoid'])) ? $hotelbar['depositoid'] : 1;
	$depositval = (!empty($hotelbar['depositvalue'])) ? $hotelbar['depositvalue'] : 0;
	$depositvalue = (!empty($hotelbar['depositvalue'])) ? $hotelbar['depositvalue'] : 0;
?>
<style type="text/css">
div.green{
	padding:0 7px;
	border:3px solid #fff;
	background-color:#f6fbfa;
}
div.frame-cp{
	padding: 20px;
	border: 1px solid #ddd;
}
div.roomoffer{
	border-bottom:1px solid #bcbdbd;
	padding:10px 0;
}
div.roomoffer:last-of-type{
	border-bottom:none;
}
</style>
<script type="text/javascript">
$(function() {
	$('textarea').trumbowyg({
		btns: ['btnGrp-design', 'link', 'btnGrp-justify', 'btnGrp-lists'],
		fullscreenable: false
	});
});
</script>
<section class="content-header">
    <h1>BAR / Flexible Rate Setting</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-cog"></i>  Settings</a></li>
        <li class="active">Bar / Flexible Rate Setting</li>
    </ol>
</section>
<section class="content" id="promotion">
        <div class="box box-form">
            <div class="row">
            	<div class="col-md-5 green">
                    <form class="form-box form-horizontal" method="post" enctype="multipart/form-data" id="data-input" action="#">
                	<div class="">
						<h1>Bar Setting</h1>
						<br>
						<div class="form-group">
                            <div class="col-md-12">
                                <label>BAR Name</label>
								<br>
                                <input name="name" type="text" value="<?=$hotelbar['name']?>" class="long">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <label>Headline</label>
                                <textarea name="headline"><?=$hotelbar['headline']?></textarea>
                            </div>
                            <div class="col-md-12">
                            	<br>
                                <label>BAR Service &amp; Facilities</label>
                                <textarea name="servicefacilities"><?=$hotelbar['servicefacilities']?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                            <div class="panel panel-danger">
                                <div class="panel-heading" style="color:#000">
                                <label>Deposit Type</label>
                                &nbsp; <span style="font-size:0.9em; color:#de0000"><i class="fa fa-info-circle"></i> This amount will be declared as booking guarantee that charged by hotel upon booking received.</span>
                                <div>
                                <?php
                                    try {
                                        $stmt = $db->query("select d.* from deposit d inner join published p using (publishedoid) where p.publishedoid = '1'");
                                        $r_dt = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                        foreach($r_dt as $row){
                                            if($row['depositoid'] == $deposit){ $selected = "checked"; $depositval = $depositvalue; }else{ $selected=""; $depositval = ""; }
                                            echo"<input type='radio' name='deposit' value='".$row['depositoid']."' ".$selected.">  ".$row['name'];
                                            if($row['depositoid'] == '2'){
                                                echo"<div class='depositval' style='display:none'><input type='text' class='medium'  name='depositvalue' value='".$depositval."'> Deposit for IDR Amount</div>";
                                            }else if($row['depositoid'] == '3'){
                                                echo"<div class='depositval' style='display:none'><input type='text' class='medium'  name='depositvalue' value='".$depositval."'> Deposit for %(percentage)from total</div>";
                                            }else if($row['depositoid'] == '4'){
                                                echo"<div class='depositval' style='display:none'><input type='text' class='medium'  name='depositvalue' value='".$depositval."'> night(s) amount</div>";
                                            }
                                            echo "<br>";
                                        }
                                    }catch(PDOException $ex) {
                                        echo "Invalid Query";
                                        die();
                                    }
                                ?>
                                </div>
                                </div>
                            </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 text-right"><button type="button" class="btn btn-primary" id="save-barsetting">Save</button></div>
                        </div>
                        <div class="clear"></div>
                    </div>
        		</form>
                </div>
                <div class="col-md-7">
                    <div class="frame-cp">
                        <div class="row">
                            <div class="col-md-6"><h1>Set Cancellation Policy</h1></div>
                            <div class="col-md-6 text-right"><button type="button" class="btn btn-success" id="btn_CPModal">Cancellation Policy Setting</button></div>
							<!-- data-toggle="modal" data-target="#CPModal" -->
                        </div>
                        <div class="row" id="assigned-cp"> 
                        <?php
                        $stmt = $db->query("select ro.roomofferoid, ro.name, ot.offername from roomoffer ro inner join room r using (roomoid) inner join offertype ot using (offertypeoid) where r.hoteloid = '".$hoteloid."' and ro.publishedoid = '1' and r.publishedoid = '1'");
                        $r_roomoffer = $stmt->fetchAll(PDO::FETCH_ASSOC);
                        foreach($r_roomoffer as $roomoffer){
                        ?>
                            <div class="roomoffer" ro="<?=$roomoffer['roomofferoid']?>">
                                <div class="header">
                                    <div class="row">
                                        <div class="col-md-12">
										<?=$roomoffer['name']?><br /><small><?=$roomoffer['offername']?></small>
                                        <input type="hidden" name="roomoffer[]" value="<?=$roomoffer['roomofferoid']?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="table-responsive" id="rate">
                                    <table class="table table-striped">
                                        <tr class="text-center"><th>&nbsp;</th><th>Start Date</th><th>End Date</th><th>Cancellation Policy</th><th>Action</th></tr>
                                        <?php
										$stmt = $db->query("select cpa.*, cp.name as policyname, cp.description as policynote from cancellationpolicyapply cpa inner join cancellationpolicy cp using (cancellationpolicyoid) inner join roomoffer ro using (roomofferoid)  inner join room r using (roomoid) inner join channel ch using (channeloid) where ro.roomofferoid = '".$roomoffer['roomofferoid']."' and ch.channeloid = '".$channeloid."' order by cpa.policyapplyoid");
										$r_cp = $stmt->fetchAll(PDO::FETCH_ASSOC);
										foreach($r_cp as $row){
                                        ?>
                                            <tr>
                                                <td><input type="hidden" name="cpa-<?=$roomoffer['roomofferoid']?>[]" value="<?=$row['policyapplyoid']?>" /></td>
                                                <td><?php echo date("d M Y", strtotime($row['startdate'])); ?></td>
                                                <td><?php echo date("d M Y", strtotime($row['enddate'])); ?></td>
                                                <td><?php echo $row['policyname']; ?></td>
                                                <td><button type="button" class="btn btn-danger btn-xs remove-cp" value="<?=$row['policyapplyoid']?>"><i class="fa fa-remove"></i></button></td>
                                            </tr>
                                        <?php	
                                        }
                                        ?>
                                    </table>
                                </div>
                            </div>
                        <?php
                        }
                        ?>	
                        </div>
                    </div>
                </div>
			</div>
		</div>
</section>
<script type="text/javascript">
$(function(){
	$(document).ready(function(){ 
		$('input[type=radio][name=deposit]:checked').each(function(){
			if($(this).prop("checked") == true){
				$(this).click();
			}
		});
	});
	
	$('body').on('click', 'button.remove-cp', function (e){
		var cp = $(this).val();
		$.ajax({
			url		: '<?php echo $base_url; ?>/includes/bar-setting/remove-cancellationpolicy.php',
			type	: 'post',
			data	: { id : cp },
			success	: function(response){
				window.location.reload();
			}
		});
	});
	
	$('body').on('change','select[name=cancellationpolicy]', function(e) {
		desc = $('select[name=cancellationpolicy] option:selected').attr('note');
		p_desc = $('#CPModal').find('div.note-cp').html(desc);
	});
	
	$('body').on('change','input[name=choose-room]', function(e) {
		action		= $(this).val();
		elem_target	= $('#CPModal').find('input:checkbox[name="roomoffer[]"]');
		if(action == "select"){
			elem_target.prop('checked',true);
		}else if(action == "unselect"){
			elem_target.prop('checked',false);
		}
	});
	
	$('body').on('click', '#CPModal button#assign-cp', function(e){	
		var formcp = $('#CPModal').find('form#form-assign-cp');	
		$.ajax({
			url		: '<?php echo $base_url; ?>/includes/bar-setting/save-cancellationpolicy.php',
			type	: 'post',
			data	: formcp.serialize(),
			success	: function(response){
				window.location.reload();
			}
		});
		
		$('.modal').modal('hide');
	});
	
	$(document).ready(function(){ 
		$('input[type=radio][name=deposit]:checked').each(function(){
			if($(this).prop("checked") == true){
				$(this).click();
			}
		});
	});
	
	$('body').on('click','input[type="radio"][name=deposit]', function(e) {
		allElem = $('div.depositval');
		allElem.css('display','none');
		
		
		if($(this).val() == '2' || $(this).val() == '3'  || $(this).val() == '4' ){
			applyval = $(this).next('div.depositval');
			if($(this).prop( "checked" ) == true){
				applyval.css('display','block');
			}else{
				applyval.css('display','none');
			}
		}
		
		$('input[type="text"][name="depositvalue"]').prop('disabled',true);
		$(this).next('div.depositval').children('input[type="text"][name="depositvalue"]').prop('disabled',false);
	});
	
	
	$('body').on('click', 'button#btn_CPModal', function (e){
		$('#CPModal').modal('show');
	});
	
	$('body').on('click', 'button#save-barsetting', function (e){
		var form = $('form#data-input');
		$.ajax({
			url		: '<?php echo $base_url; ?>/includes/bar-setting/save-barsetting.php',
			type	: 'post',
			data	: form.serialize(),
			success	: function(response){
				window.location.reload();
			}
		});
	});

	
	var dates = $("#sdate, #edate").datepicker({
		beforeShow: function() {
			setTimeout(function(){ $('.ui-datepicker').css('z-index', 9999); }, 0);
		},
		defaultDate: "+0", changeMonth: true, numberOfMonths: 1, changeYear: true,
		onSelect: function( selectedDate ) {
			var option = this.id == "sdate" ? "minDate" : "maxDate",
				instance = $( this ).data( "datepicker" );
				date = $.datepicker.parseDate( instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings ); dates.not( this ).datepicker( "option", option, date );;
			if($("#sdate").val() == $("#edate").val()) { //if value from #from same with #to or value #from more than value of #to
				var x = $("#startdate").val(); var temp = new Array(); temp = x.split(" "); var d = parseInt(temp[0]); var m = temp[1]; var y = parseInt(temp[2]);
				switch(m) {
					case "January"	: if(d==31) { d = 1; m = "February"; }else { d = d+1; } break;
					case "February"	: if(y%4 == 0) { if(d==29) { d = 1; m = "March"; }else { d = d+1; } }else { if(d==28) { d = 1; m = "March"; }else { d = d+1; } } break;
					case "March"	: if(d==31) { d = 1; m = "April"; }else { d = d+1; } break;
					case "April"	: if(d==30) { d = 1; m = "May"; }else { d = d+1; } break;
					case "May"		: if(d==31) { d = 1; m = "June"; }else { d = d+1; } break;
					case "June"		: if(d==30) { d = 1; m = "July"; }else { d = d+1; } break;
					case "July"		: if(d==31) { d = 1; m = "August"; }else { d = d+1; }break;
					case "August"	: if(d==31) { d = 1; m = "September"; }else { d = d+1; } break;
					case "September": if(d==30) { d = 1; m = "October"; }else { d = d+1; } break;
					case "October"	: if(d==31) { d = 1; m = "November"; }else { d = d+1; } break;
					case "November"	: if(d==30) { d = 1; m = "December"; }else { d = d+1; } break;
					case "December"	: if(d==31) { d = 1; m = "January"; y = y + 1; }else { d = d+1;} break;
				}$("#edate").val(d+" "+m+" "+y);
			} 
		} 
	}); 
});
</script>