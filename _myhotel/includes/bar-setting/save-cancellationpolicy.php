<?php
try {
	include("../../conf/connection.php");
	
	$startdate	= date('Y-m-d', strtotime($_POST['startdate']));
	$enddate	= date('Y-m-d', strtotime($_POST['enddate']));
	$cancellationpolicy	= $_POST['cancellationpolicy'];
	$channeloid = 1;
	
	if(isset($_POST['roomoffer'])){
		foreach($_POST['roomoffer'] as $key => $roomoffer){
			$stmt = $db->prepare("INSERT INTO cancellationpolicyapply (roomofferoid, channeloid, startdate, enddate, cancellationpolicyoid, publishedoid) VALUES (:a , :b , :c , :d , :e , :f)");
			$stmt->execute(array(':a' => $roomoffer, ':b' => $channeloid, ':c' => $startdate, ':d' => $enddate , ':e' => $cancellationpolicy , ':f' => 1));
		}
	}
	
}catch(PDOException $ex) {
	echo "0";
	die();
}
?>