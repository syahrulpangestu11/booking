<li <?php if($uri2=="dashboard"){ echo "class=active"; }?>>
    <a href="<?php echo"$base_url"; ?>/dashboard"><i class="fa fa-dashboard"></i><span>Menagement Dashboard</span></a>
</li>

<li class="treeview <?php if($uri2=="sales-marketing"){ echo "active"; }?>">
    <a href="#">
        <i class="fa fa-bullhorn"></i><span>Sales &amp; Marketing</span>
        <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
        <li <?php if($uri3=="hotel-assessment"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/sales-marketing/hotel-assessment"><i class="fa fa-circle-o"></i> Hotel Assessment</a></li>
        <li <?php if($uri3=="stage-process"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/sales-marketing/stage-process"><i class="fa fa-circle-o"></i> Stage Process</a></li>
        <li <?php if($uri3=="wdm-stage"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/sales-marketing/wdm-stage"><i class="fa fa-circle-o"></i> WDM</a></li>
        <li <?php if($uri3=="ibe-stage"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/sales-marketing/ibe-stage"><i class="fa fa-circle-o"></i> IBE</a></li>
        <li <?php if($uri3=="other-stage"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/sales-marketing/other-stage"><i class="fa fa-circle-o"></i> Other Stage</a></li>
        <li <?php if($uri3=="bispro-report"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/sales-marketing/bispro-report"><i class="fa fa-circle-o"></i> Bispro Report</a></li>
        <li <?php if($uri3=="kpi"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/sales-marketing/kpi"><i class="fa fa-circle-o"></i> KPI</a></li>
        <li <?php if($uri3=="daily-update-sm"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/sales-marketing/daily-update-sm"><i class="fa fa-circle-o"></i> Daily Update SM</a></li>
        <li <?php if($uri3=="management-summary"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/sales-marketing/management-summary"><i class="fa fa-circle-o"></i> Management Summary</a></li>
        <li <?php if($uri3=="production-detail"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/sales-marketing/production-detail"><i class="fa fa-circle-o"></i> Production Dtl</a></li>
        <li <?php if($uri3=="occupancy-detail"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/sales-marketing/occupancy-detail"><i class="fa fa-circle-o"></i> Distributed Dtl</a></li>
        <li <?php if($uri2=="alloccupancyreport"){ echo "class=active"; }?>><a href="<?php echo"$base_url"; ?>/alloccupancyreport"><i class="fa fa-circle-o"></i> All Occupancy Report</a></li>
    </ul>
</li>

<li <?php if($uri2=="hotel"){ echo "class=active"; }?>>
    <a href="<?php echo"$base_url"; ?>/hotel"><i class="fa fa-home"></i><span>Hotel</span></a>
</li>
