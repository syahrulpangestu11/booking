<?php
session_start();
date_default_timezone_set("Asia/Makassar");
header("Content-Type: application/xls");

header("Pragma: no-cache");
header("Expires: 0");

if(empty($_SESSION['_oid'])){
    die();
}

include("../../conf/connection.php");
include("../../includes/pms-report/function_report.php");

$checkin = $_POST['a'];
$stay = $_POST['b'];
$hoteloid = $_POST['c'];

$stmt = $db->prepare("SELECT hoteloid, hotelcode, hotelname FROM hotel WHERE hoteloid=:a AND publishedoid='1' ORDER BY hotelname");
$stmt->execute(array(':a' => $hoteloid));
$hotel = $stmt->fetch(PDO::FETCH_ASSOC);

header("Content-Disposition: attachment; filename=GuestDepositReport_".$hotel['hotelname']."_".date("YmdHis").".xls");
?>
<table id="example" class="display" cellspacing="0" width="100%" border="0">
  <thead>
    <tr><th colspan="14">Guest Deposit</th></tr>
    <tr><th colspan="14"><?=$hotel['hotelname']?></th></tr>
  </thead>
</table>

<table id="example" class="display" cellspacing="0" width="100%" border="1">
<thead>
    <tr>
      <th>Reservation ID</th>
      <th>Firstname</th>
      <th>Lastname</th>
      <th>Arrival Date</th>
      <th>Departure Date</th>
      <th>Nights</th>
      <th>Num of Adult/Child</th>
      <th>Rooms</th>
      <th>Status</th>
      <th>Currency</th>
      <th>Room Total</th>
      <th>Charges Total</th>
      <th>Paid</th>
      <th>Remaining Balance</th>
    </tr>
</thead>
<tbody>
    <?php
    $data = getReservationDataInHouse($db, $hoteloid, $checkin, $stay);
    foreach($data as $row){
        $bookingoid = $row['bookingoid'];
        $bookingnumber = $row['bookingnumber'];
        $firstname = $row['firstname'];
        $lastname = $row['lastname'];
        $checkin = date("d-M-Y", strtotime($row['checkin']));
        $checkout = date("d-M-Y", strtotime($row['checkout']));
        $diff = $row['diff'];
        $adult = empty($row['adult']) ? "0" : $row['adult'];
        $child = empty($row['child']) ? "0" : $row['child'];
        $roomnumber = $row['roomnumber'];
        $currcode = empty($row['currencycode']) ? "IDR" : $row['currencycode'];

        if($row['bookingstatusoid'] == "5"){
            $status = "Cancelled";
        }else if($row['bookingstatusoid'] == "7"){
            $status = "No Show";
        }else{
            $status = $row['status'];
        }

        echo '<tr>
            <td>'.$bookingnumber.'</td>
            <td>'.$firstname.'</td>
            <td>'.$lastname.'</td>
            <td align="center">'.$checkin.'</td>
            <td align="center">'.$checkout.'</td>
            <td align="center">'.$diff.'</td>
            <td align="center">'.$adult.'/'.$child.'</td>
            <td>'.$roomnumber.'</td>
            <td>'.$status.'</td>
            <td style="padding-left:10px">'.$currcode.'</td>
            <td align="right">'.number_format($row['totalroom'], 2, ',', '.').'</td>
            <td align="right">'.number_format($row['totalcharge'], 2, ',', '.').'</td>
            <td align="right">'.number_format($row['paid'], 2, ',', '.').'</td>
            <td align="right">'.number_format($row['paid_balance'], 2, ',', '.').'</td>
        </tr>';
    }
    ?>
</tbody>
</table>
