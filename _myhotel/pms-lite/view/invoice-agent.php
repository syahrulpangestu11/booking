<?php
try{
  session_start();
	error_reporting(E_ALL ^ E_NOTICE);
  include ('../../conf/connection.php');
  include("../../includes/pms-lite/class-pms-lite.php");

  $hoteloid = $_SESSION['_hotel'];
  $bookingnumber = $_REQUEST['bookingnumber'];

  $pmslite = new PMSReservation($db);
  $pmslite->setBooking($bookingnumber);
  $datarsv = $pmslite->getBookingData();
  $pmslite->setPMSHotel($datarsv['hoteloid']);

  $pmslitedata = new PMSEditRSV($db);
  $pmslitedata->setBooking($bookingnumber);
  $dataroom = $pmslitedata->getBookingRoomData();
  /*--------------------------------------*/
  $fileVersion = date("Y.m.d.H.i.s");
?>
<link rel="stylesheet" href="../../lib/bootstrap/css/bootstrap.min.css?v=<?=$fileVersion;?>">
<link rel="stylesheet" href="../style-print.css?v=<?=$fileVersion;?>">
<script type="text/javascript" src="../..//js/jquery-1.11.0.min.js"></script>
<?php  include("../component/script-view-pmslite.php"); ?>
<div id="container">
  <div id="print">
    <div class="row">
      <div class="col-md-offset-7 col-md-5">
        <h2><?=$pmslite->hoteldata['hotelname']?></h2>
        <?=$pmslite->hoteldata['address']?><br>
        Phone : <?=$pmslite->hoteldata['phone']?><br>
        E-mail : <?=$pmslite->hoteldata['email']?><br>
        Website : <?=$pmslite->hoteldata['website']?>
      </div>
    </div>
    <div class="row">
      <div class="col-md-7">
        <div class="box">
          <h3>BOOKING DETAILS</h3>
          <div class="row">
            <div class="col-md-12">
              <b>Agent Detail : </b><br>
              <?php
                $agent = $pmslite->agentDetail($datarsv['agentoid']);
                echo "<b>".$agent['agentname']." - ".$agent['phone']." / ".$agent['email']."</b>";
              ?>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <b>Guest Name :</b> <?=$datarsv['guestname']?><br>
              <b>Address :</b><br><?=$datarsv['address']?>
            </div>
            <div class="col-md-6">
              <b>Phone :</b> <?=$datarsv['phone']?><br>
              <b>E-mail :</b> <?=$datarsv['email']?>
            </div>
          </div>
          <div class="row">
            <div class="col-md-2"><b>Created on</b><br><?=date('d M Y', strtotime($datarsv['bookingtime']))?></div>
            <div class="col-md-6">
              <table>
                <thead><tr><th>Stay Details</th><th>Room(s) / Person(s)</th></tr></thead>
                <tbody>
                    <tr>
                      <td>
                        <?php
                          foreach($pmslite->detailStay() as $staydtl){
                            echo $staydtl['room']." (".$staydtl['roomnumber'].")<br>".date('d M', strtotime($staydtl['checkin']))." - ".date('d M', strtotime($staydtl['checkout']))." (".$staydtl['night']." night)<br>";
                          }
                        ?>
                      </td>
                      <td>
                        <?php
                        $stayoccupancy = $pmslite->summaryStayOccupancy();
                        echo $stayoccupancy['jmlroom']." room(s) / ". $stayoccupancy['person']." (". $stayoccupancy['adult']." adult / ".$stayoccupancy['child']." children)";
                        ?>
                      </td>
                    </tr>
                </tbody>
              </table>
            </div>
            <div class="col-md-4">
              <b>Amount :</b>
              <br>
              <?=$datarsv['currencycode']." ".number_format($datarsv['grandtotal'])?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row"><div class="col-md-12 text-center"><h1>ACCOUNT STATEMENT</h1></div></div>
    <div class="row">
      <div class="col-md-12">
        <table class="table table-bordered">
          <thead>
            <tr><th></th><th>Date</th><th>Description-References</th><th>Folio#</th><th>Disc/Allow</th><th>Charges</th><th>Tax</th><th>Payment</th></tr>
          </thead>
          <tbody>
            <?php
              $no = 0;
              foreach($dataroom as $dr){
            ?>
            <tr>
              <td><?=++$no?></td>
              <td><?=date('d M Y', strtotime($datarsv['bookingtime']))?></td>
              <td><?=$dr['promotion']?> <?=$dr['room']?> / <?=$pmslitedata->getRoomNumber($dr['bookingroomoid'])?></td>
              <td></td>
              <td></td>
              <td><?=$datarsv['currencycode']." ".number_format($dr['totalr'])?></td>
              <td></td>
              <td></td>
            </tr>
            <?php
              }
            ?>

            <?php
              foreach($pmslitedata->getBookingChargeData() as $key => $othercharges){
            ?>
              <tr>
                <td><?=++$no?></td>
                <td><?=date('d M Y', strtotime($othercharges['created']))?></td>
                <td>Qty <?=$othercharges['qty']?> <?=$othercharges['product']?> - <?=$othercharges['pos']?></td>
                <td></td>
                <td></td>
                <td><?=$datarsv['currencycode']." ".number_format($othercharges['total'])?></td>
                <td></td>
                <td></td>
              </tr>
            <?php
              }
            ?>

            <?php
              foreach($pmslite->paymentList() as $payment){
            ?>
              <tr>
                <td><?=++$no?></td>
                <td><?=date('d M Y', strtotime($payment['paymentdate']))?></td>
                <td><?=$payment['description']?></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td><?=$datarsv['currencycode']." ".number_format($payment['amount'])?></td>
              </tr>
            <?php
              }
            ?>
          </tbody>
          <tfoot>
            <tr><th colspan="3">Total</th><th></th><th></th><th><?=$datarsv['currencycode']." ".number_format($datarsv['totalroom']+$datarsv['totalextra'])?></th><th></th><th><?=$datarsv['currencycode']." ".number_format($datarsv['paid'])?></th></tr>
          </tfoot>
        </table>
      </div>
    </div>
    <div class="row"><div class="col-md-8 col-xs-6 text-right"><b>Booking Total</b></div><div class="col-md-4 col-xs-6"><?=$datarsv['currencycode']." ".number_format($datarsv['totalroom']+$datarsv['totalextra'])?></div></div>
    <div class="row"><div class="col-md-8 col-xs-6 text-right"><b>Other Charges</b></div><div class="col-md-4 col-xs-6"><?=$datarsv['currencycode']." ".number_format($datarsv['totalcharge'])?></div></div>
    <div class="row"><div class="col-md-8 col-xs-6 text-right"><b>Total Tax</b></div><div class="col-md-4 col-xs-6"></div></div>
    <div class="row"><div class="col-md-8 col-xs-6 text-right"><b>Total Disc/Allow</b></div><div class="col-md-4 col-xs-6"></div></div>
    <div class="row"><div class="col-md-8 col-xs-6 text-right"><b>Total Paid</b></div><div class="col-md-4 col-xs-6"><?=$datarsv['currencycode']." ".number_format($datarsv['paid'])?></div></div>
    <div class="row"><div class="col-md-8 col-xs-6 text-right"><b>Balance</b></div><div class="col-md-4 col-xs-6"><?=$datarsv['currencycode']." ".number_format($datarsv['paid_balance'])?></div></div>
  </div>
  <div id="action-button" class="row">
    <div class="col-xs-12 text-center">
      <button type="button" name="close" class="btn btn-sm btn-default">Close</button>
      <button type="button" name="print-invoice-agent" class="btn btn-sm btn-primary">Print</button>
    </div>
  </div>
</div>
<form id="print-invoice-agent" action="../print/pinvoice-agent.php" target="_blank" method="post">
  <input name="bookingnumber" type="hidden" value="<?=$datarsv['bookingnumber']?>">
</form>
<?php
}catch(Exception $e){
  echo $e->getMessage();
}
?>
