<?php
session_start();
date_default_timezone_set("Asia/Makassar");
header("Content-Type: application/xls");
header("Content-Disposition: attachment; filename=PaymentReport_".date("YmdHis").".xls");
header("Pragma: no-cache");
header("Expires: 0");

if(empty($_SESSION['_oid'])){
    die();
}

try{
    include("../../conf/connection.php");
    include("../../includes/pms-report/function_report.php");

    //$month = $_POST['month'];
    //$year = $_POST['year'];
    $hotelcode = $_POST['hotelcode'];

    $bdall = $_POST['bdall'];
    $bdstartdate = $_POST['bdstartdate'];
    $bdenddate = $_POST['bdenddate'];
    $ciall = $_POST['ciall'];
    $cistartdate = $_POST['cistartdate'];
    $cienddate = $_POST['cienddate'];

    //$armonth = array("January"=>1,"February"=>2,"March"=>3,"April"=>4,"May"=>5,"June"=>6,"July"=>7,"August"=>8,"September"=>9,"October"=>10,"November"=>11,"December"=>12);
    //$month = $armonth[$month];

    $startperiode = date('Y-m-d', strtotime($month));
    $lastperiode = date('Y-m-d', strtotime($year));

    $hl = "";
    if($hotelcode == '0'){
        $hotels = getAssignedHotels($db);
        if(count($hotels) <= 0){
            $hl .= "h.hoteloid='0'";
        }else{
            $tmp = array();
            foreach ($hotels as $v) {
                $tmp[] = $v['hoteloid'];
            }
            $hl .= "h.hoteloid IN (".implode(',', $tmp).")";
        }
        //$arr = array(':b'=>$startperiode, ':c'=>$lastperiode);
        
        $title_report = "All Property";
    }else{
        $hl = "h.hoteloid=:a";
        //$arr = array(':a'=>$hotelcode, ':b'=>$startperiode, ':c'=>$lastperiode);
        $arr = array(':a'=>$hotelcode);

        $stmt = $db->prepare("SELECT hoteloid, hotelcode, hotelname FROM hotel WHERE hoteloid=:a AND publishedoid='1' ORDER BY hotelname");
        $stmt->execute(array(':a' => $hotelcode));
        $hotel = $stmt->fetch(PDO::FETCH_ASSOC);
        $title_report = $hotel['hotelname'];
    }
?>
<table>
    <tr><td><?=$title_report?></td></tr>
    <tr><td>MONTHLY PAYMENT REPORT</td></tr>
    <tr><td><?=$month?> - <?=$year?></td></tr>
</table>
<table id="example" class="display" cellspacing="0" width="100%">
  <thead>
    <tr>
      <th>No</th>
      <th>Invoice Date</th>
      <th>Description</th>
      <th>Name of Villa</th>
      <th>Guest Name</th>
      <th>Agent Name</th>
      <th>Check In</th>
      <th>Check Out</th>
      <th>No Invoice</th>
      <th colspan="2">Total Invoice</th>
      <th colspan="2">Total Payment</th>
      <th colspan="2">Balance</th>
      <th>Payment Method</th>
      <th>Remark</th>
    </tr>
  </thead>
  <tbody>
    <?php
      $no = 0;

      $msg = "";
      if(!empty($bdall)){
        $msg .= "AND date(b.`dateofinvoice`)>='".date("Y-m-d", strtotime($bdstartdate))."' AND date(b.`dateofinvoice`)<='".date("Y-m-d", strtotime($bdenddate))."' ";
      }
      if(!empty($ciall)){
        $msg .= "AND date(br.`checkin`)>='".date("Y-m-d", strtotime($cistartdate))."' AND date(br.`checkin`)<='".date("Y-m-d", strtotime($cienddate))."' ";
      }

      $syntax_report = "select date(b.dateofinvoice) as invoicedate, h.hotelname, CONCAT(c.firstname, ' ', c.lastname) as guestname, ag.agentname, br.checkin, br.checkout, b.invnumofhotel, b.invnumofchain, cr.currencycode, b.grandtotal, b.paid, (b.grandtotal - b.paid) as balance, bp.paymentmethod, case when (b.grandtotal - b.paid) = 0 then 'Full Payment' when (b.grandtotal > b.paid and b.paid > 0) then 'Guest Deposit' else 'AR Outstanding' end as remarks
      from booking b
      inner join currency cr using (currencyoid)
      inner join customer c using (custoid)
      inner join hotel h using (hoteloid)
      inner join (select bookingoid, checkin, checkout from bookingroom where pmsstatusoid not in ('0') group by bookingoid) br on br.bookingoid = b.bookingoid
      left join agent ag using (agentoid)
      left join (select bookingoid, group_concat(DISTINCT method SEPARATOR ', ') as paymentmethod from bookingpaymentdtl inner join pmspaymenttype using (pmspaymenttypeoid) group by bookingoid) bp on bp.bookingoid = b.bookingoid
      where (".$hl.") and bookingstatusoid in (4,5,7) ".$msg."
      order by br.checkin";
      //echo $syntax_report;
      $q_report = $db->prepare($syntax_report);
      $q_report->execute($arr);
      foreach($q_report->fetchAll(PDO::FETCH_ASSOC) as $row){
        $invoicenumber = formatedInvoiceNagisa($row['invnumofhotel'], $row['invnumofchain'], $row['invoicedate']);

        if($row['currencycode'] == 'USD'){
          $totalinv['usd'] += $row['grandtotal'];
          $totalpaid['usd'] += $row['paid'];
          $totalblnc['usd'] += $row['balance'];
        }else{
          $totalinv['idr'] += $row['grandtotal'];
          $totalpaid['idr'] += $row['paid'];
          $totalblnc['idr'] += $row['balance'];
        }
    ?>
      <tr>
        <td><?=++$no?></td>
        <td align="center"><?=(empty($row['invoicedate']) ? '-' : date('d M Y', strtotime($row['invoicedate'])))?></td>
        <td>Villa Rental</td>
        <td><?=$row['hotelname']?></td>
        <td><?=$row['guestname']?></td>
        <td><?=$row['agentname']?></td>
        <td align="center"><?=date('d M Y', strtotime($row['checkin']))?></td>
        <td align="center"><?=date('d M Y', strtotime($row['checkout']))?></td>
        <td align="center"><?=$invoicenumber?></td>
        <td><?=$row['currencycode']?></td>
        <td align="right" style="border-left:none"><?php if($row['grandtotal'] > 0){ echo number_format($row['grandtotal'], 2, '.', ','); }else{ echo "0"; }?></td>
        <td><?=$row['currencycode']?></td>
        <td align="right" style="border-left:none"><?php if($row['paid'] > 0){ echo number_format($row['paid'], 2, '.', ','); }else{ echo "0"; }?></td>
        <td><?=$row['currencycode']?></td>
        <td align="right" style="border-left:none"><?php if($row['balance'] > 0){ echo number_format($row['balance'], 2, '.', ','); }else{ echo "0"; }?></td>
        <td align="center"><?=$row['paymentmethod']?></td>
        <td align="center"><?=$row['remarks']?></td>
      </tr>
    <?php
      }
    ?>
  </tbody>
  <tfoot>
    <tr>
      <th colspan="9" rowspan="2" style="text-align:center">TOTAL</th>
      <th>IDR</th>
      <th style="border-left:none; text-align:right"><?php if($totalinv['idr'] > 0){ echo number_format($totalinv['idr'], 2, '.', ','); }else{ echo "0"; }?></th>
      <th>IDR</th>
      <th style="border-left:none; text-align:right"><?php if($totalpaid['idr'] > 0){ echo number_format($totalpaid['idr'], 2, '.', ','); }else{ echo "0"; }?></th>
      <th>IDR</th>
      <th style="border-left:none; text-align:right"><?php if($totalblnc['idr'] > 0){ echo number_format($totalblnc['idr'], 2, '.', ','); }else{ echo "0"; }?></th>
      <th colspan="2" rowspan="2"></th>
    </tr>
    <tr>
      <th>USD</th>
      <th style="border-left:none; text-align:right"><?php if($totalinv['usd'] > 0){ echo number_format($totalinv['usd'], 2, '.', ','); }else{ echo "0"; }?></th>
      <th>USD</th>
      <th style="border-left:none; text-align:right"><?php if($totalpaid['usd'] > 0){ echo number_format($totalpaid['usd'], 2, '.', ','); }else{ echo "0"; }?></th>
      <th>USD</th>
      <th style="border-left:none; text-align:right"><?php if($totalblnc['usd'] > 0){ echo number_format($totalblnc['usd'], 2, '.', ','); }else{ echo "0"; }?></th>
    </tr>
  </tfoot>
</table>
<?php
}catch(Exception $e){
    echo "Failed to load the data! ".$e->getMessage();
    die();
}
?>
