<?php
session_start();
date_default_timezone_set("Asia/Makassar");
header("Content-Type: application/xls");
header("Content-Disposition: attachment; filename=ARLedgerReport_".date("YmdHis").".xls");
header("Pragma: no-cache");
header("Expires: 0");

if(empty($_SESSION['_oid'])){
    die();
}

include("../../conf/connection.php");
include("../../includes/pms-report/function_report.php");

$month = $_POST['a'];
$year = $_POST['b'];
$hoteloid = $_POST['c'];

$stmt = $db->prepare("SELECT hoteloid, hotelcode, hotelname FROM hotel WHERE hoteloid=:a AND publishedoid='1' ORDER BY hotelname");
$stmt->execute(array(':a' => $hoteloid));
$hotel = $stmt->fetch(PDO::FETCH_ASSOC);

?>
<table id="example" class="display" cellspacing="0" width="100%" border="1">
<thead>
    <tr>
        <th rowspan="2">Date</th>
        <th rowspan="2" style="width:250px">Description</th>
        <th rowspan="2" style="width:100px">Name of Villa</th>
        <th rowspan="2" style="width:125px">Guest Name</th>
        <th rowspan="2">Agent Name</th>
        <th rowspan="2">Check In</th>
        <th rowspan="2">Check Out</th>
        <th rowspan="2" style="width:145px">No Invoice</th>
        <th colspan="2">Amount</th>
        <th rowspan="2">Remarks</th>
        <th rowspan="2">Booking Ref</th>
        <th rowspan="2">Guest Status</th>
    </tr>
    <tr>
        <th class="bleft">IDR</th>
        <th>USD</th>
    </tr>
</thead>
<tbody>
    <?php
    $armonth = array("January"=>1,"February"=>2,"March"=>3,"April"=>4,"May"=>5,"June"=>6,"July"=>7,"August"=>8,"September"=>9,"October"=>10,"November"=>11,"December"=>12);
    $data = getReservationARLedgerRange($db, $hoteloid, $month, $year);
    $titles = getCustTitle($db);

    $idrtotal = 0; $usdtotal = 0;
    foreach($data as $row){
        if(isset($titles[$row['title']])) $title = $titles[$row['title']]; else $title = $row['title'];
        $guestname = $title." ".$row['firstname']." ".$row['lastname'];
        $checkin = date("d/m/Y", strtotime($row['checkin']));
        $checkout = date("d/m/Y", strtotime($row['checkout']));
        $created = date("d/m/Y", strtotime($row['created']));
        $desc = $row['description'];
        $agentname = empty($row['agentname']) ? "-" : $row['agentname'];
        if($row['bookingstatusoid'] == "5"){
            $status = "Cancelled";
        }else if($row['bookingstatusoid'] == "7"){
            $status = "No Show";
        }else{
            $status = $row['status'];
        }
        $note = empty($row['note']) ? "" : $row['note'];

        $idnuminv = $row['invnumofhotel'];
        $aidnuminv = $row['invnumofchain'];
        $invrawdate = $row['dateofinvoice'];
        switch(strlen($idnuminv)){
          case 1: $idnuminv='000'.$idnuminv; break;
          case 2: $idnuminv='00'.$idnuminv; break;
          case 3: $idnuminv='0'.$idnuminv; break;
        }
        switch(strlen($aidnuminv)){
          case 1: $aidnuminv='000'.$aidnuminv; break;
          case 2: $aidnuminv='00'.$aidnuminv; break;
          case 3: $aidnuminv='0'.$aidnuminv; break;
        }
        if(empty($invrawdate) or $invrawdate=='0000-00-00' or $invrawdate=='1970-01-01'){
            $noinvoice = "-";
        }else{
            $invdate = date('d-M-Y', strtotime($invrawdate));
            $invmonth = date('m', strtotime($invrawdate));
            $invyear = date('Y', strtotime($invrawdate));
            $noinvoice = $idnuminv."/NBPM-".$aidnuminv."/".$invmonth."/".$invyear;
        }
        $bookingnumber = $row['bookingnumber'];
        $currcode = empty($row['currencycode']) ? "IDR" : $row['currencycode'];
        if($currcode == "USD"){
            $usd = number_format($row['total'], 2, ',', '.');
            $idr = "";
            $usdtotal += $row['total'];
        }else{
            $idr = number_format($row['total'], 2, ',', '.');
            $usd = "";
            $idrtotal += $row['total'];
        }

        echo '<tr>
            <td>'.$created.'</td>
            <td>'.$desc.'</td>
            <td>'.$hotel['hotelname'].'</td>
            <td>'.$guestname.'</td>
            <td>'.$agentname.'</td>
            <td>'.$checkin.'</td>
            <td>'.$checkout.'</td>
            <td>'.$noinvoice.'</td>
            <td align="right">'.$idr.'</td>
            <td align="right">'.$usd.'</td>
            <td>'.$note.'</td>
            <td>'.$bookingnumber.'</td>
            <td>'.$status.'</td>
        </tr>';

    }
    ?>
</tbody>
<tfoot>
    <tr>
        <th colspan="8" align="center">TOTAL</th>
        <th align="right"><?=number_format($idrtotal, 2, ',', '.')?></th>
        <th align="right"><?=number_format($usdtotal, 2, ',', '.')?></th>
        <th></th>
        <th></th>
        <th></th>
    </tr>
</tfoot>
</table>
