<section>
  <h1>Select Room Type</h1>
</section>
<section class="bg-white">
<?php
  $q_hotelchain = "SELECT c.cityname, s.statename, h.hoteloid, h.hotelname, h.stars, h.publishedoid, ht.category from hotel h inner join hoteltype ht using (hoteltypeoid) inner join published using (publishedoid) inner join city c using (cityoid) inner join state s using (stateoid) inner join country ct using (countryoid) where h.hoteloid = '".$hoteloid."'";
  $stmt = $db->query($q_hotelchain);
  $hotelchain = $stmt->fetch(PDO::FETCH_ASSOC);
?>
<div class="row">
  <div class="col-md-12 title-hotel">
    <h3><?=$hotelchain['hotelname']?></h3>
    <i class="fa fa-map-marker"></i> <?=$hotelchain['cityname']?>
  </div>
</div>
<?php
  $q_roomtype = "select r.roomoid, r.name, r.publishedoid from room r inner join published p using (publishedoid) inner join hotel h using (hoteloid) where h.hoteloid = '".$hoteloid."' and p.publishedoid not in (3)";
  $stmt = $db->query($q_roomtype);
  $r_roomtype = $stmt->fetchAll(PDO::FETCH_ASSOC);
  foreach($r_roomtype as $roomtype){
?>
  <div class="row list-room">
    <div class="col-xs-8">
      <h4><?=$roomtype['name']?></h4>
      <?php if($roomtype['publishedoid'] == 1){ ?><span class="status active"><i class="fa fa-bookmark"></i> active</span><?php }else{ ?><span class="status notactive"><i class="fa fa-bookmark-o"></i> not active</span><?php } ?>
    </div>
    <div class="col-xs-4 text-right"><button type="button" class="btn btn-primary" act="manage-room" data-id="<?=$roomtype['roomoid']?>">MANAGE <i class="fa fa-chevron-right"></i></button></div>
  </div>
<?php
  }
?>
</section>
