<?php
  include('includes/bootstrap.php');
  include('mobile/includes/script-mobile.php');
?>
<link rel="stylesheet" href="<?php echo"$base_url"; ?>/mobile/css/style.css" type="text/css">
<header>
  <div id="xheader">
      <div id="logo"><img src="<?=$base_url?>/images/logo.png"></div>
      <div class="menu-side">
          <section class="parent">
              <ul class="parent-menu">
                <li><a href="#" act="switch-to-dekstop"><i class="fa fa-television"></i> <span style="display:inline;">Full Dahsboard</span></a></li>
              </ul>
          </section>
      </div>
  </div>
  <?php include('includes/header.page.php'); ?>
</header>
<section class="mobile-ver-content">
  <?php
    switch($uri2){
      case "manage-room" : include('page/manage-room.php'); break;
      case "process-manage-room" : include('page/process-manage-room.php'); break;
      default : include('page/list-room.php'); break;
    }
  ?>

  <section>
    <div class="row">
      <div class="col-md-12">
        <button type="button" class="btn btn-success full-width" name="dekstop-ver" act="switch-to-dekstop">View Full Dashboard</button>
      </div>
    </div>
  </section>
</section>
