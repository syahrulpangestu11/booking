<?php
class ReconciledBooking{
	var $bookingoid;
	var $bookingnumber;
	var $commission;
	var $hoteloid;

	public function __construct($db){
		$this->db = $db;
	}

	public function setBooking($bookingoid){
		$stmt = $this->db->prepare("select bookingoid, bookingnumber, bookingstatusoid, gbhpercentage, hoteloid from booking where bookingoid = :a");
		$stmt->execute(array(':a' => $bookingoid));

		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		$this->bookingoid		= $result['bookingoid'];
		$this->bookingnumber	= $result['bookingnumber'];
		$this->hoteloid			= $result['hoteloid'];
		$this->commission		= $result['gbhpercentage'];
	}

	public function reconcileRoomDate($bookingroom_non_reconcile, $bookingroom){
		$stmt = $this->db->query("update bookingroomdtl set reconciled = '1' where bookingroomdtloid not in ('".$bookingroom_non_reconcile."') and bookingroomoid in  ('".$bookingroom."')");
	}

	public function summaryPerBookingRoom($bookingroomoid){
		$stmt = $this->db->prepare("select br.bookingroomoid, sum(brd.rateafter) as roomtotal, sum(brd.extrabed) as extrabedtotal from bookingroomdtl brd inner join bookingroom br using (bookingroomoid) where br.bookingroomoid = :a and reconciled = :b");
		$stmt->execute(array(':a' => $bookingroomoid, ':b' => 0));
		$result = $stmt->fetch(PDO::FETCH_ASSOC);

		return $result;
	}

	public function detailTotalBooking(){
		$stmt = $this->db->query("select roomtotal, extratotal, otherchargetotal, ifnull(roomtotal, 0) + ifnull(extratotal, 0) + ifnull(otherchargetotal, 0) as grandtotal from (select (select sum(totalr) from bookingroom where bookingoid = '".$this->bookingoid."' group by bookingoid) as roomtotal, (select sum(total) from bookingextra where bookingoid = '".$this->bookingoid."' group by bookingoid) as extratotal, (select sum(total) from bookingcharges where bookingoid = '".$this->bookingoid."' group by bookingoid) as otherchargetotal) as allbookingdtl");
		$result = $stmt->fetch(PDO::FETCH_ASSOC);

		return $result;
	}

	public function updateBookingTotal($bookingtotal, $commission){
		$thebukingcollect = $bookingtotal * $commission / 100;
		$hotelcollect = $bookingtotal - $thebukingcollect;

		$stmt = $this->db->prepare("update booking set grandtotalr = :a, gbhcollect = :b, hotelcollect = :c where bookingoid = :id");
		$stmt->execute(array(':a' => $bookingtotal, ':b' => $thebukingcollect, ':c' => $hotelcollect, ':id' => $this->bookingoid));
	}
}


try {

	include "../conf/connection.php";
	$booking		= new ReconciledBooking($db);
	$booking->setBooking($_POST['bid']);

	$brd_non_reconcile = implode("','", $_POST['brd']);
	$br = implode("','", $_POST['br']);
	$booking->reconcileRoomDate($brd_non_reconcile, $br);


	foreach($_POST['br'] as $key => $bookingroomoid){
		$roomtotal = $_POST['totalreconcile'][$key];
		$checkout = date('Y-m-d', strtotime(str_replace('/','-',$_POST['checkout'][$key])));
		$dtlroom = $booking->summaryPerBookingRoom($bookingroomoid);
		$totalperroom = $roomtotal + $dtlroom['extrabedtotal'];

		/*---- UPDATE DETAIL ROOM ----*/
		$update_br_roomtotal = "update bookingroom set roomtotalr = '".$roomtotal."', extrabedtotal = '".$dtlroom['extrabedtotal']."', totalr = '".$totalperroom."', checkoutr = '".$checkout."' WHERE bookingroomoid = '".$bookingroomoid."'";
		$stmt = $db->query($update_br_roomtotal);
	}

	$dtltotalbooking = $booking->detailTotalBooking();
	$booking->updateBookingTotal($dtltotalbooking['grandtotal'], $booking->commission);

	echo "success";

}catch(Exception $ex) {
	echo "Invalid Query";
	print($ex ->getMessage());
	die();
}
?>
