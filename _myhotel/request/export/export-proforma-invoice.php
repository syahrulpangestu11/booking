<?php
session_start();
ob_start();
require_once('../../lib/tcpdf/tcpdf.php');
include('../../conf/connection.php');

$invoiceoid = $_REQUEST['io'];

/*--------------------------------------*/
$fileVersion = date("Y.m.d.H.i.s");
$today = date("Ymd");
$now = date("YmdHis");
/*--------------------------------------*/
$sInv = "SELECT i.*, ist.status FROM invoice i INNER JOIN invoicestatus ist USING (invoicestatusoid) WHERE i.invoiceoid = :a";
$stmt = $db->prepare($sInv);
$stmt->execute(array(':a' => $_GET['io']));
$inv = $stmt->fetch(PDO::FETCH_ASSOC);
    $amount_reconciled = $_GET['reconciledamount_temp'];
    $amount_subtotal = $_GET['reconciledamount_temp'];
    $amount_vat = $amount_subtotal * $_GET['ppn'] / 100;
    $amount_total = $amount_subtotal + $amount_vat;


$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Extranet Powered by TheBuking');
$pdf->SetTitle('Proforma Invoice TheBuking - '.$inv['invoicenumber'].' - '.$inv['hotelname'].' - '.$today);
$pdf->SetSubject('Invoce to '.$inv['hotelname']);
$pdf->SetKeywords('Invoce to Hotel', 'invoice', $inv['invoicenumber']);

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set margins
$pdf->SetMargins(8, 10, 8, true);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}
// $fontname = $pdf->addTTFfont('../../css/fonts/Roboto-Regular.ttf', 'TrueTypeUnicode', '', 32);
$pdf->SetFont('helvetica', '', 10);

$pdf->AddPage();

$html = '
<table style=""  cellpadding="5" cellspacing="0">
    <tr>
        <td style="width: 8%; border-left: 3px solid #333; border-top: 3px solid #333; font-size: 1px;">&nbsp;</td>
        <td style="width: 2%; border-top: 3px solid #333; font-size: 1px;">&nbsp;</td>
        <td style="width: 30%; border-top: 3px solid #333; font-size: 1px;">&nbsp;</td>
        <td style="width: 17%; border-top: 3px solid #333; font-size: 1px;">&nbsp;</td>
        <td style="width: 12%; border-top: 3px solid #333; font-size: 1px;">&nbsp;</td>
        <td style="width: 2%; border-top: 3px solid #333; font-size: 1px;">&nbsp;</td>
        <td style="width: 13%; border-top: 3px solid #333; font-size: 1px;">&nbsp;</td>
        <td style="width: 5%; border-top: 3px solid #333; font-size: 1px;">&nbsp;</td>
        <td style="width: 11%; border-right: 3px solid #333; border-top: 3px solid #333; font-size: 1px; ">&nbsp;</td>
    </tr>
    <tr>
        <td style="border-left: 3px solid #333; vertical-align: top;" colspan="5"><h3 style="font-size: 15px;">PT Wesolve Solusi Indonesia</h3><h1 style="font-size: 25px;">PROFORMA INVOICE</h1>
        </td>
        <td style="border-right: 3px solid #333; text-align: center;" colspan="4">
            <img alt="Logo" src="https://thebuking.com/pict/wsi-logo.jpg" style="height: 80px;" border="0">
        </td>
    </tr>
    <tr>
        <td style="border-left: 3px solid #333; vertical-align: top;">Office</td>
        <td style="vertical-align: top;">:</td>
        <td>Gedung Graha 9</td>
        <td></td>
        <td style="border-bottom: 3px solid #C41E32;">NPWP</td>
        <td style="border-bottom: 3px solid #C41E32;">:</td>
        <td style="border-bottom: 3px solid #C41E32; border-right: 3px solid #333;" colspan="3">'.$_GET['npwp'].'</td>
    </tr>
    <tr>
        <td style="border-left: 3px solid #333; vertical-align: top;"></td>
        <td style="vertical-align: top;">:</td>
        <td>Jl. Penataran No. 09 Pegangsaan, Menteng
            <br>Jakarta Pusat, DKI Jakarta
            <br>Telp. : (021) 8068 3025
        </td>
        <td></td>
        <td></td>
        <td></td>
        <td style="border-right: 3px solid #333;" colspan="3"></td>
    </tr>
    <tr>
        <td colspan="9" style="border-left: 3px solid #333; border-right: 3px solid #333; border-bottom: 1px solid #333;">&nbsp;</td>
    </tr>
    <tr>
        <td style="border-left: 3px solid #333;">To</td>
        <td>:</td>
        <td colspan="2">'.$_GET['pthotel'].'</td>
        <td style="border-left: 1px solid #333;">Invoice No.</td>
        <td>:</td>
        <td style="border-left: 1px solid #333; border-bottom: 1px solid #333; border-right: 3px solid #333;" colspan="3">'.$_GET['invoicenumber'].'</td>
    </tr>
    <tr>
        <td style="border-left: 3px solid #333;"></td>
        <td></td>
        <td colspan="2">('.$inv['hotelname'].')</td>
        <td style=" border-left: 1px solid #333;">Contract No.</td>
        <td>:</td>
        <td style="border-left: 1px solid #333; border-bottom: 1px solid #333; border-right: 3px solid #333;" colspan="3">'.$_GET['contractnumber'].'</td>
    </tr>
    <tr>
        <td style="border-left: 3px solid #333;">Attn</td>
        <td>:</td>
        <td colspan="2">'.$_GET['attentionpic'].'</td>
        <td style=" border-left: 1px solid #333;">Date</td>
        <td>:</td>
        <td style="border-left: 1px solid #333; border-right: 3px solid #333;" colspan="3">'.date("j M Y").'</td>
    </tr>
    <tr>
        <td colspan="9" style="border-left: 3px solid #333; border-right: 3px solid #333; border-top: 1px solid #333;">&nbsp;</td>
    </tr>
    <tr>
        <td style="text-align: center; font-weight: bold; background-color: #bbb; border-left: 3px solid #333; border-top: 1px solid #333; border-right: 1px solid #333;">No.</td>
        <td style="text-align: center; font-weight: bold; background-color: #bbb; border-top: 1px solid #333; border-right: 1px solid #333;" colspan="6">Description</td>
        <td style="text-align: center; font-weight: bold; background-color: #bbb; border-right: 3px solid #333; border-top: 1px solid #333;" colspan="2">Amount</td>
    </tr>
    <tr>
        <td style="border-top: 1px solid #333; border-left: 3px solid #333; border-right: 1px solid #333">&nbsp;</td>
        <td style="border-top: 1px solid #333; border-right: 1px solid #333" colspan="6">&nbsp;</td>
        <td style="border-top: 1px solid #333; border-right: 3px solid #333;" colspan="2">&nbsp;</td>
    </tr>
    <tr>
        <td style="border-right: 1px solid #333; border-left: 3px solid #333; text-align: center;">1</td>
        <td style="border-right: 1px solid #333" colspan="6">'.$_GET['invoicedescription'].'</td>
        <td style="">Rp</td>
        <td style="text-align: right; border-right: 3px solid #333;">'.number_format($amount_reconciled).'</td>
    </tr>
    <tr>
        <td style="border-right: 1px solid #333; border-left: 3px solid #333; text-align: center;">&nbsp;</td>
        <td style="border-right: 1px solid #333" colspan="6">&nbsp;</td>
        <td style="">&nbsp;</td>
        <td style="text-align: right; border-right: 3px solid #333;">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="7" style="border-top: 3px solid #333; border-left: 3px solid #333;">Sub Total</td>
        <td style="border-top: 3px solid #333;">Rp</td>
        <td style="border-top: 3px solid #333; text-align: right; border-right: 3px solid #333;">'.number_format($amount_subtotal).'</td>
    </tr>
    <tr>
        <td colspan="7" style="border-top: 1px solid #333; border-left: 3px solid #333;">VAT</td>
        <td style="border-top: 1px solid #333;">Rp</td>
        <td style="border-top: 1px solid #333; text-align: right; border-right: 3px solid #333;">'.number_format($amount_vat).'</td>
    </tr>
    <tr>
        <td colspan="7" style="border-top: 1px solid #333; border-left: 3px solid #333;"><b>Total</b></td>
        <td style="border-top: 1px solid #333;"><b>Rp</b></td>
        <td style="border-top: 1px solid #333; text-align: right; border-right: 3px solid #333;"><b>'.number_format($amount_total).'</b></td>
    </tr>
    <tr>
        <td colspan="9" style="border-top: 3px solid #333;">&nbsp;</td>
    </tr>

    <tr>
        <td style="border-top: 3px solid #333; border-left: 3px solid #333; border-bottom: 3px solid #333;">Due</td>
        <td style="border-top: 3px solid #333; border-bottom: 3px solid #333;">:</td>
        <td colspan="7" style="border-top: 3px solid #333; border-bottom: 3px solid #333; border-right: 3px solid #333;"></td>
    </tr>
    <tr>
        <td colspan="9" style="">&nbsp;</td>
    </tr>
    <tr>
        <td style="border: 3px solid #333;" colspan="3">
            <i><b>Please remit your payment to:</b></i>
            <br>
            <br> <b>PT Wesolve Solusi Indonesia</b>
            <br> <b>Acc. No (IDR): 092 145 4120</b>
            <br> <b>Bank Central Asia (BCA) Cabang Tebet</b>
            <br> Gedung Gajah Blok N-O
            <br> Jl. Dr. Saharjo No. 111, Tebet
            <br> Jakarta 12820                
        </td>
        <td>&nbsp;</td>
        <td colspan="5" style="vertical-align: top; text-align: center;">Jakarta, '.date("j M Y").'</td>
    </tr>
    <tr>
        <td colspan="4">&nbsp;</td>
        <td colspan="5" style="text-align: center;"><b>( Lalo Siahaan )</b>
            <br>Direktur Keuangan
        </td>
    </tr>
</table>
    ';
    
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->lastPage();
// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('Proforma Invoice TheBuking - '.$inv['hotelname'].' - '.$today.'.pdf', 'D');
?>
