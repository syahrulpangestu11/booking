<?php
	include('../../conf/connection.php');
	include('../../includes/class/reporting1.php');
	
	$hoteloid	= $_GET['hoteloid'];
	if(isset($_GET['year'])){ $year = $_GET['year']; }else{ $year = date('Y'); }
	
	$report = new Reporting1($db);
	$report->setHotel($hoteloid);

    $data = array();
	$n = 0;
	for($m=1; $m<=12; $m++){
		$n++;
		$periode1 = $year.'-'.$m.'-01'; 
		$periode2 = date("Y-m-t",strtotime($periode1));
		
		$report->setPeriode($periode1, $periode2);
		
		$total_confirmed = $report->materializeBookingTotal('grandtotal', 4);
		$total_cancelled = $report->materializeBookingTotal('cancellationamount', array(5,7,8));
		$total_revenue = $total_confirmed + $total_cancelled;
		$total_commission = $report->materializeBookingCommission();
                                
        $typecomm = '';
        /*base commission*/
        $query_base_commission = "select value as base, typecomm from hotelcommission where hoteloid = '".$hoteloid."' and ( (startdate <= '".$periode1."' and enddate >= '".$periode1."') or (startdate <= '0000-00-00' and enddate >= '0000-00-00')) and type = 'base' order by priority ASC limit 1";
        $stmt	= $db->query($query_base_commission);
        if($stmt->rowCount()){
            $result = $stmt->fetch(PDO::FETCH_ASSOC);
            $typecomm = $result['typecomm'];
        }
        /*override commission*/
        $query_override_commission = "select value as override, typecomm from hotelcommission where hoteloid = '".$hoteloid."' and ( (startdate <= '".$periode1."' and enddate >= '".$periode1."') or (startdate <= '0000-00-00' and enddate >= '0000-00-00')) and type = 'override' order by priority ASC limit 1";
        $stmt	= $db->query($query_override_commission);
        if($stmt->rowCount()){
            $result = $stmt->fetch(PDO::FETCH_ASSOC);
            $typecomm = $result['typecomm'];
        }
        if(empty($typecomm) or $typecomm == '2' or $typecomm == '4') $total_commission = 0;
		
		array_push($data, array("No" => $n, "Periode" => date('F Y', strtotime($periode1)), "1st Stay Periode" => date('d F Y', strtotime($periode1)), "2nd Stay Periode" => date('d F Y', strtotime($periode2)), "Total Booking" => $report->materializeBooking(), "Room Night" => $report->materializeRoomNight(), "Confirmed Booking" => number_format($total_confirmed), "Penalty Cancelled" => number_format($total_cancelled), "Total Revenue" => number_format($total_revenue), "Total Commission" => number_format($total_commission)));
	}
    
    function filterData(&$str)
    {
        $str = preg_replace("/\t/", "\\t", $str);
        $str = preg_replace("/\r?\n/", "\\n", $str);
        if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
    }
    
    // file name for download
    $fileName = "commission_report_summary_".date('mdY').".xls";
    
    // headers for download
    header("Content-Disposition: attachment; filename=\"$fileName\"");
    header("Content-Type: application/vnd.ms-excel");
    
    $flag = false;
    foreach($data as $row) {
        if(!$flag) {
            // display column names as first row
            echo implode("\t", array_keys($row)) . "\n";
            $flag = true;
        }
        // filter data
        array_walk($row, 'filterData');
        echo implode("\t", array_values($row)) . "\n";

    }
    
    exit;
?>