<?php
	$stmt = $db->prepare("select affhotelwdmtemplateoid from affhotelwdmtemplate where hoteloid = :a");
	$stmt->execute(array(':a' => $_POST['hoteloid']));
	$foundtemplate = $stmt->rowCount();
	if($foundtemplate == 0){
		$stmt = $db->prepare("insert into affhotelwdmtemplate (hoteloid, wdmtemplateoid) value (:a, :b)");
		$stmt->execute(array(':a' => $_POST['hoteloid'], ':b' => $_POST['wdmtemplate']));
	}else{
		$stmt = $db->prepare("update affhotelwdmtemplate set wdmtemplateoid = :b where hoteloid = :a");
		$stmt->execute(array(':a' => $_POST['hoteloid'], ':b' => $_POST['wdmtemplate']));
	}
?>