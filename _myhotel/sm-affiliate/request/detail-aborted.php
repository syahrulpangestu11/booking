<?php
	session_start();
	error_reporting(E_ALL ^ E_NOTICE);
	include ('../../conf/connection.php');

	$stmt		= $db->prepare("select affiliatehotelstatusoid, status from affiliatehotelstatus as `as` where as.affiliatehotelstatusoid = :a");
	$stmt->execute(array(':a' => $_GET['status']));
	$statussm	= $stmt->fetch(PDO::FETCH_ASSOC);

	$stmt	= $db->prepare("select h.hotelname, h.hoteloid, h.createdby, h.affiliatehotelstatusoid, as.status, h.affibestatusoid, aw.ibestatus, h.affibestatusoid, ai.ibestatus from hotel as `h` inner join affiliatehotelstatus as `as` using (affiliatehotelstatusoid) left join affibestatus as `aw` using (affibestatusoid) left join affibestatus as `ai` using (affibestatusoid) where h.hoteloid = :a");
	$stmt->execute(array(':a' => $_GET['id']));
	$affsm	= $stmt->fetch(PDO::FETCH_ASSOC);
?>

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">[<?=$statussm['status'];?>] Change Sales and Marketing Status</h4>
        <h5 id="hotelname"><?=$affsm['hotelname'];?></h5>
        <i class="fa fa-user"></i> <?=$affsm['createdby'];?> <span id="hotel-creator"></span><input type="hidden" name="hoteloid" value="<?=$affsm['hoteloid'];?>" />
    </div>
    <div class="modal-body">
			<div class="row">
			    <div class="col-md-6">
			        <div class="row">
			            <div class="col-lg-12">
			                <div class="form-group">
			                    <label class="control-label col-lg-4 text-left">Current Status</label>
			                    <div class="col-lg-6">
			                        <input type="text" name="currentstatus" class="form-control" readonly value="<?=$affsm['status']?>"><input type="hidden" name="statusfrom"  value="<?=$affsm['affiliatehotelstatusoid']?>"/>
			                    </div>
			                </div>
			                <div class="form-group">
			                    <label class="control-label col-lg-4 text-left">Change Status to</label>
			                    <div class="col-lg-6">
			                        <select name="statusto" class="form-control chzn-select" tabindex="7">
			                            <?php
											$statusto = array(13);
											foreach($statusto as $status){
												$stmt = $db->query("SELECT `affiliatehotelstatusoid`, `status` FROM `affiliatehotelstatus` as `as` where `as`.`affiliatehotelstatusoid` = '".$status."' and `as`.`publishedoid` = '1'");
												$sm = $stmt->fetch(PDO::FETCH_ASSOC);
										?>
										<option value="<?=$sm['affiliatehotelstatusoid'];?>"><?=$sm['status'];?></option>
			                            <?php
											}
										?>
			                        </select>
			                    </div>
			                </div>
			            </div>
			        </div>
			        <div class="row">
						<?php
						foreach($statusto as $status){
							$stmt = $db->query("SELECT `affiliatehotelstatusoid`, `status` FROM `affiliatehotelstatus` as `as` where `as`.`affiliatehotelstatusoid` = '".$status."' and `as`.`publishedoid` = '1'");
							$sm = $stmt->fetch(PDO::FETCH_ASSOC);
						?>
			            <div class="box dark box-detail-status" id="box-status-<?=$sm['affiliatehotelstatusoid'];?>">
			                <header>
			                    <h5><?=$sm['status'];?></h5>
			                    <!-- .toolbar -->
			                    <div class="toolbar">
			                      <nav style="padding: 8px;">
			                          <a href="javascript:;" class="btn btn-default btn-xs collapse-box">
			                              <i class="fa fa-minus"></i>
			                          </a>
			                          <a href="javascript:;" class="btn btn-default btn-xs full-box">
			                              <i class="fa fa-expand"></i>
			                          </a>
			                      </nav>
			                    </div>            <!-- /.toolbar -->
			                </header>
			                <?php include ('../page/status-modal/form/form-sm-'.$status.'.php'); ?>
						</div>
						<?php
						}
			            ?>
			    </div>
			    </div>
			    <div class="col-md-6">
			        <?php include('../page/status-modal/email/email-internal.php'); ?>
			    </div>
			</div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary change-status">Save changes</button>
    </div>
