<?php
try {
	if(empty($_POST['dateoverride'])){
		$logtime = date('Y-m-d H:i:s');
	}else{
		$logtime = date('Y-m-d H:i:s', strtotime($_POST['dateoverride']));
	}


	$stmt = $db->prepare("select affiliatehotelstatusoid from hotel h where h.hoteloid = :a");
	$stmt->execute(array(':a' => $_POST['hoteloid']));
	$row		= $stmt->fetch(PDO::FETCH_ASSOC);
	$currenthotelstatus = $row['affiliatehotelstatusoid'];

	if(isset($_POST['statusfrom']) and !empty($_POST['statusfrom'])){
		$from_type 		= 'sm';
		$from_status	= $_POST['statusfrom'];
		$stmt = $db->prepare("select affiliatelogoid, al.startdate from affiliatelog al inner join hotel h using (hoteloid) where to_type = :a and to_status = :b and al.hoteloid = :c order by al.startdate desc limit 1");
		$stmt->execute(array(':a' => $from_type, ':b' => $from_status, ':c' => $_POST['hoteloid']));
		$log		= $stmt->fetch(PDO::FETCH_ASSOC);
		$lastlog	= $log['affiliatelogoid'];
		$lasttime	= $log['startdate'];
		$idletime	= knowIdleTime($lasttime, $logtime);
	}

	if(isset($_POST['ibefrom']) and !empty($_POST['ibefrom'])){
		$from_type		= 'ibe';
		$from_status	= $_POST['ibefrom'];
		$stmt = $db->prepare("select affiliatelogoid, al.startdate from affiliatelog al inner join hotel h using (hoteloid) where to_type = :a and to_status = :b and al.hoteloid = :c order by al.startdate desc limit 1");
		$stmt->execute(array(':a' => $from_type, ':b' => $from_status, ':c' => $_POST['hoteloid']));
		$log		= $stmt->fetch(PDO::FETCH_ASSOC);
		$lastlog	= $log['affiliatelogoid'];
		$lasttime	= $log['startdate'];
		$idletime	= knowIdleTime($lasttime, $logtime);
	}

	if(isset($_POST['wdmfrom']) and !empty($_POST['wdmfrom'])){
		$from_type		= 'wdm';
		$from_status	= $_POST['wdmfrom'];
		$stmt = $db->prepare("select affiliatelogoid, al.startdate from affiliatelog al inner join hotel h using (hoteloid) where to_type = :a and to_status = :b and al.hoteloid = :c order by al.startdate desc limit 1");
		$stmt->execute(array(':a' => $from_type, ':b' => $from_status, ':c' => $_POST['hoteloid']));
		$log		= $stmt->fetch(PDO::FETCH_ASSOC);
		$lastlog	= $log['affiliatelogoid'];
		$lasttime	= $log['startdate'];
		$idletime	= knowIdleTime($lasttime, $logtime);
	}

	if(isset($_POST['statusto']) and !empty($_POST['statusto'])){
		$to_type = 'sm';
		if(empty($_POST['note']) or !isset($_POST['note'])){ $_POST['note'] = ""; }

		$list_status_to = array($_POST['statusto']);
		if($_POST['statusto'] == 14){
				$list_status_to = array(6,7);
		}else if($_POST['statusto'] == 15){
			$list_status_to = array(8);
		}
		foreach($list_status_to as $to_status){
			$stmt = $db->prepare("insert into affiliatelog (hoteloid, startdate, from_type, from_status, to_type, to_status, note, useroid, displayname) values (:a, :b, :c, :d, :e, :f, :g, :h, :i)");
			$stmt->execute(array(':a' => $_POST['hoteloid'], ':b' => $logtime, ':c' => $from_type, ':d' => $from_status, ':e' => $to_type, ':f' => $to_status, ':g' => $_POST['note'], ':h' => $_SESSION['_oid'], ':i' => $_SESSION['_initial']));
			$affiliatelogid = $db->lastInsertId();
		}

		// change to commerce from ibe ready to commerce
		if($_POST['ibefrom'] == 5 and $_POST['statusto'] == 8 and $currenthotelstatus == 14){
			$_POST['statusto'] = 15;
		}else if($_POST['statusfrom'] == 7 and $_POST['statusto'] == 8 and $currenthotelstatus == 14){
			$_POST['statusto'] = 6;
		}

		$stmt = $db->prepare("update hotel set affiliatehotelstatusoid = :a where hoteloid = :b");
		$stmt->execute(array(':a' => $_POST['statusto'], ':b' => $_POST['hoteloid']));

		$stmt = $db->prepare("update affiliatelog set enddate = :a, idletime = :b where affiliatelogoid = :c");
		$stmt->execute(array(':a' => $logtime, ':b' => $idletime, ':c' => $lastlog));

		// stop implementation progress
		if($_POST['statusto'] == 4 or $_POST['statusto'] == 13){
			include('stop-implementation-progress.php');
		}
	}

	switch($_POST['statusto']){
		case 2 : include('hotel-bussiness-review.php'); break;
		case 5 : include('subscribe-form.php'); break;
		default : break;
	}


	if(isset($_POST['ibeto']) and !empty($_POST['ibeto'])){
		$to_type = 'ibe';
		if(empty($_POST['note_ibe']) or !isset($_POST['note_ibe'])){ $_POST['note_ibe'] = ""; }

		$stmt = $db->prepare("insert into affiliatelog (hoteloid, startdate, from_type, from_status, to_type, to_status, note, useroid, displayname) values (:a, :b, :c, :d, :e, :f, :g, :h, :i)");
		$stmt->execute(array(':a' => $_POST['hoteloid'], ':b' => $logtime, ':c' => $from_type, ':d' => $from_status, ':e' => $to_type, ':f' => $_POST['ibeto'], ':g' => $_POST['note_ibe'], ':h' => $_SESSION['_oid'], ':i' => $_SESSION['_initial']));
		$affiliatelogid = $db->lastInsertId();

		$stmt = $db->prepare("update hotel set affibestatusoid = :a where hoteloid = :b");
		$stmt->execute(array(':a' => $_POST['ibeto'], ':b' => $_POST['hoteloid']));

		$stmt = $db->prepare("update affiliatelog set enddate = :a, idletime = :b where affiliatelogoid = :c");
		$stmt->execute(array(':a' => $logtime, ':b' => $idletime, ':c' => $lastlog));
	}

	if(isset($_POST['wdmto']) and !empty($_POST['wdmto'])){
		$to_type = 'wdm';
		if(empty($_POST['note_wdm']) or !isset($_POST['note_wdm'])){ $_POST['note_wdm'] = ""; }

		$stmt = $db->prepare("insert into affiliatelog (hoteloid, startdate, from_type, from_status, to_type, to_status, note, useroid, displayname) values (:a, :b, :c, :d, :e, :f, :g, :h, :i)");
		$stmt->execute(array(':a' => $_POST['hoteloid'], ':b' => $logtime, ':c' => $from_type, ':d' => $from_status, ':e' => $to_type, ':f' => $_POST['wdmto'], ':g' => $_POST['note_wdm'], ':h' => $_SESSION['_oid'], ':i' => $_SESSION['_initial']));
		$affiliatelogid = $db->lastInsertId();

		$stmt = $db->prepare("update hotel set affwdmstatusoid = :a where hoteloid = :b");
		$stmt->execute(array(':a' => $_POST['wdmto'], ':b' => $_POST['hoteloid']));

		$stmt = $db->prepare("update affiliatelog set enddate = :a, idletime = :b where affiliatelogoid = :c");
		$stmt->execute(array(':a' => $logtime, ':b' => $idletime, ':c' => $lastlog));
	}

	/* TEMPLATE ----------------------------------------------*/
	if(isset($_POST['wdmtemplate'])){
		include('change-hotel-template.php');
	}
	/* DETAIL ----------------------------------------------*/
	$status_dtl_sm	= array(3);
	$status_dtl_ibe	= array(2, 3);
	$status_dtl_wdm	= array(3, 4);
	if(
		(in_array($_POST['statusfrom'], $status_dtl_sm) and $_POST['statusto'] == 0)
		or (in_array($_POST['ibefrom'], $status_dtl_ibe) and $_POST['ibeto'] == 0)
		or (in_array($_POST['wdmfrom'], $status_dtl_wdm) and $_POST['wdmto'] == 0)
	){
		include('save-log-detail.php');
	}
	/* UPLOAD FILE AGREEMENT ----------------------------------------------*/
	$status_dtl_sm	= array(14, 6);
	if((in_array($_POST['statusfrom'], $status_dtl_sm) and $_POST['statusto'] == 0) or $_POST['statusto'] == 14){
		include('save-file-agreement.php');
	}
	/* UPLOAD FILE UAT WDM ----------------------------------------------*/
	$status_dtl_wdm	= array(6);
	if(in_array($_POST['wdmto'], $status_dtl_sm) ){
		include('save-file-uat-wdm.php');
	}
	/* UPLOAD FILE UAT WDM ----------------------------------------------*/
	$status_dtl_ibe	= array(5);
	if(in_array($_POST['ibeto'], $status_dtl_sm) ){
		include('save-file-uat-ibe.php');
	}

	include('sm-affiliate/mailer/sender-internal.php');

	if(in_array($_POST['statusto'], array('1', '2'))){
		header('location: '.$base_url.'/'.$uri2.'/hotel-assessment');
	}else if(in_array($_POST['wdmto'], array('0', '1', '2', '3', '4', '5', '7', '8'))){
		header('location: '.$base_url.'/'.$uri2.'/wdm-stage');
	}else if(in_array($_POST['ibeto'], array('0', '1', '2', '3', '4'))){
		header('location: '.$base_url.'/'.$uri2.'/ibe-stage');
	}else if(in_array($_POST['statusto'], array('4', '10', '11', '12', '13'))){
		header('location: '.$base_url.'/'.$uri2.'/other-stage');
	}else{
		header('location: '.$base_url.'/'.$uri2.'/stage-process');
	}
}catch (PDOException $e) {
	print_r($e->getMessage());
}
?>
