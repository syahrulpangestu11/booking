<?php
if(!empty($_POST['affoid'])){
	include ('../class/affiliate.php');
	$affiliate = new Affiliate($db);
	$affiliate->setAffiliate($_POST['affoid']);
	$profile = $affiliate->profileAffiliate($affiliate->affiliateoid);
}
	
	if(empty($_POST['widgetconnectivity'])){ $_POST['widgetconnectivity'] = "not yet"; }
	if(empty($_POST['pms'])){ $_POST['pms'] = "not yet"; }
	if(empty($_POST['paymentgateway'])){ $_POST['paymentgateway'] = "not yet"; }
	if(empty($_POST['websitelive'])){ $_POST['websitelive'] = "not yet"; }

	$stmt = $db->prepare("INSERT INTO affiliatehotel_productionlog (affiliatehoteloid, affiliatelogoid, widgetconnectivity, widgetconnectivity_note, pms, pms_note, paymentgateway, paymentgateway_note, websitelive, websitelive_note, created, createdby, updated, updatedby) VALUES (:a, :b, :c, :d, :e, :f, :g, :h, :i, :j, :s, :t, :u, :v)");
	$stmt->execute(array(':a' => $_POST['affhotel'], ':b' => $affiliatelogid, ':c' => $_POST['widgetconnectivity'], ':d' => $_POST['note_widgetconnectivity'], ':e' => $_POST['pms'], ':f' => $_POST['note_pms'], ':g' => $_POST['paymentgateway'], ':h' => $_POST['note_paymentgateway'], ':i' => $_POST['websitelive'], ':j' => $_POST['note_websitelive'], ':s' => date('Y-m-d H:i:s'), ':t' => $_POST['usname'], ':u' => date('Y-m-d H:i:s'), ':v' => $_POST['usname']));
	$productionlogid = $db->lastInsertId();
	
	$stmt = $db->prepare("select count(affiliatehotel_productionoid) existproduction from affiliatehotel_production where affiliatehoteloid = :a");
	$stmt->execute(array(':a' => $_POST['affhotel']));
	$count = $stmt->fetch(PDO::FETCH_ASSOC);
	
	$stmt = $db->prepare("select * from affiliatehotel_productionlog where affiliatehotel_productionlogoid = :id");
	$stmt->execute(array(':id' => $productionlogid));
	$row = $stmt->fetch(PDO::FETCH_ASSOC);
	
	if($count['existproduction'] == 0){
		$stmt_insert_agreement = $db->prepare("INSERT INTO affiliatehotel_production (affiliatehoteloid, widgetconnectivity, widgetconnectivity_note, pms, pms_note, paymentgateway, paymentgateway_note, websitelive, websitelive_note, created, createdby, updated, updatedby) VALUES (:a, :b, :c, :d, :e, :f, :g, :h, :i, :j, :k, :l, :m)");
		$stmt_insert_agreement->execute(array(':a' => $row['affiliatehoteloid'], ':b' => $row['widgetconnectivity'], ':c' => $row['widgetconnectivity_note'], ':d' => $row['pms'], ':e' => $row['pms_note'], ':f' => $row['paymentgateway'], ':g' => $row['paymentgateway_note'], ':h' => $row['websitelive'], ':i' => $row['websitelive_note'], ':j' => $row['created'], ':k' => $row['createdby'], ':l' => $row['updated'], ':m' => $row['updatedby']));
	}else{
		$stmt_update_agreement = $db->prepare("UPDATE `affiliatehotel_production` SET `widgetconnectivity` = :a , `widgetconnectivity_note` = :b , `pms` = :c , `pms_note` = :d , `paymentgateway` = :e , `paymentgateway_note` = :f , `websitelive` = :g , `websitelive` = :h , `websitelive_note` = :i , `updated` = :j , `updatedby` = :k WHERE `affiliatehoteloid` = :id");
		$stmt_update_agreement->execute(array(':id' => $row['affiliatehoteloid'], ':a' => $row['widgetconnectivity'], ':b' => $row['widgetconnectivity_note'], ':c' => $row['pms'], ':d' => $row['pms_note'], ':e' => $row['paymentgateway'], ':f' => $row['paymentgateway_note'], ':g' => $row['websitelive'], ':h' => $row['websitelive_note'], ':i' => $row['updated'], ':j' => $row['updatedby']));
	}

?>