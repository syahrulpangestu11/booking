<?php
	session_start();
	error_reporting(E_ALL ^ E_NOTICE);
	include ('../../conf/connection.php');
		
	$stmt	= $db->prepare("select h.hotelname, h.hoteloid, h.createdby, h.affiliatehotelstatusoid, as.status, h.affibestatusoid, aw.ibestatus, h.affibestatusoid, ai.ibestatus from hotel as `h` inner join affiliatehotelstatus as `as` using (affiliatehotelstatusoid) left join affibestatus as `aw` using (affibestatusoid) left join affibestatus as `ai` using (affibestatusoid) where h.hoteloid = :a");
	$stmt->execute(array(':a' => $_GET['id']));
	$affibe	= $stmt->fetch(PDO::FETCH_ASSOC);
?>

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">[<?=$affibe['ibestatus'];?>] Change IBE Status</h4>
        <h5 id="hotelname"><?=$affibe['hotelname'];?></h5>
        <i class="fa fa-user"></i> <?=$affibe['createdby'];?> <span id="hotel-creator"></span><input type="hidden" name="hoteloid" value="<?=$affibe['hoteloid'];?>" />
    </div>
    <div class="modal-body">
    <?php
        switch($affibe['affibestatusoid']){
            case 1	: include('../page/ibe-modal/ibe-account-creation.php'); break;
			case 2	: include('../page/ibe-modal/ibe-content-agregate.php'); break;
			case 3	: include('../page/ibe-modal/ibe-training.php'); break;
			case 4	: include('../page/ibe-modal/ibe-uat-checklist.php'); break;
			case 5	: include('../page/ibe-modal/ibe-ready-commerce.php'); break;
        }
    ?>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary change-status">Save changes</button>
    </div>
