<?php
error_reporting(E_ALL ^ E_NOTICE);
include ('../../conf/connection.php');

$stmt = $db->prepare("select h.hotelname, h.hoteloid, h.createdby, ahs.status, ahs.affiliatehotelstatusoid, ahg.benefit_website, ahg.commissiontype from hotel h inner join affiliatehotelstatus ahs using (affiliatehotelstatusoid) left join affiliatehotel_agreement ahg using (hoteloid) where h.hoteloid = :a");
$stmt->execute(array(':a' => $_GET['id']));
$row = $stmt->fetch(PDO::FETCH_ASSOC);

		echo"
		<xml>
			<affiliatename>".$row['createdby']."</affiliatename>
			<hotel>".$row['hotelname']."</hotel>
			<hotelcode>".$row['hoteloid']."</hotelcode>
			<status>".$row['status']."</status>
			<statuscode>".$row['affiliatehotelstatusoid']."</statuscode>
			<flatfeecommission>".$row['flatfeecommission']."</flatfeecommission>
			<commission>".$row['commission']."</commission>
			<basestandard>".$row['base_standard_commission']."</basestandard>
			<markupstandard>".$row['markup_standard_commission']."</markupstandard>
			<basefreewebsite>".$row['base_freewebsite_commission']."</basefreewebsite>
			<markupfreewebsite>".$row['markup_freewebsite_commission']."</markupfreewebsite>
			<benefitwebsite>".$row['benefit_website']."</benefitwebsite>
			<commissiontype>".$row['commissiontype']."</commissiontype>
		</xml>
		";

?>