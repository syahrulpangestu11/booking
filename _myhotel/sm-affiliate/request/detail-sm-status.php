<?php
	session_start();
	error_reporting(E_ALL ^ E_NOTICE);
	include ('../../conf/connection.php');
	
	$stmt		= $db->prepare("select affiliatehotelstatusoid, status from affiliatehotelstatus as `as` where as.affiliatehotelstatusoid = :a");
	$stmt->execute(array(':a' => $_GET['status']));
	$statussm	= $stmt->fetch(PDO::FETCH_ASSOC);
	
	$stmt	= $db->prepare("select h.hotelname, h.hoteloid, h.createdby, h.affiliatehotelstatusoid, as.status, h.affibestatusoid, aw.ibestatus, h.affibestatusoid, ai.ibestatus from hotel as `h` inner join affiliatehotelstatus as `as` using (affiliatehotelstatusoid) left join affibestatus as `aw` using (affibestatusoid) left join affibestatus as `ai` using (affibestatusoid) where h.hoteloid = :a");
	$stmt->execute(array(':a' => $_GET['id']));
	$affsm	= $stmt->fetch(PDO::FETCH_ASSOC);
?>

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">[<?=$statussm['status'];?>] Change Sales and Marketing Status</h4>
        <h5 id="hotelname"><?=$affsm['hotelname'];?></h5>
        <i class="fa fa-user"></i> <?=$affsm['createdby'];?> <span id="hotel-creator"></span><input type="hidden" name="hoteloid" value="<?=$affsm['hoteloid'];?>" />
    </div>
    <div class="modal-body">
    <?php
		switch($statussm['affiliatehotelstatusoid']){
			case 1	: include('../page/status-modal/prospect-hotel.php'); break;
			case 2	: include('../page/status-modal/waiting-bussiness-approval.php'); break;
			case 3	: include('../page/status-modal/progress.php'); break;
			case 4	: include('../page/status-modal/rejection.php'); break;
			case 5	: include('../page/status-modal/subscribe-form.php'); break;
			case 6	: include('../page/status-modal/implementation.php'); break;
			case 7	: include('../page/status-modal/agreement-validation.php'); break;
			case 8	: include('../page/status-modal/commerce.php'); break;
			case 9	: include('../page/status-modal/to-renewal.php'); break;
			case 13	: include('../page/status-modal/failed.php'); break;
		}
	?>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary change-status">Save changes</button>
    </div>