<?php
if(!empty($_POST['to-email']) or !empty($_POST['cc-email'])){
	require_once('includes/mailer/class.phpmailer.php');
	$mail = new PHPMailer(true);
	try{
		include('authentication.php');
		
		$mail->SetFrom('info@thebuking.com', 'Taskbook System');
		
		if(count($_POST['to-email']) > 0){
			foreach($_POST['to-email'] as $key => $value){
				$mail->AddAddress($value, $value);
			}
		}		

		if(count($_POST['cc-email']) > 0){
			foreach($_POST['cc-email'] as $key => $value){
				$mail->AddCC($value, $value);
			}
		}
		
		
		$mail->Subject = $_POST['subject-email'];
		$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
		$mail->MsgHTML($_POST['email-internal']);
		
		$mail->Send();
		
	}catch (phpmailerException $e) {
		echo $e->getMessage();
	}catch (Exception $e) {
		echo $e->getMessage();
	}
}
?>