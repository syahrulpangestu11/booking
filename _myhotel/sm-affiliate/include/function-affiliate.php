<?php
function knowIdleTime($starttime, $endtime){
	$d1 = new DateTime($starttime);
	$d2 = new DateTime($enddtime);
	$diff = $d2->diff($d1);
	//return $diff->format("%a d %h h %i m");
	return $diff->format("%a d");
}

function showDataDashboard($status, $liststatus, $target, $page, $keyword){
	global $db;
	global $base_url;

	$batas=5;
	$halaman=isset($page) ? $page : 0 ;
	if(empty($halaman)){
		$posisi=0; $halaman=1;
	}else{
		$posisi = ($halaman-1) * $batas;
	}
	$no=$posisi+1;
	$j = $halaman + ($halaman - 1) * ($batas - 1);

	$paging_query = " limit ".$posisi.", ".$batas;
	$num = $posisi;

	if(!empty($keyword)){
		$filter_hotel = " and h.hotelname like '%".$keyword."%' ";
	}else{
		$filter_hotel = "";
	}

	if(is_array($status)){ $status = implode(",", $status); }
?>
    <table class="table table-striped responsive-table">
        <thead>
        <?php
		if($status == 1){
		    $column = array('#', 'Hotel', 'Action', 'PIC');
		}else if($status == 2){
		    $column = array('#', 'Hotel', 'Idle Time', 'Notes', 'Action', 'PIC');
		}else if($status == 6){
				$column = array('#', 'Date', 'Hotel', 'Idle Time', 'Notes', 'PIC');
		}else if($status == 8){
		    $column = array('#', 'Hotel', 'Rev', 'Comm', 'PIC');
		}else{
		    $column = array('#', 'Date', 'Hotel', 'Idle Time', 'Notes', 'Action', 'PIC');
		}
		foreach($column as $th){
		    echo "<th>".$th."</th>";
		}
		?>
        <tbody>
<?php
	if(is_array($liststatus)){ $liststatus = implode(',', $liststatus); }
    $no = 0;
		if($liststatus == 8){
	    $main_query = "select h.hoteloid, h.hotelname, h.created, h.createdby, h.affiliatehotelstatusoid, ahs.status, hc.typecomm, hc.priority, (case hc.typecomm when 2 then hc.value when 4 then (hc.value/12) else (select sum(gbhcollect) from (select distinct(b.bookingoid), b.hoteloid, b.gbhcollect, min(br.checkin) as mincheckin from booking b inner join bookingroom br using (bookingoid) where pmsstatus = '0' group by b.bookingoid) x where x.hoteloid = h.hoteloid and mincheckin between '".date('Y-m-01')."' and '".date('Y-m-t')."') end) commission,
			(select sum(grandtotal) from (select distinct(b.bookingoid), b.hoteloid, b.grandtotal, min(br.checkin) as mincheckin from booking b inner join bookingroom br using (bookingoid) where pmsstatus = '0' group by b.bookingoid) x1 where x1.hoteloid = h.hoteloid and mincheckin between '".date('Y-m-01')."' and '".date('Y-m-t')."') as revenue
			from hotel h inner join affiliatehotelstatus ahs using (affiliatehotelstatusoid) left join (select distinct(hoteloid), priority, typecomm, value from hotelcommission where (startdate <= '".date('Y-m-01')."' and enddate >= '".date('Y-m-01')."') order by priority desc) hc on hc.hoteloid = h.hoteloid where affiliatehotelstatusoid in (".$liststatus.") ".$filter_hotel." and h.hoteloid not in ('1') group by h.hoteloid order by revenue desc ";
		}else{
			if($liststatus == 1){
				$main_query = "select h.hoteloid, h.hotelname, h.created, h.createdby, h.affiliatehotelstatusoid, ahs.status from hotel h inner join affiliatehotelstatus ahs using (affiliatehotelstatusoid) where affiliatehotelstatusoid in (".$liststatus.") ".$filter_hotel." group by h.hoteloid order by created desc";
			}else{
		    $main_query = "select h.hoteloid, h.hotelname, h.created, h.createdby, h.affiliatehotelstatusoid, ahs.status, al.startdate from hotel h inner join affiliatehotelstatus ahs using (affiliatehotelstatusoid) left join affiliatelog al on al.hoteloid = h.hoteloid where affiliatehotelstatusoid in (".$liststatus.") and al.to_status in (".$liststatus.")  ".$filter_hotel." and al.to_type = 'sm' group by h.hoteloid order by al.startdate";
			}
		}
		//echo $main_query;
		$stmt = $db->query($main_query.$paging_query);
		$row_count = $stmt->rowCount();

    foreach($stmt->fetchAll(PDO::FETCH_ASSOC) as $row){

		if($row['affiliatehotelstatusoid'] == "1"){
			$createdby = (empty($row['createdby']))? "ind" : $row['createdby'];
		}else{
			$stmt_pic = $db->prepare("select u.username from affiliatelog al inner join hotel h using (hoteloid) inner join users u using (useroid) where to_type = :a and to_status = :b and al.hoteloid = :c order by al.startdate desc limit 1");
			$stmt_pic->execute(array(':a' => 'sm', ':b' => '2', ':c' => $row['hoteloid']));
			$result_pic = $stmt_pic->fetch(PDO::FETCH_ASSOC);
			$createdby = (empty($result_pic['username']))? "ind" : $result_pic['username'];
		}


		$stmt2 = $db->prepare("select al.affiliatelogoid, al.startdate as logtime, al.note from affiliatelog al inner join hotel h using (hoteloid) where to_type = :a and to_status = :b and al.hoteloid = :c order by al.startdate desc limit 1");
		$stmt2->execute(array(':a' => 'sm', ':b' => $status, ':c' => $row['hoteloid']));
		$log = $stmt2->fetch(PDO::FETCH_ASSOC);
		$idletime = knowIdleTime($log['logtime'], date('Y-m-d H:i:s'));

		$note = $log['note'];

		if($status == 3){
			$stmt3 = $db->prepare("select * from afflogdetail where affiliatelogoid = :a order by detail_date desc");
	    $stmt3->execute(array(':a' => $log['affiliatelogoid']));
	   	foreach($stmt3->fetchAll(PDO::FETCH_ASSOC) as $logdtl){
				$note.= '<br>'.date('dmY', strtotime($logdtl['detail_date']))." - ".$logdtl['detail_note'];
			}
		}
?>
    <tr class="direct-detail-hotel" data-id="<?php echo $row['hoteloid']; ?>">
    <?php
		if($status == 1){
		    $column = array(++$num, date('j M Y', strtotime($row['created'])), $row['hotelname'], $createdby);
		}else if($status == 2){
		    $column = array(++$num, $row['hotelname'], $idletim, $note, $createdby);
		}else if($status == 8){
		    $column = array(++$num, $row['hotelname'], number_format($row['revenue']), number_format($row['commission']), $createdby);
		}else{
		    $column = array(++$num, date('j M Y', strtotime($log['logtime'])), $row['hotelname'], $idletime, $note, $createdby);
		}
		foreach($column as $key => $td){
				if($key == 4 and $status == 6){
					echo "<td>";
					include('detail-note-implementation.php');
					echo "</td>";
				}else{
					echo "<td>".$td."</td>";
				}
		}
		if($status != 6 and $status != 8){
		?>
        <td><button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="<?php echo $target; ?>" data-id="<?php echo $row['hoteloid']; ?>" data-status="<?php echo $status; ?>"><i class="fa fa-share"></i></button></td>
        <?php
		}
		?>
    </tr>
<?php
	}
?>
        </tbody>
    </table>
		<?php if($status == '8'){ echo "<div class='row'><div class='col-md-12 text-right'><small>* Based on distributed reservation</small></div></div>"; } ?>
		<div class="row" style="margin-top:10px">
			<div class="col-md-12 text-right">
				<a href="<?php echo $base_url; ?>/sales-marketing/list-hotel/?smstatus=<?php echo $status; ?>" target="_blank">show all data</a>
			</div>
		</div>
		<?php
			$status_query = "select count(h.hoteloid) as countdata from hotel h inner join affiliatehotelstatus ahs using (affiliatehotelstatusoid) where affiliatehotelstatusoid in (".$liststatus.") ".$filter_hotel;

			//echo $status_query;

			$stmt = $db->query($status_query);
			$count = $stmt->fetch(PDO::FETCH_ASSOC);

			$jmldata = $count['countdata'];
			$tampildata = $row_count;

			//echo $jmldata." | ".$tampildata;

			if($page > 0){
				include("../../includes/paging/post-paging.php");
			}else{
				include("includes/paging/post-paging.php");
			}
	 	?>
<?php
}
?>

<?php
function showDataWDM($status, $target){
	global $db;
	global $base_url;

	if(is_array($status)){ $status = implode(",", $status); }
?>
    <table class="table table-striped responsive-table">
        <thead>
        <?php
		if($status == 7){
		    $column = array('#', 'Date', 'Hotel');
		}else{
		    $column = array('#', 'Date', 'Hotel', 'Idle Time', 'Action');
		}

		foreach($column as $th){
		    echo "<th>".$th."</th>";
		}
		?>
        <tbody>
<?php
    $no = 0;
    $stmt = $db->query("select h.hoteloid, h.hotelname, h.created, h.affwdmstatusoid, aw.wdmstatus from hotel h inner join affwdmstatus aw using (affwdmstatusoid) where affwdmstatusoid in (".$status.") limit 5");
    foreach($stmt->fetchAll(PDO::FETCH_ASSOC) as $row){

		$stmt2 = $db->prepare("select al.startdate as logtime from affiliatelog al inner join hotel h using (hoteloid) where to_type = :a and to_status = :b and al.hoteloid = :c order by al.startdate desc limit 1");
		$stmt2->execute(array(':a' => 'wdm', ':b' => $row['affwdmstatusoid'], ':c' => $row['hoteloid']));
		$log = $stmt2->fetch(PDO::FETCH_ASSOC);
		$idletime = knowIdleTime($log['logtime'], date('Y-m-d H:i:s'));
?>
    <tr class="direct-detail-hotel" data-id="<?php echo $row['hoteloid']; ?>">
    <?php
		if($status == 7){
			$column = array(++$no, date('j M Y', strtotime($log['logtime'])), $row['hotelname']);
		}else{
			$column = array(++$no, date('j M Y', strtotime($log['logtime'])), $row['hotelname'], $idletime);
		}

		foreach($column as $td){
		    echo "<td>".$td."</td>";
		}
		if($status != 7){
		?>
        <td><button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="<?php echo $target; ?>" data-id="<?php echo $row['hoteloid']; ?>"><i class="fa fa-share"></i></button></td>
        <?php
		}
		?>
    </tr>
<?php
	}
?>
        </tbody>
    </table>
		<div class="row">
			<div class="col-md-6">
				<?php
					$status_query = "select count(h.hoteloid) as countdata from hotel h inner join affwdmstatus aw using (affwdmstatusoid) where affwdmstatusoid in (".$status.")";
					$stmt = $db->query($status_query);
					$count = $stmt->fetch(PDO::FETCH_ASSOC);
					echo "showing ".$no." of ".$count['countdata']." data";
				?>
			</div>
			<div class="col-md-6 text-right">
				<a href="<?php echo $base_url; ?>/sales-marketing/list-hotel/?wdmstatus=<?php echo $status; ?>" target="_blank">show all data</a>
			</div>
		</div>
<?php
}
?>

<?php
function showDataIBE($status, $target){
	global $db;
	global $base_url;

	if(is_array($status)){ $status = implode(",", $status); }
?>
    <table class="table table-striped responsive-table">
        <thead>
        <?php
		$column = array('#', 'Date', 'Hotel', 'Idle Time', 'Action');
		foreach($column as $th){
		    echo "<th>".$th."</th>";
		}
		?>
        <tbody>
<?php
    $no = 0;
		if($status != '6'){
			$status_query = "select h.hoteloid, h.hotelname, h.created, h.affibestatusoid, ai.ibestatus from hotel h inner join affibestatus ai using (affibestatusoid) where affibestatusoid in (".$status.") and affiliatehotelstatusoid not in ('8', '9', '10', '11', '12', '13', '15') limit 5";
		}else{
			$status_query = "select h.hoteloid, h.hotelname, h.created, h.affibestatusoid, ai.ibestatus from hotel h inner join affibestatus ai using (affibestatusoid) where affibestatusoid in (".$status.") limit 5";
		}
    $stmt = $db->query($status_query);
    foreach($stmt->fetchAll(PDO::FETCH_ASSOC) as $row){

		$stmt2 = $db->prepare("select al.startdate as logtime from affiliatelog al inner join hotel h using (hoteloid) where to_type = :a and to_status = :b and al.hoteloid = :c order by al.startdate desc limit 1");
		$stmt2->execute(array(':a' => 'ibe', ':b' => $row['affibestatusoid'], ':c' => $row['hoteloid']));
		$log = $stmt2->fetch(PDO::FETCH_ASSOC);
		$idletime = knowIdleTime($log['logtime'], date('Y-m-d H:i:s'));
?>
    <tr class="direct-detail-hotel" data-id="<?php echo $row['hoteloid']; ?>">
    <?php
		$column = array(++$no, date('j M Y', strtotime($log['logtime'])), $row['hotelname'], $idletime);
		foreach($column as $td){
		    echo "<td>".$td."</td>";
		}
		if($status != 8){
		?>
        <td><button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="<?php echo $target; ?>" data-id="<?php echo $row['hoteloid']; ?>"><i class="fa fa-share"></i></button></td>
        <?php
		}
		?>
    </tr>
<?php
	}
?>
        </tbody>
    </table>
		<div class="row">
			<div class="col-md-6">
				<?php
					$status_query = "select count(h.hoteloid) as countdata from hotel h inner join affibestatus ai using (affibestatusoid) where affibestatusoid in (".$status.") and affiliatehotelstatusoid not in ('8', '9', '10', '11', '12', '13', '15')";
					$stmt = $db->query($status_query);
					$count = $stmt->fetch(PDO::FETCH_ASSOC);
					echo "showing ".$no." of ".$count['countdata']." data";
				?>
			</div>
			<div class="col-md-6 text-right">
				<a href="<?php echo $base_url; ?>/sales-marketing/list-hotel/?ibestatus=<?php echo $status; ?>">show all data</a>
			</div>
		</div>
<?php
}
?>


<?php
function countPerStatus($liststatus){
	global $db;
	if(is_array($liststatus)){ $liststatus = implode(',', $liststatus); }

    $stmt = $db->query("select count(hoteloid) as countstatus from hotel h inner join affiliatehotelstatus ahs using (affiliatehotelstatusoid) where affiliatehotelstatusoid in (".$liststatus.")");
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
	echo $row['countstatus'];
}
?>
