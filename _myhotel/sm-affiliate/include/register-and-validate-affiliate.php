<?php
$affiliate = new Affiliate($db);

$email = $_POST['email'];
$password = $_POST['password'];

if($uri2 == 'register'){
	try {
		$foundemail = $affiliate->checkExistedEmail($email);
		if($foundemail == 0){
			$firstname = $_POST['firstname'];
			$lastname = $_POST['lastname'];
			$phone = $_POST['phone'];
			
			$affiliate->registerAffiliate($firstname, $lastname, $email, $phone, $password);
			$affiliate->setSessionAffiliate($affiliate->affiliateoid);
			header('location: '.$baseurl.'/dashboard');
		}else{
			header('location: '.$baseurl.'/sign-in');
		}
	}catch (Exception $e) {
		echo "something went wrong when register your account :(<br>"; 
		//echo $e->getMessage();
	}
}else if($uri2 == 'validate'){
	try {
		$founduser = $affiliate->validateAffiliate($email, $password);
		if($founduser > 0){
			$affiliate->setSessionAffiliate($affiliate->affiliateoid);
			header('location: '.$baseurl.'/dashboard');
		}else{
			header('location: '.$baseurl.'/sign-in');
		}
	}catch (Exception $e) {
		echo "something went wrong when sign in your account :(<br>"; 
		//print_r($_POST);
		//echo $e->getMessage();
	}
}
?>