<!-- Pagination -->
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css">
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>

<section class="content">
    <div id="dashboard">

    <div class="row">
        <div class="box box-form">
            <div class="box-body">
  						<form method="get" enctype="multipart/form-data" class="form-inline" action="<?php echo $base_url; ?>/<?=$uri2?>/list-hotel/">
								<div class="form-group">
                  <label for="text1" class="control-label">Name</label>
                  <input type="text" name="name" class="form-control" value="<?php echo $name; ?>">
                </div>
								<div class="form-group">
                  <label for="text1" class="control-label">SM Status</label>
                  <select name="smstatus" class="form-control">
										<option value="">Show all SM status</option>
										<?php
										$stmt = $db->query("SELECT * FROM `affiliatehotelstatus` as `as` where `as`.publishedoid = '1'");
										$list_smstatus = $stmt->fetchAll(PDO::FETCH_ASSOC);
										foreach($list_smstatus as $smstatus){
											if($smstatus['affiliatehotelstatusoid'] == $_REQUEST['smstatus']){ $selected = "selected"; }else{ $selected = ""; }
										?>
										<option value="<?=$smstatus['affiliatehotelstatusoid']?>" <?=$selected?>><?=$smstatus['status']?></option>
										<?php
										}
										?>
									</select>
                </div>
            </form>
          </div><!-- /.box-body -->
       </div>

        <div class="box">

    <table id="bispro-report" cellspacing="0" class="table table-striped">
        <thead>
          <tr>
              <td>Hotel</td>
              <td>PIC</td>
              <?php
              $stmt = $db->query("SELECT * FROM `affiliatehotelstatus` as `as` where `as`.`publishedoid` = '1'");
              $list_affhotelstatus = $stmt->fetchAll(PDO::FETCH_ASSOC);
              foreach($list_affhotelstatus as $affhotelstatus){
              ?>
                  <td><?=$affhotelstatus['status'];?></td>
              <?php
              }
              ?>
          </tr>
        </thead>
        <tbody>
		<?php
            $stmt = $db->query("SELECT h.hoteloid, hotelname, h.created affiliatehotelstatusoid, affibestatusoid, affwdmstatusoid, ah.status, x.startdate  FROM `hotel` as `h` inner join affiliatehotelstatus ah using (affiliatehotelstatusoid) left join (select hoteloid, startdate from affiliatelog where to_type = 'sm' and to_status = '8') as x on x.hoteloid=h.hoteloid where h.affiliatehotelstatusoid not in (0,1) order by startdate desc, CASE affiliatehotelstatusoid WHEN '8' THEN 1 WHEN '6' THEN 2 WHEN '5' THEN 3 WHEN '3' THEN 4 ELSE 5 END, affiliatehotelstatusoid");

            $list_hotel = $stmt->fetchAll(PDO::FETCH_ASSOC);
            foreach($list_hotel as $hotel){

              if($row['affiliatehotelstatusoid'] == "1"){
          			$createdby = (empty($hotel['createdby']))? "ind" : $hotel['createdby'];
          		}else{
          			$stmt_pic = $db->prepare("select u.username from affiliatelog al inner join hotel h using (hoteloid) inner join users u using (useroid) where to_type = :a and to_status = :b and al.hoteloid = :c order by al.startdate desc limit 1");
          			$stmt_pic->execute(array(':a' => 'sm', ':b' => '2', ':c' => $hotel['hoteloid']));
          			$result_pic = $stmt_pic->fetch(PDO::FETCH_ASSOC);
          			$createdby = (empty($result_pic['username']))? "ind" : $result_pic['username'];
          		}
		?>
        	<tr>
            <td><a href="<?=$base_url?>/sales-marketing/detail-sm-hotel/<?=$hotel['hoteloid']?>" target="_blank" style="font-weight:bold; text-decoration:underline;"><?=$hotel['hotelname'];?></a></td>
            <td><?=$createdby?></td>
      			<?php
      			foreach($list_affhotelstatus as $affhotelstatus){
      				$stmt = $db->prepare("select al.* from affiliatelog as al where to_type = :a and to_status = :b and hoteloid = :c");
      				$stmt->execute(array(':a' => 'sm', ':b' => $affhotelstatus['affiliatehotelstatusoid'], ':c' => $hotel['hoteloid']));
      				$found_log = $stmt->rowCount();
      				if($found_log > 0){
      				$log = $stmt->fetch(PDO::FETCH_ASSOC);
                  ?>
                      <td>
      				        <?=date('d M Y H:i', strtotime($log['startdate']));?><br />
                      <?=$log['note'];?>
                      <?php
                        if($log['to_status'] == 5){
                            $stmt = $db->prepare("select affhotel_sfoid, sf_file from affhotel_sf where hoteloid = :a");
                            $stmt->execute(array(':a' => $hotel['hoteloid']));
                            $result = $stmt->fetch(PDO::FETCH_ASSOC);
                            if(!empty($result['sf_file'])){ echo "<br><a href='".$result['sf_file']."' target='_blank'><i class='fa fa-paperclip'></i> SF</a>"; }
                        }else if($log['to_status'] == 7){
                            $stmt = $db->prepare("select affagreementoid, file_agreement from affagreement where hoteloid = :a");
                            $stmt->execute(array(':a' => $hotel['hoteloid']));
                            $result = $stmt->fetch(PDO::FETCH_ASSOC);
                            if(!empty($result['file_agreement'])){ echo "<br><a href='".$result['file_agreement']."' target='_blank'><i class='fa fa-paperclip'></i> PKS</a>"; }
                        }
                      ?>
                      </td>
                  <?php
      				}
      				else{
      					echo '<td></td>';
      				}
            }
            ?>
          </tr>
      <?php
      }
      ?>
      </tbody>
    </table>

        </div>
    </div>
</section>

<script type="text/javascript">
$( function() {

	$('#bispro-report').DataTable({
    "ordering": false,
		"scrollX": true
		//"lengthMenu": [[25, 50, 75, -1], [25, 50, 75, "All"]]
	});

});
</script>
