<div id="div-1" class="body collapse in" aria-expanded="true">
    <div class="form-group">
        <label class="control-label col-md-3">Date</label>
        <div class="col-md-6">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                <input type="text" name="detail-date" id="detail-date" class="form-control" value="<?php echo date("d F Y"); ?>" required="required">
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-3">Type</label>
        <div class="col-md-6">
            <select name="detail-type" class="form-control chzn-select" tabindex="7">
                <?php
                    $type_progress = array('Meeting', 'Email', 'Call', 'Comment');
                    foreach($type_progress as $type){
                ?>
                <option value="<?=$type;?>"><?=$type;?></option>
                <?php
                    }
                ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-3">Note</label>
        <div class="col-md-8">
            <textarea id="note_wdm" name="detail-note" class="form-control"></textarea>
        </div>
    </div>
</div>
