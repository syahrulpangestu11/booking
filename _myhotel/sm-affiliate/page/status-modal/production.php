<div class="modal fade statusmodal" id="productionModal" tabindex="-1" role="dialog" aria-labelledby="productionModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">[Production] Change Affiliate Hotel Status</h4>
            </div>
            <form class="form-horizontal">
            <div class="modal-body">
				<div class="row">
                    <div class="col-lg-4 col-xs-12">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="text1" class="control-label col-lg-12 text-left">Hotel Name</label>
                                    <div class="col-lg-12">
										<input name="hotelname" class="form-control" readonly><input type="hidden" name="affhotel" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-12 text-left">Affliate Name</label>
                                    <div class="col-lg-12">
                                    	<input type="text" name="affiliatename" class="form-control" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-12 text-left">Current Status</label>
                                    <div class="col-lg-12">
                                    	<input type="text" name="currentstatus" class="form-control" readonly><input type="hidden" name="statusfrom"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-12 text-left">Change Status to</label>
                                    <div class="col-lg-12">
                                        <select name="statusto" class="form-control chzn-select" tabindex="9">
                                            <option value="9">To Renewal</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
						</div>
                    </div>
                    <div class="col-lg-8 col-xs-12">
                        <div class="row">
                            <?php include('form-status/form-to-renewal.php'); ?>
						</div>
					</div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary change-status">Save changes</button>
            </div>
			</form>
        </div>
    </div>
</div>