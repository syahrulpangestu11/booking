<?php
$hoteloid = $uri4;
$stmt = $db->prepare("select h.*, c.name as chainname from hotel h left join chain c using (chainoid) where h.hoteloid = :a");
$stmt->execute(array(':a' => $hoteloid)); 
$hotel = $stmt->fetch(PDO::FETCH_ASSOC);

?>
<div class="row detail-hotel">
	<div class="col-lg-7">
        <div class="row">
            <div class="col-lg-12 col-xs-12">    
                <div class="box dark">
                    <h3><?php echo $hotel['hotelname']; ?></h3>
                    Chain : <?php echo $hotel['chainname']; ?><br />
                    Type : Hotel<br /> 
                    Star Rating : <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><br />
                    <i class="fa fa-globe"></i> <a href="<?php echo $hotel['website']; ?>" target="_blank"><?php echo $hotel['website']; ?></a><br />
                    <i class="fa fa-map-marker"></i> <?php echo $hotel['address']; ?><br />
                    <i class="fa fa-phone"></i> <?php echo $hotel['phone']; ?> | <i class="fa fa-fax"></i> <?php echo $hotel['fax']; ?><br />
                </div>
  			</div>
        </div>              
        <div class="row">
            <div class="col-lg-12 col-xs-12">
                <div class="box dark">
                    <header>
                        <div class="icons"><i class="fa fa-address-book-o"></i></div>
                        <h5>Hotel Contact Management</h5>
                        <div class="toolbar">
                          <nav style="padding: 8px;">
                              <a href="javascript:;" class="btn btn-default btn-xs collapse-box"><i class="fa fa-minus"></i></a>
                              <a href="javascript:;" class="btn btn-default btn-xs full-box"><i class="fa fa-expand"></i></a>
                          </nav>
                        </div>
                    </header>
                    <div id="div-1" class="body collapse in" aria-expanded="true">
                    <table class="table responsive-table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                            </tr>
                        </thead>
                        <tbody>
							<?php
							$no = 0;
                            $stmt = $db->query("select * from hotelcontact where hoteloid = '".$hoteloid."' AND publishedoid = '1' order by email");
                            $r_mail = $stmt->fetchAll(PDO::FETCH_ASSOC);
                            foreach($r_mail as $row){
                            ?>
                            <tr>
                            	<td><?=++$no?></td>
                                <td><?=$row['name']?></td>
                                <td><?=$row['email']?></td>
                                <td><?=$row['phone']?></td>
                            </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xs-12">
                <div class="box dark">
                    <header>
                        <div class="icons"><i class="fa fa-folder-open"></i></div>
                        <h5>Current Booking Enginee Information</h5>
                        <div class="toolbar">
                          <nav style="padding: 8px;">
                              <a href="javascript:;" class="btn btn-default btn-xs collapse-box"><i class="fa fa-minus"></i></a>
                              <a href="javascript:;" class="btn btn-default btn-xs full-box"><i class="fa fa-expand"></i></a>
                          </nav>
                        </div>
                    </header>
                    <div id="div-1" class="body collapse in" aria-expanded="true">
                    	<label>Current Booking Enginee :</label> <?php echo $hotel['current_be_name']; ?><br />
                       	<label>End of contract :</label> <?php echo $hotel['end_of_contract_month']; ?> / <?php echo $hotel['end_of_contract_year']; ?><br />
                        <label>Penalty if broken the current contract :</label> <i><?php echo $hotel['penalty_if_broken_contract']; ?></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-5">
        <div class="row">
            <div class="col-lg-12 col-xs-12">
                <div class="box dark">
                    <header>
                        <div class="icons"><i class="fa fa-user-circle-o"></i></div>
                        <h5>Current Commission</h5>
                        <div class="toolbar">
                          <nav style="padding: 8px;">
                              <a href="javascript:;" class="btn btn-default btn-xs collapse-box"><i class="fa fa-minus"></i></a>
                              <a href="javascript:;" class="btn btn-default btn-xs full-box"><i class="fa fa-expand"></i></a>
                          </nav>
                        </div>
                    </header>
                    <div id="div-1" class="body collapse in" aria-expanded="true">
                    <?php
					$stmt = $db->prepare("select * from affiliatehotel_agreement where hoteloid = :a");
					$stmt->execute(array(':a' => $hotel['hoteloid']));
					$agreement = $stmt->fetch(PDO::FETCH_ASSOC);
					?>

                    	<label>Commission Type :</label> <?php echo $agreement['commissiontype']; ?><br />
                        <?php if($agreement['commissiontype'] == "flat fee") { ?>
                        Flat Fee Billing Periode : <?php echo $agreement['flatfee_billingtype']; ?><br />
                        Flat Fee Amount : <?php echo number_format($agreement['flatfee_amount']); ?><br />
                        Affiliate Commission : <?php echo $agreement['flatfee_commission']; ?>% from flat fee amount<br />
                        <?php }else if($agreement['commissiontype'] == "commission percent"){ ?>
                        The B&uuml;king Commission : <?php echo $agreement['thebuking_commission']; ?>%<br />
                        Affiliate Commission : <?php echo $agreement['affiliate_commission']; ?>% from agreement<br />
                        Minimum Guarantee Commission : <?php echo number_format($agreement['min_guarantee_commission']); ?>
                        <?php }else if($agreement['commissiontype'] == "commission markup"){ ?>
                        Base Commission : <?php echo $agreement['base_commission']; ?>%<br />
                        Affiliate Commission Markup : <?php echo $agreement['affiliate_commission_markup']; ?>%<br />
                        Agreement Commission : <?php echo $agreement['thebuking_commission']; ?>%<br />
                        Minimum Guarantee Commission : <?php echo number_format($agreement['min_guarantee_commission']); ?>
						<?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xs-12">
                <div class="box dark">
                    <header>
                        <div class="icons"><i class="fa fa-info-circle"></i></div>
                        <h5>Log Status</h5>
                        <div class="toolbar">
                          <nav style="padding: 8px;">
                              <a href="javascript:;" class="btn btn-default btn-xs collapse-box"><i class="fa fa-minus"></i></a>
                              <a href="javascript:;" class="btn btn-default btn-xs full-box"><i class="fa fa-expand"></i></a>
                          </nav>
                        </div>
                    </header>
                    <div id="div-1" class="body collapse in" aria-expanded="true">
                    <table id="log-status" class="table table-striped responsive-table">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Date</th>
                                <th>From</th>
                                <th>To</th>
                                <th>By</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
						    $no = 0;
						    $stmt = $db->prepare("select al.*, x.status as statusfrom, y.status as statusto  from affiliatelog al inner join (select ahs.status, ahs.affiliatehotelstatusoid from affiliatehotelstatus ahs) x on x.affiliatehotelstatusoid = al.from_status  inner join (select ahs.status, ahs.affiliatehotelstatusoid from affiliatehotelstatus ahs) y on y.affiliatehotelstatusoid = al.to_status where al.hoteloid = :a and al.from_type = :b order by al.startdate desc");
						    $stmt->execute(array(':a' => $hoteloid, ':b' => 'sm')); 
							foreach($stmt->fetchAll(PDO::FETCH_ASSOC) as $row){
						?>
                            <tr>
                            	<td><?php echo ++$no; ?></td>
                            	<td><?php echo date('j M Y H:i', strtotime($row['startdate'])); ?></td>
                                <td><?php echo $row['statusfrom']; ?></td>
                                <td><?php echo $row['statusto']; ?></td>
                                <td><?php echo $row['displayname']; ?></td>
                            </tr>
                            <tr><td colspan="5"><label>Note :</label> <?php echo $row['note']; ?>
                            <?php
								/*
								switch($row['to_status']){
									case 2 : include('include/log-hotel-agreement.php'); break;
									case 7 : include('include/log-hotel-production.php'); break;
									case 8 : include('include/log-hotel-production.php'); break;
									default : break;
								}
								*/
							?>
                            </td></tr>
                        <?php 
							}
						?>
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>