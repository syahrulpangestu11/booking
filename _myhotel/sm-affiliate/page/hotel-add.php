<form class="form-horizontal" method="post" enctype="multipart/form-data" action="<?php echo $baseurl; ?>/register-process-hotel">
    <input type="hidden" name="dfltstate" value="2">
    <input type="hidden" name="dfltcity" value="0">
    <input type="hidden" name="dfltchain" value="0">
    <div class="row">
        <div class="col-lg-12 col-xs-12">
            <div class="box dark">
                <header>
                    <div class="icons"><i class="fa fa-home"></i></div>
                    <h5>Hotel Information</h5>
                    <div class="toolbar">
                      <nav style="padding: 8px;">
                          <a href="javascript:;" class="btn btn-default btn-xs collapse-box"><i class="fa fa-minus"></i></a>
                          <a href="javascript:;" class="btn btn-default btn-xs full-box"><i class="fa fa-expand"></i></a>
                      </nav>
                    </div>
                </header>
                <div id="div-1" class="body collapse in" aria-expanded="true">
					<div class="row">
                        <div class="col-lg-6 col-xs-12">
                            <div class="form-group">
                                <label for="text1" class="control-label col-lg-4">Hotel Name</label>
                                <div class="col-lg-8">
                                    <input type="text" name="name" id="hotel-search" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="text1" class="control-label col-lg-4">Chain</label>
                                <div class="col-lg-8">
                                    <select name="chain" class="form-control">
                    <?php
                    try {
                        $stmt = $db->query("select * from chain where publishedoid = 1");
                        $r_hoteltype = $stmt->fetchAll(PDO::FETCH_ASSOC);
                        foreach($r_hoteltype as $row){
                    ?>
                        <option value="<?php echo $row['chainoid']; ?>"><?php echo $row['name']; ?></option>
                    <?php	
                        }
                    }catch(PDOException $ex) {
                        echo "Invalid Query";
                        die();
                    }
                    ?>
                                        <option value="0">Other</option>
                                    </select>
                                    <input type="text" name="chainname" class="form-control other" placeholder="Type Chain Name" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="text1" class="control-label col-lg-4">Brand</label>
                                <div class="col-lg-8 loc-brand">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="text1" class="control-label col-lg-4">Star Rating</label>
                                <div class="col-lg-2">
                                    <input type="number" name="star" min="0" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="text1" class="control-label col-lg-4">Website</label>
                                <div class="col-lg-8">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-globe"></i></span>
                                        <input type="text" name="website" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-xs-12">
                            <div class="form-group">
                                <label for="text1" class="control-label col-lg-4">Address</label>
                                <div class="col-lg-8">
                                    <textarea id="address" name="address" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="text1" class="control-label col-lg-4">Country</label>
                                <div class="col-lg-8">
                                    <select name="country" class="form-control">
                                        <?php
	try {
		global $db;
		$stmt = $db->query("select * from country");
		$r_country = $stmt->fetchAll(PDO::FETCH_ASSOC);
		foreach($r_country as $row){
			if($row['countryoid'] == 11){ $selected = "selected"; }else{ $selected=""; }
	?>
		<option value="<?php echo $row['countryoid']; ?>" <?php echo $selected; ?>><?php echo $row['countryname']; ?></option>
	<?php	
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}
										?>
                                    </select>
                                    <input type="text" name="countryname" class="form-control other" placeholder="Type Country Name" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="text1" class="control-label col-lg-4">State</label>
                                <div class="col-lg-8 loc-state">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="text1" class="control-label col-lg-4">City</label>
                                <div class="col-lg-8 loc-city">
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-xs-12">
            <div class="box dark">
                <header>
                    <div class="icons"><i class="fa fa-address-book-o"></i></div>
                    <h5>Hotel Contact Management</h5>
                    <div class="toolbar">
                      <nav style="padding: 8px;">
                          <a href="javascript:;" class="btn btn-default btn-xs collapse-box"><i class="fa fa-minus"></i></a>
                          <a href="javascript:;" class="btn btn-default btn-xs full-box"><i class="fa fa-expand"></i></a>
                      </nav>
                    </div>
                </header>
                <div id="div-1" class="body collapse in" aria-expanded="true">
                    <div class="form-group">
                        <label for="text1" class="control-label col-lg-3">Owner Contact</label>
                        <div class="col-lg-3">
							<input type="text" name="owner" placeholder="Name" class="form-control">
						</div>
                        <div class="col-lg-3">
                            <div class="input-group">
								<span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                <input type="text" name="owner_email" placeholder="Email Address" class="form-control">
							</div>
						</div>
                        <div class="col-lg-3">
                            <div class="input-group">
								<span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                <input type="text" name="owner_phone" placeholder="Phone Number" class="form-control">
							</div>
						</div>
                    </div>
                    <div class="form-group">
                        <label for="text1" class="control-label col-lg-3">General Manager Contact</label>
                        <div class="col-lg-3">
							<input type="text" name="generalmanager" placeholder="Name" class="form-control">
						</div>
                        <div class="col-lg-3">
                            <div class="input-group">
								<span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                <input type="text" name="gm_email" placeholder="Email Address" class="form-control">
							</div>
						</div>
                        <div class="col-lg-3">
                            <div class="input-group">
								<span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                <input type="text" name="gm_phone" placeholder="Phone Number" class="form-control">
							</div>
						</div>
                    </div>
                    <div class="form-group">
                        <label for="text1" class="control-label col-lg-3">Director of Sales Contact</label>
                        <div class="col-lg-3">
							<input type="text" name="directorofsales" placeholder="Name" class="form-control">
						</div>
                        <div class="col-lg-3">
                            <div class="input-group">
								<span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                <input type="text" name="dos_email" placeholder="Email Address" class="form-control">
							</div>
						</div>
                        <div class="col-lg-3">
                            <div class="input-group">
								<span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                <input type="text" name="dos_phone" placeholder="Phone Number" class="form-control">
							</div>
						</div>
                    </div>
                    <div class="form-group">
                        <label for="text1" class="control-label col-lg-3">E-commerce Contact</label>
                        <div class="col-lg-3">
							<input type="text" name="ecommerce" placeholder="Name" class="form-control">
						</div>
                        <div class="col-lg-3">
                            <div class="input-group">
								<span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                <input type="text" name="ecomm_email" placeholder="Email Address" class="form-control">
							</div>
						</div>
                        <div class="col-lg-3">
                            <div class="input-group">
								<span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                <input type="text" name="ecomm_phone" placeholder="Phone Number" class="form-control">
							</div>
						</div>
                    </div>
                    <div class="form-group">
                        <label for="text1" class="control-label col-lg-3">Finance Contact</label>
                        <div class="col-lg-3">
							<input type="text" name="finance" placeholder="Name" class="form-control">
						</div>
                        <div class="col-lg-3">
                            <div class="input-group">
								<span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                <input type="text" name="finance_email" placeholder="Email Address" class="form-control">
							</div>
						</div>
                        <div class="col-lg-3">
                            <div class="input-group">
								<span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                <input type="text" name="finance_phone" placeholder="Phone Number" class="form-control">
							</div>
						</div>
                    </div>
                    <div class="form-group">
                        <label for="text1" class="control-label col-lg-3">Hotel / General Info Contact</label>
                        <div class="col-lg-3">
                            <div class="input-group">
								<span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                <input type="text" name="hotel_email" placeholder="Email Address" class="form-control">
							</div>
						</div>
                        <div class="col-lg-3">
                            <div class="input-group">
								<span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                <input type="text" name="hotel_phone" placeholder="Phone Number" class="form-control">
							</div>
						</div>
                        <div class="col-lg-3">
                            <div class="input-group">
								<span class="input-group-addon"><i class="fa fa-fax"></i></span>
                                <input type="text" name="hotel_fax" placeholder="Fax Number" class="form-control">
							</div>
						</div>
</div>
				</div>
			</div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-xs-12">
            <div class="box dark">
                <header>
                    <div class="icons"><i class="fa fa-folder-open"></i></div>
                    <h5>Current Booking Enginee Information</h5>
                    <div class="toolbar">
                      <nav style="padding: 8px;">
                          <a href="javascript:;" class="btn btn-default btn-xs collapse-box"><i class="fa fa-minus"></i></a>
                          <a href="javascript:;" class="btn btn-default btn-xs full-box"><i class="fa fa-expand"></i></a>
                      </nav>
                    </div>
                </header>
                <div id="div-1" class="body collapse in" aria-expanded="true">
                    <div class="form-group">
                        <label for="text1" class="control-label col-lg-4">Current Booking Enginee</label>
                        <div class="col-lg-4">
                            <select name="be_id" class="form-control">
                                <option value="">select booking enginee</option>
                                <option value="0">Other</option>
                            </select>
                            <input type="text" name="be" class="form-control other" placeholder="Type Booking Enginee Name" />
						</div>
                    </div>
                    <div class="form-group">
                        <label for="text1" class="control-label col-lg-4">End of Contract</label>
                        <div class="col-lg-3">
                            <select name="contract_year" class="form-control">
                                <option value="0">select year</option>
                                <?php 
								for($i = $startyear; $i <= $startyear+$numyear; $i++){
                                ?>
                                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php	
								}
								?>
                            </select>
						</div>
                        <div class="col-lg-3">
                            <select name="contract_month" class="form-control">
                                <option value="0">select month</option>
                                <?php 
								for($i = 1; $i <= 12; $i++){
                                ?>
                                <option value="<?php echo $i; ?>"><?php echo date('F', strtotime(date('Y').'-'.$i.'-01')); ?></option>
                                <?php	
								}
								?>
                            </select>
						</div>
                    </div>
                    <div class="form-group">
                        <label for="text1" class="control-label col-lg-4">Penalty if broken the current contract</label>
                        <div class="col-lg-8">
                            <textarea id="penalty" name="penalty" class="form-control"></textarea>
						</div>
                    </div>
				</div>
			</div>
        </div>
    </div>
    <div class="row">
    	<div class="col-lg-12">
            <div class="form-group">
                <div class="col-lg-12">
                    <button type="submit" class="btn btn-primary" style="width:100%">Register New Hotel</button>
                </div>
            </div>
        </div>
    </div>
</form>