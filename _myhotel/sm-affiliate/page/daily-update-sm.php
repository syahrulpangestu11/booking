<?php
if(isset($_POST['date'])){
  $defaultdate = date('Y-m-d', strtotime($_POST['date']));
}else{
  $defaultdate = date('Y-m-d');
}
?>
<script>
$(function(){
  $( "#datepicker" ).datepicker({
    changeMonth: true,
    changeYear: true
  });
});
</script>
<style>
.table-bordered > tbody > tr:first-child > td{ padding:8px; }
</style>
<section class="content-header">
    <h1>Daily Update SM</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-bullhorn"></i> Sales &amp; Marketing</a></li>
        <li class="active"><a href="#"><i class="fa fa-building"></i> Daily Update SM</a></li>
    </ol>
</section>
<section class="content">
  <div class="box">
      <div class="box-body box-form">
          <form method="post" class="form-inline" action="<?php echo $base_url.'/'.$uri2.'/daily-update-sm'?>">
          <div class="row">
              <div class="col-md-12">
                  <div class="form-group">
                      <label>Show from</label>
                  </div>
                  <div class="form-group">
                      <input type="text" class="form-control" id="datepicker" name="date" value="<?=date('d F Y', strtotime($defaultdate))?>">
                  </div>
                  <div class="form-group">
                      <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-search"></i> Show</button>
                  </div>
              </div>
          </div>
        </form>
      </div>
  </div>

  <div class="box">
    <div class="box-body box-form">
      <table class="table table-bordered">
        <thead>
          <tr style="background-color: #232323; color: #FFF;">
            <th>&nbsp;</th>
            <?php
            for($i = 0; $i < 7; $i++){
              $date = date('d M', strtotime($defaultdate.' +'.$i.'day'));
              echo "<th style='text-align:center' width='12.5%'>".$date."</th>";
            }
            ?>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th vertical-align="top" style="background-color: #eaeaea;">Planning</th>
            <?php
            for($i = 0; $i < 7; $i++){
              $date = date('Y-m-d', strtotime($defaultdate.' +'.$i.'day'));
              echo "<td vertical-align='top' style='font-size:0.87em'>";
              $stmt = $db->query("select h.hoteloid, h.hotelname, ahs.status, al.startdate, al.note, al.affiliatelogoid, u.username from hotel h inner join affiliatehotelstatus ahs using (affiliatehotelstatusoid) left join affiliatelog al on h.hoteloid = al.hoteloid left join users u using (useroid) left join afflogdetail ald on al.affiliatelogoid = ald.affiliatelogoid where al.to_status in ('2') and al.to_type = 'sm' and (date(startdate) = '".$date."' or date(detail_date) = '".$date."') group by h.hoteloid order by al.startdate");
              $no = 0;
              foreach($stmt->fetchAll(PDO::FETCH_ASSOC) as $row){
                echo "<a href='".$base_url."/".$uri2."/detail-sm-hotel/".$row['hoteloid']."' target='_blank'><b>".++$no.") ".$row['hotelname']."</b></a><br>";

                if(!empty($row['note'])){ $note = $row['note']." [".$row['username']."]<br>"; }else{ $note = "[".$row['username']."]<br>"; }

                $stmt3 = $db->prepare("select afflogdetail.*, users.username from afflogdetail left join users using (useroid) where affiliatelogoid = :a and detail_date <= '".$date."' order by detail_date desc");
                $stmt3->execute(array(':a' => $row['affiliatelogoid']));
                foreach($stmt3->fetchAll(PDO::FETCH_ASSOC) as $logdtl){
                  $note.= "&bull; ".date('d/M/Y', strtotime($logdtl['detail_date']))." - ".$logdtl['detail_note']." [".$logdtl['username']."]<br>";
                }

                if(!empty($note)){ echo $note; }
              }
              echo "</td>";
            }
            ?>
          </tr>
          <tr>
            <th vertical-align="top" style="background-color: #eaeaea;">SM</th>
            <?php
            for($i = 0; $i < 7; $i++){
              $date = date('Y-m-d', strtotime($defaultdate.' +'.$i.'day'));
              echo "<td vertical-align='top' style='font-size:0.87em'>";
              $stmt = $db->query("select h.hoteloid, h.hotelname, ahs.status, al.startdate, al.note, al.affiliatelogoid, u.username from hotel h inner join affiliatehotelstatus ahs using (affiliatehotelstatusoid) left join affiliatelog al on h.hoteloid = al.hoteloid left join users u using (useroid) left join afflogdetail ald on al.affiliatelogoid = ald.affiliatelogoid where al.to_status in ('3') and al.to_type = 'sm' and (date(startdate) = '".$date."' or date(detail_date) = '".$date."') group by h.hoteloid order by al.startdate");
              $no = 0;
              foreach($stmt->fetchAll(PDO::FETCH_ASSOC) as $row){
                echo "<a href='".$base_url."/".$uri2."/detail-sm-hotel/".$row['hoteloid']."' target='_blank'><b>".++$no.") ".$row['hotelname']."</b></a><br>";

                if(!empty($row['note'])){ $note = $row['note']." [".$row['username']."]<br>"; }else{ $note = "[".$row['username']."]<br>"; }

          			$stmt3 = $db->prepare("select afflogdetail.*, users.username from afflogdetail left join users using (useroid) where affiliatelogoid = :a and detail_date <= '".$date."' order by detail_date desc");
          	    $stmt3->execute(array(':a' => $row['affiliatelogoid']));
          	   	foreach($stmt3->fetchAll(PDO::FETCH_ASSOC) as $logdtl){
          				$note.= "&bull; ".date('d/M/Y', strtotime($logdtl['detail_date']))." - ".$logdtl['detail_note']." [".$logdtl['username']."]<br>";
          			}

                if(!empty($note)){ echo $note; }
              }
              echo "</td>";
            }
            ?>
          </tr>
          <tr>
            <th vertical-align="top" style="background-color: #eaeaea;">SF</th>
            <?php
            for($i = 0; $i < 7; $i++){
              $date = date('Y-m-d', strtotime($defaultdate.' +'.$i.'day'));
              echo "<td vertical-align='top' style='font-size:0.87em'>";
              $stmt = $db->query("select h.hoteloid, h.hotelname, ahs.status, al.startdate, al.note, al.affiliatelogoid, u.username from hotel h inner join affiliatehotelstatus ahs using (affiliatehotelstatusoid) left join affiliatelog al on h.hoteloid = al.hoteloid left join users u using (useroid) where al.to_status in ('5') and al.to_type = 'sm' and date(startdate) = '".$date."' group by h.hoteloid order by al.startdate");
              $no = 0;
              foreach($stmt->fetchAll(PDO::FETCH_ASSOC) as $row){
                echo "<a href='".$base_url."/".$uri2."/detail-sm-hotel/".$row['hoteloid']."' target='_blank'><b>".++$no.") ".$row['hotelname']."</b></a><br>";
                if(!empty($row['note'])){ echo $row['note']." [".$row['username']."]<br>"; }else{ echo "[".$row['username']."]<br>"; }
              }
              echo "</td>";
            }
            ?>
          </tr>
          <tr>
            <th vertical-align="top" style="background-color: #eaeaea;">Implementation</th>
            <?php
            for($i = 0; $i < 7; $i++){
              $date = date('Y-m-d', strtotime($defaultdate.' +'.$i.'day'));
              echo "<td vertical-align='top' style='font-size:0.87em'>";
              $stmt = $db->query("select h.hoteloid, h.hotelname, ahs.status, al.startdate, al.note, al.affiliatelogoid, u.username from hotel h inner join affiliatehotelstatus ahs using (affiliatehotelstatusoid) left join affiliatelog al on h.hoteloid = al.hoteloid left join users u using (useroid) where al.to_status in ('6') and al.to_type = 'sm' and date(startdate) = '".$date."' group by h.hoteloid order by al.startdate");
              $no = 0;
              foreach($stmt->fetchAll(PDO::FETCH_ASSOC) as $row){
                echo "<a href='".$base_url."/".$uri2."/detail-sm-hotel/".$row['hoteloid']."' target='_blank'><b>".++$no.") ".$row['hotelname']."</b></a><br>";
                if(!empty($row['note'])){ echo $row['note']." [".$row['username']."]<br>"; }else{ echo "[".$row['username']."]<br>"; }
              }
              echo "</td>";
            }
            ?>
          </tr>
          <tr>
            <th vertical-align="top" style="background-color: #eaeaea;">Rejection</th>
            <?php
            for($i = 0; $i < 7; $i++){
              $date = date('Y-m-d', strtotime($defaultdate.' +'.$i.'day'));
              echo "<td vertical-align='top' style='font-size:0.87em'>";
              $stmt = $db->query("select h.hoteloid, h.hotelname, ahs.status, al.startdate, al.note, al.affiliatelogoid, u.username from hotel h inner join affiliatehotelstatus ahs using (affiliatehotelstatusoid) left join affiliatelog al on h.hoteloid = al.hoteloid left join users u using (useroid) where al.to_status in ('4') and al.to_type = 'sm' and date(startdate) = '".$date."' group by h.hoteloid order by al.startdate");
              $no = 0;
              foreach($stmt->fetchAll(PDO::FETCH_ASSOC) as $row){
                echo "<a href='".$base_url."/".$uri2."/detail-sm-hotel/".$row['hoteloid']."' target='_blank'><b>".++$no.") ".$row['hotelname']."</b></a><br>";
                if(!empty($row['note'])){ echo $row['note']." [".$row['username']."]<br>"; }else{ echo "[".$row['username']."]<br>"; }
              }
              echo "</td>";
            }
            ?>
          </tr>
          <tr>
            <th vertical-align="top" style="background-color: #eaeaea;">Aborted</th>
            <?php
            for($i = 0; $i < 7; $i++){
              $date = date('Y-m-d', strtotime($defaultdate.' +'.$i.'day'));
              echo "<td vertical-align='top' style='font-size:0.87em'>";
              $stmt = $db->query("select h.hoteloid, h.hotelname, ahs.status, al.startdate, al.note, al.affiliatelogoid, u.username from hotel h inner join affiliatehotelstatus ahs using (affiliatehotelstatusoid) left join affiliatelog al on h.hoteloid = al.hoteloid left join users u using (useroid) where al.to_status in ('13') and al.to_type = 'sm' and date(startdate) = '".$date."' group by h.hoteloid order by al.startdate");
              $no = 0;
              foreach($stmt->fetchAll(PDO::FETCH_ASSOC) as $row){
                echo "<a href='".$base_url."/".$uri2."/detail-sm-hotel/".$row['hoteloid']."' target='_blank'><b>".++$no.") ".$row['hotelname']."</b></a><br>";
                if(!empty($row['note'])){ echo $row['note']." [".$row['username']."]<br>"; }else{ echo "[".$row['username']."]<br>"; }
              }
              echo "</td>";
            }
            ?>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</section>
