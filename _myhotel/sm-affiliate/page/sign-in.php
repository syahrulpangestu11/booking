<div class="form-signin container">
	<div class="row">
    	<div class="col-md-12">
    <div class="text-center">
        <img src="<?php echo $baseurl; ?>/assets/img/logo.png" alt="The B&Uuml;king Logo">
    </div>
    <hr>
    <div class="tab-content">
        <div id="login" class="tab-pane active row">
            <form class="form-horizontal" action="<?php echo $baseurl; ?>/validate" method="post">
                <p class="text-muted text-center">
                    Enter your email and password
                </p>
                <div class="form-group">
                    <div class="col-md-12">
                        <input type="email" name="email" placeholder="Email" class="form-control top">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <input type="password" name="password" placeholder="Password" class="form-control bottom">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6">
                        <button class="btn btn-lg btn-primary btn-block" type="button">Register</button>
                    </div>
                    <div class="col-md-6">
                        <button class="btn btn-lg btn-primary  btn-block" type="submit">Sign in</button>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <button class="btn btn-lg btn-primary btn-line btn-block" type="button"><i class="fa fa-facebook-official"></i> login with facebook</button>
                    </div>
                </div>
            </form>
        </div>
        <div id="forgot" class="tab-pane">
            <form action="index.html">
                <p class="text-muted text-center">Enter your valid e-mail</p>
                <input type="email" placeholder="mail@domain.com" class="form-control">
                <br>
                <button class="btn btn-lg btn-danger btn-block" type="submit">Recover Password</button>
            </form>
        </div>
        <div id="signup" class="tab-pane">
            <form class="form-horizontal" action="<?php echo $baseurl; ?>/register" method="post">
                <p class="text-muted text-center">
                    Fill Your Data
                </p>
                <div class="form-group">
                    <div class="col-md-12">
                        <input type="text" name="firstname" placeholder="First Name" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <input type="text" name="lastname" placeholder="Last Name" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <input type="email" name="email" placeholder="Email" class="form-control top">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <input type="text" name="phone" placeholder="Phone Number" class="form-control top">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <input type="password" name="password" placeholder="Password" class="form-control bottom">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6">
                        <button class="btn btn-lg btn-primary btn-block" type="submit">Register</button>
                    </div>
                    <div class="col-md-6">
                        <button class="btn btn-lg btn-primary  btn-block" type="button">Sign in</button>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <button class="btn btn-lg btn-primary btn-line btn-block" type="button"><i class="fa fa-facebook-official"></i> login with facebook</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <hr>
    <div class="text-center">
        <ul class="list-inline">
            <li><a class="text-muted" href="#login" data-toggle="tab">Login</a></li>
            <li><a class="text-muted" href="#forgot" data-toggle="tab">Forgot Password</a></li>
            <li><a class="text-muted" href="#signup" data-toggle="tab">Signup</a></li>
        </ul>
    </div>
    </div>
</div>
