<?php include('includes/management/function/function-production-occupancy.php'); ?>
<style type="text/css">
  .table-bordered>thead>tr>th{ vertical-align: top!important; text-align: center;}
  .table-bordered tbody tr:nth-child(even) td{
      background-color: #f9fafa;
  }
  .table-bordered tfoot tr th{
      background-color: #ebf2f2;
  }
</style>
<section class="content-header">
    <h1>KPI</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-bullhorn"></i> Sales &amp; Marketing</a></li>
        <li class="active"><a href="#"><i class="fa fa-building"></i> KPI</a></li>
    </ol>
</section>
<section class="content">
  <div class="box">
      <div class="box-body box-form">
          <form method="post" class="form-inline" action="<?php echo $base_url.'/'.$uri2.'/kpi'?>">
          <div class="row">
              <div class="col-md-12">
                  <div class="form-group">
                      <label>Year</label>
                      <select name="year" class="form-control">
                        <?php
                          $startyear = 2017;
                          $year = $_POST['year'];
                          if(empty($_POST['year'])){ $year = $startyear; }
                          for($i = $startyear; $i <= date('Y'); $i++){
                            if($i == $year){ $selected = "selected"; }else{ $selected = ""; }
                        ?>
                        <option value="<?=$i?>" <?=$selected?>><?=$i?></option>
                        <?php
                          }
                        ?>
                      </select>
                      <select name="user" class="form-control">
                        <option value="" <?=$selected?>>Show all</option>
                        <?php
                          $stmt = $db->query("select useroid, username, displayname from users where userstypeoid in ('1', '2')");
                          $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
                          foreach($result as $user){
                            if($_POST['user'] == $user['useroid']){ $selected = "selected"; }else{ $selected = ""; }
                        ?>
                        <option value="<?=$user['useroid']?>" <?=$selected?>><?=$user['displayname']?></option>
                        <?php
                          }
                        ?>
                      </select>
                  </div>
                  <div class="form-group">
                      <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-search"></i> Show</button>
                  </div>
              </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <table class="table table-bordered">
                <thead>
                  <tr><th rowspan="2">Periode</th><th rowspan="2">SM</th><th rowspan="2">SF</th><th rowspan="2">Reject</th><th rowspan="2">Implementation</th><th rowspan="2">Abort</th><th colspan="4">In</th><th colspan="2">Pending</th><th rowspan="2">Commerce</th><th rowspan="2">Accumulated Commerce</th><th rowspan="2">Commission Production</th><th rowspan="2">Commission Occupancy</th></tr>
                  <tr>
                    <th>WDM</th><th>Content</th><th>Training</th><th>Live</th><th>WDM</th><th>Content</th>
                  </tr>
                </thead>
                <tbody>
                  <tr style="display:none"></tr>
                  <?php
                  $py_commerce = 0;
                  for($yy=$startyear; $yy<=($year-1); $yy++){
                    if(!empty($_POST['user'])){
                      $query_user = " and useroid = '".$_POST['user']."'";
                    }else{
                      $query_user = "";
                    }
                    $stmt = $db->prepare("select IFNULL(sum(to_type = 'sm' and to_status = 8), '0') as commerce from affiliatelog inner join hotel h using (hoteloid) where YEAR(startdate) = :b".$query_user);
                    $stmt->execute(array( ':b' => $yy));
                    $statusprev = $stmt->fetch(PDO::FETCH_ASSOC);
                    $py_commerce = $statusprev['commerce'];
                  }
                  
                  ?>
                  <tr>
                    <td><b>Previous Year</b></td>
                    <td class="text-center"></td>
                    <td class="text-center"></td>
                    <td class="text-center"></td>
                    <td class="text-center"></td>
                    <td class="text-center"></td>
                    <td class="text-center"></td>
                    <td class="text-center"></td>
                    <td class="text-center"></td>
                    <td class="text-center"></td>
                    <td class="text-center"></td>
                    <td class="text-center"></td>
                    <td class="text-center"></td>
                    <td class="text-center"><?=$py_commerce?></td>
                    <td class="text-right"></td>
                    <td class="text-right"></td>
                  </tr>
                  
                  <?php
                  $commerce = 0;
                  $total_production = 0;
                  $total_occupancy = 0;
                  $sm = array(); $sf = array(); $rjt = array(); $implementation = array(); $abt = array(); $commerce = array();
                  $wdm = array();   $content = array();   $training = array();   $wdmlive = array();
                  array_push($commerce, (int)$py_commerce);
                  for($m=1; $m <=12; $m++){
                    if(!empty($_POST['user'])){
                      $query_user = " and useroid = '".$_POST['user']."'";
                    }else{
                      $query_user = "";
                    }
                    $stmt = $db->prepare("select IFNULL(sum(to_type = 'sm' and to_status = 3), '0') as sm, IFNULL(sum(to_type = 'sm' and to_status = 4), '0') as rjt, IFNULL(sum(to_type = 'sm' and to_status = 13), '0') as abt, IFNULL(sum(to_type = 'sm' and to_status = 5), '0') as sf, IFNULL(sum(to_type = 'sm' and to_status = 6), '0') as implementation, IFNULL(sum(to_type = 'wdm' and to_status = 2), '0') as wdm, IFNULL(sum(to_type = 'wdm' and to_status = 3), '0') as content, IFNULL(sum(to_type = 'wdm' and to_status = 4), '0') as training, IFNULL(sum(to_type = 'wdm' and to_status = 7), '0') as wdmlive, IFNULL(sum(to_type = 'sm' and to_status = 8), '0') as commerce from affiliatelog inner join hotel h using (hoteloid) where MONTH(startdate) = :a and YEAR(startdate) = :b".$query_user);
                    $stmt->execute(array( ':a' => $m, ':b' => $year));
                    $status = $stmt->fetch(PDO::FETCH_ASSOC);

                    $stmt2 = $db->prepare("select IFNULL(sum(to_type = 'wdm' and to_status = 2), '0') as wdmpending, IFNULL(sum(to_type = 'wdm' and to_status = 3), '0') as contentpending from affiliatelog al inner join hotel h using (hoteloid) where ((MONTH(startdate) <= :a and YEAR(startdate) <= :b) or (MONTH(startdate) > :a1 and YEAR(startdate) < :b1)) and enddate IS NULL and h.affwdmstatusoid in ('2', '3')".$query_user);
                    $stmt2->execute(array( ':a' => $m, ':b' => $year, ':a1' => $m, ':b1' => $year));
                    $statuspending = $stmt2->fetch(PDO::FETCH_ASSOC);

                    $chart_start_date	= $year.'-'.$m.'-01';
                		$chart_to_date	= date('Y-m-t', strtotime($chart_start_date));
                    if(empty($_POST['user'])){
                  		$commission_production = pdSumValue($chart_start_date, $chart_to_date, "gbhcollect", "", array(4,5,7,8));
                      $commission_occupancy = ocSumValue($chart_start_date, $chart_to_date, "gbhcollect", "", array(4,5,7,8));
                    }else{
                      $commission_production = pdSumValueByPIC($chart_start_date, $chart_to_date, "gbhcollect", "", array(4,5,7,8), $_POST['user']);
                      $commission_occupancy = ocSumValueByPIC($chart_start_date, $chart_to_date, "gbhcollect", "", array(4,5,7,8), $_POST['user']);
                    }

                    array_push($sm, (int)$status['sm']);
                    array_push($sf, (int)$status['sf']);
                    array_push($rjt, (int)$status['rjt']);
                    array_push($abt, (int)$status['abt']);
                    array_push($implementation, (int)$status['implementation']);
                    array_push($commerce, (int)$status['commerce']);
                    array_push($wdm, (int)$status['wdm']);
                    array_push($content, (int)$status['content']);
                    array_push($training, (int)$status['training']);
                    array_push($wdmlive, (int)$status['wdmlive']);

                    $total_production+=$commission_production;
                    $total_occupancy+=$commission_occupancy;
                  ?>
                  <tr>
                    <td><?=date('F', mktime(0, 0, 0, $m, 10));?> <?=$year?></td>
                    <td class="text-center"><?=$status['sm']?></td>
                    <td class="text-center"><?=$status['sf']?></td>
                    <td class="text-center"><?=$status['rjt']?></td>
                    <td class="text-center"><?=$status['implementation']?></td>
                    <td class="text-center"><?=$status['abt']?></td>
                    <td class="text-center"><?=$status['wdm']?></td>
                    <td class="text-center"><?=$status['content']?></td>
                    <td class="text-center"><?=$status['training']?></td>
                    <td class="text-center"><?=$status['wdmlive']?></td>
                    <td class="text-center"><?=$statuspending['wdmpending']?></td>
                    <td class="text-center"><?=$statuspending['contentpending']?></td>
                    <td class="text-center"><?=$status['commerce']?></td>
                    <td class="text-center"><?=array_sum($commerce)?></td>
                    <td class="text-right"><?=number_format($commission_production)?></td>
                    <td class="text-right"><?=number_format($commission_occupancy)?></td>
                  </tr>
                  <?php
                  }
                  ?>
                </tbody>
                <tfoot>
                  <tr>
                    <th class="text-center">Total</th>
                    <th class="text-center"><?=array_sum($sm)?></th>
                    <th class="text-center"><?=array_sum($sf)?></th>
                    <th class="text-center"><?=array_sum($rjt)?></th>
                    <th class="text-center"><?=array_sum($implementation)?></th>
                    <th class="text-center"><?=array_sum($abt)?></th>
                    <th class="text-center"><?=array_sum($wdm)?></th>
                    <th class="text-center"><?=array_sum($content)?></th>
                    <th class="text-center"><?=array_sum($training)?></th>
                    <th class="text-center"><?=array_sum($wdmlive)?></th>
                    <th class="text-center"><?=$statuspending['wdmpending']?></th>
                    <th class="text-center"><?=$statuspending['contentpending']?></th>
                    <th class="text-center"><?=array_sum($commerce)?></th>
                    <th class="text-center"><?=array_sum($commerce)?></th>
                    <th class="text-right"><?=number_format($total_production)?></th>
                    <th class="text-right"><?=number_format($total_occupancy)?></th>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </form>
      </div>
  </div>
</section>
