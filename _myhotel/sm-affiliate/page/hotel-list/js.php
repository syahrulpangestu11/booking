<script type="text/javascript">
$(function(){
	$(document).ready(function(){
		getLoadData();
	});

	/************************************************************************************/
	function getLoadData(){
		$.ajax({
			url: "<?php echo $base_url; ?>/sm-affiliate/page/hotel-list/data.php",
			type: 'post',
			data: $('form#data-input').serialize(),
			success: function(data) {
				$("#data-box").html(data)
			}
		});
	}

	$('body').on('click','#paging button', function(e) {
		page	= $(this).attr('page');
		pagebox = $('form#data-input').children('input[name=page]');
		pagebox.val(page);
		getLoadData();
	});
	/************************************************************************************/
	$('body').on('click','table#dataTable tr td:nth-child(-n+6)', function(e) {
		id = $(this).parent("tr").attr("oid");
		window.location.href = "<?php echo $base_url."/".$uri2."/detail-sm-hotel/"; ?>"+id;
	});
});
</script>
<style type="text/css">
	table#dataTable tr:nth-child(n+2):hover td{ cursor:pointer; background-color:#feffeb;  }
</style>
