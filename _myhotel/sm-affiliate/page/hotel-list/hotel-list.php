<?php
	include('js.php');
	$typestatus = $uri4;
	$affiliatehotelstatus = $uri5;

	$name = $_REQUEST['name'];
	$type = $_REQUEST['type'];
	if(!empty($_REQUEST['smstatus'])){
		$smstatus = $_REQUEST['smstatus'];
	}else{
		$smstatus = $affiliatehotelstatus;
	}
?>

<section class="content-header">
    <h1>List Property</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-bullhorn"></i> Sales &amp; Marketing</a></li>
        <li class="active"><a href="#"><i class="fa fa-building"></i> List Property</a></li>
    </ol>
</section>
<section class="content">
  <div class="row">
      <div class="box box-form">
          <div class="box-body">
						<form method="get" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/<?=$uri2?>/list-hotel/">
								<input type="hidden" name="page" value="1">
								<div class="col-md-2">
									<div class="form-group">
                    <label for="text1" class="control-label">Name</label>
                    <input type="text" name="name" class="form-control" value="<?php echo $name; ?>">
                  </div>
								</div>
								<div class="col-md-2">
									<div class="form-group">
                    <label for="text1" class="control-label">SM Status</label>
                    <select name="smstatus" class="form-control">
											<option value="">Show all SM status</option>
											<?php
											$stmt = $db->query("SELECT * FROM `affiliatehotelstatus` as `as` where `as`.publishedoid = '1'");
											$list_smstatus = $stmt->fetchAll(PDO::FETCH_ASSOC);
											foreach($list_smstatus as $smstatus){
												if($smstatus['affiliatehotelstatusoid'] == $_REQUEST['smstatus']){ $selected = "selected"; }else{ $selected = ""; }
											?>
											<option value="<?=$smstatus['affiliatehotelstatusoid']?>" <?=$selected?>><?=$smstatus['status']?></option>
											<?php
											}
											?>
										</select>
                  </div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
                    <label for="text1" class="control-label">IBE Status</label>
                    <select name="ibestatus" class="form-control">
											<option value="">Show all IBE status</option>
											<?php
											$stmt = $db->query("SELECT * FROM `affibestatus` as `ai` where `ai`.`publishedoid` = '1'");
											$list_ibestatus = $stmt->fetchAll(PDO::FETCH_ASSOC);
											foreach($list_ibestatus as $ibestatus){
												if($ibestatus['affibestatusoid'] == $_REQUEST['ibestatus']){ $selected = "selected"; }else{ $selected = ""; }
											?>
											<option value="<?=$ibestatus['affibestatusoid']?>" <?=$selected?>><?=$ibestatus['ibestatus']?></option>
											<?php
											}
											?>
										</select>
                  </div>
								</div>
								<div class="col-md-2">
									<div class="form-group">
                    <label for="text1" class="control-label">WDM Status</label>
                    <select name="wdmstatus" class="form-control">
											<option value="">Show all WDM status</option>
											<?php
											$stmt = $db->query("SELECT * FROM `affwdmstatus` as `aw` where `aw`.`publishedoid` = '1'");
											$list_wdmstatus = $stmt->fetchAll(PDO::FETCH_ASSOC);
											foreach($list_wdmstatus as $wdmstatus){
												if($wdmstatus['affwdmstatusoid'] == $_REQUEST['wdmstatus']){ $selected = "selected"; }else{ $selected = ""; }
											?>
											<option value="<?=$wdmstatus['affwdmstatusoid']?>" <?=$selected?>><?=$wdmstatus['wdmstatus']?></option>
											<?php
											}
											?>
										</select>
                  </div>
								</div>
                <div class="col-md-2">
										<label for="text1" class="control-label">&nbsp;</label>
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-search"></i> Find</button>
                </div>
              </div>
          </form>
        </div><!-- /.box-body -->
     </div>

			<div class="row">
			    <div class="box">
			    	<form method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/<?=$uri2?>/list-hotel/<?=$affiliatehotelstatus?>">
			            <div id="data-box" class="box-body">
			                <div class="loader">Loading...</div>
			            </div><!-- /.box-body -->
			        </form>
			   </div>
			</div>
</section>
