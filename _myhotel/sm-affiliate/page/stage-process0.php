<div id="sm-content">
    <div id="dashboard" class="text-center row">
		<?php
        $num = 0;
        $stmt = $db->query("SELECT * FROM `affiliatehotelstatus` as `as` where `as`.`publishedoid` = '1'");
        $list_affhotelstatus = $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach($list_affhotelstatus as $affhotelstatus){
            $num++;
            if($affhotelstatus['affiliatehotelstatusoid'] == 6){
                $liststatus = array(6, 14, 15);
            }else if($affhotelstatus['affiliatehotelstatusoid'] == 7){
                $liststatus = array(7, 14);
            }else if($affhotelstatus['affiliatehotelstatusoid'] == 8){
                $liststatus = array(8, 15);
            }else{
                $liststatus = $affhotelstatus['affiliatehotelstatusoid'];
            }
        ?>
            <a class="quick-btn status-1" href="#">
            	<i class="fa fa-sign-in fa-2x"></i><span><?=$affhotelstatus['status'];?></span>
                <span class="label label-danger"><?php countPerStatus($liststatus); ?></span>
			</a>
        <?php
        }
        ?>
    </div>
    <hr />
    <div id="dashboard" class="row">
        <div class="row">
        	<?php
			$num = 0;
			$stmt = $db->query("SELECT * FROM `affiliatehotelstatus` as `as` where `as`.`publishedoid` = '1'");
			$list_affhotelstatus = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($list_affhotelstatus as $affhotelstatus){
				$num++;
				if($num % 3 == 1){ echo'</div><div class="row">'; }
				if($affhotelstatus['affiliatehotelstatusoid'] == 6){
					$liststatus = array(6, 14, 15);
				}else if($affhotelstatus['affiliatehotelstatusoid'] == 7){
					$liststatus = array(7, 14);
				}else if($affhotelstatus['affiliatehotelstatusoid'] == 8){
					$liststatus = array(8, 15);
				}else{
					$liststatus = $affhotelstatus['affiliatehotelstatusoid'];
				}

				if(is_array($liststatus)){
					$searchstatus = implode(',',$liststatus);
				}else{
					$searchstatus = $liststatus;
				}
			?>
            <div class="col-lg-4 col-xs-12">
                <div class="box inverse status-<?=$affhotelstatus['affiliatehotelstatusoid'];?>">
                    <header>
                        <h5><?=$affhotelstatus['status'];?></h5>
                        <!-- .toolbar -->
                        <div class="toolbar">
                          <nav style="padding: 8px;">
                          <input type="text" name="keyword" />
                          <button type="button" class="btn btn-xs btn-default" name="searchhotelstage" value="<?php echo $searchstatus; ?>"><i class="fa fa-search"></i></button>
                          </nav>
                        </div>
                        <!-- /.toolbar -->
                    </header>
                    <div id="div-2" class="body collapse in">
                        <?php showDataDashboard($affhotelstatus['affiliatehotelstatusoid'], $liststatus, '#sm-Modal'); ?>
                    </div>
                </div>
            </div>
            <?php
			}
			?>
    	</div>
    </div>

    <div id="dashboard" class="row">
    	<h3>WDM Status</h3>
        <div class="row">
        	<?php
			$num = 0;
			$stmt = $db->query("SELECT * FROM `affwdmstatus` as `aw` where `aw`.`publishedoid` = '1'");
			$list_affwdmstatus = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($list_affwdmstatus as $affwdm){
				$num++;
				if($num % 3 == 1){ echo'</div><div class="row">'; }
			?>
            <div class="col-md-4 col-xs-12">
                <div class="box inverse status-<?=$affwdm['affwdmstatusoid'];?>">
                    <header>
                        <div class="icons"><i class="fa fa-th-large"></i></div>
                        <h5><?=$affwdm['wdmstatus'];?></h5>
                        <!-- .toolbar -->
                        <div class="toolbar">
                          <nav style="padding: 8px;">
                          <input type="text" name="keyword" />
                          <button type="button" class="btn btn-xs btn-default" name="searchwdmstage" type-status="wdm-status" value="<?=$affwdm['affwdmstatusoid'];?>"><i class="fa fa-search"></i></button>
                          </nav>
                        </div>
                        <!-- /.toolbar -->
                    </header>
                    <div id="div-2" class="body collapse in">
                        <?php showDataWDM($affwdm['affwdmstatusoid'], '#wdm-Modal'); ?>
                    </div>
                </div>
            </div>
            <?php
			}
			?>
    	</div>
    </div>

    <div id="dashboard" class="row">
    	<h3>IBE Status</h3>
        <div class="row">
        	<?php
			$num = 0;
			$stmt = $db->query("SELECT * FROM `affibestatus` as `ai` where `ai`.`publishedoid` = '1'");
			$list_affhotelstatus = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($list_affhotelstatus as $affhotelstatus){
				$num++;
				if($num % 3 == 1){ echo'</div><div class="row">'; }
			?>
            <div class="col-md-4 col-xs-12">
                <div class="box inverse status-<?=$affhotelstatus['affibestatusoid'];?>">
                    <header>
                        <div class="icons"><i class="fa fa-th-large"></i></div>
                        <h5><?=$affhotelstatus['ibestatus'];?></h5>
                        <!-- .toolbar -->
                        <div class="toolbar">
                          <nav style="padding: 8px;">
                          <input type="text" name="keyword" />
                          <button type="button" class="btn btn-xs btn-default" name="searchibestage" type-status="ibe-status" value="<?=$affhotelstatus['affibestatusoid'];?>"><i class="fa fa-search"></i></button>
                          </nav>
                        </div>
                        <!-- /.toolbar -->
                    </header>
                    <div id="div-2" class="body collapse in">
                        <?php showDataIBE($affhotelstatus['affibestatusoid'], '#ibe-Modal'); ?>
                    </div>
                </div>
            </div>
            <?php
			}
			?>
    	</div>
    </div>
</div>
