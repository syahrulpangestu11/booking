<?php
$stmt			= $db->prepare("select * from afflogdetail where affiliatelogoid = :a");
$stmt->execute(array(':a' => $row['affiliatelogoid']));
$count_progress	= $stmt->rowCount(); 

if($count_progress > 0){
?>
	<table class="table table-bordered">
    	<tr>
        	<th>Date</th>
            <th>Note</th>
        </tr>
<?php
	foreach($stmt->fetchAll(PDO::FETCH_ASSOC) as $logdtl){
?>	
    	<tr>
        	<td><?=date('dmY', strtotime($logdtl['detail_date']))?></td>
            <td><?=$logdtl['detail_note']?></td>
        </tr>
<?php
	}
?>
    </table>
<?php
}
?>