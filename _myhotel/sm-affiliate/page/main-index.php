<script src="<?php echo $baseurl; ?>/assets/js/bootstrap3-typeahead.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.debug.js"></script>
<?php
$affiliate = new Affiliate($db);
$affiliate->setAffiliate($_SESSION['tbcaffnoidf']);
$generaldata = $affiliate->generalData();
include ('include/script-affiliate.php');
include ('include/function-affiliate.php');
?>
<div class="bg-dark dk" id="wrap">
    <div id="top">
        <header class="head">
            <div class="search-bar"><img src="<?php echo $baseurl; ?>/assets/img/logo-white.png" alt="The B&Uuml;king Logo"></div>
            <!-- /.search-bar -->
            <div class="main-bar">
                <div class="topnav">
                    <div class="btn-group">
                        <a data-placement="bottom" data-original-title="E-mail" data-toggle="tooltip"
                           class="btn btn-default btn-sm">
                            <i class="fa fa-envelope"></i>
                            <span class="label label-warning">5</span>
                        </a>
                        <a data-placement="bottom" data-original-title="Messages" href="#" data-toggle="tooltip"
                           class="btn btn-default btn-sm">
                            <i class="fa fa-comments"></i>
                            <span class="label label-danger">4</span>
                        </a>
                    </div>
                    <div class="btn-group">
                        <a data-placement="bottom" data-original-title="Fullscreen" data-toggle="tooltip"
                           class="btn btn-default btn-sm" id="toggleFullScreen">
                            <i class="glyphicon glyphicon-fullscreen"></i>
                        </a>
                        <a href="<?php echo $baseurl; ?>/sign-out" data-toggle="tooltip" data-original-title="Logout" data-placement="bottom"
                           class="btn btn-metis-1 btn-sm">
                            <i class="fa fa-power-off"></i>
                        </a>
                        <a data-placement="bottom" data-original-title="Show / Hide Left" data-toggle="tooltip"
                           class="btn btn-primary btn-sm toggle-left" id="menu-toggle">
                            <i class="fa fa-bars"></i>
                        </a>
                    </div>
                </div>
            </div>
        <!-- /.main-bar -->
        </header>
        <!-- /.head -->
    </div>
    <!-- /#top -->
        <div id="left">
            <div class="media user-media bg-dark dker">
                <div class="user-media-toggleHover">
                    <span class="fa fa-user"></span>
                </div>
                <div class="user-wrapper bg-dark">
                    <a class="user-link" href="">
                        <img class="media-object img-thumbnail user-img" alt="User Picture" src="assets/img/user.gif">
                        <span class="label label-danger user-label">16</span>
                    </a>
            
                    <div class="media-body">
                        <h5 class="media-heading">User</h5>
                        <ul class="list-unstyled user-info">
                            <li><a href=""><?php echo $_SESSION['tbcaffname']; ?></a></li>
                            <li><small>Last Access : <i class="fa fa-clock-o"></i>&nbsp;16 Mar 16:32</small></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #menu -->
            <ul id="menu" class="bg-dark dker">
                <li class="nav-header">Menu</li>
                <li class="nav-divider"></li>
                <li><a href="<?php echo $baseurl; ?>/dashboard"><i class="fa fa-dashboard"></i><span class="link-title">&nbsp;Dashboard</span></a></li>
                <li><a href="<?php echo $baseurl; ?>/profile"><i class="fa fa-user-circle-o"></i><span class="link-title">&nbsp;Profile</span></a></li>
                <li>
                     <a href="javascript:;"><i class="fa fa-building"></i><span class="link-title">&nbsp;Hotel</span><span class="fa arrow"></span></a>
                     <ul class="collapse">
                        <li><a href="<?php echo $baseurl; ?>/register-hotel"><i class="fa fa-angle-right"></i>&nbsp; Register Hotel</a></li>
                        <li><a href="<?php echo $baseurl; ?>/listhotel"><i class="fa fa-angle-right"></i>&nbsp; Hotel List</a></li>
                    </ul>
                </li>
				<li class="">
                    <a href="javascript:;"><i class="fa fa-tasks"></i><span class="link-title">&nbsp;Report</span><span class="fa arrow"></span></a>
                    <ul class="collapse">
                        <li><a href="<?php echo $baseurl; ?>/report-2"><i class="fa fa-angle-right"></i>&nbsp; Booking Report</a></li>
                        <li><a href="<?php echo $baseurl; ?>/report-1"><i class="fa fa-angle-right"></i>&nbsp; Paid Commission Report</a></li>
                    </ul>
                </li>
            </ul>
            <!-- /#menu -->
        </div>
        <!-- /#left -->
    <div id="content">
        <div class="outer">
            <div class="inner bg-light lter">
				<?php 
					switch($uri2){ 
						case 'dashboard' : include ('dashboard.php');	break;
						case 'profile' : include ('profile.php'); break;
						case 'report-1' : include ('report-1.php'); break;
						case 'report-2' : include ('report-2.php'); break;
						case 'register-hotel' : include ('hotel-add.php'); break;
						case 'register-process-hotel' : include ('include/register-hotel.php'); break;
						case 'listhotel' : include ('hotel-list.php'); break;
						case 'detailhotel' : include ('hotel-detail.php'); break;
						default	: include ('dashboard.php');	break;
					}
				?>
            </div>
            <!-- /.inner -->
        </div>
        <!-- /.outer -->
    </div>
    <!-- /#content -->
</div>

<div class="modal fade" id="responseModal" tabindex="-1" role="dialog" aria-labelledby="responseModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-info-circle"></i> Notification</h4>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>