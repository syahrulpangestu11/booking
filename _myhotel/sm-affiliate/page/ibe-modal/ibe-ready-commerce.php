<div class="row">
    <div class="col-md-6">
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <label class="control-label col-lg-4 text-left">Current Status</label>
                    <div class="col-lg-6">
                        <input type="text" name="currentstatus" class="form-control" readonly value="<?=$affibe['ibestatus'];?>"><input type="hidden" name="ibefrom" value="<?=$affibe['affibestatusoid'];?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-4 text-left">Change Status to</label>
                    <div class="col-lg-6">
                        <select name="statusto" class="form-control chzn-select" tabindex="7">
                            <?php
								$statusto = array(8);
								foreach($statusto as $status){
									$stmt = $db->query("SELECT `affiliatehotelstatusoid`, `status` FROM `affiliatehotelstatus` as `as` where `as`.`affiliatehotelstatusoid` = '".$status."' and `as`.`publishedoid` = '1'");
									$sm = $stmt->fetch(PDO::FETCH_ASSOC);
									if($affsm['affiliatehotelstatusoid'] == 14){
										$sm['affiliatehotelstatusoid'] = 15;
									}
							?>
							<option value="<?=$sm['affiliatehotelstatusoid'];?>"><?=$sm['status'];?></option>
                            <?php
								}
							?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
			<?php
			foreach($statusto as $status){
				$stmt = $db->query("SELECT `affiliatehotelstatusoid`, `status` FROM `affiliatehotelstatus` as `as` where `as`.`affiliatehotelstatusoid` = '".$status."' and `as`.`publishedoid` = '1'");
				$sm = $stmt->fetch(PDO::FETCH_ASSOC);
			?>
            <div class="box dark box-detail-status" id="box-status-<?=$sm['affiliatehotelstatusoid'];?>">
                <header>
                    <h5><?=$sm['status'];?></h5>
                    <!-- .toolbar -->
                    <div class="toolbar">
                      <nav style="padding: 8px;">
                          <a href="javascript:;" class="btn btn-default btn-xs collapse-box">
                              <i class="fa fa-minus"></i>
                          </a>
                          <a href="javascript:;" class="btn btn-default btn-xs full-box">
                              <i class="fa fa-expand"></i>
                          </a>
                      </nav>
                    </div>            <!-- /.toolbar -->
                </header>
                <?php include ('../page/status-modal/form/form-sm-'.$status.'.php'); ?>
			</div>
			<?php
			}
            ?>
        </div>
    </div>
    <div class="col-md-6">
        <?php include('email/email-internal.php'); ?>
    </div>
</div>
