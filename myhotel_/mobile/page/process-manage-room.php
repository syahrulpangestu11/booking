<?php
try{
  $channellist = array(1);
  $startdate = date("Y-m-d", strtotime($_POST['startdate']));
	$enddate = date("Y-m-d", strtotime($_POST['enddate']));

	$stmt = $db->query("SELECT DATEDIFF('".$enddate."','".$startdate."') AS DiffDate");
	$r_datediff = $stmt->fetchAll(PDO::FETCH_ASSOC);
	foreach($r_datediff as $row){
		$diff = $row['DiffDate'];
	}

  $allotment = $_POST['allotment'];
  $double = $_POST['double'];
  $cta = (isset($_POST['cta']) and !empty($_POST['cta']))  ? $_POST['cta'] :"n";
  $ctd = (isset($_POST['ctd']) and !empty($_POST['ctd']))  ? $_POST['ctd'] :"n";

  if(!isset($_POST['cta']) and !isset($_POST['ctd'])){
    $elementName = array("netdouble", "double");
    $elementNodes = array($double, $double);
  }else{
    $elementName = array("netdouble", "double", "cta", "ctd");
    $elementNodes = array($double, $double, $cta, $ctd);
  }

  $roomoid = $_POST['roomoid'];
  $roomofferoid = $_POST['roomofferoid'];

  $q_roomtype = "select r.roomoid, r.name, r.publishedoid, h.hoteloid, h.hotelcode from room r inner join published p using (publishedoid) inner join hotel h using (hoteloid) where r.roomoid = '".$roomoid."' and p.publishedoid not in (3)";
  $stmt = $db->query($q_roomtype);
  $roomtype = $stmt->fetch(PDO::FETCH_ASSOC);

  $hcode = $roomtype['hotelcode'];
  $hoteloid = $roomtype['hoteloid'];
  $roomoid = $roomtype['roomoid'];

  include('includes/function-xml.php');
  $path_xml_hotel = 'data-xml/'.$hcode.'.xml';

  if(!file_exists($path_xml_hotel)){ createXML($path_xml_hotel); }

  $xml = new DomDocument();
      $xml->preserveWhitespace = false;
      $xml->load($path_xml_hotel);
  $xpath = new DomXpath($xml);

  $root = $xml->documentElement;

  /*------------------------------------------------------------------------------------*/
  $query_tag_hotel = '//hotel[@id="'.$hoteloid.'"]';
  $query_tag_room = $query_tag_hotel.'//masterroom[@id="'.$roomoid.'"]';
  /*------------------------------------------------------------------------------------*/

	/* create <hotel id=@>---------------------------------------------------------*/
	$check_tag_hotel = $xpath->query($query_tag_hotel);
    if($check_tag_hotel->length == 0){
	    $hotel = $xml->createElement("hotel");
	        $hotel_id = $xml->createAttribute("id");
	        $hotel_id->value = $hoteloid;
	    $hotel->appendChild($hotel_id);
	    $root->appendChild($hotel);
		$check_tag_hotel = $xpath->query($query_tag_hotel);
    }

	/* create <hotel id=@><masterroom id=@>-----------------------------------------*/
    $check_tag_room = $xpath->query($query_tag_room);
    if($check_tag_room->length == 0){
	    $room = $xml->createElement("masterroom");
	        $room_id = $xml->createAttribute("id");
	        $room_id->value = $roomoid;
	    $room->appendChild($room_id);

	    $tag_hotel = $check_tag_hotel->item(0);
	    $tag_hotel->appendChild($room);
		$check_tag_room = $xpath->query($query_tag_room);
    }

  for($i=0; $i<=$diff; $i++){
  	$dateFormat = explode(" ",date("D Y-m-d",strtotime($startdate." +".$i." day")));
  	$day = $dateFormat[0];
  	$date = $dateFormat[1];

    $query_tag_rate = $query_tag_room.'/rate[@date="'.$date.'"]';
  	$check_tag_rate = $xpath->query($query_tag_rate);

    if($check_tag_rate->length == 0){
      $rate = $xml->createElement("rate");
        $rate_date = $xml->createAttribute("date");
        $rate_date->value = $date;
      $rate->appendChild($rate_date);
        $rate_day = $xml->createAttribute("day");
        $rate_day->value = $day;
      $rate->appendChild($rate_day);

      $tag_room = $check_tag_room->item(0);
      $tag_room->appendChild($rate);
      $check_tag_rate = $xpath->query($query_tag_rate);
    }

    $tag_rate = $check_tag_rate->item(0);

    /*-----------------------------------------------------------------------------*/

    if(isset($_POST['allotment'])){

      $query_tag_allotment = $query_tag_rate.'/allotment';
      $check_tag_allotment = $xpath->query($query_tag_allotment);

      if($check_tag_allotment->length == 0){
        $tag_allotment = $xml->createElement("allotment");
        $tag_rate->appendChild($tag_allotment);

        $check_tag_allotment = $xpath->query($query_tag_allotment);
      }

      $tag_allotment = $check_tag_allotment->item(0);

      foreach($channellist as $channeltype){
        $query_tag_channel = $query_tag_allotment.'/channel[@type="'.$channeltype.'"]';
        $check_tag_channel = $xpath->query($query_tag_channel);

        $channel = $xml->createElement("channel");
          $channel_type = $xml->createAttribute("type");
          $channel_type->value = $channeltype;
          $channel->appendChild($channel_type);
        $channel_allotment = $xml->createTextNode($allotment);
          $channel->appendChild($channel_allotment);

        if($check_tag_channel->length == 0){
          $tag_allotment->appendChild($channel);
        }else{
          $tag_element = $check_tag_channel->item(0);
          $tag_allotment->replaceChild($channel,$tag_element);
        }
      }

    }

    /*-----------------------------------------------------------------------------*/

    $query_tag_rateplan = $query_tag_rate.'/rateplan[@id="'.$roomofferoid.'"]';
    $check_tag_rateplan = $xpath->query($query_tag_rateplan);

    if($check_tag_rateplan->length == 0){
      $rateplan = $xml->createElement("rateplan");
        $rateplan_type = $xml->createAttribute("id");
        $rateplan_type->value = $roomofferoid;
      $rateplan->appendChild($rateplan_type);

      $tag_rate->appendChild($rateplan);

      $check_tag_rateplan = $xpath->query($query_tag_rateplan);
    }


    foreach($channellist as $channeltype){

      $query_tag_channel = $query_tag_rateplan.'/channel[@type="'.$channeltype.'"]';
      $check_tag_channel = $xpath->query($query_tag_channel);

      if($check_tag_channel->length == 0){
        $channel = $xml->createElement("channel");
          $channel_type = $xml->createAttribute("type");
          $channel_type->value = $channeltype;
        $channel->appendChild($channel_type);

        $tag_channel = $check_tag_rateplan->item(0);
        $tag_channel->appendChild($channel);

        $check_tag_channel = $xpath->query($query_tag_channel);
        $tag_channel = $check_tag_channel->item(0);

        foreach($elementName as $key => $value){
          $element = $value;
          $node = $elementNodes[$key];

          $createElement = $xml->createElement($element);
          $createNode = $xml->createTextNode($node);
          $createElement->appendChild($createNode);

          $tag_channel->appendChild($createElement);
        }
      }else{
        foreach($elementName as $key => $value){
          $element = $value;
          $node = $elementNodes[$key];

          $createElement = $xml->createElement($element);
          $createNode = $xml->createTextNode($node);
          $createElement->appendChild($createNode);

          $check_tag_element = $xpath->query($query_tag_channel.'/'.$element);
          $tag_channel = $check_tag_channel->item(0);

          if($check_tag_element->length == 0){
            $tag_channel->appendChild($createElement);
          }else{
            $tag_element = $check_tag_element->item(0);
            $tag_channel->replaceChild($createElement,$tag_element);
          }
        }
      }
    } // for each channel

  }

  $xml->formatOutput = true;
  $xml->save($path_xml_hotel) or die("Error");
?>
<script type="text/javascript">
  $(function(){
    $(document).ready(function(){
      $('div#notification-manage-rate').find('div.modal-body').html("Data successfully updated");
      $('div#notification-manage-rate').modal('show');
    });
  });
</script>
<?php
}catch(PDOException $ex) {
?>
<script type="text/javascript">
  $(function(){
    $(document).ready(function(){
      $('div#notification-manage-rate').find('div.modal-body').html("Data failed to update");
      $('div#notification-manage-rate').modal('show');
    });
  });
</script>
<?php
}
?>

<div id="notification-manage-rate" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Modal title</h4>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" act="ok">OK</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
