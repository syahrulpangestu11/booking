<?php
session_start();
date_default_timezone_set("Asia/Makassar");
header("Content-Type: application/xls");
header("Content-Disposition: attachment; filename=ReservationList_".date("YmdHis").".xls");
header("Pragma: no-cache");
header("Expires: 0");

if(empty($_SESSION['_oid'])){
    die();
}

include("../../conf/connection.php");
include("../../includes/pms-report/function_report.php");

echo '<table border="1">';
echo '<tr>
    <th>#</th>
    <th>Res ID</th>
    <th>Date</th>
    <th>Guest Name</th>
    <th>Stay Duration</th>
    <th>Room# / Type</th>
    <th>Pax</th>
    <th>Status</th>
    <th>Amount</th>
    <th>Booking Market</th>
    <th>Agent</th>
</tr>';

$hoteloid = $_POST['hoteloid'];
$_typeusr = $_SESSION['_typeusr'];
$_oid = $_SESSION['_oid'];

$titles = getCustTitle($db);
if($hoteloid > 0){
    $data = getReservationData($db, $hoteloid);
}else{
    if($_typeusr == "4"){
		$s_chain = "SELECT ua.oid FROM users u INNER JOIN userassign ua USING (useroid) WHERE ua.type = 'chainoid' AND u.useroid = '".$_oid."'";
		$q_chain = $db->query($s_chain);
		$r_chain = $q_chain->fetchAll(PDO::FETCH_ASSOC);
		foreach($r_chain as $row){
			$chainoid = $row['oid'];
		}
		$data = getReservationDataChain($db, $chainoid);
	}else{
        $data = array();
	}
}
$no=0;
foreach($data as $row){
    $no++;
    $bookingoid = $row['bookingoid'];
    $bookingnum = $row['bookingnumber'];
    if(isset($titles[$row['title']])) $title = $titles[$row['title']]; else $title = $row['title'];
    $guestname = $title." ".$row['firstname']." ".$row['lastname'];
    $checkin = date("d M y", strtotime($row['checkin']));
    $checkout = date("d M y", strtotime($row['checkout']));
    $diff = $row['diff'];
    $roomnumber = empty($row['roomnumber']) ? "-" : $row['roomnumber'];
    $room = $row['room'];
    $adult = $row['adult'];
    $child = $row['child'];
    if($row['bookingstatusoid'] == "5"){
        $status = "Cancelled";
    }else if($row['bookingstatusoid'] == "7"){
        $status = "No Show";
    }else{
        $status = $row['status'];
    }
    $grandtotal = number_format($row['grandtotal'], 2, ',', '.');
    $currcode = empty($row['currencycode']) ? "IDR" : $row['currencycode'];
    $market = empty($row['bm_name']) ? empty($row['bm_name1']) ? "-" : $row['bm_name1'] : $row['bm_name'];
    $bookingdate = date("d M y", strtotime($row['bookingtime']));
    $agentname = empty($row['agentname']) ? "-" : $row['agentname'];
    $agenttypeoid = empty($row['agenttypeoid']) ? "0" : $row['agenttypeoid'];

    ?>
    <tr>
        <td><?=$no?></td>
        <td><?=$bookingnum?></td>
        <td><?=$bookingdate?></td>
        <td><?=$guestname?></td>
        <td><?=$checkin." - ".$checkout." (".$diff.")"?></td>
        <td><?=$roomnumber."/".$room?></td>
        <td><?=$adult?>(A) <?=$child?>(C)</td>
        <td><?=$status?></td>
        <td><?=$currcode." ".$grandtotal?></td>
        <td><?=$market?></td>
        <td><?=$agentname?></td>
    </tr>
    <?php

}
echo '</table>';
?>
