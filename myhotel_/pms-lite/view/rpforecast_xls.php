<?php
session_start();
date_default_timezone_set("Asia/Makassar");
header("Content-Type: application/xls");
header("Content-Disposition: attachment; filename=ForecastReport_".date("YmdHis").".xls");
header("Pragma: no-cache");
header("Expires: 0");

if(empty($_SESSION['_oid'])){
    die();
}


  include("../../conf/connection.php");
  include("../../includes/php-function.php");
  include("../../includes/pms-lite/class-pms-lite.php");

   $pmslite = new PMSLite($db);

  $startdate = date('Y-m-d', strtotime($_POST['startdate']));
  $enddate = date('Y-m-d', strtotime($_POST['enddate']));

  $pmslite->setPeriodePMS($startdate, $enddate);

  $s_hotelchain = "select h.hoteloid, h.hotelcode, h.hotelname from hotel h inner join chain c using (chainoid) where h.chainoid in (select oid from userassign where type = 'chainoid' and useroid = '".$_SESSION['_oid']."') and h.publishedoid = '1'";
  $filter = array();
  if(!empty($_POST['hotelcode'])){
    array_push($filter, "h.hotelcode = '".$_POST['hotelcode']."'");
  }
  if(count($filter) > 0){
		$combine_filter = implode(' and ',$filter);
		$s_hotelchain = $s_hotelchain.' and '.$combine_filter;
	}
  $q_hotelchain = $db->query($s_hotelchain);
  $r_hotelchain = $q_hotelchain->fetchAll(PDO::FETCH_ASSOC);
  foreach($r_hotelchain as $hotelchain){
    $pmslite->setPMSHotel($hotelchain['hoteloid']);
?>
  <table id="inventory" border="1" cellpadding="0" cellspacing="0" style="margin-bottom:20px;">
    <thead>
      <tr>
        <th rowspan="2">Date</th>
        <?php
        $roomTypeTable = $pmslite->roomTypeTable();
        foreach($roomTypeTable as $roomtype){
            $roomallocation[$roomtype['id']] = $pmslite->roomAllocation($roomtype['id']);
            $bookroom[$roomtype['id']] = $pmslite->dataBookedRoom($roomtype['id'], "booked");
            $roomrevenue[$roomtype['id']] = $pmslite->dataInhouseRevenueUpdated($roomtype['id']);
        ?>
        <th colspan="5"><?=$roomtype['name']?></th>
        <?php
        }
        ?>
        <th colspan="3">Total Occupancy</th>
        <th colspan="2">Total Revenue</th>
      </tr>
      <tr>
        <?php foreach($roomTypeTable as $roomtype){ ?>
        <th>Allocated</th>
        <th>Booked</th>
        <th>Booked%</th>
        <th>IDR</th>
        <th>USD</th>
        <?php } ?>
        <th>Allocated</th>
        <th>Booked</th>
        <th>Booked%</th>
        <th>IDR</th>
        <th>USD</th>
      </tr>
    </thead>
    <tbody>
      <?php
      for($i = 0; $i <= $pmslite->diffdate; $i++){
      	$date = date("Y-m-d",strtotime($startdate." +".$i." day"));
      	$dateformated = date("d M y",strtotime($startdate." +".$i." day"));
        $daily_idr_revenue = 0;
        $daily_usd_revenue = 0;
        $daily_allocation = 0;
        $daily_booked = 0;
        $daily_available = 0;
      ?>
        <tr>
          <td><?=$dateformated?></td>
          <?php
            foreach($roomTypeTable as $roomtype){
              $daily_idr_revenue+=$roomrevenue[$roomtype['id']][$i]['total_idr'];
              $daily_usd_revenue+=$roomrevenue[$roomtype['id']][$i]['total_usd'];
              //$availableroom = $roomallocation[$roomtype['id']]-$bookroom[$roomtype['id']][$i];

              $daily_allocation+=$roomallocation[$roomtype['id']];
              $daily_booked+=$bookroom[$roomtype['id']][$i];
              //$daily_available+=$availableroom;

              $sum_roomallocation[$roomtype['id']]+=$roomallocation[$roomtype['id']];
              $sum_bookroom[$roomtype['id']]+=$bookroom[$roomtype['id']][$i];
              //$sum_availableroom[$roomtype['id']]+=$availableroom;
              $sum_idr_revenue[$roomtype['id']]+=$roomrevenue[$roomtype['id']][$i]['total_idr'];
              $sum_usd_revenue[$roomtype['id']]+=$roomrevenue[$roomtype['id']][$i]['total_usd'];

              $bookroom_pc = ceil(($bookroom[$roomtype['id']][$i] / $roomallocation[$roomtype['id']])  * 100);
          ?>
          <td align="center"><?=$roomallocation[$roomtype['id']]?></td>
          <td align="center"><?=$bookroom[$roomtype['id']][$i]?></td>
          <td align="center"><?=round($bookroom_pc,2)?>%</td>
          <td align="right"><?=number_format($roomrevenue[$roomtype['id']][$i]['total_idr'], 2, ',', '.')?></td>
          <td align="right"><?=number_format($roomrevenue[$roomtype['id']][$i]['total_usd'], 2, ',', '.')?></td>
          <?php
            }
            $sum_idr_hotelrevenue+=$daily_idr_revenue;
            $sum_usd_hotelrevenue+=$daily_usd_revenue;
            $sum_allocation+=$daily_allocation;
            $sum_booked+=$daily_booked;
            //$sum_available+=$daily_available;
            $daily_booked_pc = ceil(($daily_booked / $daily_allocation)  * 100);
          ?>
          <td align="center"><?=$daily_allocation?></td>
          <td align="center"><?=$daily_booked?></td>
          <td align="center"><?=round($daily_booked_pc,2)?>%</td>
          <td align="right"><?=number_format($daily_idr_revenue, 2, ',', '.')?></td>
          <td align="right"><?=number_format($daily_usd_revenue, 2, ',', '.')?></td>
        </tr>
      <?php
      }
      ?>
    </tbody>
    <tfoot>
      <tr>
        <td>Total</td>
        <?php foreach($roomTypeTable as $roomtype){
          $sum_bookroom_pc = ceil(($sum_bookroom[$roomtype['id']] / $sum_roomallocation[$roomtype['id']]) * 100);
        ?>
        <td align="center"><?=$sum_roomallocation[$roomtype['id']]?></td>
        <td align="center"><?=$sum_bookroom[$roomtype['id']]?></td>
        <td align="center"><?=round($sum_bookroom_pc, 2)?>%</td>
        <td align="right"><?=number_format($sum_idr_revenue[$roomtype['id']], 2, ',', '.')?></td>
        <td align="right"><?=number_format($sum_usd_revenue[$roomtype['id']], 2, ',', '.')?></td>
        <?php }
          $sum_booked_pc = ceil(($sum_booked / $sum_allocation) * 100);
        ?>
        <td align="center"><?=$sum_allocation?></td>
        <td align="center"><?=$sum_booked?></td>
        <td align="center"><?=round($sum_booked_pc, 2)?>%</td>
        <td align="right"><?=number_format($sum_idr_hotelrevenue, 2, ',', '.')?></td>
        <td align="right"><?=number_format($sum_usd_hotelrevenue, 2, ',', '.')?></td>
      </tr>
    </tfoot>
  </table>
<?php
  }
?>
