<?php
  include('../../conf/connection.php');
  $stmt = $db->prepare("select c.custoid, c.title, c.firstname, c.lastname, CONCAT(firstname, ' ', lastname) as guestname, c.email, c.phone, c.city, c.countryoid from customer c inner join booking b using (custoid) inner join hotel h using (hoteloid) where CONCAT(c.firstname,' ', c.lastname) like :c and (b.hoteloid = :a or h.chainoid = (select chainoid from hotel where hoteloid = :b))");
  $stmt->execute(array(':a' => $_REQUEST['hotel'], ':b' => $_REQUEST['hotel'], ':c' => '%'.$_REQUEST['firstname'].'%'));
  $fetch = $stmt->fetchAll(PDO::FETCH_ASSOC);

  foreach($fetch as $row){
    $result['value']	= htmlentities($row['guestname']);
    $result['id']		= $row['custoid'];
    $result['title']		= $row['title'];
    $result['firstname']		= $row['firstname'];
    $result['lastname']		= $row['lastname'];
    $result['email']		= $row['email'];
    $result['phone']		= $row['phone'];
    $result['city']		= $row['city'];
    $result['country']		= $row['countryoid'];
    $row_set[]			= $result;
  }

  echo json_encode($row_set);
?>
