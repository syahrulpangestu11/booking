<?php
session_start();
ob_start();
require_once('../../lib/tcpdf/tcpdf.php');
include('../../conf/connection.php');
include("../../includes/pms-lite/class-pms-lite.php");

$hoteloid = $_POST['hotel'];
$bookingroomdtloid = $_POST['bookingroomdtl'];

$pmslite = new PMSLite($db);
$pmslite->setPMSHotel($hoteloid);
$data = $pmslite->getBookingRoom($bookingroomdtloid);
/*--------------------------------------*/
$fileVersion = date("Y.m.d.H.i.s");

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('PMS Lite Powered by TheBuking');
$pdf->SetTitle('Reservation Card '.$data['bookingnumber'].' - '.$data['roomnumber']);
$pdf->SetSubject('Reservation Card '.$data['bookingnumber']." - ".$data['roomnumber']);
$pdf->SetKeywords('Reservation Card', $data['bookingnumber']);

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set margins
$pdf->SetMargins(8, 10, 8, true);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

$pdf->SetFont('dejavusans', '', 8);

$pdf->AddPage();

$html = '
    <table width="100%" border="0">
        <tr>
            <td>&nbsp;</td>
            <td>
              <h2>'.$pmslite->hoteldata['hotelname'].'</h2>
              '.$pmslite->hoteldata['address'].'<br>Phone : '.$pmslite->hoteldata['phone'].'<br>E-mail : '.$pmslite->hoteldata['email'].'<br>Website : '.$pmslite->hoteldata['website'].'
            </td>
        </tr>
    </table><br><br>
    <table width="100%" border="0">
        <tr>
            <td><h3>Check-in Card</h3></td>
            <td><h3>Reservation Number # '.$data['bookingnumber'].'</h3></td>
        </tr>
    </table><br><br>
    <table width="100%" style="border:1px solid black; padding:5px;">
        <tr>
            <td><h3>Room Type : '.$data['room'].'</h3></td>
            <td><h3>Room Number : '.$data['roomnumber'].'</h3></td>
        </tr>
    </table><br><br>
    <table width="100%" border="1" style="padding:3px;">
      <tr>
        <td width="50%">Guest : <b>'.$data['ownername'].'</b><br><br>';
        if(!empty($data['guestname'])){
          $html.= 'Guest Room Detail : <b>'.$data['guestname'].'</b>';
        }else{
          $html.= '<b><i>Guest detail not filled.<i></b>';
        }
$html.= '
      </td>
      <td width="50%">
        <table width="100%" border="0">
          <tr><td>Date(s)</td><td>: <b>'.date('d M Y', strtotime($data['checkin'])).' - '.date('d M Y', strtotime($data['checkout'])).'</b></td></tr>
          <tr><td>Night(s)</td><td>: <b>'.$data['night'].'</b></td></tr>
          <tr><td>Person(s)</td><td>: <b>'.$data['adult'].' adult &amp; '.$data['child'].' child</b></td></tr>
          <tr><td colspan="2"><br><br><table width="100%" style="border:1px solid black; padding:2px;">';
          if($_POST['show'] == "hide-price"){
            $html.= '<tr><td align="center"><br><br><b>Guest Booking Price</b><br><br></td></tr>';
          }else{
            $html.= '
            <tr><td>Booking Charges<br><i>(Seasonal Rate)</i></td><td align="right"><b>'.$data['currency'].' '.number_format($data['total']).'</b></td></tr>
            <tr><td>Total Tax</td><td align="right"><b>-</b></td></tr>
            <tr><td><b>Total Amount</b></td><td align="right"><b>'.$data['currency'].' '.number_format($data['total']).'</b></td></tr>
            <tr><td><b>Amount Paid</b></td><td align="right"><b>'.$data['currency'].' '.number_format($data['deposit']).'</b></td></tr>
            <tr><td><b>Balance</b></td><td align="right"><b>'.$data['currency'].' '.number_format($data['balance']).'</b></td></tr>';
        }
$html.= '
          </table>
        </td></tr>
      </table>
    </td>
    </tr>
  </table>
  <br /><br /><br />
  <table width="100%" border="0" style="padding:5px;"><tr><td><b>Date : </b>'.date('d M Y').'</td><td align="center">Signature</td></tr></table>';
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->lastPage();
// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('ReservationCard_'.$data['bookingnumber'].'_'.$data['roomnumber'].'.pdf', 'I');
?>
