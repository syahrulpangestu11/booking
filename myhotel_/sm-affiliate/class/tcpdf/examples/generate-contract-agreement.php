<?php
$hotelname = "nama hotel";

require_once('tcpdf_include.php');

class MYPDF extends TCPDF {

    //Page header
    public function Header() {
		if($this->getAliasNumPage() > 1){
			// Logo
			$image_file = K_PATH_IMAGES.'logo_example.jpg';
			$this->Image($image_file, 10, 10, 15, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
			// Set font
			$this->SetFont('helvetica', 'B', 20);
			// Title
			$this->Cell(0, 15, '<< TCPDF Example 003 >>', 0, false, 'C', 0, '', 0, false, 'M', 'M');
		}
    }

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', '', 9);
        // Page number
        $this->Cell(20, 5, $this->getAliasNumPage().' | page', 'R', false, 'L', 0, '', 0, false, 'T', 'M');
		$this->SetFont('helvetica', 'I', 9);
		$this->Cell(0, 5, 'Ika Nursila Dewi | ika@thebuking.com', 0, false, 'R', 0, '', 0, false, 'T', 'M');
    }
}

$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Ika Nursila Dewi | ika@thebuking.com');
$pdf->SetTitle('Agreement The Buking Booking Enginee License');
$pdf->SetSubject('Agreement The B&uuml;king');
$pdf->SetKeywords('agreement, the b&uuml;king, booking enginee, license, agreement license');

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

$pdf->SetFont('dejavusans', '', 10);

$pdf->AddPage();

$html = '<div style="text-align:center">
<h2>Agreement</h2>
<h1>the b&uuking</h1>
<h2>Booking License</h2>
</div>
<div style="text-align:center; font-size:11px;">By Ika Nursila Dewi</div>
<div style="text-align:center; font-size:11px;">
------------------------------------------------------------------------------------------------------------------------<br>
Sales & Purchase Agreement is intended solely for the use of the '.$hotelname.' and is not intended for any other purposes. The information contained in this proposal is confidential and must not be reproduced and/or disclosed to others without the prior written permission of PT WeSolve Informatika
</div>';
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->lastPage();

$pdf->AddPage();

$html = '<div style="text-align:justify;">
<h2 style="text-align:center;">Agreement</h2>
<h4 style="padding-bottom:10px; margin-bottom:10px; text-align:center;">The Büking License</h4>
This Sale & Purchase Agreement is made and signed in _{tempat}___ on _{tanggal agreement}_, by and between:
<ol>
<li>Mrs. Ika Nursila Dewi, in this case acted as Director of Sales is located at Cervino Village 27 Floor, Jalan KH. Abdullah Syafie kav.27, Jakarta, with National Identity Card 5171034101840006, (hereinafter referred to as Seller/Licensor).</li>
<li>_{nama pic}_, private sector employees, in this case acts as the {title of pic} of  _{nama hotel}_______ located at _{alamat hotel}_ ‐ Indonesia, (hereinafter referred to as Buyer).</li>
</ol>
<h4 style="padding-bottom:10px; margin-bottom:10px; text-align:center;">Weigh</h4>
<ol type="A">
<li>That the seller is the legal owner of a tailor made web-based application, which is named “The Büking”, and Seller intends to make it and sell it with a system customized to the buyer.</li>
<li>That the Purchaser intends to purchase an application that has been customized according to buyers needs to be a means of The Büking License Booking Engine.</li>
</ol>
So, therefore, based on agreements and principles, the Parties hereby agree to make the Sale and Purchase Agreement is subject to the terms and conditions below (hereinafter called the Contract):
<h4 style="text-align:center;">Article 1<br>Definitions</h4>
<ul>
<li>1.1.	Under this contract, the following terms shall have the following meanings, unless otherwise specified:</li>
</ul>
</div>
';
$pdf->writeHTML($html, true, false, true, false, '');
// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output(date('Y-m-d').'_agreement.pdf', 'I');
?>