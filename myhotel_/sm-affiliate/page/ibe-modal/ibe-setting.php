<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <label class="control-label col-lg-4 text-left">Current Status</label>
                    <div class="col-lg-6">
                        <input type="text" name="currentstatus" class="form-control" readonly value="<?=$affibe['ibestatus'];?>"><input type="hidden" name="ibefrom" value="<?=$affibe['affibestatusoid'];?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-4 text-left">Change Status to</label>
                    <div class="col-lg-6">
                        <select name="ibeto" class="form-control chzn-select" tabindex="7">
                            <?php 
								$statusto = array(2);
								$status = implode(',', $statusto);
								$stmt = $db->query("SELECT `affibestatusoid`, `ibestatus` FROM `affibestatus` as `ai` where `ai`.`affibestatusoid` in (".$status.") and `ai`.`publishedoid` = '1'");
								$list_ibe = $stmt->fetchAll(PDO::FETCH_ASSOC);
								foreach($list_ibe as $ibe){
							?>
							<option value="<?=$ibe['affibestatusoid'];?>"><?=$ibe['ibestatus'];?></option>
                            <?php
								}
							?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
			<?php 
				foreach($list_ibe as $ibe){
			?>
            <div class="box dark box-detail-ibe" id="box-ibe-<?=$ibe['affibestatusoid'];?>">
                <header>
                    <h5><?=$ibe['ibestatus'];?></h5>
                    <!-- .toolbar -->
                    <div class="toolbar">
                      <nav style="padding: 8px;">
                          <a href="javascript:;" class="btn btn-default btn-xs collapse-box">
                              <i class="fa fa-minus"></i>
                          </a>
                          <a href="javascript:;" class="btn btn-default btn-xs full-box">
                              <i class="fa fa-expand"></i>
                          </a>
                      </nav>
                    </div>            <!-- /.toolbar -->
                </header>
                <?php include ('form/form-ibe-'.$status.'.php'); ?>
			</div>               
			<?php
			}
            ?>
        </div>
    </div>
</div>