<section class="content-header">
    <h1>WDM Stage</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-bullhorn"></i>  Sales &amp; Marketing</a></li>
        <li class="active">WDM Stage</li>
    </ol>
</section>
<section class="content">
  <div id="sm-content">
    <div id="dashboard" class="row">
      <div class="row">
      <?php
      $num = 0;
      $stmt = $db->query("SELECT * FROM `affwdmstatus` as `aw` where `aw`.`publishedoid` = '1' and aw.affwdmstatusoid not in ('6')");
      $list_affwdmstatus = $stmt->fetchAll(PDO::FETCH_ASSOC);
      foreach($list_affwdmstatus as $affwdm){
        $num++;
        if($num % 3 == 1){ echo'</div><div class="row">'; }
      ?>
        <div class="col-md-4 col-xs-12">
            <div class="box inverse status-<?=$affwdm['affwdmstatusoid'];?>">
                <header>
                    <div class="icons"><i class="fa fa-th-large"></i></div>
                    <h5><?=$affwdm['wdmstatus'];?></h5>
                    <!-- .toolbar -->
                    <div class="toolbar">
                      <nav style="padding: 8px;">
                      <input type="text" name="keyword" />
                      <button type="button" class="btn btn-xs btn-default" name="searchwdmstage" type-status="wdm-status" value="<?=$affwdm['affwdmstatusoid'];?>"><i class="fa fa-search"></i></button>
                      </nav>
                    </div>
                    <!-- /.toolbar -->
                </header>
                <div id="div-2" class="body collapse in">
                    <?php showDataWDM($affwdm['affwdmstatusoid'], '#wdm-Modal'); ?>
                </div>
            </div>
        </div>
      <?php
      }
      ?>
      </div>
    </div>
  </div>
</section>
