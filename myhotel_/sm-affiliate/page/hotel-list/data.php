<?php
	error_reporting(E_ALL & ~E_NOTICE);
	include("../../../conf/connection.php");
	include("../../../includes/paging/pre-paging.php");
?>
<table id="dataTable" class="table promo-table">
    <tbody>
        <tr class="text-center">
            <td>No.</td>
            <td>Name</td>
						<td>Chain</td>
            <td>Type</td>
						<?php if($_REQUEST['smstatus'] != 1){ ?>
            <td>Note</td>
            <td>SM Status</td>
						<td>IBE Status</td>
						<td>WDM Status</td>
						<?php }else{ ?>
						<td>Address</td>
						<td>City</td>
						<td>SM Status</td>
						<?php } ?>
        </tr>
    <?php
        $main_query = "select h.*, ht.category as hoteltype, ct.cityname, ahs.status as smstatus, h.affiliatehotelstatusoid,  ibe.ibestatus, ibe.affibestatusoid, wdm.wdmstatus, wdm.affwdmstatusoid, case when ch.name = 'No Chain' then '' else ch.name end as chainname from hotel h inner join city ct using (cityoid) left join chain ch using (chainoid) inner join hoteltype ht using (hoteltypeoid) inner join affiliatehotelstatus ahs using (affiliatehotelstatusoid) left join affibestatus ibe using (affibestatusoid) left join affwdmstatus wdm using (affwdmstatusoid)";

		    $filter = array();
				if(isset($_REQUEST['name']) and $_REQUEST['name']!=''){
					array_push($filter, 'h.hotelname like "%'.$_REQUEST['name'].'%"');
				}
				if(isset($_REQUEST['type']) and $_REQUEST['type']!=''){
					array_push($filter, 'h.hoteltypeoid = "'.$_REQUEST['type'].'"');
				}
				if(!empty($_REQUEST['smstatus'])){
					$smstatus = $_REQUEST['smstatus'];
					if($smstatus == 1){
						$smstatus = '0, 1';
					}else if($smstatus == 6){
						$smstatus = '6, 14';
					}else if($smstatus == 7){
						$smstatus = '7, 14, 15';
					}
					array_push($filter, "h.affiliatehotelstatusoid in (".$smstatus.")");
				}
				if(!empty($_REQUEST['wdmstatus'])){
					$wdmstatus = $_REQUEST['wdmstatus'];
					array_push($filter, "h.affwdmstatusoid in ('".$wdmstatus."')");
				}
				if(!empty($_REQUEST['ibestatus'])){
					$ibestatus = $_REQUEST['ibestatus'];
					array_push($filter, "h.affibestatusoid in ('".$ibestatus."')");
				}

				if(isset($filter_not_in) and !empty($filter_not_in)){
					array_push($filter, $filter_not_in);
				}

        if(count($filter) > 0){
            $combine_filter = implode(' and ',$filter);
            $query = $main_query.' where '.$combine_filter;
        }else{
            $query = $main_query;
        }

        $no = 0;
				$stmt = $db->query($query.$paging_query);
        foreach($stmt->fetchAll(PDO::FETCH_ASSOC) as $row){
    ?>
        <tr oid="<?php echo $row['hoteloid']; ?>">
            <td class="sorting_1"><?php echo ++$num; ?></td>
            <td><?php echo $row['hotelname']; ?></td>
						<td style="font-size:0.9em;"><?php echo $row['chainname']; ?></td>
            <td style="font-size:0.8em;"><?php echo $row['hoteltype']; if($row['stars'] > 0){ echo " ".$row['stars']."<i class='fa fa-star'></i>"; } ?></td>

						<?php if($_REQUEST['smstatus'] == 1){ ?>

						<td><?php echo $row['address']; ?></td>
						<td><?php echo $row['cityname']; ?></td>
						<td class="text-center">
							<span class="label label-primary"><?php echo $row['smstatus']; ?></span>
							<button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#sm-Modal" data-id="<?php echo $row['hoteloid']; ?>" data-status="<?php echo $row['affiliatehotelstatusoid']; ?>">Change</button>
						</td>
						<?php }else{ ?>
            <td style="font-size:0.9em;">
							<?php
							$stmt2 = $db->prepare("select al.affiliatelogoid, al.startdate as logtime, al.note from affiliatelog al inner join hotel h using (hoteloid) where to_type = :a and to_status = :b and al.hoteloid = :c order by al.startdate desc limit 1");
							$stmt2->execute(array(':a' => 'sm', ':b' => $row['affiliatehotelstatusoid'], ':c' => $row['hoteloid']));
							$log = $stmt2->fetch(PDO::FETCH_ASSOC);
							$note = $log['note'];
							if($row['affiliatehotelstatusoid'] == 3){
								$stmt3 = $db->prepare("select * from afflogdetail where affiliatelogoid = :a order by detail_date desc");
								$stmt3->execute(array(':a' => $log['affiliatelogoid']));
								foreach($stmt3->fetchAll(PDO::FETCH_ASSOC) as $logdtl){
									$note.= '<br>'.date('dmY', strtotime($logdtl['detail_date']))." - ".$logdtl['detail_note'];
								}
							}

							echo $note."   ".$row['affiliatehotelstatusoid'];
							?>
						</td>
            <td class="text-center">
							<span class="label label-primary"><?php echo $row['smstatus']; ?></span>
							<button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#sm-Modal" data-id="<?php echo $row['hoteloid']; ?>" data-status="<?php echo $row['affiliatehotelstatusoid']; ?>">Change</button>
						</td>
						<td class="text-center">
							<?php if($row['affibestatusoid'] != 0){ ?>
							<span class="label label-primary"><?php echo $row['ibestatus']; ?></span>
							<button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#ibe-Modal" data-id="<?php echo $row['hoteloid']; ?>" data-status="<?php echo $row['affwdmstatusoid']; ?>">Change</button>
							<?php } ?>
						</td>
						<td class="text-center">
							<?php if($row['affwdmstatusoid'] != 0){ ?>
							<span class="label label-primary" ><?php echo $row['wdmstatus']; ?></span>
							<button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#wdm-Modal" data-id="<?php echo $row['hoteloid']; ?>" data-status="<?php echo $row['affibestatusoid']; ?>">Change</button>
							<?php } ?>
						</td>
						<?php } ?>
        </tr>
    <?php
        }
    ?>
    </tbody>
</table>
<?php
	// --- PAGINATION part 2 ---
	$main_query = "select count(h.hoteloid) as jmldata from hotel h";
	if(count($filter) > 0){
		$combine_filter = implode(' and ',$filter);
		$query = $main_query.' where '.$combine_filter;
	}else{
		$query = $main_query;
	}

	$stmt = $db->query($query);
	$row = $stmt->fetch(PDO::FETCH_ASSOC);
	$jmldata = $row['jmldata'];

	$tampildata = $row_count;
	include("../../../includes/paging/post-paging.php");
?>
