<table id="log-status" class="table table-bordered responsive-table">
  <thead>
      <tr><th>No.</th><th>Date</th><th>From</th><th>To</th><th>By</th><th>Note</th><th>File</th><th>Duration</th></tr>
  </thead>
  <tbody>
  <?php
    $no = 0;
    $stmt = $db->prepare("select al.*, x.status as statusfrom, y.status as statusto from affiliatelog al inner join (select ahs.status, ahs.affiliatehotelstatusoid from affiliatehotelstatus ahs) x on x.affiliatehotelstatusoid = al.from_status  inner join (select ahs.status, ahs.affiliatehotelstatusoid from affiliatehotelstatus ahs) y on y.affiliatehotelstatusoid = al.to_status where al.hoteloid = :a and al.to_type = :b order by al.startdate desc");
    $stmt->execute(array(':a' => $hoteloid, ':b' => 'sm'));
    foreach($stmt->fetchAll(PDO::FETCH_ASSOC) as $row){
  ?>
    <tr>
        <td><?php echo ++$no; ?></td>
        <td><?php echo date('d M y H:i', strtotime($row['startdate'])); ?></td>
        <td><?php echo $row['statusfrom']; ?></td>
        <td><?php echo $row['statusto']; ?></td>
        <td><?php echo $row['displayname']; ?></td>
        <td><?php echo $row['note']; ?></td>
        <td class="text-center">
        <?php
          if($row['to_status'] == 5){
            $stmt = $db->prepare("select affhotel_sfoid, sf_file from affhotel_sf where hoteloid = :a");
            $stmt->execute(array(':a' => $hoteloid));
            $result = $stmt->fetch(PDO::FETCH_ASSOC);
            if(!empty($result['sf_file'])){ echo "<a href='".$result['sf_file']."' target='_blank'><i class='fa fa-file-o'></i> SF</a>"; }else{ echo "<a href='#'><i class='fa fa-upload'></i> Upload</a>"; }
          }else if($row['to_status'] == 7){
              $stmt = $db->prepare("select affagreementoid, file_agreement from affagreement where hoteloid = :a");
              $stmt->execute(array(':a' => $hoteloid));
              $result = $stmt->fetch(PDO::FETCH_ASSOC);
              if(!empty($result['file_agreement'])){ echo "<a href='".$result['file_agreement']."' target='_blank'><i class='fa fa-file'></i> PKS</a>"; }else{ echo "<a href='#'><i class='fa fa-upload'></i> Upload</a>"; }
          }
        ?>
        </td>
        <td><?php if(!empty($row['idletime'])){ echo $row['idletime']; }else{ echo knowIdleTime($row['startdate'], date('Y-m-d H:i:s')); } ?></td>
    </tr>
    <?php
        if(in_array($row['to_status'], array(3))){
          include('progress-detail-time.php');
        }
    }
  ?>
  </tbody>
</table>
