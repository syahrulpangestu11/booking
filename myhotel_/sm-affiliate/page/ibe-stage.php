<section class="content-header">
    <h1>IBE Stage</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-bullhorn"></i>  Sales &amp; Marketing</a></li>
        <li class="active">IBE Stage</li>
    </ol>
</section>
<section class="content">
  <div id="sm-content">
    <div id="dashboard" class="row">
      <div class="row">
      <?php
      $num = 0;
      $stmt = $db->query("SELECT * FROM `affibestatus` as `ai` where `ai`.`publishedoid` = '1' and ai.affibestatusoid not in ('5')");
      $list_affhotelstatus = $stmt->fetchAll(PDO::FETCH_ASSOC);
      foreach($list_affhotelstatus as $affhotelstatus){
        $num++;
        if($num % 2 == 1){ echo'</div><div class="row">'; }
      ?>
        <div class="col-md-6 col-xs-12">
            <div class="box inverse status-<?=$affhotelstatus['affibestatusoid'];?>">
                <header>
                    <div class="icons"><i class="fa fa-th-large"></i></div>
                    <h5><?=$affhotelstatus['ibestatus'];?></h5>
                    <!-- .toolbar -->
                    <div class="toolbar">
                      <nav style="padding: 8px;">
                      <input type="text" name="keyword" />
                      <button type="button" class="btn btn-xs btn-default" name="searchibestage" type-status="ibe-status" value="<?=$affhotelstatus['affibestatusoid'];?>"><i class="fa fa-search"></i></button>
                      </nav>
                    </div>
                    <!-- /.toolbar -->
                </header>
                <div id="div-2" class="body collapse in">
                    <?php showDataIBE($affhotelstatus['affibestatusoid'], '#ibe-Modal'); ?>
                </div>
            </div>
        </div>
      <?php
      }
      ?>
      </div>
    </div>
  </div>
</section>
