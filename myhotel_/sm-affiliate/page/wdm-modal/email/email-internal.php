<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active">
    <a href="#internal-mail" aria-controls="home" role="tab" data-toggle="tab"><label>Email Internal</label></a></li>
</ul>

<div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="internal-mail">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-lg-2 text-left">To</label>
                </div>
                <div class="to-email">
                    <div class="form-group"><div class="col-xs-10"><input type="text" name="to-email[]" class="form-control" value="sales@thebuking.com"></div><div class="col-xs-2"><button type="button" name="delete" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button></div></div>
                    <div class="form-group"><div class="col-xs-12"><button type="button" name="add-to-email" class="btn btn-xs btn-primary">Add New To</button></div></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-lg-2 text-left">Cc</label>
                </div>
                <div class="cc-email">
                    <div class="form-group"><div class="col-xs-10"><input type="text" name="cc-email[]" class="form-control" value="support@thebuking.com"></div><div class="col-xs-2"><button type="button" name="delete" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button></div></div>
                    <div class="form-group"><div class="col-xs-12"><button type="button" name="add-cc-email" class="btn btn-xs btn-primary">Add New CC</button></div></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label col-lg-2 text-left">Subject</label>
                </div>
                <div class="form-group">
                    <div class="col-lg-12">
                        <input type="text" name="subject-email" class="form-control" value="Change Status <?=$affwdm['hotelname'];?> from <?=$affwdm['wdmstatus'];?> to <?=$wdm['wdmstatus'];?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-12 text-left">Email Content</label>
                </div>
                <div class="form-group">
                    <textarea name="email-internal" id="html-box" class="form-control">
                        Dear Team,
                        <br>
                        <?=$_SESSION['_initial']?> update stage process <?=$affwdm['hotelname'];?> status from <?=$affwdm['wdmstatus'];?> to <span id="writed-next-stage"></span> with this following detail :
                        <br /><br />
                        Date :<br /><?php echo date('d F Y H:i'); ?><br />
                        Note : <br />
                        <span id="writed-note"></span>
                        <br><br>
                        Warm Regards,<br>
                        <?=$_SESSION['_initial']?>
                    </textarea>
                </div>
            </div>
        </div>
	</div>
</div>
