<header><h5>WDM Stage</h5></header>
<div id="div-1" class="body collapse in" aria-expanded="true">
  <div class="row">
      <div class="col-lg-12">
          <div class="form-group">
              <label class="control-label col-lg-4 text-left">WDM Status to</label>
              <div class="col-lg-6">
                  <select name="wdmto" class="form-control chzn-select" tabindex="7">
                      <option value="0">No WDM</option>
                      <?php
                          $statusto = array(1);
                          foreach($statusto as $status){
                              $stmt = $db->query("SELECT `affwdmstatusoid`, `wdmstatus` FROM `affwdmstatus` as `aw` where `aw`.`affwdmstatusoid` = '".$status."' and `aw`.`publishedoid` = '1'");
                              $sm = $stmt->fetch(PDO::FETCH_ASSOC);
                      ?>
                      <option value="<?=$sm['affwdmstatusoid'];?>"><?=$sm['wdmstatus'];?></option>
                      <?php
                          }
                      ?>
                  </select>
              </div>
          </div>
      </div>
  </div>

  <div class="row">
      <?php
      $statusto = array(1);
      foreach($statusto as $status){
          $stmt = $db->query("SELECT `affwdmstatusoid`, `wdmstatus` FROM `affwdmstatus` as `aw` where `aw`.`affwdmstatusoid` = '".$status."' and `aw`.`publishedoid` = '1'");
          $sm = $stmt->fetch(PDO::FETCH_ASSOC);
      ?>
      <div class="box dark box-detail-wdm" id="box-wdm-<?=$sm['affwdmstatusoid'];?>">
          <header>
              <h5><?=$sm['wdmstatus'];?></h5>
              <!-- .toolbar -->
              <div class="toolbar">
                <nav style="padding: 8px;">
                    <a href="javascript:;" class="btn btn-default btn-xs collapse-box">
                        <i class="fa fa-minus"></i>
                    </a>
                    <a href="javascript:;" class="btn btn-default btn-xs full-box">
                        <i class="fa fa-expand"></i>
                    </a>
                </nav>
              </div>            <!-- /.toolbar -->
          </header>
          <?php include ('../page/wdm-modal/form/form-wdm-'.$status.'.php'); ?>
      </div>
      <?php
      }
      ?>
  </div>
</div>


<header><h5>IBE Stage</h5></header>
<div id="div-1" class="body collapse in" aria-expanded="true">
  <div class="row">
      <div class="col-lg-12">
          <div class="form-group">
              <label class="control-label col-lg-4 text-left">IBE Status to</label>
              <div class="col-lg-6">
                  <select name="ibeto" class="form-control chzn-select" tabindex="7">
                      <?php
                          $statusto = array(1);
                          foreach($statusto as $status){
                              $stmt = $db->query("SELECT `affibestatusoid`, `ibestatus` FROM `affibestatus` as `ai` where `ai`.`affibestatusoid` = '".$status."' and `ai`.`publishedoid` = '1'");
                              $sm = $stmt->fetch(PDO::FETCH_ASSOC);
                      ?>
                      <option value="<?=$sm['affibestatusoid'];?>"><?=$sm['ibestatus'];?></option>
                      <?php
                          }
                      ?>
                  </select>
              </div>
          </div>
      </div>
  </div>

  <div class="row">
      <?php
      $statusto = array(1);
      foreach($statusto as $status){
          $stmt = $db->query("SELECT `affibestatusoid`, `ibestatus` FROM `affibestatus` as `ai` where `ai`.`affibestatusoid` = '".$status."' and `ai`.`publishedoid` = '1'");
          $sm = $stmt->fetch(PDO::FETCH_ASSOC);
      ?>
      <div class="box dark box-detail-ibe" id="box-ibe-<?=$sm['affibestatusoid'];?>">
          <header>
              <h5><?=$sm['ibestatus'];?></h5>
              <!-- .toolbar -->
              <div class="toolbar">
                <nav style="padding: 8px;">
                    <a href="javascript:;" class="btn btn-default btn-xs collapse-box">
                        <i class="fa fa-minus"></i>
                    </a>
                    <a href="javascript:;" class="btn btn-default btn-xs full-box">
                        <i class="fa fa-expand"></i>
                    </a>
                </nav>
              </div>            <!-- /.toolbar -->
          </header>
          <?php include ('../page/ibe-modal/form/form-ibe-'.$status.'.php'); ?>
      </div>
      <?php
      }
      ?>
  </div>
</div>
