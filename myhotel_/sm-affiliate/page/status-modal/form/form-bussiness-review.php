<?php
	$stmt = $db->query("SELECT * FROM `affhotelbisrev` where `hoteloid` = '".$affsm['hoteloid']."'");
	$br = $stmt->fetch(PDO::FETCH_ASSOC);
?>
<div id="div-1" class="body collapse in" aria-expanded="true">
    <div class="form-group">
        <label for="text1" class="control-label col-md-3">Average Rate</label>
        <div class="col-md-3">
<input type="text" name="avgrate" class="form-control" value="<?=$br['avgrate']?>">
        </div>
        <label for="text1" class="control-label col-md-3">Number of Room</label>
        <div class="col-md-2">
<input type="text" name="number_room" class="form-control" value="<?=$br['number_room']?>">
        </div>
    </div>
    <div class="form-group">
        <label for="text1" class="control-label col-md-3">Online Revenue</label>
        <div class="col-md-2">
            <div class="input-group">
                <input type="text" name="monthly_online_revenue" class="form-control" value="<?=$br['monthly_online_revenue']?>">
                <span class="input-group-addon">%</span>
            </div>
        </div>
        <label for="text1" class="control-label col-md-offset-1  col-md-3">Total Revenue</label>
        <div class="col-md-3">
<input type="text" name="total_revenue" class="form-control" value="<?=$br['total_revenue']?>">
        </div>
    </div>
    <hr />
    <div class="row"><div class="col-md-12"><h4>Current Booking Enginee Information</h4></div></div>
    <div class="form-group">
        <label for="text1" class="control-label col-md-5">Current Booking Enginee</label>
        <div class="col-md-6">
            <input type="text" name="current_be_name" class="form-control" value="<?=$br['current_be_name']?>">
        </div>
    </div>
    <div class="form-group">
        <label for="text1" class="control-label col-md-5">End of contract</label>
        <div class="col-md-4">
            <div class="input-group">
                <span class="input-group-addon">month</span>
                <input type="text" name="end_of_contract_month" class="form-control" value="<?=$br['end_of_contract_month']?>">
                <span class="input-group-addon">year</span>
                <input type="text" name="end_of_contract_year" class="form-control" value="<?=$br['end_of_contract_year']?>">
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="text1" class="control-label col-md-5">Penalty if broken the current contract</label>
        <div class="col-md-7">
            <textarea class="form-control" name="penalty_if_broken_contract"><?=$br['penalty_if_broken_contract']?></textarea>
        </div>
    </div>
    <hr />
    <div class="row"><div class="col-md-12"><h4>Social Media Exposure</h4></div></div>
    <div class="form-group">
        <label for="text1" class="control-label col-md-3">TripAdvisor URL</label>
        <div class="col-md-5">
<input type="text" name="tripadvisor_url" class="form-control" value="<?=$br['tripadvisor_url']?>">
        </div>
        <label for="text1" class="control-label col-md-1">Rank</label>
        <div class="col-md-3">
            <div class="input-group">
                <input type="text" name="tripadvisor_rank" class="form-control" value="<?=$br['tripadvisor_rank']?>">
                <span class="input-group-addon">From</span>
                <input type="text" name="tripadvisor_rankfrom" class="form-control" value="<?=$br['tripadvisor_rankfrom']?>">
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="text1" class="control-label col-md-3">Response TripAdvisor</label>
        <div class="checkbox col-md-8">
        <?php
$response_ta = array('excellent', 'good', 'fair', 'no response');
foreach($response_ta as $rta){
if($rta == $br['tripadvisor_responsescore']){ $checked = 'checked'; }else{ $checked=''; }
?>
        <label><input type="radio" name="tripadvisor_responsescore" value="<?=$rta?>" <?=$checked?>> <?=$rta?></label>
            <?php 
}
?>
        </div>
    </div>
    <div class="form-group">
        <label for="text1" class="control-label col-md-3">Booking.com URL</label>
        <div class="col-md-5">
<input type="text" name="bookingcom_url" class="form-control" value="<?=$br['bookingcom_url']?>">
        </div>
        <label for="text1" class="control-label col-md-1">Rank</label>
        <div class="col-md-3">
            <div class="input-group">
                <input type="text" name="bookingcom_rank" class="form-control" value="<?=$br['bookingcom_rank']?>">
                <span class="input-group-addon">From</span>
                <input type="text" name="bookingcom_rankfrom" class="form-control" value="<?=$br['bookingcom_rankfrom']?>">
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="text1" class="control-label col-md-3">Traveloka URL</label>
        <div class="col-md-5">
<input type="text" name="traveloka_url" class="form-control" value="<?=$br['traveloka_url']?>">
        </div>
        <label for="text1" class="control-label col-md-1">Rank</label>
        <div class="col-md-3">
            <div class="input-group">
                <input type="text" name="traveloka_rank" class="form-control" value="<?=$br['traveloka_rank']?>">
                <span class="input-group-addon">From</span>
                <input type="text" name="traveloka_rankfrom" class="form-control" value="<?=$br['traveloka_rankfrom']?>">
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="text1" class="control-label col-md-3">Agoda URL</label>
        <div class="col-md-5">
<input type="text" name="agoda_url" class="form-control" value="<?=$br['agoda_url']?>">
        </div>
        <label for="text1" class="control-label col-md-1">Rank</label>
        <div class="col-md-3">
            <div class="input-group">
                <input type="text" name="agoda_rank" class="form-control" value="<?=$br['agoda_rank']?>">
                <span class="input-group-addon">From</span>
                <input type="text" name="agoda_rankfrom" class="form-control" value="<?=$br['agoda_rankfrom']?>">
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="text1" class="control-label col-md-3">Facebook URL</label>
        <div class="col-md-4">
<input type="text" name="facebook_url" class="form-control" value="<?=$br['facebook_url']?>">
        </div>
        <label for="text1" class="control-label col-md-2">Post Updates</label>
        <div class="col-md-3">
            <div class="input-group">
                <input type="text" name="facebook_updateweek" class="form-control" value="<?=$br['facebook_updateweek']?>">
                <span class="input-group-addon">/ week</span>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="text1" class="control-label col-md-3">Instagram URL</label>
        <div class="col-md-4">
<input type="text" name="instagram_url" class="form-control" value="<?=$br['instagram_url']?>">
        </div>
        <label for="text1" class="control-label col-md-2">Post Updates</label>
        <div class="col-md-3">
            <div class="input-group">
                <input type="text" name="instagram_updateweek" class="form-control" value="<?=$br['instagram_updateweek']?>">
                <span class="input-group-addon">/ week</span>
            </div>
        </div>
    </div>
    <hr />
    <?php
$score = array('excellent', 'good', 'fair', 'bad');
$option = array('yes', 'no');
?>
    <div class="row"><div class="col-md-12"><h4>Website Exposure</h4></div></div>
    <div class="form-group">
        <label for="text1" class="control-label col-md-4">Website URL</label>
        <div class="col-md-8">
<input type="text" name="website_url" class="form-control" value="<?=$br['website_url']?>">
        </div>
    </div>
    <div class="form-group">
        <label for="text1" class="control-label col-md-4">Check Content</label>
        <div class="checkbox col-md-8">
        <?php
foreach($score as $rta){
if($rta == $br['check_content']){ $checked = 'checked'; }else{ $checked=''; }
?>
        <label><input type="radio" name="check_content" value="<?=$rta?>" <?=$checked?>> <?=$rta?></label>
            <?php 
}
?>
        </div>
    </div>
    <div class="form-group">
        <label for="text1" class="control-label col-md-4">User Friendly</label>
        <div class="checkbox col-md-8">
        <?php
foreach($score as $rta){
if($rta == $br['user_friendly']){ $checked = 'checked'; }else{ $checked=''; }
?>
        <label><input type="radio" name="user_friendly" value="<?=$rta?>" <?=$checked?>> <?=$rta?></label>
            <?php 
}
?>
        </div>
    </div>
    <div class="form-group">
        <label for="text1" class="control-label col-md-4">Mobile Responsive</label>
        <div class="checkbox col-md-8">
        <?php
foreach($score as $rta){
if($rta == $br['mobile_responsive']){ $checked = 'checked'; }else{ $checked=''; }
?>
        <label><input type="radio" name="mobile_responsive" value="<?=$rta?>" <?=$checked?>> <?=$rta?></label>
            <?php 
}
?>
        </div>
    </div>
    <div class="form-group">
        <label for="text1" class="control-label col-md-4">Booking Widget in Website</label>
        <div class="checkbox col-md-8">
        <?php
foreach($score as $rta){
if($rta == $br['booking_widget']){ $checked = 'checked'; }else{ $checked=''; }
?>
        <label><input type="radio" name="booking_widget" value="<?=$rta?>" <?=$checked?>> <?=$rta?></label>
            <?php 
}
?>
        </div>
    </div>
    <div class="form-group">
        <label for="text1" class="control-label col-md-4">Social Media Button</label>
        <div class="checkbox col-md-8">
        <?php
foreach($score as $rta){
if($rta == $br['socmed_button']){ $checked = 'checked'; }else{ $checked=''; }
?>
        <label><input type="radio" name="socmed_button" value="<?=$rta?>" <?=$checked?>> <?=$rta?></label>
            <?php 
}
?>
        </div>
    </div>
</div>