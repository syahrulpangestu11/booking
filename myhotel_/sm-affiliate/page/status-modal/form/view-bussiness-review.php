<div id="div-1" class="body collapse in" aria-expanded="true">
<?php
	$stmt = $db->prepare("SELECT * FROM `affhotelbisrev` where `hoteloid` = :a");
	$stmt->execute(array(':a' => $affsm['hoteloid']));
	$br= $stmt->fetch(PDO::FETCH_ASSOC);

?>
Average Rate : <?=number_format($br['avgrate'])?><br />
Number of Room : <?=$br['number_room']?><br />
Percentage of Monthly Online Revenue : <?=number_format($br['monthly_online_revenue'])?><br />
Total Revenue : <?=number_format($br['total_revenue'])?><br />
<b>Current Booking Enginee</h4></b><br />
Current Booking Enginee : <?=$br['current_be_name']?> / <?=$br['end_of_contract_year']?><br />
Penalty if broken the current contract : <?=$br['penalty_if_broken_contract']?><br />
<b>Social Media Exposure</b><br />
TripAdvisor URL : <a href="<?=$br['tripadvisor_url']?>"><?=$br['tripadvisor_url']?></a>
| Rank <?=$br['tripadvisor_rank']?> from <?=$br['tripadvisor_rankfrom']?> | Response to TripAdvisor Score <?=$br['tripadvisor_responsescore']?><br />
Booking.com URL : <a href="<?=$br['bookingcom_url']?>"><?=$br['bookingcom_url']?></a> | Rank <?=$br['bookingcom_rank']?> From <?=$br['bookingcom_rankfrom']?><br />
Traveloka URL : <a href="<?=$br['traveloka_url']?>"><?=$br['traveloka_url']?></a> | Rank <?=$br['traveloka_rank']?> From <?=$br['traveloka_rankfrom']?><br />
Agoda URL : <a href="<?=$br['agoda_url']?>"><?=$br['agoda_url']?></a> | Rank <?=$br['agoda_rank']?> From <?=$br['agoda_rankfrom']?><br />
Facebook URL : <a href="<?=$br['facebook_url']?>"><?=$br['facebook_url']?></a> | Updates <?=$br['facebook_updateweek']?> / week<br />
Instagram URL : <a href="<?=$br['instagram_url']?>"><?=$br['instagram_url']?></a> | Updates <?=$br['instagram_updateweek']?> / week<br />
<b>Website Exposure</b><br />
Website URL : <a href="<?=$br['website_url']?>"><?=$br['website_url']?></a><br />
&bull; Check Content : <?=$br['check_content']?><br />
&bull; User Friendly : <?=$br['user_friendly']?><br />
&bull; Mobile Responsive : <?=$br['mobile_responsive']?><br />
&bull; Booking Widget in Website : <?=$br['booking_widget']?><br />
&bull; Social Media Button : <?=$br['socmed_button']?><br />
</div>