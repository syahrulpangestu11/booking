<div id="div-1" class="body collapse in" aria-expanded="true">
    <div class="form-group">
        <label class="control-label col-lg-3">Date</label>
        <div class="col-md-6">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                <input type="text" name="dateoverride" id="detail-date" class="form-control" value="<?php echo date("d F Y"); ?>" required="required">
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-lg-3">Note</label>
        <div class="col-lg-8">
            <textarea id="note" name="note" class="form-control"></textarea>
        </div>
    </div>
</div>
