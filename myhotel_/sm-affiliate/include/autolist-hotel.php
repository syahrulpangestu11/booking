<?php
try {

error_reporting(E_ALL ^ E_NOTICE);
include ('../configuration/connection.php');
include ('../class/affiliate.php');

	$results = array();
	$stmt = $db->prepare("select hoteloid, hotelname where hotelname like '%:a%'");
	$stmt->execute(array(':a' => $_GET['query']));
	foreach ($stmt->fetch(PDO::FETCH_COLUMN) as $row) {
        $results[] = $row['hotelname'];
    }
	return json_encode($results);

}catch (Exception $e) {
	//echo $e->getMessage();
}
?>