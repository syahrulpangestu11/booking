<?php
  /* IBE  ====================================================================================*/
  $stmt = $db->prepare("select al.*, CASE WHEN x1.status IS NULL THEN x2.status ELSE x1.status END as statusfrom, y.status as statusto  from affiliatelog al
  left join (select ahs.status, ahs.affiliatehotelstatusoid from affiliatehotelstatus ahs) x1 on x1.affiliatehotelstatusoid = al.from_status  and al.from_type = 'sm'
  left join (select ai.ibestatus as status, ai.affibestatusoid from affibestatus ai) x2 on x2.affibestatusoid = al.from_status  and al.from_type = 'ibe'
  inner join (select ai.ibestatus as status, ai.affibestatusoid from affibestatus ai) y on y.affibestatusoid = al.to_status
  where al.hoteloid = :a and al.to_type = 'ibe' order by al.startdate desc");
  $stmt->execute(array(':a' => $row['hoteloid']));
  if($stmt->rowCount() > 0){ echo "<b>IBE</b><br>"; }
  foreach($stmt->fetchAll(PDO::FETCH_ASSOC) as $row2){
?>
  <?php echo date('ymd', strtotime($row2['startdate']))." - ".$row2['statusto']; ?><br>
  <?php if(!empty($row2['note'])){ echo '<div style="padding-left:20px;">'.$row2['note'].'</div>'; } ?>
<?php
      if(in_array($row['to_status'], array(2, 3))){
        $stmt			= $db->prepare("select * from afflogdetail where affiliatelogoid = :a order by detail_date desc");
        $stmt->execute(array(':a' => $row2['affiliatelogoid']));
        $count_progress	= $stmt->rowCount();
        if($count_progress > 0){
          foreach($stmt->fetchAll(PDO::FETCH_ASSOC) as $logdtl){
?>
          <div style="padding-left:20px;">&bull; <?=date('dmY', strtotime($logdtl['detail_date']))?> <?=date('H:i', strtotime($logdtl['detail_time']))?> - <?=$logdtl['detail_note']?></div>
<?php
          }
        }
      }
  }

  /* WDM ====================================================================================*/
  $stmt = $db->prepare("select al.*, CASE WHEN x1.status IS NULL THEN x2.status ELSE x1.status END as statusfrom, y.status as statusto from affiliatelog al
  left join (select ahs.status, ahs.affiliatehotelstatusoid from affiliatehotelstatus ahs) x1 on x1.affiliatehotelstatusoid = al.from_status  and al.from_type = 'sm'
  left join (select aw.wdmstatus as status, aw.affwdmstatusoid from affwdmstatus aw) x2 on x2.affwdmstatusoid = al.from_status  and al.from_type = 'wdm'
  inner join (select aw.wdmstatus as status, aw.affwdmstatusoid from affwdmstatus aw) y on y.affwdmstatusoid = al.to_status
  where al.hoteloid = :a and al.to_type = 'wdm' order by al.startdate desc");
  $stmt->execute(array(':a' => $row['hoteloid']));
  if($stmt->rowCount() > 0){ echo "<b>WDM</b><br>"; }
  foreach($stmt->fetchAll(PDO::FETCH_ASSOC) as $row2){
?>
  <?php echo date('ymd', strtotime($row2['startdate']))." - ".$row2['statusto']; ?><br>
  <?php if(!empty($row2['note'])){ echo '<div style="padding-left:20px;">'.$row2['note'].'</div>'; } ?>
<?php
        if(in_array($row['to_status'], array(3, 4))){
        $stmt			= $db->prepare("select * from afflogdetail where affiliatelogoid = :a order by detail_date desc");
        $stmt->execute(array(':a' => $row2['affiliatelogoid']));
        $count_progress	= $stmt->rowCount();
        if($count_progress > 0){
          foreach($stmt->fetchAll(PDO::FETCH_ASSOC) as $logdtl){
?>
          <div style="padding-left:20px;">&bull; <?=date('dmY', strtotime($logdtl['detail_date']))?> <?=date('H:i', strtotime($logdtl['detail_time']))?> - <?=$logdtl['detail_note']?></div>
<?php
          }
        }
      }
  }
?>
