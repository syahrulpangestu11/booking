<?php
try {
	
	if(empty($_POST['hoteloid'])){ $_POST['hoteloid'] = 0; }
	
	$stmt = $db->prepare("insert into affiliatehotel 
	(hoteloid, affiliateoid, chainoid, chain_name, brandoid, brand_name, hotelname, star, address, countryoid, country_name, stateoid, state_name, cityoid, city_name, website_url, affiliatehotelstatusoid) values (:a, :b, :c, :d, :e, :f, :g, :h, :i, :j, :k, :l, :m, :n, :o, :p, :q)");
	$stmt->execute(array(':a' => $_POST['hoteloid'], ':b' => $affiliate->affiliateoid, ':c' => $_POST['chain'], ':d' => $_POST['chainname'], ':e' => $_POST['brand'], ':f' => $_POST['brandname'], ':g' => $_POST['name'], ':h' => $_POST['star'], ':i' => $_POST['address'], ':j' => $_POST['country'], ':k' => $_POST['countryname'], ':l' => $_POST['state'], ':m' => $_POST['statename'], ':n' => $_POST['city'], ':o' => $_POST['cityname'], ':p' => $_POST['website'], ':q' => 1)); 
	
	$hotelaffiliate = $db->lastInsertId();
	
	$stmt = $db->prepare("update affiliatehotel set 
	owner = :m1, owner_email = :m2, owner_phone = :m3, 
	generalmanager = :n1, gm_email = :n2, gm_phone = :n3, 
	directorofsales = :o1, dos_email = :o2, dos_phone = :o3, 
	ecommerce = :p1, ecomm_email = :p2, ecomm_phone = :q3, 
	finance = :q1, finance_email = :q2, finance_phone = :p3, 
	hotel_email = :r1, hotel_phone = :r2, hotel_fax = :r3 where affiliatehoteloid = :id ");
	$stmt->execute(array(':id' => $hotelaffiliate,
	':m1' => $_POST['owner'], ':m2' => $_POST['owner_email'], ':m3' => $_POST['owner_phone'],
	':n1' => $_POST['generalmanager'], ':n2' => $_POST['gm_email'], ':n3' => $_POST['gm_phone'],
	':o1' => $_POST['directorofsales'], ':o2' => $_POST['dos_email'], ':o3' => $_POST['dos_phone'],
	':p1' => $_POST['ecommerce'], ':p2' => $_POST['ecomm_email'], ':p3' => $_POST['ecomm_phone'],
	':q1' => $_POST['finance'], ':q2' => $_POST['finance_email'], ':q3' => $_POST['finance_phone'],
	':r1' => $_POST['hotel_email'], ':r2' => $_POST['hotel_phone'], ':r3' => $_POST['hotel_fax'])); 
	 
	
	$stmt = $db->prepare("update affiliatehotel set current_be_id =:a, current_be_name =:b, end_of_contract_month =:c, end_of_contract_year =:d, penalty_if_broken_contract= :e, created =:f, updated =:g  where affiliatehoteloid = :id ");
	$stmt->execute(array(':id' => $hotelaffiliate, ':a' => $_POST['be_id'], ':b' => $_POST['be'], ':c' => $_POST['contract_month'], ':d' => $_POST['contract_year'], ':e' => $_POST['penalty'], ':f' => date('Y-m-d H:i:s'), ':g' => date('Y-m-d H:i:s')));
	
	$stmt = $db->prepare("insert into affiliatelog (affiliatehoteloid, logtime, from_status, to_status, note, usertype, id, name) values (:a, :b, :c, :d, :e, :f, :g, :h)");
	$stmt->execute(array(':a' => $hotelaffiliate, ':b' => date('Y-m-d H:i:s'), ':c' => 1, ':d' => 1, ':e' => 'Registering '.$_POST['name'], ':f' => $_SESSION['tbcaff'], ':g' => $_SESSION['tbcaffnoidf'], ':h' => $_SESSION['tbcaffname']));
 
	
	header('location: '.$baseurl.'/listhotel');
	
}catch (Exception $e) {
	echo $e->getMessage();
	die();
}
?>