<?php
error_reporting(E_ALL ^ E_NOTICE);
include ('../configuration/connection.php');

$stmt = $db->prepare("select * from affiliatehotel_agreement where affiliatehotel_agreementoid = :a");
$stmt->execute(array(':a' => $_GET['id']));
$agreement = $stmt->fetch(PDO::FETCH_ASSOC);
?>
<b>Agreement Date : </b><?php echo date('d F Y', strtotime($agreement['agreementdate'])); ?></br>
<b>Length of Contract : </b><?php echo $agreement['length_of_contract']; ?> year</br>
<b>Commission Type :</b> <?php echo $agreement['commissiontype']; ?><br />
<?php if($agreement['commissiontype'] == "flatfee") { ?>
<b>Flat Fee Billing Periode :</b> <?php echo $agreement['flatfee_billingtype']; ?><br />
<b>Flat Fee Amount :</b> <?php echo number_format($agreement['flatfee_amount']); ?><br />
<b>Affiliate Commission :</b> <?php echo $agreement['flatfee_commission']; ?>% from flat fee amount<br />
<?php }else if($agreement['commissiontype'] == "commissiontype"){ ?>
<b>The B&uuml;king Commission :</b> <?php echo $agreement['thebuking_commission']; ?>%<br />
<b>Affiliate Commission :</b> <?php echo $agreement['affiliate_commission']; ?>% from agreement<br />
<b>Minimum Guarantee Commission :</b> <?php echo number_format($agreement['min_guarantee_commission']); ?>
<?php }else if($agreement['commissiontype'] == "commissionmarkup"){ ?>
<b>Base Commission :</b> <?php echo $agreement['base_commission']; ?>%<br />
<b>Affiliate Commission Markup :</b> <?php echo $agreement['affiliate_commission_markup']; ?>%<br />
<b>Agreement Commission :</b> <?php echo $agreement['thebuking_commission']; ?>%<br />
<b>Minimum Guarantee Commission :</b> <?php echo number_format($agreement['min_guarantee_commission']); ?>
<?php } ?>
<b>Siteminder : </b><?php echo $agreement['siteminder']; ?></br>
<b>Property Management System : </b><?php echo $agreement['pms']; ?></br>
<b>Internet Payment Gateway : </b><?php echo $agreement['ipg']; ?></br>
<b>Company Name : </b><?php echo $agreement['company_name']; ?></br>
<b>Company Address : </b><?php echo $agreement['company_address']; ?></br>
<b>Company NPWP: </b><?php echo $agreement['company_npwp']; ?></br>
<b>PIC Name : </b><?php echo $agreement['pic_name']; ?></br>
<b>PIC ID Number : </b><?php echo $agreement['pic_id_number']; ?></br>