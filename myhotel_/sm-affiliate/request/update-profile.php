<?php
$response = "failed";

try {
	
error_reporting(E_ALL ^ E_NOTICE);
include ('../configuration/connection.php');
include ('../class/affiliate.php');

$affiliate = new Affiliate($db);
$affiliate->setAffiliate($_POST['affoid']);
$generaldata = $affiliate->generalData();

switch($_POST['mode']){
	case 'profile' :
		$foundemail = $affiliate->checkChangedEmail($email, $affiliate->affiliateoid);
		if($foundemail == 0){
			$stmt = $db->prepare("update affiliate set firstname = :a, lastname = :b, email = :c, phone = :d where affiliateoid = :e");
			$stmt->execute(array(':a' => $_POST['firstname'], ':b' => $_POST['lastname'], ':c' => $_POST['email'], ':d' => $_POST['phone'], ':e' => $affiliate->affiliateoid));
			$response = "success";
		}else{
			$response = "email has been used by another account";
		}
	break;
	case 'bank-account' :
		$stmt = $db->prepare("update affiliate set bankname = :a, bankbranch = :b, accountnumber = :c, accountname = :d where affiliateoid = :e");
		$stmt->execute(array(':a' => $_POST['bank'], ':b' => $_POST['branch'], ':c' => $_POST['accountnumber'], ':d' => $_POST['accountname'], ':e' => $affiliate->affiliateoid));
		$response = "success";
	break;
	case 'change-password' :
		$stmt = $db->prepare("update affiliate set password = :a where affiliateoid = :b");
		$stmt->execute(array(':a' => sha1($_POST['password']), ':b' => $affiliate->affiliateoid));
		$response = "success";
	break;
	case 'aff-commission' :
		$stmt = $db->prepare("update affiliate set flatfeecommission = :a, commission = :b, base_standard_commission = :c, markup_standard_commission = :d, base_freewebsite_commission = :e, markup_freewebsite_commission = :f where affiliateoid = :id");
		$stmt->execute(array(':a' => $_POST['flatfeecommission'], ':b' => $_POST['commission'], ':c' => $generaldata['base_standard_commission'], ':d' => $_POST['markup_standard_commission'], ':e' => $generaldata['base_freewebsite_commission'], ':f' => $_POST['markup_freewebsite_commission'], ':id' => $affiliate->affiliateoid));
		$response = "success";
	break;
	default : echo "you can't access this page."; print_r($_POST); die(); break;
}

}catch (Exception $e) {
	echo $e->getMessage();
}
echo $response;
?>