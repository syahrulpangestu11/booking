<?php
	session_start();
	error_reporting(E_ALL ^ E_NOTICE);
	include ('../../conf/connection.php');
	include ('../include/function-affiliate.php');

	$status = $_POST['status'];
	$keyword	= $_POST['keyword'];
	$typestatus	= $_POST['type'];

		if($typestatus == "ibe-status"){
			$status_field = "ibe.ibestatus as status, h.affibestatusoid";
			$status_field_where = "ibe.affibestatusoid";
			$status_join = "affibestatus ibe using (affibestatusoid)";
			$data_modal = '#ibe-Modal';
			$filter_not_in = "and affiliatehotelstatusoid not in ('8', '9', '10', '11', '12', '13', '15')";
		}else if($typestatus == "wdm-status"){
			$status_field = "wdm.wdmstatus as status, h.affwdmstatusoid";
			$status_field_where = "wdm.affwdmstatusoid";
			$status_join = "affwdmstatus wdm using (affwdmstatusoid)";
			$data_modal = '#wdm-Modal';
			$filter_not_in = "";
		}

    $stmt = $db->query("select h.hoteloid, h.hotelname, h.created, ".$status_field." from hotel h inner join ".$status_join." where h.hotelname like '%".$keyword."%' and ".$status_field_where." in (".$status.") ".$filter_not_in." limit 5");
    foreach($stmt->fetchAll(PDO::FETCH_ASSOC) as $row){

		if($typestatus == "ibe-status"){
			$stmt2 = $db->prepare("select al.startdate as logtime from affiliatelog al inner join hotel h using (hoteloid) where to_type = :a and to_status = :b and al.hoteloid = :c order by al.startdate desc limit 1");
			$stmt2->execute(array(':a' => 'ibe', ':b' => $row['affibestatusoid'], ':c' => $row['hoteloid']));
		}else if($typestatus == "wdm-status"){
			$stmt2 = $db->prepare("select al.startdate as logtime from affiliatelog al inner join hotel h using (hoteloid) where to_type = :a and to_status = :b and al.hoteloid = :c order by al.startdate desc limit 1");
			$stmt2->execute(array(':a' => 'wdm', ':b' => $row['affwdmstatusoid'], ':c' => $row['hoteloid']));
		}

		$log = $stmt2->fetch(PDO::FETCH_ASSOC);
		$idletime = knowIdleTime($log['logtime'], date('Y-m-d H:i:s'));
?>
    <tr class="direct-detail-hotel" data-id="<?php echo $row['hoteloid']; ?>">
    <?php
		if($typestatus == "ibe-status"){
			$column = array(++$no, date('j M Y', strtotime($log['logtime'])), $row['hotelname'], $idletime);
		}else if($typestatus == "wdm-status"){
			if($status == 7){
				$column = array(++$no, date('j M Y', strtotime($log['logtime'])), $row['hotelname']);
			}else{
				$column = array(++$no, date('j M Y', strtotime($log['logtime'])), $row['hotelname'], $idletime);
			}
		}

		foreach($column as $td){
		    echo "<td>".$td."</td>";
		}

		if($typestatus == "ibe-status"){
			if($status != 8){
		?>
	        <td><button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="<?php echo $data_modal; ?>" data-id="<?php echo $row['hoteloid']; ?>"><i class="fa fa-share"></i></button></td>
	<?php
			}
		}else if($typestatus == "wdm-status"){
			if($status != 7){
	?>
	        <td><button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="<?php echo $data_modal; ?>" data-id="<?php echo $row['hoteloid']; ?>"><i class="fa fa-share"></i></button></td>
	<?php
			}
		}
	?>
    </tr>
<?php
	}
?>
