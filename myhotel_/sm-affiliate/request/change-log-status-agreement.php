<?php
	$profile['flatfeecommission'] = 0;
	$profile['commission'] = 0;
	$profile['base_standard_commission'] = 0;
	$profile['markup_standard_commission'] = 0;
	$profile['base_freewebsite_commission'] = 0;
	$profile['markup_freewebsite_commission'] = 0;
	
	$stmt = $db->prepare("INSERT INTO affiliatehotel_agreementlog (hoteloid, affiliatelogoid, agreementdate, length_of_contract, commissiontype, benefit_website, siteminder, siteminder_name, pms, pms_name, ipg, ipg_name, company_name, company_address, company_npwp, pic_name, pic_title, pic_id_number, created, createdby, updated, updatedby) VALUES (:a, :b, :c, :d, :e, :f, :g, :h, :i, :j, :k, :l, :m, :n, :o, :p, :q, :r, :s, :t, :u, :v)");
	$stmt->execute(array(':a' => $_POST['hoteloid'], ':b' => $affiliatelogid, ':c' => date('Y-m-d', strtotime($_POST['agreementdate'])), ':d' => $_POST['length_of_contract'], ':e' => $_POST['commissiontype'], ':f' => $_POST['benefit'], ':g' => $_POST['siteminder'], ':h' => $_POST['siteminder_name'], ':i' => $_POST['pms'], ':j' => $_POST['pms_name'], ':k' => $_POST['ipg'], ':l' => $_POST['ipg_name'], ':m' => $_POST['companyname'], ':n' => $_POST['companyaddress'], ':o' => $_POST['companynpwp'], ':p' => $_POST['picname'], ':q' => $_POST['pictitle'], ':r' => $_POST['picidnumber'], ':s' => $logtime, ':t' => $_SESSION['_initial'], ':u' => $logtime, ':v' => $_SESSION['_initial']));
	$aggreementlogid = $db->lastInsertId();
	
	if($_POST['commissiontype'] == 'flat fee'){
		$stmt = $db->prepare("UPDATE affiliatehotel_agreementlog SET flatfee_billingtype = :a, flatfee_amount = :b, flatfee_commission = :c WHERE affiliatehotel_agreementlogoid = :id");
		$stmt->execute(array(':a' => $_POST['flatfeebillingtype'], ':b' => $_POST['flatfee_amount'], ':c' => $profile['flatfeecommission'], ':id' => $aggreementlogid));
	}else if($_POST['commissiontype'] == 'commission percent'){
		$stmt = $db->prepare("UPDATE affiliatehotel_agreementlog SET thebuking_commission = :a, affiliate_commission = :b, min_guarantee_commission = :c WHERE affiliatehotel_agreementlogoid = :id");
		$stmt->execute(array(':a' => $_POST['thebuking_commission'], ':b' => $profile['commission'], ':c' => $_POST['minguarantee'], ':id' => $aggreementlogid));
	}else if($_POST['commissiontype'] == 'commission markup'){
		$stmt = $db->prepare("UPDATE affiliatehotel_agreementlog SET base_commission = :a, thebuking_commission = :b, 	affiliate_commission_markup = :c, min_guarantee_commission = :d WHERE affiliatehotel_agreementlogoid = :id");
		$stmt->execute(array(':a' => $profile['base_standard_commission'], ':b' => $profile['base_standard_commission']+$profile['markup_standard_commission'], ':c' => $profile['markup_standard_commission'], ':d' => $_POST['minguarantee'], ':id' => $aggreementlogid));
	}

	if($_POST['commissiontype'] == 'paid website'){
		$stmt = $db->prepare("UPDATE affiliatehotel_agreementlog SET benefit_website_amount = :a WHERE affiliatehotel_agreementlogoid = :id");
		$stmt->execute(array(':a' => $_POST['paidwebsite'], ':id' => $aggreementlogid));
	}
	
	$stmt = $db->prepare("select count(affiliatehotel_agreementoid) existagreement from affiliatehotel_agreement where hoteloid = :a");
	$stmt->execute(array(':a' => $_POST['hoteloid']));
	$count = $stmt->fetch(PDO::FETCH_ASSOC);
	
	$stmt = $db->prepare("select * from affiliatehotel_agreementlog where affiliatehotel_agreementlogoid = :id");
	$stmt->execute(array(':id' => $aggreementlogid));
	$row = $stmt->fetch(PDO::FETCH_ASSOC);
	
	if($count['existagreement'] == 0){
		$stmt_insert_agreement = $db->prepare("INSERT INTO `affiliatehotel_agreement` (`hoteloid`, `commissiontype`, `flatfee_billingtype`, `flatfee_commission`, `flatfee_amount`, `thebuking_commission`, `base_commission`, `affiliate_commission`, `affiliate_commission_markup`, `min_guarantee_commission`, `benefit_website`, `benefit_website_amount`, `agreementdate`, `length_of_contract`, `siteminder`, `siteminder_name`, `pms`, `pms_name`, `ipg`, `ipg_name`, `company_name`, `company_address`, `company_npwp`, `pic_name`, `pic_title`, `pic_id_number`, `status_agreement`, `created`, `createdby`, `updated`, `updatedby`) VALUES (:a, :b, :c, :d, :e, :f, :g, :h, :i, :j, :k, :n, :o, :p, :q, :r, :s, :t, :u, :v, :w, :x, :y, :z, :aa, :ab, :ac, :ad, :ae, :af, :ag)");
		$stmt_insert_agreement->execute(array(':a' => $row['hoteloid'], ':b' => $row['commissiontype'], ':c' => $row['flatfee_billingtype'], ':d' => $row['flatfee_commission'], ':e' => $row['flatfee_amount'], ':f' => $row['thebuking_commission'], ':g' => $row['base_commission'], ':h' => $row['affiliate_commission'], ':i' => $row['affiliate_commission_markup'], ':j' => $row['min_guarantee_commission'], ':k' => $row['benefit_website'], ':n' => $row['benefit_website_amount'], ':o' => $row['agreementdate'], ':p' => $row['length_of_contract'], ':q' => $row['siteminder'], ':r' => $row['siteminder_name'], ':s' => $row['pms'], ':t' => $row['pms_name'], ':u' => $row['ipg'], ':v' => $row['ipg_name'], ':w' => $row['company_name'], ':x' => $row['company_address'], ':y' => $row['company_npwp'], ':z' => $row['pic_name'], ':aa' => $row['pic_title'], ':ab' => $row['pic_id_number'], ':ac' => $row['status_agreement'], ':ad' => $row['created'], ':ae' => $row['createdby'], ':af' => $_SESSION['_initial'], ':ag' => $row['updatedby']));
	}else{
		$stmt_update_agreement = $db->prepare("UPDATE `affiliatehotel_agreement` SET `commissiontype` = :a , `flatfee_billingtype` = :b , `flatfee_commission` = :c , `flatfee_amount` = :d , `thebuking_commission` = :e , `base_commission` = :f , `affiliate_commission` = :g , `affiliate_commission_markup` = :h , `min_guarantee_commission` = :i , `benefit_website` = :j, `benefit_website_amount` = :m , `agreementdate` = :n , `length_of_contract` = :o , `siteminder` = :p , `siteminder_name` = :q , `pms` = :r , `pms_name` = :s , `ipg` = :t , `ipg_name` = :u , `company_name` = :v , `company_address` = :w , `company_npwp` = :x , `pic_name` = :y , `pic_title` = :z , `pic_id_number` = :aa , `status_agreement` = :ab , `updated` = :ac , `updatedby` = :ad WHERE `hoteloid` = :id");
		$stmt_update_agreement->execute(array(':id' => $row['hoteloid'], ':a' => $row['commissiontype'], ':b' => $row['flatfee_billingtype'], ':c' => $row['flatfee_commission'], ':d' => $row['flatfee_amount'], ':e' => $row['thebuking_commission'], ':f' => $row['base_commission'], ':g' => $row['affiliate_commission'], ':h' => $row['affiliate_commission_markup'], ':i' => $row['min_guarantee_commission'], ':j' => $row['benefit_website'], ':m' => $row['benefit_website_amount'], ':n' => $row['agreementdate'], ':o' => $row['length_of_contract'], ':p' => $row['siteminder'], ':q' => $row['siteminder_name'], ':r' => $row['pms'], ':s' => $row['pms_name'], ':t' => $row['ipg'], ':u' => $row['ipg_name'], ':v' => $row['company_name'], ':w' => $row['company_address'], ':x' => $row['company_npwp'], ':y' => $row['pic_name'], ':z' => $row['pic_title'], ':aa' => $row['pic_id_number'], ':ab' => $row['status_agreement'], ':ac' => $_SESSION['_initial'], ':ad' => $row['updatedby']));
	}

?>