<?php
	session_start();
	error_reporting(E_ALL ^ E_NOTICE);
	include ('../../conf/connection.php');
	
	$stmt	= $db->prepare("select h.hotelname, h.hoteloid, h.createdby, h.affiliatehotelstatusoid, as.status, h.affwdmstatusoid, aw.wdmstatus, h.affibestatusoid, ai.ibestatus from hotel as `h` inner join affiliatehotelstatus as `as` using (affiliatehotelstatusoid) left join affwdmstatus as `aw` using (affwdmstatusoid) left join affibestatus as `ai` using (affibestatusoid) where h.hoteloid = :a");
	$stmt->execute(array(':a' => $_GET['id']));
	$affwdm	= $stmt->fetch(PDO::FETCH_ASSOC);
?>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">[<?=$affwdm['wdmstatus'];?>] Change WDM Status</h4>
        <h5 id="hotelname"><?=$affwdm['hotelname'];?></h5>
        <i class="fa fa-user"></i> <?=$affwdm['createdby'];?> <span id="hotel-creator"></span><input type="hidden" name="hoteloid" value="<?=$affwdm['hoteloid'];?>" />
    </div>
    <div class="modal-body">
    <?php
        switch($affwdm['affwdmstatusoid']){
            case 1	: include('../page/wdm-modal/wdm-queue.php'); break;
			case 2	: include('../page/wdm-modal/wdm-assign-template.php'); break;
			case 3	: include('../page/wdm-modal/wdm-content-agregate.php'); break;
			case 4	: include('../page/wdm-modal/wdm-training.php'); break;
			case 5	: include('../page/wdm-modal/wdm-content-uat.php'); break;
			case 6	: include('../page/wdm-modal/wdm-to-live.php'); break;
        }
    ?>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary change-status">Save changes</button>
    </div>
