<?php
	$allowedExtension = array('jpg', 'jpeg', 'png');
	$tmp = explode('.', $_FILES['image']['name']);
	$extension = end($tmp);
	$image = '';
	if(!empty($_FILES['image']['name']) and isset($_FILES['image']['name'])){
		if(in_array($extension, $allowedExtension)){
			$imagename = str_replace(' ', '_', $_FILES['image']['name']);
			$imagename = str_replace('.'.$extension, '', $imagename);
			$ext = '.'.strtolower($extension);
			$image_new_name = $hcode.substr($imagename, 0, 10).date('Ymd_His').$ext;
			$folder_destination = 'pict/banner/';

			move_uploaded_file($_FILES['image']['tmp_name'], $upload_base.$folder_destination.$image_new_name);
			$image = $web_url.'/'.$folder_destination.$image_new_name;

			try {
				$stmt = $db->prepare("update hotel set banner = :a  where hoteloid = :b");
				$stmt->execute(array(':a' => $image, ':b' => $_SESSION['_hotel']));
			}catch(PDOException $ex) {
				echo "Invalid Query"; //print($ex);
				die();
			}

		}else{
			echo'<script>alert("Data not saved.\\nInvalid file.")</script>';
		}
	}


	$tmp_logo = explode('.', $_FILES['logo']['name']);
	$extension_logo = end($tmp_logo);
	$logo = '';
	if(!empty($_FILES['logo']['name']) and isset($_FILES['logo']['name'])){
		if(in_array($extension_logo, $allowedExtension)){
			$logoname = str_replace(' ', '_', $_FILES['logo']['name']);
			$logoname = str_replace('.'.$extension_logo, '', $logoname);
			$ext = '.'.strtolower($extension_logo);
			$logo_new_name = $hcode.substr($logoname, 0, 10).date('Ymd_His').$ext;
			$folder_destination = 'pict/logo/';

			move_uploaded_file($_FILES['logo']['tmp_name'], $upload_base.$folder_destination.$logo_new_name);
			$logo = $web_url.'/'.$folder_destination.$logo_new_name;

			try {
				$stmt = $db->prepare("update hotel set logo = :a  where hoteloid = :b");
				$stmt->execute(array(':a' => $logo, ':b' => $_SESSION['_hotel']));
			}catch(PDOException $ex) {
				echo "Invalid Query"; //print($ex);
				die();
			}

		}else{
			echo'<script>alert("Data not saved.\\nInvalid file.")</script>';
		}
	}


	try {
		$autoexpand = (isset($_POST['autoexpand'])) ? $_POST['autoexpand'] : "n";
		$showlogo = (isset($_POST['showlogo'])) ? $_POST['showlogo'] : "n";
		$tabca = (isset($_POST['tabca'])) ? $_POST['tabca'] : "";

		$stmt = $db->prepare("update hotel set template = :a, colortone = :b, autoexpand = :c, showlogo = :d, list_groupby = :e  where hoteloid = :id");
		$stmt->execute(array(':a' => $_POST['template'], ':b' => $_POST['colortone'], ':c' => $autoexpand, ':d' => $showlogo, ':e' => $tabca, ':id' => $_SESSION['_hotel']));
	}catch(PDOException $ex) {
		echo "Invalid Query"; //print($ex);
		die();
	}

	header("Location: ". $base_url ."/booking-enginee-appearance");

?>
