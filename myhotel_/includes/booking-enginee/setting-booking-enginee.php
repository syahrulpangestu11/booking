<?php
    include('includes/bootstrap.php');

    $stmt = $db->prepare("select hoteloid, template, colortone, autoexpand, banner, logo, showlogo, list_groupby, show_bar from hotel where hoteloid = :a");
    $stmt->execute(array(':a' => $_SESSION['_hotel']));
	$data = $stmt->fetch(PDO::FETCH_ASSOC);
?>

<section class="content-header">
    <h1>
        Your Booking Enginee
    </h1>
    <ol class="breadcrumb">
        <li class="active"><a href="#"><i class="fa fa-link"></i> Your Booking Enginee</a></li>
    </ol>
</section>

<section class="content" id="rooms">
	<div class="row">
        <div class="col-xs-12">
        <form class="form-box" method="post" enctype="multipart/form-data" id="data-input" action="<?=$base_url?>/booking-enginee-appearance/save">
		<div class="box box-form">
            <div class="box-header with-border">
                <h3 class="box-title">General</h3>
            </div>
            <div id="data-box" class="box-body">
                <ul class="inline form-input">
                    <li>
                        <label>Your Booking Enginee URL</label>
                        <input type="text" class="long" name="url" value="https://thebuking.com/ibe/index.php?hcode=<?php echo $hcode;?>" style="width:50%"><a href="https://thebuking.com/ibe/index.php?hcode=<?php echo $hcode;?>" target="_blank"><button type="button" class="pure-button green" style="margin:0 0 0 20px;"><i class="fa fa-send-o"></i> Visit Link</button></a>
                    </li>
                    <li>
                        <label>Booking Enginee Header Logo</label>
                        <?php
                        if(!empty($data['logo'])){?>
                          Current Image :
                          <img class="current-image" src="<?=$data['logo'];?>" alt="">
                          <!-- <br> -->
                          <?php
                        } ?>
                        <?php if($data['showlogo'] == 'y'){ $checked = 'checked="checked"'; }else{ $checked= ''; } ?>
                        <input type="checkbox" name="showlogo" value="y" <?=$checked;?> /> Display logo on header
                        <br><br>
                        Upload New Image:
                        <br>
                        <input type='file' class='input-text input-image' name='logo' accept='image/*'>
                        <img class='input-image-preview' id='preview' src='#' alt=""/>

                    </li>
                    <li>
                        <label>Booking Enginee Header Banner</label>
                        <?php
                        if(!empty($data['banner'])){?>
                          Current Image :
                          <img class="current-image" src="<?=$data['banner'];?>" alt="">
                          <br>
                          <?php
                        } ?>
                        Upload New Image : 
                        <br><i>(recommended banner size : 1300 px &times; 700 px )</i>
                        <br>
                        <input type="file" class="input-text input-image" name="image" accept='image/*'>
                        <img class='input-image-preview' id='preview' src='#' alt=""/>
                    </li>
                    <li>
                        <label>Choose Your Template</label>
                        <select name="template">
                            <?php
							$templatechoice = array(array('Template 1', '1'), array('Template 2', '2'), array('Template 3', '3'));
							foreach($templatechoice as $key => $template){
								if($template[1] == $data['template']){ $selected = 'selected="selected"'; }else{ $selected= ''; }
								echo '<option value="'.$template[1].'" '.$selected.'>'.$template[0].'</option>';
							}
							?>
                        </select>
                        <div class="thumb-templates" style="padding:10px 0;">
                        <?php
							foreach($templatechoice as $key => $template){
                                echo '<div class="item" t="'.$template[1].'" style="display:inline-block;padding:10px;">
                                    <a href="'.$base_url.'/images/template'.$template[1].'.jpg'.'" target="_blank"><img src="'.$base_url.'/images/template'.$template[1].'.jpg'.'" style="width:200px;"></a>
                                    <div align="center">'.$template[0].'</div>
                                </div>';
                            }
                        ?>
                        </div>
                    </li>
                    <li style="display:none;">
                    <?php
						if($data['autoexpand'] == 'y'){ $checked = 'checked="checked"'; }else{ $checked= ''; }
					?>
                    <label><input type="checkbox" name="autoexpand" value="y" <?=$checked;?> /> Auto expand list promotion / room</label></li>
                    <?php /*<li>
                        <label>Color Tone</label>
                        <select name="colortone">
                            <?php
							$tonechoice = array(array('Color Tone 1', 'C1'), array('Color Tone 2', 'C2'));
							foreach($tonechoice as $key => $tone){
								if($tone[1] == $data['colortone']){ $selected = 'selected="selected"'; }else{ $selected= ''; }
								echo '<option value="'.$tone[1].'" '.$selected.'>'.$tone[0].'</option>';
							}
							?>
                            </select>
                    </li>
                    <li><input type="checkbox" name="autoexpand" value="y" /> auto expand</li>
                    <li>
                        <label>Default View</label>
                        <select name="defaultview">
                            <?php
							$viewchoice = array('room list', 'offer list');
							foreach($viewchoice as $key => $view){
								if($view == $data['defaultview']){ $selected = 'selected="selected"'; }else{ $selected= ''; }
								echo '<option value="'.$view.'" '.$selected.'>'.$view.'</option>';
							}
							?>
                            </select>
                    </li>*/?>
                    <li>
                        <label>Default Check Availability Tab grouping on</label>
                        <select name="tabca">
                            <?php if($data['list_groupby'] == 'room'){ $sel1='selected'; $sel0=''; }else{ $sel1=''; $sel0='selected'; }?>
                            <option value="" <?=$sel0?>>Promotion</option>
                            <option value="room" <?=$sel1?>>Room Type</option>
                        </select>
                    </li>
				</ul>
                <div class="clear"></div>
			</div>
            <div class="box-footer">
                <button type="submit" class="btn default-button">Save</button>
            </div>
   		</div>
        </form>
   		</div>
    </div>

    <div class="row">
        <div class="col-xs-12">
        <form class="form-box" method="post" enctype="multipart/form-data" action="<?=$base_url?>/booking-enginee-appearance/save-bar">
		<div class="box box-form">
            <div class="box-header with-border">
                <h3 class="box-title">BAR Appearance</h3>
            </div>
            <div class="box-body">
                <ul class="inline form-input">
                    <li>
                        <label>Flexible Rate on Check Availability</label>
                        <select name="tabca">
                            <?php if($data['show_bar'] == 'n'){ $sel1='selected'; $sel0=''; }else{ $sel1=''; $sel0='selected'; }?>
                            <option value="y" <?=$sel0?>>Show Flexible Rate</option>
                            <option value="n" <?=$sel1?>>Hide Flexible Rate</option>
                        </select>
                    </li>
                    <li>
                        <label>Rateplan Setting</label>
                        <table class="table table-bordered table-condensed table-striped">
                            <tr>
                                <th>Rateplan</th>
                                <th>Display on Flexible Rate</th>
                            </tr>
                            <?php
                            $sb_id = "select ro.roomofferoid, ro.name AS roomoffername, ro.show_bar
                                    from hotel h
                                    inner join room r using (hoteloid)
                                    left join view v using (viewoid)
                                    inner join roomoffer ro using (roomoid)
                                    inner join offertype ot using (offertypeoid)
                                    left join (select GROUP_CONCAT(bed.name SEPARATOR ' / ') as bedconfiguration, roomoid from bed inner join roombed using (bedoid) group by roomoid) as bedroom on bedroom.roomoid = r.roomoid
                                    where h.hoteloid = '".$data['hoteloid']."' and r.publishedoid = '1' and ro.publishedoid = '1' and ot.publishedoid = '1' order by r.roomoid";
                            $stmt	= $db->query($sb_id);
                            $ab_id	= $stmt->fetchAll(PDO::FETCH_ASSOC);
                            foreach($ab_id as $ab_idx){
                                $rooid = $ab_idx['roomofferoid'];
                                $roname = $ab_idx['roomoffername'];
                                $ro_showbar = $ab_idx['show_bar'];
                            ?>
                            <tr>
                                <td><?=$roname?></td>
                                <td><select name="show[<?=$rooid?>]">
                                    <?php if($ro_showbar == 'n'){ $sel1='selected'; $sel0=''; }else{ $sel1=''; $sel0='selected'; }?>
                                    <option value="y" <?=$sel0?>>Show</option>
                                    <option value="n" <?=$sel1?>>Hide</option>
                                </select></td>
                            </tr>
                            <?php
                            }
                            ?>
                        </table>
                    </li>
				</ul>
			</div>
            <div class="box-footer">
                <button type="submit" class="btn default-button">Save</button>
            </div>
   		</div>
        </form>
        </div>
    </div>
</section>

<style media="screen">
  .current-image,
  .input-image-preview {
    height: 100px;
    display: block;
    margin: 5px 0 10px 0;
    background-image: linear-gradient(45deg, #DDD 25%, transparent 25%), linear-gradient(-45deg, #DDD 25%, transparent 25%), linear-gradient(45deg, transparent 75%, #DDD 75%), linear-gradient(-45deg, transparent 75%, #DDD 75%);
  background-size: 20px 20px;
  background-position: 0 0, 0 10px, 10px -10px, -10px 0px;
  }

  ul.form-input > li {margin-bottom: 20px; display: block;}
  .box{ padding:10px; }
  html,body,aside{ min-height:auto !important; }
</style>

<script type="text/javascript">
$(function(){

  function readURL(input, previewTarget) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            previewTarget
                .attr('src', e.target.result)
                .height(100)
                .show();
        };
        reader.readAsDataURL(input.files[0]);
    }
  }

  $(document).ready(function(){
    $('.input-image-preview').hide();
  });

  $('.input-image').each(function(){
    $('.input-image').change(function() {
      var thisElem = $(this);
      var thisElemJS = this;
      var previewTarget = thisElem.closest('li').find('.input-image-preview');
      var currentImage = thisElem.closest('li').find('.current-image');
      var sizeKB = thisElemJS.files[0].size / 1024;
      var img = new Image();
      img.src = window.URL.createObjectURL( this.files[0] );
      img.onload = function() {
        var width = img.naturalWidth,
            height = img.naturalHeight;
        // alert(sizeKB+' ** '+width +'×'+height);

        if(sizeKB > 200){
            // alert("Your image more than 200 KB, please compress or resize your image.");
            $dialogNotice.html("Your image more than 200 KB, please compress or resize your image");
            $dialogNotice.dialog("open");
            thisElem.val("");
            previewTarget.attr('src',"#");
        }else{
            var ext = thisElem.val().split('.').pop().toLowerCase();
            if($.inArray(ext, ['jpg','jpeg', 'png', 'gif']) == -1) {
              // alert('Invalid extension! Please make sure you upload image file type (.jpg , .jpeg , .png , or .gif).');
              $dialogNotice.html("Invalid extension! Please make sure you upload image file type (.jpg , .jpeg , .png , or .gif)");
              $dialogNotice.dialog("open");
              thisElem.val("");
              previewTarget.attr('src',"#");
            }else{
              readURL(thisElemJS, previewTarget);
              previewTarget.show();
              // currentImage.hide();
          }
        }
      }

    });
  });

  var $dialogNotice = $('<div id="dialog-notice"></div>')
	.dialog({
		autoOpen: false,
		title: 'Notification',
		buttons: {
			"Ok": function(){
				$(this).dialog( "close" );
				// document.location.href = "<?php echo $base_url; ?>/hotel-profile/rooms";
			}
		}
	});

});

</script>
