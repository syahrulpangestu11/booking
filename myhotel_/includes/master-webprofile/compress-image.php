<?php
	$imagename = $uploadedname . $ext; // (1000 x 667)
	$imagename_t = $uploadedname . "-t" . $ext; // (160 x 107)

	$imagename2 = $uploadedname2 . $ext2; // (1000 x 667)
	$imagename_t2 = $uploadedname2 . "-t" . $ext2; // (160 x 107)
	
	$imagename3 = $uploadedname3 . $ext3; // (1000 x 667)
	$imagename_t3 = $uploadedname3 . "-t" . $ext3; // (160 x 107)
	
	$imagename4 = $uploadedname4 . $ext4; // (1000 x 667)
	$imagename_t4 = $uploadedname4 . "-t" . $ext4; // (160 x 107)

	function saveImage($tn, $savePath, $imageQuality="100") {
		// *** Get extension
		$extension = strrchr($savePath, '.');
		$extension = strtolower($extension);

		switch($extension) {
			case '.jpg':
			case '.jpeg':
				if (imagetypes() & IMG_JPG) {
					imagejpeg($tn, $savePath, $imageQuality);
				}
				break;
			case '.gif':
				if (imagetypes() & IMG_GIF) {
					imagegif($tn, $savePath);
				}
				break;
			case '.png':
				// *** Scale quality from 0-100 to 0-9
				$scaleQuality = round(($imageQuality/100) * 9);

				// *** Invert quality setting as 0 is best, not 9
				$invertScaleQuality = 9 - $scaleQuality;
				if (imagetypes() & IMG_PNG) {
						imagepng($tn, $savePath, $invertScaleQuality);
				}
				break;
			// ... etc
			default:
				// *** No extension - No save.
				break;
		}

		imagedestroy($tn);
	}

	if($filename!=''){

			//-------- mengupload gambar ke server
			$source = $_FILES['photos']['tmp_name'];
			$folder = "myhotel/images/";
			$target = $folder.$imagename;
			move_uploaded_file($source, $upload_base . $target);

			$file = $target; //This is the original file

			//-------- menghapus gambar lama
			if($oldphotos!=""){
				$oldfile= str_replace($web_url.'/'.$folder,'',$oldphotos);
				if(file_exists('images/'.$oldfile)){ unlink('images/'.$oldfile); }
			}

			//-------- menurunkan resolusi gambar
			list($width, $height) = getimagesize($upload_base . $file) ;

			$save = $folder . $imagename_t; //This is the new file you saving
			if($width < 500 or $height < 500){
				$modwidth = $width * 50 /100;
				$modheight = $height * 50 /100;
			}else{
				$modwidth = $width * 30 /100;
				$modheight = $height * 30 /100;
			}
			$tn = imagecreatetruecolor($modwidth, $modheight) ;
			switch($filetype) {
				case "image/gif":	$image = imagecreatefromgif($upload_base . $file); break;
				case "image/jpeg": $image = imagecreatefromjpeg($upload_base . $file); break;
				case "image/png": $image = imagecreatefrompng($upload_base . $file); break;
			}

			//prevent tranparant background turn into black
		  if($filetype == "image/gif" or $filetype == "image/png"){
		    imagecolortransparent($tn, imagecolorallocatealpha($tn, 0, 0, 0, 127));
		    imagealphablending($tn, false);
		    imagesavealpha($tn, true);
		  }


			if(file_exists('images/'.$imagename)){ unlink('images/'.$imagename); } //delete original file
			imagecopyresampled($tn, $image, 0, 0, 0, 0, $modwidth, $modheight, $width, $height) ;
			saveImage($tn, $upload_base . $save, 75);
	}

	if($filename2!=''){
			//-------- mengupload gambar ke server
			$source2 = $_FILES['icon']['tmp_name'];
			$folder = "myhotel/images/";
			$target2 = $folder.$imagename2;
			move_uploaded_file($source2, $upload_base . $target2);

			$file2 = $target2; //This is the original file

			//-------- menghapus gambar lama
			if($oldicon!=""){
				$oldfile2= str_replace($web_url.'/'.$folder,'',$oldicon);
				if(file_exists('images/'.$oldfile2)){ unlink('images/'.$oldfile2); }
			}

			//-------- menurunkan resolusi gambar
			list($width2, $height2) = getimagesize($upload_base . $file2) ;

			$save2 = $folder . $imagename_t2; //This is the new file you saving
			if($width2 < 500 or $height2 < 500){
				$modwidth2 = $width2 * 50 /100;
				$modheight2 = $height2 * 50 /100;
			}else{
				$modwidth2 = $width2 * 30 /100;
				$modheight2 = $height2 * 30 /100;
			}
			$tn2 = imagecreatetruecolor($modwidth2, $modheight2);
			switch($filetype2) {
				case "image/gif":	$image2 = imagecreatefromgif($upload_base . $file2); break;
				case "image/jpeg": $image2 = imagecreatefromjpeg($upload_base . $file2); break;
				case "image/png": $image2 = imagecreatefrompng($upload_base . $file2);
				break;
			}

			//prevent tranparant background turn into black
		  if($filetype2 == "image/gif" or $filetype2 == "image/png"){
		    imagecolortransparent($tn2, imagecolorallocatealpha($tn2, 0, 0, 0, 127));
		    imagealphablending($tn2, false);
		    imagesavealpha($tn2, true);
		  }

			if(file_exists('images/'.$imagename2)){ unlink('images/'.$imagename2); }//delete original file
			imagecopyresampled($tn2, $image2, 0, 0, 0, 0, $modwidth2, $modheight2, $width2, $height2) ;
			saveImage($tn2, $upload_base . $save2, 75);
	}
	
	if($filename3!=''){
		//-------- mengupload gambar ke server
		$source3 = $_FILES['favicon']['tmp_name'];
		$folder = "myhotel/images/";
		$target3 = $folder.$imagename3;
		move_uploaded_file($source3, $upload_base . $target3);

		$file3 = $target3; //This is the original file

		//-------- menghapus gambar lama
		if($oldfavicon!=""){
			$oldfile3= str_replace($web_url.'/'.$folder,'',$oldfavicon);
			if(file_exists('images/'.$oldfile3)){ unlink('images/'.$oldfile3); }
		}

		//-------- menurunkan resolusi gambar
		list($width3, $height3) = getimagesize($upload_base . $file3) ;

		$save3 = $folder . $imagename_t3; //This is the new file you saving
		if($width3 < 500 or $height3 < 500){
			$modwidth3 = $width3 * 50 /100;
			$modheight3 = $height3 * 50 /100;
		}else{
			$modwidth3 = $width3 * 30 /100;
			$modheight3 = $height3 * 30 /100;
		}
		$tn3 = imagecreatetruecolor($modwidth3, $modheight3);
		switch($filetype3) {
			case "image/gif":	$image3 = imagecreatefromgif($upload_base . $file3); break;
			case "image/jpeg": $image3 = imagecreatefromjpeg($upload_base . $file3); break;
			case "image/png": $image3 = imagecreatefrompng($upload_base . $file3);
			break;
		}

		//prevent tranparant background turn into black
	  if($filetype3 == "image/gif" or $filetype3 == "image/png"){
		imagecolortransparent($tn3, imagecolorallocatealpha($tn3, 0, 0, 0, 127));
		imagealphablending($tn3, false);
		imagesavealpha($tn3, true);
	  }

		if(file_exists('images/'.$imagename3)){ unlink('images/'.$imagename3); }//delete original file
		imagecopyresampled($tn3, $image3, 0, 0, 0, 0, $modwidth3, $modheight3, $width3, $height3) ;
		saveImage($tn3, $upload_base . $save3, 75);
}

if($filename4!=''){
	//-------- mengupload gambar ke server
	$source4 = $_FILES['photos2']['tmp_name'];
	$folder = "myhotel/images/";
	$target4 = $folder.$imagename4;
	move_uploaded_file($source4, $upload_base . $target4);

	$file4 = $target4; //This is the original file

	//-------- menghapus gambar lama
	if($oldlogo2!=""){
		$oldfile4= str_replace($web_url.'/'.$folder,'',$oldlogo2);
		if(file_exists('images/'.$oldfile4)){ unlink('images/'.$oldfile4); }
	}

	//-------- menurunkan resolusi gambar
	list($width4, $height4) = getimagesize($upload_base . $file4) ;

	$save4 = $folder . $imagename_t4; //This is the new file you saving
	if($width4 < 500 or $height4 < 500){
		$modwidth4 = $width4 * 50 /100;
		$modheight4 = $height4 * 50 /100;
	}else{
		$modwidth4 = $width4 * 30 /100;
		$modheight4 = $height4 * 30 /100;
	}
	$tn4 = imagecreatetruecolor($modwidth4, $modheight4);
	switch($filetype4) {
		case "image/gif":	$image4 = imagecreatefromgif($upload_base . $file4); break;
		case "image/jpeg": $image4 = imagecreatefromjpeg($upload_base . $file4); break;
		case "image/png": $image4 = imagecreatefrompng($upload_base . $file4);
		break;
	}

	//prevent tranparant background turn into black
  if($filetype4 == "image/gif" or $filetype4 == "image/png"){
	imagecolortransparent($tn4, imagecolorallocatealpha($tn4, 0, 0, 0, 127));
	imagealphablending($tn4, false);
	imagesavealpha($tn4, true);
  }

	if(file_exists('images/'.$imagename4)){ unlink('images/'.$imagename4); }//delete original file
	imagecopyresampled($tn4, $image4, 0, 0, 0, 0, $modwidth4, $modheight4, $width4, $height4) ;
	saveImage($tn4, $upload_base . $save4, 75);
}
?>
