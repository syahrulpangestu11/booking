<script type="text/javascript">
$(function(){
   $("#dialog-error, #dialog-success").dialog({
      autoOpen: false,
	  buttons: { "Ok": redirect },
	  close: function(event, ui) { redirect }
   });

   function redirect(){
	   $(location).attr("href", "<?php echo $base_url; ?>/webprofile");
   }
});

function openDialog($type,$message,$warning){
  var $dialog = $("#dialog-"+$type);
  $dialog.find(".description").empty();
  if($message!=undefined){ $dialog.find(".description").text($message); }
  if($warning!=undefined){ console.log($warning); }

  $(function(){ $( document ).ready(function(){ $dialog.dialog("open"); }); });
}
</script>

<div id="dialog-error" title="Error Notification">
  <p>We&rsquo;re very sorry, we can't save your data right now.<br />Please make sure you upload image file type (ex: .jpg , .png , .gif)</p><p class="description"></p>
</div>
<div id="dialog-success" title="Confirmation Notice">
  <p>Data succesfully updated</p><p class="description"></p>
</div>

<?php

$inquiry = $_POST['inquiry'];
$inquiry_wording = $_POST['inquiry_wording'];
$name = $_POST['name'];
$shortcode = $_POST['shortcode'];
$oldphotos = $_POST['oldphotos'];
$oldphotos2 = $_POST['oldphotos2'];
// $imglogo = $_POST['photos'];
$oldicon = $_POST['oldicon'];
$oldfavicon = $_POST['oldfavicon'];
// $imgicon = $_POST['icon'];
$description = $_POST['description'];
$aboutus = $_POST['aboutus'];
$email = $_POST['email'];
$web = $_POST['web'];
$phone = $_POST['phone'];
$fax = $_POST['fax'];
$whatsapp = $_POST['whatsapp'];
$address = $_POST['address'];
$address = $_POST['address'];
$city = $_POST['city'];
$socialmediaurl = $_POST['socialmediaurl'];
$privacypolicy = $_POST['privacypolicy'];
$termscondition = $_POST['termscondition'];

	function logJS($data,$title=""){
		if(!empty($title)){
			echo '<script>console.log("'.$title.' : ");</script>';
		}
		echo '<script>console.log('.json_encode($data).');</script>';
	}

  function getFileName($name1,$extension1){
    $filename1='';
    if($name1!=''){
      $filename1 = strtolower($name1);
  		$filename1 = str_replace(" ", "_", $filename1);
  		$filename1 = str_replace(".".$extension1, "", $filename1);
  		$filename1 = substr($filename1, 0, 20);
    }
    return $filename1;
  }
  function checkExistAndType($filename1,$filetype1,$extension1){
    $sts = false;
    if($filename1!=''){
        $allowedExts = array("jpg", "jpeg", "gif", "png");
        if((    ($filetype1 == "image/gif")
          || ($filetype1 == "image/jpeg")
          || ($filetype1 == "image/png")
          || ($filetype1 == "image/pjpeg")
          )  && in_array(strtolower($extension1), $allowedExts)){
            $sts=true;
        }
    }else{
      $sts=true;
    }
    return $sts;
  }
  function checkExistAndError($filename1,$fileerror1){
    return ($filename1!='' && $fileerror1 > 0 ? true : false);
  }

	if((!empty($_FILES["photos"]["name"]) or $_FILES["photos"]["name"]!='')
    ||(!empty($_FILES["icon"]["name"]) or $_FILES["icon"]["name"]!='')
    ||(!empty($_FILES["favicon"]["name"]) or $_FILES["favicon"]["name"]!='')
    ||(!empty($_FILES["photos2"]["name"]) or $_FILES["photos2"]["name"]!='')){

    //variable for image logo
    $fileerror = $_FILES["photos"]["error"] ;
    $filetype = $_FILES["photos"]["type"] ;
    $filename_ori = $_FILES["photos"]["name"];
    $tmp = explode(".", $filename_ori);
    $extension = end($tmp);
		$ext = ".".strtolower($extension);
		$filename = getFileName($filename_ori,$extension);
		$uploadedname = "logo_".$filename."-".date("Y_m_d_H_i_s");

    //variable for image icon
    $fileerror2 = $_FILES["icon"]["error"] ;
    $filetype2 = $_FILES["icon"]["type"] ;
    $filename_ori2 = $_FILES["icon"]["name"];
    $tmp2 = explode(".", $filename_ori2);
    $extension2 = end($tmp2);
		$ext2 = ".".strtolower($extension2);
    $filename2 = getFileName($filename_ori2,$extension2);
    $uploadedname2 = "icon_".$filename2."-".date("Y_m_d_H_i_s");
    
    //variable for image favicon
    $fileerror3 = $_FILES["favicon"]["error"] ;
    $filetype3 = $_FILES["favicon"]["type"] ;
    $filename_ori3 = $_FILES["favicon"]["name"];
    $tmp3 = explode(".", $filename_ori3);
    $extension3 = end($tmp3);
		$ext3 = ".".strtolower($extension3);
    $filename3 = getFileName($filename_ori3,$extension3);
		$uploadedname3 = "favicon_".$filename3."-".date("Y_m_d_H_i_s");

    //variable for image logo 2
    $fileerror4 = $_FILES["photos2"]["error"] ;
    $filetype4 = $_FILES["photos2"]["type"] ;
    $filename_ori4 = $_FILES["photos2"]["name"];
    $tmp4 = explode(".", $filename_ori4);
    $extension4 = end($tmp4);
		$ext4 = ".".strtolower($extension4);
		$filename4 = getFileName($filename_ori4,$extension4);
    $uploadedname4 = "logo2_".$filename4."-".date("Y_m_d_H_i_s");
    
    if (checkExistAndType($filename,$filetype,$extension) 
        && checkExistAndType($filename2,$filetype2,$extension2)
        && checkExistAndType($filename3,$filetype3,$extension3) 
        && checkExistAndType($filename4,$filetype4,$extension4) ){
      if (checkExistAndError($filename,$fileerror) 
          || checkExistAndError($filename2,$fileerror2)
          || checkExistAndError($filename3,$fileerror3) 
          || checkExistAndError($filename4,$fileerror4) ){
            logJS(1);
  			?>
    			<script>openDialog("error"); </script>
        <?php
			}else{
				include"compress-image.php"; //function for upload and compress image
        $show_thumbnail_path = ($filename!=""?$web_url."/".$folder.$imagename_t:$oldphotos);
        $show_thumbnail_path2 = ($filename2!=""?$web_url."/".$folder.$imagename_t2:$oldicon);
        $show_thumbnail_path3 = ($filename3!=""?$web_url."/".$folder.$imagename_t3:$oldfavicon);
        $show_thumbnail_path4 = ($filename4!=""?$web_url."/".$folder.$imagename_t4:$oldphotos2);
				try {
            $stmt = $db->prepare("UPDATE masterprofile SET
                                        name = :a, description = :b, email = :c, phone = :d, whatsapp = :e,
                                        address = :f, socialmediaurl = :g, privacypolicy = :h, termscondition = :i,
                                        updated = :j, updatedby = :k, 
                                        imglogo = :l, imgicon = :m, imgfavicon = :n, imglogo2 = :o,
                                        aboutus = :p, inquiry_status = :q, cityoid = :r, inquiry_wording = :s, shortcode = :t, fax = :u, web=:v
                                  WHERE masterprofileoid = :id");
            $stmt->execute(array(':a' => $name, ':b' => $description, ':c' => $email, ':d' => $phone, ':e' => $whatsapp,
                                  ':f' => $address, ':g' => $socialmediaurl, ':h' => $privacypolicy, ':i' => $termscondition,
                                  ':j' => date("Y-m-d H:i:s"), ':k' => $_SESSION['_user'],
                                  ':l' => $show_thumbnail_path, ':m' => $show_thumbnail_path2,
                                  ':n' => $show_thumbnail_path3, ':o' => $show_thumbnail_path4,
                                  ':p' => $aboutus, ':q' => $inquiry, ':r' => $city, ':s'=> $inquiry_wording, ':t'=> $shortcode, ':u'=> $fax, ':v'=> $web  ,':id' => '1'));

					// $_SESSION['_initial'] = $displayname;
		      ?>
    			   <script>openDialog("success"); </script>
          <?php
				}catch(PDOException $ex) {
          logJS(2);
          ?>
    			<script>openDialog("error","",<?=json_encode($ex->getMessage());?>); </script>
        <?php
				}

			}
		}else{
      logJS(3);
      ?>
			<script>openDialog("error"); </script>
		<?php
		}

	}else{
    try {
      $stmt = $db->prepare("UPDATE masterprofile SET
                                  name = :a, description = :b, email = :c, phone = :d, whatsapp = :e,
                                  address = :f, socialmediaurl = :g, privacypolicy = :h, termscondition = :i,
                                  updated = :j, updatedby = :k, aboutus = :l, inquiry_status = :m, cityoid = :n,inquiry_wording=:o,shortcode=:p,fax=:q,web=:r
                            WHERE masterprofileoid = :id");
      $stmt->execute(array(':a' => $name, ':b' => $description, ':c' => $email, ':d' => $phone, ':e' => $whatsapp,
                            ':f' => $address, ':g' => $socialmediaurl, ':h' => $privacypolicy, ':i' => $termscondition,
                            ':j' => date("Y-m-d H:i:s"), ':k' => $_SESSION['_user'], ':id' => '1', ':l' => $aboutus, 
                            ':m' => $inquiry, ':n' => $city, ':o'=> $inquiry_wording,':p'=>$shortcode,':q'=>$fax,':r'=>$web));
  		?>
         <script>openDialog("success"); </script>
  		<?php
		}catch(PDOException $ex) {
      logJS(4);
  		?>
        <script>openDialog("error","",<?=json_encode($ex->getMessage());?>); </script>
  		<?php
		}
	}
?>
