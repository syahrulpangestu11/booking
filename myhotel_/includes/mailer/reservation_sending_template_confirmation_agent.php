<?php



$mail_agent = new PHPMailer(true); // the true param means it will throw exceptions on errors, which we need to catch
$mail_agent->IsSMTP(); // telling the class to use SMTP
try {
    
    $sql_mailer = "SELECT * FROM mailercompany INNER JOIN mailertype USING (mailertypeoid) WHERE companyoid='1' and publishedoid='1'";
    $qrun_mailer = $db->query($sql_mailer);
    $run_mailer = $qrun_mailer->fetchAll(PDO::FETCH_ASSOC);
    foreach($run_mailer as $row_mailer){
        switch($row_mailer['mailertypeoid']){
            case 1 : $mail_agent->SetFrom($_profile['SMTP_Username'], $row_mailer['name']); break;
            case 2 : $mail_agent->AddAddress($row_mailer['email'], $row_mailer['name']); break;
            case 3 : $mail_agent->AddCC($row_mailer['email'], $row_mailer['name']); break;
            case 4 : $mail_agent->AddBCC($row_mailer['email'], $row_mailer['name']); break;
            case 5 : $mail_agent->AddReplyTo($row_mailer['email'], $row_mailer['name']); break;
        }
    }

    $mail_agent->AddAddress($agent_mail,$agent_name);
    $mail_agent->Subject = $_profile['name']." [A] Booking Confirmation for [".$hotelname."] - [".$bookingnumber."]";
    $mail_agent->AltBody = 'To view the message, please use an HTML compatible email viewer!'; // optional - MsgHTML will create an alternate automatically
    $mail_agent->MsgHTML($body_send_to_agent);
    if($mail_agent->Send()) {
        include('reservation_sending_template_confirmation_hotel.php');
    }
    
} catch (phpmailerException $e) { echo"<b>Php Mailer Error [agent] :</b><br>"; echo $e->errorMessage();  //Pretty error messages from PHPMailer
} catch (Exception $e) { echo"<b>All Error :</b><br>"; echo $e->getMessage(); //Boring error messages from anything else!
} 

?>