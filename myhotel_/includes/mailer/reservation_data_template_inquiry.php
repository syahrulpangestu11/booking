<?php

$s_data="select booking.*, customer.*, currency.*, bookingstatus.note as status, bookingpayment.*, country.countryname 
from booking 
inner join bookingstatus using (bookingstatusoid) 
inner join currency using (currencyoid) 
inner join customer using (custoid) 
inner join country using (countryoid) 
left join bookingpayment using (bookingoid) 
where booking.bookingoid = '".$bookingoid."' group by booking.bookingoid";
$q_data = $db->query($s_data);
$data = $q_data->fetch(PDO::FETCH_ASSOC);

$user_ip = $data['user_ip'];
$user_browser = $data['bookingnumber'];
$bookingnumber = $data['bookingnumber'];
$email = $data['email'];
$phone = $data['phone'];
$firstname = $data['firstname'];
$lastname = $data['lastname'];
$country = $data['countryname'];
$grandtotal = $data['currencycode']." ".number_format($data['grandtotal']);
$hotelcollect = $data['currencycode']." ".number_format($data['hotelcollect']);

$guestname = $firstname." ".$lastname;
$guestemail = $data['email'];
    
$query_i = "select booking.*, h.hoteloid, h.hotelname, ct.cityname ,h.email as hotelemail from booking 
            inner join hotel h using (hoteloid)  inner join city ct using (cityoid)
            where bookingoid='$bookingoid'";
$i_data = $db->query($query_i);
$inq = $i_data->fetch(PDO::FETCH_ASSOC);

$inquiry_status=($inq['bookingstatusoid']==1?true:false);
$hoteloid = $inq['hoteloid'];
$hotelname = $inq['hotelname'];
$hotelemail = $hotel['hotelemail'];
// $min_checkin = date("d F Y", strtotime($inq['checkin']));
$min_checkin = '';
$max_checkin = '';
$currency = $data['currencycode'];
$roomtotal = $data['currencycode']." ".number_format(0);
$extrabedtotal = $data['currencycode']." ".number_format(0);
$extrabedtotal_nominal = 0;
$cityname = $inq['cityname'];

$ar_note = array();
$ar_roomname = array();
$ar_checkin = array();
$ar_checkout = array();
$ar_numberroom = array();
$ar_maxoccupancy = array();
$ar_total = array();
$ar_breakfast = array();
$ar_extrabed = array();
$ar_adult = array();
$ar_child = array();
    
$s_room_promo = "select br.*, b.hoteloid, 'INQUIRY' as roomname, br.adult as maxadult, ch.name as channelname, 
0 as jmlroom, 0 as total, 0 as extrabed, c.currencycode, checkin, checkout,b.note 
from bookingroom br
inner join currency c using (currencyoid)
inner join booking b using (bookingoid) 
inner join channel ch using (channeloid)
where b.bookingoid = '".$bookingoid."' ";
$q_room_promo = $db->query($s_room_promo);
$rp_data = $q_room_promo->fetchAll(PDO::FETCH_ASSOC);

foreach($rp_data as $rp){
    array_push($ar_roomname, $rp['roomname']);
    array_push($ar_checkin, date("d F Y", strtotime($rp['checkin'])));
    array_push($ar_checkout, date("d F Y", strtotime($rp['checkout'])));
    array_push($ar_numberroom, $rp['jmlroom']);
    array_push($ar_maxoccupancy, $rp['maxadult']);
    array_push($ar_total, $rp['currencycode']." - ".number_format($rp['total']));
    array_push($ar_breakfast, $rp['breakfast']);
    array_push($ar_extrabed, $rp['extrabed']);
    array_push($ar_note, $rp['note']);
    array_push($ar_adult, $rp['adult']);
    array_push($ar_child, $rp['child']);
}

$arrDataBooking = array();
$arrDataBooking['booking_min_checkin'] = $min_checkin;
$arrDataBooking['booking_max_checkout'] = $max_checkout;
$arrDataBooking['booking_number'] = $bookingnumber;
$arrDataBooking['booking_email_date'] = date("d F Y");
$arrDataBooking['booking_total_room_rate'] = $roomtotal;
$arrDataBooking['booking_total_extrabed_date'] = ($extrabedtotal_nominal>0?$min_checkin.' - '.$max_checkout:'-');
$arrDataBooking['booking_total_extrabed_rate'] = ($extrabedtotal_nominal>0?$extrabedtotal:'-');
$arrDataBooking['booking_grand_total'] = $grandtotal;
$arrDataBooking['booking_currency'] = $currency;//new
$arrDataBooking['booking_extra'] = '';//new
$arrDataBooking['booking_grand_deposit'] = '';//new
$arrDataBooking['booking_grand_balance'] = '';//new
$arrDataBooking['booking_grand_cancellation'] = '';//new
$arrDataBooking['booking_cancellation_reason'] = '';//new

$arrDataUser = array();
$arrDataUser['user_ip'] = $user_ip;//new
$arrDataUser['user_browser'] = $user_browser;//new

$arrDataHotel = array();
$arrDataHotel['hotel_banner'] = '';//new
$arrDataHotel['hotel_name'] = $hotelname;
$arrDataHotel['hotel_email'] = $hotelemail;
$arrDataHotel['hotel_address'] = $hoteladdress;
$arrDataHotel['hotel_city'] = $cityname;

$arrDataGuest = array();
$arrDataGuest['guest_email'] = $email;//new
$arrDataGuest['guest_phone'] = $phone;//new
$arrDataGuest['guest_firstname'] = $firstname;
$arrDataGuest['guest_lastname'] = $lastname;
$arrDataGuest['guest_country'] = $country;

$arrDataRoom = array(); $num=0;
foreach($ar_roomname as $key => $value){
    $tmp = array();$num++;
    $tmp['no'] = $num;//
    $tmp['country'] = $country;
    $tmp['occupancy'] = $ar_maxoccupancy[$key];
    $tmp['adult'] = $ar_adult[$key];
    $tmp['child'] = $ar_child[$key];
    $tmp['checkin'] = $ar_checkin[$key];
    $tmp['checkout'] = $ar_checkout[$key];
    $tmp['room'] = $value;
    $tmp['note'] = $ar_note[$key];
    $tmp['extrabed'] = $ar_extrabed[$key];
    $tmp['inclusion'] = (empty($ar_extrabed[$key])?'':$ar_extrabed[$key].' Extrabed(s)');
    $tmp['number_room'] = $ar_numberroom[$key];
    $tmp['breakfast'] = $ar_breakfast[$key];
    $tmp['room_total'] = '';
    $tmp['total'] = $ar_total[$key];
    array_push($arrDataRoom, $tmp);
}

$arrDataExtra = array();

$arrDataTemplate = array();
$arrDataTemplate['company'] = $_profile;
$arrDataTemplate['booking'] = $arrDataBooking;
$arrDataTemplate['user'] = $arrDataUser;
$arrDataTemplate['hotel'] = $arrDataHotel;
$arrDataTemplate['guest'] = $arrDataGuest;
$arrDataTemplate['_detail'] = $arrDataRoom;
$arrDataTemplate['_detail_extra'] = $arrDataExtra;
?>