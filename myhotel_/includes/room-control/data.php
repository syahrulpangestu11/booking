<?php
include("../ajax-include-file.php");

$startdate = date("Y-m-d", strtotime($_REQUEST['startdate']));
$enddate = date("Y-m-d", strtotime($_REQUEST['enddate']));
$roomoffer = $_REQUEST['rt'];
$channeltype = $_REQUEST['ch'];
try {	
	$stmt = $db->query("SELECT DATEDIFF('".$enddate."','".$startdate."') AS DiffDate");
	$r_datediff = $stmt->fetchAll(PDO::FETCH_ASSOC);
	foreach($r_datediff as $row){
		$diff = $row['DiffDate'];
	}
}catch(PDOException $ex) {
	echo "Invalid Query";
	die();
}

	try {
		$stmt = $db->query("select h.hoteloid, h.hotelcode, r.roomoid from room r inner join hotel h using (hoteloid) inner join roomoffer ro using (roomoid) where ro.roomofferoid = '".$roomoffer."'");
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
			$r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_room as $row){
				$hoteloid = $row['hoteloid'];
				$hcode = $row['hotelcode'];
				$roomoid = $row['roomoid'];
			}
		}else{
			echo "No Result";
			die();
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}


?>
    <input type="hidden" name="roomoid" value="<?php echo $roomoid; ?>" />
    <input type="hidden" name="roomoffer" value="<?php echo $roomoffer; ?>" />
    <input type="hidden" name="channeloid" value="<?php echo $channeltype; ?>" />
    <input type="hidden" name="startdate" value="<?php echo $_REQUEST['startdate']; ?>" />
    <input type="hidden" name="enddate" value="<?php echo $_REQUEST['enddate']; ?>" />
<table class="table table-fill table-fill-centered">
    <tr>
        <td>Date</td>
        <td>Day</td>
        <td>Close Out Regular</td>
        <td>Closed to Arrival</td>
        <td>Closed to Departure</td>
        <td>&nbsp;</td>
    </tr>
    <tr class="autofilled">
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>
            <select name="co_reg" tgt="blackout">
                <option value="">&nbsp;</option>
                <option value="n">no</option>
                <option value="y">yes</option>
            </select>
        </td>
        <td>
            <select name="co_arr" tgt="cta">
                <option value="">&nbsp;</option>
                <option value="y">yes</option>
                <option value="n">no</option>
            </select>
        </td>
        <td>
            <select name="co_dept" tgt="ctd">
                <option value="">&nbsp;</option>
                <option value="y">yes</option>
                <option value="n">no</option>
            </select>
        </td>
        <td><button type="button" class="small-button orange">Auto Fill</button></td>
    </tr>
    <?php
	$xml =  simplexml_load_file("../../data-xml/".$hcode.".xml");
	
    for($i=0;$i<=$diff;$i++){
        $dateFormat = explode("/",date("D/Y-m-d/d F Y",strtotime($startdate." +".$i." day")));
        $day = $dateFormat[0]; 
        $date = $dateFormat[1];
        $show_date = $dateFormat[2];
        
        list($check_regular, $check_arrival, $check_departure) = array("","","");

        $query_tag = '//hotel[@id="'.$hoteloid.'"]/masterroom[@id="'.$roomoid.'"]/rate[@date="'.$date.'"]/rateplan[@id="'.$roomoffer.'"]/channel[@type="'.$channeltype.'"]';
        foreach($xml->xpath($query_tag) as $rate) { 	
		    list($blackout, $cta, $ctd) = array($rate->blackout , $rate->cta , $rate->ctd);
		}
		
		$blakout_check = ''; $cta_check = ''; $ctd_check = '';
		if(isset($blackout) and $blackout == "y"){ $blakout_check = 'checked = "checked"'; }
		if(isset($cta) and $cta == "y"){ $cta_check = 'checked = "checked"'; }
		if(isset($ctd) and $ctd == "y"){ $ctd_check = 'checked = "checked"'; }
    ?>
    <tr class="list">
        <td><?php echo $show_date; ?></td>
        <td><?php echo $day; ?></td>
        <td class="algn-center">
            <input type="hidden" name="date-<?php echo $i; ?>" value="<?php echo $date; ?>" />
            <input type="checkbox" tgt="blackout" name="blackout-<?php echo $i; ?>" value="y" <?php echo $blakout_check; ?> />
        </td>
        <td class="algn-center">
            <input type="checkbox" tgt="cta"  name="cta-<?php echo $i; ?>" value="y" <?php echo $cta_check; ?> />
        </td>
        <td class="algn-center">
            <input type="checkbox" tgt="ctd"  name="ctd-<?php echo $i; ?>" value="y" <?php echo $ctd_check; ?> />
        </td>
        <td>&nbsp;</td>
    </tr>	
    <?php
    }
    ?>
</table> 
<br /><br />
<button type="submit" class="small-button blue"><i class="fa fa-save"></i>Save</button>
&nbsp;&nbsp;
<button type="reset" class="small-button blue">Reset</button>
