<?php
class Logging{
    public $hotelcode, $log_file, $upload_base, $ip_address, $browser, $monthyear;

    // construct
    public function  __construct($hotelcode, $custom=""){
      $this->hotelcode = $hotelcode;
      $this->uploadBase();
      if($custom != "no-open"){
        $this->openLog();
      }
      $this->getIPAddress();
      $this->browser = $this->getBrowser();
    }

    public function uploadBase(){
      $upload_base = $_SERVER['DOCUMENT_ROOT'];
    	$exp = explode("/", dirname($_SERVER['PHP_SELF']));
      array_pop($exp);
      array_pop($exp);
      $imp = implode("/", $exp);
    	$upload_base = $upload_base.$imp."/";

      $this->upload_base = $upload_base;
    }

    public function setLogPeriode($month, $year){
      $this->monthyear = $year.$month;
    }

    // check path (create if doesn't available)
    public function openLog(){
      if(empty($this->monthyear)){
        $this->monthyear = date('Ym');
      }
      $path_directory = $this->upload_base."log/".$this->monthyear;
      $this->upload_base = $path_directory;
      // create directory if doesn't exist
      if(!file_exists($path_directory)) {
        mkdir($path_directory, 0777, true);
      }
      
      // open file & create if doesn't exist
      $path_url = $path_directory."/apps-".$this->hotelcode."-".$this->monthyear.".log";
      if(file_exists($path_url)) {
        $log_file = fopen($path_url, "a") or die("Unable to open file!");
      }else{
        $log_file = fopen($path_url, "w") or die("Unable to open file!");
      }
      $this->log_file = $log_file;
    }

    public function closeLog(){
      fclose($this->log_file);
    }

    public function getIPAddress(){
      if(!empty($_SERVER['HTTP_CLIENT_IP'])){
        $ip = $_SERVER['HTTP_CLIENT_IP'];
      }else if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
      }else{
        $ip = $_SERVER['REMOTE_ADDR'];
      }
      $this->ip_address = $ip;
    }

    public function createLog($log_value){
      $datetime = date('[Y-m-d H:i:s]');
      $value = implode(" ~ ", $log_value);
      fwrite($this->log_file, $this->ip_address." ~ ".$datetime." ~ ".$value." ~ ".$_SESSION['_user']." ~ ".$this->browser['name'].PHP_EOL);
    }

    public function getBrowser(){
      $u_agent = $_SERVER['HTTP_USER_AGENT'];
      $bname = 'Unknown';
      $platform = 'Unknown';
      $version= "";

      //First get the platform?
      if (preg_match('/linux/i', $u_agent)) {
        $platform = 'linux';
      }else if (preg_match('/macintosh|mac os x/i', $u_agent)) {
        $platform = 'mac';
      }else if (preg_match('/windows|win32/i', $u_agent)) {
        $platform = 'windows';
      }

      // Next get the name of the useragent yes seperately and for good reason
      if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)){
        $bname = 'Internet Explorer';
        $ub = "MSIE";
      }elseif(preg_match('/Firefox/i',$u_agent)){
        $bname = 'Mozilla Firefox';
        $ub = "Firefox";
      }elseif(preg_match('/Chrome/i',$u_agent)){
        $bname = 'Google Chrome';
        $ub = "Chrome";
      }elseif(preg_match('/Safari/i',$u_agent)){
        $bname = 'Apple Safari';
        $ub = "Safari";
      }elseif(preg_match('/Opera/i',$u_agent)){
        $bname = 'Opera';
        $ub = "Opera";
      }
      elseif(preg_match('/Netscape/i',$u_agent))
      {
        $bname = 'Netscape';
        $ub = "Netscape";
      }

      // finally get the correct version number
      $known = array('Version', $ub, 'other');
      $pattern = '#(?<browser>' . join('|', $known) .
      ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
      if (!preg_match_all($pattern, $u_agent, $matches)) {
        // we have no matching number just continue
      }

      // see how many we have
      $i = count($matches['browser']);
      if ($i != 1) {
        //we will have two since we are not using 'other' argument yet
        //see if version is before or after the name
        if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
          $version= $matches['version'][0];
        }else {
          $version= $matches['version'][1];
        }
      }else {
        $version= $matches['version'][0];
      }

      // check if we have a number
      if ($version==null || $version=="") {$version="?";}

      return array(
        'userAgent' => $u_agent,
        'name'      => $bname,
        'version'   => $version,
        'platform'  => $platform,
        'pattern'    => $pattern,
      );
    }
}
?>
