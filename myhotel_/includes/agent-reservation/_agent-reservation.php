<style media="screen">
  .room-image img{
    width: 100px;
  }
</style>

<section class="content-header">
    <h1>
        Agent Reservation
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-laptop"></i> Settings</a></li>
        <li>Hotel Profile</li>
        <li class="active">Basic Info</li>
    </ol>
</section>
<section class="content">

  <div class="row">
      <div class="box">
          <div class="box-body">
              <div class="form-group">
          <form method="get" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/room-control/">
                  <label>Arrival</label>
                  &nbsp;
                  <input type="text" name="sdate" id="startdate" placeholder="" value="<?php echo $start; ?>">
                  &nbsp;&nbsp;

                  <label>Departure </label>
                  &nbsp;
                  <input type="text" name="edate" id="enddate" placeholder="" value="<?php echo $end; ?>">
                  &nbsp;&nbsp;

                  <label>Room(s) </label>
                  &nbsp;
                  <select name="room" style="width: 53px;">
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                  </select>
                  &nbsp;&nbsp;

                  <label>Person(s)</label>
                  &nbsp;
                  <select name="person" style="width: 60px;">
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                  </select>



                  <button type="submit" class="small-button blue"><i class="fa fa-search"></i>Check Availability</button>
              </form>
              </div>
          </div><!-- /.box-body -->
     </div>
  </div>

  <div class="row">
    <div class="box box-form">
      <div class="box-header with-border">
          <h1>Choose room</h1>
        </div>
        <div class="box-body">
          <table class="table table-fill table-fill-centered">
              <tr>
              	<td>Booking Number</td>
                  <td>Booking Status</td>
                  <td>Guest Name</td>
                  <td>Book Date</td>
                  <td>Checkin Date</td>
                  <td>Checkout Date</td>
                  <td>&nbsp;</td>
              </tr>
              <?php for ($i=0; $i < 2; $i++) { ?>
              <tr class="list">
              	  <td><div class="room-image"><img src="http://thebuking.com/cp-beind/picture/room/suar1.jpg" alt=""></div></td>
                  <td>in booking process</td>
                  <td>Vijay India</td>
                  <td>15 January 2017</td>
                  <td>15 January 2017<br></td>
                  <td>18 January 2017<br></td>
                  <td class="algn-center">
                  </td>
              </tr>
              <?php } ?>
          </table>

          <ul class="block">
            <?php for ($i=0; $i < 4; $i++) { ?>
            <li>
              <ul class="inline-block">
              <li class="room-image side-lef"><img class="img-responsive hoverZoomLink" src="http://thebuking.com/cp-beind/picture/room/suar1.jpg" alt=""></li>
              <li class="side-lef"><h3 class="offer-info popup-trigger" href="#room-popup-0953104">Deluxe Room Only</h3>
                <ul class="inline-block room-headline">
                    <li><i class="fa fa-expand"></i> 28.7 sqm</li>
                    <li><i class="fa fa-male"></i> <i class="fa fa-female"></i> 2 pax</li>
                    <!-- <li><i class="fa fa-hotel"></i> <a class="room-info popup-trigger" href="#room-popup-0953104">Description</a></li> -->
                    <li><i class="fa fa-eye"></i> City</li>
                    <li><i class="fa fa-yelp"></i> Air Conditioning</li>
                    <li><i class="fa fa-wifi"></i> Wifi</li>
                </ul>
              </li>
              <li><button type="submit" class="small-button blue">Select Room</button></li>
              <div class="clear"></div>
            </li>
            <?php } ?>
          </ul>
        </div>
  </div>

  <div class="row">
    <div class="box box-form">
      <div class="box-header with-border">
          <h1>Reservation Summary</h1>
        </div>
        <div id="data-box" class="box-body">
          <form class="process-booking" method="post" action="index.php?page=payment&hcode=<?=$hotelcode?>">
              <div id="summary">
                  <ul id="breakdown-total" class="block">
                  </ul>
                  <ul class="ul-table" style="padding:5px;">
                      <li col="total"><span><b>Grand Total</b></span><span>0</span></li>
                      <li col="deposit"><span><b>Deposit Total</b></span><span>0</span></li>
                      <li col="balance"><span><b>Balance <small>(paid at hotel)</small></b></span><span>0</span></li>
                  </ul>
              </div>
          </form>
        </div>
        <div class="box-footer" align="center" style="padding-right: 10%;padding-top: 15px;">
          <button type="submit" class="small-button blue">Continue</button>
        </div>
  </div>

	<!-- <div class="row">
        <form method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/hotel-profile/basic-info/saveinfo">
        <input type="hidden" name="hoteloid" value="<?php echo $hoteloid; ?>">
        <div class="box box-form">
	        <div class="box-header with-border">
            	<h1>Reservation Summary</h1>
            </div>
            <div id="data-box" class="box-body">
            <ul class="inline form-input">
                <li>
                  <label style="display:block">Room Type</label>
                  <select name="country" class="input-select">
                      <?php
                          try {
                              $stmt = $db->query("select * from country");
                              $r_type = $stmt->fetchAll(PDO::FETCH_ASSOC);
                              foreach($r_type as $row){
                                  if($countryoid == $row['countryoid']){ $selected = "selected"; }else{ $selected=""; }
                                  echo"<option value='".$row['countryoid']."' ".$selected.">".$row['countryname']."</option>";
                              }
                          }catch(PDOException $ex) {
                              echo "Invalid Query";
                              die();
                          }
                      ?>
                  </select>
                </li>
                <li>
                    <div class="side-left"><label style="display:block">Property Name</label>
                    <input type="text" class="input-text" name="name" value="<?php echo $hotelname; ?>">
                    </div>
                    <div class="side-right"><label style="display:block">Location</label>
                        <select name="state" class="input-select">
                            <?php
                                try {
                                    $stmt = $db->query("select * from state");
                                    $r_type = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                    foreach($r_type as $row){
                                        if($stateoid == $row['stateoid']){ $selected = "selected"; }else{ $selected=""; }
                                        echo"<option value='".$row['stateoid']."' ".$selected.">".$row['statename']."</option>";
                                    }
                                }catch(PDOException $ex) {
                                    echo "Invalid Query";
                                    die();
                                }
                            ?>
                        </select>
                    </div>
                    <div class="clear"></div>
                </li>
                <li>
                    <div class="side-left"><label style="display:block">Star Rating</label>
                    <input type="text" class="input-text" name="star" value="<?php echo $star; ?>"></div>
                    <div class="side-right"><label style="display:block">Area</label>
                        <select name="city" class="input-select">
                            <?php
                                try {
                                    $stmt = $db->query("select * from city");
                                    $r_type = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                    foreach($r_type as $row){
                                        if($cityoid == $row['cityoid']){ $selected = "selected"; }else{ $selected=""; }
                                        echo"<option value='".$row['cityoid']."' ".$selected.">".$row['cityname']."</option>";
                                    }
                                }catch(PDOException $ex) {
                                    echo "Invalid Query";
                                    die();
                                }
                            ?>
                        </select>
                    </div>
                    <div class="clear"></div>
                </li>
                <li>
                    <div class="side-left"><label style="display:block">Property Type</label>
                        <select name="hoteltype" class="input-select">
                            <?php
                                try {
                                    $stmt = $db->query("select * from hoteltype");
                                    $r_type = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                    foreach($r_type as $row){
                                        if($hoteltype == $row['hoteltypeoid']){ $selected = "selected"; }else{ $selected=""; }
                                        echo"<option value='".$row['hoteltypeoid']."' ".$selected.">".$row['category']."</option>";
                                    }
                                }catch(PDOException $ex) {
                                    echo "Invalid Query";
                                    die();
                                }
                            ?>
                        </select>
                    </div>
                    <div class="side-right"><label style="display:block">Postal Code</label>
                    <input type="text" class="input-text" name="postalcode" value="<?php echo $postalcode; ?>"></div>
                    <div class="clear"></div>
                </li>
                <li>
                    <div class="side-left"><label style="display:block">Hotel Chain</label>
                    	<select name="chain" class="input-select">
                        	<option value="">- No Chain -</option>
                            <?php
                                try {
                                    $stmt = $db->query("select * from chain");
                                    $r_type = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                    foreach($r_type as $row){
                                        if($chainoid == $row['chainoid']){ $selected = "selected"; }else{ $selected=""; }
                                        echo"<option value='".$row['chainoid']."' ".$selected.">".$row['name']."</option>";
                                    }
                                }catch(PDOException $ex) {
                                    echo "Invalid Query";
                                    die();
                                }
                            ?>
                        </select>
                    </div>
                    <div class="side-right"><label style="display:block">Phone</label>
                    <input type="text" class="input-text" name="phone" value="<?php echo $hotelphone; ?>"></div>
                    <div class="clear"></div>
                </li>
                <li>
                    <div class="side-left"><label style="display:block">Street Address</label>
                    <textarea name="address"><?php echo $address; ?></textarea></div>
                    <div class="side-right"><label style="display:block">Fax</label>
                    <input type="text" class="input-text" name="fax" value="<?php echo $hotelfax; ?>"></div>
                    <div class="clear"></div>
                </li>
                <li>
                    <div class="side-left"><label style="display:block">Email</label>
                    <input type="text" class="input-text" name="email" value="<?php echo $hotelemail; ?>"></div>
                    <div class="side-right"><label style="display:block">Website</label>
                    <input type="text" class="input-text" name="website" value="<?php echo $hotelwebsite; ?>" ></div>
                    <div class="clear"></div>
                </li>
                <li>
                    <div class="side-left"><label style="display:block">Property URL</label>
                    <input type="text" class="input-text" name="htl_url" value="<?php echo $hotelhtlurl; ?>" ></div>
                    <div class="side-right"><label style="display:block">Favicon</label>
                    <input type="file" name="logo" /></div>
                    <div class="clear"></div>
                </li>
                <li>
                	<?php if(empty($hotelbeurl)){ $hotelbeurl = 'https://www.moh.com/ibeind/index.php?hcode='.$hotelcode;} ?>
                    <div class="side-left"><label style="display:block">Booking Engine URL</label>
                    <input type="text" class="input-text" name="be_url" value="<?php echo $hotelbeurl; ?>"></div>
                    <div class="side-right"></div>
                    <div class="clear"></div>
                </li>
                <li>
                    <div class="side-left"><label style="display:block">Property Description</label>
                    <textarea name="description"><?php echo $description; ?></textarea></div>
                    <div class="side-right"><label style="display:block">Property General Policy</label>
                    <textarea name="policy"><?php echo $policy; ?></textarea></div>
                    <div class="clear"></div>
                </li>
                <div class="clear"></div>
            </ul>
        	</div>
            <div class="box-footer" align="center" style="padding-right: 10%;padding-top: 15px;">
				<button type="submit" class="small-button blue">Save</button>
                <button type="reset" class="small-button blue">Reset</button>
            </div>
   		</div>
        </form>
    </div> -->


</section>
