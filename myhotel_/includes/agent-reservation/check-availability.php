<style>
div.room-image > img{
    width: 225px;
    height: 125px;
}
</style>
<ul>
<li>
<?php
$xml =  simplexml_load_file("data-xml/".$hcode.".xml"); 
$channeloid = 1;
$currencyoid = 1;
$agenthoteloid = 1;

		$syntax_currency = "select currencycode from currency where currencyoid = '".$currencyoid."'";
		$stmt	= $db->query($syntax_currency);
		$result_currency = $stmt->fetchAll(PDO::FETCH_ASSOC);
		foreach($result_currency as $currency){
			$currency = $currency['currencycode'];
		}

$listroom = array();

$s_room = "select ro.*, r.roomoid, r.name as masterroom, r.hoteloid from roomoffer ro inner join published p using (publishedoid) inner join room r using (roomoid) inner join offertype using (offertypeoid) where r.hoteloid = '".$hoteloid."' and p.publishedoid = '1'";
$stmt = $db->query($s_room);
$q_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
foreach($q_room as $room){
	$available = true;
	$breakdownrate = array();
	$roomtotal = 0; $roomwithsurcharge = 0;
	for($n=0; $n<$night; $n++){
		$date = date("Y-m-d",strtotime($checkin." +".$n." day"));
		
		$query_tag = '//hotel[@id="'.$room['hoteloid'].'"]/masterroom[@id="'.$room['roomoid'].'"]/rate[@date="'.$date.'"]/allotment/channel[@type="'.$channeloid.'"]/text()';
		foreach($xml->xpath($query_tag) as $tagallotment){
			$allotment = (int)$tagallotment[0];
		}
		
		if($allotment > 0){
			$syntax_bar = "select rate, surcharge from agenthotelrate where agenthoteloid = '".$agenthoteloid."' and roomofferoid = '".$room['roomofferoid']."' and '".$date."' >= startdate and '".$date."' <= enddate order by priority limit 1";
			$stmt = $db->query($syntax_bar);
			$row_bar = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			foreach($row_bar as $row){
				$rate = $row['rate'];
				$surcharge = $row['surcharge'];
			}
			
			if($rate > 0){
				$roomtotal = $roomtotal + $rate;
				$totalrate = $rate + $surcharge;
				$roomwithsurcharge = $roomwithsurcharge + $totalrate;
				array_push($breakdownrate, $totalrate);
			}else{
				$available = false;
				$break;
			}
			
		}else{
			$available = false;
			break;
		}
	}
	
	if($available == true){
		$s_picture = "SELECT photourl FROM hotelphoto hp WHERE hp.hoteloid = '".$room['hoteloid']."' AND hp.flag = 'main' AND ref_table = 'room' AND ref_id = '".$room['roomoid']."'";
		$stmt	= $db->query($s_picture);
		$picture = $stmt->fetch(PDO::FETCH_ASSOC);
		
		$dataroom = array($room['roomoid'], $room['masterroom'], $room['roomofferoid'], $room['name'], $picture['photourl'], $breakdownrate, $roomwithsurcharge);
		array_push($listroom, $dataroom);
	}
}

if(count($listroom) > 0){
	?>
    <table>
    <?php
	foreach($listroom as $room){
	?>
    <tr valign="top">
        <td><div class="room-image"><img class="img-responsive" src="<?=$room[4]?>" alt=""></div></td>
        <td><h2><?=$room[3]?></h2></td>
        <td>total <?=$currency?> <?=number_format($room[6])?> for <?=$night?> night(s)<br>avg <?=$currency?> <?=number_format($room[6]/$night)?> /night<br>
        <div><button type="button">Select Room</button></div>
        </td>
    </tr>
    <?php
	}
	?>
    </table>
<?php
}
?>
</li>
<li>
    
</li>
</ul>