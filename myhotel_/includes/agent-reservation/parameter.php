<?php
	function differenceDate($arrival, $departure){
		$date1 = new DateTime($arrival);
		$date2 = new DateTime($departure);
		$interval = $date1->diff($date2);
		return $interval->days;
	}

	$addtocheckout = 3;
	$checkin	= (isset($_REQUEST['checkin'])) ? date("Y-m-d", strtotime($_REQUEST['checkin'])) : date("Y-m-d",mktime(0,0,0,date("m"),date("d"),date("Y")));
	$checkout	= (isset($_REQUEST['checkout'])) ? date("Y-m-d", strtotime($_REQUEST['checkout'])) : date("Y-m-d",strtotime($_REQUEST['checkin'].' +'.$addtocheckout.' day'));
	
	$show_checkin = date("d F Y", strtotime($checkin));
	$show_checkout = date("d F Y", strtotime($checkout));
	
	$night = differenceDate($checkin, $checkout);
?>