<?php
	function dateDiff($start, $end){
		$startdateStamp = date_create($start);
		$enddateStamp = date_create($end);
		$interval = date_diff($startdateStamp , $enddateStamp);
		return $interval->format('%a');
	}
?>