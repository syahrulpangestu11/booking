<script type="text/javascript">
$(function(){

	$(document).ready(function(){
		getLoadData();
		$('select[name=commissiontype]').change();
		$('select[name=discounttype]').change();
	});


	$('body').on('click','button.edit', function(e) {
		pid = $(this).attr("pid");
		url = "<?php echo $base_url; ?>/promocode/edit/?pid="+pid;
      	$(location).attr("href", url);
	});

	$('body').on('click','button.add', function(e) {
		pid = $(this).attr("pid");
		url = "<?php echo $base_url; ?>/promocode/add";
      	$(location).attr("href", url);
	});

   var $dialogDelete = $('<div id="dialog-del"></div>')
	.html('Are you sure you want to delete this data?')
	.dialog({
		autoOpen: false,
		title: 'Confirmation',
		buttons: {
			"Delete": function(){
				var url = $(this).data('url');
				var id = $(this).data('id');
				$(this).dialog("close");
				deleteAction(url, id);
			},
			Cancel: function(){ $( this ).dialog("close"); }
		}
	});

	var $dialogNotification = $('<div id="dialog-notice"></div>')
	.dialog({
		autoOpen: false,
		title: 'Notification',
		buttons: {
			"Ok": function(){
				var callback = '<?php echo $base_url; ?>/promocode';
				$(this).dialog( "close" );
				$(location).attr("href", callback);
			}
		}
	});

	function deleteAction(url, id){
		$.ajax({
			url: url,
			type: 'post',
			data: { id : id},
			success: function(data) {
				if(data == "1"){
					$dialogNotification.html("Data has been deleted");
					$dialogNotification.dialog("open");
				}else{
					$dialogNotification.html("We&rsquo;re very sorry, we can't save your data right now");
					$dialogNotification.dialog("open");
				}
			}
		});
	}

	$('body').on('click','button.delete', function(e) {
		id = $(this).attr("pid");
		$dialogDelete.data("url", "<?php echo $base_url; ?>/includes/promocode/delete-promocode.php");
		$dialogDelete.data("id", id);
		$dialogDelete.dialog("open");
	});

	function getLoadData(){
		$.ajax({
			url: "<?php echo"$base_url"; ?>/includes/promocode/data.php",
			type: 'post',
			data: $('form#data-input').serialize(),
			success: function(data) {
				$("#data-box").html(data)
			}
		});
	}

   $("#dialog-error, #dialog-success").dialog({
	  autoOpen: false,
	  buttons: { "Ok": redirect },
	  close: function(event, ui) { redirect }
   });

   function redirect(){
	   $(location).attr("href", "<?php echo $base_url; ?>/promocode");
   }

	$('body').on('change','select[name=commissiontype]', function(e) {
		dataval = $(this).find('option:selected').attr('data');
		if($(this).val() == "commission amount"){
			$('li#commission-value').html('<span class="label">Commision Amount :</span><input type="text" name="commission" class="short" value="'+dataval+'"> per materialize booking');
		}else{
			$('li#commission-value').html('<span class="label">Commision :</span><input type="text" name="commission" class="short" value="'+dataval+'"> % per materialize booking');
		}
	});

	$('body').on('change','select[name=discounttype]', function(e) {
		dataval = $(this).find('option:selected').attr('data');
		if($(this).val() == "discount amount"){
			$('li#discount-value').html('<span class="label">Discount Amount :</span><input type="text" name="discount" class="short" value="'+dataval+'"> per promotion');
		}else{
			$('li#discount-value').html('<span class="label">Discount :</span><input type="text" name="discount" class="short" value="'+dataval+'"> % per per promotion');
		}
	});

	$('body').on('click','#paging button', function(e) {
		page	= $(this).attr('page');
		pagebox = $('form#data-input').children('input[name=page]');
		pagebox.val(page);
		getLoadData();
	}); 
});
</script>

<div id="dialog-error" title="Error Notification">
  <p>We&rsquo;re very sorry, we can't save your data right now.</p>
</div>
<div id="dialog-success" title="Confirmation Notice">
  <p>Data succesfully updated</p>
</div>
