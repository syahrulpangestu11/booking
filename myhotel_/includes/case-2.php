<?php
	$next = true;
$block_role_all = array(7);
if(in_array($_SESSION['_typeusr'], $block_role_all) and $next == true){
	$next = false;
	if($uri2=="dashboard" or $uri2=="commission-report" or empty($uri2)){
		if($uri3=="detail"){
			include("reports/commission-report/commission-report-detail.php");
		}else{
			include("reports/commission-report/commission-report-summary.php");
		}
	}else if($uri2=="booking"){
        if($uri3=="detail"){
            include("booking/detail-booking.php");
		}else{
			include("booking/view-booking.php");
		}
    }else if($uri2=="performance-reports"){
		if($uri3=="reservation-report"){
			include("reports/reservation-report.php");
		}else{
			include("reports/reservation-report.php");
		}
	}else if($uri2=="adr-report"){
		include("reports/adr-report/adr-report.php");
	}else if($uri2=="inhouse-report"){
		include("reports/inhouse-report/inhouse-report.php");
	}else if($uri2=="booking-promo"){
		include("reports/booking-promo/js.php");
		include("reports/booking-promo/view-booking.php");
	}else if($uri2=="booking-source"){
		include("reports/booking-source/js.php");
		include("reports/booking-source/view-booking.php");
	}else{
		$next = true;
	}
}

$block_role_all = array(6);
if(in_array($_SESSION['_typeusr'], $block_role_all) and $next == true){
	$next = false;
	if($uri2=="dashboard" or $uri2=="availability" or empty($uri2)){
        include("inventory/inventory.php");
    }else if($uri2=="booking"){
        if($uri3=="detail"){
            include("booking/detail-booking.php");
		}else{
			include("booking/view-booking.php");
		}
    }else{
		$next = true;
	}
}

$block_role_all = array(1, 2, 3, 4, 5);
if(in_array($_SESSION['_typeusr'], $block_role_all) and $next == true){
	$next = false;

    if($uri2=="dashboard" or $uri2 =="" or empty($uri2)){
        include("home.php");
	}else if($uri2=="notification"){
        include("notif.php");
	}else if($uri2=="profile"){
        include("profile/view-profile.php");
	}else if($uri2=="save-profile"){
        include("profile/view-profile-save.php");
	}else if($uri2=="webprofile"){
        include("master-webprofile/view-webprofile.php");
				if($uri3=="save"){
	        include("master-webprofile/save-webprofile.php");
				}
	}else if($uri2=="payment-method"){
		include("payment-method/js.php");
		include("payment-method/view-payment-method.php");
	}else if($uri2=="contact-management-web"){
		include("master-mailer/view-mailer.php");
	}else if($uri2=="faq"){
		include("master-faq/js.php");
		if($uri3=="add"){
			include("master-faq/add-faq.php");
		}else if($uri3=="edit"){
			include("master-faq/edit-faq.php");
		}else if($uri3=="add-save"){
      		include("master-faq/add-faq-save.php");
		}else if($uri3=="edit-save"){
      		include("master-faq/edit-faq-save.php");
		}else if($uri3=="delete"){
  			include("master-faq/delete-faq.php");
		}else{
      		include("master-faq/view-faq.php");
		}
	}else{
		$next = true;
	}
}

/*-----------------------------------------------------------------------------------------*/
/*/
$block_role_management = array(1, 2, 5);
if(in_array($_SESSION['_typeusr'], $block_role_management) and $next == true){

	$next = false;
    if($uri2=="management-dashboard"){
        include("management/dashboard/management-dashboard.php");
	}else if($uri2=="transaction-details"){
		if($uri3=="production"){
			include("management/dashboard/transaction-details-production.php");
		}else if($uri3=="occupancy"){
			include("management/dashboard/transaction-details-occupancy.php");
		}
	}else if($uri2=="sales-marketing"){
        include("sm-affiliate/case-sm.php");
	}else if($uri2=="hotel"){
		include("suadm/hotel/js.php");
		include("suadm/hotel/function.php");
		if($uri3=="add"){ include("suadm/hotel/add-hotel.php");
		}else if($uri3=="edit"){ include("suadm/hotel/edit-hotel.php");
		}else{ include("suadm/hotel/view-hotel.php"); }
	}else if($uri2=="invoicing"){
		include("suadm/invoicing/js.php");
		if($uri3=="action"){
			include("suadm/invoicing/invoicing-action.php");
		}else if($uri3=="action-save"){
			include("suadm/invoicing/invoicing-action-save.php");
		}else if($uri3=="generate"){
			include("suadm/invoicing/invoicing-generate.php");
		}else{
			include("suadm/invoicing/invoicing.php");
		}
	}else{
		$next = true;
	}
}
//*/

/*-----------------------------------------------------------------------------------------*/

$block_role_marketing = array(1);
if(in_array($_SESSION['_typeusr'], $block_role_marketing) and $next == true){
	$next = false;
	if($uri2=="admin-settings"){
		if($uri3=="save"){
			include("suadm/settings/update-settings.php");
		}else{
			include("suadm/settings/view-settings.php");
		}
	}else{
		$next = true;
	}
}
/*-----------------------------------------------------------------------------------------*/

$block_role_marketing = array(1, 2);
if(in_array($_SESSION['_typeusr'], $block_role_marketing) and $next == true){

	$next = false;
	if($uri2=="chain"){
		include("suadm/chain/js.php");
		if($uri3=="add"){ include("suadm/chain/add-cp.php");
		}else if($uri3=="edit"){ include("suadm/chain/edit-cp.php");
		}else{ include("suadm/chain/view-cp.php"); }
	}else if($uri2=="activities"){
		include("suadm/activities/js.php");
		include("suadm/hotel/function.php");
		if($uri3=="add"){ include("suadm/activities/add-activities.php");
		}else if($uri3=="edit"){ include("suadm/activities/edit-activities.php");
		}else{ include("suadm/activities/view-activities.php"); }
	}else if($uri2=="country"){
		include("suadm/destination/country/js.php");
		include("suadm/destination/country/view-country.php");
	}else if($uri2=="state"){
		include("suadm/destination/state/js.php");
		include("suadm/destination/state/view-state.php");
		if($uri3=="save-picture"){
			include("suadm/destination/state/save-state-pict.php");
		}
	}else if($uri2=="city"){
		include("suadm/destination/city/js.php");
		include("suadm/destination/city/view-city.php");
		if($uri3=="save-picture"){
			include("suadm/destination/city/save-city-pict.php");
		}
	}else if($uri2=="web-event"){
		if($uri3=="send-email"){ include("web-event/event-email-send.php");
		}else if($uri3=="send-no-email"){ include("web-event/event-email-send-no.php");
		}else{ include("web-event/event-view-list.php"); }
	}else if($uri2=="web-contact"){
		if($uri3=="send-email"){ include("web-contact/contact-email-send.php");
		}else{ include("web-contact/contact-view-list.php"); }
	}else if($uri2=="web-trial"){
		if($uri3=="send-email"){ include("web-trial/trial-email-send.php");
		}else{ include("web-trial/trial-view-list.php"); }
	}else if($uri2=="web-demo"){
		if($uri3=="send-email"){ include("web-demo/demo-email-send.php");
		}else{ include("web-demo/demo-view-list.php"); }
	// }else if($uri2=="promotion-template"){
	// 	include("suadm/bank-promotion/js.php");
	// 	if($uri3=="add"){
	// 		include("suadm/bank-promotion/add-cp.php");
	// 	}else if($uri3=="add-process"){
	// 		include("suadm/bank-promotion/add-cp-save.php");
	// 	}else if($uri3=="edit"){
	// 		include("suadm/bank-promotion/edit-cp.php");
	// 	}else if($uri3=="edit-process"){
	// 		include("suadm/bank-promotion/edit-cp-save.php");
	// 	}else{
	// 		include("suadm/bank-promotion/view-cp.php");
	// 	}
	// }else if($uri2=="poi"){
	// 	include("suadm/poi/js.php");
	// 	if($uri3=="add"){
	// 		include("suadm/poi/add-cp.php");
	// 	}else if($uri3=="add-process"){
	// 		include("suadm/poi/add-cp-save.php");
	// 	}else if($uri3=="edit"){
	// 		include("suadm/poi/edit-cp.php");
	// 	}else if($uri3=="edit-process"){
	// 		include("suadm/poi/edit-cp-save.php");
	// 	}else{
	// 		include("suadm/poi/view-cp.php");
	// 	}
	// }else if($uri2=="extra-template"){
	// 	include("suadm/bank-extra/js.php");
	// 	if($uri3=="add"){
	// 		include("suadm/bank-extra/add-bank-extra.php");
	// 	}else if($uri3=="add-process"){
	// 		include("suadm/bank-extra/add-bank-extra-save.php");
	// 	}else if($uri3=="edit"){
	// 		include("suadm/bank-extra/edit-bank-extra.php");
	// 	}else if($uri3=="edit-process"){
	// 		include("suadm/bank-extra/edit-bank-extra-save.php");
	// 	}else{
	// 		include("suadm/bank-extra/view-bank-extra.php");
	// 	}
	// }else if($uri2=="package-template"){
	// 	include("suadm/bank-package/js.php");
	// 	if($uri3=="add"){
	// 		include("suadm/bank-package/add-bank-package.php");
	// 	}else if($uri3=="add-process"){
	// 		include("suadm/bank-package/add-bank-package-save.php");
	// 	}else if($uri3=="edit"){
	// 		include("suadm/bank-package/edit-bank-package.php");
	// 	}else if($uri3=="edit-process"){
	// 		include("suadm/bank-package/edit-bank-package-save.php");
	// 	}else{
	// 		include("suadm/bank-package/view-bank-package.php");
	// 	}
	}else if($uri2=="email-template"){
		include("master-email-template/js.php");
		if($uri3=="save"){
            include("master-email-template/save-email-template.php");
        }else{
			include("master-email-template/view-email-template.php");
		}
	}else if($uri2=="contact-management-all"){
		include("contact-management-all/js.php");
		if($uri3=="add"){
			include("contact-management-all/add-contact-management.php");
		}else if($uri3=="addprocess"){
            include("contact-management-all/add-contact-management-process.php");
		}else if($uri3=="edit"){
			include("contact-management-all/edit-contact-management.php");
		}else if($uri3=="editprocess"){
            include("contact-management-all/edit-contact-management-process.php");
        }else{
            include("contact-management-all/view-contact-management-all.php");
		}
	}else if($uri2=="contacts"){
		include("contacts/js.php");
		if($uri3=="add"){
			include("contacts/add-contacts.php");
		}else if($uri3=="addprocess"){
            include("contacts/add-contacts-process.php");
		}else if($uri3=="edit"){
			include("contacts/edit-contacts.php");
		}else if($uri3=="editprocess"){
            include("contacts/edit-contacts-process.php");
        }else{
            include("contacts/view-contacts.php");
		}
	}else if($uri2=="booking-all"){
        if($uri3=="detail"){
            include("booking-all/detail-booking.php");
		}else if($uri3=="confirmation"){
			include("booking-all/payment_proof.php");
		}else if($uri3=="confirmation-save"){
			include("booking-all/payment_proof_save.php");
		}else if($uri3=="resend-confirmation"){
            include("booking-all/resend-email-confirmation.php");
		}else if($uri3=="modify"){
            include("booking-all/modify/page-modify.php");
		}else{
			include("booking-all/view-booking.php");
		}
	// }else if($uri2=="promotion-all"){
	// 	include("promotion-all/js.php");
	// 	if($uri3=="add"){
	// 		include("promotion-all/add-promotion.php");
	// 	}else if($uri3=="add-process"){
	// 		include("promotion-all/add-promotion-save.php");
	// 	}else if($uri3=="edit"){
	// 		include("promotion-all/edit-promotion.php");
	// 	}else if($uri3=="edit-process"){
	// 		include("promotion-all/edit-promotion-save.php");
	// 	}else if($uri3=="template"){
	// 		include("promotion-all/view-promotion-template.php");
	// 	}else{
	// 		include("promotion-all/view-promotion.php");
	// 	}
	}else if($uri2=="alloccupancyreport"){
	    include("includes/suadm/occupancy-report/view.php");
	    $next = false;
	}else{
		$next = true;
	}
}

/* >>>>>>>>>>>>>>>>>>>> */

$block_role_manage_all_hotel = array(1, 2, 4, 5);
if(in_array($_SESSION['_typeusr'], $block_role_manage_all_hotel) and $next == true){

	$next = false;
   	if($uri2=="hotel"){
		include("suadm/hotel/js.php");
		include("suadm/hotel/function.php");
		if($uri3=="add"){ include("suadm/hotel/add-hotel.php");
		}else if($uri3=="edit"){ include("suadm/hotel/edit-hotel.php");
		}else{ include("suadm/hotel/view-hotel.php"); }
	}else if($uri2=="chain-availability"){
		include("chain-availability/chain-availability.php");
	}else if($uri2=="chain-availability-lite"){
		include("chain-availability-lite/chain-availability.php");
	}else if($uri2=="inhouse-chain"){
		include("reports/inhouse-chain/inhouse-chain.php");
	}else if($uri2=="revenue-chain"){
		include("reports/revenue-chain/revenue-chain.php");
	}else if($uri2=="production-chain"){
		include("reports/production-chain/production-chain.php");
	}else{
		$next = true;
	}
}

/*-----------------------------------------------------------------------------------------*/

$block_role_manage_hotel = array(1, 2, 3, 4);
if(in_array($_SESSION['_typeusr'], $block_role_manage_hotel) and $next == true){
	$next = false;
	if($uri2=="room-control"){
		if($uri3=="process"){
			include("room-control/save-room-control.php");
		}else{
			include("room-control/view-room-control.php");
		}
	}else if($uri2=="rate-control"){
		if($uri3=="upload"){
			include("rate-control/save-rate-control.php");
		}else{
			include("rate-control/view-rate-control.php");
		}
	}else if($uri2=="load-tarif"){
		ini_set('max_execution_time', 0);
		include("load-tarif/js.php");
		if($uri3=="upload"){
			include("load-tarif/load-tarif-xml-save.php");
		}else if($uri3=="excel-import"){
			include("load-tarif/load-tarif-excel.php");
		}else if($uri3=="upload-excel"){
			include("load-tarif/load-tarif-excel-save.php");
		}else{
			include("load-tarif/load-tarif-xml.php");
		}
	}else if($uri2=="promotions"){
		include("promotion/js.php");
		if($uri3=="add"){
			include("promotion/add-promotion.php");
		}else if($uri3=="add-process"){
			include("promotion/add-promotion-save.php");
		}else if($uri3=="edit"){
			include("promotion/edit-promotion.php");
		}else if($uri3=="edit-process"){
			include("promotion/edit-promotion-save.php");
		}else if($uri3=="template"){
			include("promotion/view-promotion-template.php");
		}else{
			include("promotion/view-promotion.php");
		}
	}else if($uri2=="package"){
		include("package/js.php");
		if($uri3=="add"){
			include("package/add-package.php");
		}else if($uri3=="add-process"){
			include("package/add-package-save.php");
		}else if($uri3=="edit"){
			include("package/edit-package.php");
		}else if($uri3=="edit-process"){
			include("package/edit-package-save.php");
		}else if($uri3=="template"){
			include("package/view-package-template.php");
		}else{
			include("package/view-package.php");
		}
	}else if($uri2=="extra"){
		include("extra/js.php");
		if($uri3=="add"){
			include("extra/add-extra.php");
		}else if($uri3=="add-process"){
			include("extra/add-extra-save.php");
		}else if($uri3=="edit"){
			include("extra/edit-extra.php");
		}else if($uri3=="editcategory"){
			include("extra/edit-extra-category.php");
		}else if($uri3=="edit-process"){
			include("extra/edit-extra-save.php");
		}else if($uri3=="edit-category-process"){
			include("extra/edit-extra-category-save.php");
		}else if($uri3=="template"){
			include("extra/view-extra-template.php");
		}else if($uri3=="category"){
			include("extra/view-extra-category.php");
		}else if($uri3=="add-category"){
			include("extra/add-extra-category.php");
		}else if($uri3=="add-process-category"){
			include("extra/add-process-category.php");
		}else{
			include("extra/view-extra.php");
		}
	}else if($uri2=="promocode"){
		include("promocode/js.php");
		if($uri3=="add"){
			include("promocode/add-promocode.php");
		}else if($uri3=="add-process"){
			include("promocode/add-promocode-save.php");
		}else if($uri3=="edit"){
			include("promocode/edit-promocode.php");
		}else if($uri3=="edit-process"){
			include("promocode/edit-promocode-save.php");
		}else{
			include("promocode/view-promocode.php");
		}
	}else if($uri2=="promocode-all"){
		include("promocode-all/js.php");
		if($uri3=="add"){
			include("promocode-all/add-promocode.php");
		}else if($uri3=="add-process"){
			include("promocode-all/add-promocode-save.php");
		}else if($uri3=="edit"){
			include("promocode-all/edit-promocode.php");
		}else if($uri3=="edit-process"){
			include("promocode-all/edit-promocode-save.php");
		}else{
			include("promocode-all/view-promocode.php");
		}
	}else if($uri2=="promotion-all"){
		include("promotion-all/js.php");
		if($uri3=="add"){
			include("promotion-all/add-promotion.php");
		}else if($uri3=="add-process"){
			include("promotion-all/add-promotion-save.php");
		}else if($uri3=="apply-process"){
			include("promotion-all/apply-hotel-save.php");
		}else if($uri3=="edit"){
			include("promotion-all/edit-promotion.php");
		}else if($uri3=="edit-process"){
			include("promotion-all/edit-promotion-save.php");
		}else if($uri3=="applyhotel"){
			include("promotion-all/apply-hotel.php");
		}else{
			include("promotion-all/view-promotion.php");
		}
	}else if($uri2=="package-all"){
		include("package-all/js.php");
		if($uri3=="add"){
			include("package-all/add-package.php");
		}else if($uri3=="add-process"){
			include("package-all/add-package-save.php");
		}else if($uri3=="apply-process"){
			include("package-all/apply-hotel-save.php");
		}else if($uri3=="edit"){
			include("package-all/edit-package.php");
		}else if($uri3=="edit-process"){
			include("package-all/edit-package-save.php");
		}else if($uri3=="applyhotel"){
			include("package-all/apply-hotel.php");
		}else{
			include("package-all/view-package.php");
		}
	}else if($uri2=="cancellation-policy"){
        if($uri3=="editprocess"){
            include("hotel-settings/update-hotel-settings.php");
        }else{
            include("cancellation-policy/view-cancellation-policy.php");
        }
	}else if($uri2=="surcharge"){
        if($uri3=="editprocess"){
            include("hotel-settings/update-hotel-settings.php");
        }else{
            include("surcharge/view-surcharge.php");
        }
	}else if($uri2=="booking"){
        if($uri3=="detail"){
            include("booking/detail-booking.php");
		}else if($uri3=="resend-confirmation"){
            include("booking/resend-email-confirmation.php");
		}else if($uri3=="modify"){
            include("booking/modify/page-modify.php");
		}else{
			include("booking/view-booking.php");
		}
	}else if($uri2=="hotel-settings"){
        if($uri3=="editprocess"){
            include("hotel-settings/update-hotel-settings.php");
        }else{
            include("hotel-settings/view-hotel-settings.php");
        }
	}else if($uri2=="contact-management"){
		include("contact-management/js.php");
		if($uri3=="add"){
			include("contact-management/add-contact-management.php");
		}else if($uri3=="addprocess"){
            include("contact-management/add-contact-management-process.php");
		}else if($uri3=="edit"){
			include("contact-management/edit-contact-management.php");
		}else if($uri3=="editprocess"){
            include("contact-management/edit-contact-management-process.php");
        }else{
            include("contact-management/view-contact-management.php");
        }
	}else if($uri2=="room-settings"){
		include("room-settings/js.php");
		if($uri3=="edit"){
			include("room-settings/edit-room-settings.php");
		}else if($uri3=="edit-process"){
			include("room-settings/edit-room-settings-save.php");
		}else{
			include("room-settings/view-room-settings.php");
		}
	}else if($uri2=="tax-settings"){
		include("tax-settings/js.php");
        if($uri3=="add"){
            include("tax-settings/add-tax-settings.php");
		}else if($uri3=="edit"){
            include("tax-settings/edit-tax-settings.php");
        }else{
            include("tax-settings/view-tax-settings.php");
        }
/*	}else if($uri2=="hotel-ranking"){
		include("hotel-ranking/js.php");
        include("hotel-ranking/view-hotel-ranking.php");
*/
	}else if($uri2=="hotel-profile"){
		include("hotel-profile/get-data-hotel.php");
		include("hotel-profile/js.php");
		if($uri3=="basic-info"){
			if($uri4=="saveinfo"){
				include("hotel-profile/basic-info/save-basic-info.php");
			}else{
				include("hotel-profile/basic-info/basic-info.php");
			}
		}else if($uri3=="maps"){
			include("hotel-profile/maps/maps.php");
		}else if($uri3=="facilities"){
			include("hotel-profile/facilities/facilities.php");
		}else if($uri3=="photo"){
			if($uri4=="add"){
				include("hotel-profile/photos/add-photos.php");
			}else if($uri4=="add-process"){
				include("hotel-profile/photos/add-photos-save.php");
			}else{
				include("hotel-profile/photos/view-photos.php");
			}
		}else if($uri3=="rooms"){
			if($uri4=="add"){
				include("hotel-profile/room/add-room.php");
			}else if($uri4=="edit"){
				include("hotel-profile/room/edit-room.php");
			}else if($uri4=="photo" and $uri5=="add-process"){
				include("hotel-profile/room/photos/add-photos-save.php");
			}else{
				include("hotel-profile/room/view-room.php");
			}
		}else{
			include("hotel-profile/view-hotel-profile.php");
		}
	}else if($uri2=="cancellationpolicy"){
		include("suadm/cancellationpolicy/js.php");
		if($uri3=="add"){
			include("suadm/cancellationpolicy/add-cp.php");
		}else if($uri3=="add-process"){
			include("suadm/cancellationpolicy/add-cp-save.php");
		}else if($uri3=="edit"){
			include("suadm/cancellationpolicy/edit-cp.php");
		}else if($uri3=="edit-process"){
			include("suadm/cancellationpolicy/edit-cp-save.php");
		}else{
			include("suadm/cancellationpolicy/view-cp.php");
		}
	}else if($uri2=="tracking-analytics"){
		if($uri3=="ip-whitelist"){
			include("tracking-analytics/ip-whitelist/list-ip-whitelist.php");
		}else{
			include("tracking-analytics/dashboard-tracking-analytics.php");
		}
	}else if($uri2=="booking-enginee-appearance"){
		if($uri3=="save"){
			include("booking-enginee/save-setting-booking-enginee.php");
		}else if($uri3=="save-bar"){
			include("booking-enginee/save-bar-booking-enginee.php");
		}else{
			include("booking-enginee/setting-booking-enginee.php");
		}

	}else if($uri2=="performance-reports"){
		if($uri3=="reservation-report"){
			include("reports/reservation-report.php");
		}else{
			include("reports/reservation-report.php");
		}
	}else if($uri2=="commission-report"){
		if($uri3=="detail"){
			include("reports/commission-report/commission-report-detail.php");
		}else{
			include("reports/commission-report/commission-report-summary.php");
		}
	}else if($uri2=="adr-report"){
		// if($uri3=="detail"){
		// 	include("reports/adr-report/adr-report.php");
		// }else{
			include("reports/adr-report/adr-report.php");
	}else if($uri2=="inhouse-report"){
				include("reports/inhouse-report/inhouse-report.php");
	}else if($uri2=="availability"){
        include("inventory/inventory.php");
	}else if($uri2=="agent"){
		include("agent/js.php");
		include("agent/function.php");
		if($uri3=="add"){
			include("agent/add-agent.php");
		}else if($uri3=="edit"){
			include("agent/edit-agent.php");
		}else{
			include("agent/view-agent.php");
		}
	}else if($uri2=="agent-rate"){
		include("agent-rate/js.php");
		include("agent-rate/edit-agent-rate.php");
	}else if($uri2=="agent-creditfacility"){
		include("agent-creditfacility/js.php");
		include("agent-creditfacility/edit-agent-creditfacility.php");
	}else if($uri2=="agent-reservation"){
		//include("agent-reservation/js.php");
		if($uri3=="edit"){
			include("agent-reservation/edit-agent-rate.php");
		}else if($uri3=="edit-process"){
			include("agent-reservation/edit-agent-rate-save.php");
		}else{
			include("agent-reservation/agent-reservation.php");
		}
	}else if($uri2=="all-agents" and $_SESSION['_typeusr'] != "3"){
		include("agents/js.php");
		include("agents/function.php");
		if($uri3=="add"){
			include("agents/add-agent.php");
		}else if($uri3=="edit"){
			include("agents/edit-agent.php");
		}else{
			include("agents/view-agent.php");
		}
	}else if($uri2=="crm"){
		if($uri3=="loyalty-member-program"){
			include("crm/js.php");
			if($uri4=="edit"){
				include("crm/setting-loyalty-program.php");
			}else{
				include("crm/view-loyalty-program.php");
			}
		}else if($uri3=="member"){
			if($uri4=="detail"){
				include("crm/member/detail-member.php");
			}else{
				include("crm/member/view-member.php");
			}
		}else if($uri3=="email-template"){
			include("crm/email-template/js.php");
			if($uri4=="add"){
				include("crm/email-template/add-email-template.php");
			}else{
				include("crm/email-template/view-email-template.php");
			}
		}
	}else if($uri2=="bar-setting"){
		if($uri3=="loyalty-member-program"){
			include("crm/js.php");
		}else{
			include("bar-setting/view-bar-setting.php");
		}
	// }else if($uri2=="payment-method"){
	// 	include("payment-method/js.php");
	// 	include("payment-method/view-payment-method.php");
	}
	//headline-icon
	else if($uri2=="headline-icon"){
		include("headline-icon/js.php");
		if($uri3=="add"){
			include("headline-icon/add-hi.php");
		}else if($uri3=="add-process"){
			include("headline-icon/add-hi-save.php");
		}else if($uri3=="edit"){
			include("headline-icon/edit-hi.php");
		}else if($uri3=="edit-process"){
			include("headline-icon/edit-hi-save.php");
		}else{
			include("headline-icon/view-hi.php");
		}
	}
	else if($uri2=="booking-ref"){
		include("reports/booking-ref/js.php");
		include("reports/booking-ref/view-booking.php");
	}
	else if($uri2=="booking-promo"){
		include("reports/booking-promo/js.php");
		include("reports/booking-promo/view-booking.php");
	}
	else if($uri2=="booking-source"){
		include("reports/booking-source/js.php");
		include("reports/booking-source/view-booking.php");

	}else if($uri2=="master-rate"){
		include('includes/bootstrap.php');
		include("master-rate/js.php");
		if($uri3=="detail"){
			include("master-rate/master-rate-detail.php");
		}else {
			include("master-rate/master-rate-view.php");
		}
	}
	else if($uri2=="master-roomnumber"){
		include("master-roomnumber/js.php");
		if($uri3=="add"){
			include("master-roomnumber/add-roomnumber.php");
		}else if($uri3=="add-process"){
			include("master-roomnumber/add-roomnumber-save.php");
		}else if($uri3=="edit"){
			include("master-roomnumber/edit-roomnumber.php");
		}else if($uri3=="edit-process"){
			include("master-roomnumber/edit-roomnumber-save.php");
		}else{
			include("master-roomnumber/view-roomnumber.php");
		}
	}else if($uri2=="insightreport"){
	    include("includes/reports/insight-report/view-insight-report.php");
	    $next = false;
	}else if($uri2=="activity-log"){
		include('includes/bootstrap.php');
		include("master-rate/js.php");
		include("log/view-log.php");
	}else if($uri2=="email-template"){
		include('includes/bootstrap.php');
		if($uri3=="upselling-email"){
			include("email-template/upselling-email/upselling-email.php");
		}else if($uri3=="upselling-email-save"){
			include("email-template/upselling-email/save-email.php");
		}else{
			include("email-template/view-email-template.php");
		}
	}else{
		$next = true;
	}
}

// $block_role_superadmin = array(1);
$block_role_superadmin = array(1,2);//basma
if(in_array($_SESSION['_typeusr'], $block_role_superadmin) and $next == true){
	$next = false;
	if($uri2=="connectivity"){
		if($uri3=="save"){
			include("connectivity/save-connectivity.php");
		}else{
			include("connectivity/view-connectivity.php");
		}
	}else if($uri2=="import-external"){
		if($uri3=="mapping-field"){
			include("import-external/mapping-field.php");
		}else if($uri3=="import-data"){
			include("import-external/import-data.php");
		}else{
			include("import-external/define-hotel.php");
		}
	    $next = false;
	}else if($uri2=="user"){
		include("suadm/user/js.php");
		if($uri3=="add"){ include("suadm/user/add-user.php");
		}else if($uri3=="add-save"){ include("suadm/user/add-user-save.php");
		}else if($uri3=="edit"){ include("suadm/user/edit-user.php");
		}else if($uri3=="edit-save"){ include("suadm/user/edit-user-save.php");
		}else{ include("suadm/user/view-user.php"); }
	}else{
		$next = true;
	}
}


// include('case-pms.php');

if($next == true){ echo "<h1 style='text-align:center'>You are not authorized for this page.</h1>"; }
?>
