<?php
	// include("function.php");
	try {
		$stmt = $db->query("SELECT a.* 
						FROM mastertemplate_email a
                        where masterprofileoid = '1'
                        order by email_type,email_to");
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
			$r_mastertemplate_email = $stmt->fetchAll(PDO::FETCH_ASSOC);
		}else{
			echo "No Result";
			die();
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}
?>
<style>
	form span{font-style: italic;color:red;font-size: 0.9em;display: none;}
	form button{float:right;margin:5px;}
	form .form-control{width:100%;}
	form .box-input-image img{height: 100%; max-width: 100%;}
	form .box-input-image label{}
        form .box-input-image{
		width: 100%;
		height: 150px;
		line-height: 150px;
		border: 1px solid #ccc;
		margin: 5px 0;
		text-align: center;
		padding: 0px;
		background: #f8f8f8;
	}
	form .bootstrap-tagsinput{display: block; height: 82px;border-radius: 0;padding:6px 12px;}
	form .bootstrap-tagsinput span{font-family: sans-serif; font-size: 85%;font-style: normal;}
    form textarea{box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075); }
    form label{text-transform:capitalize;vertical-align:bottom;}
    form {margin-bottom:25px;}
</style>
<section class="content-header">
    <h1>
        Email Template
    </h1>
    <ol class="breadcrumb">
        <li class="active"><a href="#"><i class="fa fa-book"></i> Web Profile</a></li>
    </ol>
</section>
<section class="content">
	<div class="row">
        <div class="box box-form">
            <div id="data-box" class="box-body">
            <h1>EMAIL TEMPLATE</h1><hr />
            

		          <div class="row">
                        <div class="col-md-12 col-xs-12">
                                <?php
                                    foreach($r_mastertemplate_email as $row){
                                        // $masterprofileoid = $row['masterprofileoid'];
                                        // $publishedoid = $row['publishedoid'];
                                        $mastertemplate_emailoid = $row['mastertemplate_emailoid'];
                                        $email_type = $row['email_type'];
                                        $email_to = $row['email_to'];
                                        $email_body = $row['email_body'];
                                        $email_variable = $row['email_variable'];
                                        $created = $row['created_at'];
                                        $createdby = $row['created_by'];
                                        $updated = $row['updated_at'];
                                        $updatedby = $row['updated_by'];
                                        ?>
                                        <form method="post" enctype="multipart/form-data" action="<?php echo $base_url; ?>/email-template/save">
                                            <input type="hidden" name="mastertemplate_emailoid" value="<?php echo $mastertemplate_emailoid; ?>" />
                                            
                                            <div class="row">
                                                <div class="col-md-12 col-xs-12">
                                                    <label style="margin-top: 15px;">Email <?php echo $email_type; ?>  : <?php echo $email_to; ?></label>
                                                    <button type="submit" class="btn btn-primary submit" style="float:right;">Save</button>
                                                    <div style="clear:both;margin-bottom: 10px;"></div>
                                                </div>
                                                <div class="col-md-8 col-xs-12">
                                                    <div class="form-group">
                                                        <textarea class="form-control" name="email_body" rows="9"><?php echo $email_body; ?></textarea>
                                                    </div>
                                                    <?php
                                                        if(!empty($updated) and !empty($updatedby)){
                                                            echo "<i class='fa fa-clock-o'></i> Last updated on ".date('d/M/Y H:i', strtotime($updated))." <small>(GMT+8)</small> by <b>".$updatedby."</b>";
                                                        }
                                                    ?>
                                                </div>
                                                <div class="col-md-4 col-xs-12">
                                                    <div style="height: 185px;overflow-y: scroll;padding:10px;border:1px solid #ccc;">
                                                        <?php 
                                                        preg_match_all('/[$](.*?)[;]/', $email_variable, $output_array);
                                                        foreach($output_array[0] as $key => $data){
                                                            $titleData = preg_replace('/[_]/',' ',$output_array[1][$key]);
                                                            ?>
                                                                <p>
                                                                    <span style="text-transform: capitalize; font-weight: 700; display: inline-block; font-style: normal; 
                                                                                font-size: inherit; color: #333;">
                                                                        <?=$titleData;?> : 
                                                                    </span>
                                                                    <?=$data;?>
                                                                </p>
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                    <i>* data variables that used by template.</i>
                                                </div>
                                            </div>
                                        </form>

                                        <?php
                                    }
                                ?>
                        </div>
                    </div>
            </div>
   		</div>
    </div>
</section>
<script>

//--------- prevent enter as submit action
$('body').on('keypress','input', function(e) { if (e.keyCode == 13){ e.preventDefault(); } });


</script>
