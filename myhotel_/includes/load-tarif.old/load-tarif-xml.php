<?php
	include('includes/bootstrap.php');
	$start = date("d F Y");
	$end = date("d F Y",  strtotime(date("Y-m-d")." +1 month" ));
	$daylist = array("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat");
?>
<section class="content-header">
    <h1>
        Load Rates
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i>  Allotments &amp; Rates </a></li>
        <li class="active">Load Rates</li>
    </ol>
</section>
<section class="content">
    <div class="box box-form">
        <div class="box-body">
            <form method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/load-tarif/upload">
                    
			<div class="row">
                <div class="form-group col-md-6">
                    <label>Rate Plan</label>
                    <select name="roomoffer" class="form-control">
                    <?php
                        try {
                            $stmt = $db->query("select r.roomoid, r.name, x.offernum from room r inner join hotel h using (hoteloid) left join (select count(roomofferoid) as offernum, ro.roomoid from roomoffer ro group by ro.roomoid) as x on x.roomoid = r.roomoid where h.hoteloid = '".$hoteloid."' and x.offernum > 0 and r.publishedoid = '1'");
                            $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                            foreach($r_room as $row){
                                echo"<optgroup label = '".$row['name']."'>";
                                try {
                                    $stmt = $db->query("select ro.* from roomoffer ro inner join published p using (publishedoid) inner join room r using (roomoid) inner join offertype using (offertypeoid) where r.roomoid = '".$row['roomoid']."' and p.publishedoid = '1'");
                                    $r_offer = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                    foreach($r_offer as $row1){
                                       echo"<option value='".$row['roomoid']."-".$row1['roomofferoid']."' minrate='".floor($row1['minrate'])."'>  ".$row1['name']."</option>";
                                    }
                                }catch(PDOException $ex) {
                                    echo "Invalid Query";
                                    die();
                                }     
                                echo"</optgroup>";                       
                            }
                        }catch(PDOException $ex) {
                            echo "Invalid Query";
                            die();
                        }
                    
                    ?>   
                    </select>
                </div>
                <div class="form-group col-md-6" style="display:none;">
                    <label>Channel</label>
                    <select name="channeloid" class="form-control">
                    <?php
                        try {
                            $stmt = $db->query("select * from channel");
                            $r_plan = $stmt->fetchAll(PDO::FETCH_ASSOC);
                            foreach($r_plan as $row){
                                echo"<option value='".$row['channeloid']."'>".$row['name']."</option>";
                            }
                        }catch(PDOException $ex) {
                            echo "Invalid Query";
                            die();
                        }
                    
                    ?>        	
                    </select> &nbsp;
                </div>
            	<div class="form-group col-md-2">
                	<label>Min Stay</label>
                    <div class="input-group">
                        <input type="number" name="minstay" class="form-control" placeholder="" value="1"> 
      					<div class="input-group-addon">night</div>
					</div>
                </div>
            	<div class="form-group col-md-2">
                	<label>Max Stay</label>
                    <div class="input-group">
                        <input type="number" name="maxstay" class="form-control" placeholder="" value="1"> 
      					<div class="input-group-addon">night</div>
					</div>
                 </div>
            </div>
            
            <div class="row">
            	<div class="form-group col-md-3">
                	<label>Start Date</label>
                    <div class="input-group">
      					<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
						<input type="text" name="startdate" class="form-control" id="startdate" placeholder="" value="<?php echo $start; ?>">
					</div>
                </div>
            	<div class="form-group col-md-3">
                	<label>End Date</label>
                    <div class="input-group">
      					<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                        <input type="text" name="enddate" class="form-control" id="enddate" placeholder="" value="<?php echo $end; ?>">
					</div>
                </div>
            	<div class="form-group col-md-6">
                	<label>Applicable Day of Week</label>
                    <div>
                        <?php
                            foreach($daylist as $day){
                                echo"<input type='checkbox' name='day[]' value='".$day."' checked>".$day."&nbsp;&nbsp;";
                            }
                        ?>
                    </div>
                </div>
            </div>
            
            <div class="row">
            	<div class="form-group col-md-2">
                	<label>Currency</label>
                    <select name="currency" class="form-control">
                        <?php
                        try {
                            $stmt = $db->query("select * from currency where publishedoid = '1'");
                            $r_published = $stmt->fetchAll(PDO::FETCH_ASSOC);
                            foreach($r_published as $row){
                        ?>
                            <option value="<?php echo $row['currencyoid']; ?>"><?php echo $row['currencycode']; ?></option>
                        <?php	
                            }
                        }catch(PDOException $ex) {
                            echo "Invalid Query";
                            die();
                        }
                        ?>
                    </select>
                </div>
            	<div class="form-group col-md-2">
                	<label>Rate</label>
                    <input type="text" name="double" class="form-control" placeholder="">
                </div>
            	<div class="form-group col-md-2">
                	<label>Extra Bed</label>
                    <input type="text" name="extrabed" class="form-control" placeholder="">
                </div>
            	<div class="form-group col-md-2">
                	<label>Allotment</label>
                    <input type="number" name="allotment" class="form-control" placeholder="" value="99"> 
                </div>
            	<div class="form-group col-md-2" style="display:none;">
                	<label>Allotment</label>
					<input type="number" name="topup" class="form-control" placeholder="" value="0">
                </div>
            	<div class="form-group col-md-2">
                	<label>Breakfast</label>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="breakfast" value="y"> included ( <i class="fa fa-cutlery"></i> )
                        </label>
                    </div>
                </div>
			</div>
            
            <div class="row">   
                <div class="form-group">
                    <div class="col-sm-6">
                        <button type="button" class="btn btn-primary" id="upload-rate">Upload Rate</button>
                    </div>
                    <div class="col-sm-6 text-right">
                        <a href="<?php echo"$base_url"; ?>/import-external"><button type="button" class="btn btn-success">Import from external data</button></a>
                    </div>
                </div> 
            </div>        

            </form>
       </div>
    </div>
</section>
