<script type="text/javascript">
$(function(){
	$('body').on('click','button[fnc=fromexcel]', function(){
      	document.location.href = "<?php echo $base_url; ?>/load-tarif/excel-import";
	});
	
	$('body').on('click','button#upload-rate', function(){
      	minrate	= parseFloat($('select[name="roomoffer"] option:selected').attr('minrate'));
		double 	= parseFloat($('input[name=double]').val());
		if(double < minrate || isNaN(double)){
			$("div.dialog-notification").html('Your input rate less than room minimum rate.<br/>Please change your rate more than '+minrate+'.')
			dialog.dialog("open");
		}else{
			$('form#data-input').submit();
		}
	});
	
    var dialog = $("<div class='dialog-notification' style='height:auto'></div>").dialog({
      autoOpen: false,
      modal: true,
	  title: 'Notification',
      buttons: {
        'Ok': function() {
          dialog.dialog( "close" );
        }
      },
      close: function() {
        dialog.dialog( "close" );
      }
    });
});
</script>
<style>
.ui-widget-overlay {
    background: #000000;
    opacity: .5;
    filter: Alpha(Opacity=30);
}
</style>