<script type="text/javascript">
$(function(){	
	$('#dashboard div').click(function(){
		linking = $(this).attr('link');
		if(linking !== ''){
			$(location).attr("href", linking);
		}
	});
});
</script>
<div id="dashboard">
    <ul class="inline-block">
        <li>
            <h1>Manage Your Site</h1>
            <div link="<?php echo $base_url; ?>/hotel">
            	<div>
                    <div class="left"><img src="<?php echo $base_url; ?>/images/bed.png"></div>
                    <div class="right">Manage <h1>Hotel</h1></div>
                    <div class="clear"></div>
                </div>
            </div> 
        </li>
        <li>
            <h1>&nbsp;</h1>
            <div link="<?php echo $base_url; ?>/user">
            	<div>
                    <div class="left"><img src="<?php echo $base_url; ?>/images/people.png"></div>
                    <div class="right">Manage <h1>User</h1></div>
                    <div class="clear"></div>
                </div>
            </div> 
        </li>
    </ul>
</div>