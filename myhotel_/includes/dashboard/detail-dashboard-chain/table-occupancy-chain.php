<div class="row">
  <div class="col-sm-12">
      <div class="box box-primary">
        <div class="box-header">
          <h4>Occupancy Summary from <?=date('d F Y', strtotime($start))?> to <?=date('d F Y', strtotime($end))?></h4>
          <i>(Table View)</i>
        </div>
        <div class="box-body">

<table id="occupancy-table" cellspacing="0" class="table table-striped">
  <thead>
    <tr>
        <th>Hotel Name</th>
        <th>Commission</th>
        <th>Revenue</th>
        <th>Cancelled Value</th>
        <th>Net to Hotel</th>
        <th>Bookings</th>
        <th>Confirmed</th>
        <th>Cancelled</th>
        <th>R/N</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $s_list_hotel_chain = "select hoteloid, hotelname from hotel h inner join chain c using (chainoid) where h.chainoid = '".$chainoid."'";
    $stmt = $db->query($s_list_hotel_chain);
    $hotel_chain = $stmt->fetchAll(PDO::FETCH_ASSOC);
    foreach($hotel_chain as $row){
      $hoteloid = $row['hoteloid'];
      $hotelname = $row['hotelname'];
      $jml_visit = 0; $jml_booking = 0; $jml_confirm = 0; $jml_cancel = 0; $jml_roomnight = 0;
      $totalreservation = 0; $totalconfirmed = 0; $totalcancelled = 0; $totalnoshow = 0;

      //$jml_visit		= getVisit($hoteloid, "availability", $start, $end);
      $jml_booking	= occupancyBooking($start, $end, $hoteloid, array(4,5,7,8));
      $jml_confirm	= occupancyBooking($start, $end, $hoteloid, 4);
      $jml_cancel 	= occupancyBooking($start, $end, $hoteloid, array(5,7,8));
      $jml_roomnight	= occupancyRoomNight($start, $end, $hoteloid, 4);

      $revenue		= occupancyRevenue($start, $end, $hoteloid, 4);
      $commission	= occupancyCommission($start, $end, $hoteloid);
      $cancellationamount		= occupancyRevenue($start, $end, $hoteloid, array(5,7,8));

      $nethotel = ($revenue+$cancellationamount) - $commission;
    ?>
    <tr>
      <td><?=$row['hotelname']?></td>
      <td><?=number_format($commission)?></td>
      <td><?=number_format($revenue)?></td>
      <td><?=number_format($cancellationamount)?></td>
      <td><?=number_format($nethotel)?></td>
      <td><?=$jml_booking?></td>
      <td><?=$jml_confirm?></td>
      <td><?=$jml_cancel?></td>
      <td><?=$jml_roomnight?></td>
    </tr>
    <?php
    }
    ?>
  </tbody>
</table>
      </div>
    </div>
  </div>
</div>
