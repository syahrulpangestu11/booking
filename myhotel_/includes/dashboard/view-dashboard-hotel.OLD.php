<script type="text/javascript">
$(function(){	
	$('#dashboard div').click(function(){
		linking = $(this).attr('link');
		if(linking !== ''){
			$(location).attr("href", linking);
		}
	});
});
</script>
<?php
$today = date("Y-m-d");
$today_plus_7 = date("Y-m-d",strtotime($today." +7 day"));

$this_week_firstdate = date("Y-m-d",strtotime("this week"));
$this_week_lastdate = date("Y-m-d",strtotime("this week +6 days"));

$this_month_firstdate = date("Y-m-d",strtotime("first day of this month"));
$this_month_lastdate = date("Y-m-d",strtotime("last day of this month"));

$query_form = "
	from booking b 
	inner join bookingstatus bs using (bookingstatusoid) 
	inner join customer c using (custoid) 
	inner join bookingroom br using (bookingoid) 
	inner join roomoffer ro using (roomofferoid) 
	inner join room r using (roomoid) 
	inner join hotel h using (hoteloid) 
	left join bookingpayment using (bookingoid) 
	where h.hoteloid = '".$hoteloid."' ";
	
$query_group_bookingoid = " group by b.bookingoid";

$query_form_detail = "
	from booking b 
	inner join bookingstatus bs using (bookingstatusoid) 
	inner join customer c using (custoid) 
	inner join bookingroom br using (bookingoid) 
	inner join roomoffer ro using (roomofferoid) 
	inner join room r using (roomoid) 
	inner join hotel h using (hoteloid)
	inner join bookingroomdtl brd using (bookingroomoid)  
	left join bookingpayment using (bookingoid) 
	where h.hoteloid = '".$hoteloid."' ";

?>
<div class="box box-form">
    <h1>Hotel : <?php echo $_SESSION['_hotelname']; ?></h1>
</div>
<div id="dashboard">
    <ul class="inline-block">
        <li>
            <h1>Manage Your Business</h1>
            <div link="<?php echo $base_url; ?>/room-control">
            	<div>
                    <div class="left"><img src="<?php echo $base_url; ?>/images/bed.png"></div>
                    <div class="right">Manage my <h1>Rooms</h1></div>
                    <div class="clear"></div>
                </div>
            </div> 
            <div link="<?php echo $base_url; ?>/rate-control">
                <div>
                    <div class="left"><img src="<?php echo $base_url; ?>/images/rate.png"></div>
                    <div class="right">Manage my <h1>Rates</h1></div>
                    <div class="clear"></div>
                </div>
            </div> 
            <div link="<?php echo $base_url; ?>/hotel-ranking">
            	<div>
                    <div class="left"><img src="<?php echo $base_url; ?>/images/percent.png"></div>
                    <div class="right"><h1>Commission</h1>override for higher ranking</div>
                    <div class="clear"></div>
                </div>
            </div> 
        </li>
        <li>
            <h1>Your Statistics</h1>
            <div>
            	<div>
                    <div class="left"><img src="<?php echo $base_url; ?>/images/batery.png"></div>
                    <div class="right">Your content score is <h1>97%</h1>Your competitorave have an average content score of 98%</div>
                    <div class="clear"></div>
                </div>
            </div> 
            <div>
            	<div>
                    <div class="left"><img src="<?php echo $base_url; ?>/images/people.png"></div>
                    <div class="right">On average you have<h1>124</h1>website visitors per day</div>
                    <div class="clear"></div>
                </div>
            </div> 
            <div>
            	<div>
                    <div class="left"><img src="<?php echo $base_url; ?>/images/cart.png"></div>
                    <div class="right"><h1>0,75 %</h1>conversion rate 98%</div>
                    <div class="clear"></div>
                </div>
            </div> 
            <div>
            	<div>
                    <div class="left"><img src="<?php echo $base_url; ?>/images/key.png"></div>
                    <?php
                    $query = "select count(bookingroomdtloid) as numberroom ".$query_form_detail." and (DATE(bookingtime) >= '".$this_month_firstdate."' and DATE(bookingtime) <= '".$this_month_lastdate."') ";
					try {
						$stmt = $db->query($query);
						$r_arrival = $stmt->fetchAll(PDO::FETCH_ASSOC);
						foreach($r_arrival as $row){
							$numofroom = $row['numberroom'];
						}
					}catch(PDOException $ex) {
						echo "Invalid Query";
						print($ex);
						die();
					}
					?>
                    <div class="right"><h1><?php echo $numofroom; ?></h1>room nights sold this month</div>
                    <div class="clear"></div>
                </div>
            </div> 
        </li>
        <li>
            <h1>Happening Now</h1>
            <div>
            	<div>
                    <div class="left"><img src="<?php echo $base_url; ?>/images/checkin.png"></div>
                    <div class="right"><h1>Arrivals</h1></div>
                    <div class="clear"></div>
                    <div>
                    <?php
					$startdate = $today;
					$enddate = $today;
                    $query = "select b.bookingoid, CONCAT(firstname, ' ', lastname) as guestname ".$query_form." and (DATE(bookingtime) >= '".$startdate."' and DATE(bookingtime) <= '".$enddate."') ".$query_group_bookingoid." limit 3";
					try {
						$stmt = $db->query($query);
						$r_arrival = $stmt->fetchAll(PDO::FETCH_ASSOC);
						foreach($r_arrival as $row){
							echo $row['guestname']."<br>";
							try {
								$stmt = $db->query("select count(bookingroomoid)as jmlroom, checkin, checkout from bookingroom where bookingoid = '".$row['bookingoid']."' group by checkin, checkout");
								$r_cico = $stmt->fetchAll(PDO::FETCH_ASSOC);
								foreach($r_cico as $row1){
									echo $row1['jmlroom']." room &bull; ci/co ".$row1['checkin']."/".$row1['checkout'];
								}
							}catch(PDOException $ex) {
								echo "Invalid Query";
								print($ex);
								die();
							}

						}
					}catch(PDOException $ex) {
						echo "Invalid Query";
						print($ex);
						die();
					}
					?>
                    </div>
                    
                </div>
            </div> 
            <div>
            	<div>
                    <div class="left"><img src="<?php echo $base_url; ?>/images/data.png"></div>
                    <?php
                    $query = "select count(b.bookingoid) as numberrsv ".$query_form." and (DATE(bookingtime) >= '".$this_week_firstdate."' and DATE(bookingtime) <= '".$this_week_lastdate."') ".$query_group_bookingoid;
					try {
						$stmt = $db->query($query);
						$r_arrival = $stmt->fetchAll(PDO::FETCH_ASSOC);
						foreach($r_arrival as $row){
							$numofrsv = $row['numberrsv'];
						}
					}catch(PDOException $ex) {
						echo "Invalid Query";
						print($ex);
						die();
					}
					?>
                    <div class="right"><h1><?php echo $numofrsv; ?></h1>reservation this week</div>
                    <div class="clear"></div>
                    <div>
                    <?php
					$startdate = date("Y-m-d");
					$enddate = date("Y-m-d");
                    $query = "select b.bookingoid, CONCAT(firstname, ' ', lastname) as guestname ".$query_form." and (DATE(bookingtime) >= '".$todaydate."' and DATE(bookingtime) <= '".$today_plus_7."') ".$query_group_bookingoid." limit 3";
					try {
						$stmt = $db->query($query);
						$r_arrival = $stmt->fetchAll(PDO::FETCH_ASSOC);
						foreach($r_arrival as $row){
							echo $row['guestname']."<br>";
							try {
								$stmt = $db->query("select count(bookingroomoid)as jmlroom, checkin, checkout from bookingroom where bookingoid = '".$row['bookingoid']."' group by checkin, checkout");
								$r_cico = $stmt->fetchAll(PDO::FETCH_ASSOC);
								foreach($r_cico as $row1){
									echo $row1['jmlroom']." room &bull; ci/co ".$row1['checkin']."/".$row1['checkout'];
								}
							}catch(PDOException $ex) {
								echo "Invalid Query";
								print($ex);
								die();
							}

						}
					}catch(PDOException $ex) {
						echo "Invalid Query";
						print($ex);
						die();
					}
					?>
                    </div>

                </div>
            </div> 
            <div>
            	<div>
                    <div class="left"><img src="<?php echo $base_url; ?>/images/write.png"></div>
                    <div class="right"><h1>1</h1>latest guest review</div>
                    <div class="clear"></div>
                </div>
            </div> 
        </li>
    </ul>
</div>