<!-- Bootstrap -->
<link href="<?php echo $base_url?>/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<!-- Theme style -->
<link href="<?php echo $base_url?>/lib/admin-lte/css/AdminLTE.min.css" rel="stylesheet" type="text/css">
<!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
<link href="<?php echo $base_url?>/lib/admin-lte/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css">
<!-- Fixing -->
<link href="<?php echo $base_url?>/css/style.css?v=<?=$fileVersion;?>" rel="stylesheet" type="text/css">
<!-- JQUERY UI -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- Pagination -->
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css">
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>

<style type="text/css">
	h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 { 
		font-family: inherit; 
		font-weight: 600;
		line-height: inherit;
		color: inherit;
	}
	.wrapper {
		position: inherit;
		overflow: hidden!important;
	}
	.left-side {
		padding-top: inherit;
	}
	.sidebar > .sidebar-menu li > a:hover {
		background-color: rgba(72, 115, 175, 0.26);
	}
	.sidebar .sidebar-menu .treeview-menu {
		background-color: rgb(14, 26, 43);
	}
	.sidebar .sidebar-menu .treeview-menu .treeview-menu-child {
		background-color: rgba(0, 0, 0, 0.5);
	}
	.sidebar > .sidebar-menu li.active > a {
		background-color: rgba(197, 45, 47, 0.55);
	}
	.sidebar > .sidebar-menu > li.treeview.active > a {
		background-color: inherit;
	}
	.sidebar > .sidebar-menu > li.treeview.active li.treeview-child.active > a {
		background-color: inherit;
	}
	.sidebar .sidebar-menu > li > a > .fa {
		width: 28px;
		font-size: 16px;
	}
	.sidebar-menu li>a>.fa-angle-left {
		position: relative;
		margin-right: inherit;
	}
	.sidebar .sidebar-menu > li > a > .fa.pull-right {
		right: 0;
		text-align: center;
	}
	.box {
		border-radius: inherit;
		border-top: inherit;
	}
	.ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default {
		background-color: #ededed !important;
	}
	#example_wrapper .col-sm-12 {
        overflow-x: scroll;
    }
    #example > thead {
        background-color: #eaeaea;
    }
</style>

<?php
    if(isset($_GET['startdate'])){
    	$startdate = date('Y-m-d', strtotime($_GET['startdate']));
    }else{
    	$startdate = date("Y-m-d",strtotime("first day of last month"));//date('Y-m-01');
    }
    
    $show_startdate = date('d F Y', strtotime($startdate));
    $start = date('Ymd', strtotime($startdate));
    
    if(isset($_GET['enddate'])){
    	$enddate = date('Y-m-d', strtotime($_GET['enddate']));
    }else{
    	$enddate = date("Y-m-d",strtotime('last day of last month'));//date('Y-m-d');
    }
    
    $show_enddate = date('d F Y', strtotime($enddate));
    $end = date('Ymd', strtotime($enddate));
    
    $lastmonthstart = date("d-m-Y",strtotime("first day of last month"));
    $lastmonthend = date("d-m-Y",strtotime("last day of last month"));
    $currentmonthstart = date("d-m-Y",strtotime("first day of this month"));
    $currentmonthend = date("d-m-Y");
?>

<div class="box box-form">
    <form method="GET" action="<?php echo $base_url.'/dashboard/'?>">
	<div style="float:right;">
	    <a href="<?php echo $base_url.'/dashboard/?startdate='.$lastmonthstart.'&enddate='.$lastmonthend?>"><button type="button" class="small-button blue">Last Month</button></a>
	    <a href="<?php echo $base_url.'/dashboard/?startdate='.$currentmonthstart.'&enddate='.$currentmonthend?>"><button type="button" class="small-button blue">Current Month</button></a>
		 &nbsp; &nbsp; or specify the periode : <input type="text" name="startdate" id="from" class="calinput" readonly value="<?php echo date('d-m-Y', strtotime($start));?>" style="width:100px"> &nbsp;- &nbsp;
            <input type="text" id="to" name="enddate" class="calinput" readonly value="<?php echo date('d-m-Y', strtotime($end));?>" style="width:100px">
            <button type="submit" class="small-button blue"><i class="fa fa-search"></i>Show</button>
	</div>
	</form>
    <h2 style="line-height:35px;">Filter by Date</h2>
	<div style="clear:both;"></div>
</div>
<div id="dashboard">
    
<?php
    if(isset($_REQUEST['roomtype'])){ $roomtype = $_REQUEST['roomtype']; }else{ $roomtype = ""; }

	$usr = $_SESSION['_typeusr'];//$_REQUEST['usr'];
	$useroid = $_SESSION['_oid'];//$_REQUEST['useroid'];

	/*if($_SESSION['_typeusr'] == "4"){
		$main_query =
			"SELECT c.cityname, s.statename, h.hoteloid, h.hotelname, h.stars, h.publishedoid, (case when h.publishedoid = 2 then 'status-red' when h.publishedoid = 1 then 'status-green' else 'status-black' end) as status, ht.category
			from hotel h inner join hoteltype ht using (hoteltypeoid) inner join published using (publishedoid)
			inner join city c using (cityoid) inner join state s using (stateoid) inner join country ct using (countryoid)
			inner join
		";
	}else{*/
		$main_query =
			"SELECT c.cityname, s.statename, h.hoteloid, h.hotelname, h.stars, h.publishedoid, (case when h.publishedoid = 2 then 'status-red' when h.publishedoid = 1 then 'status-green' else 'status-black' end) as status, ht.category
			from hotel h inner join hoteltype ht using (hoteltypeoid) inner join published using (publishedoid)
			inner join city c using (cityoid) inner join state s using (stateoid) inner join country ct using (countryoid)
		";
	// }

	$filter = array();
	if(isset($_REQUEST['name']) and $_REQUEST['name']!=''){
		array_push($filter, 'h.hotelname like "%'.$_REQUEST['name'].'%"');
	}
	if(isset($_REQUEST['type']) and $_REQUEST['type']!=''){
		array_push($filter, 'h.hoteltypeoid = "'.$_REQUEST['type'].'"');
	}
	if(isset($_REQUEST['star']) and $_REQUEST['star']!=''){
		array_push($filter, 'h.stars = "'.$_REQUEST['star'].'"');
	}
	if(isset($_REQUEST['country']) and $_REQUEST['country']!=''){
		array_push($filter, 'ct.countryoid = "'.$_REQUEST['country'].'"');
	}

	// array_push($filter, 'h.publishedoid not in (3) and h.hotelstatusoid IN (1,10) ');
	array_push($filter, 'h.publishedoid not in (3) ');

	//--- Filter CHAIN
	if($usr == "4"){
		$s_chain = "SELECT ua.oid FROM users u INNER JOIN userassign ua USING (useroid) WHERE ua.type = 'chainoid' AND u.useroid = '".$useroid."'";
		// echo $s_chain;
		$q_chain = $db->query($s_chain);
		$n_chain = $q_chain->rowCount();
		$r_chain = $q_chain->fetchAll(PDO::FETCH_ASSOC);
		foreach($r_chain as $row){
			array_push($filter, "h.chainoid = '".$row['oid']."'");
			// array_push($filter, 'h.hotelname LIKE "%amaz%" ');
		}
	}

	if(count($filter) > 0){
		$combine_filter = implode(' and ',$filter);
		$query = $main_query.' where '.$combine_filter;
	}else{
		$query = $main_query;
	}

	try {
		$main_query = $query." ORDER BY h.hoteloid DESC ";
		$stmt = $db->query($main_query.$paging_query);
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
		    
		    //---------------------------
		    
		    function getClick($hoteloid, $page, $start, $end){
        		global $xpath;
        		if($page == "all"){
        			$countavailability = $xpath->query('//record[@hotel="'.$hoteloid.'"][number(translate(@date,"-","")) >= '.$start.' and number(translate(@date,"-","")) <=  '.$end.']');
        		}else{
        			$countavailability = $xpath->query('//record[@hotel="'.$hoteloid.'"][@page="'.$page.'"][number(translate(@date,"-","")) >= '.$start.' and number(translate(@date,"-","")) <=  '.$end.']');
        		}
        	
        		$avg = $countavailability->length / date('d');
        		return $countavailability->length.' - avg '.ceil($avg).' /day';
        	}
        	
        	function getVisit($hoteloid, $page, $start, $end){
        		global $xpath;
        		if($page == "all"){
        			$countavailability = $xpath->query('//record[@hotel="'.$hoteloid.'"][number(translate(@date,"-","")) >= '.$start.' and number(translate(@date,"-","")) <=  '.$end.']');
        		}else{
        			$countavailability = $xpath->query('//record[@hotel="'.$hoteloid.'"][@page="'.$page.'"][number(translate(@date,"-","")) >= '.$start.' and number(translate(@date,"-","")) <=  '.$end.']');
        		}
        		return $countavailability->length;
        	}
        	
        	function getHitsData($hoteloid, $tag, $page, $start, $end){
        		global $xpath;
        		$data = array();
        		
        		if(!is_array($tag)){ $tag = array($tag);}
        		
        		foreach($tag as $key => $valuetag){
        			$query_tag = '//record[@hotel="'.$hoteloid.'"][@page="'.$page.'"][number(translate(@date,"-","")) >= '.$start.' and number(translate(@date,"-","")) <=  '.$end.']/'.$valuetag;
        			foreach($xpath->query($query_tag) as $tag_result){
        				
        				if($valuetag == "tb_source" or $valuetag == "utm_source"){
        					if(empty($tag_result->textContent)){
        						$tag_result->textContent = "bookingbutton";
        					}
        				}
        				
        				if(in_array_multidimension($tag_result->textContent, $data)){
        					$list_key_data = array_column($data, 'name');
        					$key = array_search($tag_result->textContent, $list_key_data);
        		
        					$hits = $data[$key]['hits'] + 1;
        					$data[$key]['hits'] = $hits;
        				}else{
        					array_push($data, array('name' => $tag_result->textContent, 'hits' => 1));
        				}
        			}
        		}
        		return $data;
        	}
        	
        	function getReferalSource($hoteloid, $tag, $page, $start, $end){
        		global $xpath;
        		$data = array();
        		
        		$organic = 0; $referral = 0;
        		
        		if(!is_array($tag)){ $tag = array($tag);}
        		
        		foreach($tag as $key => $valuetag){
        			$query_tag = '//record[@hotel="'.$hoteloid.'"][@page="'.$page.'"][number(translate(@date,"-","")) >= '.$start.' and number(translate(@date,"-","")) <=  '.$end.']/'.$valuetag;
        			foreach($xpath->query($query_tag) as $tag_result){
        				if(empty($tag_result->textContent) or $tag_result->textContent == "bookingbutton"){
        					$organic++;
        				}else{
        					$referral++;
        				}
        			}
        		}
        		return array($organic,$referral);
        	}
        
        	function in_array_multidimension($item , $array){
        		return preg_match('/"'.$item.'"/i' , json_encode($array));
        	}
		    
		    //---------------------------
        
            function differenceDate($start, $end){
                $date1 = new DateTime($start);
                $date2 = new DateTime($end);
                $interval = $date1->diff($date2);
                return $interval->days;
            }
            
            function countResult($start, $end, $hoteloid, $roomtype, $status){
                global $db;
                
                $main_query = "select count(DISTINCT b.bookingoid) as jmldata from booking b inner join hotel h using (hoteloid) inner join bookingroom br using (bookingoid) inner join roomoffer ro using (roomofferoid) inner join room r using (roomoid) where (date(bookingtime) >= '".$start."' and date(bookingtime) <= '".$end."') and h.hoteloid = '".$hoteloid."'";
                $filter = array();
                if($status != ""){
                    array_push($filter, "b.bookingstatusoid = '".$status."'");
                }
                if($roomtype != ""){
                    array_push($filter, "r.roomoid = '".$roomtype."'");
                }
                
                if(count($filter) > 0){
                    $combine_filter = implode(' and ',$filter);
                    $query = $main_query.' and '.$combine_filter;
                }else{
                    $query = $main_query;
                }
                
                $stmt	= $db->query($query);
                $result = $stmt->fetch(PDO::FETCH_ASSOC);
    
                return $result['jmldata'];
            }
            
            $day = differenceDate($start, $end);
            

			echo '<div class="row">
			    <div class="col-sm-12">
			        <div class="box box-primary">
			            <div class="box-header">
			                <h4>Summary from '.date('d F Y', strtotime($start)).' to '.date('d F Y', strtotime($end)).'</h4>
			            </div>
			            <div class="box-body">';
			
			echo '<table id="example" cellspacing="0" class="table table-striped">
                  <thead>
                    <tr>
                        <th>Hotel Name</th>
                        <th>Commission</th>
                        <th>Revenue</th>
                        <th>Cancelled Value</th>
                        <th>Net to Hotel</th>
                        <th>Visits</th>
                        <th>Bookings</th>
                        <th>Confirmed</th>
                        <th>Cancelled</th>
                        <th>R/N</th>
                        <th>Conversion Rate</th>
                    </tr>
                  </thead>
                  <tbody>';
			
			$r_hotel = $stmt->fetchAll(PDO::FETCH_ASSOC); $nn=1;
            
			foreach($r_hotel as $row){
				$hoteloid = $row['hoteloid'];
				$hotelname = $row['hotelname'];
				$cityname = $row['cityname']; $statename = $row['statename'];
				$star =  $row['stars']; if(empty($star) or $star < 1){ $star = "-"; }
				$type = $row['category'];
				
				//------------------------
				
				$today = date("Y-m-d");
                $today_plus_7 = date("Y-m-d",strtotime($today." +7 day"));
                
                $this_week_firstdate = date("Y-m-d",strtotime("this week"));
                $this_week_lastdate = date("Y-m-d",strtotime("this week +6 days"));
                
                $this_month_firstdate = date("Y-m-d",strtotime("first day of this month"));
                $this_month_lastdate = date("Y-m-d",strtotime("last day of this month"));
                
                $query_form = "
                	from booking b
                	inner join hotel h using (hoteloid)
                	inner join bookingstatus bs using (bookingstatusoid)
                	inner join customer c using (custoid)
                	inner join bookingroom br using (bookingoid)
                	inner join roomoffer ro using (roomofferoid)
                	inner join room r using (roomoid)
                	left join bookingpayment using (bookingoid)
                	where h.hoteloid = '".$hoteloid."' ";
                
                $query_group_bookingoid = " group by b.bookingoid";
                
                $query_form_detail = "
                	from booking b
                	inner join hotel h using (hoteloid)
                	inner join bookingstatus bs using (bookingstatusoid)
                	inner join customer c using (custoid)
                	inner join bookingroom br using (bookingoid)
                	inner join roomoffer ro using (roomofferoid)
                	inner join room r using (roomoid)
                	inner join bookingroomdtl brd using (bookingroomoid)
                	left join bookingpayment using (bookingoid)
                	where h.hoteloid = '".$hoteloid."' ";
                
                	include('includes/dashboard/function-tracking.php');
                
                	$startdate = date('Y-m-01');
                	$enddate = date('Y-m-d');
                	$allvisit = getVisit($hoteloid, "availability", $start, $end);
                
                	if($allvisit > 0){
                		$averagevisit = floor($allvisit / date('t'));
                	}else{
                		$averagevisit = 0;
                	}
                
                	$sum_allbooking 	= "select count(bookingoid) as booking from booking inner join hotel h using (hoteloid) where (date(bookingtime) >= '".$start."' and date(bookingtime) <= '".$end."') and h.hoteloid = '".$hoteloid."' and booking.bookingstatusoid = '4'";
                	$stmt			= $db->query($sum_allbooking);
                	$arr_allbooking	= $stmt->fetch(PDO::FETCH_ASSOC);
                	$countbooking 	= $arr_allbooking['booking'];
                
                	$sum_allroomnight 	= "select count(bookingroomdtloid) as roomnight from bookingroomdtl inner join bookingroom using (bookingroomoid) inner join booking using (bookingoid) inner join hotel h using (hoteloid) where (date(bookingtime) >= '".$start."' and date(bookingtime) <= '".$end."') and h.hoteloid = '".$hoteloid."' and booking.bookingstatusoid = '4'";
                	
                	$stmt				= $db->query($sum_allroomnight);
                	$arr_allroomnight	= $stmt->fetch(PDO::FETCH_ASSOC);
                	$countroomnight		= $arr_allroomnight['roomnight'];
                
                	if($countbooking > 0){
                		$connversionrate_booking = number_format($countbooking / $allvisit * 100,2,",",".");
                	}else{
                		$connversionrate_booking = 0;
                	}
                
                	if($countroomnight > 0){
                		$connversionrate_roomnight = number_format($countroomnight / $allvisit * 100,2,",",".");
                	}else{
                		$connversionrate_roomnight = 0;
                	}
				
				//------------------------
				
				$show_date = array();
                $point_reservation = array();
                $point_confirmed = array();
                $point_cancelled = array();
                $point_noshow = array();
                for($i = 0; $i <= $day; $i++){
                    $date	= date('Y-m-d', strtotime($from. '+'.$i.' day'));
                    array_push($show_date, date('d M', strtotime($date)));
                    array_push($point_reservation, countResult($date, $date, $hoteloid, $roomtype, ""));
                    array_push($point_confirmed, countResult($date, $date, $hoteloid, $roomtype, 4));
                    array_push($point_cancelled, countResult($date, $date, $hoteloid, $roomtype, 5));
                    array_push($point_noshow, countResult($date, $date, $hoteloid, $roomtype, 7));
                }
                
                $sum_revenue 	= "select sum(grandtotal) as revenue, count(bookingoid) as confirmed from booking inner join hotel h using (hoteloid) where (date(bookingtime) >= '".$start."' and date(bookingtime) <= '".$end."') and h.hoteloid = '".$hoteloid."'  and bookingstatusoid in ('4')";
                $stmt			= $db->query($sum_revenue);
                $arr_revenue	= $stmt->fetch(PDO::FETCH_ASSOC); 
        		$revenue 		= floor($arr_revenue['revenue']);
        		
                $sum_commission	= "select sum(gbhcollect) as commission from booking inner join hotel h using (hoteloid) where (date(bookingtime) >= '".$start."' and date(bookingtime) <= '".$end."') and h.hoteloid = '".$hoteloid."' and bookingstatusoid in ('4')";
                $stmt			= $db->query($sum_commission);
                $arr_commission = $stmt->fetch(PDO::FETCH_ASSOC); 
        		$commission		= floor($arr_commission['commission']);
        
                $sum_cancellationamount = "select sum(cancellationamount) as cancelled from booking inner join hotel h using (hoteloid) where (date(bookingtime) >= '".$start."' and date(bookingtime) <= '".$end."') and h.hoteloid = '".$hoteloid."' and bookingstatusoid in ('5','7')";
                $stmt					= $db->query($sum_cancellationamount);
                $arr_cancellationamount = $stmt->fetch(PDO::FETCH_ASSOC);
        		$cancellationamount		= floor($arr_cancellationamount['cancelled']);
        		
        		$nethotel = $revenue - $commission;
        		
        		$totalreservation = countResult($start, $end, $hoteloid, $roomtype, "");
        		$totalconfirmed = countResult($start, $end, $hoteloid, $roomtype, 4);
        		if($totalreservation > 0){ 
                    $barconfirmed = floor($totalconfirmed / $totalreservation * 100); 
                }else{
                    $barconfirmed = 0;
                }
                $totalcancelled = countResult($start, $end, $hoteloid, $roomtype, 5); 
                if($totalreservation > 0){ 
                    $barcancelled = floor($totalcancelled / $totalreservation * 100); 
                }else{
                    $barcancelled = 0;
                }
                $totalnoshow = countResult($start, $end, $hoteloid, $roomtype, 7);
                if($totalreservation > 0){ 
                    $barnoshow = floor($totalnoshow / $totalreservation * 100); 
                }else{
                    $barnoshow = 0;
                }
                
                $numofroom = 0;
                $query = "select count(bookingroomdtloid) as numberroom ".$query_form_detail." and (DATE(bookingtime) >= '".$start."' and DATE(bookingtime) <= '".$end."') ";
                $stmt = $db->query($query);
                $r_arrival = $stmt->fetchAll(PDO::FETCH_ASSOC);
                foreach($r_arrival as $row){
                    $numofroom = $row['numberroom'];
                }

				/*echo '<div class="col-md-4">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title" style="font-size:14px">'.$hotelname.'</h3>
					</div>
					<div class="box-body">
						<div>
							<div class="progress-group">
								<span class="progress-text">Visits: </span>
								<span class="progress-number"><b>'.$averagevisit.'</b></span>
								<!--<div align="right">increase 0% from last</div>-->
								<div class="progress sm">
								<div class="progress-bar progress-bar-aqua" style="width: 0%"></div>
								</div>
							</div>
							<!-- /.progress-group -->
							<div class="progress-group">
								<span class="progress-text">Bookings</span>
								<span class="progress-number"><b>'.number_format($totalreservation).'</b></span>
								<!--<div align="right">decrease 0% from last</div>-->
								<div class="progress sm">
								<div class="progress-bar progress-bar-yellow" style="width: 100%"></div>
								</div>
							</div>
							<!-- /.progress-group -->
							<div class="progress-group">
								<span class="progress-text">Confirmed</span>
								<span class="progress-number"><b>'.number_format($totalconfirmed).'</b>/'.number_format($totalreservation).'</span>

								<div class="progress sm">
								<div class="progress-bar progress-bar-green" style="width: '.$barconfirmed.'%"></div>
								</div>
							</div>
							<!-- /.progress-group -->
							<div class="progress-group">
								<span class="progress-text">Cancelled</span>
								<span class="progress-number"><b>'.number_format($totalnoshow).'</b>/'.number_format($totalreservation).'</span>

								<div class="progress sm">
								<div class="progress-bar progress-bar-red" style="width: '.$barnoshow.'%"></div>
								</div>
							</div>
							<!-- /.progress-group -->
							<div class="progress-group">
								<span class="progress-text">R/N</span>
								<span class="progress-number"><b>'.$numofroom.'</b></span>
							</div>
							<!-- /.progress-group -->
							<div class="progress-group">
								<span class="progress-text">Conversion Rate</span>
								<span class="progress-number"><b>'.$connversionrate_booking.'%</b></span>

								<div class="progress sm">
								<div class="progress-bar progress-bar-blue" style="width: '.$connversionrate_booking.'%"></div>
								</div>
							</div>
							<!-- /.progress-group -->
						</div>
						<ul class="nav nav-pills nav-stacked">
							<li><a style="cursor:default">Revenue <span class="pull-right text-green"><!--<i class="fa fa-angle-up"></i>--> IDR '.number_format($revenue).'</span></a></li>
				 			<li><a style="cursor:default">Confirmed <span class="pull-right text-green"> '.number_format($totalconfirmed).'</span></a></li>
							<li><a style="cursor:default">Cancelled <span class="pull-right text-red"> IDR '.number_format($cancellationamount).'</span></a></li>
							<li><a style="cursor:default">Commission <span class="pull-right text-yellow"> IDR '.number_format($commission).'</span></a></li>
							<li><a style="cursor:default">Net to Hotel <span class="pull-right text-yellow"> IDR '.number_format($nethotel).'</span></a></li>
						</ul>
					</div>
					<div class="box-footer text-center">
						<button type="button" class="small-button blue manage" hoid="'.$hoteloid.'" style="width:100%">Manage</button>
					</div>
				</div>
				</div>';
				
				if($nn % 3 == 0){
					echo '</div><div class="row">';
				}*/
				
				echo '<tr>
                        <td>'.$hotelname.'</td>
                        <td>IDR '.number_format($commission).'</td>
                        <td>IDR '.number_format($revenue).'</td>
                        <td>IDR '.number_format($cancellationamount).'</td>
                        <td>IDR '.number_format($nethotel).'</td>
                        <td>'.$averagevisit.'</td>
                        <td>'.number_format($totalreservation).'</td>
                        <td>'.number_format($totalconfirmed).'</td>
                        <td>'.number_format($totalnoshow).'</td>
                        <td>'.$numofroom.'</td>
                        <td>'.$connversionrate_booking.'%</td>
                    </tr>';
                
				
				$nn++;
			}
            
            echo  '</tbody>
                    </table>';
			
			echo '</div>
			    </div>
			 </div>
			</div>';

		}

	}catch(PDOException $ex) {
		echo "Invalid Query";
		print($ex);
		die();
	}

?>

    <script>
    $( function() {
        var dateFormat = "dd-mm-yy",
        from = $( "#from" )
            .datepicker({
            defaultDate: "+1d",
            changeMonth: true,
			dateFormat: "dd-mm-yy",
            numberOfMonths: 1
            })
            .on( "change", function() {
            to.datepicker( "option", "minDate", getDate( this ) );
            }),
        to = $( "#to" ).datepicker({
            defaultDate: "+1d",
            changeMonth: true,
			dateFormat: "dd-mm-yy",
            numberOfMonths: 1
        })
        .on( "change", function() {
            from.datepicker( "option", "maxDate", getDate( this ) );
        });
    
        function getDate( element ) {
            var date;
            try {
                date = $.datepicker.parseDate( dateFormat, element.value );
            } catch( error ) {
                date = null;
            }
            return date;
        }
        
        $('body').on('click','button.manage', function(e) {
        	var loginas_hc = $(this).attr('hoid');
        	var loginas_hname = $(this).parent().parent().children('div').eq(0).children('h3').html();
        	$.ajax({
        		type	: 'POST', cache: false,
        		url		: '<?php echo"$base_url"; ?>/includes/suadm/hotel/change-session.php',
        		data	: { loginas : loginas_hc, hname : loginas_hname },
        		success	: function(rsp){ if(rsp == "1"){ document.location.href = '<?php echo"$base_url"; ?>/dashboard'; } }
        	});
        });
        
        $('#example').DataTable({
            "aaSorting": [
              [1, "dsc"], [2, "dsc"]
            ],
        });
    } );
    </script>

</div>
