<div class="box box-warning box-form">
  <div id="data-box" class="box-body">
    <h1>Dynamic Rate Rules</h1>
    <br>
    <div class="row">
      <div class="col-md-12">
        <div class="row">
          <div class="col-xs-2"><label>Rule Status</label></div>
          <div class="col-xs-6">
            <label class="switch">
              <?php
              $stmt = $db->prepare("select dynamicrate from hotel where hoteloid = :a");
              $stmt->execute(array(':a' => $hoteloid));
              $activate = $stmt->fetch(PDO::FETCH_ASSOC);
              ?>
              <input type="checkbox" name="activerule" <?php if($activate['dynamicrate'] == "1"){ echo "checked"; } ?>>
              <span class="slider round"></span>
            </label>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6"><h1>Condition</h1></div>
          <div class="col-md-6 text-right"><button type="button" class="btn btn-primary" name="add-condition"  data-toggle="modal" data-target="#addCondition">Add Condition</button></div>
        </div>
        <hr>
        <div class="list-condition">
          <?php
          $stmt = $db->prepare("select * from dynamicrate where hoteloid = :a and publishedoid = '1'");
          $stmt->execute(array(':a' => $_SESSION['_hotel']));
          foreach($stmt->fetchAll(PDO::FETCH_ASSOC) as $rule){
          ?>
          <div>
            <div>
              <div class="col-md-6"><label>If allocation &le; <?=$rule['allocation']?></label></div>
              <div class="col-md-6 text-right">
                <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#updateCondition" data-id="<?=$rule['dynamicrateoid']?>"><i class="fa fa-pencil"></i></button>
                <button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#removeCondition" data-id="<?=$rule['dynamicrateoid']?>"><i class="fa fa-trash"></i></button>
              </div>
            </div>
            <table class="table table-striped">
              <tr><th>Room &amp; Rate Plan</th><th>Update Rate to</th></tr>
              <?php
                $stmt = $db->prepare("select r.name as roomname, ro.name as roomoffer, mr.bar, cr.currencycode, mr.rate from dynamicraterule dr inner join roomoffer ro using (roomofferoid) inner join room r using (roomoid) inner join masterrate mr using (masterrateoid) inner join currency cr using (currencyoid) inner join dynamicrate d using (dynamicrateoid) where dr.dynamicrateoid = :a and d.publishedoid = '1'");
                $stmt->execute(array(':a' => $rule['dynamicrateoid']));
                foreach($stmt->fetchAll(PDO::FETCH_ASSOC) as $dynamicrate){
              ?>
                <tr><td><?=$dynamicrate['roomname']." ".$dynamicrate['roomoffer']?></td><td><?=$dynamicrate['bar']." - ".$dynamicrate['currencycode']." ".number_format($dynamicrate['rate'])?></td></tr>
              <?php
                }
             ?>
            </table>
          </div>
          <?php
          }
          ?>
        </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="addCondition" tabindex="-1" role="dialog" aria-labelledby="addCondition">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Condition</h4>
      </div>
      <div class="modal-body">
      loading ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" name="save">Save changes</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="removeCondition" tabindex="-1" role="dialog" aria-labelledby="removeCondition">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Delete Condition</h4>
      </div>
      <div class="modal-body">Are you sure to delete this dynamic rate rule?<input type="hidden" name="dynamicrule"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-danger" name="delete">Yes</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="updateCondition" tabindex="-1" role="dialog" aria-labelledby="updateCondition">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Update Condition</h4>
      </div>
      <div class="modal-body">
      loading ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" name="save">Save changes</button>
      </div>
    </div>
  </div>
</div>
