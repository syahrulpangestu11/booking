<script type="text/javascript">
$(function(){

function showInventory(date, request){
	box = $('div#show-inventory');
	hotelcode = $('select[name=hotelcode]').find(":selected").val();
	$.ajax({
		url: "<?php echo"$base_url"; ?>/includes/reports/revenue-chain/data-revenue-chain.php",
		type: 'post',
		data: { lastdate: date, request: request, hotelcode : hotelcode },
		success: function(data) {
			box.html(data);
		}
	});
}

$(document).on('change','input[name=date]', function(e){
	showInventory($(this).val(), 'today');
});

$(document).on('change','select[name=hotelcode]', function(e){
	date = $('input[name=spotdate]').val();
	showInventory(date, 'today');
});

$(document).ready(function(){
	showInventory('<?=date('Y-m-d');?>', 'today');
});

$(document).on('click','button.inv-nav', function(e){
	date = $('input[name=spotdate]').val();
	request = $(this).val();
	showInventory(date, request);
});

$(document).ready(function(){
	showInventory('<?=date('Y-m-d');?>', 'today');
});

$( "#datepicker" ).datepicker({
	changeMonth: true,
	changeYear: true,
});

});
</script>
