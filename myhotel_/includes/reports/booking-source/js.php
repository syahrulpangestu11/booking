<script type="text/javascript">
$(function(){
	$(document).ready(function(){ 
		getLoadData();
	});
	
	function getLoadData(){
		$.ajax({
			url: "<?php echo"$base_url"; ?>/includes/reports/booking-source/data.php",
			type: 'post',
			data: $('form#data-input').serialize(),
			success: function(data) {
				$("#data-box").html(data)
			}
		});
	}	

	$('body').on('click','tr.list', function(e) {	
		bn = $(this).children('td').eq(0).html();
		$(location).attr("href", "<?php echo"$base_url"; ?>/booking/detail/?no="+bn);
	});
});
</script>