<?php
try{
    session_start();
    error_reporting(E_ALL ^ E_NOTICE);
    include ('../../../conf/connection.php');

    $hoteloid = $_POST["hoteloid"];
    
    $msg = "";
    $query = "";
    if(isset($_POST["roomtype"]) and is_array($_POST["roomtype"]) and !in_array("0", $_POST["roomtype"])){
        $in = implode(",",$_POST["roomtype"]);
        $query .= "AND r.roomoid IN (".$in.")";
    }
    
    $stmt = $db->prepare("SELECT r.`roomoid`, r.`name` 
        FROM `room` r INNER JOIN `roomoffer` ro USING(`roomoid`)
        WHERE r.`hoteloid`=:a AND r.publishedoid NOT IN (3) AND ro.publishedoid NOT IN (3) ".$query."
        GROUP BY r.`roomoid`");
    $stmt->execute(array(":a"=>$hoteloid));
    $r_xroom = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
    if($_POST["filtersearch"]=="booking"){
        if($_POST["period"]=="annual"){
            $year = $_POST["annually"];
            $query .= "AND YEAR(b.bookingtime)='".$year."' ";
            $msg = "<p>Filter by Booking Date Annualy on ".$year."</p>";
            $theader = $year;
        }else if($_POST["period"]=="month"){
            $year = $_POST["monthly_year"];
            $month = $_POST["monthly_month"];
            $query .= "AND YEAR(b.bookingtime)='".$year."' AND MONTH(b.bookingtime)='".$month."' ";
            $msg = "<p>Filter by Booking Date Monthly on ".date("F", strtotime($year."-".$month."-1"))." ".$year."</p>";
            $theader = date("F", strtotime($year."-".$month."-1"))." ".$year;
        }else if($_POST["period"]=="week"){
            $year = $_POST["weekly_year"];
            $week = explode('|',$_POST["weekly_week"]);
            $query .= "AND b.bookingtime >= '".$week[0]."' AND b.bookingtime <= '".$week[1]."' ";
            $msg = "<p>Filter by Booking Date Weekly on ".date("d M Y", strtotime($week[0]))." until ".date("d M Y", strtotime($week[1]))."</p>";
            $theader = date("d M Y", strtotime($week[0]))." until ".date("d M Y", strtotime($week[1]));
        }else if($_POST["period"]=="range"){
            $startdate = date("Y-m-d", strtotime($_POST["range_start"]));
            $enddate = date("Y-m-d", strtotime($_POST["range_end"]));
            $query .= "AND b.bookingtime >= '".$startdate."' AND b.bookingtime <= '".$enddate."' ";
            $msg = "<p>Filter by Booking Date Range on ".date("d M Y", strtotime($startdate))." until ".date("d M Y", strtotime($enddate))."</p>";
            $theader = date("d M Y", strtotime($startdate))." until ".date("d M Y", strtotime($enddate));
        }
    }else if($_POST["filtersearch"]=="checkin"){
        if($_POST["period"]=="annual"){
            $year = $_POST["annually"];
            $query .= "AND YEAR(br.checkin)='".$year."' ";
            $msg = "<p>Filter by Check-in Date Annualy on ".$year."</p>";
            $theader = $year;
        }else if($_POST["period"]=="month"){
            $year = $_POST["monthly_year"];
            $month = $_POST["monthly_month"];
            $query .= "AND YEAR(br.checkin)='".$year."' AND MONTH(br.checkin)='".$month."' ";
            $msg = "<p>Filter by Check-in Date Monthly on ".date("F", strtotime($year."-".$month."-1"))." ".$year."</p>";
            $theader = date("F", strtotime($year."-".$month."-1"))." ".$year;
        }else if($_POST["period"]=="week"){
            $year = $_POST["weekly_year"];
            $week = explode('|',$_POST["weekly_week"]);
            $query .= "AND br.checkin >= '".$week[0]."' AND br.checkin <= '".$week[1]."' ";
            $msg = "<p>Filter by Check-in Date Weekly on ".date("d M Y", strtotime($week[0]))." until ".date("d M Y", strtotime($week[1]))."</p>";
            $theader = date("d M Y", strtotime($week[0]))." until ".date("d M Y", strtotime($week[1]));
        }else if($_POST["period"]=="range"){
            $startdate = date("Y-m-d", strtotime($_POST["range_start"]));
            $enddate = date("Y-m-d", strtotime($_POST["range_end"]));
            $query .= "AND br.checkin >= '".$startdate."' AND br.checkin <= '".$enddate."' ";
            $msg = "<p>Filter by Check-in Date Range on ".date("d M Y", strtotime($startdate))." until ".date("d M Y", strtotime($enddate))."</p>";
            $theader = date("d M Y", strtotime($startdate))." until ".date("d M Y", strtotime($enddate));
        }
    }

    $stmt = $db->prepare("SELECT r.`roomoid`, r.`name`, COUNT(`bookingroomoid`) AS bid, sum(br.`total`) AS rev, sum(br.`night`) AS rnight, 
        sum(br.`night`)/COUNT(`bookingroomoid`) As avgstay, DATEDIFF(br.`checkin`,b.`bookingtime`) as avglead, 
        sum(br.`total`) / sum(br.`night`) as avgrate
        FROM `bookingroom` br INNER JOIN `booking` b USING(`bookingoid`) INNER JOIN `roomoffer` ro USING(`roomofferoid`) 
            INNER JOIN `room` r USING(`roomoid`)
        WHERE r.`hoteloid`=:a AND pmsstatus='0' AND r.publishedoid NOT IN (3) AND ro.publishedoid NOT IN (3) ".$query." GROUP BY r.`roomoid`, r.`name`");
    $stmt->execute(array(":a"=>$hoteloid));
    $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
    $data_room = array();
    $data_revenue = array();
    $data_rnight = array();
    $data_avgstay = array();
    $data_avglead = array();
    $data_avgrate = array();
    foreach($r_room as $room){
        $data_room[$room['roomoid']] = $room['bid'];
        $data_revenue[$room['roomoid']] = $room['rev'];
        $data_rnight[$room['roomoid']] = $room['rnight'];
        $data_avgstay[$room['roomoid']] = $room['avgstay'];
        $data_avglead[$room['roomoid']] = $room['avglead'];
        $data_avgrate[$room['roomoid']] = $room['avgrate'];
    }
    
    //***********------- COMPARE -------*************//
    $msg1 = "";
    $r_room1 = array();
    if(isset($_POST['compare']) and $_POST['compare']=='1'){
        $query1 = "";
        if(isset($_POST["roomtype"]) and is_array($_POST["roomtype"]) and !in_array("0", $_POST["roomtype"])){
            $in = implode(",",$_POST["roomtype"]);
            $query1 .= "AND r.roomoid IN (".$in.")";
        }
        if($_POST["filtersearch"]=="booking"){
            if($_POST["period"]=="annual"){
                $year = $_POST["var_annually"];
                $query1 .= "AND YEAR(b.bookingtime)='".$year."' ";
                $msg1 = "<p>Compare with ".$year."</p>";
                $theader1 = $year;
            }else if($_POST["period"]=="month"){
                $year = $_POST["var_monthly_year"];
                $month = $_POST["var_monthly_month"];
                $query1 .= "AND YEAR(b.bookingtime)='".$year."' AND MONTH(b.bookingtime)='".$month."' ";
                $msg1 = "<p>Compare with ".date("F", strtotime($year."-".$month."-1"))." ".$year."</p>";
                $theader1 = date("F", strtotime($year."-".$month."-1"))." ".$year;
            }else if($_POST["period"]=="week"){
                $year = $_POST["var_weekly_year"];
                $week = explode('|',$_POST["var_weekly_week"]);
                $query1 .= "AND b.bookingtime >= '".$week[0]."' AND b.bookingtime <= '".$week[1]."' ";
                $msg1 = "<p>Compare with ".date("d M Y", strtotime($week[0]))." until ".date("d M Y", strtotime($week[1]))."</p>";
                $theader1 = date("d M Y", strtotime($week[0]))." until ".date("d M Y", strtotime($week[1]));
            }else if($_POST["period"]=="range"){
                $startdate = date("Y-m-d", strtotime($_POST["var_range_start"]));
                $enddate = date("Y-m-d", strtotime($_POST["var_range_end"]));
                $query1 .= "AND b.bookingtime >= '".$startdate."' AND b.bookingtime <= '".$enddate."' ";
                $msg1 = "<p>Compare with ".date("d M Y", strtotime($startdate))." until ".date("d M Y", strtotime($enddate))."</p>";
                $theader1 = date("d M Y", strtotime($startdate))." until ".date("d M Y", strtotime($enddate));
            }
        }else if($_POST["filtersearch"]=="checkin"){
            if($_POST["period"]=="annual"){
                $year = $_POST["var_annually"];
                $query1 .= "AND YEAR(br.checkin)='".$year."' ";
                $msg1 = "<p>Compare with ".$year."</p>";
                $theader1 = $year;
            }else if($_POST["period"]=="month"){
                $year = $_POST["var_monthly_year"];
                $month = $_POST["var_monthly_month"];
                $query1 .= "AND YEAR(br.checkin)='".$year."' AND MONTH(br.checkin)='".$month."' ";
                $msg1 = "<p>Compare with ".date("F", strtotime($year."-".$month."-1"))." ".$year."</p>";
                $theader1 = date("F", strtotime($year."-".$month."-1"))." ".$year;
            }else if($_POST["period"]=="week"){
                $year = $_POST["var_weekly_year"];
                $week = explode('|',$_POST["var_weekly_week"]);
                $query1 .= "AND br.checkin >= '".$week[0]."' AND br.checkin <= '".$week[1]."' ";
                $msg1 = "<p>Compare with ".date("d M Y", strtotime($week[0]))." until ".date("d M Y", strtotime($week[1]))."</p>";
                $theader1 = date("d M Y", strtotime($week[0]))." until ".date("d M Y", strtotime($week[1]));
            }else if($_POST["period"]=="range"){
                $startdate = date("Y-m-d", strtotime($_POST["var_range_start"]));
                $enddate = date("Y-m-d", strtotime($_POST["var_range_end"]));
                $query1 .= "AND br.checkin >= '".$startdate."' AND br.checkin <= '".$enddate."' ";
                $msg1 = "<p>Compare with ".date("d M Y", strtotime($startdate))." until ".date("d M Y", strtotime($enddate))."</p>";
                $theader1 = date("d M Y", strtotime($startdate))." until ".date("d M Y", strtotime($enddate));
            }
        }
    
        $stmt = $db->prepare("SELECT r.`roomoid`, r.`name`, COUNT(`bookingroomoid`) AS bid, sum(br.`total`) AS rev, sum(br.`night`) AS rnight,
            sum(br.`night`)/COUNT(`bookingroomoid`) As avgstay, DATEDIFF(br.`checkin`,b.`bookingtime`) as avglead, 
            sum(br.`total`) / sum(br.`night`) as avgrate 
            FROM `bookingroom` br INNER JOIN `booking` b USING(`bookingoid`) INNER JOIN `roomoffer` ro USING(`roomofferoid`) 
                INNER JOIN `room` r USING(`roomoid`)
            WHERE r.`hoteloid`=:a AND pmsstatus='0' AND r.publishedoid NOT IN (3) AND ro.publishedoid NOT IN (3) ".$query1." GROUP BY r.`roomoid`, r.`name`");
        $stmt->execute(array(":a"=>$hoteloid));
        $r_room1 = $stmt->fetchAll(PDO::FETCH_ASSOC);
        
        $data_room1 = array();
        $data_revenue1 = array();
        $data_rnight1 = array();
        $data_avgstay1 = array();
        $data_avglead1 = array();
        $data_avgrate1 = array();
        foreach($r_room1 as $room1){
            $data_room1[$room1['roomoid']] = $room1['bid'];
            $data_revenue1[$room1['roomoid']] = $room1['rev'];
            $data_rnight1[$room1['roomoid']] = $room1['rnight'];
            $data_avgstay1[$room1['roomoid']] = $room1['avgstay'];
            $data_avglead1[$room1['roomoid']] = $room['avglead'];
            $data_avgrate1[$room1['roomoid']] = $room['avgrate'];
        }
    }

}catch(PDOException $ex){
    die("Error");
}
?>
<ul class="nav nav-tabs">
    <li class="nav-item active"><a class="nav-link" id="tbbooking-tab" data-toggle="tab" href="#tbbooking">Booking</a></li>
    <li class="nav-item"><a class="nav-link" id="tbrevenue-tab" data-toggle="tab" href="#tbrevenue">Revenue</a></li>
    <li class="nav-item"><a class="nav-link" id="tbrnight-tab" data-toggle="tab" href="#tbrnight">Room Nights</a></li>
    <li class="nav-item"><a class="nav-link" id="tbavgstay-tab" data-toggle="tab" href="#tbavgstay">Average Stay</a></li>
    <li class="nav-item"><a class="nav-link" id="tbavglead-tab" data-toggle="tab" href="#tbavglead">Average Lead</a></li>
    <li class="nav-item"><a class="nav-link" id="tbavgrate-tab" data-toggle="tab" href="#tbavgrate">Average Rate</a></li>
</ul>
<div class="tab-content">
    <div class="tab-pane fade in active" id="tbbooking">
        <div class="tab-container">
            <div class="row">
                <div class="col-md-5">
                    <div><?=$msg?><?=$msg1?></div>
                    <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>-</th>
                        <th>Room Type</th>
                        <th><?=$theader?></th>
                        <?php if($msg1!=""){ echo '<th>'.$theader1.'</th><th>% Variance</th>'; }?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                        $sv = array();
                        $sw = array();
                        $sx = array();
                        $x = 1;
                        foreach($r_xroom as $room){
                            if(empty($data_room[$room['roomoid']])){ $data_room[$room['roomoid']]="0"; }
                            if($msg1!=""){ 
                                if(empty($data_room1[$room['roomoid']])){ $data_room1[$room['roomoid']]="0"; } 
                                $n1 = floatval($data_room1[$room['roomoid']]) - floatval($data_room[$room['roomoid']]);
                                $n2 = (floatval($data_room[$room['roomoid']]) != 0) ? floatval($data_room[$room['roomoid']]) : 1;
                                $compare = round(($n1/$n2) * 100, 2);
                                $q='<td align="right">'.number_format($data_room1[$room['roomoid']],0,',','.').'</td><td align="right">'.$compare.'%</td>'; 
                            }else{ 
                                $q=''; 
                            }
                            echo '
                            <tr>
                                <td>RT'.$x.'</td>
                                <td>'.$room['name'].'</td>
                                <td align="right">'.number_format($data_room[$room['roomoid']],0,',','.').'</td>
                                '.$q.'
                            </tr>
                            ';
                            $sv[] = "RT".$x;
                            $sw[] = $data_room[$room['roomoid']];
                            $sx[] = $data_room1[$room['roomoid']];
                            $x++;
                        }
                    ?>
                    </tbody>
                    </table>
                </div>
                <div class="col-md-7">
                    <canvas id="myChart"></canvas>
                    <div id="data-name" style="display:none"><?=implode(";", $sv)?></div>
                    <div id="data-bid" style="display:none"><?=implode(";", $sw)?></div>
                    <div id="data-com" style="display:none"><?=implode(";", $sx)?></div>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="tbrevenue">
        <div class="tab-container">
            <div class="row">
            <div class="col-md-5">
                    <div><?=$msg?><?=$msg1?></div>
                    <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>-</th>
                        <th>Room Type</th>
                        <th><?=$theader?></th>
                        <?php if($msg1!=""){ echo '<th>'.$theader1.'</th><th>% Variance</th>'; }?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                        $sv = array();
                        $sw = array();
                        $sx = array();
                        $x = 1;
                        foreach($r_xroom as $room){
                            if(empty($data_revenue[$room['roomoid']])){ $data_revenue[$room['roomoid']]="0"; }
                            if($msg1!=""){ 
                                if(empty($data_revenue1[$room['roomoid']])){ $data_revenue1[$room['roomoid']]="0"; } 
                                $n1 = floatval($data_revenue1[$room['roomoid']]) - floatval($data_revenue[$room['roomoid']]);
                                $n2 = (floatval($data_revenue[$room['roomoid']]) != 0) ? floatval($data_revenue[$room['roomoid']]) : 1;
                                $compare = round(($n1/$n2) * 100, 2) . "%";
                                if(empty($data_revenue[$room['roomoid']]) || empty($data_revenue1[$room['roomoid']])){ $compare = ""; }
                                $q='<td align="right">'.number_format($data_revenue1[$room['roomoid']],0,',','.').'</td><td align="right">'.$compare.'</td>'; 
                            }else{ 
                                $q=''; 
                            }
                            echo '
                            <tr>
                                <td>RT'.$x.'</td>
                                <td>'.$room['name'].'</td>
                                <td align="right">'.number_format($data_revenue[$room['roomoid']],0,',','.').'</td>
                                '.$q.'
                            </tr>
                            ';
                            $sv[] = "RT".$x;
                            $sw[] = $data_revenue[$room['roomoid']];
                            $sx[] = $data_revenue1[$room['roomoid']];
                            $x++;
                        }
                    ?>
                    </tbody>
                    </table>
                </div>
                <div class="col-md-7">
                    <canvas id="myChart1"></canvas>
                    <div id="data-name1" style="display:none"><?=implode(";", $sv)?></div>
                    <div id="data-bid1" style="display:none"><?=implode(";", $sw)?></div>
                    <div id="data-com1" style="display:none"><?=implode(";", $sx)?></div>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="tbrnight">
        <div class="tab-container">
            <div class="row">
            <div class="col-md-5">
                    <div><?=$msg?><?=$msg1?></div>
                    <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>-</th>
                        <th>Room Type</th>
                        <th><?=$theader?></th>
                        <?php if($msg1!=""){ echo '<th>'.$theader1.'</th><th>% Variance</th>'; }?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                        $sv = array();
                        $sw = array();
                        $sx = array();
                        $x = 1;
                        foreach($r_xroom as $room){
                            if(empty($data_rnight[$room['roomoid']])){ $data_rnight[$room['roomoid']]="0"; }
                            if($msg1!=""){ 
                                if(empty($data_rnight1[$room['roomoid']])){ $data_rnight1[$room['roomoid']]="0"; } 
                                $n1 = floatval($data_rnight1[$room['roomoid']]) - floatval($data_rnight[$room['roomoid']]);
                                $n2 = (floatval($data_rnight[$room['roomoid']]) != 0) ? floatval($data_rnight[$room['roomoid']]) : 1;
                                $compare = round(($n1/$n2) * 100, 2);
                                $q='<td align="right">'.number_format($data_rnight1[$room['roomoid']],0,',','.').'</td><td align="right">'.$compare.'%</td>'; 
                            }else{ 
                                $q=''; 
                            }
                            echo '
                            <tr>
                                <td>RT'.$x.'</td>
                                <td>'.$room['name'].'</td>
                                <td align="right">'.number_format($data_rnight[$room['roomoid']],0,',','.').'</td>
                                '.$q.'
                            </tr>
                            ';
                            $sv[] = "RT".$x;
                            $sw[] = $data_rnight[$room['roomoid']];
                            $sx[] = $data_rnight1[$room['roomoid']];
                            $x++;
                        }
                    ?>
                    </tbody>
                    </table>
                </div>
                <div class="col-md-7">
                    <canvas id="myChart2"></canvas>
                    <div id="data-name2" style="display:none"><?=implode(";", $sv)?></div>
                    <div id="data-bid2" style="display:none"><?=implode(";", $sw)?></div>
                    <div id="data-com2" style="display:none"><?=implode(";", $sx)?></div>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="tbavgstay">
        <div class="tab-container">
            <div class="row">
            <div class="col-md-5">
                    <div><?=$msg?><?=$msg1?></div>
                    <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>-</th>
                        <th>Room Type</th>
                        <th><?=$theader?></th>
                        <?php if($msg1!=""){ echo '<th>'.$theader1.'</th><th>% Variance</th>'; }?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                        $sv = array();
                        $sw = array();
                        $sx = array();
                        $x = 1;
                        foreach($r_xroom as $room){
                            if(empty($data_avgstay[$room['roomoid']])){ $data_avgstay[$room['roomoid']]="0"; }
                            if($msg1!=""){ 
                                if(empty($data_avgstay1[$room['roomoid']])){ $data_avgstay1[$room['roomoid']]="0"; } 
                                $n1 = floatval($data_avgstay1[$room['roomoid']]) - floatval($data_avgstay[$room['roomoid']]);
                                $n2 = (floatval($data_avgstay[$room['roomoid']]) != 0) ? floatval($data_avgstay[$room['roomoid']]) : 1;
                                $compare = round(($n1/$n2) * 100, 2);
                                $q='<td align="right">'.number_format($data_avgstay1[$room['roomoid']],1,',','.').'</td><td align="right">'.$compare.'%</td>'; 
                            }else{ 
                                $q=''; 
                            }
                            echo '
                            <tr>
                                <td>RT'.$x.'</td>
                                <td>'.$room['name'].'</td>
                                <td align="right">'.number_format($data_avgstay[$room['roomoid']],1,',','.').'</td>
                                '.$q.'
                            </tr>
                            ';
                            $sv[] = "RT".$x;
                            $sw[] = $data_avgstay[$room['roomoid']];
                            $sx[] = $data_avgstay1[$room['roomoid']];
                            $x++;
                        }
                    ?>
                    </tbody>
                    </table>
                </div>
                <div class="col-md-7">
                    <canvas id="myChart3"></canvas>
                    <div id="data-name3" style="display:none"><?=implode(";", $sv)?></div>
                    <div id="data-bid3" style="display:none"><?=implode(";", $sw)?></div>
                    <div id="data-com3" style="display:none"><?=implode(";", $sx)?></div>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="tbavglead">
        <div class="tab-container">
            <div class="row">
            <div class="col-md-5">
                    <div><?=$msg?><?=$msg1?></div>
                    <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>-</th>
                        <th>Room Type</th>
                        <th><?=$theader?></th>
                        <?php if($msg1!=""){ echo '<th>'.$theader1.'</th><th>% Variance</th>'; }?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                        $sv = array();
                        $sw = array();
                        $sx = array();
                        $x = 1;
                        foreach($r_xroom as $room){
                            if(empty($data_avglead[$room['roomoid']])){ $data_avglead[$room['roomoid']]="0"; }
                            if($msg1!=""){ 
                                if(empty($data_avglead1[$room['roomoid']])){ $data_avglead1[$room['roomoid']]="0"; } 
                                $n1 = floatval($data_avglead1[$room['roomoid']]) - floatval($data_avglead[$room['roomoid']]);
                                $n2 = (floatval($data_avglead[$room['roomoid']]) != 0) ? floatval($data_avglead[$room['roomoid']]) : 1;
                                $compare = round(($n1/$n2) * 100, 2);
                                $q='<td align="right">'.number_format($data_avglead1[$room['roomoid']],0,',','.').'</td><td align="right">'.$compare.'%</td>'; 
                            }else{ 
                                $q=''; 
                            }
                            echo '
                            <tr>
                                <td>RT'.$x.'</td>
                                <td>'.$room['name'].'</td>
                                <td align="right">'.number_format($data_avglead[$room['roomoid']],0,',','.').'</td>
                                '.$q.'
                            </tr>
                            ';
                            $sv[] = "RT".$x;
                            $sw[] = $data_avglead[$room['roomoid']];
                            $sx[] = $data_avglead1[$room['roomoid']];
                            $x++;
                        }
                    ?>
                    </tbody>
                    </table>
                </div>
                <div class="col-md-7">
                    <canvas id="myChart4"></canvas>
                    <div id="data-name4" style="display:none"><?=implode(";", $sv)?></div>
                    <div id="data-bid4" style="display:none"><?=implode(";", $sw)?></div>
                    <div id="data-com4" style="display:none"><?=implode(";", $sx)?></div>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="tbavgrate">
        <div class="tab-container">
            <div class="row">
            <div class="col-md-5">
                    <div><?=$msg?><?=$msg1?></div>
                    <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>-</th>
                        <th>Room Type</th>
                        <th><?=$theader?></th>
                        <?php if($msg1!=""){ echo '<th>'.$theader1.'</th><th>% Variance</th>'; }?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                        $sv = array();
                        $sw = array();
                        $sx = array();
                        $x = 1;
                        foreach($r_xroom as $room){
                            if(empty($data_avgrate[$room['roomoid']])){ $data_avgrate[$room['roomoid']]="0"; }
                            if($msg1!=""){ 
                                if(empty($data_avgrate1[$room['roomoid']])){ $data_avgrate1[$room['roomoid']]="0"; } 
                                $n1 = floatval($data_avgrate1[$room['roomoid']]) - floatval($data_avgrate[$room['roomoid']]);
                                $n2 = (floatval($data_avgrate[$room['roomoid']]) != 0) ? floatval($data_avgrate[$room['roomoid']]) : 1;
                                $compare = round(($n1/$n2) * 100, 2);
                                $q='<td align="right">'.number_format($data_avgrate1[$room['roomoid']],0,',','.').'</td><td align="right">'.$compare.'%</td>'; 
                            }else{ 
                                $q=''; 
                            }
                            echo '
                            <tr>
                                <td>RT'.$x.'</td>
                                <td>'.$room['name'].'</td>
                                <td align="right">'.number_format($data_avgrate[$room['roomoid']],0,',','.').'</td>
                                '.$q.'
                            </tr>
                            ';
                            $sv[] = "RT".$x;
                            $sw[] = $data_avgrate[$room['roomoid']];
                            $sx[] = $data_avgrate1[$room['roomoid']];
                            $x++;
                        }
                    ?>
                    </tbody>
                    </table>
                </div>
                <div class="col-md-7">
                    <canvas id="myChart5"></canvas>
                    <div id="data-name5" style="display:none"><?=implode(";", $sv)?></div>
                    <div id="data-bid5" style="display:none"><?=implode(";", $sw)?></div>
                    <div id="data-com5" style="display:none"><?=implode(";", $sx)?></div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>