<section class="content-header">
    <h1>
        Performance Reports
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="#"><i class="fa fa-bar-chart-o"></i> Reports</a></li>
        <li class="active">Performance Reports</li>
    </ol>
</section>
<section class="content">

<div class="box box-form">
    <h2>Hotel : <?php echo $_SESSION['_hotelname']; ?></h2>
</div>
<div id="dashboard">

	<div id="newdashboard">
		<!-- Bootstrap -->
		<link href="<?php echo $base_url?>/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
		<!-- Theme style -->
		<link href="<?php echo $base_url?>/lib/admin-lte/css/AdminLTE.min.css" rel="stylesheet" type="text/css">
		<!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
		<link href="<?php echo $base_url?>/lib/admin-lte/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css">
		<!-- Fixing -->
		<link href="<?php echo $base_url?>/css/style.css" rel="stylesheet" type="text/css">
		<!-- JQUERY UI -->
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

		<style type="text/css">
			h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 { 
				font-family: inherit; 
				font-weight: 600;
				line-height: inherit;
				color: inherit;
			}
			.wrapper {
				position: inherit;
				overflow: hidden!important;
			}
			.left-side {
				padding-top: inherit;
			}
			.sidebar > .sidebar-menu li > a:hover {
				background-color: rgba(72, 115, 175, 0.26);
			}
			.sidebar .sidebar-menu .treeview-menu {
				background-color: rgb(14, 26, 43);
			}
			.sidebar .sidebar-menu .treeview-menu .treeview-menu-child {
				background-color: rgba(0, 0, 0, 0.5);
			}
			.sidebar > .sidebar-menu li.active > a {
				background-color: rgba(197, 45, 47, 0.55);
			}
			.sidebar > .sidebar-menu > li.treeview.active > a {
				background-color: inherit;
			}
			.sidebar > .sidebar-menu > li.treeview.active li.treeview-child.active > a {
				background-color: inherit;
			}
			.sidebar .sidebar-menu > li > a > .fa {
				width: 28px;
				font-size: 16px;
			}
			.sidebar-menu li>a>.fa-angle-left {
				position: relative;
				margin-right: inherit;
			}
			.sidebar .sidebar-menu > li > a > .fa.pull-right {
				right: 0;
				text-align: center;
			}
			.box {
				border-radius: inherit;
				border-top: inherit;
			}
			.ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default {
				background-color: #ededed !important;
			}
		</style>

		<div class="row">
		<div class="col-md-12">
			<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Reservation Report</h3> &nbsp; 
				<select class="form-control" style="display:inline-block;width:auto;margin-left:20px;">
					<option>All Room Type</option>
					<option>Deluxe Garden View</option>
					<option>Ocean View Room</option>
				</select> &nbsp; 
				Range : <input type="text" id="from" class="calinput" readonly value="<?php echo date('m/d/Y', strtotime('-10 day'));?>" style="width:120px"> &nbsp;- &nbsp;
				<input type="text" id="to" class="calinput" readonly value="<?php echo date('m/d/Y');?>" style="width:120px">
				<div class="box-tools pull-right">
				<a href="<?php echo $base_url?>/booking" class="btn btn-sm bg-blue btn-flat pull-left">View Reservation</a> &nbsp;  
				<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				<?php /*<div class="btn-group">
					<button class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown"><i class="fa fa-wrench"></i></button>
					<ul class="dropdown-menu" role="menu">
					<li><a href="#">Action</a></li>
					<li><a href="#">Another action</a></li>
					<li><a href="#">Something else here</a></li>
					<li class="divider"></li>
					<li><a href="#">Separated link</a></li>
					</ul>
				</div> */ ?>
				<button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
				</div>
				
			</div><!-- /.box-header -->
			<div class="box-body">
				<div class="row">
				<div class="col-md-8">
					<p class="text-center">
					<strong>Sales: <?php echo date('d M, Y', strtotime('-10 day'));?> - <?php echo date('d M, Y');?></strong>
					</p>
					<div class="chart">
					<!-- Sales Chart Canvas -->
					<canvas id="salesChart" style="height: 180px;"></canvas>
					</div><!-- /.chart-responsive -->
				</div><!-- /.col -->
				<div class="col-md-4">
					<p class="text-center">
					<strong>Summary</strong>
					</p>
					<div class="progress-group">
					<span class="progress-text">Total Reservation</span>
					<span class="progress-number"><b>200</b></span>
					<div class="progress sm">
						<div class="progress-bar progress-bar-aqua" style="width: 100%"></div>
					</div>
					</div><!-- /.progress-group -->
					<div class="progress-group">
					<span class="progress-text">Total Confirmed Booking</span>
					<span class="progress-number"><b>180</b>/200</span>
					<div class="progress sm">
						<div class="progress-bar progress-bar-green" style="width: 90%"></div>
					</div>
					</div><!-- /.progress-group -->
					<div class="progress-group">
					<span class="progress-text">Total Cancelled Booking</span>
					<span class="progress-number"><b>20</b>/200</span>
					<div class="progress sm">
						<div class="progress-bar progress-bar-red" style="width: 10%"></div>
					</div>
					</div><!-- /.progress-group -->
					<div class="progress-group">
					<span class="progress-text">Total No Show</span>
					<span class="progress-number"><b>30</b>/200</span>
					<div class="progress sm">
						<div class="progress-bar progress-bar-yellow" style="width: 15%"></div>
					</div>
					</div><!-- /.progress-group -->
				</div><!-- /.col -->
				</div><!-- /.row -->
			</div><!-- ./box-body -->
			<div class="box-footer">
				<div class="row">
				<div class="col-sm-3 col-xs-6">
					<div class="description-block border-right">
					<span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 100%</span>
					<h5 class="description-header">IDR 420,000,000</h5>
					<span class="description-text">TOTAL REVENUE</span>
					</div><!-- /.description-block -->
				</div><!-- /.col -->
				<div class="col-sm-3 col-xs-6">
					<div class="description-block border-right">
					<span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 5%</span>
					<h5 class="description-header">IDR 21,000,000</h5>
					<span class="description-text">TOTAL COMMISSION</span>
					</div><!-- /.description-block -->
				</div><!-- /.col -->
				<div class="col-sm-3 col-xs-6">
					<div class="description-block border-right">
					<span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 95%</span>
					<h5 class="description-header">IDR 399,000,000</h5>
					<span class="description-text">NET TO HOTEL</span>
					</div><!-- /.description-block -->
				</div><!-- /.col -->
				<div class="col-sm-3 col-xs-6">
					<div class="description-block">
					<span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 20%</span>
					<h5 class="description-header">1000</h5>
					<span class="description-text">GOAL COMPLETIONS</span>
					</div><!-- /.description-block -->
				</div>
				</div><!-- /.row -->
			</div><!-- /.box-footer -->
			</div><!-- /.box -->
		</div><!-- /.col -->
		</div><!-- /.row -->

		<!-- Bootstrap 3.3.6 -->
		<script src="<?php echo $base_url?>/lib/bootstrap/js/bootstrap.min.js"></script>
		<!-- chartjs -->
		<script src="<?php echo $base_url?>/lib/plugins/chartjs/Chart.min.js"></script>
		<script>
			$(function () {
				'use strict';

				/* ChartJS
				* -------
				* Here we will create a few charts using ChartJS
				*/

				//-----------------------
				//- MONTHLY SALES CHART -
				//-----------------------

				// Get context with jQuery - using jQuery's .get() method.
				var salesChartCanvas = $("#salesChart").get(0).getContext("2d");
				// This will get the first returned node in the jQuery collection.
				var salesChart = new Chart(salesChartCanvas);

				var salesChartData = {
					labels: ["11-Jan", "12-Jan", "13-Jan", "14-Jan", "15-Jan", "16-Jan", "17-Jan", "18-Jan", "19-Jan", "20-Jan"],
					datasets: [
					{
						label: "Reservation",
						fillColor: "rgba(0, 192, 239, 0.5)",
						strokeColor: "rgba(0, 192, 239, 0.5)",
						pointColor: "rgba(0, 192, 239, 0.5)",
						pointStrokeColor: "#c1c7d1",
						pointHighlightFill: "#fff",
						pointHighlightStroke: "rgb(220,220,220)",
						data: [18, 25, 14, 30, 28, 12, 27, 13, 12, 21]
					},
					{
						label: "Confirmed Booking",
						fillColor: "rgba(0, 166, 90, 0.5)",
						strokeColor: "rgba(0, 166, 90, 0.5)",
						pointColor: "rgba(0, 166, 90, 0.5)",
						pointStrokeColor: "rgba(60,141,188,1)",
						pointHighlightFill: "#fff",
						pointHighlightStroke: "rgba(60,141,188,1)",
						data: [16, 24, 9, 27, 28, 12, 23, 11, 10, 20]
					},
					{
						label: "Cancelled Booking",
						fillColor: "rgba(221, 75, 57, 0.5)",
						strokeColor: "rgba(221, 75, 57, 0.5)",
						pointColor: "rgba(221, 75, 57, 0.5)",
						pointStrokeColor: "rgba(60,141,188,1)",
						pointHighlightFill: "#fff",
						pointHighlightStroke: "rgba(60,141,188,1)",
						data: [2, 1, 5, 3, 0, 0, 4, 2, 2, 1]
					},
					{
						label: "No Show",
						fillColor: "rgba(243, 156, 18, 0.5)",
						strokeColor: "rgba(243, 156, 18, 0.5)",
						pointColor: "rgba(243, 156, 18, 0.5)",
						pointStrokeColor: "rgba(60,141,188,1)",
						pointHighlightFill: "#fff",
						pointHighlightStroke: "rgba(60,141,188,1)",
						data: [0, 0, 3, 5, 8, 5, 2, 1, 4, 2]
					}
					]
				};

				var salesChartOptions = {
					//Boolean - If we should show the scale at all
					showScale: true,
					//Boolean - Whether grid lines are shown across the chart
					scaleShowGridLines: false,
					//String - Colour of the grid lines
					scaleGridLineColor: "rgba(0,0,0,.05)",
					//Number - Width of the grid lines
					scaleGridLineWidth: 1,
					//Boolean - Whether to show horizontal lines (except X axis)
					scaleShowHorizontalLines: true,
					//Boolean - Whether to show vertical lines (except Y axis)
					scaleShowVerticalLines: true,
					//Boolean - Whether the line is curved between points
					bezierCurve: true,
					//Number - Tension of the bezier curve between points
					bezierCurveTension: 0.3,
					//Boolean - Whether to show a dot for each point
					pointDot: false,
					//Number - Radius of each point dot in pixels
					pointDotRadius: 4,
					//Number - Pixel width of point dot stroke
					pointDotStrokeWidth: 1,
					//Number - amount extra to add to the radius to cater for hit detection outside the drawn point
					pointHitDetectionRadius: 20,
					//Boolean - Whether to show a stroke for datasets
					datasetStroke: true,
					//Number - Pixel width of dataset stroke
					datasetStrokeWidth: 2,
					//Boolean - Whether to fill the dataset with a color
					datasetFill: true,
					//String - A legend template
					legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%=datasets[i].label%></li><%}%></ul>",
					//Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
					maintainAspectRatio: true,
					//Boolean - whether to make the chart responsive to window resizing
					responsive: true
				};

				//Create the line chart
				salesChart.Line(salesChartData, salesChartOptions);

				//---------------------------
				//- END MONTHLY SALES CHART -
				//---------------------------

				// Get context with jQuery - using jQuery's .get() method.
				var salesChartCanvas1 = $("#salesChart1").get(0).getContext("2d");
				// This will get the first returned node in the jQuery collection.
				var salesChart1 = new Chart(salesChartCanvas1);
				//Create the line chart
				salesChart1.Line(salesChartData, salesChartOptions);
			});
		</script>
		<script>
		$( function() {
			var dateFormat = "mm/dd/yy",
			from = $( "#from" )
				.datepicker({
				defaultDate: "+1d",
				changeMonth: true,
				numberOfMonths: 1
				})
				.on( "change", function() {
				to.datepicker( "option", "minDate", getDate( this ) );
				}),
			to = $( "#to" ).datepicker({
				defaultDate: "+1d",
				changeMonth: true,
				numberOfMonths: 1
			})
			.on( "change", function() {
				from.datepicker( "option", "maxDate", getDate( this ) );
			});
		
			function getDate( element ) {
			var date;
			try {
				date = $.datepicker.parseDate( dateFormat, element.value );
			} catch( error ) {
				date = null;
			}
		
			return date;
			}
		} );
		</script>

		<div class="row">
		<div class="col-md-12">
			<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Reservation Report</h3> &nbsp; 
				<select class="form-control" style="display:inline-block;width:auto;margin-left:20px;">
					<option>All Room Type</option>
					<option>Deluxe Garden View</option>
					<option>Ocean View Room</option>
				</select> &nbsp; 
				Range : <input type="text" id="from" class="calinput" readonly value="<?php echo date('m/d/Y', strtotime('-10 day'));?>" style="width:120px"> &nbsp;- &nbsp;
				<input type="text" id="to" class="calinput" readonly value="<?php echo date('m/d/Y');?>" style="width:120px">
				<div class="box-tools pull-right">
				<a href="<?php echo $base_url?>/booking" class="btn btn-sm bg-blue btn-flat pull-left">View Reservation</a> &nbsp;  
				<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				<?php /*<div class="btn-group">
					<button class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown"><i class="fa fa-wrench"></i></button>
					<ul class="dropdown-menu" role="menu">
					<li><a href="#">Action</a></li>
					<li><a href="#">Another action</a></li>
					<li><a href="#">Something else here</a></li>
					<li class="divider"></li>
					<li><a href="#">Separated link</a></li>
					</ul>
				</div> */ ?>
				<button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
				</div>
				
			</div><!-- /.box-header -->
			<div class="box-body">
				<div class="row">
				<div class="col-md-8">
					<p class="text-center">
					<strong>Sales: <?php echo date('d M, Y', strtotime('-10 day'));?> - <?php echo date('d M, Y');?></strong>
					</p>
					<div class="chart">
					<!-- Sales Chart Canvas -->
					<canvas id="salesChart1" style="height: 180px;"></canvas>
					</div><!-- /.chart-responsive -->
				</div><!-- /.col -->
				<div class="col-md-4">
					<p class="text-center">
					<strong>Summary</strong>
					</p>
					<div class="progress-group">
					<span class="progress-text">Total Reservation</span>
					<span class="progress-number"><b>200</b></span>
					<div class="progress sm">
						<div class="progress-bar progress-bar-aqua" style="width: 100%"></div>
					</div>
					</div><!-- /.progress-group -->
					<div class="progress-group">
					<span class="progress-text">Total Confirmed Booking</span>
					<span class="progress-number"><b>180</b>/200</span>
					<div class="progress sm">
						<div class="progress-bar progress-bar-green" style="width: 90%"></div>
					</div>
					</div><!-- /.progress-group -->
					<div class="progress-group">
					<span class="progress-text">Total Cancelled Booking</span>
					<span class="progress-number"><b>20</b>/200</span>
					<div class="progress sm">
						<div class="progress-bar progress-bar-red" style="width: 10%"></div>
					</div>
					</div><!-- /.progress-group -->
					<div class="progress-group">
					<span class="progress-text">Total No Show</span>
					<span class="progress-number"><b>30</b>/200</span>
					<div class="progress sm">
						<div class="progress-bar progress-bar-yellow" style="width: 15%"></div>
					</div>
					</div><!-- /.progress-group -->
				</div><!-- /.col -->
				</div><!-- /.row -->
			</div><!-- ./box-body -->
			</div><!-- /.box -->
		</div><!-- /.col -->
		</div><!-- /.row -->
	</div>
</div>

</section>