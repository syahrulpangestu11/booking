<script type="text/javascript">
$(function(){

function showInventory(date, request){
	box = $('div#show-inventory')
	$.ajax({
		url: "<?php echo"$base_url"; ?>/includes/reports/inhouse-report/data.php",
		type: 'post',
		data: { lastdate: date, request: request, hcode : <?=$hcode?>, base : '<?=$base_url?>' },
		success: function(data) {
			    box.html(data);
		}
	});
}

$(document).on('change','input[name=date]', function(e){
	showInventory($(this).val(), 'today');
});


$(document).ready(function(){
	showInventory('<?=date('Y-m-d');?>', 'today');
});

$(document).on('click','button.inv-nav', function(e){
	date = $('input[name=spotdate]').val();
	request = $(this).val();
	showInventory(date, request);
});

$(document).ready(function(){
	showInventory('<?=date('Y-m-d');?>', 'today');
});

$( "#datepicker" ).datepicker({
	changeMonth: true,
	changeYear: true,
});

});
</script>
