 <?php
	if(isset($_REQUEST['rt'])){
		$roomoffer = $_REQUEST['rt'];
		$channeloid = $_REQUEST['ch'];
	}
	
	if(isset($_REQUEST['sdate']) and !empty($_REQUEST['sdate'])){
		$start = date("d F Y", strtotime($_REQUEST['sdate']));
	}else{
        // $start = date("d F Y");
        $start = "";
	}

	if(isset($_REQUEST['edate']) and !empty($_REQUEST['edate'])){
		$end = date("d F Y", strtotime($_REQUEST['edate']));
	}else{
        // $end = date("d F Y",  strtotime(date("Y-m-d")." +1 month" ));
        $end = "";
	}
?>
<section class="content-header">
    <h1>
       	Promotions
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Allotments &amp; Rates</a></li>
        <li class="active">Promotions</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="box">
            <div class="box-body">
                <div class="form-group">
            <form method="get" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/promotions/">
                    <input type="hidden" name="hoteloid" value="<?php echo $hoteloid; ?>" />
                    <input type="hidden" name="hcode" value="<?php echo $hcode; ?>" />
                    <label>Room Type  :</label> &nbsp;&nbsp;
					<select name="rt" class="input-select">
                    <option value="">All Room Type</option>
					<?php
                        try {
                            $stmt = $db->query("select r.roomoid, r.name, x.offernum from room r inner join hotel h using (hoteloid) left join (select count(roomofferoid) as offernum, ro.roomoid from roomoffer ro group by ro.roomoid) as x on x.roomoid = r.roomoid where h.hoteloid = '".$hoteloid."' and x.offernum > 0 and r.publishedoid = '1'");
                            $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                            foreach($r_room as $row){
                                echo"<optgroup label = '".$row['name']."'>";
                                try {
                                    $stmt = $db->query("select ro.* from roomoffer ro inner join published p using (publishedoid) inner join room r using (roomoid) inner join offertype using (offertypeoid) where r.roomoid = '".$row['roomoid']."' and p.publishedoid = '1'");
                                    $r_offer = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                    foreach($r_offer as $row1){
                                        if($row1['roomofferoid'] == $roomoffer){ $selected = 'selected = "selected"';  }else{ $selected = ''; }
                                       echo"<option value='".$row1['roomofferoid']."' ".$selected.">  ".$row1['name']."</option>";
                                    }
                                }catch(PDOException $ex) {
                                    echo "Invalid Query";
                                    die();
                                }     
                                echo"</optgroup>";                       
                            }
                        }catch(PDOException $ex) {
                            echo "Invalid Query";
                            die();
                        }
                    
                    ?>   
                    </select><input type="hidden" name="ch" value="1" />
                    &nbsp;&nbsp;
                    <?php /*
					<label>Channel :</label> &nbsp;&nbsp;
                    <select name="ch">
                    <option value="">All Channel</option>
                    <?php
                        try {
                            $stmt = $db->query("select * from channel");
                            $r_plan = $stmt->fetchAll(PDO::FETCH_ASSOC);
                            foreach($r_plan as $row){
								if($row['channeloid'] == $channeloid){ $selected = 'selected = "selected"';  }else{ $selected = ''; }
                                echo"<option value='".$row['channeloid']."' ".$selected.">".$row['name']."</option>";
                            }
                        }catch(PDOException $ex) {
                            echo "Invalid Query";
                            die();
                        }
                    
                    ?>        	
                    </select> &nbsp;
					*/ ?>
                    <br><br>
                    <input type="radio" name="range" value="sale" checked="checked" />  <label>Sale Date </label>
                    &nbsp;&nbsp;
                    <input type="radio" name="range" value="stay" />  <label>Stay Date </label>
                    <br><br>
                    <label>From </label> &nbsp;
                    <input type="text" name="sdate" id="startdate-today" placeholder="" value="<?php echo $start; ?>"> &nbsp;&nbsp;
                    <label>To </label> &nbsp;
                    <input type="text" name="edate" id="enddate-today" placeholder="" value="<?php echo $end; ?>"> &nbsp;&nbsp;                    
                    <button type="submit" class="small-button blue"><i class="fa fa-search"></i>Find</button>
                    
                    <?php if(isset($_REQUEST['rt'])){ ?>
                    <a href="<?=$base_url;?>/promotions" class="small-button soft-red"><i class="fa fa-times"></i>Clear</a>
                    <?php } ?>
                </form>
                </div>
            </div><!-- /.box-body -->
       </div>
    </div>
    
    <div class="row">
        <div class="box">
        	<form method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/room-control/process">
                <div id="data-box" class="box-body">
                    <div class="loader">Loading...</div>
                </div><!-- /.box-body -->
            </form>
       </div>
    </div>

</section>
