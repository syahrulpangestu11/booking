<?php
	$extraoid = $_POST['extraoid'];
	$name = (isset($_POST['name'])) ? $_POST['name'] : "";
	$published = $_POST['published'];

	$salefrom = date("Y-m-d", strtotime($_POST['salefrom']));
	$saleto = date("Y-m-d", strtotime($_POST['saleto']));

	$headline = $_POST['headline'];
	$description = $_POST['description'];

	$allowedExtension = array('jpg', 'jpeg', 'png');
	$tmp = explode('.', $_FILES['image']['name']);
	$extension = end($tmp);
	$image = '';
	if(!empty($_FILES['image']['name']) and isset($_FILES['image']['name'])){
		if(in_array($extension, $allowedExtension)){
			$imagename = str_replace(' ', '_', $_FILES['image']['name']);
			$imagename = str_replace('.'.$extension, '', $imagename);
			$ext = '.'.strtolower($extension);
			$image_new_name = $hcode.substr($imagename, 0, 10).date('Ymd_His').$ext;
			$folder_destination = 'pict/extra/';

			move_uploaded_file($_FILES['image']['tmp_name'], $upload_base.$folder_destination.$image_new_name);
			$image = $web_url.'/'.$folder_destination.$image_new_name;

			try {
				$stmt = $db->prepare("update extra_template set picture = :a  where extra_templateoid = :eoid");
				$stmt->execute(array(':a' => $image, ':eoid' => $extraoid));
			}catch(PDOException $ex) {
				echo "Invalid Query"; print($ex);
				die();
			}

		}else{
			echo'<script>alert("Data not saved.\\nInvalid file.")</script>';
		}
	}

	try {
		$stmt = $db->prepare("update extra_template set name = :a, headline = :b, description= :c, startbook = :d, endbook =:e, publishedoid = :f where extra_templateoid = :eoid");
		$stmt->execute(array(':a' => $name, ':b' => $headline, ':c' => $description, ':d' => $salefrom, ':e' => $saleto, ':f' => $published, ':eoid' => $extraoid));
	?>
		<script type="text/javascript">
            $(function(){ $( document ).ready(function(){ $("#dialog-success").dialog("open"); }); });
        </script>
	<?php
	}catch(PDOException $ex) {
	?>
		<script type="text/javascript">
            $(function(){ $( document ).ready(function(){ $("#dialog-error").dialog("open"); }); });
        </script>
	<?php
	}
?>
