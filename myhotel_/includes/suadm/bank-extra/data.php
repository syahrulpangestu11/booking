<button type="button" class="pure-button blue add"><i class="fa fa-cube"></i>Create New Extra Template</button>

<?php
	include("../../../conf/connection.php");
	$datenow = date("Y-m-d");

	try {

		$main_query = "select e.* from extra_template e";
		$filter = array();
		if(isset($_REQUEST['name']) and $_REQUEST['name']!=''){
			array_push($filter, 'e.name like "%'.$_REQUEST['name'].'%"');
		}
		array_push($filter, 'publishedoid not in (3)');
		if(count($filter) > 0){
			$combine_filter = implode(' and ',$filter);
			$query = $main_query.' where '.$combine_filter;
		}else{
			$query = $main_query;
		}

		$stmt = $db->query($query);
		$row_count = $stmt->rowCount();
?>
<table class="table promo-table">
	<tr>
		<td>Extra Name</td>
		<td>Sell Until</td>
		<td>Status</td>
		<td class="algn-right">&nbsp;</td>
	</tr>
<?php
		if($row_count > 0) {
			$r_promo = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_promo as $row){
				$published = ($row['publishedoid']==1) ? "Active" : "Inactive";
				$expirydate = date("d/M/Y", strtotime($row['endbook']));
?>
    <tr>
        <td><?php echo $row['name']; ?></td>
        <td><?php echo $expirydate; ?></td>
				<td><?php echo $published; ?></td>
        <td class="algn-right">
					<button type="button" class="pencil edit" pid="<?php echo $row['extra_templateoid']; ?>">Edit</button>
	        <button type="button" class="trash delete" pid="<?php echo $row['extra_templateoid']; ?>">Delete</button>
        </td>
    </tr>
<?php
			}
		}
?>
</table>
<?php
	}catch(PDOException $ex) {
		echo "Invalid Query";
		print($ex);
		die();
	}
?>
