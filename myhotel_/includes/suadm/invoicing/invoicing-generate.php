<?php
$generate_error = true;

$sHotel = "SELECT h.hoteloid, h.hotelname, h.pthotel, h.npwp, h.contractnumber, h.hotelsubscribeoid from hotel h where h.publishedoid not in ('3') and h.hotelstatusoid IN ('1') ORDER BY h.hotelname";
$stmt = $db->prepare($sHotel);
$stmt->execute();
$hotel = $stmt->fetchAll(PDO::FETCH_ASSOC);

include('includes/class/reporting1.php');

$report = new Reporting1($db);
$invoice = array();
echo "<pre>";
$sqlInsert = "INSERT INTO `invoice` (
  `invoiceoid`, `hoteloid`, `hotelname`, `month`, `year`, 
  `totalbooking`, `roomnight`, `confirmedbooking`, `penaltycancelled`, `totalrevenue`, 
  `reconcileamount`, `reconciledamount`, `invoiced`, `ppn`, `paid`, 
  `invoicepajakoid`, `nofakturpajak`, `fakturpajak`, 
  `invoicestatusoid`, `emailstatusoid`, `createdby`, `createdatetime`, 
  `npwp`, `pthotel`, `attentionpic`, `attentionpicemail`, 
  `contractnumber`, `invoicenumber`, `invoicedescription`, 
  `hotelcontactoid`, `fileproformainvoice`, `fileinvoice`, `filecommissionreportdetail`) VALUES ";
$n = 0;

foreach ($hotel as $h) {

  $pthotel = !empty($h['pthotel']) ? $h['pthotel'] : '';
  $npwp = !empty($h['npwp']) ? $h['npwp'] : '';
  $contractnumber = !empty($h['contractnumber']) ? $h['contractnumber'] : '';

  
  
  $n++;
  $m = date("n", strtotime("first day of previous month"));
  $y = date("Y", strtotime("first day of previous month"));
  $now = date('Y-m-d H:i:s');
  
  $periode1 = $y.'-'.$m.'-01';
  $periode2 = date("Y-m-t",strtotime($periode1));
  $report->setHotel($h['hoteloid']);
  $report->setPeriode($periode1, $periode2);
  $total_booking = $report->materializeBookingTotal('grandtotal', array(4,5,7,8));
  $total_rn = $report->materializeRoomNight();
  $total_confirmed = $report->materializeBookingTotal('grandtotal', 4);
  $total_cancelled = $report->materializeBookingTotal('cancellationamount', array(5,7,8));
  $total_revenue = $total_confirmed + $total_cancelled;
  $total_commission = $report->materializeBookingCommission();

  echo $h['hotelsubscribeoid'];
  if($h['hotelsubscribeoid']==2){
    $reconcileamount = $total_commission;
    $reconciledamount = 0;
    $invoicestatusoid = 1;
  }else{
    $sCommission = "SELECT * FROM hotelcommission hc WHERE hoteloid = :a AND typecomm = 2 ORDER BY priority ASC LIMIT 1;";
    $stmt = $db->prepare($sCommission);
    $stmt->execute(array(':a' => $h['hoteloid']));
    $commission = $stmt->fetch(PDO::FETCH_ASSOC);
    $reconcileamount = $commission['value'];
    $reconciledamount = $reconcileamount;
    $invoicestatusoid = 4;
  }
  
  $h['hotelname'] = addslashes($h['hotelname']);

  $sqlInsertValue[$n] = "(
    NULL, ".$h['hoteloid'].", '".$h['hotelname']."', '".$m."', '".$y."', 
    '".$total_booking."', '".$total_rn."', '".$total_confirmed."', '".$total_cancelled."', '".$total_revenue."',
    '".$reconcileamount."', '".$reconciledamount."', 0, 0, 0, 
    0, '', '', 
    '".$invoicestatusoid."', '1', '".$_SESSION['user']."', '".$now."', 
    'npwp', '".$pthotel."', '".$attentionpic."', '".$attentionpicemail."', 
    '', '', '', 
    '', '', '', ''
    )";
      
       
}
$sqlInsertValues = implode(', ',$sqlInsertValue);
$sqlGenerateInvoice = $sqlInsert.$sqlInsertValues;
// echo $sqlGenerateInvoice;
try{
  $stmt = $db->prepare($sqlGenerateInvoice);
  $stmt->execute();
  $generate_error = false;
}catch (Exception $e) {
  echo "Cannot Generate Records.<hr>";
  echo 'Caught exception: '.  $e->getMessage(). "<hr>";
}
echo '</pre>';

if($generate_error == false){
	header('Location: '.$base_url.'/invoicing');
}
?>