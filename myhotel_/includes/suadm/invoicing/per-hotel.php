<div class="row">
  <div class="col-md-12">
    <table id="invoicing-table" cellspacing="0" class="table table-striped">
      <thead>
        <tr>
          <th>No.</th>
          <th>Month</th>
          <th>Total Booking</th>
          <th>Room Night</th>
          <th>Confirmed Booking</th>
          <th>Penalty Cancelled</th>
          <th>Total Revenue</th>
          <th>Reconcile Amount</th>
          <th>Reconciled</th>
          <th>Paid</th>
          <th class="text-center">Action</th>
          <th class="text-center">Status</th>
          <th class="text-center">Files</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $sInv = "SELECT i.*, ist.status FROM invoice i INNER JOIN invoicestatus ist USING (invoicestatusoid) WHERE i.hoteloid = :a AND i.year = :b AND reconcileamount > 0 ORDER BY month";
        $stmt = $db->prepare($sInv);
        $stmt->execute(array(':a' => $_POST['hotel'], ':b' => $year));
        
        $invAll = $stmt->fetchAll(PDO::FETCH_ASSOC);
        // echo "<pre>";
        // print_r($_POST);
        // echo "</pre>";
        $n=0;
        foreach($invAll as $inv){
          $n++;
          $month = date("F", mktime(0, 0, 0, $inv['month'], 10));
          switch ($inv['invoicestatusoid']) {
            case 1: $btnClass = 'btn-warning'; $btnText = "Reconcile"; break;
            // case 2: $btnClass = 'btn-warning'; $btnText = "Reconcile"; break;
            case 3: $btnClass = 'btn-warning'; $btnText = "Reconcile Amount"; break;
            case 4: $btnClass = 'btn-primary'; $btnText = "Send Invoice"; break;
            case 5: $btnClass = 'btn-success'; $btnText = "Update"; break;
            case 6: $btnClass = 'btn-success'; $btnText = "Update Faktur"; break;
            case 7: $btnClass = 'btn-info'; $btnText = "Close"; break;
            default: $btn = false; $btnClass = 'hidden'; break;
          }
          $btnHref = $base_url."/invoicing/action/?io=".$inv['invoiceoid'];
          
          $showFCRD = empty($inv['filecommissionreportdetail']) ? " hidden " : "";
          $showPI = empty($inv['fileproformainvoice']) ? " hidden " : "";
          $showI = empty($inv['fileinvoice']) ? " hidden " : "";
          $showFP = empty($inv['fakturpajak']) ? " hidden " : "";
          ?>
          <tr>
            <td><?=$n;?></td>
            <td><?=$month;?></td>
            <td><?=number_format($inv['totalbooking']);?></td>
            <td><?=$inv['roomnight'];?></td>
            <td><?=number_format($inv['confirmedbooking']);?></td>
            <td><?=number_format($inv['penaltycancelled']);?></td>
            <td><?=number_format($inv['totalrevenue']);?></td>
            <td><?=number_format($inv['reconcileamount']);?></td>
            <td><?=number_format($inv['reconciledamount']);?></td>
            <td><?=number_format($inv['paid']);?></td>       
            <td class="text-center">
              <a class="btn-invoicing-action btn btn-xs <?=$btnClass;?>" href="<?=$btnHref;?>" ><?=$btnText;?></a>
            </td>
            <td class="text-center"><?=$inv['status'];?></td>
            <td class="text-center">
              <a target="_blank" title="File Commission Report Detail" class="text-success <?=$showFCRD;?>" href="<?=$inv['filecommissionreportdetail'];?>"><i class="fa fa-file-excel-o"></i></a>
              <a target="_blank" title="File Proforma Invoice" class="text-danger  <?=$showPI;?>" href="<?=$inv['fileproformainvoice'];?>"><i class="fa fa-file-pdf-o"></i></a>
              <a target="_blank" title="File Invoice" class="text-primary  <?=$showI;?>" href="<?=$inv['fileinvoice'];?>"><i class="fa fa-file-text"></i></a>
              <a target="_blank" title="File Faktur Pajak" class="text-warning  <?=$showFP;?>" href="<?=$inv['fakturpajak'];?>"><i class="fa fa-file-text-o"></i></a>
            </td>
          </tr>
        <?php
        }
        ?>
      </tbody>
    </table>
  </div>
</div>
