<?php
$invoicing_error = true;

// echo '<pre>';
// print_r($_POST);
// echo '</pre>';
try{

	//*/
	function uploadFile($dbFieldName, $allowedExtension, $file_name, $file_tmp, $folder_destination){
		global $upload_base, $web_url;
		$file = array();
		$tmp = explode('.', $file_name);
		$extension = end($tmp);
		$file_url = '';
		if(!empty($file_name)){
			if(in_array($extension, $allowedExtension)){
				$file_newname = str_replace(' ', '_', $file_name);
				$file_newname = str_replace('.'.$extension, '', $file_newname);
				$ext = '.'.strtolower($extension);
				$file_finalname = substr($file_newname, 0, 10).date('Ymd_His').$ext;
				$upload_destination = $upload_base.$folder_destination.$file_finalname;
				move_uploaded_file($file_tmp, $upload_destination);
				$file_url = $web_url.'/'.$folder_destination.$file_finalname;
				$file['uploadpath'] = $upload_destination;
				$file['url'] = $file_url;
				$file['name'] = $file_name;
			}
		}
		return $file;
	}
	if($_POST['newinvoicestatusoid'] == '3' and !empty($_FILES['filecommissionreportdetail']['name'])){
		$filecommissionreportdetail = uploadFile('filecommissionreportdetail', array('xls', 'xlsx'), $_FILES['filecommissionreportdetail']['name'], $_FILES['filecommissionreportdetail']['tmp_name'], 'document/commission-report/');
	}
	if($_POST['newinvoicestatusoid'] == '5' and !empty($_FILES['fileproformainvoice']['name'])){
		$fileproformainvoice = uploadFile('fileproformainvoice', array('pdf'), $_FILES['fileproformainvoice']['name'], $_FILES['fileproformainvoice']['tmp_name'], 'document/proforma-invoice/');
	}
	if($_POST['newinvoicestatusoid'] == '7' and !empty($_FILES['fileinvoice']['name'])){
		$fileinvoice = uploadFile('fileinvoice', array('pdf', 'jpg', 'jpeg', 'png', 'doc', 'docx', 'xls', 'xlsx'), $_FILES['fileinvoice']['name'], $_FILES['fileinvoice']['tmp_name'], 'document/invoice/');
	}
	if($_POST['newinvoicestatusoid'] == '7' and !empty($_FILES['fakturpajak']['name'])){
		$fakturpajak = uploadFile('fakturpajak', array('pdf', 'jpg', 'jpeg', 'png', 'doc', 'docx', 'xls', 'xlsx'), $_FILES['fakturpajak']['name'], $_FILES['fakturpajak']['tmp_name'], 'document/faktur-pajak/');
	}

	//*/

	// $allowedExtension = array('xls', 'xlsx');
	// $tmp = explode('.', $_FILES['filecommissionreportdetail']['name']);
	// $extension = end($tmp);
	// $filecommissionreportdetail = '';
	// if(!empty($_FILES['filecommissionreportdetail']['name'])){
	// 	if(in_array($extension, $allowedExtension)){
	// 		$filecommissionreportdetail_name = str_replace(' ', '_', $_FILES['filecommissionreportdetail']['name']);
	// 		$filecommissionreportdetail_name = str_replace('.'.$extension, '', $filecommissionreportdetail_name);
	// 		$ext = '.'.strtolower($extension);
	// 		$filecommissionreportdetail__new_name = substr($filecommissionreportdetail_name, 0, 10).date('Ymd_His').$ext;
	// 		$folder_destination = 'document/commission-report/';
	// 		$upload_destination['filecommissionreportdetail'] = $upload_base.$folder_destination.$filecommissionreportdetail__new_name;
	// 		move_uploaded_file($_FILES['filecommissionreportdetail']['tmp_name'], $upload_destination['filecommissionreportdetail']);
	// 		$filecommissionreportdetail = $web_url.'/'.$folder_destination.$filecommissionreportdetail__new_name;			
	// 	}else{
	// 		echo'<script>alert("Data Commission Report Detail not saved.\\nInvalid file.")</script>';
	// 	}
	// }

	// $allowedExtension = array('pdf');
	// $tmp = explode('.', $_FILES['fileproformainvoice']['name']);
	// $extension = end($tmp);
	// $fileproformainvoice = '';
	// if(!empty($_FILES['fileproformainvoice']['name'])){
	// 	if(in_array($extension, $allowedExtension)){
	// 		$fileproformainvoice_name = str_replace(' ', '_', $_FILES['fileproformainvoice']['name']);
	// 		$fileproformainvoice_name = str_replace('.'.$extension, '', $fileproformainvoice_name);
	// 		$ext = '.'.strtolower($extension);
	// 		$fileproformainvoice__new_name = substr($fileproformainvoice_name, 0, 10).date('Ymd_His').$ext;
	// 		$folder_destination = 'document/proforma-invoice/';
	// 		$upload_destination['fileproformainvoice'] = $upload_base.$folder_destination.$fileproformainvoice__new_name;
	// 		move_uploaded_file($_FILES['fileproformainvoice']['tmp_name'], $upload_destination['fileproformainvoice']);
	// 		$fileproformainvoice = $web_url.'/'.$folder_destination.$fileproformainvoice__new_name;			
	// 		// echo'<script>alert("Berhasil.")</script>';
	// 	}else{
	// 		echo'<script>alert("Data Proforma Invoice not saved.\\nInvalid file.")</script>';
	// 	}
	// }

	// $allowedExtension = array('pdf', 'jpg', 'jpeg', 'png');
	// $tmp = explode('.', $_FILES['fileinvoice']['name']);
	// $extension = end($tmp);
	// $fileinvoice = '';
	// if(!empty($_FILES['fileinvoice']['name'])){
	// 	if(in_array($extension, $allowedExtension)){
	// 		$fileinvoice_name = str_replace(' ', '_', $_FILES['fileinvoice']['name']);
	// 		$fileinvoice_name = str_replace('.'.$extension, '', $fileinvoice_name);
	// 		$ext = '.'.strtolower($extension);
	// 		$fileinvoice__new_name = substr($fileinvoice_name, 0, 10).date('Ymd_His').$ext;
	// 		$folder_destination = 'document/invoice/';
	// 		$upload_destination['fileinvoice'] = $upload_base.$folder_destination.$fileinvoice__new_name;
	// 		move_uploaded_file($_FILES['fileinvoice']['tmp_name'], $upload_destination['fileinvoice']);
	// 		$fileinvoice = $web_url.'/'.$folder_destination.$fileinvoice__new_name;			
	// 		// echo'<script>alert("Berhasil.")</script>';
	// 	}else{
	// 		echo'<script>alert("Data Invoice not saved.\\nInvalid file.")</script>';
	// 	}
	// }

	// $allowedExtension = array('pdf', 'jpg', 'jpeg', 'png');
	// $tmp = explode('.', $_FILES['fakturpajak']['name']);
	// $extension = end($tmp);
	// $fakturpajak = '';
	// if(!empty($_FILES['fakturpajak']['name'])){
	// 	if(in_array($extension, $allowedExtension)){
	// 		$fakturpajak_name = str_replace(' ', '_', $_FILES['fakturpajak']['name']);
	// 		$fakturpajak_name = str_replace('.'.$extension, '', $fakturpajak_name);
	// 		$ext = '.'.strtolower($extension);
	// 		$fakturpajak__new_name = substr($fakturpajak_name, 0, 10).date('Ymd_His').$ext;
	// 		$folder_destination = 'document/faktur-pajak/';
	// 		$upload_destination['fakturpajak'] = $upload_base.$folder_destination.$fakturpajak__new_name;
	// 		move_uploaded_file($_FILES['fakturpajak']['tmp_name'], $upload_destination['fakturpajak']);
	// 		$fakturpajak = $web_url.'/'.$folder_destination.$fakturpajak__new_name;			
	// 		// echo'<script>alert("Berhasil.")</script>';
	// 	}else{
	// 		echo'<script>alert("Data Faktur Pajak not saved.\\nInvalid file.")</script>';
	// 	}
	// }

	$sUpdateInv = "UPDATE invoice SET 
		hotelcontactoid = :b, 
		reconciledamount = :c, 
		invoiced = :d, 
		ppn = :e,
		paid = :f,
		nofakturpajak = :h,
		npwp = :j,
		pthotel = :k,
		attentionpic = :l,
		contractnumber = :m,
		invoicenumber = :n,
		invoicedescription = :s
		WHERE invoiceoid = :a
	";
	$stmt = $db->prepare($sUpdateInv);
	$stmt->execute(array(
		':a' => $_POST['invoiceoid'],
		':b' => $_POST['hotelcontactoid'],
		':c' => $_POST['reconciledamount'],
		':d' => $_POST['invoiced'],
		':e' => $_POST['ppn'],
		':f' => $_POST['paid'],
		':h' => $_POST['nofakturpajak'],
		':j' => $_POST['npwp'],
		':k' => $_POST['pthotel'],
		':l' => $_POST['attns'],
		':m' => $_POST['contractnumber'],
		':n' => $_POST['invoicenumber'],
		':s' => $_POST['invoicedescription']
	));

	// COMMISSION REPORT DETAIL
	if($_POST['newinvoicestatusoid'] == '3' and !empty($filecommissionreportdetail)){
		$sUpdateInvFileCRD = "UPDATE invoice SET 
			filecommissionreportdetail = :r
			WHERE invoiceoid = :a
		";
		$stmt = $db->prepare($sUpdateInvFileCRD);
		$stmt->execute(array(
			':a' => $_POST['invoiceoid'],
			':r' => $filecommissionreportdetail['url']
		));
	}

	// PROFORMA INVOICE
	if($_POST['newinvoicestatusoid'] == '5' and !empty($fileproformainvoice)){
		$sUpdateInvFilePRI = "UPDATE invoice SET 
			fileproformainvoice = :p
			WHERE invoiceoid = :a
		";
		$stmt = $db->prepare($sUpdateInvFilePRI);
		$stmt->execute(array(
			':a' => $_POST['invoiceoid'],
			':p' => $fileproformainvoice['url']
		));
	}


	// INVOICE
	if($_POST['newinvoicestatusoid'] == '7' and !empty($fileinvoice)){
		$sUpdateInvFileINV = "UPDATE invoice SET 
			fileinvoice = :q
			WHERE invoiceoid = :a
		";
		$stmt = $db->prepare($sUpdateInvFileINV);
		$stmt->execute(array(
			':a' => $_POST['invoiceoid'],
			':q' => $fileinvoice['url']
		));
	}

	// FAKTUR PAJAK
	if($_POST['newinvoicestatusoid'] == '7' and !empty($fakturpajak)){
		$sUpdateInvFileFP = "UPDATE invoice SET 
			fakturpajak = :i
			WHERE invoiceoid = :a
		";
		$stmt = $db->prepare($sUpdateInvFileFP);
		$stmt->execute(array(
			':a' => $_POST['invoiceoid'],
			':i' => $fakturpajak['url']
		));
	}

	if($_POST['newinvoicestatusoid'] == '3' or $_POST['newinvoicestatusoid'] == '5' or $_POST['newinvoicestatusoid'] == '7'){
		$attns=json_decode($_POST['attns'], true);
		include 'invoicing-action-send.php';
	}else{
		$invoicing_error = false;
	}

	// reset array biar kosong
	$filecommissionreportdetail = array();
	$fileproformainvoice = array();
	$fileinvoice = array();
	$fakturpajak = array();

} catch (Exception $e) {
	echo "<b>All Error :</b><br>"; 
	echo $e->getMessage();
}

if($invoicing_error == false){
	header('Location: '.$base_url.'/invoicing');

	$sUpdateInv = "UPDATE invoice SET 
		invoicestatusoid = :o
		WHERE invoiceoid = :a
	";
	$stmt = $db->prepare($sUpdateInv);
	$stmt->execute(array(
		':a' => $_POST['invoiceoid'],
		':o' => $_POST['newinvoicestatusoid']
	));
}
?>