<?php
	$poioid = $_POST['cpoid'];
	$title = (isset($_POST['title'])) ? $_POST['title'] : "";
	$headline = $_POST['headline'];
	$article = $_POST['article'];
	
	$metatitle = $_POST['metatitle'];
	$metadescription = $_POST['metadescription'];
	$metakeyword = $_POST['metakeyword'];

	$allowedExtension = array('jpg', 'jpeg', 'png');
	$tmp = explode('.', $_FILES['image']['name']);
	$extension = end($tmp);
	$image = '';
	if(!empty($_FILES['image']['name'])){
		if(in_array($extension, $allowedExtension)){
			$imagename = str_replace(' ', '_', $_FILES['image']['name']);
			$imagename = str_replace('.'.$extension, '', $imagename);
			$ext = '.'.strtolower($extension);
			$image_new_name = substr($imagename, 0, 10).date('Ymd_His').$ext;
			$folder_destination = 'pict/poi/';

			move_uploaded_file($_FILES['image']['tmp_name'], $upload_base.$folder_destination.$image_new_name);
			$image = $web_url.'/'.$folder_destination.$image_new_name;

			try {
				$stmt = $db->prepare("update poi set poioid = :ac  where poioid = :ab");
				$stmt->execute(array(':ab' => $poioid, ':ac' => $image));
			}catch(PDOException $ex) {
				echo "Invalid Query"; print($ex);
				die();
			}

		}else{
			echo'<script>alert("Image not saved.\\nInvalid file.")</script>';
		}
	}
	
	$published = $_POST['published'];
	$category = $_POST['category'];

	try {
		$stmt = $db->prepare("UPDATE `poi` SET `title`=:b,`headline`=:d,`article`=:e,`meta_title`=:f,`meta_description`=:g,`meta_keyword`=:h,`poicategoryoid`=:i,`publishedoid`=:j WHERE `poioid`=:a");
		$stmt->execute(array(
		    ':a' => $poioid,
		    ':b' => $title,
		    ':d' => $headline,
		    ':e' => $article,
		    ':f' => $metatitle,
		    ':g' => $metadescription,
		    ':h' => $metakeyword,
		    ':i' => $category,
		    ':j' => $published
		    ));
		    
	}catch(PDOException $ex) {
		echo "Invalid Query";
		print($ex);
		die();
	}
	
	if(count($_POST['area'])>0){
		$query_delete_icon = "DELETE FROM `poiarea` WHERE `poioid`='".$poioid."'";
		$stmt = $db->query($query_delete_icon);

		foreach ($_POST['area'] as $key => $value) {
    		try {
    			$stmt = $db->prepare("insert into poiarea (`poioid`, `areaoid`) values (:a,:b)");
    			$stmt->execute(array(':a' => $poioid, ':b' => $value));
    		}catch(PDOException $ex) {
    			echo "Invalid Query";
    			print($ex);
    			die();
    		}
    	    if($value == '0'){ break; }
    	}
	}else{
		$query_delete_icon = "DELETE FROM `poiarea` WHERE `poioid`='".$poioid."'";
		$stmt = $db->query($query_delete_icon);
	}
	
	if(count($_POST['city'])>0){
		$query_delete_icon = "DELETE FROM `poicity` WHERE `poioid`='".$poioid."'";
		$stmt = $db->query($query_delete_icon);

		foreach ($_POST['city'] as $key => $value) {
    		try {
    			$stmt = $db->prepare("insert into poicity (`poioid`, `cityoid`) values (:a,:b)");
    			$stmt->execute(array(':a' => $poioid, ':b' => $value));
    		}catch(PDOException $ex) {
    			echo "Invalid Query";
    			print($ex);
    			die();
    		}
    	    if($value == '0'){ break; }
    	}
	}else{
		$query_delete_icon = "DELETE FROM `poicity` WHERE `poioid`='".$poioid."'";
		$stmt = $db->query($query_delete_icon);
	}

	header("Location: ". $base_url ."/poi");
?>