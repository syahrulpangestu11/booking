<script type="text/javascript">
$(function(){
	
	$(document).ready(function(){ 
		
		getLoadData();
	});
	
	function readURL(input, target) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
				target.attr('src', e.target.result).show();
			};
			reader.readAsDataURL(input.files[0]);
		}
	}
	
	$('body').on('change','input[name=image]', function(e){
		box_preview_image = $('div.preview-image');
		if(this.files[0].size <= 0){
			box_preview_image.html('<div class="panel panel-warning"><div class="panel-heading">N/A Image for Promotion</div></div>');
		}else{
			var ext = $(this).val().split('.').pop().toLowerCase();
			if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
				box_preview_image.html('<div class="panel panel-warning"><div class="panel-heading">Invalid extension! Please upload image with JPEG, PNG, JPG, and GIF type only.</div></div>');
			}else{
				box_preview_image.html('<img src="#" id="preview-image" class="img-responsive" alt="Responsive image" style="width:200px;">');
				target = $('div.preview-image').children('img');
				readURL(this, target);
			}
		}
	});
	
	$('body').on('click','button.edit', function(e) {
		pid = $(this).attr("pid");
		url = "<?php echo $base_url; ?>/poi/edit/?cpoid="+pid;
      	$(location).attr("href", url);
	});

	$('body').on('click','button.cancel', function(e) {
		pid = $(this).attr("pid");
		url = "<?php echo $base_url; ?>/poi";
      	$(location).attr("href", url);
	});

	$('body').on('click','button.add', function(e) {
		pid = $(this).attr("pid");
		url = "<?php echo $base_url; ?>/poi/add";
      	$(location).attr("href", url);
	});
	
   var $dialogDelete = $('<div id="dialog-del"></div>')
	.html('Are you sure you want to delete this data?')
	.dialog({
		autoOpen: false,
		title: 'Confirmation',
		buttons: { 
			"Delete": function(){ 
				var url = $(this).data('url'); 
				var id = $(this).data('id'); 
				$(this).dialog("close"); 
				deleteAction(url, id);
			},
			Cancel: function(){ $( this ).dialog("close"); }
		}
	});
	
	var $dialogNotification = $('<div id="dialog-notice"></div>')
	.dialog({
		autoOpen: false,
		title: 'Notification',
		buttons: { 
			"Ok": function(){ 
				var callback = '<?php echo $base_url; ?>/poi'; 
				$(this).dialog( "close" );
				$(location).attr("href", callback); 
			}
		}
	});
	
	function deleteAction(url, id){
		$.ajax({
			url: url,
			type: 'post',
			data: { id : id},
			success: function(data) {
				if(data == "1"){
					$dialogNotification.html("Data has been deleted");
					$dialogNotification.dialog("open");
				}else{
					$dialogNotification.html("We&rsquo;re very sorry, we can't save your data right now");
					$dialogNotification.dialog("open");
				}
			}
		});
	}
	
	$('body').on('click','button.delete', function(e) {
		id = $(this).attr("pid");
		$dialogDelete.data("url", "<?php echo $base_url; ?>/includes/suadm/poi/delete-cp.php");
		$dialogDelete.data("id", id);
		$dialogDelete.dialog("open");
	});
		
	function getLoadData(){
		$.ajax({
			url: "<?php echo"$base_url"; ?>/includes/suadm/poi/data.php",
			type: 'post',
			data: $('form#data-input').serialize(),
			success: function(data) {
				$("#data-box").html(data)
			}
		});
	}
});
</script>