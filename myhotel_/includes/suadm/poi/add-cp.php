<?php
	include('includes/bootstrap.php');
?>
<script type="text/javascript">
$(function() {
	$('textarea#html-box').trumbowyg({
		btns: ['viewHTML',
		  '|', 'btnGrp-design',
		  '|', 'link',
		  '|', 'btnGrp-justify',
		  '|', 'btnGrp-lists'],
		fullscreenable: false
	});
});
</script>
<section class="content-header">
    <h1>
        Create POI Data
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i>  POI Data </a></li>
        <li class="active">Create New</li>
    </ol>
</section>
<section class="content" id="promotion">
	<div class="row">
        <form class="form-box form-horizontal form-box" method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/poi/add-process">
        <div class="box box-form">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-md-12">POI Title</label>
                    <div class="col-md-12"><input type="text" class="form-control" name="title" required="required" style="width:100%"></div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">Upload New Promotion Image</label>
                    <div class="col-md-12"><input type="file" class="form-control" name="image"><i>recommended size : 550px x 310px</i></div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <label>Headline</label>
                        <textarea id="html-box" name="headline"></textarea><br>
                    </div>
                    <div class="col-md-12">
                        <label>Article</label>
                        <textarea id="html-box" name="article"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <label>POI Category</label>
                        <select name="category" class="form-control">
                        <?php
                            try {
                                $stmt = $db->query("select * from poicategory");
                                $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                foreach($r_room as $row){
                                    echo"<option value='".$row['poicategoryoid']."'>".$row['categoryname']."</option>";
                                }
                            }catch(PDOException $ex) {
                                echo "Invalid Query";
                                die();
                            }
                        ?>
                        </select><br>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="col-md-12">
                        <label>Meta Title</label><br>
                        <textarea name="metatitle" class="form-control" style="width:100%" rows="3"></textarea><br>
                    </div>
                    <div class="col-md-12">
                        <label>Meta Description</label><br>
                        <textarea name="metadescription" class="form-control" style="width:100%" rows="3"></textarea><br>
                    </div>
                    <div class="col-md-12">
                        <label>Meta Keyword</label><br>
                        <textarea name="metakeyword" class="form-control" style="width:100%" rows="3"></textarea><br>
                    </div>
                    <div class="col-md-12">
                        <label>Area</label>
                        <select name="area[]" class="form-control" multiple rows="4">
                            <option value='0'>- All Area -</option>
                        <?php
                            try {
                                $stmt = $db->query("select * from state");
                                $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                foreach($r_room as $row){
                                    echo"<option value='".$row['stateoid']."'>".$row['statename']."</option>";
                                }
                            }catch(PDOException $ex) {
                                echo "Invalid Query";
                                die();
                            }
                        ?>
                        </select><br>
                    </div>
                    <div class="col-md-12">
                        <label>City</label>
                        <select name="city[]" class="form-control" multiple rows="4">
                            <option value='0'>- All City -</option>
                        <?php
                            try {
                                $stmt = $db->query("select * from city");
                                $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                foreach($r_room as $row){
                                    echo"<option value='".$row['cityoid']."'>".$row['cityname']."</option>";
                                }
                            }catch(PDOException $ex) {
                                echo "Invalid Query";
                                die();
                            }
                        ?>
                        </select><br>
                    </div>
                    <div class="col-md-12">
                        <label>Publish POI</label>
                        <select name="published" class="form-control">
                        <?php
                            try {
                                $stmt = $db->query("select * from published where showthis = 'y' ORDER BY publishedoid DESC");
                                $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                foreach($r_room as $row){
                                    echo"<option value='".$row['publishedoid']."' style='text-transform: capitalize;'>".$row['note']."</option>";
                                }
                            }catch(PDOException $ex) {
                                echo "Invalid Query";
                                die();
                            }
                        ?>
                        </select><br>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
            <div class="box-footer" style="margin-top:15px">
                <div class="col-md-12 text-right">
                    <button class="btn btn-danger cancel" type="button">Cancel</button>
                    <button class="btn btn-primary" type="submit">Save Data</button>
                </div>
            </div>
		</div>
		</form>
    </div>
</section>