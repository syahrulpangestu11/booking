
  <?php
  error_reporting(E_ALL ^ E_NOTICE);
  require_once("../../../conf/connection.php");
  include("../../paging/pre-paging.php");
  ?>
  <div class="row">
    <div class="col-md-12">
      <?php
        $main_query = "select ch.chainoid, ch.name as chainname from chain ch where ch.publishedoid = '1'";

        $filter = array();
        if(!empty($_POST['chainname'])){
          array_push($filter, 'ch.name like "%'.$_POST['chainname'].'%"');
        }

        if(count($filter) > 0){ $filter_query = ' and '.implode(" and ", $filter); }else{ $filter_query = "";  }
        $stmt = $db->query($main_query.$filter_query.' order by ch.chainoid desc '.$paging_query);
      ?>
      <table class="table table-bordered">
        <thead><tr><th class="text-center">Apply</th><th class="text-center">Chain / Group</th></tr></thead>
        <tbody>
          <tr style="display:none;"><td></td></tr>
        <?php
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach($result as $chain){
        ?>
          <tr>
            <td class="text-center"><input type="checkbox" name="choicechain" value="<?=$chain['chainoid']?>" data-name="<?=$chain['chainname']?>"></td>
            <td><?=$chain['chainname']?></td>
          </tr>
        <?php
        }
        if(empty($result)){ echo "<tr><td colspan='3'><i class='fa fa-info-circle'></i> No data found</td></tr>"; }
        ?>
        </tbody>
      </table>
      <?php
      $main_count_query = "select count(chainoid) as jml from chain ch where ch.publishedoid = '1'";
      $stmt = $db->query($main_count_query.$filter_query);
      $jmldata = $stmt->fetchColumn();

      $tampildata = $row_count;
      include("../../paging/post-paging.php");
      ?>
    </div>
  </div>
