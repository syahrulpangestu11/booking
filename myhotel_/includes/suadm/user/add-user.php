<?php
	$username = $_GET['uo'];
	$emailrole = array('to','cc','bcc');
	$enum = array('y', 'n');
?>
<style>
#listproperty {
    max-height: 245px;
    overflow: scroll;
}
#role-box{
    background-color: #f2fbfd;
    border: 1px solid #d9ddde;
	margin:10px 0;
	padding:10px;
}
.form-group input[type='text']{ width:100%; }
</style>
<section class="content-header">
    <h1>
        Edit User
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-laptop"></i> User</a></li>
        <li class="active">Edit User</li>
    </ol>
</section>
<section class="content">
<form method="post" enctype="multipart/form-data" class="form-box" id="data-input" action="<?php echo $base_url; ?>/user/add-save">
      <div class="box box-form">
        <div class="box-body">
					<h3>Contact Information</h3>
          <div class="row">
						<div class="col-md-6 col-xs-12">
							<div class="form-group">
              	<label>Name</label><input type="text" class="form-control" name="displayname" required="required">
              </div>
							<div class="form-group">
								<label>Email</label><input type="text" class="form-control" name="email">
							</div>
							<div class="form-group">
								<label>Mobile Phone</label><input type="text" class="form-control" name="mobile">
							</div>
							<div class="form-group">
                <label>User Status</label>
                <select class="form-control" name="status">
                <?php
               		$liststatus = array(0,1);
									$liststatus_name = array('Inactive', 'Active');
                  foreach($liststatus as $key => $value){
										if($key == 1){ $selected = "selected"; }else{ $selected = ""; }
                ?>
                  <option value="<?php echo $value; ?>" <?=$selected?>><?php echo $liststatus_name[$key]; ?></option>
                <?php
                    }
                ?>
                </select>
							</div>
						</div>
						<div class="col-md-6 col-xs-12">
							<div class="panel panel-primary">
								<div class="panel-heading">User Setting</div>
								<div class="panel-body">
									<div class="form-group">
										<label>Username</label><input type="text" class="form-control" name="username" required="required">
									</div>
									<div class="form-group">
										<label>Password</label><input type="password" class="form-control" name="password" required="required">
									</div>
									<hr>
									<div class="form-group">
										<label>User privilige to access :</label>
							      <div class="checkbox">
							        <label><input type="checkbox" name="ibeuser" value="y" data-target="ibe-role" checked> IBE (Internet Booking Enginee)</label>
							      </div>
										<!-- <div class="checkbox">
							        <label><input type="checkbox" name="pmsuser" value="y" data-target="pms-role" > PMS (Property Management System)</label>
							      </div> -->
								  </div>
									<div class="form-group ibe-role">
										<label>Access IBE as : </label>
										<select class="form-control" name="role">
										<?php
										try {
												if($_SESSION['_typeusr'] == 1){
														$xwhere = "";
												}else if ($_SESSION['_typeusr'] == 2){
														$xwhere = "where userstypeoid in (3,4)";
												}else if ($_SESSION['_typeusr'] == 3){
														$xwhere = "where userstypeoid in (3)";
												}else if ($_SESSION['_typeusr'] == 4){
														$xwhere = "where userstypeoid in (3)";
												}

												$stmt = $db->query("select * from userstype ".$xwhere);
												$r_userstype = $stmt->fetchAll(PDO::FETCH_ASSOC);
												foreach($r_userstype as $row){
										?>
												<option value="<?php echo $row['userstypeoid']; ?>"><?php echo $row['usertype']; ?></option>
										<?php
												}
										}catch(PDOException $ex) {
												echo "Invalid Query";
												die();
										}
										?>
										</select>
									</div>
									<!-- <div class="form-group pms-role">
										<label>Access PMS as : </label>
										<select class="form-control" name="role_pms">
										<?php
										// try {
										// 		$stmt = $db->query("select * from userpmstype");
										// 		$r_userstype = $stmt->fetchAll(PDO::FETCH_ASSOC);
										// 		foreach($r_userstype as $row){
										?>
												<option value="<?php //echo $row['userpmstypeoid']; ?>"><?php //echo $row['userpmstype']; ?></option>
										<?php
										// 		}
										// }catch(PDOException $ex) {
										// 		echo "Invalid Query";
										// 		die();
										// }
										?>
										</select>
									</div> -->
								</div>
							</div>
						</div>
					</div>
        </div>
   		</div>

			<div class="box box-form">
        <div class="box-body">
					<div class="row">
						<div class="col-md-6"><h1>Properti to manage</h1></div>
						<div class="col-md-6 text-right">
							<button type="button" name="select-property" class="btn btn-primary">Select Property</button>
							<!--  data-toggle="modal" data-target="#selectPropertyModal" -->
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-12 col-xs-12">
							<table id="list-applied-property" class="table table-bordered">
								<thead></thead>
								<tbody>
									<tr style="display:none;"></tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<button type="button" class="btn btn-danger cancel">Cancel</button>
								<button type="submit" class="btn btn-primary submit">Save</button>
							</div>
						</div>
					</div>
			</div>
    </form>
</section>


<script>
$('button[name=select-property]').on('click',function(e){
	e.preventDefault();
	$('#selectPropertyModal').modal('show');
})
</script>
