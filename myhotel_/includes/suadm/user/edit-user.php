<?php
	$username = $_GET['uo'];
	$emailrole = array('to','cc','bcc');
	$enum = array('y', 'n');

	$stmt = $db->query("select u.* from users u where u.username = '".$_REQUEST['uo']."'");
	$row_count = $stmt->rowCount();
	if($row_count > 0) {
		$user = $stmt->fetch(PDO::FETCH_ASSOC);
	}
?>
<style>
#listproperty {
    max-height: 245px;
    overflow: scroll;
}
#role-box{
    background-color: #f2fbfd;
    border: 1px solid #d9ddde;
	margin:10px 0;
	padding:10px;
}
.form-group input[type='text']{ width:100%; }
</style>
<section class="content-header">
    <h1>
        Edit User
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-laptop"></i> User</a></li>
        <li class="active">Edit User</li>
    </ol>
</section>
<section class="content">
<form method="post" enctype="multipart/form-data" class="form-box" id="data-input" action="<?php echo $base_url; ?>/user/edit-save">
	<input type="hidden" name="useroid" value="<?php echo $user['useroid']; ?>" />
      <div class="box box-form">
        <div class="box-body">
					<h3>Contact Information</h3>
          <div class="row">
						<div class="col-md-6 col-xs-12">
							<div class="form-group">
              	<label>Name</label><input type="text" class="form-control" name="displayname" required="required" value="<?=$user['displayname']?>">
              </div>
							<div class="form-group">
								<label>Email</label><input type="text" class="form-control" name="email" value="<?=$user['email']?>">
							</div>
							<div class="form-group">
								<label>Mobile Phone</label><input type="text" class="form-control" name="mobile" value="<?=$user['mobile']?>">
							</div>
							<div class="form-group">
                <label>User Status</label>
                <select class="form-control" name="status">
                <?php
               		$liststatus = array(0,1);
									$liststatus_name = array('Inactive', 'Active');
                  foreach($liststatus as $key => $value){
										if($value == $user['status']){ $selected = "selected"; }else{ $selected = ""; }
                ?>
                  <option value="<?php echo $value; ?>" <?=$selected?>><?php echo $liststatus_name[$key]; ?></option>
                <?php
                    }
                ?>
                </select>
							</div>
						</div>
						<div class="col-md-6 col-xs-12">
							<div class="panel panel-primary">
								<div class="panel-heading">User Setting</div>
								<div class="panel-body">
									<div class="form-group">
										<label>Username</label><input type="text" class="form-control" name="username" required="required" value="<?=$user['username']?>" readonly>
									</div>
									<div class="form-group">
										<label>Password</label><input type="password" class="form-control" name="password">
									</div>
									<hr>
									<div class="form-group">
										<label>User privilige to access :</label>
							      <div class="checkbox">
							        <label><input type="checkbox" name="ibeuser" value="y" data-target="ibe-role" <?php if($user['ibeuser'] == "y"){ echo "checked"; } ?>> IBE (Internet Booking Enginee)</label>
							      </div>
										<!-- <div class="checkbox">
							        <label><input type="checkbox" name="pmsuser" value="y" data-target="pms-role"
									 <?php //if($user['pmsuser'] == "y"){ echo "checked"; } ?>> PMS (Property Management System)</label>
							      </div> -->
								  </div>
									<div class="form-group ibe-role" <?php if($user['ibeuser'] == "n"){ echo "style='display:none'"; } ?>>
										<label>Access IBE as : </label>
										<select class="form-control" name="role" <?php if($user['ibeuser'] == "n"){ echo "disabled"; } ?>>
										<?php
										try {
												if($_SESSION['_typeusr'] == 1){
														$xwhere = "";
												}else if ($_SESSION['_typeusr'] == 2){
														$xwhere = "where userstypeoid in (3,4)";
												}else if ($_SESSION['_typeusr'] == 3){
														$xwhere = "where userstypeoid in (3)";
												}else if ($_SESSION['_typeusr'] == 4){
														$xwhere = "where userstypeoid in (3)";
												}

												$stmt = $db->query("select * from userstype ".$xwhere);
												$r_userstype = $stmt->fetchAll(PDO::FETCH_ASSOC);
												foreach($r_userstype as $row){
													if($row['userstypeoid'] == $user['userstypeoid']){ $selected = "selected"; }else{ $selected = ""; }
										?>
												<option value="<?php echo $row['userstypeoid']; ?>" <?=$selected?>><?php echo $row['usertype']; ?></option>
										<?php
												}
										}catch(PDOException $ex) {
												echo "Invalid Query";
												die();
										}
										?>
										</select>
									</div>
									<!-- <div class="form-group pms-role" <?php //if($user['pmsuser'] == "n"){ echo "style='display:none'"; } ?>>
										<label>Access PMS as : </label>
										<select class="form-control" name="role_pms" <?php //if($user['pmsuser'] == "n"){ echo "disabled"; } ?>>
										<?php
										// try {
										// 		$stmt = $db->query("select * from userpmstype");
										// 		$r_userstype = $stmt->fetchAll(PDO::FETCH_ASSOC);
										// 		foreach($r_userstype as $row){
										// 			if($row['userpmstypeoid'] == $user['userpmstypeoid']){ $selected = "selected"; }else{ $selected = ""; }
										?>
												<option value="<?php //echo $row['userpmstypeoid']; ?>" <?php //echo $selected?>><?php //echo $row['userpmstype']; ?></option>
										<?php
										// 		}
										// }catch(PDOException $ex) {
										// 		echo "Invalid Query";
										// 		die();
										// }
										?>
										</select>
									</div> -->
								</div>
							</div>
						</div>
					</div>
        </div>
   		</div>

			<div class="box box-form">
        <div class="box-body">
					<div class="row">
						<div class="col-md-6"><h1>Properti to manage</h1></div>
						<div class="col-md-6 text-right">
							<button type="button" name="select-property" class="btn btn-primary" <?php if($user['userstypeoid'] == 1 or $user['userstypeoid'] == 2 ){ echo "style='display:none'"; } ?>>Select Property</button>
							<!--  data-toggle="modal" data-target="#selectPropertyModal" -->
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-12 col-xs-12">
							<table id="list-applied-property" class="table table-bordered">
								<?php
									if($user['userstypeoid'] == '3' or $user['userstypeoid'] == '6' or in_array($user['userpmstypeoid'], array(2, 4, 5))){
										echo "<thead><tr><th>#</th><th>Hotel</th><th>Chain</th><th></th></tr></thead>";
										echo "<tbody><tr style='display:none;'></tr>";
										$stmt = $db->prepare("select userassignoid, c.chainoid, c.name as chainname, h.hotelname, h.hoteloid from userassign ua inner join hotel h on h.hoteloid = ua.oid left join chain c using (chainoid) where useroid = :a and ua.type='hoteloid'");

										$stmt->execute(array(':a' => $user['useroid']));
										$userassign = $stmt->fetchAll(PDO::FETCH_ASSOC);
										foreach($userassign as $property){
											echo "<tr><td><input type='hidden' name='slchotel[]' value='".$property['hoteloid']."'></td><td>".$property['hotelname']."</td><td>".$property['chainname']."</td><td class='text-center'><a href='#' id='remove' style='color:#ec5757'><i class='fa fa-trash'></i></a></td></tr>";
										}
										echo "</tbody>";
									}else if($user['userstypeoid'] == '4' or in_array($user['userpmstypeoid'], array(1, 3))){
										echo "<thead><tr><th>#</th><th>Chain</th><th></th></tr></thead>";
										echo "<tbody><tr style='display:none;'></tr>";
										$stmt = $db->prepare("select userassignoid, c.chainoid, c.name from userassign ua inner join chain c on c.chainoid = ua.oid where useroid = :a and ua.type='chainoid'");
										$stmt->execute(array(':a' => $user['useroid']));
										$userassign = $stmt->fetchAll(PDO::FETCH_ASSOC);
										foreach($userassign as $property){
											echo "<tr><td><input type='hidden' name='slcchain[]' value='".$property['chainoid']."'></td><td>".$property['name']."</td><td class='text-center'><a href='#' id='remove' style='color:#ec5757'><i class='fa fa-trash'></i></a></td></tr>";
										}
										echo "</tbody>";
									}else{
										echo "<thead><tr><th>User will manage all property</th></tr></thead>";
									}
								?>
							</table>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<button type="button" class="btn btn-danger cancel">Cancel</button>
								<button type="submit" class="btn btn-primary submit">Save</button>
							</div>
						</div>
						<div class="col-md-66">
							<?php
							if(!empty($user['created']) and !empty($user['createdby'])){
								echo "<i class='fa fa-pencil-square-o'></i> Created on ".date('d/M/Y H:i', strtotime($user['created']))." <small>(GMT+8)</small> by <b>".$user['createdby']."</b><br>";
							}
							if(!empty($user['updated']) and !empty($user['updatedby'])){
								echo "<i class='fa fa-clock-o'></i> Last updated on ".date('d/M/Y H:i', strtotime($user['updated']))." <small>(GMT+8)</small> by <b>".$user['updatedby']."</b>";
							}
							?>

						</div>
					</div>
			</div>
    </form>
</section>


<script>
$('button[name=select-property]').on('click',function(e){
	e.preventDefault();
	$('#selectPropertyModal').modal('show');
})
</script>
