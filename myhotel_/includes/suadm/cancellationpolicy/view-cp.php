<section class="content-header">
    <h1>
       	Cancellation Policy
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Cancellation Policy</a></li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="box">
            <div class="box-body">
                <div class="form-group">
				<form method="get" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/cancellationpolicy/">
            		<input type="hidden" name="hoteloid" value="<?php echo $hoteloid; ?>" />
                    <label>Cancellation Policy Name </label> &nbsp;
                    <input type="text" name="name" class="medium" value="<?php echo $_GET['name']; ?>" />&nbsp;&nbsp;                    
                    <button type="submit" class="blue-button">Find</button>
                </form>
                </div>
            </div><!-- /.box-body -->
       </div>
    </div>
    
    <div class="row">
        <div class="box">
        	<form method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/room-control/process">
                <div id="data-box" class="box-body">
                    <div class="loader">Loading...</div>
                </div><!-- /.box-body -->
            </form>
       </div>
    </div>

</section>
