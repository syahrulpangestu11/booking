<script type="text/javascript">
$(function() {
	$('textarea').trumbowyg({
		btns: ['viewHTML',
		  '|', 'btnGrp-design',
		  '|', 'link',
		  '|', 'btnGrp-justify',
		  '|', 'btnGrp-lists'],
		fullscreenable: false
	});
});
</script>
<style type="text/css">.trumbowyg-box{ padding-top:50px; }</style>
<section class="content-header">
    <h1>
       	Cancellation Policy
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Cancellation Policy</a></li>
        <li class="active">Add</li>
    </ol>
</section>

<section class="content" id="basic-info">
	<div class="row">
        <div class="box box-form">
            <form method="post" enctype="multipart/form-data" id="data-input" class="form-box" action="<?=$base_url?>/cancellationpolicy/add-process">
            <input type="hidden" name="hoteloid" value="<?php echo $hoteloid; ?>">
            <ul class="inline form-input">
                <li>
                    <div class="side-left"><label>Cancellation Policy Name</label></div>
                    <div class="side-right"><input type="text" class="input-text" name="name"></div>
                    <div class="clear"></div>
                </li>
                <li>
                    <div class="side-left"><label>Cancellation Policy Description</label></div>
                    <div class="side-right"><textarea name="description" style="height:100px"></textarea></div>
                    <div class="clear"></div>
                </li>
                <li>
                    <div class="side-left"><label>Terms &amp; Condition</label></div>
                    <div class="side-right">
                        <select name="termcondition" class="input-select">
                        <?php
                            $termscon=array('free cancellation', 'non refundable');
                            foreach($termscon as $tc){
                                if($tc == $termcondition){ $selected = "selected"; }else{ $selected=""; }
                                echo"<option value='".$tc."' ".$selected.">".$tc."</option>";
                            }
                        ?>        	
                        </select>
                    </div>
                    <div class="clear"></div>
                </li>
                <li>
                    <div class="side-left"><label>Cancellation Day</label></div>
                    <div class="side-right">
                        <input type="number" class="input-text" name="cancellationday" min="0" value="7" />
                    </div>
                    <div class="clear"></div>
                </li>
                <li>
                    <div class="side-left"><label>Type Cancellation</label></div>
                    <div class="side-right">
                        <input type="radio" name="typecancellation" value="night" checked="checked" /> <input type="number" class="input-text small" name="night" value="1" min="0" /> night charge<br /> 
                        <input type="radio" name="typecancellation" value="full amount" /> full amount<br /> 
                    </div>
                    <div class="clear"></div>
                </li>
                <li>
                    <div class="side-left"><label>Publish Cancellation Policy</label></div>
                    <div class="side-right">
                        <select name="published" class="input-select">
                        <?php
                            try {
                                $stmt = $db->query("select * from published where showthis = 'y'");
                                $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                foreach($r_room as $row){
                                    if($row['publishedoid'] == '1'){ $selected = "selected"; }else{ $selected=""; }
                                    echo"<option value='".$row['publishedoid']."' ".$selected.">".$row['note']."</option>";
                                }
                            }catch(PDOException $ex) {
                                echo "Invalid Query";
                                die();
                            }
                        ?>        	
                        </select>
                    </div>
                    <div class="clear"></div>
                </li>
                <li>
                    <div class="side-left">&nbsp;</div>
                    <div class="side-right"><button type="submit" class="submit">Save</button>&nbsp;<button class="pure-button red cancel" type="button">Cancel</button></div>
                    <div class="clear"></div>
                </li>
                <div class="clear"></div>
            </ul>
            </form>
   		</div>
    </div>
</section>
