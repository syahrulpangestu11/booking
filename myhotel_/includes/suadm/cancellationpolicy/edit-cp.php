<section class="content-header">
    <h1>
       	Cancellation Policy
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Cancellation Policy</a></li>
        <li class="active">Edit</li>
    </ol>
</section>
<script type="text/javascript">
$(function() {
	$('textarea').trumbowyg({
		btns: ['viewHTML',
		  '|', 'btnGrp-design',
		  '|', 'link',
		  '|', 'btnGrp-justify',
		  '|', 'btnGrp-lists'],
		fullscreenable: false
	});
});
</script>
<style type="text/css">.trumbowyg-box{ padding-top:50px; }</style>
<?php
	$cpoid = $_GET['cpoid'];
	try {
		$stmt = $db->query("select cp.* from cancellationpolicy cp where cancellationpolicyoid = '".$cpoid."'");
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
			$r_promo = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_promo as $row){
				$hoteloid = $row['hoteloid'];
				$name = $row['name'];
				$description = $row['description'];
				$published = $row['publishedoid'];
				$termcondition= $row['termcondition'];
				$cancellationday = $row['cancellationday'];
				$typecancellation = $row['cancellationtype'];
				$night = $row['night'];
			}
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}
?>
<section class="content" id="basic-info">
	<div class="row">
        <div class="box box-form">
            <form method="post" enctype="multipart/form-data" id="data-input" class="form-box" action="<?=$base_url?>/cancellationpolicy/edit-process">
            <input type="hidden" name="hoteloid" value="<?php echo $hoteloid; ?>">
            <input type="hidden" name="cpoid" value="<?php echo $cpoid; ?>">
            <ul class="inline form-input">
                <li>
                    <div class="side-left"><label>Cancellation Policy Name</label></div>
                    <div class="side-right"><input type="text" class="input-text" name="name" value="<?php echo $name; ?>"></div>
                    <div class="clear"></div>
                </li>
                <li>
                    <div class="side-left"><label>Cancellation Policy Description</label></div>
                    <div class="side-right"><textarea name="description" style="height:100px"><?php echo $description; ?></textarea></div>
                    <div class="clear"></div>
                </li>
                <li>
                    <div class="side-left"><label>Terms &amp; Condition</label></div>
                    <div class="side-right">
                        <select name="termcondition" class="input-select">
                        <?php
                            $termscon=array('free cancellation', 'non refundable');
                            foreach($termscon as $tc){
                                if($tc == $termcondition){ $selected = "selected"; }else{ $selected=""; }
                                echo"<option value='".$tc."' ".$selected.">".$tc."</option>";
                            }
                        ?>        	
                        </select>
                    </div>
                    <div class="clear"></div>
                </li>
                <li>
                    <div class="side-left"><label>Cancellation Day</label></div>
                    <div class="side-right">
                        <input type="number" class="input-text" name="cancellationday" min="0" value="<?=$cancellationday?>" />
                    </div>
                    <div class="clear"></div>
                </li>
                <li>
                    <div class="side-left"><label>Type Cancellation</label></div>
                    <div class="side-right">
                    <?php if($typecancellation == "night"){ $checked = 'checked'; }else{ $checked = ''; } ?>
                        <input type="radio" name="typecancellation" value="night" <?=$checked?> /> <input type="number" class="input-text small" name="night" value="<?=$night?>" min="0" /> night charge<br /> 
                    <?php if($typecancellation == "full amount"){ $checked = 'checked'; }else{ $checked = ''; } ?>
                        <input type="radio" name="typecancellation" value="full amount" <?=$checked?>  /> full amount<br /> 
                    </div>
                    <div class="clear"></div>
                </li>
                <li>
                    <div class="side-left"><label>Publish Cancellation Policy</label></div>
                    <div class="side-right">
                        <select name="published" class="input-select">
                        <?php
                            try {
                                $stmt = $db->query("select * from published where showthis = 'y'");
                                $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                foreach($r_room as $row){
                                    if($row['publishedoid'] == $published){ $selected = "selected"; }else{ $selected=""; }
                                    echo"<option value='".$row['publishedoid']."' ".$selected.">".$row['note']."</option>";
                                }
                            }catch(PDOException $ex) {
                                echo "Invalid Query";
                                die();
                            }
                        ?>        	
                        </select>
                    </div>
                    <div class="clear"></div>
                </li>
                <li>
                    <div class="side-left">&nbsp;</div>
                    <div class="side-right"><button type="submit" class="submit">Save</button>&nbsp;<button class="pure-button red cancel" type="button">Cancel</button></div>
                    <div class="clear"></div>
                </li>
                <div class="clear"></div>
            </ul>
            </form>
   		</div>
    </div>
</section>
