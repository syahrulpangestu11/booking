<?php
	$name = (isset($_POST['name'])) ? $_POST['name'] : "";

	$stay = (isset($_POST['stay'])) ? $_POST['stay'] : "0";

	$headline = $_POST['headline'];
	$description = $_POST['description'];
	$servicefacilities = $_POST['servicefacilities'];
	$termcondition = $_POST['termcondition'];

	$deposit = $_POST['deposit'];
	$depositvalue = (isset($_POST['depositvalue'])) ? $_POST['depositvalue'] : 0;

	$allowedExtension = array('jpg', 'jpeg', 'png');
	$tmp = explode('.', $_FILES['image']['name']);
	$extension = end($tmp);
	$image = '';
	if(!empty($_FILES['image'])){
		if(in_array($extension, $allowedExtension)){
			$imagename = str_replace(' ', '_', $_FILES['image']['name']);
			$imagename = str_replace('.'.$extension, '', $imagename);
			$ext = '.'.strtolower($extension);
			$image_new_name = substr($imagename, 0, 10).date('Ymd_His').$ext;
			$folder_destination = 'pict/packageimage/';

			move_uploaded_file($_FILES['image']['tmp_name'], $upload_base.$folder_destination.$image_new_name);
			$image = $web_url.'/'.$folder_destination.$image_new_name;
		}else{
			echo'<script>alert("Data image not saved.\\nInvalid file.")</script>';
		}
	}

	$min_ci = $_POST['min_ci'];
	$max_ci = $_POST['max_ci'];

	$published = $_POST['published'];

	try {
		$stmt = $db->prepare("insert into package_template (name, stay, publishedoid, headline, description, servicefacilities, termcondition, depositoid, depositvalue, packageimage, min_bef_ci, max_bef_ci)
		values (:c,:d,:t,:v,:w,:x,:y,:z,:aa,:ab,:ac,:ad)");
		$stmt->execute(array(':c' => $name, ':d' => $stay, ':t' => $published , ':v' => $headline, ':w' => $description, ':x' => $servicefacilities, ':y' => $termcondition, ':z' => $deposit, ':aa' => $depositvalue, ':ab' => $image, ':ac' => $min_ci, ':ad' => $max_ci));
		$packageoid = $db->lastInsertId();
	}catch(PDOException $ex) {
		echo "Invalid Query";
		print($ex);
		die();
	}

	$type = 'package';
	foreach ($_POST['icon'] as $key => $value) {
		$iconoid = $value;
		try {
			$stmt = $db->prepare("insert into termheadlinepackage_template (type, id, termheadlineoid) values (:a,:b,:c)");
			$stmt->execute(array(':a' => $type, ':b' => $packageoid, ':c' => $iconoid));
		}catch(PDOException $ex) {
			echo "Invalid Query";
			print($ex);
			die();
		}
	}

	header("Location: ". $base_url ."/package-template");
?>
