<?php
	session_start();
	include("../../../conf/connection.php");

	// Hotel Code
	$hcode_today = date("ymd");

	$s_hc = "select h.hotelcode FROM hotel h WHERE h.hotelcode LIKE '".$hcode_today."%'";
	$q_hc = $db->query($s_hc);
	$n_hc = $q_hc->rowCount();
	// $r_hc = $q_hc->fetchAll(PDO::FETCH_ASSOC);

	$urutan_new = $n_hc+1;
	// $id_new = sprintf("%03d", $urutan_new);
	$id_new = str_pad($urutan_new, 3, "0", STR_PAD_LEFT);
	$hcode_new = $hcode_today.$id_new;

	// -------------------- end of Hotel Code -------------------------------

	$name = $_POST['name'];
	$chain = $_POST['chain'];
	$star = $_POST['star'];
	$type = $_POST['type'];
	$totalroom = $_POST['totalroom'];
	$address = $_POST['address'];
	$latitude = $_POST['latitude'];
	$longitude = $_POST['longitude'];
	$phone = $_POST['phone'];
	$website = $_POST['website'];
	$email = $_POST['email'];
	$published = $_POST['published'];
	$city = $_POST['city'];
	$hotelstatus = $_POST['hotelstatus'];
	$hotelsubscribe = (empty($_POST['hotelsubscribe'])?'':$_POST['hotelsubscribe']);

	try {
		$stmt = $db->prepare("INSERT INTO hotel (hotelcode,hotelname,stars,hoteltypeoid,totalroom,address,latitude,longitude,phone,website,email,publishedoid, cityoid, chainoid, hotelstatusoid, createdby, created, updatedby, updated, hotelsubscribeoid,
			pthotel,
			npwp,
			contractnumber
		) VALUES (:hc, :a, :b, :c, :d, :e, :f, :g, :h, :i, :j, :k, :l, :m, :n, :a1, :a2, :a3, :a4, :a5,
			:pthotel,
			:npwp,
			:contractnumber
		)");
		$stmt->execute(array(':hc' => $hcode_new, ':a' => $name, ':b' => $star, ':c' => $type, ':d' => $totalroom, ':e' => $address, ':f' => $latitude, ':g' => $longitude, ':h' => $phone, ':i' => $website, ':j' => $email, ':k' => $published, ':l' => $city, ':m' => $chain, ':n' => $hotelstatus, ':a1' => $_SESSION['_user'], ':a2' => date('Y-m-d H:i:s'), ':a3' => $_SESSION['_user'], ':a4' => date('Y-m-d H:i:s'), ':a5' => $hotelsubscribe,
			':pthotel' => $_POST['pthotel'],
			':npwp' => $_POST['npwp'],
			':contractnumber' => $_POST['contractnumber']
		));
		$affected_rows = $stmt->rowCount();
		$hoteloid = $db->lastInsertId();

		/*
		* commission
		*/

		$tarr = array('base','private_sales','override');

		if(isset($_POST['comm_new_startdate']) and count($_POST['comm_new_startdate'])>0){
			foreach($_POST['comm_new_startdate'] as $key => $val){
				if(in_array($_POST['comm_new_type'][$key], $tarr)){
					$stmt = $db->prepare("INSERT INTO `hotelcommission`(`hoteloid`, `startdate`, `enddate`, `value`, `priority`, `type`, `typecomm`) VALUES (:a, :b, :c, :d, :e, :f, :g)");
					$stmt->execute(array(':a' => $hoteloid, ':b' => date('Y-m-d',strtotime($_POST['comm_new_startdate'][$key])), ':c' => date('Y-m-d',strtotime($_POST['comm_new_enddate'][$key])), ':d' => $_POST['comm_new_value'][$key], ':e' => $_POST['comm_new_priority'][$key], ':f' => $_POST['comm_new_type'][$key], ':g' => $_POST['typecomm'][$key]));
				}
			}
		}

		/*
		* feature
		*/

		if(isset($_POST['foid']) and count($_POST['foid']) > 0){
			foreach($_POST['foid'] as $key =>  $value){
				$featureoid = $value;
				$commission = $_POST['comm-'.$featureoid];

				$stmt = $db->prepare("INSERT INTO hotelfeature (hoteloid, featureoid, commission) VALUES (:a, :b, :c)");
				$stmt->execute(array(':a' => $hoteloid, ':b' => $featureoid, ':c' => $commission));
			}
		}

	}catch(PDOException $ex) {
		echo "0";
		die();
	}
	echo "1";
?>
