
<style>
    span.label{color:#333;font-size:100%;}
    .form-box select.long { width: 350px; }
</style>
<section class="content-header">
    <h1>
        Hotel
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-laptop"></i> Hotel</a></li>
        <li class="active">Add</li>
    </ol>
</section>
<section class="content">
    <form class="form-box" method="post" enctype="multipart/form-data" id="data-input" action="#">

	<div class="row">
        <div class="box box-form">
            <input type="hidden" name="dfltstate" value="2">
            <input type="hidden" name="dfltcity" value="0">
            <h3><i class="fa fa-building-o"></i> HOTEL PROFILE</h3>
            <!-- <input type="text" name="hotelcode" value=""> -->
            <ul class="block">
                <li>
                    <span class="label">Property Name</span>
                    <input type="text" class="long" name="name">
				</li>
                <li>
                    <span class="label">Chain</span>
                    <select name="chain" class="long">
                    <?php
                    try {
                    $stmt = $db->query("select * from chain where publishedoid = '1' order by name");
                    $r_chain = $stmt->fetchAll(PDO::FETCH_ASSOC);
                    foreach($r_chain as $row){
                      if($row['name'] == "No Chain"){ $selected = "selected"; }else{ $selected = ""; }
                      echo"<option value='".$row['chainoid']."' ".$selected.">".$row['name']."</option>"; }
                    }catch(PDOException $ex) {
                    echo "Invalid Query"; die();
                    }
                    ?>
                    </select>
				</li>
                <li>
                    <span class="label">Star Rating</span>
                    <input type="number" class="small" name="star" min="0" max="7">
                </li>
                <li>
                    <span class="label">Property Type</span>
                    <select name="type" class="input-select">
                    <?php
                    try {
                        $stmt = $db->query("select * from hoteltype");
                        $r_hoteltype = $stmt->fetchAll(PDO::FETCH_ASSOC);
                        foreach($r_hoteltype as $row){
                    ?>
                        <option value="<?php echo $row['hoteltypeoid']; ?>"><?php echo $row['category']; ?></option>
                    <?php
                        }
                    }catch(PDOException $ex) {
                        echo "Invalid Query";
                        die();
                    }
                    ?>
                    </select>
                </li>
                <li>
                    <span class="label">Number of Rooms</span>
                    <input type="number" class="small" name="totalroom" min="0">
                </li>
                <li>
                    <span class="label">PT Name</span>
                    <input type="text" class="long" name="pthotel">
                </li>
                <li>
                    <span class="label">N.P.W.P.</span>
                    <input type="text" class="long" name="npwp">
                </li>
                <li>
                    <span class="label">Contract Number</span>
                    <input type="text" class="long" name="contractnumber">
				</li>
            </ul>

            <h3><i class="fa fa-map-marker"></i>  HOTEL LOCATION</h3>
            <ul class="block">
                <li>
                    <span class="label">Street Address</span>
                    <textarea name="address"></textarea>
				</li>
                <li>
                    <span class="label float-left" style="vertical-align:middle">&nbsp;</span>
                    <div class="side-left">
                        <ul class="inline-block">
                            <li>
                            	Country<br />
                                <select name="country" class="input-select">
									<?php getCountry(11); ?>
                                </select>
                            </li>
                            <li class="loc-state"></li>
                            <li class="loc-city"></li>
                        </ul>
                    </div>
                    <div class="clear"></div>
                </li>
                <li>
                    <span class="label float-left" style="vertical-align:middle">Maps  Location</span>
                    <div class="side-left">
                        <ul class="inline-block">
                            <li>Latitude<br /><input type="text" class="medium" name="latitude"></li>
                            <li>Longitude<br /><input type="text" class="medium" name="longitude"></li>
                        </ul>
                    </div>
                    <div class="clear"></div>
				</li>
            </ul>

            <h3><i class="fa fa-phone"></i> HOTEL CONTACT</h3>
            <ul class="block">
                <li>
                    <span class="label">Main Phone Number</span>
                    <input type="text" class="medium" name="phone">
				</li>
                <li>
                    <span class="label">Property Website</span>
                    <input type="text" class="long" name="website">
				</li>
                <li>
                    <span class="label">Property Email</span>
                    <input type="text" class="long" name="email">
				</li>
            </ul>

            <h3><i class="fa fa-cog"></i>  HOTEL STATUS</h3>
            <ul class="block">
                <li>
                    <span class="label"><b>IBE Status</b></span>
                    <select name="hotelstatus">
                    <?php getStatusIBE(10); ?>
                    </select>
                </li>
                <li>
                    <span class="label"><b>Publish Property</b></span>
                    <select name="published">
                    <?php getPublished(0); ?>
                    </select>
                </li>
                <!-- <li>
                    <span class="label"><b>Subscribe Type</b></span>
                    <select name="hotelsubscribe">
                    <?php //getSubscribe(0); ?>
                    </select>
                </li> -->
            </ul>


            <!-- <h3>HOTEL COMMISSION SCHEME</h3>
            <table class="table table-fill table-fill-centered" id="data-commission">
                <tr>
                    <td>Scheme</td>
                    <td>Start Date</td>
                    <td>End Date</td>
                    <td>Commission Value</td>
                    <td>Priority</td>
                    <td>Type</td>
                    <td>Action</td>
                </tr>
                <tr>
                    <td>
                      <select name="newtypecomm">
                        <option value="1">Commission % per materialize booking</option>
                        <option value="2">Monthly Subscribe Fee</option>
                        <option value="3">Commission amount per materialize booking</option>
                        <option value="4">Annual Subscribe Fee</option>
                      </select>
                    </td>
                    <td><input type="text" class="medium startdate" name="newstartdate"/></td>
                    <td><input type="text" class="medium enddate" name="newenddate"/></td>
                    <td><input type="text" class="medium" name="newvalue"/></td>
                    <td><input type="text" class="medium" name="newpriority"/></td>
                    <td><select class="medium" name="newtype">
                        <option value="override">Override</option>
                        <option value="base" <?php echo $dis;?> >Base Commission</option>
                    </select></td>
                    <td><button type="button" class="small-button blue add-commission">Add</button></td>
                </tr>
            </table>

            <h3><i class="fa fa-check-square-o"></i> HOTEL FEATURE</h3>
            <table class="table table-fill">
            <tbody>
            <tr>
                <td>Subscribe</td>
                <td>Feature</td>
                <td>Commission (%)</td>
            </tr>
            <?php
            // $s_hotel_feature = "SELECT f.* from `feature` f inner join `published` p using (`publishedoid`) where f.`publishedoid` = '1'";
			// $stmt = $db->query($s_hotel_feature);
			// $r_hotel_feature = $stmt->fetchAll(PDO::FETCH_ASSOC);
			// foreach($r_hotel_feature as $key => $row){
			?>
            <tr class="list">
                <td><input type="checkbox" name="foid[]" value="<?php //echo $row['featureoid']?>" /></td>
                <td><?php //echo $row['name']?></td>
                <td><input type="number" name="comm-<?php //echo row['featureoid']?>" value="5" min="0" /></td>
            </tr>
            <?php
			// }
			?>
            </tbody>
            </table> -->
            <div class="form-input">
				<button type="button" class="pure-button red cancel-edit">Cancel</button>
				<button type="button" class="submit-add">Save</button></div>
    		</div>
    </div>

    </form>
</section>
