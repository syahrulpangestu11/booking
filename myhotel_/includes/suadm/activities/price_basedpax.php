<?php
	error_reporting(E_ALL & ~E_NOTICE);
	include("../../../conf/connection.php");
?>
<table class="table table-fill table-fill-centered basedpax">
    <tr>
        <td>From (pax)</td>
		<td>Until (pax)</td>
        <td>Price for per pcs</td>
        <td>Price for adult</td>
        <td>Price for children</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
    	<td colspan="5">&nbsp;</td>
        <td><button type="button" class="small-button blue add basedpax">Add New Rate</button></td>
    </tr>
    <?php
	if(!empty($_POST['io'])){
		$itemoid	= $_POST['io'];
		$multiprice	= 1;
		try {
			$stmt = $db->query("select *, currency.currencycode from multiprice inner join currency using (currencyoid) where typeid='item' and idreference='".$itemoid."' and multipricetypeoid='".$multiprice."'");
			$row_count = $stmt->rowCount();
			if($row_count > 0) {
				$r_mp = $stmt->fetchAll(PDO::FETCH_ASSOC);
				foreach($r_mp as $row){
					$mpoi		= $row['multipriceoid'];
					$priceqty	= $row['rateprice'];
					$priceadult	= $row['rateadult'];
					$pricechild	= $row['ratechild'];
					$currency	= $row['currencyoid'];
					$min		= $row['min'];
					$max		= $row['max'];
	?>
    <tr>
		<td>
			<input type="hidden" class="medium" name="mpoi[]"  min="1" value="<?php echo $mpoi; ?>">
        	<input type="number" class="small" name="min[]"  min="1" value="<?php echo $min; ?>">
		</td>
        <td><input type="number" class="small" name="max[]" value="<?php echo $max; ?>"></td>
        <td><input type="text" class="medium" name="priceqty[]" value="<?php echo $priceqty; ?>"></td>
        <td><input type="text" class="medium" name="priceadult[]" value="<?php echo $priceadult; ?>"></td>
        <td><input type="text" class="medium" name="pricechild[]" value="<?php echo $pricechild; ?>"></td>
        <td><button type="button" class="trash delete" pid="<?php echo $mpoi; ?>">Delete</button></td>
    </tr>
	<?php
				}
			}
		}catch(PDOException $ex) {
			echo "Invalid Query";
			die();
		}
	}
	?>
	<tr>
    	<td><input type="number" class="small" name="i_min[]" min="1"></td>
        <td><input type="number" class="small" name="i_max[]" min="1"></td>
        <td><input type="text" class="medium" name="i_priceqty[]"></td>
        <td><input type="text" class="medium" name="i_priceadult[]"></td>
        <td><input type="text" class="medium" name="i_pricechild[]"></td>
        <td><button type="button" class="trash delete">Delete</button></td>
	</tr>
</table>