<button type="button" class="small-button blue add-button">Create New Hotel</button>
<ul class="block">
	<li><span class="status-green square">sts</span> Active &nbsp;&nbsp;&nbsp; <span class="status-black square">sts</span> Inactive &nbsp;&nbsp;&nbsp; <span class="status-red square">sts</span> Expired</li>
</ul>

<?php
	error_reporting(E_ALL & ~E_NOTICE);
	include("../../../conf/connection.php");
	
			
	$main_query = "select i.itemoid, i.itemname, company, c.cityname, s.statename, i.publishedoid, (case when i.publishedoid = 2 then 'status-red' when i.publishedoid = 1 then 'status-green' else 'status-black' end) as status from item i inner join published using (publishedoid) inner join city c using (cityoid) inner join state s using (stateoid) inner join country ct using (countryoid)";

	$filter = array();
	if(isset($_REQUEST['name']) and $_REQUEST['name']!=''){
		array_push($filter, 'i.hotelname like "%'.$_REQUEST['name'].'%"');
	}
	if(isset($_REQUEST['country']) and $_REQUEST['country']!=''){
		array_push($filter, 'ct.countryoid = "'.$_REQUEST['country'].'"');
	}
	
	if(count($filter) > 0){
		$combine_filter = implode(' and ',$filter);
		$query = $main_query.' where '.$combine_filter;
	}else{
		$query = $main_query;
	}

	try {
		$stmt = $db->query($query);
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
?>
<table class="table promo-table">
<?php
		$r_hotel = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_hotel as $row){
				$itemoid	= $row['itemoid'];
				$itemname	= $row['itemname'];
				$cityname	= $row['cityname'];
				$statename = $row['statename'];
?>
    <tr>
        <td class="<?php echo $row['status']; ?>"><?php echo $itemname; ?></td>
        <td><?php echo $statename." &rarr; ".$cityname; ?></td>
        <td><?php echo $type; ?></td>
        <td class="algn-right">
        <button type="button" class="pencil edit-button" hoid="<?php echo $itemoid; ?>">Edit</button>
        </td>
    </tr>	
<?php				
			}
?>
</table>
<?php
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		print($ex);
		die();
	}
?>
