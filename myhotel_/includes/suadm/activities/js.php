<script type="text/javascript">
$(function(){
	
	function submitDataCallback(url, forminput){
		$.ajax({
			url: url,
			type: 'post',
			data: forminput.serialize(),
			success: function(data) {
				if(data == "1"){
					$dialogNotice.html("Data succesfully updated");
					$dialogNotice.dialog("open");
				}else{
					$dialogNotice.html("We&rsquo;re very sorry, we can't save your data right now"+data);
					$dialogNotice.dialog("open");
				}
			}
		});
	}
	
	
	var $dialogNotice = $('<div id="dialog-notice"></div>')
	.dialog({
		autoOpen: false,
		title: 'Notification',
		buttons: { 
			"Ok": function(){ 
				$( this ).dialog( "close" );
				document.location.href = "<?php echo $base_url; ?>/activities";
			}
		}
	});
	
/*-------------------------------------------------------------------------------------------------*/
	
	$('body').on('click','button.add-button', function(e) {
		url = "<?php echo $base_url; ?>/activities/add";
      	$(location).attr("href", url);
	});
	
	$('body').on('click','button.edit-button', function(e) {
		hoid = $(this).attr("hoid");
		url = "<?php echo $base_url; ?>/activities/edit/?ho="+hoid;
		$(location).attr("href", url);
	});
	
	$('button.submit-add').click(function(){
		url = '<?php echo $base_url; ?>/includes/suadm/activities/add-hotel-save.php';
		forminput = $('form#data-input');
		submitDataCallback(url, forminput);
	});
	
	$('button.submit-edit').click(function(){
		url = '<?php echo $base_url; ?>/includes/suadm/activities/edit-hotel-save.php';
		forminput = $('form#data-input');
		submitDataCallback(url, forminput);
	});
	

/*-------------------------------------------------------------------------------------------------*/
	$('body').on('change','select[name=country]', function(e) {
		$.get("<?php echo $base_url; ?>/includes/suadm/hotel/function-ajax.php", { 
			func: "stateRqst",
			country: $('select[name=country]').val(),
			state: $('input[type=hidden][name=dfltstate]').val()
      	}, function(response){
			$('.loc-state').html(unescape(response));
			$('select[name=state]').change();
      	});	
	});

	$('body').on('change','select[name=state]', function(e) {
		$.get("<?php echo $base_url; ?>/includes/suadm/hotel/function-ajax.php", { 
			func: "cityRqst",
			state: $('select[name=state]').val(),
			city: $('input[type=hidden][name=dfltcity]').val()
      	}, function(response){
			$('.loc-city').html(unescape(response));
      	});	
	});
	
/*-------------------------------------------------------------------------------------------------*/
	
	$('body').on('click','button.delete-button', function(e) {
		id = $(this).attr("hoid");
		$dialogDelete.data("url", "<?php echo $base_url; ?>/includes/suadm/activities/delete-hotel.php");
		$dialogDelete.data("id", id);
		$dialogDelete.dialog("open");
	});
	
/*-------------------------------------------------------------------------------------------------*/
	
	$('body').on('change','input[type=radio][name=multiprice]', function(e) {
		multiprice	= $(this).val();
		io			= $(this).attr('io');
		
		var targetbox	= $(this).parent().parent().parent().children('li').eq(2);
		
		if(multiprice == '1'){
			url_target = '<?php echo $base_url; ?>/includes/suadm/activities/price_basedpax.php';
		}else if(multiprice == '2'){
			url_target = '<?php echo $base_url; ?>/includes/suadm/activities/price_basedbundling.php';
		}else{
			url_target = '<?php echo $base_url; ?>/includes/suadm/activities/price_single.php';
		}

		$.ajax({
			url: url_target,
			type: 'post',
			data: {io : io },
			success: function(response){
				
				targetbox.html(response);
			}
		});
	});
/*-------------------------------------------------------------------------------------------------*/

	$(document).ready(function(){ 
		getLoadData();
		$('select[name=country]').change();
		$('input[type=radio][name=multiprice]:checked').change();
	});
	
/*-------------------------------------------------------------------------------------------------*/
	
	function getLoadData(){
		$.ajax({
			url: "<?php echo"$base_url"; ?>/includes/suadm/activities/data.php",
			type: 'post',
			data: $('form#data-input').serialize(),
			success: function(data) {
				$("#data-box").html(data)
			}
		});
	}
	
/*-------------------------------------------------------------------------------------------------*/
	
	function deleteAction(url, id){
		$.ajax({
			url: url,
			type: 'post',
			data: { id : id},
			success: function(data) {
				if(data == "1"){
					$dialogNotification.html("Data has been deleted");
					$dialogNotification.dialog("open");
				}else{
					$dialogNotification.html("We&rsquo;re very sorry, we can't save your data right now");
					$dialogNotification.dialog("open");
				}
			}
		});
	}
   
   
   var $dialogDelete = $('<div id="dialog-del"></div>')
	.html('Are you sure you want to delete this data?')
	.dialog({
		autoOpen: false,
		title: 'Confirmation',
		buttons: { 
			"Delete": function(){ 
				var url = $(this).data('url'); 
				var id = $(this).data('id'); 
				$(this).dialog("close"); 
				deleteAction(url, id);
			},
			Cancel: function(){ $( this ).dialog("close"); }
		}
	});
	
	var $dialogNotification = $('<div id="dialog-notice"></div>')
	.dialog({
		autoOpen: false,
		title: 'Notification',
		buttons: { 
			"Ok": function(){ 
				var callback = '<?php echo $base_url; ?>/activities'; 
				$(this).dialog( "close" );
				$(location).attr("href", callback); 
			}
		}
	});
	
/*-------------------------------------------------------------------------------------------------*/
	
	$('body').on('click','button.add.basedpax', function(e){
		table		= $('table.table-fill');
		
		appendElmt	= '<tr><td><input type="number" class="small" name="i_min[]" min="1"></td><td><input type="number" class="small" name="i_max[]" min="1"></td><td><input type="text" class="medium" name="i_priceqty[]"></td><td><input type="text" class="medium" name="i_priceadult[]"></td><td><input type="text" class="medium" name="i_pricechild[]"></td><td><button type="button" class="trash delete">Delete</button></td></tr>';
		
		table.append(appendElmt);	
	});
	
	$('body').on('click','button.add.basedbundling', function(e){
		table		= $('table.table-fill');
		
		appendElmt	= '<tr><td><input type="number" class="small" name="i_min[]" min="1"></td><td><input type="number" class="small" name="i_max[]" min="1"></td><td><input type="text" class="medium" name="i_priceqty[]"></td><td><button type="button" class="trash delete">Delete</button></td></tr>';
		
		table.append(appendElmt);	
	});
	
	$('body').on('click','button.trash', function(e){
		$(this).parent().parent().remove();
	});
			
});
</script>