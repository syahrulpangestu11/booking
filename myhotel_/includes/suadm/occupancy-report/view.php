<?php
	include('includes/bootstrap.php'); 
	include('includes/reports/function/function-report.php');
	include('includes/class/reporting1.php');
	
	$report = new Reporting1($db);
?>
<!-- Pagination -->
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css">
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>

<?php
    if(isset($_GET['year'])){
    	$year = $_GET['year'];
    }else{
    	$year = date('Y');
    }
    
    if(!isset($_GET['chain'])){
        $_GET['chain'] = '52';
    }
?>

<section class="content-header">
    <h1>Occupancy Report</h1>
    <ol class="breadcrumb">
    	<li>Report</li>
        <li class="active">Occupancy Report</li>
    </ol>
</section>
<section class="content">
    <div id="dashboard">
    
        <div class="box">
            <div class="box-body box-form">
                <form method="GET" class="form-inline" action="<?php echo $base_url.'/alloccupancyreport/'?>">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <select name="chain" class="form-control">
                                <option value="">All Chain</option>
                            <?php
                            $stmt = $db->prepare("SELECT chainoid, chain.name FROM hotel INNER JOIN chain USING(chainoid) WHERE hotel.publishedoid=1 AND hotelstatusoid=1 GROUP BY chainoid");
                        	$stmt->execute(array());
                            $r_hotels = $stmt->fetchAll(PDO::FETCH_ASSOC);
                            foreach($r_hotels as $hotelx){
                                if(isset($_GET['chain']) && $_GET['chain']==$hotelx['chainoid']){ $selected="selected"; }else{ $selected=""; }
                                echo '<option value="'.$hotelx['chainoid'].'" '.$selected.'>'.$hotelx['name'].'</option>';
                            }
                            ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <select name="year" class="form-control">
                            	<?php 
                            	for($i = date('Y'); $i >= 2017; $i--){ 
                                    if(isset($_GET['year']) && $_GET['year']==$i){ $selected="selected"; }else{ $selected=""; }
                                	echo '<option value="'.$i.'" '.$selected.'>'.$i.'</option>';
                                } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Show</button>
                        </div>
                    </div>
            	</div>
                </form>
            </div>
        </div>
        
        <div class="box box-primary">
            <div class="box-body">
            	<div class="row" style="margin-bottom:10px;text-align:right;">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <button type="button" class="btn btn-primary" id="btprint">Export to Excel</button>
                        </div>
                    </div>
                </div>
            	<div class="row">
                    <table id="commissionReportTable" cellspacing="0" class="table table-striped">
                        <thead>
                            <tr>
                                <th rowspan="2">Hotel</th>
                                <th rowspan="2">Chain</th>
                            	<?php
                                    for($m=1; $m<=12; $m++){
								        $periode1 = $year.'-'.$m.'-01'; 
                                        echo '<th colspan="2">'.date('M Y', strtotime($periode1)).'</th>';
                                    }
                                ?>
                                <th colspan="2">Total</th>
                            </tr>
                            <tr>
                            	<?php
                                    for($m=1; $m<=12; $m++){
                                        echo '
                                            <th>Comm</th>
                                            <th>RN</th>
                                        ';
                                    }
                                ?>
                                <th>Comm</th>
                                <th>RN</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        $add = "";
                        if(isset($_GET['chain']) && !empty($_GET['chain'])){
            		        $add .= " AND chainoid='".intval($_GET['chain'])."'";
            		    }
                        
                        $stmt = $db->prepare("SELECT hoteloid, hotelname, chain.name FROM hotel INNER JOIN chain USING(chainoid) WHERE hotel.publishedoid=1 AND hotelstatusoid=1 ".$add." 
                            ORDER BY chainoid, hotelname ");
                    	$stmt->execute(array());
                        $r_hotels = $stmt->fetchAll(PDO::FETCH_ASSOC);
                        
                        $no=$num+1;
                        foreach($r_hotels as $hotelx){
                            $hoteloid = $hotelx['hoteloid'];
                            $hotelname = $hotelx['hotelname'];
                            $chainname = $hotelx['name'];
                            
                            $report->setHotel($hoteloid);
                        ?>
                            <tr link="">
                                <td><?=$hotelname?></td>
                                <td><?=$chainname?></td>
                        	<?php
                        	$stotal_commission = 0;
                        	$stotal_roomnight = 0;
                            for($m=1; $m<=12; $m++){
								$periode1 = $year.'-'.$m.'-01'; 
								$periode2 = date("Y-m-t",strtotime($periode1));
								
								$report->setPeriode($periode1, $periode2);
								
								//$total_confirmed = $report->materializeBookingTotal('grandtotal', 4);
								//$total_cancelled = $report->materializeBookingTotal('cancellationamount', array(5,7,8));
								//$total_revenue = $total_confirmed + $total_cancelled;
								$total_commission = $report->materializeBookingCommission();
								$total_roomnight = $report->materializeRoomNight();
								
								$stotal_commission += $total_commission;
                        	    $stotal_roomnight += $total_roomnight;
							?>
                                    <td class="text-right"><?=number_format($total_commission/1000)?></td>
                                    <td class="text-right"><?=number_format($total_roomnight)?></td>
                            <?php
							}
							?>
                                <td class="text-right"><?=number_format($stotal_commission/1000)?></td>
                                <td class="text-right"><?=number_format($stotal_roomnight)?></td>
                            </tr>
                        <?php
                            $no++;
                        }
                        ?>
                        </tbody>
					</table>
				</div>
			</div>
		</div>
        
	</div>
</section>

<script type="text/javascript">
$( function() {
	$('#commissionReportTable').DataTable({
		"aaSorting": [
		  [26, "dsc"],
		],
		"paging": true,
		"autoWidth": false
	});
	$('#btprint').click(function(){
	    <?php
	    $add = array();
	    if(isset($_GET['chain'])){
	        $add[] = "chain=".$_GET['chain'];
	    }
	    if(isset($_GET['year'])){
	        $add[] = "year=".$_GET['year'];
	    }
	    if(count($add) > 0){
	        $sadd = "?".implode("&", $add);
	    }else{
	        $sadd = "";
	    }
	    $url = $base_url . '/includes/suadm/occupancy-report/print.php'.$sadd;
	    ?>
	    location.href = "<?=$url?>";
	});
});
</script>
<style type="text/css">
table.dataTable{ font-size:0.9em }
table.dataTable tbody > tr:hover{ cursor:pointer; background-color:#e2ebf3 }
.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
    padding: 4px;
}
table.dataTable thead>tr>th.sorting_asc, table.dataTable thead>tr>th.sorting_desc, table.dataTable thead>tr>th.sorting, table.dataTable thead>tr>td.sorting_asc, table.dataTable thead>tr>td.sorting_desc, table.dataTable thead>tr>td.sorting {
    padding-right: 8px;
}
table.dataTable thead .sorting:after, table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:after, table.dataTable thead .sorting_desc:after, table.dataTable thead .sorting_asc_disabled:after, table.dataTable thead .sorting_desc_disabled:after {
    opacity: 0.021;
}
</style>