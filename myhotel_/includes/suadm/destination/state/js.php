<script type="text/javascript">
$(function(){
	$(document).ready(function(){ getLoadData(); });

	function getLoadData(){
		$.ajax({
			url: "<?php echo"$base_url"; ?>/includes/suadm/destination/state/data.php",
			type: 'post',
			data: $('form#data-input').serialize(),
			success: function(data) {
				$("#data-box").html(data)
			}
		});
	}

	$('body').on('click','button.add', function(e) {
		url = '<?php echo $base_url; ?>/includes/suadm/destination/state/add-state.php';
		forminput = $('form#form-add');
		submitData(url, forminput);
	});

	function submitData(url, forminput){
		$.ajax({
			url: url,
			type: 'post',
			data: forminput.serialize(),
			success: function(data) {
				if(data == "1"){
					$dialogNotice.html("Data succesfully updated");
					$dialogNotice.dialog("open");
				}else{
					$dialogNotice.html("We&rsquo;re very sorry, we can't save your data right now");
					$dialogNotice.dialog("open");
				}
			}
		});
	}

	var $dialogNotice = $('<div id="dialog-notice"></div>')
    	.dialog({
    		autoOpen: false,
    		title: 'Notification',
			buttons: {
				"Ok": function(){
					$( this ).dialog( "close" );
					$(location).attr("href", "<?php echo $base_url; ?>/state");
				}
			}
    	});

	var loadingBar = $('<div class="loader">Loading...</div>');

	//--------- cancel add/change picture
	$('body').on('click','button.btn-cancel-pict', function(e) {
			e.preventDefault();
			$(this).parents('td').children('.form-state-pict').css('display','none');
			$(this).parents('td').children('.state-pict').css('display','block');
	});

	//--------- open add/change picture
	$('body').on('click','button.state-pict', function(e) {
		e.preventDefault();
			$('.form-state-pict').css('display','none');
			$('.state-pict').css('display','block');
			$(this).parent().children('.form-state-pict').css('display','block');
			$(this).parent().children('.state-pict').css('display','none');
	});

	//--------- validation and preview image

	$('body').on('change','input.input-image', function(e) {
		readURL(this);
	});
	function readURL(input) {
		var $sts = true;
		var $box = $('#preview_' + $(input).attr('id'));
		$box.empty();

		//--- validation image extension
		var $ext = input.files[0].name.split('.').pop().toLowerCase();
		if($.inArray($ext, ['gif','png','jpg','jpeg']) == -1) {
			$sts=false;
			$(input).val('');
			$box.append('<label>Uploaded file is not a valid image. Only JPG, PNG and GIF files are allowed.</label>');
			$box.find('label').css('color','red');
		}else{
			$box.find('label').css('color','black');
		}

		//--- validation image size
		if($sts){
			var size = input.files[0].size/1024/1024;
			var n = size.toFixed(2);
			if(size>1){
				$sts = false;
				$(input).val('');
				$box.append('<label>Your file size is: ' + n + 'MB, and it is too large to upload! Maximum : 1MB or less.</label>');
				$box.find('label').css('color','red');
			}else{
				$box.find('label').css('color','black');
			}
		}

		//--- preview image
		if($sts){
		  if (input.files && input.files[0]) {
		    var reader = new FileReader();
		    reader.onload = function(e) { $box.append('<img src="'+e.target.result+'" />') }
		    reader.readAsDataURL(input.files[0]);
		  }
		}
	}
});
</script>
