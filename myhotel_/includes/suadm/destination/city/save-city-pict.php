<?php

  include("../../../../conf/connection.php");
  $countryoid = $_POST['country'];
  $stateoid = $_POST['state'];
  $cityoid = $_POST['cityoid'];
  $city = $_POST['city'];
  $oldpict = $_POST['oldpict'];;

?>
<script type="text/javascript">
$(function(){
   $("#dialog-error, #dialog-success").dialog({
      autoOpen: false,
	  buttons: { "Ok": redirect },
	  close: function(event, ui) { redirect }
   });

   function redirect(){
	   $(location).attr("href", "<?php echo $base_url; ?>/city/?country=<?=$countryoid;?>&state=<?=$stateoid;?>&name=<?='';//$city;?>");
   }
});

function openDialog($type,$message,$warning){
  var $dialog = $("#dialog-"+$type);
  $dialog.find(".description").empty();
  if($message!=undefined){ $dialog.find(".description").text($message); }
  if($warning!=undefined){ console.log($warning); }

  $(function(){ $( document ).ready(function(){ $dialog.dialog("open"); }); });
}
</script>

<div id="dialog-error" title="Error Notification">
  <p>We&rsquo;re very sorry, we can't save your data right now.<br />Please make sure you upload image file type (ex: .jpg , .png , .gif)</p><p class="description"></p>
</div>
<div id="dialog-success" title="Confirmation Notice">
  <p>Data succesfully updated</p><p class="description"></p>
</div>
<?php


	function logJS($data,$title=""){
		if(!empty($title)){
			echo '<script>console.log("'.$title.' : ");</script>';
		}
		echo '<script>console.log('.json_encode($data).');</script>';
	}

  function getFileName($name1,$extension1){
    $filename1='';
    if($name1!=''){
      $filename1 = strtolower($name1);
  		$filename1 = str_replace(" ", "_", $filename1);
  		$filename1 = str_replace(".".$extension1, "", $filename1);
  		$filename1 = substr($filename1, 0, 20);
    }
    return $filename1;
  }
  function checkExistAndType($filename1,$filetype1,$extension1){
    $sts = false;
    if($filename1!=''){
        $allowedExts = array("jpg", "jpeg", "gif", "png");
        if((    ($filetype1 == "image/gif")
          || ($filetype1 == "image/jpeg")
          || ($filetype1 == "image/png")
          || ($filetype1 == "image/pjpeg")
          )  && in_array(strtolower($extension1), $allowedExts)){
            $sts=true;
        }
    }else{
      $sts=true;
    }
    return $sts;
  }
  function checkExistAndError($filename1,$fileerror1){
    return ($filename1!='' && $fileerror1 > 0 ? true : false);
  }


  	if((!empty($_FILES["photos"]["name"]) or $_FILES["photos"]["name"]!='')){

      //variable for image logo
      $fileerror = $_FILES["photos"]["error"] ;
      $filetype = $_FILES["photos"]["type"] ;
      $filename_ori = $_FILES["photos"]["name"];
      $tmp = explode(".", $filename_ori);
      $extension = end($tmp);
  		$ext = ".".strtolower($extension);
  		$filename = getFileName($filename_ori,$extension);
  		$uploadedname = "city_".$filename."-".date("Y_m_d_H_i_s");

  		if (checkExistAndType($filename,$filetype,$extension)){
  			if (checkExistAndError($filename,$fileerror)){
    			?>
      			<script>openDialog("error"); </script>
          <?php
  			}else{
  				include"compress-image.php"; //function for upload and compress image
          // $show_thumbnail_path = ($filename!=""?$web_url."/".$folder.$imagename_t:$oldphotos);
          $show_thumbnail_path = ($filename!=""?$web_url."/".$folder.$imagename:$oldphotos);
  				try {
              $stmt = $db->prepare("UPDATE city SET citypict = :a WHERE cityoid = :id");
              $stmt->execute(array(':a' => $show_thumbnail_path, ':id' => $cityoid));
  		      ?>
      			   <script>openDialog("success"); </script>
            <?php
  				}catch(PDOException $ex) {
  				?>
      			<script>openDialog("error","",<?=json_encode($ex->getMessage());?>); </script>
          <?php
  				}

  			}
  		}else{
  		?>
  			<script>openDialog("error"); </script>
  		<?php
  		}

  	}else{
      try {
          $stmt = $db->prepare("UPDATE city SET citypict = :a WHERE cityoid = :id");
          $stmt->execute(array(':a' => '', ':id' => $cityoid));
    		?>
           <script>openDialog("success"); </script>
    		<?php
  		}catch(PDOException $ex) {
    		?>
          <script>openDialog("error","",<?=json_encode($ex->getMessage());?>); </script>
    		<?php
  		}
  	}

 ?>
