<?php
	error_reporting(E_ALL & ~E_NOTICE);
	include("../../../../conf/connection.php");

	$base_url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://';
	$base_url .= $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']);
	//http://localhost/en/myhotel/includes/suadm/destination/city
	$base_url = preg_replace('/\/(\w+)\/(\w+)\/(\w+)\/(\w+)\z/', '', $base_url);
	//http://localhost/en/myhotel

?>
<style>
.input-image{
    border: 0;
    box-shadow: none;
    width: 110px;
		margin:0 auto;
}
.city-pict{margin:0 auto;}

.box-input-image img{height: 100%; max-width: 100%;}
.box-input-image label{}
.box-input-image{
	width: 100%;
	height: 185px;
	line-height: 163px;
	border: 1px solid #ccc;
	margin: 5px 0;
	text-align: center;
	padding: 10px;
	background: #f8f8f8;
}
</style>
<table class="table table-fill table-fill-centered">
    <tr>
        <td>No.</td>
        <td>Country</td>
        <td>State</td>
        <td>City Name</td>
        <td>&nbsp;</td>
    </tr>
    <tr class="autofilled">
     	<td>&nbsp;</td>
        <td>
		<select name="citycountry">
            <?php
                try {
                    $stmt = $db->query("select * from country");
                    $r_continent = $stmt->fetchAll(PDO::FETCH_ASSOC);
                    foreach($r_continent as $row){
						if($row['countryoid'] == $_REQUEST['country']){ $selected = "selected"; }else{ $selected=""; }
						echo"<option value='".$row['countryoid']."' ".$selected.">".$row['countryname']."</option>";
                    }
                }catch(PDOException $ex) {
                    echo "Invalid Query";
                    die();
                }

            ?>
            </select>
        </td>
        <td><div class="loc-state"></div></td>
        <td><input type="text" name="city" class="medium" placeholder="Create new city"></td>
        <td><button type="button" class="blue-button add">Add</button></td>
    </tr>
<?php
	$name = $_REQUEST['name'];

	$main_query = "select ct.citypict, ct.cityoid, ct.cityname, s.statename, s.stateoid, c.countryname, c.countryoid, ct.cityname from city ct inner join state s using (stateoid) inner join country c using (countryoid)";
	$filter = array();
	if(isset($_REQUEST['name']) and $_REQUEST['name']!=''){
		array_push($filter, 'ct.cityname like "%'.$_REQUEST['name'].'%"');
	}
	if(isset($_REQUEST['country']) and $_REQUEST['country']!=''){
		array_push($filter, 'c.countryoid = "'.$_REQUEST['country'].'"');
	}
	if(isset($_REQUEST['state']) and $_REQUEST['state']!=''){
		array_push($filter, 'st.stateoid = "'.$_REQUEST['state'].'"');
	}
	if(count($filter) > 0){
		$combine_filter = implode(' and ',$filter);
		$query = $main_query.' where '.$combine_filter;
	}else{
		$query = $main_query;
	}
	try {
		$stmt = $db->query($query." order by c.countryname, s.statename, ct.cityname");
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
		$r_country = $stmt->fetchAll(PDO::FETCH_ASSOC);
			$i = 0;
			foreach($r_country as $row){
				$i++;
			?>
            <tr class="list">
            	<td><?=$i?></td>
                <td style="text-align:left"><?=$row['countryname']?></td>
                <td style="text-align:left"><?=$row['statename']?></td>
                <td style="text-align:left"><?=$row['cityname']?></td>
                <td>
										<form method="post" enctype="multipart/form-data" id="form-pict" action="<?=$base_url;?>/city/save-picture" class="form-city-pict" style="display:none;">
											<div id="preview_city-pict-<?=$row['cityoid']?>" class="box-input-image ">
												<?php if(empty($row['citypict'])){ ?>
																<label>No Picture</label>
												<?php }else{ ?>
																<img src="<?=$row['citypict'];?>" />
												<?php } ?>
											</div>
											<input type="hidden" name="cityoid" value="<?php echo $row['cityoid']; ?>">
											<input type="hidden" name="country" value="<?php echo $row['countryoid']; ?>">
											<input type="hidden" name="state" value="<?php echo $row['stateoid']; ?>">
											<input type="hidden" name="city" value="<?php echo $row['cityname']; ?>">
											<input type="hidden" name="oldpict" value="<?php echo $row['citypict']; ?>">
											<input type="file" class="input-image" id="city-pict-<?=$row['cityoid']?>" name="photos" value="<?php echo $row['citypict']; ?>">
											<button type="submit" class="btn btn-primary inline-block">Save</button>
											<button type="button" class="btn btn-cancel-pict btn-secondary inline-block">Cancel</button>
										</form>
										<button type="button" class="btn city-pict btn-<?=(empty($row['citypict'])?'warning':'danger')?>"><?=(empty($row['citypict'])?'Add':'Show')?> Picture</button>
								</td>
            </tr>
            <?php
			}
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		print($ex);
		die();
	}
?>
</table>
