<section class="content-header">
    <h1>Destination : City</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Destination</a></li>
        <li class="active">City</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="box">
            <div class="box-body">
                <div class="form-group">
				<form method="get" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/city/">
                    <label>Country</label> &nbsp;&nbsp;
                    <select name="country">
                    	<option value="">- show all country -</option>
                    <?php
                        try {
                            $stmt = $db->query("select * from country");
                            $r_continent = $stmt->fetchAll(PDO::FETCH_ASSOC);
                            foreach($r_continent as $row){
								if($row['countryoid'] == $_REQUEST['country']){ $selected = "selected"; }else{ $selected=""; }
                                echo"<option value='".$row['countryoid']."' ".$selected.">".$row['countryname']."</option>";
                            }
                        }catch(PDOException $ex) {
                            echo "Invalid Query";
                            die();
                        }

                    ?>
                    </select>
                    <label>State</label> &nbsp;
                    <span class="loc-state"><select name="state"></select></span>
                    <label>City</label> &nbsp;&nbsp;
                    <input type="text" name="name" value="<?=$_GET['name']?>">
                    <button type="submit" class="blue-button">Find</button>
                </form>
                </div>
            </div><!-- /.box-body -->
       </div>
    </div>
    <div class="row">
        <div class="box">
        	<form method="post" enctype="multipart/form-data" id="form-add">
                <div id="data-box" class="box-body">
                    <div class="loader">Loading...</div>
                </div><!-- /.box-body -->
            </form>
       </div>
    </div>
</section>
