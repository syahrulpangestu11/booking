<?php
	include("../../../conf/connection.php");

	$name = (isset($_POST['name'])) ? $_POST['name'] : "";
	$description = (isset($_POST['description'])) ? $_POST['description'] : "";
	$logourl = (isset($_POST['logourl'])) ? $_POST['logourl'] : "";
	$published = (isset($_POST['published'])) ? $_POST['published'] : "";
	
	try {
		$stmt = $db->prepare("INSERT INTO `chain`(`logo`, `name`, `description`, `publishedoid`) values (:a,:b,:c,:d)");
		$stmt->execute(array(':a' => $logourl ,':b' => $name ,':c' => $description ,':d' => $published));
		$promotionoid = $db->lastInsertId();
	}catch(PDOException $ex) {
		echo "0";
		die();
	}
	echo "1";
?>

