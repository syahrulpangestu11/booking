<?php
	include("../../../conf/connection.php");

	$cpoid = $_POST['cpoid'];
	$logourl = (isset($_POST['logourl'])) ? $_POST['logourl'] : "";
	$name = (isset($_POST['name'])) ? $_POST['name'] : "";
	$description = (isset($_POST['description'])) ? $_POST['description'] : "";
	$published = (isset($_POST['published'])) ? $_POST['published'] : "";
		
	try {
		$stmt = $db->prepare("update chain set logo = :a, name = :b, description = :c, publishedoid = :d where chainoid = :e");
		$stmt->execute(array(':a' => $logourl ,':b' => $name ,':c' => $description ,':d' => $published ,':e' => $cpoid));
	}catch(PDOException $ex) {
		echo "0".$ex->getMessage();
		die();
	}
	echo "1";
?>

