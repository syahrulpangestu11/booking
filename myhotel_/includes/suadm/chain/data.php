<button type="button" class="blue-button add">Create New Chain</button>
<style>.tmphide{display:none}</style>
<?php
	include("../../../conf/connection.php");
	include("../../paging/pre-paging.php");
	
	$main_query = "SELECT cp.*, (case when cp.publishedoid = 0 then 'status-red' when cp.publishedoid = 1 then 'status-green' else 'status-black' end) as status FROM `chain` cp inner join `published` p using (`publishedoid`)";
	$filter = array();
	if(isset($_REQUEST['name']) and $_REQUEST['name']!=''){
		array_push($filter, 'cp.name like "%'.$_REQUEST['name'].'%"');
	}
	array_push($filter, 'p.publishedoid not in (3)');
	if(count($filter) > 0){
		$combine_filter = implode(' and ',$filter);
		$query = $main_query.' where '.$combine_filter;
	}else{
		$query = $main_query;
	}
	try {
		$stmt = $db->query($query.$paging_query);
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
?>
<table class="table promo-table">
    <tr>
        <td>Chain</td>
        <td>Description</td>
        <td>&nbsp;</td>
    </tr>	
<?php
			$r_promo = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_promo as $row){
?>
    <tr>
        <td class="<?php echo $row['status']; ?>"><?php echo $row['name']; ?>
        <br /><br /><?php if(!empty($row['logo'])) echo '<img src="'.$row['logo'].'" style="max-height:50px" alt="Logo"/>';?></td>
        <td width="50%"><?php echo $row['description']; ?></td>
        <td class="algn-right">
        <button type="button" class="pencil edit" pid="<?php echo $row['chainoid']; ?>">Edit</button>
        <button type="button" class="trash delete" pid="<?php echo $row['chainoid']; ?>">Delete</button>
		<button type="button" class="small-button blue manage tmphide" hoid="<?php echo $hoteloid; ?>">Manage</button>
        </td>
    </tr>	
<?php				
			}
?>
</table>
<?php
		}
		
		$main_count_query = "select count(*) as jml FROM `chain` cp inner join `published` p using (`publishedoid`)";
		if(count($filter) > 0){
			$combine_filter = implode(' and ',$filter);
			$main_count_query = $main_count_query.' where '.$combine_filter;
		}
		$stmt = $db->query($main_count_query);
		$jmldata = $stmt->fetchColumn();	
	
		$tampildata = $row_count;
		include("../../paging/post-paging.php");	

	}catch(PDOException $ex) {
		echo "Invalid Query";
		print($ex);
		die();
	}
?>
