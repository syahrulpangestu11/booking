<?php 
$sr_allow_usr = array('1','2','5');
if(!in_array($_SESSION['_typeusr'], $sr_allow_usr)){
    $s_chain = "SELECT ua.oid, ua.type FROM users u INNER JOIN userassign ua USING (useroid) WHERE ua.type IN ('chainoid', 'hoteloid') AND u.useroid = '".$_SESSION['_oid']."'";
    $q_chain = $db->query($s_chain);
    $r_chain = $q_chain->fetchAll(PDO::FETCH_ASSOC);
    $rxoid = array(); $rxtype = array();
    foreach($r_chain as $row){
        $rxoid[] = $row['oid'];
        $rxtype[] = $row['type'];
    }

    $rxchain = array(); $rxhotel = array();
    foreach($rxtype as $k => $v){
        if($v == 'chainoid'){
            $rxchain[] = $rxoid[$k];
        }else if($v == 'hoteloid'){
            $rxhotel[] = $rxoid[$k];
        }
    }

    $msg = "";
    if(count($rxchain) > 0){
        $imp = implode(",", $rxchain);
        $msg .= "AND chainoid IN (".$imp.")";
    }
    if(count($rxhotel) > 0){
        $imp = implode(",", $rxhotel);
        $msg .= "AND hoteloid IN (".$imp.")";
    }

    $qx = array();
    if($msg != ""){
        $stmt = $db->prepare("SELECT hoteloid FROM hotel WHERE publishedoid='1' ".$msg." AND show_reminder='1' ");
        $stmt->execute(array());
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach($data as $vals){
            $qx[] = $vals['hoteloid'];
        }
    }
    if(count($qx) > 0){
?>
<script src="<?php echo"$base_url"; ?>/js/modal/jquery.modal.min.js"></script>
<link rel="stylesheet" href="<?php echo"$base_url"; ?>/js/modal/jquery.modal.min.css" />
<style>.blocker{z-index: 3000;} #blink{background-color: #ffe049;text-align: center;padding: 2px;font-weight: 600;} #blink a, #ex1 h3{color: #b81206;}</style>
<div id="blink"><a id="ex1_opener" href="#ex1" rel="modal1:open">PAYMENT ALERT REMINDER</a></div>
<div id="ex1" class="modal1">
  <h3>PAYMENT ALERT REMINDER</h3>
  <p>Dear Partner</p>
  <p>We would like to inform you that we have not received the payment of the invoice(s). Kindly note that late payment of commisions may result in reduced visibility on the website and trigger an auto-suspension.</p>
  <p>If you already made a payment, please forward us a copy of the payment proof by email at sales@thebuking.com.
  <p>Should you have any questions, please contact our Team directly.</p>
  <p>Thank you for working with TheBuking</p>
</div>
<script>$(function(){$('#ex1_opener').click();});</script>
<?php 
    }
}?>

<div class="loader-wrapper"></div>

<div id="xheader">
    <?php include "menu-buking-products.php";?>
    <!-- <div id="logo"><img src="<?=$base_url?>/images/logo.png"></div> -->
    <?php include "menu.php";?>
</div>
<div id="child-menu" class="treeview-menu">
</div>
