<section class="content-header">
    <h1>Reservation</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-user-o"></i> Reservation</a></li>
    </ol>
</section>
<section class="content">
	<?php
	include('include/function-ibeind.php');
	include('include/function-ajax.php');
	include('include/set-session.php');
	
	$startdate	= (isset($_REQUEST['startdate'])) ? date("Y-m-d", strtotime($_REQUEST['startdate'])) : date("Y-m-d",mktime(0,0,0,date("m"),date("d"),date("Y")));
	$enddate	= (isset($_REQUEST['enddate'])) ? date("Y-m-d", strtotime($_REQUEST['enddate'])) : date("Y-m-d",strtotime($startdate.' +3 day'));
	$night = differenceDate($startdate, $enddate);
	$hoteloid = $_POST['hotel'];
	?>
    <div class="box">
		<form class="form-inline" method="post" action="<?=$base_url?>/booking">
    	<div class="row">
            <div class="form-group">
                <label>Check In</label>
                <div class="input-group"><div class="input-group-addon"><i class="fa fa-calendar"></i></div><input type="text" class="form-control" name="startdate" id="startdate" required value="<?=$startdate?>"></div>
            </div>
            <div class="form-group">
                <label>Check Out</label>
                <div class="input-group"><div class="input-group-addon"><i class="fa fa-calendar"></i></div><input type="text" class="form-control" name="enddate" id="enddate" required value="<?=$enddate?>"></div>
            </div>
            <div class="form-group">
                <label>Select Hotel</label>
                <select class="form-control" name="hotel">
                <?php
					$s_agent_hotel = "select h.hoteloid, h.hotelcode, h.hotelname from hotel h inner join agenthotel ah using (hoteloid) inner join agent a using (agentoid) where h.publishedoid = '1' and a.agentoid = '".$_SESSION['_oid']."'";
					$stmt = $db->query($s_agent_hotel);
					$r_agent_hotel = $stmt->fetchAll(PDO::FETCH_ASSOC);
					foreach($r_agent_hotel as $hotel){
						if($hotel['hoteloid'] == $hoteloid){ $selected = 'selected = "selected"'; }else{ $selected = ''; } 
				?>
                	<option value="<?=$hotel['hoteloid']?>" <?=$selected?>><?=$hotel['hotelname']?></option>
                <?php	
					}
				?>
                </select>
            </div>
            <div class="form-group">
            	<button type="submit" name="checkavailability" class="btn btn-danger">Check Availability</button>
            </div>
        </div>
        </form>
    </div>
    <div class="box">
    	<div class="row">
        	<div class="col-md-8">
				<?php if(isset($_POST['hotel'])){ include('include/check-availability.php');  }else{ ?> 
                <div class="panel panel-warning">
                	<div class="panel-heading"><i class="fa fa-search"></i> Choose your stay first.</div>
                    <div class="panel-body">Please choose your stay with form check availability above <i class="fa fa-hand-o-up"></i></div>
				</div>
                <?php } ?>
            </div>
            <div class="col-md-4">
            	<form method="post" action="<?=$base_url?>/booking/payment">
                <input type="hidden" name="hoteloid" value="<?=$hoteloid?>" />
                <div id="box-summary-reservation">
                    <div class="row" id="title"><div class="col-md-12"><h4>Reservation Summary</h4></div></div>
                    <div class="row"><div class="col-md-12"><ul class="block" id="breakdown-total"></ul></div></div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-xs-6"><h5>Grand Total</h5></div>
                                <div class="col-xs-6"><h5 id="grandtotal">0</h5></div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6"><h5>Number of Room</h5></div>
                                <div class="col-xs-6"><h5 id="numberroom">0 room(s)</h5></div>
                            </div>
                        </div>
                        <div class="col-md-12"><button type="submit" class="btn btn-primary" disabled="disabled">BOOK NOW</button></div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</section>