<?php
	$s_data_hotel = "select hotelcode from hotel h where hoteloid = '".$hoteloid."'";
	$stmt = $db->query($s_data_hotel);
	$r_data_hotel = $stmt->fetch(PDO::FETCH_ASSOC);
	$hcode = $r_data_hotel['hotelcode'];
	$agenthoteloid = $_SESSION['_oid'];
	
	$xml =  simplexml_load_file("data-xml/".$hcode.".xml");
	
	$channeloid = 1;
	$currencyoid = 1;
		$s_currency = "select currencycode from currency where currencyoid = '".$currencyoid."'";
		$stmt	= $db->query($s_currency);
		$r_currency = $stmt->fetch(PDO::FETCH_ASSOC);
	$currency = $r_currency['currencycode'];
		
	$listroom = array();
	

	$s_room = "select ro.*, r.roomoid, r.name as masterroom, r.hoteloid from roomoffer ro inner join published p using (publishedoid) inner join room r using (roomoid) inner join offertype using (offertypeoid) where r.hoteloid = '".$hoteloid."' and p.publishedoid = '1'";
	$stmt = $db->query($s_room);
	$q_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
	foreach($q_room as $room){
		$available = true;
		$breakdownrate = array();
		$roomtotal = 0; $roomwithsurcharge = 0;
		for($n=0; $n<$night; $n++){
			$allotment = 0;
			
			$date = date("Y-m-d",strtotime($startdate." +".$n." day"));
			
			$query_tag = '//hotel[@id="'.$room['hoteloid'].'"]/masterroom[@id="'.$room['roomoid'].'"]/rate[@date="'.$date.'"]/allotment/channel[@type="'.$channeloid.'"]/text()';
			foreach($xml->xpath($query_tag) as $tagallotment){
				$allotment = (int)$tagallotment[0];
			}
			
			if($allotment > 0){
				$syntax_bar = "select rate, surcharge from agenthotelrate where agenthoteloid = '".$agenthoteloid."' and roomofferoid = '".$room['roomofferoid']."' and '".$date."' >= startdate and '".$date."' <= enddate order by priority limit 1";
				$stmt = $db->query($syntax_bar);
				$row_bar = $stmt->fetchAll(PDO::FETCH_ASSOC);
				
				foreach($row_bar as $row){
					$rate = $row['rate'];
					$surcharge = $row['surcharge'];
				}
				
				if($rate > 0){
					$roomtotal = $roomtotal + $rate;
					$totalrate = $rate + $surcharge;
					$roomwithsurcharge = $roomwithsurcharge + $totalrate;
					array_push($breakdownrate, $totalrate);
				}else{
					$available = false;
					$break;
				}
				
			}else{
				$available = false;
				break;
			}
		}
		
		if($available == true){
			$s_picture = "SELECT photourl FROM hotelphoto hp WHERE hp.hoteloid = '".$room['hoteloid']."' AND hp.flag = 'main' AND ref_table = 'room' AND ref_id = '".$room['roomoid']."'";
			$stmt	= $db->query($s_picture);
			$picture = $stmt->fetch(PDO::FETCH_ASSOC);
			
			$dataroom = array($room['roomoid'], $room['masterroom'], $room['roomofferoid'], $room['name'], $picture['photourl'], $breakdownrate, $roomwithsurcharge);
			array_push($listroom, $dataroom);
		}
	}
	
	if(count($listroom) > 0){
		foreach($listroom as $room){
?>
    <div class="list-room row">
        <div class="col-md-3"><div class="room-image"><img class="img-responsive" src="<?=$room[4]?>" alt=""></div></div>
        <div class="col-md-5"><h2><?=$room[3]?></h2></div>
        <div class="col-md-4 text-right">Total <?=$currency?> <?=number_format($room[6])?> for <?=$night?> night(s)<br>avg <?=$currency?> <?=number_format($room[6]/$night)?> /night<br>
        <div><button type="button" name="select-room" class="btn btn-success select-room" prm="<?=$startdate?> <?=$enddate?> <?=$hoteloid?> <?=$room[0]?> <?=$room[2]?>">Select Room</button></div>
        </div>
    </div>
<?php
		}
	}
?>