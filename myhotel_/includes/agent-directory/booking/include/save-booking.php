<?php
	$s_hoteloid = "select h.hoteloid, h.hotelcode, ah.agenthoteloid, a.agentname, a.email from hotel h left join agenthotel ah using (hoteloid) inner join agent a using (agentoid) where h.hoteloid = '".$_POST['hoteloid']."' and ah.agentoid = '".$_SESSION['_oid']."'";
	$stmt	= $db->query($s_hoteloid);
	$hotel = $stmt->fetch(PDO::FETCH_ASSOC);
		$hoteloid = $hotel['hoteloid'];
		$hotelcode = $hotel['hotelcode'];
		$agenthoteloid = $hotel['agenthoteloid'];
		
	/*-------------------------*/
	$agent_email	= $hotel['email'];
	$agent_name		= $hotel['agentname'];
	/*-------------------------*/

	$path_xml_hotel = "../myhotel/data-xml/".$hotelcode.".xml";
	$xml = new DomDocument();
		$xml->preserveWhitespace = false;
		$xml->load($path_xml_hotel);
	$xpath = new DomXpath($xml);

	$root = $xml->documentElement;
	
	$xml_file =  simplexml_load_file($path_xml_hotel);
	
	$email		= $_POST['email'];
	$title		= $_POST['title'];
	$firstname	= $_POST['firstname'];
	$lastname	= $_POST['lastname'];
	$city		= $_POST['city'];
	$country	= $_POST['country'];
	$phone		= $_POST['phone'];
	$request	= $_POST['request'];
	
	$bookingtime = date("Y-m-d H:i:s");
	$timelimit = date("Y-m-d H:i:s", strtotime($bookingtime. '+48 hour'));
	$bookingstatus = 4;
	$promocode = 'thebuking';
	$pc_commission = 0;
	$point = 0;
	$agentoid = $_SESSION['_oid'];
	
	$user_ip = $_SERVER['REMOTE_ADDR'];
	$user_browser = $_SERVER['HTTP_USER_AGENT'];
	
	$bn_firstformat = date('Ymd');
	$same = true;
	$syntax_bookingnumber = "select count(bookingoid) as numberofbooking from booking bm where date(bookingtime) = '".$today."'";
	$stmt = $db->query($syntax_bookingnumber);
	$row_getID = $stmt->fetchAll(PDO::FETCH_ASSOC);
	foreach($row_getID as $row){
		$lastelement = $row['numberofbooking'];
	}
	do{
		$lastelement++;
		$numbering = sprintf("%05d", $lastelement);
		$bookingnumber = $bn_firstformat.$numbering;
		
		$syntax_bookingnumber = "select bookingoid from booking bm where bm.bookingnumber = '".$bookingnumber."'";
		$stmt	= $db->query($syntax_bookingnumber);
		$row_count = $stmt->rowCount();
		if($row_count == 0){ 
			$same = false; 
		}else{
			$same = true;
		}
	}while($same == true);
	
	function randomPin($len){
		$sets='0123456789'; $setLen=strlen($sets)-1; $s='';
		for($p=1;$p<=$len;$p++){ $s.=$sets{mt_rand(0,$setLen)}; } return $s;
	}
	$pin = randomPin(4);
		
	/*-------------------------*/
	$guest_email	= $email;
	$guest_name		= $title.' '.$firstname.' '.$lastname;	
	/*-------------------------*/
	
	$stmt = $db->prepare("INSERT INTO `customer` (`title`, `firstname`, `lastname`, `email`, `city`, `countryoid`, `phone`) VALUES (:a , :b , :c , :d , :e , :f, :g)");
	$stmt->execute(array(':a' => $title, ':b' => $firstname, ':c' => $lastname, ':d' => $email, ':e' => $city , ':f' => $country , ':g' => $phone));
	$customeroid = $db->lastInsertId();
		
	/*---------------------------------------------------------------------------------------------*/
			
	$session_transaction = "select count(bookingtempoid) as numberroom, sum(total) as total, sum(deposit) as deposit, sum(balance) as balance, c.currencyoid, (select value from hotelcommission where hoteloid = '".$hoteloid."' and ( (startdate <= '".$today."' and enddate >= '".$today."') or (startdate <= '0000-00-00' and enddate >= '0000-00-00')) limit 1) as commission from bookingtemp inner join currency c using (currencyoid) where session_id = '".$_SESSION['tokenSession']."'";
	$stmt	= $db->query($session_transaction);
	$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
	foreach($result as $row){
		
		if(empty($row['commission']) or $row['commission'] == 0){
			$commission = 15;
		}else{
			$commission = $row['commission'];
		}
		
		$grandtotal = $row['total'];
		$deposit = $row['deposit'];
		$balance = $row['balance'];
			$hotelcollect = $row['total'] * (100 - $commission) / 100 ;
			$gbhcollect = $row['total'] * $commission / 100 ;
		$currencyoid = $row['currencyoid'];
		
		$numberroom = $row['numberroom'];
	}
	
	
	if(empty($currencyoid)){ $currencyoid = 1; }

	$stmt = $db->prepare("INSERT INTO `booking` 
(`hoteloid`, `bookingnumber`, `custoid`, `bookingtime`, `bookingtimelimit`,`bookingstatusoid`, `note`, `user_ip`, `user_browser`, `grandtotal`, `currencyoid`, `hotelcollect`, `gbhcollect`, `gbhpercentage`, `pin`, `deposit`, `balance`, `promocode`, `promocodecomm`, `grandtotalr`, `agentoid`, `point`) VALUES 
(:hid, :a , :b , :c , :d , :e , :f, :g, :h, :i, :j, :k, :l, :m, :n, :o, :p, :q, :r, :aa, :ab, :ac)");
	$stmt->execute(array(':hid' => $hoteloid, ':a' => $bookingnumber, ':b' => $customeroid, ':c' => $bookingtime, ':d' => $timelimit, ':e' => $bookingstatus, ':f' => $request, ':g' => $user_ip, ':h' => $user_browser, ':d' => $timelimit, ':e' => $bookingstatus, ':f' => $request, ':g' => $user_ip, ':h' => $user_browser, ':i' => $grandtotal, ':j' => $currencyoid, ':k' => $hotelcollect, ':l' => $gbhcollect, ':m' => $commission, ':n' => $pin, ':o' => $deposit, ':p' => $balance, ':q' => $promocode, ':r' => $pc_commission, ':aa' => $grandtotal, ':ab' => $agentoid, ':ac' => $point));
	$bookingoid = $db->lastInsertId();
	 
	/*---------------------------------------------------------------------------------------------*/

	$query_booking_temporer = "select bt.*, r.roomoid, r.hoteloid from bookingtemp bt inner join roomoffer ro using (roomofferoid) inner join room r using (roomoid) where session_id ='".$_SESSION['tokenSession']."'";
	$stmt	= $db->query($query_booking_temporer);
	$fetch_booking_temporer = $stmt->fetchAll(PDO::FETCH_ASSOC);
	foreach($fetch_booking_temporer as $temp){
		
		$stmt = $db->prepare("INSERT INTO `bookingroom` 
	(`bookingoid`, `roomofferoid`, `channeloid`, `promotionoid`, `checkin`, `checkout`, `night`, `adult`, `child`, `roomtotal`, `extrabedtotal`, `total`, `currencyoid`, `breakfast`, `extrabed`, `deposit`, `balance`, `hotel`, `room`, `promotion`, `cancellationpolicyoid`, `cancellationname`, `termcondition`, `cancellationday`, `cancellationtype`, `cancellationtypenight`, `roomtotalr`, `totalr`, `checkoutr`) VALUES (:a , :b , :c , :d , :e , :f, :g, :h, :i, :j, :k, :l, :m, :n, :o, :p, :q, :r, :s, :t, :aa, :ab, :ac, :ad, :ae, :af, :ba, :bb, :bc)");
		$stmt->execute(array(':a' => $bookingoid, ':b' => $temp['roomofferoid'], ':c' => $temp['channeloid'], ':d' => $temp['promotionoid'], ':e' => $temp['checkin'], ':f' => $temp['checkout'], ':g' => $temp['night'], ':h' => $temp['adult'], ':i' => $temp['child'], ':j' => $temp['roomtotal'], ':k' => $temp['extrabedtotal'], ':l' => $temp['total'], ':m' => $temp['currencyoid'], ':n' => $temp['breakfast'], ':o' => $temp['extrabed'], ':p' => $temp['deposit'], ':q' => $temp['balance'], ':r' => $temp['hotel'], ':s' => $temp['room'], ':t' => $temp['promotion'], ':aa' => $temp['cancellationpolicyoid'], ':ab' => $temp['cancellationname'], ':ac' => $temp['termcondition'], ':ad' => $temp['cancellationday'], ':ae' => $temp['cancellationtype'], ':af' => $temp['cancellationtypenight'], ':ba' => $temp['roomtotal'], ':bb' => $temp['total'], ':bc' => $temp['checkout']));
		$bookingroomoid = $db->lastInsertId();
		
		$query_booking_temporer_detail = "select btd.* from bookingtempdtl btd where bookingtempoid ='".$temp['bookingtempoid']."'";
		$stmt	= $db->query($query_booking_temporer_detail);
		$fetch_booking_temporer_detail = $stmt->fetchAll(PDO::FETCH_ASSOC);
		foreach($fetch_booking_temporer_detail as $tempdtl){
			
			$stmt = $db->prepare("insert into `bookingroomdtl` (`bookingroomoid`, `date`, `rate`, `discount`, `rateafter`, `extrabed`, `total`, `currencyoid`) VALUES 
		(:a , :b , :c , :d , :e , :f, :g, :h)");
			$stmt->execute(array(':a' => $bookingroomoid, ':b' => $tempdtl['date'], ':c' => $tempdtl['rate'], ':d' => $tempdtl['discount'], ':e' => $tempdtl['rateafter'], ':f' => $tempdtl['extrabed'], ':g' => $tempdtl['total'], ':h' => $tempdtl['currencyoid']));
			
			$query_tag_allotment = '//hotel[@id="'.$temp['hoteloid'].'"]//masterroom[@id="'.$temp['roomoid'].'"]/rate[@date="'.$tempdtl['date'].'"]/allotment';
			
			$query_tag_allotment_channel = $query_tag_allotment.'/channel[@type="'.$temp['channeloid'].'"]';
			
			$query_tag_allotment_channel_value = $query_tag_allotment_channel.'/text()';
			
			foreach($xml_file->xpath($query_tag_allotment_channel_value) as $tagallotment){
				$allotment = (int)$tagallotment[0];
			}
			
			$new_allotment = $allotment - 1;
			
			$check_tag_allotment = $xpath->query($query_tag_allotment);
			$tag_allotment = $check_tag_allotment->item(0);
			
			$channel = $xml->createElement("channel");
				$channel_type = $xml->createAttribute("type");
				$channel_type->value = $temp['channeloid'];
			$channel->appendChild($channel_type);
				$channel_allotment = $xml->createTextNode($new_allotment);
			$channel->appendChild($channel_allotment);
			
			$check_tag_allotment_channel = $xpath->query($query_tag_allotment_channel);
			
			if($check_tag_channel->length == 0){
				$tag_allotment->appendChild($channel);
			}else{
				$tag_allotment_channel = $check_tag_allotment_channel->item(0);
				$tag_allotment->replaceChild($channel,$tag_allotment_channel);
			}
			
		}
		
		$granddeposit +=  $temp['deposit'];
		$grandbalance +=  $temp['balance'];
	}
	
	$remove_session = "delete bookingtemp, bookingtempdtl from bookingtemp, bookingtempdtl where bookingtemp.bookingtempoid = bookingtempdtl.bookingtempoid and bookingtemp.session_id = '".$_SESSION['tokenSession']."'";
	$stmt	= $db->query($remove_session);
	/*---------------------------------------------------------------------------------------------*/
	
	$cc_type = $_POST['cardtype'];
	$cc_number = $_POST['cardnumber'];
	$cc_holder = $_POST['cardholder'];
	$cc_exp_month = $_POST['expmonth'];
	$cc_exp_year = $_POST['expyear'];
	$cc_csc = $_POST['csc'];
	$payment_type = $_POST['payment_type'];
	
	if($payment_type == "cc"){

		$stmt = $db->prepare("select cardoid from creditcard where cardcode = :a");
		$stmt->execute(array(':a' => $cc_type));
		$card = $stmt->fetch(PDO::FETCH_ASSOC);
		
		$stmt = $db->prepare("insert into bookingpayment (`bookingoid`, `type`, `cardoid`, `cardnumber`, `cardholder`, `expmonth`, `expyear`, `cvc`) values (:a , :b , :c , :d , :e , :f, :g, :h)");
		$stmt->execute(array(':a' => $bookingoid, ':b' => $payment_type, ':c' => $card['cardoid'], ':d' => $cc_number, ':e' => $cc_holder, ':f' => $cc_exp_month, ':g' => $cc_exp_year, ':h' => $cc_csc));
		$paymentoid = $db->lastInsertId();
	}else if($payment_type == "creditfacility"){
		$stmt = $db->prepare("insert into bookingpayment (`bookingoid`, `type`) values (:a, :b)");
		$stmt->execute(array(':a' => $bookingoid, ':b' => $payment_type));
		$paymentoid = $db->lastInsertId();
		
		$stmt = $db->prepare("insert into agenthotelcreditfacility (`agenthoteloid`, `date`, `type`, `amount`) values (:a , :b, :c, :d)");
		$stmt->execute(array(':a' => $agenthoteloid, ':b' => date('Y-m-d H:i:s'), ':c' => 'debit', ':d' => $grandtotal));

		$stmt = $db->prepare("update agenthotel set creditfacility = creditfacility - :a where agenthoteloid = :b");
		$stmt->execute(array(':a' => $grandtotal, ':b' => $agenthoteloid));
	}
		
	
	$stmt = $db->prepare("update `booking` set `grandbalance` = :a , `granddeposit` = :b where `bookingoid` = :c");
	$stmt->execute(array(':a' => $grandbalance, ':b' => $granddeposit, ':c' => $bookingoid));	
	
	$xml->formatOutput = true;
	$xml->save($path_xml_hotel) or die("Error");
?>