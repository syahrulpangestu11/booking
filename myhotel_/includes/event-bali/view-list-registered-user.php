<?php
include('connection-thebukingcom.php');

	$stmt = $dbTB->query("select count(formregisteroid) as registered from formregister");
	$registered = $stmt->fetch(PDO::FETCH_ASSOC);
	$stmt = $dbTB->query("select count(formregisteroid) as emailed from formregister where sendemail = '1'");
	$emailed = $stmt->fetch(PDO::FETCH_ASSOC);
?>
<style>
button.send-button{
    background-color: #5fbeaa;
    height: 34px;
    color: #ffffff;
    border: 1px solid transparent;
    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    border-radius: 4px;
    display: inline-block;
    font-size: 14px;
    text-rendering: auto;
    vertical-align: top;
}
</style>

<section class="content-header">
    <h1>
       	Event Bali
    </h1>
    <ol class="breadcrumb">
        <li class="active"><a href="#"><i class="fa fa-table"></i> Event - Bali</a></li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="box">
            <div id="data-box" class="box-body">
            <form method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/event-bali/send-email">
            <input type="hidden" name="id" value="" />
<h1>Event : The Büking Introduction &amp; Creativity Content Mastery</h1>
<h2><i class="fa fa-user"></i> Registered : <?php echo $registered['registered']; ?> | <i class="fa fa-envelope-o"></i> Emailed : <?php echo $emailed['emailed']; ?></h2>
<table class="table promo-table">
	<tr>
		<td>No.</td>
		<td>Registration Date</td>
		<td>Name</td>
		<td>Hotel</td>
		<td>Position Title</td>
		<td>Email</td>
        <td>Mobile</td>
        <td>Action</td>
	</tr>
	<?php
	$no = 0;
	$stmt = $dbTB->query("select * from formregister order by submitdate desc");
	$row_count = $stmt->rowCount();
	if($row_count > 0) {
		$r_registered = $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach($r_registered as $data){
	?>
	<tr>
		<td><?php echo ++$no; ?></td>
		<td><?php echo date('d/M/Y', strtotime($data['submitdate'])); ?></td>
		<td><?php echo $data['name']; ?></td>
		<td><?php echo $data['hotel']; ?></td>
		<td><?php echo $data['position_title']; ?></td>
		<td style="text-transform:lowercase"><?php echo $data['official_email']; ?></td>
        <td><?php echo $data['mobile_phone']; ?></td>
        <td>
        <?php if($data['sendemail'] == 0){ ?>
        <button type="button" class="send-button" style="background-image:none;" id="<?php echo $data['formregisteroid']; ?>"><i class="fa fa-envelope"></i> send email</button>
        <?php }else{ ?>
        <span style="color:#c30059; font-weight:bold;"><i class="fa fa-check"></i> emailed</span>
		<?php } ?>
        </td>
	</tr>
	<?php
        }
	}
	?>
</table>
            </form>
            </div><!-- /.box-body -->
       </div>
    </div>
</section>

<script type="text/javascript">
$(function(){
	$('body').on('click','button.send-button', function(e) {
		$('input[name=id]').val($(this).attr('id'));
		$('form#data-input').submit();
	});
});
</script>