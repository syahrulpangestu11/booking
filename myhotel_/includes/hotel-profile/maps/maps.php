<?php
$latitude = !empty($latitude) ? $latitude : '-8.659243';
$longitude = !empty($longitude) ? $longitude : '115.207850';
 ?>

<section class="content-header">
    <h1>
        Maps
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-laptop"></i> Settings</a></li>
        <li>Hotel Profile</li>
        <li class="active">Maps</li>
    </ol>
</section>
<section class="content" id="maps">
	<div class="row">
        <div class="box box-form">
            <div id="data-box" class="box-body">
                <h1>Maps</h1>
                <form method="post" enctype="multipart/form-data" id="data-input" action="#">
                <input type="hidden" name="hoteloid" value="<?php echo $hoteloid; ?>">
                <div class="form-input">
                    <div class="side-left">
                        <label>Latitude</label>
                        <input type="text" class="input-text" name="latitude" id="latitude" value="<?php echo $latitude; ?>">
                        <label>Longitude</label>
                        <input type="text" class="input-text" name="longitude" id="longitude" value="<?php echo $longitude; ?>">
                        <button type="button" class="submit">Save</button>
                    </div>
                    <div class="side-right">
                      <!-- <div id="location" style="width: 500px; height: 400px;"></div> -->
                      <!-- <div id="map" style="width: 500px; height: 400px;"></div> -->

                      <div id='map_canvas' style="width: 500px; height: 400px;"></div>
                      <div id="current">Drag the marker to reposition.</div>
                      <div id="current-address"></div>

                      <script>
                        function initMap() {
                  
                          var map = new google.maps.Map(document.getElementById('map_canvas'), {
                              zoom: 12,
                              center: new google.maps.LatLng(5.3290729,111.7404585),
                              mapTypeId: google.maps.MapTypeId.ROADMAP
                          });

                          var myMarker = new google.maps.Marker({
                              zoom: 12,
                              position: new google.maps.LatLng('<?php echo $latitude; ?>','<?php echo $longitude; ?>'),
                              draggable: true
                          });

                          var geocoder = new google.maps.Geocoder;
                          var infowindow = new google.maps.InfoWindow;

                          google.maps.event.addListener(myMarker, 'dragend', function (evt) {
                            var markerLat = evt.latLng.lat().toFixed(3);
                            var markerLng = evt.latLng.lng().toFixed(3);
                              document.getElementById('current').innerHTML = '<p>Marker dropped: Current Lat: ' + markerLat + ' Current Lng: ' + markerLng + '</p>';
                              document.getElementById('latitude').value = markerLat;
                              document.getElementById('longitude').value = markerLng;

                              geocodeLatLng(geocoder, map, infowindow, markerLat, markerLng, 'current-address');
                          });

                          google.maps.event.addListener(myMarker, 'dragstart', function (evt) {
                              document.getElementById('current').innerHTML = '<p>Currently dragging marker...</p>';
                          });

                          map.setCenter(myMarker.position);
                          myMarker.setMap(map);
                        }

                        function geocodeLatLng(geocoder, map, infowindow, markerLat, markerLng, targetDiv) {
                          var latlng = {lat: parseFloat(markerLat), lng: parseFloat(markerLng)};

                          geocoder.geocode({'location': latlng}, function(results, status) {
                            if (status === 'OK') {
                              if (results[1]) {
                                document.getElementById(targetDiv).innerHTML = results[1].formatted_address;
                              } else {
                                document.getElementById(targetDiv).innerHTML ='No results found';
                              }
                            } else {
                              // document.getElementById(targetDiv).innerHTML ='Geocoder failed due to: ' + status;
                            }
                          });
                        }


                      </script>
                    </div>
                    <div class="clear"></div>
                </div>
                </form>
        </div>
   		</div>
    </div>
</section>
<!--
<script type="text/javascript">
$(function(){

	$('#location').locationpicker({
		location: {latitude: <?php echo $latitude; ?>, longitude:  <?php echo $longitude; ?>},
		radius: 0,
		inputBinding: {
			latitudeInput: $('input[name=latitude]'),
			longitudeInput: $('input[name=longitude]')
		}
	});
	var marker = new google.maps.Marker({
    position: myLatlng,
    title:"Hello World!"
});

google.maps.event.addListener(map, "idle", function(){
    marker.setMap(map);
});
	/*$('#maps').on('.ui-tabs-active', function(){
		$('#location').locationpicker('autosize');
	});*/
});
</script> -->

<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBWXzEQnkj0wpzxDJYXYpYczjy1yh4GHoo&callback=initMap"></script>
