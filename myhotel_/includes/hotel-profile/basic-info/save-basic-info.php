<script type="text/javascript">
$(function(){
   $("#dialog").dialog({
      autoOpen: false,
	  buttons: { "Ok": redirect },
	  close: function(event, ui) { redirect }
   });

   function redirect(){
	   $(location).attr("href", "<?php echo $base_url; ?>/hotel-profile/basic-info");
   }
});
</script>
<?php
	$hoteloid = $_POST['hoteloid'];
	$country = $_POST['country'];
	$name = $_POST['name'];
	$state = $_POST['state'];
	$star = $_POST['star'];
	$city = $_POST['city'];
	$hoteltype = $_POST['hoteltype'];
	$phone = $_POST['phone'];
	$chain = $_POST['chain'];
	$fax = $_POST['fax'];
	$address = $_POST['address'];
	$email = $_POST['email'];
	$htl_url = $_POST['htl_url'];
	//$be_url = $_POST['be_url'];
	$website = $_POST['website'];
	$description = $_POST['description'];
	$policy = $_POST['policy'];
	$postalcode = $_POST['postalcode'];
	$starthotelprice = $_POST['starthotelprice'];

  $socialmedia_url = $_POST['socialmedia_url'];
  $fblike_script = $_POST['fblike_script'];

  	$allowedExtension = array('jpg', 'jpeg', 'png', 'ico');
	$tmp_logo = explode('.', $_FILES['favicon']['name']);
	$extension_logo = end($tmp_logo);
	$logo = '';
	if(!empty($_FILES['favicon']['name']) and isset($_FILES['favicon']['name'])){
		if(in_array($extension_logo, $allowedExtension)){
			$logoname = str_replace(' ', '_', $_FILES['favicon']['name']);
			$logoname = str_replace('.'.$extension_logo, '', $logoname);
			$ext = '.'.strtolower($extension_logo);
			$logo_new_name = $hcode.substr($logoname, 0, 10).date('Ymd_His').$ext;
			$folder_destination = 'pict/logo/';

			move_uploaded_file($_FILES['favicon']['tmp_name'], $upload_base.$folder_destination.$logo_new_name);
			$logo = $web_url.'/'.$folder_destination.$logo_new_name;

			try {
				$stmt = $db->prepare("update hotel set favicon = :a  where hoteloid = :b");
				$stmt->execute(array(':a' => $logo, ':b' => $hoteloid));
			}catch(PDOException $ex) {
				echo "Invalid Query"; //print($ex);
				die();
			}
		}
	}

	try {
		//$stmt = $db->prepare("UPDATE hotel SET hotelname=:a , hoteltypeoid=:b , stars=:c , address=:d, email=:e, cityoid=:f, chainoid=:g, phone=:h, fax=:i, website=:j, description=:k, policy=:l, htl_url=:m, be_url=:n, postalcode=:o WHERE hoteloid=:hoid");
		//$stmt->execute(array(':a' => $name, ':b' => $hoteltype, ':c' => $star, ':d' => $address, ':e' => $email, ':f' => $city, ':g' => $chain, ':h' => $phone, ':i' => $fax, ':j' => $website, ':k' => $description, ':l' => $policy, ':m' => $htl_url, ':n' => $be_url, ':o' => $postalcode, ':hoid' => $hoteloid));
		$stmt = $db->prepare("UPDATE hotel SET hotelname=:a , hoteltypeoid=:b , stars=:c , `address`=:d, email=:e, cityoid=:f, chainoid=:g, phone=:h, fax=:i, website=:j, `description`=:k, `policy`=:l, htl_url=:m, postalcode=:o, socialmedia_url=:p, fblike_script=:q, starthotelprice=:r WHERE hoteloid=:hoid");
		$stmt->execute(array(
      ':a' => $name, ':b' => $hoteltype, ':c' => $star, ':d' => $address, ':e' => $email, ':f' => $city, ':g' => $chain,
      ':h' => $phone, ':i' => $fax, ':j' => $website, ':k' => $description, ':l' => $policy,
      ':m' => $htl_url, ':o' => $postalcode, ':p' => $socialmedia_url, ':q' => $fblike_script, ':r' => $starthotelprice,
      ':hoid' => $hoteloid));
		$affected_rows = $stmt->rowCount();
	}catch(PDOException $ex) {
		echo $ex;
		die();
	}
?>
<div id="dialog" title="Confirmation Notice">
  <p>Data succesfully updated</p>
</div>
<script type="text/javascript">
$(function(){
    $( document ).ready(function() {
        $("#dialog").dialog("open");
    });
});
</script>
