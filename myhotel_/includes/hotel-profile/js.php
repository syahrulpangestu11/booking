<!-- <script type="text/javascript" src='http://maps.google.com/maps/api/js?sensor=false&libraries=places'></script> -->

<!-- <script src="<?php echo $base_url; ?>/js/locationpicker.jquery.js"></script> -->
<script type="text/javascript">
$(function(){
	$('#basic-info button.edit').click(function(){
		$('input.input-text, select.input-select, textarea').attr('disabled', false);
		$('input.input-text, select.input-select, textarea').removeClass('unbordered');
		$(this).addClass('hide');
		$('button.submit').removeClass('hide');
	});

	$('#maps button.submit').click(function(){
		url = '<?php echo $base_url; ?>/includes/hotel-profile/maps/maps-save.php';
		forminput = $('#maps form#data-input');
		submitData(url, forminput);
	});

	$('#card button.submit').click(function(){
		url = '<?php echo $base_url; ?>/includes/hotel-profile/facilities/facilities-card-save.php';
		forminput = $('#card form#data-input');
		submitData(url, forminput);
	});

	$('#hotel button.submit').click(function(){
		url = '<?php echo $base_url; ?>/includes/hotel-profile/facilities/facilities-hotel-facilities-save.php';
		forminput = $('#hotel form#data-input');
		submitData(url, forminput);
	});

	$('#sports button.submit').click(function(){
		url = '<?php echo $base_url; ?>/includes/hotel-profile/facilities/facilities-hotel-facilities-save.php';
		forminput = $('#sports form#data-input');
		submitData(url, forminput);
	});

	$('#language button.submit').click(function(){
		url = '<?php echo $base_url; ?>/includes/hotel-profile/facilities/facilities-language-save.php';
		forminput = $('#language form#data-input');
		submitData(url, forminput);
	});

	$('#info button.submit').click(function(){
		url = '<?php echo $base_url; ?>/includes/hotel-profile/facilities/facilities-useful-information-save.php';
		forminput = $('#info form#data-input');
		submitData(url, forminput);
	});


	function submitData(url, forminput){
		//alert(url+"____"+forminput.serialize());
		$.ajax({
			url: url,
			type: 'post',
			data: forminput.serialize(),
			success: function(data) {
				if(data == "1"){
					$dialogNotice.html("Data succesfully updated");
					$dialogNotice.dialog("open");
				}else{
					$dialogNotice.html("We&rsquo;re very sorry, we can't save your data right now");
					$dialogNotice.dialog("open");
				}
			}
		});
	}

	$(document).on('click','#basic-info button.submit', function(e) {
		$.ajax({
			url: '<?php echo $base_url; ?>/includes/hotel-profile/basic-info/basic-info-save.php',
			type: 'post',
			data: $('#basic-info form#data-input').serialize(),
			success: function(data) {
				$('input.input-text, select.input-select, textarea').attr('disabled', true);
				//$('input.input-text, select.input-select, textarea').addClass('unbordered');
				$('button.submit').addClass('hide');
				$('button.edit').removeClass('hide');
				if(data == "1"){
					$dialogNotice.html("Data succesfully updated");
					$dialogNotice.dialog("open");
				}else{
					$dialogNotice.html("We&rsquo;re very sorry, we can't save your data right now");
					$dialogNotice.dialog("open");
				}
			}
		});
	});

	var $dialogNotice = $('<div id="dialog-notice"></div>')
	.dialog({
		autoOpen: false,
		title: 'Notification',
		buttons: {
			"Ok": function(){ $( this ).dialog( "close" ); }
		}
	});

 	$("#facilities button:first").button({
		icons: { primary: "ui-icon-contact" }
    }).next().button({
		icons: { primary: "ui-icon-home" }
    }).next().button({
		icons: { primary: "ui-icon-volume-on" }
    }).next().button({
		icons: { primary: "ui-icon-heart" }
    }).next().button({
		icons: { primary: "ui-icon-help" }
    });

	$('#fcl-menu button').click(function(){
		var tab_id = $(this).attr('link-tab');

		$('#fcl-menu button').removeClass('current');
		$('.fcl-content').removeClass('current');

		$(this).addClass('current');
		$("#"+tab_id).addClass('current');
	})

	$('input').iCheck({
		checkboxClass: 'icheckbox_square-blue',
		radioClass: 'iradio_square-blue',
		increaseArea: '20%' // optional
	});

	$("#accor-photos").accordion();

	$('body').on('click','#photo button.add-button', function(e) {
		url = "<?php echo $base_url; ?>/hotel-profile/photo/add";
      	$(location).attr("href", url);
	});

	$('body').on('click', '#photo button.star-button', function(e){
		pid = $(this).attr("pid");
		$.ajax({
			url: '<?php echo $base_url; ?>/includes/hotel-profile/photos/set-main-photos.php',
			type: 'post',
			data: { pid : pid},
			success: function(data) {
				alert(data);
				if(data == "1"){
					$dialogDelPhotoNotif.html("Image has been set as main picture");
					$dialogDelPhotoNotif.dialog("open");
				}else{
					$dialogDelPhotoNotif.html("We&rsquo;re very sorry, we can't update your data right now");
					$dialogDelPhotoNotif.dialog("open");
				}
			}
		});
	});

	$('body').on('click', '#photo button.delete', function(e){
		pid = $(this).attr("pid");
		$dialogDelPhoto.data("pid", pid);
		$dialogDelPhoto.dialog("open");
	});

	$('body').on('click', '#photo button.main', function(e){
		pid = $(this).attr("pid");
		mainPictureProcess(pid);
	});


	var $dialogDelPhoto = $('<div id="dialog-del"></div>')
	.html('Are you sure you want to delete this data?')
	.dialog({
		autoOpen: false,
		title: 'Confirmation',
		buttons: {
			"Delete": function(){
				var pid = $(this).data('pid');
				deleteProcess(pid);
				$( this ).dialog( "close" );
			},
			Cancel: function(){
				$( this ).dialog( "close" );
			}
		}
	});

	function deleteProcess(pid){
		$.ajax({
			url: '<?php echo $base_url; ?>/includes/hotel-profile/photos/delete-photos.php',
			type: 'post',
			data: { pid : pid},
			success: function(data) {
				if(data == "1"){
					$dialogDelPhotoNotif.html("Data has been deleted");
					$dialogDelPhotoNotif.dialog("open");
				}else{
					$dialogDelPhotoNotif.html("We&rsquo;re very sorry, we can't delete your data right now");
					$dialogDelPhotoNotif.dialog("open");
				}
			}
		});
	}

	function mainPictureProcess(pid){
		$.ajax({
			url: '<?php echo $base_url; ?>/includes/hotel-profile/photos/set-main-photos.php',
			type: 'post',
			data: { pid : pid},
			success: function(data) {
				if(data == "1"){
					$dialogDelPhotoNotif.html("Picture has been set as main image");
					$dialogDelPhotoNotif.dialog("open");
				}else{
					$dialogDelPhotoNotif.html("We&rsquo;re very sorry, we can't save your setting right now");
					$dialogDelPhotoNotif.dialog("open");
				}
			}
		});
	}


	var $dialogDelPhotoNotif = $('<div id="dialog-del"></div>')
	.html('Are you sure you want to delete this data?')
	.dialog({
		autoOpen: false,
		title: 'Notification',
		buttons: {
			"Ok": function(){
				$(this).dialog( "close" );
      			$(location).attr("href", "<?php echo $base_url; ?>/hotel-profile/photo");
			}
		}
	});


	/*---------------------------------------------------------------------------------*/

	function redirectRoomView(){
		url = "<?php echo $base_url; ?>/hotel-profile/rooms";
      	$(location).attr("href", url);
	}

	$('body').on('click','#rooms button.add-button', function(e) {
		url = "<?php echo $base_url; ?>/hotel-profile/rooms/add";
      	$(location).attr("href", url);
	});

	$('body').on('click','#rooms button.edit-button', function(e) {
		ro = $(this).attr("ro");
		url = "<?php echo $base_url; ?>/hotel-profile/rooms/edit/?ro="+ro;
      	$(location).attr("href", url);
	});


	$('body').on('click','#rooms button.submit-add', function(e) {
		url = '<?php echo $base_url; ?>/includes/hotel-profile/room/add-room-save.php';
		forminput = $('#rooms form#data-input');
		submitDataCallback(url, forminput);
	});

	$('body').on('click','#rooms button.submit-edit', function(e) {
		url = '<?php echo $base_url; ?>/includes/hotel-profile/room/edit-room-save.php';
		forminput = $('#rooms form#data-input');
		submitDataCallback(url, forminput);
	});

	$('body').on('click','#rooms button.cancel', function(e){ redirectRoomView(); });

	function submitDataCallback(url, forminput){
		$.ajax({
			url: url,
			type: 'post',
			data: forminput.serialize(),
			success: function(data) {
				if(data == "1"){
					$dialogNoticeRooms.html("Data succesfully updated");
					$dialogNoticeRooms.dialog("open");
				}else{
					$dialogNoticeRooms.html("We&rsquo;re very sorry, we can't save your data right now");
					$dialogNoticeRooms.dialog("open");
				}
			}
		});
	}

	var $dialogNoticeRooms = $('<div id="dialog-notice"></div>')
	.dialog({
		autoOpen: false,
		title: 'Notification',
		buttons: {
			"Ok": function(){ $( this ).dialog( "close" );  redirectRoomView(); }
		}
	});

	/*------------------------------------------------*/
	$(document).on('click','#basic-info #data-age-policy .add-button', function(e) {
		url = '<?php echo $base_url; ?>/includes/hotel-settings/update-hotel-settings.php';
		forminput = $('form#data-age-policy');
		submitData(url, forminput);
	});

	/*------------------------------------------------*/
	$(document).on('focus',"#data-commission .startdate", function(){
		$(this).datepicker({
			//defaultDate: "+1w",
			changeMonth: true, changeYear:true,
			onClose: function( selectedDate ) {
				$( this ).parent().parent().children("td").eq(1).children("input.enddate").datepicker( "option", "minDate", selectedDate );
			}
		});
	});
	$(document).on('focus',"#data-commission .enddate", function(){
		$(this).datepicker({
			//defaultDate: "+1w",
			changeMonth: true, changeYear:true,
			onClose: function( selectedDate ) {
				$( this ).parent().parent().children("td").eq(0).children("input.startdate").datepicker( "option", "maxDate", selectedDate );
			}
		});
	});

	$('body').on('click','button.add-email', function(e) {

		tableTarget = $('table.table-fill');
		parentTable = $(this).parent().parent().parent();

		startdate = parentTable.find('input[name=newstartdate]').val();
		enddate = parentTable.find('input[name=newenddate]').val();
		value = parentTable.find('input[name=newvalue]').val();
		priority = parentTable.find('input[name=newpriority]').val();
		type = parentTable.find('select[name=newtype]').val();
		s1='';s2='';s3='';
		if(type=='override') s1='selected';
		if(type=='private_sales') s2='selected';
		if(type=='base') s3='selected';

		if(startdate.trim().length !== 0 && enddate.trim().length !== 0 && value.trim().length !== 0 && priority.trim().length !== 0 && type.trim().length !== 0){

			if($('#data-commission').attr('ds')=='disabled'){external = "<option value='private_sales' "+s2+" disabled>Private Sales</option><option value='base' "+s3+" disabled>Base Commission</option>";}else{external = "<option value='private_sales' "+s2+">Private Sales</option><option value='base' "+s3+">Base Commission</option>";}

			tableTarget.append("<tr x='data'><td><input type='text' class='medium startdate' name='new_startdate[]' value='"+startdate+"'></td><td><input type='text' class='medium enddate' name='new_enddate[]' value='"+enddate+"'></td><td><input type='text' class='medium' name='new_value[]' value='"+value+"'></td><td><input type='text' class='medium' name='new_priority[]' value='"+priority+"'></td><td><select class='medium' name='new_type[]'><option value='override' "+s1+">Override</option>"+external+"</select></td><td>&nbsp;</td></tr>");

			$(this).parent().parent().find('input').val('');
		}
	});

	$('#data-commission button.add-button').click(function(){
		var base = 0;
		$("#data-commission tr[x=data]").each(function(index, element) {
            var el = $(this);
			var xval = 0;
			if(el.children("td").eq(4).children("select").val() == 'base'){
				xval = el.children("td").eq(2).children("input").val();
				if(xval > base){
					base = xval;
				}
			}
        });

		var base_must_low = 0;
		if(base != 0){
			$("#data-commission tr[x=data]").each(function(index, element) {
				var el = $(this);
				var xval = 0;
				if(el.children("td").eq(4).children("select").val() != 'base'){
					xval = el.children("td").eq(2).children("input").val();
					if(xval < base){
						base_must_low++;
					}
				}
			});
		}
		/*
		if(base_must_low > 0){
			$dialogNotice.html("We&rsquo;re very sorry, Base Commission values should not be lower than another commission type!");
			$dialogNotice.dialog("open");
		}else{
		*/
			url = '<?php echo $base_url; ?>/includes/hotel-profile/basic-info/hotel-commission-save.php';
			forminput = $('form#data-commission');
			submitDataRefresh(url, forminput);
		/*
		}
		*/
	});

	function submitDataRefresh(url, forminput){
		$.ajax({
			url: url,
			type: 'post',
			data: forminput.serialize(),
			success: function(data) {
				if(data == "1"){
					$dialogNoticeRefresh.html("Data succesfully updated");
					$dialogNoticeRefresh.dialog("open");
				}else{
					$dialogNoticeRefresh.html("We&rsquo;re very sorry, we can't save your data right now");
					$dialogNoticeRefresh.dialog("open");
				}
			}
		});
	}

	var $dialogNoticeRefresh = $('<div id="dialog-notice"></div>')
	.dialog({
		autoOpen: false,
		title: 'Notification',
		buttons: {
			"Ok": function(){ $( this ).dialog( "close" );  location.reload(); }
		}
	});

});
</script>
