<?php
require_once("includes/hotel-profile/photos/compress-image-function.php");

try {

	$imagename = $picture_upload_name; // (1000 x 667)
	$imagename_t = "t-".$picture_upload_name; // (160 x 107)

	//-------- mengupload gambar ke server
	$source = $picture_tmp_name;
	$folder = "pict/property/";
	// $folder = "myhotel/files/photos/property/";
	$target = $folder.$imagename;
	move_uploaded_file($source, $upload_base . $target);

	$file = $target; //This is the original file

	//-------- menurunkan resolusi gambar
	list($width, $height) = getimagesize($upload_base . $file) ;


	$save = $folder . $imagename_t; //This is the new file you saving
	if($width < 500 or $height < 500){
		$modwidth = $width * 50 /100;
		$modheight = $height * 50 /100;
	}else{
		$modwidth = $width * 30 /100;
		$modheight = $height * 30 /100;
	}
	$tn = imagecreatetruecolor($modwidth, $modheight) ;
	// $tn = imagecreate($modwidth, $modheight) ;
	ini_set('memory_limit', '-1');
	switch($picture_type) {
		case "image/gif":	$image = imagecreatefromgif($upload_base . $file); break;
		case "image/jpeg": $image = imagecreatefromjpeg($upload_base . $file); break;
		case "image/png": $image = imagecreatefrompng($upload_base . $file); break;
	}
	imagecopyresampled($tn, $image, 0, 0, 0, 0, $modwidth, $modheight, $width, $height) ;
	// echo "<h1>var=".$upload_base."***".$web_url."***".$target."</h1>";
	// echo "<h1>var=".$modwidth."x".$modheight."</h1>";
	saveImage($tn, $upload_base . $save, 75);



	// $save = $folder . $imagename; //This is the new file you saving
	// $modwidth = $width;
	// $modheight = $height;
	// echo "<h1>var=".$upload_base."***".$web_url."***".$target."</h1>";
	// $tn = imagecreatetruecolor($modwidth, $modheight) ;
	// switch($picture_type) {
	// 	case "image/gif":	$image = imagecreatefromgif($upload_base . $file); break;
	// 	case "image/jpeg": $image = imagecreatefromjpeg($upload_base . $file); break;
	// 	case "image/png": $image = imagecreatefrompng($upload_base . $file); break;
	// }
	// imagecopyresampled($tn, $image, 0, 0, 0, 0, $modwidth, $modheight, $width, $height) ;
	// saveImage($tn, $upload_base . $save, 75);


}catch(Exception $ex) {
	echo $ex->getMessage();
	// echo "error om";
	die();
}
?>
