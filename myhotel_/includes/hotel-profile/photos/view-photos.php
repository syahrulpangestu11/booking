<section class="content-header">
    <h1>
        Photo
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-laptop"></i> Settings</a></li>
        <li>Hotel Profile</li>
        <li class="active">Photo</li>
    </ol>
</section>
<section class="content" id="photo">
	<div class="row">
        <div class="box box-form">
            <div id="data-box" class="box-body">

                <h1>Photo</h1>
                    <button class="pure-button add-button" type="button" rt="<?php echo $roomoid; ?>">Add new photo</button>
                <div id="accor-photos">
                <?php
                    echo"<h3>Hotel Pictures</h3><div><ul class='inline-block'>";
                    try {
                        $stmt = $db->prepare("select hotelphotooid, thumbnailurl, photourl, flag from hotelphoto inner join hotel h using (hoteloid) where h.hoteloid=:hoid and ref_table=:a");
                        $stmt->execute(array(':a' => 'hotel', ':hoid' => $hoteloid));
                        $r_photo = $stmt->fetchAll(PDO::FETCH_ASSOC);
                        foreach($r_photo as $photo){
							if($photo['flag'] == "main"){ $class="bg-light-blue"; }else{ $class =""; }
                            echo"
                            <li>
                                <div><img src='".$photo['thumbnailurl']."' class='thumbnail'></div>
                                <div class='algn-center'>
                                    <button type='button' class='star main ".$class."' pid='".$photo['hotelphotooid']."'>Main Picture</button>
                                    <button type='button' class='trash2 delete' pid='".$photo['hotelphotooid']."'>Delete</button>
                                </div>
                            </li>";						
                        }
                    }catch(PDOException $ex) {
                        echo "Invalid Query";
                        die();
                    }
                    echo"</ul><div class='clear'></div></div>";
                
                    
                    try {
                        $stmt = $db->query("select roomoid, r.name from room r inner join hotel h using (hoteloid) where h.hoteloid = '".$hoteloid."'");
                        $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                        foreach($r_room as $row){
                            echo"<h3>".$row['name']."</h3><div><ul class='inline-block'>";
                            try {
                                $stmt = $db->prepare("select hotelphotooid, thumbnailurl, photourl, flag from hotelphoto inner join hotel h using (hoteloid) where h.hoteloid=:hoid and ref_table=:a and ref_id=:b");
                                $stmt->execute(array(':a' => 'room', ':b' => $row['roomoid'], ':hoid' => $hoteloid));
                                $r_photo = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                foreach($r_photo as $photo){
									if($photo['flag'] == "main"){ $class="bg-light-blue"; }else{ $class =""; }
                                    echo"
                                    <li>
                                        <div><img src='".$photo['thumbnailurl']."' class='thumbnail'></div>
                                        <div class='algn-center'>
											<button type='button' class='star main ".$class."' pid='".$photo['hotelphotooid']."'>Main Picture</button>
											<button type='button' class='trash2 delete' pid='".$photo['hotelphotooid']."'>Delete</button>
                                        </div>
                                    </li>";						
                                }
                            }catch(PDOException $ex) {
                                echo "Invalid Query";
                                die();
                            }
                            echo"</ul></div>";
                        }
                    }catch(PDOException $ex) {
                        echo "Invalid Query";
                        die();
                    }
                ?>
                </div>
        </div>
   		</div>
    </div>
</section>
