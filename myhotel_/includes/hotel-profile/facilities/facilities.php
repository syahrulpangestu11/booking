<section class="content-header">
    <h1>
        Facilities
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-laptop"></i> Settings</a></li>
        <li>Hotel Profile</li>
        <li class="active">Facilities</li>
    </ol>
</section>
<section class="content" id="facilities">
	<div class="row">
        <div class="box box-form">
            <div id="data-box" class="box-body">

                <div id="fcl-menu">
                    <button link-tab="hotel" class="current">Hotel Facilities</button>
                    <button link-tab="card">Card Accepted at this Property</button>
                    <button link-tab="language">Language Spoken</button>
                    <button link-tab="sports">Sports and Recreations</button>
                    <button link-tab="info">Useful Information</button>
                </div>
                <div id="hotel" class="fcl-content current"><?php include("facilities-hotel-facilities.php"); ?></div>
                <div id="card" class="fcl-content"><?php include("facilities-card.php"); ?></div>
                <div id="language" class="fcl-content"><?php include("facilities-language.php"); ?></div>
                <div id="sports" class="fcl-content"><?php include("facilities-sports-recreations.php"); ?></div>
                <div id="info" class="fcl-content"><?php include("facilities-useful-information.php"); ?></div>
                <style>
                        .fcl-content{
                            display: none;
                            padding: 10px;
                        }
                        .fcl-content.current{
                            display: inherit;
                        }
                </style>
        </div>
   		</div>
    </div>
</section>
