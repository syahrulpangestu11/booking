<?php
	include("../../../conf/connection.php");
	
	$hoteloid = $_POST['hoteloid'];
	$fcltype = $_POST['fcltype'];
	try {
		$stmt = $db->prepare("update hotelfacilities hf inner join facilities using (facilitiesoid) set hf.publishedoid=:a where hoteloid=:hoid and facilitiestypeoid=:b");
		$stmt->execute(array(':hoid' => $hoteloid, ':a' => '0', ':b' => $fcltype));
		$affected_rows = $stmt->rowCount();
	}catch(PDOException $ex) {
		echo "0";
		die();
	}

	foreach ($_POST['hotelfcl'] as $facilities){
		try {
			$stmt = $db->prepare("insert into hotelfacilities (`hoteloid`, `facilitiesoid`, `publishedoid`) values (:hoid , :a , :b)");
			$stmt->execute(array(':hoid' => $hoteloid, ':a' => $facilities, ':b' => '1'));
			$affected_rows = $affected_rows + $stmt->rowCount();
		}catch(PDOException $ex) {
			echo "0";
			die();
		}
	}

	if($affected_rows > 0){
		try {
			$stmt = $db->prepare("delete hf from hotelfacilities hf inner join facilities using (facilitiesoid) where hoteloid=:hoid and hf.publishedoid=:a and facilitiestypeoid=:b");
			$stmt->execute(array(':hoid' => $hoteloid, ':a' => '0', ':b' => $fcltype));
			$affected_rows = $stmt->rowCount();
		}catch(PDOException $ex) {
			echo "0";
			die();
		}
	}

	echo "1";
?>