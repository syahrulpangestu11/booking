<?php
	$facilitieslist = array();
	try {
		$stmt = $db->prepare("select hf.facilitiesoid from hotelfacilities hf inner join facilities f using (facilitiesoid) where hf.hoteloid=:hoid and f.facilitiestypeoid=:a and hf.publishedoid=:b and f.publishedoid =:c");
		$stmt->execute(array(':hoid' => $hoteloid, ':a' => '1', ':b' => '1', ':c' => '1'));
		$r_facilities = $stmt->fetchAll(PDO::FETCH_ASSOC);
		foreach($r_facilities as $row){
			array_push($facilitieslist, $row['facilitiesoid']);
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}
?>
<form method="post" enctype="multipart/form-data" id="data-input" action="#">
	<input type="hidden" name="hoteloid" value="<?php echo $hoteloid; ?>">
    <input type="hidden" name="fcltype" value="1">
    <div class="form-input">
        <ul class="inline-triple">
        <?php
            try {
                $stmt = $db->query("select f.* from facilities f inner join facilitiestype ft using (facilitiestypeoid) where ft.facilitiestypeoid = '1' and publishedoid = '1'");
                $r_facilities = $stmt->fetchAll(PDO::FETCH_ASSOC);
                foreach($r_facilities as $row){
						if(in_array($row['facilitiesoid'] , $facilitieslist, true)){ $checked = "checked"; }else{ $checked = ""; }
                    echo"<li><input type='checkbox' name='hotelfcl[]' value='".$row['facilitiesoid']."' ".$checked.">&nbsp;&nbsp;<span class='capitalize'>".$row['name']."</span></li>";

                }
            }catch(PDOException $ex) {
                echo "Invalid Query";
                die();
            }
        ?>
        </ul>
        <button type="button" class="submit">Save</button>
    </div>
</form>
