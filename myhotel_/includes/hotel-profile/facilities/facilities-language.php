<?php
	$languagelist = array();
	try {
		$stmt = $db->query("select hl.languageoid from hotellanguage hl inner join language using (languageoid) where hl.hoteloid = '".$hoteloid."' and hl.publishedoid = '1'");
		$r_language = $stmt->fetchAll(PDO::FETCH_ASSOC);
		foreach($r_language as $row){
			array_push($languagelist, $row['languageoid']);
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}
?>
<form method="post" enctype="multipart/form-data" id="data-input" action="#">
	<input type="hidden" name="hoteloid" value="<?php echo $hoteloid; ?>">
    <div class="form-input">
        <ul class="inline-four">
        <?php
            try {
                $stmt = $db->query("select * from language where publishedoid = '1'");
                $r_card = $stmt->fetchAll(PDO::FETCH_ASSOC);
                foreach($r_card as $row){
					if(in_array($row['languageoid'] , $languagelist, true)){ $checked = "checked"; }else{ $checked = ""; }
                    echo"<li><input type='checkbox' name='language[]' value='".$row['languageoid']."' ".$checked.">&nbsp;&nbsp;<span class='capitalize'>".$row['languagetitle']."</span></li>";

                }
            }catch(PDOException $ex) {
                echo "Invalid Query";
                die();
            }
        ?>
        </ul>
        <button type="button" class="submit">Save</button>
    </div>
</form>
