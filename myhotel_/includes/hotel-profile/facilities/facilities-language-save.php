<?php
	include("../../../conf/connection.php");
	
	$hoteloid = $_POST['hoteloid'];
	try {
		$stmt = $db->prepare("update hotellanguage set publishedoid=:a where hoteloid=:hoid");
		$stmt->execute(array(':hoid' => $hoteloid, ':a' => '0'));
		$affected_rows = $stmt->rowCount();
	}catch(PDOException $ex) {
		echo "0";
		die();
	}

	foreach ($_POST['language'] as $language){
		try {
			$stmt = $db->prepare("insert into hotellanguage (`hoteloid`, `languageoid`, `publishedoid`) values (:hoid , :a , :b)");
			$stmt->execute(array(':hoid' => $hoteloid, ':a' => $language, ':b' => '1'));
			$affected_rows = $affected_rows + $stmt->rowCount();
		}catch(PDOException $ex) {
			echo "0";
			die();
		}
	}

	if($affected_rows > 0){
		try {
			$stmt = $db->prepare("delete from hotellanguage where hoteloid=:hoid and publishedoid=:a");
			$stmt->execute(array(':hoid' => $hoteloid, ':a' => '0'));
			$affected_rows = $stmt->rowCount();
		}catch(PDOException $ex) {
			echo "0";
			die();
		}
	}

	echo "1";
?>