<section class="content-header">
    <h1>
        Room Type
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-laptop"></i> Settings</a></li>
        <li>Hotel Profile</li>
        <li class="active">Rooms</li>
    </ol>
</section>

<?php
// phpinfo();
// echo "<pre>";
// print_r ($_SERVER);
// echo "</pre>";
?>

<section class="content" id="rooms">
	<div class="row">
        <div class="box box-form">
            <div id="data-box" class="box-body">

                <div>
                    <button class="small-button add-button blue" type="button" rt="<?php echo $roomoid; ?>">Add new Room Type</button>
                    <!-- <a href="<?php echo $base_url; ?>/room-settings"><input type="button" value="Room Settings" class="small-button green" /></a>
                    <a href="<?php echo $base_url; ?>/hotel-profile/photo"><input type="button" value="Upload Photo" class="small-button green" /></a> -->
                </div>
                <br/>
                <ul id="accordion">

                <!-- <table class="table table-bordered"> -->
                    <!-- <tr>
                        <td>Image</td>
                        <td>Room Type Name</td>
                        <td>Published</td>
                        <td>&nbsp;</td>
                    </tr> -->
                    <?php
                    try {
                        $stmt = $db->query("SELECT r.* FROM room r inner join hotel h using (hoteloid) where h.hoteloid = '".$hoteloid."' AND r.publishedoid NOT IN (3) ");
                        $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                        $accordion_id=0;
                        foreach($r_room as $row){
							unset($thumbnail);
                            try {
                                $stmt = $db->prepare("select thumbnailurl from hotelphoto inner join hotel h using (hoteloid) where h.hoteloid=:hoid and ref_table=:a and ref_id=:b and flag=:c");
                                $stmt->execute(array(':a' => 'room', ':b' => $row['roomoid'], ':c' => 'main', ':hoid' => $hoteloid));
                                $r_photo = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                foreach($r_photo as $photo){
									                $thumbnail =  $photo['thumbnailurl'];
								                }
                            }catch(PDOException $ex) {
                                echo "Invalid Query";
                                die();
                            }
                ?>
                   <li>
                      <div class="header">
                        <div class="table-cell image"><img src="<?php echo $thumbnail; ?>" class="thumbnail"></div>
                        <div class="table-cell"><?php echo $row['name']; ?></div>
                        <div class="table-cell">
                            <?php // if($row['publishedoid'] == "1"){ $checked = "checked"; }else{ $checked = "";  } ?>
                            <!-- <input type="checkbox" name="published" disabled="disabled" value="1" <?php echo $checked; ?> /> -->
                            <?php if($row['publishedoid'] == "1"){ $status = "Active"; }else{ $status = "Inactive";  } ?>
                            Status: <?=$status;?>
                        </div>
                        <div class="table-cell algn-right">
                            <button type="button" class="small-button blue edit-roomtype" ro="<?php echo $row['roomoid']; ?>" rel="<?=$accordion_id;?>">Edit</button>
                            <button type="button" class="small-button red delete-roomtype" ro="<?php echo $row['roomoid']; ?>">Delete</button>
                        </div>
                      </div>
                      <div class="content">
                        <?php
                        $_GET['ro'] = $row['roomoid'];
                        include("includes/hotel-profile/room/edit-room.php");
                        ?>
                      </div>
                    </li>
                    <?php
                    $accordion_id++;
                        }
                    }catch(PDOException $ex) {
                        echo "Invalid Query";
                        die();
                    }
                    ?>
                <!-- </table> -->
              </ul>
        	</div>

   		</div>
    </div>
</section>

<style media="screen">

  .ui-widget, .ui-widget * {font-family: "Proxima Nova" !important;}
  #accordion .header {background: #f4f8fb !important; border: 1px solid #bbb; color: #222; margin-top: 10px; font-weight: normal;}
  #accordion .header.ui-accordion-header-active,
  #accordion .header.ui-state-hover {background-color: #5d9cec !important; border-color: transparent; color: #fff;}
  #accordion .header .table-cell {display: table-cell; vertical-align: middle; padding: 0 10px;}
  #accordion .header .table-cell:nth-of-type(1) {width: 10%;}
  #accordion .header .table-cell:nth-of-type(2) {width: 30%;}


  #accordion .header .image .thumbnail {width: 100%;}

  #accordion .content {
    background: #F4F8FB !important; border: 1px solid #bbb !important;
    border-top: none;
  }


  ul.tabs{
    margin: 0px;
    padding: 0px;
    list-style: none;
  }
  ul.tabs li{
    background: #5D9CEC;
    color: #fff;
    display: inline-block;
    padding: 10px 15px;
    cursor: pointer;
    font-weight: bold;
  }

  ul.tabs li.current{
    background: #fff;
    color: #222;
    /*font-weight: bold;*/
    border: 1px solid #bbb;
    border-bottom: none;
    position: relative;
  }
  ul.tabs li.current::after{
    content: "";
    position: absolute;
    bottom: -5px;
    left: 0;
    width: 100%;
    border: 5px solid #fff;
  }

  .tab-content{
    display: none;
    background: #fff;
    padding: 15px;
    border: 1px solid #bbb;
  }

  .tab-content.current{
    display: inherit;
    /*margin-top: -2px;*/
  }

  .tab-content .this-inline-block {margin-left: 0; margin-right: 15px;}
  .tab-content .this-inline-block:last-of-type {margin-right: 0;}


  .table-photos tr td:nth-of-type(1) {width: 200px;}
  .table-photos tr td:nth-of-type(1) img {max-width: 100%;}
  /*.table-photos tr td:nth-of-type(1) img.input-image-preview {width: inherit;}*/
  .table-photos tr td:nth-of-type(2) {display: none;}
  .table-photos tr td:nth-of-type(3) button {width: auto; height: auto; padding: 10px 10px 10px 25px;}
  .table-photos tr td:nth-of-type(3) button[disabled='disabled'] {cursor: default;}
</style>


<script type="text/javascript">
$(function(){
// $( document ).ready(function(){
  // $(this).next(".content").find(".tabs").find(".tab-link").first().click();

  $( "#accordion" ).accordion({
    header: ".header",
    collapsible: true,
    active: false,
    heightStyle: "content",
    activate: function( event, ui ) {
      var header = $(this).find(".header.ui-accordion-header-active");
      header.next(".content").find(".tabs").find(".tab-link").first().click();
    }
  });

  $("#accordion .header .delete-roomtype").click(function() {
    $(this).closest(".header").unbind('click');
  });


  // Tabs
  $('ul.tabs li').click(function(){
  	var tab_id = $(this).attr('data-tab');

  	$('ul.tabs li').removeClass('current');
  	$('.tab-content').removeClass('current');

  	$(this).addClass('current');
  	$("#"+tab_id).addClass('current');
  });

  // AJAX submitDataCallback
  function submitDataCallback(url, forminput){
    // alert("fi="+forminput.serialize());
    $.ajax({
      url: url,
      type: 'post',
      data: forminput.serialize(),
      success: function(data) {
        if(data == "1"){
          $dialogNotice.html("Data succesfully updated");
          $dialogNotice.dialog("open");
        }else{
          // $dialogNotice.html("We&rsquo;re very sorry, we can't save your data right now. Error = "+data);
          $dialogNotice.html("We&rsquo;re very sorry, we can't save your data right now. " + data);
          $dialogNotice.dialog("open");
        }
      }
    });
  }

  function redirectURL(isRedirect = false, url){
    if(isRedirect==true){
      $(location).attr("href", url);
    }
  }

  var $dialogNotice = $('<div id="dialog-notice"></div>')
	.dialog({
		autoOpen: false,
		title: 'Notification',
		buttons: {
			"Ok": function(){
				$(this).dialog( "close" );
				// document.location.href = "<?php echo $base_url; ?>/hotel-profile/rooms";
			}
		}
	});

  $('button.submit-edit-room').click(function(){
		url = '<?php echo $base_url; ?>/includes/hotel-profile/room/edit-room-save.php';
		forminput = $(this).closest('form#data-input');
		submitDataCallback(url, forminput);
	});


  // /*
  // TOMBOL ADD RATE PLAN
  $('body').on('click','button.add-rate-plan', function(e) {
		tableTarget = $(this).closest('table.table-fill');
		// parentTable = $(this).parent().parent().parent();
    parentTable = $(this).closest("table.table-fill");

		name = parentTable.find('input[name=offername]').val();
		offertype = parentTable.find('select[name=offertype]').val();
		type = parentTable.find('select[name=offertype] option:selected').text();
		minrate = parentTable.find('input[name=offerminrate]').val();

		if(name.length !== 0){

			tableTarget.append("<tr><td></td><td><input type = 'text' class = 'medium' name = 'new_name[]' value = '"+name+"'></td><td class='select_type'></td><td><input type = 'text' class = 'medium' name = 'new_minrate[]' value = '"+minrate+"'></td><td>&nbsp;</td><td><button type='button' class='small-button red del'>Delete</button></td></tr>");

			appendDestination = tableTarget.find('.select_type').last();
			tableTarget.find('select[name=offertype]').clone().attr('name','new_type[]').appendTo(appendDestination);
			newSelect = tableTarget.find('select[name="new_type[]"]').last();
			newSelect.val(offertype);

			$(this).closest('tr').find('input').val('');
			$(this).closest('tr').find('select').prop('seletedIndex', 0);
		}
  });


  // TOMBOL DELETE RATE PLAN

   var $dialogDelete = $('<div id="dialog-del"></div>')
	.html('Are you sure you want to delete this data?')
	.dialog({
		autoOpen: false,
		title: 'Confirmation',
		buttons: {
			"Delete": function(){
				var elem = $(this).data('elem');
				$(this).dialog("close");
				deleteElem(elem);
			},
			Cancel: function(){ $( this ).dialog("close"); }
		}
	});

	var $dialogNoticeRedirect = $('<div id="dialog-notice"></div>')
	.dialog({
		autoOpen: false,
		title: 'Notification',
		buttons: {
			"Ok": function(){
				$(this).dialog("close");
        $(location).attr("href", "<?php echo $base_url; ?>/hotel-profile/rooms");
			}
		}
	});


	function deleteElem(elem){
		elem.parent().parent().remove();
	}

	$('body').on('click','.del', function(e) {
		// id = $(this).attr("pid");
		// $dialogDelete.data("url", "<?php echo $base_url; ?>/includes/promotion/delete-promotion.php");
		// $dialogDelete.data("id", id);
    $dialogDelete.data("elem", $(this));
		$dialogDelete.dialog("open");
	});



  //--------------------------------------------------- PHOTOS BROO -----------------------------------------------------------------
  photosOf = '<input type="hidden" name="reference[]" class="input-select photo-ref">';

  function readURL(input, previewTarget) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            previewTarget
                .attr('src', e.target.result)
                .height(100)
                .show();
        };
        reader.readAsDataURL(input.files[0]);
    }
  }

  $(document).ready(function(){
    appendContent();
  });

	$('body').on('click','button.add-photos', function(e) {
		appendContent($(this));
	});

	function appendContent(trigger){
    if(trigger==undefined){
  		tableTarget = $('table.table-photos');
    }else{
      tableTarget = trigger.closest('table.table-photos');
    }
    tableTarget.find('button.add-photos').remove();
    tableTarget.append(
      "<tr>"+
        "<td>"+
          "<input type='file' class='input-text input-image' name='photos[]' accept='image/*'>"+
          "<br><img class='input-image-preview' id='preview' src='#' alt='' />"+
        "</td>"+
        "<td>"+photosOf+"</td>"+
        "<td><button type='button' class='blue-button add-photos'>+ Add More Photo</button></td>"+
      "</tr>");

    $('.photo-ref').each(function(){
      var photo_select_ref = $(this);
      var roomoid = photo_select_ref.closest('.form-wrapper').find('input[name="roomoid"]').val();
      photo_select_ref.val('room-'+roomoid);
    });

    $('.input-image').each(function(){
      $('.input-image').change(function() {
        var thisElem = $(this);
        var thisElemJS = this;
        var previewTarget = thisElem.closest('tr').find('.input-image-preview');
        var sizeKB = thisElemJS.files[0].size / 1024;
        var img = new Image();
        img.src = window.URL.createObjectURL( this.files[0] );
        img.onload = function() {
          var width = img.naturalWidth,
              height = img.naturalHeight;
          // alert(sizeKB+' ** '+width +'×'+height);

          if(sizeKB > 200){
              // alert("Your image more than 200 KB, please compress or resize your image.");
              $dialogNotice.html("Your image more than 200 KB, please compress or resize your image");
              $dialogNotice.dialog("open");
              thisElem.val("");
              previewTarget.attr('src',"#");
          }else{
              var ext = thisElem.val().split('.').pop().toLowerCase();
              if($.inArray(ext, ['jpg','jpeg', 'png', 'gif']) == -1) {
                // alert('Invalid extension! Please make sure you upload image file type (.jpg , .jpeg , .png , or .gif).');
                $dialogNotice.html("Invalid extension! Please make sure you upload image file type (.jpg , .jpeg , .png , or .gif)");
                $dialogNotice.dialog("open");
                thisElem.val("");
                previewTarget.attr('src',"#");
              }else{
              readURL(thisElemJS, previewTarget);
            }
          }
        }

      });
    });
	}

	$('body').on('click', '#room-photo button.delete', function(e){
		pid = $(this).attr("pid");
		$dialogDelPhoto.data("pid", pid);
		$dialogDelPhoto.dialog("open");
	});

	$('body').on('click', '#room-photo button.main', function(e){
		pid = $(this).attr("pid");
		mainPictureProcess(pid);
	});

	$('body').on('click', '#room-photo button.main-flexible', function(e){
		pid = $(this).attr("pid");
		mainPictureFRProcess(pid);
	});


	var $dialogDelPhoto = $('<div id="dialog-del"></div>')
	.html('Are you sure you want to delete this data?')
	.dialog({
		autoOpen: false,
		title: 'Confirmation',
		buttons: {
			"Delete": function(){
				var pid = $(this).data('pid');
				deleteProcess(pid);
				$( this ).dialog( "close" );
			},
			Cancel: function(){
				$( this ).dialog( "close" );
			}
		}
	});

	function deleteProcess(pid){
		$.ajax({
			url: '<?php echo $base_url; ?>/includes/hotel-profile/photos/delete-photos.php',
			type: 'post',
			data: { pid : pid},
			success: function(data) {
				if(data == "1"){
					$dialogNoticeRedirect.html("Data has been deleted");
					$dialogNoticeRedirect.dialog("open");
				}else{
					$dialogNoticeRedirect.html("We&rsquo;re very sorry, we can't delete your data right now");
					$dialogNoticeRedirect.dialog("open");
				}
			}
		});
	}

	function mainPictureProcess(pid){
		$.ajax({
			url: '<?php echo $base_url; ?>/includes/hotel-profile/photos/set-main-photos.php',
			type: 'post',
			data: { pid : pid, rqst : "main" },
			success: function(data) {
				if(data == "1"){
					$dialogNoticeRedirect.html("Picture has been set as main image");
					$dialogNoticeRedirect.dialog("open");
				}else{
					$dialogNoticeRedirect.html("We&rsquo;re very sorry, we can't save your setting right now");
					$dialogNoticeRedirect.dialog("open");
				}
			}
		});
	}

	function mainPictureFRProcess(pid){
		$.ajax({
			url: '<?php echo $base_url; ?>/includes/hotel-profile/photos/set-main-photos.php',
			type: 'post',
			data: { pid : pid, rqst : "flexiblerate" },
			success: function(data) {
				if(data == "1"){
					$dialogNoticeRedirect.html("Picture has been set as main image");
					$dialogNoticeRedirect.dialog("open");
				}else{
					$dialogNoticeRedirect.html("We&rsquo;re very sorry, we can't save your setting right now");
					$dialogNoticeRedirect.dialog("open");
				}
			}
		});
	}


  //*/----------------------- Delete Roomtype ------------------------------
  var $dialogDeleteRT = $('<div id="dialog-del"></div>')
 	.html('Are you sure you want to delete this room?')
 	.dialog({
 		autoOpen: false,
 		title: 'Confirmation',
 		buttons: {
 			"Delete": function(){
 				var rid = $(this).data('rid');
        var url = '<?php echo $base_url; ?>/includes/hotel-profile/room/delete-room.php';
        deleteProcessRT(url, rid);
 				$(this).dialog("close");
 			},
 			Cancel: function(){
 				$(this).dialog("close");
 			}
 		}
 	});

  function deleteProcessRT(url, rid){
		$.ajax({
			url: url,
			type: 'post',
			data: { rid : rid},
			success: function(data) {
				if(data == "1"){
					$dialogDeleteRTNotif.html("Data has been deleted");
					$dialogDeleteRTNotif.dialog("open");
				}else{
					$dialogDeleteRTNotif.html("We&rsquo;re very sorry, we can't delete your data right now");
					$dialogDeleteRTNotif.dialog("open");
				}
			}
		});
	}

  $('.delete-roomtype').click(function(){
    rid = $(this).attr("ro");
		$dialogDeleteRT.data("rid", rid);
    $dialogDeleteRT.dialog("open");
  });

  var $dialogDeleteRTNotif = $('<div id="dialog-del"></div>')
	.html('Are you sure you want to delete this data?')
	.dialog({
		autoOpen: false,
		title: 'Notification',
		buttons: {
			"Ok": function(){
				$(this).dialog("close");
  			$(location).attr("href", "<?php echo $base_url; ?>/hotel-profile/rooms");
			}
		}
	});
  //*/
});
</script>
<?php
// echo "<pre>";
// print_r($_POST);
// echo "</pre>";
?>
