<section class="content-header">
    <h1>
        Add Room Type
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-laptop"></i> Settings</a></li>
        <li>Hotel Profile</li>
        <li>Rooms</li>
        <li class="active">Add</li>
    </ol>
</section>
<section class="content" id="rooms">
	<div class="row">
        <div class="box box-form">
            <div id="data-box" class="box-body">

                <h1>Room Settings</h1>
				<form method="post" enctype="multipart/form-data" id="data-input" action="#">

                <div class="form-input form-wrapper">
                  <input type="hidden" name="hoteloid" value="<?php echo $hoteloid; ?>">
                  <input type="hidden" name="bu" value="<?php echo $base_url; ?>">

                  <input type="hidden" name="added_roomoid" id="added-roomoid" value="">


                  <style media="screen">
                  ul.tabs{
                    margin: 0px;
                    padding: 0px;
                    list-style: none;
                  }
                  ul.tabs li{
                    background: #5D9CEC;
                    color: #fff;
                    display: inline-block;
                    padding: 10px 15px;
                    cursor: pointer;
                    font-weight: bold;
                  }

                  ul.tabs li.current{
                    background: #fff;
                    color: #222;
                    /*font-weight: bold;*/
                    border: 1px solid #bbb;
                    border-bottom: none;
                    position: relative;
                  }
                  ul.tabs li.current::after{
                    content: "";
                    position: absolute;
                    bottom: -5px;
                    left: 0;
                    width: 100%;
                    border: 5px solid #fff;
                  }

                  .tab-content{
                    display: none;
                    background: #fff;
                    padding: 15px;
                    border: 1px solid #bbb;
                  }

                  .tab-content.current{
                    display: inherit;
                    /*margin-top: -2px;*/
                  }

                  .tab-content .this-inline-block {margin-left: 0; margin-right: 40px;}
                  .tab-content .this-inline-block:last-of-type {margin-right: 0;}

                  .table-photos tr td:nth-of-type(2) {display: none;}


                	.form-input tr td {vertical-align: top;}
                	.form-input textarea.input-text {height: 70px !important;}
                	.form-input input[type=text].short {width: 50px;}

                  </style>

                  <ul class="tabs">
  									<li class="tab-link" data-tab="tab-1">1. Basic Info</li>
  									<li class="tab-link" data-tab="tab-2">2. Bed Configuration</li>
  									<li class="tab-link" data-tab="tab-3">3. Room Facilities</li>
  									<li class="tab-link" data-tab="tab-4">4. Rate Plan</li>
  									<li class="tab-link" data-tab="tab-5">5. Photos</li>
  								</ul>

                  <div id="tab-1" class="tab-content" data-next="tab-2">
  									<table class="form-input">
                      <tr>
                          <td>Room Type</td>
                          <td><input type="text" class="input-text" id="roomname" name="name" required="required" ></td>
                      </tr>
                      <tr>
                          <td>Room Type Description</td>
                          <td><textarea class="input-text" name="description"></textarea></td>
                      </tr>
                      <tr>
                          <td>Status Room</td>
                          <td>
                            <select name="published" class="input-select">
                                <?php
                                try {
                                    $stmt = $db->query("select * from published where showthis = 'y'");
                                    $r_published = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                    foreach($r_published as $row){
                                ?>
                                    <option value="<?php echo $row['publishedoid']; ?>"><?php echo $row['note']; ?></option>
                                <?php
                                    }
                                }catch(PDOException $ex) {
                                    echo "Invalid Query";
                                    die();
                                }
                                ?>
                            </select>
                          </td>
                      	</tr>
  	                    <tr>
  	                    	<td>Minimum Room Size</td>
  	                        <td>
  	                        <input type="text" class="input-text" name="roomsize" >
  	                        &nbsp;&nbsp;
  	                        <input type="checkbox" name='roomsize_inc_balcon'> Size include balcon / terrace
  	                        </td>
  	                    </tr>
  	                    <tr>
                          <td>View</td>
                          <td>
                            <select name="view" class="input-select">
              								<?php
                                try {
                                    $stmt = $db->query("SELECT * FROM view ORDER BY name");
                                    $r_view = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                    foreach($r_view as $row){
                                ?>
                                    <option value="<?php echo $row['viewoid']; ?>"><?php echo $row['name']; ?></option>
                                <?php
                                    }
                                }catch(PDOException $ex) {
                                    echo "Invalid Query";
                                    die();
                                }
                                ?>
                            </select>

                          </td>
  	                    </tr>
                        <tr>
  												<td>No. of Rooms</td>
  												<td><input type="text" class="small short input-text"  name="numroom" value="<?php echo $numroom; ?>"></td>
  											</tr>
											<tr style="display:none;">
  												<td>Allotment Alert Level</td>
  												<td><input type="text" class="small short input-text"  name="allotmentalert" value="<?php echo $allotmentalert; ?>"></td>
  											</tr>
  											<tr>
  												<td>Max. Occupancy on Existing Bedding</td>
  												<td><input type="text" class="small short input-text"  name="adult" value="<?php echo $adult; ?>"></td>
  											</tr>
  											<tr>
  												<td>Max. Extra Beds</td>
  												<td><input type="text" class="small short input-text"  name="maxextrabed" value="<?php echo $maxextrabed; ?>"></td>
  											</tr>
  											<tr>
  												<td>Max. Children</td>
  						            <td><input type="text" class="small short input-text"  name="child" value="<?php echo $child; ?>"></td>
  											</tr>
  										</table>

  										<button type="button" class="next-add-room">Next</button>
  								</div>

                	<div id="tab-2" class="tab-content current" data-next="tab-3">
                    <ul class="inline-triple">
                    <?php
                        try {
                            $stmt = $db->query("select * from bed");
                            $r_card = $stmt->fetchAll(PDO::FETCH_ASSOC);
                            foreach($r_card as $row){
                                echo"<li><input type='checkbox' name='bed[]' value='".$row['bedoid']."'>&nbsp;&nbsp;<span class='capitalize'>".$row['name']."</span></li>";

                            }
                        }catch(PDOException $ex) {
                            echo "Invalid Query";
                            die();
                        }
                    ?>
                    <div class="clear"></div>
                    </ul>

                    <button type="button" class="next-add-room">Next</button>
                	</div>
                	<div id="tab-3" class="tab-content">
                    <ul class="block this-inline-block">
                    <?php
                        try {
                            $stmt = $db->query("select f.*, CHAR_LENGTH(f.name) AS pjg_nama from facilities f inner join facilitiestype ft using (facilitiestypeoid) where ft.facilitiestypeoid = '3' and publishedoid = '1' ORDER BY pjg_nama");
                            $r_facilities = $stmt->fetchAll(PDO::FETCH_ASSOC);
                            $n_facilities = $stmt->rowCount();
                            $m=ceil($n_facilities/5);
                            $n=0;
                            foreach($r_facilities as $row){
                                $n++;
                                echo"<li><input type='checkbox' name='roomfcl[]' value='".$row['facilitiesoid']."'>&nbsp;&nbsp;<span class='capitalize'>".$row['name']."</span></li>";
                                if($n%$m==0){
                                  echo "</ul><ul class='block this-inline-block'>";
                                }

                            }
                        }catch(PDOException $ex) {
                            echo "Invalid Query";
                            die();
                        }
                    ?>
                    <div class="clear"></div>
                    </ul>

                    <button type="button" class="submit-add-room">Save</button>
                	</div>
                	<div id="tab-4" class="tab-content">
                    <?php
                    include("includes/hotel-profile/room/room-settings/add-room-settings.php");
                    ?>
                    <button type="button" class="submit-add-room">Save</button>
                    <button type="button" class="cancel">Cancel</button>
                	</div>

                  <div id="tab-5" class="tab-content">
                    <?php include("includes/hotel-profile/room/photos/add-photos.php"); ?>
                  </div>

                    <!-- <button type="button" class="submit-add">Save</button>
                    <button type="button" class="cancel">Cancel</button> -->
                </div>
                </form>

			</div>
   		</div>
    </div>
</section>


<script type="text/javascript">
$(function(e){

  $('ul.tabs li').click(function(){
    var tab_id = $(this).attr('data-tab');

    $('ul.tabs li').removeClass('current');
    $('.tab-content').removeClass('current');

    $(this).addClass('current');
    $("#"+tab_id).addClass('current');
  });


  $(".tabs").find(".tab-link").first().click();
  $('li[data-tab="tab-4"]').hide();
  $('li[data-tab="tab-5"]').hide();


  $('#added-roomoid').change(function(){
    // alert('asd');
    var added_roomoid = $(this).val();
    if(added_roomoid.length == 0 ) {
      $('li[data-tab="tab-4"]').hide();
      $('li[data-tab="tab-5"]').hide();
    }else{
      $('li[data-tab="tab-4"]').show();
      $('li[data-tab="tab-5"]').show();
    }
    $('#room-settings-rt').val(added_roomoid);
    // alert("added="+$(this).val());
    // alert("added#="+$('#added-roomoid').val());
  });

  // AJAX submitDataCallback_AddRoom
  function submitDataCallback_AddRoom(url, forminput){
    // alert("fi="+forminput.serialize());
    $.ajax({
      url: url,
      type: 'post',
      data: forminput.serialize(),
      success: function(data) {
        // alert("data="+data);
        var dataSplit = data.split("---");
        var tipe = dataSplit[0];
        var dataStatus = dataSplit[dataSplit.length-1];

        if(tipe=='r'){
          var roomoid = dataSplit[1];
          $('#added-roomoid').val(roomoid);
          $('#added-roomoid').change();
          $('li[data-tab="tab-4"]').click();
        }else if(tipe=='rs'){
          $('#added-roomoid').change();
          $('li[data-tab="tab-5"]').click();
          appendContent();
        }

        if(dataStatus == "1"){
          $dialogNotice.html("Data succesfully updated");
          $dialogNotice.dialog("open");
        }else{
          $dialogNotice.html("We&rsquo;re very sorry, we can't save your data right now. Error = "+data);
          $dialogNotice.dialog("open");
        }
      }
    });
  }

  var $dialogNotice = $('<div id="dialog-notice"></div>')
  .dialog({
    autoOpen: false,
    title: 'Notification',
    buttons: {
      "Ok": function(){
        $( this ).dialog( "close" );
        // document.location.href = "<?php echo $base_url; ?>/hotel-profile/rooms";
      }
    }
  });

  $('button.submit-add-room').click(function(){
    url = '<?php echo $base_url; ?>/includes/hotel-profile/room/add-room-save.php';
    forminput = $(this).closest('form#data-input');
    roomname = $('#roomname');
    if(roomname.val().length !== 0){
      submitDataCallback_AddRoom(url, forminput);
    }else{
      $('li[data-tab="tab-1"]').click();
      roomname.focus();
    }
  });

  $('button.next-add-room').click(function(){
    var nextTab = $(this).closest('.tab-content').attr("data-next");
    $(".tabs").find('li[data-tab="'+nextTab+'"]').click();
  });





  // TOMBOL ADD RATE PLAN
  $('body').on('click','button.add-rate-plan', function(e) {
    tableTarget = $(this).closest('table.table-fill');
    // parentTable = $(this).parent().parent().parent();
    parentTable = $(this).closest("table.table-fill");

    name = parentTable.find('input[name=offername]').val();
    offertype = parentTable.find('select[name=offertype]').val();
    type = parentTable.find('select[name=offertype] option:selected').text();
    minrate = parentTable.find('input[name=offerminrate]').val();

    if(name.length !== 0){

      tableTarget.append("<tr><td></td><td><input type = 'text' class = 'medium' name = 'new_name[]' value = '"+name+"'></td><td class='select_type'></td><td><input type = 'text' class = 'medium' name = 'new_minrate[]' value = '"+minrate+"'></td><td>&nbsp;</td><td><button type='button' class='small-button red del'>Delete</button></td></tr>");

      appendDestination = tableTarget.find('.select_type').last();
      tableTarget.find('select[name=offertype]').clone().attr('name','new_type[]').appendTo(appendDestination);
      newSelect = tableTarget.find('select[name="new_type[]"]').last();
      newSelect.val(offertype);

      $(this).closest('tr').find('input').val('');
      $(this).closest('tr').find('select').prop('seletedIndex', 0);
    }
  });


    // TOMBOL DELETE RATE PLAN

       var $dialogDelete = $('<div id="dialog-del"></div>')
      .html('Are you sure you want to delete this data?')
      .dialog({
        autoOpen: false,
        title: 'Confirmation',
        buttons: {
          "Delete": function(){
            var elem = $(this).data('elem');
            $(this).dialog("close");
            deleteElem(elem);
          },
          Cancel: function(){ $( this ).dialog("close"); }
        }
      });

      var $dialogNotification = $('<div id="dialog-notice"></div>')
      .dialog({
        autoOpen: false,
        title: 'Notification',
        buttons: {
          "Ok": function(){
            var callback = '<?php echo $base_url; ?>/promotions';
            $(this).dialog( "close" );
            //$(location).attr("href", callback);
          }
        }
      });

      function deleteAction(url, id){
        $.ajax({
          url: url,
          type: 'post',
          data: { id : id},
          success: function(data) {
            if(data == "1"){
              $dialogNotification.html("Data has been deleted");
              $dialogNotification.dialog("open");
            }else{
              $dialogNotification.html("We&rsquo;re very sorry, we can't save your data right now");
              $dialogNotification.dialog("open");
            }
          }
        });
      }

      function deleteElem(elem){
        elem.parent().parent().remove();
      }

      $('body').on('click','.del', function(e) {
        // id = $(this).attr("pid");
        // $dialogDelete.data("url", "<?php echo $base_url; ?>/includes/promotion/delete-promotion.php");
        // $dialogDelete.data("id", id);
        $dialogDelete.data("elem", $(this));
        $dialogDelete.dialog("open");
      });





  // ********************************************** PHOTOS BROO ******************************************

  photosOf = '<input type="text" name="reference[]" class="input-select photo-ref">';

  $('body').on('click','button.add-photos', function(e) {
    appendContent($(this));
  });

  function appendContent(trigger){
    if(trigger==undefined){
      tableTarget = $('table.table-photos');
    }else{
      tableTarget = trigger.closest('table.table-photos');
    }
    tableTarget.find('button.add-photos').remove();
    tableTarget.append("<tr><td><input type='file' class='input-text' name='photos[]' accept='image/*'></td><td>"+photosOf+"</td><td><button type='button' class='blue-button add-photos'>+ Add More Photo</button></td></tr>");

    $('.photo-ref').each(function(){
      var photo_select_ref = $(this);
      var roomoid = $('#added-roomoid').val();
      photo_select_ref.val('room-'+roomoid);
    });
  }
});
</script>
