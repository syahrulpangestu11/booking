
            <!-- <table id="data-box" class="box-body"> -->

                <?php
                    try {
                        $stmt = $db->query("select roomoid, r.name from room r inner join hotel h using (hoteloid) where h.hoteloid = '".$hoteloid."' AND roomoid = '".$roomoid."'");
                        $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                        foreach($r_room as $row){
                            try {
                                $stmt = $db->prepare("select hotelphotooid, thumbnailurl, photourl, flag, flag_flexible_rate from hotelphoto inner join hotel h using (hoteloid) where h.hoteloid=:hoid and ref_table=:a and ref_id=:b");
                                $stmt->execute(array(':a' => 'room', ':b' => $row['roomoid'], ':hoid' => $hoteloid));
                                $r_photo = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                foreach($r_photo as $photo){
									if($photo['flag'] == "main"){
                                      $class="bg-light-blue";
                                      $disabled = "disabled='disabled'";
                                    }else{
                                      $class ="";
                                      $disabled="";
                                    }
									
									if($photo['flag_flexible_rate'] == "y"){
                                      $class_fr		="bg-light-blue";
                                      $disabled_fr = "disabled='disabled'";
                                    }else{
                                      $class_fr		="";
                                      $disabled_fr	="";
                                    }
                                    echo"
                                    <tr>
                                        <td><img src='".$photo['thumbnailurl']."' class='thumbnail'></td>
                                        <td class='algn-center'>&nbsp;</td>
                                        <td class='algn-center'>
											<button type='button' class='small-button star main ".$class."' pid='".$photo['hotelphotooid']."' ".$disabled.">Main Picture</button>
											<button type='button' class='small-button trash2 delete' pid='".$photo['hotelphotooid']."'>Delete</button>
											<button type='button' class='small-button star main-flexible ".$class_fr."' pid='".$photo['hotelphotooid']."' ".$disabled_fr.">Set as Flexible Rate Image</button>
                                        </td>
                                    </tr>";
                                }
                            }catch(PDOException $ex) {
                                echo "Invalid Query";
                                die();
                            }
                        }
                    }catch(PDOException $ex) {
                        echo "Invalid Query";
                        die();
                    }
                ?>
        <!-- </table> -->
