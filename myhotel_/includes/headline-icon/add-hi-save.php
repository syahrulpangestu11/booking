<?php
	$name = (isset($_POST['name'])) ? $_POST['name'] : "";
	$description = (isset($_POST['description'])) ? $_POST['description'] : "";
	
	$allowedExtension = array('jpg', 'jpeg', 'png');
	$tmp = explode('.', $_FILES['image']['name']);
	$extension = end($tmp);
	$image = '';
	if(!empty($_FILES['image']['name']) and isset($_FILES['image']['name'])){
		if(in_array($extension, $allowedExtension)){
			$imagename = str_replace(' ', '_', $_FILES['image']['name']);
			$imagename = str_replace('.'.$extension, '', $imagename);
			$ext = '.'.strtolower($extension);
			$image_new_name = $hcode.substr($imagename, 0, 10).date('Ymd_His').$ext;
			$folder_destination = 'pict/icon/';

			move_uploaded_file($_FILES['image']['tmp_name'], $upload_base.$folder_destination.$image_new_name) or die('upload failed.');
			$image = $web_url.'/'.$folder_destination.$image_new_name;

		}else{
			echo'<script>alert("Data not saved.\\nInvalid file.");</script>';
		}
	}
		
	try {
	    if(!empty($name)){
    		$stmt = $db->prepare("INSERT INTO `termheadline`(`termheadlineoid`, `hoteloid`, `term_icon_src`, `icon_title`, `title`, `description`) VALUES (NULL,:a,:b,:c,:d,:e)");
    		$stmt->execute(array(':a' => $hoteloid,':b' => $image, ':c' => $name, ':d' => $name, ':e' => $description));
    		$promotionoid = $db->lastInsertId();
	    }else{
	        echo'<script>alert("Data not saved.\\nPlease type the title of headline icon.");</script>';
	    }
	}catch(PDOException $ex) {
		echo "Invalid Query";
		print($ex);
		die();
	}
	
	header("Location: ". $base_url ."/headline-icon");
?>

