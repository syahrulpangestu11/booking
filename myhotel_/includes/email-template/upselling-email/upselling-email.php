<section class="content-header">
    <h1>
       	Upselling Email
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-cog"></i> Setting</a></li>
        <li>Email Template</li>
        <li class="active">Upselling Email</li>
    </ol>
</section>
<style>
.form-group input[type=text]{
  width:100%!important;
}
.preview-email{
  border: 1px solid #888;
  padding: 10px;
  font-family: sans-serif;
  font-size: 0.9em;
}
.preview-email .subject{
  font-style: italic;
  border-bottom: 1px solid #565252;
  padding-bottom: 5px;
  margin-bottom: 10px;
}
.email-banner{
  background: #eae9e9;
  text-align: center;
  padding: 20px 10px;
}
.email-tagline{
  background: #000;
  color: #FFF;
  border: 1px solid #949494;
  text-align: center;
  font-size: 1.1em;
  padding: 10px;
  margin-bottom: 10px;
}
.email-detail-booking {
  background: #eae9e9;
  padding: 15px 10px;
  margin: 8px 0;
  font-style: italic;
  text-align: center;
}
</style>
<script type="text/javascript">
$(function() {
  $(document).ready(function() {
    console.log("ready!");
  });

	$('textarea#html-box').trumbowyg({
		btns: ['viewHTML',
		  '|', 'btnGrp-design',
		  '|', 'link',
		  '|', 'btnGrp-justify',
		  '|', 'btnGrp-lists'],
		fullscreenable: false,
    autogrow: true
	});

  $('body').on('keyup','input[name=subject]', function(e){
    $('span.subject-email').html($(this).val());
  });

  $('textarea[name=header]').trumbowyg().on('tbwchange ', function(){
    $('div.email-header').html($(this).trumbowyg('html'));
  });

  $('textarea[name=footer]').trumbowyg().on('tbwchange ', function(){
    $('div.email-footer').html($(this).trumbowyg('html'));
  });
});
</script>
<?php
  $stmt = $db->prepare("select * from emailupselling where hoteloid = :a");
  $stmt->execute(array(':a' => $_SESSION['_hotel']));
  $result = $stmt->fetch(PDO::FETCH_ASSOC);
?>
<section class="content">
  <form method="post" class="form-horizontal" action="<?=$base_url?>/email-template/upselling-email-save">
    <div class="box box-alert">
      <div class="box-body">
        <div class="row">
          <div class="col-md-12">
            <h1><i class="fa fa-cog"></i> General Setting</h1>
          </div>
        </div>
        <br>
        <div class="form-group">
          <label class="col-md-3">Activation Fiture</label>
          <div class="col-md-3">
            <label class="switch">
              <input type="checkbox" name="upselling-email-activate" value="y" <?php if($result['active'] == "y"){ echo "checked"; } ?>>
              <span class="slider round"></span>
            </label>
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-3">Temporary booking will be saved for</label>
          <div class="col-md-3">
            <div class="input-group">
              <input type="text" class="form-control" name="temporary-time" value="<?=$result['temporarytime']?>">
              <div class="input-group-addon"><i class="fa fa-clock-o"></i> minutes before expired</div>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-3">Email Upselling will be sent</label>
          <div class="col-md-4">
            <div class="input-group">
              <input type="text" class="form-control" name="mailing-time" value="<?=$result['mailingtime']?>">
              <div class="input-group-addon"><i class="fa fa-clock-o"></i> minutes before temporary periode end</div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="box box-alert">
      <div class="box-body">
        <div class="row">
          <div class="col-md-12">
            <h1><i class="fa fa-envelope"></i> Email Template</h1>
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-md-12">Subject</label>
              <div class="col-md-12">
                <input type="text" class="form-control" name="subject" value="<?=$result['subject']?>">
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-12">Tagline</label>
              <div class="col-md-12">
                <input type="text" class="form-control" name="tagline" value="<?=$result['mailtagline']?>">
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-12">[Body] Header</label>
              <div class="col-md-12">
                <textarea id="html-box" name="header"><?=$result['mailbodyheader']?></textarea>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-12">[Body] Footer</label>
              <div class="col-md-12">
                <textarea id="html-box" name="footer"><?=$result['mailbodyfooter']?></textarea>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <label><i class="fa fa-eye"></i> Preview Email</label>
            <div class="preview-email">
              <div class="subject">
                <div><b>Subject : </b><span class="subject-email"><?=$result['subject']?></span></div>
                <div><b>To : </b>example@email.com</div>
              </div>
              <div class="email-banner"><span style="font-size:1.5em;"><i class="fa fa-image"></i></span><br>[IMG Banner]</div>
              <div class="email-tagline"><?=$result['mailtagline']?></div>
              <div class="email-header"><?=$result['mailbodyheader']?></div>
              <div class="email-detail-booking">Guest booking detail.</div>
              <div class="email-footer"><?=$result['mailbodyfooter']?></div>
            </div>
          </div>
        </div>
        <br>
        <div class="form-group">
          <div class="col-md-12">
            <button type="submit" class="btn btn-primary">Save</button>
            <a href="<?=$base_url?>/email-template"><button type="submit" class="btn btn-danger">Cancel</button></a>
          </div>
        </div>
    </div>
  </form>
</section>
