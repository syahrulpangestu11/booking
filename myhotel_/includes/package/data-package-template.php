<button type="button" class="pure-button green cancel"><< Back to Package</button>

<?php
	include("../../conf/connection.php");

	$main_query = "select * FROM `package_template` ";
	$filter = array();
	if(isset($_REQUEST['name']) and $_REQUEST['name']!=''){
		array_push($filter, 'name like "%'.$_REQUEST['name'].'%"');
	}
	array_push($filter, 'publishedoid not in (3)');
	if(count($filter) > 0){
		$combine_filter = implode(' and ',$filter);
		$query = $main_query.' where '.$combine_filter;
	}else{
		$query = $main_query;
	}
	try {
		$stmt = $db->query($query);
		$row_count = $stmt->rowCount();
?>
<table class="table promo-table">
    <tr>
        <th align="left" style="padding:7px;">Image</th>
        <th align="left" style="padding:7px;">Package Name</th>
        <th align="left" width="30%" style="padding:7px;text-transform:none;">Description</th>
        <th align="left" style="padding:7px;">Stay</td>
        <th align="left" style="padding:7px;">Status</td>
        <th class="algn-right">&nbsp;</th>
    </tr>
<?php
		if($row_count > 0) {
			$r_promo = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_promo as $row){
				$published = ($row['publishedoid']==1) ? "Active" : "Inactive" ;
				if(!empty($row['packageimage'])){
				    $img = '<img src="'.$row['packageimage'].'" style="height:75px">';
				}else{
				    $img = '-';
				}
?>
    <tr>
        <td><?php echo $img; ?></td>
        <td><?php echo $row['name']; ?></td>
        <td><?php echo $row['description']; ?></td>
        <td><?php echo $row['stay']; ?> night</td>
        <td><?php echo $published; ?></td>
        <td class="algn-right">
        <button type="button" class="pure-button single copy blue" pid="<?php echo $row['package_templateoid']; ?>">Copy</button>
        </td>
    </tr>
<?php
			}
		}
?>
</table>
<?php
	}catch(PDOException $ex) {
		echo "Invalid Query";
		print($ex);
		die();
	}
?>
