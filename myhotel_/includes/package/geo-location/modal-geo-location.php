<div class="modal fade" id="geoModalCountry" tabindex="-1" role="dialog" aria-labelledby="geoModalCountry">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus-circle"></i> Add Country for Promotion</h4>
            </div>
            <div class="modal-body">
                * System will add data in active page only.
                <form method="post" id="geo-location-country" class="form-inline" enctype="multipart/form-data" action="#">
                    <div class="row">
                        <div id="list"></div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="assign-country" value="show">Select Country</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="geoModalCountryNoShow" tabindex="-1" role="dialog" aria-labelledby="geoModalCountryNoShow">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus-circle"></i> Add No Show Country for Promotion</h4>
            </div>
            <div class="modal-body">
                * System will add data in active page only.
                <form method="post" id="geo-location-country-noshow" class="form-inline" enctype="multipart/form-data" action="#">
                    <div class="row">
                        <div id="list"></div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="assign-country" value="noshow">Select Country</button>
            </div>
        </div>
    </div>
</div>
