<script type="text/javascript">
$(function(){
	
	$(document).ready(function(){
		checkExistedRatePlan();
	});
	
    $('#RatePlanModal').on('show.bs.modal', function (e) {
		$.ajax({
			url		: '<?php echo $base_url; ?>/includes/package/modal/rqst-action.php',
			type	: 'post',
			data	: $('div#assigned-rateplan :input').serialize() + '&request=add-rateplan',
			success	: function(response){
				$('#RatePlanModal div.modal-body div.form-group').html(response);
			}
		});
	});
	
	$('body').on('click', '#RatePlanModal button#assign-rateplan', function (e){
		var forminput = $('#RatePlanModal').find('form#form-assign-rateplan');
		
		$.ajax({
			url		: '<?php echo $base_url; ?>/includes/package/modal/rqst-action.php',
			type	: 'post',
			data	: forminput.serialize() + '&request=assign-rateplan',
			success	: function(response){
				$('div#assigned-rateplan').append(response);
			}
		});
		
		checkExistedRatePlan();
		$('.modal').modal('hide');
	});
	
	$('body').on('click', 'button.remove-rateplan', function (e){
		$(this).parent().parent().parent().parent('div.rateplan').remove();
	});

	$(document).on('focus',".from, .to", function(){
		$('.from, .to').datepicker({
            defaultDate: "+1",
            changeMonth: true,
            dateFormat: 'dd MM yy',
            numberOfMonths: 1,
            onClose: function(selectedDate) {
				if($(this).hasClass('from')){
					var theOtherDate = $(this).parent().parent().find('.to');
					$(theOtherDate).datepicker("option", "minDate", selectedDate);
				}else{
					var theOtherDate = $(this).parent().parent().find('.from');
					$(theOtherDate).datepicker("option", "maxDate", selectedDate);    
				}
            }
        });
	});
	
	$('body').on('click', 'button.add-rate', function (e){
		var forminput = $(this).parent('td').parent('tr').find('input');
		var table = $(this).parent('td').parent('tr').closest('tbody');
		var rateplan = $(this).attr('rp');
				
		$.ajax({
			url		: '<?php echo $base_url; ?>/includes/package/modal/rqst-action.php',
			type	: 'post',
			data	: forminput.serialize() + '&rateplan=' + rateplan + '&request=add-rate',
			success	: function(response){
				forminput.val('');
				forminput.last().val(10);
				table.append(response);
			}
		});
	});
	
	$('body').on('click', 'button.remove-rate', function (e){
		$(this).parent().parent('tr').remove();
	});
	
	function checkExistedRatePlan(){
		existrateplan = $("div#assigned-rateplan > div").length;
		if(existrateplan == 0){
			if($('div#assigned-rateplan > div.panel-notification').length == 0){
				$('div#assigned-rateplan').append('<div class="panel panel-warning panel-notification"><div class="panel-heading text-center">Select room and setting your rate to complete your package</div></div>');
			}
		}else{
			$('div#assigned-rateplan > div.panel-notification').remove();
		}
	}
	
});
</script>

<style type="text/css">
div.panel-notification{
	margin-top:15px;
}
div#assigned-rateplan > div.rateplan{
	border: 1px solid #e8e8e8;
	margin-top:5px;
}
div#assigned-rateplan > div.rateplan > div.header{
	background-color: rgb(236, 240, 241);
	font-weight: bold;
}
div#assigned-rateplan > div.rateplan > div.header, div#assigned-rateplan > div.rateplan > div.content{
	padding:5px;
	min-height:inherit;
}
div#rate table{
	background-color:#FFF;
	font-size:0.9em;
}
div#rate table tr > th, div#rate table tr > td{ text-align:center; }
div#rate table input[type=text], div#rate table input[type=number]{
	border-radius:0;
	padding:3px;
	font-size:1em;
}
</style>

<div class="modal fade" id="RatePlanModal" tabindex="-1" role="dialog" aria-labelledby="RatePlanModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus-circle"></i> Choose Rate Plan</h4>
      </div>
      <div class="modal-body">
        <form method="post" id="form-assign-rateplan" class="form-inline" enctype="multipart/form-data" action="#">
          <div class="form-group">
            loading ...
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="assign-rateplan">Add New Rate Plan</button>
      </div>
    </div>
  </div>
</div>