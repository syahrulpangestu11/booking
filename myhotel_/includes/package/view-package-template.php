<section class="content-header">
    <h1>
       	Package Template
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Allotments &amp; Rates</a></li>
        <li class="active">Package Template</li>
    </ol>
</section>
<section class="content">
  <div class="row">
      <div class="box">
          <div class="box-body">
              <div class="form-group">
      <form method="get" enctype="multipart/form-data" id="data-input-template" action="<?php echo $base_url; ?>/package/template/">
              <input type="hidden" name="hoteloid" value="<?php echo $hoteloid; ?>" />
                  <label>Package Template Name </label> &nbsp;
                  <input type="text" name="name" class="medium" value="<?php echo $_GET['name']; ?>" />&nbsp;&nbsp;
                  <button type="submit" class="blue-button">Find</button>
              </form>
              </div>
          </div><!-- /.box-body -->
     </div>
  </div>

    <div class="row">
        <div class="box">
        	<form method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>">
                <div id="data-box-template" class="box-body">
                    <div class="loader">Loading...</div>
                </div><!-- /.box-body -->
            </form>
       </div>
    </div>

</section>
<script type="text/javascript">
$(function(){

	function getLoadDataTemplate(){
		$.ajax({
			url: "<?php echo"$base_url"; ?>/includes/package/data-package-template.php",
			type: 'post',
			data: $('form#data-input-template').serialize(),
			success: function(data) {
				$("#data-box-template").html(data)
			}
		});
	}

	$(document).ready(function(){
		getLoadDataTemplate();
	});

});
</script>
