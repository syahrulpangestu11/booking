<script type="text/javascript">
$(function(){

	$(document).ready(function(){

		getLoadData();

		$('input[type=radio][name=discounttype]:checked, input[type=radio][name=discountapply]:checked, input[type=radio][name=deposit]:checked, input[type=radio][name=link-style]:checked').each(function(){
			if($(this).prop("checked") == true){
				$(this).click();
			}
		});
		$('input[type="checkbox"][sign="rateplan"]').each(function(){
			if($(this).prop( "checked" ) == true){
				$(this).prop('checked',false);
				$(this).click();
			}
		});
	});

	$('body').on('click','input[type=radio][name=discounttype]', function(e) {

		if($(this).val() == '2'){
			$('li.applybox').css('display','none');
		}else{
			$('li.applybox').css('display','block');
		}

		if($(this).prop( "checked" ) == true){
			label = $(this).attr('label');
			$('input[type=text][name=discountvalue]').next('span.label').html(label);
		}
	});

	$('body').on('click','button.change-published', function(e) {
		id = $(this).attr("pid");
		old_publishedoid = $(this).attr("current-publishedoid");
		$.ajax({
			url: "<?php echo $base_url; ?>/includes/package/change-published-package.php",
			type: 'post',
			data: {
				id : id,
				old_publishedoid : old_publishedoid
			},
			success: function(data) {
				var callback = '<?php echo $base_url; ?>/package';
				$(location).attr("href", callback);
			}
		});
	});

	$('body').on('click','button.edit', function(e) {
		pid = $(this).attr("pid");
		url = "<?php echo $base_url; ?>/package/edit/?pid="+pid;
      	$(location).attr("href", url);
	});

	$('body').on('click','button.add', function(e) {
		pid = $(this).attr("pid");
		url = "<?php echo $base_url; ?>/package/add";
      	$(location).attr("href", url);
	});

	$('body').on('click','button.add-package', function(e) {
		pid = $(this).attr("pid");
		url = "<?php echo $base_url; ?>/package/add";
      	$(location).attr("href", url);
	});

	$('body').on('click','button.cancel', function(e) {
		url = "<?php echo $base_url; ?>/package";
      	$(location).attr("href", url);
	});

	$('body').on('click','button.template', function(e) {
		url = "<?php echo $base_url; ?>/package/template";
      	$(location).attr("href", url);
	});

	$('body').on('click','button.copy', function(e) {
		pid = $(this).attr("pid");
		url = "<?php echo $base_url; ?>/package/add/?tid="+pid;
      	$(location).attr("href", url);
	});

   var $dialogDelete = $('<div id="dialog-del"></div>')
	.html('Are you sure you want to delete this data?')
	.dialog({
		autoOpen: false,
		title: 'Confirmation',
		buttons: {
			"Delete": function(){
				var url = $(this).data('url');
				var id = $(this).data('id');
				$(this).dialog("close");
				deleteAction(url, id);
			},
			Cancel: function(){ $( this ).dialog("close"); }
		}
	});

	var $dialogNotification = $('<div id="dialog-notice"></div>')
	.dialog({
		autoOpen: false,
		title: 'Notification',
		buttons: {
			"Ok": function(){
				var callback = '<?php echo $base_url; ?>/package';
				$(this).dialog( "close" );
				$(location).attr("href", callback);
			}
		}
	});

	function deleteAction(url, id){
		$.ajax({
			url: url,
			type: 'post',
			data: { id : id},
			success: function(data) {
				if(data == "1"){
					$dialogNotification.html("Data has been deleted");
					$dialogNotification.dialog("open");
				}else{
					$dialogNotification.html("We&rsquo;re very sorry, we can't save your data right now");
					$dialogNotification.dialog("open");
				}
			}
		});
	}

	$('body').on('click','button.delete', function(e) {
		id = $(this).attr("pid");
		$dialogDelete.data("url", "<?php echo $base_url; ?>/includes/package/delete-package.php");
		$dialogDelete.data("id", id);
		$dialogDelete.dialog("open");
	});

	$('body').on('click','input[type="radio"][name=discountapply]', function(e) {
		allElem = $('div.applyval');
		allElem.css('display','none');

		if($(this).val() == '2'){
			$('input[type="checkbox"][name="applyvalue[]"]').prop('disabled',true);
			$('input[type="text"][name="applyvalue"]').prop('disabled',false);
		}else{
			$('input[type="checkbox"][name="applyvalue[]"]').prop('disabled',false);
			$('input[type="text"][name="applyvalue"]').prop('disabled',true);
		}

		if($(this).val() == '2' || $(this).val() == '3' ){
			applyval = $(this).next('div.applyval');
			if($(this).prop( "checked" ) == true){
				applyval.css('display','block');
			}else{
				applyval.css('display','none');
			}
		}
	});

	$('body').on('click','input[type="radio"][name=deposit]', function(e) {
		allElem = $('div.depositval');
		allElem.css('display','none');


		if($(this).val() == '2' || $(this).val() == '3'  || $(this).val() == '4' ){
			applyval = $(this).next('div.depositval');
			if($(this).prop( "checked" ) == true){
				applyval.css('display','block');
			}else{
				applyval.css('display','none');
			}
		}

		$('input[type="text"][name="depositvalue"]').prop('disabled',true);
		$(this).next('div.depositval').children('input[type="text"][name="depositvalue"]').prop('disabled',false);
	});

	function getLoadData(){
		$.ajax({
			url: "<?php echo"$base_url"; ?>/includes/package/data.php",
			type: 'post',
			data: $('form#data-input').serialize(),
			success: function(data) {
				$("#data-box").html(data)
			}
		});
	}
	/*
	$('body').on('click','input[type="checkbox"][name="roomtype[]"]', function(e) {
		rt_channel = $(this).parent().children('ul.roomtype-channel');
		li_rt = $(this).parent();
		if($(this).prop( "checked" ) == true){
			rt_channel.css('display','block');
			li_rt.addClass('selected')
		}else{
			rt_channel.css('display','none');
			li_rt.removeClass('selected')
		}
	});
	*/
	$('body').on('click','input:radio[name="select"]', function(e) {
		if($(this).val() == 'all'){
			$('input[type="checkbox"][sign="rateplan"], .mutliSelect input[type="checkbox"]').each(function(){
				if($(this).prop( "checked" ) == false){
					$(this).click();
				}
			});
		}else{
			$('input[type="checkbox"][sign="rateplan"], .mutliSelect input[type="checkbox"]').each(function(){
				if($(this).prop( "checked" ) == true){
					$(this).click();
				}
			});
		}
	});
	$('body').on('click','input[type="checkbox"][sign="rateplan"]', function(e) {
		dl_dropdown = $(this).parent().children('dl.dropdown');
		if($(this).prop( "checked" ) == true){
			dl_dropdown.css('display','block');
		}else{
			dl_dropdown.css('display','none');
		}
	});

	$(".dropdown dt a").on('click', function () {
	  $(this).parent().parent().children("dd").children("ul").slideToggle('fast');
	});

	$('.mutliSelect input[type="checkbox"]').on('click', function () {
		val = $(this).attr('val');
		var title = $(this).closest('.mutliSelect').find('input[type="checkbox"]').attr('val'),
		  title = $(this).attr('val') + ",",
		  box = $(this).parent().parent().parent().parent().children('dt').children('a').children('.multiSel'),
		  hida = $(this).parent().parent().parent().parent().children('dt').children('a').children('.hida'),
		  dropdown_dt = $(this).parent().parent().parent().parent().children('dt').children('a');

		  if ($(this).is(':checked')) {
			  var html = '<span title="' + val + '">' + title + '</span>';
			  box.append(html);
			  hida.hide();
		  } else {
			  box.children('span[title="' + val + '"]').remove();
			  var ret = hida;
			  dropdown_dt.append(ret);
		  }
	});

	$('input[name=night]').on('change', function(){
		night = $(this).val();
		$('[data-night]').html(night);
	});

	$('body').on('click','input[name=link-style]', function(e) {
		textarea = $('textarea[name="direct-link"]');
		urlpromotion = 'https://thebuking.com/ibe/index.php?hcode=<?=$hcode?>&night='+encodeURI($('input[name=stay]').val())+'&package='+encodeURI($('input[name=name]').val())+'&requirementcheckin='+encodeURI($('input[name=min_ci]').val());
		var snurl = "";
		if($(this).val() == 'primary'){
			snurl = urlpromotion + '&show=package_only';
		}else{
			snurl = urlpromotion + '&show=all';
		}
		textarea.val(snurl);

		snurl = snurl + "&utm_source=" + encodeURI($('input[name="utm_source"]').val()) + "&utm_campaign=" + encodeURI($('input[name="utm_campaign"]').val());
		textarea2 = $('textarea[name="direct2-link"]');
		textarea2.val(snurl);
	});

	$('body').on('keyup','.utmset', function(e) {
		textarea = $('textarea[name="direct-link"]');
		var snurl = textarea.val();
		snurl = snurl + "&utm_source=" + encodeURI($('input[name="utm_source"]').val()) + "&utm_campaign=" + encodeURI($('input[name="utm_campaign"]').val());
		textarea2 = $('textarea[name="direct2-link"]');
		textarea2.val(snurl);
	});

	$('body').on('click','button.copy-link', function(e) {
		var $temp = $("<input>");
		$("body").append($temp);
		$temp.val($('textarea[name="direct-link"]').val()).select();
		document.execCommand("copy");
		$temp.remove();
	});
	
	$('body').on('click','button.copy2-link', function(e) {
		var $temp = $("<input>");
		$("body").append($temp);
		$temp.val($('textarea[name="direct2-link"]').val()).select();
		document.execCommand("copy");
		$temp.remove();
	});

	$('body').on('click','button.copy-link-list', function(e) {
		var thisLink = $(this).attr('link');
		var $temp = $("<input>");
		$("body").append($temp);
		$temp.val(thisLink).select();
		document.execCommand("copy");
		$temp.remove();
	});

	function readURL(input, target) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
				target.attr('src', e.target.result).show();
			};
			reader.readAsDataURL(input.files[0]);
		}
	}

	$('body').on('change','input[name=image]', function(e){
		box_preview_image = $('div.preview-image');
		if(this.files[0].size <= 0){
			box_preview_image.html('<div class="panel panel-warning"><div class="panel-heading">N/A Image for Package</div></div>');
		}else{
			var ext = $(this).val().split('.').pop().toLowerCase();
			if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
				box_preview_image.html('<div class="panel panel-warning"><div class="panel-heading">Invalid extension! Please upload image with JPEG, PNG, JPG, and GIF type only.</div></div>');
			}else{
				box_preview_image.html('<img src="#" id="preview-image" class="img-responsive" alt="Responsive image" style="width:200px;">');
				target = $('div.preview-image').children('img');
				readURL(this, target);
			}
		}
	});

	$('body').on('change','select[name=typepackage]', function(e){
		if($(this).val() == "2"){
			$('div.long-stay-feature').css('display', 'block');
		}else{
			$('div.long-stay-feature').css('display', 'none');
		}
	});

});
</script>
<style type="text/css">
div.triple{
	padding:0 7px;
	border:3px solid #fff;
}
div.triple:first-child{
	background-color:#f6fbfa;
}
div.triple:nth-of-type(2){
	background-color:#fff7f8;
}
div.triple:nth-of-type(3){
	background-color:#eaffe9;
}
</style>
<?php
	$listpackagetype = array(1 => 'Bundled Package', 2 => 'Long Stay Package', 3 => 'Basic Package with Daily Rate')
?>
