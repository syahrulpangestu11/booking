<body class="login">
<div id="header">
    <div class="left">
    	<div class="main-logo"><a href="https://thebuking.com"><img src="<?php echo"$base_url"; ?>/images/logo.png" alt="TheBuking"></a></div>
        <ul>
        	<li><a href="https://thebuking.com">THEBUKING.COM</a></li>
        </ul>
    </div>
    <div class="clear"></div>
</div>
<div id="container">
	<div class="relative">
        <div id="loginform" class="relative">
            <h2>Reset Password</h2>
    		<a class="close" href="<?php echo"$base_url"; ?>" title="Back to home"><i class="fa fa-times"></i></a>
            <p>Please enter your login and email address and you will receive an email with the instructions to reset your password</p>
            <form method="post" action="<?php echo"$base_url"; ?>/resetprocess">
                <div class="form-group">
                    <input type="text" class="textbox" name="user" placeholder="Username" autocomplete="off">
                </div>
                <div class="form-group">
                    <input type="text" class="textbox" name="email" placeholder="Email" autocomplete="off">
                </div>
                <div class="form-group">
                    <input type="submit" class="bt-submit" value="Reset">
                </div>
            </form>
            <div class="form-group font-small" style="padding-top:10px">
                <a href="<?php echo"$base_url"; ?>/login">Back to Login</a>
            </div>
        </div>
    </div>
</div>
<script>
	$(function() {
		$("#loginform form").validate({
			rules: {
			   user: "required", email: { required: true, email: true }
			}
		});
	});
</script>
</body>