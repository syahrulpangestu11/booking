<script type="text/javascript">
$(function(){
   $("#dialog-error, #dialog-success").dialog({
      autoOpen: false,
	  buttons: { "Ok": redirect },
	  close: function(event, ui) { redirect }
   });
   
   function redirect(){
	   $(location).attr("href", "<?php echo $base_url; ?>/dashboard");
   }
});
</script>

<div id="dialog-error" title="Error Notification">
  <p>We&rsquo;re very sorry, we can't save your data right now.<br />Please make sure you upload image file type (ex: .jpg , .png , .gif)</p>
</div>
<div id="dialog-success" title="Confirmation Notice">
  <p>Data succesfully updated</p>
</div>

<?php
	$username = $_POST['username'];
	$displayname = $_POST['displayname'];
	$password = $_POST['password'];
	$currentpassword = $_POST['currentpassword'];

	$allowedExts = array("jpg", "jpeg", "gif", "png");
	$tmp = explode(".", $_FILES["photos"]["name"]);
	$extension = end($tmp);
	
	if(!empty($_FILES["photos"]["name"]) or $_FILES["photos"]["name"]!=''){
		
		$filetype = $_FILES["photos"]["type"] ;
		$filename = str_replace(" ", "_", $_FILES["photos"]["name"]);
		$filename = str_replace(".".$extension, "", $filename);
		$filename = substr($filename, 0, 20);
		$uploadedname = $filename."-".date("Y_m_d_H_i_s");
		$ext = ".".strtolower($extension);
			
		if ((($filetype == "image/gif") || ($filetype == "image/jpeg") || ($filetype == "image/png") || ($filetype == "image/pjpeg")) && in_array($extension, $allowedExts)){
			if ($_FILES["photos"]["error"] > 0){
			?>
                <script type="text/javascript">
                	$(function(){ $( document ).ready(function(){ $("#dialog-error").dialog("open"); }); });
                </script>
            <?php
			}else{
				include"compress-image.php";
				$show_thumbnail_path = $web_url."/".$folder.$imagename_t;
				try {
					
					if(!empty($password) and !empty($currentpassword)){
						$stmt = $db->prepare("SELECT * FROM users inner join userstype using (userstypeoid) WHERE username=:usr AND password=SHA1(:pwd)");
						$stmt->execute(array(':usr' => $username, ':pwd' => $password));
						$row_count = $stmt->rowCount();
							
						if($row_count > 0) {
							$stmt = $db->prepare("update users set password=SHA1(:pwd), displayname = :a, picture = :b where username=:usr");
							$stmt->execute(array(':usr' => $username, ':pwd' => $currentpassword, ':a' => $displayname , ':b' => $show_thumbnail_path));
						}
					}else{
						$stmt = $db->prepare("update users set displayname = :a, picture = :b where username=:usr");
						$stmt->execute(array(':a' => $displayname , ':b' => $show_thumbnail_path , ':usr' => $username));
					}
					$_SESSION['_userpict'] = $show_thumbnail_path;
					$_SESSION['_initial'] = $displayname;
				?>
                    <script type="text/javascript">
                   		$(function(){ $( document ).ready(function(){ $("#dialog-success").dialog("open"); }); });
                    </script>
                <?php	
				}catch(PDOException $ex) {
				?>
					<script type="text/javascript">
                        $(function(){ $( document ).ready(function(){ $("#dialog-error").dialog("open"); }); });
                    </script>
                <?php	
				}
		
			}
		}else{
		?>
			<script type="text/javascript">
                $(function(){ $( document ).ready(function(){ $("#dialog-error").dialog("open"); }); });
            </script>
		<?php
		}
			
	}else{
		try {
			if(!empty($password) and !empty($currentpassword)){
				$stmt = $db->prepare("SELECT * FROM users inner join userstype using (userstypeoid) WHERE username=:usr AND password=SHA1(:pwd)");
				$stmt->execute(array(':usr' => $username, ':pwd' => $password));
				$row_count = $stmt->rowCount();
					
				if($row_count > 0) {
					$stmt = $db->prepare("update users set password=SHA1(:pwd), displayname = :a where username=:usr");
					$stmt->execute(array(':usr' => $username, ':pwd' => $currentpassword, ':a' => $displayname));
				}
			}else{
				$stmt = $db->prepare("update users set displayname = :a where username=:usr");
				$stmt->execute(array(':a' => $displayname, ':usr' => $username));
			}
		?>
			<script type="text/javascript">
				$(function(){ $( document ).ready(function(){ $("#dialog-success").dialog("open"); }); });
			</script>
		<?php	
		}catch(PDOException $ex) {
		?>
			<script type="text/javascript">
				$(function(){ $( document ).ready(function(){ $("#dialog-error").dialog("open"); }); });
			</script>
		<?php	
		}
	}
?>