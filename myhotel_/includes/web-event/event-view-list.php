<style media="screen">
.pure-button > i.fa, .small-button > i.fa {
	margin-right: 0px;
}
.color-green {color:#5fbeaa;}
.color-orange {color: #F59226;}
.color-red {color:#E02726;}
.color-purple {color:#b198dc;}

.dialog-detail {font-size: 12px;}
.dialog-detail table tr td {vertical-align: top;}
</style>

<?php
include('conf/connection-thebukingcom.php');
include("includes/paging/pre-paging.php");

// $event_region = "jogja";
$event_region = $uri3;
$s_event = "SELECT * FROM event e WHERE e.region = '".$event_region."'";
$q_event = $dbTB->query($s_event) or die(mysqli_error());
$r_event = $q_event->fetch(PDO::FETCH_ASSOC);

$eventoid = $r_event['eventoid'];
$event_name = $r_event['name'];
$event_date = date("d F Y", strtotime($r_event['date']));
$event_venue = $r_event['venue'];
$event_address = $r_event['address'];
$event_time = $r_event['time'];



	$stmt = $dbTB->query("select count(formregisteroid) as registered from formregister WHERE memberof_yes = 'no' AND eventoid = '".$eventoid."'");
	$registered = $stmt->fetch(PDO::FETCH_ASSOC);

	$stmt = $dbTB->query("select count(formregisteroid) as member from formregister where memberof_yes = 'yes' AND eventoid = '".$eventoid."'");
	$member = $stmt->fetch(PDO::FETCH_ASSOC);

	$stmt = $dbTB->query("select count(formregisteroid) as paid from formregister where memberof_yes = 'yes' AND sendemail = '2' AND eventoid = '".$eventoid."'");
	$paid = $stmt->fetch(PDO::FETCH_ASSOC);

	$stmt = $dbTB->query("select count(formregisteroid) as confirmed from formregister where sendemail = '3' AND eventoid = '".$eventoid."'");
	$confirmed = $stmt->fetch(PDO::FETCH_ASSOC);
?>

<section class="content-header">
    <h1>
       	Event
    </h1>
    <ol class="breadcrumb">
        <li class="active"><a href="#"><i class="fa fa-table"></i> Event</a></li>
    </ol>
</section>
<section class="content">
	<!-- <div class="row">
			<div class="box">
				sdf
			</div>
	</div> -->

    <div class="row">
        <div class="box">
            <div id="data-box" class="box-body">
            <form method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/web-event/send-email">
            <input type="hidden" name="id" value="" />
<h1>Event : <?=$event_name;?></h1>
<h2>
	<i class="fa fa-user"></i> Registered : <?php echo $registered['registered']; ?>
	&nbsp;&nbsp;/&nbsp;&nbsp;
	<i class="fa fa-users"></i> Registered Member : <?php echo $member['member']; ?> &nbsp;(<?php echo $paid['paid']; ?> paid)
	&nbsp;&nbsp;/&nbsp;&nbsp;
	<i class="fa fa-envelope-o"></i> Confirmed : <?php echo $confirmed['confirmed']; ?>
</h2>
<table class="table promo-table">
	<tr>
		<td>No.</td>
		<td>Registration Date</td>
		<td>Name</td>
		<td>Hotel</td>
		<td>Position Title</td>
		<td>Email</td>
    <td>Mobile</td>
		<td>Status</td>
    <td style="width: 130px;">Action</td>
	</tr>
	<?php
	$nomor = $no-1;
	$main_query = "select * from formregister WHERE eventoid = '".$eventoid."' order by submitdate desc";
	$stmt = $dbTB->query($main_query.$paging_query);
	// $stmt = $dbTB->query($main_query);
	$row_count = $stmt->rowCount();
	if($row_count > 0) {
		$r_registered = $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach($r_registered as $data){
					$submitdate = date('d/M/Y', strtotime($data['submitdate']));
					$submitdate_1 = date('d F Y, H:i:s', strtotime($data['submitdate']));
					if(!empty($data['top_ota_1'])){ $top_ota = $data['top_ota_1']; }
					if(!empty($data['top_ota_2'])){ $top_ota .= ", ".$data['top_ota_2']; }
					if(!empty($data['top_ota_3'])){ $top_ota .= ", ".$data['top_ota_3']; }
					if(!empty($data['top_ota_4'])){ $top_ota .= ", ".$data['top_ota_4']; }
					if(!empty($data['top_ota_5'])){ $top_ota .= ", ".$data['top_ota_5']; }

					switch ($data['sendemail']) {
						case 0:
							if($data['memberof_yes']=="yes"){
								$status = "<i>Member Registered</i>";
							}else{
								$status = "Registered";
							}
							break;
						case 1:
							if($eventoid==1){
								$status = '<b class="color-green"><i class="fa fa-check"></i> Confirmed</b>';
							}else{
								$status = '<b class="color-purple"><i class="fa fa-dollar"></i> Paid</b>';
							}
							break;
						case 2: $status = '<span class="color-red"><i class="fa fa-times"></i> Rejected</span>'; break;
						case 3: $status = '<b class="color-green"><i class="fa fa-check"></i> Confirmed</b>'; break;
						default: $status = "Registered"; break;
					}

					if($eventoid==1){
						$r_ep = array(
							'title' => $data['title'],
							'name' => $data['name'],
							'position_title' => $data['position_title'],
							'official_email' => $data['official_email'],
							'private_email' => $data['private_email'],
							'mobile_phone' => $data['mobile_phone']
						);
					}else{
						$s_ep = "select ep.* FROM eventparticipant ep WHERE ep.formregisteroid = '".$data['formregisteroid']."'";
						$q_ep = $dbTB->query($s_ep);
						$r_ep = $q_ep->fetch(PDO::FETCH_ASSOC);

						$q_ep_all = $dbTB->query($s_ep);
						$r_ep_all = $q_ep_all->fetchAll(PDO::FETCH_ASSOC);
					}
	?>
	<tr>
		<td><?php echo ++$nomor; ?></td>
		<td><?php echo $submitdate; ?></td>
		<td><?php echo $r_ep['name']; ?></td>
		<td><?php echo $data['hotel']; ?></td>
		<td><?php echo $r_ep['position_title']; ?></td>
		<td style="text-transform:lowercase"><?php echo $r_ep['official_email']; ?></td>
    <td><?php echo $r_ep['mobile_phone']; ?></td>
		<td><?php echo $status; ?></td>
    <td>
		<button type="button" class="detail-button small-button blue" id="<?php echo $data['formregisteroid']; ?>"><i class="fa fa-list"></i></button>
    <?php
		/*
		if($data['sendemail'] == 0){
			?>
			<button type="button" class="send-no-button small-button red" id="<?php echo $data['formregisteroid']; ?>"><i class="fa fa-ban"></i></button>
			<!-- <button type="button" class="small-button orange"><i class="fa fa-dollar"></i> Pending</button> -->
    	<?php
			if($data['memberof_yes']=="no"){
				?>
				<button type="button" class="send-button small-button green" id="<?php echo $data['formregisteroid']; ?>"><i class="fa fa-envelope"></i></button>
				<?php
			}
		}else if($data['sendemail'] == 1){
			?>
			<button type="button" class="send-button small-button green" id="<?php echo $data['formregisteroid']; ?>"><i class="fa fa-envelope"></i></button>
			<!-- <b class="color-green"><i class="fa fa-dollar"></i> Done</b> -->
			<!-- <button type="button" class="small-button purple"><i class="fa fa-dollar"></i> Done</button> -->
			<?php
		} */ ?>

		<button type="button" class="send-no-button small-button red" id="<?php echo $data['formregisteroid']; ?>"><i class="fa fa-ban"></i></button>
		<button type="button" class="send-button small-button green" id="<?php echo $data['formregisteroid']; ?>"><i class="fa fa-envelope"></i></button>


		<!-- Pop Up Detail -->
		<div id="dialog-detail-<?=$data['formregisteroid'];?>" class="dialog-detail" title="Registered User Detail" style="display: none;">
			Register Date : <b><?=$submitdate_1;?></b><br><br>
			<div class="this-inline-block">

				<table>
					<tr>
						<td colspan="3"><b><u>Hotel Information</u></b></td>
					</tr>
					<tr>
						<td>Hotel</td>
						<td>:</td>
						<td><?=$data['hotel'];?></td>
					</tr>
					<tr>
						<td>Hotel Chain</td>
						<td>:</td>
						<td><?=$data['hotel_chain'];?></td>
					</tr>
					<tr>
						<td>Address</td>
						<td>:</td>
						<td><?=$data['hotel_address']." - ".$data['city'];?></td>
					</tr>
					<tr>
						<td>Phone / Fax</td>
						<td>:</td>
						<td><?=$data['phone']." / ".$data['fax'];?></td>
					</tr>
					<tr>
						<td>Website</td>
						<td>:</td>
						<td><?=$data['website'];?></td>
					</tr>
					<tr>
						<td>Corporate Hotel</td>
						<td>:</td>
						<td><?=$data['n_corporate_hotel']." (".$data['list_corporate_hotel'].")";?></td>
					</tr>
				</table>

				<br>

				<table>
					<tr>
						<td colspan="3"><b><u>Other Information</u></b></td>
					</tr>
					<tr>
						<td>Current Booking Engine</td>
						<td>:</td>
						<td>
							<?=$data['current_be'];?> (<?=$data['be_share_percentage'];?>% online share, <?=$data['be_room_night'];?> room nights)
						</td>
					</tr>
					<tr>
						<td>Current Chain Manager</td>
						<td>:</td>
						<td><?=$data['current_cm'];?></td>
					</tr>
					<tr>
						<td>Current PMS</td>
						<td>:</td>
						<td><?=$data['current_pms'];?></td>
					</tr>
					<tr>
						<td>Top OTA</td>
						<td>:</td>
						<td><?=$top_ota;?></td>
					</tr>
					<tr>
						<td>Current Internet Provider</td>
						<td>:</td>
						<td><?=$data['current_isp'];?></td>
					</tr>
				</table>

			</div>
			<!-- <br><br> -->
			<div class="this-inline-block">
				<table>
					<tr>
						<td colspan="3"><b><u>Participant Information</u></b></td>
					</tr>
					<?php
					if($eventoid==1){
						?>
						<tr>
							<td>Name</td>
							<td>:</td>
							<td><?=$data['title']." ".$data['name'];?></td>
						</tr>
						<tr>
							<td>Position</td>
							<td>:</td>
							<td><?=$data['position_title'];?></td>
						</tr>
						<tr>
							<td>Official Email</td>
							<td>:</td>
							<td><?=$data['official_email'];?></td>
						</tr>
						<tr>
							<td>Private Email</td>
							<td>:</td>
							<td><?=$data['private_email'];?></td>
						</tr>
						<tr>
							<td>Mobile</td>
							<td>:</td>
							<td><?=$data['mobile_phone'];?></td>
						</tr>
						<?php
					}else{
						foreach ($r_ep_all as $key=>$ep) {
							?>
							<tr><td colspan="3" style="color: #C52D2F;">#<?php echo $key+1;?></td></tr>
							<tr>
								<td>Name</td>
								<td>:</td>
								<td><?=$ep['title']." ".$ep['name'];?></td>
							</tr>
							<tr>
								<td>Position</td>
								<td>:</td>
								<td><?=$ep['position_title'];?></td>
							</tr>
							<tr>
								<td>Official Email</td>
								<td>:</td>
								<td><?=$ep['official_email'];?></td>
							</tr>
							<tr>
								<td>Private Email</td>
								<td>:</td>
								<td><?=$ep['private_email'];?></td>
							</tr>
							<tr>
								<td>Mobile</td>
								<td>:</td>
								<td><?=$ep['mobile_phone'];?></td>
							</tr>
							<?php
						}
					} ?>
				</table>

			</div>

		</div>

    </td>
	</tr>


	<?php
        }
	}

	?>
</table>
            </form>
            </div><!-- /.box-body -->

						<?php
						// --- PAGINATION part 2 ---
						$main_count_query = "select count(formregisteroid) as jml from formregister WHERE eventoid = '".$eventoid."'";
						$stmt = $dbTB->query($main_count_query);
						$jmldata = $stmt->fetchColumn();

						$tampildata = $row_count;
						include("includes/paging/post-paging.php");
						?>
       </div>
    </div>
</section>

<script type="text/javascript">
$(function(){
	$(".dialog-detail").dialog({
		 autoOpen: false,
		 minWidth: 900,
		 buttons: [
			 {
			 text: "Ok",
	      click: function() {
	        $( this ).dialog( "close" );
	      }
		 }
	 ]
	 //close: function(event, ui) { redirect }
	});

	$('body').on('click','button.send-button', function(e) {
		$('input[name=id]').val($(this).attr('id'));
		$('form#data-input').submit();
	});

	$('body').on('click','button.send-no-button', function(e) {
		id = $(this).attr('id');
		// $('form#data-input').submit();
		url = "<?php echo $base_url; ?>/web-event/send-no-email/?id="+id;
		$(location).attr("href", url);
	});

	$('body').on('click','button.detail-button', function(e) {
		// $('input[name=id]').val($(this).attr('id'));
		id = $(this).attr("id");
		// url = "<?php echo $base_url; ?>/web-event/detail/?id="+id;
		$("#dialog-detail-"+id).dialog("open");
		//$(location).attr("href", url);
	});

	$('body').on('click','#paging button', function(e) {
		page	= $(this).attr('page');
		url = "<?php echo $base_url; ?>/web-event/<?php echo $event_region;?>/?page="+page;
		$(location).attr("href", url);
	});

});

</script>
