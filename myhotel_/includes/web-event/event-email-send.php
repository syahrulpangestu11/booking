<script type="text/javascript">
$(function(){
   $("#dialog").dialog({
      autoOpen: false,
	  buttons: { "Ok": redirect },
	  close: function(event, ui) { redirect }
   });

   function redirect(){
	   document.location.href = '<?=$base_url?>/web-event';
   }
});
</script>
<?php
    include('conf/connection-thebukingcom.php');

	$id = $_POST['id'];
  $sintak = "
  select fr.formregisteroid, fr.submitdate, ep.*, e.name AS event_name, e.date AS event_date, e.venue AS event_venue, e.address AS event_address, e.time AS event_time
  from formregister fr INNER JOIN eventparticipant ep USING (formregisteroid) INNER JOIN event e USING (eventoid) where formregisteroid = '".$id."'";
  // echo $sintak;

	$stmt = $dbTB->query($sintak) or die(mysqli_error());
	$registration = $stmt->fetch(PDO::FETCH_ASSOC);

	$email = $registration['official_email'];
	$name = $registration['name'];
  $event_name = $registration['event_name'];
  $event_date = date("d F Y", strtotime($registration['event_date']));
  $event_venue = $registration['venue'];
  $event_address = $registration['address'];
  $event_time = $registration['time'];

  $s_ep = "select ep.* FROM eventparticipant ep WHERE ep.formregisteroid = '".$registration['formregisteroid']."'";
  $q_ep_all = $dbTB->query($s_ep) or die(mysqli_error());
  $r_ep_all = $q_ep_all->fetchAll(PDO::FETCH_ASSOC);

	try{
		ob_start();
			include 'includes/web-event/event-email-body.php';
		$body = ob_get_clean();

    // $subject = $event_name;
    $subject = "Confirmation email for The Buking Event";

		require_once('includes/mailer/class.phpmailer.php');
		$mail = new PHPMailer(true);

		//echo $body;

		$mail->IsSMTP(); // telling the class to use SMTP

		try{
		    
            $mail->SMTPDebug = 0;
            //Ask for HTML-friendly debug output
            $mail->Debugoutput = 'html';
            //Set the hostname of the mail server
            $mail->Host = "smtp.gmail.com";
            //Set the SMTP port number - likely to be 25, 465 or 587
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            //Whether to use SMTP authentication
            $mail->SMTPAuth = true;
            //Username to use for SMTP authentication
            $mail->Username = "booking@thebuking.com";//"info@thebuking.com";
            //Password to use for SMTP authentication
            $mail->Password = "Res3!4nd";//"KofiEn4k";



			$mail->SetFrom("info@thebuking.com", "The Buking");
			$mail->AddAddress($email, $name);

			$mail->Subject = $subject;
			$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
			$mail->MsgHTML($body);

			$stmt = $dbTB->query("update formregister set sendemail = '3' where formregisteroid = '".$id."'");

			$mail->Send();

			?>
    <div id="dialog" title="Confirmation Notice">
      <p>Email has been sent to <?php echo $email; ?></p>
    </div>
    <script type="text/javascript">
    $(function(){
        $( document ).ready(function() {
            $("#dialog").dialog("open");
        });
    });
    </script>
			<?php

		} catch (phpmailerException $e) {
			//echo"<b>Php Mailer Error :</b><br>"; echo $e->errorMessage();
			?>
    <div id="dialog" title="Confirmation Notice">
      <p>Email failed to send</p>
    </div>
    <script type="text/javascript">
    $(function(){
        $( document ).ready(function() {
            $("#dialog").dialog("open");
        });
    });
    </script>
			<?php
		} catch (Exception $e) {
			//echo"<b>All Error :</b><br>"; echo $e->getMessage();
			?>
    <div id="dialog" title="Confirmation Notice">
      <p>Email failed to send</p>
    </div>
    <script type="text/javascript">
    $(function(){
        $( document ).ready(function() {
            $("#dialog").dialog("open");
        });
    });
    </script>
			<?php
		}
	}catch(PDOException $ex) {
		echo"<b>All Error :</b><br>"; echo $ex;
	}
?>
