<?php
	if(empty($_REQUEST['rt']) and empty($_REQUEST['sdate']) and empty($_REQUEST['edate'])){
		$start = date("d F Y");
		$end = date("d F Y",  strtotime(date("Y-m-d")." +10 days" ));
	}else{
		$start = date("d F Y", strtotime($_REQUEST['sdate']));
		$end = date("d F Y", strtotime($_REQUEST['edate']));
		$roomoffer = $_REQUEST['rt'];
		$channeltype = $_REQUEST['channel'];
	}
		
	include("js.php");
?>
<section class="content-header">
    <h1>
        Cancellation Policy
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i>  Allotments &amp; Rates </a></li>
        <li class="active">Cancellation Policy</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="box box-form">
            <div class="box-body">
                <div class="form-group">
                    <form method="get" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/cancellation-policy/">
                    	<input type="hidden" name="ho" value="<?php echo $hoteloid; ?>" />
                        <table>
                            <tr>
                                <td>
                                    <label>Room Type</label> &nbsp;
                                    <select name="rt" class="input-select">
                                    <?php
                                        try {
                                            $stmt = $db->query("select r.roomoid, r.name, x.offernum from room r inner join hotel h using (hoteloid) left join (select count(roomofferoid) as offernum, ro.roomoid from roomoffer ro group by ro.roomoid) as x on x.roomoid = r.roomoid where h.hoteloid = '".$hoteloid."' and x.offernum > 0 and r.publishedoid = '1'");
                                            $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                            foreach($r_room as $row){
                                                echo"<optgroup label = '".$row['name']."'>";
                                                try {
                                                    $stmt = $db->query("select ro.* from roomoffer ro inner join published p using (publishedoid) inner join room r using (roomoid) inner join offertype using (offertypeoid) where r.roomoid = '".$row['roomoid']."' and p.publishedoid = '1'");
                                                    $r_offer = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                                    foreach($r_offer as $row1){
                                                        if($row1['roomofferoid'] == $roomoffer){ $selected = 'selected = "selected"';  }else{ $selected = ''; }
                                                       echo"<option value='".$row1['roomofferoid']."' ".$selected.">  ".$row1['name']."</option>";
                                                    }
                                                }catch(PDOException $ex) {
                                                    echo "Invalid Query";
                                                    die();
                                                }     
                                                echo"</optgroup>";                       
                                            }
                                        }catch(PDOException $ex) {
                                            echo "Invalid Query";
                                            die();
                                        }
                                    
                                    ?>   
                                    </select>
                                   	&nbsp;&nbsp;
                                    <label>Channel</label> &nbsp;&nbsp;
                                    <select name="ch">
                                    <?php
                                        try {
                                            $stmt = $db->query("select * from channel");
                                            $r_plan = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                            foreach($r_plan as $row){
												if($row['channeloid'] == $channeltype){ $selected = 'selected = "selected"';  }else{ $selected = ''; }
												echo"<option value='".$row['channeloid']."' ".$selected.">".$row['name']."</option>";
                                            }
                                        }catch(PDOException $ex) {
                                            echo "Invalid Query";
                                            die();
                                        }
                                    
                                    ?>        	
                                    </select> &nbsp;
                                </td> 
                            </tr>
                            <tr>
                                <td>
                                    <label>Date Range From</label> &nbsp;&nbsp;
                                    <input type="text" name="sdate" id="startdate" placeholder="" value="<?php echo $start; ?>"> &nbsp;&nbsp;
                                    <label>To</label> &nbsp;&nbsp;
                                    <input type="text" name="edate" id="enddate" placeholder="" value="<?php echo $end; ?>"> &nbsp;&nbsp;
                                    <button type="submit" class="small-button blue">Search</button>
                                </td> 
                            </tr>
                        </table>
                    </form>
                </div>
            </div><!-- /.box-body -->
       </div>
    </div>
    
    <div class="row">
        <div class="box">
        <form id="form-add" method="post" enctype="multipart/form-data" action="<?php echo $base_url; ?>/rate-control/upload">
            <table class="table table-fill table-fill-centered">
                <tr>
                    <td>Checkin In Date From</td>
                    <td>Checkin In Date Until</td>
                    <td>Cancellation Policy</td>
                    <td>Description</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="algn-center"><input type="text"class="medium" id="startcalendar" name="startdate"></td>
                    <td class="algn-center"><input type="text"class="medium" id="endcalendar" name="enddate"></td>
                    <td>
                        <select name="cancellation">
                            <option value="" selected="selected">&nbsp;</option>
                        <?php
                            try {
                                    $stmt = $db->query("select cp.* from cancellationpolicy cp inner join published p using (publishedoid) where p.publishedoid = '1' and cp.hoteloid in ('0','".$hoteloid."')");
                                $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                foreach($r_room as $row){
                                    echo"<option value='".$row['cancellationpolicyoid']."' note='".$row['description']."'>".$row['name']."</option>";
                                }
                            }catch(PDOException $ex) {
                                echo "Invalid Query";
                                die();
                            }
                        ?>        	
                        </select>
                    </td>
                    <td>
                        <p></p>	
                    </td>
                    <td><button type="button" class="small-button blue add">Add</button></td>
                </tr>
            </table> 
		</form>
       </div>
    </div>

</section>
