<script type="text/javascript">
$(function(){
	$(document).ready(function(){ getLoadData(); });
	
	function getLoadData(){
		$.ajax({
			url: "<?php echo"$base_url"; ?>/includes/cancellation-policy/data.php",
			type: 'post',
			data: $('form#data-input').serialize(),
			success: function(data) {
				$("table.table").append(data)
			}
		});
	}
	
	$('body').on('click','button.add', function(e) {
		url = '<?php echo $base_url; ?>/includes/cancellation-policy/add-cancellation-policy-save.php';
		forminput = $('form#form-add');
		submitData(url, forminput);
	});
	
	$('body').on('change','select[name=cancellation]', function(e) {
		desc = $('select[name=cancellation] option:selected').attr('note');
		p_desc = $(this).parent().parent().children('td').eq(3).children('p').html(desc);
	});
	
	function submitData(url, forminput){
		$.ajax({
			url: url,
			type: 'post',
			data: forminput.serialize(),
			success: function(data) {
				if(data == "1"){
					$dialogNotice.html("Data succesfully updated");
					$dialogNotice.dialog("open");
				}else{
					$dialogNotice.html("We&rsquo;re very sorry, we can't save your data right now");
					$dialogNotice.dialog("open");
				}
			}
		});
	}
	
	var $dialogNotice = $('<div id="dialog-notice"></div>')
    	.dialog({
    		autoOpen: false,
    		title: 'Notification',
			buttons: { 
				"Ok": function(){ 
					$( this ).dialog( "close" );
					$(location).attr("href", "<?php echo $base_url; ?>/cancellation-policy"); 
				}
			}
    	});

   var $dialogDel = $('<div id="dialog-del"></div>')
	.html('Are you sure you want to delete this data?')
	.dialog({
		autoOpen: false,
		title: 'Confirmation',
		buttons: { 
			"Delete": function(){ 
				var to = $(this).data('pid'); 
				deleteProcess(pid);
				$( this ).dialog( "close" ); 
			},
			Cancel: function(){ $( this ).dialog( "close" ); }
		}
	});

	
	$('body').on('click','button.delete-button', function(e) {
		pid = $(this).attr("pid");
		$dialogDel.data("pid", pid);
		$dialogDel.dialog("open");
	});
	
	function deleteProcess(pid){
		$.ajax({
			url: '<?php echo $base_url; ?>/includes/cancellation-policy/delete-cancellation-policy.php',
			type: 'post',
			data: { pid : pid},
			success: function(data) {
				if(data == "1"){
					$dialogNotice.html("Data has been deleted");
					$dialogNotice.dialog("open");
				}else{
					$dialogNotice.html("We&rsquo;re very sorry, we can't save your data right now");
					$dialogNotice.dialog("open");
				}
			}
		});
	}
});
</script>