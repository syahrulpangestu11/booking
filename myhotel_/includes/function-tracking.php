<?php

function TAHotelCode($hoteloid){
  global $db;
  $stmt	= $db->query("select hotelcode from hotel where hoteloid = '".$hoteloid."'");
  $result = $stmt->fetch(PDO::FETCH_ASSOC);
  return $result['hotelcode'];
}

  $hotelcode 	= TAHotelCode($hoteloid);

  $path = "../ibe/tracking-analytics/property/log_".$hotelcode."_".date('Y')."_".date('m').".xml";
  // echo "<pre>";
  // echo $path;
  // echo "<hr>";
  // print_r($_SESSION);
  // echo "</pre>";

  // ----- OLD ------
	// $path = "../ibe/tracking-analytics/log-tracking.xml";

	$xmllog = new DomDocument();
	$xmllog->preserveWhitespace = false;
	$xmllog->load($path);

	$xpath = new DomXpath($xmllog);

	$periode = "monthly";
	switch($periode){
		case "monthly" :
		$start = date('Ym').'01';
		$end = date('Ymd');
		break;
	}

	function IPWhitelist($hoteloid){
		global $db;

		$stmt = $db->prepare("SELECT * FROM ip_whitelist where hoteloid = :a");
		$stmt->execute(array(':a' => $hoteloid));
		if($stmt->rowCount() > 0){
			$filter_ip = array();
			$ipwhitelist = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($ipwhitelist as $ip){
				array_push($filter_ip, 'text() != "'.$ip['ip'].'"');
			}
			//$filter_whitelist = '/ipaddress['.implode(' and ', $filter_ip).']';
			$filter_whitelist = '/ipaddress[not(*)][not(normalize-space())]';
			//filter_whitelist_same_level = '[ipaddress['.implode(' and ', $filter_ip).']]';
			$filter_whitelist_same_level = '[ipaddress[not(*)][not(normalize-space())]]';
		}else{
			$filter_whitelist = '';
			$filter_whitelist_same_level = '';
		}

		return array($filter_whitelist, $filter_whitelist_same_level);

	}

	function in_array_multidimension($item , $array){
		return preg_match('/"'.$item.'"/i' , json_encode($array));
	}

	$pie_color = array( "#f56954", "#00a65a", "#f39c12", "#00c0ef", "#3c8dbc", "#d2d6de");
	$month_new_ta = 9;
  $year_new_ta = 2017;


  function TAPeriodeDate($start, $end){
    $startdate = new DateTime(substr($start, 0, 4)."-".substr($start, 4, 2)."-".substr($start, 6, 2));
    $enddate = new DateTime(substr($end, 0, 4)."-".substr($end, 4, 2)."-".substr($end, 6, 2));
    $diff = $enddate->diff($startdate)->format("%a") + 1;

    return array('start_year' => (int)substr($start, 0, 4), 'start_month' => (int)substr($start, 4, 2), 'end_year' => (int)substr($end, 0, 4), 'end_month' =>  (int)substr($end, 4, 2), 'numberofday' => $diff);
  }


  function getVisit($hoteloid, $page, $start, $end){
    global $month_new_ta;
    global $year_new_ta;
    $result = 0;

    $filter_ip = IPWhitelist($hoteloid);
    $query_tag = 	'//record[@hotel="'.$hoteloid.'"][@page="'.$page.'"][number(translate(@date,"-","")) >=  '.$start.' and number(translate(@date,"-","")) <=  '.$end.']'.$filter_ip[0];

    $check_periode = TAPeriodeDate($start, $end);

    if($check_periode['start_year'] <= $year_new_ta and $check_periode['start_month'] <= $month_new_ta){
      global $xpath;
      $countavailability = $xpath->query($query_tag);
      $result = $result + $countavailability->length;
    }

    if(($check_periode['start_year'] >= $year_new_ta) or ($check_periode['end_year'] <= $year_new_ta and $check_periode['end_month'] >= $month_new_ta) ){
      $hotelcode = TAHotelCode($hoteloid);
      $query_tag = 	'//record[@hotel="'.$hoteloid.'"][@page="'.$page.'"][@track="visit"][number(translate(@date,"-","")) >=  '.$start.' and number(translate(@date,"-","")) <=  '.$end.']'.$filter_ip[0];
      for($i = $check_periode['start_year']; $i <= $check_periode['end_year']; $i++){
        if($i < $check_periode['end_year']){ $limit_month = 12; }else{ $limit_month = $check_periode['end_month'];  }
        for($j = $check_periode['start_month']; $j <= $limit_month; $j++){
          $path_ta = "../ibe/tracking-analytics/property/log_".$hotelcode."_".$i."_".sprintf('%02d', $j).".xml";
          if(file_exists($path_ta)){
            $xmllog_new = new DomDocument();
            $xmllog_new->preserveWhitespace = false;
            $xmllog_new->load($path_ta);

            $xpath_new = new DomXpath($xmllog_new);
            $countavailability = $xpath_new->query($query_tag);
            $result = $result + $countavailability->length;
          }
        }
      }
    }

    return $result;
  }

  function getClick($hoteloid, $page, $start, $end){
    global $month_new_ta;
    global $year_new_ta;
    $result = 0;

    if($page == "availability-visit"){ $page = 'availability'; $track = '[@track="visit"]';
    }else if($page == "availability-hit"){ $page = 'availability'; $track = '[@track="hit"]';
    }else{ $track = ''; }

    $filter_ip = IPWhitelist($hoteloid);
    $query_tag = 	'//record[@hotel="'.$hoteloid.'"][@page="'.$page.'"][number(translate(@date,"-","")) >=  '.$start.' and number(translate(@date,"-","")) <=  '.$end.']'.$filter_ip[0];

    $check_periode = TAPeriodeDate($start, $end);

    if($check_periode['start_year'] <= $year_new_ta and $check_periode['start_month'] <= $month_new_ta){
      global $xpath;
      $countavailability = $xpath->query($query_tag);
      $result = $countavailability->length;
    }

    if(($check_periode['start_year'] >= $year_new_ta) or ($check_periode['end_year'] <= $year_new_ta and $check_periode['end_month'] >= $month_new_ta)){
      $hotelcode = TAHotelCode($hoteloid);
      $query_tag = 	'//record[@hotel="'.$hoteloid.'"][@page="'.$page.'"]'.$track.'[number(translate(@date,"-","")) >=  '.$start.' and number(translate(@date,"-","")) <=  '.$end.']'.$filter_ip[0];
      for($i = $check_periode['start_year']; $i <= $check_periode['end_year']; $i++){
        if($i < $check_periode['end_year']){ $limit_month = 12; }else{ $limit_month = $check_periode['end_month'];  }
        for($j = $check_periode['start_month']; $j <= $limit_month; $j++){
          $path_ta = "../ibe/tracking-analytics/property/log_".$hotelcode."_".$i."_".sprintf('%02d', $j).".xml";
          if(file_exists($path_ta)){
            $xmllog_new = new DomDocument();
            $xmllog_new->preserveWhitespace = false;
            $xmllog_new->load($path_ta);

            $xpath_new = new DomXpath($xmllog_new);
            $countavailability = $xpath_new->query($query_tag);
            $result = $result + $countavailability->length;
          }
        }
      }
    }

		$avg = $result / $check_periode['numberofday'];
		return $result.' - avg '.ceil($avg).' /day';
		//return $path_ta;
	}

  function getReferalSource($hoteloid, $tag, $page, $start, $end){
    global $month_new_ta;
    global $year_new_ta;
    $data = array();
    $organic = 0; $referral = 0;

    $filter_ip = IPWhitelist($hoteloid);

    $check_periode = TAPeriodeDate($start, $end);

    if(!is_array($tag)){ $tag = array($tag); }

    if($check_periode['start_year'] <= $year_new_ta and $check_periode['start_month'] <= $month_new_ta){
      global $xpath;
      foreach($tag as $key => $valuetag){
        $query_tag = '//record[@hotel="'.$hoteloid.'"][@page="'.$page.'"][number(translate(@date,"-","")) >= '.$start.' and number(translate(@date,"-","")) <=  '.$end.']'.$filter_ip[1].'/'.$valuetag;
        foreach($xpath->query($query_tag) as $tag_result){
          if(empty($tag_result->textContent) or $tag_result->textContent == "bookingbutton"){
            $organic++;
          }else{
            $referral++;
          }
        }
      }
    }

    if(($check_periode['start_year'] >= $year_new_ta) or ($check_periode['end_year'] <= $year_new_ta and $check_periode['end_month'] >= $month_new_ta) ){
      $hotelcode = TAHotelCode($hoteloid);
      for($i = $check_periode['start_year']; $i <= $check_periode['end_year']; $i++){
        if($i < $check_periode['end_year']){ $limit_month = 12; }else{ $limit_month = $check_periode['end_month'];  }
        for($j = $check_periode['start_month']; $j <= $limit_month; $j++){
          $path_ta = "../ibe/tracking-analytics/property/log_".$hotelcode."_".$i."_".sprintf('%02d', $j).".xml";
          if(file_exists($path_ta)){
            $xmllog_new = new DomDocument();
            $xmllog_new->preserveWhitespace = false;
            $xmllog_new->load($path_ta);

            $xpath_new = new DomXpath($xmllog_new);
            foreach($tag as $key => $valuetag){
              $query_tag = '//record[@hotel="'.$hoteloid.'"][@page="'.$page.'"][@track="visit"][number(translate(@date,"-","")) >= '.$start.' and number(translate(@date,"-","")) <=  '.$end.']'.$filter_ip[1].'/'.$valuetag;
              foreach($xpath_new->query($query_tag) as $tag_result){
                if(empty($tag_result->textContent) or $tag_result->textContent == "bookingbutton"){
                  $organic++;
                }else{
                  $referral++;
                }
              }
            }
          }
        }
      }
    }

    return array($organic,$referral);
  }


  function TABrowser($hoteloid, $start, $end){
    global $month_new_ta;
    global $year_new_ta;
    global $pie_color;
    $browser = array();

    $filter_ip = IPWhitelist($hoteloid);

    $check_periode = TAPeriodeDate($start, $end);

    if(!is_array($tag)){ $tag = array($tag); }

    if($check_periode['start_year'] <= $year_new_ta and $check_periode['start_month'] <= $month_new_ta){
      global $xpath;
      $query_get_browser = '//record[@hotel="'.$hoteloid.'"][@page="availability"][number(translate(@date,"-","")) >=  '.$start.' and number(translate(@date,"-","")) <=  '.$end.']'.$filter_ip[1].'/browser/text()';
      foreach($xpath->query($query_get_browser) as $tag_browser){
  			if(strlen($tag_browser->textContent) < 50 == true){
  				if(in_array_multidimension($tag_browser->textContent, $browser)){
  					$list_key_browser = array_column($browser, 'name');
  					$key = array_search($tag_browser->textContent, $list_key_browser);

  					$used = $browser[$key]['used'] + 1;
  					$browser[$key]['used'] = $used;
  				}else{
  					array_push($browser, array('name' => $tag_browser->textContent, 'used' => 1));
  					end($browser);
  					$key = key($browser);
  					$browser[$key]['color'] = $pie_color[$key];
  				}
  			}
  		}
    }

    if(($check_periode['start_year'] >= $year_new_ta) or ($check_periode['end_year'] <= $year_new_ta and $check_periode['end_month'] >= $month_new_ta) ){
      $hotelcode = TAHotelCode($hoteloid);
      for($i = $check_periode['start_year']; $i <= $check_periode['end_year']; $i++){
        if($i < $check_periode['end_year']){ $limit_month = 12; }else{ $limit_month = $check_periode['end_month'];  }
        for($j = $check_periode['start_month']; $j <= $limit_month; $j++){
          $path_ta = "../ibe/tracking-analytics/property/log_".$hotelcode."_".$i."_".sprintf('%02d', $j).".xml";
          if(file_exists($path_ta)){
            $xmllog_new = new DomDocument();
            $xmllog_new->preserveWhitespace = false;
            $xmllog_new->load($path_ta);

            $xpath_new = new DomXpath($xmllog_new);
            $query_get_browser = '//record[@hotel="'.$hoteloid.'"][@page="availability"][@track="visit"][number(translate(@date,"-","")) >=  '.$start.' and number(translate(@date,"-","")) <=  '.$end.']'.$filter_ip[1].'/browser/text()';
            foreach($xpath_new->query($query_get_browser) as $tag_browser){
        			if(strlen($tag_browser->textContent) < 50 == true){
        				if(in_array_multidimension($tag_browser->textContent, $browser)){
        					$list_key_browser = array_column($browser, 'name');
        					$key = array_search($tag_browser->textContent, $list_key_browser);

        					$used = $browser[$key]['used'] + 1;
        					$browser[$key]['used'] = $used;
        				}else{
        					array_push($browser, array('name' => $tag_browser->textContent, 'used' => 1));
        					end($browser);
        					$key = key($browser);
        					$browser[$key]['color'] = $pie_color[$key];
        				}
        			}
            }
          }
        }
      }
    }
    return $browser;
  }

  function getHitsData($hoteloid, $tag, $page, $filter_tag, $start, $end){
    global $month_new_ta;
    global $year_new_ta;
    $data = array();

    $filter_ip = IPWhitelist($hoteloid);
    $check_periode = TAPeriodeDate($start, $end);


    if($check_periode['start_year'] <= $year_new_ta and $check_periode['start_month'] <= $month_new_ta){
      global $xpath;
      $query_tag = '//record[@hotel="'.$hoteloid.'"][@page="'.$page.'"][number(translate(@date,"-","")) >=  '.$start.' and number(translate(@date,"-","")) <=  '.$end.']'.$filter_ip[1].$filter_tag.'/'.$tag.'/text()';
      foreach($xpath->query($query_tag) as $tag_result){
  			$textcontent = str_replace("/","", $tag_result->textContent);
  			if(in_array_multidimension($textcontent, $data)){
  				$list_key_data = array_column($data, 'name');
  				$key = array_search($textcontent, $list_key_data);

  				$hits = $data[$key]['hits'] + 1;
  				$data[$key]['hits'] = $hits;
  			}else{
  				array_push($data, array('name' => $textcontent, 'hits' => 1));
  			}
  		}
    }

    if(($check_periode['start_year'] >= $year_new_ta) or ($check_periode['end_year'] <= $year_new_ta and $check_periode['end_month'] >= $month_new_ta) ){
      $hotelcode = TAHotelCode($hoteloid);
      for($i = $check_periode['start_year']; $i <= $check_periode['end_year']; $i++){
        if($i < $check_periode['end_year']){ $limit_month = 12; }else{ $limit_month = $check_periode['end_month'];  }
        for($j = $check_periode['start_month']; $j <= $limit_month; $j++){
          $path_ta = "../ibe/tracking-analytics/property/log_".$hotelcode."_".$i."_".sprintf('%02d', $j).".xml";
          if(file_exists($path_ta)){
            $xmllog_new = new DomDocument();
            $xmllog_new->preserveWhitespace = false;
            $xmllog_new->load($path_ta);

            $xpath_new = new DomXpath($xmllog_new);
            $query_tag = '//record[@hotel="'.$hoteloid.'"][@page="'.$page.'"][number(translate(@date,"-","")) >=  '.$start.' and number(translate(@date,"-","")) <=  '.$end.']/'.$tag.'/text()';
            foreach($xpath_new->query($query_tag) as $tag_result){
        			$textcontent = str_replace("/","", $tag_result->textContent);
        			if(in_array_multidimension($textcontent, $data)){
        				$list_key_data = array_column($data, 'name');
        				$key = array_search($textcontent, $list_key_data);

        				$hits = $data[$key]['hits'] + 1;
        				$data[$key]['hits'] = $hits;
        			}else{
        				array_push($data, array('name' => $textcontent, 'hits' => 1));
        			}
        		}
          }
        }
      }
    }
    return $data;
  }

  function getSoldData($hoteloid, $tag, $page, $start, $end, $value){
    global $month_new_ta;
    global $year_new_ta;
    $result = 0;

    $filter_ip = IPWhitelist($hoteloid);
    $check_periode = TAPeriodeDate($start, $end);

    $query_tag = '//record[@hotel="'.$hoteloid.'"][@page="'.$page.'"][number(translate(@date,"-","")) >=  '.$start.' and number(translate(@date,"-","")) <=  '.$end.']'.$filter_ip[1].'/'.$tag.'[text() = "'.$value.'"]';

    if($check_periode['start_year'] <= $year_new_ta and $check_periode['start_month'] <= $month_new_ta){
      global $xpath;
      $countsold = $xpath->query($query_tag);
      $result = $result + $countsold->length;
    }

    if(($check_periode['start_year'] >= $year_new_ta) or ($check_periode['end_year'] <= $year_new_ta and $check_periode['end_month'] >= $month_new_ta) ){
      $hotelcode = TAHotelCode($hoteloid);
      for($i = $check_periode['start_year']; $i <= $check_periode['end_year']; $i++){
        if($i < $check_periode['end_year']){ $limit_month = 12; }else{ $limit_month = $check_periode['end_month'];  }
        for($j = $check_periode['start_month']; $j <= $limit_month; $j++){
          $path_ta = "../ibe/tracking-analytics/property/log_".$hotelcode."_".$i."_".sprintf('%02d', $j).".xml";
          if(file_exists($path_ta)){
            $xmllog_new = new DomDocument();
            $xmllog_new->preserveWhitespace = false;
            $xmllog_new->load($path_ta);

            $xpath_new = new DomXpath($xmllog_new);
            $countsold = $xpath_new->query($query_tag);
            $result = $result + $countsold->length;
          }
        }
      }
    }
    return $result;
  }
?>
