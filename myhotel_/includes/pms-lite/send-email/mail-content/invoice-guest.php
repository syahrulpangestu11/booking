<?php
try{
  session_start();
	error_reporting(E_ALL ^ E_NOTICE);
  include ('conf/connection.php');
  include("includes/pms-lite/class-pms-lite.php");

  $hoteloid = $_POST['hotel'];
  $bookingnumber = $_REQUEST['bookingnumber'];

  $pmslite = new PMSReservation($db);
  $pmslite->setBooking($bookingnumber);
  $datarsv = $pmslite->getBookingData();
  $pmslite->setPMSHotel($datarsv['hoteloid']);

  $pmslitedata = new PMSEditRSV($db);
  $pmslitedata->setBooking($bookingnumber);
  $dataroom = $pmslitedata->getBookingRoomData();
  /*--------------------------------------*/
  $fileVersion = date("Y.m.d.H.i.s");

  $hotelname = $pmslite->hoteldata['hotelname'];
  $hoteloid = $pmslite->hoteldata['hoteloid'];
?>
    <div>
      <h2><?=$pmslite->hoteldata['hotelname']?></h2>
      <?=$pmslite->hoteldata['address']?><br>Phone : <?=$pmslite->hoteldata['phone']?><br>E-mail : <?=$pmslite->hoteldata['email']?><br>Website : <?=$pmslite->hoteldata['website']?>
    </div>
    <br><br>
    <table width="100%" style="border:1px solid #000; padding:3px;">
        <tr><td colspan="4"><h3>BOOKING DETAILS</h3></td></tr>
        <tr>
          <td colspan="2"><b>Guest Name :</b><?=$datarsv['guestname']?><br><b>Address :</b><br><?=$datarsv['address']?></td>
          <td colspan="2"><b>Phone :</b><?=$datarsv['phone']?><br><b>E-mail :</b> <?=$datarsv['email']?></td>
        </tr>
        <tr>
          <td valign="top"><b>Created on</b><br><?=date('d M Y', strtotime($datarsv['bookingtime']))?></td>
          <td valign="top"><b>Stay Details</b><br>
          <?php
          foreach($pmslite->detailStay() as $staydtl){
          echo $staydtl['room']." (".$staydtl['roomnumber'].")<br>".date('d M', strtotime($staydtl['checkin']))." - ".date('d M', strtotime($staydtl['checkout']))." (".$staydtl['night']." night)<br>";
          }
          echo '</td><td><b>Room(s) / Person(s)</b><br>';
          $stayoccupancy = $pmslite->summaryStayOccupancy();
          echo $stayoccupancy['jmlroom']." room(s) / ". $stayoccupancy['person']." (". $stayoccupancy['adult']." adult / ".$stayoccupancy['child']." children)";
          ?>
          </td>
          <td valign="top"><b>Amount :</b><br><?=$datarsv['currencycode']?> <?=number_format($datarsv['grandtotal'])?></td>
        </tr>
    </table><br>
    <h1 style="text-align:center">ACCOUNT STATEMENT</h1>
    <table width="100%" border="1" style="border:1px solid #000; padding:1px;">
      <thead>
        <tr style="text-align:center; font-weight:bold; background-color:gray;"><th>#</th><th>Date</th><th>Description-References</th><th>Folio#</th><th>Disc/Allow</th><th>Charges</th><th>Tax</th><th>Payment</th></tr>
      </thead>
      <tbody>
      <?php
        $no = 0;
        foreach($dataroom as $dr){
      ?>
      <tr>
        <td><?=++$no?></td>
        <td><?=date('d M Y', strtotime($datarsv['bookingtime']))?></td>
        <td><?=$dr['promotion']?> <?=$dr['room']?> / <?=$pmslitedata->getRoomNumber($dr['bookingroomoid'])?></td>
        <td></td>
        <td></td>
        <td><?=$datarsv['currencycode']." ".number_format($dr['totalr'])?></td>
        <td></td>
        <td></td>
      </tr>
      <?php
        }
      ?>

      <?php
        foreach($pmslitedata->getBookingChargeData() as $key => $othercharges){
      ?>
        <tr>
          <td><?=++$no?></td>
          <td><?=date('d M Y', strtotime($othercharges['created']))?></td>
          <td>Qty <?=$othercharges['qty']?> <?=$othercharges['product']?> - <?=$othercharges['pos']?></td>
          <td></td>
          <td></td>
          <td><?=$datarsv['currencycode']." ".number_format($othercharges['total'])?></td>
          <td></td>
          <td></td>
        </tr>
      <?php
        }
      ?>

      <?php
      foreach($pmslite->paymentList() as $payment){
      ?>
        <tr>
          <td><?=++$no?></td>
          <td><?=date('d M Y', strtotime($payment['paymentdate']))?></td>
          <td><?=$payment['note']?></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td><?=$datarsv['currencycode']?> <?=number_format($payment['amount'])?></td>
        </tr>';
      <?php
      }
      ?>
      </tbody>
      <tfoot>
        <tr><th colspan="3" align="right"><b>Total</b></th><th></th><th></th><th><?=$datarsv['currencycode']?> <?=number_format($datarsv['totalroom']+$datarsv['totalextra'])?></th><th></th><th><?=$datarsv['currencycode']?> <?=number_format($datarsv['paid'])?></th></tr>
      </tfoot>
    </table>
    <br><br>
    <table>
      <tr>
        <td align="right">
          <table>
            <tr><td><b>Booking Total</b></td><td><?=$datarsv['currencycode']?> <?=number_format($datarsv['totalroom']+$datarsv['totalextra'])?></td></tr>
            <tr><td><b>Other Charges</b></td><td><?=$datarsv['currencycode']?> <?=number_format($datarsv['totalcharge'])?></td></tr>
            <tr><td><b>Total Tax</b></td><td></td></tr>
            <tr><td><b>Total Disc/Allow</b></td><td></td></tr>
            <tr><td><b>Total Paid</b></td><td><?=$datarsv['currencycode']?> <?=number_format($datarsv['paid'])?></td></tr>
            <tr><td><b>Balance</b></td><td><?=$datarsv['currencycode']?> <?=number_format($datarsv['paid_balance'])?></td></tr>
          </table>
        </td>
      </tr>
    </table>
<?php
}catch(Exception $e){
  echo $e->getMessage();
}
?>
