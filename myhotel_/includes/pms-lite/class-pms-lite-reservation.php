<?php
  class PMSReservation extends PMSLite{

    function allActiveAgent(){
      $stmt = $this->db->prepare("select a.agentoid, a.agentname from agent a inner join agenthotel ah using (agentoid) where ah.hoteloid = :a and a.publishedoid = 1 order by a.agentname");
      $stmt->execute(array(':a' => $this->hoteloid));
  		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);

  		return $result;
    }

    function agentDetail($agentoid){
      $stmt = $this->db->prepare("select a.*, ah.agenthoteloid from agent a inner join agenthotel ah using (agentoid) where a.agentoid = :a");
      $stmt->execute(array(':a' => $agentoid));
      $result = $stmt->fetch(PDO::FETCH_ASSOC);

  		return $result;
    }

    function paymentList(){
      $stmt = $this->db->prepare("select * from bookingpaymentdtl inner join booking b using (bookingoid) where b.bookingoid = :a");
      $stmt->execute(array(':a' => $this->bookingoid));
      $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

      return $result;
    }

    function paymentListDetail($paymentoid){
      $stmt = $this->db->prepare("select bpd.*, c.currencycode from bookingpaymentdtl bpd inner join currency c using (currencyoid) inner join booking b using (bookingoid) where bpd.bookingpaymentdtloid = :a");
      $stmt->execute(array(':a' => $paymentoid));
      $result = $stmt->fetch(PDO::FETCH_ASSOC);

      return $result;
    }

    function detailStay(){
      $stmt = $this->db->prepare("select room, checkin, checkoutr as checkout, night, roomnumber from bookingroom inner join bookingroomdtl using (bookingroomoid) left join roomnumber using (roomnumberoid) where bookingoid = :a group by bookingroomoid");
      $stmt->execute(array(':a' => $this->bookingoid));
      $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

  		return $result;
    }

    function summaryStayOccupancy(){
      $stmt = $this->db->prepare("select count(bookingroomoid) as jmlroom, sum(adult) as adult, sum(child) as child, sum(adult)+sum(child) as person from bookingroom where bookingoid = :a");
      $stmt->execute(array(':a' => $this->bookingoid));
      $result = $stmt->fetch(PDO::FETCH_ASSOC);

  		return $result;
    }

    /*----------------------------------------------------------------------*/

    public function loadShowPrice($arrival, $departure, $roomoid, $roomofferoid, $channel, $roombooked, $tknSession, $bookingmarket, $agenthoteloid){

      $remove_session = "delete bookingtemp, bookingtempdtl from bookingtemp, bookingtempdtl where bookingtemp.bookingtempoid = bookingtempdtl.bookingtempoid and bookingtemp.session_id = '".$tknSession."'";
      $stmt	= $this->db->query($remove_session);

      /*-------------------------------------------------------------------------*/

      $night = $this->dateDiff($arrival, $departure);
      $xml_hotel = $this->hoteloid; $xml_room = $roomoid; $xml_roomoffer = $roomofferoid; $xml_channel = $channel;
      $xml = $this->xmltarif;
      $list_allotment = array(); $promoNight = array();

      for($n=0; $n<$night; $n++){
        $extrabed = ''; $rate = 0; $currency = 1;
        $xml_date = date('Y-m-d', strtotime($arrival. ' +'.$n.' days'));

        /*-------------------------------------------------------------------------*/

        if($bookingmarket == 3 and $agenthoteloid != 0){
          $syntax_bar = "select rate, surcharge from agenthotelrate where agenthoteloid = '".$agenthoteloid."' and roomofferoid = '".$xml_roomoffer."' and '".$xml_date."' >= startdate and '".$xml_date."' <= enddate order by priority limit 1";
          $stmt = $this->db->query($syntax_bar);
          $row_bar = $stmt->fetch(PDO::FETCH_ASSOC);
          $rate = $row_bar['rate'] + $row_bar['surcharge'];
        }else{
          $query_tag = '//hotel[@id="'.$xml_hotel.'"]/masterroom[@id="'.$xml_room.'"]/rate[@date="'.$xml_date.'"]/rateplan[@id="'.$xml_roomoffer.'"]/channel[@type="'.$xml_channel.'"]';
          foreach($xml->xpath($query_tag) as $tagrate){
            $extrabed = (float)$tagrate->extrabed;
            $rate = $tagrate->double;
            $currency = $tagrate->currency;
            if($extrabed == 0 and $extrabed_available == true){ $extrabed_available = false; }
          }
        }

        /*-------------------------------------------------------------------------*/

        if($rate > 0){

          if($extrabed > 0){ $extrabedavailable = "Available"; }else{ $extrabedavailable = "Not available"; }

          $s_currency = "SELECT `currencycode`, `currencytitle` FROM `currency` WHERE `currencyoid`='".$currency."'";
          $stmt = $this->db->query($s_currency);
          $a_currency = $stmt->fetchAll(PDO::FETCH_ASSOC);
          foreach($a_currency as $r_currency){
            $currencyname = $r_currency['currencycode'];
          }

          $disc_note = "";
          $totalpernight = $rate;
          /*------------------------------------------*/
          $roomcharge = $totalpernight;
          $total_roomcharge += $totalpernight;
          /*------------------------------------------*/
          if($with_extrabed == "y"){
            $totalpernight = $totalpernight + $extrabed;
          }else{
            $extrabed = 0;
          }
          $total_extrabed += $extrabed;
          /*------------------------------------------*/
          $totalperroom += $totalpernight;

          $promoNight[$n]['xml_date'] = $xml_date;
          $promoNight[$n]['allotment'] = $allotment;
          $promoNight[$n]['extrabedavailable'] = $extrabedavailable;
          $promoNight[$n]['rate'] = $rate;
          $promoNight[$n]['rateafter'] = $roomcharge;
          $promoNight[$n]['extrabed'] = $extrabed;
          $promoNight[$n]['currencyoid'] = $currency;
          $promoNight[$n]['currencyname'] = $currencyname;
          $promoNight[$n]['breakdate_name'] = $breakdate_name;
          $promoNight[$n]['totalpernight'] = $totalpernight;
          $promoNight[$n]['disc_note'] = $disc_note;

          $j++;

        }else{
          break;
          $show = "No";
        }
      } /* for night */

      $show = "No";
      if(count($promoNight) == $night){
        $show = "Yes";
      }

      $stmt = $this->db->prepare("select currencycode from currency ro where currencyoid = :a and ro.publishedoid in (1)");
      $stmt->execute(array(':a' => $currency));
      $result = $stmt->fetch(PDO::FETCH_ASSOC);

      if($show == "Yes" and $totalperroom > 0 and $roombooked>0){
        $submittime = date('Y-m-d H:i:s');
        $channeloid = $xml_channel;
        $promotionoid = 0; $flag = 0; $breakfast = "n"; $with_extrabed = "n";
        $totaldeposit = 0; $totalbalance = $totalperroom; $hotelname = $this->hotelname;
        $this->setRoom($roomoid); $roomtype = $this->roomType(); $roomname = $roomtype['roomname']; $promoname = "Flexible Rate";

        for($i=1; $i<=$roombooked; $i++){
          $stmt = $this->db->prepare("INSERT INTO `bookingtemp` (`submittime`, `session_id`, `roomofferoid`, `channeloid`, `promotionoid`, `checkin`, `checkout`, `night`, `adult`, `child`, `flag`, `breakfast`, `extrabed`, `roomtotal`, `extrabedtotal`, `total`, `deposit`, `balance`, `currencyoid`, `hotel`, `room`, `promotion`, `promocode`, `pc_commission`, `cancellationpolicyoid`, `cancellationname`, `termcondition`, `cancellationday`, `cancellationtype`, `cancellationtypenight`, `rate_termcondition`, `rate_cancellationpolicy`, `promocodecomm`)  VALUES (:a , :b , :c , :d , :e , :f, :g, :h, :i, :j, :k, :l, :m, :n1, :n2, :o, :p, :q, :r, :s, :t, :u, :v, :w, :aa, :ab, :ac, :ad, :ae, :af, :ba, :bb, :bc)");
          $stmt->execute(array(':a' => $submittime, ':b' => $tknSession, ':c' => $roomofferoid, ':d' => $channeloid, ':e' => $promotionoid, ':f' => $arrival, ':g' => $departure, ':h' => $night, ':i' => 0, ':j' => 0, ':k' => $flag, ':l' => $breakfast, ':m' => $with_extrabed, ':n1' => $total_roomcharge, ':n2' => $total_extrabed, ':o' => $totalperroom, ':p' => $totaldeposit, ':q' => $totalbalance, ':r' => $currency, ':s' => $hotelname, ':t' => $roomname, ':u' => $promoname, ':v' => $promocode, ':w' => $applycommission, ':aa' => $cancellationpolicyoid, ':ab' => $cancellationname, ':ac' => $termcondition, ':ad' => $cancellationday, ':ae' => $cancellationtype, ':af' => $cancellationtypenight, ':ba' => $rate_termcondition, ':bb' => $rate_cancellationpolicy, ':bc' => $pc_commission_total));
          $tempoid = $this->db->lastInsertId();

          $listTotal = array();
          foreach($promoNight as $j => $value2){
            if($with_extrabed == "y"){ $value2['extrabed'] = 0; }

            $stmt = $this->db->prepare("INSERT INTO `bookingtempdtl` (`bookingtempoid`, `date`, `rate`, `discount`, `rateafter`, `extrabed`, `total`, `currencyoid`)  VALUES (:a , :b , :c , :d , :e , :f, :g, :h)");
            $stmt->execute(array(':a' => $tempoid, ':b' => $value2['xml_date'], ':c' => $value2['rate'], ':d' => $value2['disc_note'], ':e' => $value2['rateafter'], ':f' => $value2['extrabed'], ':g' => $value2['totalpernight'], ':h' => $value2['currencyoid']));
            array_push($listTotal, $value2['totalpernight']);
          }
        }

        return $result['currencycode']." ".number_format($roombooked*$totalperroom);
      }else{
        return "<failed> Rate not available. Please choose another date.";
      }

    }

    public function loadShowEditPrice($arrival, $departure, $roomoid, $roomofferoid, $channel, $roombooked, $tknSession, $bookingmarket, $agenthoteloid){

  		$night = $this->dateDiff($arrival, $departure);
  		$xml_hotel = $this->hoteloid; $xml_room = $roomoid; $xml_roomoffer = $roomofferoid; $xml_channel = $channel;
  		$xml = $this->xmltarif;
  		$list_allotment = array(); $promoNight = array();

  		for($n=0; $n<$night; $n++){
  			$extrabed = ''; $rate = 0; $currency = 1;
  			$xml_date = date('Y-m-d', strtotime($arrival. ' +'.$n.' days'));

        if($bookingmarket == 3 and $agenthoteloid != 0){
          $syntax_bar = "select rate, surcharge from agenthotelrate where agenthoteloid = '".$agenthoteloid."' and roomofferoid = '".$xml_roomoffer."' and '".$xml_date."' >= startdate and '".$xml_date."' <= enddate order by priority limit 1";
          $stmt = $this->db->query($syntax_bar);
          $row_bar = $stmt->fetch(PDO::FETCH_ASSOC);
          $rate = $row_bar['rate'] + $row_bar['surcharge'];
        }else{
          $query_tag = '//hotel[@id="'.$xml_hotel.'"]/masterroom[@id="'.$xml_room.'"]/rate[@date="'.$xml_date.'"]/rateplan[@id="'.$xml_roomoffer.'"]/channel[@type="'.$xml_channel.'"]';
          foreach($xml->xpath($query_tag) as $tagrate){
    				$extrabed = (float)$tagrate->extrabed;
    				$rate = $tagrate->double;
    				$currency = $tagrate->currency;
    				if($extrabed == 0 and $extrabed_available == true){ $extrabed_available = false; }
    			}
        }
  			if($extrabed > 0){ $extrabedavailable = "Available"; }else{ $extrabedavailable = "Not available"; }

  			$s_currency = "SELECT `currencycode`, `currencytitle` FROM `currency` WHERE `currencyoid`='".$currency."'";
  			$stmt = $this->db->query($s_currency);
  			$a_currency = $stmt->fetchAll(PDO::FETCH_ASSOC);
  			foreach($a_currency as $r_currency){
  				$currencyname = $r_currency['currencycode'];
  			}

  			$disc_note = "";
  			$totalpernight = $rate;
  			/*------------------------------------------*/
  			$roomcharge = $totalpernight;
  			$total_roomcharge += $totalpernight;
  			/*------------------------------------------*/
  			if($with_extrabed == "y"){
  				$totalpernight = $totalpernight + $extrabed;
  			}else{
  				$extrabed = 0;
  			}
  			$total_extrabed += $extrabed;
  			/*------------------------------------------*/
  			$totalperroom += $totalpernight;

  			$promoNight[$n]['xml_date'] = $xml_date;
  			$promoNight[$n]['allotment'] = $allotment;
  			$promoNight[$n]['extrabedavailable'] = $extrabedavailable;
  			$promoNight[$n]['rate'] = $rate;
  			$promoNight[$n]['rateafter'] = $roomcharge;
  			$promoNight[$n]['extrabed'] = $extrabed;
  			$promoNight[$n]['currencyoid'] = $currency;
  			$promoNight[$n]['currencyname'] = $currencyname;
  			$promoNight[$n]['breakdate_name'] = $breakdate_name;
  			$promoNight[$n]['totalpernight'] = $totalpernight;
  			$promoNight[$n]['disc_note'] = $disc_note;

  			$j++;
  		} /* for night */

  		$show = "No";
  		if(count($promoNight) == $night){
  			$show = "Yes";
  		}

  		$stmt = $this->db->prepare("select currencycode from currency ro where currencyoid = :a and ro.publishedoid in (1)");
  		$stmt->execute(array(':a' => $currency));
  		$result = $stmt->fetch(PDO::FETCH_ASSOC);

  		if($show == "Yes" and $totalperroom > 0 and $roombooked > 0){
  			return $result['currencycode']." ".number_format($roombooked*$totalperroom);
  		}else{
  			return "<failed>";
  		}
  	}

    public function saveEditPrice($arrival, $departure, $roomoid, $roomofferoid, $channel, $roombooked, $bookingroomoid, $bookingmarket, $agenthoteloid){

      $change_roomdtl = "update bookingroomdtl set pmsreconciled = '1', reconciled = '1' where bookingroomoid = '".$bookingroomoid."'";
      $stmt	= $this->db->query($change_roomdtl);

      /*-------------------------------------------------------------------------*/

      $night = $this->dateDiff($arrival, $departure);
      $xml_hotel = $this->hoteloid; $xml_room = $roomoid; $xml_roomoffer = $roomofferoid; $xml_channel = $channel;
      $xml = $this->xmltarif;
      $list_allotment = array(); $promoNight = array();

      for($n=0; $n<$night; $n++){
        $extrabed = ''; $rate = 0; $currency = 1;
        $xml_date = date('Y-m-d', strtotime($arrival. ' +'.$n.' days'));

        if($bookingmarket == 3 and $agenthoteloid != 0){
          $syntax_bar = "select rate, surcharge from agenthotelrate where agenthoteloid = '".$agenthoteloid."' and roomofferoid = '".$xml_roomoffer."' and '".$xml_date."' >= startdate and '".$xml_date."' <= enddate order by priority limit 1";
          $stmt = $this->db->query($syntax_bar);
          $row_bar = $stmt->fetch(PDO::FETCH_ASSOC);
          $rate = $row_bar['rate'] + $row_bar['surcharge'];
        }else{
    			$query_tag = '//hotel[@id="'.$xml_hotel.'"]/masterroom[@id="'.$xml_room.'"]/rate[@date="'.$xml_date.'"]/rateplan[@id="'.$xml_roomoffer.'"]/channel[@type="'.$xml_channel.'"]';
    			foreach($xml->xpath($query_tag) as $tagrate){
    				$extrabed = (float)$tagrate->extrabed;
    				$rate = $tagrate->double;
    				$currency = $tagrate->currency;
    				if($extrabed == 0 and $extrabed_available == true){ $extrabed_available = false; }
    			}
        }

        if($extrabed > 0){ $extrabedavailable = "Available"; }else{ $extrabedavailable = "Not available"; }

        $s_currency = "SELECT `currencycode`, `currencytitle` FROM `currency` WHERE `currencyoid`='".$currency."'";
        $stmt = $this->db->query($s_currency);
        $a_currency = $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach($a_currency as $r_currency){
          $currencyname = $r_currency['currencycode'];
        }

        $disc_note = "";
        $totalpernight = $rate;
        /*------------------------------------------*/
        $roomcharge = $totalpernight;
        $total_roomcharge += $totalpernight;
        /*------------------------------------------*/
        if($with_extrabed == "y"){
          $totalpernight = $totalpernight + $extrabed;
        }else{
          $extrabed = 0;
        }
        $total_extrabed += $extrabed;
        /*------------------------------------------*/
        $totalperroom += $totalpernight;

        $promoNight[$n]['xml_date'] = $xml_date;
        $promoNight[$n]['allotment'] = $allotment;
        $promoNight[$n]['extrabedavailable'] = $extrabedavailable;
        $promoNight[$n]['rate'] = $rate;
        $promoNight[$n]['rateafter'] = $roomcharge;
        $promoNight[$n]['extrabed'] = $extrabed;
        $promoNight[$n]['currencyoid'] = $currency;
        $promoNight[$n]['currencyname'] = $currencyname;
        $promoNight[$n]['breakdate_name'] = $breakdate_name;
        $promoNight[$n]['totalpernight'] = $totalpernight;
        $promoNight[$n]['disc_note'] = $disc_note;

        $j++;
      } /* for night */

      $show = "No";
      if(count($promoNight) == $night){
        $show = "Yes";
      }

      $stmt = $this->db->prepare("select currencycode from currency ro where currencyoid = :a and ro.publishedoid in (1)");
      $stmt->execute(array(':a' => $currency));
      $result = $stmt->fetch(PDO::FETCH_ASSOC);
      $currencyoid = 1;

      if($show == "Yes" and $totalperroom > 0 and $roombooked>0){
        $submittime = date('Y-m-d H:i:s');
        $channeloid = $xml_channel;
        $promotionoid = 0; $flag = 0; $breakfast = "n"; $with_extrabed = "n";
        $totaldeposit = 0; $totalbalance = $totalperroom; $hotelname = $this->hotelname;
        $this->setRoom($roomoid); $roomtype = $this->roomType(); $roomname = $roomtype['roomname']; $promoname = "Flexible Rate";

        for($i=1; $i<=$roombooked; $i++){
          $stmt = $this->db->prepare("UPDATE `bookingroom` SET `roomofferoid` = :a, `checkin` = :b, `checkout` = :c, `checkoutr` = :d, `night` = :e, `roomtotal` = :f, `extrabedtotal` = :g, `total` = :h, `currencyoid` = :i, `breakfast` = :j, `extrabed` = :k, `deposit` = :l, `balance` = :m, `room` = :n, `promotion` = :o, `cancellationpolicyoid` = :p, `cancellationname` = :q, `termcondition` = :r, `cancellationday` = :s, `cancellationtype` = :t, `cancellationtypenight` = :u, `roomtotalr` = :v, `totalr` = :w, `rate_termcondition` = :x, `rate_cancellationpolicy` = :y where bookingroomoid = :id");

          $stmt->execute(array(':id' => $bookingroomoid, ':a' => $roomofferoid, ':b' => $arrival, ':c' => $departure, ':d' => $departure, ':e' => $night, ':f' => $total_roomcharge, ':g' => $total_extrabed, ':h' => $totalperroom, ':i' => $currencyoid, ':j' => $breakfast, ':k' => $with_extrabed, ':l' => $totaldeposit, ':m' => $totalbalance, ':n' => $roomname, ':o' => $promoname, ':p' => $cancellationpolicyoid, ':q' => $cancellationname, ':r' => $termcondition, ':s' => $cancellationday, ':t' => $cancellationtype, ':u' => $cancellationtypenight, ':v' => $total_roomcharge, ':w' => $totalperroom, ':x' => $rate_termcondition, ':y' => $rate_cancellationpolicy));

          $listTotal = array();
          foreach($promoNight as $j => $value2){
            if($with_extrabed == "y"){ $value2['extrabed'] = 0; }

            $stmt = $this->db->prepare("INSERT INTO `bookingroomdtl` (`bookingroomoid`, `date`, `rate`, `discount`, `rateafter`, `extrabed`, `total`, `currencyoid`)  VALUES (:a , :b , :c , :d , :e , :f, :g, :h)");
            $stmt->execute(array(':a' => $bookingroomoid, ':b' => $value2['xml_date'], ':c' => $value2['rate'], ':d' => $value2['disc_note'], ':e' => $value2['rateafter'], ':f' => $value2['extrabed'], ':g' => $value2['totalpernight'], ':h' => $value2['currencyoid']));
            array_push($listTotal, $value2['totalpernight']);
          }
        }

        return true;
      }else{
        return false;
      }
    }

    function reorderRoom($list_roomnumber_daily){
  		$reorder = array_count_values($list_roomnumber_daily);
      arsort($reorder);
      $list_roomnumber = array();
      foreach($reorder as $key => $value){
        array_push($list_roomnumber, $key);
      }
  		return $list_roomnumber;
  	}
  }
?>
