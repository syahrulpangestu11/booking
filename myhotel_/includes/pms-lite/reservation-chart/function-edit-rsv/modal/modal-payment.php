<div class="modal fade pms-lite-modal" id="paymentDetails" tabindex="-1" role="dialog" aria-labelledby="paymentDetails">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Payment Details</h4>
      </div>
      <div class="modal-body">
        <form id="form-add-payment-details" method="post">
          <input type="hidden" name="bookingoid" value="<?=$datarsv['bookingoid']?>">
          <div class="row">
      			<div class="col-md-6"><h3>Select Payment Method</h3></div>
      			<div class="col-md-6">
              <select name="type" class="form-control input-sm">
                <?php
                $stmt = $db->prepare("select * from pmspaymenttype where publishedoid = '1'");
                $stmt->execute();
                $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
                foreach($result as $paymenttype){
                ?>
                <option value="<?=$paymenttype['pmspaymenttypeoid']?>"><?=$paymenttype['method']?></option>
                <?php
                }
                ?>
              </select>
            </div>
      		</div>
          <hr />
          <!-- CC -->
          <div class="cc">
            <div class="row">
              <div class="col-md-2">
                <div class="form-group">
                  <label for="inputEmail3">Credit Card</label>
                  <select name="card" class="form-control input-sm">
                  <?php
                  $stmt = $db->prepare("select * from creditcard where publishedoid = '1'");
                  $stmt->execute();
                  $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
                  foreach($result as $currency){
                  ?>
                  <option value="<?=$currency['cardoid']?>" <?=$selected?>><?=$currency['cardname']?></option>
                  <?php
                  }
                  ?>
                  </select>
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label for="inputEmail3">Card Number</label><input type="text" class="form-control" name="cardnumber">
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label for="inputEmail3">Card Holder</label><input type="text" class="form-control" name="cardholder">
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label for="inputEmail3">Expired Month</label>
                  <select name="expmonth" class="form-control">
                    <?php
                        $monthNumber	= array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');
                        $monthName		= array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
                        foreach($monthNumber as $key => $value){
                            $selected = '';
                            if($monthName[$key] == date('F')){ $selected = 'selected'; }
                    ?>
                        <option value="<?=$monthNumber[$key]?>" <?=$selected?>><?=$monthName[$key]?></option>
                    <?php
                        }
                    ?>
                    </select>
                  </div>
                </div>
                <div class="col-md-1">
                  <div class="form-group">
                    <label for="inputEmail3">Expired Year</label>
                    <select name="expyear" class="form-control">
                    <?php for($y = date('Y'); $y <= date('Y')+10; $y++){ ?>
                    <option value="<?=$y?>"><?=$y?></option>
                    <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="col-md-1">
                  <div class="form-group">
                    <label for="inputEmail3">CVC</label><input type="text" class="form-control" name="cvc">
                  </div>
                </div>
            </div>
          </div>
          <div class="bank-transfer" style="display:none">
            <div class="row">
              <div class="col-md-2">
                <div class="form-group">
                  <label for="inputEmail3">Bank Name</label><input type="text" class="form-control" name="bank" disabled>
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label for="inputEmail3">Account Name</label><input type="text" class="form-control" name="cardholder" disabled>
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label for="inputEmail3">Account Number</label><input type="text" class="form-control" name="cardnumber" disabled>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-2">
              <div class="form-group">
                <label for="inputEmail3">Payment Date</label><input type="text" class="form-control" name="paymentdate" id="startdate" value="<?=date('d F Y')?>">
              </div>
            </div>
            <div class="col-md-1">
              <div class="form-group">
                <label for="inputEmail3">Currency</label>
                <select name="currency" class="form-control input-sm">
                <?php
                $stmt = $db->prepare("select * from currency where publishedoid = '1'");
                $stmt->execute();
                $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
                foreach($result as $currency){
                ?>
                <option value="<?=$currency['currencyoid']?>" <?=$selected?>><?=$currency['currencycode']?></option>
                <?php
                }
                ?>
                </select>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label for="inputEmail3">Amount</label><input type="text" class="form-control" name="amount" value="<?=$datarsv['grandbalance']?>">
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label for="inputEmail3">Administration Fee</label><input type="text" class="form-control" name="administrationfee" value="0">
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label for="inputEmail3">Receipt #</label><input type="text" class="form-control" name="receipt">
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="inputEmail3">Note</label><input type="text" class="form-control" name="note">
              </div>
            </div>
          </div>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" name="btn-submit" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
