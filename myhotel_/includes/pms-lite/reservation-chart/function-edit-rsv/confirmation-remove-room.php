<?php
try{
  session_start();
	error_reporting(E_ALL ^ E_NOTICE);
	include ('../../../../conf/connection.php');
  include ('../../../../conf/baseurl.php');
  include ('../../class-pms-lite.php');

  $hoteloid = $_SESSION['_hotel'];
  $bookingroomdtloid = $_POST['brid'];

  $pmslite = new PMSLite($db);
  $pmslite->setPMSHotel($hoteloid);
  $data = $pmslite->getBookingRoom($bookingroomdtloid);
  /*--------------------------------------*/

?>
  <form id="form-remove-room" method="post">
    <input type="hidden" name="bookingroom" value="<?=$data['bookingroomoid']?>">
    <div class="row">
			<div class="col-md-6"><h3><?=$data['guestname']?></h3></div>
			<div class="col-md-4"><h3>Room Type : <?=$data['room']?></h3></div>
      <div class="col-md-2"><h3>Room Number : <?=$data['roomnumber']?></h3></div>
		</div>
    <hr />
    <div class="row">
      <div class="col-md-6">
        <div class="row"><div class="col-md-12"><h4><span class="blue">Guest Details :</span></h4></div></div>
        <div class="row"><div class="col-md-12"><?=$data['guestname']?></div></div>
        <div class="row"><div class="col-md-12"><h4><span class="blue">Rate Details :</span> <?=$data['rateplan']?></h4></div></div>
      </div>
      <div class="col-md-6">
        <div class="row"><div class="col-md-12"><h4><span class="blue">Stay Details :</span></h4></div></div>
        <div class="row"><div class="col-md-12"><b>Check in date :</b> <?=date('d M Y', strtotime($data['checkin']))?></div></div>
        <div class="row"><div class="col-md-12"><b>Check out date :</b> <?=date('d M Y', strtotime($data['checkout']))?></div></div>
        <div class="row"><div class="col-md-12"><b>Room Nights :</b> <?=$data['night']?></div></div>
        <div class="row"><div class="col-md-12"><b>Room Occupancy :</b> <?=$data['adult']?> Adults <?=$data['child']?> Children</div></div>
        <div class="row"><div class="col-md-12" style="margin-top:5px;"><b>Total Amount :</b> <?=$data['currency']?> <?=number_format($data['total'])?></div></div>
      </div>
    </div>
    <hr />
    <div class="row">
      <div class="col-md-6"><b>Are you sure you want to remove this room?</b></div>
      <div class="col-md-6 text-right">
        <button type="button" name="remove-room" class="btn btn-sm btn-danger" data-id="<?=$bookingroomdtloid?>">Remove Room</button>
        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </form>
<?php
}catch(Exception $e){
  echo $e->getMessage();
}
?>
