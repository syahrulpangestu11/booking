<?php
try{
  session_start();
	error_reporting(E_ALL ^ E_NOTICE);
	include ('../../../../conf/connection.php');
  include ('../../class-pms-lite.php');

  $hoteloid = $_SESSION['_hotel'];

  $pmslite = new PMSLite($db);
  $pmslite->bookingoid = $_POST['bookingoid'];
  $datarsv = $pmslite->getBookingData();

  $stmt = $db->prepare("select method from pmspaymenttype where pmspaymenttypeoid = :a");
  $stmt->execute(array(':a' => $_POST['type']));
  $paymethod = $stmt->fetch(PDO::FETCH_ASSOC);

  $total_payment = $_POST['amount'] + $_POST['administrationfee'];
  $description = "Paid by ".$datarsv['guestname']." with ".$paymethod['method'];
  if(!empty($_POST['receipt'])){
    $description.=' Receipt# '.$_POST['receipt'];
  }
  if(!empty($_POST['note'])){
    $description.=' - '.$_POST['note'];
  }

  $stmt = $db->prepare("insert into bookingpaymentdtl (bookingoid, pmspaymenttypeoid, paymentdate, currencyoid, amount, administrationfee, total, note, description, created, createdby, updated, updatedby, receipt) value (:a, :b, :c, :d, :e, :f, :g, :h, :i, :j, :k, :l, :m, :n)");
  $stmt->execute(array(':a' => $_POST['bookingoid'], ':b' => $_POST['type'], ':c' => date('Y-m-d', strtotime($_POST['paymentdate'])), ':d' => $_POST['currency'], ':e' => $_POST['amount'] , ':f' => $_POST['administrationfee'] , ':g' => $total_payment, ':h' => $_POST['note'], ':i' => $description, ':j' => date('Y-m-d H:i:s'), ':k' => $_SESSION['_user'], ':l' => date('Y-m-d H:i:s'), ':m' => $_SESSION['_user'], ':n' => $_POST['receipt']));
  $paymentid = $db->lastInsertId();

  if($_POST['type'] == "1"){
    $q_card = "select cardname from creditcard c where c.cardoid = '".$_POST['card']."'";
    $stmt = $db->query($q_card);
    $card = $stmt->fetch(PDO::FETCH_ASSOC);
    $_POST['bank'] = $card['cardname'];

    $stmt = $db->prepare("update bookingpaymentdtl set bank = :a, cardoid = :b, accountnumber =:c, accountname = :d, expmonth = :e, expyear = :f, cvc = :g where bookingpaymentdtloid = :id");
    $stmt->execute(array(':a' => $_POST['bank'], ':b' => $_POST['card'], ':c' => $_POST['cardnumber'], ':d' => $_POST['cardholder'] , ':e' => $_POST['expmonth'] , ':f' => $_POST['expyear'], ':g' => $_POST['cvc'], ':id' => $paymentid));
  }else if($_POST['type'] == "2"){
    $stmt = $db->prepare("update bookingpaymentdtl set bank = :a, accountnumber = :b, accountname =:c where bookingpaymentdtloid = :id");
    $stmt->execute(array(':a' => $_POST['bank'], ':b' => $_POST['cardnumber'], ':c' => $_POST['cardholder'], ':id' => $paymentid));
  }

  $pmslite->RecalculateBooking($_POST['bookingoid']);

  echo "1";
?>
<?php
}catch(Exception $e){
  echo $e->getMessage();
}
?>
