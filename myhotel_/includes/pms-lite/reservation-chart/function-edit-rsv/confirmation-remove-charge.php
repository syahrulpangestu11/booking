<?php
try{
  session_start();
	error_reporting(E_ALL ^ E_NOTICE);
	include ('../../../../conf/connection.php');
  include ('../../../../conf/baseurl.php');
  include ('../../class-pms-lite.php');

  $hoteloid = $_SESSION['_hotel'];
  $bookingchargeoid = $_POST['bcid'];

  $pmslite = new PMSLite($db);
  $pmslite->setPMSHotel($hoteloid);
  $data = $pmslite->getBookingCharge($bookingchargeoid);
  /*--------------------------------------*/

?>
  <form id="form-remove-charge" method="post">
    <input type="hidden" name="bookingcharges" value="<?=$data['bookingchargesoid']?>">
    <div class="row">
      <div class="col-md-6">
        <div class="row"><div class="col-md-12"><h4><span class="blue">POS : </span><?=$data['pos']?></h4></div></div>
        <div class="row"><div class="col-md-12"><h4><span class="blue">Product : </span> <?=$data['product']?></h4></div></div>
      </div>
      <div class="col-md-6">
        <div class="row"><div class="col-md-12"><h4><span class="blue">Details :</span></h4></div></div>
        <div class="row"><div class="col-md-12"><b>Price :</b> <?=$data['currencycode']." ".number_format($data['price'])?></div></div>
        <div class="row"><div class="col-md-12"><b>Qty :</b> <?=$data['qty']?></div></div>
        <div class="row"><div class="col-md-12" style="margin-top:5px;"><b>Total :</b> <?=$data['currencycode']?> <?=number_format($data['total'])?></div></div>
      </div>
    </div>
    <hr />
    <div class="row">
      <div class="col-md-6"><b>Are you sure you want to remove this charge?</b></div>
      <div class="col-md-6 text-right">
        <button type="button" name="remove-charge" class="btn btn-sm btn-danger" data-id="<?=$data['bookingchargesoid']?>">Remove</button>
        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </form>
<?php
}catch(Exception $e){
  echo $e->getMessage();
}
?>
