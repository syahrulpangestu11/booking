<?php
  include("includes/pms-lite/class-pms-lite.php");
  include("includes/pms-lite/reservation-chart/script-reservation-chart.php");

  $bookingoid = $_POST['bookingoid'];
  $hoteloid = $_POST['hotel'];

  $pmslite = new PMSEditRSV($db);
  $pmslite->setPMSHotel($hoteloid);
  $pmslite->setSessionRole($_SESSION['_typepmsusr']);
  $pmslite->setBookingbyID($bookingoid);
  $datarsv = $pmslite->getBookingData();
  $dataroom = $pmslite->getBookingRoomData();

  $pmslitersv = new PMSReservation($db);
  $pmslitersv->setBookingbyID($bookingoid);
  $stayoccupancy = $pmslitersv->summaryStayOccupancy();
  $pmslitersv->RecalculateBooking($datarsv['bookingoid']);

  if(in_array($hoteloid,$avoid_adjust)){
    $adjust = 1;
  }
?>
<style>
  tr.breakdown-rate{ display: none;}
</style>
<section class="content-header">
  <h1>Reservation Chart</h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-circle"></i>  PMS Lite</a></li>
    <li><a href="#"><i class="fa fa-circle"></i>  Reservation Chart</a></li>
  </ol>
</section>
<section id="pms-lite" class="content" style="margin-left: 5px;margin-right: 5px;padding-left: 10px;padding-right: 10px;">
  <div class="box box-form box-success">
    <div class="box-body">
      <div class="row">
        <div class="col-md-6">
          <h1>Booking Number : <?php echo $datarsv['bookingnumber']; ?></h1>
        </div>
        <div class="col-md-6 text-right">
          <i class="fa fa-clock-o"></i> <?php echo $datarsv['bookdate']; ?><br>
          <i class="fa fa-lock"></i> PIN : <?php echo $datarsv['pin']; ?>
        </div>
      </div>
      <hr class="hr-wide" />
      <div class="row">
        <div class="col-md-6">
          <div class="row"><div class="col-md-6"><h3>Guest Details</h3></div><div class="col-md-6 text-right"></div></div>
          <div class="row">
            <div class="col-md-12">
              <h2><?php echo $datarsv['guestname']; ?></h2>
              <?php echo $datarsv['city']; ?>, <?php echo $datarsv['countryname']; ?><br>
              &#9742; <?php echo $datarsv['phone']; ?><br>
              &#9993; <?php echo $datarsv['email']; ?><br>
            </div>
          </div>
          <hr class="hr-dotted">
          <div class="row"><div class="col-md-6"><h3>Sales / Guest Request</h3></div><div class="col-md-6 text-right"></div></div>
          <div class="row">
            <div class="col-md-12">
              <?php if(!empty($datarsv['note'])){ echo $datarsv['note']; }else{ echo "n/a"; } ?>
            </div>
          </div>
          <hr class="hr-dotted">
          <div class="row">
            <div class="col-md-12">
              <h3>Status</h3>
              <?=$datarsv['bookingstatus']?><br>
              Last Updated  <?=date('d F Y H:i:s', strtotime($datarsv['updated']))?> by <?=$datarsv['updatedby']?>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="row">
            <div class="col-md-6">
              <h3>Stay Details</h3>
              <?php
                $q_stay_details = "select count(bookingroomoid), br.checkin, br.checkout, br.night, DATEDIFF(br.checkout, br.checkin) AS nights from bookingroom br inner join booking b using (bookingoid) where b.bookingoid = '".$bookingoid."' group by br.checkin, checkout";
                $stmt = $db->query($q_stay_details);
                $r_stay_detail = $stmt->fetchAll(PDO::FETCH_ASSOC);
                foreach($r_stay_detail as $stay){
              ?>
                <div class="row"><div class="col-md-5">Check in:</div><div class="col-md-5"><?=date('d F Y', strtotime($stay['checkin']))?></div></div>
                <div class="row"><div class="col-md-5">Duration:</div><div class="col-md-5"><?php if(empty($stay['night'])){ echo $stay['nights'];}else{ echo $stay['night'];}?> nights</div></div>
                <div class="row"><div class="col-md-5">Check out:</div><div class="col-md-5"><?=date('d F Y', strtotime($stay['checkout']))?></div></div>
              <?php
                }
              ?>
            </div>
            <div class="col-md-6">
              <div class="row"><div class="col-md-6"><h3>Arrival</h3></div><div class="col-md-6 text-right"></div></div>
              <div class="row"><div class="col-md-5">Arrival Flight:</div><div class="col-md-5"><?=$datarsv['arrivalflight']?></div></div>
              <div class="row"><div class="col-md-5">Arrival Date:</div><div class="col-md-5"><?php if(!empty($datarsv['arrivaldate'])){ echo date('d F Y', strtotime($datarsv['arrivaldate'])); } ?></div></div>
              <div class="row"><div class="col-md-5">Arrival Time:</div><div class="col-md-5"><?php if(!empty($datarsv['arrivaltime'])){ echo date('H:i', strtotime($datarsv['arrivaltime'])); } ?></div></div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <hr class="hr-dotted" />
              <h3>Room Details</h3>
              <div class="row"><div class="col-md-5">Number of Rooms:</div><div class="col-md-5"><?=$stayoccupancy['jmlroom']?> room(s)</div></div>
              <div class="row"><div class="col-md-5">Adults:</div><div class="col-md-5"><?=$stayoccupancy['adult']?></div></div>
              <div class="row"><div class="col-md-5">Children:</div><div class="col-md-5"><?=$stayoccupancy['child']?></div></div>
              <div class="row"><div class="col-md-5">Total Guest:</div><div class="col-md-5"><?=$stayoccupancy['person']?></div></div>
            </div>
            <div class="col-md-6">
              <div class="row title-manage">
                <hr class="hr-dotted" />
                <div class="col-md-6"><h3>Credit Card Details</h3></div>
                <div class="col-md-6 text-right"></div>
              </div>
              <?php
              if($datarsv['cardoid'] != 0){
                $q_cc_dtl = "select bp.*, cc.cardname from bookingpayment bp left join creditcard cc using (cardoid) inner join booking b using (bookingoid) where bp.paymentoid = '".$datarsv['paymentoid']."'";
                $stmt = $db->query($q_cc_dtl);
                $cc_dtl = $stmt->fetch(PDO::FETCH_ASSOC);
              ?>
              <div class="row"><div class="col-md-5">Card</div><div class="col-md-5"><?=$cc_dtl['cardname']?></div></div>
              <div class="row"><div class="col-md-5">Card Number</div><div class="col-md-5"><?=$pmslite->ccMasking($cc_dtl['cardnumber'])?></div></div>
              <div class="row"><div class="col-md-5">Card Holder</div><div class="col-md-5"><?=$cc_dtl['cardholder']?></div></div>
              <div class="row"><div class="col-md-5">Exp Date</div><div class="col-md-5"><?=$cc_dtl['expmonth']?> / <?=$cc_dtl['expyear']?></div></div>
              <div class="row"><div class="col-md-5">CVC</div><div class="col-md-5"><?=$pmslite->cvcMasking($cc_dtl['cvc'])?></div></div>
              <?php
              }
              ?>
            </div>
          </div>
        </div>
      </div>
      <hr />
      <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-12">
              <h3>Promotion / Rate Details</h3>
              <table class="table table-bordered" id="table-detail-rsv">
                <thead>
                  <tr><th rowspan="2">No.</th><th rowspan="2">Room Type</th><th rowspan="2">Room</th><th rowspan="2">Remark</th><th rowspan="2">Guest Name</th><th colspan="2">Stay Periode</th><th colspan="2">Occupancy</th><th rowspan="2">Extra Bed</th><th rowspan="2">Status</th><th rowspan="2">Grand Total (<?=$datarsv['currencycode']?>)</th><th rowspan="2">Action</th></tr>
                  <tr><th>Check-in</th><th>Check-out</th><th>Adult</th><th>Children</th></tr>
                </thead>
                <tbody>
                  <tr style="display:none"></tr>
                  <?php
                    $no = 0;
                    foreach($dataroom as $dr){
                      $room = $pmslite->detailRatePlan($dr['roomofferoid']);
                  ?>
                  <tr>
                    <td><?=++$no?></td>
                    <td><?=$dr['room']?></td>
                    <td><?=$pmslite->getRoomNumber($dr['bookingroomoid'])?></td>
                    <td><?=$dr['promotion']?></td>
                    <td><?=$dr['guestroom']?></td>
                    <td class="text-center"><?=date('d M Y', strtotime($dr['checkin']))?></td>
                    <td class="text-center"><?=date('d M Y', strtotime($dr['checkoutr']))?></td>
                    <td class="text-center"><?=$dr['adult']?></td>
                    <td class="text-center"><?=$dr['child']?></td>
                    <td class="text-center"><?=$dr['extrabed']?></td>
                    <td><?=$dr['pmsstatus']?></td>
                    <td class="text-right"><?=number_format($dr['totalr'] * $adjust,2)?></td>
                    <td class="text-center"></td>
                  </tr>
                  <tr class="breakdown-rate">
                    <td colspan="9" style="padding:10px;">
                      <table class="table table-bordered" id="table-detail-rsv">
                        <thead>
                          <tr><th>Guest Name</th><th>Date</th><th>Rate</th><th>Room Number</th></tr>
                        </thead>
                        <tbody>
                          <tr style="display:none"></tr>
                          <?php
                          foreach($pmslite->roomDateDetail($dr['bookingroomoid']) as $brd){
                          ?>
                          <tr>
                            <td><div class="form-group"><input type="text" class="form-control" name="guest[]" value="<?=$brd['guestroom']?>"></td>
                            <td class="text-center"><?=date('d/M/Y', strtotime($brd['date']))?></div></td>
                            <td class="text-center"><?=$brd['currencycode']." ".number_format($brd['total'] * $adjust,2)?></div></td>
                            <td>
                              <select name="roomnumber[]">
                                <option value="<?=$brd['roomnumberoid']?>"><?=$brd['roomnumber']?></option>
                                <?php
                                foreach($pmslite->roomDateDetail($dr['bookingroomoid']) as $availableroom){
                                ?>
                                <option value="<?=$brd['roomnumberoid']?>"><?=$brd['roomnumber']?></option>
                                <?php
                                }
                                ?>
                              </select>
                            </td>
                          </tr>
                          <?php
                          }
                          ?>
                        </tbody>
                      </table>
                    </td>
                  </tr>
                  <?php
                    }
                  ?>
                </tbody>
                <tfoot>
                  <tr>
                    <th colspan="11">Total Room</th>
                    <th colspan="2"><?=number_format($datarsv['totalroom'] * $adjust,2)?></th>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
          
          <?php if($hide_other_charges == 0){?>

          <hr />
          
          <div class="row title-manage">
            <div class="col-md-6"><h3>Other Charges</h3></div>
            <div class="col-md-6 text-right"></div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <table class="table table-bordered table-other-charge" id="table-detail-rsv">
                <thead>
                  <tr><th>No.</th><th>POS</th><th>Product</th><th>Price (<?=$datarsv['currencycode']?>)</th><th>Qty</th><th>Total (<?=$datarsv['currencycode']?>)</th><th>&nbsp;</th></tr>
                </thead>
                <tbody>
                  <tr style="display:none"></tr>
                  <?php
                  foreach($pmslite->getBookingChargeData() as $key => $othercharges){
                  ?>
                  <tr>
                    <td><?=$key+1?></td>
                    <td><?=$othercharges['pos']?></td>
                    <td><?=$othercharges['product']?></td>
                    <td class="text-right"><?=number_format($othercharges['price'] * $adjust,2)?></td>
                    <td class="text-center"><?=$othercharges['qty']?></td>
                    <td class="text-right"><?=number_format($othercharges['total'] * $adjust,2)?></td>
                    <td class="text-center"></td>
                  </tr>
                  <?php
                  }

                  if(empty($othercharges)){
                    echo "<tr><td colspan='7' class='text-center'><i>no data available</i></td></tr>";
                  }
                  ?>
                </tbody>
                <tfoot>
                  <tr>
                    <th colspan="5">Total Other Charge</th>
                    <th colspan="2"><?=number_format($datarsv['totalcharge'] * $adjust,2)?></th>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>

          <hr />

          <div class="row title-manage">
            <div class="col-md-6"><h3>Payment Details</h3></div>
            <div class="col-md-6 text-right"></div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <table class="table table-bordered table-other-charge" id="table-detail-rsv">
                <thead><tr><th>#</th><th>Date</th><th>Description - References</th><th>Payment</th><th>Administration Fee</th><th>Amount</th><th>&nbsp;</th></tr></thead>
                <tbody>
                  <tr style="display:none;"><td>&nbsp;</td></tr>
                  <?php
                  $i = 1;
                  $q_paydtl = "select bpd.*, c.currencycode from bookingpaymentdtl bpd inner join currency c using (currencyoid) inner join booking b using (bookingoid) where b.bookingoid = '".$datarsv['bookingoid']."'";
                  $stmt = $db->query($q_paydtl);
                  $r_paydtl = $stmt->fetchAll(PDO::FETCH_ASSOC);
                  foreach($r_paydtl as $paydtl){
                  ?>
                  <tr>
                    <td><?=$i++?></td>
                    <td><?=date('d M Y', strtotime($paydtl['paymentdate']))?></td>
                    <td><?=$paydtl['description']?></td>
                    <td class="text-right"><?=$paydtl['currencycode']?> <?=number_format($paydtl['amount'] * $adjust,2)?></td>
                    <td class="text-right"><?=$paydtl['currencycode']?> <?=number_format($paydtl['admnistrationfee'] * $adjust,2)?></td>
                    <td class="text-right"><?=$paydtl['currencycode']?> <?=number_format($paydtl['total'] * $adjust,2)?></td>
                    <td class="text-center"></td>
                  </tr>
                  <?php
                  }
                  ?>
                </tbody>
              </table>
            </div>
          </div>

          <?php }?>

        </div>
      </div>
          
      <?php if($hide_other_charges == 0){?>

      <hr />

      <div class="row">
        <div class="col-md-4">
          <div class="row"><div class="col-md-6"><h3>Source Detail</h3></div><div class="col-md-6 text-right"><a href="#" data-toggle="modal" data-target="#sourceDetails"><i class="fa fa-pencil"></i> Edit</a></div></div>
          <?php
            $source = $pmslite->BookingSource();
          ?>
          <div class="row">
            <div class="col-md-12">
              <div class="row"><div class="col-md-5">Channel:</div><div class="col-md-5"><?=$source['bookingchannel']?></div></div>
              <div class="row"><div class="col-md-5">Booking Market:</div><div class="col-md-5"><?=$source['bookingmarket']?></div></div>
            </div>
          </div>
          <?php if($datarsv['agentoid'] != '0'){ ?>
          <div class="row" style="margin-top:10px">
            <div class="col-md-12">
              <div class="row"><div class="col-md-5">Travel Agent:</div><div class="col-md-5"><?=$source['agentname']?></div></div>
              <div class="row"><div class="col-md-5">Agent Phone:</div><div class="col-md-5"><?=$source['agentphone']?></div></div>
              <div class="row"><div class="col-md-5">Agent Email:</div><div class="col-md-5"><?=$source['agentemail']?></div></div>
            </div>
          </div>
          <!--
          <div class="row">
            <div class="col-md-12">
              <button type="button" class="btn btn-default" name="email-invoice-agent">Email invoive to agent</button>
              <a href="<?=$base_url?>/pms-lite/view/invoice-agent.php?bookingnumber=<?=$datarsv['bookingnumber']?>" target="_blank"><button type="button" class="btn btn-primary" name="email-invoice-agent">Print invoice to agent</button></a>
            </div>
          </div>
          -->
          <?php } ?>
          <div class="row" style="margin-top:10px">
            <div class="col-md-12">
              <div class="row"><div class="col-md-5">UTM:</div><div class="col-md-5"><?=$datarsv['utm_source']?> -<?=$datarsv['utm_campaign']?></div></div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <h3>Commission Breakdown</h3>
          <table class="table table-bordered" id="table-reservation-chart">
            <tr style="display:none"><td>&nbsp;</td></tr>
            <tr><td>TheBuking Commissionable Amount</td><td class="text-right"><?php echo number_format($datarsv['grandtotalr'] * $adjust,2); ?></td></tr>
            <tr><td>TheBuking Commission</td><td class="text-right"><?php echo number_format($datarsv['gbhcollect'] * $adjust,2); ?></td></tr>
            <tr><td>Total Confirmed to Hotel</td><td class="text-right"><?php echo number_format($datarsv['hotelcollect'] * $adjust,2); ?></td></tr>
            <tr><td>Affiliate Commission</td><td class="text-right"><?php echo $promocode_comm; ?></td></tr>
          </table>
          <h3>Promo Code</h3>
          <table class="table table-bordered" id="table-reservation-chart">
            <tr style="display:none"><td>&nbsp;</td></tr>
            <tr><td>Promo Code</td><td><?=$datarsv['promocode']?></td></tr>
            <tr><td>PIC</td><td></td></tr>
            <tr><td>Commission</td><td><?php echo $promocode_comm; ?></td></tr>
          </table>
        </div>
        <div class="col-md-4">
          <h3>Total Cost Confirmed with Guest</h3>
          <table class="table table-bordered" id="table-reservation-chart">
            <tr style="display:none"><td>&nbsp;</td></tr>
            <tr><td>Room Rate</td><td class="text-right"><?php echo $datarsv['currencycode']." ".number_format($datarsv['totalroom'] * $adjust,2); ?></td></tr>
            <tr><td>Room Extra Bed Rate</td><td class="text-right"><?php echo $datarsv['currencycode']." ".number_format($extrabedtotal * $adjust,2); ?></td></tr>
            <tr><td>Extra Total</td><td class="text-right"><?php echo $datarsv['currencycode']." ".number_format($datarsv['totalextra'] * $adjust,2); ?></td></tr>
            <tr><td>Additional Charge</td><td class="text-right"><?php echo $datarsv['currencycode']." ".number_format($datarsv['totalcharge'] * $adjust,2); ?></td></tr>
            <tr><td>Total Confirmed to Guest</td><td class="text-right"><?php echo $datarsv['currencycode']." ".number_format($datarsv['grandtotalr'] * $adjust,2); ?></td></tr>
            <tr><td>Guest Deposit Info from IBE</td><td class="text-right"><?php echo $datarsv['currencycode']." ".number_format($datarsv['granddeposit'] * $adjust,2); ?></td></tr>
            <tr><td>Guest Balace Info from IBE</td><td class="text-right"><?php echo $datarsv['currencycode']." ".number_format($datarsv['grandbalance'] * $adjust,2); ?></td></tr>
          </table>
        </div>
      </div>
      <hr />
      <div class="row">
        <div class="col-md-12 text-right">
          <?php
            $q_paydtl = "select sum(amount), c.currencycode from bookingpaymentdtl bpd left join creditcard cc using (cardoid) inner join bookingpayment using (paymentoid) inner join currency c using (currencyoid) inner join booking b using (bookingoid) where b.bookingoid = '".$bookingoid."' group by b.bookingoid";
          ?>
          <div class="row">
            <div class="col-md-10"><h3>Total</h3></div><div class="col-md-2"><h3><?php echo $datarsv['currencycode']." ".number_format($datarsv['grandtotalr'] * $adjust,2); ?></h3></div>
            <div class="col-md-10"><h3>Additional Charges</h3></div><div class="col-md-2"><h3><?php echo $datarsv['currencycode']." ".number_format($datarsv['totalcharge'] * $adjust,2); ?></h3></div>
            <div class="col-md-10"><h3>Tax</h3></div><div class="col-md-2"><h3>-</h3></div>
            <div class="col-md-10"><h3>Deposit</h3></div><div class="col-md-2"><h3><?php echo $datarsv['currencycode']." ".number_format($datarsv['paid'] * $adjust,2); ?></h3></div>
            <div class="col-md-10"><h3>Balance</h3></div><div class="col-md-2"><h3><?php echo $datarsv['currencycode']." ".number_format($datarsv['paid_balance'] * $adjust,2); ?></h3></div>
          </div>
        </div>
      </div>
      <hr />
      <?php /*<div class="row">
        <div class="col-md-6" id="actButton"></div>
        <div class="col-md-6 text-right">
          <button type="button" class="btn btn-primary" name="view-invoice">View Invoice</button>
        </div>
      </div>*/?>
      <br>

      <?php }?>

      <div class="row">
        <div class="col-md-12">
          <a href="<?=$base_url?>/pms-lite/reservation-chart"><button type="button" class="btn btn-default" name="back"><i class="fa fa-undo"></i> Back To Reservation Chart</button></a>
        </div>
      </div>
    </div>
  </div>
</section>

<form id="invoice-guest" action="<?=$base_url?>/pms-lite/view/invoice-guest.php" target="_blank" method="post">
  <input name="bookingnumber" type="hidden" value="<?=$datarsv['bookingnumber']?>">
  <input name="hotel" type="hidden" value="<?=$hoteloid?>">
</form>
<form id="print-invoice-guest" action="<?=$base_url?>/pms-lite/print/pinvoice-guest.php" target="_blank" method="post">
  <input name="bookingnumber" type="hidden" value="<?=$datarsv['bookingnumber']?>">
  <input name="hotel" type="hidden" value="<?=$hoteloid?>">
</form>
<form id="reservation-edit" action="<?=$base_url?>/pms-lite/reservation-detail" method="post">
  <input name="bookingnumber" type="hidden" value="<?=$datarsv['bookingnumber']?>">
  <input name="hotel" type="hidden" value="<?=$hoteloid?>">
</form>
<?php include('modal-edit-rsv.php'); ?>

<?php $bookingoid = $datarsv['bookingoid'];?>
<style> @media (min-width: 768px) { #cancelModal .modal-dialog, #noShowModal .modal-dialog {width: 80%;} }</style>
<div class="mdbook modal fade" id="cancelModal" tabindex="-1" role="dialog" aria-labelledby="cancelModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form method="post" action="#">
      <input type="hidden" name="pms" value="1" />
      <input type="hidden" name="bid" value="<?=$bookingoid?>" />
      <input type="hidden" name="changestatus" value="" />
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"></h4>
      </div>
      <div class="modal-body">
      	<?php include("includes/booking/modify/page-modify.php"); ?>
      	<div align="right"><input type="checkbox" name="sendtoguest" value="1"> CC this email to Guest &nbsp;</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-sm btn-danger submit">Submit</button>
      </div>
      </form>
    </div>
  </div>
</div>
<div class="mdbook modal fade" id="noShowModal" tabindex="-1" role="dialog" aria-labelledby="noShowModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form method="post" action="#">
      <input type="hidden" name="pms" value="1" />
      <input type="hidden" name="bid" value="<?=$bookingoid?>" />
      <input type="hidden" name="changestatus" value="" />
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"></h4>
      </div>
      <div class="modal-body">
      	<?php include("includes/booking/modify/page-modify.php"); ?>
      	<div align="right"><input type="checkbox" name="sendtoguest" value="1"> CC this email to Guest &nbsp;</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-sm btn-danger submit">Submit</button>
      </div>
      </form>
    </div>
  </div>
</div>
<div class="mdbook modal fade" id="NotificationModal" tabindex="-1" role="dialog" aria-labelledby="NotificationModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-info-circle"></i> Notification</h4>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>
<div class="mdbook modal fade" id="AlertModal" tabindex="-1" role="dialog" aria-labelledby="AlertModal">
  <div class="modal-dialog" role="document" style="width:300px;">
    <div class="modal-content"  style="color:#FFF">
      <div class="modal-header" style="background-color: #ad3223;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-info-circle"></i> Alert</h4>
      </div>
      <div class="modal-body" style="background-color: #FFF; color:#000">
      </div>
      <div class="modal-footer" style="background-color: #ad3223;">
        <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="changeCurrency" tabindex="-1" role="dialog" aria-labelledby="changeCurrency">
  <div class="modal-dialog" role="document" style="width:300px;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="fa fa-info-circle"></i> Change Currency</h4>
      </div>
      <div class="modal-body" style="background-color: #FFF; color:#000">
        <form id="form-edit-currency">
          <input type="hidden" name="bid" value="<?=$bookingoid?>" />
          <div class="form-group">
            <label>Change Currency to :</label>
            <select class="form-control" name="currency">
              <?php
              $stmt = $db->prepare("select * from currency where publishedoid = '1'");
              $stmt->execute();
              $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
              foreach($result as $currency){
              ?>
              <option value="<?=$currency['currencyoid']?>" <?=$selected?>><?=$currency['currencycode']?></option>
              <?php
              }
              ?>
            </select>
          </div>
          <div class="form-group">
            <label>Apply this currency to :</label>
            <div class="checkbox"><label><input type="checkbox" value="y" name="room" checked> Room</label></div>
            <div class="checkbox"><label><input type="checkbox" value="y" name="charges" checked> Other Charges</label></div>
            <div class="checkbox"><label><input type="checkbox" value="y" name="payment" checked> Payment</label></div>
          </div>
        </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" name="save" class="btn btn-primary" data-dismiss="modal">Update</button>
      </div>
    </div>
  </div>
</div>
