<?php
try{
  include("includes/pms-lite/reservation-chart/script-reservation-chart.php");
  include("includes/pms-lite/class-pms-lite.php");
  $pmslite = new PMSLite($db);
  $pmslite->setSessionRole($_SESSION['_typepmsusr']);

  if(!isset($_POST['startdate']) and !isset($_POST['enddate'])){
    $_POST['startdate'] = date('d F Y', strtotime(date('d F Y'). ' -1 days'));
    $_POST['enddate'] = date('d F Y', strtotime($_POST['startdate']. ' +30 days'));
  }
?>
<section class="content-header">
  <h1>Reservation Chart</h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-circle"></i>  PMS Lite</a></li>
    <li><a href="#"><i class="fa fa-circle"></i>  Reservation Chart</a></li>
  </ol>
</section>
<section id="pms-lite" class="content">
  <form class="form-inline" method="post" action="<?=$base_url?>/pms-lite/reservation-chart/">
  <div class="box box-form box-warning">
    <div class="box-body">
      <div class="row">
        <div class="col-md-12">
          <h1 style="margin-bottom:10px;">FRONT DESK</h1>
        </div>
      </div>
      <div class="row">
        <?php if(empty($_SESSION['_hotel'])){ ?>
          <div class="col-md-3 col-xs-6">
            <select class="form-control" name="zarea" id="zarea" style="width:100%">
              <option value="" selected>Show All Area</option>
              <?php
                $s_hotelcity = "select ct.cityoid, ct.cityname, h.hotelname from hotel h inner join city ct using (cityoid) inner join chain c using (chainoid) where ((h.chainoid in (select oid from userassign where type = 'chainoid' and useroid = '".$_SESSION['_oid']."')) or (h.hoteloid in (select oid from userassign where type = 'hoteloid' and useroid = '".$_SESSION['_oid']."'))) and h.publishedoid = '1' group by ct.cityoid";
                $q_hotelcity = $db->query($s_hotelcity);
                $r_hotelcity = $q_hotelcity->fetchAll(PDO::FETCH_ASSOC);
                $x=0; $htlname="";
                foreach($r_hotelcity as $hotelcity){
                  if($hotelcity['cityoid'] == $_POST['zarea']){ $selected = "selected"; }else{ $selected = ""; }
                  if($x==0 and !isset($_REQUEST['zarea'])){ $x = 1; $selected = "selected"; $_REQUEST['zarea'] = $hotelcity['cityoid'];  }
              ?>
                <option value="<?=$hotelcity['cityoid']?>" <?=$selected?>><?=$hotelcity['cityname']?></option>
              <?php
                }
              ?>
            </select>
          </div>
          <div class="col-md-3 col-xs-6">
              <input type="text" class="form-control" name="hotelname" style="width:100%" placeholder="Hotel, villa, residence name">
              <input type="hidden" name="zhotel">
          </div>
        <?php } ?>
        <div class="col-md-6 text-right">
          <div class="form-group">
            <label>Periode</label>
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
              <input type="text" class="form-control" name="startdate" id="startdate" readonly="readonly" value="<?=date('d F Y', strtotime($_POST['startdate']))?>">
            </div>
          </div>
          <div class="form-group">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
              <input type="text" class="form-control" name="enddate" id="enddate" readonly="readonly" value="<?=date('d F Y', strtotime($_POST['enddate']))?>">
            </div>
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-primary">CHECK</button>
          </div>
        </div>
      </div>
      <?php
        if((!empty($_SESSION['_typepmsusr']) and empty($_SESSION['_hotel'])) or ($_SESSION['_typeusr'] == '4' and empty($_SESSION['_hotel']))){
          $filter = array();
          if(!empty($_REQUEST['zarea']) and empty($_POST['hotelname'])){ array_push($filter, 'h.cityoid = "'.$_REQUEST['zarea'].'"'); }
          if(!empty($_POST['zhotel'])){ array_push($filter, 'h.hotelcode = "'.$_POST['zhotel'].'"'); }
          if(!empty($_POST['hotelname'])){ array_push($filter, 'h.hotelname like "%'.$_POST['hotelname'].'%"'); }

          if(count($filter) > 0){ $combine_filter = ' and '.implode(' and ',$filter); }else{ $combine_filter = ""; }

          $s_hotelchain = "select h.hoteloid, h.hotelcode, h.hotelname from hotel h inner join chain c using (chainoid) where (h.chainoid in (select oid from userassign where type = 'chainoid' and useroid = '".$_SESSION['_oid']."' ".$combine_filter.") or h.hoteloid in (select oid from userassign where type = 'hoteloid' and useroid = '".$_SESSION['_oid']."' ".$combine_filter.")) and h.publishedoid = '1'";

        }else{
          $s_hotelchain = "select h.hoteloid, h.hotelcode, h.hotelname from hotel h inner join chain c using (chainoid) where  h.hoteloid = '".$_SESSION['_hotel']."' and h.publishedoid = '1'";
        }
        //print_r($s_hotelchain);
        $q_hotelchain = $db->query($s_hotelchain);
        $r_hotelchain = $q_hotelchain->fetchAll(PDO::FETCH_ASSOC);

        $s_pmsstatus = "select * from pmsstatus where pmsstatusoid not in ('0','1','3','8') and publishedoid = '1'";
        $q_pmsstatus = $db->query($s_pmsstatus);
        $r_pmsstatus = $q_pmsstatus->fetchAll(PDO::FETCH_ASSOC);

        foreach($r_hotelchain as $hotelchain){
          $pmslite->setPMSHotel($hotelchain['hoteloid']);
      ?>
      <!------------------------------------------------------------------>
      <div class="row">
        <div class="col-md-12 pms-legend text-right">
          Legend :
          <?php
          foreach($r_pmsstatus as $pmsstatus){
            echo "<span class='".$pmslite->convertPMSStatus($pmsstatus['pmsstatusoid'])."'></span>".$pmsstatus['status']." ";
          }
          echo "<span class='ooo'></span>Out of Order";
          ?>
        </div>
        <div class="table-responsive col-md-12">
          <?php
            $pmslite->setPeriodePMS(date('Y-m-d', strtotime($_POST['startdate'])), date('Y-m-d', strtotime($_POST['enddate'])));
            $dataDateTable = $pmslite->dataDateTable();
            $numberDateColumn = count($dataDateTable);
            $roomTypeTable = $pmslite->roomTypeTable();
            $dataBlockedTable = $pmslite->dataBlockedTable();
          ?>
          <table id="table-reservation-chart" class="table table-bordered">
            <tr class="hotelname"><td colspan="32"><h2><?=$hotelchain['hotelname']?></h2></td></tr>
            <?php if(in_array($_SESSION['_typepmsusr'], array(1, 3))){ ?>
            <tr class="blocked">
              <td>Blocked</td>
              <?php foreach($dataBlockedTable as $blockedColumn){ ?>
              <td><a href="#" data-toggle="modal" data-target="#assignRoom" data-hotel="<?=$hotelchain['hoteloid']?>" data-date="<?=$blockedColumn['date']?>"><div class="full-box"><?=$blockedColumn['unassigned']?></div></a></td>
              <?php } ?>
            </tr>
            <?php } ?>
            <tr class="date-periode">
              <td>&nbsp;</td>
              <?php foreach($dataDateTable as $dateColumn){ ?>
              <td><?=$dateColumn['day']?><br><?=$dateColumn['month']?></td>
              <?php } ?>
            </tr>
            <?php
              foreach($roomTypeTable as $roomtypeRow){
                $roomNumber = $pmslite->roomNumber($roomtypeRow['id']);
                //print_r($roomNumber);
            ?>
            <tr class="roomtype"><td colspan="<?=$numberDateColumn+1?>"><?=$roomtypeRow['name']?></td></tr>
              <?php
              foreach($roomNumber as $NumberRow){
                $roomAssignedColumn = $pmslite->dataRoomAssignedTable($roomtypeRow['id'], $NumberRow['id']);
                //print_r($roomAssignedColumn);
              ?>
              <tr class="roomnumber <?=$_SESSION['_typepmsusr']?>">
                <td>
                <?php 
                  if($_SESSION['_typepmsusr'] == '1'){
                    echo '<a title="Click to set Out-of-order" data-toggle="modal" data-target="#createOOO" data-date="'.date('Y-m-d', strtotime($_POST['startdate'])).'" data-room="'.$roomtypeRow['id'].'" data-roomnumber="'.$NumberRow['id'].'" data-hotel="'.$hotelchain['hoteloid'].'">'.$NumberRow['roomnumber'].'</a>';
                  }else{
                    echo $NumberRow['roomnumber'];
                  }
                ?>
                </td>
                <?php foreach($roomAssignedColumn as $roomAssign){ echo $roomAssign;  } /* foreach date assign */ ?>
              </tr>
              <?php
              } /* foreach room number */
              ?>
            <?php
              } /* foreach room type */
            ?>

          </table>
        </div>
      </div>
      <!-------------------------------------------------------------------->
      <?php
        }
      ?>
    </div>
  </div>
  </form>
</section>
<?php
  include('modal-reservation-chart.php');
}catch(Exception $e){
  echo 'Caught exception: '.$e->getMessage();
}
?>
