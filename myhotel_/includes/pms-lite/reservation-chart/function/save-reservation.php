<?php
try{
  session_start();
	error_reporting(E_ALL ^ E_NOTICE);
	include ('../../../../conf/connection.php');
  include ('../../class-pms-lite.php');

  $hoteloid = $_POST['hotel'];
  $pmslite = new PMSReservation($db);
  $pmslite->setPMSHotel($hoteloid);
  $tknSession = $pmslite->hotelcode."_u".$_SESSION['_oid'];

  $bookingmarket = $_POST['bookingmarket'];

  /* SAVE GUEST ----------------------------------------------------------------- */
  $set_defaultname = $_POST['firstname']." ".$_POST['lastname'];

  if(empty($_POST['cust'])){
    $pmslite->saveGuest($_POST['title'], $_POST['firstname'], $_POST['lastname'], $_POST['email'], $_POST['phone'], $_POST['city'], $_POST['country']);
  }else{
    $pmslite->customeroid = $_POST['cust'];
  }

  /* SAVE BOOKING BASIC ----------------------------------------------------------------- */
  $bookingtime = date("Y-m-d H:i:s");
	$timelimit = date("Y-m-d H:i:s", strtotime($bookingtime. '+48 hour'));

  if($_POST['bookingmarket'] == 3 or $_POST['bookingmarket'] == 5){ $agent = $_POST['agent']; }else{ $agent = 0; }

  if(strtotime(date('Y-m-d')) <= strtotime($_POST['checkin'])){
    $bookingnumber = $pmslite->generateookingNumber();
  }else{
    $bookingnumber = $pmslite->generateBookingNumberLast($_POST['checkin']);
    $bookingtime = date("Y-m-d H:i:s", strtotime($_POST['checkin']));
  	$timelimit = date("Y-m-d H:i:s", strtotime($bookingtime. '+48 hour'));
  }

  $pmslite->saveBookingBasic($hoteloid, $bookingnumber, $pmslite->randomPin(4), $pmslite->customeroid, $bookingtime, $timelimit, 4, $_POST['bookingchannel'], $_POST['bookingmarket'], $agent, 1, $_POST['note']);

	/*------------------------------------------------------------------*/

	/*default commission*/
	$commission = 5;
	/*base commission*/
	$query_base_commission = "select value as base from hotelcommission where hoteloid = '".$hoteloid."' and ( (startdate <= '".$today."' and enddate >= '".$today."') or (startdate <= '0000-00-00' and enddate >= '0000-00-00')) and type = 'base' limit 1";
	$stmt	= $db->query($query_base_commission);
	if($stmt->rowCount()){
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		$commission = $result['base'];
	}
	/*override commission*/
	$query_override_commission = "select value as override from hotelcommission where hoteloid = '".$hoteloid."' and ( (startdate <= '".$today."' and enddate >= '".$today."') or (startdate <= '0000-00-00' and enddate >= '0000-00-00')) and type = 'override' order by priority ASC limit 1";
	$stmt	= $db->query($query_override_commission);
	if($stmt->rowCount()){
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		$commission = $result['override'];
	}

	/*------------------------------------------------------------------*/
	$grandtotal		= 0;
	$granddeposit	= 0;
	$grandbalance	= 0;

  if($bookingmarket == "1"){

  	$session_transaction = "select sum(total) as total, sum(deposit) as deposit, sum(balance) as balance, c.currencyoid from bookingtemp left join currency c using (currencyoid) where session_id = '".$tknSession."'";
  	$stmt	= $db->query($session_transaction);
  	$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
  	foreach($result as $row){

  		$currencyoid = $row['currencyoid'];
  		$grandtotal +=  $row['total'];
  		$granddeposit +=  $row['deposit'];
  		$grandbalance +=  $row['balance'];

  		$hotelcollect = $grandtotal * (100 - $commission) / 100 ;
  		$gbhcollect = $grandtotal * $commission / 100 ;
  	}

  	/*------------------------------------------------------------------*/

  	$promocode_transaction = "select count(bookingtempoid) as appliedcommission, x.* from bookingtemp bt inner join (select * from promocode where hoteloid = '".$hoteloid."' and publishedoid = '1') x on x.promocode = bt.promocode where session_id = '".$tknSession."' and bt.promocode <> '' group by bt.promocode";
  	$stmt	= $db->query($promocode_transaction);
  	$result = $stmt->fetch(PDO::FETCH_ASSOC);
  	if($result['appliedcommission'] > 0){
  		$promocode = $result['promocode'];
  		$pc_pic = $result['pic_name'];
  		$pc_picemail = $result['pic_email'];
  		if($result['discounttype'] == "discount percentage"){
  			$pc_desc = "discount ".$result['discount']."%";
  		}else{
  			$pc_desc = "discount ".$result['discount']."%";
  		}

  		$session_pc_commission = "select sum(promocodecomm) as promocodecomm from bookingtemp where session_id = '".$tknSession."'";
  		$stmt	= $db->query($session_pc_commission);
  		$result_pc_commission = $stmt->fetch(PDO::FETCH_ASSOC);
  		$pc_commission = $result_pc_commission['promocodecomm'];

  		if($result['commissiontype'] == "commission percentage"){
  			$pc_commissionpercentage = $result['commission'];
  		}else if($result['commissiontype'] == "commission amount"){
  			$pc_commissionpercentage = 0;
  		}

  	}else{
  		$promocode = 'thebuking';
  		$pc_desc = '';
  		$pc_pic = '';
  		$pc_picemail = '';
  		$pc_commission = 0;
  		$pc_commissiopercentage = 0;
  	}
    $currencyoid = 1;
  }else{
    $currencyoid = $_POST['currency'];

    foreach($_POST['roomtotal'] as $key => $roomtotal){
      $grandtotal = $grandtotal + $roomtotal;
      $granddeposit = 0;
      $grandbalance = $grandtotal;

      $hotelcollect = $grandtotal * (100 - $commission) / 100 ;
  		$gbhcollect = $grandtotal * $commission / 100 ;
    }

    $promocode = 'thebuking';
    $pc_desc = '';
    $pc_pic = '';
    $pc_picemail = '';
    $pc_commission = 0;
    $pc_commissiopercentage = 0;

  }

  $stmt	= $db->prepare("update booking set grandtotal = :a, grandtotalr = :a1, gbhpercentage = :b, gbhcollect = :c, hotelcollect = :d, granddeposit = :e, grandbalance = :f, promocode = :g, promocodecomm = :h, promocodedesc = :i, promocodeaffiliasi = :j, pcaffpicname = :k, pcaffpicemail = :l, promocodecommpercentage = :m, currencyoid = :o, updated = :p, updatedby =:q where bookingoid = :n");
  $stmt->execute(array(':a' => $grandtotal, ':a1' => $grandtotal, ':b' => $commission, ':c' => $gbhcollect, ':d' => $hotelcollect, ':e' => $granddeposit, ':f' => $grandbalance, ':g' => $promocode, ':h' => $pc_commission, ':i' => $pc_desc, ':j' => 'yes', ':k' => $pc_pic, ':l' => $pc_picemail, ':m' => $pc_commissionpercentage, ':n' => $pmslite->bookingoid, ':o' => $currencyoid , ':p' => date('Y-m-d H:i:s'), ':q' => $_SESSION['_user']));


  if($bookingmarket == '1'){
    include('save-reservation-bookingtemp.php');
  }else{
    include('save-reservation-customprice.php');
  }


  $pmslite->RecalculateBooking($pmslite->bookingoid);
  /*-----------------------------------------------------------*/

  $payment_type = "cc";
	$stmt = $db->prepare("insert into bookingpayment (`bookingoid`, `type`) values (:a , :b)");
	$stmt->execute(array(':a' => $pmslite->bookingoid, ':b' => $payment_type));
	$paymentoid = $db->lastInsertId();

  echo "1";

}catch(Exception $e){
  echo $e->getMessage();
}
?>
