<?php
try{
  session_start();
	error_reporting(E_ALL ^ E_NOTICE);
	include ('../../../../conf/connection.php');
  include ('../../../../conf/baseurl.php');
  include ('../../class-pms-lite.php');

  $oooid = $_POST['oooid'];

  $pmslite = new PMSLite($db);
  $data = $pmslite->getOOO($oooid);
  /*--------------------------------------*/

?>
  <form id="form-view-ooo" method="post">
    <input type="hidden" name="oooid" value="<?=$oooid?>">
    <div class="row">
			<div class="col-md-6"><h3>Room Type : <?=$data['room']?></h3></div>
      <div class="col-md-6"><h3>Room Number : <?=$data['roomnumber']?></h3></div>
		</div>
    <hr />
    <div class="row">
      <div class="col-md-12">
        <h4><span class="blue">Details :</span></h4>
        <div class="row"><div class="col-md-12"><b>From :</b> <?=date('d F Y', strtotime($data['startdate']))?></div></div>
        <div class="row"><div class="col-md-12"><b>To :</b> <?=date('d F Y', strtotime($data['enddate']))?></div></div>
        <div class="row"><div class="col-md-12"><b>Reason :</b> <?=$data['reason']?></div></div>
      </div>
    </div>
    <hr />
    <div class="row">
      <div class="col-md-12 text-right">
        <?php if($_SESSION['_typepmsusr'] == '4' or $_SESSION['_typepmsusr'] == '1'){ ?>
        <button type="button" name="edit" class="btn btn-sm btn-primary">Edit</button>
        <button type="button" name="clear-date" class="btn btn-sm btn-danger">Clear Date</button>
        <?php } ?>
        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </form>
  <form id="view-reservation-detail" action="https://localhost/thebuking/myhotel/pms-lite/reservation-detail" method="post">
    <input name="bookingoid" type="hidden" value="<?=$data['bookingoid']?>">
    <input name="hotel" type="hidden" value="<?=$hoteloid?>">
  </form>
<?php
}catch(Exception $e){
  echo $e->getMessage();
}
?>
