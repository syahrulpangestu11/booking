<?php
try{
  session_start();
	error_reporting(E_ALL ^ E_NOTICE);
	include ('../../../../conf/connection.php');
  include ('../../class-pms-lite.php');

  $hoteloid = $_SESSION['_hotel'];
  $pmslite = new PMSReservation($db);
  $pmslite->setPMSHotel($hoteloid);

  $checkin = date('Y-m-d', strtotime($_POST['checkin']));
  $checkout = date('Y-m-d', strtotime($_POST['checkout']));
  $bookingroomoid = $_POST['bookingroomoid'];
  $pmsstatus = $_POST['pmsstatus'];
  $bookingroom = $pmslite->dataBookingRoom($bookingroomoid);
  $brd = $pmslite->dataBookingRoomDtlNonAssigned($bookingroomoid);


    $stmt = $db->prepare("UPDATE `bookingroom` SET `guestroom` = :a, `adult` = :b, `child` = :c, `pmsstatusoid` = :d WHERE bookingroomoid = :id");
    $stmt->execute(array(':id' => $bookingroomoid, ':a' => $_POST['guest'], ':b' => $_POST['adult'], ':c' => $_POST['child'], ':d' => $_POST['pmsstatus']));
    
        // Prepare Data
        $stmt = $db->prepare("SELECT `roomoid` FROM `roomoffer` INNER JOIN `bookingroom` USING(`roomofferoid`) WHERE `bookingroomoid` = :a");
        $stmt->execute(array(':a' => $bookingroomoid));
        $r_ro = $stmt->fetch(PDO::FETCH_ASSOC);
        $vroomoid = $r_ro['roomoid'];
        
        $stmt = $db->prepare("SELECT COUNT(`roomnumberoid`) AS jml FROM `roomnumber` WHERE `roomoid` = :a AND `publishedoid` = 1");
        $stmt->execute(array(':a' => $vroomoid));
        $r_ro = $stmt->fetch(PDO::FETCH_ASSOC);
        $vroomnumber = $r_ro['jml'];
        
        $data6 = '{
            "token": "pfUxpxmKTJNTStNhPK2Nly4UKxbcVWwNRUT1SXnPfRiWqhvmINnJ4XnefNNBuTsx",
            "hcode": "'.$pmslite->hotelcode.'",
            "roomid": "'.$vroomoid.'",
            "data": [';
        $z = 0;

    foreach($brd as $key2 => $br_detail){
      $date = $_POST['date'][$key2];
      $roomnumber = $_POST['roomnumber'][$key2];
      $stmt = $db->prepare("update `bookingroomdtl` set guestroom = :a, roomnumberoid = :b where bookingroomdtloid = :id");
      $stmt->execute(array(':a' => $_POST['guest'], ':b' => $roomnumber, ':id' => $br_detail['bookingroomdtloid']));
      
        // Prepare Data
        $stmt = $db->prepare("SELECT `date` FROM `bookingroomdtl` WHERE `bookingroomdtloid` = :a");
        $stmt->execute(array(':a' => $br_detail['bookingroomdtloid']));
        $r_rd = $stmt->fetch(PDO::FETCH_ASSOC);
        $vdate = $r_rd['date'];
        
        $stmt = $db->prepare("SELECT COUNT(`bookingroomdtloid`) jml
                FROM `bookingroomdtl` brd
                    LEFT JOIN `bookingroom` br USING(`bookingroomoid`)
                    LEFT JOIN `booking` b USING(`bookingoid`)
                    LEFT JOIN `roomoffer` ro USING(`roomofferoid`)
                WHERE b.`bookingstatusoid` IN (4) AND br.`pmsstatusoid` IN (2,4,7) 
                	AND ro.`roomoid` = :a AND brd.`date` = :b");
        $stmt->execute(array(':a' => $vroomoid, ':b' => $vdate));
        $r_ro = $stmt->fetch(PDO::FETCH_ASSOC);
        $vtxnumber = $r_ro['jml'];
        
        $vval = $vroomnumber - $vtxnumber;
        
        if($z != 0){ $data6 .=","; }else{ $z = 1; }
        $data6 .= '
            {
                "from": "'.$vdate.'",
                "to": "'.$vdate.'",
                "value": "'.$vval.'"
            }
        ';
        
    }
    
        // Prepare Data
        $data6 .= '
            ]
        }';
    
        if($_POST['pmsstatus'] == '2' || $_POST['pmsstatus'] == 7){
            // The data to send to the API
            $postData = (array) json_decode($data6);
            
            // Setup cURL
            $ch = curl_init('https://thebuking.com/apiv1/save/allotment_inrange/');
            curl_setopt_array($ch, array(
                CURLOPT_POST => TRUE,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_SSL_VERIFYPEER => FALSE,
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json'
                ),
                CURLOPT_POSTFIELDS => json_encode($postData)
            ));
            
            // Send the request
            $response = curl_exec($ch);
            
            // Check for errors
            if($response === FALSE){
                die(curl_error($ch));
            }
            
            // Decode the response
            $responseData = json_decode($response, TRUE);
        }

  //print_r($response);
  echo "1";

}catch(Exception $e){
  echo $e->getMessage();
}
?>
