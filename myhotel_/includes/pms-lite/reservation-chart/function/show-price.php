<?php
try{
  session_start();
	error_reporting(E_ALL ^ E_NOTICE);
	include ('../../../../conf/connection.php');
  include ('../../class-pms-lite.php');

  $hoteloid = $_POST['hotel'];
  $checkin = date('Y-m-d', strtotime($_POST['checkin']));
  $checkout = date('Y-m-d', strtotime($_POST['checkout']));
  $roomoid = $_POST['room'];
  $roomofferoid = $_POST['roomofferoid'];
  $roombooked = $_POST['rooms'];
  $roomnumber = $_POST['roomnumber'];

  $pmslite = new PMSReservation($db);
  $pmslite->setPMSHotel($hoteloid);
  $pmslite->loadXML();
  $pmslite->setRoom($roomoid);
  $room = $pmslite->roomType();

  $rplan = $pmslite->detailRatePlan($roomofferoid);

  $night = $pmslite->dateDiff($checkin, $checkout);
  /*--------------------------------------*/
  $_SESSION['tokenSession'.$pmslite->hotelcode] = $pmslite->hotelcode."_u".$_SESSION['_oid'];
  /*--------------------------------------*/
  $bookingmarket = $_POST['bookingmarket'];

  $show_list_date = array();
  $list_date = array();
  $list_roomnumber = array();
  $list_roomnumber_daily = array();
  for($n=0; $n<$night; $n++){
    $date = date('Y-m-d', strtotime($checkin. ' +'.$n.' days'));
    array_push($list_date, $date);
    array_push($show_list_date, date('d M Y', strtotime($date)));
    $available_room = $pmslite->loadShowAssignRoom($date, $roomoid);
    array_push($list_roomnumber,  $available_room);
    foreach($available_room as $ar){
      array_push($list_roomnumber_daily,  $ar['roomnumberoid']);
    }
  }

  $order_room = $pmslite->reorderRoom($list_roomnumber_daily);

  if($bookingmarket != 1){
?>
  <input type="hidden" name="night" value="<?=$night?>">
  <hr style="border-top: 2px dashed #b7d3d1;"/>
  <h3><i class="fa fa-key"></i> Setting Room</h3>
  <div class="row">
    <div class="col-md-12">
      <table class="table">
        <thead>
          <tr class="text-center"><th>#</th><th>Guest Room*</th><th>Adult</th><th>Child</th><th>Extra Bed</th><th>Remark</th><th>Room Total</th><th>Date</th><th>Room</th><th>Daily Rate</th></tr>
        </thead>
        <tbody>
          <?php for($r=1; $r<=$roombooked; $r++){ ?>
          <tr>
            <td rowspan="<?=$night?>"><?=$r?></td>
            <td rowspan="<?=$night?>">
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-addon"><i class="fa fa-user"></i></div>
                  <input type="text" name="guest[]" class="form-control">
                </div>
              </div>
            </td>
            <td rowspan="<?=$night?>">
              <div class="form-group">
                <select class="form-control" name="adult[]">
                  <?php for($i = 1; $i <= $room['adult']; $i++){ ?>
                    <option value="<?=$i?>"><?=$i?></option>
                  <?php } ?>
                </select>
              </div>
            </td>
            <td rowspan="<?=$night?>">
                <div class="form-group">
                  <select class="form-control" name="child[]">
                    <?php for($i = 0; $i <= $room['child']; $i++){ ?>
                      <option value="<?=$i?>"><?=$i?></option>
                    <?php } ?>
                  </select>
                </div>
            </td>
            <td rowspan="<?=$night?>" align="center">
              <div class="form-group"><input type="checkbox" name="extrabed[]" value="y"></div>
            </td>
            <td rowspan="<?=$night?>">
              <div class="form-group"><input type="text" name="remark[]" class="form-control" value="Special Rate"></div>
            </td>
            <td rowspan="<?=$night?>">
              <div class="form-group"><input type="text" name="roomtotal[]" class="form-control" for-room="<?=$r?>"></div>
            </td>
          <?php
            foreach($show_list_date as $key => $date){
              if($key > 0){ echo '<tr>'; }
          ?>
            <td><?=$date?></td>
            <td>
              <select name="roomnumber-<?=$r?>[]" class="form-control">
                <?php
                  if($r == 1){
                    if(array_search($roomnumber, array_column($list_roomnumber[$key], 'roomnumberoid')) !== False){ $roomtoorder = $roomnumber; }else{ $roomtoorder = $order_room[$r-1]; }
                  }else{
                    $roomtoorder = $order_room[$r-1];
                  }

                  foreach($list_roomnumber[$key] as $key2 => $rn){
                    if($rn['roomnumberoid'] == $roomtoorder){ $selected = "selected" ; }else{ $selected = "";  }
                ?>
                <option value="<?=$rn['roomnumberoid']?>" <?=$selected?>><?=$rn['roomnumber']?> </option>
                <?php } ?>
              </select>
            </td>
            <td><input type="text" name="rate-<?=$r?>[]" class="form-control" rate-room="<?=$r?>" fnc="dailyrate" value="<?=$rplan['minrate']?>"></td>
          </tr>
          <?php } ?>
          <?php } ?>
        </tbody>
      </table>
      <small>* If guest room empty system will default get name from guest detail.</small>
    </div>
  </div>
  <div class="row">
    <div class="col-md-2">
      <div class="form-group">
        <label>Currency :</label>
        <select name="currency" class="form-control">
          <?php
          $stmt = $db->prepare("select * from currency where publishedoid = '1'");
          $stmt->execute();
          $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
          foreach($result as $currency){
            if($mr['currencyoid'] == $currency['currencyoid']){ $selected = "selected"; }else{ $selected = ""; }
          ?>
          <option value="<?=$currency['currencyoid']?>" <?=$selected?> currname="<?=$currency['currencycode']?>"><?=$currency['currencycode']?></option>
          <?php
          }
          ?>
        </select>
      </div>
    </div>
    <div class="col-md-10 text-right">
      <h3 style="font-size: 1.2em;">Total :</h3>
      <h2 style="font-size: 1.5em;margin-top: 5px;" fnc="grandtotal"></h2>
    </div>
  </div>
  |
  <button type="button" class="btn btn-sm btn-default" data-dismiss="modal" aria-label="Close">Close</button>
  <button type="button" name="next-action" value="1" class="btn btn-sm btn-primary">Temp Reserve</button>
  <button type="button" name="next-action" value="2" class="btn btn-sm btn-danger">Reserve</button>
  <?php if(strtotime($checkin) <= strtotime(date('Y-m-d'))){ ?>
  <button type="button" name="next-action" value="4" class="btn btn-sm btn-success">Check in</button>
  <?php } ?>
  <button type="button" name="next-action" value="7" class="btn btn-sm btn-warning">Hold</button>
<?php
  }else{
    $response = $pmslite->loadShowPrice($checkin, $checkout, $roomoid, $roomofferoid, '1', $roombooked, $_SESSION['tokenSession'.$pmslite->hotelcode], $bookingmarket, $agenthoteloid);

    if(strpos($response, '<failed>') !== false){
    ?>
      <div class="row">
        <div class="col-md-12 text-right">
          <h2 style="font-size: 1.2em;"><i class="fa fa-warning"></i> <?=$response?></h2>
        </div>
      </div>
      |
      <button type="button" class="btn btn-sm btn-default" data-dismiss="modal" aria-label="Close">Close</button>
    <?php
    }else{
      $show_list_date = array();
      $list_date = array();
      $list_roomnumber = array();
      $list_roomnumber_daily = array();
      for($n=0; $n<$night; $n++){
        $date = date('Y-m-d', strtotime($checkin. ' +'.$n.' days'));
        array_push($list_date, $date);
        array_push($show_list_date, date('d M Y', strtotime($date)));
        $available_room = $pmslite->loadShowAssignRoom($date, $roomoid);
        array_push($list_roomnumber,  $available_room);
        foreach($available_room as $ar){
          array_push($list_roomnumber_daily,  $ar['roomnumberoid']);
        }
      }

      $order_room = $pmslite->reorderRoom($list_roomnumber_daily);
    ?>
      <div class="row">
        <div class="col-md-12 text-right">
          <h3 style="font-size: 1.2em;">Total :</h3>
          <h2 style="font-size: 1.5em;margin-top: 5px;"><?=$response?></h2>
        </div>
      </div>
      <hr style="border-top: 2px dashed #b7d3d1;"/>
      <h3><i class="fa fa-key"></i> Setting Room</h3>
      <div class="row">
        <div class="col-md-12">
          <table class="table">
            <thead>
              <tr><th>#</th><th>Guest Room*</th><th>Adult</th><th>Child</th><th>Assign Room</th>
            </thead>
            <tbody>
              <?php for($r=1; $r<=$roombooked; $r++){ ?>
              <tr>
                <td><?=$r?></td>
                <td>
                  <div class="form-group">
                    <div class="input-group">
                      <div class="input-group-addon"><i class="fa fa-user"></i></div>
                      <input type="text" name="guest[]" class="form-control">
                    </div>
                  </div>
                </td>
                <td>
                  <div class="form-group">
                    <select class="form-control" name="adult[]">
                      <?php for($i = 1; $i <= $room['adult']; $i++){ ?>
                        <option value="<?=$i?>"><?=$i?></option>
                      <?php } ?>
                    </select>
                  </div>
                </td>
                <td>
                    <div class="form-group">
                      <select class="form-control" name="child[]">
                        <?php for($i = 0; $i <= $room['child']; $i++){ ?>
                          <option value="<?=$i?>"><?=$i?></option>
                        <?php } ?>
                      </select>
                    </div>
                </td>
                <td>
                  <?php foreach($show_list_date as $key => $date){ ?>
                  <div class="row">
                    <div class="col-xs-6"><?=$date?></div>
                    <div class="col-xs-6">
                      <select name="roomnumber-<?=$r?>[]" class="form-control">
                        <?php foreach($list_roomnumber[$key] as $key2 => $rn){

                          if($rn['roomnumberoid'] == $order_room[$r-1]){ $selected = "selected" ; }else{ $selected = "";  }
                        ?>
                        <option value="<?=$rn['roomnumberoid']?>" <?=$selected?>><?=$rn['roomnumber']?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                  <?php } ?>
                </td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
          <small>* If guest room empty system will default get name from guest detail.</small>
        </div>
      </div>
      |
      <button type="button" class="btn btn-sm btn-default" data-dismiss="modal" aria-label="Close">Close</button>
      <button type="button" name="next-action" value="1" class="btn btn-sm btn-primary">Temp Reserve</button>
      <button type="button" name="next-action" value="2" class="btn btn-sm btn-danger">Reserve</button>
      <?php if(strtotime($checkin) <= strtotime(date('Y-m-d'))){ ?>
      <button type="button" name="next-action" value="4" class="btn btn-sm btn-success">Check in</button>
      <?php } ?>
      <button type="button" name="next-action" value="7" class="btn btn-sm btn-warning">Hold</button>
<?php
    }
  }
}catch(Exception $e){
  echo $e->getMessage();
}
?>
