<?php
  /* SAVE BOOKING BASIC ----------------------------------------------------------------- */

  $stmt	= $db->query("select bt.*, r.roomoid, r.hoteloid from bookingtemp bt inner join roomoffer ro using (roomofferoid) inner join room r using (roomoid) where session_id ='".$tknSession."'");
  $row_count = $stmt->rowCount();
  if($row_count>0){
    $fetch_booking_temporer = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $r = 0;
    foreach($fetch_booking_temporer as $key => $temp){
      $r++;
      $guestroom = $_POST['guest'][$key]; if(empty($guestroom)){ $guestroom = $set_defaultname; }
      $adult = $_POST['adult'][$key];
      $child = $_POST['child'][$key];

      $roomnumber = $_POST["roomnumber-".$r];

      $stmt = $db->prepare("INSERT INTO `bookingroom`
    (`bookingoid`, `roomofferoid`, `channeloid`, `promotionoid`, `packageoid`, `checkin`, `checkout`, `night`, `adult`, `child`, `roomtotal`, `extrabedtotal`, `total`, `currencyoid`, `breakfast`, `extrabed`, `deposit`, `balance`, `hotel`, `room`, `promotion`, `cancellationpolicyoid`, `cancellationname`, `termcondition`, `cancellationday`, `cancellationtype`, `cancellationtypenight`, `roomtotalr`, `totalr`, `checkoutr`, `rate_termcondition`, `rate_cancellationpolicy`, `promocode`, `promocodecomm`, `pmsstatusoid`, `guestroom`) VALUES (:a , :b , :c , :d, :d2 , :e , :f, :g, :h, :i, :j, :k, :l, :m, :n, :o, :p, :q, :r, :s, :t, :aa, :ab, :ac, :ad, :ae, :af, :ba, :bb, :bc, :ca, :cb, :cc, :cd, :da, :ea)");
      $stmt->execute(array(':a' => $pmslite->bookingoid, ':b' => $temp['roomofferoid'], ':c' => $temp['channeloid'], ':d' => $temp['promotionoid'], ':d2' => $temp['packageoid'], ':e' => $temp['checkin'], ':f' => $temp['checkout'], ':g' => $temp['night'], ':h' => $adult, ':i' => $child, ':j' => $temp['roomtotal'], ':k' => $temp['extrabedtotal'], ':l' => $temp['total'], ':m' => $temp['currencyoid'], ':n' => $temp['breakfast'], ':o' => $temp['extrabed'], ':p' => $temp['deposit'], ':q' => $temp['balance'], ':r' => $temp['hotel'], ':s' => $temp['room'], ':t' => $temp['promotion'], ':aa' => $temp['cancellationpolicyoid'], ':ab' => $temp['cancellationname'], ':ac' => $temp['termcondition'], ':ad' => $temp['cancellationday'], ':ae' => $temp['cancellationtype'], ':af' => $temp['cancellationtypenight'], ':ba' => $temp['roomtotal'], ':bb' => $temp['total'], ':bc' => $temp['checkout'], ':ca' => $temp['rate_termcondition'], ':cb' => $temp['rate_cancellationpolicy'], ':cc' => $temp['promocode'], ':cd' => $temp['promocodecomm'], ':da' => $_POST['pmsstatus'], ':ea' => $guestroom));
      $bookingroomoid = $db->lastInsertId();
      
        // Prepare Data
        $stmt = $db->prepare("SELECT `roomoid` FROM `roomoffer` WHERE `roomofferoid` = :a");
        $stmt->execute(array(':a' => $temp['roomofferoid']));
        $r_ro = $stmt->fetch(PDO::FETCH_ASSOC);
        $vroomoid = $r_ro['roomoid'];
        
        $stmt = $db->prepare("SELECT COUNT(`roomnumberoid`) AS jml FROM `roomnumber` WHERE `roomoid` = :a AND `publishedoid` = 1");
        $stmt->execute(array(':a' => $vroomoid));
        $r_ro = $stmt->fetch(PDO::FETCH_ASSOC);
        $vroomnumber = $r_ro['jml'];
        
        $data6 = '{
            "token": "pfUxpxmKTJNTStNhPK2Nly4UKxbcVWwNRUT1SXnPfRiWqhvmINnJ4XnefNNBuTsx",
            "hcode": "'.$pmslite->hotelcode.'",
            "roomid": "'.$vroomoid.'",
            "data": [';
        $z = 0;

      $stmt	= $db->query("select btd.* from bookingtempdtl btd where bookingtempoid ='".$temp['bookingtempoid']."'");
      $fetch_booking_temporer_detail = $stmt->fetchAll(PDO::FETCH_ASSOC);
      foreach($fetch_booking_temporer_detail as $key2 => $tempdtl){
        $stmt = $db->prepare("insert into `bookingroomdtl` (`bookingroomoid`, `date`, `rate`, `discount`, `rateafter`, `extrabed`, `total`, `currencyoid`, `guestroom`, `roomnumberoid`) VALUES (:a , :b , :c , :d , :e , :f, :g, :h, :i, :j)");
        $stmt->execute(array(':a' => $bookingroomoid, ':b' => $tempdtl['date'], ':c' => $tempdtl['rate'], ':d' => $tempdtl['discount'], ':e' => $tempdtl['rateafter'], ':f' => $tempdtl['extrabed'], ':g' => $tempdtl['total'], ':h' => $tempdtl['currencyoid'], ':i' =>  $guestroom, ':j' => $roomnumber[$key2]));
      
        // Prepare Data
        $stmt = $db->prepare("SELECT COUNT(`bookingroomdtloid`) jml
                FROM `bookingroomdtl` brd
                    LEFT JOIN `bookingroom` br USING(`bookingroomoid`)
                    LEFT JOIN `booking` b USING(`bookingoid`)
                    LEFT JOIN `roomoffer` ro USING(`roomofferoid`)
                WHERE b.`bookingstatusoid` IN (4) AND br.`pmsstatusoid` IN (2,4,7) 
                	AND ro.`roomoid` = :a AND brd.`date` = :b");
        $stmt->execute(array(':a' => $vroomoid, ':b' => $tempdtl['date']));
        $r_ro = $stmt->fetch(PDO::FETCH_ASSOC);
        $vtxnumber = $r_ro['jml'];
        
        $vval = $vroomnumber - $vtxnumber;
        
        if($z != 0){ $data6 .=","; }else{ $z = 1; }
        $data6 .= '
            {
                "from": "'.$tempdtl['date'].'",
                "to": "'.$tempdtl['date'].'",
                "value": "'.$vval.'"
            },
        ';
        
      }
      
        // Prepare Data
        $data6 .= '
            ]
        }';
    
        if($_POST['pmsstatus'] == '2' || $_POST['pmsstatus'] == '4' || $_POST['pmsstatus'] == '7'){
            // The data to send to the API
            $postData = (array) json_decode($data6);
            
            // Setup cURL
            $ch = curl_init('https://thebuking.com/apiv1/save/allotment_inrange/');
            curl_setopt_array($ch, array(
                CURLOPT_POST => TRUE,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_SSL_VERIFYPEER => FALSE,
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json'
                ),
                CURLOPT_POSTFIELDS => json_encode($postData)
            ));
            
            // Send the request
            $response = curl_exec($ch);
            
            // Check for errors
            if($response === FALSE){
                die(curl_error($ch));
            }
            
            // Decode the response
            $responseData = json_decode($response, TRUE);
        }
    }

    $remove_session = "delete bookingtemp, bookingtempdtl from bookingtemp, bookingtempdtl where bookingtemp.bookingtempoid = bookingtempdtl.bookingtempoid and bookingtemp.session_id = '".$_SESSION['tokenSession']."'";
    $stmt	= $db->query($remove_session);
  }
?>
