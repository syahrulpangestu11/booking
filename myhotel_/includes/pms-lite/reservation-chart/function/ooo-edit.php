<?php
try{
  session_start();
	error_reporting(E_ALL ^ E_NOTICE);
	include ('../../../../conf/connection.php');
  include ('../../class-pms-lite.php');

  $oooid = $_POST['oooid'];

  $pmslite = new PMSLite($db);
  $data = $pmslite->getOOO($oooid);
  /*--------------------------------------*/
?>
  <form id="form-edit-ooo" method="post">
    <input type="hidden" name="oooid" value="<?=$oooid?>">
    <div class="row">
      <div class="col-md-12">
        <div class="row">
    			<div class="col-md-6"><h3>Room Type : <?=$data['room']?></h3></div>
          <div class="col-md-6"><h3>Room Number : <?=$data['roomnumber']?></h3></div>
    		</div>
        <div class="row">
          <div class="col-md-3 col-xs-6">
            <div class="form-group">
              <label>From</label>
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                  <input type="text" name="checkin" id="startdate-today" class="form-control" value="<?=date('d M Y', strtotime($data['startdate']))?>">
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-xs-6">
            <div class="form-group">
              <label>To</label>
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                  <input type="text" name="checkout" id="enddate-today" class="form-control" value="<?=date('d M Y', strtotime($data['enddate']))?>">
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-xs-12">
            <div class="form-group">
              <label>Reason</label>
              <textarea name="reason" class="form-control"><?=$data['reason']?></textarea>
            </div>
          </div>
        </div>
        <div class="show-detail-create-rsv">
        </div>
      </div>
    </div>
    <hr/>
    <div class="row">
      <div class="col-md-12 text-right">
        <button type="button" name="save" class="btn btn-sm btn-primary">Save</button>
        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </form>
<?php
}catch(Exception $e){
  echo $e->getMessage();
}
?>
