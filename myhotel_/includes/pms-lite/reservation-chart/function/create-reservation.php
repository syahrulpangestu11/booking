<?php
try{
  session_start();
	error_reporting(E_ALL ^ E_NOTICE);
	include ('../../../../conf/connection.php');
  include ('../../class-pms-lite.php');

  $hoteloid = $_POST['hotel'];
  $checkin = date('d M Y', strtotime($_POST['date']));
  $checkout = date('d M Y', strtotime($checkin. ' +1 days'));
  $roomoid = $_POST['room'];
  $roomnumberoid = $_POST['roomnumber'];

  $pmslite = new PMSLite($db);
  $pmslite->setPMSHotel($hoteloid);
  $pmslite->setRoom($roomoid);
  $room = $pmslite->roomType();
  $rateplan = $pmslite->RatePlan();
  /*--------------------------------------*/
  $_SESSION['tokenSession'.$pmslite->hotelcode] = $pmslite->hotelcode."_u".$_SESSION['_oid'];

?>
  <form id="form-create-reservation" method="post">
    <input type="hidden" name="hotel" value="<?=$hoteloid?>">
    <input type="hidden" name="room" value="<?=$roomoid?>">
    <input type="hidden" name="roomnumber" value="<?=$roomnumberoid?>">
    <div class="row">
      <div class="col-md-12">
        <div class="row"><div class="col-md-12"><h3>Room Type : <?=$room['roomname']?></h3></div></div>
        <div class="row">
          <div class="col-md-3 col-xs-6">
            <div class="form-group">
              <label>Check in</label>
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                  <input type="text" name="checkin" id="startdate-today" class="form-control" value="<?=$checkin?>">
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-xs-6">
            <div class="form-group">
              <label>Check out</label>
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                  <input type="text" name="checkout" id="enddate-today" class="form-control" value="<?=$checkout?>">
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4 col-xs-8">
            <div class="form-group">
              <label>Rate Plan</label><select name="roomofferoid" class="form-control"><?php foreach($rateplan as $rp){ ?><option value="<?=$rp['roomofferoid']?>"><?=$rp['rateplan']?></option><?php } ?></select>
            </div>
          </div>
          <div class="col-md-2 col-xs-4">
            <div class="form-group">
              <label>Rooms</label><select name="rooms" class="form-control"><?php for($i=1; $i<=5; $i++){ ?><option value="<?=$i?>"><?=$i?></option><?php } ?></select>
            </div>
          </div>
        </div>
        <div class="show-detail-create-rsv">
        </div>
      </div>
    </div>
    <hr style="border-top: 2px dashed #b7d3d1;"/>
    <div class="row">
      <div class="col-md-12">
        <div class="row">
          <div class="col-md-6">
            <div class="row">
              <div class="col-md-12 bg-success">
                <div class="row"><div class="col-md-12"><h3><i class="fa fa-user-circle-o"></i> Guest Detail</h3></div></div>
                <div class="row">
                  <div class="col-xs-2">
                    <div class="form-group">
                      <label>Title <span class="mandatory">*</span></label>
                      <select name="title" class="form-control">
                        <?php
                        $stmt = $db->prepare("select * from title");
                        $stmt->execute();
                        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
                        foreach($result as $title){
                        ?>
                        <option value="<?=$title['titleoid']?>"><?=$title['name']?></option>
                        <?php
                        }
                        ?>
                      </select><input type="hidden" name="cust">
                    </div>
                  </div>
                  <div class="col-xs-5">
                    <div class="form-group">
                      <label>First Name <span class="mandatory">*</span></label>
                      <input type="text" name="firstname" class="form-control" required>
                    </div>
                  </div>
                  <div class="col-xs-5">
                    <div class="form-group">
                      <label>Last Name <span class="mandatory">*</span></label>
                      <input type="text" name="lastname" class="form-control" required>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-6">
                    <div class="form-group">
                      <label>Email</label>
                      <input type="text" name="email" class="form-control" required>
                    </div>
                  </div>
                  <div class="col-xs-6">
                    <div class="form-group">
                      <label>Phone / Mobile</label>
                      <input type="text" name="phone" class="form-control" required>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-6">
                    <div class="form-group">
                      <label>City</label>
                      <input type="text" name="city" class="form-control" required>
                    </div>
                  </div>
                  <div class="col-xs-6">
                    <div class="form-group">
                      <label>Country</label>
                      <select name="country" class="form-control">
                        <?php
                        $stmt = $db->prepare("select * from country");
                        $stmt->execute();
                        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
                        foreach($result as $country){
                          if(strtolower($country['countryname']) == "indonesia"){ $selected = 'selected'; }else{ $selected = ""; }
                        ?>
                        <option value="<?=$country['countryoid']?>" <?=$selected?>><?=$country['countryname']?></option>
                        <?php
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12"><span class="mandatory">*</span> <i>required field</i></div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="row">
              <div class="col-md-12">
                <div class="row"><div class="col-md-12"><h3><i class="fa fa-comments-o"></i> Sales / Guest Request</h3></div></div>
                <div class="row"><div class="col-md-12"><textarea name="note"></textarea></div></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <hr />
    <div class="row">
      <div class="col-md-5 button-response">
      </div>
      <div class="col-md-2">
        <div class="form-group">
          <label>Booking Channel</label>
          <select name="bookingchannel" class="form-control">
            <?php
            $default_bookingchannel = 2;
            $stmt = $db->prepare("select * from bookingchannel where publishedoid = '1'");
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            foreach($result as $bc){
              if($bc['bookingchanneloid'] == $default_bookingchannel){ $selected = "selected"; }else{ $selected=""; }
            ?>
            <option value="<?=$bc['bookingchanneloid']?>" <?=$selected?>><?=$bc['bc_name']?></option>
            <?php
            }
            ?>
          </select>
        </div>
      </div>
      <div class="col-md-2">
        <div class="form-group">
          <label>Booking Market</label>
          <?php $default_bookingmarket = 2; ?>
          <input name="oldbookingmarket" type="hidden" value="<?=$default_bookingmarket?>">
          <select name="bookingmarket" class="form-control">
            <?php
            $stmt = $db->prepare("select * from bookingmarket where publishedoid = '1'");
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            foreach($result as $bm){
              if($bm['bookingmarketoid'] == $default_bookingmarket){ $selected = "selected"; }else{ $selected=""; }
            ?>
            <option value="<?=$bm['bookingmarketoid']?>" <?=$selected?>><?=$bm['bm_name']?></option>
            <?php
            }
            ?>
          </select>
        </div>
      </div>
      <div class="col-md-3 booking-market">
        <div class="form-group">
          <label>Select Agent:</label>
          <select name="agent" class="form-control">
            <option value="0">No Agent</option>
            <?php
            $stmt = $db->prepare("select a.agentoid, a.agentname from agent a where a.publishedoid = 1 order by a.agentname");
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            foreach($result as $agent){
            ?>
            <option value="<?=$agent['agentoid']?>"><?=$agent['agentname']?></option>
            <?php
            }
            ?>
          </select>
        </div>
      </div>
    </div>
  </form>
<?php
}catch(Exception $e){
  echo $e->getMessage();
}
?>
