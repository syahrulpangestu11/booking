<?php
class PMSLite{
	var $db;
  var $hoteloid;
	var $hotelcode;
	var $hotelname;
	var $hoteldata;
	var $xmltarif;
	var $roomoid;
  var $startdate;
  var $enddate;
  var $diffdate;
	var $bookingoid;
	var $session;

  public function __construct($db){
		$this->db = $db;
	}

	public function setSessionRole($session){
		$this->session = $session;
	}

  public function setPMSHotel($hoteloid){
		$stmt = $this->db->prepare("select * from hotel  where hoteloid = :a");
		$stmt->execute(array(':a' => $hoteloid));
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
    $this->hoteloid = $hoteloid;
		$this->hotelcode = $result['hotelcode'];
		$this->hotelname = $result['hotelname'];
		$this->hoteldata = $result;
  }

  public function setPeriodePMS($startdate, $enddate){
    $this->startdate = $startdate;
    $this->enddate = $enddate;
    $this->diffdate = $this->dateDiff($startdate, $enddate);
  }

	public function dateDiff($startdate, $enddate){
		$date1 = date_create($startdate);
		$date2 = date_create($enddate);
		$diff = date_diff($date1,$date2);

		return $diff->format("%a");
	}

  public function dataDateTable(){
    $dataDateTable = array();
    for($i = 0; $i <= $this->diffdate; $i++){
      $date = date('Y-m-d', strtotime($this->startdate. ' +'.$i.' days'));
      array_push($dataDateTable, array('day' => date('d', strtotime($date)), 'month' => date('M', strtotime($date)), 'date' => $date));
    }

    return $dataDateTable;
  }

  public function roomTypeTable(){
    $stmt = $this->db->prepare("select r.roomoid, r.name as roomname, r.publishedoid from room r where r.hoteloid = :a and r.publishedoid not in (3)");
		$stmt->execute(array(':a' => $this->hoteloid));
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $roomTypeTable = array();
    foreach($result as $row){
      array_push($roomTypeTable, array('id' => $row['roomoid'], 'name' => $row['roomname'], 'status' => $row['publishedoid']));
    }

    return $roomTypeTable;
  }

  public function roomNumber($roomoid){
    $stmt = $this->db->prepare("select rn.roomnumberoid, rn.roomnumber, rn.publishedoid from roomnumber rn where rn.roomoid = :a and rn.publishedoid not in (3)");
		$stmt->execute(array(':a' => $roomoid));
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $roomNumber = array();
    foreach($result as $row){
      array_push($roomNumber, array('id' => $row['roomnumberoid'], 'roomnumber' => $row['roomnumber'], 'status' => $row['publishedoid']));
    }

    return $roomNumber;
  }

	public function roomAllocation($roomoid){
    $stmt = $this->db->prepare("select count(rn.roomnumberoid) as jmlroom from roomnumber rn where rn.roomoid = :a and rn.publishedoid not in (3)");
		$stmt->execute(array(':a' => $roomoid));
		$result = $stmt->fetch(PDO::FETCH_ASSOC);

    return $result['jmlroom'];
  }

  public function dataBlockedTable(){
    $dataBlockedTable = array();
    for($i = 0; $i <= $this->diffdate; $i++){
      $date = date('Y-m-d', strtotime($this->startdate. ' +'.$i.' days'));
      $stmt = $this->db->prepare("select count(bookingroomdtloid) as unassigned from bookingroomdtl brd inner join bookingroom br using (bookingroomoid) inner join booking b using (bookingoid) where b.hoteloid = :a and brd.date = :b and b.bookingstatusoid = '4' and br.pmsstatusoid in ('0', '1') and brd.reconciled = '0'");
      $stmt->execute(array(':a' => $this->hoteloid, ':b' => $date));
      $result = $stmt->fetch(PDO::FETCH_ASSOC);
      array_push($dataBlockedTable, array('date'=> $date, 'unassigned' => $result['unassigned']));
    }

    return $dataBlockedTable;
  }

	public function dataRoomAssignedTable($roomtypeoid, $roomnumber){
    $dataRoomAssignedTable = array();
		$bookingroomoid = array();
		$room_ooo = array();
    for($i = 0; $i <= $this->diffdate; $i++){
      $date = date('Y-m-d', strtotime($this->startdate. ' +'.$i.' days'));
      $stmt = $this->db->prepare("select br.bookingroomoid, brd.bookingroomdtloid,  br.checkout, br.pmsstatusoid, br.guestroom as guestname, CONCAT(c.firstname, ' ', c.lastname) as guestbook from bookingroomdtl brd inner join bookingroom br using (bookingroomoid) inner join roomoffer ro using (roomofferoid) inner join room r using (roomoid) inner join booking b using (bookingoid) inner join customer c using (custoid) where b.hoteloid = :a and r.roomoid = :b and brd.roomnumberoid = :c and  brd.date = :d and b.bookingstatusoid = '4' and br.pmsstatusoid not in ('0', '1') "); //and brd.reconciled = '0'
      $stmt->execute(array(':a' => $this->hoteloid, ':b' => $roomtypeoid, ':c' => $roomnumber, ':d' => $date));
			$found = $stmt->rowCount();
			if($found == 0){
				array_push($bookingroomoid, 0);

				$stmt = $this->db->prepare("select oooid, reason from room_ooo where roomnumberoid = :a and (startdate <= :b and enddate >= :c) and status = :d limit 1");
	      $stmt->execute(array(':a' => $roomnumber, ':b' => $date, ':c' => $date, ':d' => '1'));
				$found_ooo = $stmt->rowCount();

				if($found_ooo > 0){

					$result_ooo = $stmt->fetch(PDO::FETCH_ASSOC);
					if(end($room_ooo) == $result_ooo['oooid']){
						$removed = array_pop($dataRoomAssignedTable);
						$colspan++;
						$colspan_statement = 'colspan="'.$colspan.'"';
					}else{
						$colspan = 1;
						$colspan_statement = '';
					}

					if(!empty($result_ooo['reason'])){
						$reason_ooo = $result_ooo['reason'];
					}else{
						$reason_ooo = "Out of Order";
					}

					array_push($dataRoomAssignedTable, '<td class="ooo" '.$colspan_statement.'><a data-toggle="modal" data-target="#viewOOO" data-oooid="'.$result_ooo['oooid'].'" data-room="'.$roomtypeoid.'" data-roomnumber="'.$roomnumber.'" data-hotel="'.$this->hoteloid.'"><div class="full-box"><i>'.$reason_ooo.'<i></div></a></td>');

					array_push($room_ooo, $result_ooo['oooid']);
				}else {
					array_push($room_ooo, 0);
					/* ini ntar yg dipakai biar g bisa booking back date */
					/*
					if(strtotime($date) >= strtotime(date('Y-m-d'))){
						array_push($dataRoomAssignedTable, '<td class="empty"><a data-toggle="modal" data-target="#createReservation" data-date="'.$date.'" data-room="'.$roomtypeoid.'" data-roomnumber="'.$roomnumber.'" data-hotel="'.$this->hoteloid.'"><div class="full-box">&nbsp;</div></a></td>');
					}else{
						array_push($dataRoomAssignedTable, '<td class="block"><div class="full-box">&nbsp;</div></td>');
					}
					*/
					/* sementara ini dulu, nanti di ganti script diatas */
					switch($this->session){
						case 1 : $targetmodal = '#createReservation'; break;
						case 3 : $targetmodal = '#createReservation'; break;
						case 4 : $targetmodal = '#createOOO'; break;
						default :  $targetmodal = ''; break;
					}
					array_push($dataRoomAssignedTable, '<td class="empty"><a data-toggle="modal" data-target="'.$targetmodal.'" data-date="'.$date.'" data-room="'.$roomtypeoid.'" data-roomnumber="'.$roomnumber.'" data-hotel="'.$this->hoteloid.'"><div class="full-box">&nbsp;</div></a></td>');
				}
			}else{
				$result = $stmt->fetch(PDO::FETCH_ASSOC);
				if(end($bookingroomoid) == $result['bookingroomoid']){
					$removed = array_pop($dataRoomAssignedTable);
					$colspan++;
					$colspan_statement = 'colspan="'.$colspan.'"';
				}else{
					$colspan = 1;
					$colspan_statement = '';
				}
				if(empty($result['guestname'])){ $guestname = $result['guestbook']; }else{ $guestname = $result['guestname']; }
				array_push($dataRoomAssignedTable, '<td class="'.$this->convertPMSStatus($result['pmsstatusoid']).'" '.$colspan_statement.'><a data-toggle="modal" data-target="#viewDetail" data-id="'.$result['bookingroomdtloid'].'" data-rn="'.$result['roomnumberoid'].'" data-hotel="'.$this->hoteloid.'"><div class="full-box">'.$guestname.'</div></a></td>');
				array_push($bookingroomoid, $result['bookingroomoid']);
				array_push($room_ooo, 0);
			}
    }

    return $dataRoomAssignedTable;
  }

	public function convertPMSStatus($statusoid){
		$class_pmsstatus = array('2' => 'reserved', '3' => 'cancelled', '4' => 'checkin', '5' => 'checkout', '6' => 'earlycheckout', '7' => 'hold');
		return $class_pmsstatus[$statusoid];
	}

	/*public function dataInhouseRoom($roomtypeoid){
		$dataInhouseRoomTable = array();
		$dataRevenueRoomTable = array();
		for($i = 0; $i <= $this->diffdate; $i++){
			$date = date('Y-m-d', strtotime($this->startdate. ' +'.$i.' days'));
			$stmt = $this->db->prepare("select br.bookingroomoid, brd.bookingroomdtloid,  br.checkout, br.pmsstatusoid, br.guestroom as guestname, CONCAT(c.firstname, ' ', c.lastname) as guestbook, brd.total, cr.currencycode from bookingroomdtl brd inner join currency cr using (currencyoid) inner join bookingroom br using (bookingroomoid) inner join roomoffer ro using (roomofferoid) inner join room r using (roomoid) inner join booking b using (bookingoid) inner join customer c using (custoid) where b.hoteloid = :a and r.roomoid = :b and brd.roomnumberoid = :c and  brd.date = :d and b.bookingstatusoid = '4' and br.pmsstatusoid not in ('0', '1') and brd.reconciled = '0'");
			$stmt->execute(array(':a' => $this->hoteloid, ':b' => $roomtypeoid, ':c' => $roomnumber, ':d' => $date));
			$found = $stmt->rowCount();
			if($found == 0){
					array_push($dataInhouseRoomTable, '<td></td>');
					array_push($dataRevenueRoomTable, '<td></td>');
			}else{
				$result = $stmt->fetch(PDO::FETCH_ASSOC);

				if(empty($result['guestname'])){ $guestname = $result['guestbook']; }else{ $guestname = $result['guestname']; }
				array_push($dataInhouseRoomTable, '<td>'.$guestname.'</td>');
				array_push($dataRevenueRoomTable, '<td>'.$result['currencycode'].' '.number_format($result['total'],2).'</td>');
			}
		}

		return array($dataInhouseRoomTable, $dataRevenueRoomTable);
	}
	*/
	public function dataInhouseRevenue($roomtypeoid){
		$dataInhouseRevenue = array();
		for($i = 0; $i <= $this->diffdate; $i++){
			$date = date('Y-m-d', strtotime($this->startdate. ' +'.$i.' days'));
			$stmt = $this->db->prepare("select sum(brd.total) as total, cr.currencycode from bookingroomdtl brd inner join currency cr using (currencyoid) inner join bookingroom br using (bookingroomoid) inner join roomoffer ro using (roomofferoid) inner join room r using (roomoid) inner join booking b using (bookingoid) inner join customer c using (custoid) where b.hoteloid = :a and r.roomoid = :b and  brd.date = :c and b.bookingstatusoid = '4' and br.pmsstatusoid not in ('0', '1') and brd.reconciled = '0' group by cr.currencyoid");
			$stmt->execute(array(':a' => $this->hoteloid, ':b' => $roomtypeoid, ':c' => $date));
			$found = $stmt->rowCount();
			if($found == 0){
					array_push($dataInhouseRevenue, '-');
			}else{
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				$data = '';
				foreach($result as $rs){ $data.=$rs['currencycode']." ".number_format($rs['total'],2).'<br>'; }
				array_push($dataInhouseRevenue, '<b>'.$data.'</b>');
			}
		}

		return $dataInhouseRevenue;
	}

	public function dataInhouseRevenueUpdated($roomtypeoid){
		$dataInhouseRevenue = array();
		for($i = 0; $i <= $this->diffdate; $i++){
			$date = date('Y-m-d', strtotime($this->startdate. ' +'.$i.' days'));
			$stmt = $this->db->prepare("select SUM(CASE WHEN brd.currencyoid = 1 THEN brd.total ELSE 0 END) AS total_idr, SUM(CASE WHEN brd.currencyoid = 2 THEN brd.total ELSE 0 END) AS total_usd from bookingroomdtl brd inner join currency cr using (currencyoid) inner join bookingroom br using (bookingroomoid) inner join roomoffer ro using (roomofferoid) inner join room r using (roomoid) inner join booking b using (bookingoid) inner join customer c using (custoid) where b.hoteloid = :a and r.roomoid = :b and  brd.date = :c and b.bookingstatusoid = '4' and br.pmsstatusoid not in ('0', '1') and brd.reconciled = '0' group by cr.currencyoid");
			$stmt->execute(array(':a' => $this->hoteloid, ':b' => $roomtypeoid, ':c' => $date));
			$found = $stmt->rowCount();
			if($found == 0){
				$result = array('total_idr' => 0, 'total_usd' => 0);
			}else{
				$result = $stmt->fetch(PDO::FETCH_ASSOC);
			}
			array_push($dataInhouseRevenue, $result);
		}
		return $dataInhouseRevenue;
	}

	public function dataBookedRoom($roomtypeoid, $type){
		switch($type){
			case "booked" : $pmsstatus = "not in ('0', '1', '3', '7', '8')"; break;
			case "inhouse" : $pmsstatus = "in ('4', '5', '6')"; break;
		}
		$dataBookedRoom = array();
		for($i = 0; $i <= $this->diffdate; $i++){
			$date = date('Y-m-d', strtotime($this->startdate. ' +'.$i.' days'));
			$stmt = $this->db->prepare("select count(brd.bookingroomdtloid) as jmlbooked from bookingroomdtl brd inner join bookingroom br using (bookingroomoid) inner join roomoffer ro using (roomofferoid) inner join room r using (roomoid) inner join booking b using (bookingoid) where b.hoteloid = :a and r.roomoid = :b and  brd.date = :c and b.bookingstatusoid = '4' and br.pmsstatusoid ".$pmsstatus." and brd.reconciled = '0' and brd.pmsreconciled = '0'");
			$stmt->execute(array(':a' => $this->hoteloid, ':b' => $roomtypeoid, ':c' => $date));
			$result = $stmt->fetch(PDO::FETCH_ASSOC);
			array_push($dataBookedRoom, $result['jmlbooked']);
		}
		return $dataBookedRoom;
	}

	public function dataInhouseHotelRevenue(){
		$dataInhouseRevenue = array();
		for($i = 0; $i <= $this->diffdate; $i++){
			$date = date('Y-m-d', strtotime($this->startdate. ' +'.$i.' days'));
			$stmt = $this->db->prepare("select sum(brd.total) as total, cr.currencycode from bookingroomdtl brd inner join currency cr using (currencyoid) inner join bookingroom br using (bookingroomoid) inner join roomoffer ro using (roomofferoid) inner join room r using (roomoid) inner join booking b using (bookingoid) inner join customer c using (custoid) where b.hoteloid = :a and brd.date = :b and b.bookingstatusoid = '4' and br.pmsstatusoid not in ('0', '1') and brd.reconciled = '0' group by cr.currencyoid");
			$stmt->execute(array(':a' => $this->hoteloid, ':b' => $date));
			$found = $stmt->rowCount();
			if($found == 0){
					array_push($dataInhouseRevenue, '<td></td>');
			}else{
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				$data = '';
				foreach($result as $rs){ $data.=$rs['currencycode']." ".number_format($rs['total'],2).'<br>'; }
				array_push($dataInhouseRevenue, '<td><b>'.$data.'</b></td>');
			}
		}

		return $dataInhouseRevenue;
	}

	public function dataInhouseChainRevenue($userid){
		$dataInhouseChainRevenue = array();
		for($i = 0; $i <= $this->diffdate; $i++){
			$date = date('Y-m-d', strtotime($this->startdate. ' +'.$i.' days'));
			$stmt = $this->db->prepare("select sum(brd.total) as total, cr.currencycode from bookingroomdtl brd inner join currency cr using (currencyoid) inner join bookingroom br using (bookingroomoid) inner join roomoffer ro using (roomofferoid) inner join room r using (roomoid) inner join hotel h using (hoteloid) inner join booking b using (bookingoid) inner join customer c using (custoid) where h.chainoid in (select oid from userassign where type = 'chainoid' and useroid = :a) and brd.date = :b and b.bookingstatusoid = '4' and br.pmsstatusoid not in ('0', '1') and brd.reconciled = '0' group by cr.currencyoid");
			$stmt->execute(array(':a' => $userid, ':b' => $date));
			$found = $stmt->rowCount();
			if($found == 0){
					array_push($dataInhouseChainRevenue, '<td></td>');
			}else{
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
				$data = '';
				foreach($result as $rs){ $data.=$rs['currencycode']." ".number_format($rs['total'],2).'<br>'; }
				array_push($dataInhouseChainRevenue, '<td><b>'.$data.'</b></td>');
			}
		}

		return $dataInhouseChainRevenue;
	}

	/*------------------------------------------------------------------------*/

	public function unassignedRoom($date){

		$stmt = $this->db->prepare("select b.bookingnumber, br.bookingroomoid, cr.currencycode, br.total, ro.roomofferoid, br.checkin, br.checkout, br.night, br.adult, br.child, r.roomoid, r.name as roomname, CONCAT(c.firstname, ' ', c.lastname) as guestname, c.email, c.phone from bookingroomdtl brd inner join currency cr using (currencyoid)  inner join bookingroom br using (bookingroomoid) inner join roomoffer ro using (roomofferoid) inner join room r using (roomoid) inner join booking b using (bookingoid) inner join customer c using (custoid) where b.hoteloid = :a and brd.date = :b and b.bookingstatusoid = '4' and br.pmsstatusoid in ('0', '1') and brd.reconciled = '0'");
		$stmt->execute(array(':a' => $this->hoteloid, ':b' => $date));
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);

		return $result;
	}

	public function listRoomNumber($roomoid){

		$stmt = $this->db->prepare("select rn.roomnumberoid, rn.roomnumber from roomnumber rn inner join room r using (roomoid) where r.roomoid = :a and rn.publishedoid in (1)");
		$stmt->execute(array(':a' => $roomoid));
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);

		return $result;
	}

	/*------------------------------------------------------------------------*/

	public function setRoom($roomoid){
		$this->roomoid = $roomoid;
	}

	public function roomType(){
		$stmt = $this->db->prepare("select r.roomoid, r.name as roomname, r.adult, r.child from room r where r.roomoid = :a and r.publishedoid in (1)");
		$stmt->execute(array(':a' => $this->roomoid));
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	public function roomTypeNumber($roomnumberoid){
		$stmt = $this->db->prepare("select roomnumberoid, roomnumber from roomnumber where roomnumberoid = :a and publishedoid in (1)");
		$stmt->execute(array(':a' => $roomnumberoid));
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	public function ratePlan(){
		$stmt = $this->db->prepare("select ro.roomofferoid, ro.name as rateplan from roomoffer ro where ro.roomoid = :a and ro.publishedoid in (1)");
		$stmt->execute(array(':a' => $this->roomoid));
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	public function hotelRoomType($hoteloid){
		$stmt = $this->db->prepare("select r.roomoid, r.name as roomname, r.adult, r.child from room r where r.hoteloid = :a and r.publishedoid in (1)");
		$stmt->execute(array(':a' => $hoteloid));
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	/*------------------------------------------------------------------------*/

	public function getBookingRoom($bookingroomdtloid){
		$stmt = $this->db->prepare("select b.bookingoid, DATEDIFF(br.checkout, br.checkin) AS nights, b.bookingnumber, b.note, br.bookingroomoid, brd.bookingroomdtloid, br.checkin, br.checkout, br.night, br.room, br.adult, br.child, br.total, br.deposit, br.balance, cr.currencycode as currency, br.pmsstatusoid, br.guestroom as guestname, CONCAT(firstname, ' ', lastname) as ownername, c.email, c.phone, rn.roomnumber, ro.name as rateplan, r.name as roomname from bookingroom br inner join currency cr using (currencyoid)  inner join bookingroomdtl brd using (bookingroomoid) inner join roomoffer ro using (roomofferoid) inner join room r using (roomoid) inner join roomnumber rn using (roomnumberoid) inner join booking b using (bookingoid) inner join customer c using (custoid) where brd.bookingroomdtloid = :a");
		$stmt->execute(array(':a' => $bookingroomdtloid));
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	public function dataBookingRoom($bookingroomoid){
		$stmt = $this->db->prepare("select b.bookingoid, b.bookingnumber, b.bookingchanneloid, b.bookingmarketoid, b.agentoid, b.currencyoid,  br.bookingroomoid, br.checkin, br.checkout, br.night, r.name as room, br.adult, br.child, br.total, br.deposit, br.balance, cr.currencycode as currency, br.pmsstatusoid, br.guestroom, CONCAT(c.firstname, ' ', c.lastname) as customername, c.email, c.phone, ro.name as rateplan, r.roomoid, br.roomofferoid from bookingroom br inner join currency cr using (currencyoid) inner join roomoffer ro using (roomofferoid) inner join room r using (roomoid) inner join booking b using (bookingoid) inner join customer c using (custoid) where br.bookingroomoid = :a");
		$stmt->execute(array(':a' => $bookingroomoid));
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	public function dataBookingRoomDtl($bookingroomoid){
		$stmt = $this->db->prepare("select brd.bookingroomdtloid, brd.date, brd.roomnumberoid, rn.roomnumber from bookingroomdtl brd inner join currency cr using (currencyoid) inner join roomnumber rn using (roomnumberoid) where brd.bookingroomoid = :a  ");//and brd.reconciled = '0' and brd.pmsreconciled = '0'
		$stmt->execute(array(':a' => $bookingroomoid));
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	public function dataBookingRoomDtlNonAssigned($bookingroomoid){
		$stmt = $this->db->prepare("select brd.bookingroomdtloid, brd.date from bookingroomdtl brd inner join currency cr using (currencyoid) where brd.bookingroomoid = :a  ");//and brd.reconciled = '0' and brd.pmsreconciled = '0'
		$stmt->execute(array(':a' => $bookingroomoid));
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	public function getBookingCharge($bookingchargesoid){
		$stmt = $this->db->prepare("select bc.*, tp.pos, cr.currencycode from bookingcharges bc inner join typepos tp using (typeposoid) inner join currency cr using (currencyoid) where bookingchargesoid = :a");
		$stmt->execute(array(':a' => $bookingchargesoid));
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		return $result;
	}


	/*------------------------------------------------------------------------*/

	public function assignRoom($bookingroomoid, $roomnumberoid){
		$stmt = $this->db->prepare("update bookingroomdtl set roomnumberoid = :a where bookingroomoid = :id");
		$stmt->execute(array(':a' => $roomnumberoid, ':id' => $bookingroomoid));
	}

	public function changePMSStatus($bookingroomoid, $pmsstatusoid){
		$stmt = $this->db->prepare("update bookingroom set pmsstatusoid = :a where bookingroomoid = :id");
		$stmt->execute(array(':a' => $pmsstatusoid, ':id' => $bookingroomoid));
	}

	public function freeRoomNumber($checkin, $checkout, $roomoid){
		$stmt = $this->db->prepare("select rn.roomnumberoid, rn.roomnumber from roomnumber rn where rn.roomnumberoid not in (select distinct(roomnumberoid) as usedroom from bookingroom br inner join roomoffer ro using (roomofferoid) inner join room r using (roomoid) where ro.roomofferoid = :c and br.pmsstatusoid not in ('0', '1') and
( (br.checkin <= :a and br.checkout >= :b) or (br.checkin <= :a and (br.checkout >= :a and br.checkout <=: b)) or ((br.checkin >= :a and br.checkin <= :b) and br.checkout >= :b) ))");
		$stmt->execute(array(':a' => $checkin, ':b' => $checkout, ':c' => $roomoid));
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	public function getfreeRoomNumber($checkin, $checkout, $roomofferoid){
		$stmt = $this->db->query("select rn.roomnumberoid, rn.roomnumber from roomnumber rn inner join room r using (roomoid) inner join roomoffer ro using (roomoid) where rn.roomnumberoid not in (select distinct(roomnumberoid) as usedroom from bookingroom br inner join roomoffer ro using (roomofferoid) inner join room r using (roomoid) where ro.roomofferoid = '".$roomofferoid."' and br.pmsstatusoid not in ('0', '1') and
( (br.checkin <= '".$checkin."' and br.checkout >= '".$checkout."') or (br.checkin <= '".$checkin."' and (br.checkout >= '".$checkin."' and br.checkout <= '".$checkout."')) or ((br.checkin >= '".$checkin."' and br.checkin <= '".$checkout."') and br.checkout >= '".$checkout."') )) and ro.roomofferoid = '".$roomofferoid."' limit 1");
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		return $result['roomnumberoid'];
	}

	/*------------------------------------------------------------------------*/
	public function loadXML(){
		$this->xmltarif =  simplexml_load_file("../../../../data-xml/".$this->hotelcode.".xml");
	}

	public function loadPrice($arrival, $departure, $roomoid, $roomofferoid, $channel, $roombooked, $tknSession){

		$remove_session = "delete bookingtemp, bookingtempdtl from bookingtemp, bookingtempdtl where bookingtemp.bookingtempoid = bookingtempdtl.bookingtempoid and bookingtemp.session_id = '".$tknSession."'";
		$stmt	= $this->db->query($remove_session);

		/*-------------------------------------------------------------------------*/

		$night = $this->dateDiff($arrival, $departure);
		$xml_hotel = $this->hoteloid; $xml_room = $roomoid; $xml_roomoffer = $roomofferoid; $xml_channel = $channel;
		$xml = $this->xmltarif;
		$list_allotment = array(); $promoNight = array();

		for($i=0; $i<$night; $i++){
			$allotment = 0; $extrabed = ''; $rate = 0; $currency = 1; $closeout = 'n';

			$xml_date = date('Y-m-d', strtotime($arrival. ' +'.$i.' days'));
			$query_tag = '//hotel[@id="'.$xml_hotel.'"]/masterroom[@id="'.$xml_room.'"]/rate[@date="'.$xml_date.'"]/allotment/channel[@type="'.$xml_channel.'"]/text()';
			foreach($xml->xpath($query_tag) as $tagallotment){
				$allotment = (int)$tagallotment[0];
				array_push($list_allotment, $allotment);
			}

			$query_tag_closeout = '//hotel[@id="'.$xml_hotel.'"]/masterroom[@id="'.$xml_room.'"]/rate[@date="'.$xml_date.'"]/closeout/text()';
			foreach($xml->xpath($query_tag_closeout) as $tagcloseout){ $closeout = $tagcloseout;	}

			$query_tag = '//hotel[@id="'.$xml_hotel.'"]/masterroom[@id="'.$xml_room.'"]/rate[@date="'.$xml_date.'"]/rateplan[@id="'.$xml_roomoffer.'"]/channel[@type="'.$xml_channel.'"]';
			foreach($xml->xpath($query_tag) as $tagrate){
				$extrabed = (float)$tagrate->extrabed;
				$rate = $tagrate->double;
				$currency = $tagrate->currency;
				if($extrabed == 0 and $extrabed_available == true){ $extrabed_available = false; }
				list($blackout, $cta, $ctd) = array($tagrate->blackout , $tagrate->cta , $tagrate->ctd);
			}

			$blakout_check = 0; $cta_check = 0; $ctd_check = 0;
			if((isset($blackout) and $blackout == "y") or $closeout == 'y'){ $blakout_check = 1; }
			if($n == $night-1){
				$departure	= date("Y-m-d",strtotime($arrival." +".$night." day"));
				$query_tag_ctd	= '//hotel[@id="'.$xml_hotel.'"]/masterroom[@id="'.$xml_room.'"]/rate[@date="'.$departure.'"]/rateplan[@id="'.$xml_roomoffer.'"]/channel[@type="'.$xml_channel.'"]';
				foreach($xml->xpath($query_tag_ctd) as $tag_ctd){ if($tag_ctd->ctd == "y"){ $night++; } }
			}

			if($extrabed > 0){ $extrabedavailable = "Available"; }else{ $extrabedavailable = "Not available"; }

			$s_currency = "SELECT `currencycode`, `currencytitle` FROM `currency` WHERE `currencyoid`='".$currency."'";
			$stmt = $this->db->query($s_currency);
			$a_currency = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($a_currency as $r_currency){
				$currencyname = $r_currency['currencycode'];
			}

			if($allotment > 0 and $blakout_check == 0){

				$disc_note = "";
				$totalpernight = $rate;
				/*------------------------------------------*/
				$roomcharge = $totalpernight;
				$total_roomcharge += $totalpernight;
				/*------------------------------------------*/
				if($with_extrabed == "y"){
					$totalpernight = $totalpernight + $extrabed;
				}else{
					$extrabed = 0;
				}
				$total_extrabed += $extrabed;
				/*------------------------------------------*/
				$totalperroom += $totalpernight;

				$promoNight[$n]['xml_date'] = $xml_date;
				$promoNight[$n]['allotment'] = $allotment;
				$promoNight[$n]['extrabedavailable'] = $extrabedavailable;
				$promoNight[$n]['rate'] = $rate;
				$promoNight[$n]['rateafter'] = $roomcharge;
				$promoNight[$n]['extrabed'] = $extrabed;
				$promoNight[$n]['currencyoid'] = $currency;
				$promoNight[$n]['currencyname'] = $currencyname;
				$promoNight[$n]['breakdate_name'] = $breakdate_name;
				$promoNight[$n]['totalpernight'] = $totalpernight;
				$promoNight[$n]['disc_note'] = $disc_note;

				$j++;
			}else{
				return die("<noroom> there is no room available");
			}
		} /* for night */

		$show = "No";
		if(count($promoNight) == $night){
			if(min($list_allotment) >= $roombooked){
				$show = "Yes";
			}else{
				return die("<notenoughroom> there is not enough room for $roombooked");
			}
		}

		$stmt = $this->db->prepare("select currencycode from currency ro where currencyoid = :a and ro.publishedoid in (1)");
		$stmt->execute(array(':a' => $currency));
		$result = $stmt->fetch(PDO::FETCH_ASSOC);

		if($show == "Yes" and $totalperroom > 0){
			$submittime = date('Y-m-d H:i:s');
			$channeloid = $xml_channel;
			$promotionoid = 0; $flag = 0; $breakfast = "n"; $with_extrabed = "n";
			$totaldeposit = 0; $totalbalance = $totalperroom; $hotelname = $this->hotelname;
			$this->setRoom($roomoid); $roomtype = $this->roomType(); $roomname = $roomtype['roomname']; $promoname = "Flexible Rate";

			for($i=1; $i<=$roombooked; $i++){
				$stmt = $this->db->prepare("INSERT INTO `bookingtemp` (`submittime`, `session_id`, `roomofferoid`, `channeloid`, `promotionoid`, `checkin`, `checkout`, `night`, `adult`, `child`, `flag`, `breakfast`, `extrabed`, `roomtotal`, `extrabedtotal`, `total`, `deposit`, `balance`, `currencyoid`, `hotel`, `room`, `promotion`, `promocode`, `pc_commission`, `cancellationpolicyoid`, `cancellationname`, `termcondition`, `cancellationday`, `cancellationtype`, `cancellationtypenight`, `rate_termcondition`, `rate_cancellationpolicy`, `promocodecomm`)  VALUES (:a , :b , :c , :d , :e , :f, :g, :h, :i, :j, :k, :l, :m, :n1, :n2, :o, :p, :q, :r, :s, :t, :u, :v, :w, :aa, :ab, :ac, :ad, :ae, :af, :ba, :bb, :bc)");
				$stmt->execute(array(':a' => $submittime, ':b' => $tknSession, ':c' => $roomofferoid, ':d' => $channeloid, ':e' => $promotionoid, ':f' => $arrival, ':g' => $departure, ':h' => $night, ':i' => 0, ':j' => 0, ':k' => $flag, ':l' => $breakfast, ':m' => $with_extrabed, ':n1' => $total_roomcharge, ':n2' => $total_extrabed, ':o' => $totalperroom, ':p' => $totaldeposit, ':q' => $totalbalance, ':r' => $currency, ':s' => $hotelname, ':t' => $roomname, ':u' => $promoname, ':v' => $promocode, ':w' => $applycommission, ':aa' => $cancellationpolicyoid, ':ab' => $cancellationname, ':ac' => $termcondition, ':ad' => $cancellationday, ':ae' => $cancellationtype, ':af' => $cancellationtypenight, ':ba' => $rate_termcondition, ':bb' => $rate_cancellationpolicy, ':bc' => $pc_commission_total));
				$tempoid = $this->db->lastInsertId();

				$listTotal = array();
				foreach($promoNight as $j => $value2){
					if($with_extrabed == "y"){ $value2['extrabed'] = 0; }

					$stmt = $this->db->prepare("INSERT INTO `bookingtempdtl` (`bookingtempoid`, `date`, `rate`, `discount`, `rateafter`, `extrabed`, `total`, `currencyoid`)  VALUES (:a , :b , :c , :d , :e , :f, :g, :h)");
					$stmt->execute(array(':a' => $tempoid, ':b' => $value2['xml_date'], ':c' => $value2['rate'], ':d' => $value2['disc_note'], ':e' => $value2['rateafter'], ':f' => $value2['extrabed'], ':g' => $value2['totalpernight'], ':h' => $value2['currencyoid']));
					array_push($listTotal, $value2['totalpernight']);
				}
			}

			return $result['currencycode']." ".number_format($roombooked*$totalperroom);
		}


	}

	public function loadShowAvailableRoom($arrival, $night, $roomoid){
		$availableroom = array();
		for($i=0; $i<$night; $i++){
			$date = date('Y-m-d', strtotime($arrival. ' +'.$i.' days'));
			$stmt = $this->db->query("select count(roomnumberoid) as availableroom from roomnumber where roomoid = '".$roomoid."' and roomnumberoid not in (select distinct(brd.roomnumberoid) from bookingroomdtl brd inner join bookingroom br using (bookingroomoid) inner join roomoffer using (roomofferoid) inner join room r using (roomoid) inner join booking b using (bookingoid) where brd.date = '".$date."' and brd.reconciled = '0' and brd.pmsreconciled = '0' and b.bookingstatusoid = '4' and r.roomoid = '".$roomoid."' and br.pmsstatusoid not in (0, 1, 3))");
			$result = $stmt->fetch(PDO::FETCH_ASSOC);

			array_push($availableroom, $result['availableroom']);
		}
		return min($availableroom);
	}

	public function loadShowAssignRoom($date, $roomoid){
		$stmt = $this->db->query("select roomnumberoid, roomnumber from roomnumber where roomoid = '".$roomoid."' and roomnumberoid not in (select distinct(brd.roomnumberoid) from bookingroomdtl brd inner join bookingroom br using (bookingroomoid) inner join roomoffer using (roomofferoid) inner join room r using (roomoid) inner join booking b using (bookingoid) where brd.date = '".$date."'  and brd.pmsreconciled = '0' and b.bookingstatusoid = '4' and r.roomoid = '".$roomoid."' and br.pmsstatusoid not in (0, 1, 3)) and roomnumber.publishedoid = '1'");//and brd.reconciled = '0'
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);

		return $result;
	}

	public function loadShowAssignRoomEdit($date, $roomoid, $bookingroomoid){
		$stmt = $this->db->query("select roomnumberoid, roomnumber from roomnumber where roomoid = '".$roomoid."' and roomnumberoid not in (select distinct(brd.roomnumberoid) from bookingroomdtl brd inner join bookingroom br using (bookingroomoid) inner join roomoffer using (roomofferoid) inner join room r using (roomoid) inner join booking b using (bookingoid) where brd.date = '".$date."'  and brd.pmsreconciled = '0' and b.bookingstatusoid = '4' and r.roomoid = '".$roomoid."' and brd.bookingroomoid != '".$bookingroomoid."')");//and brd.reconciled = '0'
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);

		return $result;
	}
	/*---------------------------------------------------------------*/

	public function setBooking($bookingnumber){
		$stmt = $this->db->prepare("select bookingoid from booking  where bookingnumber = :a");
		$stmt->execute(array(':a' => $bookingnumber));
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		$this->bookingoid = $result['bookingoid'];
	}

	public function setBookingbyID($bookingoid){
		$stmt = $this->db->prepare("select bookingoid from booking  where bookingoid = :a");
		$stmt->execute(array(':a' => $bookingoid));
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		$this->bookingoid = $result['bookingoid'];
	}


	public function getBookingData(){
		$stmt = $this->db->prepare("select b.*, c.*, DATE_FORMAT(bookingtime, '%W, %d %M %Y at %H:%i:%s') as bookdate, CONCAT(firstname, ' ', lastname) as guestname, ctr.countryname, h.hotelname, bp.paymentoid, bp.cardoid, cr.currencycode, bs.note as bookingstatus from booking b inner join bookingstatus bs using (bookingstatusoid) inner join currency cr using (currencyoid) inner join hotel h using (hoteloid) inner join customer c using (custoid) inner join country ctr using (countryoid) left join bookingpayment bp using (bookingoid) where b.bookingoid = :a");
		$stmt->execute(array(':a' => $this->bookingoid));
		$result = $stmt->fetch(PDO::FETCH_ASSOC);

		return $result;

	}

	public function removeRoom($bookingroomoid){
		$stmt = $this->db->prepare("DELETE FROM bookingroom WHERE bookingroomoid = :a");
		$stmt->execute(array(':a' => $bookingroomoid));

		$stmt = $this->db->prepare("DELETE FROM bookingroomdtl WHERE bookingroomoid = :a");
		$stmt->execute(array(':a' => $bookingroomoid));
	}

	public function removeCharge($bookingchargesoid){
		$stmt = $this->db->prepare("DELETE FROM bookingcharges WHERE bookingchargesoid = :a");
		$stmt->execute(array(':a' => $bookingchargesoid));
	}

	public function removePayment($paymentoid){
		$stmt = $this->db->prepare("DELETE FROM bookingpaymentdtl WHERE bookingpaymentdtloid = :a");
		$stmt->execute(array(':a' => $paymentoid));
	}

	/* SAVE ---------------------------------------------------------------*/

	function randomPin($len){
		$sets='0123456789'; $setLen=strlen($sets)-1; $s='';
		for($p=1;$p<=$len;$p++){ $s.=$sets{mt_rand(0,$setLen)}; }
		return $s;
	}

	function generateookingNumber(){
		$bn_firstformat = date('Ymd');
		$same = true;

		$stmt = $this->db->query("select count(bookingoid) as numberofbooking from booking bm where date(bookingtime) = '".$today."'");
		$row_getID = $stmt->fetchAll(PDO::FETCH_ASSOC);
		foreach($row_getID as $row){ $lastelement = $row['numberofbooking']; }

		do{
			$lastelement++;
			$numbering = sprintf("%05d", $lastelement);
			$bookingnumber = $bn_firstformat.$numbering;

			$stmt	= $this->db->query("select bookingoid from booking bm where bm.bookingnumber = '".$bookingnumber."'");
			$row_count = $stmt->rowCount();
			if($row_count == 0){ $same = false; }else{ $same = true; }
		}while($same == true);

		return $bookingnumber;
	}

	function generateBookingNumberLast($pastdate){
		$pastdate = date('Y-m-d', strtotime($pastdate));
		$bn_firstformat = date('Ymd', strtotime($pastdate));;
		$same = true;

		$stmt = $this->db->query("select count(bookingoid) as numberofbooking from booking bm where date(bookingtime) = '".$pastdate."'");
		$row_getID = $stmt->fetchAll(PDO::FETCH_ASSOC);
		foreach($row_getID as $row){ $lastelement = $row['numberofbooking']; }

		do{
			$lastelement++;
			$numbering = sprintf("%05d", $lastelement);
			$bookingnumber = $bn_firstformat.$numbering;

			$stmt	= $this->db->query("select bookingoid from booking bm where bm.bookingnumber = '".$bookingnumber."'");
			$row_count = $stmt->rowCount();
			if($row_count == 0){ $same = false; }else{ $same = true; }
		}while($same == true);

		return $bookingnumber;
	}

	var $customeroid;

	public function saveGuest($title, $firstname, $lastname, $email, $phone, $city, $country){
		$stmt = $this->db->prepare("INSERT INTO `customer` (`title`, `firstname`, `lastname`, `email`, `city`, `countryoid`, `phone`) VALUES (:a , :b , :c , :d , :e , :f, :g)");
		$stmt->execute(array(':a' => $title, ':b' => $firstname, ':c' => $lastname, ':d' => $email, ':e' => $city , ':f' => $country , ':g' => $phone));
		$this->customeroid = $this->db->lastInsertId();
	}

	public function saveBookingBasic($hoteloid, $bookingnumber, $pin, $customeroid, $bookingtime, $timelimit, $bookingstatus, $bookingchannel, $bookingmarket, $agent, $pmsstatus, $note){
		$stmt = $this->db->prepare("INSERT INTO `booking` (`hoteloid`, `bookingnumber`, `pin`, `custoid`, `bookingtime`, `bookingtimelimit`,`bookingstatusoid`, `bookingchanneloid`, `bookingmarketoid`, `agentoid`, `pmsstatus`, `note`) VALUES (:a, :b, :c, :d, :e, :f, :g, :h, :i, :j, :k, :l)");
		$stmt->execute(array(':a' => $hoteloid, ':b' => $bookingnumber, ':c' => $pin, ':d' => $customeroid, ':e' => $bookingtime, ':f' => $timelimit, ':g' => $bookingstatus, ':h' =>$bookingchannel, ':i' => $bookingmarket, ':j' => $agent,  ':k' => $pmsstatus,  ':l' => $note));
		$this->bookingoid = $this->db->lastInsertId();
	}

	public function RecalculateBooking($bookingoid){
		// bookingroom
		$stmt = $this->db->prepare("SELECT sum(total) AS roomtotal, sum(deposit) AS roomdeposit, sum(balance) AS roombalance from bookingroom where bookingoid = :a");
		$stmt->execute(array(':a' => $bookingoid));
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		$roomtotal = $result['roomtotal'];
		$roomdeposit = $result['roomdeposit'];
		$roombalance = $result['roombalance'];

		// extra
		$stmt = $this->db->prepare("SELECT sum(total) AS extratotal from bookingextra where bookingoid = :a");
		$stmt->execute(array(':a' => $bookingoid));
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		$extratotal = $result['extratotal'];

		// charge
		$stmt = $this->db->prepare("SELECT sum(total) AS chargetotal from bookingcharges where bookingoid = :a");
		$stmt->execute(array(':a' => $bookingoid));
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		$chargetotal = $result['chargetotal'];

		$grandtotal = $roomtotal + $extratotal + $chargetotal;

		// paid
		$stmt = $this->db->prepare("SELECT sum(amount) AS paid from bookingpaymentdtl where bookingoid = :a");
		$stmt->execute(array(':a' => $bookingoid));
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		$paid = $result['paid'];
		$paid_balance = $grandtotal - $paid;

		//commission
		$stmt = $this->db->prepare("SELECT gbhpercentage as commission from booking where bookingoid = :a");
		$stmt->execute(array(':a' => $bookingoid));
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		$commission = $result['commission'];

		$thebuking_commission = $grandtotal * $commission / 100;
		$nethotel = $grandtotal - $commission;

		$stmt = $this->db->prepare("UPDATE booking set totalroom = :a, totalextra = :b, totalcharge = :c, grandtotal = :d, grandtotalr = :e, granddeposit = :f, grandbalance =:g, paid = :h, paid_balance = :i, hotelcollect = :j, gbhcollect = :k where bookingoid = :id");
		$stmt->execute(array(':id' => $bookingoid, ':a' => $roomtotal, ':b' => $extratotal, ':c' => $chargetotal, ':d' => $grandtotal, ':e' => $grandtotal, ':f' => $roomdeposit, ':g' => $roombalance, ':h' => $paid, ':i' => $paid_balance, ':j' => $nethotel, ':k' => $thebuking_commission));
	}

	function ccMasking($number, $maskingCharacter = 'x') {
    return substr($number, 0, 1) . str_repeat($maskingCharacter, strlen($number) - 3) . substr($number, -4);
	}
	function cvcMasking($number, $maskingCharacter = 'x') {
    return str_repeat($maskingCharacter, strlen($number));
	}

  function getDailyRateBookingbyDate($date, $bookingroomoid){
      $stmt = $this->db->prepare("SELECT `total`, `roomnumberoid`, `roomnumber` FROM `bookingroomdtl` LEFT JOIN `roomnumber` USING(`roomnumberoid`) WHERE `bookingroomoid`=:a AND `date`=:b");
      $stmt->execute(array(':a' => $bookingroomoid, ':b' => $date));
			$result = $stmt->fetch(PDO::FETCH_ASSOC);

			return $result;
  }

	function detailRatePlan($roomoffer){
		$stmt = $this->db->prepare("select ro.roomofferoid, ro.name as roomoffer, r.adult as maxadult, r.child as maxchild, ro.minrate from roomoffer ro inner join room r using (roomoid) where ro.roomofferoid = :a");
		$stmt->execute(array(':a' => $roomoffer));
		$result = $stmt->fetch(PDO::FETCH_ASSOC);

		return $result;
	}

	function getOOO($oooid){
		$stmt = $this->db->prepare("select ro.*, rn.roomnumber, r.name as room from room_ooo ro left join roomnumber rn using (roomnumberoid) left join room r using (roomoid) where oooid = :a");
		$stmt->execute(array(':a' => $oooid));
		$result = $stmt->fetch(PDO::FETCH_ASSOC);

		return $result;
	}
}

include('class-pms-lite-reservation.php');

class PMSEditRSV extends PMSLite{
	function getBookingRoomData(){
		$stmt = $this->db->prepare("select br.bookingroomoid, br.roomofferoid, br.guestroom, br.room, br.promotion, br.adult, br.child, br.checkin, br.checkout, br.checkoutr, br.extrabed, br.currencyoid, c.currencycode, br.totalr, ps.status as pmsstatus, br.pmsstatusoid from bookingroom br inner join currency c using (currencyoid) left join pmsstatus ps using (pmsstatusoid)  where br.bookingoid = :a group by br.bookingroomoid");
		$stmt->execute(array(':a' => $this->bookingoid));
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);

		return $result;
	}

	function getRoomNumber($bookingroomoid){
		$stmt = $this->db->prepare("select rn.roomnumber from bookingroomdtl brd inner join roomnumber rn using (roomnumberoid) where bookingroomoid = :a limit 1");
		$stmt->execute(array(':a' => $bookingroomoid));
		$result = $stmt->fetch(PDO::FETCH_ASSOC);

		return $result['roomnumber'];
	}

	function detailRatePlan($roomoffer){
		$stmt = $this->db->prepare("select ro.roomofferoid, ro.name as roomoffer, r.adult as maxadult, r.child as maxchild from roomoffer ro inner join room r using (roomoid) where ro.roomofferoid = :a");
		$stmt->execute(array(':a' => $roomoffer));
		$result = $stmt->fetch(PDO::FETCH_ASSOC);

		return $result;
	}

	function getBookingChargeData(){
		$stmt = $this->db->prepare("select bc.*, p.pos, c.currencycode from bookingcharges bc inner join currency c using (currencyoid) inner join typepos p using (typeposoid) inner join currency using (currencyoid) where bookingoid = :a");
		$stmt->execute(array(':a' => $this->bookingoid));
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);

		return $result;
	}

	function roomDetail($roomoffer){
		$stmt = $this->db->prepare("select ro.roomofferoid, ro.name as roomoffer, r.adult as maxadult, r.child as maxchild from roomoffer ro inner join room r using (roomoid) where ro.roomofferoid = :a");
		$stmt->execute(array(':a' => $roomoffer));
		$result = $stmt->fetch(PDO::FETCH_ASSOC);

		return $result;
	}

	function roomDateDetail($bookingroomoid){
		$stmt = $this->db->prepare("select brd.*, rn.roomnumber, c.currencycode from bookingroomdtl brd inner join roomnumber rn using (roomnumberoid) inner join currency c using (currencyoid) where brd.bookingroomoid = :a and brd.reconciled = '0'");
		$stmt->execute(array(':a' => $bookingroomoid));
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);

		return $result;
	}

	function roomNumberCheck($roomoid, $date){
		$stmt = $this->db->prepare("select rn.roomnumberoid, rn.roomnumber from roomnumber where roomnumberoid not in (select brd.roomnumberoid from bookingroomdtl brd inner join bookingroom br using (bookingroomoid) inner join booking b using (bookingoid) where brd.date = :a and b.bookingstatusoid = '4' and r.roomoid = :b)");
		$stmt->execute(array(':a' => $date, ':b' => $roomoid));
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);

		return $result;
	}

	function BookingSource(){
		$stmt = $this->db->prepare("SELECT bc.bc_name as bookingchannel, bm.bm_name as bookingmarket, a.agentname, a.email as agentemail, a.phone as agentphone from booking b left join bookingchannel bc using (bookingchanneloid) left join bookingmarket bm using (bookingmarketoid) left join agent a using (agentoid) where b.bookingoid = :a");
		$stmt->execute(array(':a' => $this->bookingoid));
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	function getBookingExtraData(){
		$stmt = $this->db->prepare("select be.*, p.name, c.currencycode from bookingextra be inner join currency c using (currencyoid) inner join extra p using (extraoid) where bookingoid = :a");
		$stmt->execute(array(':a' => $this->bookingoid));
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);

		return $result;
	}
}
?>
