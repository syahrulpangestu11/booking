<?php
 if(empty($_REQUEST['rt']) and empty($_REQUEST['sdate']) and empty($_REQUEST['edate'])){
   $start = date("d F Y");
   $end = date("d F Y",  strtotime(date("Y-m-d")." +1 month" ));
 }else{
   $start = date("d F Y", strtotime($_REQUEST['sdate']));
   $end = date("d F Y", strtotime($_REQUEST['edate']));
   $roomoffer = $_REQUEST['rt'];
   $channeloid = $_REQUEST['ch'];
 }

 include("js.php");
?>
<section class="content-header">
   <h1>
       Extra
   </h1>
   <ol class="breadcrumb">
       <li><a href="#"><i class="fa fa-table"></i> Allotments &amp; Rates</a></li>
       <li class="active">Extra</li>
   </ol>
</section>
<section class="content">
   <div class="row">
       <div class="box">
           <div class="box-body">
               <div class="form-group">
               <form method="get" enctype="multipart/form-data" id="data-input-template" action="<?php echo $base_url; ?>/extra/template/">
                   <label>Extra Name </label> &nbsp;
                   <input type="text" name="name" class="medium" value="<?php echo $_GET['name']; ?>" />&nbsp;&nbsp;
                   <button type="submit" class="blue-button">Find</button>
               </form>
               </div>
           </div><!-- /.box-body -->
      </div>
   </div>

   <div class="row">
       <div class="box">
         <form method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/extra/template/">
               <div id="data-box-template" class="box-body">
                   <div class="loader">Loading...</div>
               </div><!-- /.box-body -->
           </form>
      </div>
   </div>
</section>

<script type="text/javascript">
$(function(){

	function getLoadDataTemplate(){
		$.ajax({
			url: "<?php echo"$base_url"; ?>/includes/extra/data-extra-template.php",
			type: 'post',
			data: $('form#data-input-template').serialize(),
			success: function(data) {
				$("#data-box-template").html(data)
			}
		});
	}

	$(document).ready(function(){
		getLoadDataTemplate();
	});

});
</script>
