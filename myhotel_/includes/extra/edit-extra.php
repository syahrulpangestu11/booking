<?php
	$extraoid = $_GET['pid'];
	try {
		$stmt = $db->query("select e.* from extra e inner join hotel h using (hoteloid) where extraoid = '".$extraoid."'");
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
			$r_promo = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_promo as $row){
				$name = $row['name'];
				$salefrom = date("d F Y", strtotime($row['startbook']));
				$saleto = date("d F Y", strtotime($row['endbook']));
				$published = $row['publishedoid'];
				$currency = $row['currencyoid'];
                $category = $row['category'];
                $stock = $row['stock'];
				$price = $row['price'];
                $type = $row['type'];
				$headline = $row['headline'];
				$description = $row['description'];
				$picture = $row['picture'];
			}
		}else{
			echo "No Result";
			die();
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}
?>

<style>
    .form-box ul li span.label{
        color: black;
        font-size: 14px;
        font-weight: normal;
        text-align: left;}
     input[type=file]{display: inline-block;}
</style>
<script type="text/javascript">
$(function() {
	$('textarea.wsyg').trumbowyg({
		btns: ['viewHTML',
		  '|', 'btnGrp-design',
		  '|', 'link',
		  '|', 'btnGrp-justify',
		  '|', 'btnGrp-lists'],
		fullscreenable: false
	});
});
</script>
<section class="content-header">
    <h1>
        Edit Extra
    </h1>
		<ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Hotel</a></li>
        <li class="active">Extra Template</li>
    </ol>
</section>
<section class="content">
	<div class="row">
        <form class="form-box" method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/extra/edit-process">
        <input type="hidden" name="extraoid" value="<?=$extraoid;?>">
        <div class="box box-form">
        <ul class="inline-half">
            <li>
            <h1>Extra</h1>
            <ul class="block">
                <li>
                    <span class="label">Extra Name:</span>
                    <input type="text" name="name" value="<?=$name;?>" style="width:100%">
                </li>
                <li>
                    <span class="label">Category:</span>
                    <select name="category" class="input-select" style="width:100%">>
                        <?php
                        try {
                            $stmt = $db->query("select * from categoryextras where publishedoid = '1'");
                            $r_published = $stmt->fetchAll(PDO::FETCH_ASSOC);
                            foreach($r_published as $row){
                                if($row['categoryname'] == $category){ $selected = "selected"; }else{ $selected=""; }
                        ?> 
                            <option value="<?php echo $row['categoryname']; ?>" <?=$selected?>><?php echo $row['categoryname']; ?></option>
                        <?php
                            }
                        }catch(PDOException $ex) {
                            echo "Invalid Query";
                            die();
                        }
                        ?>
                    </select>
                </li>
                 <li>
                    <span class="label">Qty Stock:</span>
                    <input type="text" class="medium" value="<?=$stock;?>" name="qtystock">
                </li>
                <li>
                    <span class="label">Picture</span>
                    <input type="file" class="medium" name="image">
                </li>
                <li>
                <?php if(empty($picture)){ ?>
                <img id="preview" src="#" alt="" style="display:none" />
                <?php }else{ ?>
                <img id="preview" src="<?=$picture?>" alt=""/>
                <?php } ?>
                </li>
                <li>
                    <span class="label">Direct Link</span>
                    <textarea name="direct-link" class="form-control" style="resize: none;width: 100%;" readonly><?php echo 'https://thebuking.com/ibe/index.php?hcode='.$hcode.'&extra='.urlencode($name)?></textarea>
                </li>
                <li>
                    <div class="form-group">
                        <div style="float:left;width:50%;">
                            <label class="col-md-2">Source</label>
                            <div class="col-md-4"><input type="text" class="form-control utmset" name="utm_source" value="" style="width:100%"/></div>
                        </div>
                        <div style="float:left;width:50%;">
                            <label class="col-md-2">Champaign</label>
                            <div class="col-md-4"><input type="text" class="form-control utmset" name="utm_campaign" value="" style="width:100%"/></div>
                        </div>
                        <div style="clear:both"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2">URL Link</label>
                        <div class="col-md-10"><textarea name="direct2-link" class="form-control" style="width:100%"><?php echo 'https://thebuking.com/ibe/index.php?hcode='.$hcode.'&extra='.urlencode($name)?></textarea></div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12"><button type="button" class="btn btn-warning btn-sm copy2-link"><i class="fa fa-link"></i> Copy Link to Clipboard</button></div>
                    </div>
                </li>
            </ul>
            </li>
            <li>
            <div style="text-align:right;"><button class="default-button" type="submit">Save Extra</button></div>
            <ul class="block">
                <li><span class="label"><b>Sale Date</b></span></li>
                <li>
                    <span class="label">Sale Date From:</span>
                    <input type="text"class="medium" id="startdate" name="salefrom" value="<?=$salefrom;?>">
                </li>
                <li>
                    <span class="label">Sale Date To:</span>
                    <input type="text"class="medium" id="enddate"; name="saleto" value="<?=$saleto;?>">
                </li>
				<li>
                    <span class="label"><b>Price:</b></span>
                    <select name="currency" class="input-select">
                        <?php
                        try {
                            $stmt = $db->query("select * from currency where publishedoid = '1'");
                            $r_published = $stmt->fetchAll(PDO::FETCH_ASSOC);
                            foreach($r_published as $row){
								if($row['currencyoid'] == $currency){ $selected = "selected"; }else{ $selected=""; }
                        ?>
                            <option value="<?php echo $row['currencyoid']; ?>" <?=$selected?>><?php echo $row['currencycode']; ?></option>
                        <?php
                            }
                        }catch(PDOException $ex) {
                            echo "Invalid Query";
                            die();
                        }
                        ?>
                    </select>
                    <input type="text" class="medium" name="price" value="<?=floor($price);?>">
                    <select name="type" class="input-select">
                        <?php
                        try {
                            $stmt = $db->query("select * from extratype");
                            $r_published = $stmt->fetchAll(PDO::FETCH_ASSOC);
                            foreach($r_published as $row){
                                if($row['extratypename'] == $type){ $selected = "selected"; }else{ $selected=""; }
                        ?>
                            <option value="<?php echo $row['extratypename']; ?>" <?=$selected?>><?php echo $row['extratypename']; ?></option>
                        <?php
                            }
                        }catch(PDOException $ex) {
                            echo "Invalid Query";
                            die();
                        }
                        ?>
                    </select>
                </li>
                <li>
                    <span class="label">Publish Extra :</span>
                    <select name="published">
                    <?php
                        try {
                            $stmt = $db->query("select * from published where showthis = 'y'");
                            $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                            foreach($r_room as $row){
								if($row['publishedoid'] == $published){ $selected = "selected"; }else{ $selected=""; }
								echo"<option value='".$row['publishedoid']."' ".$selected.">".$row['note']."</option>";
                            }
                        }catch(PDOException $ex) {
                            echo "Invalid Query";
                            die();
                        }
                    ?>
                    </select>
                </li>
            </ul>
            </li>
        </ul>
        </div>
        <div class="box box-form">
			<h2>HEADLINE &amp; DETAIL</h2>
            <ul class="inline-half">
                <li>
                    <h3>Headline</h3>
                    <textarea name="headline" class="wsyg"><?=$headline;?></textarea>
                </li>
				<li>
                	<h3>Description</h3>
                   	<textarea name="description" class="wsyg"><?=$description;?></textarea>
				</li>
			</ul>
            <div class="clear"></div>
            <div class="clear"></div>
            <button class="default-button" type="submit">Save Extra</button>
   		</div>
		</form>
    </div>
</section>
