<?php
	include('includes/bootstrap.php');

	include('includes/function-tracking.php');

	if(isset($_GET['startdate'])){
		$startdate = date('Y-m-d', strtotime($_GET['startdate']));
	}else{
		$startdate = date('Y-m-01');
	}

	$show_startdate = date('d F Y', strtotime($startdate));
	$start = date('Ymd', strtotime($startdate));

	if(isset($_GET['enddate'])){
		$enddate = date('Y-m-d', strtotime($_GET['enddate']));
	}else{
		$enddate = date('Y-m-d');
	}

	$show_enddate = date('d F Y', strtotime($enddate));
	$end = date('Ymd', strtotime($enddate));

	$pie_color = array( "#f56954", "#00a65a", "#f39c12", "#00c0ef", "#3c8dbc", "#d2d6de");


	$filter_ip = IPWhitelist($hoteloid);

	$referal_website = getReferalSource($hoteloid, array('utm_source', 'tb_source'), 'availability', $start, $end);
	$browser = TABrowser($hoteloid, $start, $end);
?>
<section class="content">
<!-- Font Awesome -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- jvectormap -->
<link rel="stylesheet" href="<?php echo $base_url; ?>/lib/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Theme style -->
<div id="newdashboard">
    <div class="row">
        <!-- Left col -->
        <div class="col-md-4">
          <div class="info-box bg-yellow">
            <span class="info-box-icon"><i class="ion ion-log-in"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">1. Visit Booking Enginee</span>
              <span class="info-box-number"><?php print_r(getClick($hoteloid, "availability-visit", $start, $end)); ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>

          <div class="info-box bg-green">
            <span class="info-box-icon"><i class="ion ion-ios-search"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">2. Check Availability</span>
              <span class="info-box-number"><?php echo getClick($hoteloid, "availability-hit", $start, $end); ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>

          <div class="info-box bg-red">
            <span class="info-box-icon"><i class="ion ion-ios-pricetags-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">3. Click Promotion / Rate</span>
              <span class="info-box-number"><?php echo getClick($hoteloid, "click-room", $start, $end); ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>

          <div class="info-box bg-aqua">
            <span class="info-box-icon"><i class="ion ion-ios-cart-outline"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">4. Click Book Now</span>
              <span class="info-box-number"><?php echo getClick($hoteloid, "book-now", $start, $end); ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>

          <div class="info-box bg-purple">
            <span class="info-box-icon"><i class="ion ion-ios-personadd-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">5. Input Guest Data</span>
              <span class="info-box-number">
								<?php
								$input_guest =  getVisit($hoteloid, "payment", $start, $end);
								if($input_guest == 0){
									$date1 = new DateTime($startdate);
							    $date2 = new DateTime($enddate);
							    $diff = $date2->diff($date1)->format("%a") + 1;

									$query = "select count(b.bookingoid) as clickbooking from booking b inner join hotel h using (hoteloid) where h.hoteloid = '".$hoteloid."' and bookingtime >= '".$startdate."' and bookingtime <= '".$enddate."'";
									$stmt = $db->query($query);
									$row = $stmt->fetch(PDO::FETCH_ASSOC);
									$result = $row['clickbooking'];
									$avg = $result / $diff;
									$input_guest = $result.' - avg '.ceil($avg).' /day';
								}else{
									$input_guest =  getClick($hoteloid, "payment", $start, $end);
								}
								echo $input_guest;
								?>
							</span>
            </div>
            <!-- /.info-box-content -->
          </div>

          <div class="info-box bg-blue">
            <span class="info-box-icon"><i class="ion-cash"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">6. Payment</span>
              <span class="info-box-number"><?php echo $input_guest; ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>

          <div class="info-box bg-black">
            <span class="info-box-icon"><i class="ion-ios-close-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">7. Cancelled</span>
              <span class="info-box-number">0 - Avg 0 / day</span>
            </div>
            <!-- /.info-box-content -->
          </div>
        </div>

        <div class="col-md-8">
          <!-- MAP & BOX PANE -->
          <div class="box box-success">
            <div class="box-header with-border">
              <div class="row">
                <div class="col-md-4">
                  <h3 class="box-title">Visitors Report</h3>
                </div>
                <div class="col-md-8">
                <form method="get" action="<?=$base_url?>/tracking-analytics/">
                  <div class="row">
                      <div class="col-md-5">
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <input type="text" name="startdate" id="startdate" class="form-control" placeholder="Start Periode" value="<?=$show_startdate?>">
                          </div>
                      </div>
                      <div class="col-md-5">
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <input type="text" name="enddate" id="enddate" class="form-control" placeholder="End Periode" value="<?=$show_enddate?>">
                          </div>
                      </div>
                      <div class="col-md-2">
                      	<button type="submit" class="btn btn-xs btn-primary">View</button>
                      </div>
                  </div>
                </form>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <div class="row">
                <div class="col-md-9 col-sm-8">
                  <div class="pad">
                    <!-- Map will be created here -->
                    <div id="world-map-markers" style="height: 325px;"></div>
                  </div>
                </div>
                <!-- /.col -->
                <div class="col-md-3 col-sm-4">
                  <div class="pad box-pane-right bg-green" style="min-height: 280px">
                    <div class="description-block margin-bottom">
                      <div class="sparkbar pad" data-color="#fff">90,70,90,70,75,80,70</div>
                      <h5 class="description-header"><?php echo getVisit($hoteloid, "availability", $start, $end); ?></h5>
                      <span class="description-text">Visits</span>
                    </div>
                    <!-- /.description-block -->
                    <div class="description-block margin-bottom">
                      <div class="sparkbar pad" data-color="#fff">90,50,90,70,61,83,63</div>
                      <h5 class="description-header"><?=$referal_website[1]?></h5>
                      <span class="description-text">Referrals</span>
                    </div>
                    <!-- /.description-block -->
                    <div class="description-block">
                      <div class="sparkbar pad" data-color="#fff">90,50,90,70,61,83,63</div>
                      <h5 class="description-header"><?=$referal_website[0]?></h5>
                      <span class="description-text">Organic</span>
                    </div>
                    <!-- /.description-block -->
                  </div>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Browser Usage</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                  <div class="row">

                  <div class="col-md-7">
                    <div class="chart-responsive">
                    <canvas id="pieChart" height="150"></canvas>
                    </div>
                  </div>

                  <div class="col-md-5">
                  <ul class="chart-legend clearfix">
                  	<?php
						foreach($browser as $key => $browser_val){
					?>
                    	<li><i class="fa fa-circle-o" style="color:<?=$browser_val['color']?>"></i> <?=$browser_val['name']?></li>
                    <?php
						}
					?>
                  </ul>
                  </div>

                  </div>
                  <!-- ./chart-responsive -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
          </div>

        </div>

      </div>

    <div class="row">
        <div class="col-md-6">
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Promotions</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>#</th>
                    <th>Promotion</th>
                    <th>Hits</th>
                    <th>Sold</th>
                  </tr>
                  </thead>
                  <tbody>
					<?php
                        $no = 0;
                        $promotion = getHitsData($hoteloid, 'promotion/promo', 'click-room', '', $start, $end);
                        if(!empty($promotion)){
							$list_key_hits = array_column($promotion, 'hits');
							arsort($list_key_hits);
                            foreach($list_key_hits as $key => $value){
                    ?>
                      <tr>
                        <td><?=++$no?></td>
                        <td><?=$promotion[$key]['name']?></td>
                        <td><?=$promotion[$key]['hits']?></td>
                        <td>
													<?php
														$sold = getSoldData($hoteloid, 'promotion/promo', 'payment', $start, $end, $promotion[$key]['name']);
														if($sold == 0){
															$stmt = $db->prepare("select count(br.bookingroomoid) as sold from bookingroom br inner join booking b using (bookingoid) inner join hotel h using (hoteloid) where h.hoteloid = '".$hoteloid."' and bookingtime >= '".$startdate."' and bookingtime <= '".$enddate."' and br.promotion = :a");
															$stmt->execute(array(':a' => $promotion[$key]['name']));
															$row = $stmt->fetch(PDO::FETCH_ASSOC);
															$sold = $row['sold'];
														}
														echo $sold;
													?>
												</td>
                      </tr>
                    <?php
                            }
                        }
                    ?>
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
          </div>

          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Room Type</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>#</th>
                    <th>Room Type</th>
                    <th>Hits</th>
                    <th>Sold</th>
                  </tr>
                  </thead>
                  <tbody>
					<?php
                        $no = 0;
                        $roomtype = getHitsData($hoteloid, 'room/type', 'click-room', '', $start, $end);
                        if(!empty($roomtype)){
							$list_key_hits = array_column($roomtype, 'hits');
							arsort($list_key_hits);
                            foreach($list_key_hits as $key => $value){
                    ?>
                      <tr>
                        <td><?=++$no?></td>
                        <td><?=$roomtype[$key]['name']?></td>
                        <td><?=$roomtype[$key]['hits']?></td>
                        <td>
													<?php
														$sold = getSoldData($hoteloid, 'room/type', 'payment', $start, $end, $roomtype[$key]['name']);
														if($sold == 0){
															$stmt = $db->prepare("select count(br.bookingroomoid) as sold from bookingroom br inner join booking b using (bookingoid) inner join hotel h using (hoteloid) where h.hoteloid = '".$hoteloid."' and bookingtime >= '".$startdate."' and bookingtime <= '".$enddate."' and br.room = :a");
															$stmt->execute(array(':a' => $roomtype[$key]['name']));
															$row = $stmt->fetch(PDO::FETCH_ASSOC);
															$sold = $row['sold'];
														}
														echo $sold;
													?>
												</td>
                      </tr>
                    <?php
                            }
                        }
                    ?>
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
          </div>
        </div>
        <div class="col-md-6">
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Reference</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin referal">
                  <thead>
                  <tr>
                    <th>#</th>
                    <th>Reference</th>
                    <th>Hits</th>
                  </tr>
                  </thead>
                  <tbody>
					<?php
                        $no = 0;
                        $referal = getHitsData($hoteloid, 'utm_source', 'availability', '', $start, $end);
                        if(!empty($referal)){
							$list_key_hits = array_column($referal, 'hits');
							arsort($list_key_hits);
                            foreach($list_key_hits as $key => $value){
                    ?>
                      <tr data-toggle="modal" data-target="#details-<?=$referal[$key]['name']?>">
                        <td><?=++$no?></td>
                        <td><?=$referal[$key]['name']?></td>
                        <td><?=$referal[$key]['hits']?></td>
                      </tr>
                    <?php
                            }
                        }
                    ?>
                  </tbody>
                </table>
              </div>
              <?php
			  	/* --- show champaign --- */
				if(!empty($referal)){
					foreach($referal as $key => $value){
				?>
                <div class="modal fade" id="details-<?=$referal[$key]['name']?>" tabindex="-1" role="dialog" aria-labelledby="details-<?=$referal[$key]['name']?>">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><i class="fa fa-volume-up"></i> Champaign for referal <?=$referal[$key]['name']?></h4>
                      </div>
                      <div class="modal-body">
                            <table class="table no-margin">
                              <thead><tr><th>#</th><th>Champaign</th><th>Hits</th></tr></thead>
                              <tbody>
                                <?php
                                    $no = 0;
                                    $champaign = getHitsData($hoteloid, 'utm_champaign', 'availability', '[utm_source[text() = "'.$referal[$key]['name'].'"]]', $start, $end);
                                    if(!empty($champaign)){
                                        $list_key_hits = array_column($champaign, 'hits');
                                        arsort($list_key_hits);
                                        foreach($list_key_hits as $key => $value){
                                ?>
                                  <tr>
                                    <td><?=++$no?></td>
                                    <td><?=$champaign[$key]['name']?></td>
                                    <td><?=$champaign[$key]['hits']?></td>
                                  </tr>
                                <?php
                                        }
                                    }
                                ?>
                              </tbody>
                            </table>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                      </div>
                    </div><!-- /.modal-content -->
                  </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                <?php
					}
				}
			  ?>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
          </div>

          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Device</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>#</th>
                    <th>Device</th>
                    <th>Usage</th>
                  </tr>
                  </thead>
                  <tbody>
					<?php
                        $no = 0;
                        $device = getHitsData($hoteloid, 'device', 'availability', '', $start, $end);
                        if(!empty($device)){
							$list_key_hits = array_column($device, 'hits');
							arsort($list_key_hits);
                            foreach($list_key_hits as $key => $value){
                    ?>
                      <tr>
                        <td><?=++$no?></td>
                        <td><?=$device[$key]['name']?></td>
                        <td><?=$device[$key]['hits']?></td>
                      </tr>
                    <?php
                            }
                        }
                    ?>
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
          </div>

          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Platform</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>#</th>
                    <th>Platform</th>
                    <th>Usage</th>
                  </tr>
                  </thead>
                  <tbody>
					<?php
                        $no = 0;
                        $platform = getHitsData($hoteloid, 'platform', 'availability', '', $start, $end);
                        if(!empty($platform)){
							$list_key_hits = array_column($platform, 'hits');
							arsort($list_key_hits);
                            foreach($list_key_hits as $key => $value){
                    ?>
                      <tr>
                        <td><?=++$no?></td>
                        <td><?=$platform[$key]['name']?></td>
                        <td><?=$platform[$key]['hits']?></td>
                      </tr>
                    <?php
                            }
                        }
                    ?>
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
          </div>
        </div>
    </div>
</div>
<!-- FastClick -->
<script src="<?php echo $base_url; ?>/lib/plugins/fastclick/fastclick.js"></script>
<!-- Sparkline -->
<script src="<?php echo $base_url; ?>/lib/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo $base_url; ?>/lib/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo $base_url; ?>/lib/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="<?php echo $base_url; ?>/lib/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS 1.0.1 -->
<script src="<?php echo $base_url; ?>/lib/plugins/chartjs/Chart.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo $base_url; ?>/admin-lte/js/demo.js"></script>

<script type="text/javascript">
$(function () {
  //-------------
  //- PIE CHART -
  //-------------
  // Get context with jQuery - using jQuery's .get() method.
  var pieChartCanvas = $("#pieChart").get(0).getContext("2d");
  var pieChart = new Chart(pieChartCanvas);
  var PieData = [
	<?php
		foreach($browser as $key => $browser_val){
	?>
		{
		  value: <?=$browser_val['used']?>,
		  color: '<?=$browser_val['color']?>',
		  highlight: '<?=$browser_val['color']?>',
		  label: '<?=$browser_val['name']?>'
		},
	<?php
		}
	?>
  ];
  var pieOptions = {
    //Boolean - Whether we should show a stroke on each segment
    segmentShowStroke: true,
    //String - The colour of each segment stroke
    segmentStrokeColor: "#fff",
    //Number - The width of each segment stroke
    segmentStrokeWidth: 1,
    //Number - The percentage of the chart that we cut out of the middle
    percentageInnerCutout: 50, // This is 0 for Pie charts
    //Number - Amount of animation steps
    animationSteps: 100,
    //String - Animation easing effect
    animationEasing: "easeOutBounce",
    //Boolean - Whether we animate the rotation of the Doughnut
    animateRotate: true,
    //Boolean - Whether we animate scaling the Doughnut from the centre
    animateScale: false,
    //Boolean - whether to make the chart responsive to window resizing
    responsive: true,
    // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
    maintainAspectRatio: false,
    //String - A legend template
    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
    //String - A tooltip template
    tooltipTemplate: "<%=value %> <%=label%> users"
  };
  //Create pie or douhnut chart
  // You can switch between pie and douhnut using the method below.
  pieChart.Doughnut(PieData, pieOptions);
  //-----------------
  //- END PIE CHART -
  //-----------------

  /* jVector Maps
   * ------------
   * Create a world map with markers
   */
  $('#world-map-markers').vectorMap({
    map: 'world_mill_en',
    normalizeFunction: 'polynomial',
    hoverOpacity: 0.7,
    hoverColor: false,
    backgroundColor: 'transparent',
    regionStyle: {
      initial: {
        fill: 'rgba(210, 214, 222, 1)',
        "fill-opacity": 1,
        stroke: 'none',
        "stroke-width": 0,
        "stroke-opacity": 1
      },
      hover: {
        "fill-opacity": 0.7,
        cursor: 'pointer'
      },
      selected: {
        fill: 'yellow'
      },
      selectedHover: {
      }
    },
    markerStyle: {
      initial: {
        fill: '#00a65a',
        stroke: '#111'
      }
    },
    markers: [
      {latLng: [-8.2405, 115.092], name: 'Vatican City'}
    ]
  });

  /* SPARKLINE CHARTS
   * ----------------
   * Create a inline charts with spark line
   */

  //-----------------
  //- SPARKLINE BAR -
  //-----------------
  $('.sparkbar').each(function () {
    var $this = $(this);
    $this.sparkline('html', {
      type: 'bar',
      height: $this.data('height') ? $this.data('height') : '30',
      barColor: $this.data('color')
    });
  });

  //-----------------
  //- SPARKLINE PIE -
  //-----------------
  $('.sparkpie').each(function () {
    var $this = $(this);
    $this.sparkline('html', {
      type: 'pie',
      height: $this.data('height') ? $this.data('height') : '90',
      sliceColors: $this.data('color')
    });
  });

  //------------------
  //- SPARKLINE LINE -
  //------------------
  $('.sparkline').each(function () {
    var $this = $(this);
    $this.sparkline('html', {
      type: 'line',
      height: $this.data('height') ? $this.data('height') : '90',
      width: '100%',
      lineColor: $this.data('linecolor'),
      fillColor: $this.data('fillcolor'),
      spotColor: $this.data('spotcolor')
    });
  });

	var dateFormat = "dd MM yy",
	from = $( "#startdate" )
		.datepicker({
		defaultDate: "+1d",
		changeMonth: true,
		changeYear: true,
		dateFormat: "dd MM yy",
		numberOfMonths: 1
		})
		.on( "change", function() {
		to.datepicker( "option", "minDate", getDate( this ) );
		}),
	to = $( "#enddate" ).datepicker({
		defaultDate: "+1d",
		changeMonth: true,
		changeYear: true,
		dateFormat: "dd MM yy",
		numberOfMonths: 1
	})
	.on( "change", function() {
		from.datepicker( "option", "maxDate", getDate( this ) );
	});
});
</script>
<style>
table.referal tbody > tr{ cursor:pointer; }
</style>
</section>
