<?php
	session_start();
	include("../../conf/connection.php");

	$name = $_POST['name'];
	$address = $_POST['address'];
	$phone = $_POST['phone'];
	$website = $_POST['website'];
	$email = $_POST['email'];
	$published = $_POST['published'];

	$c_useroid = $_SESSION['_oid'];
	$c_hotelcreatoroid = $_SESSION['_hotel'];
	$c_datecreatedoid = date('Y-m-d H:i:s');

	$hoid = $_POST['hoid'];
	//$published_agent = $_POST['published_agent'];
	$published_agent = '1';

	$city = $_POST['city'];
	$agenttype = $_POST['agenttype'];

	try {
		$s_a = $db->prepare("INSERT INTO agent (agentname,address,cityoid,website,email,phone,publishedoid,useroid,hotelcreatoroid,datecreated,agenttypeoid) VALUES (:a, :b, :c, :d, :e, :f, :g, :h, :i, :j,:k)");
		$s_a->execute(array(':a' => $name, ':b' => $address, ':c' => $city, ':d' => $website, ':e' => $email, ':f' => $phone, ':g' => $published_agent, ':h' => $c_useroid, ':i' => $c_hotelcreatoroid, ':j' => $c_datecreatedoid, ':k' => $agenttype));

		$last_id = $db->lastInsertId();
		$stmt = $db->query("SELECT agentoid FROM agent ORDER BY agentoid DESC LIMIT 1");
		$r_agent = $stmt->fetchAll(PDO::FETCH_ASSOC);
		foreach($r_agent as $row){
			$agentoid = $row['agentoid'];

			$s_ah = $db->prepare("INSERT INTO agenthotel (hoteloid,agentoid,publishedoid) VALUES (:h, :i, :j)");
			$s_ah->execute(array(':h' => $hoid, ':i' => $agentoid, ':j' => $published));

		}

		$affected_rows = $s_a->rowCount();
	}catch(PDOException $ex) {
		echo "0".$ex;
		die();
	}
	echo "1";
?>
