<script type="text/javascript">
$(function(){
	$('body').on('click','button.add-button, button.edit-button', function(e) {
		lp = $(this).attr('lp');
		form = $('form#edit-lp');
		form.find('input[name=lp]').val(lp);
		form.submit();
	});
	/*
	* BUTTON
	*/
	$('#hotelListModal').on('show.bs.modal', function (e) {
		generateHotel(1);
	});
	// jalankan fungsi saat pencarian di modal hotel
	$('#hotelListModal button#find').click(function(){
		generateHotel(1);
	});
	// jalankan fungsi paging list pelanggan di modal hotel
	$('body').on('click','#hotelListModal #paging button', function(e) {
		page	= $(this).attr('page');
		generateHotel(page);
	});

	$('#promotionListModal').on('show.bs.modal', function (e) {
		 var button = $(e.relatedTarget)
		 $(this).find('input[name=ho]').val(button.data('hotel'));
		 $(this).find('input[name=lph]').val(button.data('lph'));
		 generatePromotion(1);
	});
	// jalankan fungsi saat pencarian di modal promotion
	$('#promotionListModal button#find').click(function(){
		generatePromotion(1);
	});
	// jalankan fungsi paging list pelanggan di modal promotion
	$('body').on('click','#promotionListModal #paging button', function(e) {
		page	= $(this).attr('page');
		generatePromotion(page);
	});

	/*
	* LOAD DATA
	*/

	// fungsi untuk menampilkan list pelanggan pada modal sesuai nomor paging
	function generateHotel(page){
		var chain = $('#hotelListModal').find('input[name=chain]').val();
		var name = $('#hotelListModal').find('input[name=name]').val();
		var lp = $('input[name=loyaltyprogram]').val();
		var listbox = $('#hotelListModal').find('div#list');
		$.ajax({
			url		: '<?php echo $base_url; ?>/includes/crm/list-hotel.php',
			type	: 'post',
			data	: { chain : chain, name : name, lp : lp, page : page },
			success	: function(response) {
				listbox.html(response);
			}
		});
	}

	function generatePromotion(page){
		var ho = $('#promotionListModal').find('input[name=ho]').val();
		var lph = $('#promotionListModal').find('input[name=lph]').val();
		var listbox = $('#promotionListModal').find('div#list');
		$.ajax({
			url		: '<?php echo $base_url; ?>/includes/crm/list-promotion.php',
			type	: 'post',
			data	: { name : name, ho : ho, lph : lph, page : page },
			success	: function(response) {
				listbox.html(response);
			}
		});
	}

	$('button.submit-add').click(function(){
		currentbutton = $(this);
		forminput = $('form#data-input');
		$.ajax({
			url: '<?php echo $base_url; ?>/includes/crm/rqst-action.php',
			type: 'post',
			data: forminput.serialize() + '&request=new-program',
			success: function(data) {
				if(data == "success"){
					$dialogNotice.html("Loyalty Member Program has been created. System will refresh the page");
					$dialogNotice.dialog("open");
				}else{
					$dialogNotice.html("We&rsquo;re very sorry, we can't save your data right now"+data);
					$dialogNotice.dialog("open");
				}
			}
		});
	});

	$('button.submit-edit').click(function(){
		currentbutton = $(this);
		forminput = $('form#data-input');
		$.ajax({
			url: '<?php echo $base_url; ?>/includes/crm/rqst-action.php',
			type: 'post',
			data: forminput.serialize() + '&ho=<?=$hoteloid?>&request=update-program',
			success: function(data) {
				if(data != "error"){
					$dialogNotice.html("Loyalty Member Program has been created. System will refresh the page");
					$dialogNotice.dialog("open");
				}else{
					$dialogNotice.html("We&rsquo;re very sorry, we can't save your data right now");
					$dialogNotice.dialog("open");
				}
			}
		});
	});

	$('body').on('click', '#hotelListModal button#assign-hotel', function (e){
		var forminput = $('#hotelListModal').find('form#form-assign-hotel');
		var lp = $('input[name=loyaltyprogram]').val();
		$.ajax({
			url		: '<?php echo $base_url; ?>/includes/crm/rqst-action.php',
			type	: 'post',
			data	: forminput.serialize() + '&loyaltyprogram=' + lp + '&request=assign-hotel',
			success	: function(response){
				$('div#assigned-hotel').find('ul#accordion').append(response);
				$('#accordion').accordion("refresh");
			}
		});
		$('#hotelListModal').modal('toggle');
	});


	$('body').on('click', '#promotionListModal button#assign-promotion', function (e){
		var forminput = $('#promotionListModal').find('form#form-assign-promotion');
		var lph = $('input[name=lph]').val();
		var targetbox = $('div#assigned-hotel').find('li[lph='+lph+']').find('div#assigned-promotion');
		//alert();
		$.ajax({
			url		: '<?php echo $base_url; ?>/includes/crm/rqst-action.php',
			type	: 'post',
			data	: forminput.serialize() + '&loyaltyprogramhotel=' + lph + '&request=assign-promotion',
			success	: function(response){
				targetbox.append(response);
			}
		});
		$('#promotionListModal').modal('toggle');
	});

	$('body').on('click', '#membershipModal button#save', function (e){
		modal = $('#membershipModal');
		//alert($('div#membership').find('table').html());
		$('table#membership').children('tbody').append('<tr><td><input type="text" name="m-name[]" class="form-control" value="'+ modal.find('input[name=name]').val() +'" /></td><td><input type="text" name="m-startpoint[]" class="form-control" value="'+ modal.find('input[name=startpoint]').val() +'" /></td><td><input type="text" name="m-endpoint[]" class="form-control" value="'+ modal.find('input[name=endpoint]').val() +'" /></td><td><input type="text" class="form-control" name="m-description[]" value="'+ modal.find('textarea[name=description]').val() +'"></td><td><div class="input-group"><input type="number" class="form-control" name="m-discount[]" value="'+ modal.find('input[name=discount]').val() +'" required><div class="input-group-addon">%</div></div></td><td><button type="button" class="btn btn-danger btn-sm" id="remove-membership"><i class="fa fa-trash-o"></i></button></td></tr>');
		$('#membershipModal').modal('toggle');
	});

	$('body').on('click', 'button#remove-membership', function (e){
		$(this).parent().parent('tr').remove();
	});

	/*confirmation before remove promotion*/
	$('#confirmDelPromotion').on('show.bs.modal', function (e) {
		 var promotion = $(e.relatedTarget).data('lpp');
		 $(e.currentTarget).find('input[name="lpp"]').val(promotion);
	});
	/*remove promotion*/
	$('body').on('click', '#confirmDelPromotion button#remove-lpp', function (e){
		$('#confirmDelPromotion').modal('toggle');
		var lpp = $('#form-remove-promotion').find('input[name=lpp]').val();
		$.ajax({
			url		: '<?php echo $base_url; ?>/includes/crm/rqst-action.php',
			type	: 'post',
			data	: { lpp : lpp, request : 'unassign-promotion'},
			success	: function(response){
				if(response == 'success'){
					$dialogNotice.html("Promotion has been removed");
					$dialogNotice.dialog("open");
				}else{
					$dialogNotice.html("Promotion failed to remove");
					$dialogNotice.dialog("open");
				}
			}
		});
	});

	$('button.cancel').click(function(){
		$(location).attr('href', '<?=$base_url?>/crm/loyalty-member-program')
	});

	/*
	* DIALOG
	*/

	var $dialogNotice = $('<div id="dialog-notice"></div>')
	.dialog({
		autoOpen: false,
		title: 'Notification',
		buttons: {
			"Ok": function(){
				$( this ).dialog( "close" );
				location.reload();
			}
		}
	});

	/*
	* ACCORDION
	*/

	$( "#accordion" ).accordion({
		header: ".header",
		collapsible: true,
		active: false,
		heightStyle: "content",
	});

});
</script>
