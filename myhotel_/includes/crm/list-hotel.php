<?php
	error_reporting(E_ALL ^ E_NOTICE);
	include("../../conf/connection.php");
	
	$loyaltyprogramoid = $_POST['lp'];
	$s_existed_hotel = "SELECT GROUP_CONCAT(hoteloid) as existed from `loyaltyprogramhotel` where `loyaltyprogramoid` = '".$loyaltyprogramoid."'";
	$stmt = $db->query($s_existed_hotel);
	$r_existed_hotel = $stmt->fetch(PDO::FETCH_ASSOC);
	if(!empty($r_existed_hotel['existed'])){
		$exist_hotel = $r_existed_hotel['existed'];
	}else{
		$exist_hotel = '';
	}
?>

<div class="box-body">
    <table class="table table-striped">
        <thead>
            <tr><th>No.</th>
            <th>Hotel</th>
            <th>Chain</th>
            <th>Select</th>
        </tr></thead>
        <tbody>
        <?php
            // main query
            $batas=10; 
			$halaman=isset($_REQUEST['page']) ? $_REQUEST['page'] : 0 ;
			if(empty($halaman)){ 
				$posisi=0; $halaman=1;
			}else{ 
				$posisi = ($halaman-1) * $batas; 
			}
			$no=$posisi+1;
			$j = $halaman + ($halaman - 1) * ($batas - 1); 
			
			$paging_query = " limit ".$posisi.", ".$batas;
			$num = $posisi;
            
            $main_query ="select h.hoteloid, h.hotelname, c.name as chainname from hotel h left join chain c using (chainoid)";
            
            // query tambahan jika ada filter
            $filter = array();
            if(isset($_REQUEST['name']) and $_REQUEST['name']!=''){
                array_push($filter, 'h.hotelname like "%'.$_REQUEST['keyword'].'%"');
            }
            if(isset($_REQUEST['chain']) and $_REQUEST['chain']!=''){
                array_push($filter, 'h.chainoid = "'.$_REQUEST['chain'].'"');
            }
			array_push($filter, 'h.publishedoid <> "3"');
			if(!empty($exist_hotel)){
				array_push($filter, 'h.hoteloid not in ('.$exist_hotel.')');
			}
            // menggabungkan query utama dengan query filter
            if(count($filter) > 0){
                $combine_filter = implode(' and ',$filter);
                $syntax_list = $main_query.' where '.$combine_filter;
            }else{
                $syntax_list = $main_query;
            }
            
            $syntax_list_paging = $syntax_list." ".$paging_query;
            $stmt = $db->query($syntax_list_paging);
			$result_list = $stmt->fetchAll(PDO::FETCH_ASSOC);
            foreach($result_list as $list){
                $num++;
        ?>
            <tr>
                <td><?php echo $num; ?></td>
                <td><?php echo $list['hotelname']; ?></td>
                <td><?php echo $list['chainname']; ?></td>
                <td><input type="checkbox" name="hotel[]" value="<?php echo $list['hoteloid']; ?>"></td>
            </tr>
        <?php
            }
        ?>
        </tbody>
    </table>
    <?php require_once('paging/post-paging.php'); ?>
</div>
