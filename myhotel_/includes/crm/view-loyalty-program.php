 <?php
	$name = $_POST['name'];
	$email = $_POST['email'];

  if(isset($_SESSION['_hotel']) and !empty($_SESSION['_hotel'])){
    $stmt = $db->prepare("select lp.* from loyaltyprogram lp left join hotel h using (hoteloid) where h.hoteloid = :a or (lp.chainoid = (select chainoid from hotel where hoteloid = :b)) and lp.chainoid != '1'");
    $stmt->execute(array(':a' => $hoteloid, ':b' => $hoteloid));
  }else{
    $stmt = $db->query("select * from loyaltyprogram lp where (lp.chainoid in (select oid from userassign where type = 'chainoid' and useroid = '".$_SESSION['_oid']."') or lp.hoteloid in (select oid from userassign where type = 'hoteloid' and useroid = '".$_SESSION['_oid']."')) or (lp.hoteloid in (select hoteloid from hotel where chainoid in (select oid from userassign where type = 'chainoid' and useroid = '".$_SESSION['_oid']."')))");
  }
  $found_lp = $stmt->rowCount();
?>
<section class="content-header">
    <h1>Loyalty Member Program</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-handshake-o"></i> Customer Relationship Management</a></li>
        <li class="active">Loyalty Member Program</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="box">
            <div class="box-body">
                <div class="form-group">
            	<form method="get" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/crm/member/">
            		<input type="hidden" name="hoteloid" value="<?php echo $hoteloid; ?>" />
                    <label>Loyalty Member Program Name  :</label> &nbsp;&nbsp;
					<input type="text" name="name" class="input-text" value="<?php echo $name; ?>">&nbsp;
                    <button type="submit" class="small-button blue"><i class="fa fa-search"></i>Find</button>
                </form>
                </div>
            </div><!-- /.box-body -->
       </div>
    </div>

    <div class="row">
        <div class="box">
        	<form method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/room-control/process">
                <div id="data-box" class="box-body">
                    <?php if($found_lp == 0){ ?>
                    <button type="button" class="small-button blue add-button">Create New Loyalty Member Program</button>
                    <?php } ?>
                    <table class="table table-fill">
                        <tr align="center">
                            <td>Name</td>
                            <td>Periode</td>
                            <td>Type</td>
                            <td>Hotel &amp; Promotion Subsribe</td>
                            <td>&nbsp;</td>
                        </tr>
                        <?php
            							$r_lp = $stmt->fetchAll(PDO::FETCH_ASSOC);
            							foreach($r_lp as $lp){
            						?>
                        <tr valign="top">
                            <td><?=$lp['name']?></td>
                            <td align="center"><?=date('d/M/Y', strtotime($lp['startdate']))?> - <?=date('d/M/Y', strtotime($lp['enddate']))?></td>
                            <td>in - house</td>
                            <td>
							<?php
                                $stmt = $db->prepare("select h.hotelname, h.hoteloid, lph.loyaltyprogramhoteloid from hotel h inner join loyaltyprogramhotel lph using (hoteloid) where lph.loyaltyprogramoid = :a");
                                $stmt->execute(array(':a' => $lp['loyaltyprogramoid']));
                                $r_lp_hotel = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                foreach($r_lp_hotel as $hotel){
									echo "<b>".$hotel['hotelname']."</b><br>";

									$stmt = $db->prepare("select p.name, lpp.loyaltyprogrampromotionoid from promotion p inner join loyaltyprogrampromotion lpp using (promotionoid) where lpp.loyaltyprogramhoteloid = :a");
									$stmt->execute(array(':a' => $hotel['loyaltyprogramhoteloid']));
									$r_lp_promotion = $stmt->fetchAll(PDO::FETCH_ASSOC);
									foreach($r_lp_promotion as $promotion){
										echo $promotion['name']."<br>";
									}
								}
                            ?>
                            </td>
                            <td>
                              <button type="button" class="pencil edit-button" lp="<?=$lp['loyaltyprogramoid']?>">Edit</button>&nbsp;
                              <button type="button" class="trash delete-button"  lp="<?=$lp['loyaltyprogramoid']?>">Delete</button></td>
                        </tr>
                        <?php
							}
						?>
                    </table>
                </div><!-- /.box-body -->
            </form>
       </div>
    </div>

    <form id="edit-lp" method="post" action="<?php echo $base_url; ?>/crm/loyalty-member-program/edit">
      <input type="hidden" name="lp" value="">
    </form>
</section>
