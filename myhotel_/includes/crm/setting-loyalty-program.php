<link rel="stylesheet" href="<?php echo $base_url; ?>/lib/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo $base_url; ?>/lib/admin-lte/css/AdminLTE.min.css">
<link href="<?php echo $base_url?>/css/style.css?v=1" rel="stylesheet" type="text/css">
<script src="<?php echo $base_url; ?>/lib/bootstrap/js/bootstrap.min.js"></script>
		<style type="text/css">
			h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
				font-family: inherit;
				font-weight: 600;
				line-height: inherit;
				color: inherit;
				margin-bottom:10px!important;
			}
			.wrapper {
				position: inherit;
				overflow: hidden!important;
			}
			.left-side {
				padding-top: inherit;
			}
			.sidebar > .sidebar-menu li > a:hover {
				background-color: rgba(72, 115, 175, 0.26);
			}
			.sidebar .sidebar-menu .treeview-menu {
				background-color: rgb(14, 26, 43);
			}
			.sidebar .sidebar-menu .treeview-menu .treeview-menu-child {
				background-color: rgba(0, 0, 0, 0.5);
			}
			.sidebar > .sidebar-menu li.active > a {
				background-color: rgba(197, 45, 47, 0.55);
			}
			.sidebar > .sidebar-menu > li.treeview.active > a {
				background-color: inherit;
			}
			.sidebar > .sidebar-menu > li.treeview.active li.treeview-child.active > a {
				background-color: inherit;
			}
			.sidebar .sidebar-menu > li > a > .fa {
				width: 28px;
				font-size: 16px;
			}
			.sidebar-menu, .main-sidebar .user-panel, .sidebar-menu>li.header{
				white-space:normal!important;
			}
			.form-group input[type=text]{
				width:100%!important;
			}

  #accordion .header {background: #f4f8fb !important; border: 1px solid #bbb; color: #222; margin-top: 10px; font-weight: normal;}
  #accordion .header.ui-accordion-header-active,
  #accordion .header.ui-state-hover {background-color: #5d9cec !important; border-color: transparent; color: #fff;}
  #accordion .header .table-cell {display: table-cell; vertical-align: middle; padding: 0 10px;}
  #accordion .header .table-cell:nth-of-type(1) {width: 10%;}
  #accordion .header#accordion .header .table-cell:nth-of-type(2) {width: 30%;}
  #accordion .content{ min-height:0!important; }
  #accordion #assigned-promotion > div{ margin-bottom:5px; }

  #accordion .header .image .thumbnail {width: 100%;}

  #accordion .content {
    background: #F4F8FB !important; border: 1px solid #bbb !important;
    border-top: none;
  }

  .btn-xs{ padding : 1px 5px!important; }
		</style>
<?php
	try {
		if(!empty($_POST['lp'])){
			$lp = $_POST['lp'];
		}else{
			$lp = $_SESSION['_loyaltyprogram'];
		}
		$stmt = $db->query("select * from loyaltyprogram lp where lp.loyaltyprogramoid = '".$lp."'");
		$activeLMP = $stmt->rowCount();

		if($activeLMP == 0){
	    	$startdate = date('d F Y');
			$enddate = date('d F Y', strtotime('+3 month'));
			$campaign['earnpointmethod'] = "transaction";
		?>
        <style type="text/css">
		div#step-2, div#step-3, button.submit-edit{ display:none; }
        </style>
        <?php
		}else{
			$campaign = $stmt->fetch(PDO::FETCH_ASSOC);
	    	$startdate = date('d F Y', strtotime($campaign['startdate']));
			$enddate = date('d F Y', strtotime($campaign['enddate']));
		}

	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}
?>
<section class="content-header">
    <h1>Loyalty Member Program</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-handshake-o"></i> Customer Relationship Management</a></li>
        <li class="active">Loyalty Member Program</li>
    </ol>
</section>
<section class="content">
    <form method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/promotions/add-process">
    <div class="box box-form" id="step-1">
        <div class="row">
            <div class="col-md-12"><h1>Loyalty Member Program Detail</h1></div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group col-md-12">
                    <label>Loyalty Member Program Name</label>
                    <input type="text" class="form-control" name="name" required value="<?=$campaign['name']?>">
                </div>
                <div class="form-group col-md-12">
                    <label>Description</label>
                    <textarea class="form-control" name="description"><?=$campaign['description']?></textarea>
                </div>
                <div class="form-group col-md-12" style="display:none;">
                    <label>Loyalty Member Program Logo</label>
                    <input type="file" class="form-control" name="logo">
                </div>
                <div class="form-group col-md-6">
                    <label>Publish Loyalty Member Program</label>
                    <select name="published" class="form-control">
                        <?php
                            try {
                                $stmt = $db->query("select * from published where showthis = 'y'");
                                $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                foreach($r_room as $row){
                                    if($row['publishedoid'] == $campaign['publishedoid']){ $selected = "selected"; }else{ $selected=""; }
                                    echo"<option value='".$row['publishedoid']."' ".$selected.">".$row['note']."</option>";
                                }
                            }catch(PDOException $ex) {
                                echo "Invalid Query";
                                die();
                            }
                        ?>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group col-md-6">
                    <label>Start Periode</label>
                    <div class="input-group"><div class="input-group-addon"><i class="fa fa-calendar"></i></div><input type="text" class="form-control" name="startdate" id="startdate" required value="<?=$startdate?>"></div>
                </div>
                <div class="form-group col-md-6">
                    <label>End Periode</label>
                    <div class="input-group"><div class="input-group-addon"><i class="fa fa-calendar"></i></div><input type="text" class="form-control" name="enddate" id="enddate" required value="<?=$enddate?>"></div>
                </div>
                <div class="form-group col-md-8">
                    <label>Earn Point</label>
                    <div class="input-group"><div class="input-group-addon">IDR</div><input type="number" class="form-control" name="conversionpoint" required value="<?=floor($campaign['conversionpoint'])?>"><div class="input-group-addon">/ 1 point</div></div>
                </div>
								<div class="form-group col-md-12">
                    <label><input type="radio" name="earnpointmethod" value="transaction" <?php if($campaign['earnpointmethod'] == "transaction"){ echo "checked"; } ?>> Earn Point from transaction booking</label>
                    <label><input type="radio" name="earnpointmethod" value="afterredeem" <?php if($campaign['earnpointmethod'] == "afterredeem"){ echo "checked"; } ?>> Earn Point from <b style="color:#f00">transaction booking after redemption</b></label>
                </div>
								<div class="form-group col-md-8">
                    <label>Redeem Point</label>
                    <div class="input-group"><div class="input-group-addon">1 Point = IDR</div><input type="number" class="form-control" name="conversionamount" required value="<?=floor($campaign['conversionamount'])?>"></div>
                </div>
				<?php if($activeLMP == 0){ ?>
                    <div class="form-group col-md-12 text-right">
                        <button type="button" class="small-button blue submit-add">Save Detail &amp; Continue Assign Hotel <i class="fa fa-arrow-right"></i></button>
                    </div>
                <?php }else{ ?>
                    <div class="form-group col-md-12 text-right">
                        <button type="button" class="small-button blue submit-edit">Save Change</button>
                        <input type="hidden" name="loyaltyprogram" value="<?=$campaign['loyaltyprogramoid']?>" />
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>

    <div class="box box-form" id="step-2">
        <div class="row">
             <div class="col-md-6"><h1><i class="fa fa-users"></i> Apply For Membership</h1></div>
             <div class="col-md-6 text-right"><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#membershipModal">Add New Membership</button></div>
        </div>
        <div class="row">
            <table id="membership" class="table table-striped">
                <thead>
                    <tr>
                      <th rowspan="2">Membership</th>
											<th colspan="2">Range Point</th>
                      <th rowspan="2">Description</th>
                      <th rowspan="2">Discount</th>
                      <th rowspan="2">&nbsp;</th>
                    </tr>
										<tr>
											<th>From</th>
											<th>To</th>
										</tr>
                </thead>
                <tbody>
					<?php
                        $stmt = $db->prepare("select * from loyaltyprogrammembership where loyaltyprogramoid = :a");
                        $stmt->execute(array(':a' => $campaign['loyaltyprogramoid']));
						$currentmembership = $stmt->rowCount();
                        if($currentmembership == 0){
					?>
                    <tr>
                        <td><input type="text" name="m-name[]" class="form-control" value="Regular Membership" /></td>
												<td><input type="text" class="form-control" name="m-startpoint[]" value="0"></td>
												<td><input type="text" class="form-control" name="m-endpoint[]" value="100"></td>
                        <td><input type="text" name="m-description[]" class="form-control"></td>
                        <td><div class="input-group"><input type="number" class="form-control" name="m-discount[]" value="10" required><div class="input-group-addon">%</div></div></td>
                        <td><button type="button" class="btn btn-danger btn-sm" id="remove-membership"><i class="fa fa-trash-o"></i></button></td>
                    </tr>
                    <?php
						}else{
							$r_membership = $stmt->fetchAll(PDO::FETCH_ASSOC);
							foreach($r_membership as $membership){
                    ?>
                    <tr>
                        <td>
                        	<input type="hidden" name="membershipoid[]" value="<?=$membership['loyaltyprogrammembershipoid']?>">
                        	<input type="text" name="cm-name[]" class="form-control" value="<?=$membership['name']?>" />
                        </td>
												<td><input type="text" name="cm-startpoint[]" class="form-control" value="<?=$membership['startpoint']?>"></td>
												<td><input type="text" name="cm-endpoint[]" class="form-control" value="<?=$membership['endpoint']?>"></td>
                        <td><input type="text" name="cm-description[]" class="form-control" value="<?=$membership['description']?>"></td>
                        <td><div class="input-group"><input type="number" class="form-control" name="cm-discount[]" value="<?=floor($membership['discount'])?>" required><div class="input-group-addon">%</div></div></td>
                        <td><button type="button" class="btn btn-danger btn-sm" id="remove-membership"><i class="fa fa-trash-o"></i></button></td>
                    </tr>
                    <?php
							}
						}
					?>
                </tbody>
            </table>
        </div>
    </div>

    <div class="box box-form" id="step-3">
        <div class="row">
            <div class="col-md-6"><h1><i class="fa fa-building"></i> Apply Campaign To Hotel</h1></div>
            <div class="col-md-6 text-right"><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#hotelListModal">Assign New Hotel</button></div>
        </div>
        <div class="row" id="assigned-hotel">
            <ul id="accordion">
            <?php
				$stmt = $db->prepare("select h.hotelname, h.hoteloid, lph.loyaltyprogramhoteloid from hotel h inner join loyaltyprogramhotel lph using (hoteloid) where lph.loyaltyprogramoid = :a");
				$stmt->execute(array(':a' => $campaign['loyaltyprogramoid']));
				$r_lp_hotel = $stmt->fetchAll(PDO::FETCH_ASSOC);
				foreach($r_lp_hotel as $hotel){
			?>
                <li lph="<?=$hotel['loyaltyprogramhoteloid']?>">
                    <div class="header">
                        <div class="row">
                            <div class="col-md-6"><?=$hotel['hotelname']?></div>
                            <div class="col-md-6 text-right">
                            	<?php if($campaign['chainoid'] != 0){ ?>
                                <button type="button" class="btn btn-warning btn-sm" data-lph="<?=$hotel['loyaltyprogramhoteloid']?>"><i class="fa fa-trash-o"></i> Remove Hotel</button>
                                <?php } ?>
                                <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#promotionListModal" data-hotel="<?=$hotel['hoteloid']?>"  data-lph="<?=$hotel['loyaltyprogramhoteloid']?>">Select Promotion</button>
                            </div>
                        </div>
                    </div>
                    <div class="content" id="assigned-promotion">
						<?php
                            $stmt = $db->prepare("select p.name, lpp.loyaltyprogrampromotionoid from promotion p inner join loyaltyprogrampromotion lpp using (promotionoid) where lpp.loyaltyprogramhoteloid = :a");
                            $stmt->execute(array(':a' => $hotel['loyaltyprogramhoteloid']));
                            $r_lp_promotion = $stmt->fetchAll(PDO::FETCH_ASSOC);
                            foreach($r_lp_promotion as $promotion){
                        ?>
							<div class="col-md-6"><button type="button" class="btn btn-danger btn-xs" id="remove-promotion" data-lpp="<?=$promotion['loyaltyprogrampromotionoid']?>" data-toggle="modal" data-target="#confirmDelPromotion" ><i class="fa fa-close"></i></button> <?=$promotion['name']?></div>
                        <?php
							}
						?>
                    </div>
                </li>
            <?php
				}
			?>
            </ul>
        </div>
				<br>
				<div class="row">
						<div class="form-group col-md-12 text-right">
								<button type="button" class="small-button red cancel">Cancel</button>
								<button type="button" class="small-button blue submit-edit">Save Change</button>
						</div>
				</div>
    </div>

    </form>
</section>

<!-- Modal -->
<div class="modal fade" id="hotelListModal" tabindex="-1" role="dialog" aria-labelledby="hotelListModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus-circle"></i> Add New Hotel</h4>
      </div>
      <div class="modal-body">
        <form method="post" id="form-assign-hotel" class="form-inline" enctype="multipart/form-data" action="#">
          <input type="hidden" name="chain" value="<?=$campaign['chainoid']?>">
          <div class="form-group"><label>Hotel Name</label></div>
          <div class="form-group"><input type="text" class="form-control" name="name" placeholder="Hotel Name"></div>
          <div class="form-group"><button type="button" class="btn btn-info" id="find">Find</button></div>

          <div class="row">
            <div id="list"></div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="assign-hotel">Assign Selected Hotel</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="promotionListModal" tabindex="-1" role="dialog" aria-labelledby="promotionListModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus-circle"></i> Add Promotion</h4>
      </div>
      <div class="modal-body">
        <form method="post" id="form-assign-promotion" class="form-inline" enctype="multipart/form-data" action="#">
          <input type="hidden" name="ho" value="">
          <input type="hidden" name="lph" value="">
          <div class="form-group"><label>Promotion Name</label></div>
          <div class="form-group"><input type="text" class="form-control" name="name" placeholder="Promotion Name"></div>
          <div class="form-group"><button type="button" class="btn btn-info" id="find">Find</button></div>

          <div class="row">
            <div id="list"></div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="assign-promotion">Assign Selected Promotion</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="membershipModal" tabindex="-1" role="dialog" aria-labelledby="membershipModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus-circle"></i> Add Membership</h4>
      </div>
      <div class="modal-body">
        <form method="post" id="form-membership" enctype="multipart/form-data" action="#">
        <div class="row">
          <div class="form-group col-md-12">
			<label>Membership</label>
            <input type="text" name="name" class="form-control" placeholder="Membership Type" required="required" />
          </div>
					<div class="form-group col-md-12">
			<label>Point</label>
            	<div class="input-group"><div class="input-group-addon">from</div><input type="number" class="form-control" name="startpoint" value="0" required><div class="input-group-addon">to</div><input type="number" class="form-control" name="endpoint" value="100" required></div>
          </div>
          <div class="form-group col-md-12">
			<label>Description</label>
            <textarea name="description" class="form-control"></textarea>
          </div>
          <div class="form-group col-md-3">
			<label>Discount</label>
			<div class="input-group"><input type="number" class="form-control" name="discount" value="10" required><div class="input-group-addon">%</div></div>
          </div>
        </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="save">Save</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="confirmDelPromotion" tabindex="-1" role="dialog" aria-labelledby="confirmDelPromotion">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus-trash"></i> Unsign Promotion</h4>
      </div>
      <div class="modal-body">
        <form method="post" id="form-remove-promotion" class="form-inline" enctype="multipart/form-data" action="#">
          Are you sure want to remove this promotion from loyalty program?
					<input type="hidden" name="lpp">
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
        <button type="button" class="btn btn-danger" id="remove-lpp">Yes</button>
      </div>
    </div>
  </div>
</div>
