<script type="text/javascript">
$(function(){
	
	$(document).ready(function(){ 
		getLoadData();
	});
	
	$('body').on('click','#paging button', function(e) {
		page	= $(this).attr('page');
		pagebox = $('form#data-input').children('input[name=page]');
		pagebox.val(page);
		getLoadData(); 
	});
	
	function getLoadData(){
		$.ajax({
			url: "<?php echo"$base_url"; ?>/includes/crm/member/data.php",
			type: 'post',
			data: $('form#data-input').serialize(),
			success: function(data) {
				$("#data-box").html(data)
			}
		});
	}

	$(document).on('click', ".change-published", function(){
		var id = $(this).attr("id");
		var pub = $(this).attr("current-publishedoid");
		$.ajax({
			url: "<?php echo"$base_url"; ?>/includes/crm/member/changestatus.php",
			type: 'post',
			data: {"id":id, "pub":pub},
			success: function(data) {
				getLoadData();
			}
		});
	});

	$(document).on('click', ".remove-member", function(){
		var id = $(this).attr("id");
		if(confirm("Are you sure want to remove this member?")){
			$.ajax({
				url: "<?php echo"$base_url"; ?>/includes/crm/member/removemember.php",
				type: 'post',
				data: {"id":id},
				success: function(data) {
					getLoadData();
				}
			});
		}
	});

	$(document).on('click', ".edit-member", function(){
		var id =  $(this).attr("id");
		location.href = "<?php echo"$base_url"; ?>/crm/member/detail/?for="+id;
	});
});
</script>