<?php
	error_reporting(E_ALL & ~E_NOTICE);
	include("../../../conf/connection.php");
	
    $id = $_POST['id'];
    $pub = $_POST['pub'];

    if($pub == '0'){
        $npub = '1';
    }else{
        $npub = '0';
    }

    try {
		$stmt = $db->prepare("UPDATE `memberhotel` SET `publishedoid` = :a  WHERE memberhoteloid = :id");
        $stmt->execute(array(':id' => $id, ':a' => $npub));

	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}

?>