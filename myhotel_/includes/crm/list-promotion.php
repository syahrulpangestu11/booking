<?php
	error_reporting(E_ALL ^ E_NOTICE);
	include("../../conf/connection.php");

	$hoteloid = $_POST['ho'];
	$loyaltyprogramhoteloid = $_POST['lph'];

	$s_existed_promotion = "SELECT GROUP_CONCAT(promotionoid) as existed from `loyaltyprogrampromotion` where `loyaltyprogramhoteloid` = '".$loyaltyprogramhoteloid."'";
	$stmt = $db->query($s_existed_promotion);
	$r_existed_promotion = $stmt->fetch(PDO::FETCH_ASSOC);
	if(!empty($r_existed_promotion['existed'])){
		$exist_promotion = $r_existed_promotion['existed'];
	}else{
		$exist_promotion = '';
	}
?>

<div class="box-body">
    <table class="table table-striped">
        <thead>
            <tr><th>No.</th>
            <th>Promotion</th>
            <th>Periode</th>
            <th>Select</th>
        </tr></thead>
        <tbody>
        <?php
            // main query
            $batas=10;
			$halaman=isset($_REQUEST['page']) ? $_REQUEST['page'] : 0 ;
			if(empty($halaman)){
				$posisi=0; $halaman=1;
			}else{
				$posisi = ($halaman-1) * $batas;
			}
			$no=$posisi+1;
			$j = $halaman + ($halaman - 1) * ($batas - 1);

			$paging_query = " limit ".$posisi.", ".$batas;
			$num = $posisi;

            $main_query ="select p.promotionoid, p.name, p.salefrom, p.saleto from promotion p inner join hotel h using (hoteloid)";

            // query tambahan jika ada filter
            $filter = array();
            if(isset($_REQUEST['name']) and $_REQUEST['name']!=''){
                array_push($filter, 'p.promotionname like "%'.$_REQUEST['keyword'].'%"');
            }
			array_push($filter, 'p.hoteloid = "'.$hoteloid.'"');
			array_push($filter, 'p.publishedoid <> "3"');
			array_push($filter, 'p.publishedoid = "1"');

			if(!empty($exist_promotion)){
				array_push($filter, 'p.promotionoid not in ('.$exist_promotion.')');
			}
			// menggabungkan query utama dengan query filter
            if(count($filter) > 0){
                $combine_filter = implode(' and ',$filter);
                $syntax_list = $main_query.' where '.$combine_filter;
            }else{
                $syntax_list = $main_query;
            }

            $syntax_list_paging = $syntax_list." order by salefrom, saleto ".$paging_query;
            $stmt = $db->query($syntax_list_paging);
			$result_list = $stmt->fetchAll(PDO::FETCH_ASSOC);
            foreach($result_list as $list){
                $num++;
        ?>
            <tr>
                <td><?php echo $num; ?></td>
                <td><?php echo $list['name']; ?></td>
                <td><?php echo date('d/M/Y', strtotime($list['salefrom']))." - ".date('d/M/Y', strtotime($list['saleto'])); ?></td>
                <td class="text-center"><input type="checkbox" name="promotion[]" value="<?php echo $list['promotionoid']; ?>"></td>
            </tr>
        <?php
            }
        ?>
        </tbody>
    </table>
    <?php require_once('paging/post-paging.php'); ?>
</div>
