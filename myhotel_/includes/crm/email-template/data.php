<button type="button" class="small-button blue add-button">Create New Email Template</button>
<table class="table table-fill">
    <tr align="center">
        <td>Email Type</td>
        <td>Name</td>
        <td>&nbsp; </td>
    </tr>	
    <tr>
        <td>Booking</td>
        <td>Email 14 days before checkin</td>
        <td class="algn-right">
            <button type="button" class="pencil edit-button" hoid="1">Edit</button>
            <button type="button" class="trash delete-button" hoid="1">Delete</button>
        </td>
	</tr>	
    <tr>
        <td>Booking</td>
        <td>Email 2 days before check in</td>
        <td class="algn-right">
            <button type="button" class="pencil edit-button" hoid="1">Edit</button>
            <button type="button" class="trash delete-button" hoid="1">Delete</button>
        </td>
	</tr>	
    <tr>
        <td>Booking</td>
        <td>Email during stay</td>
        <td class="algn-right">
            <button type="button" class="pencil edit-button" hoid="1">Edit</button>
            <button type="button" class="trash delete-button" hoid="1">Delete</button>
        </td>
	</tr>	
    <tr>
        <td>Booking</td>
        <td>Email 1 day after check out</td>
        <td class="algn-right">
            <button type="button" class="pencil edit-button" hoid="1">Edit</button>
            <button type="button" class="trash delete-button" hoid="1">Delete</button>
        </td>
	</tr>	
    <tr>
        <td>Occasional</td>
        <td>Email birthday</td>
        <td class="algn-right">
            <button type="button" class="pencil edit-button" hoid="1">Edit</button>
            <button type="button" class="trash delete-button" hoid="1">Delete</button>
        </td>
	</tr>	
</table>
