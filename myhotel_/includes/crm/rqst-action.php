<?php
try {

session_start();
error_reporting(E_ALL ^ E_NOTICE);
include("../../conf/connection.php");

$currenttime = date('Y-m-d H:i:s');

switch($_POST['request']){
    case "new-program" :
	  $startdate = date('Y-m-d', strtotime($_POST['startdate']));
		$enddate = date('Y-m-d', strtotime($_POST['enddate']));
		$description = (!empty($_POST['description']) and isset($_POST['description'])) ? $_POST['description'] : "";

    if(!empty($_SESSION['_hotel'])){
      $hoteloid = $_SESSION['_hotel'];
      $chainoid = 0;
    }else{
      $stmt = $db->query("select oid from userassign where type = 'chainoid' and useroid = '".$_SESSION['_oid']."' limit 1");
      $result = $stmt->fetch(PDO::FETCH_ASSOC);
      $hoteloid = 0;
      $chainoid = $result['oid'];
    }

		$stmt = $db->prepare("INSERT INTO loyaltyprogram (name, description, publishedoid, hoteloid, chainoid, startdate, enddate, conversionpoint, created, createdby, updated, updatedby, `conversionamount`, `earnpointmethod`) VALUES (:a, :b, :c, :d1, :d2, :e, :f, :g, :h, :i, :j, :k, :l, :m)");
		$stmt->execute(array(':a' => $_POST['name'], ':b' => $description, ':c' => $_POST['published'], ':d1' => $hoteloid, ':d2' => $chainoid, ':e' => $startdate, ':f' => $enddate, ':g' => $_POST['conversionpoint'], ':h' => $currenttime, ':i' => $_SESSION['_user'], ':j' => $currenttime, ':k' => $_SESSION['_user'], ':l' => $_POST['conversionamount'], ':m' => $_POST['earnpointmethod']));
		$loyaltyprogramoid = $db->lastInsertId();

    if(!empty($hoteloid)){
  		$stmt = $db->prepare("INSERT INTO loyaltyprogramhotel (loyaltyprogramoid, hoteloid) VALUES (:a, :b)");
  		$stmt->execute(array(':a' => $loyaltyprogramoid, ':b' => $hoteloid));
    }

    $_SESSION['_loyaltyprogram'] = $loyaltyprogramoid;

		echo 'success';

	break;

    case "update-program" :
	  $loyaltyprogramoid = $_POST['loyaltyprogram'];
	  $startdate = date('Y-m-d', strtotime($_POST['startdate']));
		$enddate = date('Y-m-d', strtotime($_POST['enddate']));
		$description = (!empty($_POST['description']) and isset($_POST['description'])) ? $_POST['description'] : "";

		$stmt = $db->prepare("UPDATE loyaltyprogram SET name = :a, description = :b, publishedoid = :c, startdate = :e, enddate = :f, conversionpoint = :g, updated = :h, updatedby = :i, conversionamount = :j, earnpointmethod = :k WHERE loyaltyprogramoid = :id");
		$stmt->execute(array(':a' => $_POST['name'], ':b' => $description, ':c' => $_POST['published'], ':e' => $startdate, ':f' => $enddate, ':g' => $_POST['conversionpoint'], ':h' => $currenttime, ':i' => $_SESSION['_user'], ':id' => $loyaltyprogramoid, ':j' => $_POST['conversionamount'], ':k' => $_POST['earnpointmethod']));

		if(count($_POST['membershipoid']) > 0){
			$current_membership = implode("','", $_POST['membershipoid']);
			$stmt = $db->query("delete from loyaltyprogrammembership where loyaltyprogrammembershipoid not in ('".$current_membership."') and loyaltyprogramoid = '".$loyaltyprogramoid."'");

			foreach($_POST['membershipoid'] as $key =>  $value){
				$stmt = $db->prepare("UPDATE loyaltyprogrammembership SET name = :a, description = :b, discount = :c, startpoint = :d, endpoint = :e WHERE loyaltyprogrammembershipoid = :id");
				$stmt->execute(array(':a' => $_POST['cm-name'][$key], ':b' => $_POST['cm-description'][$key], ':c' => $_POST['cm-discount'][$key], ':d' => $_POST['cm-startpoint'][$key], ':e' => $_POST['cm-endpoint'][$key], ':id' => $value));
			}
		}

		if(count($_POST['m-name']) > 0){
			foreach($_POST['m-name'] as $key =>  $value){
				$stmt = $db->prepare("INSERT INTO loyaltyprogrammembership (loyaltyprogramoid, name, description, discount, startpoint, endpoint) VALUES (:id, :a, :b, :c, :d, :e)");
				$stmt->execute(array(':a' => $value, ':b' => $_POST['m-description'][$key], ':c' => $_POST['m-discount'][$key], ':d' => $_POST['m-startpoint'][$key], ':e' => $_POST['m-endpoint'][$key], ':id' => $loyaltyprogramoid));
			}
		}

		echo 'success';
	break;

	/*
	* ASSIGN HOTEL
	*/

	case "assign-hotel" :
		$loyaltyprogramoid = $_POST['loyaltyprogram'];
		if(count($_POST['hotel']) > 0){
			foreach($_POST['hotel'] as $key => $hoteloid){
				$stmt = $db->prepare("INSERT INTO loyaltyprogramhotel (loyaltyprogramoid, hoteloid) VALUES (:a, :b)");
				$stmt->execute(array(':a' => $loyaltyprogramoid, ':b' => $hoteloid));
				$loyaltyprogramhoteloid = $db->lastInsertId();

				$stmt = $db->prepare("select hoteloid, hotelname from hotel where hoteloid = :a");
				$stmt->execute(array(':a' => $hoteloid));
				$hotel = $stmt->fetch(PDO::FETCH_ASSOC);
				?>
                 <li lph="<?=$loyaltyprogramhoteloid?>">
                    <div class="header">
                        <div class="row">
                            <div class="col-md-6"><?=$hotel['hotelname']?></div>
                            <div class="col-md-6 text-right">
                                <button type="button" class="btn btn-warning btn-sm" data-lph="<?=$loyaltyprogramhoteloid?>"><i class="fa fa-trash-o"></i> Remove Hotel</button>
                                <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#promotionListModal" data-hotel="<?=$hotel['hoteloid']?>"  data-lph="<?=$loyaltyprogramhoteloid?>">Select Promotion</button>
                            </div>
                        </div>
                    </div>
                    <div class="content" id="assigned-promotion">
                    </div>
                </li>
                <?php
			}
		}

	break;

	/*
	* ASSIGN PROMOTION
	*/

	case "assign-promotion" :
		$loyaltyprogramhoteloid = $_POST['loyaltyprogramhotel'];
		if(count($_POST['promotion']) > 0){
			foreach($_POST['promotion'] as $key => $promotionoid){
				$stmt = $db->prepare("INSERT INTO loyaltyprogrampromotion (loyaltyprogramhoteloid, promotionoid) VALUES (:a, :b)");
				$stmt->execute(array(':a' => $loyaltyprogramhoteloid, ':b' => $promotionoid));
				$loyaltyprogrampromotionoid = $db->lastInsertId();

				$stmt = $db->prepare("select name from promotion where promotionoid = :a");
				$stmt->execute(array(':a' => $promotionoid));
				$promotion = $stmt->fetch(PDO::FETCH_ASSOC);
				?>
                 <div class="col-md-6"><button type="button" class="btn btn-danger btn-xs" id="remove-promotion" data-lpp="<?=$loyaltyprogrampromotionoid?>"><i class="fa fa-close"></i></button> <?=$promotion['name']?></div>
                <?php
			}
		}
	break;

	/*
	* UNASSIGN HOTEL
	*/

	case "unassign-hotel" :
		$loyaltyprogramhoteloid = $_POST['loyaltyprogramhotel'];

		$stmt = $db->prepare("DELETE FROM loyaltyprogramhotel where loyaltyprogramhoteloid = :a");
		$stmt->execute(array(':a' => $loyaltyprogramhoteloid));

		$stmt = $db->prepare("DELETE FROM loyaltyprogrampromotion where loyaltyprogramhoteloid = :a");
		$stmt->execute(array(':a' => $loyaltyprogramhoteloid));

		echo "success";
	break;

	/*
	* UNASSIGN HOTEL
	*/

	case "unassign-promotion" :
		$loyaltyprogrampromotionoid = $_POST['lpp'];

		$stmt = $db->prepare("DELETE FROM loyaltyprogrampromotion where loyaltyprogrampromotionoid = :a");
		$stmt->execute(array(':a' => $loyaltyprogrampromotionoid));

		echo "success";
		break;
}

}catch(Exception $ex) {
	echo "error";
	echo $ex->getMessage();
	die();
}
?>
