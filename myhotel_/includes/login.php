 <body class="login">
<div id="header">
    <div class="left">
    	<div class="main-logo"><a href="<?=str_replace('/myhotel', '', $base_url);?>"><img src="<?php echo $_profile['imglogo2']; ?>" alt="<?=$_profile['name'];?>"></a></div>
        <ul>
        	<li><a href="<?=str_replace('/myhotel', '', $base_url);?>"><?=$_profile['web'];?></a></li>
        </ul>
    </div>
    <div class="clear"></div>
</div>
<div id="container">
	<div class="relative">
    	<div class="login-title">
        	<span class="heading"><?php echo $_profile['name']; ?></span>
            <!-- <span>Complete solutions designed to maximize direct booking and increase your hotel's revenue.</span> -->
        </div>
        <div id="loginform">
            <h2>Log into your account</h2>
            <?php if(isset($message)){echo $message;}?><br>
            <form method="post" action="<?php echo"$base_url"; ?>/loginprocess">
                <div class="form-group">
                    <input type="text" class="textbox" name="user" placeholder="Username" autocomplete="off">
                </div>
                <div class="form-group">
                    <input type="password" class="textbox" name="pass" placeholder="Password" autocomplete="off">
                </div>
                <div class="form-group">
                    <label class="checkbox">
                        <input name="remember" type="checkbox" value="Remember Me">Remember Me
                    </label>
                </div>
                <div class="form-group">
                    <input type="submit" class="bt-submit" value="Login">
                </div>
                <div class="form-group font-small">
                    <a href="<?php echo"$base_url"; ?>/reset">I forgot my password</a>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
	$(function() {
		$("#loginform form").validate({
			rules: {
			   user: "required", pass: "required"
			}
		});
	});
</script>
</body>
