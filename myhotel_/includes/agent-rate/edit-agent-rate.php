<?php
	$start = date("d F Y");
	$end = date("d F Y",  strtotime(date("Y-m-d")." +1 year" ));
	$daylist = array("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat");
	$agenthoteloid = $_GET['aho'];

	try {
		$stmt = $db->query("select a.*, ah.agenthoteloid from agenthotel ah inner join agent a USING (agentoid) where ah.agenthoteloid = '".$agenthoteloid."'");
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
			$r_hotel = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_hotel as $row){
				$agentoid = $row['agentoid'];
				$agenthoteloid = $row['agenthoteloid'];
				$agentname = $row['agentname'];
				$cityname = $row['cityname']; $statename = $row['statename'];
				$email = $row['email'];
				$phone = $row['phone'];
				$status = $row['status'];
			}
		}else{
			echo "No Result";
			die();
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}
?>
<section class="content-header">
    <h1>
        Agent Rate - <?php echo $agentname; ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i>  Settings</a></li>
        <li>Agent Rate</li>
        <li class="active">Edit</li>
    </ol>
</section>
<section class="content">
    <form class="form-box" method="post" enctype="multipart/form-data" id="data-input" action="#">
    <input type="hidden" name="rt" value="<?php echo $roomoid; ?>" />

		<?php
		$s_room = "select ro.*, r.roomoid, r.name as masterroom from roomoffer ro inner join published p using (publishedoid) inner join room r using (roomoid) inner join offertype using (offertypeoid) where r.hoteloid = '".$hoteloid."' and p.publishedoid not in(3)";
		$stmt = $db->query($s_room);
		$q_room = $stmt->fetchAll(PDO::FETCH_ASSOC);

		foreach($q_room as $room){
		?>
		<div class="row">
        <div class="box box-form">
			<div class="box-header with-border">
          		<h1>Room Type : <?=$room['name'];?></h1>
        	</div>

            <div class="box-body">

							<table class="table table-fill table-fill-centered">
									<tr>
											<td>Rate Name</td>
											<td>Start Date</td>
											<td>End Date</td>
											<td>Rate</td>
											<td>Surcharge</td>
											<td>Priority</td>
											<td>Action</td>
									</tr>
									<tr>
                                    <form>
											<td><input type="text" class="medium" name="note"/></td>
											<td><input type="text" class="medium startdate" name="startdate"/></td>
											<td><input type="text" class="medium enddate" name="enddate"/></td>
											<td><input type="text" class="medium" name="rate"/></td>
											<td><input type="text" class="medium" name="surcharge"/></td>
											<td><input type="text" class="smaller" name="priority" value="5"/></td>
											<td><button type="button" class="small-button blue add" ro="<?=$room['roomofferoid'];?>" ah="<?=$agenthoteloid?>">Add</button></td>
									</form>
                                    </tr>
								<?php
								try {
									$stmt = $db->query("select * from agenthotelrate where agenthoteloid = '".$agenthoteloid."' and roomofferoid = '".$room['roomofferoid']."' order by startdate");
									$r_rate = $stmt->fetchAll(PDO::FETCH_ASSOC);
									foreach($r_rate as $row){
										
										$row['startdate'] = date('d F Y',strtotime($row['startdate']));
										$row['enddate'] = date('d F Y',strtotime($row['enddate']));

										echo"
										<tr>
										<form>
											<td><input type='text' class='medium' name='note' value='".$row['note']."'></td>
											<td>
												<input type='hidden' name='id[]' value='".$row['agenthotelrateoid']."'>
												<input type='text' class='medium startdate' name='startdate' value='".$row['startdate']."'>
											</td>
											<td><input type='text' class='medium enddate' name='enddate' value='".$row['enddate']."'></td>
											<td><input type='text' class='medium' name='rate' value='".floor($row['rate'])."'></td>
											<td><input type='text' class='medium' name='surcharge' value='".floor($row['surcharge'])."'></td>
											<td><input type='text' class='smaller' name='priority' value='".$row['priority']."'></td>
											<td><button type='button' class='small-button blue edit' ahr='".$row['agenthotelrateoid']."'><i class='fa fa-save'></i></button></td>
										</form>
										</tr>
										";

									}
								}catch(PDOException $ex) {
									echo "Invalid Query";
									die();
								}
								?>
								</table>
            </div><!-- /.box-body -->
       </div>
    </div>
		<?php } ?>
<a href="<?php echo $base_url; ?>/agent"><button type="button" class="small-button blue">Back to Agents</button></a>
    </form>
</section>
