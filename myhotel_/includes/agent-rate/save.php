<?php
try {
	include("../ajax-include-file.php");
	
	
$note = $_POST['note'];
$startdate = date('Y-m-d', strtotime($_POST['startdate']));
$enddate = date('Y-m-d', strtotime($_POST['enddate']));
$rate = $_POST['rate'];
$surcharge = $_POST['surcharge'];
$priority = $_POST['priority'];
$request = $_POST['request'];

if($request == "save"){
	$agenthoteloid = $_POST['ah'];
	$roomofferoid = $_POST['ro'];
	
	$stmt = $db->prepare("insert into `agenthotelrate` (`agenthoteloid`, `roomofferoid`, `startdate`, `enddate`, `rate`, `surcharge`, `priority`, `note`) value (:a, :b, :c, :d, :e, :f, :g, :h)");
	$stmt->execute(array(':a' => $agenthoteloid, ':b' => $roomofferoid, ':c' => $startdate, ':d' => $enddate, ':e' => $rate, ':f' => $surcharge, ':g' => $priority, ':h' => $note));
}else{
	$agenthotelrateoid = $_POST['ahr'];
	
	$stmt = $db->prepare("update `agenthotelrate` set `startdate` = :a, `enddate` = :b, `rate` =:c, `surcharge` =:d, `priority` =:e, `note` =:f where `agenthotelrateoid` = :id");
	$stmt->execute(array(':a' => $startdate, ':b' => $enddate, ':c' => $rate, ':d' => $surcharge, ':e' => $priority, ':f' => $note, ':id' => $agenthotelrateoid));
}

echo "success";

}catch(PDOException $ex) {
	echo "Invalid Query";
	die();
}

?>