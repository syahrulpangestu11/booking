<?php
	include("../../conf/connection.php");

	$taxoid = $_POST['taxoid'];
	$startdate = date("Y-m-d", strtotime($_POST['startdate']));
	$enddate = date("Y-m-d", strtotime($_POST['enddate']));
	$taxtype = $_POST['taxtype'];
	$chargetype = $_POST['chargetype'];
	if(isset($_POST['fee'])){ $fee = $_POST['fee']; }else{ $fee = "n"; }
	$taxpercent = $_POST['taxpercent'];
	$published = $_POST['published'];
	
	try {
		$stmt = $db->prepare("UPDATE tax SET startdate=:a , enddate=:b , taxtypeoid=:c , chargetypeoid=:d , fee=:e , taxpercent=:f , publishedoid=:g WHERE taxoid=:to");
		$stmt->execute(array(':a' => $startdate, ':b' => $enddate, ':c' => $taxtype, ':d' => $chargetype, ':e' => $fee , ':f' => $taxpercent , ':g' => $published , ':to' => $taxoid));
		echo "1";
	}catch(PDOException $ex) {
		echo "0";
		die();
	}
?>