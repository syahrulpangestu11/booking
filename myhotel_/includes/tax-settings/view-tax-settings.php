<?php
	if(empty($_REQUEST['rt']) and empty($_REQUEST['sdate']) and empty($_REQUEST['edate'])){
		$start = date("d F Y");
		$end = date("d F Y",  strtotime(date("Y-m-d")." +1 month" ));
	}else{
		$start = date("d F Y", strtotime($_REQUEST['sdate']));
		$end = date("d F Y", strtotime($_REQUEST['edate']));
		$roomtype = $_REQUEST['rt'];
	}
?>
<section class="content-header">
    <h1>
        Tax Settings
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Settings</a></li>
        <li class="active">Tax Settings</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="box">
            <div class="box-body">
                <div class="form-group">
                <form method="get" action="#" id="data-input">
                    <label>Date Range From </label> &nbsp;
                    <input type="text" name="sdate" id="startdate" placeholder="" value="<?php echo $start; ?>"> &nbsp;&nbsp;
                    <label>To </label> &nbsp;
                    <input type="text" name="edate" id="enddate" placeholder="" value="<?php echo $end; ?>"> &nbsp;&nbsp;                    
                    <button type="button" class="pure-button find-button">Find</button>
                </form>
                </div>
            </div><!-- /.box-body -->
       </div>
    </div>
    
    <div class="row">
        <div class="box">
            <div id="data-box" class="box-body">
            
            <div class="side-right algn-right">
                <button class="pure-button add-button" type="button" rt="<?php echo $roomoid; ?>">Add Tax Setting</button>
            </div>

            <table class="table table-bordered">
                <tr>
                    <td>Stay Date from</td>
                    <td>to</td>
                    <td>Tax Type</td>
                    <td>Charge Type</td>
                    <td>Fee</td>
                    <td>Tax Value</td>
                    <td>Published</td>
                    <td>&nbsp;</td>
                </tr>
                <?php
				$stmt = $db->query("select t.*, tt.name from tax t inner join taxtype tt using (taxtypeoid) inner join hotel h using (hoteloid) where h.hoteloid = '".$hoteloid."'");
				$r_tax = $stmt->fetchAll(PDO::FETCH_ASSOC);
				foreach($r_tax as $row){
					$show_startdate = date("d F Y", strtotime($row['startdate']));
					$show_enddate = date("d F Y", strtotime($row['enddate']));
                ?>
                <tr>
                    <td><?php echo $show_startdate; ?></td>
                    <td><?php echo $show_enddate; ?></td>
                    <td class="algn-center"><?php echo $row['name']; ?></td>
                    <td class="algn-center">Per Booking</td>
                    <td class="algn-center">
					<?php if($row['fee'] == "y"){ $checked = "checked"; }else{ $checked = "";  } ?>
                    <input type="checkbox" name="fee" disabled="disabled" value="y" <?php echo $checked; ?> />
            		</td>
                    <td class="algn-center"><input type="text" value="<?php echo $row['taxpercent']; ?>" /></td>
                    <td class="algn-center">
					<?php if($row['publishedoid'] == "1"){ $checked = "checked"; }else{ $checked = "";  } ?>
                    <input type="checkbox" name="published" disabled="disabled" value="1" <?php echo $checked; ?> />
            		</td>
                    <td class="algn-center">
                        <button type="button" class="action-button edit-button" to="<?php echo $row['taxoid']; ?><?php echo $row['roomrateoid']; ?>"></button>
                        <button type="button" class="action-button delete-button" to="<?php echo $row['taxoid']; ?>"></button>
                    </td>
                </tr>	
                <?php
                }
                ?>
            </table>            
            </div><!-- /.box-body -->
       </div>
    </div>

</section>
