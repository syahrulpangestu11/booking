<?php
	if(empty($_REQUEST['rt']) and empty($_REQUEST['startdate']) and empty($_REQUEST['enddate'])){
		$start = date("d F Y");
		$end = date("d F Y",  strtotime(date("Y-m-d")." +1 year" ));
	}else{
		$start = date("d F Y", strtotime($_REQUEST['sdate']));
		$end = date("d F Y", strtotime($_REQUEST['edate']));
		$roomoffer = $_REQUEST['rt'];
		$channeltype = $_REQUEST['ch'];
	}
	$daylist = array("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat");
	
	include('js.php');
?>
<section class="content-header">
    <h1>
        Surcharge
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i>  Allotments &amp; Rates </a></li>
        <li class="active">Surcharge</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="box box-form">
            <div class="box-body">
                <div class="form-group">
                <form method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/surcharge/">
                    <input type="hidden" name="ho" value="<?php echo $hoteloid; ?>" />
                    <table>
                        <tr>
                            <td>
                                <label>Room Type</label> &nbsp;
                                <select name="rt" class="input-select">
                                <?php
                                    try {
                                        $stmt = $db->query("select r.roomoid, r.name, x.offernum from room r inner join hotel h using (hoteloid) left join (select count(roomofferoid) as offernum, ro.roomoid from roomoffer ro group by ro.roomoid) as x on x.roomoid = r.roomoid where h.hoteloid = '".$hoteloid."' and x.offernum > 0 and r.publishedoid = '1'");
                                        $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                        foreach($r_room as $row){
                                            echo"<optgroup label = '".$row['name']."'>";
                                            try {
                                                $stmt = $db->query("select ro.* from roomoffer ro inner join published p using (publishedoid) inner join room r using (roomoid) inner join offertype using (offertypeoid) where r.roomoid = '".$row['roomoid']."' and p.publishedoid = '1'");
                                                $r_offer = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                                foreach($r_offer as $row1){
                                                    if($row1['roomofferoid'] == $roomoffer){ $selected = 'selected = "selected"';  }else{ $selected = ''; }
                                                   echo"<option value='".$row1['roomofferoid']."' ".$selected.">  ".$row1['name']."</option>";
                                                }
                                            }catch(PDOException $ex) {
                                                echo "Invalid Query";
                                                die();
                                            }     
                                            echo"</optgroup>";                       
                                        }
                                    }catch(PDOException $ex) {
                                        echo "Invalid Query";
                                        die();
                                    }
                                
                                ?>   
                                </select>
                                &nbsp;&nbsp;
                                <label>Channel</label> &nbsp;&nbsp;
                                <select name="ch">
                                <?php
                                    try {
                                        $stmt = $db->query("select * from channel");
                                        $r_plan = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                        foreach($r_plan as $row){
                                            if($row['channeloid'] == $channeltype){ $selected = 'selected = "selected"';  }else{ $selected = ''; }
                                            echo"<option value='".$row['channeloid']."' ".$selected.">".$row['name']."</option>";
                                        }
                                    }catch(PDOException $ex) {
                                        echo "Invalid Query";
                                        die();
                                    }
                                
                                ?>        	
                                </select> &nbsp;
                            </td> 
                        </tr>
                        <tr>
                            <td>
                                <label>Date Range From</label> &nbsp;&nbsp;
                                <input type="text" name="sdate" id="startdate" placeholder="" value="<?php echo $start; ?>"> &nbsp;&nbsp;
                                <label>To</label> &nbsp;&nbsp;
                                <input type="text" name="edate" id="enddate" placeholder="" value="<?php echo $end; ?>"> &nbsp;&nbsp;
                                <button type="submit" class="small-button blue">Search</button>
                            </td> 
                        </tr>
                    </table>
                </form>
			</div>
        </div><!-- /.box-body -->
   </div>
</div>
    
    <form method="post" enctype="multipart/form-data" id="form-add" action="#">
    <div class="row">
        <div class="box">
			<table class="table table-fill table-fill-centered">
                <tr>
                    <td>Stay Date From</td>
                    <td>To</td>
                    <td>Applicable Day of Week</td>
                    <td>Surcharge Name</td>
                    <td>Is Commissionable</td>
                    <td>Surcharge Value</td>
                    <td>Currency</td>
                </tr>
                <tr>
                    <td class="algn-center"><input type="text"class="medium" id="startcalendar" name="startdate"></td>
                    <td class="algn-center"><input type="text"class="medium" id="endcalendar" name="enddate"></td>
                    <td>
						<?php
                            foreach($daylist as $day){
                                echo"<input type='checkbox' name='apply[]' value='".$day."' checked>".$day."&nbsp;&nbsp;";
                            }
                        ?>
                    </td>
                    <td><input type="text"class="medium" name="name"></td>
                    <td><input type="checkbox" name="commisionable" value="y" /></td>
                    <td><input type="text" name="value" /></td>
                    <td>&nbsp;</td>
                </tr>
            </table> 
            <button type="button" class="small-button blue add">Add</button>  
          
       </div>
    </div>
    
    <div class="row">
        <div class="box">
            <div id="data-box" class="box-body">
            </div>
        </div>
    </div>
    </form>
    
</section>
