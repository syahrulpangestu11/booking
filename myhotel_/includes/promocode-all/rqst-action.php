<?php
// try {

session_start();
error_reporting(E_ALL ^ E_NOTICE);
include("../../conf/connection.php");

$currenttime = date('Y-m-d H:i:s');

switch($_POST['request']){
    case "new-program" :
	  $startdate = date('Y-m-d', strtotime($_POST['startdate']));
		$enddate = date('Y-m-d', strtotime($_POST['enddate']));
		$description = (!empty($_POST['description']) and isset($_POST['description'])) ? $_POST['description'] : "";

    if(!empty($_SESSION['_hotel'])){
      $hoteloid = $_SESSION['_hotel'];
      $chainoid = 0;
    }else{
      $stmt = $db->query("select oid from userassign where type = 'chainoid' and useroid = '".$_SESSION['_oid']."' limit 1");
      $result = $stmt->fetch(PDO::FETCH_ASSOC);
      $hoteloid = 0;
      $chainoid = $result['oid'];
    }

		$stmt = $db->prepare("INSERT INTO loyaltyprogram (name, description, publishedoid, hoteloid, chainoid, startdate, enddate, conversionpoint, created, createdby, updated, updatedby, `conversionamount`, `earnpointmethod`) VALUES (:a, :b, :c, :d1, :d2, :e, :f, :g, :h, :i, :j, :k, :l, :m)");
		$stmt->execute(array(':a' => $_POST['name'], ':b' => $description, ':c' => $_POST['published'], ':d1' => $hoteloid, ':d2' => $chainoid, ':e' => $startdate, ':f' => $enddate, ':g' => $_POST['conversionpoint'], ':h' => $currenttime, ':i' => $_SESSION['_user'], ':j' => $currenttime, ':k' => $_SESSION['_user'], ':l' => $_POST['conversionamount'], ':m' => $_POST['earnpointmethod']));
		$loyaltyprogramoid = $db->lastInsertId();

    if(!empty($hoteloid)){
  		$stmt = $db->prepare("INSERT INTO loyaltyprogramhotel (loyaltyprogramoid, hoteloid) VALUES (:a, :b)");
  		$stmt->execute(array(':a' => $loyaltyprogramoid, ':b' => $hoteloid));
    }

    $_SESSION['_loyaltyprogram'] = $loyaltyprogramoid;

		echo 'success';

	break;

    case "update-program" :
	  $loyaltyprogramoid = $_POST['loyaltyprogram'];
	  $startdate = date('Y-m-d', strtotime($_POST['startdate']));
		$enddate = date('Y-m-d', strtotime($_POST['enddate']));
		$description = (!empty($_POST['description']) and isset($_POST['description'])) ? $_POST['description'] : "";

		$stmt = $db->prepare("UPDATE loyaltyprogram SET name = :a, description = :b, publishedoid = :c, startdate = :e, enddate = :f, conversionpoint = :g, updated = :h, updatedby = :i, conversionamount = :j, earnpointmethod = :k WHERE loyaltyprogramoid = :id");
		$stmt->execute(array(':a' => $_POST['name'], ':b' => $description, ':c' => $_POST['published'], ':e' => $startdate, ':f' => $enddate, ':g' => $_POST['conversionpoint'], ':h' => $currenttime, ':i' => $_SESSION['_user'], ':id' => $loyaltyprogramoid, ':j' => $_POST['conversionamount'], ':k' => $_POST['earnpointmethod']));

		if(count($_POST['membershipoid']) > 0){
			$current_membership = implode("','", $_POST['membershipoid']);
			$stmt = $db->query("delete from loyaltyprogrammembership where loyaltyprogrammembershipoid not in ('".$current_membership."') and loyaltyprogramoid = '".$loyaltyprogramoid."'");

			foreach($_POST['membershipoid'] as $key =>  $value){
				$stmt = $db->prepare("UPDATE loyaltyprogrammembership SET name = :a, description = :b, discount = :c, startpoint = :d, endpoint = :e WHERE loyaltyprogrammembershipoid = :id");
				$stmt->execute(array(':a' => $_POST['cm-name'][$key], ':b' => $_POST['cm-description'][$key], ':c' => $_POST['cm-discount'][$key], ':d' => $_POST['cm-startpoint'][$key], ':e' => $_POST['cm-endpoint'][$key], ':id' => $value));
			}
		}

		if(count($_POST['m-name']) > 0){
			foreach($_POST['m-name'] as $key =>  $value){
				$stmt = $db->prepare("INSERT INTO loyaltyprogrammembership (loyaltyprogramoid, name, description, discount, startpoint, endpoint) VALUES (:id, :a, :b, :c, :d, :e)");
				$stmt->execute(array(':a' => $value, ':b' => $_POST['m-description'][$key], ':c' => $_POST['m-discount'][$key], ':d' => $_POST['m-startpoint'][$key], ':e' => $_POST['m-endpoint'][$key], ':id' => $loyaltyprogramoid));
			}
		}

		echo 'success';
	break;

	/*
	* ASSIGN HOTEL
	*/

	case "assign-hotel" :
		$loyaltyprogramoid = $_POST['loyaltyprogram'];
		if(count($_POST['hotel']) > 0){
			foreach($_POST['hotel'] as $key => $hoteloid){
				// $stmt = $db->prepare("INSERT promocodeapplyall (hoteloid) VALUES (:a)");
				// $stmt->execute(array(':a' => $hoteloid));
				// $loyaltyprogramhoteloid = $db->lastInsertId();

				$stmt = $db->prepare("select hoteloid, hotelname from hotel where hoteloid = :a");
				$stmt->execute(array(':a' => $hoteloid));
				$hotel = $stmt->fetch(PDO::FETCH_ASSOC);
				?>
                 <li lph="<?=$hoteloid?>">
                    <div class="header">
                        <div class="row">
                            <div class="col-md-6"><?=$hotel['hotelname']?><input type="hidden" name="hoteloid[]" value="<?=$hotel['hoteloid']?>"></div>
                            <div class="col-md-6 text-right">
                                <button type="button" class="btn btn-warning btn-sm" data-lph="<?=$hoteloid?>"><i class="fa fa-trash-o"></i> Remove Hotel</button>
                                <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#promotionListModal" data-hotel="<?=$hoteloid?>" data-lph="<?=$hoteloid?>">Select Promotion</button>
                            </div>
                        </div>
                    </div>
                    <div class="content" id="assigned-promotion">
                    </div>
                </li>
                <?php
			}
		}

	break;

	/*
	* ASSIGN PROMOTION
	*/

	case "assign-promotion" :
		
		// echo $promotype;
		if($_POST['promotion-promotion']) {
		if(count($_POST['promotion-promotion']) > 0){

				$promocodeoid = $_POST['promocodeoid'];	
				$applydiscount = (isset($_POST['discountapply-promotion']) ? $_POST['discountapply-promotion'] : "n");
			   $applycommission = (isset($_POST['commissionapply-promotion']) ? $_POST['commissionapply-promotion'] : "n");
			   // echo $applydiscount; echo $applycommission;
			foreach($_POST['promotion-promotion'] as $key => $promotionoid){
				
				$stmt = $db->prepare("select promocodeapplyoid from promocodeapply where promocodeoid = :a and id = :b");
				$stmt->execute(array(':a' => $promocodeoid, ':b' => $promotionoid));
				// $stmt = $db->prepare("update promocodeapply set apply VALUES (:a)");
				
				$loyaltyprogramhoteloid = $db->lastInsertId();
				$row_count = $stmt->rowCount();
				if($row_count > 0){
					
					$rowpromo_found = $stmt->fetch(PDO::FETCH_ASSOC);
					
					$stmt = $db->prepare("update promocodeapply set applydiscount=:a, applycommission=:b where promocodeapplyoid=:c");
					$stmt->execute(array(':a' => $applydiscount, ':b' => $applycommission, ':c' => $rowpromo_found['promocodeapplyoid']));
					// array_push($existedapply, $rowpromo_found['promocodeapplyalloid']);
				}else{
					$promotype = $_POST['promotype-promotion'];
				$hoteloid = $_POST['hoteloid-promotion'];
				// echo $hoteloid;
					$s_stmt ="select name from promotion where promotionoid = '".$promotionoid."'";
					$stmt = $db->query($s_stmt);
						// echo $s_stmt;
						$promotion = $stmt->fetch(PDO::FETCH_ASSOC);?>
						<div class="col-md-6"><button type="button" class="btn btn-danger btn-xs" id="remove-promotion" data-lpp="<?=$promotionoid?>"><i class="fa fa-close"></i></button> <?=$promotion['name']?><input type="hidden" name="apply[]" value="<?=$promotionoid?>- <?=$promotype?>"><input type="hidden" name="discountapplys" value="<?=$applydiscount?>"><input type="hidden" name="commissionapplys" value="<?=$applycommission?>"></div>
			<?php }
				
			
				
			
					}
			}
		}else if($_POST['promotion-package'] ){
			if(count($_POST['promotion-package']) > 0){
			   
			   $promocodeoid = $_POST['promocodeoid'];	
			   $applydiscount = (isset($_POST['discountapply-package']) ? $_POST['discountapply-package'] : "n");
			   $applycommission = (isset($_POST['commissionapply-package']) ? $_POST['commissionapply-package'] : "n");
			   // echo $applydiscount; echo $applycommission; 
				foreach($_POST['promotion-package'] as $key => $promotionoid){
				$stmt = $db->prepare("select promocodeapplyoid from promocodeapply where promocodeoid = :a and id = :b");
				$stmt->execute(array(':a' => $promocodeoid, ':b' => $promotionoid));
				// $stmt = $db->prepare("update promocodeapply set apply VALUES (:a)");
				
				$loyaltyprogramhoteloid = $db->lastInsertId();
				$row_count = $stmt->rowCount();
				if($row_count > 0){
					
					$rowpromo_found = $stmt->fetch(PDO::FETCH_ASSOC);
					
					$stmt = $db->prepare("update promocodeapply set applydiscount=:a, applycommission=:b where promocodeapplyoid=:c");
					$stmt->execute(array(':a' => $applydiscount, ':b' => $applycommission, ':c' => $rowpromo_found['promocodeapplyoid']));
					// array_push($existedapply, $rowpromo_found['promocodeapplyalloid']);
				}else{
					$hoteloid = $_POST['hoteloid-package'];
					$promotype = $_POST['promotype-package'];
					$stmt = $db->prepare("select name, hoteloid from package where packageoid = :a");
						$stmt->execute(array(':a' => $promotionoid));
						$promotion = $stmt->fetch(PDO::FETCH_ASSOC);
			
				
				?>
                 <div class="col-md-6"><button type="button" class="btn btn-danger btn-xs" id="remove-promotion" data-lpp="<?=$promotionoid?>"><i class="fa fa-close"></i></button> <?=$promotion['name']?><input type="hidden" name="apply[]" value="<?=$promotionoid?>- <?=$promotype?>">
                 <input type="hidden" name="discountapplys" value="<?=$applydiscount?>"><input type="hidden" name="commissionapplys" value="<?=$applycommission?>"></div></div>
            <?php }
        }

			}

		}else if($_POST['promotion-flexiblerate'] ){ 
			$applybar = (isset($_POST['applybar']) ? $_POST['applybar'] : "n");$applybardiscount = (isset($_POST['discountapply-flexiblerate']) ? $_POST['discountapply-flexiblerate'] : "n");
			   $applybarcommission = (isset($_POST['commissionapply-flexiblerate']) ? $_POST['commissionapply-flexiblerate'] : "n");?>

			<div class="col-md-6"><button type="button" class="btn btn-danger btn-xs" id="remove-promotion"><i class="fa fa-close"></i></button> <?=$_POST['flexiblerate']?><input type="hidden" name="applybar" value="<?=$applybar?>"><input type="hidden" name="discountapply" value="<?=$applybardiscount?>"><input type="hidden" name="commissionapply" value="<?=$applybarcommission?>"></div>
		<?php }
	break;

	/*
	* UNASSIGN HOTEL
	*/

	case "unassign-hotel" :
		$loyaltyprogramhoteloid = $_POST['loyaltyprogramhotel'];

		$stmt = $db->prepare("DELETE FROM loyaltyprogramhotel where loyaltyprogramhoteloid = :a");
		$stmt->execute(array(':a' => $loyaltyprogramhoteloid));

		$stmt = $db->prepare("DELETE FROM loyaltyprogrampromotion where loyaltyprogramhoteloid = :a");
		$stmt->execute(array(':a' => $loyaltyprogramhoteloid));

		echo "success";
	break;

	/*
	* UNASSIGN HOTEL
	*/

	case "unassign-promotion" :
		$promotionoid = $_POST['lpp'];
		// echo($promotionoid);
		$stmt = $db->prepare("DELETE FROM promocodeapply where id = :a");
		$stmt->execute(array(':a' => $promotionoid));

		echo "success";
		break;
}

// }catch(Exception $ex) {
	// echo "error";
	// echo $ex->getMessage();
	die();
// }
?>
