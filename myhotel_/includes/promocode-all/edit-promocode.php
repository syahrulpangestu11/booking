<script type="text/javascript">
$(function() {
    $('textarea').trumbowyg({
        btns: ['viewHTML',
          '|', 'btnGrp-design',
          '|', 'link',
          '|', 'btnGrp-justify',
          '|', 'btnGrp-lists'],
        fullscreenable: false
    });
});
</script>
<style type="text/css">
            h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
                font-family: inherit;
                font-weight: 600;
                line-height: inherit;
                color: inherit;
                margin-bottom:10px!important;
            }
            .wrapper {
                position: inherit;
                overflow: hidden!important;
            }
            .left-side {
                padding-top: inherit;
            }
            .sidebar > .sidebar-menu li > a:hover {
                background-color: rgba(72, 115, 175, 0.26);
            }
            .sidebar .sidebar-menu .treeview-menu {
                background-color: rgb(14, 26, 43);
            }
            .sidebar .sidebar-menu .treeview-menu .treeview-menu-child {
                background-color: rgba(0, 0, 0, 0.5);
            }
            .sidebar > .sidebar-menu li.active > a {
                background-color: rgba(197, 45, 47, 0.55);
            }
            .sidebar > .sidebar-menu > li.treeview.active > a {
                background-color: inherit;
            }
            .sidebar > .sidebar-menu > li.treeview.active li.treeview-child.active > a {
                background-color: inherit;
            }
            .sidebar .sidebar-menu > li > a > .fa {
                width: 28px;
                font-size: 16px;
            }
            .sidebar-menu, .main-sidebar .user-panel, .sidebar-menu>li.header{
                white-space:normal!important;
            }
            .form-group input[type=text]{
                width:100%!important;
            }

  #accordion .header {background: #f4f8fb !important; border: 1px solid #bbb; color: #222; margin-top: 10px; font-weight: normal;}
  #accordion .header.ui-accordion-header-active,
  #accordion .header.ui-state-hover {background-color: #5d9cec !important; border-color: transparent; color: #fff;}
  #accordion .header .table-cell {display: table-cell; vertical-align: middle; padding: 0 10px;}
  #accordion .header .table-cell:nth-of-type(1) {width: 10%;}
  #accordion .header#accordion .header .table-cell:nth-of-type(2) {width: 30%;}
  #accordion .content{ min-height:0!important; }
  #accordion #assigned-promotion > div{ margin-bottom:5px; }

  #accordion .header .image .thumbnail {width: 100%;}

  #accordion .content {
    background: #F4F8FB !important; border: 1px solid #bbb !important;
    border-top: none;
  }

  .btn-xs{ padding : 1px 5px!important; }
        </style>
<?php
	$promocode = $_GET['pid'];
	try {
		$stmt = $db->query("select pc.* from promocode pc where promocode = '".$promocode."'");
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
			$pc = $stmt->fetch(PDO::FETCH_ASSOC);
            $promocodeoid = $pc['promocodeoid'];
            // echo($promocodeoid);
			$startdate = date("d F Y", strtotime($pc['startdate']));
			$enddate = date("d F Y", strtotime($pc['enddate']));
		}else{
			echo "No Result";
			die();
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}
?>
<script type="text/javascript">
$(function() {
	$('textarea').trumbowyg({
		btns: ['viewHTML',
		  '|', 'btnGrp-design',
		  '|', 'link',
		  '|', 'btnGrp-justify',
		  '|', 'btnGrp-lists'],
		fullscreenable: false
	});
});
</script>
<section class="content-header">
    <h1>
        Edit Promo Code
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i>  Allotments &amp; Rates </a></li>
        <li class="active">Promo Code</li>
    </ol>
</section>
<section class="content">
	<form class="form-box" method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/promocode-all/edit-process">
	<input type="hidden" name="promocode" value="<?php echo $pc['promocode']; ?>"/>
    <input type="hidden" name="promocodeoid" value="<?php echo $pc['promocodeoid']; ?>"/>
	<div class="row">
        <div class="box box-form">
            <h1>Promo Code</h1>
            <ul class="inline-half colored">
				<li>
                    <ul class="block">
                        <li><h3>DETAIL PROMO CODE</h3></li>
                        <li>
                            <span class="label"><b>Promo Code:</b></span>
                            <input type="text" class="medium" name="code" required="required" value="<?php echo $pc['promocode']; ?>">
                        </li>
                        <li>
                            <span class="label">Promo Code Name:</span>
                            <input type="text" class="long" name="name" value="<?php echo $pc['name']; ?>">
                        </li>
                        <li>
                            <h3>Description :<!----></h3>
                            <textarea name="description"><?php echo $pc['description']; ?></textarea>
                            <div class="clear"></div>
                        </li>
                        <div class="clear"></div>
                        <li>
                        	<div class="clear"></div>
                            <span class="label">Publish Promo Code :</span>
                            <select name="published">
                            <?php
                                try {
                                    $stmt = $db->query("select * from published where showthis = 'y'");
                                    $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                    foreach($r_room as $row){
										if($row['publishedoid'] == $pc['publishedoid']){ $selected = "selected"; }else{ $selected=""; }
                                        echo"<option value='".$row['publishedoid']."' ".$selected.">".$row['note']."</option>";
                                    }
                                }catch(PDOException $ex) {
                                    echo "Invalid Query";
                                    die();
                                }
                            ?>
                            </select>
                        </li>
                	</ul>
                    <?php if($_SESSION['_typeusr'] == '1'){?>
                    <ul class="block">
                        <li><h3>Allow Zero Transaction</h3></li>
                        <li>
                            <span class="label">Check to allow :</span>
                            <input type="checkbox" name="allowzerotrx" value="1" <?php if($pc['allowzerotrx'] == '1') echo 'checked=checked'; ?>>
                        </li>
                    </ul>
                    <?php }?>
                </li>
								<li>
                	<ul class="block">
                        <li><h3>PERIODE OF PROMO CODE</h3></li>
                        <li>
                            <span class="label">Start Date From:</span>
                            <input type="text"class="medium" id="startdate" name="startdate" required="required" value="<?php echo $startdate; ?>" autocomplete="off">
                        </li>
                        <li>
                            <span class="label">End Date To:</span>
                            <input type="text"class="medium" id="enddate" name="enddate" required="required" value="<?php echo $enddate; ?>" autocomplete="off">
                        </li>
                        <li><h3>DISCOUNT</h3></li>
                        <li>
                            <span class="label">Discount Type:</span>
                            <select name="discounttype" class="input-select">
                                <?php
                                	$codetype = array('discount percentage', 'discount amount');
                                    foreach($codetype as $value){
										if($value == $pc['discounttype']){ $selected = "selected"; $dataval=$pc['discount']; }else{ $selected=""; $dataval=""; }
                                ?>
                                    <option value="<?php echo $value; ?>" <?php echo $selected; ?> data="<?php echo $dataval; ?>"><?php echo $value; ?></option>
                                <?php
                                    }
                                ?>
                            </select>
                        </li>
                        <li id="discount-value"></li>
                        <li><h3>COMMISSION</h3></li>
                        <li>
                            <span class="label">Commission Type:</span>
                            <select name="commissiontype" class="input-select">
                                <?php
                                	$codetype = array('commission percentage', 'commission amount');
                                    foreach($codetype as $value){
										if($value == $pc['commissiontype']){ $selected = "selected"; $dataval=$pc['commission']; }else{ $selected=""; $dataval=""; }
                                ?>
                                    <option value="<?php echo $value; ?>" <?php echo $selected; ?> data="<?php echo $dataval; ?>"><?php echo $value; ?></option>
                                <?php
                                    }
                                ?>
                            </select>
                        </li>
                        <li id="commission-value"></li>
                        <li>
                            <span class="label"><b>PIC Name:</b></span>
                            <input type="text" class="medium" name="pic_name" value="<?php echo $pc['pic_name']; ?>">
                        </li>
                        <li>
                            <span class="label"><b>PIC Contact Number:</b></span>
                            <input type="text" class="medium" name="pic_number" value="<?php echo $pc['pic_number']; ?>">
                        </li>
                    </ul>

                </li>
            </ul>
            <div class="clear"></div>
        </div>
    </div>
    <div class="row">
    	<div class="box box-form" id="step-3">
        <div class="row">
            <div class="col-md-6"><h1><i class="fa fa-building"></i> Apply Campaign To Hotel</h1></div>
            <div class="col-md-6 text-right"><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#hotelListModal">Assign New Hotel</button></div>
        </div>
        <div class="row" id="assigned-hotel">
            <ul id="accordion">
            <?php
                $stmt = $db->prepare("select p.*, h.hotelname from promocode p inner join hotel h using (hoteloid) where p.promocode = :a");
                $stmt->execute(array(':a' => $promocode));
                $r_lp_hotel = $stmt->fetchAll(PDO::FETCH_ASSOC);
                foreach($r_lp_hotel as $hotel){
            ?>
                <li lph="<?=$hotel['hoteloid']?>">
                    <div class="header">
                        <div class="row">
                            <div class="col-md-6"><?=$hotel['hotelname']?></div>
                            <div class="col-md-6 text-right">
                                <?php if($campaign['chainoid'] != 0){ ?>
                                <button type="button" class="btn btn-warning btn-sm" data-lph="<?=$hotel['hoteloid']?>" data-pcoid="<?=$promocodeoid?>"><i class="fa fa-trash-o"></i> Remove Hotel</button>
                                <?php } ?>
                                <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#promotionListModal" data-hotel="<?=$hotel['hoteloid']?>" data-pcoid="<?=$promocodeoid?>" data-lph="<?=$hotel['hoteloid']?>">Select Promotion</button>
                            </div>
                        </div>
                    </div>
                    <div class="content" id="assigned-promotion">
                        <?php if($hotel['applybar'] == 'y'){?>
                            <div class="col-md-6"><button type="button" class="btn btn-danger btn-xs" id="remove-promotion" data-lpp="<?=$promotion['oid']?>" data-toggle="modal" data-target="#confirmDelPromotion" ><input type="hidden" name="applybar" value="<?=$hotel['applybar']?>"><input type="hidden" name="discountapply" value="<?=$hotel['applybardiscount']?>"><input type="hidden" name="commissionapply" value="<?=$hotel['applybarcommission']?>"><i class="fa fa-close"></i></button> Best Flexiblerate</div>
                        <?php }
                            $stmt = $db->prepare("select p.name, promotionoid as oid from promotion p inner join promocodeapply on p.promotionoid = promocodeapply.id where p.hoteloid = :a and promocodeoid = :b union select p.name, packageoid as oid from
                                package p inner join promocodeapply on p.packageoid = promocodeapply.id where p.hoteloid = :d and promocodeoid = :e");
                            $stmt->execute(array(':a' => $hotel['hoteloid'], ':b' => $promocodeoid, ':d' => $hotel['hoteloid'], ':e' => $promocodeoid));
                            $r_lp_promotion = $stmt->fetchAll(PDO::FETCH_ASSOC);
                            foreach($r_lp_promotion as $promotion){
                        ?>  
                            <div class="col-md-6"><button type="button" class="btn btn-danger btn-xs" id="remove-promotion" data-lpp="<?=$promotion['oid']?>" data-toggle="modal" data-target="#confirmDelPromotion" ><i class="fa fa-close"></i></button> <?=$promotion['name']?></div>
                        <?php
                            }

                        ?>
                    </div>
                </li>
            <?php
                }
            ?>
            </ul>
        </div>
                <br>
                <div class="row">
                        <div class="form-group col-md-12 text-right">
                                <button type="button" class="small-button red cancel">Cancel</button>
                                <button type="submit" class="small-button blue submit-edit">Save Change</button>
                        </div>
                </div>
    </div>
    </div>
	</form>
</section>

<div class="modal fade" id="hotelListModal" tabindex="-1" role="dialog" aria-labelledby="hotelListModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus-circle"></i> Add New Hotel</h4>
      </div>
      <div class="modal-body">
        <form method="post" id="form-assign-hotel" class="form-inline" enctype="multipart/form-data" action="#">
          <input type="hidden" name="chain" value="<?=$campaign['chainoid']?>">
          <div class="form-group"><label>Hotel Name</label></div>
          <div class="form-group"><input type="text" class="form-control" name="name" placeholder="Hotel Name"></div>
          <div class="form-group"><button type="button" class="btn btn-info" id="find">Find</button></div>

          <div class="row">
            <div id="list"></div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="assign-hotel">Assign Selected Hotel</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="promotionListModal" tabindex="-1" role="dialog" aria-labelledby="promotionListModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus-circle"></i> Add Promotion</h4>
      </div>
      <div class="modal-body">
        <form method="post" id="form-assign-promotion" class="form-inline" enctype="multipart/form-data" action="#">
          <input type="hidden" name="ho" value="">
          <input type="hidden" name="lph" value="">
          <input type="hidden" name="pcoid" value="">
          <div class="form-group"><label>Promotion Name</label></div>
          <div class="form-group"><input type="text" class="form-control" name="name" placeholder="Promotion Name"></div>
          <div class="form-group"><button type="button" class="btn btn-info" id="find">Find</button></div>

          <div class="row">
            <div id="list"></div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="assign-promotion">Assign Selected Promotion</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="confirmDelPromotion" tabindex="-1" role="dialog" aria-labelledby="confirmDelPromotion">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus-trash"></i> Unsign Promotion</h4>
      </div>
      <div class="modal-body">
        <form method="post" id="form-remove-promotion" class="form-inline" enctype="multipart/form-data" action="#">
          Are you sure want to remove this promotion from promocode?
                    <input type="hidden" name="lpp">
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
        <button type="button" class="btn btn-danger" id="remove-lpp">Yes</button>
      </div>
    </div>
  </div>
</div>