<li <?php if($uri2=="dashboard"){ echo "class=active"; }?>>
    <a href="<?php echo"$base_url"; ?>/dashboard"><i class="fa fa-dashboard"></i><span>Dashboard</span></a>
</li>
<li <?php if($uri2=="hotel"){ echo "class=active"; }?>>
    <a href="<?php echo"$base_url"; ?>/hotel"><i class="fa fa-home"></i><span>Hotel</span></a>
</li>
<?php /*?>
<li <?php if($uri2=="user"){ echo "class=active"; }?>>
    <a href="<?php echo"$base_url"; ?>/user"><i class="fa fa-user"></i><span>User</span></a>
</li>
<li <?php if($uri2=="chain-availability"){ echo "class=active"; }?>>
    <a href="<?php echo"$base_url"; ?>/chain-availability"><i class="fa fa-table"></i><span>Chain Availability</span></a>
</li>
<?php */?>

<li class="treeview <?php if($uri2=="chain-availability" or $uri2=="chain-availability-lite"){ echo "active"; }?>">
    <a href="#">
        <i class="fa fa-table"></i> <span> Group Availability</span>
        <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
        <li <?php if($uri2=="chain-availability"){ echo "class=active"; }?>><a href="<?=$base_url?>/chain-availability"><i class="fa fa-info-circle"></i> Full Availability</a></li>
        <li <?php if($uri2=="chain-availability-lite"){ echo "class=active"; }?>><a href="<?=$base_url?>/chain-availability-lite"><i class="fa fa-info-circle"></i> Lite Availability</a></li>
    </ul>
</li>

<?php
    $activemenu = array("inhouse-chain", "revenue-chain", "production-chain");
	if(in_array($uri2, $activemenu)){ $active = "active"; }else{ $active = ""; }
?>
<li class="treeview <?=$active?>">
    <a href="#">
        <i class="fa fa-bar-chart-o"></i> <span>IBE Reports</span>
        <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
        <li class="<?php if($uri2=="inhouse-chain"){ echo "active"; }?>"><a href="<?php echo "$base_url"; ?>/inhouse-chain"><i class="fa fa-circle-o"></i> Inhouse by Chain</a></li>
        <li class="<?php if($uri2=="revenue-chain"){ echo "active"; }?>"><a href="<?php echo "$base_url"; ?>/revenue-chain"><i class="fa fa-circle-o"></i> Revenue by Chain</a></li>
        <li class="<?php if($uri2=="production-chain"){ echo "active"; }?>"><a href="<?php echo "$base_url"; ?>/production-chain"><i class="fa fa-circle-o"></i> Production by Chain</a></li>
    </ul>
</li>

<?php
/* CRM / LOYALTY MEMBER MENU ================================================================================*/
// check if hotel has feature pms.
$q_feature_crm = "select count(hotelfeatureoid) as `allowcrm` from hotelfeature inner join hotel h using (hoteloid) inner join chain c using (chainoid) where (h.chainoid in (select oid from userassign where type = 'chainoid' and useroid = '".$_SESSION['_oid']."') or h.hoteloid in (select oid from userassign where type = 'hoteloid' and useroid = '".$_SESSION['_oid']."')) and h.publishedoid = '1' and featureoid = '1'";

$stmt = $db->query($q_feature_crm);
$feature_crm = $stmt->fetch(PDO::FETCH_ASSOC);
if($feature_crm['allowcrm'] > 0){ $allowcrm = true; }else{ $allowcrm = false; }

if($allowcrm == true){
?>
<li class="treeview <?=$active?>">
	<a href="#"><i class="fa fa-handshake-o"></i> <span> Customer Relation</span><i class="fa fa-angle-left pull-right"></i></a>
  <ul class="treeview-menu">
      <li <?php if($uri3=="loyalty-member-program"){ echo "class=active"; }?>><a href="<?=$base_url?>/crm/loyalty-member-program"><i class="fa fa-circle-o"></i> Loyalty Member Program</a></li>
      <li <?php if($uri3=="member"){ echo "class=active"; }?>><a href="<?=$base_url?>/crm/member"><i class="fa fa-users"></i> Member</a></li>
	</ul>
</li>
<?php
}
?>
