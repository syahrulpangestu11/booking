<?php
function dateInterval($d1, $d2){
    $datetime1 = new DateTime($d1);
    $datetime2 = new DateTime($d2);
    $interval = $datetime1->diff($datetime2);
    return $interval->format('%R%a');
}

try{
    session_start();
    error_reporting(E_ALL ^ E_NOTICE);
    include ('../../../conf/connection.php');
    
    $msg = "";
    $query = "";
    if(isset($_POST["property"]) and is_array($_POST["property"]) and !in_array("0", $_POST["property"])){
        $in = implode(",",$_POST["property"]);
        $query .= "AND r.hoteloid IN (".$in.")";
    }else{
        $s_chain = "SELECT ua.oid, ua.type FROM users u INNER JOIN userassign ua USING (useroid) WHERE ua.type IN ('chainoid', 'hoteloid') AND u.useroid = '".$_SESSION['_oid']."'";
        $q_chain = $db->query($s_chain);
        $r_chain = $q_chain->fetchAll(PDO::FETCH_ASSOC);
        $rxoid = array(); $rxtype = array();
        foreach($r_chain as $row){
            $rxoid[] = $row['oid'];
            $rxtype[] = $row['type'];
        }

        $rxchain = array(); $rxhotel = array();
        foreach($rxtype as $k => $v){
            if($v == 'chainoid'){
                $rxchain[] = $rxoid[$k];
            }else if($v == 'hoteloid'){
                $rxhotel[] = $rxoid[$k];
            }
        }

        $msg = "";
        if(count($rxchain) > 0){
            $imp = implode(",", $rxchain);
            $msg .= "AND chainoid IN (".$imp.")";
        }
        if(count($rxhotel) > 0){
            $imp = implode(",", $rxhotel);
            $msg .= "AND hoteloid IN (".$imp.")";
        }

        if($msg != ""){
            $stmt = $db->prepare("SELECT hoteloid, hotelcode, hotelname FROM hotel WHERE publishedoid='1' ".$msg." ORDER BY hotelname");
            $stmt->execute(array());
            $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $qx = array();
            foreach($data as $vals){
                $qx[] = $vals['hoteloid'];
            }
            $in = implode(",",$qx);
            $query .= "AND r.hoteloid IN (".$in.")";
        }else{
            die();
        }
    }
    
    $stmt = $db->prepare("SELECT r.`hoteloid`, r.`hotelname` 
        FROM `hotel` r 
        WHERE r.publishedoid NOT IN (3) ".$query);
    $stmt->execute();
    $r_xroom = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
    $query0 = ""; $dye = 0;
    if($_POST["filtersearch"]=="booking"){
        if($_POST["period"]=="annual"){
            $year = $_POST["annually"];
            $query0 .= "AND YEAR(b.bookingtime)='".$year."' ";
            $msg = "<p>Filter by Booking Date Annualy on ".$year."</p>";
            $theader = $year; 
            $dye = date("z", mktime(0,0,0,12,31,$year)) + 1;
        }else if($_POST["period"]=="month"){
            $year = $_POST["monthly_year"];
            $month = $_POST["monthly_month"];
            $query0 .= "AND YEAR(b.bookingtime)='".$year."' AND MONTH(b.bookingtime)='".$month."' ";
            $msg = "<p>Filter by Booking Date Monthly on ".date("F", strtotime($year."-".$month."-1"))." ".$year."</p>";
            $theader = date("F", strtotime($year."-".$month."-1"))." ".$year;
            $dye = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        }else if($_POST["period"]=="week"){
            $year = $_POST["weekly_year"];
            $week = explode('|',$_POST["weekly_week"]);
            $query0 .= "AND b.bookingtime >= '".$week[0]."' AND b.bookingtime <= '".$week[1]."' ";
            $msg = "<p>Filter by Booking Date Weekly on ".date("d M Y", strtotime($week[0]))." until ".date("d M Y", strtotime($week[1]))."</p>";
            $theader = date("d M Y", strtotime($week[0]))." until ".date("d M Y", strtotime($week[1]));
            $dye = intval(dateInterval($week[0],$week[1]));
        }else if($_POST["period"]=="range"){
            $startdate = date("Y-m-d", strtotime($_POST["range_start"]));
            $enddate = date("Y-m-d", strtotime($_POST["range_end"]));
            $query0 .= "AND b.bookingtime >= '".$startdate."' AND b.bookingtime <= '".$enddate."' ";
            $msg = "<p>Filter by Booking Date Range on ".date("d M Y", strtotime($startdate))." until ".date("d M Y", strtotime($enddate))."</p>";
            $theader = date("d M Y", strtotime($startdate))." until ".date("d M Y", strtotime($enddate));
            $dye = intval(dateInterval($startdate,$enddate));
        }
    }else if($_POST["filtersearch"]=="checkin"){
        if($_POST["period"]=="annual"){
            $year = $_POST["annually"];
            $query0 .= "AND YEAR(br.checkin)='".$year."' ";
            $msg = "<p>Filter by Check-in Date Annualy on ".$year."</p>";
            $theader = $year;
            $dye = date("z", mktime(0,0,0,12,31,$year)) + 1;
        }else if($_POST["period"]=="month"){
            $year = $_POST["monthly_year"];
            $month = $_POST["monthly_month"];
            $query0 .= "AND YEAR(br.checkin)='".$year."' AND MONTH(br.checkin)='".$month."' ";
            $msg = "<p>Filter by Check-in Date Monthly on ".date("F", strtotime($year."-".$month."-1"))." ".$year."</p>";
            $theader = date("F", strtotime($year."-".$month."-1"))." ".$year;
            $dye = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        }else if($_POST["period"]=="week"){
            $year = $_POST["weekly_year"];
            $week = explode('|',$_POST["weekly_week"]);
            $query0 .= "AND br.checkin >= '".$week[0]."' AND br.checkin <= '".$week[1]."' ";
            $msg = "<p>Filter by Check-in Date Weekly on ".date("d M Y", strtotime($week[0]))." until ".date("d M Y", strtotime($week[1]))."</p>";
            $theader = date("d M Y", strtotime($week[0]))." until ".date("d M Y", strtotime($week[1]));
            $dye = intval(dateInterval($week[0],$week[1]));
        }else if($_POST["period"]=="range"){
            $startdate = date("Y-m-d", strtotime($_POST["range_start"]));
            $enddate = date("Y-m-d", strtotime($_POST["range_end"]));
            $query0 .= "AND br.checkin >= '".$startdate."' AND br.checkin <= '".$enddate."' ";
            $msg = "<p>Filter by Check-in Date Range on ".date("d M Y", strtotime($startdate))." until ".date("d M Y", strtotime($enddate))."</p>";
            $theader = date("d M Y", strtotime($startdate))." until ".date("d M Y", strtotime($enddate));
            $dye = intval(dateInterval($startdate,$enddate));
        }
    }else if($_POST["filtersearch"]=="stay"){
        if($_POST["period"]=="annual"){
            $year = $_POST["annually"];
            $query0 .= "AND YEAR(bdt.date)='".$year."' ";
            $msg = "<p>Filter by Stay Date Annualy on ".$year."</p>";
            $theader = $year;
            $dye = date("z", mktime(0,0,0,12,31,$year)) + 1;
        }else if($_POST["period"]=="month"){
            $year = $_POST["monthly_year"];
            $month = $_POST["monthly_month"];
            $query0 .= "AND YEAR(bdt.date)='".$year."' AND MONTH(bdt.date)='".$month."' ";
            $msg = "<p>Filter by Stay Date Monthly on ".date("F", strtotime($year."-".$month."-1"))." ".$year."</p>";
            $theader = date("F", strtotime($year."-".$month."-1"))." ".$year;
            $dye = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        }else if($_POST["period"]=="week"){
            $year = $_POST["weekly_year"];
            $week = explode('|',$_POST["weekly_week"]);
            $query0 .= "AND bdt.date >= '".$week[0]."' AND bdt.date <= '".$week[1]."' ";
            $msg = "<p>Filter by Stay Date Weekly on ".date("d M Y", strtotime($week[0]))." until ".date("d M Y", strtotime($week[1]))."</p>";
            $theader = date("d M Y", strtotime($week[0]))." until ".date("d M Y", strtotime($week[1]));
            $dye = intval(dateInterval($week[0],$week[1]));
        }else if($_POST["period"]=="range"){
            $startdate = date("Y-m-d", strtotime($_POST["range_start"]));
            $enddate = date("Y-m-d", strtotime($_POST["range_end"]));
            $query0 .= "AND bdt.date >= '".$startdate."' AND bdt.date <= '".$enddate."' ";
            $msg = "<p>Filter by Stay Date Range on ".date("d M Y", strtotime($startdate))." until ".date("d M Y", strtotime($enddate))."</p>";
            $theader = date("d M Y", strtotime($startdate))." until ".date("d M Y", strtotime($enddate));
            $dye = intval(dateInterval($startdate,$enddate));
        }
    }

    $stmt = $db->prepare("SELECT hoteloid, hotelname, count(`roomnumberoid`) AS numofroom,
            (SELECT COUNT(bdt.`bookingroomdtloid`) FROM `bookingroom` br INNER JOIN `booking` b USING(`bookingoid`) INNER JOIN `bookingroomdtl` bdt USING(`bookingroomoid`) WHERE br.`pmsstatusoid`!=0 AND `bookingstatusoid` in (4) AND br.pmsstatusoid in (2,4,5,6) AND b.`hoteloid`=h.`hoteloid` ".$query0.") AS roomsold
        FROM hotel h
            LEFT JOIN `room` r USING(`hoteloid`)
            LEFT JOIN `roomnumber` rn USING(`roomoid`)
        WHERE h.`publishedoid` not in (3) AND r.`publishedoid` not in (3) AND rn.`publishedoid` not in (3) ".$query." 
        GROUP BY hoteloid, hotelname");
    $stmt->execute();
    $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
    $data_room = array(); $data_room_s = array();
    foreach($r_room as $room){
        $data_room[$room['hoteloid']] = round(($room['roomsold']*100)/($dye*$room['numofroom']),2);
        $data_room_s[$room['hoteloid']] = $room['roomsold'];
    }
    
    //***********------- COMPARE -------*************//
    $msg1 = ""; $dye1 = 0;
    $r_room1 = array();
    if(isset($_POST['compare']) and $_POST['compare']=='1'){
        if($_POST["filtersearch"]=="booking"){
            if($_POST["period"]=="annual"){
                $year = $_POST["var_annually"];
                $query1 .= "AND YEAR(b.bookingtime)='".$year."' ";
                $msg1 = "<p>Compare with ".$year."</p>";
                $theader1 = $year;
                $dye1 = date("z", mktime(0,0,0,12,31,$year)) + 1;
            }else if($_POST["period"]=="month"){
                $year = $_POST["var_monthly_year"];
                $month = $_POST["var_monthly_month"];
                $query1 .= "AND YEAR(b.bookingtime)='".$year."' AND MONTH(b.bookingtime)='".$month."' ";
                $msg1 = "<p>Compare with ".date("F", strtotime($year."-".$month."-1"))." ".$year."</p>";
                $theader1 = date("F", strtotime($year."-".$month."-1"))." ".$year;
                $dye1 = cal_days_in_month(CAL_GREGORIAN, $month, $year);
            }else if($_POST["period"]=="week"){
                $year = $_POST["var_weekly_year"];
                $week = explode('|',$_POST["var_weekly_week"]);
                $query1 .= "AND b.bookingtime >= '".$week[0]."' AND b.bookingtime <= '".$week[1]."' ";
                $msg1 = "<p>Compare with ".date("d M Y", strtotime($week[0]))." until ".date("d M Y", strtotime($week[1]))."</p>";
                $theader1 = date("d M Y", strtotime($week[0]))." until ".date("d M Y", strtotime($week[1]));
                $dye1 = intval(dateInterval($week[0],$week[1]));
            }else if($_POST["period"]=="range"){
                $startdate = date("Y-m-d", strtotime($_POST["var_range_start"]));
                $enddate = date("Y-m-d", strtotime($_POST["var_range_end"]));
                $query1 .= "AND b.bookingtime >= '".$startdate."' AND b.bookingtime <= '".$enddate."' ";
                $msg1 = "<p>Compare with ".date("d M Y", strtotime($startdate))." until ".date("d M Y", strtotime($enddate))."</p>";
                $theader1 = date("d M Y", strtotime($startdate))." until ".date("d M Y", strtotime($enddate));
                $dye1 = intval(dateInterval($startdate,$enddate));
            }
        }else if($_POST["filtersearch"]=="checkin"){
            if($_POST["period"]=="annual"){
                $year = $_POST["var_annually"];
                $query1 .= "AND YEAR(br.checkin)='".$year."' ";
                $msg1 = "<p>Compare with ".$year."</p>";
                $theader1 = $year;
                $dye1 = date("z", mktime(0,0,0,12,31,$year)) + 1;
            }else if($_POST["period"]=="month"){
                $year = $_POST["var_monthly_year"];
                $month = $_POST["var_monthly_month"];
                $query1 .= "AND YEAR(br.checkin)='".$year."' AND MONTH(br.checkin)='".$month."' ";
                $msg1 = "<p>Compare with ".date("F", strtotime($year."-".$month."-1"))." ".$year."</p>";
                $theader1 = date("F", strtotime($year."-".$month."-1"))." ".$year;
                $dye1 = cal_days_in_month(CAL_GREGORIAN, $month, $year);
            }else if($_POST["period"]=="week"){
                $year = $_POST["var_weekly_year"];
                $week = explode('|',$_POST["var_weekly_week"]);
                $query1 .= "AND br.checkin >= '".$week[0]."' AND br.checkin <= '".$week[1]."' ";
                $msg1 = "<p>Compare with ".date("d M Y", strtotime($week[0]))." until ".date("d M Y", strtotime($week[1]))."</p>";
                $theader1 = date("d M Y", strtotime($week[0]))." until ".date("d M Y", strtotime($week[1]));
                $dye1 = intval(dateInterval($week[0],$week[1]));
            }else if($_POST["period"]=="range"){
                $startdate = date("Y-m-d", strtotime($_POST["var_range_start"]));
                $enddate = date("Y-m-d", strtotime($_POST["var_range_end"]));
                $query1 .= "AND br.checkin >= '".$startdate."' AND br.checkin <= '".$enddate."' ";
                $msg1 = "<p>Compare with ".date("d M Y", strtotime($startdate))." until ".date("d M Y", strtotime($enddate))."</p>";
                $theader1 = date("d M Y", strtotime($startdate))." until ".date("d M Y", strtotime($enddate));
                $dye1 = intval(dateInterval($startdate,$enddate));
            }
        }else if($_POST["filtersearch"]=="stay"){
            if($_POST["period"]=="annual"){
                $year = $_POST["var_annually"];
                $query1 .= "AND YEAR(bdt.date)='".$year."' ";
                $msg1 = "<p>Compare with ".$year."</p>";
                $theader1 = $year;
                $dye1 = date("z", mktime(0,0,0,12,31,$year)) + 1;
            }else if($_POST["period"]=="month"){
                $year = $_POST["var_monthly_year"];
                $month = $_POST["var_monthly_month"];
                $query1 .= "AND YEAR(bdt.date)='".$year."' AND MONTH(bdt.date)='".$month."' ";
                $msg1 = "<p>Compare with ".date("F", strtotime($year."-".$month."-1"))." ".$year."</p>";
                $theader1 = date("F", strtotime($year."-".$month."-1"))." ".$year;
                $dye1 = cal_days_in_month(CAL_GREGORIAN, $month, $year);
            }else if($_POST["period"]=="week"){
                $year = $_POST["var_weekly_year"];
                $week = explode('|',$_POST["var_weekly_week"]);
                $query1 .= "AND bdt.date >= '".$week[0]."' AND bdt.date <= '".$week[1]."' ";
                $msg1 = "<p>Compare with ".date("d M Y", strtotime($week[0]))." until ".date("d M Y", strtotime($week[1]))."</p>";
                $theader1 = date("d M Y", strtotime($week[0]))." until ".date("d M Y", strtotime($week[1]));
                $dye1 = intval(dateInterval($week[0],$week[1]));
            }else if($_POST["period"]=="range"){
                $startdate = date("Y-m-d", strtotime($_POST["var_range_start"]));
                $enddate = date("Y-m-d", strtotime($_POST["var_range_end"]));
                $query1 .= "AND bdt.date >= '".$startdate."' AND bdt.date <= '".$enddate."' ";
                $msg1 = "<p>Compare with ".date("d M Y", strtotime($startdate))." until ".date("d M Y", strtotime($enddate))."</p>";
                $theader1 = date("d M Y", strtotime($startdate))." until ".date("d M Y", strtotime($enddate));
                $dye1 = intval(dateInterval($startdate,$enddate));
            }
        }
    
        $stmt = $db->prepare("SELECT hoteloid, hotelname, count(`roomnumberoid`) AS numofroom,
                (SELECT COUNT(bdt.`bookingroomdtloid`) FROM `bookingroom` br INNER JOIN `booking` b USING(`bookingoid`) INNER JOIN `bookingroomdtl` bdt USING(`bookingroomoid`) WHERE br.`pmsstatusoid`!=0 AND `bookingstatusoid` in (4) AND br.pmsstatusoid in (2,4,5,6) AND b.`hoteloid`=h.`hoteloid` ".$query1.") AS roomsold
            FROM hotel h
                LEFT JOIN `room` r USING(`hoteloid`)
                LEFT JOIN `roomnumber` rn USING(`roomoid`)
            WHERE h.`publishedoid` not in (3) AND r.`publishedoid` not in (3) AND rn.`publishedoid` not in (3) ".$query." 
            GROUP BY hoteloid, hotelname");
        $stmt->execute();
        $r_room1 = $stmt->fetchAll(PDO::FETCH_ASSOC);
        
        $data_room1 = array(); $data_room_s1 = array();
        foreach($r_room1 as $room1){
            $data_room1[$room1['hoteloid']] = round(($room1['roomsold']*100)/($dye1*$room1['numofroom']),2);
            $data_room_s1[$room1['hoteloid']] = $room1['roomsold'];
        }
    }

}catch(PDOException $ex){
    die("Error");
}
?>
<ul class="nav nav-tabs">
    <li class="nav-item active"><a class="nav-link" id="tbbooking-tab" data-toggle="tab" href="#tbbooking">Occupancy</a></li>
</ul>
<div class="tab-content">
    <div class="tab-pane fade in active" id="tbbooking">
        <div class="tab-container">
            <div class="row">
                <div class="col-lg-12">
                    <canvas id="myChart"></canvas>
                </div>
                <div class="col-lg-12">
                    <div><?=$msg?><?=$msg1?></div>
                    <table class="table table-striped tablesorter" id="myTable">
                    <thead>
                    <tr>
                        <th>-</th>
                        <th>Property</th>
                        <th><?=$theader?></th>
                        <th>Room Sold</th>
                        <?php if($msg1!=""){ echo '<th>'.$theader1.'</th><th>% Variance</th>'; }?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                        $sv = array();
                        $sw = array();
                        $sx = array();
                        $x = 1;
                        foreach($r_xroom as $room){
                            if(empty($data_room[$room['hoteloid']])){ $data_room[$room['hoteloid']]="0"; }
                            if($msg1!=""){ 
                                if(empty($data_room1[$room['hoteloid']])){ $data_room1[$room['hoteloid']]="0"; } 
                                $n1 = floatval($data_room1[$room['hoteloid']]) - floatval($data_room[$room['hoteloid']]);
                                $n2 = (floatval($data_room[$room['hoteloid']]) != 0) ? floatval($data_room[$room['hoteloid']]) : 1;
                                $compare = $data_room1[$room['hoteloid']]-$data_room[$room['hoteloid']]; //round(($n1/$n2) * 100, 2);
                                $q='<td>'.number_format($data_room1[$room['hoteloid']],2,'.',',').'%</td><td>'.$compare.'%</td>'; 
                            }else{ 
                                $q=''; 
                            }
                            echo '
                            <tr>
                                <td>H'.$x.'</td>
                                <td>'.$room['hotelname'].'</td>
                                <td>'.number_format($data_room[$room['hoteloid']],2,'.',',').'%</td>
                                <td>'.number_format($data_room_s[$room['hoteloid']],0,'.',',').'</td>
                                '.$q.'
                            </tr>
                            ';
                            $epl = explode(" ", $room['hotelname']);
                            $sepl = (count($epl)<=1) ? $epl[0] : $epl[0]." ".$epl[1];
                            $sv[] = $sepl;//"H".$x;
                            $sw[] = $data_room[$room['hoteloid']];
                            $sx[] = $data_room1[$room['hoteloid']];
                            $x++;
                        }
                    ?>
                    </tbody>
                    </table>
                    <div id="data-name" style="display:none"><?=implode(";", $sv)?></div>
                    <div id="data-bid" style="display:none"><?=implode(";", $sw)?></div>
                    <div id="data-com" style="display:none"><?=implode(";", $sx)?></div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>