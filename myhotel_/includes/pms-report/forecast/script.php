<script type="text/javascript">
$(function(){

function showInventory(){
	box = $('div#show-inventory');
	box.html("loading ...");
	$.ajax({
		url: "<?php echo"$base_url"; ?>/includes/pms-report/forecast/data_daily_forecast.php",
		type: 'post',
		data: $('form#daily-revenue').serialize(),
		success: function(data) {
			box.html(data);
		}
	});
}

$(document).on('click','#btexport',function(){
		$('#exporttoexcel').submit();
});

$(document).on('click','form#daily-revenue button', function(e){
	showInventory();
});

$(document).ready(function(){
	showInventory();
});

$( "#startdate2" ).datepicker({
	defaultDate: "0",
	dateFormat: "dd M y",
	changeMonth: true, changeYear:true,
	onClose: function( selectedDate ) {
		$( "#enddate2" ).datepicker( "option", "minDate", selectedDate );
	}
});
$( "#enddate2" ).datepicker({
	defaultDate: "0",
	dateFormat: "dd M y",
	changeMonth: true, changeYear:true,
	onClose: function( selectedDate ) {
		$( "#startdate2" ).datepicker( "option", "maxDate", selectedDate );
	}
});

});
</script>
