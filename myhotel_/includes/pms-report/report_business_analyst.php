<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous"> -->

<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    });
</script>
<section class="content-header">
    <h1>
       	Report PMS - Business Analysis
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Report PMS</a></li>
    </ol>
</section>
<section class="content">
    


<div class="row">
<div class="box box-form"><div class="box-body" style="font-size: 12px;">

<div class="form-group">
    <!-- Select Room Type:   
    <input type="checkbox" id="myChec1">Superior    
    <input type="checkbox" id="myCheck">Deluxe 
    <input type="checkbox" id="myCheck">Suites
    &nbsp;&nbsp;&nbsp;&nbsp;
    <select name="status" class="form-control">
        <option value="volvo">All Status</option>
        <option value="saab">Check In</option>
        <option value="fiat">Check Out</option>
        <option value="audi">Reserve</option>
        <option value="volvo">Temporary Reserve</option>
    </select>
        
    <select name="market" class="form-control">
        <option value="volvo">All Market</option>
        <option value="volvo">Direct website</option>
        <option value="saab">direct booking</option>
        <option value="fiat">Travel agent</option>
        <option value="audi">Corporate</option>
        <option value="volvo">Online Travel Agent (OTA)</option>
        <option value="saab">Vacation Rental</option>
        <option value="fiat">Wedding & event</option>
        <option value="audi">Affiliate</option>
    </select> -->
    
    <label>Between:</label> &nbsp;&nbsp;
    <input type='text' class="form-control" value="2017-10-01"/>
    <label>And:</label> &nbsp;&nbsp;
    <input type='text' class="form-control" value="2017-10-31"/>
</div>
        </div></div>
</div>




    <div class="row">
        <div class="box box-form"><div class="box-body" style="font-size: 12px;">
            <table id="example" class="display" cellspacing="0" width="100%">
            <thead>
              <tr>
                <td>#</td>
                <td>Source</td>
                <td>Nights</td>
                <td>Occupancy%</td>
                <td>Pax</td>
                <td>&nbsp;Room Revenue&nbsp;</td>
                <td>Revenue%</td>
                <td>&nbsp;ARR&nbsp;</td>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>1</td>
                <td>Not Specified</td>
                <td>3</td>
                <td>1.99%</td>
                <td>6</td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3,788,123 </td>
                <td>1.85%</td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp; 3,130,680 </td>
              </tr>
              <tr>
                <td>2</td>
                <td>Direct Booking</td>
                <td>7</td>
                <td>4.64%</td>
                <td>12</td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 7,576,246 </td>
                <td>3.69%</td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp; 6,261,361 </td>
              </tr>
              <tr>
                <td>3</td>
                <td>Direct Website</td>
                <td>12</td>
                <td>7.95%</td>
                <td>20</td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 15,152,493 </td>
                <td>7.38%</td>
                <td>&nbsp;&nbsp; 12,522,721 </td>
              </tr>
              <tr>
                <td>4</td>
                <td>OTA</td>
                <td>30</td>
                <td>19.87%</td>
                <td>50</td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 45,457,479 </td>
                <td>22.15%</td>
                <td>&nbsp;&nbsp; 37,568,164 </td>
              </tr>
              <tr>
                <td>5</td>
                <td>Travel Agent</td>
                <td>35</td>
                <td>23.18%</td>
                <td>40</td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 59,094,723 </td>
                <td>28.80%</td>
                <td>&nbsp;&nbsp; 48,838,614 </td>
              </tr>
              <tr>
                <td>6</td>
                <td>Corporate</td>
                <td>30</td>
                <td>19.87%</td>
                <td>64</td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 39,396,482 </td>
                <td>19.20%</td>
                <td>&nbsp;&nbsp; 32,559,076 </td>
              </tr>
              <tr>
                <td>7</td>
                <td>Vacation Rental</td>
                <td>5</td>
                <td>3.31%</td>
                <td>12</td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3,156,769 </td>
                <td>1.54%</td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp; 2,608,900 </td>
              </tr>
              <tr>
                <td>8</td>
                <td>Wedding &amp; Event</td>
                <td>29</td>
                <td>19.21%</td>
                <td>44</td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 31,567,694 </td>
                <td>15.38%</td>
                <td>&nbsp;&nbsp; 26,089,003 </td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>Total</td>
                <td>151</td>
                <td>&nbsp;</td>
                <td>248</td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 205,190,009 </td>
                <td>&nbsp;</td>
                <td>&nbsp;169,578,520 </td>
              </tr>
            </tbody>
            </table>
      </div></div>
    </div>
</section>