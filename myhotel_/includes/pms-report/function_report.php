<?php
function getReservationData($db, $hoteloid){
    try {
        $stmt = $db->prepare("SELECT bookingoid, bookingnumber, bookingtime, title, firstname, lastname, c.phone, c.mobilephone, c.email,
                checkin, checkout, DATEDIFF(checkout, checkin) AS diff, br.room, br.adult, br.child, currencycode, b.grandtotal, b.bookingtime, rn.roomnumber,
                b.bm_name, bm.bm_name as bm_name1, countryname, pm.status, pm.`pmsstatusoid`, brd.bookingroomdtloid, b.agentoid, g.agentname, g.agenttypeoid, bookingstatusoid
            FROM `booking` b
                INNER JOIN `bookingroom` br USING(`bookingoid`)
                INNER JOIN `bookingroomdtl` brd USING(`bookingroomoid`)
                INNER JOIN `customer` c USING(`custoid`)
                LEFT JOIN `currency` cr ON cr.`currencyoid` = b.`currencyoid`
                LEFT JOIN `roomoffer` ro ON ro.`roomofferoid` = br.`roomofferoid`
                LEFT JOIN `room` r ON r.`roomoid` = ro.`roomoid`
                LEFT JOIN `roomnumber` rn ON brd.`roomnumberoid` = rn.`roomnumberoid`
                LEFT JOIN `bookingmarket` bm ON bm.`bookingmarketoid` = b.`bookingmarketoid`
                LEFT JOIN `country` cn ON cn.`countryoid` = c.`countryoid`
                LEFT JOIN `pmsstatus` pm ON pm.`pmsstatusoid` = br.`pmsstatusoid`
                LEFT JOIN `agent` g ON g.`agentoid`=b.`agentoid`
            WHERE `bookingstatusoid` in (4,5,7) AND b.hoteloid=:a AND br.pmsstatusoid not in ('0') AND brd.reconciled = '0'
            GROUP BY `bookingoid` DESC");

        $stmt->execute(array(':a'=>$hoteloid));
        $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $r_room;

    }catch(PDOException $ex) {
        return array();
    }
}
function getReservationDataChain($db, $chainoid){
    try {
        $stmt = $db->prepare("SELECT bookingoid, bookingnumber, bookingtime, title, firstname, lastname, c.phone, c.mobilephone, c.email,
                checkin, checkout, DATEDIFF(checkout, checkin) AS diff, br.room, br.adult, br.child, currencycode, b.grandtotal, b.bookingtime, rn.roomnumber,
                b.bm_name, bm.bm_name as bm_name1, countryname, pm.status, pm.`pmsstatusoid`, brd.bookingroomdtloid, g.agentname, g.agenttypeoid, bookingstatusoid
            FROM `booking` b
                INNER JOIN `bookingroom` br USING(`bookingoid`)
                INNER JOIN `bookingroomdtl` brd USING(`bookingroomoid`)
                INNER JOIN `customer` c USING(`custoid`)
                LEFT JOIN `currency` cr ON cr.`currencyoid` = b.`currencyoid`
                LEFT JOIN `roomoffer` ro ON ro.`roomofferoid` = br.`roomofferoid`
                LEFT JOIN `room` r ON r.`roomoid` = ro.`roomoid`
                LEFT JOIN `roomnumber` rn ON brd.`roomnumberoid` = rn.`roomnumberoid`
                LEFT JOIN `bookingmarket` bm ON bm.`bookingmarketoid` = b.`bookingmarketoid`
                LEFT JOIN `country` cn ON cn.`countryoid` = c.`countryoid`
                LEFT JOIN `pmsstatus` pm ON pm.`pmsstatusoid` = br.`pmsstatusoid`
                LEFT JOIN `agent` g ON g.`agentoid`=b.`agentoid`
            WHERE `bookingstatusoid` in (4,5,7) AND b.hoteloid IN (SELECT hoteloid FROM hotel WHERE chainoid=:a) AND br.pmsstatusoid not in ('0') AND brd.reconciled = '0'
            GROUP BY `bookingoid` DESC");

        $stmt->execute(array(':a'=>$chainoid));
        $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $r_room;

    }catch(PDOException $ex) {
        return array();
    }
}

function getCustTitle($db){
    try {
        $stmt = $db->prepare("SELECT titleoid, name FROM title");
        $stmt->execute();
        $r_data = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $data = array();
        foreach($r_data as $row){
            $data[$row['titleoid']] = $row['name'];
        }
        return $data;
    }catch(PDOException $ex) {
        return array();
    }
}

function getHotelRoom($db, $hoteloid){
    try {
        $stmt = $db->prepare("SELECT r.* FROM `room` r INNER JOIN `hotel` h USING(`hoteloid`) WHERE r.`publishedoid`=1 AND h.`hoteloid`=:a ORDER BY r.name ASC");
        $stmt->execute(array(':a'=>$hoteloid));
        $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $r_room;

    }catch(PDOException $ex) {
        return array();
    }
}

function getPMSStatus($db, $avoid = array()){
    try {
        if(count($avoid) > 0){
            $string = "AND `pmsstatusoid` NOT IN (".implode(",", $avoid).")";
        }else{
            $string = "";
        }

        $stmt = $db->prepare("SELECT * FROM `pmsstatus` WHERE `publishedoid`=1 ".$string." ORDER BY `pmsstatusoid`");
        $stmt->execute();
        $r_data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $r_data;

    }catch(PDOException $ex) {
        return array();
    }
}

function getBookingMarket($db){
    try {
        $stmt = $db->prepare("SELECT * FROM `bookingmarket` WHERE `publishedoid`=1 ORDER BY `bookingmarketoid`");
        $stmt->execute();
        $r_data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $r_data;

    }catch(PDOException $ex) {
        return array();
    }
}

function getAgent($db, $hoteloid){
    try {
        $stmt = $db->prepare("SELECT ap.* FROM `agent` a INNER JOIN `agenthotel` ah USING(`agentoid`) INNER JOIN `agenttype` ap USING(`agenttypeoid`)
            WHERE a.`publishedoid`=1 AND ah.`publishedoid`=1 AND ap.`publishedoid`=1 AND `hoteloid`=:ho GROUP BY ap.`agenttypeoid` ORDER BY typename");
        $stmt->execute(array(':ho'=>$hoteloid));
        $r_data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $r_data;

    }catch(PDOException $ex) {
        return array();
    }
}

function getAgentChain($db, $chainoid){
    try {
        $stmt = $db->prepare("SELECT ap.* FROM `agent` a INNER JOIN `agenthotel` ah USING(`agentoid`) INNER JOIN `agenttype` ap USING(`agenttypeoid`)
            WHERE a.`publishedoid`=1 AND ah.`publishedoid`=1 AND ap.`publishedoid`=1 AND `hoteloid` IN (SELECT hoteloid FROM hotel WHERE chainoid=:ca) GROUP BY ap.`agenttypeoid` ORDER BY typename");
        $stmt->execute(array(':ca'=>$chainoid));
        $r_data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $r_data;

    }catch(PDOException $ex) {
        return array();
    }
}

/*function getReservationDataIncome($db, $hoteloid, $month, $year){
    $lastperiode = $year."-".$month."-25";
    $startperiode = date('Y-m', strtotime($lastperiode." - 1 month"))."-25";
    try {
        $stmt = $db->prepare("SELECT bookingoid, bookingnumber, bookingtime, title, firstname, lastname, c.phone, c.mobilephone, c.email,
                checkin, checkout, DATEDIFF(checkout, checkin) AS diff, br.room, br.adult, br.child, currencycode, b.grandtotal, b.bookingtime, rn.roomnumber,
                b.bm_name, bm.bm_name as bm_name1, countryname, pm.status, pm.`pmsstatusoid`, brd.bookingroomdtloid, b.agentoid, g.agentname, g.agenttypeoid, bookingstatusoid, br.total
            FROM `booking` b
                INNER JOIN `bookingroom` br USING(`bookingoid`)
                INNER JOIN `bookingroomdtl` brd USING(`bookingroomoid`)
                INNER JOIN `customer` c USING(`custoid`)
                LEFT JOIN `currency` cr ON cr.`currencyoid` = b.`currencyoid`
                LEFT JOIN `roomoffer` ro ON ro.`roomofferoid` = br.`roomofferoid`
                LEFT JOIN `room` r ON r.`roomoid` = ro.`roomoid`
                LEFT JOIN `roomnumber` rn ON brd.`roomnumberoid` = rn.`roomnumberoid`
                LEFT JOIN `bookingmarket` bm ON bm.`bookingmarketoid` = b.`bookingmarketoid`
                LEFT JOIN `country` cn ON cn.`countryoid` = c.`countryoid`
                LEFT JOIN `pmsstatus` pm ON pm.`pmsstatusoid` = br.`pmsstatusoid`
                LEFT JOIN `agent` g ON g.`agentoid`=b.`agentoid`
            WHERE `bookingstatusoid` in (4,7) AND b.hoteloid=:a AND br.pmsstatusoid in (2,4,5,6,7) AND br.pmsstatusoid not in ('0') AND brd.reconciled = '0' AND (date(checkin) between :b AND :c)
            GROUP BY `bookingoid` ORDER BY checkin ASC");

        $stmt->execute(array(':a'=>$hoteloid, ':b'=>$startperiode, ':c'=>$lastperiode));
        $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $r_room;

    }catch(PDOException $ex) {
        return array();
    }
}*/

function getAssignedHotels($db){
    $s_chain = "SELECT ua.oid, ua.type FROM users u INNER JOIN userassign ua USING (useroid) WHERE ua.type IN ('chainoid', 'hoteloid') AND u.useroid = '".$_SESSION['_oid']."'";
    $q_chain = $db->query($s_chain);
    $r_chain = $q_chain->fetchAll(PDO::FETCH_ASSOC);
    $rxoid = array(); $rxtype = array();
    foreach($r_chain as $row){
        $rxoid[] = $row['oid'];
        $rxtype[] = $row['type'];
    }
    
    $rxchain = array(); $rxhotel = array();
    foreach($rxtype as $k => $v){
        if($v == 'chainoid'){
            $rxchain[] = $rxoid[$k];
        }else if($v == 'hoteloid'){
            $rxhotel[] = $rxoid[$k];
        }
    }
    
    $msg = "";
    if(count($rxchain) > 0){
        $imp = implode(",", $rxchain);
        $msg .= "AND chainoid IN (".$imp.")";
    }
    if(count($rxhotel) > 0){
        $imp = implode(",", $rxhotel);
        $msg .= "AND hoteloid IN (".$imp.")";
    }
    
    if($msg != ""){
        $stmt = $db->prepare("SELECT hoteloid, hotelcode, hotelname FROM hotel WHERE publishedoid='1' ".$msg." ORDER BY hotelname");
        $stmt->execute(array());
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
    }else{
        $data = array();
    }

    return $data;
}

function getReservationDataIncomeRange($db, $hoteloid, $from, $to){
    $startperiode = date('Y-m-d', strtotime($from));
    $lastperiode = date('Y-m-d', strtotime($to));

    $hl = "";
    if($hoteloid == '0'){
        $hotels = getAssignedHotels($db);
        if(count($hotels) <= 0){
            $hl .= "b.hoteloid='0'";
        }else{
            $tmp = array();
            foreach ($hotels as $v) {
                $tmp[] = $v['hoteloid'];
            }
            $hl .= "b.hoteloid IN (".implode(',', $tmp).")";
        }
        $arr = array(':b'=>$startperiode, ':c'=>$lastperiode);
    }else{
        $hl = "b.hoteloid=:a";
        $arr = array(':a'=>$hoteloid, ':b'=>$startperiode, ':c'=>$lastperiode);
    }
    try {
        $stmt = $db->prepare("SELECT bookingoid, bookingnumber, bookingtime, title, firstname, lastname, c.phone, c.mobilephone, c.email,
                checkin, checkout, DATEDIFF(checkout, checkin) AS diff, br.room, br.adult, br.child, currencycode, b.grandtotal, b.bookingtime, rn.roomnumber,
                b.bm_name, bm.bm_name as bm_name1, countryname, pm.status, pm.`pmsstatusoid`, brd.bookingroomdtloid, b.agentoid, g.agentname, g.agenttypeoid, bookingstatusoid, br.total
            FROM `booking` b
                INNER JOIN `bookingroom` br USING(`bookingoid`)
                INNER JOIN `bookingroomdtl` brd USING(`bookingroomoid`)
                INNER JOIN `customer` c USING(`custoid`)
                LEFT JOIN `currency` cr ON cr.`currencyoid` = b.`currencyoid`
                LEFT JOIN `roomoffer` ro ON ro.`roomofferoid` = br.`roomofferoid`
                LEFT JOIN `room` r ON r.`roomoid` = ro.`roomoid`
                LEFT JOIN `roomnumber` rn ON brd.`roomnumberoid` = rn.`roomnumberoid`
                LEFT JOIN `bookingmarket` bm ON bm.`bookingmarketoid` = b.`bookingmarketoid`
                LEFT JOIN `country` cn ON cn.`countryoid` = c.`countryoid`
                LEFT JOIN `pmsstatus` pm ON pm.`pmsstatusoid` = br.`pmsstatusoid`
                LEFT JOIN `agent` g ON g.`agentoid`=b.`agentoid`
            WHERE `bookingstatusoid` in (4,7) AND ".$hl." AND br.pmsstatusoid in (2,4,5,6,7) AND br.pmsstatusoid not in ('0') AND brd.reconciled = '0' AND (date(checkin) between :b AND :c)
            GROUP BY `bookingoid` ORDER BY checkin ASC");

        $stmt->execute($arr);
        $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $r_room;

    }catch(PDOException $ex) {
        return array();
    }
}

function getReservationARLedgerRange($db, $hoteloid, $from, $to){
    $startperiode = date('Y-m-d', strtotime($from));
    $lastperiode = date('Y-m-d', strtotime($to));

    $hl = "";
    if($hoteloid == '0'){
        $hotels = getAssignedHotels($db);
        if(count($hotels) <= 0){
            $hl .= "b.hoteloid='0'";
        }else{
            $tmp = array();
            foreach ($hotels as $v) {
                $tmp[] = $v['hoteloid'];
            }
            $hl .= "b.hoteloid IN (".implode(',', $tmp).")";
        }
        $arr = array(':b'=>$startperiode, ':c'=>$lastperiode);
    }else{
        $hl = "b.hoteloid=:a";
        $arr = array(':a'=>$hoteloid, ':b'=>$startperiode, ':c'=>$lastperiode);
    }
    try {
        $stmt = $db->prepare("SELECT `bookingpaymentdtloid`, `created`, `description`, title, firstname, lastname, g.agentname, br.checkin, br.checkout,
            b.invnumofhotel, b.invnumofchain, b.dateofinvoice, bpd.amount, bpd.`administrationfee`, bpd.`total`, bpd.`receipt`, bpd.`note`, pm.status, currencycode,
            b.bookingstatusoid, b.bookingnumber
            FROM `bookingpaymentdtl` bpd
            	INNER JOIN `booking` b USING(`bookingoid`)
                INNER JOIN `bookingroom` br USING(`bookingoid`)
            	INNER JOIN `customer` c USING(`custoid`)
                LEFT JOIN `currency` cr ON cr.`currencyoid` = b.`currencyoid`
                LEFT JOIN `agent` g ON g.`agentoid`=b.`agentoid`
                LEFT JOIN `pmsstatus` pm ON pm.`pmsstatusoid` = br.`pmsstatusoid`
            WHERE `bookingstatusoid` in (4,7) AND ".$hl." AND br.pmsstatusoid in (2,4,5,6,7) AND br.pmsstatusoid not in ('0') AND (date(br.checkin) between :b AND :c)
            GROUP BY `bookingpaymentdtloid` ORDER BY br.checkin");

        $stmt->execute($arr);
        $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $r_room;

    }catch(PDOException $ex) {
        return array();
    }
}

function getReservationDataAllArray($db, $rxtype, $rxoid){
    try {
        $rxchain = array();
        $rxhotel = array();

        foreach($rxtype as $k => $v){
            if($v == 'chainoid'){
                $rxchain[] = $rxoid[$k];
            }else if($v == 'hoteloid'){
                $rxhotel[] = $rxoid[$k];
            }
        }

        $msg = "";
        if(count($rxchain) > 0){
            $imp = implode(",", $rxchain);
            $msg .= "AND h.chainoid IN (".$imp.") ";
        }
        if(count($rxhotel) > 0){
            $imp = implode(",", $rxhotel);
            $msg .= "AND h.hoteloid IN (".$imp.") ";
        }

        if($msg != ""){
            $stmt = $db->prepare("SELECT bookingoid, bookingnumber, bookingtime, title, firstname, lastname, c.phone, c.mobilephone, c.email,
                    checkin, checkout, DATEDIFF(checkout, checkin) AS diff, br.room, br.adult, br.child, currencycode, b.grandtotal, b.bookingtime, rn.roomnumber,
                    b.bm_name, bm.bm_name as bm_name1, countryname, pm.status, pm.`pmsstatusoid`, brd.bookingroomdtloid, g.agentname, g.agenttypeoid, bookingstatusoid, h.hotelname
                FROM `booking` b
                    INNER JOIN `bookingroom` br USING(`bookingoid`)
                    INNER JOIN `bookingroomdtl` brd USING(`bookingroomoid`)
                    INNER JOIN `customer` c USING(`custoid`)
                    INNER JOIN `hotel` h ON h.`hoteloid` = b.`hoteloid`
                    LEFT JOIN `currency` cr ON cr.`currencyoid` = b.`currencyoid`
                    LEFT JOIN `roomoffer` ro ON ro.`roomofferoid` = br.`roomofferoid`
                    LEFT JOIN `room` r ON r.`roomoid` = ro.`roomoid`
                    LEFT JOIN `roomnumber` rn ON brd.`roomnumberoid` = rn.`roomnumberoid`
                    LEFT JOIN `bookingmarket` bm ON bm.`bookingmarketoid` = b.`bookingmarketoid`
                    LEFT JOIN `country` cn ON cn.`countryoid` = c.`countryoid`
                    LEFT JOIN `pmsstatus` pm ON pm.`pmsstatusoid` = br.`pmsstatusoid`
                    LEFT JOIN `agent` g ON g.`agentoid`=b.`agentoid`
                WHERE `bookingstatusoid` in (4,5,7) ".$msg." AND br.pmsstatusoid not in ('0') AND brd.reconciled = '0'
                GROUP BY `bookingoid` DESC");

            $stmt->execute(array());
            $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $r_room;
        }else{
            return array();
        }

    }catch(PDOException $ex) {
        return array();
    }
}

function getReservationDataInHouse($db, $hoteloid, $checkin, $stay){
    try {
        $msg = "";
        if($checkin != 0){
            $date = date('Y-m-d', strtotime($checkin));
            $msg .= "AND checkin = '".$date."' ";
        }
        if($stay != 0){
            $date = date('Y-m-d', strtotime($stay));
            $msg .= "AND (checkin <= '".$date."' AND checkout >= '".$date."')";
        }

        $stmt = $db->prepare("SELECT bookingoid, bookingnumber, bookingtime, title, firstname, lastname, c.phone, c.mobilephone, c.email,
                checkin, checkout, DATEDIFF(checkout, checkin) AS diff, br.room, br.adult, br.child, currencycode, b.grandtotal, b.bookingtime, rn.roomnumber,
                b.bm_name, bm.bm_name as bm_name1, countryname, pm.status, pm.`pmsstatusoid`, brd.bookingroomdtloid, g.agentname, g.agenttypeoid, bookingstatusoid, br.total, b.totalroom, b.totalcharge, b.paid, b.paid_balance
            FROM `booking` b
                INNER JOIN `bookingroom` br USING(`bookingoid`)
                INNER JOIN `bookingroomdtl` brd USING(`bookingroomoid`)
                INNER JOIN `customer` c USING(`custoid`)
                LEFT JOIN `currency` cr ON cr.`currencyoid` = b.`currencyoid`
                LEFT JOIN `roomoffer` ro ON ro.`roomofferoid` = br.`roomofferoid`
                LEFT JOIN `room` r ON r.`roomoid` = ro.`roomoid`
                LEFT JOIN `roomnumber` rn ON brd.`roomnumberoid` = rn.`roomnumberoid`
                LEFT JOIN `bookingmarket` bm ON bm.`bookingmarketoid` = b.`bookingmarketoid`
                LEFT JOIN `country` cn ON cn.`countryoid` = c.`countryoid`
                LEFT JOIN `pmsstatus` pm ON pm.`pmsstatusoid` = br.`pmsstatusoid`
                LEFT JOIN `agent` g ON g.`agentoid`=b.`agentoid`
            WHERE `bookingstatusoid` in (4) AND b.hoteloid=:a AND br.pmsstatusoid in (4,5,6) AND br.pmsstatusoid not in ('0') AND brd.reconciled = '0' ".$msg."
            GROUP BY `bookingoid` ORDER BY checkin ASC");

        $stmt->execute(array(':a'=>$hoteloid));
        $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $r_room;

    }catch(PDOException $ex) {
        return array();
    }
}

function getReservationChargesRange($db, $hoteloid, $from, $to){
    $startperiode = date('Y-m-d', strtotime($from));
    $lastperiode = date('Y-m-d', strtotime($to));

    $hl = "";
    if($hoteloid == '0'){
        $hotels = getAssignedHotels($db);
        if(count($hotels) <= 0){
            $hl .= "b.hoteloid='0'";
        }else{
            $tmp = array();
            foreach ($hotels as $v) {
                $tmp[] = $v['hoteloid'];
            }
            $hl .= "b.hoteloid IN (".implode(',', $tmp).")";
        }
        $arr = array(':b'=>$startperiode, ':c'=>$lastperiode);
    }else{
        $hl = "b.hoteloid=:a";
        $arr = array(':a'=>$hoteloid, ':b'=>$startperiode, ':c'=>$lastperiode);
    }
    try {
        $stmt = $db->prepare("SELECT tp.`pos`, bc.`bookingoid`, `product`, `qty`, `price`, bc.`total`, bc.`created`, bc.`createdby`, `displayname`, c.`currencycode`, title, firstname, lastname, br.checkin, br.checkout
            FROM `bookingcharges` bc
            	LEFT JOIN `typepos` tp USING(`typeposoid`)
                LEFT JOIN `users` us ON bc.`createdby` = us.`username`
                INNER JOIN `booking` b ON b.`bookingoid` = bc.`bookingoid`
                INNER JOIN `bookingroom` br ON b.`bookingoid` = br.`bookingoid`
                INNER JOIN `currency` c ON b.`currencyoid` = c.`currencyoid`
                INNER JOIN `customer` cu USING(`custoid`)
            WHERE `bookingstatusoid` in (4,7) AND ".$hl." AND br.pmsstatusoid in (2,4,5,6,7) AND br.pmsstatusoid not in ('0') AND (date(br.checkin) between :b AND :c)
            GROUP BY `bookingchargesoid`
            ORDER BY br.checkin");

        $stmt->execute($arr);
        $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $r_room;

    }catch(PDOException $ex) {
        return array();
    }
}

function getOccupancyData($db, $hoteloid, $month, $year){
    try {
        $stmt = $db->prepare("SELECT `name` AS roomtype, count(`roomnumberoid`) AS available,
                (SELECT SUM(bdt.`total`) FROM `bookingroom` br INNER JOIN `booking` b USING(`bookingoid`) INNER JOIN `bookingroomdtl` bdt USING(`bookingroomoid`) INNER JOIN `roomoffer` ro USING(`roomofferoid`) WHERE br.`pmsstatusoid`!=0 AND `bookingstatusoid` in (4) AND br.pmsstatusoid in (2,4,5,6) AND ro.`roomoid`=r.`roomoid` AND `hoteloid`=:v AND b.`currencyoid`='1' AND MONTH(bdt.date) = :b1 AND YEAR(bdt.date) = :c1) AS roomrevidr,
                (SELECT SUM(bdt.`total`) FROM `bookingroom` br INNER JOIN `booking` b USING(`bookingoid`) INNER JOIN `bookingroomdtl` bdt USING(`bookingroomoid`) INNER JOIN `roomoffer` ro USING(`roomofferoid`) WHERE br.`pmsstatusoid`!=0 AND `bookingstatusoid` in (4) AND br.pmsstatusoid in (2,4,5,6) AND ro.`roomoid`=r.`roomoid` AND `hoteloid`=:w AND b.`currencyoid`='2' AND MONTH(bdt.date) = :b2 AND YEAR(bdt.date) = :c2) AS roomrevusd,
                (SELECT COUNT(bdt.`bookingroomdtloid`) FROM `bookingroom` br INNER JOIN `booking` b USING(`bookingoid`) INNER JOIN `bookingroomdtl` bdt USING(`bookingroomoid`) INNER JOIN `roomoffer` ro USING(`roomofferoid`) WHERE br.`pmsstatusoid`!=0 AND `bookingstatusoid` in (4) AND br.pmsstatusoid in (2,4,5,6) AND ro.`roomoid`=r.`roomoid` AND `hoteloid`=:x AND MONTH(bdt.date) = :b3 AND YEAR(bdt.date) = :c3) AS roomsold,
                (SELECT SUM(br.`adult`) FROM `bookingroom` br INNER JOIN `booking` b USING(`bookingoid`) INNER JOIN `bookingroomdtl` bdt USING(`bookingroomoid`) INNER JOIN `roomoffer` ro USING(`roomofferoid`) WHERE br.`pmsstatusoid`!=0 AND `bookingstatusoid` in (4) AND br.pmsstatusoid in (2,4,5,6) AND ro.`roomoid`=r.`roomoid` AND `hoteloid`=:y AND MONTH(bdt.date) = :b4 AND YEAR(bdt.date) = :c4) AS pax
            FROM `room` r
                LEFT JOIN `roomnumber` rn USING(`roomoid`)
            WHERE `hoteloid`=:a AND r.`publishedoid` not in (3) AND rn.`publishedoid` not in (3)
            GROUP BY `name`");

        $stmt->execute(array(':a'=>$hoteloid, ':b1'=>$month, ':c1'=>$year, ':b2'=>$month, ':c2'=>$year, ':b3'=>$month, ':c3'=>$year, ':b4'=>$month, ':c4'=>$year,
        ':v'=>$hoteloid,':w'=>$hoteloid,':x'=>$hoteloid,':y'=>$hoteloid));
        $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $r_room;

    }catch(PDOException $ex) {
        return array();
    }
}

function formatedInvoiceNagisa($idnuminv, $aidnuminv, $invrawdate){
  switch(strlen($idnuminv)){
    case 1: $idnuminv='000'.$idnuminv; break;
    case 2: $idnuminv='00'.$idnuminv; break;
    case 3: $idnuminv='0'.$idnuminv; break;
  }
  switch(strlen($aidnuminv)){
    case 1: $aidnuminv='000'.$aidnuminv; break;
    case 2: $aidnuminv='00'.$aidnuminv; break;
    case 3: $aidnuminv='0'.$aidnuminv; break;
  }
  if(empty($invrawdate) or $invrawdate=='0000-00-00' or $invrawdate=='1970-01-01'){
      $noinvoice = "-";
  }else{
      $invdate = date('d-M-Y', strtotime($invrawdate));
      $invmonth = date('m', strtotime($invrawdate));
      $invyear = date('Y', strtotime($invrawdate));
      $noinvoice = $idnuminv."/NBPM-".$aidnuminv."/".$invmonth."/".$invyear;
  }
  return $noinvoice;
}
?>
