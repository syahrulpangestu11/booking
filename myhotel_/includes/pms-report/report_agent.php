<?php include('includes/bootstrap.php'); ?>
<script src="<?php echo $base_url?>/lib/plugins/chartjs/Chart.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.4/Chart.bundle.min.js" type="text/javascript"></script>
<script>
    $(document).ready(function() {
        var bdall = $('input[name="bdall"]:checked').val();
        var ciall = $('input[name="ciall"]:checked').val();

        $('#btfilter').click(function(){
            bdall = $('input[name="bdall"]:checked').val();
            ciall = $('input[name="ciall"]:checked').val(); console.log(bdall+"-"+ciall);

            $('#example').children('tbody').css('display', 'none');
            $('#xload').css('display', '');
            setTimeout(function(){
                table.draw();
                $('#example').children('tbody').css('display', '');
                $('#xload').css('display', 'none');
            }, 3000);
        });

        $( "#startdate1" ).datepicker({
    		defaultDate: "0",
    		dateFormat: "dd M y",
    		changeMonth: true, changeYear:true,
    		onClose: function( selectedDate ) {
                $( "#enddate1" ).datepicker( "option", "minDate", selectedDate );
    		},
    		onSelect: function(dateText) {
                //table.draw();
    		}
        });
        $( "#enddate1" ).datepicker({
          	defaultDate: "0",
    		dateFormat: "dd M y",
          	changeMonth: true, changeYear:true,
         	onClose: function( selectedDate ) {
            	$( "#startdate1" ).datepicker( "option", "maxDate", selectedDate );
          	},
    		onSelect: function(dateText) {
                //table.draw();
    		}
        });

        $( "#startdate2" ).datepicker({
    		defaultDate: "0",
    		dateFormat: "dd M y",
    		changeMonth: true, changeYear:true,
    		onClose: function( selectedDate ) {
                $( "#enddate2" ).datepicker( "option", "minDate", selectedDate );
    		},
    		onSelect: function(dateText) {
                //table.draw();
    		}
        });
        $( "#enddate2" ).datepicker({
          	defaultDate: "0",
    		dateFormat: "dd M y",
          	changeMonth: true, changeYear:true,
         	onClose: function( selectedDate ) {
            	$( "#startdate2" ).datepicker( "option", "maxDate", selectedDate );
          	},
    		onSelect: function(dateText) {
                //table.draw();
    		}
        });

        $('#example').on('click', 'a.print', function() {
            $(this).parent().children('form.reservation-card').submit();
        });
        $('#example').on('click', 'a.detail', function() {
            $(this).parent().parent().find('form.view-reservation-detail').submit();
        });
        $('#btexport').click(function(){
            $('#exporttoexcel').submit();
        });
        $('input[name="bdall"]').change(function(){
            var n = $(this).val();
            if(n == '1'){
                $('#bdall').css('display', 'inline-block');
            }else{
                $('#bdall').css('display', 'none');
            }
        });
        $('input[name="ciall"]').change(function(){
            var n = $(this).val();
            if(n == '1'){
                $('#ciall').css('display', 'inline-block');
            }else{
                $('#ciall').css('display', 'none');
            }
        });

        $('#xload').css('display', 'none');

        $('#btfilter').click(function(){
            $('#xload').css('display', '');
            $('#loaddata').html("");

            setTimeout(function (){
                var c = $("#zhotel").val();
                $.post('<?php echo $base_url; ?>/includes/pms-report/ajax/report_agent.php',  $('form#form-filter').serialize(), function(response){
                    $('#loaddata').html(response);
                    $('#xload').css('display', 'none');
                });
            }, 2000);
        });

        $(document).on('click','#btexport',function(){
            $('#exporttoexcel').submit();
        });
    });
</script>
<style>
    .content .form-group label {
        padding-right:10px;
    }
    .content .form-group label, .content .form-group select, .content .form-group input {
        margin-bottom:5px;
    }
    a.detail {
        color: #1200ff;
    }
    a.detail:hover {
        text-decoration: underline;
    }
    .btn {
        border-radius: 3px;
        -webkit-box-shadow: none;
        box-shadow: none;
        border: 1px solid transparent;
    }
    .btn-primary {
        color: #fff;
        background-color: #337ab7;
        border-color: #2e6da4;
    }
    .btn-primary {
        background-color: #3c8dbc;
        border-color: #367fa9;
    }
    #example {
        border: 1px solid #999;
    }
    #example tbody td, #example tfoot th {
        border-top: 1px solid #999;
    }
    #example tbody td, #example thead th, #example tfoot th {
        border-left: 1px dashed #ddd;
        padding: 2px 2px;
    }
    #example tbody tr td:first-child, #example thead tr th:first-child, #example tfoot tr th:first-child {
        border-left: 0;
    }
    .bleft {border-left: 1px dashed #ddd !important;}
    .bnoleft {border-left: 0 !important;}
    #example thead, #example tfoot {
        background-color: #d4ebec;
    }
</style>

<section class="content-header">
    <h1>
       	Report PMS -  Production by Agent
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Report PMS</a></li>
    </ol>
</section>

<section class="content">
  <div class="row">
      <div class="box box-form">
          <div class="box-body" style="font-size:12px;">
            <form class="form-inline" id="form-filter">
              <div class="row">
                <div class="form-group">
                    <label>Property : </label>
                    <?php
                    if($hoteloid > 0){
                        $stmt = $db->prepare("SELECT hoteloid, hotelcode, hotelname FROM hotel WHERE hoteloid=:a AND publishedoid='1' ORDER BY hotelname");
    	                $stmt->execute(array(':a' => $hoteloid));
                        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
                    }else{
                        $s_chain = "SELECT ua.oid, ua.type FROM users u INNER JOIN userassign ua USING (useroid) WHERE ua.type IN ('chainoid', 'hoteloid') AND u.useroid = '".$_SESSION['_oid']."'";
                		$q_chain = $db->query($s_chain);
                		$r_chain = $q_chain->fetchAll(PDO::FETCH_ASSOC);
                		$rxoid = array(); $rxtype = array();
                		foreach($r_chain as $row){
                			$rxoid[] = $row['oid'];
                			$rxtype[] = $row['type'];
                      echo "<input type='hidden' name='chain' value = '".$row['oid']."'>";
                		}

                		$rxchain = array(); $rxhotel = array();
                        foreach($rxtype as $k => $v){
                            if($v == 'chainoid'){
                                $rxchain[] = $rxoid[$k];
                            }else if($v == 'hoteloid'){
                                $rxhotel[] = $rxoid[$k];
                            }
                        }

                        $msg = "";
                        if(count($rxchain) > 0){
                            $imp = implode(",", $rxchain);
                            $msg .= "AND chainoid IN (".$imp.")";
                        }
                        if(count($rxhotel) > 0){
                            $imp = implode(",", $rxhotel);
                            $msg .= "AND hoteloid IN (".$imp.")";
                        }

                		if($msg != ""){
                            $stmt = $db->prepare("SELECT hoteloid, hotelcode, hotelname FROM hotel WHERE publishedoid='1' ".$msg." ORDER BY hotelname");
        	                $stmt->execute(array());
                            $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
                		}else{
                		    $data = array();
                		}
                    }
                    ?>
                    <select name="hotel" class="form-control" id="zhotel">
                        <option value="">Show All Hotel</option>
                        <?php
                            foreach($data as $vals){
                                echo '<option value="'.$vals['hoteloid'].'">'.$vals['hotelname'].'</option>';
                            }
                        ?>
                    </select>

                </div>
              </div>
              <div class="row">
                <div class="form-group">
                    <label>Booking Periode Date:</label>
                    <input type="radio" name="bdall" value="0" checked>All &nbsp;
                    <input type="radio" name="bdall" value="1">Range &nbsp;
                    <div id="bdall" style="display:none;">
                        <label>from:</label>
                        <input type="text" class="form-control" name="bstartdate" id="startdate1" readonly="readonly" value="<?=date("d M y")?>" style="width:100px">
                        <label>to:</label>
                        <input type="text" class="form-control" name="benddate" id="enddate1" readonly="readonly" value="<?=date("d M y", strtotime("+1 Month"))?>" style="width:100px">
                    </div>
                </div>
              </div>
              <div class="row">
                <div class="form-group">
                    <label>Stay Periode:</label>
                    <input type="radio" name="ciall" value="0">All &nbsp;
                    <input type="radio" name="ciall" value="1" checked>Range &nbsp;
                    <div id="ciall" style="display:inline-block;">
                        <label>from:</label>
                        <input type="text" class="form-control" name="cstartdate" id="startdate2" readonly="readonly" value="<?=date("1 M y")?>" style="width:100px">
                        <label>to:</label>
                        <input type="text" class="form-control" name="cenddate" id="enddate2" readonly="readonly" value="<?=date("t M y")?>" style="width:100px">
                    </div>
                     &nbsp;
                    <button type="button" class="btn btn-primary" id="btfilter">SHOW REPORT</button>
                </div>
              </div>
            </form>
          </div>
      </div>
  </div>
  <div class="row">
      <div class="box box-form">
          <div class="box-body" style="font-size: 12px;">
              <div id="xload" align="center"><img src="../images/loading.gif" style="width:10%"></div>

              <div id="loaddata"></div>
          </div>
      </div>
  </div>
</section>
