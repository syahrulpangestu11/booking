<?php
    $month = array("January","February","March","April","May","June","July","August","September","October","November","December");
    $year = array();
    $curyear = intval(date("Y"));
    for($i=$curyear;$i>=2017;$i--){
        $year[] = $i;
    }
?>
<script>
    $(document).ready(function() {
        $('#xload').css('display', 'none');
        
        $('#btfilter1').click(function(){
            $('#xload').css('display', '');
            $('#loaddata').html("");
            
            setTimeout(function (){
                var a = $("#startdate2").val();
                var b = $("#enddate2").val();
                var c = $("#zhotel").val();
                
                if(a.trim()!="" && b.trim()!="" && c.trim()!=""){
                    $.post('<?php echo $base_url; ?>/includes/pms-report/ajax/report_othercharge.php', { 'a':a, 'b':b, 'c':c }, function(response){
                        $('#loaddata').html(response);
                        $('#xload').css('display', 'none');
                    });
                }else{
                    alert("All field can't be empty!");
                    $('#xload').css('display', 'none');
                }
            }, 2000);
        });
        
        $(document).on('click','#btexport',function(){
            $('#exporttoexcel').submit();
        });
        
        $( "#startdate2" ).datepicker({
    		defaultDate: "0",
    		dateFormat: "dd M y",
    		changeMonth: true, changeYear:true,
    		onClose: function( selectedDate ) {
                $( "#enddate2" ).datepicker( "option", "minDate", selectedDate );
    		},
    		onSelect: function(dateText) {}
        });
        $( "#enddate2" ).datepicker({
          	defaultDate: "0",
    		dateFormat: "dd M y",
          	changeMonth: true, changeYear:true,
         	onClose: function( selectedDate ) {
            	$( "#startdate2" ).datepicker( "option", "maxDate", selectedDate );
          	},
    		onSelect: function(dateText) {}
        });
    });
</script>
<style>
    .content .form-group label {
        padding-right:10px;
    }
    .content .form-group label, .content .form-group select, .content .form-group input {
        margin-bottom:5px;
    }
    a.detail {
        color: #1200ff;
    }
    a.detail:hover {
        text-decoration: underline;
    }
    .btn {
        border-radius: 3px;
        -webkit-box-shadow: none;
        box-shadow: none;
        border: 1px solid transparent;
    }
    .btn-primary {
        color: #fff;
        background-color: #337ab7;
        border-color: #2e6da4;
    }
    .btn-primary {
        background-color: #3c8dbc;
        border-color: #367fa9;
    }
    #example {
        border: 1px solid #999;
    }
    #example tbody td, #example tfoot th {
        border-top: 1px solid #999;
    }
    #example tbody td, #example thead th, #example tfoot th {
        border-left: 1px dashed #ddd;
        padding: 2px 2px;
    }
    #example tbody tr td:first-child, #example thead tr th:first-child, #example tfoot tr th:first-child {
        border-left: 0;
    }
    .bleft {border-left: 1px dashed #ddd !important;}
    .bnoleft {border-left: 0 !important;}
    #example thead, #example tfoot {
        background-color: #d4ebec;
    }
    html,body,aside{ min-height:auto !important; }
    #xcontent{ background-color:inherit; }
</style>
<section class="content-header">
    <h1>
       	Report PMS - Other Charge Report
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Report PMS</a></li>
    </ol>
</section>
<section class="content">
<div class="row">
    <div class="box box-form">
        <div class="box-body" style="font-size:13px;">
            <div class="form-group">
                <label>Property : </label>
                <?php
                if($hoteloid > 0){
                    $stmt = $db->prepare("SELECT hoteloid, hotelcode, hotelname FROM hotel WHERE hoteloid=:a AND publishedoid='1' ORDER BY hotelname");
	                $stmt->execute(array(':a' => $hoteloid));
                    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
                }else{
                    $s_chain = "SELECT ua.oid, ua.type FROM users u INNER JOIN userassign ua USING (useroid) WHERE ua.type IN ('chainoid', 'hoteloid') AND u.useroid = '".$_SESSION['_oid']."'";
            		$q_chain = $db->query($s_chain);
            		$r_chain = $q_chain->fetchAll(PDO::FETCH_ASSOC);
            		$rxoid = array(); $rxtype = array();
            		foreach($r_chain as $row){
            			$rxoid[] = $row['oid'];
            			$rxtype[] = $row['type'];
            		}
            		
            		$rxchain = array(); $rxhotel = array();
                    foreach($rxtype as $k => $v){
                        if($v == 'chainoid'){
                            $rxchain[] = $rxoid[$k];
                        }else if($v == 'hoteloid'){
                            $rxhotel[] = $rxoid[$k];
                        }
                    }
                    
                    $msg = "";
                    if(count($rxchain) > 0){
                        $imp = implode(",", $rxchain);
                        $msg .= "AND chainoid IN (".$imp.")";
                    }
                    if(count($rxhotel) > 0){
                        $imp = implode(",", $rxhotel);
                        $msg .= "AND hoteloid IN (".$imp.")";
                    }
            		
            		if($msg != ""){
                        $stmt = $db->prepare("SELECT hoteloid, hotelcode, hotelname FROM hotel WHERE publishedoid='1' ".$msg." ORDER BY hotelname");
    	                $stmt->execute(array());
                        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
            		}else{
            		    $data = array();
            		}
                }
                ?>
                <select class="form-control" id="zhotel">
                    <option value="">- Select Hotel -</option>
                    <?php
                        if(count($data) > 1){
                            echo '<option value="0">All Hotels</option>';
                        }
                        foreach($data as $vals){
                            echo '<option value="'.$vals['hoteloid'].'">'.$vals['hotelname'].'</option>';
                        }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label>Check-in Date</label>
                <div id="ciall" style="display:inline-block;">
                    <label>From:</label>
                    <input type="text" class="form-control" name="cistartdate" id="startdate2" readonly="readonly" value="<?=date("d M y", strtotime("-1 Month"))?>" style="width:75px">
                    <label>To:</label>
                    <input type="text" class="form-control" name="cienddate" id="enddate2" readonly="readonly" value="<?=date("d M y")?>" style="width:75px">
                </div>
                    &nbsp; 
                <button type="button" class="btn btn-primary" id="btfilter1">SHOW REPORT</button>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="box box-form">
        <div class="box-body" style="font-size: 12px;">
            <div id="xload" align="center"><img src="../images/loading.gif" style="width:10%"></div>
            
            <div id="loaddata"></div>
        </div>
    </div>
</div>
</section>