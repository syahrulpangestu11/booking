<?php
try{
  session_start();
  error_reporting(0);
  include("../../../conf/connection.php");
  include("../../php-function.php");
  include("../../pms-lite/class-pms-lite.php");
  $pmslite = new PMSLite($db);

  $startmonth = $_POST['month'];
  $startyear = $_POST['year'];
  $date = $startyear."-".$startmonth."-01";

  $day_in_month = array();
  $periode = array();
  $periode_startdate = array();
  for($i=0; $i<=2; $i++){
    $new_date = date("Y-m-d", strtotime($date." +".$i." month"));
    $exp_date = explode("-",$new_date);
    $d = cal_days_in_month(CAL_GREGORIAN, $exp_date[1], $exp_date[0]);
    array_push($day_in_month, $d);
    array_push($periode, date("F Y", strtotime($new_date)));
    array_push($periode_startdate, $new_date);
  }

  $pmslite->setPeriodePMS($startdate, $enddate);

  ?>

  <div style="float:right">
        <button type="button" class="btn btn-primary" id="btexport" style="margin-bottom:5px">Export to Excel</button>
        <form id="exporttoexcel" action="<?=$base_url?>/myhotel/pms-lite/view/rp3forecast_xls.php" method="post" target="_blank">
            <input name="month" type="hidden" value="<?=$_POST['month']?>">
            <input name="year" type="hidden" value="<?=$_POST['year']?>">
            <input name="hotelcode" type="hidden" value="<?=$_POST['hotelcode']?>">
        </form>
        </div>
    </div>

  <?php

  $s_hotelchain = "select h.hoteloid, h.hotelcode, h.hotelname from hotel h inner join chain c using (chainoid) where h.chainoid in (select oid from userassign where type = 'chainoid' and useroid = '".$_SESSION['_oid']."') and h.publishedoid = '1'";
  $filter = array();
  if(!empty($_POST['hotelcode'])){
    $s_hotelchain = "select h.hoteloid, h.hotelcode, h.hotelname from hotel h inner join chain c using (chainoid) where h.publishedoid = '1'";
    array_push($filter, "h.hotelcode = '".$_POST['hotelcode']."'");
  }
  if(count($filter) > 0){
		$combine_filter = implode(' and ',$filter);
		$s_hotelchain = $s_hotelchain.' and '.$combine_filter;
	}
  $q_hotelchain = $db->query($s_hotelchain);
  $r_hotelchain = $q_hotelchain->fetchAll(PDO::FETCH_ASSOC);
  foreach($r_hotelchain as $hotelchain){
?>
  <table id="inventory" cellpadding="0" cellspacing="0" style="margin-bottom:20px;">
    <thead>
      <tr>
        <?php foreach($periode as $column_periode){
          $s_allocation[$column_periode] = 0;
          $s_available[$column_periode] = 0;
          $s_booked[$column_periode] = 0;
          $s_totalidr[$column_periode] = 0;
          $s_totalusd[$column_periode] = 0;
        ?>
        <th colspan="7"><?=$column_periode?></th>
        <?php } ?>
      </tr>
      <tr>
        <?php foreach($periode as $column_periode){ ?>
        <th>Date</th>
        <th>Allocated</th>
        <th>Available</th>
        <th>Booked</th>
        <th>Booked%</th>
        <th>IDR</th>
        <th>USD</th>
        <?php } ?>
      </tr>
    </thead>
    <tbody>
      <?php
      for($i = 1; $i <= max($day_in_month); $i++){
      ?>
        <tr>
          <?php
            foreach($periode_startdate as $key => $startdate){
              $date = date("Y-m-d",strtotime($startdate." +".($i-1)." day"));
              $setdate = date("d M",strtotime($date));
              if($i > $day_in_month[$key]){ echo "<td></td><td></td><td></td><td></td><td></td><td></td><td></td>"; }else{
                $q_data = "select count(brd.bookingroomdtloid) as booked, SUM(CASE WHEN brd.currencyoid = 1 THEN brd.total ELSE 0 END) AS total_idr, SUM(CASE WHEN brd.currencyoid = 2 THEN brd.total ELSE 0 END) AS total_usd from booking b inner join hotel h using (hoteloid) inner join bookingroom br using (bookingoid) inner join bookingroomdtl brd using (bookingroomoid) where b.bookingstatusoid = '4' and br.pmsstatusoid not in ('0','1') and b.hoteloid = '".$hotelchain['hoteloid']."' and brd.date = '".$date."' and brd.reconciled = '0' and brd.pmsreconciled = '0' group by b.hoteloid";
                //echo $q_data;
                $stmt = $db->prepare($q_data);
                $stmt->execute();
                $data = $stmt->fetch(PDO::FETCH_ASSOC);

                $stmt = $db->prepare("select count(rn.roomnumberoid) as jmlroom from roomnumber rn inner join room r using (roomoid) where r.hoteloid = :a and rn.publishedoid not in (3) and r.publishedoid in (1)");
            		$stmt->execute(array(':a' => $hotelchain['hoteloid']));
            		$result = $stmt->fetch(PDO::FETCH_ASSOC);
                $allocation = $result['jmlroom'];
                $available = $allocation - $data['booked'];
                $booked_pc = ceil(($data['booked']/$allocation) * 100);
          ?>
          <td align="center"><?=$setdate?></td>
          <td align="center"><?=$allocation?></td>
          <td align="center"><?=$available?></td>
          <td align="center"><?=number_format($data['booked'])?></td>
          <td align="center"><?=$booked_pc?>%</td>
          <td align="right"><?=number_format($data['total_idr'], 2)?></td>
          <td align="right"><?=number_format($data['total_usd'], 2)?></td>
          <?php
                $s_allocation[date("F Y",strtotime($date))]+=intval($allocation);
                $s_available[date("F Y",strtotime($date))]+=intval($available);
                $s_booked[date("F Y",strtotime($date))]+=number_format($data['booked']);
                $s_totalidr[date("F Y",strtotime($date))]+=$data['total_idr'];
                $s_totalusd[date("F Y",strtotime($date))]+=$data['total_usd'];
              }
          } ?>
        </tr>
      <?php
      }
      ?>
    </tbody>
    <thead>
      <tr>
        <?php foreach($periode as $column_periode){ ?>
        <th style="text-align:center">Total</th>
        <th style="text-align:center"><?=$s_allocation[$column_periode]?></th>
        <th style="text-align:center"><?=$s_available[$column_periode]?></th>
        <th style="text-align:center"><?=$s_booked[$column_periode]?></th>
        <th style="text-align:center"><?=ceil(($s_booked[$column_periode]/$s_allocation[$column_periode]) * 100)?>%</th>
        <th style="text-align:right"><?=number_format($s_totalidr[$column_periode], 2)?></th>
        <th style="text-align:right"><?=number_format($s_totalusd[$column_periode],2)?></th>
        <?php } ?>
      </tr>
    </thead>
  </table>
<?php
  }
}catch(Exception $e){
  echo $e->getMessage();
}
?>
