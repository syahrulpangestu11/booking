<?php
try{
    session_start();
	error_reporting(0);
	include ('../../../conf/connection.php');

    $month = $_POST['a'];
    $year = $_POST['b'];
    $hoteloid = $_POST['c'];

    $stmt = $db->prepare("SELECT hoteloid, hotelcode, hotelname FROM hotel WHERE hoteloid=:a AND publishedoid='1' ORDER BY hotelname");
    $stmt->execute(array(':a' => $hoteloid));
    $hotel = $stmt->fetch(PDO::FETCH_ASSOC);

    include("../function_report.php");

}catch(PDOException $e){
    echo "Failed to load the data!"; //. $e->getMessage();
    die();
}
?>
<div align="center">
    <h2 style="text-transform:uppercase"><?=$hotel['hotelname']?></h2>
    <h2>OCCUPANCY REPORT</h2>
    <h2>MONTH : <?=$month?> <?=$year?></h2>
</div>
<br><br>
<div style="margin-bottom:10px">
    <div style="float:left"></div>
    <div style="float:right">
        <button type="button" class="btn btn-primary" id="btexport" style="margin-bottom:5px">Export to Excel</button>
        <form id="exporttoexcel" action="<?=$base_url?>/myhotel/pms-lite/view/rpoccupancy_xls.php" method="post" target="_blank">
            <input name="a" type="hidden" value="<?=$month?>">
            <input name="b" type="hidden" value="<?=$year?>">
            <input name="c" type="hidden" value="<?=$hoteloid?>">
        </form>
    </div>
    <div style="clear:both"></div>
</div>
<table id="example" class="display" cellspacing="0" width="100%">
<thead>
    <tr>
        <th>#</th>
        <th>Room Type</th>
        <th>Number of Room</th>
        <th>Room Available</th>
        <th>Room Revenue</th>
        <th>Sold Rooms</th>
        <th>Sold Rooms%</th>
        <th>Pax</th>
        <th>ARR/ADR</th>
        <th>RevPar</th>
    </tr>
</thead>
<tbody>
    <?php
    $armonth = array("January"=>1,"February"=>2,"March"=>3,"April"=>4,"May"=>5,"June"=>6,"July"=>7,"August"=>8,"September"=>9,"October"=>10,"November"=>11,"December"=>12);
    $data = getOccupancyData($db, $hoteloid, $armonth[$month], $year);
    $titles = getCustTitle($db);

    $no=0; $a1=0; $a2=0; $a3=0; $a4=0; $a6=0; $a7=0; $a8=0;
    foreach($data as $row){
        $no++;
        $roomtype = $row['roomtype'];
        $available = $row['available'];
        $roomrevidr = $row['roomrevidr'];
        $roomrevusd = $row['roomrevusd'];
        $roomsold = $row['roomsold'];
        $dateofmonth = cal_days_in_month(CAL_GREGORIAN, $armonth[$month], $year);
        $roomsoldpc = $roomsold / ($dateofmonth * $available) * 100;
        $pax = $row['pax'];

        echo '
            <tr>
                <td align="center">'.$no.'</td>
                <td>'.$roomtype.'</td>
                <td align="center">'.$available.'</td>
                <td align="center">'.$dateofmonth * $available.'</td>
                <td>IDR '.number_format($roomrevidr, 2, '.', ',').'<br>USD '.number_format($roomrevusd, 2, '.', ',').'</td>
                <td align="center">'.$roomsold.'</td>
                <td align="center">'.round($roomsoldpc,2).' %</td>
                <td align="center">'.$pax.'</td>
                <td>IDR '.number_format($roomrevidr/$roomsold, 2, '.', ',').'<br>USD '.number_format($roomrevusd/$roomsold, 2, '.', ',').'</td>
                <td>IDR '.number_format($roomrevidr/($dateofmonth * $available), 2, '.', ',').'<br>USD '.number_format($roomrevusd/($dateofmonth * $available), 2, '.', ',').'</td>
            </tr>
        ';

        $a1+=$available;
        $a2+=$dateofmonth * $available;
        $a31+=$roomrevidr;
        $a32+=$roomrevusd;
        $a4+=$roomsold;
        $a6+=$pax;
        $a71+=$roomrevidr/$roomsold;
        $a72+=$roomrevusd/$roomsold;

    }
    ?>
</tbody>
<tfoot>
    <tr>
        <th colspan="2">TOTAL</th>
        <th><?=$a1?></th>
        <th><?=$a2?></th>
        <th align="left"><?='IDR '.number_format($a31, 2, '.', ',').'<br>USD '.number_format($a32, 2, '.', ',')?></th>
        <th><?=$a4?></th>
        <th><?=round($a4*100/$a2,2).' %'?></th>
        <th><?=$a6?></th>
        <th align="left"><?='IDR '.number_format($a31/$a4, 2, '.', ',').'<br>USD '.number_format($a32/$a4, 2, '.', ',')?></th>
        <th align="left"><?='IDR '.number_format($a31/$a2, 2, '.', ',').'<br>USD '.number_format($a32/$a2, 2, '.', ',')?></th>
    </tr>
</tfoot>
</table>
