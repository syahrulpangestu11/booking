<?php
  session_start();
  error_reporting(0);
  include ('../../../conf/connection.php');

  $filter = array();

  if(!empty($_POST['hotel'])){
    $hoteloid = $_POST['hotel'];
    $stmt = $db->prepare("SELECT hoteloid, hotelcode, hotelname FROM hotel WHERE hoteloid=:a AND publishedoid='1' ORDER BY hotelname");
    $stmt->execute(array(':a' => $hoteloid));
    $hotel = $stmt->fetch(PDO::FETCH_ASSOC);
    array_push($filter, "b.hoteloid = '".$hoteloid."'");
  }else{
    $chainoid = $_POST['chain'];
    $stmt = $db->prepare("SELECT chainoid, name as chainname FROM chain WHERE chainoid=:a");
    $stmt->execute(array(':a' => $chainoid));
    $chain = $stmt->fetch(PDO::FETCH_ASSOC);
    array_push($filter, "h.chainoid = '".$chainoid."'");
  }

  if($_POST['bdall'] == 1){
    $start_book = date('Y-m-d',strtotime($_POST['bstartdate']));
    $end_book = date('Y-m-d',strtotime($_POST['benddate']));
    array_push($filter, "(DATE(b.bookingtime) >= '".$start_book."' and DATE(b.bookingtime) <= '".$end_book."')");
  }
  if($_POST['ciall'] == 1){
    $start_arrival = date('Y-m-d',strtotime($_POST['cstartdate']));
    $end_arrival = date('Y-m-d',strtotime($_POST['cenddate']));
    array_push($filter, "(brd.date between '".$start_arrival."' and '".$end_arrival."') and brd.pmsreconciled = '0' and brd.reconciled = '0'");
  }

  if(count($filter)){ $query_filter = ' and '.implode(' and ', $filter); }else{  $query_filter = ''; }

    $q_country_report = "select ct.countryname, count(distinct(b.bookingoid)) as jmlbooking, count(brd.bookingroomdtloid) as roomnight, SUM(CASE WHEN br.currencyoid = 1 THEN brd.total ELSE 0 END) AS total_idr, SUM(CASE WHEN br.currencyoid = 2 THEN brd.total ELSE 0 END) AS total_usd from booking b inner join customer c using (custoid) left join country ct using (countryoid) inner join hotel h using (hoteloid) inner join bookingroom br using (bookingoid) inner join bookingroomdtl brd using (bookingroomoid)  where b.bookingstatusoid = '4' and br.pmsstatusoid in ('2', '4','5','6') ".$query_filter." group by ct.countryoid";
    //echo $q_country_report;
    $stmt = $db->prepare($q_country_report);
    $stmt->execute();
    $r_country_report = $stmt->fetchAll(PDO::FETCH_ASSOC);
?>
<div align="center">
    <h2 style="text-transform:uppercase"><?=$hotel['hotelname']?></h2>
    <h2> Production by Country Report</h2>
    <?php if($_POST['bdall'] == 1){ ?><h2>Booking Periode: <?=date('d F Y',strtotime($_POST['bstartdate']))?> - <?=date('d F Y',strtotime($_POST['benddate']))?></h2><?php } ?>
    <?php if($_POST['ciall'] == 1){ ?><h2>Stay Periode : <?=date('d F Y',strtotime($_POST['cstartdate']))?> - <?=date('d F Y',strtotime($_POST['cenddate']))?></h2><?php } ?>
</div>

<div class="row">
  <div class="col-md-6">
    <canvas id="myChart"></canvas>
  </div>
  <div class="col-md-6">
    <div style="margin-bottom:10px">
      <div style="float:left"></div>
      <div style="float:right">
        <button type="button" class="btn btn-primary" id="btexport" style="margin-bottom:5px;">Export to Excel</button>
        <form id="exporttoexcel" action="<?=$base_url?>/myhotel/pms-lite/view/rpcountry_xls.php" method="post" target="_blank">
          <?php
            foreach($_POST as $nameinput => $value){
              echo "<input type='hidden' name='".$nameinput."' value='".$value."'>";
            }
          ?>
        </form>
      </div>
    </div>
    <div style="clear:both"></div>

    <table id="example" class="display" cellspacing="0" width="100%">
      <thead>
          <tr>
              <th rowspan="2" class="text-center">Country</th>
              <th rowspan="2" class="text-center">Total Reservation</th>
              <th rowspan="2" class="text-center">Total Room Night</th>
              <th colspan="2" class="text-center">Amount</th>
          </tr>
          <tr>
              <th class="text-center">IDR</th>
              <th class="text-center">USD</th>
          </tr>
      </thead>
      <tbody>
        <?php
          $countryname = array();
          $country_idr = array(); $country_usd = array();
          foreach($r_country_report as $ar){
            array_push($countryname, $ar['countryname']);
            array_push($country_idr, $ar['total_idr']);
            array_push($country_usd, $ar['total_usd']);

            $a1+=$ar['jmlbooking'];
            $a2+=$ar['roomnight'];
            $a3+=$ar['total_idr'];
            $a4+=$ar['total_usd'];
        ?>
        <tr>
          <td><?=$ar['countryname']?></td>
          <td align="right"><?=$ar['jmlbooking']?></td>
          <td align="right"><?=$ar['roomnight']?></td>
          <td align="right"><?php if($ar['total_idr'] > 0){ echo number_format($ar['total_idr'], 2); }?></td>
          <td align="right"><?php if($ar['total_usd'] > 0){ echo number_format($ar['total_usd'], 2); }?></td>
        </tr>
        <?php
        }
        ?>
      </tbody>
      <tfoot>
        <tr>
          <th>TOTAL</th>
          <th class="text-right"><?=$a1?></th>
          <th class="text-right"><?=$a2?></th>
          <th class="text-right"><?=number_format($a3, 2)?></th>
          <th class="text-right"><?=number_format($a4, 2)?></th>
        </tr>
      </tfoot>
    </table>
  </div>
</div>

<script>
var ctx = document.getElementById("myChart").getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: [<?php echo '"'.implode('","', $countryname).'"'; ?>],
        datasets: [{
            label: 'Amount IDR',
            data: [<?php echo '"'.implode('","', $country_idr).'"'; ?>],
            backgroundColor: 'rgba(255, 188, 0, 1)',
            borderWidth: 1,
            yAxisID: 'first-y-axis'
        },
        {
            label: 'Amount USD',
            data: [<?php echo '"'.implode('","', $country_usd).'"'; ?>],
            backgroundColor: 'rgba(51, 175, 20, 1)',
            borderWidth: 1,
            yAxisID: 'second-y-axis'
        }]
    },
    options: {
      scales: {
          yAxes: [{
              id: 'first-y-axis',
              type: 'linear',
              position: 'left'
          }, {
              id: 'second-y-axis',
              type: 'linear',
              position: 'right'
          }]
      }
    }
});
</script>
