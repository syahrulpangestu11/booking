<?php
session_start();
error_reporting(0);
include ('../../../conf/connection.php');
include("../function_report.php");

if(empty($_POST['hoteloid'])){
    $s_chain = "SELECT ua.oid, ua.type FROM users u INNER JOIN userassign ua USING (useroid) WHERE ua.type IN ('chainoid', 'hoteloid') AND u.useroid = '".$_SESSION['_oid']."'";
    $q_chain = $db->query($s_chain);
    $r_chain = $q_chain->fetchAll(PDO::FETCH_ASSOC);
    $rxoid = array(); $rxtype = array();
    foreach($r_chain as $row){
        $rxoid[] = $row['oid'];
        $rxtype[] = $row['type'];
    }

    $rxchain = array(); $rxhotel = array();
    foreach($rxtype as $k => $v){
        if($v == 'chainoid'){
            $rxchain[] = $rxoid[$k];
        }else if($v == 'hoteloid'){
            $rxhotel[] = $rxoid[$k];
        }
    }

    $msg = "";
    if(count($rxchain) > 0){
        $imp = implode(",", $rxchain);
        $msg .= "AND chainoid IN (".$imp.")";
    }
    if(count($rxhotel) > 0){
        $imp = implode(",", $rxhotel);
        $msg .= "AND hoteloid IN (".$imp.")";
    }

    if($msg != ""){
        $stmt = $db->prepare("SELECT hoteloid, hotelcode, hotelname FROM hotel WHERE publishedoid='1' ".$msg." ORDER BY hotelname");
        $stmt->execute(array());
        $data1 = $stmt->fetchAll(PDO::FETCH_ASSOC);
    }else{
        $data1 = array();
    }

    $hotel = array();
    foreach($data1 as $dt){
        $hotel[] = $dt['hoteloid'];
    }
}else{
    $hotel = array();
    $hotel[] = $_POST['hoteloid'];
}
  
$roomtype = $_POST['rt'];
$status = $_POST['status'];
$market = $_POST['market'];
$agent = $_POST['agent'];
$bdall = $_POST['bdall'];
$bdstartdate = $_POST['bdstartdate'];
$bdenddate = $_POST['bdenddate'];
$ciall = $_POST['ciall'];
$cistartdate = $_POST['cistartdate'];
$cienddate = $_POST['cienddate'];

$msg = "";
if(!empty($roomtype)){
    $msg .= "AND r.`roomoid`='".$roomtype."' ";
}else{
    if(!empty($hotel)){
        $msg .= "AND b.`hoteloid` IN (".implode(",",$hotel).") ";
    }else{
        $msg .= "AND b.`hoteloid` = 0 ";
    }
}
if(!empty($status)){
    $msg .= "AND pm.`pmsstatusoid`='".$status."' ";
}
if(!empty($market)){
    $msg .= "AND bm.`bookingmarketoid`='".$market."' ";
}
if(!empty($agent)){
    $msg .= "AND g.`agenttypeoid`='".$agent."' ";
}
if(!empty($bdall)){
    $msg .= "AND b.`bookingtime`>='".date("Y-m-d", strtotime($bdstartdate))."' AND b.`bookingtime`<='".date("Y-m-d", strtotime($bdenddate))."' ";
}
if(!empty($ciall)){
    $msg .= "AND br.`checkin`>='".date("Y-m-d", strtotime($cistartdate))."' AND br.`checkin`<='".date("Y-m-d", strtotime($cienddate))."' ";
}

$stmt = $db->prepare("SELECT b.hoteloid, bookingoid, bookingnumber, bookingtime, title, firstname, lastname, c.phone, c.mobilephone, c.email, r.name as roomname, 
        checkin, checkout, DATEDIFF(checkout, checkin) AS diff, br.room, br.adult, br.child, currencycode, b.grandtotal, b.bookingtime, rn.roomnumber,
        b.bm_name, bm.bm_name as bm_name1, countryname, pm.status, pm.`pmsstatusoid`, brd.bookingroomdtloid, b.agentoid, g.agentname, g.agenttypeoid, bookingstatusoid
    FROM `booking` b
        INNER JOIN `bookingroom` br USING(`bookingoid`)
        INNER JOIN `bookingroomdtl` brd USING(`bookingroomoid`)
        INNER JOIN `customer` c USING(`custoid`)
        LEFT JOIN `currency` cr ON cr.`currencyoid` = b.`currencyoid`
        LEFT JOIN `roomoffer` ro ON ro.`roomofferoid` = br.`roomofferoid`
        LEFT JOIN `room` r ON r.`roomoid` = ro.`roomoid`
        LEFT JOIN `roomnumber` rn ON brd.`roomnumberoid` = rn.`roomnumberoid`
        LEFT JOIN `bookingmarket` bm ON bm.`bookingmarketoid` = b.`bookingmarketoid`
        LEFT JOIN `country` cn ON cn.`countryoid` = c.`countryoid`
        LEFT JOIN `pmsstatus` pm ON pm.`pmsstatusoid` = br.`pmsstatusoid`
        LEFT JOIN `agent` g ON g.`agentoid`=b.`agentoid`
    WHERE `bookingstatusoid` in (4,5,7) ".$msg." AND br.pmsstatusoid not in ('0')
    GROUP BY `bookingoid` DESC"); // AND brd.reconciled = '0'

$stmt->execute(array());
$r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
?>
<table id="example" class="display" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>#</th>
            <th>Res ID</th>
            <th>Date</th>
            <th>Guest Name</th>
            <th>C/I</th>
            <th>C/O</th>
            <th>Stay</th>
            <th>Room# / Room Type</th>
            <th>Pax</th>
            <th>Status</th>
            <th>Amount</th>
            <th>Booking Market</th>
            <th>Agent</th>
            <th>Check in Card</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $titles = getCustTitle($db);
        $no=0;
        foreach($r_room as $row){
            $hoteloid = $row['hoteloid'];
            if(in_array($hoteloid,$avoid_adjust)){
                $adjust = 1;
            }

            $no++;
            $bookingoid = $row['bookingoid'];
            $bookingnum = $row['bookingnumber'];
            if(isset($titles[$row['title']])) $title = $titles[$row['title']]; else $title = $row['title'];
            $guestname = $title." ".$row['firstname']." ".$row['lastname'];
            $checkin = date("d M y", strtotime($row['checkin']));
            $checkout = date("d M y", strtotime($row['checkout']));
            $diff = $row['diff'];
            $roomnumber = empty($row['roomnumber']) ? "-" : $row['roomnumber'];
            $room = $row['room'];
            $roomname = empty($room)?$row['roomname']:$room;
            $adult = $row['adult'];
            $child = $row['child'];
            if($row['bookingstatusoid'] == "5"){
                $status = "Cancelled";
            }else if($row['bookingstatusoid'] == "7"){
                $status = "No Show";
            }else{
                $status = $row['status']; 
            }
            $grandtotal = number_format($row['grandtotal'] * $adjust, 2, '.', ',');
            $currcode = empty($row['currencycode']) ? "IDR" : $row['currencycode'];
            $market = empty($row['bm_name']) ? empty($row['bm_name1']) ? "-" : $row['bm_name1'] : $row['bm_name'];
            $bookingdate = date("d M y", strtotime($row['bookingtime']));
            $agentname = empty($row['agentname']) ? "-" : $row['agentname'];
            $agenttypeoid = empty($row['agenttypeoid']) ? "0" : $row['agenttypeoid'];
        ?>
        <tr class="gradeX">
            <td><?=$no?></td>
            <td><a class="detail" title="View Detail"><?=$bookingnum?></a></td>	
            <td><?=$bookingdate?></td>
            <td><?=$guestname?></td>
            <td class="acin" x="<?=$checkin?>"><?=$checkin?></td>
            <td><?=$checkout?></td>
            <td><?=$diff." night"?></td>
            <td><?=$roomnumber." / ".$roomname?></td>
            <td><?=$adult?>(A) <?=$child?>(C)</td>
            <td><?=$status?></td>
            <td><?=$currcode." ".$grandtotal?></td>
            <td><?=$market?></td>
            <td class="agt" x="<?=$agenttypeoid?>"><?=$agentname?></td>
            <td align="center">
                <?php if(in_array($_SESSION['_typepmsusr'], array('5','6'))){ ?>
                <span>-</span>
                <?php }else{?>
                <a class="print" title="Print Reservation Card"><i class="fa fa-lg fa-print"></i></a>
                <form class="reservation-card" action="<?=$base_url?>/myhotel/pms-lite/view/reservation-card.php" method="post" target="_blank">
                    <input name="bookingroomdtl" type="hidden" value="<?=$row['bookingroomdtloid']?>">
                    <input name="hotel" type="hidden" value="<?=$hoteloid?>">
                </form>
                <?php }?>
                <form class="view-reservation-detail" action="<?=$base_url?>/myhotel/pms-lite/reservation-detail" method="post" target="_blank">
                    <input name="bookingoid" type="hidden" value="<?=$bookingoid?>">
                    <input name="hotel" type="hidden" value="<?=$hoteloid?>">
                </form>
            </td>
        </tr>
        <?php
        }
        ?>
    </tbody>
</table>
<div align="right">
    <button type="button" class="btn btn-primary" id="btexport">Export to Excel</button>
    <form id="exporttoexcel" action="<?=$base_url?>/myhotel/pms-lite/view/rsvlist_xls1.php" method="post" target="_blank">
        <input name="rt" type="hidden" value="<?=$_POST['rt'];?>">
        <input name="status" type="hidden" value="<?=$_POST['status'];?>">
        <input name="market" type="hidden" value="<?=$_POST['market'];?>">
        <input name="agent" type="hidden" value="<?=$_POST['agent'];?>">
        <input name="bdall" type="hidden" value="<?=$_POST['bdall'];?>">
        <input name="bdstartdate" type="hidden" value="<?=$_POST['bdstartdate'];?>">
        <input name="bdenddate" type="hidden" value="<?=$_POST['bdenddate'];?>">
        <input name="ciall" type="hidden" value="<?=$_POST['ciall'];?>">
        <input name="cistartdate" type="hidden" value="<?=$_POST['cistartdate'];?>">
        <input name="cienddate" type="hidden" value="<?=$_POST['cienddate'];?>">
    </form>
</div>