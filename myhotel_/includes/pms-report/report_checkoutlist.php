<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous"> -->

<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    });
</script>
<section class="content-header">
    <h1>
       	Report PMS - Check-out List
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Report PMS</a></li>
    </ol>
</section>
<section class="content">
    


<div class="row">
<div class="box box-form"><div class="box-body" style="font-size: 12px;">

<div class="form-group">
    Select Room Type:   
    <input type="checkbox" id="myChec1">Superior    
    <input type="checkbox" id="myCheck">Deluxe 
    <input type="checkbox" id="myCheck">Suites
    &nbsp;&nbsp;&nbsp;&nbsp;
    <!-- <select name="status" class="form-control">
        <option value="volvo">All Status</option>
        <option value="saab">Check In</option>
        <option value="fiat">Check Out</option>
        <option value="audi">Reserve</option>
        <option value="volvo">Temporary Reserve</option>
    </select>
        
    <select name="market" class="form-control">
        <option value="volvo">All Market</option>
        <option value="volvo">Direct website</option>
        <option value="saab">direct booking</option>
        <option value="fiat">Travel agent</option>
        <option value="audi">Corporate</option>
        <option value="volvo">Online Travel Agent (OTA)</option>
        <option value="saab">Vacation Rental</option>
        <option value="fiat">Wedding & event</option>
        <option value="audi">Affiliate</option>
    </select> -->
    
    <label>Between:</label> &nbsp;&nbsp;
    <input type='text' class="form-control" value="2017-10-01"/>
    <label>And:</label> &nbsp;&nbsp;
    <input type='text' class="form-control" value="2017-10-31"/>
</div>
        </div></div>
</div>




    <div class="row">
        <div class="box box-form"><div class="box-body" style="font-size: 12px;">
            <table id="example" class="display" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Res ID</th>
                    <th>Guest Name</th>
                    <th>Stay Duration</th>
                    <th>Room# / Type</th>
                    <th>Pax</th>
                    <th>Status</th>
                    <th>Amount</th>
                    <th>Preferences/Notes</th>
                    <th>Check in Card</th>
                    
                </tr>
                </thead>
                <tbody>
                <tr class="gradeX">
                    <td>1</td>
                    <td>100311</td>	
                    <td>Austin Holden</td>
                    <td>10 Oct - 18 Oct (7)</td>
                    <td>131/Luxury</td>
                    <td>1(A) 0(C)</td>
                    <td>Checked Out</td>
                    <td>Rp 7,381,481.80</td>
                    <td>&nbsp;</td>
                    <td align="center"><a href="#"><i class="fa fa-lg fa-print"></i></a> <a href="#"><i class="fa fa-lg fa-envelope-o"></i> </a></td>
                </tr>
                <tr class="gradeX">
                    <td>2</td>
                    <td>100312</td>	
                    <td>Jeane Julia</td>
                    <td>09 Oct - 18 Oct (7)</td>
                    <td>121/SUperior</td>
                    <td>2(A) 0(C)</td>
                    <td>Checked Out</td>
                    <td>Rp 2,481,5481.80</td>
                    <td>&nbsp;</td>
                    <td align="center"><a href="#"><i class="fa fa-lg fa-print"></i></a> <a href="#"><i class="fa fa-lg fa-envelope-o"></i> </a></td>
                </tr>
                    <tr class="gradeX">
                    <td>3</td>
                    <td>100313</td>	
                    <td>Nugraha Adi</td>
                    <td>11 Oct - 12 Oct (7)</td>
                    <td>134/Luxury</td>
                    <td>1(A) 0(C)</td>
                    <td>Checked Out</td>
                    <td>Rp 1,381,559.00</td>
                    <td>&nbsp;</td>
                    <td align="center"><a href="#"><i class="fa fa-lg fa-print"></i></a> <a href="#"><i class="fa fa-lg fa-envelope-o"></i> </a></td>
                </tr>
                    <tr class="gradeX">
                    <td>4</td>
                        <td>100211</td>	
                    <td>Zesfi Surya</td>
                    <td>08 Oct - 18 Oct (7)</td>
                    <td>111/Luxury</td>
                    <td>2(A) 0(C)</td>
                    <td>Checked Out</td>
                    <td>Rp 7,381,481.80</td>
                    <td>&nbsp;</td>
                    <td align="center"><a href="#"><i class="fa fa-lg fa-print"></i></a> <a href="#"><i class="fa fa-lg fa-envelope-o"></i> </a></td>
                </tr>
                    <tr class="gradeX">
                    <td>5</td>
                    <td>100211</td>	
                    <td>Hanny K.</td>
                    <td>11 Oct - 14 Oct (7)</td>
                    <td>111/Luxury</td>
                    <td>1(A) 1(C)</td>
                    <td>Checked Out</td>
                    <td>Rp 3,381,481.80</td>
                    <td>&nbsp;</td>
                    <td align="center"><a href="#"><i class="fa fa-lg fa-print"></i></a> <a href="#"><i class="fa fa-lg fa-envelope-o"></i> </a></td>
                </tr>  
                    <tr class="gradeX">
                        <td>6</td>
                    <td>100311</td>	
                    <td>Lorensio D.</td>
                    <td>10 Oct - 15 Oct (7)</td>
                    <td>101/Luxury</td>
                    <td>2(A) 0(C)</td>
                    <td>Checked Out</td>
                    <td>Rp 7,586,481.40</td>
                    <td>&nbsp;</td>
                    <td align="center"><a href="#"><i class="fa fa-lg fa-print"></i></a> <a href="#"><i class="fa fa-lg fa-envelope-o"></i> </a></td>
                </tr>                                  
                
                </tbody>
            </table>
        </div></div>
    </div>
</section>