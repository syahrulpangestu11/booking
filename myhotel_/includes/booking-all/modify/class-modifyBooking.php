<?php
class modifyBooking{
	var $bookingoid;
	var $bookingnumber;
	var $bookingstatus;
	var $hoteloid;
	var $hotelcode;
	var $status_cancel = array(5, 8);
	var $bookingroomoid;
	var $commission;
	
	public function __construct($db){
		$this->db = $db;
	}
	
	public function verifyBooking($bookingnumber, $pin){
		$stmt = $this->db->prepare("select bookingoid, bookingnumber, bookingstatusoid, gbhpercentage, hoteloid from booking where bookingnumber = :a and pin = :b");
		$stmt->execute(array(':a' => $bookingnumber, ':b' => $pin));
		
		$count = $stmt->rowCount();
		
		if($count == 0){
			$found = false;
		}else{
			$result = $stmt->fetch(PDO::FETCH_ASSOC);
			$this->bookingoid		= $result['bookingoid'];
			$this->bookingnumber	= $result['bookingnumber'];
			$this->bookingstatus	= $result['bookingstatusoid'];
			$this->hoteloid			= $result['hoteloid'];
			$this->commission		= $result['gbhpercentage'];
			$found = true;
		}
		return $found;
	}
	
	public function validToModify(){
		$stmt = $this->db->prepare("select min(br.checkin) as min_checkin from bookingroom br inner join booking b using (bookingoid) where br.bookingoid = :a");
		$stmt->execute(array(':a' => $this->bookingoid));
		$currentbooking = $stmt->fetch(PDO::FETCH_ASSOC);
		
		$status_cancel = array(5, 8);
		
		if(strtotime($currentbooking['min_checkin']) <= strtotime(date('Y-m-d')) or in_array($this->bookingstatus, $this->status_cancel)){
			$enable_modify = false;
		}else{
			$enable_modify = true;
		}
		return $enable_modify;
	}
	
	public function setSession(){
		$_SESSION['thebuking_modify_bookingnumber']	= $this->bookingnumber;
		$_SESSION['thebuking_modify_pin']			= $this->pin;
		$_SESSION['thebuking_modify_status']		= true;
	}
	
	public function masterBooking(){
		$s_master_booking	= "select b.*, b.note as guestrequest, c.*, cr.*, bs.note as status, bp.*, ct.countryname, cc.cardname, h.banner, h.hotelname, h.email as hotelemail, h.stars, h.hoteloid
		from booking b
		inner join hotel h using (hoteloid)
		inner join bookingstatus bs using (bookingstatusoid)
		inner join currency cr using (currencyoid)
		inner join customer c using (custoid)
		inner join country ct using (countryoid)
		left join bookingpayment bp using (bookingoid)
		left join creditcard cc using (cardoid)
		where b.bookingoid = '".$this->bookingoid."' group by b.bookingoid";
		$stmt				= $this->db->query($s_master_booking);
		$master_booking		= $stmt->fetch(PDO::FETCH_ASSOC);
		return $master_booking;
	}
	
	public function masterHotel(){
		$s_master_hotel	= "select h.*, c.cityname, s.statename, ct.countryname from hotel h inner join city c using (cityoid) inner join state s using (stateoid) inner join country ct using (countryoid) where hoteloid = '".$this->hoteloid."'";
		$stmt			= $this->db->query($s_master_hotel);
		$master_hotel	= $stmt->fetch(PDO::FETCH_ASSOC);
		return $master_hotel;
	}
	
	public function detailBooking(){
		$s_detail_booking	= "select br.*, c.currencycode from bookingroom br left join currency c using (currencyoid) where br.bookingoid = '".$this->bookingoid."' and bookingroomstatusoid not in (3)";
		$stmt				= $this->db->query($s_detail_booking);
		$detail_booking		= $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $detail_booking;
	}
	
	public function summaryBookingConfirm(){
		$s_summary_booking	= "select count(bookingroomoid) as numberroom, checkin, checkout from bookingroom br left join currency c using (currencyoid) where br.bookingoid = '".$this->bookingoid."' and bookingroomstatusoid not in (3) group by checkin, checkout";
		$stmt				= $this->db->query($s_summary_booking);
		$summary_booking	= $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $summary_booking;
	}
	
	public function summaryBooking(){
		$s_summary_booking	= "select count(bookingroomoid) as numberroom, checkin, checkout from bookingroom br left join currency c using (currencyoid) where br.bookingoid = '".$this->bookingoid."' group by checkin, checkout";
		$stmt				= $this->db->query($s_summary_booking);
		$summary_booking	= $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $summary_booking;
	}
	
	public function setBooking($bookingoid){
		$this->bookingoid = $bookingoid;
		
		$s_booking	= "select b.gbhpercentage from booking b where b.bookingoid = '".$this->bookingoid."'";
		$stmt		= $this->db->query($s_booking);
		$booking	= $stmt->fetch(PDO::FETCH_ASSOC);
		
		$this->commission	= $booking['gbhpercentage'];
	}
	
	public function setBookingRoom($bookingroomoid){
		$this->bookingroomoid = $bookingroomoid;
		if(empty($this->bookingoid)){
			$s_booking	= "select b.bookingoid, b.gbhpercentage from bookingroom br inner join booking b using (bookingoid) where br.bookingroomoid = '".$this->bookingroomoid."'";
			$stmt		= $this->db->query($s_booking);
			$booking	= $stmt->fetch(PDO::FETCH_ASSOC);
			
			$this->bookingoid	= $booking['bookingoid'];
			$this->commission	= $booking['gbhpercentage'];
		}
	}
	
	public function setBookingRoomID($bookingroomoid){
		$this->bookingroomoid = $bookingroomoid;
	}

	public function setBookingStatus($bookingstatusoid){
		$this->bookingstatus = $bookingstatusoid;
	}


	public function cancellationFee(){
		$s_cancelroom	= "select br.*, c.currencycode from bookingroom br inner join currency c using (currencyoid) inner join booking b using (bookingoid) where bookingroomoid = '".$this->bookingroomoid."'";
		$stmt			= $this->db->query($s_cancelroom);
		$cancelroom		= $stmt->fetch(PDO::FETCH_ASSOC);
		
		$select_total	= "SELECT DATEDIFF('".$cancelroom['checkin']."','".date('Y-m-d')."') AS DiffDate";
		$stmt			= $this->db->query($select_total);
		$diffdate		= $stmt->fetch(PDO::FETCH_ASSOC);
		$before_checkin	= $diffdate['DiffDate'];
		
		if($cancelroom['termcondition'] == "non refundable"){
			$cancellationfee	=  $cancelroom['total'];
		}else if($cancelroom['termcondition'] == "free cancellation"){
			if($before_checkin > $cancelroom['cancellationday']){
				$cancellationfee = 0;
			}else{
				if($cancelroom['cancellationtype'] == "full amount"){
					$cancellationfee = $cancelroom['total'];
				}else{
					$cancellationfee = 0;
					for($i = 0; $i < $cancelroom['cancellationtypenight']; $i++){
						$date = date('Y-m-d', strtotime($cancelroom['checkin'].' +'.$i.' day'));
						
						$q_breakdownrate	= "select total from bookingroomdtl where bookingroomoid = '".$cancelroom['bookingroomoid']."' and reconciled = '0' and `date` = '".$date."'";
						$stmt				= $this->db->query($q_breakdownrate);
						$breakdowntotal		= $stmt->fetch(PDO::FETCH_ASSOC);
						$cancellationfee	= $cancellationfee + $breakdowntotal['total'];
					}
				}
			}
		}
		
		if($cancellationfee == 0){
			$show_cancellationfee = "free";
		}else{
			$show_cancellationfee = $cancelroom['currencycode']." ".number_format($cancellationfee);
		}
		return array($cancellationfee, $show_cancellationfee);
	}
	
	public function recalculateBooking(){
		/* from cancelled room */
		if($this->bookingstatus == '4'){
			$s_cancellationamount	= "select sum(br.cancellationamount) as cancellationamount from bookingroom br where br.bookingoid = '".$this->bookingoid."' and br.bookingroomstatusoid = '3'";
			$stmt					= $this->db->query($s_cancellationamount);
			$scncl					= $stmt->fetch(PDO::FETCH_ASSOC);
		}else{
			$s_cancellationamount	= "select sum(br.cancellationamount) as cancellationamount from bookingroom br where br.bookingoid = '".$this->bookingoid."'";
			$stmt					= $this->db->query($s_cancellationamount);
			$scncl					= $stmt->fetch(PDO::FETCH_ASSOC);
		}
		/* from confirmed & reconcile room */
		$s_confirmedamount	= "select sum(br.total) as confirmamount from bookingroom br where br.bookingoid = '".$this->bookingoid."' and br.bookingroomstatusoid in ('1','2')";
		$stmt				= $this->db->query($s_confirmedamount);
		$scnfrm				= $stmt->fetch(PDO::FETCH_ASSOC);
		/* from extra */
		$s_extraamount	= "select sum(be.total) as confirmextra from bookingextra be where be.bookingoid = '".$this->bookingoid."'";
		$stmt			= $this->db->query($s_extraamount);
		$extra			= $stmt->fetch(PDO::FETCH_ASSOC);

		
		$grandtotal			= $scnfrm['confirmamount'] + $extra['confirmextra'];
		
		if($this->bookingstatus == '4'){
			$finalrevenue		= $scncl['cancellationamount'] +  $scnfrm['confirmamount'] + $extra['confirmextra'];
		}else{
			$finalrevenue		= $scncl['cancellationamount'];
		}
		
		$finalcommission	= $finalrevenue * $this->commission / 100;
		$nethotel			= $finalrevenue - $finalcommission;
		
		$stmt = $this->db->prepare("update booking set grandtotal = :a, hotelcollect = :b, gbhcollect = :c, cancellationamount= :d where bookingoid = :id");
		$stmt->execute(array(':a' => $grandtotal, ':b' => $nethotel, ':c' => $finalcommission, ':d' => $scncl['cancellationamount'], ':id' => $this->bookingoid));
	}	
}
?>