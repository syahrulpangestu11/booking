<?php
	include("../ajax-include-file.php");

$startdate = date("Y-m-d", strtotime($_REQUEST['startdate']));
$enddate = date("Y-m-d", strtotime($_REQUEST['enddate']));
$status = $_REQUEST['status'];
$viewby = $_REQUEST['viewby'];
$hoteloid = $_REQUEST['ho'];
$agentoid = $_REQUEST['ag'];

if($hoteloid == "all"){
	$where_hoteloid = "";
}else{
	$where_hoteloid = " and h.hoteloid = '".$hoteloid."' ";
}
if($agentoid == "all"){
	$where_agentoid = "";
}else{
	$where_agentoid = " and b.agentoid = '".$agentoid."' ";
}
if($status == "all"){
	$where_booking_status = "";
}else{
	$where_booking_status = " and b.bookingstatusoid = '".$status."' and pmsstatus = '0' ";
}

$query = "select b.bookingnumber, b.bookingoid, b.bookingtime, bs.note as status, CONCAT(firstname, ' ', lastname) as guestname,
	cr.currencycode, b.grandtotal, b.grandtotalr,h.hotelname
	#,ag.agentname
	,'' as agentname
	from booking b
	inner join currency cr using (currencyoid)
	inner join hotel h using (hoteloid)
	inner join bookingstatus bs using (bookingstatusoid)
	inner join customer c using (custoid)
	#inner join agent ag using (agentoid)
	left join bookingpayment using (bookingoid)
	left join bookingroom br using (bookingoid)
	where (DATE(".$viewby.") >= '".$startdate."' and DATE(".$viewby.") <= '".$enddate."')
	".$where_hoteloid.$where_agentoid.$where_booking_status."
	and pmsstatus = '0' group by b.bookingoid";
    // echo "<script>console.log(".json_encode($query).");</script>";
?>
<table class="table table-fill table-fill-centered">
    <tr>
    	<td>Booking Number</td>
        <td>Booking Status</td>
        <!-- <td>Agent </td> -->
        <td>Hotel </td>
        <td>Guest </td>
        <td>Book Date</td>
        <td>Checkin Date</td>
        <td>Checkout Date</td>
        <td>Grand Total</td>
		<td>Manage</td>
    </tr>
<?php
	try {
		$stmt = $db->query($query);
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
			$r_s = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_s as $row){
				$bookdate = date("d F Y", strtotime($row['bookingtime']));

				unset($checkin); unset($checkout);
				$checkin = array(); $checkout = array();

				try {
					$stmt = $db->query("select checkin, checkout from bookingroom where bookingoid = '".$row['bookingoid']."' group by checkin, checkout");
					$r_cico = $stmt->fetchAll(PDO::FETCH_ASSOC);
					foreach($r_cico as $arr){
						array_push($checkin , date("d F Y", strtotime($arr['checkin'])));
						array_push($checkout , date("d F Y", strtotime($arr['checkout'])));
					}
				}catch(PDOException $ex) {
					echo "Invalid Query";
					print($ex);
					die();
				}
?>
    <tr class="list">
		<td><b><?=$row['bookingnumber']?></b></td>
		<td style="text-align:left"><?=$row['status']?></td>
        <!-- <td style="text-align:left"><?php echo $row['agentname']; ?></td> -->
        <td style="text-align:left"><?php echo $row['hotelname']; ?></td>
        <td style="text-align:left"><?php echo $row['guestname']; ?></td>
        <td style="text-align:left"><?php echo $bookdate; ?></td>
        <td style="text-align:left"><?php foreach($checkin as $value){ echo $value."<br>"; } ?></td>
        <td style="text-align:left"><?php foreach($checkout as $value){ echo $value."<br>"; } ?></td>
        <td style="text-align:right"><?php echo $row['currencycode']." ".number_format($row['grandtotalr']); ?></td></td>
		
		<td>
			<button type='button' class='btn btn-primary open-button' uo='<?=$row['bookingnumber']?>'>Open</button>
			<!-- <button type='button' class='btn btn-warning confirm-button' uo='<?=$row['bookingnumber']?>'>Confirmation</button> -->
		</td>
    </tr>
<?php
			}
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		print($ex);
		die();
	}
?>
