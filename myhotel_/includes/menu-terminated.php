            <?php
			try {
				$stmt = $db->query("select hotelcode, chainoid from hotel where hoteloid = '".$hoteloid."'");
				$row_count = $stmt->rowCount();

				$r_hotel = $stmt->fetchAll(PDO::FETCH_ASSOC);
				foreach($r_hotel as $row){ $hcode = $row['hotelcode']; $hchain = $row['chainoid']; }
			}catch(PDOException $ex) {
				echo "Invalid Query"; die();
			}

            $s_existed_hotel_feature = "SELECT GROUP_CONCAT(featureoid) as existed from `hotelfeature` where `hoteloid` = '".$hoteloid."'";
			$stmt = $db->query($s_existed_hotel_feature);
			$r_existed_hotel_feature = $stmt->fetch(PDO::FETCH_ASSOC);
			if(!empty($r_existed_hotel_feature['existed'])){
				$subscribe_feature = explode(',', $r_existed_hotel_feature['existed']);
			}else{
				$subscribe_feature = array();
			}
			?>

<?php
    $activemenu = array("booking", "performance-reports", "commission-report", "adr-report", "inhouse-report", "booking-source", "booking-promo", "dashboard");
	if(in_array($uri2, $activemenu)){ $active = "active"; }else{ $active = ""; }
?>
<li class="treeview <?=$active?>">
    <a href="#">
        <i class="fa fa-bar-chart-o"></i> <span>Reports</span>
        <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
        <li class="<?php if($uri2=="booking"){ echo "active"; }?>"><a href="<?php echo "$base_url"; ?>/booking"><i class="fa fa-circle-o"></i> Bookings</a></li>
        <li class="<?php if($uri3=="reservation-report"){ echo "active"; }?>"><a href="<?=$base_url?>/performance-reports/reservation-report"><i class="fa fa-circle-o"></i> Reservation Report</a></li>
        <li class="<?php if($uri2=="commission-report" or $uri2=="dashboard"){ echo "active"; }?>"><a href="<?php echo "$base_url"; ?>/commission-report"><i class="fa fa-circle-o"></i> Commission Report</a></li>
        <li class="<?php if($uri2=="adr-report"){ echo "active"; }?>"><a href="<?php echo "$base_url"; ?>/adr-report"><i class="fa fa-circle-o"></i> ADR Report</a></li>
        <li class="<?php if($uri2=="inhouse-report"){ echo "active"; }?>"><a href="<?php echo "$base_url"; ?>/inhouse-report"><i class="fa fa-circle-o"></i> Inhouse Report</a></li>
        <li class="<?php if($uri2=="booking-source"){ echo "active"; }?>"><a href="<?php echo "$base_url"; ?>/booking-source"><i class="fa fa-circle-o"></i> Booking by Source</a></li>
        <li class="<?php if($uri2=="booking-promo"){ echo "active"; }?>"><a href="<?php echo "$base_url"; ?>/booking-promo"><i class="fa fa-circle-o"></i> Booking by Promocode</a></li>
    </ul>
</li>


            <script type="text/javascript">
			$(function(){
				$('a.manage').click(function(){
					var loginas_hc = '0';
					var loginas_hname = '';
					$.ajax({
						type	: 'POST', cache: false,
						url		: '<?=$base_url?>/includes/suadm/hotel/change-session.php',
						data	: { loginas : loginas_hc, hname : loginas_hname },
						success	: function(rsp){ if(rsp == "1"){ $(location).attr("href", "<?=$base_url?>/dashboard"); } }
					});
				});

			});
            </script>
