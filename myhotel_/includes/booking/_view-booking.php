<?php
	if(empty($_REQUEST['bs']) and empty($_REQUEST['sdate']) and empty($_REQUEST['edate'])){
		$start = date("d F Y");
		$end = date("d F Y",  strtotime(date("Y-m-d")." +1 month" ));
	}else{
		$start = $_REQUEST['sdate'];
		$end = $_REQUEST['edate'];
		$roomtype = $_REQUEST['bs'];
	}
	include("js.php");
?>
<section class="content-header">
    <h1>
        Bookings
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Reports</a></li>
        <li class="active">Booking</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="box">
            <div class="box-body">
                <div class="form-group">
                <form method="get" action="<?php echo $base_url; ?>/booking/">
                    <br><br>
                    <label>Booking date from </label> &nbsp;
                    <input type="text" name="sdate" placeholder="" value="<?php echo $start; ?>"> &nbsp;&nbsp;
                    <label>to </label> &nbsp;
                    <input type="text" name="edate" placeholder="" value="<?php echo $end; ?>">
                    <br><br> 
                    <label>Booking Status  : </label> &nbsp;
                    <select name="bs">
                    	<option value="">view all</option>
                        <?php
                        $sql = "select * from bookingstatus order by bookingstatusoid desc";
                        $query = mysql_query($sql);
                        while($row = mysql_fetch_array($query)){
							if($row['bookingstatusoid'] == $bs){ $selected = 'selected = "selected"';  }else{ $selected = ''; }
                        ?>
                        <option value="<?php echo $row['bookingstatusoid']; ?>" <?php echo $selected; ?>><?php echo $row['note']; ?></option>
                        <?php		
                        }
                        ?>
                    </select>&nbsp;&nbsp;
                    <button type="submit" class="pure-button find-button">Find</button>
                </form>
                </div>
			</div><!-- /.box-body -->
       </div>
    </div>
    
        <div class="row">
        <div class="box">
            <div id="data-box" class="box-body">
            </div><!-- /.box-body -->
       </div>
    </div>

</section>