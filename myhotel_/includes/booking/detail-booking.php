<?php
	include("js.php");

	$bookingnumber = $_REQUEST['no'];

	$query = "select b.*, DATE_FORMAT(bookingtime, '%W, %d %M %Y at %H:%i:%s') as bookdate, bs.note as status, bs.bookingstatusoid, cur.currencycode,
	CONCAT(firstname, ' ', lastname) as guestname, c.address, c.city, c.state, c.zipcode, c.phone, c.email,
	ctr.countryname,
	h.hotelname
	from booking b
	inner join hotel h using (hoteloid)
	inner join bookingstatus bs using (bookingstatusoid)
	inner join currency cur using (currencyoid)
	inner join customer c using (custoid)
	inner join country ctr using (countryoid)
	left join bookingpayment using (bookingoid)
	where h.hoteloid = '".$hoteloid."' and b.bookingnumber = '".$bookingnumber."'
	group by b.bookingoid";

		$stmt = $db->query($query);
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
			$r_s = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_s as $datarsv){
				$bookingoid = $datarsv['bookingoid'];
				$bookingnumber = $datarsv['bookingnumber'];
				$grandtotal = $datarsv['currencycode']." ".number_format($datarsv['grandtotal']);
				$grandtotal_r = $datarsv['currencycode']." ".number_format($datarsv['grandtotalr']);
				$grandbalance = $datarsv['currencycode']." ".number_format($datarsv['grandbalance']);
				$granddeposit = $datarsv['currencycode']." ".number_format($datarsv['granddeposit']);
				$hotelcollect = $datarsv['currencycode']." ".number_format($datarsv['hotelcollect']);
				$gbhcollect = $datarsv['currencycode']." ".number_format($datarsv['gbhcollect']);
				$gbhpercentage = $datarsv['gbhpercentage'];

				$gbhcollect_bef_r = $datarsv['currencycode']." ".number_format($datarsv['grandtotal'] * $gbhpercentage / 100);
				$hotelcollect_bef_r = $datarsv['currencycode']." ".number_format($datarsv['grandtotal'] * (100-$gbhpercentage) / 100);

				$s_room		=  $db->query("select sum(roomtotal) as roomtotal, sum(roomtotalr) as roomtotalr, sum(extrabedtotal) as extrabedtotal from bookingroom where bookingoid = '".$datarsv['bookingoid']."' group by bookingoid");
				$room		= $s_room->fetch(PDO::FETCH_ASSOC);
				$roomtotal = $datarsv['currencycode']." ".number_format($room['roomtotal']);
				$extrabedtotal = $datarsv['currencycode']." ".number_format($room['extrabedtotal']);

				$s_extra	=  $db->query("select sum(total) as extratotal from bookingextra where bookingoid = '".$datarsv['bookingoid']."' group by bookingoid");
				$extra		= $s_extra->fetch(PDO::FETCH_ASSOC);
				$extratotal = $datarsv['currencycode']." ".number_format($extra['extratotal']);

				$note = $datarsv['note'];
				$bookingstatus = $datarsv['status'];
				$bookingstatusoid = $datarsv['bookingstatusoid'];

				$updated = $datarsv['updated'];
				$updatedby = $datarsv['updatedby'];

				$roomtotal_r = $room['roomtotal'];
				$roomtotal_rr = $room['roomtotalr'];

				$show_roomtotal_r = $datarsv['currencycode']." ".number_format($room['roomtotal']);
				$show_roomtotal_rr = $datarsv['currencycode']." ".number_format($room['roomtotalr']);

				if($datarsv['promocode'] != "thebuking"){
					$promocode = $datarsv['promocode'];
					$promocode_pic = $datarsv['pcaffpicname']." / ".$datarsv['pcaffpicemail'] ;
				}else{
					$promocode = "-";
					$promocode_pic = "-";
				}

				$promocode_comm = $datarsv['currencycode']." ".number_format($datarsv['promocodecomm']);
			}
		}

		$ar_bookingroom = array();
		$ar_roomname = array(); $ar_roomoid = array();
		$ar_checkin = array(); $ar_checkout = array(); $ar_checkoutr = array();
		$ar_numberroom = array();
		$ar_child = array(); $ar_adult = array();
		$ar_total = array(); $ar_totalr = array();
		$ar_breakfast = array();
		$ar_extrabed = array();
		$ar_cancellationamount = array(); $ar_cancellationpolicy = array();
		$ar_promotion = array(); $ar_promotionoid = array();

		$q_detail = "select br.*, count(bookingroomoid) as jmlroom, sum(br.extrabed) as extrabed, c.currencycode, checkin, checkout, checkoutr
		from bookingroom br
		inner join currency c using (currencyoid)
		inner join booking b using (bookingoid)
		where b.bookingoid = '".$bookingoid."'
		group by br.bookingroomoid";

		$stmt = $db->query($q_detail);
		$row_count_room = $stmt->rowCount();
		if($row_count_room > 0) {
			$r_detail = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_detail as $rp){
				array_push($ar_bookingroom, $rp['bookingroomoid']);
				array_push($ar_roomname, $rp['room']);
				array_push($ar_roomoid, $rp['roomofferoid']);
				array_push($ar_promotion, $rp['promotion']);
				array_push($ar_promotionoid, $rp['promotionoid']);
				array_push($ar_checkin, date("d F Y", strtotime($rp['checkin'])));
				array_push($ar_checkout, date("d F Y", strtotime($rp['checkout'])));
				array_push($ar_checkoutr, date("d F Y", strtotime($rp['checkoutr'])));
				array_push($ar_numberroom, $rp['jmlroom']);
				array_push($ar_adult, $rp['adult']);
				array_push($ar_child, $rp['child']);
				array_push($ar_total, $rp['currencycode']." ".number_format($rp['total']));
				array_push($ar_totalr, $rp['currencycode']." ".number_format($rp['totalr']));
				array_push($ar_cancellationamount, $rp['currencycode']." ".number_format($rp['cancellationamount']));
				array_push($ar_cancellationpolicy, $rp['cancellationname']);
				array_push($ar_breakfast, $rp['breakfast']);
				array_push($ar_extrabed, $rp['extrabed']);
			}

			$q_max_co = "select max(checkoutr) as maxcheckout from bookingroom br where bookingoid = '".$bookingoid."'";
			$stmt = $db->query($q_max_co);
			$r_max_co = $stmt->fetch(PDO::FETCH_ASSOC);
			$max_co = $r_max_co['maxcheckout'];
		}
?>
<link rel="stylesheet" href="<?php echo $base_url; ?>/lib/bootstrap/css/bootstrap.min.css">
<link href="<?php echo $base_url?>/css/style.css?v=1" rel="stylesheet" type="text/css">
<style type="text/css">
	#detail-booking h1, #detail-booking h2{
		margin:0;
		font-weight:100;

	}
	#detail-booking h1{ font-size: 1.5em; }
	#detail-booking h2{ font-size: 1.2em; }
	#detail-booking h3{ font-size: 1em; }

	#detail-booking table { border-collapse: collapse; margin-bottom:10px; }
	#detail-booking table > tbody > tr > td { border: 1px solid #C9C9C9; padding:2px; }
	#detail-booking table.ch-table { margin-bottom:0; }
	#detail-booking table.ch-table > tbody > tr > td { border-bottom: 1px solid #434444; border-right:none; border-left:none; border-top:none; }
	#detail-booking table.ch-table > tbody > tr > td:first-of-type { padding-right:10px; }
	#detail-booking table.ch-table > tbody > tr > td:nth-of-type(2) { text-align:right; }
	#detail-booking table.ch-table > tbody > tr:last-of-type > td{ border:none; }

	table tr.list-detail{ cursor:pointer; }
	table tr.list-breakdown{ font-size: 0.85em;}

	ul.breakdown-rate li{ margin:0 0 5px; }
	ul.breakdown-rate li > label, ul.breakdown-rate li > span{ display:block; text-align:center; padding:3px 5px; }
	ul.breakdown-rate li > label{ background-color:#ced0d2; margin:0 }
	ul.breakdown-rate li > span{ border:1px solid #ced0d2; }


	.table-bordered > tbody > tr:first-child > td {
    text-align: left;
		padding: 1px 5px;
    background-color: inherit;
    color: inherit;
	}
</style>

<section class="content-header">
    <h1>
        Booking Number : <?php echo $bookingnumber; ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i>  Reports </a></li>
        <li class="active">Booking</li>
        <li class="active">Detail <?php echo $bookingnumber; ?></li>
    </ol>
</section>
<section class="content" id="detail-booking">
  <div class="row">
    <div class="box box-form">
      <div class="box-body">
        <div class="row">
          <div class="col-md-12 text-center">
						<h1><?php echo $datarsv['guestname']; ?></h1>
						<?php echo $datarsv['city']; ?>, <?php echo $datarsv['countryname']; ?><br>
						&#9742; <?php echo $datarsv['phone']; ?> &#9993; <?php echo $datarsv['email']; ?><br>
						<?php echo $datarsv['bookdate']; ?> | Booking Number : <?php echo $datarsv['bookingnumber']; ?> | <i class="fa fa-lock"></i> PIN : <?php echo $datarsv['pin']; ?>
          </div>
        </div>
				<div class="row">
          <div class="col-md-12 text-center" style="padding:20px 0 10px;">
						<h1><?php echo $datarsv['hotelname']; ?></h1>
          </div>
        </div>
				<div class="row">
          <div class="col-md-12">
						<?php if($row_count_room > 0) { ?>
						<table class="table table-bordered">
							<thead>
								<tr><th>#</th><th>Room Type</th><th>Date</th><th>Occupancy</th><th>Breakfast Included</th><th>No. of Extra Bed</th><th>Total</th></tr>
							</thead>
							<tbody>
							<?php
								$r = 0;
								foreach($ar_roomname as $key => $value){
							?>
								<tr class='list-detail'>
									<td><i class='fa fa-caret-down'></i> <?=++$r?></td>
									<td><?=$value?><br><?=$ar_promotion[$key]?></td>
									<td>checkin : <?=$ar_checkin[$key]?><br>checkout : <?=$ar_checkoutr[$key]?></td>
									<td><i class='fa fa-male'></i> <?=$ar_adult[$key]?> <i class='fa fa-child'></i> <?=$ar_child[$key]?>
									</td>
									<td><?=$ar_breakfast[$key]?></td>
									<td><?=$ar_extrabed[$key]?></td>
									<td><?=$ar_totalr[$key]?></td>
								</tr>
								<tr class="list-breakdown">
									<td colspan="7">
										<label>Breakdown Rate Room #<?=$r?></label>
										<ul class="inline-block breakdown-rate">
											<?php
											$q_breakdownrate = "select * from bookingroomdtl where bookingroomoid = '".$ar_bookingroom[$key]."' and reconciled = '0'";
											$stmt = $db->query($q_breakdownrate);
											$breakdownrate = $stmt->fetchAll(PDO::FETCH_ASSOC);
											foreach($breakdownrate as $key1 => $brd){
											?>
												<li><label><?=date('d/M/Y', strtotime($brd['date']))?></label><span><?=$rp['currencycode']?> <?=number_format($brd['total'])?></span></li>
											<?php
											}
											?>
										</ul>
									</td>
								</tr>
							<?php
								}
							?>
							</tbody>
						</table>
					<?php } ?>
          </div>
        </div>

				<div class="row">
					<div class="col-md-12">
				<?php
					$ar_extraname = array();
					$ar_qty = array();
					$ar_price = array(); $ar_total_extra = array();

					$q_detail = "select be.*, sum(be.total) as total, sum(be.deposit) as deposit, sum(be.balance) as balance, c.currencycode, e.name as extra
					from bookingextra be
					inner join currency c using (currencyoid)
					inner join booking b using (bookingoid)
					inner join extra e using (extraoid)
					where b.bookingoid = '".$bookingoid."' group by extraoid";
					try {
							$stmt = $db->query($q_detail);
							$row_count = $stmt->rowCount();
							if($row_count > 0) {
								$r_detail = $stmt->fetchAll(PDO::FETCH_ASSOC);
								foreach($r_detail as $rp){
									array_push($ar_extraname, $rp['extra']);
									array_push($ar_qty, $rp['qty']);
									array_push($ar_price, $rp['currencycode']." ".number_format($rp['price']));
									array_push($ar_total_extra, $rp['currencycode']." ".number_format($rp['total']));
								}
							}
					}catch(PDOException $ex) {
							echo "Invalid Query";
							print($ex);
							die();
					}
					if(count($ar_extraname) > 0) {
					?>
					<table class="table table-bordered">
						<thead>
							<tr>
									<th>Extra</th>
									<th>Qty</th>
									<th>Price</th>
									<th>Total</th>
							</tr>
						</thead>
						<tbody>
						<?php
							foreach($ar_extraname as $key => $value){
									echo"
									<tr>
											<td>".$ar_extraname[$key]."</td>
											<td class='center'>".$ar_qty[$key]."</td>
											<td>".$ar_price[$key]."</td>
											<td>".$ar_total_extra[$key]."</td>
									</tr>
									";
							}
						?>
						</tbody>
					</table>
					<?php
					}
					?>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<table class="table table-bordered">
							<tr><td><h3>Guest Note / Request : <h3><?php if(!empty($note)){ echo $note; }else{ echo "n/a"; } ?></td></tr>
						</table>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<h3>Total Cost Confirmed with Guest</h3>
						<table class="table table-bordered">
							<tr><td>Room Rate</td><td><?php echo $show_roomtotal_rr; ?></td></tr>
							<tr><td>Room Extra Bed Rate</td><td><?php echo $extrabedtotal; ?></td></tr>
							<tr><td>Extra Total</td><td><?php echo $extratotal; ?></td></tr>
							<tr><td>Total Confirmed to Guest</td><td><?php echo $grandtotal_r; ?></td></tr>
							<tr><td>Guest Deposit</td><td><?php echo $granddeposit; ?></td></tr>
							<tr><td>Guest Balace</td><td><?php echo $grandbalance; ?></td></tr>
						</table>
					</div>
					<div class="col-md-4">
						<h3>Commission Breakdown</h3>
						<table class="table table-bordered">
							<tr><td>Commissionable Amount</td><td><?php echo $grandtotal_r; ?></td></tr>
							<tr><td>Commission</td><td><?php echo $gbhcollect; ?></td></tr>
							<tr>
								<td>
									Total Confirmed to Hotel
									<?php if($datarsv['redeemamount'] > 0){ echo "<br><small>with redeemed point (".$datarsv['currencycode']." ".number_format($datarsv['redeemamount']).")</small>"; } ?>
								</td>
								<td><?php echo $hotelcollect; ?></td>
							</tr>
						</table>
						<table class="table table-bordered">
								<tr><td>Status : <?=$bookingstatus?><br>Last Updated  <?=date('d F Y H:i:s', strtotime($updated))?> by <?=$updatedby?></td></tr>
						</table>
					</div>
					<div class="col-md-4">
						<?php if(!empty($datarsv['memberoid']) and $datarsv['memberoid'] != 0){
							$stmt = $db->prepare("select membership, pointbefore from memberpointlog where memberoid = :a and bookingoid = :b order by logdate asc limit 1");
							$stmt->execute(array(':a' => $datarsv['memberoid'], ':b' => $datarsv['bookingoid']));
							$memberdtl = $stmt->fetch(PDO::FETCH_ASSOC);
						?>
						<h3>Loyalty Member Program</h3>
						<table class="table table-bordered">
							<tr><td>Membership Type</td><td><?php echo $memberdtl['membership']; ?></td></tr>
							<tr><td>Point Balance</td><td><?php echo $memberdtl['pointbefore']; ?></td></tr>
							<tr><td>Point Earn</td><td><?php echo $datarsv['point']; ?></td></tr>
							<tr><td>Point Redeem</td><td><?php echo $datarsv['redeempoint']." (- ".$datarsv['currencycode']." ".number_format($datarsv['redeemamount']).")"; ?></td></tr>
						</table>
						<?php } ?>
						<h3>Promo Code</h3>
						<table class="table table-bordered">
							<tr><td>Promo Code</td><td><?php echo $promocode; ?></td></tr>
							<tr><td>PIC</td><td><?php echo $promocode_pic; ?></td></tr>
							<tr><td>Commission</td><td><?php echo $promocode_comm; ?></td></tr>
						</table>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 text-right" style="font-size:0.8em">
						ip : <?=$datarsv['user_ip']?> | browser : <?=$datarsv['user_browser']?>	| utm_source : <?=$datarsv['utm_source']?> | utm_link : <?=$datarsv['utm_link']?> | utm_champaign : <?=$datarsv['utm_campaign']?>
					</div>
				</div>
			    <?php if(($_SESSION['_typeusr'] != '6' && $_SESSION['_typeusr'] != '7') || ($_SESSION['_typeusr'] == '7' && $hoteloid == '1645')){?>
				<div class="row" style="margin-top:10px;">
					<div class="col-md-4">
						<form method="post" action="<?=$base_url?>/booking/resend-confirmation">
							<input type="hidden" name="bookingnumber" value="<?=$bookingnumber?>">
							<b>Resend Email Confirmation to :</b><br>
							<input type="text" name="resend-email" required="required">
							<button type="submit" class="small-button blue"><i class="fa fa-send"></i> Send</button>
						</form>
					</div>
					<div class="col-md-8 text-right">
						<?php if($row_count_room == 0  or ($row_count_room > 0 and date('ymd') <= date('ymd', strtotime($max_co)))){ ?>
						<button type="button" class="small-button blue opencc"><i class="fa fa-credit-card"></i>Show Payment Detail</button>
						<?php } ?>
						<?php if($bookingstatusoid != "5" and $bookingstatusoid != "7"){ ?>
						<button type="button" class="small-button purple" bn="<?php echo $bookingnumber; ?>" data-toggle="modal" data-target="#reconcileModal"><i class="fa fa-balance-scale"></i>Reconcile</button>
						<button type="button" class="small-button red" bn="<?php echo $bookingnumber; ?>" data-toggle="modal" data-target="#cancelModal" data-title="Booking Cancellation" data-status="5" data-submit="Cancel Booking"><i class="fa fa-times-circle"></i>Booking Cancellation</button>
						<button type="button" class="small-button green" bn="<?php echo $bookingnumber; ?>" data-toggle="modal" data-target="#cancelModal" data-title="No Show" data-status="7" data-submit="Mark No Show"><i class="fa fa-ban"></i>Mark No Show</button>
						<?php } ?>
					</div>
				</div>
				<?php }?>
      </div>
    </div>
  </div>

	<div>
		<?php
		$query_room_reconcilled = "select count(bookingroomdtloid) as reconciled from bookingroomdtl brd inner join bookingroom br using (bookingroomoid) where reconciled = '1' and bookingoid = '".$bookingoid."'";
		try {
		$stmt = $db->query($query_room_reconcilled);
		$row_count = $stmt->rowCount();
		$countreconcile = $stmt->fetch(PDO::FETCH_ASSOC);
		if($countreconcile['reconciled']> 0){
		?>
		<div class="row">
			<div class="box box-form">
				<div class="box-body">
		<div style="font-size:0.9em">
		<h2><i class="fa fa-chevron-right"></i> History Reconcile</h2>
		<table class="table table-fill" id="detail-booking-table">
				<tr valign="top"><td>#</td><td>Room Type</td><td>Date</td><td>Occupancy</td><td>Breakfast Included</td><td>No. of Extra Bed</td><td>Total</td></tr>
				<?php
				$r = 0;
				foreach($ar_roomname as $key => $value){
						echo"
								<tr valign='top'>
										<td>".++$r."</td>
										<td>".$value."<br>".$ar_promotion[$key]."</td>
										<td>checkin : ".$ar_checkin[$key]."<br>checkout : ".$ar_checkout[$key]."</td>
										<td><i class='fa fa-male'></i> ".$ar_adult[$key]." <i class='fa fa-child'></i> ".$ar_child[$key]."</td>
										<td>".$ar_breakfast[$key]."</td>
										<td>".$ar_extrabed[$key]."</td>
										<td>".$ar_total[$key]."</td>
								</tr>";
				?>
				<tr class="list-breakdown">
					<td colspan="7">
						<label>Breakdown Rate Room #<?=$r?></label>
						<ul class="inline-block breakdown-rate">
							<?php
							$q_breakdownrate = "select * from bookingroomdtl where bookingroomoid = '".$ar_bookingroom[$key]."'";
							$stmt = $db->query($q_breakdownrate);
							$breakdownrate = $stmt->fetchAll(PDO::FETCH_ASSOC);
							foreach($breakdownrate as $key1 => $brd){
							?>
								<li><label><?=date('d/M/Y', strtotime($brd['date']))?></label><span><?=$rp['currencycode']?> <?=number_format($brd['total'])?></span></li>
							<?php
							}
							?>
						</ul>
					</td>
				</tr>
				<?php

				}
		?>
		</table>
		<table width="100%">
				<tr valign="top"><td>
										<table width="100%">
												<tr valign="top">
														<td>Total Cost Confirmed with Guest</td>
														<td>
																<table class="ch-table">
																		<tr><td>Room Rate</td><td><?php echo $roomtotal; ?></td></tr>
																		<tr><td>Room Extra Bed Rate</td><td><?php echo $extrabedtotal; ?></td></tr>
																		<tr><td>Extra Total</td><td><?php echo $extratotal; ?></td></tr>
																		<tr><td>Total Confirmed to Guest</td><td><?php echo $grandtotal; ?></td></tr>
																		<tr><td>Guest Deposit</td><td><?php echo $granddeposit; ?></td></tr>
							<tr><td>Guest Balace</td><td><?php echo $grandbalance; ?></td></tr>
																</table>
														</td>
												</tr>
										</table>
				</td><td>
										<table width="100%">
												<tr valign="top">
														<td>Commission Breakdown</td>
														<td>
																<table class="ch-table">
																		<tr><td>Commissionable Amount</td><td><?php echo $grandtotal; ?></td></tr>
																		<tr><td>Commission</td><td><?php echo $gbhcollect_bef_r; ?></td></tr>
																		<tr><td>Total Confirmed to Hotel</td><td><?php echo $hotelcollect_bef_r; ?></td></tr>
																</table>
														</td>
												</tr>
										</table>
							</td></tr>
					</table>
					</div>
				</div>
			</div>
		</div>
		<?php
				}
		}catch(PDOException $ex) {
			echo "Invalid Query";
			print($ex);
			die();
		}
		?>
	</div>
</section>

<form method="post" id="TheForm" action="<?php echo"$base_url"; ?>/paymentdetail" target="_blank">
<input type="hidden" name="bookingnumber" value="<?=$bookingnumber?>" />
</form>
<script src="<?php echo $base_url; ?>/lib/bootstrap/js/bootstrap.min.js"></script>
<?php include('modal.php'); ?>
