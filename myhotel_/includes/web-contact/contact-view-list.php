<?php
include('conf/connection-thebukingcom.php');

	$stmt = $dbTB->query("select count(formcontactoid) as registered from formcontact");
	$registered = $stmt->fetch(PDO::FETCH_ASSOC);
	$stmt = $dbTB->query("select count(formcontactoid) as emailed from formcontact where sendemail = '1'");
	$emailed = $stmt->fetch(PDO::FETCH_ASSOC);
?>

<section class="content-header">
    <h1>
       	Contact Us
    </h1>
    <ol class="breadcrumb">
        <li class="active"><a href="#"><i class="fa fa-table"></i> Contact Us</a></li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="box">
            <div id="data-box" class="box-body">
            <form method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/web-contact/send-email">
            <input type="hidden" name="id" value="" />
<h1>Contact Us</h1>
<h2><i class="fa fa-user"></i> Message : <?php echo $registered['registered']; ?></h2>
<table class="table promo-table">
	<tr>
		<td>No.</td>
		<td>Registration Date</td>
		<td>Name</td>
		<!-- <td>Company</td> -->
		<td>Email</td>
    <!-- <td>Mobile</td> -->
		<td>Subject</td>
		<td>Message</td>
    <td>Action</td>
	</tr>
	<?php
	$no = 0;
	$stmt = $dbTB->query("select * from formcontact order by submitdate desc");
	$row_count = $stmt->rowCount();
	if($row_count > 0) {
		$r_registered = $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach($r_registered as $data){
					$data['message'] = (strlen($data['message'])>=20) ? "<i>".substr($data['message'], 0, 20)."...</i>" : $data['message'];
	?>
	<tr>
		<td><?php echo ++$no; ?></td>
		<td><?php echo date('d/M/Y', strtotime($data['submitdate'])); ?></td>
		<td><?php echo $data['name']; ?></td>
		<!-- <td><?php echo $data['companyname']; ?></td> -->
		<td style="text-transform:lowercase"><?php echo $data['email']; ?></td>
    <!-- <td><?php echo $data['phone']; ?></td> -->
		<td><?php echo $data['subject']; ?></td>
		<td><?php echo $data['message']; ?></td>
    <td>
		<button type="button" class="detail-button small-button blue" id="<?php echo $data['formcontactoid']; ?>"><i class="fa fa-list"></i> Detail</button>
    <?php if($data['sendemail'] == 0){ ?>
    <button type="button" class="send-button small-button green" id="<?php echo $data['formcontactoid']; ?>"><i class="fa fa-envelope"></i> Send Email</button>
    <?php }else{ ?>
    <b class="color-magenta"><i class="fa fa-check"></i> Emailed</b>
		<?php } ?>
    </td>
	</tr>
	<?php
        }
	}
	?>
</table>
            </form>
            </div><!-- /.box-body -->
       </div>
    </div>
</section>

<script type="text/javascript">
$(function(){
	$('body').on('click','button.send-button', function(e) {
		$('input[name=id]').val($(this).attr('id'));
		$('form#data-input').submit();
	});

	$('body').on('click','button.detail-button', function(e) {
		$('input[name=id]').val($(this).attr('id'));
		id = $(this).attr("id");
		url = "<?php echo $base_url; ?>/web-contact/detail/?id="+id;
		$(location).attr("href", url);
	});
});
</script>
