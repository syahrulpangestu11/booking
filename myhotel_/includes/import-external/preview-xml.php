<?php
//---------------------------------------------------------------------
$endl = "\n";
$nbsp1 = "  "; $nbsp2 = "    "; $nbsp3 = "      "; $nbsp4 = "        "; $nbsp5 = "          ";
echo '<textarea style="width:100%;height:500px;" disabled><?xml version="1.0" encoding="utf-8"?>'.$endl;
echo '<data type="external" source="'.$partner.'">'.$endl;
echo '<hotel id="'.$hoteloid.'">'.$endl;

foreach($arr_roomoid as $key => $val){
	echo $nbsp1.'<masterroom id="'.$val.'">'.$endl;
		foreach($arr_ratedate[$arr[$key]] as $key1 => $val1){
			$x = strtotime($val1);
			$a = array('Sun','Mon','Tue','Wed','Thu','Fri','Sat');
			echo $nbsp2.'<rate date="'.date('Y-m-d',$x).'" day="'.$a[date('w',$x)].'">'.$endl;
			echo $nbsp3.'<rateplan id="'.$arr_roomofferoid[$key].'">'.$endl;
			echo $nbsp4.'<channel type="'.$channel.'">'.$endl;
				if(empty($arr_extpolicy[$arr[$key]][$val1])){
					echo $nbsp5.'<extendedpolicy/>'.$endl;
				}else{
					echo $nbsp5.'<extendedpolicy>'.$arr_extpolicy[$arr[$key]][$val1].'</extendedpolicy>'.$endl;
				}
				
				if(empty($arr_netsingle[$arr[$key]][$val1])){
					echo $nbsp5.'<netsingle/>'.$endl;
				}else{
					echo $nbsp5.'<netsingle>'.$arr_netsingle[$arr[$key]][$val1].'</netsingle>'.$endl;
				}
				
				if(empty($arr_netdouble[$arr[$key]][$val1])){
					echo $nbsp5.'<netdouble/>'.$endl;
				}else{
					echo $nbsp5.'<netdouble>'.$arr_netdouble[$arr[$key]][$val1].'</netdouble>'.$endl;
				}
				
				if(empty($arr_netextrabed[$arr[$key]][$val1])){
					echo $nbsp5.'<netextrabed/>'.$endl;
				}else{
					echo $nbsp5.'<netextrabed>'.$arr_netextrabed[$arr[$key]][$val1].'</netextrabed>'.$endl;
				}
				
				if(empty($arr_single[$arr[$key]][$val1])){
					echo $nbsp5.'<single/>'.$endl;
				}else{
					echo $nbsp5.'<single>'.$arr_single[$arr[$key]][$val1].'</single>'.$endl;
				}
				
				if(empty($arr_double[$arr[$key]][$val1])){
					echo $nbsp5.'<double/>'.$endl;
				}else{
					echo $nbsp5.'<double>'.$arr_double[$arr[$key]][$val1].'</double>'.$endl;
				}
				
				if(empty($arr_extrabed[$arr[$key]][$val1])){
					echo $nbsp5.'<extrabed/>'.$endl;
				}else{
					echo $nbsp5.'<extrabed>'.$arr_extrabed[$arr[$key]][$val1].'</extrabed>'.$endl;
				}
				
				if(empty($currency)){
					echo $nbsp5.'<currency/>'.$endl;
				}else{
					echo $nbsp5.'<currency>'.$currency.'</currency>'.$endl;
				}
				
				if(empty($arr_allotment[$arr[$key]][$val1])){
					echo $nbsp5.'<allotment/>'.$endl;
				}else{
					echo $nbsp5.'<allotment>'.$arr_allotment[$arr[$key]][$val1].'</allotment>'.$endl;
				}
				
				if(empty($arr_topup[$arr[$key]][$val1])){
					echo $nbsp5.'<topup/>'.$endl;
				}else{
					echo $nbsp5.'<topup>'.$arr_topup[$arr[$key]][$val1].'</topup>'.$endl;
				}
				
				if(empty($arr_breakfast[$arr[$key]][$val1])){
					echo $nbsp5.'<breakfast/>'.$endl;
				}else{
					if($arr_breakfast[$arr[$key]][$val1] == "Yes" or $arr_breakfast[$arr[$key]][$val1] == "1" or $arr_breakfast[$arr[$key]][$val1] == "True"){
						echo $nbsp5.'<breakfast>y</breakfast>'.$endl;
					}else{
						echo $nbsp5.'<breakfast>n</breakfast>'.$endl;
					}
				}
				
				if(empty($arr_minstay[$arr[$key]][$val1])){
					echo $nbsp5.'<minstay/>'.$endl;
				}else{
					if($arr_minstay[$arr[$key]][$val1] > 0){
						echo $nbsp5.'<minstay>'.$arr_minstay[$arr[$key]][$val1].'</minstay>'.$endl;
					}else{
						echo $nbsp5.'<minstay/>'.$endl;
					}
				}
				
				if(empty($arr_maxstay[$arr[$key]][$val1])){
					echo $nbsp5.'<maxstay/>'.$endl;
				}else{
					if($arr_maxstay[$arr[$key]][$val1] > 0){
						echo $nbsp5.'<maxstay>'.$arr_maxstay[$arr[$key]][$val1].'</maxstay>'.$endl;
					}else{
						echo $nbsp5.'<maxstay/>'.$endl;
					}
				}
				
				if(empty($arr_blackout[$arr[$key]][$val1])){
					echo $nbsp5.'<blackout/>'.$endl;
				}else{
					if($arr_blackout[$arr[$key]][$val1] == "Yes" or $arr_blackout[$arr[$key]][$val1] == "1" or $arr_blackout[$arr[$key]][$val1] == "True"){
						echo $nbsp5.'<blackout>y</blackout>'.$endl;
					}else{
						echo $nbsp5.'<blackout>n</blackout>'.$endl;
					}
				}
				
				if(empty($arr_surcharge[$arr[$key]][$val1])){
					echo $nbsp5.'<surcharge/>'.$endl;
				}else{
					if($arr_surcharge[$arr[$key]][$val1] == "Yes" or $arr_surcharge[$arr[$key]][$val1] == "1" or $arr_surcharge[$arr[$key]][$val1] == "True"){
						echo $nbsp5.'<surcharge>y</surcharge>'.$endl;
					}else{
						echo $nbsp5.'<surcharge>n</surcharge>'.$endl;
					}
				}
			echo $nbsp4.'</channel>'.$endl;
			echo $nbsp3.'</rateplan>'.$endl;
			echo $nbsp2.'</rate>'.$endl;
		}
	echo $nbsp1.'</masterroom>'.$endl;
}

echo '</hotel>'.$endl;
echo '</data></textarea>';
//---------------------------------------------------------------------
?>