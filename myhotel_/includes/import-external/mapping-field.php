<?php
$hoteloid = $_SESSION['_hotel'];

if(isset($_FILES["sourcefile"])){
    $partner=$_POST['partner'];
    $currency=$_POST['currency'];
    $channel=$_POST['channel'];
	$message="";

	if($_FILES["sourcefile"]["error"] > 0){
		$message = "Error! Invalid source file. Return Code: " . $_FILES["sourcefile"]["error"] . "<br />";
    }else if($_FILES["sourcefile"]["type"] != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" 
      && $_FILES["sourcefile"]["type"] != "application/vnd.ms-excel"){
        $message = "Error! Invalid file type.<br />";
    }else if(empty($partner) or empty($currency) or empty($channel)){
        $message = "Error! There are still any empty field.<br />";
    }else{
		$srcfilename = date("YmdHi") . $_FILES['sourcefile']['name'];
        $source = $_FILES['sourcefile']['tmp_name'];
		$target = "files/external/" . $srcfilename;
		move_uploaded_file($source, $target);
        
        include 'lib/phpexcel/PHPExcel/IOFactory.php';
		$inputFileName = $target; 

        try {
            PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
			$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
		} catch(Exception $e) {
			$message = ('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		}
        
		/*$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
		$objPHPExcel->getActiveSheet()->setTitle('Rates');
		$objWriter->save($inputFileName);*/
        
		//echo $objPHPExcel->getActiveSheet()->getTitle();
		
        $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
		$arrayCount = count($allDataInSheet);
        //$arrayCount = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
        
		//unlink($inputFileName);
        
        $alldata = array();
        $arr = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO');
        
        try {
            for($i=1; $i<=$arrayCount; $i+=1){
                foreach($arr as $value){
                    $alldata[$i][$value] = trim($allDataInSheet[$i][$value]);
                }
            }
        }catch(PDOException $ex) {
            $message = ('Error loading file '.$e->getMessage());
        }
    }
}else{
    $message = "Error! File not exist.<br />";
}

function select_excel_field($name, $selected="")
{	
    $arr = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO');
    echo '<select name="'.$name.'">';
    echo '<option value="">-- Select Field --</option>';
    foreach($arr as $value){
		if($selected == $value){
        	echo '<option value="'.$value.'" selected="selected">Column '.$value.'</value>';
		}else{
			echo '<option value="'.$value.'">Column '.$value.'</value>';
		}
    }
    echo '</select>';
}

try {                         
    $stmt = $db->query("SELECT `roomofferoid`, `roomoffer`.`name`, `roomoffer`.`roomoid`, `room`.`name` AS `roomname` FROM `roomoffer` INNER JOIN `room` USING(`roomoid`) WHERE `hoteloid`='".$hoteloid."' AND `roomoffer`.`publishedoid`='1'");
    $r_roomoffer = $stmt->fetchAll(PDO::FETCH_ASSOC);
}catch(PDOException $ex) {
    echo "Invalid Query";
    die();
}
?>
<style>
    .excel-preview{ height:200px;border:1px solid #3C8DBC;overflow:scroll; }
    .excel-preview td{ padding:2px 10px;border-right:1px solid #CCCCCC; }
</style>
<section class="content-header">
    <h1>
        Import External Data
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Settings</a></li>
        <li class="active">Import External Data</li>
    </ol>
</section>
<?php
if(!empty($message)){
?>
<section class="content">
    <div class="row">
    	<div class="alert alert-danger">
          <?php echo $message;?>
        </div>
        <a href="<?php echo"$base_url"; ?>/import-external">Back</a>
    </div>
</section>
<?php
}else{
?>
<section class="content">
    <div class="row">
        <div class="box box-form">
            <div class="excel-preview">
                <table>
                    <?php
                    $arr = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO');

                    echo "<tr>
                        <th align='center'>&nbsp;</th>";
                    
                    foreach($arr as $value){
                        echo "<th align='center'>".$value."</th>";
                    }
                    
                    echo "</tr>";
                        
                    for($i=1; $i<=6; $i+=1){
                        echo "<tr>
                            <td><b>".$i."</b></td>";

                        foreach($arr as $value){
                            echo "<td>".$alldata[$i][$value]."</td>";
                        }

                        echo "</tr>";
                    }
                    ?>
                </table>
            </div>
            <br>
            <form method="post" enctype="multipart/form-data" action="<?php echo $base_url; ?>/import-external/import-data">
            <table style="width:100%">
            <tr>
                <td colspan="3">Read data from row number &nbsp; :
                    &nbsp; <input type="text" name="startrow" value="<?php if($partner=='1'){echo "2";}else{echo "0";}?>" size="5"/></td>
            </tr>
            <tr>
                <td colspan="3">Data type &nbsp; :
                    &nbsp; <input type="radio" name="datatype" value="0" checked/> group by inventory &nbsp; <input type="radio" name="datatype" disabled value="1"/> a data in each row</td>
            </tr>
            <tr>
                <td colspan="3"><hr/></td>
            </tr>
            <tr>
                <td style="padding-right:20px;width:15%">Rate Date</td>
                <td>:</td>
                <td><?php if($partner=='1'){select_excel_field("ratedate","E");}else{select_excel_field("ratedate");}?></td>
            </tr>
            <tr>
                <td style="padding-right:20px;">Extended Policy</td>
                <td>:</td>
                <td><?php select_excel_field("extendedpolicy");?></td>
            </tr>
            <?php /*<tr>
                <td style="padding-right:20px;">Net Single Rate</td>
                <td>:</td>
                <td><?php if($partner=='1'){select_excel_field("netsingle","F");}else{select_excel_field("netsingle");}?></td>
            </tr>
            <tr>
                <td style="padding-right:20px;">Net Double Rate</td>
                <td>:</td>
                <td><?php if($partner=='1'){select_excel_field("netdouble","G");}else{select_excel_field("netdouble");}?></td>
            </tr>
            <tr>
                <td style="padding-right:20px;">Net Extrabed Rate</td>
                <td>:</td>
                <td><?php if($partner=='1'){select_excel_field("netextrabed","G");}else{select_excel_field("netextrabed");}?></td>
            </tr>
            <tr>
                <td style="padding-right:20px;">Single Rate</td>
                <td>:</td>
                <td><?php if($partner=='1'){select_excel_field("single","F");}else{select_excel_field("single");}?></td>
            </tr>*/?>
            <tr>
                <td style="padding-right:20px;">Rate</td>
                <td>:</td>
                <td><?php if($partner=='1'){select_excel_field("double","AB");}else{select_excel_field("double");}?></td>
            </tr>
            <tr>
                <td style="padding-right:20px;">Extrabed</td>
                <td>:</td>
                <td><?php if($partner=='1'){select_excel_field("extrabed","AF");}else{select_excel_field("extrabed");}?></td>
            </tr>
            <?php /*<tr>
                <td style="padding-right:20px;">Currency</td>
                <td>:</td>
                <td><?php select_excel_field("currency");?></td>
            </tr>*/?>
            <tr>
                <td style="padding-right:20px;">Allotment</td>
                <td>:</td>
                <td><?php if($partner=='1'){select_excel_field("allotment","U");}else{select_excel_field("allotment");}?></td>
            </tr>
            <tr>
                <td style="padding-right:20px;">Topup</td>
                <td>:</td>
                <td><?php select_excel_field("topup");?></td>
            </tr>
            <tr>
                <td style="padding-right:20px;">Breakfast</td>
                <td>:</td>
                <td><?php select_excel_field("breakfast");?></td>
            </tr>
            <tr>
                <td style="padding-right:20px;">Min Stay</td>
                <td>:</td>
                <td><?php if($partner=='1'){select_excel_field("minstay","AH");}else{select_excel_field("minstay");}?></td>
            </tr>
            <tr>
                <td style="padding-right:20px;">Max Stay</td>
                <td>:</td>
                <td><?php if($partner=='1'){select_excel_field("maxstay","AI");}else{select_excel_field("maxstay");}?></td>
            </tr>
            <?php /*<tr>
                <td style="padding-right:20px;">Blackout</td>
                <td>:</td>
                <td><?php select_excel_field("blackout");?></td>
            </tr>*/?>
            <tr>
                <td style="padding-right:20px;">Surcharge</td>
                <td>:</td>
                <td><?php if($partner=='1'){select_excel_field("surcharge","X");}else{select_excel_field("surcharge");}?></td>
            </tr>
            <tr>
                <td colspan="3"><hr/></td>
            </tr>
            </table>
            <table style="width:100%">
            <tr>
                <td style="padding-right:20px;width:15%">Room Name</td>
                <td>:</td>
                <td><?php select_excel_field("roomname");?></td>
            </tr>
            <tr>
                <td style="padding-right:20px;width:15%">Rate Plan Name</td>
                <td>:</td>
                <td><?php select_excel_field("rateplan");?></td>
            </tr>
            </table>
            <br>
            <table style="width:100%;border:1px solid #666">
                <thead><tr>
                    <th style="width:5%">No</th>
                    <th align="left" style="width:22.5%">Excel Room Name</th>
                    <th align="left" style="width:22.5%">Excel Rate Plan</th>
                    <th align="left" style="width:22.5%">DBase Room Type</th>
                    <th align="left" style="width:22.5%">DBase Rate Plan</th>
                    <th style="width:5%"></th>
                </tr></thead>
                <tbody id="roomnamepicker"></tbody>
            </table>
            <script>
            function findInArr(arrx, findx){
                resx = false;
                for(var j=0; j<arrx.length; j++){
                    if(arrx[j] == findx){
                        resx = true;
                    }
                }
                return resx;
            }
            $(function(){
                var allxdata = [];
                <?php
                for($i=1; $i<=$arrayCount; $i+=1){
                    echo 'allxdata['.$i.'] = {';
                    foreach($arr as $value){
                        echo $value.':"'.$alldata[$i][$value].'",';
                    }
                    echo '};';
                }
                ?>
                
                $('select[name="rateplan"]').change(function(){
                    var x = $('select[name="roomname"]').val();
                    var y = $('input[name="startrow"]').val();
                    var z = $('select[name="rateplan"]').val();
                    
                    if(x != "" || z != ""){
                        if(y == '0' || y == ""){
                            alert("Please fill the row number first!");
                            $('select[name="roomname"]').val("");
                        }else{
                            var arr = [];
                            var arrm = [];
                            var arrp = [];
                            for(var i=parseInt(y); i<allxdata.length; i++){
                                if(findInArr(arr, allxdata[i][x]+""+allxdata[i][z]) === false && allxdata[i][z]!="<inventory>" && allxdata[i][z]!="" && allxdata[i][y]!=""){
                                    arr.push(allxdata[i][x]+""+allxdata[i][z]);
                                    arrm.push(allxdata[i][x]);
                                    arrp.push(allxdata[i][z]);
                                }
                            }
                            $('#roomnamepicker').html('');
                            for(var i=0; i<arr.length; i++){
                                html = '<tr><td style="text-align:center">'+(i+1)+'</td><td>'+arrm[i]+'</td><td>'+arrp[i]+'</td><td>';
                                <?php
                                echo 'html += \'<select class="rtype" name="xroomname[]"><option value="0">-- Create New Room --</option>';
                                
                                $roomoidarray = array();
                                foreach($r_roomoffer as $row){
                                    if(!in_array($row['roomoid'],$roomoidarray)){
                                        array_push($roomoidarray,$row['roomoid']);
                                        echo '<option value="'.$row['roomoid'].'">'.$row['roomname'].'</option>';
                                    }
                                }

                                echo '</select>\';';
                                ?>
                                html += '</td><td>';
                                <?php
                                echo 'html += \'<select name="xrateplan[]"><option value="0">-- Create New Rate Plan --</option>';
                                
                                foreach($r_roomoffer as $row){
                                    echo '<option class="rplan hide" rid="'.$row['roomoid'].'" value="'.$row['roomofferoid'].'">'.$row['name'].'</option>';
                                }

                                echo '</select>\';';
                                ?>
                                html += '</td><td></td></tr>';
                                
                                $('#roomnamepicker').append(html);
                            }
                        }
                    }
                });

                $(document).on('change', ".rtype", function(){
                    var n = $(this).val();
                    $(this).parent().parent().find('select[name="xrateplan[]"]').prop('selectedIndex', 0);
                    $(this).parent().parent().find(".rplan").each(function(){
                        var rid = $(this).attr("rid");
                        if(rid==n || rid=="0"){
                            $(this).removeClass("hide");
                        }else{
                            $(this).addClass("hide");
                        }
                    });
                });
            });    
            </script>
            <br>
            <table style="width:100%">
            <tr>
                <td colspan="3"><hr/></td>
            </tr>
            <tr>
                <td style="padding-right:20px;width:20%">Regular (has closeout)</td>
                <td>:</td>
                <td><?php if($partner=='1'){select_excel_field("regular","N");}else{select_excel_field("regular");}?></td>
            </tr>
            <tr>
                <td style="padding-right:20px;">Arrival (has no arrival)</td>
                <td>:</td>
                <td><?php if($partner=='1'){select_excel_field("arrival","O");}else{select_excel_field("arrival");}?></td>
            </tr>
            <tr>
                <td style="padding-right:20px;">Departure (has no checkout)</td>
                <td>:</td>
                <td><?php if($partner=='1'){select_excel_field("departure","P");}else{select_excel_field("departure");}?></td>
            </tr>
            <tr>
                <td colspan="3"><hr/></td>
            </tr>
            <tr>
                <td colspan="3" align="right" style="padding-right:30px"><button type="submit" class="small-button blue">Next</button></td>
            </tr>
            </table>
            <input type="hidden" name="partner" value="<?=$_POST['partner']?>">
            <input type="hidden" name="currency" value="<?=$_POST['currency']?>">
            <input type="hidden" name="channel" value="<?=$_POST['channel']?>">
            <input type="hidden" name="target" value="<?=$target?>">
            <input type="hidden" name="srcfilename" value="<?=$srcfilename?>">
            </form>
        </div>
    </div>
</section>
<?php
}
?>