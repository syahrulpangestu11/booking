<?php 
// 5 hours
set_time_limit(18000);
ini_set('memory_limit','1536M');
ini_set('max_execution_time', 18000);

$hoteloid = $_SESSION['_hotel'];

function findInArr($arrx, $findx){
    $resx = false;
    for($j=0; $j<count($arrx); $j++){
        if($arrx[$j] == $findx){
            $resx = true;
        }
    }
    return $resx;
}

include 'lib/phpexcel/PHPExcel/IOFactory.php';
$inputFileName = $_POST['target'];

try {
    PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
    $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
} catch(Exception $e) {
    die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
}

$allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
$arrayCount = count($allDataInSheet);

$src_filename = $_POST['srcfilename'];
$partner = $_POST['partner'];
$channel = $_POST['channel'];
$startReadFrom = $_POST['startrow'];
$dataXMLType = $_POST['datatype'];
$roomnameField = $_POST['roomname'];
$ratenameField = $_POST['rateplan'];
$ratedateField = $_POST['ratedate'];
$extpolicyField = $_POST['extendedpolicy'];
$netsingleField = $_POST['double'];
$netdoubleField = $_POST['double'];
$netextrabedField = $_POST['extrabed'];
$singleField = $_POST['double'];
$doubleField = $_POST['double'];
$extrabedField = $_POST['extrabed'];
$currency = $_POST['currency'];
$allotmentField = $_POST['allotment'];
$topupField = $_POST['topup'];
$breakfastField = $_POST['breakfast'];
$minstayField = $_POST['minstay'];
$maxstayField = $_POST['maxstay'];
$blackoutField = $_POST['regular'];
$surchargeField = $_POST['surcharge'];
$regularField = $_POST['regular'];
$arrivalField = $_POST['arrival'];
$departureField = $_POST['departure'];

$arr = array();
if($dataXMLType == '0') {
	$counter = 0;
	$newdbRoomName = array();
	$newdbRoomID = array();
	$weekday = array('Sun','Mon','Tue','Wed','Thu','Fri','Sat');

	/*$stmt = $db->query("SELECT * FROM `currency`");
    $x_currency = $stmt->fetchAll(PDO::FETCH_ASSOC);
	$listCurrency = array();
	foreach($x_currency as $row){
		$listCurrency[$row['currencycode']] = $row['currencyoid'];
	}*/

	$preparedData = array();
	$roomIDExisting = array();
	$rateIDExisting = array();

	for($i=$startReadFrom; $i<=$arrayCount; $i++){
		$roomnameData = trim($allDataInSheet[$i][$roomnameField]);
		$ratenameData = trim($allDataInSheet[$i][$ratenameField]);
		$ratedateData = trim($allDataInSheet[$i][$ratedateField]);

		if($ratenameData!="<inventory>" && $ratenameData!="" && $roomnameData!=""){ 
			if(findInArr($arr, $roomnameData."".$ratenameData) === false){
				array_push($arr, $roomnameData."".$ratenameData);

				$dbRoomID = $_POST['xroomname'][$counter];
				$dbRateID = $_POST['xrateplan'][$counter];

				if($dbRoomID == '0'){
					if(!in_array($roomnameData, $newdbRoomName)){
						$stmt = $db->prepare("INSERT INTO `room`(`roomoid`, `hoteloid`, `name`, `description`, `roomsize`, `roomsize_inc_balcon`, `adult`, `child`, `roompict`, `priority`, `publishedoid`, `extrabed`, `numroom`, `allotmentalert`, `maxextrabed`, `minrate`, `viewoid`) VALUES (NULL,:a,:b,'','','','','','','','1','notavailable','','','','','')");
						$stmt->execute(array(':a' => $hoteloid, ':b' => $roomnameData));
						$roomID = $db->lastInsertId();
						$newdbRoomID[] = $roomID;
						$newdbRoomName[] = $roomnameData;
					}else{
						$key = array_search($roomnameData, $newdbRoomName);
						$roomID = $newdbRoomID[$key];
					}

					$stmt = $db->prepare("INSERT INTO `roomoffer`(`roomofferoid`, `roomoid`, `name`, `standardname`, `offertypeoid`, `publishedoid`) VALUES (NULL,:a,:b,:c,'4','1')");
					$stmt->execute(array(':a' => $roomID, ':b' => $ratenameData, ':c' => $ratenameData));
					$rateID = $db->lastInsertId();
				}else{
					$roomID = $dbRoomID;
					
					if($dbRateID == '0'){
						$stmt = $db->prepare("INSERT INTO `roomoffer`(`roomofferoid`, `roomoid`, `name`, `standardname`, `offertypeoid`, `publishedoid`) VALUES (NULL,:a,:b,:c,'4','1')");
						$stmt->execute(array(':a' => $roomID, ':b' => $ratenameData, ':c' => $ratenameData));
						$rateID = $db->lastInsertId();
					}else{
						$rateID = $dbRateID;
					}
				}

				$roomIDExisting[$roomnameData."".$ratenameData] = $roomID;
				$rateIDExisting[$roomnameData."".$ratenameData] = $rateID;

				$counter++;
			}

			$preparedData[$roomnameData][$ratenameData][$ratedateData]["masterroom"] = $roomIDExisting[$roomnameData."".$ratenameData];
			$preparedData[$roomnameData][$ratenameData][$ratedateData]["roomname"] = $roomnameData;
			$preparedData[$roomnameData][$ratenameData][$ratedateData]["ratedate"] = date("Y-m-d", strtotime($ratedateData));
			$preparedData[$roomnameData][$ratenameData][$ratedateData]["rateday"] = $weekday[date('w',$x)];
			$preparedData[$roomnameData][$ratenameData][$ratedateData]["allotment"] = trim($allDataInSheet[$i][$allotmentField]);
			$preparedData[$roomnameData][$ratenameData][$ratedateData]["rateplan"] = $rateIDExisting[$roomnameData."".$ratenameData];
			$preparedData[$roomnameData][$ratenameData][$ratedateData]["rateplanname"] = $ratenameData;
			$preparedData[$roomnameData][$ratenameData][$ratedateData]["extendedpolicy"] = trim($allDataInSheet[$i][$extpolicyField]);
			$preparedData[$roomnameData][$ratenameData][$ratedateData]["netsingle"] = trim($allDataInSheet[$i][$netsingleField]);
			$preparedData[$roomnameData][$ratenameData][$ratedateData]["netdouble"] = trim($allDataInSheet[$i][$netdoubleField]);
			$preparedData[$roomnameData][$ratenameData][$ratedateData]["netextrabed"] = trim($allDataInSheet[$i][$netextrabedField]);
			$preparedData[$roomnameData][$ratenameData][$ratedateData]["single"] = trim($allDataInSheet[$i][$singleField]);
			$preparedData[$roomnameData][$ratenameData][$ratedateData]["double"] = trim($allDataInSheet[$i][$doubleField]);
			$preparedData[$roomnameData][$ratenameData][$ratedateData]["extrabed"] = trim($allDataInSheet[$i][$extrabedField]);
			$preparedData[$roomnameData][$ratenameData][$ratedateData]["currency"] = trim($allDataInSheet[$i][$currency]);
			$preparedData[$roomnameData][$ratenameData][$ratedateData]["topup"] = trim($allDataInSheet[$i][$topupField]);
			$preparedData[$roomnameData][$ratenameData][$ratedateData]["breakfast"] = trim($allDataInSheet[$i][$breakfastField]);
			$preparedData[$roomnameData][$ratenameData][$ratedateData]["minstay"] = trim($allDataInSheet[$i][$minstayField]);
			$preparedData[$roomnameData][$ratenameData][$ratedateData]["maxstay"] = trim($allDataInSheet[$i][$maxstayField]);
			$preparedData[$roomnameData][$ratenameData][$ratedateData]["blackout"] = trim($allDataInSheet[$i][$blackoutField]);
			$preparedData[$roomnameData][$ratenameData][$ratedateData]["surcharge"] = trim($allDataInSheet[$i][$surchargeField]);
			$preparedData[$roomnameData][$ratenameData][$ratedateData]["cta"] = trim($allDataInSheet[$i][$arrivalField]);
			$preparedData[$roomnameData][$ratenameData][$ratedateData]["ctd"] = trim($allDataInSheet[$i][$departureField]);

		}else if($ratenameData=="<inventory>" && $ratenameData!="" && $roomnameData!=""){
			$inventoryData[$roomnameData][$ratedateData]["masterroom"] = $roomIDExisting[$roomnameData."".$ratenameData];
			$inventoryData[$roomnameData][$ratedateData]["roomname"] = $roomnameData;
			$inventoryData[$roomnameData][$ratedateData]["ratedate"] = date("Y-m-d", strtotime($ratedateData));
			$inventoryData[$roomnameData][$ratedateData]["rateday"] = $weekday[date('w',$x)];
			$inventoryData[$roomnameData][$ratedateData]["allotment"] = trim($allDataInSheet[$i][$allotmentField]);
			$inventoryData[$roomnameData][$ratedateData]["rateplan"] = $rateIDExisting[$roomnameData."".$ratenameData];
			$inventoryData[$roomnameData][$ratedateData]["rateplanname"] = $ratenameData;
			$inventoryData[$roomnameData][$ratedateData]["extendedpolicy"] = trim($allDataInSheet[$i][$extpolicyField]);
			$inventoryData[$roomnameData][$ratedateData]["netsingle"] = trim($allDataInSheet[$i][$netsingleField]);
			$inventoryData[$roomnameData][$ratedateData]["netdouble"] = trim($allDataInSheet[$i][$netdoubleField]);
			$inventoryData[$roomnameData][$ratedateData]["netextrabed"] = trim($allDataInSheet[$i][$netextrabedField]);
			$inventoryData[$roomnameData][$ratedateData]["single"] = trim($allDataInSheet[$i][$singleField]);
			$inventoryData[$roomnameData][$ratedateData]["double"] = trim($allDataInSheet[$i][$doubleField]);
			$inventoryData[$roomnameData][$ratedateData]["extrabed"] = trim($allDataInSheet[$i][$extrabedField]);
			$inventoryData[$roomnameData][$ratedateData]["currency"] = trim($allDataInSheet[$i][$currency]);
			$inventoryData[$roomnameData][$ratedateData]["topup"] = trim($allDataInSheet[$i][$topupField]);
			$inventoryData[$roomnameData][$ratedateData]["breakfast"] = trim($allDataInSheet[$i][$breakfastField]);
			$inventoryData[$roomnameData][$ratedateData]["minstay"] = trim($allDataInSheet[$i][$minstayField]);
			$inventoryData[$roomnameData][$ratedateData]["maxstay"] = trim($allDataInSheet[$i][$maxstayField]);
			$inventoryData[$roomnameData][$ratedateData]["blackout"] = trim($allDataInSheet[$i][$blackoutField]);
			$inventoryData[$roomnameData][$ratedateData]["surcharge"] = trim($allDataInSheet[$i][$surchargeField]);
			$inventoryData[$roomnameData][$ratedateData]["cta"] = trim($allDataInSheet[$i][$arrivalField]);
			$inventoryData[$roomnameData][$ratedateData]["ctd"] = trim($allDataInSheet[$i][$departureField]);
		}
	}

	foreach ($preparedData as $keyRoom => $valueRoom) {
		foreach ($valueRoom as $keyRate => $valueRate) {
			foreach ($valueRate as $keyDate => $valueDate) {
				$preparedData[$keyRoom][$keyRate][$keyDate]["allotment"] = $inventoryData[$keyRoom][$keyDate]["allotment"];
				$preparedData[$keyRoom][$keyRate][$keyDate]["surcharge"] = $inventoryData[$keyRoom][$keyDate]["surcharge"];
				$preparedData[$keyRoom][$keyRate][$keyDate]["breakfast"] = $inventoryData[$keyRoom][$keyDate]["breakfast"];
				
				if(empty($preparedData[$keyRoom][$keyRate][$keyDate]["blackout"])){
					$preparedData[$keyRoom][$keyRate][$keyDate]["blackout"] = $inventoryData[$keyRoom][$keyDate]["blackout"];
				}
				if(empty($preparedData[$keyRoom][$keyRate][$keyDate]["cta"])){
					$preparedData[$keyRoom][$keyRate][$keyDate]["cta"] = $inventoryData[$keyRoom][$keyDate]["cta"];
				}
				if(empty($preparedData[$keyRoom][$keyRate][$keyDate]["ctd"])){
					$preparedData[$keyRoom][$keyRate][$keyDate]["ctd"] = $inventoryData[$keyRoom][$keyDate]["ctd"];
				}

				//closeoutofroom
				$preparedData[$keyRoom][$keyRate][$keyDate]["closeout"] = $inventoryData[$keyRoom][$keyDate]["blackout"];
			}
		}
	}

	/*echo '<pre>';
	print_r($preparedData);
	echo '</pre>';
	die();*/
}else{
	$preparedData = array();
}

//---------------------------------------------------------------------
?>

<section class="content-header">
    <h1>
        Import External Data
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Settings</a></li>
        <li class="active">Import External Data</li>
    </ol>
</section>
<section class="content">
    <?php
	try {
		include('includes/function-xml.php');
		$xml_filename = $hcode.'.xml';
		$path_xml_hotel = 'data-xml/'.$xml_filename;

		include('save-xml.php');
		include('save-log.php');
		?>
        <div class="row">
            <div id="dialog" title="Confirmation Notice">
              <p>Data succesfully added</p>
            </div>
        </div>
        <script type="text/javascript">
		$(function(){
			$( document ).ready(function() {
				$("#dialog").dialog("open");
			});
		});
		</script>
        <?php
	} catch(Exception $e) {
		echo 'Error :'.$e->getMessage();
		?>
        <div id="dialog" title="Error Notification">
          <p>We&rsquo;re very sorry, we can't save your data right now.</p>
        </div>
        <script type="text/javascript">
		$(function(){
			$( document ).ready(function() {
				$("#dialog").dialog("open");
			});
		});
		</script>
        <?php
	}
	?>
</section>
<script type="text/javascript">
$(function(){
   $("#dialog").dialog({
      autoOpen: false,
	  buttons: { "Ok": redirect },
	  close: function(event, ui) { redirect }
   });
   
   function redirect(){
	   $(location).attr("href", "<?php echo $base_url; ?>/import-external");
   }
});
</script>