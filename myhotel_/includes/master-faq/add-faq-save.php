<?php
	$type = $_POST['type'];
	$question = $_POST['question'];
	$answer = $_POST['answer'];
	try{
		$stmt = $db->prepare("insert into masterfaq (type, question, answer, createdby, created, updatedby, updated) values (:a, :b, :c, :d, :e, :f, :j)");
		$stmt->execute(array(':a' => $type, ':b' => $question, ':c' => $answer, ':d' => $_SESSION['_user'], ':e' => date('Y-m-d H:i:s'), ':f' => $_SESSION['_user'], ':j' => date('Y-m-d H:i:s')));
		$masterfaqoid = $db->lastInsertId();

	?>
		<script type="text/javascript">
        $(function(){ $( document ).ready(function(){ $("#dialog-success").dialog("open"); }); });
    </script>
	<?php
	}catch(PDOException $ex) {
		print_r($ex->getMessage());
	?>
			<script type="text/javascript">
        $(function(){ $( document ).ready(function(){ $("#dialog-error").dialog("open"); }); });
    	</script>
	<?php
	}
?>
