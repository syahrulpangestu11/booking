<?php
include("../ajax-include-file.php");
$startdate = date("Y-m-d", strtotime($_REQUEST['startdate']));
$enddate = date("Y-m-d", strtotime($_REQUEST['enddate']));
$hoteloid = $_REQUEST['ho'];
$roomoffer = $_REQUEST['rt'];
$channeltype = $_REQUEST['channel'];

	try {
		$stmt = $db->query("SELECT DATEDIFF('".$enddate."','".$startdate."') AS DiffDate");
		$r_datediff = $stmt->fetchAll(PDO::FETCH_ASSOC);
		foreach($r_datediff as $row){
			$diff = $row['DiffDate'];
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}


	try {
		$stmt = $db->query("select h.hotelcode, r.roomoid, ro.minrate from room r inner join hotel h using (hoteloid) inner join roomoffer ro using (roomoid) where ro.roomofferoid = '".$roomoffer."'");
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
			$r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_room as $row){
				$hcode = $row['hotelcode'];
				$roomoid = $row['roomoid'];
				$minrate = $row['minrate'];
			}
		}else{
			echo "No Result";
			die();
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}

	$xml =  simplexml_load_file("../../data-xml/".$hcode.".xml");

?>
    <input type="hidden" name="roomoid" value="<?php echo $roomoid; ?>" />
    <input type="hidden" name="roomoffer" value="<?php echo $roomoffer; ?>" />
    <input type="hidden" name="channeloid" value="<?php echo $channeltype; ?>" />
    <input type="hidden" name="startdate" value="<?php echo $_REQUEST['startdate']; ?>" />
    <input type="hidden" name="enddate" value="<?php echo $_REQUEST['enddate']; ?>" />
    <input type="hidden" id="minrate" value="<?php echo $minrate; ?>" />

    <div>
    <button type="submit" class="small-button blue"><i class="fa fa-save"></i> Save</button>
    &nbsp;&nbsp;
    <button type="reset" class="small-button blue">Reset</button>
    &nbsp;&nbsp;&nbsp;&nbsp;
    <i>Please note that you cannot upload rate below the minimum rate you have set.</i>
    </div>
    <br />

    <table class="table table-fill table-fill-centered">
        <tr>
            <td>Date</td>
            <td>Day</td>
            <?php /*
						<td colspan="2">Single</td>
            <td colspan="2">Double</td>
            <td colspan="2">Extra Bed</td>
						*/ ?>
            <td>Rates</td>
            <td>Extra Bed</td>
            <td>Allotment</td>
						<?php /*
            <td>Min Stay</td>
            <td>Max Stay</td>
            <td>Promotion<br />Blackout</td
						*/ ?>
            <td>Include<br />Breakfast</td>
						<?php /*
            <td style="display:none">View<br />Surcharge</td>
						*/ ?>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>Sell Include Tax<br /> <i>(min. <b><?php echo intval($minrate);?></b>)</i></td>
            <td>Sell Include Tax</td>
						<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr class="autofilled">
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td><input type="text" name="setdouble" tgt="double[]" /></td>
            <td><input type="text" name="setextrabed" tgt="extrabed[]" /></td>
            <td><input type="text" name="setalootment" class="short" tgt="allotment[]" /></td>
						<?php /*
						<td style="display:none"><input type="text" name="setminstay" class="short" tgt="minstay[]" /></td>
            <td style="display:none"><input type="text" name="setmaxstay" class="short" tgt="maxstay[]" /></td>
            */ ?>
            <td>
                <select name="setbreakfast" tgt="bf[]">
                    <option value="">&nbsp;</option>
                    <option value="y">yes</option>
                    <option value="n">no</option>
                </select>
            </td>
            <td><button type="button" class="small-button blue">Auto Fill</button></td>
        </tr>

<?php
for($i=0;$i<=$diff;$i++){
	$dateFormat = explode(" ",date("D Y-m-d d-M-Y",strtotime($startdate." +".$i." day")));
	$day = $dateFormat[0];
	$date = $dateFormat[1];
	$dateformated = $dateFormat[2];

	list($extendedpolicy, $netsingle, $netdouble, $netextrabed, $single, $double, $extrabed, $currency, $allotment, $topup, $breakfast, $minstay, $maxstay, $blackout, $surcharge) = array("n", "0", "0", "0", "0", "0", "0", "1", "0", "0", "y", "1", "0", "n", "n");

	echo"<tr class='list'>";
	echo"<td>".$dateformated."<input type='hidden' name='date[]' value='".$date."'></td>";
	echo"<td>".$day."</td>";

	$allotment = '';
	$query_tag_allotment = '//hotel[@id="'.$hoteloid.'"]/masterroom[@id="'.$roomoid.'"]/rate[@date="'.$date.'"]/allotment/channel[@type="'.$channeltype.'"]';
	foreach($xml->xpath($query_tag_allotment) as $allocation) {
		$allotment = $allocation;
	}

	$query_tag = '//hotel[@id="'.$hoteloid.'"]/masterroom[@id="'.$roomoid.'"]/rate[@date="'.$date.'"]/rateplan[@id="'.$roomoffer.'"]/channel[@type="'.$channeltype.'"]';
	foreach($xml->xpath($query_tag) as $rate) {
		list($extendedpolicy, $netsingle, $single, $netdouble, $double, $netextrabed, $extrabed, $currency, $topup, $breakfast, $minstay, $maxstay, $blackout, $surcharge) = array($rate->extendedpolicy , $rate->netsingle , $rate->single , $rate->netdouble , $rate->double , $rate->netextrabed , $rate->extrabed , $rate->currency, $rate->topup , $rate->breakfast , $rate->minstay , $rate->maxstay ,  $rate->blackout , $rate->surcharge);
	}
	//----
	/*
	try {
		$stmt = $db->query("SELECT `gfilename` FROM `log_import`");
		$r_log = $stmt->fetchAll(PDO::FETCH_ASSOC);

		foreach($r_log as $row){
			$gfilename = $row['gfilename'];
			$xmlext = simplexml_load_file("../../data-xml/external/".$gfilename);

			foreach($xmlext->xpath($query_tag) as $rate) {

				list($extendedpolicy, $netsingle, $single, $netdouble, $double, $netextrabed, $extrabed, $currency, $allotment, $topup, $breakfast, $minstay, $maxstay, $blackout, $surcharge) = array($rate->extendedpolicy , $rate->netsingle , $rate->single , $rate->netdouble , $rate->double , $rate->netextrabed , $rate->extrabed , $rate->currency , $rate->allotment , $rate->topup , $rate->breakfast , $rate->minstay , $rate->maxstay ,  $rate->blackout , $rate->surcharge);

			}
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}
	*/
	//----

//	echo"<td>".$netsingle."</td>";
//	echo"<td><input type='text' name='single[]' value='".$single."'></td>";
//	echo"<td>".$netdouble."</td>";
	echo"<td><input type='text' name='double[]' value='".floatval($double)."'></td>";
//	echo"<td>".$netextrabed."</td>";
	echo"<td><input type='text' name='extrabed[]' value='".floatval($extrabed)."'></td>";
	echo"<td><input type='text' name='allotment[]' class='short' value='".$allotment."'></td>";
//	echo"<td><input type='text' name='minstay[]' class='short' value='".$minstay."'></td>";
//	echo"<td><input type='text' name='maxstay[]' class='short' value='".$maxstay."'></td>";
/*	if($blackout == 'y'){ $checked = "checked"; }else{ $checked=""; $blackout="n"; }
	echo"<td><input type='checkbox' name='bo[]' value='y' ".$checked."><input type='hidden' name='blackout[]' value='".$blackout."'></td>";
*/
	if($breakfast == 'y'){ $checked = "checked"; }else{ $checked=""; $breakfast="n"; }
	echo"<td><input type='checkbox' name='bf[]' value='y' ".$checked."><input type='hidden' name='breakfast[]' value='".$breakfast."'></td>";
	if($surcharge == 'y'){ $checked = "checked"; }else{ $checked=""; $surcharge="n"; }
//	echo"<td><input type='checkbox' name='sc[]' value='y' ".$checked."><input type='hidden' name='surcharge[]' value='".$surcharge."'></td>";
	echo"<td>&nbsp;</td>";

	echo"</tr>";

}
?>
    </table>
    <br />
    <div style="text-align:right">
    <button type="submit" class="small-button blue">Save</button>
    &nbsp;&nbsp;
    <button type="reset" class="small-button blue">Reset</button>
    </div>
