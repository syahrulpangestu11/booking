<?php
include("../../conf/connection.php");

$hoteloid = $_POST['hoteloid'];
$infantageuntil = isset($_POST['infantageuntil']) ? $_POST['infantageuntil'] : "";
$childagefrom = isset($_POST['childagefrom']) ? $_POST['childagefrom'] : "";
$childageuntil = isset($_POST['childageuntil']) ? $_POST['childageuntil'] : "";
$minguestage = isset($_POST['minguestage']) ? $_POST['minguestage'] : "";
$extrabedagefrom = isset($_POST['extrabedagefrom']) ? $_POST['extrabedagefrom'] : "";
$childstayfree = isset($_POST['childstayfree']) ? $_POST['childstayfree'] : "";
$smsbookingnotif = isset($_POST['smsbookingnotif']) ? $_POST['smsbookingnotif'] : "";
$ycsbookingemail = isset($_POST['ycsbookingemail']) ? $_POST['ycsbookingemail'] : "";
$uble = isset($_POST['unacknowledgebookinglistemail']) ? $_POST['unacknowledgebookinglistemail'] : "";
$citble = isset($_POST['checkintomorrowbookinglistemail']) ? $_POST['checkintomorrowbookinglistemail'] : "";

	$stmt = $db->prepare("SELECT `hoteloid` FROM `hotelsettings` WHERE `hoteloid`=:a");
	$stmt->execute(array(':a' => $hoteloid));
	$row_count = $stmt->rowCount();
	
	if($row_count>0){
		try {
			$stmt = $db->prepare("UPDATE `hotelsettings` SET `infantageuntil`= :a, `childagefrom`= :b,`childageuntil`= :c, `minguestage`= :d,`extrabedagefrom`= :e,`childstayfree`= :f,`smsbookingnotif`= :g,`ycsbookingemail`= :h,`unacknowledgebookinglistemail`= :i,`checkintomorrowbookinglistemail`= :j WHERE `hoteloid`= :k");
			$stmt->execute(array(':a' => $infantageuntil, ':b' => $childagefrom, ':c' => $childageuntil, ':d' => $minguestage, ':e' => $extrabedagefrom, ':f' => $childstayfree, ':g' => $smsbookingnotif, ':h' => $ycsbookingemail, ':i' => $uble, ':j' => $citble, ':k' => $hoteloid));
		}catch(PDOException $ex) {
			echo "0";
			die();
		}
	}else{
		try {
			$stmt = $db->prepare("INSERT INTO `hotelsettings`(`hoteloid`, `infantageuntil`, `childagefrom`, `childageuntil`, `minguestage`, `extrabedagefrom`, `childstayfree`, `smsbookingnotif`, `ycsbookingemail`, `unacknowledgebookinglistemail`, `checkintomorrowbookinglistemail`) VALUES (:k,:a, :b, :c, :d, :e, :f, :g, :h, :i, :j)");
			$stmt->execute(array(':a' => $infantageuntil, ':b' => $childagefrom, ':c' => $childageuntil, ':d' => $minguestage, ':e' => $extrabedagefrom, ':f' => $childstayfree, ':g' => $smsbookingnotif, ':h' => $ycsbookingemail, ':i' => $uble, ':j' => $citble, ':k' => $hoteloid));
		}catch(PDOException $ex) {
			echo "0";
			die();
		}
	}
	
	$id = array(); $name = array(); $email = array(); $new_name = array(); $new_email = array();	

	if(isset($_POST['id'])){
		foreach($_POST['id'] as $key => $value){ array_push($id, $value); }
	}
	if(isset($_POST['name'])){
		foreach($_POST['name'] as $key => $value){ array_push($name, $value); }
	}
	if(isset($_POST['email'])){
		foreach($_POST['email'] as $value){ array_push($email, $value); }
	}
	if(isset($_POST['new_name'])){
		foreach($_POST['new_name'] as $value){ array_push($new_name, $value); }
	}
	if(isset($_POST['new_email'])){
		foreach($_POST['new_email'] as $value){ array_push($new_email, $value); }
	}
	
	try {
		$stmt = $db->prepare("update mailerhotel set publishedoid = :a where hoteloid = :b");
		$stmt->execute(array(':a' => 3, ':b' => $hoteloid));
	}catch(PDOException $ex) {
		echo "Invalid Query";
		print($ex);
		die();
	}	
	
	if(count($id)>0 and count($name)>0 and count($email)>0){
		foreach($id as $key => $value){
			try {
				$stmt = $db->prepare("update mailerhotel set name = :a, email = :b, publishedoid = :c where mailerhoteloid = :d");
				$stmt->execute(array(':a' => $name[$key] ,':b' => $email[$key],':c' => 1 ,':d' => $value));
			}catch(PDOException $ex){
				echo "Invalid Query";
				print($ex);
				die();
			}
		}
	}
	
	try {
		$stmt = $db->prepare("delete from mailerhotel where publishedoid = :a and hoteloid = :b");
		$stmt->execute(array(':a' => 3, ':b' => $hoteloid));
	}catch(PDOException $ex) {
		echo "Invalid Query";
		print($ex);
		die();
	}	
	
	if(count($new_name)>0 and count($new_email)>0){
		foreach($new_name as $key => $value){
			try {
				$stmt = $db->prepare("insert into mailerhotel (name, email, hoteloid) values (:a , :b , :c)");
				$stmt->execute(array(':a' => $value, ':b' => $new_email[$key], ':c' => $hoteloid));
			}catch(PDOException $ex) {
				echo "Invalid Query";
				print($ex);
				die();
			}	
		}
	}
	
	echo "1";
	
?>