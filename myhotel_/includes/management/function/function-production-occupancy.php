<?php
function pdCountBooking($start, $end, $hoteloid, $status){
	global $db;

	$main_query = "select count(DISTINCT b.bookingoid) as jmldata from booking b inner join hotel h using (hoteloid) inner join bookingroom br using (bookingoid) inner join roomoffer ro using (roomofferoid) inner join room r using (roomoid) where (date(bookingtime) >= '".$start."' and date(bookingtime) <= '".$end."') and h.hotelstatusoid = '1' and b.pmsstatus = '0'";
	$filter = array();
	if($status != ""){
		if(is_array($status)){
			$filter_status = implode("','", $status);
			array_push($filter, "b.bookingstatusoid in ('".$filter_status."')");
		}else{
			array_push($filter, "b.bookingstatusoid = '".$status."'");
		}
	}
	if(!empty($hoteloid)){
		array_push($filter, "h.hoteloid = '".$hoteloid."'");
	}

	if(count($filter) > 0){
		$combine_filter = implode(' and ',$filter);
		$query = $main_query.' and '.$combine_filter;
	}else{
		$query = $main_query;
	}

	$stmt	= $db->query($query);
	$result = $stmt->fetch(PDO::FETCH_ASSOC);

	return $result['jmldata'];
}

function pdRoomNight($start, $end, $hoteloid, $status){
	global $db;

	$main_query = "select count(DISTINCT brd.bookingroomdtloid) as jmldata from bookingroomdtl brd inner join bookingroom br using (bookingroomoid) inner join booking b using (bookingoid) inner join hotel h using (hoteloid) where (date(bookingtime) >= '".$start."' and date(bookingtime) <= '".$end."') and h.hotelstatusoid = '1' and b.pmsstatus = '0'";
	$filter = array();

	if($status != ""){
		if(is_array($status)){
			$filter_status = implode("','", $status);
			array_push($filter, "b.bookingstatusoid in ('".$filter_status."')");
		}else{
			array_push($filter, "b.bookingstatusoid = '".$status."'");
		}
	}
	if(!empty($hoteloid)){
		array_push($filter, "h.hoteloid = '".$hoteloid."'");
	}

	if(count($filter) > 0){
		$combine_filter = implode(' and ',$filter);
		$query = $main_query.' and '.$combine_filter;
	}else{
		$query = $main_query;
	}

	$stmt	= $db->query($query);
	$result = $stmt->fetch(PDO::FETCH_ASSOC);

	return $result['jmldata'];
}

function pdSumValue($start, $end, $field, $hoteloid, $status){
	global $db;
	$sum_value 	= "select sum(".$field.") as value from booking b inner join hotel h using (hoteloid) where (date(bookingtime) >= '".$start."' and date(bookingtime) <= '".$end."') and h.hotelstatusoid = '1' and b.pmsstatus = '0'";
	$filter = array();
	if($status != ""){
		if(is_array($status)){
			$filter_status = implode("','", $status);
			array_push($filter, "b.bookingstatusoid in ('".$filter_status."')");
		}else{
			array_push($filter, "b.bookingstatusoid = '".$status."'");
		}
	}
	if(!empty($hoteloid)){
		array_push($filter, "h.hoteloid = '".$hoteloid."'");
	}

	if(count($filter) > 0){
		$combine_filter = implode(' and ',$filter);
		$sum_value = $sum_value.' and '.$combine_filter;
	}

	$stmt		= $db->query($sum_value);
	$arr_value 	= $stmt->fetch(PDO::FETCH_ASSOC);
	$value		= floor($arr_value['value']);

	return $value;
}

/*----------------------------------------------------------------------------------------*/


function ocCountBooking($start, $end, $hoteloid, $status){
	global $db;
	if(is_array($status)){
		$status = implode("','", $status);
	}
	$filter_hotel = (!empty($hoteloid)) ? "and h.hoteloid = '".$hoteloid."'" : "";

	$query = "select count(bookingoid) as occupancy_booking
from (select b.bookingoid, min(checkin) as min_checkin from bookingroom br inner join booking b using (bookingoid) inner join hotel h using (hoteloid) where h.hotelstatusoid = '1' ".$filter_hotel." and b.bookingstatusoid in ('".$status."')  and b.pmsstatus = '0' group by b.bookingoid) as t1
where (min_checkin >= '".$start."' and min_checkin <= '".$end."')";
	$stmt	= $db->query($query);
	$result = $stmt->fetch(PDO::FETCH_ASSOC);

	return $result['occupancy_booking'];
}

function ocRoomNight($start, $end, $hoteloid, $status){
	global $db;
	if(is_array($status)){
		$status = implode("','", $status);
	}
	$filter_hotel = (!empty($hoteloid)) ? "and h.hoteloid = '".$hoteloid."'" : "";

	$query = "select count(bookingroomdtloid) as occupancy_roomnight from bookingroomdtl inner join bookingroom br using (bookingroomoid) where br.bookingoid in (select bookingoid from (select b.bookingoid, min(checkin) as min_checkin from bookingroom br inner join booking b using (bookingoid) inner join hotel h using (hoteloid) where h.hotelstatusoid = '1' ".$filter_hotel." and b.bookingstatusoid in ('".$status."') and b.pmsstatus = '0' group by b.bookingoid) as t1 where (min_checkin >= '".$start."' and min_checkin <= '".$end."'))";
	$stmt	= $db->query($query);
	$result = $stmt->fetch(PDO::FETCH_ASSOC);

	return $result['occupancy_roomnight'];
}

function ocSumValue($start, $end, $field, $hoteloid, $status){
	global $db;
	if(is_array($status)){
		$status = implode("','", $status);
	}
	$filter_hotel = (!empty($hoteloid)) ? "and h.hoteloid = '".$hoteloid."'" : "";

	$query = "select sum(".$field.") as value
from (select b.".$field.", min(checkin) as min_checkin from bookingroom br inner join booking b using (bookingoid) inner join hotel h using (hoteloid) where h.hotelstatusoid = '1' ".$filter_hotel." and b.pmsstatus = '0' group by b.bookingoid) as t1
where (min_checkin >= '".$start."' and min_checkin <= '".$end."')";
	$stmt	= $db->query($query);
	$result = $stmt->fetch(PDO::FETCH_ASSOC);

	return $result['value'];

}

function pdSumValueByPIC($start, $end, $field, $hoteloid, $status, $useroid){
	global $db;
	$sum_value 	= "select sum(".$field.") as value from booking b inner join hotel h using (hoteloid) inner join affiliatelog al using (hoteloid) where (date(bookingtime) >= '".$start."' and date(bookingtime) <= '".$end."') and h.hotelstatusoid = '1' and b.pmsstatus = '0' and al.to_type = 'sm' and al.to_status in ('8', '15') and al.useroid = '".$useroid."'";
	$filter = array();
	if($status != ""){
		if(is_array($status)){
			$filter_status = implode("','", $status);
			array_push($filter, "b.bookingstatusoid in ('".$filter_status."')");
		}else{
			array_push($filter, "b.bookingstatusoid = '".$status."'");
		}
	}
	if(!empty($hoteloid)){
		array_push($filter, "h.hoteloid = '".$hoteloid."'");
	}

	if(count($filter) > 0){
		$combine_filter = implode(' and ',$filter);
		$sum_value = $sum_value.' and '.$combine_filter;
	}

	$stmt		= $db->query($sum_value);
	$arr_value 	= $stmt->fetch(PDO::FETCH_ASSOC);
	$value		= floor($arr_value['value']);

	return $value;
}

function ocSumValueByPIC($start, $end, $field, $hoteloid, $status, $useroid){
	global $db;
	if(is_array($status)){
		$status = implode("','", $status);
	}
	$filter_hotel = (!empty($hoteloid)) ? "and h.hoteloid = '".$hoteloid."'" : "";

	$query = "select sum(".$field.") as value
from (select b.".$field.", min(checkin) as min_checkin from bookingroom br inner join booking b using (bookingoid) inner join hotel h using (hoteloid) inner join affiliatelog al using (hoteloid) where h.hotelstatusoid = '1' ".$filter_hotel." and b.pmsstatus = '0' and al.to_type = 'sm' and al.to_status in ('8', '15') and al.useroid = '".$useroid."' group by b.bookingoid) as t1
where (min_checkin >= '".$start."' and min_checkin <= '".$end."')";
	$stmt	= $db->query($query);
	$result = $stmt->fetch(PDO::FETCH_ASSOC);

	return $result['value'];

}
?>
