<?php
	function trend($current, $last){
		if($current > $last){
			return "<span class='trend green'><i class='fa fa-arrow-up'></i></span>";
		}else if($current < $last){
			return "<span class='trend red'><i class='fa fa-arrow-down'></i></span>";
		}else{
			return "<span class='trend'><i class='fa fa-exchange'></i></span>";
		}
	}

	function occupancyBooking($startdate, $enddate, $hoteloid, $status){
		global $db;
		if(is_array($status)){
			$status = implode("','", $status);
		}
		$filter_hotel = (!empty($hoteloid)) ? "and h.hoteloid = '".$hoteloid."'" : "";

		$query = "select count(bookingoid) as occupancy_booking
from (select b.bookingoid, min(checkin) as min_checkin from bookingroom br inner join booking b using (bookingoid) inner join hotel h using (hoteloid) where h.hotelstatusoid = '1' ".$filter_hotel." and b.bookingstatusoid in ('".$status."') and b.pmsstatus = '0' group by b.bookingoid) as t1
where (min_checkin >= '".$startdate."' and min_checkin <= '".$enddate."')";
		$stmt	= $db->query($query);
		$result = $stmt->fetch(PDO::FETCH_ASSOC);

		return $result['occupancy_booking'];
		//return $query;
	}

	function occupancyRoomNight($startdate, $enddate, $hoteloid, $status){
		global $db;
		if(is_array($status)){
			$status = implode("','", $status);
		}
		$filter_hotel = (!empty($hoteloid)) ? "and h.hoteloid = '".$hoteloid."'" : "";

		$query = "select count(bookingroomdtloid) as occupancy_roomnight from bookingroomdtl inner join bookingroom br using (bookingroomoid) where br.bookingoid in (select bookingoid from (select b.bookingoid, min(checkin) as min_checkin from bookingroom br inner join booking b using (bookingoid) inner join hotel h using (hoteloid) where h.hotelstatusoid = '1' ".$filter_hotel." and b.bookingstatusoid in ('".$status."') and b.pmsstatus = '0' group by b.bookingoid) as t1 where (min_checkin >= '".$startdate."' and min_checkin <= '".$enddate."'))";
		$stmt	= $db->query($query);
		$result = $stmt->fetch(PDO::FETCH_ASSOC);

		return $result['occupancy_roomnight'];
		//return $query;
	}

	function occupancyRevenue($startdate, $enddate, $hoteloid, $status){
		global $db;
		if(is_array($status)){
			$status = implode("','", $status);
		}
		$filter_hotel = (!empty($hoteloid)) ? "and h.hoteloid = '".$hoteloid."'" : "";

		$query = "select sum(grandtotal) as occupancy_revenue
from (select b.grandtotal, min(checkin) as min_checkin from bookingroom br inner join booking b using (bookingoid) inner join hotel h using (hoteloid) where h.hotelstatusoid = '1' ".$filter_hotel." and b.bookingstatusoid in ('".$status."') and b.pmsstatus = '0' group by b.bookingoid) as t1
where (min_checkin >= '".$startdate."' and min_checkin <= '".$enddate."')";
		$stmt	= $db->query($query);
		$result = $stmt->fetch(PDO::FETCH_ASSOC);

		return $result['occupancy_revenue'];
	}

	function occupancyCommission($startdate, $enddate, $hoteloid){
		global $db;
		if(is_array($status)){
			$status = implode("','", $status);
		}
		$filter_hotel = (!empty($hoteloid)) ? "and h.hoteloid = '".$hoteloid."'" : "";

		$query = "select sum(gbhcollect) as occupancy_commission
from (select b.gbhcollect, min(checkin) as min_checkin from bookingroom br inner join booking b using (bookingoid) inner join hotel h using (hoteloid) where h.hotelstatusoid = '1' ".$filter_hotel." and b.pmsstatus = '0' group by b.bookingoid) as t1
where (min_checkin >= '".$startdate."' and min_checkin <= '".$enddate."')";
		$stmt	= $db->query($query);
		$result = $stmt->fetch(PDO::FETCH_ASSOC);

		return $result['occupancy_commission'];
	}

	function RoomNight($start, $end, $hoteloid, $status){
		global $db;

		$main_query = "select count(DISTINCT brd.bookingroomdtloid) as jmldata from bookingroomdtl brd inner join bookingroom br using (bookingroomoid) inner join booking b using (bookingoid) inner join hotel h using (hoteloid) where (date(brd.date) >= '".$start."' and date(brd.date) <= '".$end."') and b.pmsstatus = '0' and h.hotelstatusoid = '1'";

		$filter = array();
		if(!empty($hoteloid) and $hoteloid != 0){
			array_push($filter, "h.hoteloid = '".$hoteloid."'");
		}
		if($status != ""){
			if(is_array($status)){
				$filter_status = implode("','", $status);
				array_push($filter, "b.bookingstatusoid in ('".$filter_status."')");
			}else{
				array_push($filter, "b.bookingstatusoid = '".$status."'");
			}
		}

		if(count($filter) > 0){
			$combine_filter = implode(' and ',$filter);
			$query = $main_query.' and '.$combine_filter;
		}else{
			$query = $main_query;
		}

		$stmt	= $db->query($query);
		$result = $stmt->fetch(PDO::FETCH_ASSOC);

		return $result['jmldata'];
	}

	function RoomNightOccupany($start, $end, $status){
		global $db;
		if(is_array($status)){
			$status = implode("','", $status);
		}

		$query = "select count(bookingroomdtloid) as roomnight from bookingroomdtl inner join bookingroom br using (bookingroomoid) where br.bookingoid in (select bookingoid from (select b.bookingoid, min(checkin) as min_checkin from bookingroom br inner join booking b using (bookingoid) inner join hotel h using (hoteloid) where  h.hotelstatusoid = '1' and b.bookingstatusoid in ('".$status."') and b.pmsstatus = '0' group by b.bookingoid) as t1 where (min_checkin >= '".$start."' and min_checkin <= '".$end."'))";
		$stmt	= $db->query($query);
		$result = $stmt->fetch(PDO::FETCH_ASSOC);

		return $result['roomnight'];
	}

	function BookingOccupancy($start, $end, $status){
		global $db;
		if(is_array($status)){
			$status = implode("','", $status);
		}

		$query = "select count(bookingoid) as totalbooking
from (select b.bookingoid, min(checkin) as min_checkin from bookingroom br inner join booking b using (bookingoid) inner join hotel h using (hoteloid) where h.hotelstatusoid = '1' and b.bookingstatusoid in ('".$status."') and b.pmsstatus = '0' group by b.bookingoid) as t1
where (min_checkin >= '".$start."' and min_checkin <= '".$end."')";
		$stmt	= $db->query($query);
		$result = $stmt->fetch(PDO::FETCH_ASSOC);

		return $result['totalbooking'];
	}
?>
