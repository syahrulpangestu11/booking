<style>
table#table-dashboard-management tr:first-of-type, table#table-dashboard-management tr:nth-of-type(2) {
	background-color: #e0e0e0;
}
table#table-dashboard-management tr > th {
	text-align:center;
}
span.trend{ font-size:1.2em; }
.green{ color:#063; }
.red{ color:#900; }
h4.title-chart{ text-align:center; font-weight:bold; font-size:1.05em; }
</style>
<?php
	include ('includes/bootstrap.php');
	include('includes/management/management-dashboard/function-management-dashboard.php');
	include('includes/reports/function/function-report.php');
	
	$_POST['year_1'] = date('Y');
	$_POST['month_1'] = date('m');
	$year = $_POST['year_1'];
	$month = $_POST['month_1'];
	
	$from_year = $year."-01-01";
	$to_year = date('Y-m-d');
	
	$from	= $year."-".$month."-01";
	$to		= $year."-".$month."-".date('t', strtotime($from));
	
	if($month == 1){
		$month_before	= 12;
		$year_before	= $year - 1;
	}else{
		$month_before	= $month - 1;
		$year_before	= $year;
	}
	$from_before	= $year_before."-".$month_before."-01";
	$to_before		= date('Y-m-t', strtotime($from_before));
	
	$from_this_month		= $year."-".$month."-01";
	$to_this_month			= date('Y-m-t', strtotime($from_this_month));
	$from_before_occupancy	= $year_before."-".$month_before."-01";
	$to_before_occupancy	= $year_before."-".$month_before."-".date('t', strtotime($from_before));

	$tm_booking = countResult($from, $to, '', '', 4);
	$lm_booking = countResult($from_before, $to_before, '', '', 4);
	$booking_ytd = countResult($from_year, $to_year, '', '', 4);
	$yt_booking = "N/A";
	$ytp_booking = "N/A";
	
	$tm_roomnight = countRoomNight($from, $to, '', '');
	$lm_roomnight = countRoomNight($from_before, $to_before, '', '');
	$roomnight_ytd = countRoomNight($from_year, $to_year, '', '');
	$yt_roomnight = "N/A";
	$ytp_roomnight = "N/A";
	
	$tm_sales = SumRevenue($from, $to, '');
	$lm_sales = SumRevenue($from_before, $to_before,'');
	$sales_ytd = SumRevenue($from_year, $to_year, '');
	$yt_sales = "N/A";
	$ytp_sales = "N/A";

	$tm_comm = SumCommission($from, $to, '');
	$lm_comm = SumCommission($from_before, $to_before,'');
	$comm_ytd = SumCommission($from_year, $to_year, '');
	$yt_comm = "N/A";
	$ytp_comm = "N/A";
		
	$show_date = array(); $point_reservation = array(); $point_confirmed = array(); $point_cancelled = array();
	$point_noshow = array();
	$point_roomnight = array(); $point_roomnight_cancelled = array();
	$point_commission = array(); $point_revenue = array();
		
	for($i = 1; $i <= date('m'); $i++){
		$chart_start_date	= $year.'-'.$i.'-01';
		$chart_to_date	= date('Y-m-t', strtotime($chart_start_date));
		
		array_push($show_date, date('M', strtotime($chart_start_date)));
		
		array_push($point_reservation, countResult($chart_start_date, $chart_to_date, "", "", array(4, 5, 7, 8)));
		array_push($point_confirmed, countResult($chart_start_date, $chart_to_date, "", "", 4));
		array_push($point_cancelled, countResult($chart_start_date, $chart_to_date, "", "", array(5,7,8)));
		
		array_push($point_roomnight, RoomNight($chart_start_date, $chart_to_date, "", 4));
		array_push($point_roomnight_cancelled, RoomNight($chart_start_date, $chart_to_date, "", array(5,7,8)));
		
		$commission_legend = floatval(str_replace(',', '', SumCommission($chart_start_date, $chart_to_date, '')));
		if($commission_legend > 0){
			$commission_legend = $commission_legend / 1000;
		}
		$revenue_legend = floatval(str_replace(',', '', SumRevenue($chart_start_date, $chart_to_date, '')));
		if($revenue_legend > 0){
			$revenue_legend = $revenue_legend / 1000;
		}
		
		array_push($point_commission, $commission_legend);
		array_push($point_revenue, $revenue_legend);
	}
	
	
	/* -------------------------------------------------------------------------------------------------------*/
	
	$tmp_booking	= occupancyBooking($from_this_month, $to_this_month, "", 4);
	$lmp_booking	= occupancyBooking($from_before_occupancy, $to_before_occupancy, "", 4);
	$bookingp_ytd	= occupancyBooking($from_year, $to_this_month, "", 4);
	
	$tmp_roomnight	= occupancyRoomNight($from, $to, "", 4);
	$lmp_roomnight	= occupancyRoomNight($from_before_occupancy, $to_before_occupancy, "", 4);
	$roomnightp_ytd	= occupancyRoomNight($from_year, $to_this_month, "", 4);

	$tmp_sales 		= occupancyRevenue($from, $to, "", 4);
	$lmp_sales 		= occupancyRevenue($from_before_occupancy, $to_before_occupancy, "", 4);
	$salesp_ytd 	= occupancyRevenue($from_year, $to_this_month, "", 4);
	
	$tmp_comm		= occupancyCommission($from, $to, "");
	$lmp_comm		= occupancyCommission($from_before_occupancy, $to_before_occupancy, "");
	$commp_ytd 		= occupancyCommission($from_year, $to_this_month, "");
	
	$show_date_occupancy = array();
	$point_roomnight_occupancy = array(); $point_roomnight_cancelled_occupancy = array(); 
	$point_confirmed_occupancy = array(); $point_cancelled_occupancy = array();
	$point_revenue_occupancy = array(); $point_commission_occupancy = array();
	
	for($i = 1; $i <= 12; $i++){
		$chart_start_date	= $year.'-'.$i.'-01';
		$chart_to_date	= date('Y-m-t', strtotime($chart_start_date));
		
		array_push($show_date_occupancy, date('M', strtotime($chart_start_date)));
		array_push($point_roomnight_occupancy, RoomNightOccupany($chart_start_date, $chart_to_date, 4));
		array_push($point_roomnight_cancelled_occupancy, occupancyRoomNight($chart_start_date, $chart_to_date, "", array(5,7,8)));	
		array_push($point_confirmed_occupancy, occupancyBooking($chart_start_date, $chart_to_date, "", 4));
		array_push($point_cancelled_occupancy, occupancyBooking($chart_start_date, $chart_to_date, "", array(5,7,8)));
		
		$commission_legend = occupancyCommission($chart_start_date, $chart_to_date, "");
		if($commission_legend > 0){
			$commission_legend = $commission_legend / 1000;
		}else{
			$commission_legend = 0;
		}
		
		$revenue_legend = occupancyRevenue($chart_start_date, $chart_to_date, "", array(4, 5, 7, 8));
		if($revenue_legend > 0){
			$revenue_legend = $revenue_legend / 1000;
		}else{
			$revenue_legend = 0;
		}

		array_push($point_revenue_occupancy, $revenue_legend);		
		array_push($point_commission_occupancy, $commission_legend);		
	}
?>
<div id="newdashboard">
	<div class="box">
        <div class="box-body">
        	<div class="row">
            	<div class="col-md-12 text-center"><h2>DASHBOARD MANAGEMENT</h2></div>
            </div>
            <div class="row">
            	<div class="col-md-12">
                    <h4>Hotel Production</h4>
				</div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table id="table-dashboard-management" class="table table-striped table-bordered">
                        <tr>
                            <th rowspan="2">&nbsp;</th>
                            <th rowspan="2">This Month</th>
                            <th rowspan="2">Last Month</th>
                            <th rowspan="2">Trend</th>
                            <th rowspan="2">YTD</th>
                            <th colspan="2">Year Target</th>
                        </tr>
                        <tr>
                            <th>%</th>
                            <th>IDR</th>
                        </tr>
                        <tr>
                        	<td>Booking</td>
                            <td><?=$tm_booking?></td>
                            <td><?=$lm_booking?></td>
                            <td><?=trend($tm_booking, $lm_booking)?></td>
                            <td><?=$booking_ytd?></td>
                            <td><?=$yt_booking?></td>
                            <td><?=$ytp_booking?></td>
                        </tr>
                        <tr>
                        	<td>Room Night</td>
                            <td><?=$tm_roomnight?></td>
                            <td><?=$lm_roomnight?></td>
                            <td><?=trend(str_replace(',', '', $tm_roomnight), str_replace(',', '', $lm_roomnight))?></td>
                            <td><?=$roomnight_ytd?></td>
                            <td><?=$yt_roomnight?></td>
                            <td><?=$ytp_roomnight?></td>
                        </tr>
                        <tr>
                        	<td>Sales</td>
                            <td><?=$tm_sales?></td>
                            <td><?=$lm_sales?></td>
                            <td><?=trend(str_replace(',', '', $tm_sales), str_replace(',', '', $lm_sales))?></td>
                            <td><?=$sales_ytd?></td>
                            <td><?=$yt_sales?></td>
                            <td><?=$ytp_sales?></td>
                        </tr>
                        <tr>
                        	<td>Commission</td>
                            <td><?=$tm_comm?></td>
                            <td><?=$lm_comm?></td>
                            <td><?=trend(str_replace(',', '', $tm_comm), str_replace(',', '', $lm_comm))?></td>
                            <td><?=$comm_ytd?></td>
                            <td><?=$yt_comm?></td>
                            <td><?=$ytp_comm?></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="row">
            	<div class="col-md-6">
                    <div class="chart">
                   		<canvas id="revenue-chart"></canvas>
                    </div>
                    <h4 class="title-chart">Production Revenue &amp; Commission <?php echo $year; ?></h4>
                </div>
            	<div class="col-md-6">
                    <div class="chart">
                   		<canvas id="chart-production"></canvas>
                    </div>
                    <h4 class="title-chart">Production Booking &amp; Room Night <?php echo $year; ?></h4>
                </div>
            </div>
            <div class="row">
            	<div class="col-md-12">
                    <h4>Hotel Occupancy (TheB&uuml;king Distributed Revenue)</h4>
				</div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table id="table-dashboard-management" class="table table-striped table-bordered">
                        <tr>
                            <th rowspan="2">&nbsp;</th>
                            <th rowspan="2">This Month</th>
                            <th rowspan="2">Last Month</th>
                            <th rowspan="2">Trend</th>
                            <th rowspan="2">YTD</th>
                            <th colspan="2">Year Target</th>
                        </tr>
                        <tr>
                            <th>%</th>
                            <th>IDR</th>
                        </tr>
                        <tr>
                        	<td>Booking</td>
                            <td><?=number_format($tmp_booking)?></td>
                            <td><?=number_format($lmp_booking)?></td>
                            <td><?=trend($tmp_booking, $lmp_booking)?></td>
                            <td><?=number_format($bookingp_ytd)?></td>
                            <td><?=$yt_booking?></td>
                            <td><?=$ytp_booking?></td>
                        </tr>
                        <tr>
                        	<td>Room Night</td>
                            <td><?=number_format($tmp_roomnight)?></td>
                            <td><?=number_format($lmp_roomnight)?></td>
                            <td><?=trend($tmp_roomnight, $lmp_roomnight)?></td>
                            <td><?=number_format($roomnightp_ytd)?></td>
                            <td><?=$yt_roomnight?></td>
                            <td><?=$ytp_roomnight?></td>
                        </tr>
                        <tr>
                        	<td>Sales</td>
                            <td><?=number_format($tmp_sales)?></td>
                            <td><?=number_format($lmp_sales)?></td>
                            <td><?=trend(str_replace(',', '', $tmp_sales), str_replace(',', '', $lmp_sales))?></td>
                            <td><?=number_format($salesp_ytd)?></td>
                            <td><?=$yt_sales?></td>
                            <td><?=$ytp_sales?></td>
                        </tr>
                        <tr>
                        	<td>Commission</td>
                            <td><?=number_format($tmp_comm)?></td>
                            <td><?=number_format($lmp_comm)?></td>
                            <td><?=trend(str_replace(',', '', $tmp_comm), str_replace(',', '', $lmp_comm))?></td>
                            <td><?=number_format($commp_ytd)?></td>
                            <td><?=$yt_comm?></td>
                            <td><?=$ytp_comm?></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="row">
            	<div class="col-md-6">
                    <div class="chart">
                   		<canvas id="occupancy-revenue-chart"></canvas>
                    </div>
                    <h4 class="title-chart">Occupancy Revenue &amp; Commission <?php echo $year; ?></h4>
                </div>
            	<div class="col-md-6">
                    <div class="chart">
                   		<canvas id="chart-occupancy"></canvas>
                    </div>
                    <h4 class="title-chart">Occupancy Booking &amp; Room Night <?php echo $year; ?></h4>
                </div>
            </div>
            <div class="row" style="margin-top:30px;">
            	<div class="col-md-12 text-center">
                	<a href="<?=$base_url;?>/transaction-details/production"><button type="button" class="btn btn-success">View Transaction Detail Production</button></a>
                	<a href="<?=$base_url;?>/transaction-details/occupancy"><button type="button" class="btn btn-warning">View Transaction Detail Occupancy</button></a>
                </div>
            </div>
    	</div>
    </div>
</div>


<script src="<?php echo $base_url?>/lib/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<!-- chartjs -->
<script src="<?php echo $base_url?>/lib/plugins/chartjs/Chart.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.4/Chart.bundle.min.js" type="text/javascript"></script>


<script type="text/javascript">
    $(function () {
        'use strict';
		
        //---------------------------
        // PRODUCTION REVENUE CHART
        //---------------------------
		
		var revenue_chart = document.getElementById('revenue-chart');
		new Chart(revenue_chart, {
		  type: 'line',
		  data: {
			labels: [<?php echo '"'.implode('","', $show_date).'"'; ?>],
			datasets: [{
			  label: 'Revenue',
			  yAxisID: 'Revenue',
			  data: [<?php echo implode(',', $point_revenue); ?>],
			  backgroundColor: "rgba(255, 255, 255, 0)",
			  borderColor: "rgba(255, 188, 0, 1)",
			}, {
			  label: 'Commission',
			  yAxisID: 'Commission',
			  data: [<?php echo implode(',', $point_commission); ?>],
			  backgroundColor: "rgba(255, 255, 255, 0)",
			  borderColor: "rgba(51, 175, 20, 1)",
			}]
		  },
		  options: {
			scales: {
			  yAxes: [{
				id: 'Revenue',
				type: 'linear',
				position: 'left',
			  }, {
				id: 'Commission',
				type: 'linear',
				position: 'right',
			  }]
			}
		  }
		});	

        //---------------------------
        // OCCUPANCY REVENUE CHART
        //---------------------------
		
		var revenue_chart_occupancy = document.getElementById('occupancy-revenue-chart');
		new Chart(revenue_chart_occupancy, {
		  type: 'line',
		  data: {
			labels: [<?php echo '"'.implode('","', $show_date_occupancy).'"'; ?>],
			datasets: [{
			  label: 'Revenue',
			  yAxisID: 'Revenue',
			  data: [<?php echo implode(',', $point_revenue_occupancy); ?>],
			  backgroundColor: "rgba(255, 255, 255, 0)",
			  borderColor: "rgba(255, 188, 0, 1)",
			}, {
			  label: 'Commission',
			  yAxisID: 'Commission',
			  data: [<?php echo implode(',', $point_commission_occupancy); ?>],
			  backgroundColor: "rgba(255, 255, 255, 0)",
			  borderColor: "rgba(51, 175, 20, 1)",
			}]
		  },
		  options: {
			scales: {
			  yAxes: [{
				id: 'Revenue',
				type: 'linear',
				position: 'left',
			  }, {
				id: 'Commission',
				type: 'linear',
				position: 'right',
			  }]
			}
		  }
		});	
		
        //---------------------------
        // PRODUCTION BOOKING CHART
        //---------------------------
		
		var chart_production = document.getElementById('chart-production');
		new Chart(chart_production, {
		  type: 'line',
		  data: {
			labels: [<?php echo '"'.implode('","', $show_date).'"'; ?>],
			datasets: [{
			  label: 'Confirmed',
			  yAxisID: 'Booking',
			  data: [<?php echo implode(',', $point_confirmed); ?>],
			  backgroundColor: "rgba(255, 255, 255, 0)",
			  borderColor: "rgba(12, 161, 206, 1)",
			}, {
			  label: 'Cancelled',
			  yAxisID: 'Booking',
			  data: [<?php echo implode(',', $point_cancelled); ?>],
			  backgroundColor: "rgba(255, 255, 255, 0)",
			  borderColor: "rgba(212, 17, 37, 1)",
			}, {
			  label: 'RN',
			  yAxisID: 'RN',
			  data: [<?php echo implode(',', $point_roomnight); ?>],
			  backgroundColor: "rgba(255, 255, 255, 0)",
			  borderColor: "rgba(95, 193, 134, 1)",
			}, {
			  label: 'RN Cancelled',
			  yAxisID: 'RN',
			  data: [<?php echo implode(',', $point_roomnight_cancelled); ?>],
			  backgroundColor: "rgba(255, 255, 255, 0)",
			  borderColor: "rgba(163, 60, 216, 1)",
			}]
		  },
		  options: {
			scales: {
			  yAxes: [{
				id: 'Booking',
				type: 'linear',
				position: 'left',
			  }, {
				id: 'RN',
				type: 'linear',
				position: 'right',
			  }]
			}
		  }
		});	
		
        //---------------------------
        // OCCUPANCY BOOKING CHART
        //---------------------------
		
		var chart_occupancy = document.getElementById('chart-occupancy');
		new Chart(chart_occupancy, {
		  type: 'line',
		  data: {
			labels: [<?php echo '"'.implode('","', $show_date_occupancy).'"'; ?>],
			datasets: [{
			  label: 'Confirmed',
			  yAxisID: 'Booking',
			  data: [<?php echo implode(',', $point_confirmed_occupancy); ?>],
			  backgroundColor: "rgba(255, 255, 255, 0)",
			  borderColor: "rgba(12, 161, 206, 1)",
			}, {
			  label: 'Cancelled',
			  yAxisID: 'Booking',
			  data: [<?php echo implode(',', $point_cancelled_occupancy); ?>],
			  backgroundColor: "rgba(255, 255, 255, 0)",
			  borderColor: "rgba(212, 17, 37, 1)",
			}, {
			  label: 'RN',
			  yAxisID: 'RN',
			  data: [<?php echo implode(',', $point_roomnight_occupancy); ?>],
			  backgroundColor: "rgba(255, 255, 255, 0)",
			  borderColor: "rgba(95, 193, 134, 1)",
			}, {
			  label: 'RN Cancelled',
			  yAxisID: 'RN',
			  data: [<?php echo implode(',', $point_roomnight_cancelled_occupancy); ?>],
			  backgroundColor: "rgba(255, 255, 255, 0)",
			  borderColor: "rgba(163, 60, 216, 1)",
			}]
		  },
		  options: {
			scales: {
			  yAxes: [{
				id: 'Booking',
				type: 'linear',
				position: 'left',
			  }, {
				id: 'RN',
				type: 'linear',
				position: 'right',
			  }]
			}
		  }
		});	
		
});
</script>
