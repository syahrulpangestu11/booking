<?php
  $date1 = date_create($startdate);
  $date2 = date_create($enddate);
  $diff  = date_diff($date1,$date2);
  $day   = $diff->format("%a");

  $query_hotel_occupancy = "select hoteloid, hotelname, count(bookingoid) as total_production from (select  h.hoteloid, h.hotelname, b.bookingoid, min(checkin) as min_checkin from bookingroom br inner join booking b using (bookingoid) inner join hotel h using (hoteloid) where  h.publishedoid not in (3) and h.hotelstatusoid = '1' and bookingstatusoid in ('4') and b.pmsstatus = '0' group by b.bookingoid) as t1 where (min_checkin >= '".$startdate."' and min_checkin <= '".$enddate."') group by hoteloid order by total_production desc limit 5";
  $stmt = $db->query($query_hotel_occupancy);
  $hotel_chain = $stmt->fetchAll(PDO::FETCH_ASSOC);
  $a_hoteloid = array();
  $a_hotelcode = array();
  $a_hotelname = array();
  foreach($hotel_chain as $row){
    array_push($a_hoteloid, $row['hoteloid']);
    array_push($a_hotelcode, $row['hotelcode']);
    array_push($a_hotelname, $row['hotelname']);

    $varname_production = 'production_'.$row['hoteloid'];
    $$varname_production = array();
  }

  $point_date = array();
  for($i = 0; $i <= $day; $i++){
      $date = date('Y-m-d', strtotime($startdate. ' +'.$i.' days'));
      array_push($point_date, date('d M', strtotime($date)));

      foreach($a_hoteloid as $key => $hoteloid){
          $sum_production = "select count(bookingoid) as total_production from (select h.hoteloid, h.hotelname, b.bookingoid, min(checkin) as min_checkin, grandtotal from bookingroom br inner join booking b using (bookingoid) inner join hotel h using (hoteloid) where h.hoteloid = '".$hoteloid."' and b.bookingstatusoid in ('4') and b.pmsstatus = '0' group by b.bookingoid) as t1 where (min_checkin >= '".$date."' and min_checkin <= '".$date."')";
          $stmt = $db->query($sum_production);
          $rslt_production = $stmt->fetch(PDO::FETCH_ASSOC);

          $total_production = floor($rslt_production['total_production']);

          $varname_production = 'production_'.$hoteloid;
          array_push($$varname_production, $total_production);

      }
  }

  array_push($a_hoteloid, 0);
  array_push($a_hotelname, "Other");
  $production_0 = array();

  for($i = 0; $i <= $day; $i++){
      $date = date('Y-m-d', strtotime($startdate. ' +'.$i.' days'));
      array_push($point_date, date('d M', strtotime($date)));

      foreach($a_hoteloid as $key => $hoteloid){
          $sum_production = "select count(bookingoid) as total_production from (select h.hoteloid, h.hotelname, b.bookingoid, min(checkin) as min_checkin, grandtotal from bookingroom br inner join booking b using (bookingoid) inner join hotel h using (hoteloid) where h.hoteloid not in ('".implode("','", $a_hoteloid)."') and b.bookingstatusoid in ('4') and b.pmsstatus = '0' group by b.bookingoid) as t1 where (min_checkin >= '".$date."' and min_checkin <= '".$date."')";
          $stmt = $db->query($sum_production);
          $rslt_production = $stmt->fetch(PDO::FETCH_ASSOC);

          $total_production = floor($rslt_production['total_production']);

          array_push($production_0, $total_production);

      }
  }

  /*--------------------------------------------------------------------------*/

  $query_hotel_chain = "select hoteloid, hotelname, sum(grandtotal) as total_revenue from (select  h.hoteloid, h.hotelname, b.bookingoid, min(checkin) as min_checkin, grandtotal from bookingroom br inner join booking b using (bookingoid) inner join hotel h using (hoteloid) where h.publishedoid not in (3) and h.hotelstatusoid = '1' and bookingstatusoid in ('4') and b.pmsstatus = '0' group by b.bookingoid) as t1 where (min_checkin >='".$startdate."' and min_checkin <= '".$enddate."') group by hoteloid order by total_revenue desc limit 5";
  $stmt = $db->query($query_hotel_chain);
  $hotel_chain = $stmt->fetchAll(PDO::FETCH_ASSOC);
  $b_hoteloid = array();
  $b_hotelcode = array();
  $b_hotelname = array();
  foreach($hotel_chain as $row){
    array_push($b_hoteloid, $row['hoteloid']);
    array_push($b_hotelcode, $row['hotelcode']);
    array_push($b_hotelname, $row['hotelname']);

    $varname_revenue = 'revenue_'.$row['hoteloid'];
    $$varname_revenue = array();
  }

  for($i = 0; $i <= $day; $i++){
    $date = date('Y-m-d', strtotime($startdate. ' +'.$i.' days'));
    foreach($b_hoteloid as $key => $hoteloid){
        $sum_production = "select sum(grandtotal) as total_revenue
        from (select b.bookingoid, grandtotal, min(checkin) as min_checkin from bookingroom br inner join booking b using (bookingoid) inner join hotel h using (hoteloid) where h.hoteloid = '".$hoteloid."' and b.bookingstatusoid in ('4') and b.pmsstatus = '0' group by b.bookingoid) as t1
        where (min_checkin >= '".$date."' and min_checkin <= '".$date."')";
        $stmt = $db->query($sum_production);
      	$rslt_production = $stmt->fetch(PDO::FETCH_ASSOC);

        $total_revenue = floor($rslt_production['total_revenue']);

        $varname_revenue = 'revenue_'.$hoteloid;
        array_push($$varname_revenue, $total_revenue);
    }
  }

  array_push($b_hoteloid, 0);
  array_push($b_hotelname, "Other");
  $revenue_0 = array();

  for($i = 0; $i <= $day; $i++){
    $date = date('Y-m-d', strtotime($startdate. ' +'.$i.' days'));
    foreach($b_hoteloid as $key => $hoteloid){
        $sum_production = "select sum(grandtotal) as total_revenue
        from (select b.bookingoid, grandtotal, min(checkin) as min_checkin from bookingroom br inner join booking b using (bookingoid) inner join hotel h using (hoteloid) where h.hoteloid not in ('".implode("','", $b_hoteloid)."') and b.bookingstatusoid in ('4') and b.pmsstatus = '0' group by b.bookingoid) as t1
        where (min_checkin >= '".$date."' and min_checkin <= '".$date."')";
        $stmt = $db->query($sum_production);
        $rslt_production = $stmt->fetch(PDO::FETCH_ASSOC);

        $total_revenue = floor($rslt_production['total_revenue']);
        array_push($revenue_0, $total_revenue);
    }
  }

  $chart_border_color = array("rgba(255, 188, 0, 1)", "rgba(51, 175, 20, 1)", "rgba(12, 161, 206, 1)", "rgba(163, 60, 216, 1)", "rgba(189, 33, 33, 1)", "rgba(150, 141, 141, 1)");

?>
<div class="row">
  <div class="col-md-6">
    <h2>Distibuted Production All Booking Confirmed</h2>
    <div class="chart"><canvas id="production-chain-chart"></canvas></div>
  </div>
  <div class="col-md-6">
    <h2>Distibuted Revenue Confirmed</h2>
    <div class="chart"><canvas id="revenue-chain-chart"></canvas></div>
  </div>
</div>

<script src="<?php echo $base_url?>/lib/plugins/chartjs/Chart.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.4/Chart.bundle.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(function () {
    'use strict';

    //---------------------------
    // PRODUCTION CHAIN CHART
    //---------------------------

    var production_chart = document.getElementById('production-chain-chart');
    new Chart(production_chart, {
      type: 'line',
      data: {
      labels: [<?php echo '"'.implode('","', $point_date).'"'; ?>],
      datasets: [
        <?php
        foreach($a_hoteloid as $key => $hoteloid){
          $varname_production = 'production_'.$hoteloid;
        ?>
        {
        label: "<?=$a_hotelname[$key]?>",
        data: [<?php echo implode(',', $$varname_production); ?>],
        backgroundColor: "rgba(255, 255, 255, 0)",
        borderColor: "<?=$chart_border_color[$key]?>",
        },
        <?php
        }
        ?>
      ]
      },
      options: {
        responsive: true
      }
    });

    //---------------------------
    // PRODUCTION CHAIN CHART
    //---------------------------

    var revenue_chart = document.getElementById('revenue-chain-chart');
    new Chart(revenue_chart, {
      type: 'line',
      data: {
      labels: [<?php echo '"'.implode('","', $point_date).'"'; ?>],
      datasets: [
        <?php
        foreach($b_hoteloid as $key => $hoteloid){
          $varname_revenue = 'revenue_'.$hoteloid;
        ?>
        {
        label: "<?=$b_hotelname[$key]?>",
        data: [<?php echo implode(',', $$varname_revenue); ?>],
        backgroundColor: "rgba(255, 255, 255, 0)",
        borderColor: "<?=$chart_border_color[$key]?>",
        },
        <?php
        }
        ?>
      ]
      },
      options: {
        responsive: true
      }
    });
});
</script>
