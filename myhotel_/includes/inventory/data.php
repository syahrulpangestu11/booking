<?php
include("../ajax-include-file.php");

$hotelcode = $_POST['hcode'];

$stmt = $db->query("select hoteloid from hotel h where h.hotelcode = '".$hotelcode."'");
$hotel = $stmt->fetch(PDO::FETCH_ASSOC);
$hoteloid = $hotel['hoteloid'];

//-- PatchStaah
include("../patch_staah.php");
$staah_connect = getHotelStaahConnect($hoteloid);
//-- END PatchStaah

$lastdate = $_POST['lastdate'];
$request = $_POST['request'];
$channel = 1;

$xml_tarif =  simplexml_load_file("../../data-xml/".$hotelcode.".xml");
$xml_external = array();

/*$stmt = $db->query("SELECT `gfilename` FROM `log_import` where hoteloid = '".$hoteloid."'");
$r_log = $stmt->fetchAll(PDO::FETCH_ASSOC);
foreach($r_log as $row){
	$gfilename = $row['gfilename'];
	$$gfilename = simplexml_load_file("../../data-xml/external/".$gfilename);
	array_push($xml_external, $gfilename);
}
*/
function showDataXML($hotel, $room, $date, $roomoffer, $channel){
	global $db;
	global $xml_tarif;
	global $xml_external;

	$rate=0; $closetime='';  $breakfast='n'; $cta='n'; $ctd='n'; $existtag = 0;

	$query_tag = '//hotel[@id="'.$hotel.'"]/masterroom[@id="'.$room.'"]/rate[@date="'.$date.'"]/rateplan[@id="'.$roomoffer.'"]/channel[@type="'.$channel.'"]';
	foreach($xml_tarif->xpath($query_tag) as $rowXml){
		$rate = $rowXml->double;
		$closetime = $rowXml->blackout;
		$breakfast = $rowXml->breakfast;
		$cta = $rowXml->cta;
		$ctd = $rowXml->ctd;
		$existtag = 1;
	}

/*	foreach($xml_external as $xmlExt){
		global $$xmlExt;
		foreach($$xmlExt->xpath($query_tag) as $rowXml){
			$rate = $rowXml->double;
			$closetime = $rowXml->blackout;
			$breakfast = $rowXml->breakfast;
			$cta = $rowXml->cta;
			$ctd = $rowXml->ctd;
			$existtag = 1;
		}
	}
*/
	return array($rate, $closetime, $breakfast, $cta, $ctd, $existtag);
}

function getCloseOutRTXML($hotel, $room, $date, $channel){
	global $db;
	global $xml_tarif;
	global $xml_external;

	$closeout = 'n';
	$existtag = 0;

	$query_tag = '//hotel[@id="'.$hotel.'"]/masterroom[@id="'.$room.'"]/rate[@date="'.$date.'"]/closeout';
	foreach($xml_tarif->xpath($query_tag) as $co){
		$closeout = $co;
		$existtag = 1;
	}

	return array($closeout, $existtag);
}


function getAllocationXML($hotel, $room, $date, $channel){
	global $db;
	global $xml_tarif;
	global $xml_external;

	$allocation =0;
	$existtag = 0;

	$query_tag = '//hotel[@id="'.$hotel.'"]/masterroom[@id="'.$room.'"]/rate[@date="'.$date.'"]/allotment/channel[@type="'.$channel.'"]';
	foreach($xml_tarif->xpath($query_tag) as $allotment){
		$allocation = $allotment;
		$existtag = 1;
	}

/*	foreach($xml_external as $xmlExt){
		global $$xmlExt;
		foreach($$xmlExt->xpath($query_tag) as $allotment){
			$allocation = $allotment;
			$existtag = 1;
		}
	}
*/
	return array($allocation, $existtag);
}

switch($request){
	case 'next' : $begin = 1 ; $end = 10; break;
	case 'forward' : $begin = 10 ; $end = 19; break;
	case 'previous' : $begin = -1 ; $end = 8; break;
	case 'backward' : $begin = -10 ; $end = -1; break;
	default :  $begin = 0; $end = 9; break;
}

$showDay = array(); $showDate = array(); $showMonth = array(); $dateArr = array();

for($i = $begin; $i <= $end; $i++){
	$date = date("Y-m-d",strtotime($lastdate." +".$i." day"));
	$dateformated = date("D-d-M",strtotime($lastdate." +".$i." day"));
	$exploDate = explode("-", $dateformated);

	array_push($dateArr, $date);
	array_push($showDay, $exploDate[0]);
	array_push($showDate, $exploDate[1]);
	array_push($showMonth, $exploDate[2]);
}

$roomArr = array();

$stmt = $db->query("select r.roomoid, r.name from room r inner join published p using (publishedoid) inner join hotel h using (hoteloid)  where h.hotelcode = '".$hotelcode."' and p.publishedoid not in (3)");
$dataroom = $stmt->fetchAll(PDO::FETCH_ASSOC);
foreach($dataroom as $roomtype){
	$dataChildRoom = array();
	array_push($dataChildRoom, $roomtype['roomoid'], $roomtype['name']);

	$roomcloseout = array();
	$roomallocation = array();
	$rtparam = array();
	foreach($dateArr as $date){
	    
	    //-- PatchStaah
	    if($staah_connect){
	        $staah_room = getRoomData($hoteloid, $roomtype['roomoid'], $date);
	        $staah_allotment = $staah_room[0];
	        $staah_closeoutroom = $staah_room[1];

			echo '<div style="display:none">'.$date.'_'.$staah_connect.'_'.$staah_allotment.'</div>';
	        
	        if($staah_allotment !== "" and $staah_allotment >= 0){
	            $getAllocation = array($staah_allotment, 1);
	            array_push($roomallocation, $getAllocation);
	        }else{
	            $getAllocation = getAllocationXML($hoteloid, $roomtype['roomoid'], $date, $channel);
    		    array_push($roomallocation, $getAllocation);
	        }
	        
	        if($staah_closeoutroom != ""){
	            $getCloseOutRT = array($staah_closeoutroom, 1);
        		array_push($roomcloseout, $getCloseOutRT);
	        }else{
                $getCloseOutRT = getCloseOutRTXML($hoteloid, $roomtype['roomoid'], $date, $channel);
        		array_push($roomcloseout, $getCloseOutRT);
	        }
	    }else{
            $getCloseOutRT = getCloseOutRTXML($hoteloid, $roomtype['roomoid'], $date, $channel);
    		array_push($roomcloseout, $getCloseOutRT);
    
    		$getAllocation = getAllocationXML($hoteloid, $roomtype['roomoid'], $date, $channel);
    		array_push($roomallocation, $getAllocation);
	    }
	    //-- END PatchStaah

	    /*
		$getCloseOutRT = getCloseOutRTXML($hoteloid, $roomtype['roomoid'], $date, $channel);
		array_push($roomcloseout, $getCloseOutRT);

		$getAllocation = getAllocationXML($hoteloid, $roomtype['roomoid'], $date, $channel);
		array_push($roomallocation, $getAllocation);
        */
		array_push($rtparam, $hoteloid.'_'.$roomtype['roomoid'].'_'.$date.'_'.$channel);
	}

	array_push($dataChildRoom, $roomcloseout, $roomallocation, $rtparam);

	$stmt = $db->query("select ro.roomofferoid, ro.offertypeoid, ro.name from roomoffer ro inner join published p using (publishedoid) inner join room r using (roomoid) inner join offertype using (offertypeoid) where r.roomoid = '".$roomtype['roomoid']."' and p.publishedoid = '1'");
	$dataroomoffer = $stmt->fetchAll(PDO::FETCH_ASSOC);
	$dataChildRoomOffer = array();
	foreach($dataroomoffer as $roomoffer){
		array_push($dataChildRoomOffer, array($roomoffer['roomofferoid'], $roomoffer['name'], $roomoffer['offertypeoid']));

		$inventory[$roomoffer['roomofferoid']] = array();
		$rtoparam[$roomoffer['roomofferoid']] = array();
		foreach($dateArr as $date){
		    
		    //-- PatchStaah
    	    if($staah_connect){
    	        $staah_rates = getRateData($hoteloid, $roomtype['roomoid'], $roomoffer['roomofferoid'], $date);
    	        $staah_rate = $staah_rates[0];
    	        $staah_closeoutrate = $staah_rates[1];
    	        $staah_cta = $staah_rates[2];
    	        $staah_ctd = $staah_rates[3];
    	        
	            $getData = showDataXML($hoteloid, $roomtype['roomoid'], $date, $roomoffer['roomofferoid'], $channel);
	            if($staah_rate !== "" and $staah_rate >= 0){ $getData[0] = $staah_rate; }
	            if($staah_closeoutrate != ""){ $getData[1] = $staah_closeoutrate; }
	            if($staah_cta != ""){ $getData[3] = $staah_cta; }
	            if($staah_ctd != ""){ $getData[4] = $staah_ctd; }
		        array_push($inventory[$roomoffer['roomofferoid']], $getData);
    	    }else{
                $getData = showDataXML($hoteloid, $roomtype['roomoid'], $date, $roomoffer['roomofferoid'], $channel);
			    array_push($inventory[$roomoffer['roomofferoid']], $getData);
    	    }
    	    //-- END PatchStaah

		    /*
			$getData = showDataXML($hoteloid, $roomtype['roomoid'], $date, $roomoffer['roomofferoid'], $channel);
			array_push($inventory[$roomoffer['roomofferoid']], $getData);
			*/
			array_push($rtoparam[$roomoffer['roomofferoid']], $hoteloid.'_'.$roomtype['roomoid'].'_'.$date.'_'.$channel.'_'.$roomoffer['roomofferoid']);
		}

	}
	array_push($dataChildRoom, $dataroomoffer);
	array_push($roomArr, $dataChildRoom);
}
?>

<?php
    if(strtotime($dateArr[0]) >= strtotime(date('Y-m-d 00:00:00'))){
?>

<table id="inventory" cellpadding="0" cellspacing="0">
	<thead>
    	<tr>
    		<th>&nbsp;</th>
            <?php
			foreach($dateArr as $key => $date){
				if($key == 0){
			?>
            <input type="hidden" name="spotdate" value="<?=$date?>" />
            <?php
				}
            ?>
            <th><?php echo $showDay[$key]; ?><br /><?php echo $showDate[$key]; ?><br /><?php echo $showMonth[$key]; ?></th>
            <?php
			}
			?>
        </tr>
    </thead>
    <tbody>
    <?php
	foreach($roomArr as $key => $room){
	?>
        <tr class="roomtype">
            <td><?php echo $room[1]; ?></td>
            <?php
			foreach($room[2] as $key => $closeout){
				if($closeout[0] == "y"){ $class_co = "close"; }else{ $class_co = "open"; }
			?>
            <td data-param="<?=$room[4][$key]?>" data-existed="<?php echo $closeout[1]; ?>"><div node="closeout" class="closeout <?=$class_co?>">&nbsp;</div></td>
            <?php
			}
			?>
        </tr>
        <tr>
            <td>Allocation</td>
            <?php
			foreach($room[3] as $key => $allocation){
			?>
            <td <?php if($allocation[0]==0){echo 'class="bg-s0"';}?> data-param="<?=$room[4][$key]?>" data-existed="<?php echo $allocation[1]; ?>"><span class="clickable"><?php echo $allocation[0]; ?></span><input type="text" name="allotment" value="<?php echo $allocation[0]; ?>" /></td>
            <?php
			}
			?>
		</tr>
    <?php
        foreach($room[5] as $key2 => $roomoffer){
			$rate = array(); $closeout = array(); $breakfast = array(); $cta = array(); $ctd = array(); $existedtag = array();
			foreach($inventory[$roomoffer['roomofferoid']] as $key => $inv){
				array_push($rate, $inv[0]);
				array_push($closeout, $inv[1]);
				array_push($breakfast, $inv[2]);
				array_push($cta, $inv[3]);
				array_push($ctd, $inv[4]);
				array_push($existedtag, $inv[5]);



				try {
					$stmt = $db->query("select h.hotelcode, r.roomoid, ro.minrate from room r inner join hotel h using (hoteloid) inner join roomoffer ro using (roomoid) where ro.roomofferoid = '".$roomoffer['roomofferoid']."'");
					$row_count = $stmt->rowCount();
					if($row_count > 0) {
						$r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
						foreach($r_room as $row){
							$hcode = $row['hotelcode'];
							$roomoid = $row['roomoid'];
							$minrate = $row['minrate'];
						}
					}else{
						echo "No Result";
						die();
					}
				}catch(PDOException $ex) {
					echo "Invalid Query";
					die();
				}



			}
    ?>
		<tr class="roomoffer">
            <td><?php echo $roomoffer['name']; ?></td>
            <?php
			foreach($closeout as $key => $invClose){
				if($invClose == "y"){ $class_co = "close"; }else{ $class_co = "open"; }
			?>
            <td data-param="<?=$rtoparam[$roomoffer['roomofferoid']][$key]?>" data-existed="<?php echo $existedtag[$key]; ?>"><div node="blackout" class="closeout <?=$class_co?>">&nbsp;</div></td>
            <?php
			}
			?>
        </tr>
        <tr>
            <td>rates</td>
            <?php
			foreach($rate as $key => $invRate){
			?>
            <td <?php if($invRate==0){echo 'class="bg-s0"';}?> data-param="<?=$rtoparam[$roomoffer['roomofferoid']][$key]?>" data-existed="<?php echo $existedtag[$key]; ?>"><span class="clickable" x="rate"><?php echo floatval($invRate); ?></span><input type="text" name="double" class="rate" data-minrate="<?=$minrate;?>" value="<?php echo $invRate; ?>" /></td>
            <?php
			}
			?>
        </tr>
        <tr>
            <td>CTA / CTD</td>
            <?php
			foreach($cta as $key => $invCTA){
				if($invCTA == "y"){ $checked_cta = "checked = 'checked'"; }else{ $checked_cta = ""; }
				if($ctd[$key] == "y"){ $checked_ctd = "checked = 'checked'"; }else{ $checked_ctd = ""; }
			?>
            <td data-param="<?=$rtoparam[$roomoffer['roomofferoid']][$key]?>" data-existed="<?php echo $existedtag[$key]; ?>">
            <input type="checkbox" name="cta" value="y" <?=$checked_cta?> /> A &nbsp;<input type="checkbox" name="ctd" value="y" <?=$checked_ctd?> /> D</td>
            <?php
			}
			?>
        </tr>
        <tr>
            <td>Breakfast</td>
            <?php
			foreach($breakfast as $key => $invBreakfast){
				if($invBreakfast == 'y'){ $checked = 'checked="checked"'; }else{ $checked = ''; }
			?>
            <td data-param="<?=$rtoparam[$roomoffer['roomofferoid']][$key]?>" data-existed="<?php echo $existedtag[$key]; ?>"><input type="checkbox" name="breakfast" value="y" <?php echo $checked; ?> /></td>
            <?php
			}
			?>
        </tr>
    <?php
		}
	}
	?>
    </tbody>
</table>

<?php
    }
?>
