<?php
session_start();
error_reporting(E_ALL ^ E_NOTICE);
include("../../conf/connection.php");

$hcode = $_POST['hcode'];
$request = $_POST['request'];

$parameter = explode('_', $_POST['parameter']);
$hoteloid = $parameter[0]; $roomoid = $parameter[1]; $date = $parameter[2]; $channel = $parameter[3];
if(count($parameter) > 4){
    $roomofferoid = $parameter[4];
}else{
	$roomofferoid = 0;
}
$day = date("D Y-m-d",strtotime($date));

$nodeValue = $_POST['value'];
$existed = $_POST['existed'];

$path_xml_hotel = '../../data-xml/'.$hcode.'.xml';

if(!file_exists($path_xml_hotel)){ createXML($path_xml_hotel); }

$xml = new DomDocument();
    $xml->preserveWhitespace = false;
    $xml->load($path_xml_hotel);
$xpath = new DomXpath($xml);

$root = $xml->documentElement;
/*------------------------------------------------------------------------------------*/
$query_tag_hotel = '//hotel[@id="'.$hoteloid.'"]';
$query_tag_room = $query_tag_hotel.'//masterroom[@id="'.$roomoid.'"]';
$query_tag_rate = $query_tag_room.'/rate[@date="'.$date.'"]';
$query_tag_closeout = $query_tag_rate.'/closeout';
$query_tag_allotment = $query_tag_rate.'/allotment';
$query_tag_allotment_channel = $query_tag_allotment.'/channel[@type="'.$channel.'"]';
$query_tag_rateplan = $query_tag_rate.'/rateplan[@id="'.$roomofferoid.'"]';
$query_tag_rateplan_channel = $query_tag_rateplan.'/channel[@type="'.$channel.'"]';
$query_tag_rateplan_channel_element = $query_tag_rateplan.'/channel[@type="'.$channel.'"]/'.$request;
/*------------------------------------------------------------------------------------*/

if($existed == 1){
	switch($request){
		case 'closeout' :
			$query_parent = $query_tag_rate;
			$query_target = $query_tag_closeout;
			$listElement = array('closeout');
		break;
		case 'allotment' :
			$query_parent = $query_tag_allotment;
			$query_target = $query_tag_allotment_channel;
			$listElement = array('channel');
			$nodeAttr = array(array('type', $channel));
		break;
		case 'double' :
			$query_parent = $query_tag_rateplan_channel;
			$query_target = $query_tag_rateplan_channel;
			$listElement = array('netsingle', 'netdouble', 'single', 'double');
		break;
		default :
			$query_parent = $query_tag_rateplan_channel;
			$query_target = $query_tag_rateplan_channel.'/'.$request;
			$listElement = array($request);
		break;
	}

	$check_tag_parent = $xpath->query($query_parent);
	$tag_parent = $check_tag_parent->item(0);

	foreach($listElement as $nodeElement){
		if(count($listElement) > 1){
			$query_new_target = $query_target.'/'.$nodeElement;
			$check_tag_target = $xpath->query($query_new_target);
		}else{
			$check_tag_target = $xpath->query($query_target);
		}

    $oldValue = $check_tag_target->item(0)->nodeValue;

		$createElement = $xml->createElement($nodeElement);
		if(isset($nodeAttr) and count($nodeAttr) > 0){
			foreach($nodeAttr as $attribute){
				$createElement_Attr = $xml->createAttribute($attribute[0]);
				$createElement_Attr->value = $attribute[1];
				$createElement->appendChild($createElement_Attr);
			}
		}
		$createElement_value = $xml->createTextNode($nodeValue);
		$createElement->appendChild($createElement_value);

		if($check_tag_target->length == 0){
			$tag_parent->appendChild($createElement);
		}else{
			$tag_target = $check_tag_target->item(0);
			$tag_parent->replaceChild($createElement, $tag_target);
		}
	}
}else{
  $oldValue = "";

	switch($request){
		case 'closeout' :
			$targetLine = array('closeout');
		break;
		case 'allotment' :
			$targetLine = array('allotment');
		break;
		default :
			$targetLine = array('rateplan');
		break;
	}

	/* create <hotel id=@>---------------------------------------------------------*/
	$check_tag_hotel = $xpath->query($query_tag_hotel);
    if($check_tag_hotel->length == 0){
	    $hotel = $xml->createElement("hotel");
	        $hotel_id = $xml->createAttribute("id");
	        $hotel_id->value = $hoteloid;
	    $hotel->appendChild($hotel_id);
	    $root->appendChild($hotel);
		$check_tag_hotel = $xpath->query($query_tag_hotel);
    }

	/* create <hotel id=@><masterroom id=@>-----------------------------------------*/
    $check_tag_room = $xpath->query($query_tag_room);
    if($check_tag_room->length == 0){
	    $room = $xml->createElement("masterroom");
	        $room_id = $xml->createAttribute("id");
	        $room_id->value = $roomoid;
	    $room->appendChild($room_id);

	    $tag_hotel = $check_tag_hotel->item(0);
	    $tag_hotel->appendChild($room);
		$check_tag_room = $xpath->query($query_tag_room);
    }

	/* create <hotel id=@><masterroom id=@><rate date=@ day=@>----------------------*/
	$check_tag_rate = $xpath->query($query_tag_rate);
    if($check_tag_rate->length == 0){
		$rate = $xml->createElement("rate");
	        $rate_date = $xml->createAttribute("date");
	        $rate_date->value = $date;
		$rate->appendChild($rate_date);
	        $rate_day = $xml->createAttribute("day");
	        $rate_day->value = $day;
		$rate->appendChild($rate_day);

		$tag_room = $check_tag_room->item(0);
		$tag_room->appendChild($rate);
		$check_tag_rate = $xpath->query($query_tag_rate);
    }

	/* create <hotel id=@><masterroom id=@><rate date=@ day=@>----------------------*/
	$check_tag_rate = $xpath->query($query_tag_rate);
    if($check_tag_rate->length == 0){
		$rate = $xml->createElement("rate");
	        $rate_date = $xml->createAttribute("date");
	        $rate_date->value = $date;
		$rate->appendChild($rate_date);
	        $rate_day = $xml->createAttribute("day");
	        $rate_day->value = $day;
		$rate->appendChild($rate_day);

		$tag_room = $check_tag_room->item(0);
		$tag_room->appendChild($rate);
		$check_tag_rate = $xpath->query($query_tag_rate);
    }

	$tag_rate = $check_tag_rate->item(0);

	/* create <hotel id=@><masterroom id=@><rate date=@ day=@><closeout>----------------------*/
	if(in_array('closeout', $targetLine)){
		$check_tag_closeout = $xpath->query($query_tag_closeout);

		$closeout = $xml->createElement("closeout");
			$closeout_value = $xml->createTextNode($nodeValue);
		$closeout->appendChild($closeout_value);

		if($check_tag_closeout->length == 0){
			$tag_rate->appendChild($closeout);
		}else{
			$tag_closeout = $check_tag_closeout->item(0);
			$tag_rate->replaceChild($closeout, $tag_closeout);
		}
	}

	/* create <hotel id=@><masterroom id=@><rate date=@ day=@><allotment> ----------------------*/
	if(in_array('allotment', $targetLine)){
		/* create <allotment> ------------------------------*/
		$check_tag_allotment = $xpath->query($query_tag_allotment);

		$allotment = $xml->createElement("allotment");

		if($check_tag_allotment->length == 0){
			$tag_rate->appendChild($allotment);
			$check_tag_allotment = $xpath->query($query_tag_allotment);
		}

		$tag_allotment = $check_tag_allotment->item(0);

		/* create <allotment><channel type=@> ------------------------------*/
		$check_tag_allotment_channel = $xpath->query($query_tag_allotment_channel);

		$allotment_channel = $xml->createElement("channel");
	        $allotment_channel_type = $xml->createAttribute("type");
	        $allotment_channel_type->value = $channel;
		$allotment_channel->appendChild($allotment_channel_type);
	        $allotment_channel_value = $xml->createTextNode($nodeValue);
		$allotment_channel->appendChild($allotment_channel_value);

		if($check_tag_allotment_channel->length == 0){
			$tag_allotment->appendChild($allotment_channel);
		}else{
			$tag_allotment_channel = $check_tag_allotment_channel->item(0);
			$tag_allotment->replaceChild($allotment_channel, $tag_allotment_channel);
		}
	}

	/* create <hotel id=@><masterroom id=@><rate date=@ day=@><rateplan id=@> ----------------------*/
	if(in_array('rateplan', $targetLine)){
		/* create <rateplan id=@> ------------------------------*/
		$check_tag_rateplan = $xpath->query($query_tag_rateplan);

		$rateplan = $xml->createElement("rateplan");
	        $rateplan_id = $xml->createAttribute("id");
	        $rateplan_id->value = $roomofferoid;
		$rateplan->appendChild($rateplan_id);

		if($check_tag_rateplan->length == 0){
			$tag_rate->appendChild($rateplan);
			$check_tag_rateplan = $xpath->query($query_tag_rateplan);
		}

		$tag_rateplan = $check_tag_rateplan->item(0);

		/* create <rateplan><channel type=@> ------------------------------*/
		$check_tag_rateplan_channel = $xpath->query($query_tag_rateplan_channel);

		$rateplan_channel = $xml->createElement("channel");
	        $rateplan_channel_type = $xml->createAttribute("type");
	        $rateplan_channel_type->value = $channel;
		$rateplan_channel->appendChild($rateplan_channel_type);

		if($check_tag_rateplan_channel->length == 0){
			$tag_rateplan->appendChild($rateplan_channel);
			$check_tag_rateplan_channel = $xpath->query($query_tag_rateplan_channel);
		}

		$tag_rateplan_channel = $check_tag_rateplan_channel->item(0);

		$check_tag_rateplan_channel_element = $xpath->query($query_tag_rateplan_channel_element);

		$node_element = $xml->createElement($request);
			$node_element_value = $xml->createTextNode($nodeValue);
		$node_element->appendChild($node_element_value);

		if($check_tag_rateplan_channel_element->length == 0){
			$tag_rateplan_channel->appendChild($node_element);
		}else{
			$tag_rateplan_channel_element = $check_tag_rateplan_channel_element->item(0);
			$tag_rateplan_channel->replaceChild($node_element, $tag_rateplan_channel_element);
		}

	}
}

/*----------------------------------------------------------------------*/
if(in_array($request, array("allotment", "closeout"))){
  $stmt = $db->query("select r.name from room r where r.roomoid = '".$roomoid."'");
}else{
  $stmt = $db->query("select ro.name from roomoffer ro where ro.roomofferoid = '".$roomofferoid."'");
}
$r_room = $stmt->fetch(PDO::FETCH_ASSOC);

if(in_array($request, array("allotment", "double"))){
  if(empty($oldValue)){ $oldValue = "0"; }
}else{
  if(empty($oldValue)){ $oldValue = "null"; }
}

/* LOG ---------------------------------------------------*/
  include('../log/class-logging.php');
  $log = new Logging($hcode);
  $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

  if($request == "double"){ $request = "rate"; }
  $log_value = array(
    "action" => "Availability",
    "page" => $actual_link,
    "room" => $r_room['name'],
    "old-value" => "date : ".$date." | ".$request." : ".$oldValue,
    "new-value" => "date : ".$date." | ".$request." : ".$nodeValue
  );
  $log->createLog($log_value);
/* ---------------------------------------------------*/

$dynamicrate = false;
/* DR Change Availabilty ================================================== */
if($request == "allotment" and $_POST['activedr'] == "1"){// and $hoteloid == "1"
  $newallotment = $nodeValue;
  $stmt = $db->query("select ro.* from roomoffer ro inner join published p using (publishedoid) inner join room r using (roomoid) inner join hotel h using (hoteloid) inner join offertype using (offertypeoid) where r.roomoid = '".$roomoid."' and p.publishedoid = '1' and h.dynamicrate = '1'");
  $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
  foreach($r_room as $row){
    $stmt = $db->prepare("select mr.rate from masterrate mr inner join dynamicraterule dr using (masterrateoid) inner join dynamicrate d using (dynamicrateoid) where d.hoteloid = :a and dr.roomofferoid = :b and (:c <= allocation) and d.publishedoid = '1' order by allocation asc limit 1");
    $stmt->execute(array(':a' => $hoteloid, ':b' => $row['roomofferoid'], ':c' => $newallotment));
    $foundrule = $stmt->rowCount();
    if($foundrule > 0){
      $newrate = $stmt->fetch(PDO::FETCH_ASSOC);
      $check_tag_rate = $xpath->query($query_tag_rate.'/rateplan[@id="'.$row['roomofferoid'].'"]/channel[@type="'.$channel.'"]/double');
      if($check_tag_rate->length > 0){
        $oldrate = $check_tag_rate->item(0)->nodeValue;
        $check_tag_rate->item(0)->nodeValue = round($newrate['rate']);
        $dynamicrate = true;
        $log_value = array(
          "action" => "Availability",
          "page" => $actual_link,
          "room" => $row['name'],
          "old-value" => "date : ".$date." | "."rate : ".$oldrate,
          "new-value" => "date : ".$date." | "."rate : ".round($newrate['rate'])." | dynamicrate : on"
        );
        $log->createLog($log_value);
      }
    }
  }
}

$log->closeLog($log_value);

$xml->formatOutput = true;
$xml->save($path_xml_hotel) or die("Error");



/*----------------------------------------------------------------------*/

if($dynamicrate == true){
  echo "success-dynamic";
}else{
  echo "success";
}
?>
