<?php
    // include('includes/bootstrap.php');
    include('includes/promotion/geo-location/geo-location.php');

    $daylist = array("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat");
    $stmt = $db->query("select max(priority)+1 as maxpriority from promotion where hoteloid = '".$hoteloid."' and publishedoid = '1'");
    $r_max = $stmt->fetchAll(PDO::FETCH_ASSOC);
    foreach($r_max as $row){
        $maxpriority = $row['maxpriority'];
    }
    
    $minstay = 0;
    $min_ci = 0;
    $max_ci = 0;
    $maxstay = 0;
    $discounttype = '1';
    $discountapply = '1';
    $minroom = '1';
    $deposit = '1';
    
    if(isset($_GET['tid'])){
        $promooid = $_GET['tid'];
        try {
            $stmt = $db->query("select * from promotion_template where templateoid = '".$promooid."'");
            $row_count = $stmt->rowCount();
            if($row_count > 0) {
                $r_promo = $stmt->fetchAll(PDO::FETCH_ASSOC);
                foreach($r_promo as $row){
                    $name = $row['name'];
                    $promoimage = $row['promoimage'];
                    $minstay = $row['minstay'];
                    $maxstay = $row['maxstay'];
                    $discounttype = $row['discounttypeoid'];
                    $discountapply = $row['discountapplyoid'];
                    $applyvalue = $row['applyvalue'];
                    $discountvalue = floor($row['discountvalue']);
                    $minroom = $row['minroom'];
                    $published = $row['publishedoid'];
                    $deposit = $row['depositoid'];
                    $depositvalue = $row['depositvalue'];
    
                    $min_ci = $row['min_bef_ci'];
                    $max_ci = $row['max_bef_ci'];
    
                    $headline = $row['headline'];
                    $description = $row['description'];
                    $servicefacilities = $row['servicefacilities'];
                    $termcondition = $row['termcondition'];
    
                }
            }else{
                echo "No Result";
                die();
            }
        }catch(PDOException $ex) {
            echo "Invalid Query";
            die();
        }
    }
?>
<script type="text/javascript">
$(function() {
    $('textarea#html-box').trumbowyg({
        btns: ['viewHTML',
          '|', 'btnGrp-design',
          '|', 'link',
          '|', 'btnGrp-justify',
          '|', 'btnGrp-lists'],
        fullscreenable: false
    });
});
</script>
<style type="text/css">
            h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
                font-family: inherit;
                font-weight: 600;
                line-height: inherit;
                color: inherit;
                margin-bottom:10px!important;
            }
            .wrapper {
                position: inherit;
                overflow: hidden!important;
            }
            .left-side {
                padding-top: inherit;
            }
            .sidebar > .sidebar-menu li > a:hover {
                background-color: rgba(72, 115, 175, 0.26);
            }
            .sidebar .sidebar-menu .treeview-menu {
                background-color: rgb(14, 26, 43);
            }
            .sidebar .sidebar-menu .treeview-menu .treeview-menu-child {
                background-color: rgba(0, 0, 0, 0.5);
            }
            .sidebar > .sidebar-menu li.active > a {
                background-color: rgba(197, 45, 47, 0.55);
            }
            .sidebar > .sidebar-menu > li.treeview.active > a {
                background-color: inherit;
            }
            .sidebar > .sidebar-menu > li.treeview.active li.treeview-child.active > a {
                background-color: inherit;
            }
            .sidebar .sidebar-menu > li > a > .fa {
                width: 28px;
                font-size: 16px;
            }
            .sidebar-menu, .main-sidebar .user-panel, .sidebar-menu>li.header{
                white-space:normal!important;
            }
            .form-group input[type=text]{
                width:100%!important;
            }

  #accordion .header {background: #f4f8fb !important; border: 1px solid #bbb; color: #222; margin-top: 10px; font-weight: normal;}
  #accordion .header.ui-accordion-header-active,
  #accordion .header.ui-state-hover {background-color: #5d9cec !important; border-color: transparent; color: #fff;}
  #accordion .header .table-cell {display: table-cell; vertical-align: middle; padding: 0 10px;}
  #accordion .header .table-cell:nth-of-type(1) {width: 10%;}
  #accordion .header#accordion .header .table-cell:nth-of-type(2) {width: 30%;}
  #accordion .content{ min-height:0!important; }
  #accordion #assigned-promotion > div{ margin-bottom:5px; }

  #accordion .header .image .thumbnail {width: 100%;}

  #accordion .content {
    background: #F4F8FB !important; border: 1px solid #bbb !important;
    border-top: none;
  }

  .btn-xs{ padding : 1px 5px!important; }
        </style>
<section class="content-header">
    <h1>
        Create Promotions
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i>  Allotments &amp; Rates </a></li>
        <li class="active">Promotions</li>
    </ol>
</section>
<section class="content" id="promotion">
    <?php
    if($_POST['hoteloid'] > 0){
    foreach($_POST['hoteloid'] as $key => $hoteloid){
        echo $hoteloid;
        }
    }
    if($_POST['roomofferoid'] > 0){
    foreach($_POST['roomofferoid'] as $key => $roomofferoid){
        echo $roomofferoid;
        }
    }    
    ?>
    <div class="row">
        <form class="form-box form-horizontal form-box" method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/promotion-all/add-process">
        <div class="box box-form">
            <div class="form-group"><div class="col-md-12"><h1>PROMOTION</h1></div></div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-md-12">Promotion Name</label>
                    <div class="col-md-12"><input type="text" class="form-control" name="name" required="required" value="<?=$name?>" style="width:100%"></div>
                </div>
                <div class="form-group" style="display:none">
                    <label class="col-md-12">Priority Order</label>
                    <div class="col-md-12"><input type="number" class="form-control" min="1" max="<?php echo $maxpriority; ?>"  name="priority" value="<?php echo $priority; ?>"><input type="hidden" name="oldpriority" value="<?php echo $priority; ?>"></div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">Direct Booking Promotion Link</label>
                    <div class="col-md-12">
                        <div class="radio" style="display:inline-block;padding-right:10px;">
                            <label><input type="radio" name="link-style" value="primary" checked="checked"> show promotion only</label>
                        </div>
                        <div class="radio" style="display:inline-block;padding-right:10px;">
                            <label><input type="radio" name="link-style" value="all"> show promotion on top</label>
                        </div>
                    </div>
                    <div class="col-md-12"><textarea name="direct-link" class="form-control"></textarea></div>
                </div>
                <div class="form-group">
                    <div class="col-md-12"><button type="button" class="btn btn-warning btn-sm copy-link" name="<?=htmlspecialchars($name)?>" night="<?php if($minstay < 2){ echo 2; }else{ echo $minstay; } ?>" min-bef-ci="<?php echo $min_ci; ?>"><i class="fa fa-link"></i> Copy Link to Clipboard</button></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-md-12">Promotion Image</label>
                    <div class="col-md-12 preview-image">
                    <?php if(!empty($promoimage)){ ?>
                        <img src="<?=$promoimage?>" id="preview-image" class="img-responsive" alt="Responsive image" style="width:200px;">  
                    <?php }else{ echo "N/A Image for Promotion"; } ?>
                    </div>
                    <div class="col-md-12">
                        <div class="checkbox">
                            <?php if($popupbanner==1){ $checked = "checked"; }else{ $checked = ""; } ?>
                            <label><input type="checkbox" name="popupbanner" value="1" <?=$checked?>> Show image as <b>Popup Banner</b></label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">Upload New Promotion Image</label>
                    <div class="col-md-12"><input type="file" class="form-control" name="image"><i>recommended size : 550px x 310px</i></div>
                </div>
            </div>
            <div class="clear"></div>
            <div style="text-align:right"><button class="default-button" type="submit">Save Promotion</button></div>
            <ul class="inline-half coloreds">
                <li>
                    <h2>CONDITION</h2>
                    <ul class="block">
                        <li>
                            <span>Minimum Stay:</span>
                            <input type="text" class="small" name="minstay" value="<?=$minstay?>" min="1">
                        </li>
                        <li>
                            <span>Guest Booking Within:</span><br />
                            <span style="vertical-align:bottom;"><input type="number" class="small" name="min_ci" value="<?=$min_ci?>" min="0" /> to <input type="number"  class="small" name="max_ci" value="<?=$max_ci?>" min="0" /> days (before check in)
                            <br>
                            <span><input type="checkbox" id="max_bef_ci_today" name="max_bef_ci_today" value="1" style="margin-left:20px;"> current day only</span></span>
                        </li>
                        <li><span>Sale Date</span></li>
                        <li>
                            <span>Sale Date From:</span>
                            <input type="text"class="medium" id="startdate" name="salefrom">
                        </li>
                        <li>
                            <span>Sale Date To:</span>
                            <input type="text"class="medium" id="enddate"; name="saleto">
                        </li>
                        <li><span>Stay Date</span></li>
                        <li>
                            <span>Stay Date From:</span>
                            <input type="text"class="medium" id="startcalendar" name="stayfrom">
                        </li>
                        <li>
                            <span>Stay Date To:</span>
                            <input type="text"class="medium" id="endcalendar" name="stayto">
                        </li>
                        <li>
                            <span>Displayed on:</span><br>
                            <?php
                                foreach($daylist as $day){
                                    echo"<input type='checkbox' name='display[]' value='".$day."' checked>".$day."&nbsp;&nbsp;";
                                }
                            ?>
                        </li>
                        <li>
                            <span>Check-in on:</span><br>
                            <?php
                                foreach($daylist as $day){
                                    echo"<input type='checkbox' name='checkin[]' value='".$day."' checked>".$day."&nbsp;&nbsp;";
                                }
                            ?>
                        </li>
                        <li>
                            <span>Maximum Stay:</span>
                            <input type="text"class="small"  name="maxstay" value="<?=$maxstay?>"> [0 = N/A]
                        </li>
                        <li>
                            <span>Book Time From:</span>
                            <input type="time" class="small"  name="timefrom" value="00:00">
                        </li>
                        <li>
                            <span>Book Time To:</span>
                            <input type="time" class="small"  name="timeto" value="23:59">
                        </li>
                    </ul>
                </li>
                <li>
                    <h2>BENEFIT</h2>
                    <ul class="block">
                        <li>
                            <span>Discount Type:</span>
                            <?php
                                try {
                                    $stmt = $db->query("select dt.* from discounttype dt inner join published p using (publishedoid) where p.publishedoid = '1'");
                                    $r_dt = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                    foreach($r_dt as $row){
                                        if($row['discounttypeoid'] == $discounttype){ $selected = "checked"; }else{ $selected=""; }
                                        echo"<br><input type='radio' name='discounttype' value='".$row['discounttypeoid']."' label='".$row['labelquestion']."' ".$selected.">  ".$row['name'];
                                    }
                                }catch(PDOException $ex) {
                                    echo "Invalid Query";
                                    die();
                                }
                            ?>
                        </li>
                        <li>
                            <span><b>Discount Value:</b></span><br>
                            <input type="text" class="small"  name="discountvalue" value="<?=$discountvalue?>">
                            <span></span>
                        </li>
                        <li class="applybox">
                            <span>Apply On:</span>
                            <?php
                                try {
                                    $stmt = $db->query("select da.* from discountapply da inner join published p using (publishedoid) where p.publishedoid = '1'");
                                    $r_dt = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                    foreach($r_dt as $row){
                                        if($row['discountapplyoid'] == $discountapply){ $selected = "checked"; }else{ $selected=""; }
                                        echo"<br><input type='radio' name='discountapply' value='".$row['discountapplyoid']."' ".$selected.">  ".$row['name'];
                                        if($row['discountapplyoid'] == '2'){
                                            echo"<div class='applyval'><span class='label'>Apply on Night Number :<br></span><input type='text' class='medium'  name='applyvalue' value='".$applyvalue."'><br>
                                            ex: 2,3,4 for night number 2, 3 and 4 only</div>";
                                        }else if($row['discountapplyoid'] == '3'){
                                            $applyvalue = explode(',',$applyvalue);
                                            echo"<div class='applyval'><span class='label'>Apply on Day:</span><br>";
                                            foreach($daylist as $day){
                                                if(!empty($applyvalue)){
                                                    if(in_array($day, $applyvalue)){ $checked = "checked"; }else{ $checked = ""; }
                                                }else{
                                                    $checked = "checked";
                                                }
                                                echo"<input type='checkbox' name='applyvalue[]' value='".$day."' ".$checked.">".$day."&nbsp;&nbsp;";
                                            }
                                            echo"</div>";
                                        }
                                    }
                                }catch(PDOException $ex) {
                                    echo "Invalid Query";
                                    die();
                                }
                            ?>
                        </li>
                        <li>
                            <!-- <span>Minimum Room:</span> -->
                            <input type="hidden" class="small"  name="minroom" value="0<?php //echo $minroom;?>">
                        </li>
                    </ul>
                </li>
                
            </ul>
            <div style="text-align:right"><button class="default-button" type="submit">Save Promotion</button></div>
        </div>

        <div class="box box-form" id="headline-detail">
            <div class="row">
                <div class="col-md-6">
                    <h2>SHOW PROMOTION WITH GEO LOCATION</h2>
                    <div class="panel panel-info">
                        <div class="panel-heading text-center">
                            <div class="form-group">
                                <label class="col-md-12 text-left"><i class="fa fa-map-marker"></i> Type Country :</label>
                                <div class="col-md-12"><input type="text" name="show-geo-country" class="form-control" /></div>
                            </div>
                            <div class="list-country"><ul class="inline-block"></ul></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <h2>NOT SHOW PROMOTION WITH GEO LOCATION</h2>
                    <div class="panel panel-danger">
                        <div class="panel-heading text-center">
                            <div class="form-group">
                                <label class="col-md-12 text-left"><i class="fa fa-map-marker"></i> Type Country :</label>
                                <div class="col-md-12"><input type="text" name="noshow-geo-country" class="form-control" /></div>
                            </div>
                            <div class="list-country-noshow"><ul class="inline-block"></ul></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="box box-form">
            <div class="row">
                <h2>APPLY PROMOTION TO PROMO CODE (PC)</h2>
                <table class="table table-bordered table-condensed table-striped">
                    <tr class="text-center">
                        <th>Apply</th>
                        <th>Promo Code</th>
                        <th>Periode</th>
                        <th>Apply PC Discount</th>
                        <th>Apply PC Commission</th>
                    </tr>
                    <tr>
                        <td><input type="checkbox" name="shown_no_pc" value="y" checked="checked"/></td>
                        <td>IBE<br />
                        <span style="font-size:0.9em; color:#de0000"><i class="fa fa-info-circle"></i> If you unselect this IBE option, it means you agreed that this promotion will not appear in IBE unless you have provided other promo code to be apply to this promotion.</span>
                        </td>
                        <td>N/A</td>
                        <td>0%</td>
                        <td>0%</td>
                    </tr>
                    <?php
                        $s_promocode = "select pc.* from promocode pc inner join hotel h using (hoteloid) where h.hoteloid = '".$hoteloid."' order by pc.promocodeoid";
                        $stmt = $db->query($s_promocode);
                        $r_promocode = $stmt->fetchAll(PDO::FETCH_ASSOC);
                        foreach($r_promocode as $pc){
                            if($pc['discounttype'] == "discount percentage"){
                                $pc_discount = $pc['discount']."%";
                            }else{
                                $pc_discount = number_format($pc['discount']);
                            }
                            if($pc['commissiontype'] == "commission percentage"){
                                $pc_commission = $pc['commission']."%";
                            }else{
                                $pc_commission = number_format($pc['commission']);
                            }
                            
                            $pc_apply = ''; $pc_applydiscount = ''; $pc_applycommission = '';
                    ?>
                    <tr>
                        <td><input type="checkbox" name="pc[]" value="<?=$pc['promocodeoid']?>" <?=$pc_apply?>/></td>
                        <td><?php echo $pc['promocode']; ?></td>
                        <td><?php echo date("d/M/Y", strtotime($pc['startdate']))." - ".date("d/M/Y", strtotime($pc['startdate'])); ?></td>
                        <td><input type="checkbox" name="pcdiscount-<?=$pc['promocodeoid']?>" value="y" <?=$pc_applydiscount?>/> <?=$pc_discount?></td>
                        <td><input type="checkbox" name="pccommission-<?=$pc['promocodeoid']?>" value="y" <?=$pc_applycommission?>/><?=$pc_commission?></td>
                    </tr>
                    <?php
                        }
                    ?>
                </table>
            </div>
        </div>

<div class="box box-form" id="headline-detail">
            <div class="row">
                <h2>HEADLINE &amp; DETAIL</h2>
                <ul class="inline-triple">
                <?php
                    try {
                        if(isset($_GET['tid'])){
                        $stmt = $db->query("SELECT th.*, i.*, x.termheadlinepromooid from termheadline th left join icon i using (iconoid) left join (select termheadlinepromooid, termheadlineoid from termheadlinepromo_template thp where thp.id='".$promooid."' and thp.type='promotion') x on x.termheadlineoid = th.termheadlineoid 
                        where th.termheadlineoid not in (select termheadlineoid from termheadlinehotelremove where hoteloid = '".$hoteloid."') and th.hoteloid in ('".$hoteloid."', '0') 
                        order by icon_title");
                        }else{
                        $stmt = $db->query("SELECT th.*, i.*, x.termheadlinepromooid from termheadline th left join icon i using (iconoid) left join (select termheadlinepromooid, termheadlineoid from termheadlinepromo thp where thp.id='".$promooid."' and thp.type='promotion') x on x.termheadlineoid = th.termheadlineoid 
                        where th.termheadlineoid not in (select termheadlineoid from termheadlinehotelremove where hoteloid = '".$hoteloid."') and th.hoteloid in ('".$hoteloid."', '0') 
                        order by icon_title");
                        }
                        // where th.hoteloid='".$hoteloid."' 
                        $r_headline = $stmt->fetchAll(PDO::FETCH_ASSOC);
                        foreach($r_headline as $termheadline){
                            if($termheadline['iconoid'] == 0){
                                $icon_src = $termheadline['term_icon_src'];
                            }else{
                                $icon_src = $termheadline['icon_src'];
                            }
                            if(!empty($termheadline['termheadlinepromooid'])){ $checkapply = "checked";  }else{ $checkapply = ""; }
                            ?>
                            <li>
                            <input type="checkbox" name="icon[]" value="<?=$termheadline['termheadlineoid']?>" <?=$checkapply?>/>&nbsp;&nbsp;<img src="<?=$icon_src?>" class="icon" />&nbsp;&nbsp;<?=$termheadline['icon_title']?>
                            </li>
                            <?php
                        }
                    }catch(PDOException $ex) {
                        echo "Invalid Query";
                        die();
                    }
                ?>
                </ul>
                <div class="clear"></div>
                <div class="form-group">
                    <div class="col-md-6">
                        <label>Headline</label>
                        <textarea id="html-box" name="headline"><?=$headline?></textarea>
                    </div>
                    <div class="col-md-6">
                        <label>Promotion Description</label>
                        <textarea id="html-box" name="description"><?=$description?></textarea>
                    </div>
                    <div class="col-md-6">
                        <label>Promotion Inclusive</label>
                        <textarea id="html-box" name="servicefacilities"><?=$servicefacilities?></textarea>
                    </div>
                    <div class="col-md-6">
                        <label>Promotion Terms &amp; Condition</label>
                        <textarea id="html-box" name="termcondition"><?=$termcondition?></textarea>
                    </div>
                </div>
                <div class="form-group">
                     <div class="col-md-6">
                        <label>Cancellation Policy</label>
                        <select name="cancellation" class="form-control" required>
                            <option value=''>Choose Cancellation</option>
                        <?php
                            try {
                                $stmt = $db->query("select cp.* from cancellationpolicy cp inner join published p using (publishedoid) where p.publishedoid = '1'");
                                $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                foreach($r_room as $row){
                                    echo"<option value='".$row['cancellationpolicyoid']."'>".$row['name']."</option>";
                                }
                            }catch(PDOException $ex) {
                                echo "Invalid Query";
                                die();
                            }
                        ?>
                        </select>
                    </div> 
                    <div class="col-md-6">
                        <label>Publish Promotion</label>
                        <select name="published" class="form-control">
                        <?php
                            try {
                                $stmt = $db->query("select * from published where showthis = 'y'");
                                $r_room = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                foreach($r_room as $row){
                                    echo"<option value='".$row['publishedoid']."'>".$row['note']."</option>";
                                }
                            }catch(PDOException $ex) {
                                echo "Invalid Query";
                                die();
                            }
                        ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="panel panel-danger">
                        <div class="panel-heading" style="color:#000">
                        <label>Deposit Type</label>
                        &nbsp; <span style="font-size:0.9em; color:#de0000"><i class="fa fa-info-circle"></i> This amount will be declared as booking guarantee that charged by hotel upon booking received.</span>
                        <div>
                        <?php
                            try {
                                $stmt = $db->query("select d.* from deposit d inner join published p using (publishedoid) where p.publishedoid = '1'");
                                $r_dt = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                foreach($r_dt as $row){
                                    if($row['depositoid'] == $deposit){ $selected = "checked"; $depositval = $depositvalue; }else{ $selected=""; $depositval = ""; }
                                    echo"<input type='radio' name='deposit' value='".$row['depositoid']."' ".$selected.">  ".$row['name'];
                                    if($row['depositoid'] == '2'){
                                        echo"<div class='depositval' style='display:none'><input type='text' class='medium'  name='depositvalue' value='".$depositval."'> Deposit for IDR Amount</div>";
                                    }else if($row['depositoid'] == '3'){
                                        echo"<div class='depositval' style='display:none'><input type='text' class='medium'  name='depositvalue' value='".$depositval."'> Deposit for %(percentage)from total</div>";
                                    }else if($row['depositoid'] == '4'){
                                        echo"<div class='depositval' style='display:none'><input type='text' class='medium'  name='depositvalue' value='".$depositval."'> night(s) amount</div>";
                                    }
                                    echo "<br>";
                                }
                            }catch(PDOException $ex) {
                                echo "Invalid Query";
                                die();
                            }
                        ?>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="clear"></div>
                <div class="row">
                        
                <div class="form-group">
                    <div class="col-md-12 text-right">
                        <button class="btn btn-danger cancel" type="button">Cancel</button>
                        <button class="btn btn-primary" type="submit">Save Promotion</button>
                    </div>
                </div>
                </div>
            </div>
        </div>
       
    </div>    
        </form>
    </div>
</section>

