<div id="paging" class="text-center">
<div class="btn-group">
<?php
	$stmt = $db->query($syntax_list);
	$jmldata = $stmt->rowCount();
	
	$jmlhalaman=ceil($jmldata/$batas);
	//link ke halaman sebelumnya (previous)
	if($halaman > 1){
		$previous=$halaman-1; 
		echo'
			<button type="button" class="btn btn-primary btn-sm" page="1">First</button>
			<button type="button" class="btn btn-primary btn-sm" page="'.$previous.'">Prev</button>';
	}else{
		echo'
		<button type="button" class="btn btn-primary btn-sm" disabled="disabled">First</button>
		<button type="button" class="btn btn-primary btn-sm" disabled="disabled">Prev</button>';
	}
	$angka=($halaman > 3 ? ' <button type="button" class="btn btn-primary btn-sm" disabled>...</button> ' : " ");
	for($i=$halaman-2;$i<$halaman;$i++){
  		if ($i < 1) continue;
  		$angka .= '<button type="button" class="btn btn-primary btn-sm" page="'.$i.'">'.$i.'</button> ';
	}
	$angka .= ' <button type="button" class="btn btn-default btn-sm">'.$halaman.'</button> ';
	for($i=$halaman+1;$i<($halaman+3);$i++){
  		if ($i > $jmlhalaman) break;
  		$angka .= '<button type="button" class="btn btn-primary btn-sm" page="'.$i.'">'.$i.'</button> ';
	}
	$angka .= ($halaman+2<$jmlhalaman ? ' <button type="button" class="btn btn-primary btn-sm">...</button><button type="button" class="btn btn-primary btn-sm" page="'.$jmlhalaman.'">'.$jmlhalaman.'</button> ' : ' ');
	echo $angka;
	//link kehalaman berikutnya (Next)
	if($halaman < $jmlhalaman){
		$next=$halaman+1;
		echo '
			<button type="button" class="btn btn-primary btn-sm" page="'.$next.'">Next</button>
			<button type="button" class="btn btn-primary btn-sm" page="'.$jmlhalaman.'">Last</button> ';
	}else{
		echo '<button type="button" class="btn btn-primary btn-sm next" disabled="disabled">Next</button><button type="button" class="btn btn-primary btn-sm last" disabled="disabled">Last</button>';
	}
	
	$stmt = $db->query($syntax_list_paging);
	$crndata = $stmt->rowCount();
?>
	<button type="button" class="btn btn-primary btn-sm" disabled><?php echo "Menampilkan ".($posisi+1)." sampai ".($posisi+$crndata)." dari ".$jmldata." data"; ?></button>
</div>
</div>