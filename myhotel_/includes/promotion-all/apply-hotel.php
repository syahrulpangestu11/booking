<?php
  $promotionoid = $_GET['pid'];

    // include('includes/bootstrap.php');
    include('includes/promotion/geo-location/geo-location.php');
    // include("../ajax-include-file.php");
    // include("../paging/pre-paging.php");

    $daylist = array("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat");
    $stmt = $db->query("select max(priority)+1 as maxpriority from promotion where hoteloid = '".$hoteloid."' and publishedoid = '1'");
    $r_max = $stmt->fetchAll(PDO::FETCH_ASSOC);
    foreach($r_max as $row){
        $maxpriority = $row['maxpriority'];
    }
    
    $minstay = 0;
    $min_ci = 0;
    $max_ci = 0;
    $maxstay = 0;
    $discounttype = '1';
    $discountapply = '1';
    $minroom = '1';
    $deposit = '1';
    
    if(isset($_GET['tid'])){
        $promooid = $_GET['tid'];
        try {
            $stmt = $db->query("select * from promotion_template where templateoid = '".$promooid."'");
            $row_count = $stmt->rowCount();
            if($row_count > 0) {
                $r_promo = $stmt->fetchAll(PDO::FETCH_ASSOC);
                foreach($r_promo as $row){
                    $name = $row['name'];
                    $promoimage = $row['promoimage'];
                    $minstay = $row['minstay'];
                    $maxstay = $row['maxstay'];
                    $discounttype = $row['discounttypeoid'];
                    $discountapply = $row['discountapplyoid'];
                    $applyvalue = $row['applyvalue'];
                    $discountvalue = floor($row['discountvalue']);
                    $minroom = $row['minroom'];
                    $published = $row['publishedoid'];
                    $deposit = $row['depositoid'];
                    $depositvalue = $row['depositvalue'];
    
                    $min_ci = $row['min_bef_ci'];
                    $max_ci = $row['max_bef_ci'];
    
                    $headline = $row['headline'];
                    $description = $row['description'];
                    $servicefacilities = $row['servicefacilities'];
                    $termcondition = $row['termcondition'];
    
                }
            }else{
                echo "No Result";
                die();
            }
        }catch(PDOException $ex) {
            echo "Invalid Query";
            die();
        }
    }
?>
<script type="text/javascript">
$(function() {
    $('textarea#html-box').trumbowyg({
        btns: ['viewHTML',
          '|', 'btnGrp-design',
          '|', 'link',
          '|', 'btnGrp-justify',
          '|', 'btnGrp-lists'],
        fullscreenable: false
    });
});
</script>
<style type="text/css">
            h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
                font-family: inherit;
                font-weight: 600;
                line-height: inherit;
                color: inherit;
                margin-bottom:10px!important;
            }
            .wrapper {
                position: inherit;
                overflow: hidden!important;
            }
            .left-side {
                padding-top: inherit;
            }
            .sidebar > .sidebar-menu li > a:hover {
                background-color: rgba(72, 115, 175, 0.26);
            }
            .sidebar .sidebar-menu .treeview-menu {
                background-color: rgb(14, 26, 43);
            }
            .sidebar .sidebar-menu .treeview-menu .treeview-menu-child {
                background-color: rgba(0, 0, 0, 0.5);
            }
            .sidebar > .sidebar-menu li.active > a {
                background-color: rgba(197, 45, 47, 0.55);
            }
            .sidebar > .sidebar-menu > li.treeview.active > a {
                background-color: inherit;
            }
            .sidebar > .sidebar-menu > li.treeview.active li.treeview-child.active > a {
                background-color: inherit;
            }
            .sidebar .sidebar-menu > li > a > .fa {
                width: 28px;
                font-size: 16px;
            }
            .sidebar-menu, .main-sidebar .user-panel, .sidebar-menu>li.header{
                white-space:normal!important;
            }
            .form-group input[type=text]{
                width:100%!important;
            }

  #accordion .header {background: #f4f8fb !important; border: 1px solid #bbb; color: #222; margin-top: 10px; font-weight: normal;}
  #accordion .header.ui-accordion-header-active,
  #accordion .header.ui-state-hover {background-color: #5d9cec !important; border-color: transparent; color: #fff;}
  #accordion .header .table-cell {display: table-cell; vertical-align: middle; padding: 0 10px;}
  #accordion .header .table-cell:nth-of-type(1) {width: 10%;}
  #accordion .header#accordion .header .table-cell:nth-of-type(2) {width: 30%;}
  #accordion .content{ min-height:0!important; }
  #accordion #assigned-promotion > div{ margin-bottom:5px; }

  #accordion .header .image .thumbnail {width: 100%;}

  #accordion .content {
    background: #F4F8FB !important; border: 1px solid #bbb !important;
    border-top: none;
  }

  .btn-xs{ padding : 1px 5px!important; }
        </style>
<section class="content-header">
    <h1>
        Create Promotions
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i>  Allotments &amp; Rates </a></li>
        <li class="active">Promotions</li>
    </ol>
</section>

<section class="content" id="applypromotion">
               
        <form method="post" action="<?php echo $base_url; ?>/promotion-all/apply-process">    
        <div class="row">
           <div class="box">
            

<?php

 
  
  $datenow = date("Y-m-d");
  $range = $_REQUEST['range'];
  $hoteloid = $_REQUEST['hoteloid'];
  $hcode = $_REQUEST['hcode'];
  // $roomoffer = $_REQUEST['rt'];
  $channeloid = $_REQUEST['ch'];
  $startdate = date("Y-m-d", strtotime($_REQUEST['sdate']));
  $enddate = date("Y-m-d", strtotime($_REQUEST['edate']));

  function generateDirectPromotion($name, $minstay, $min_bef_ci){
    global $hcode;
    $link = 'https://thebuking.com/ibe/index.php?hcode='.$hcode.'&night='.$minstay.'&promotion='.$name.'&requirementcheckin='.$min_bef_ci.'&show=promo_only';
    return $link;
  }
  
  if(!empty($_REQUEST['sdate']) and empty($_REQUEST['edate'])){
    $startdate = date("Y-m-d", strtotime($_REQUEST['sdate']));
    
    if($range == "sale"){
      $filter_periode = "and (salefrom <= '".$startdate."' and saleto >= '".$startdate."')";
    }else{
      $filter_periode = "and (bookfrom <= '".$startdate."' and bookto >= '".$startdate."')";
    }
  }else if(empty($_REQUEST['sdate']) and !empty($_REQUEST['edate'])){
    $startdate = date("Y-m-d", strtotime($_REQUEST['sdate']));
    
    if($range == "sale"){
      $filter_periode = "and (salefrom <= '".$enddate."' and saleto >= '".$enddate."')";
    }else{
      $filter_periode = "and (bookfrom <= '".$enddate."' and bookto >= '".$enddate."')";
    }
  }else if(!empty($_REQUEST['sdate']) and !empty($_REQUEST['edate'])){
    if($range == "sale"){
      $filter_periode = "and 
    ( 
      (salefrom <= '".$startdate."' and (saleto >= '".$startdate."' and saleto <= '".$enddate."'))  
      or
      ((salefrom >= '".$startdate."' and salefrom <= '".$enddate."') and saleto >= '".$enddate."')
      or
      ((salefrom >= '".$startdate."' and salefrom <= '".$enddate."') and saleto <= '".$enddate."')
      or
      (salefrom <= '".$startdate."' and saleto >= '".$enddate."')
      or
      (salefrom >= '".$startdate."' and saleto <= '".$enddate."')
    )
      ";
    }else{
      $filter_periode = " and 
    ( 
      (bookfrom <= '".$startdate."' and (bookto >= '".$startdate."' and bookto <= '".$enddate."'))  
      or
      ((bookfrom >= '".$startdate."' and bookfrom <= '".$enddate."') and bookto >= '".$enddate."')
      or
      ((bookfrom >= '".$startdate."' and bookfrom <= '".$enddate."') and bookto <= '".$enddate."')
      or
      (bookfrom <= '".$startdate."' and bookto >= '".$enddate."')
      or
      (bookfrom >= '".$startdate."' and bookto <= '".$enddate."')
    )
      ";
    }
  }else{
    $filter_periode = "";
  }
  
  $filter_channel = "";

  if(!empty($roomoffer)){
    $filter_roomoffer = "and pa.roomofferoid = '".$roomoffer."'";
    $join_room = "inner join roomoffer ro using (roomofferoid) inner join room r using (roomoid) inner join channel ch using (channeloid)";
    if(!empty($channeloid)){
      $filter_channel = "and ch.channeloid = '".$channeloid."'";
    }
  }else{
    $filter_roomoffer = "";
    $join_room = "";
  }

  try {
    $stmt = $db->query("
    SELECT 'promotion' as promotype, p.promotionoid, p.name, p.saleto, p.salefrom, p.bookto, p.bookfrom, p.discountvalue, p.priority, pt.type, dt.labelquestion, p.minstay, p.min_bef_ci, (case when p.publishedoid = 1 and p.saleto < '".$datenow."' then 'status-red' when p.publishedoid = 1 then 'status-green' else 'status-black' end) as status, p.publishedoid, pb.note AS published_note from promotion p left join promotiontype pt using (promotiontypeoid) inner join discounttype dt using (discounttypeoid) left join promotionapply pa using (promotionoid) ".$join_room." left join published pb ON (p.publishedoid = pb.publishedoid) where p.hoteloid = '0' ".$filter_roomoffer." ".$filter_channel." ".$filter_periode." and p.publishedoid not in (3) and promotionoid = '".$promotionoid."' group by p.promotionoid
    ORDER BY p.publishedoid DESC, p.promotionoid DESC
    ");
    $row_count = $stmt->rowCount();
    if($row_count > 0) {
?>
<table class="table promo-table">
    <tr align="center">
        <td>Promotion</td>
        <td>Benefit</td>
        <td>Sale Date</td>
        <td>Stay Date</td>
        
    </tr> 
<?php
      $r_promo = $stmt->fetchAll(PDO::FETCH_ASSOC);
      foreach($r_promo as $row){
        $salefrom = date("d/M/Y", strtotime($row['salefrom']));
        $saleto = date("d/M/Y", strtotime($row['saleto']));
        $stayfrom = date("d/M/Y", strtotime($row['bookfrom']));
        $stayto = date("d/M/Y", strtotime($row['bookto']));
        // $promo_name = htmlspecialchars($row['name']);
        $promo_name = urlencode($row['name']);
        $promo_night = ($row['minstay'] < 2) ? 2 : $row['minstay'] ;
        if($row['status']=="status-green"){
          $published_class = " blue ";
          $published_note = $row['published_note'];
        }else if($row['status']=="status-red"){
          $published_class = " red ";
          $published_note = "expired";
        }else{ 
          $published_class = " grey " ;
          $published_note = $row['published_note'];
        }
?>
    <tr class="<?php echo $row['status']; ?>">
        <td class="<?php echo $row['status']; ?> center"><?php echo $row['name']; ?></td>
        <td class="center"><?php echo number_format($row['discountvalue'])." ".$row['labelquestion']; ?></td>
        <td class="center"><?php echo $salefrom; ?> - <?php echo $saleto; ?></td>
        <td class="center"><?php echo $stayfrom; ?> - <?php echo $stayto; ?></td>
        
    </tr> 
<?php       
      }
?>
</table>
<?php
    }
  }catch(PDOException $ex) {
    echo "Invalid Query";
    print($ex);
    die();
  }
?>
       
    </div>  <input type="hidden" name="oid" value="<?=$promotionoid?>" />
            <div class="box box-form" id="step-3">
        <div class="row">
            <div class="col-md-6"><h1><i class="fa fa-building"></i> Apply Promotion To Hotel</h1></div>
            <div class="col-md-6 text-right"><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#hotelListModal">Assign New Hotel</button></div>
        </div>
        <!-- <div class="row">
             <div class="col-md-3"><input type="text" class="form-control" name="name" placeholder="Promotion Name"></div>
          <div class="form-group"><button type="button" class="btn btn-info" id="find">Find</button></div>
        </div> -->
        <div class="row" id="assigned-hotel">
            <ul id="accordion">
             <?php
                $stmt = $db->prepare("select p.*, h.hotelname from promotionapply p inner join hotel h using (hoteloid) where p.promotionoid = :a group by hoteloid");
                $stmt->execute(array(':a' => $promotionoid));
                $r_lp_hotel = $stmt->fetchAll(PDO::FETCH_ASSOC);
                foreach($r_lp_hotel as $hotel){
            ?>
                <li lph="<?=$hotel['hoteloid']?>">
                    <div class="header">
                        <div class="row">
                            <div class="col-md-6"><?=$hotel['hotelname']?></div>
                            <div class="col-md-6 text-right">
                                <?php if($campaign['chainoid'] != 0){ ?>
                                <button type="button" class="btn btn-warning btn-sm" data-lph="<?=$hotel['hoteloid']?>" data-toggle="modal" data-target="#confirmDelHotel"><i class="fa fa-trash-o"></i> Remove Hotel</button>
                                <?php } ?>
                                <button type="button" class="btn btn-warning btn-sm" data-lph="<?=$hotel['hoteloid']?>" data-toggle="modal" data-target="#confirmDelHotel"><i class="fa fa-trash-o"></i> Remove Hotel</button>
                                <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#promotionListModal" data-hotel="<?=$promotionoid?>" data-lph="<?=$hotel['hoteloid']?>">Select Promotion</button>
                            </div>
                        </div>
                    </div>
                    <div class="content" id="assigned-promotion">
                       
                        <?php 
                            $stmt = $db->prepare("select po.*, ro.name from promotionapply po inner join hotel h using (hoteloid) inner join roomoffer ro using(roomofferoid) where po.hoteloid = :d and po.promotionoid = :e");
                            $stmt->execute(array(':d' => $hotel['hoteloid'], ':e' => $promotionoid));
                            $r_lp_promotion = $stmt->fetchAll(PDO::FETCH_ASSOC);
                            foreach($r_lp_promotion as $promotion){
                        ?>  
                            <div class="col-md-6"><button type="button" class="btn btn-danger btn-xs" id="remove-promotion" data-lpp="<?=$promotion['roomofferoid']?>" data-lph="<?=$promotion['hoteloid']?>" data-toggle="modal" data-target="#confirmDelPromotion" ><i class="fa fa-close"></i></button> <?=$promotion['name']?></div>
                        <?php
                            }

                        ?>
                    </div>
                </li>
            <?php
                }
            ?>
            </ul>
        </div>
                <br>
                <div class="row">
                        
                <div class="form-group">
                    <div class="col-md-12 text-right">

                        <button class="btn btn-danger cancelapply" type="button">Cancel</button>
                        <button class="btn btn-primary" type="submit">Save Apply Hotel</button>
                    </div>
                </div>
                </div>
    </div>
    </div>    
        </form>
    </div>
</section>
<div class="modal fade" id="hotelListModal" tabindex="-1" role="dialog" aria-labelledby="hotelListModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus-circle"></i> Add New Hotel</h4>
      </div>
      <div class="modal-body">
        <form method="post" id="form-assign-hotel" class="form-inline" enctype="multipart/form-data" action="#">
          <input type="hidden" name="chain" value="<?=$campaign['chainoid']?>">
          <input type="hidden" name="promotionoid" value="<?=$promotionoid?>">
          <div class="form-group"><label>Hotel Name</label></div>
          <div class="form-group"><input type="text" class="form-control" name="name" placeholder="Hotel Name"></div>
          <div class="form-group"><button type="button" class="btn btn-info" id="find">Find</button></div>

          <div class="row">
            <div id="list">

            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="assign-hotel">Assign Selected Hotel</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="promotionListModal" tabindex="-1" role="dialog" aria-labelledby="promotionListModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus-circle"></i> Add Roomtype</h4>
      </div>
      <div class="modal-body">
        <form method="post" id="form-assign-roomtype" class="form-inline" enctype="multipart/form-data" action="#">
          <input type="hidden" name="ho" value="">
          <input type="hidden" name="lph" value="">
          
          <div class="form-group"><label>Promotion Name</label></div>
          <div class="form-group"><input type="text" class="form-control" name="name" placeholder="Promotion Name"></div>
          <div class="form-group"><button type="button" class="btn btn-info" id="find">Find</button></div>

          <div class="row">
            <div id="list"></div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="assign-roomtype">Assign Selected Room Type</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="confirmDelPromotion" tabindex="-1" role="dialog" aria-labelledby="confirmDelPromotion">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus-trash"></i> Unassign Promotion</h4>
      </div>
      <div class="modal-body">
        <form method="post" id="form-remove-promotion" class="form-inline" enctype="multipart/form-data" action="#">
          Are you sure want to remove this roomtype from promotion?
                    <input type="hidden" name="lpp">
                    <input type="hidden" name="lph">
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
        <button type="button" class="btn btn-danger" id="remove-lpp">Yes</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="confirmDelHotel" tabindex="-1" role="dialog" aria-labelledby="confirmDelHotel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus-trash"></i> Unassign Promotion</h4>
      </div>
      <div class="modal-body">
        <form method="post" id="form-remove-hotel" class="form-inline" enctype="multipart/form-data" action="#">
          Are you sure want to remove this hotel from promotion?
                    <input type="hidden" name="lph">
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
        <button type="button" class="btn btn-danger" id="remove-lph">Yes</button>
      </div>
    </div>
  </div>
</div>
