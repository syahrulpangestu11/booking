<?php
	error_reporting(E_ALL ^ E_NOTICE);
	include("../../conf/connection.php");

	$promotionoid = $_POST['ho'];
	$hoteloid = $_POST['lph'];
    $promocodeoid = $_POST['pcoid'];
    $datenow = date("Y-m-d");
    // echo $hoteloid;    
$s_existed_promotion = "SELECT GROUP_CONCAT(roomofferoid) as existed from `promotionapply` where `promotionoid` = '".$promotionoid."' and hoteloid = '".$hoteloid."'";
    $stmt = $db->query($s_existed_promotion);
    $r_existed_promotion = $stmt->fetch(PDO::FETCH_ASSOC);
    if(!empty($r_existed_promotion['existed'])){
        $exist_promotion = $r_existed_promotion['existed'];
        $join = "inner join promotionapply pa using(roomofferoid)";
    }else{

        $exist_promotion = '';
    }

?>

<div class="box-body">
    <table class="table table-striped">
        <thead>
            <tr><th>No.</th>
            <th>Promotion</th>
            <th>Select</th>
        </tr></thead>
        <tbody>

        <?php
            // main query
            $batas=10;
			$halaman=isset($_REQUEST['page']) ? $_REQUEST['page'] : 0 ;
			if(empty($halaman)){
				$posisi=0; $halaman=1;
			}else{
				$posisi = ($halaman-1) * $batas;
			}
			$no=$posisi+1;
			$j = $halaman + ($halaman - 1) * ($batas - 1);

			$paging_query = " limit ".$posisi.", ".$batas;
			$num = $posisi;

            $main_query ="select r.roomofferoid, r.name, x.offernum from roomoffer r inner join room ro using (roomoid) inner join hotel h using (hoteloid) ".$join." left join (select count(roomofferoid) as offernum, ro.roomoid from roomoffer ro group by ro.roomoid) as x on x.roomoid = r.roomoid";
              
            // query tambahan jika ada filter
            $filter = array();
            if(isset($_REQUEST['name']) and $_REQUEST['name']!=''){
                array_push($filter, 'p.promotionname like "%'.$_REQUEST['keyword'].'%"');
            }
			array_push($filter, 'ro.hoteloid = "'.$hoteloid.'"');
            array_push($filter, 'x.offernum > 0');
			array_push($filter, 'r.publishedoid <> "3"');
			array_push($filter, 'r.publishedoid = "1"');

			if(!empty($exist_promotion)){
				array_push($filter, 'pa.roomofferoid not in ('.$exist_promotion.')');
			}
			// menggabungkan query utama dengan query filter
            if(count($filter) > 0){
                $combine_filter = implode(' and ',$filter);
                $syntax_list = $main_query.' where '.$combine_filter;
            }else{
                
                $syntax_list = $main_query;
            }
            // $syntax_list_last = " where hoteloid = '".$hoteloid."' and publishedoid not in (3) and saleto >= '".$datenow."' and salefrom <= '".$datenow."'";
            $syntax_list_paging = $syntax_list.$paging_query;
            
            $stmt = $db->query($syntax_list_paging);
			$result_list = $stmt->fetchAll(PDO::FETCH_ASSOC);?>
           <!--  <tr>
                <td>1</td>
                <td>Best Flexible Rate</td>
                <td><?php echo date('d/M/Y', strtotime($list['salefrom']))." - ".date('d/M/Y', strtotime($list['saleto'])); ?></td>
                <td class="text-center"><input type="checkbox" name="promotion-<?= $list['promotype']?>[]" value="<?=$list['oid']?> "><input type="hidden" name="promotype" value="<?= $list['promotype']?>"></td>
            </tr> -->
            
            <?php foreach($result_list as $list){
                
               
                $room_query = "select ro.* from roomoffer ro inner join published p using (publishedoid) inner join room r using (roomoid) inner join offertype using (offertypeoid) where ro.roomofferoid = '".$list['roomofferoid']."' and p.publishedoid = '1'";
        


                $stmt = $db->query($room_query);
                
                $row_count = $stmt->rowCount();
        
            
            $rowpromo_found = $stmt->fetchAll(PDO::FETCH_ASSOC);
            
            foreach($rowpromo_found as $roomtype){
            $num++;    
            
      
        ?>  
            
            <tr> <input type="hidden" name="hoteloid-<?= $list['promotype']?>" value="<?=$list['hoteloid']?>">
                <input type="hidden" name="promotionoid" value="<?=$promotionoid?>">
                <td><?php echo $num; ?></td>
                <td><?php echo $roomtype['name']; ?></td>
                <td><?php if($apply == "y"){ $checked = "checked"; }else{ $checked=""; } ?><input type="checkbox" name="roomtype[]" value="<?=$roomtype['roomofferoid']?> " <?php echo $checked; ?>></td>
                
                
            </tr>
        <?php
            }
            }
        ?>
        </tbody>
    </table>
    <?php require_once('paging/post-paging.php'); ?>
</div>
