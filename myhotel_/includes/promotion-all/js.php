<script type="text/javascript">
$(function(){
	
	$(document).ready(function(){ 
		
		getLoadData();

		$('input[type=radio][name=discounttype]:checked, input[type=radio][name=discountapply]:checked, input[type=radio][name=deposit]:checked, input[type=radio][name=link-style]:checked').each(function(){
			if($(this).prop("checked") == true){
				$(this).click();
			}
		});
		$('input[type="checkbox"][sign="rateplan"]').each(function(){
			if($(this).prop( "checked" ) == true){
				$(this).prop('checked',false);
				$(this).click();
			}
		});
		
	});
	
	$('body').on('click','input[type=radio][name=discounttype]', function(e) {
		
		if($(this).val() == '2'){
			$('li.applybox').css('display','none');
		}else{
			$('li.applybox').css('display','block');
		}
			
		if($(this).prop( "checked" ) == true){
			label = $(this).attr('label');
			$('input[type=text][name=discountvalue]').next('span.label').html(label);
		}
	});

	$('body').on('click','button.change-published', function(e) {
		id = $(this).attr("pid");
		old_publishedoid = $(this).attr("current-publishedoid");
		$.ajax({
			url: "<?php echo $base_url; ?>/includes/promotion/change-published-promotion.php",
			type: 'post',
			data: { 
				id : id,
				old_publishedoid : old_publishedoid
			},
			success: function(data) {
				var callback = '<?php echo $base_url; ?>/promotion-all'; 
				$(location).attr("href", callback); 
			}
		});
	});
	
	$('body').on('click','button.edit', function(e) {
		pid = $(this).attr("pid");
		url = "<?php echo $base_url; ?>/promotion-all/edit/?pid="+pid;
      	$(location).attr("href", url);
	});

	$('body').on('click','button.add', function(e) {
		pid = $(this).attr("pid");
		url = "<?php echo $base_url; ?>/promotion-all/add";
      	$(location).attr("href", url);
	});
	$('body').on('click','button.applyhotel', function(e) {
		pid = $(this).attr("pid");
		url = "<?php echo $base_url; ?>/promotion-all/applyhotel/?pid="+pid;
      	$(location).attr("href", url);
	});
	$('body').on('click','button.add-package', function(e) {
		pid = $(this).attr("pid");
		url = "<?php echo $base_url; ?>/package/add";
      	$(location).attr("href", url);
	});
	
	$('body').on('click','button.cancel', function(e) {
		url = "<?php echo $base_url; ?>/promotion-all";
      	$(location).attr("href", url);
	});
	$('body').on('click','button.cancelapply', function(e) {
		url = "<?php echo $base_url; ?>/promotion-all/add";
      	$(location).attr("href", url);
	});

	$('body').on('click','button.template', function(e) {
		url = "<?php echo $base_url; ?>/promotion-all/template";
      	$(location).attr("href", url);
	}); 
	
	$('body').on('click','button.copy', function(e) {
		pid = $(this).attr("pid");
		url = "<?php echo $base_url; ?>/promotion-all/add/?tid="+pid;
      	$(location).attr("href", url);
	});
	
   var $dialogDelete = $('<div id="dialog-del"></div>')
	.html('Are you sure you want to delete this data?')
	.dialog({
		autoOpen: false,
		title: 'Confirmation',
		buttons: { 
			"Delete": function(){ 
				var url = $(this).data('url'); 
				var id = $(this).data('id'); 
				$(this).dialog("close"); 
				deleteAction(url, id);
			},
			Cancel: function(){ $( this ).dialog("close"); }
		}
	});
	
	var $dialogNotification = $('<div id="dialog-notice"></div>')
	.dialog({
		autoOpen: false,
		title: 'Notification',
		buttons: { 
			"Ok": function(){ 
				var callback = '<?php echo $base_url; ?>/promotion-all'; 
				$(this).dialog( "close" );
				$(location).attr("href", callback); 
			}
		}
	});
	
	function deleteAction(url, id){
		$.ajax({
			url: url,
			type: 'post',
			data: { id : id},
			success: function(data) {
				if(data == "1"){
					$dialogNotification.html("Data has been deleted");
					$dialogNotification.dialog("open");
				}else{
					$dialogNotification.html("We&rsquo;re very sorry, we can't save your data right now");
					$dialogNotification.dialog("open");
				}
			}
		});
	}
	
	$('body').on('click','button.delete', function(e) {
		id = $(this).attr("pid");
		$dialogDelete.data("url", "<?php echo $base_url; ?>/includes/promotion/delete-promotion.php");
		$dialogDelete.data("id", id);
		$dialogDelete.dialog("open");
	});
	
	$('body').on('click','input[type="radio"][name=discountapply]', function(e) {
		allElem = $('div.applyval');
		allElem.css('display','none');
		
		if($(this).val() == '2'){
			$('input[type="checkbox"][name="applyvalue[]"]').prop('disabled',true);
			$('input[type="text"][name="applyvalue"]').prop('disabled',false);
		}else{
			$('input[type="checkbox"][name="applyvalue[]"]').prop('disabled',false);
			$('input[type="text"][name="applyvalue"]').prop('disabled',true);
		}		
		
		if($(this).val() == '2' || $(this).val() == '3' ){
			applyval = $(this).next('div.applyval');
			if($(this).prop( "checked" ) == true){
				applyval.css('display','block');
			}else{
				applyval.css('display','none');
			}
		}
	});
	
	$('body').on('click','input[type="radio"][name=deposit]', function(e) {
		allElem = $('div.depositval');
		allElem.css('display','none');
		
		
		if($(this).val() == '2' || $(this).val() == '3'  || $(this).val() == '4' ){
			applyval = $(this).next('div.depositval');
			if($(this).prop( "checked" ) == true){
				applyval.css('display','block');
			}else{
				applyval.css('display','none');
			}
		}
		
		$('input[type="text"][name="depositvalue"]').prop('disabled',true);
		$(this).next('div.depositval').children('input[type="text"][name="depositvalue"]').prop('disabled',false);
	});
	
	function getLoadData(){
		$.ajax({
			url: "<?php echo"$base_url"; ?>/includes/promotion-all/data.php",
			type: 'post',
			data: $('form#data-input').serialize(),
			success: function(data) {
				$("#data-box").html(data)
			}
		});
	}	
	/*
	$('body').on('click','input[type="checkbox"][name="roomtype[]"]', function(e) {
		rt_channel = $(this).parent().children('ul.roomtype-channel');
		li_rt = $(this).parent();
		if($(this).prop( "checked" ) == true){
			rt_channel.css('display','block');
			li_rt.addClass('selected')
		}else{
			rt_channel.css('display','none');
			li_rt.removeClass('selected')
		}
	});
	*/
	$('body').on('click','input:radio[name="select"]', function(e) {
		if($(this).val() == 'all'){
			$('input[type="checkbox"][sign="rateplan"], .mutliSelect input[type="checkbox"]').each(function(){
				if($(this).prop( "checked" ) == false){
					$(this).click();
				}
			});		
		}else{
			$('input[type="checkbox"][sign="rateplan"], .mutliSelect input[type="checkbox"]').each(function(){
				if($(this).prop( "checked" ) == true){
					$(this).click();
				}
			});	
		}
	});
	$('body').on('click','input[type="checkbox"][sign="rateplan"]', function(e) {
		dl_dropdown = $(this).parent().children('dl.dropdown');
		if($(this).prop( "checked" ) == true){
			dl_dropdown.css('display','block');
		}else{
			dl_dropdown.css('display','none');
		}
	});

	$(".dropdown dt a").on('click', function () {
	  $(this).parent().parent().children("dd").children("ul").slideToggle('fast');
	});		
	
	$('.mutliSelect input[type="checkbox"]').on('click', function () {
		val = $(this).attr('val');
		var title = $(this).closest('.mutliSelect').find('input[type="checkbox"]').attr('val'),
		  title = $(this).attr('val') + ",",
		  box = $(this).parent().parent().parent().parent().children('dt').children('a').children('.multiSel'),
		  hida = $(this).parent().parent().parent().parent().children('dt').children('a').children('.hida'),
		  dropdown_dt = $(this).parent().parent().parent().parent().children('dt').children('a');
		
		  if ($(this).is(':checked')) {
			  var html = '<span title="' + val + '">' + title + '</span>';
			  box.append(html);
			  hida.hide();
		  } else {
			  box.children('span[title="' + val + '"]').remove();
			  var ret = hida;
			  dropdown_dt.append(ret);
		  }
	});	
	
	$('input[name=night]').on('change', function(){
		night = $(this).val();
		$('[data-night]').html(night);
	});
	
	$('body').on('click','input[name=link-style]', function(e) {
		textarea = $('textarea[name="direct-link"]');
		urlpromotion = 'https://thebuking.com/ibe/index.php?hcode=<?=$hcode?>&night='+encodeURI($('input[name=minstay]').val())+'&promotion='+encodeURI($('input[name=name]').val())+'&requirementcheckin='+encodeURI($('input[name=min_ci]').val());
		var snurl = "";
		if($(this).val() == 'primary'){
			snurl = urlpromotion + '&show=promo_only';
		}else{
			snurl = urlpromotion + '&show=all';
		}
		textarea.val(snurl);

		snurl = snurl + "&utm_source=" + encodeURI($('input[name="utm_source"]').val()) + "&utm_champaign=" + encodeURI($('input[name="utm_campaign"]').val());
		textarea2 = $('textarea[name="direct2-link"]');
		textarea2.val(snurl);
	});

	$('body').on('keyup','.utmset', function(e) {
		textarea = $('textarea[name="direct-link"]');
		var snurl = textarea.val();
		snurl = snurl + "&utm_source=" + encodeURI($('input[name="utm_source"]').val()) + "&utm_champaign=" + encodeURI($('input[name="utm_campaign"]').val());
		textarea2 = $('textarea[name="direct2-link"]');
		textarea2.val(snurl);
	});
	
	$('body').on('click','button.copy-link', function(e) {
		var $temp = $("<input>");
		$("body").append($temp);
		$temp.val($('textarea[name="direct-link"]').val()).select();
		document.execCommand("copy");
		$temp.remove();
	});
	
	$('body').on('click','button.copy2-link', function(e) {
		var $temp = $("<input>");
		$("body").append($temp);
		$temp.val($('textarea[name="direct2-link"]').val()).select();
		document.execCommand("copy");
		$temp.remove();
	});

	$('body').on('click','button.copy-link-list', function(e) {
		var thisLink = $(this).attr('link');
		var $temp = $("<input>");
		$("body").append($temp);
		$temp.val(thisLink).select();
		document.execCommand("copy");
		$temp.remove();
	});
	
	function readURL(input, target) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
				target.attr('src', e.target.result).show();
			};
			reader.readAsDataURL(input.files[0]);
		}
	}
	
	$('body').on('change','input[name=image]', function(e){
		box_preview_image = $('div.preview-image');
		if(this.files[0].size <= 0){
			box_preview_image.html('<div class="panel panel-warning"><div class="panel-heading">N/A Image for Promotion</div></div>');
		}else{
			var ext = $(this).val().split('.').pop().toLowerCase();
			if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
				box_preview_image.html('<div class="panel panel-warning"><div class="panel-heading">Invalid extension! Please upload image with JPEG, PNG, JPG, and GIF type only.</div></div>');
			}else{
				box_preview_image.html('<img src="#" id="preview-image" class="img-responsive" alt="Responsive image" style="width:200px;">');
				target = $('div.preview-image').children('img');
				readURL(this, target);
			}
		}
	});
	$('#hotelListModal').on('show.bs.modal', function (e) {
		generateHotel(1);
	});
	// jalankan fungsi saat pencarian di modal hotel
	$('#hotelListModal button#find').click(function(){
		generateHotel(1);
	});
	// jalankan fungsi paging list pelanggan di modal hotel
	$('body').on('click','#hotelListModal #paging button', function(e) {
		page	= $(this).attr('page');
		generateHotel(page);
	});

	$('#promotionListModal').on('show.bs.modal', function (e) {
		 var button = $(e.relatedTarget)
		 // alert(button);
		 $(this).find('input[name=ho]').val(button.data('hotel'));
		 $(this).find('input[name=lph]').val(button.data('lph'));
		 generatePromotion(1);
	});
	// jalankan fungsi saat pencarian di modal promotion
	$('#promotionListModal button#find').click(function(){
		generatePromotion(1);
	});
	// jalankan fungsi paging list pelanggan di modal promotion
	$('body').on('click','#promotionListModal #paging button', function(e) {
		page	= $(this).attr('page');
		generatePromotion(page);
	});

	/*
	* LOAD DATA
	*/

	// fungsi untuk menampilkan list pelanggan pada modal sesuai nomor paging
	function generateHotel(page){
		var chain = $('#hotelListModal').find('input[name=chain]').val();
		var name = $('#hotelListModal').find('input[name=name]').val();
		var lp = $('input[name=oid]').val();	
		var listbox = $('#hotelListModal').find('div#list');
		
		$.ajax({
			url		: '<?php echo $base_url; ?>/includes/promotion-all/list-hotel.php',
			type	: 'post',
			data	: { chain : chain, name : name, lp : lp, page : page },
			success	: function(response) {
				listbox.html(response);
			}
		});
	}

	function generatePromotion(page){
		var ho = $('#promotionListModal').find('input[name=ho]').val();
		var lph = $('#promotionListModal').find('input[name=lph]').val();
		var pcoid = $('#promotionListModal').find('input[name=pcoid]').val();
		var listbox = $('#promotionListModal').find('div#list');
	
		$.ajax({
			url		: '<?php echo $base_url; ?>/includes/promotion-all/list-roomtype.php',
			type	: 'post',
			data	: { name : name, ho : ho, lph : lph, pcoid :pcoid, page : page },
			success	: function(response) {
				listbox.html(response);
			}
		});
	}

	// $('button.submit-add').click(function(){
	// 	currentbutton = $(this);
	// 	forminput = $('form#data-input');
	// 	$.ajax({
	// 		url: '<?php echo $base_url; ?>/includes/promotion-all/rqst-action.php',
	// 		type: 'post',
	// 		data: forminput.serialize() + '&request=new-program',
	// 		success: function(data) {
	// 			if(data == "success"){
	// 				$dialogNotice.html("Loyalty Member Program has been created. System will refresh the page");
	// 				$dialogNotice.dialog("open");
	// 			}else{
	// 				$dialogNotice.html("We&rsquo;re very sorry, we can't save your data right now"+data);
	// 				$dialogNotice.dialog("open");
	// 			}
	// 		}
	// 	});
	// });

	// $('button.submit-edit').click(function(){
	// 	currentbutton = $(this);
	// 	forminput = $('form#data-input');
	// 	$.ajax({
	// 		url: '<?php echo $base_url; ?>/includes/promotion-all/rqst-action.php',
	// 		type: 'post',
	// 		data: forminput.serialize() + '&ho=<?=$hoteloid?>&request=update-program',
	// 		success: function(data) {
	// 			if(data != "error"){
	// 				$dialogNotice.html("Loyalty Member Program has been created. System will refresh the page");
	// 				$dialogNotice.dialog("open");
	// 			}else{
	// 				$dialogNotice.html("We&rsquo;re very sorry, we can't save your data right now");
	// 				$dialogNotice.dialog("open");
	// 			}
	// 		}
	// 	});
	// });
		
	$('body').on('click', '#hotelListModal button#assign-hotel', function (e){
		var forminput = $('#hotelListModal').find('form#form-assign-hotel');
		var lp = $('input[name=loyaltyprogram]').val();
		// alert(lp);
		$.ajax({
			url		: '<?php echo $base_url; ?>/includes/promotion-all/rqst-action.php',
			type	: 'post',
			data	: forminput.serialize() + '&loyaltyprogram=' + lp + '&request=assign-hotel',
			success	: function(response){
				$('div#assigned-hotel').find('ul#accordion').append(response);
				$('#accordion').accordion("refresh");
			}
		});
		$('#hotelListModal').modal('toggle');
	});


	$('body').on('click', '#promotionListModal button#assign-roomtype', function (e){
		var forminput = $('#promotionListModal').find('form#form-assign-roomtype');
		var lph = $('input[name=lph]').val();
		var targetbox = $('div#assigned-hotel').find('li[lph='+lph+']').find('div#assigned-promotion');

		
		
		$.ajax({
			url		: '<?php echo $base_url; ?>/includes/promotion-all/rqst-action.php',
			type	: 'post',
			data	: forminput.serialize() + '&loyaltyprogramhotel=' + lph + '&request=assign-roomtype',
			success	: function(response){
				targetbox.append(response);
			}
		});
		$('#promotionListModal').modal('toggle');
	});

	$('body').on('click', '#membershipModal button#save', function (e){
		modal = $('#membershipModal');
		//alert($('div#membership').find('table').html());
		$('table#membership').children('tbody').append('<tr><td><input type="text" name="m-name[]" class="form-control" value="'+ modal.find('input[name=name]').val() +'" /></td><td><input type="text" name="m-startpoint[]" class="form-control" value="'+ modal.find('input[name=startpoint]').val() +'" /></td><td><input type="text" name="m-endpoint[]" class="form-control" value="'+ modal.find('input[name=endpoint]').val() +'" /></td><td><input type="text" class="form-control" name="m-description[]" value="'+ modal.find('textarea[name=description]').val() +'"></td><td><div class="input-group"><input type="number" class="form-control" name="m-discount[]" value="'+ modal.find('input[name=discount]').val() +'" required><div class="input-group-addon">%</div></div></td><td><button type="button" class="btn btn-danger btn-sm" id="remove-membership"><i class="fa fa-trash-o"></i></button></td></tr>');
		$('#membershipModal').modal('toggle');
	});

	$('body').on('click', 'button#remove-membership', function (e){
		$(this).parent().parent('tr').remove();
	});
	/*confirmation before remove promotion*/
	$('#confirmDelHotel').on('show.bs.modal', function (e) {
		 var promotion = $(e.relatedTarget).data('lph');
		 $(e.currentTarget).find('input[name="lph"]').val(promotion);
	});
	/*remove promotion*/
	$('body').on('click', '#confirmDelHotel button#remove-lph', function (e){
		$('#confirmDelHotel').modal('toggle');
		var lph = $('#form-remove-hotel').find('input[name=lph]').val();
		
		$.ajax({
			url		: '<?php echo $base_url; ?>/includes/promotion-all/rqst-action.php',
			type	: 'post',
			data	: { lph : lph, request : 'unassign-hotel'},
			success	: function(response){
				if(response == 'success'){
					$dialogNotice.html("Hotel has been removed");
					$dialogNotice.dialog("open");
				}else{
					$dialogNotice.html("Hotel failed to remove");
					$dialogNotice.dialog("open");
				}
			}
		});
	});

	/*confirmation before remove promotion*/
	$('#confirmDelPromotion').on('show.bs.modal', function (e) {
		 var promotion = $(e.relatedTarget).data('lpp');
		 $(e.currentTarget).find('input[name="lpp"]').val(promotion);
		 var hoteloid = $(e.relatedTarget).data('lph');
		 $(e.currentTarget).find('input[name="lph"]').val(hoteloid);
	});
	/*remove promotion*/
	$('body').on('click', '#confirmDelPromotion button#remove-lpp', function (e){
		$('#confirmDelPromotion').modal('toggle');
		var lpp = $('#form-remove-promotion').find('input[name=lpp]').val();
		var lph = $('#form-remove-promotion').find('input[name=lph]').val();
		
		$.ajax({
			url		: '<?php echo $base_url; ?>/includes/promotion-all/rqst-action.php',
			type	: 'post',
			data	: { lpp : lpp, lph : lph, request : 'unassign-roomtype'},
			success	: function(response){
				if(response == 'success'){
					$dialogNotice.html("Roomtype has been removed");
					$dialogNotice.dialog("open");
				}else{
					$dialogNotice.html("Roomtype failed to remove");
					$dialogNotice.dialog("open");
				}
			}
		});
	});

	$('button.cancel').click(function(){
		$(location).attr('href', '<?=$base_url?>/promotion-all')
	});

	/*
	* DIALOG
	*/

	var $dialogNotice = $('<div id="dialog-notice"></div>')
	.dialog({
		autoOpen: false,
		title: 'Notification',
		buttons: {
			"Ok": function(){
				$( this ).dialog( "close" );
				location.reload();
			}
		}
	});

	/*
	* ACCORDION
	*/

	$( "#accordion" ).accordion({
		header: ".header",
		collapsible: true,
		active: false,
		heightStyle: "content",
	});
	
});
</script>

<style>
	span.label{ color:#333; font-size:0.9em; } 
</style>