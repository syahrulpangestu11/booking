<?php
$activemenu = array("reservation-chart", "calendar-view", "three-month", "rsvlist");
if(in_array($uri3, $activemenu) or $uri2 == "master-roomnumber" or $uri2 == "agent"){ $active = "active"; }else{ $active = ""; }
?>
<li class="treeview <?=$active?>">
	<a href="#"><i class="fa fa-window-restore"></i> <span>Front Office</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
        <li class="<?php if($uri2=="pms-lite" and $uri3=="reservation-chart"){ echo "active"; }?>"><a href="<?=$base_url?>/pms-lite/reservation-chart"><i class="fa fa-circle-o"></i> Reservation Chart</a></li>
        <li class="<?php if($uri2=="pms-lite" and $uri3=="calendar-view"){ echo "active"; }?>"><a href="<?=$base_url?>/pms-lite/calendar-view"><i class="fa fa-circle-o"></i> Calendar View</a></li>
        <li class="<?php if($uri2=="pms-lite" and $uri3=="three-month"){ echo "active"; }?>"><a href="<?=$base_url?>/pms-lite/three-month"><i class="fa fa-circle-o"></i> 3 Month Calendar</a></li>
        <li class="<?php if($uri2=="master-roomnumber"){ echo "active"; }?>"><a href="<?=$base_url?>/master-roomnumber"><i class="fa fa-cog"></i> Master Room Number</a></li>
        <li class="<?php if($uri2=="agent"){ echo "active"; }?>"><a href="<?=$base_url?>/agent"><i class="fa fa-cog"></i> Master Agent</a></li>
        <li class="<?php if($uri2=="pms-report" and $uri3=="rsvlist"){ echo "active"; }?>"><a href="<?=$base_url?>/pms-report/rsvlist"><i class="fa fa-bed"></i> <span>Reservation List</span></a></li>
    </ul>
</li>

<li <?php if($uri2=="pms-lite" and $uri3=="housekeeping"){ echo "class=active"; }?>>
	<a href="<?=$base_url?>/pms-lite/housekeeping"><i class="fa fa-street-view"></i> <span>Housekeeping</span></a>
</li>

<?php
$activemenu = array("incomereport", "arledgerreport", "guestinhouselist", "otherchargereport");
if(in_array($uri3, $activemenu) and $uri2!="pms-lite"){ $active = "active"; }else{ $active = ""; }
?>
<li class="treeview <?=$active?>">
	<a href="#"><i class="fa fa-area-chart"></i> <span>PMS Reports</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
        <li class="<?php if($uri2=="pms-report" and $uri3=="incomereport"){ echo "active"; }?>"><a href="<?=$base_url?>/pms-report/incomereport"><i class="fa fa-circle-o"></i> Income Report</a></li>
        <li class="<?php if($uri2=="pms-report" and $uri3=="arledgerreport"){ echo "active"; }?>"><a href="<?=$base_url?>/pms-report/arledgerreport"><i class="fa fa-circle-o"></i> AR Ledger Report</a></li>
        <?php
            $check_user_access = "select count(h.hoteloid) as foundnagisa from hotel h inner join city ct using (cityoid) inner join chain c using (chainoid) where ((h.chainoid in (select oid from userassign where type = 'chainoid' and useroid = '".$_SESSION['_oid']."')) or (h.hoteloid in (select oid from userassign where type = 'hoteloid' and useroid = '".$_SESSION['_oid']."'))) and h.publishedoid = '1'and h.chainoid = '64'";
            $q_user_access = $db->query($check_user_access);
            $r_user_access = $q_user_access->fetch(PDO::FETCH_ASSOC);
            if($r_user_access['foundnagisa'] == 0){
        ?>
        <li class="<?php if($uri2=="pms-report" and $uri3=="guestinhouselist"){ echo "active"; }?>"><a href="<?=$base_url?>/pms-report/guestinhouselist"><i class="fa fa-circle-o"></i> Guest Deposit</a></li>
        <?php
            }
        ?>
        <li class="<?php if($uri2=="pms-report" and $uri3=="otherchargereport"){ echo "active"; }?>"><a href="<?=$base_url?>/pms-report/otherchargereport"><i class="fa fa-circle-o"></i> Other Charge Report</a></li>
    </ul>
</li>

<?php
//$activemenu = array("rsv", "rsvbymarket", "market", "incomereport");
/*$activemenu = array("rsv", "rsvbymarket", "market", "incomereport", "flashmgr", "forecast", "businessanalyst", "occupancyanalysis", "dailyreport", "roomperbook", "mtdytd", "housekeeping-report", "dailyrevenue");
if(in_array($uri3, $activemenu) and $uri2!="pms-lite"){ $active = "active"; }else{ $active = ""; }
?>
<li class="treeview <?=$active?>">
	<a href="#"><i class="fa fa-area-chart"></i> <span>Reservation Report</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
        <li class="<?php if($uri2=="pms-report" and $uri3=="rsv"){ echo "active"; }?>"><a href="<?=$base_url?>/pms-report/rsv"><i class="fa fa-circle-o"></i> Reservation Report</a></li>
        <li class="<?php if($uri2=="pms-report" and $uri3=="rsvbymarket"){ echo "active"; }?>"><a href="<?=$base_url?>/pms-report/rsvbymarket"><i class="fa fa-circle-o"></i> Market Report</a></li>
        <li class="<?php if($uri2=="pms-report" and $uri3=="market"){ echo "active"; }?>"><a href="<?=$base_url?>/pms-report/market"><i class="fa fa-circle-o"></i> Summary by Market</a></li>
        <li class="<?php if($uri2=="pms-report" and $uri3=="incomereport"){ echo "active"; }?>"><a href="<?=$base_url?>/pms-report/incomereport"><i class="fa fa-circle-o"></i> Income Report</a></li>
				<!----------------------->
				<li class="<?php if($uri2=="pms-report" and $uri3=="flashmgr"){ echo "active"; }?>"><a href="<?=$base_url?>/pms-report/flashmgr"><i class="fa fa-circle-o"></i> Flash Manager</a></li>
        <li class="<?php if($uri2=="pms-report" and $uri3=="forecast"){ echo "active"; }?>"><a href="<?=$base_url?>/pms-report/forecast"><i class="fa fa-circle-o"></i> Forecast</a></li>
        <li class="<?php if($uri2=="pms-report" and $uri3=="businessanalyst"){ echo "active"; }?>"><a href="<?=$base_url?>/pms-report/businessanalyst"><i class="fa fa-circle-o"></i> Business Analysis</a></li>
        <li class="<?php if($uri2=="pms-report" and $uri3=="occupancyanalysis"){ echo "active"; }?>"><a href="<?=$base_url?>/pms-report/occupancyanalysis"><i class="fa fa-circle-o"></i> Occupancy Analysis</a></li>
        <li class="<?php if($uri2=="pms-report" and $uri3=="dailyreport"){ echo "active"; }?>"><a href="<?=$base_url?>/pms-report/dailyreport"><i class="fa fa-circle-o"></i> Daily Report</a></li>
        <li class="<?php if($uri2=="pms-report" and $uri3=="roomperbook"){ echo "active"; }?>"><a href="<?=$base_url?>/pms-report/roomperbook"><i class="fa fa-circle-o"></i> Room per Book</a></li>
        <li class="<?php if($uri2=="pms-report" and $uri3=="mtdytd"){ echo "active"; }?>"><a href="<?=$base_url?>/pms-report/mtdytd"><i class="fa fa-circle-o"></i> MTD &amp; YTD Report</a></li>
        <li class="<?php if($uri2=="pms-report" and $uri3=="housekeeping"){ echo "active"; }?>"><a href="<?=$base_url?>/pms-report/housekeeping"><i class="fa fa-circle-o"></i> Housekeeping Report</a></li>
        <li class="<?php if($uri2=="pms-report" and $uri3=="dailyrevenue"){ echo "active"; }?>"><a href="<?=$base_url?>/pms-report/dailyrevenue"><i class="fa fa-circle-o"></i> Daily Revenue</a></li>
    </ul>
</li>

<?php */
/*
$activemenu = array("flashmgr", "forecast", "businessanalyst", "occupancyanalysis", "dailyreport", "roomperbook", "mtdytd", "housekeeping", "dailyrevenue");
if(in_array($uri3, $activemenu)){ $active = "active"; }else{ $active = ""; }
?>
<li class="treeview <?=$active?>">
	<a href="#"><i class="fa fa-area-chart"></i> <span>Analytics Report</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
        <li class="<?php if($uri2=="pms-report" and $uri3=="flashmgr"){ echo "active"; }?>"><a href="<?=$base_url?>/pms-report/flashmgr"><i class="fa fa-circle-o"></i> Flash Manager</a></li>
        <li class="<?php if($uri2=="pms-report" and $uri3=="forecast"){ echo "active"; }?>"><a href="<?=$base_url?>/pms-report/forecast"><i class="fa fa-circle-o"></i> Forecast</a></li>
        <li class="<?php if($uri2=="pms-report" and $uri3=="businessanalyst"){ echo "active"; }?>"><a href="<?=$base_url?>/pms-report/businessanalyst"><i class="fa fa-circle-o"></i> Business Analysis</a></li>
        <li class="<?php if($uri2=="pms-report" and $uri3=="occupancyanalysis"){ echo "active"; }?>"><a href="<?=$base_url?>/pms-report/occupancyanalysis"><i class="fa fa-circle-o"></i> Occupancy Analysis</a></li>
        <li class="<?php if($uri2=="pms-report" and $uri3=="dailyreport"){ echo "active"; }?>"><a href="<?=$base_url?>/pms-report/dailyreport"><i class="fa fa-circle-o"></i> Daily Report</a></li>
        <li class="<?php if($uri2=="pms-report" and $uri3=="roomperbook"){ echo "active"; }?>"><a href="<?=$base_url?>/pms-report/roomperbook"><i class="fa fa-circle-o"></i> Room per Book</a></li>
        <li class="<?php if($uri2=="pms-report" and $uri3=="mtdytd"){ echo "active"; }?>"><a href="<?=$base_url?>/pms-report/mtdytd"><i class="fa fa-circle-o"></i> MTD &amp; YTD Report</a></li>
        <li class="<?php if($uri2=="pms-report" and $uri3=="housekeeping"){ echo "active"; }?>"><a href="<?=$base_url?>/pms-report/housekeeping"><i class="fa fa-circle-o"></i> Housekeeping Report</a></li>
        <li class="<?php if($uri2=="pms-report" and $uri3=="dailyrevenue"){ echo "active"; }?>"><a href="<?=$base_url?>/pms-report/dailyrevenue"><i class="fa fa-circle-o"></i> Daily Revenue</a></li>
    </ul>
</li>
*/
?>
