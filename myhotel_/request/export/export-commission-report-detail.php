<?php
	include('../../conf/connection.php');
	include('../../includes/class/reporting1.php');
	
	$hoteloid	= $_GET['hoteloid'];
    if(isset($_GET['startdate']) and isset($_GET['enddate'])){
    	$startdate = date('Y-m-d', strtotime($_GET['startdate']));
		$enddate = date('Y-m-d', strtotime($_GET['enddate']));
    }else{
    	$startdate = date('Y-m-d', strtotime(date('Y').'-'.date('m').'-01'));
		$enddate = date('Y-m-t', strtotime($startdate));
    }
	
	$report = new Reporting1($db);
	$report->setHotel($hoteloid);

    $data = array();
	$n = 0;
	$report->setPeriode($startdate, $enddate);
	$list_booking = $report->listBooking();
	
	if(!empty($list_booking)){
		$i = 1;
		foreach($list_booking as $key => $row){
			$list_room = $report->listRoomBooking($row['bookingoid']);
			$checkin = array();
			$checkout = array();
			$room = array();
			$promotion = array(); 
			if(!empty($list_room)){
				foreach($list_room as $key2 => $row2){
					array_push($checkin, $row2['checkin']);
					array_push($checkout, $row2['checkout']);
					array_push($room, $row2['room']);
					array_push($promotion, $row2['promotion']);
				}
			}
			
			if($row['bookingstatusoid'] != '4'){
				$grandtotal = 0;
			}else{
				$grandtotal = $row['grandtotal'];
			}

			if($row['grandtotal'] != $row['grandtotalr']){
				$row['commission'] = $row['commissionr'];
			}
			
			$typecomm = '';
			$periode1 = date('Y-m-d', strtotime($row['bookingtime']));
			/*base commission*/
			$query_base_commission = "select value as base, typecomm from hotelcommission where hoteloid = '".$hoteloid."' and ( (startdate <= '".$periode1."' and enddate >= '".$periode1."') or (startdate <= '0000-00-00' and enddate >= '0000-00-00')) and type = 'base' order by priority ASC limit 1";
			$stmt	= $db->query($query_base_commission);
			if($stmt->rowCount()){
				$result = $stmt->fetch(PDO::FETCH_ASSOC);
				$typecomm = $result['typecomm'];
			}
			/*override commission*/
			$query_override_commission = "select value as override, typecomm from hotelcommission where hoteloid = '".$hoteloid."' and ( (startdate <= '".$periode1."' and enddate >= '".$periode1."') or (startdate <= '0000-00-00' and enddate >= '0000-00-00')) and type = 'override' order by priority ASC limit 1";
			$stmt	= $db->query($query_override_commission);
			if($stmt->rowCount()){
				$result = $stmt->fetch(PDO::FETCH_ASSOC);
				$typecomm = $result['typecomm'];
			}
			if(empty($typecomm) or $typecomm == '2' or $typecomm == '4') $row['commission'] = 0;
		
			array_push($data, array("No" => $n, "Booking Number" => $row['bookingnumber'], "Booking Time" => date('d M Y H:i', strtotime($row['bookingtime'])), "Guest Name" => $row['guestname'], "Check - in" => implode('<br>', $checkin), "Check - out" => implode('<br>', $checkout), "Room Type" => implode('<br>', $room), "Promotion" => implode('<br>', $promotion), "Confirmed Booking" => number_format($grandtotal), "Pinalty Cancelled" => number_format($row['cancellationamount']), "Commission" => number_format($row['commission']), "Status" => $row['status']));
		}
	}
    
    function filterData(&$str)
    {
        $str = preg_replace("/\t/", "\\t", $str);
        $str = preg_replace("/\r?\n/", "\\n", $str);
        if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
    }
    
    // file name for download
    $fileName = "commission_report_detail_".date('mdY').".xls";
    
    // headers for download
    header("Content-Disposition: attachment; filename=\"$fileName\"");
    header("Content-Type: application/vnd.ms-excel");
    
    $flag = false;
    foreach($data as $row) {
        if(!$flag) {
            // display column names as first row
            echo implode("\t", array_keys($row)) . "\n";
            $flag = true;
        }
        // filter data
        array_walk($row, 'filterData');
        echo implode("\t", array_values($row)) . "\n";

    }
    
    exit;
?>