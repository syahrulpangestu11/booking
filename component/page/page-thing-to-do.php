<section class="container content">
    <div class="row" style="padding-top:25px">
    	<div class="col-md-3 col-xs-12">
        	<div class="row">
            	<div class="col-xs-12">
                	<h4>Filter Result</h4>
                    <b>Activities Type</b><br />
                    <div class="checkbox"><label><input type="checkbox">Adventures</label></div>
                    <div class="checkbox"><label><input type="checkbox">Water Activities</label></div>
                    <div class="checkbox"><label><input type="checkbox">Food & Drink</label></div>
                    <div class="checkbox"><label><input type="checkbox">Spa</label></div>
                    <div class="checkbox"><label><input type="checkbox">Attractions</label></div>
                    <div class="checkbox"><label><input type="checkbox">Multi-Day & Extended Tours</label></div>
                    <div class="checkbox"><label><input type="checkbox">Show & Sport Tickets</label></div>
                    <div class="checkbox"><label><input type="checkbox">Sightseeing Passes</label></div>
                    <div class="checkbox"><label><input type="checkbox">Nightlife</label></div>
                    <div class="checkbox"><label><input type="checkbox">Theme parks</label></div>
				</div>
            </div>
        </div>
        <div class="col-md-9 col-xs-12">
        	<div class="row">
            	<div class="col-md-4 col-xs-12 listing-box white-box border-box">
                	<div class="pict thumb"><img src="<?php echo $base_url; ?>/images/bukingo/thing-to-do/bali-bird-park.jpg" /></div>
                    <div class="description">
               			<h3>Bali Bird Park</h3>
                        <span><i class="fa fa-map-marker"></i> Bali, Indonesia</span>
                        <p class="text-justify">Bali Bird Park located at the Gianyar Regency and has an area of 2,000 square metres. The Bird Park houses more than 1.000 birds representing more than 250 species in an enclosed aviary.</p>
                        <div class="row price-box">
                        	<div class="col-xs-6">
                            	<h4 class="price blue">IDR 63.000</h4>
                                <small>per adult</small>
                            </div>
                        	<div class="col-xs-6">
								<button type="button" class="btn btn-warning"><i class="fa fa-heart-o"></i> Book Tickets</button>
                            </div>
                        </div>
                    </div>
                </div>
                
            	<div class="col-md-4 col-xs-12 listing-box white-box border-box">
                	<div class="pict thumb"><img src="<?php echo $base_url; ?>/images/bukingo/thing-to-do/bali-zoo.jpg" /></div>
                    <div class="description">
               			<h3>Bali Zoo</h3>
                        <span><i class="fa fa-map-marker"></i> Bali, Indonesia</span>
                        <p class="text-justify">Large zoo featuring orangutans, elephants & African lions, plus interactive encounters & shows.</p>
                        <div class="row price-box">
                        	<div class="col-xs-6">
                            	<h4 class="price blue">IDR 321.300 </h4>
                                <small>per adult</small>
                            </div>
                        	<div class="col-xs-6">
								<button type="button" class="btn btn-warning"><i class="fa fa-heart-o"></i> Book Tickets</button>
                            </div>
                        </div>
                    </div>
                </div>
                
            	<div class="col-md-4 col-xs-12 listing-box white-box border-box">
                	<div class="pict thumb"><img src="<?php echo $base_url; ?>/images/bukingo/thing-to-do/waterboom.jpg" /></div>
                    <div class="description">
               			<h3>Waterboom Bali</h3>
                        <span><i class="fa fa-map-marker"></i> Bali, Indonesia</span>
                        <p class="text-justify">Lively water park with a rides for all ages, plus restaurants, a food court & a swim-up bar.</p>
                        <div class="row price-box">
                        	<div class="col-xs-6">
                            	<h4 class="price blue">IDR 76.500</h4>
                                <small>per adult</small>
                            </div>
                        	<div class="col-xs-6">
								<button type="button" class="btn btn-warning"><i class="fa fa-heart-o"></i> Book Tickets</button>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>