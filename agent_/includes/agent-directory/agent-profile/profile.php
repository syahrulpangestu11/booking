<?php
    include('function.php');
    include('js.php');

	$agentoid = $_SESSION['_agent_oid'];
	try {
        // ,c.cityname,s.statename,ct.countryname
        // INNER JOIN city c ON a.cityoid = c.cityoid inner join state s using (stateoid) inner join country ct using (countryoid)
        // INNER JOIN published p ON a.publishedoid = p.publishedoid
		$stmt = $db->query("
        select a.agentoid, a.agentname,  a.agentpic, a.address, a.cityoid, a.website, a.email, a.publishedoid, a.mobile, a.phone, a.picture
            ,state.stateoid,state.countryoid
        FROM agent a
        left join city using (cityoid)
        left join state using (stateoid)
		where a.agentoid = '".$agentoid."'");
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
			$r_hotel = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($r_hotel as $row){
				$agentoid = $row['agentoid'];
				$agentpic = $row['agentpic'];
				$agentname = $row['agentname'];
				$address = $row['address'];
				$website = $row['website'];
				$mobile = $row['mobile'];
				$phone = $row['phone'];
				$email = $row['email'];
				$published = $row['publishedoid'];
				$country = $row['countryoid'];
				$state = $row['stateoid'];
				$city = $row['cityoid'];
				$picture = $row['picture'];
			}
		}else{
			echo "No Result";
			die();
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}


?>
<style>
	#data-input .box-input-image img{height: 100%; max-width: 100%;}
	#data-input .box-input-image label{}
	#data-input .box-input-image{
		width: 100%;
		height: 227px;
		line-height: 163px;
		border: 1px solid #ccc;
		margin: 5px 0;
		text-align: center;
		padding: 10px;
		background: #f8f8f8;
	}
</style>

<div id="dialog-error" title="Error Notification">
  <p>We&rsquo;re very sorry, we can't save your data right now.</p><p class="description"></p>
</div>
<div id="dialog-success" title="Confirmation Notice">
  <p>Data succesfully updated</p><p class="description"></p>
</div>

<section class="content-header">
    <h1>
        Agent
    </h1>
</section>
<section class="content">
	<div class="box">
        <div>
        	<form class="" method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url;?>/profile-save">
			<input type="hidden" name="agentoid" value="<?php echo $agentoid; ?>">
            <input type="hidden" name="dfltstate" value="<?php echo $state; ?>">
            <input type="hidden" name="dfltcity" value="<?php echo $city; ?>">
            
            <div class="row">
                    
                <div class="col-md-6">
                    <h3>AGENT PROFILE</h3>
                    
                    <div class="form-group">
                        <label>Agent Pic</label>
                        <input type="text" class="long" name="pic" value="<?php echo $agentpic; ?>" class="form-control">
                    </div>
                    <div class="form-group">
                        <label class="">Agent Name</label>
                        <input type="text" class="long" name="name" value="<?php echo $agentname; ?>" class="form-control">
                    </div>
                    <div class="form-group">
                        <label class="">Website</label>
                        <input type="text" class="long" name="website" value="<?php echo $website; ?>" class="form-control">
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="">Mobile Number</label>
                            <input type="text" class="medium" name="mobile" value="<?php echo $mobile; ?>" class="form-control">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="">Phone Number</label>
                            <input type="text" class="medium" name="phone" value="<?php echo $phone; ?>" class="form-control">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-4">
                            <label>Country</label><br />
                            <select name="country" class="input-select" style="width:100%;">
                                <?php getCountry($country); ?>
                            </select>
                        </div>
                        <div class="form-group col-md-4 loc-state"> 
                            <label>State</label><br />
                            <select name="state" class="input-select" style="width:100%;">
                                <?php getState($state,$country); ?>
                            </select>
                        </div>
                        <div class="form-group col-md-4 loc-city"> 
                            <label>City</label><br />
                            <select name="city" class="input-select" style="width:100%;">
                                <?php getCity($city,$state); ?>
                            </select>
                        </div>

                    </div>
                    <div class="form-group">
                        <label class="">Street Address</label>
                        <textarea name="address" rows="2" class="form-control"><?php echo $address; ?></textarea>
                    </div>
                </div>
                <div class="col-md-6">
                    <h3>&nbsp;</h3>
                    <div class="form-group">
                        <label>Photo</label>
                        <div id="preview_picture" class="box-input-image ">
                            <?php if($picture==''){ ?>
                                            <label>No Picture</label>
                            <?php }else{ ?>
                                            <img src="<?=$picture;?>" />
                            <?php } ?>
                        </div>
                        <input type="hidden" name="oldpicture" value="<?php echo $picture; ?>">
                        <input type="file" class="form-control input-image" name="picture" value="<?php echo $picture; ?>">
                    </div>
                    <div class="form-group">
                        <label class="">Email</label>
                        <input type="text" class="long" name="email" value="<?php echo $email; ?>" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control" name="password">
                    </div>
                </div>
            </div>
            <div class="row">

                <div class="col-md-12">
                    <label class=" float-right" style="vertical-align:middle">&nbsp;</label>
                    <div class="side-right">
                        <div class="form-input">
                            <input type="submit" class="default-button" value="Submit">
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
		</form>
   		</div>
    </div>
</section>

<?php
if(isset($_GET['error'])){
    $error_image = "Please make sure you upload image file type (ex: .jpg , .png , .gif)";
    ?>
    <script>openDialog("error","","<?=($_GET['error']=='image'?$error_image:'');?>"); </script>
<?php } ?>