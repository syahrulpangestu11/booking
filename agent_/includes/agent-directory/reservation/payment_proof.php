<?php
    // include('js.php');

	$bookingnumber = $_REQUEST['no'];
	$agentoid = $_SESSION['_agent_oid'];
	try {
		$stmt = $db->query("select bookingoid FROM booking where bookingnumber = '".$bookingnumber."' limit 1");
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
            $row = $stmt->fetch();
            $bookingoid = $row['bookingoid'];
		}else{
			echo"<script>window.location.href = '".$base_url."/reservation'</script>";
            die();
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}
	try {
		$stmt = $db->query("select * FROM bookingpaymentproof where bookingoid = '".$bookingoid."'");
		$row_count = $stmt->rowCount();
		if($row_count > 0) {
			$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($result as $row){
				$bookingpaymentproofoid = $row['bookingpaymentproofoid'];
				$picture = $row['imgproof'];
				$status = $row['status'];
				$remark = $row['remark'];
				$response_by = $row['response_by'];
				$response_at = $row['response_at'];
				$uploaded_by = $row['uploaded_by'];
				$uploaded_at = $row['uploaded_at'];
			}
		}else{
			// echo "No Result";
            // die();
            $bookingpaymentproofoid = '';
            $status = 'Waiting Confirmation';
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}


?>
<script type="text/javascript">
$(function(){

	$("#dialog-error, #dialog-success").dialog({
      autoOpen: false,
	  buttons: { "Ok": redirect },
	  close: function(event, ui) { redirect }
   });
   
	$(".input-image").change(function() { readURL(this); });
    $('.cancel-button').click(function(e){ e.preventDefault(); $(location).attr("href", "<?php echo $base_url; ?>/reservation/detail/?no=<?=$bookingnumber;?>"); })

});
function redirect(){
    $(location).attr("href", "<?php echo $base_url; ?>/reservation/confirmation/?no=<?=$bookingnumber;?>");
}

function readURL(input) {
	var $sts = true;
	var $box = $('#preview_' + $(input).attr('name'));
	$box.empty();

	//--- validation image extension
	var $ext = input.files[0].name.split('.').pop().toLowerCase();
	if($.inArray($ext, ['gif','png','jpg','jpeg']) == -1) {
		$sts=false;
		$(input).val('');
		$box.append('<label>Uploaded file is not a valid image. Only JPG, PNG and GIF files are allowed.</label>');
		$box.find('label').css('color','red');
	}else{
		$box.find('label').css('color','black');
	}

	//--- validation image size
	if($sts){
		var size = input.files[0].size/1024/1024;
		var n = size.toFixed(2);
		if(size>6){
			$sts = false;
			$(input).val('');
			$box.append('<label>Your file size is: ' + n + 'MB, and it is too large to upload! Maximum : 6MB or less.</label>');
			$box.find('label').css('color','red');
		}else{
			$box.find('label').css('color','black');
		}
	}

	//--- preview image
	if($sts){
	  if (input.files && input.files[0]) {
	    var reader = new FileReader();
	    reader.onload = function(e) { $box.append('<img src="'+e.target.result+'" />') }
	    reader.readAsDataURL(input.files[0]);
	  }
	}
}


function openDialog($type,$message,$warning){
		var $dialog = $("#dialog-"+$type);
		$dialog.find(".description").empty();
		if($message!=undefined){ $dialog.find(".description").text($message); }
		if($warning!=undefined){ console.log($warning); }

		$(function(){ $( document ).ready(function(){ $dialog.dialog("open"); }); });
	}
</script>
<style>
	#data-input .box-input-image img{height: 100%; max-width: 100%;}
	#data-input .box-input-image label{}
	#data-input .box-input-image{
		width: 100%;
		height: 227px;
		line-height: 163px;
		border: 1px solid #ccc;
		margin: 5px 0;
		text-align: center;
		padding: 10px;
		background: #f8f8f8;
	}
</style>

<div id="dialog-error" title="Error Notification">
  <p>We&rsquo;re very sorry, we can't save your data right now.</p><p class="description"></p>
</div>
<div id="dialog-success" title="Confirmation Notice">
  <p>Data succesfully updated</p><p class="description"></p>
</div>

<section class="content-header">
    <h1>
        Agent
    </h1>
</section>
<section class="content">
	<div class="box">
        <div>
        	<form class="" method="post" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url;?>/reservation/confirmation-save">
			<input type="hidden" name="bookingoid" value="<?php echo $bookingoid; ?>">
			<input type="hidden" name="bookingnumber" value="<?php echo $bookingnumber; ?>">
			<input type="hidden" name="bookingpaymentproofoid" value="<?php echo $bookingpaymentproofoid; ?>">
            
            <div class="row">
                    
                <div class="col-md-6">
                    <h3>PAYMENT CONFIRMATION</h3>
                    
                    <div class="form-group">
                        <!-- <label>Photo Proof</label> -->
                        <div id="preview_picture" class="box-input-image ">
                            <?php if($picture==''){ ?>
                                            <label>No Picture</label>
                            <?php }else{ ?>
                                            <img src="<?=$picture;?>" />
                            <?php } ?>
                        </div>
                        <input type="hidden" name="oldpicture" value="<?php echo $picture; ?>">
                        <input type="file" class="form-control input-image" name="picture" value="<?php echo $picture; ?>" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <h3>&nbsp;</h3>
                    <div class="form-group">
                        <label class="">Booking Number</label>
                        <p style="text-transform:uppercase;"><?php echo $bookingnumber; ?></p>
                    </div>
                    <div class="form-group">
                        <label class="">Status</label>
                        <p style="text-transform:uppercase;"><?php echo $status; ?></p>
                    </div>
                    <?php if(!empty($remark)){
                        ?>
                        <div class="form-group">
                            <label class="">Remark by Admin</label>
                            <p><?php echo $remark; ?></p>
                        </div>
                        <?php
                    } 
                    ?>
                </div>
            </div>
            <div class="row">

                <div class="col-md-12">
                    <label class=" float-right" style="vertical-align:middle">&nbsp;</label>
                    <div class="side-right">
                        <div class="form-input">
							<?php if($status!='approve'){ ?>
                            	<input type="submit" class="default-button" value="<?php echo (empty($bookingpaymentproofoid)?"Submit":"Change"); ?>">
							<?php } ?>
                            <button class="cancel-button small-button red">Cancel</button>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
		</form>
   		</div>
    </div>
</section>

<?php
if(isset($_GET['error'])){
    $error_image = "Please make sure you upload image file type (ex: .jpg , .png , .gif)";
    ?>
    <script>openDialog("error","","<?=($_GET['error']=='image'?$error_image:'');?>"); </script>
<?php } ?>