<link rel="stylesheet" href="<?php echo $base_url; ?>/js/datepicker/themes/base/jquery.ui.all.css">
<script type="text/javascript" src="<?php echo $base_url; ?>/js/datepicker/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>/js/datepicker/ui/jquery.ui.widget.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>/js/datepicker/ui/jquery.ui.datepicker.js"></script>   
<script type="text/javascript">
$(function() {
	var dates = $("#startdate, #enddate").datepicker({
		
		defaultDate: "+0", changeMonth: true, numberOfMonths: 1, changeYear: true,
		onSelect: function( selectedDate ) {
			var option = this.id == "startdate" ? "minDate" : "maxDate",
				instance = $( this ).data( "datepicker" );
				date = $.datepicker.parseDate( instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings ); dates.not( this ).datepicker( "option", option, date );;
			if($("#startdate").val() == $("#enddate").val()) { //if value from #from same with #to or value #from more than value of #to
				var x = $("#startdate").val(); var temp = new Array(); temp = x.split(" "); var d = parseInt(temp[0]); var m = temp[1]; var y = parseInt(temp[2]);
				switch(m) {
					case "January"	: if(d==31) { d = 1; m = "February"; }else { d = d+1; } break;
					case "February"	: if(y%4 == 0) { if(d==29) { d = 1; m = "March"; }else { d = d+1; } }else { if(d==28) { d = 1; m = "March"; }else { d = d+1; } } break;
					case "March"	: if(d==31) { d = 1; m = "April"; }else { d = d+1; } break;
					case "April"	: if(d==30) { d = 1; m = "May"; }else { d = d+1; } break;
					case "May"		: if(d==31) { d = 1; m = "June"; }else { d = d+1; } break;
					case "June"		: if(d==30) { d = 1; m = "July"; }else { d = d+1; } break;
					case "July"		: if(d==31) { d = 1; m = "August"; }else { d = d+1; }break;
					case "August"	: if(d==31) { d = 1; m = "September"; }else { d = d+1; } break;
					case "September": if(d==30) { d = 1; m = "October"; }else { d = d+1; } break;
					case "October"	: if(d==31) { d = 1; m = "November"; }else { d = d+1; } break;
					case "November"	: if(d==30) { d = 1; m = "December"; }else { d = d+1; } break;
					case "December"	: if(d==31) { d = 1; m = "January"; y = y + 1; }else { d = d+1;} break;
				}$("#enddate").val(d+" "+m+" "+y);
			} 
		} 
	}); 
	
});
</script>