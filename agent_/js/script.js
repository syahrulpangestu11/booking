(function($) {
    "use strict";

    //*/
    $.fn.tree = function() {

        return this.each(function() {
            var btn = $(this).children("a").first();
            var menu = $(this).children(".treeview-menu").first();
            var isActive = $(this).hasClass('active');

            //initialize already active menus
            if (isActive) {
                // menu.show();
                btn.children(".fa-angle-left").first().removeClass("fa-angle-left").addClass("fa-angle-down");
            }
            //Slide open or close the menu on link click
            btn.click(function(e) {
                e.preventDefault();
                if (isActive) {
                    //Slide up to close menu
                    menu.slideUp();
                    isActive = false;
                    btn.children(".fa-angle-down").first().removeClass("fa-angle-down").addClass("fa-angle-left");
                    btn.parent("li").removeClass("active");
                } else {
                    //Slide down to open menu
                    menu.slideDown();
                    isActive = true;
                    btn.children(".fa-angle-left").first().removeClass("fa-angle-left").addClass("fa-angle-down");
                    btn.parent("li").addClass("active");
                }
            });

            // // Add margins to submenu elements to give it a tree look
            // menu.find("li > a").each(function() {
            //     var pad = parseInt($(this).css("margin-left")) - 5;

            //     $(this).css({"margin-left": pad + "px"});
            // });
        });

    };

    $.fn.treechild = function() {

        return this.each(function() {
            var btn = $(this).children("a").first();
            var menu = $(this).children(".treeview-menu-child").first();
            var isActive = $(this).hasClass('active');

            //initialize already active menus
            if (isActive) {
                menu.show();
                btn.children(".fa-angle-right").first().removeClass("fa-angle-right").addClass("fa-angle-down");
            }
            //Slide open or close the menu on link click
            btn.click(function(e) {
                e.preventDefault();
                if (isActive) {
                    //Slide up to close menu
                    menu.slideUp();
                    isActive = false;
                    btn.children(".fa-angle-down").first().removeClass("fa-angle-down").addClass("fa-angle-right");
                    btn.parent("li").removeClass("active");
                } else {
                    //Slide down to open menu
                    menu.slideDown();
                    isActive = true;
                    btn.children(".fa-angle-right").first().removeClass("fa-angle-right").addClass("fa-angle-down");
                    btn.parent("li").addClass("active");
                }
            });

            // // Add margins to submenu elements to give it a tree look
            // menu.find("li > a").each(function() {
            //     var pad = parseInt($(this).css("margin-left")) + 5;

            //     $(this).css({"margin-left": pad + "px"});
            // });
        });

    };
    //*/


}(jQuery));

$(function(){
    // // Di Disable karena tidak pakai tree
    // /* Sidebar tree view */
    // $(".sidebar .treeview").tree();
	// $(".sidebar .treeview-child").treechild();

    function _fix() {
        //Get window height and the wrapper height
        var height = $(window).height() - $("body > #xheader").height() - ($("body > .footer").outerHeight() || 0) - 3;
        $(".wrapper").css("min-height", height + "px");
        var content = $(".wrapper").height();
        //If the wrapper height is greater than the window
        if (content > height)
        //then set sidebar height to the wrapper
            $(".menu-side, html, body, .content-side").css("min-height", content + "px");
        else {
            //Otherwise, set the sidebar to the height of the window
            $(".menu-side, html, body, .content-side").css("min-height", height + "px");
        }
    }
    //Fire upon load
    _fix();
    //Fire when wrapper is resized
    $(window).resize(function() {
        _fix();
    });

    $(".dropdown-toggle").on("click", function(e){
        e.stopPropagation();
        $(this).parent().children(".dropdown-menu").slideToggle();
    });

    $(".dropdown-menu").click(function(e){
        e.stopPropagation();
    });

    $("body").click(function(){
        $(".dropdown-menu").slideUp();
    });

    $( "#startdate" ).datepicker({
		defaultDate: "+1w",
		changeMonth: true, changeYear:true,
		onClose: function( selectedDate ) {
            $( "#enddate" ).datepicker( "option", "minDate", selectedDate );
		}
    });
    $( "#enddate" ).datepicker({
      	defaultDate: "+1w",
      	changeMonth: true, changeYear:true,
     	onClose: function( selectedDate ) {
        	$( "#startdate" ).datepicker( "option", "maxDate", selectedDate );
      	}
    });


    $( "#startdate-2w" ).datepicker({
		defaultDate: "+1w",
		changeMonth: true, changeYear:true,
		onClose: function( selectedDate ) {
            $( "#enddate-2w" ).datepicker( "option", "minDate", selectedDate );

            var endDate = new Date(selectedDate);
            endDate.setDate(endDate.getDate()+14);
            $( "#enddate-2w" ).datepicker( "option", "maxDate", endDate );
		}
    });
    $("#enddate-2w").datepicker({
      	defaultDate: "+1w",
      	changeMonth: true, changeYear:true,
     	onClose: function( selectedDate ) {
        	$( "#startdate-2w" ).datepicker( "option", "maxDate", selectedDate );
      	}
    });

    $( "#startcalendar" ).datepicker({
		defaultDate: "+1w",
		changeMonth: true, changeYear:true,
		onClose: function( selectedDate ) {
			$( "#endcalendar" ).datepicker( "option", "minDate", selectedDate );
		}
    });
    $( "#endcalendar" ).datepicker({
      	defaultDate: "+1w",
      	changeMonth: true, changeYear:true,
     	onClose: function( selectedDate ) {
        	$( "#startcalendar" ).datepicker( "option", "maxDate", selectedDate );
      	}
    });

    $( "#startdate-today" ).datepicker({
		defaultDate: "+1w",
		changeMonth: true, changeYear:true, minDate:0,
		onClose: function( selectedDate ) {
            $( "#enddate-today" ).datepicker( "option", "minDate", selectedDate );
		}
    });
    $( "#enddate-today" ).datepicker({
      	defaultDate: "+1w",
      	changeMonth: true, changeYear:true, minDate:0,
     	onClose: function( selectedDate ) {
        	$( "#startdate-today" ).datepicker( "option", "maxDate", selectedDate );
      	}
    });

    $( "#startdate-2w-today" ).datepicker({
		defaultDate: "+1w",
		changeMonth: true, changeYear:true, minDate:0,
		onClose: function( selectedDate ) {
            $( "#enddate-2w-today" ).datepicker( "option", "minDate", selectedDate );

            var endDate = new Date(selectedDate);
            endDate.setDate(endDate.getDate()+14);
            $( "#enddate-2w-today" ).datepicker( "option", "maxDate", endDate );
		}
    });
    $("#enddate-2w-today").datepicker({
      	defaultDate: "+1w",
      	changeMonth: true, changeYear:true, minDate:0, maxDate: "+2w",
     	onClose: function( selectedDate ) {
        	$( "#startdate-2w-today" ).datepicker( "option", "maxDate", selectedDate );
      	}
    });

    $( "#startdate-1m-today" ).datepicker({
		defaultDate: "+1w",
		changeMonth: true, changeYear:true, minDate:0,
		onClose: function( selectedDate ) {
            $( "#enddate-1m-today" ).datepicker( "option", "minDate", selectedDate );

            var endDate = new Date(selectedDate);
            endDate.setDate(endDate.getDate()+31);
            $( "#enddate-1m-today" ).datepicker( "option", "maxDate", endDate );
		}
    });
    $("#enddate-1m-today").datepicker({
      	defaultDate: "+1w",
      	changeMonth: true, changeYear:true, minDate:0, maxDate: "+1m",
     	onClose: function( selectedDate ) {
        	$( "#startdate-1m-today" ).datepicker( "option", "maxDate", selectedDate );
      	}
    });


    $('li.treeview').each(function(){
        var thisMenu = $(this);
        var thisChildMenu = thisMenu.find('ul.treeview-menu');
        var thisChildMenuHTML = thisChildMenu.html();
        thisChildMenu.addClass('floating');

        if(thisMenu.hasClass('active') == true){
            $('#child-menu').html(thisChildMenuHTML);
            $('#child-menu').show();
            $('#child-menu').css('visibility', 'visible');
        }

        // $(thisMenu).click(function(){
        //     $('#child-menu').html(thisChildMenuHTML);
        //     $('#child-menu').show();
        //     $('#child-menu').css('visibility', 'visible');
        // });

        $(thisMenu).hover(function(){
            thisChildMenu.show();
        },function(){
            thisChildMenu.hide();
        });
    });

    // $(document).ready(function(){
    //     $('li.treeview.active').mouseover();
    // });

    $body = $("body");

    $(document).on({
        ajaxStart: function() { $body.addClass("loading");    },
         ajaxStop: function() { $body.removeClass("loading"); }
    });

});
