<?php
include("../conf/connection.php");
// include("baseurl.php");
?>
  $(document).ready(function(){

    <?php
    function storeToArray($s_ac){
      $q_ac = mysqli_query($conn, $s_ac) or die(mysqli_error());
      $x = 0;
      while($r_ac = mysqli_fetch_array($q_ac)){
        $name = $r_ac['name'];
        $jmlhotel = $r_ac['jmlhotel'];
        $note = ($jmlhotel > 1) ? " hotels" : " hotel";
        $json_template = '
          {
            "name": "'.$name.'",
              "jmlhotel": "'.$jmlhotel.'",
              "value": "'.$name.'",
              "note": "'.$note.'"
          }

        ';

        if($x == 0){
          $x = 1;
          $suggestion = $json_template;
        }else{
          $suggestion = ", ".$json_template;
        }
        echo $suggestion;
      }
    }

    function typeaheadOptions($jenis){

    }
    ?>

    var arr_continent = [
        <?php
      $s_ac = "
        SELECT continentname AS name, COUNT(hoteloid) AS jmlhotel FROM hotel
        RIGHT JOIN city USING (cityoid) RIGHT JOIN state USING (stateoid)
        RIGHT JOIN country USING (countryoid) RIGHT JOIN continent USING (continentoid)
        GROUP BY continentoid
      ";
      storeToArray($s_ac);
      ?>
    ];


    var arr_country = [
        <?php
      $s_ac = "
        SELECT countryname AS name, COUNT(hoteloid) AS jmlhotel FROM hotel
        RIGHT JOIN city USING (cityoid) RIGHT JOIN state USING (stateoid)
        RIGHT JOIN country USING (countryoid)
        GROUP BY countryoid
      ";
      storeToArray($s_ac);
      ?>
    ];

    var arr_state = [
        <?php
      $s_ac = "
        SELECT statename AS name, COUNT(hoteloid) AS jmlhotel FROM hotel
        RIGHT JOIN city USING (cityoid) RIGHT JOIN state USING (stateoid)
        GROUP BY stateoid
      ";
      storeToArray($s_ac);
      ?>
    ];

    var arr_city = [
        <?php
      $s_ac = "
        SELECT cityname AS name, COUNT(hoteloid) AS jmlhotel FROM hotel
        RIGHT JOIN city USING (cityoid)
        GROUP BY cityoid
      ";
      storeToArray($s_ac);
      ?>
    ];

    var arr_hotel = [
        <?php
      $s_ac = "
        SELECT hotelname AS name, stars AS jmlhotel FROM hotel
        GROUP BY hoteloid LIMIT 5
      ";
      storeToArray($s_ac);
      ?>
    ];


    $('#q-hotel').typeahead([
      <?php
      $array_jenis = array('continent','country','state','city','hotel');
      $x=0;
      foreach ($array_jenis as $key => $value) {

        if(substr($value, -1,1) == "y"){
          $append = "ies";
          $header = substr($value, 0, strlen($value)-1).$append;
        }else{
          $append = "s";
          $header = $value.$append;
        }

        if($value == "hotel"){
          $typeahead = "{
              name: '".$value."',
              header: '<div class=\"header capitalize\"><i>".$header."</i></div>',
              local: arr_".$value.",
              limit: 5,
            template: [
            '<div class=\"jmlhotel jmlstar fl_right title-blue\" jml=\"{{jmlhotel}}\"></div>',
            '<div class=\"\">{{name}}</div>'
            ].join(''),
            engine: Hogan
          }";
        }else{
          $typeahead = "{
              name: '".$value."',
              header: '<div class=\"header capitalize\"><i>".$header."</i></div>',
              local: arr_".$value.",
              limit: 5,
            template: [
            '<div class=\"jmlhotel fl_right title-blue\"><b>{{jmlhotel}}</b> {{note}}</div>',
            '<div class=\"\">{{name}}</div>'
            ].join(''),
            engine: Hogan
          }";
        }

        if($x == 0){
          $x = 1;
          $typeahead_options = $typeahead;
        }else{
          $typeahead_options = ", ".$typeahead;
        }
        echo $typeahead_options;
      }
      ?>
    ]);

    $("#q-hotel").keyup(function(){
      $(".jmlstar").each(function(){
        var elem = $(this);
        var star = elem.attr("jml");
        var x = "";
        for(var a=0; a<star; a++){
          x = x + "<img src='<?php echo $base_url;?>/images/star-on.png'>";
        }
        elem.html(x);
      });
    });


    $('#q-tour').typeahead([
      <?php
      $array_jenis = array('continent','country','state','city');
      $x=0;
      foreach ($array_jenis as $key => $value) {
        //mengubah ke bentuk jamak , misal: country->countries
        if(substr($value, -1,1) == "y"){
          $append = "ies";
          $header = substr($value, 0, strlen($value)-1).$append;
        }else{
          $append = "s";
          $header = $value.$append;
        }

        $typeahead = "{
            name: '".$value."',
            header: '<div class=\"header capitalize\"><i>".$header."</i></div>',
            local: arr_".$value.",
            limit: 5,
          template: [
          '<div class=\"jmlhotel fl_right title-blue\"><b>{{jmlhotel}}</b> {{note}}</div>',
          '<div class=\"\">{{name}}</div>'
          ].join(''),
          engine: Hogan
        }";

        if($x == 0){
          $x = 1;
          $typeahead_options = $typeahead;
        }else{
          $typeahead_options = ", ".$typeahead;
        }
        echo $typeahead_options;
      }
      ?>
    ]);

  });
