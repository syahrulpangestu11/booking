<?php error_reporting(0);
include("../conf/connection.php");
// include("baseurl.php");
?>
<?php
  include("../includes/parameter.php");
	$gethotel = 0;
  $page = "search-hotel";
  $jsondata = array();
	include("../includes/list.hotel.query.php");
	while($hotel = mysqli_fetch_array($q_hotel)){

		$hoteloid = $hotel['hoteloid'];
		$hotelname = $hotel['hotelname'];
		$star = $hotel['stars'];
		$address = $hotel['address'];
		$city = $hotel['cityname'];
		$state = $hotel['statename'];
		$country = $hotel['countryname'];
		$continent = $hotel['continentname'];
		$hoteltype = $hotel['category'];

		/*------------------------- Hotel Picture -------------------------*/
		$s_hotelphoto = "SELECT * FROM hotelphoto hp WHERE hoteloid = '$hoteloid' AND hp.flag = 'main' AND hp.ref_table = 'hotel'";
		$q_hotelphoto = mysqli_query($conn, $s_hotelphoto) or die(mysqli_error());
		$r_hotelphoto = mysqli_fetch_array($q_hotelphoto);
		$hotelpict = $r_hotelphoto['photourl'];

		include('../includes/list.hotel.rate.promotion.php');

		if($page == "search-hotel" and $minrate > 0){
      $itemhotel = array();
      $itemhotel['oid'] = $ec_hoteloid;
      $itemhotel['href'] = $href_hotel;
      $itemhotel['pict'] = $hotelpict;
      $itemhotel['name'] = $hotelname;
      $itemhotel['star'] = $star;
      $itemhotel['starimg'] = $base_url."/images/star-on.png";
      $itemhotel['address'] = $address;
      $itemhotel['city'] = $city;
      $itemhotel['type'] = $hoteltype;
      $itemhotel['headline'] = $hotel_headline;
      if($ratenet > $minrate){ $itemhotel['netrate'] = $shownetrate;  }else{ $itemhotel['netrate'] = ""; }
      $itemhotel['currency'] = $currencycode;
      $itemhotel['minrate'] = $hotel_minrate_final;
      $itemhotel['promo'] = array(); $k=0;
      foreach($promoname as $key => $name){
        $itemhotel['promo'][$k]['price'] = $price[$key];
        $itemhotel['promo'][$k]['name'] = $name;
        $k++;
      }
      array_push($jsondata, $itemhotel);
		}
	}
  if($gethotel == 0){
    //echo "empty data";
  }

  echo json_encode($jsondata);
?>
