<?php error_reporting(0);
include("../conf/connection.php");
// include("baseurl.php");
?>
<?php
$jsondata = array();
$hoteloid = substr($_REQUEST['hid'], 4, strlen($_REQUEST['hid'])-6);
$sql_h = "SELECT h.*, cityname, statename, countryname, continentname, ht.category FROM hotel h inner join hoteltype ht using (hoteltypeoid) inner join city using (cityoid) inner join state using (stateoid) INNER JOIN country USING (countryoid) INNER JOIN continent USING (continentoid) WHERE  hoteloid = '".$hoteloid."' group by hoteloid";
//$sql_h = "SELECT * FROM hotel h INNER JOIN hotelphoto hp USING (hoteloid) WHERE h.hoteloid = '".$hoteloid."' AND hp.flag = 'main'";
$run_h = mysqli_query($conn, $sql_h);
$row_h = mysqli_fetch_array($run_h);
	$hotel_name = $row_h['hotelname'];
	$hotel_address = $row_h['address'];
	$hotel_stars = $row_h['stars'];
	$hotel_totalrom = $row_h['totalroom'];
	$hotel_website = $row_h['website'];
	$hotel_area = $row_h['cityname'];
	$hotel_description = $row_h['description'];

	//Hotel Photo Main
	$s_hotelphoto = "SELECT * FROM hotelphoto hp LEFT JOIN hotelphotocontent hpc USING (hotelphotooid) WHERE hp.hoteloid = '".$hoteloid."' AND hp.flag = 'main' AND ref_table = 'hotel'";
	$q_hotelphoto = mysqli_query($conn, $s_hotelphoto);
	$r_hotelphoto = mysqli_fetch_array($q_hotelphoto);
	$hotel_pict = $r_hotelphoto['photourl'];

  $hotel = array();
  $hotel['name'] = $hotel_name;
  $hotel['address'] = $hotel_address;
  $hotel['stars'] = $hotel_stars;
  $hotel['totalrom'] = $hotel_totalrom;
  $hotel['website'] = $hotel_website;
  $hotel['area'] = $hotel_area;
  $hotel['description'] = $hotel_description;
  $hotel['pict'] = $hotel_pict;
  $hotel['starimg'] = $base_url."/images/star-on.png";

  $hotel['amenities'] = array(); $k=0;
  $s_amenities_cat = "SELECT ft.* FROM facilitiestype ft
            INNER JOIN facilities f USING (facilitiestypeoid)
            WHERE facilitiesoid IN (SELECT facilitiesoid FROM hotelfacilities hf INNER JOIN hotel h USING (hoteloid) WHERE h.hoteloid = '".$hoteloid."')
            GROUP BY facilitiestypeoid";
  $q_amenities_cat = mysqli_query($conn, $s_amenities_cat) or die(mysqli_error());
  while($r_amenities_cat = mysqli_fetch_array($q_amenities_cat)){
    $amenities_categoryoid = $r_amenities_cat['facilitiestypeoid'];
    $amenities_category = $r_amenities_cat['name'];

    $hotel['amenities'][$k]['category'] = $amenities_category;

    $s_amenities = "SELECT f.facilitiesoid, f.name , COUNT(hoteloid) AS jmlhotel
            FROM facilities f
            INNER JOIN facilitiestype ft USING (facilitiestypeoid)
            LEFT JOIN (
              SELECT hoteloid, facilitiesoid FROM hotel h INNER JOIN hotelfacilities hf USING (hoteloid)
              WHERE h.hoteloid = '".$hoteloid."'
            ) TEMP_TABLE
            USING (facilitiesoid)
            WHERE ft.facilitiestypeoid = '".$amenities_categoryoid."'
            GROUP BY facilitiesoid ORDER BY name ASC
            ";

    $q_amenities = mysqli_query($conn, $s_amenities) or die(mysqli_error());
    $n_amenities = mysqli_num_rows($q_amenities);

    $hotel['amenities'][$k]['items'] = array(); $l=0;
    while($r_amenities = mysqli_fetch_array($q_amenities)){
      $amenities_oid = $r_amenities['amenitiesoid'];
      $amenities_name = $r_amenities['name'];
      $have_amenities = ( $r_amenities['jmlhotel'] == 1 ) ? "checked" : "unchecked" ;

      $hotel['amenities'][$k]['items'][$l]["name"] = $amenities_name;
      $hotel['amenities'][$k]['items'][$l]["have"] = $have_amenities;
      $l++;
    }
    $k++;
  }
  array_push($jsondata, $hotel);

  echo json_encode($jsondata);
?>
