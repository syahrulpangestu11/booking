<style type="text/css">
.unique_total { border: 1px solid #c2d8f1; background: #f2f7fc; margin: 20px 0; padding: 15px 20px; color: #36516f; }
.unique_total .r { float: right; vertical-align: top;display: inline-block; }
.confirm { background-color: #f2fbe5; border: 1px solid #c7e4a0; padding: 20px; -moz-border-radius: 5px; -webkit-border-radius: 5px; -khtml-border-radius: 5px; border-radius: 5px; margin: 10px 0; color: #333; }
</style>

<?php
	$rpgrandtotal = number_format($_POST['grandtotal']);
	$showcurrency	 = mysqli_fetch_array(mysqli_query($conn, "select currencycode from currency where currencyoid='".$_POST['currencyoid']."'"));
	$currencycode	= $showcurrency['currencycode'];
	$get_sub = substr($_POST['bookingnumber'], 4,11);
?>
<div class="white-box border-box top-right-box">
	<h2><span class="grey">Congratulations! </span><span class="blue">Your Booking is Successful!</span></h2>
	<div class="confirm">
		<h3>Please transfer the payment to make a booking. You have <strong>3 hours</strong> to complete the payment.</h3>
	</div>
	
	<div class="unique_total">
	       <strong>Order Id</strong>  <strong class="r"><?php echo $get_sub; ?> (last digit of Virtual Account Number)</strong>
	            <hr> 
	        <strong>Virtual Account Number</strong>  <strong class="r">1234 xxxxx  <?php echo $get_sub; ?></strong>
	            <hr> 
	        <strong>Final Total </strong> <strong class="r"><?php echo $currencycode." ".$rpgrandtotal; ?></strong>
	</div>

       <h2><span class="grey">Transfer </span><span class="blue">Payment</span></h2>
        <ol style="padding-left:25px;">      
            <li><strong><div style="color: red;">Only ATM transfer via ATM Bersama network or ATM Prima network can be processed by this method of payment. For Internet Banking/Mobile Banking/Teller Transfer/Non ATM, please use other methods of payment available</div></strong></li>
            <li><strong>10 last Digit from Virtual Account Number (VA number) is your transaction id</strong></li>
            <li>There will be additional charge IDR 5,000/transaction (not included in total transaction) for payments through ATM Bersama's, Prima's or Alto's networks in accordance with each network's regulation. Payments through Permata Bank's ATMs is free of charge</li>
            <li>For Mandiri ATM users, the minimum transaction is IDR 50,000</li>
        </ol>
</div>

<div class="white-box border-box" id="transfer-policies">
       <h2><span class="grey">Payment through ATM BCA/other banks </span><span class="blue">within PRIMA network </span></h2>
        <ol style="padding-left:25px;">      
            <li>Enter your PIN</li>
            <li>Select 'Other Transactions'</li>
            <li>Select 'Transfer'</li>
            <li>Select 'To Other Bank's Account'</li>
            <li>Enter Permata Bank's code (013) and select 'Yes'</li>
            <li>Enter the exact amount of money stated in the itinerary. Important: Inexact amount will lead to failed transaction.</li>
            <li>Enter Virtual Account Number (please see your virtual account number above)</li> 
            <li>Transfer Confirmation screen that displayed Permata Bank's account number, passenger's name and the amount to be paid will appear. If everything is correct, select 'Yes'.</li>
            <li>Done</li>
        </ol>
</div>

<div class="white-box border-box" id="transfer-policies">
       <h2><span class="grey">Payment through Mandiri Bank's/other banks' ATMs </span><span class="blue">within ATM Bersama network </span></h2>
        <ol style="padding-left:25px;">      
            <li>Select language</li>
            <li>Enter your PIN</li>
            <li>Select 'Other Transactions'</li>
            <li>Select 'Transfer'</li>
            <li>Select 'To other ATM Bersama/Link Other Bank's Account'</li>
            <li>Enter bank code (013) + Virtual Account Number (please see your virtual account number above)</li> 
            <li>Enter the exact amount of money stated in the itinerary. Important: Inexact amount will lead to failed transaction.</li>
            <li>Let the transfer reference number blank and then select 'Yes'.</li>
            <li>Transfer Confirmation screen that displayed Permata Bank's account number, passenger's name and the amount to be paid will appear. If everything is correct, select 'Yes'.</li>
            <li>Done</li>
        </ol>
</div>

<div class="white-box border-box" id="transfer-policies">
       <h2><span class="grey">Payment through </span><span class="blue">Permata Bank's ATMs</span></h2>
        <ol style="padding-left:25px;">      
            <li>Select language</li>
            <li>Enter your PIN</li>
            <li>Select 'Other Transactions'</li>
            <li>Select 'Payment Transaction'</li>
            <li>Select 'Others'</li>
            <li>Select 'Virtual Account Payment'</li>
            <li>Enter Virtual Account Number (please see your virtual account number above)</li> 
            <li>Transfer Confirmation screen that displayed Virtual Account Number and passenger's name. If everything is correct, select 'Yes'.</li>
            <li>Select your account.</li>
            <li>Done</li>
        </ol>
</div>