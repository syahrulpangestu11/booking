<?php 
//Saving the guest detail first
$_SESSION['title']=$_POST['title'];				
$_SESSION['firstname']=$_POST['firstname'];					
$_SESSION['lastname']=$_POST['lastname'];
$_SESSION['email']=$_POST['email'];			
$_SESSION['mobilephone']=$_POST['mobilephone'];	
$_SESSION['phone']=$_POST['phone'];
$_SESSION['address']=$_POST['address'];	
$_SESSION['city']=$_POST['city'];										
$_SESSION['state']=$_POST['state'];				
$_SESSION['zipcode']=$_POST['zipcode'];	
$_SESSION['countryoid']=$_POST['countryoid'];

$_SESSION['arrivaldate']=$_POST['arrivaldate'];										
$_SESSION['arrivaltime']=$_POST['arrivaltime'];				
$_SESSION['note']=$_POST['note'];	

$hoteloid = substr($_POST['hoteloid'], 4, strlen($_POST['hoteloid'])-6);

$_SESSION['booking_inquiry']=$_POST['inquiry'];	
$_SESSION['booking_hoteloid']=$hoteloid;	

// if(empty($_SESSION['email'])){$_SESSION['email']=$_POST['agent_email']; }
// if(empty($_SESSION['phone'])){$_SESSION['phone']=$_POST['agent_phone'];	}

//include("save.data-guest.php"); 

// echo "<script>console.log(".json_encode($_SESSION).");</script>";

include("js_validate.php"); 
			

$s_hr = "SELECT * FROM hotel h INNER JOIN room r USING (hoteloid) INNER JOIN city USING (cityoid) INNER JOIN paymentmethod using(paymentmethodoid) 
            WHERE h.hoteloid = '$hoteloid'";
$q_hr = mysqli_query($conn, $s_hr) or die(mysqli_error());
$r_hr = mysqli_fetch_array($q_hr);
	$hotel_name = $r_hr['hotelname'];
	$hotel_address = $r_hr['address'];
	$hotel_area = $r_hr['cityname']; 
	$room_name = $r_hr['name'];
	$room_pict = $r_hr['roompict'];
	$paymentmethodoid = $r_hr['paymentmethodoid'];
	$paymentmethod = $r_hr['method'];

	// Hotel Pict
    // $s_hotelphoto = "SELECT * FROM hotelphoto hp WHERE hoteloid = '$hoteloid' AND hp.flag = 'main'";
    $s_hotelphoto = "SELECT '-' AS hotelphotooid, hoteloid, banner AS photourl, banner AS thumbnailurl, 
	'main' AS flag, 'n' AS flag_flexible_rate, 'hotel-banner' AS ref_table, hoteloid AS ref_id 
	FROM hotel
	where hoteloid = '".$hoteloid."'";
	$q_hotelphoto = mysqli_query($conn, $s_hotelphoto) or die(mysqli_error());
	$r_hotelphoto = mysqli_fetch_array($q_hotelphoto);
    $hotel_pict = $r_hotelphoto['photourl'];
    



    if($_SESSION['booking_inquiry']=='y'){
        ?>
        <form id="paymentform" action="<?=$base_url;?>/book/process" method="post">
            <input type="hidden" name="paymentmethodoid" value="3">  
            <input type="hidden" name="payment_method" value="skippayment">  
        </form>
        <script>document.forms["paymentform"].submit();</script>
        <?php

    }
?>
<style>
	/*.border-right {margin-right: 20px; padding-right: 20px;}*/
	
	#hotel-info {padding: 10px;}
	#hotel-pict {width: 100%;}
	
	.room-pict {width: 70px; height: 70px;}
	.room-detail {margin: 0 0 0 0px;}
	
	/**********************/
	
	#payment-form #payment-detail > div > .inline-block > div ,
	#payment-form #payment-detail > div > .inline-block > .table > .table-row > .table-cell {padding-bottom:5px;}
	#payment-form #payment-detail > div > .inline-block > .table > .table-row > .table-cell:nth-child(2) {padding: 0 10px;}
	
	#address_cc, #note {height: 60px; width: 180px;}
</style>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<form id="payment-form" action="<?=$base_url;?>/book/process" method="post">
<input type="hidden" name="paymentmethodoid" value="<?php echo $paymentmethodoid; ?>">
	
	<div class="clear">
		<div id="pgsch" class="container">	
            <div id="sidebar">
                <div class="white-box border-box card-rounded" style="padding: 0;">
                    <img src="<?=$hotel_pict;?>" id="hotel-pict" />
                    <div id="hotel-info">
                        <h3 class="default"><span class="maroon"><?=$hotel_name;?></span></h3>
                        <i class="small-desc"><?=$hotel_address;?></i>
                    </div>
                </div>
            </div>
            
            <div id="right-content">
                <div class="white-box border-box top-right-box card-rounded">
                    <h2><span class="grey">Reservation</span> <span class="blue">Summary</span></h2>
                    <?php include('page.guest-detail.summary.php'); ?>
                    <div class="clear"></div>
                </div>
                
                <div class="white-box border-box card-rounded">
                    Payment Method :
                    <?php
                    
                    $q_method = mysqli_query($conn, "SELECT * FROM paymentmethod where paymentmethodoid<>'2' and publishedoid='1'") or die(mysqli_error());
                    while($method = mysqli_fetch_array($q_method)){ 
                        $value = preg_replace('/\s/', '',preg_replace('/\(.*\)/','',strtolower($method['method'])));
                        $name = $method['method'];
                        $id = $method['paymentmethodoid'];
                     ?>
                     
                        <div class="inline-block" style="margin-left: 25px;">
                            <input type="radio" data-oid="<?=$id;?>" value="<?=$value;?>" name="payment_method" class="payment-method" id="method-<?=$value;?>" 
                                    style="vertical-align: top;margin-right: 2px;" />
                            <label for="method-<?=$value;?>"><?=$name;?></label>
                        </div>
                     
                     <?php   
                    }
                    ?>
                    <div class="clear"></div>
                </div>
                
                
                <div id="payment-detail" class="white-box border-box card-rounded">
                    
                    <div id="container-method-creditcard" class="container-method" style="display:none;">
                        <div class="col-md-6 col-sm-6 col-xs-12 top border-right">
                            <h2><span class="grey">Card</span> <span class="blue">Detail</span></h2>        
                            <div class="table">
                                <div class="table-row">
                                    <div class="table-cell">Card Type</div>
                                    <div class="table-cell"> : </div>
                                    <div class="table-cell">
                                        <select class="dropdown comp-creditcard" name="cardtype" id="cardtype" >
                                            <option value="Visa" selected="">Visa</option>
                                            <option value="MasterCard">Master Card</option>
                                            <option value="AmEx">American Express</option>
                                            <option value="Discover">Discover</option>
                                            <option value="JCB">JCB</option>
                                            <option value="Maestro">Maestro</option>
                                            <option value="VisaElectron">Visa Electron</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="table-row">
                                    <div class="table-cell">Card Number</div>
                                    <div class="table-cell"> : </div>
                                    <div class="table-cell"><input type="text" class="input comp-creditcard" name="cardnumber" id="cardnumber" ></div>
                                </div>
                                <div class="table-row">
                                    <div class="table-cell">Card Holder</div>
                                    <div class="table-cell"> : </div>
                                    <div class="table-cell"><input type="text" class="input comp-creditcard" name="cardholder" id="cardholder" ></div>
                                </div>
                                <div class="table-row">
                                    <div class="table-cell">Expiry</div>
                                    <div class="table-cell"> : </div>
                                    <div class="table-cell">
                                        <select class="dropdown comp-creditcard" name="month" id="month" onchange="return expiry();" >
                                            <option value="">- Month -</option>
                                            <?php
                                                $months = array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December');
                                                for($m=1; $m<=12; $m++){
                                                    if($m==date('m')){ $selected="selected='selected'"; }else{ $selected=""; }
                                                    echo "<option value='".$m."' ".$selected.">".$months[$m]."</option>"; 
                                                }
                                            ?>
                                        </select>
                                        &nbsp;
                                        <select class="dropdown comp-creditcard" name="year" id="year" onchange="return expiry();" >
                                            <option value="">- Year -</option>	 
                                            <?php 
                                                for($i=date('Y'); $i<=(date('Y')+5); $i++){
                                                    if($i==date('Y')){ $selected="selected='selected'"; }else{ $selected=""; }
                                                    echo "<option value='".$i."' ".$selected.">".$i."</option>"; 
                                                } 
                                            ?>
                                        </select>
                                        <br>
                                        <div id="expired" style="width:100px; height:20px; margin-left: 3px; width: auto; display: inline; color:#F00;"></div>
                                    </div>
                                </div>
                            </div> <!-- end of .table -->      
                            <div>
                                <img src="http://www.parador-hotels.com/payment/v1/images/visa.png" align="absmiddle" style="border:none;">
                                <img src="http://www.parador-hotels.com/payment/v1/images/mastercard.png" align="absmiddle" style="border:none;">
                                <img src="http://www.parador-hotels.com/payment/v1/images/amex.png" align="absmiddle" style="border:none;">
                            </div>
                            
                        </div> <!-- end of .inline-block.border-right -->
                        
                        
                        <div  id="payment-detail" class="col-md-6 col-sm-6 col-xs-12 top">
                            
                            <h2><span class="grey">Billing</span> <span class="blue">Address</span></h2>
                            
                            <div style="display: none;">
                                <input type="checkbox" class="comp-creditcard" name="check" onclick="sama();"> Same with guest address
                            </div>
                            <div class="table">
                                <div class="table-row">
                                    <div class="table-cell top">Address</div>
                                    <div class="table-cell"><textarea name="address_cc" id="address_cc" class="comp-creditcard" ></textarea></div>
                                </div>
                                <div class="table-row">
                                    <div class="table-cell">City</div>
                                    <div class="table-cell"><input type="text" class="input comp-creditcard" name="city_cc" id="city_cc" ></div>
                                </div>
                                <div class="table-row">
                                    <div class="table-cell">State</div>
                                    <div class="table-cell"><input type="text" class="input comp-creditcard" name="state_cc" id="state_cc" ></div>
                                </div>
                                <div class="table-row">
                                    <div class="table-cell">Country</div>
                                    <div class="table-cell">
                                        <select name="country_cc" id="country_cc"  class="comp-creditcard" >
                                            <option value="" selected>--- Please select your country ---</option>
                                            <?php 
                                            $s_country="select countryoid, countryname from country order by countryname"; $q_country=mysqli_query($conn, $s_country) or die ("SQL Error : ".$s_country);
                                            while($country=mysqli_fetch_array($q_country)){
                                                if(empty($_SESSION['nationallity'])){
                                                    if($country['countryname']=="Indonesia"){ $selected=" selected='selected' "; }else{ $selected=""; }
                                                }else{
                                                    if($country['countryoid']==$_SESSION['nationallity']){ $selected=" selected='selected' "; }else{ $selected=""; }
                                                }
                                            ?>
                                            <option value="<?php echo $country['countryoid']; ?>" <?php echo $selected; ?>><?php echo $country['countryname']; ?></option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div> 
                            
                            <div>
                                <input type="checkbox" class="comp-creditcard" name="cc_agree" id="cc-agree" value="agree"  > 
                                I declare this credit card is belong to me
                            </div>
                            
                        </div>
                    </div> 
                    
                    <!-- <div id="container-method-sof" class="container-method" style="display:none;">
                        <div>
                            <h3><span class="blue">Other</span> Information </h3>
                            <br>
                            <div class="row">
                                <div class="col-md-10">     
                                    Notes
                                    <textarea rows="4" class="form-control comp-sof" name="note" id="note_sof"></textarea>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <div id="container-method-skippayment" class="container-method" style="display:none;">
                        <div>
                            <h3><span class="blue">Other</span> Information </h3>
                            <br>
                            <div class="row">
                                <div class="col-md-10">     
                                    Notes
                                    <textarea rows="4" class="form-control comp-sp" name="note" id="note_sp"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="container-method-banktransfer" class="container-method" style="display:none;">
                        <div>
                            <div class="row">
                                <div class="col-md-6">
                                    <h3><span class="blue">Please Transfer Payment</span> to This Account </h3>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-5">Beneficiary Name</div>
                                        <div class="col-md-1 text-right">:</div>
                                        <div class="col-md-6"><b>ACCOUNT NAME</b></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-5">Account Number (IDR/Rupiah)</div>
                                        <div class="col-md-1 text-right">:</div>
                                        <div class="col-md-6"><b>Number</b></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-5">SWIFT CODE</div>
                                        <div class="col-md-1 text-right">:</div>
                                        <div class="col-md-6"><b>code</b></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-5">Bank Address</div>
                                        <div class="col-md-1 text-right">:</div>
                                        <div class="col-md-6"><b>Bank Central Indonesia (&nbsp;<img src="https://www.tiket.com/images/ico_bca.png" width="50">&nbsp;)</b>
                                        <p>Sanur, Denpasar, Bali, Indonesia</p></div>
                                    </div>
                                </div>
                                <div class="col-md-6" style="border-left:1px solid #ccc">
                                    <h3><span class="blue">Other</span> Information </h3>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-10">     
                                            Notes
                                            <textarea rows="4" class="form-control comp-bt" name="note" id="note_bt"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
            
                
                <br>
                    <br>
                <div class="white-box border-box card-rounded">
                        <div>
                            <div class="inline-block top">
                                <div id="captchaimage">
                                    <!-- <div class="g-recaptcha" data-sitekey="6Le5uYYUAAAAAHb5QuV_E5cPbWspa6n8XNNo1H76"></div> -->
                                    <!-- <input type="hidden" class="hiddenRecaptcha" name="hiddenRecaptcha" id="hiddenRecaptcha"> -->
                                </div>
                                <style>
                                    #captchaimage{ margin:0 auto 10px; }
                                    div.g-recaptcha > div > div{ margin: 0 auto; }
                                    a.blue{ color:#00F; }
                                </style>
                            </div>
                        </div>
                    <br>
                    <input type="submit" onclick="showLoadingPage()" name="submit" value="CONTINUE &gt;&gt;" class="general-submit btn-orange" />
                </div>

            </div> 
		</div> 
		
	</div>
<br>	
</form>

<script>
        $('#payment-form').validate({
            invalidHandler: function(event, validator) {
                hideLoadingPage();
            }
        });

        $(document).on('click','input.payment-method',function(){
            var elem = $(this);
            var container = (elem.attr('id')=='method-sof'?'method-creditcard':elem.attr('id'));
            $(".container-method").hide();
            $("#container-"+container).show();
            $('#payment-form input[name=paymentmethodoid]').val(elem.attr('data-oid'));
            
            
            if($("#payment-form").data('validator')){$("#payment-form").data('validator').resetForm();};
            var $settings = $('#payment-form').validate().settings;

            if(elem.attr('id')=='method-sof'){
                $.extend(true, $settings, {
                    rules: {
                        firstname: "required", lastname: "required", email: { required: true, email: true }, mobilephone : "required", 
                        address: "required", city: "required", state : "required", country : "required", 
                        arrivaldate : "required", cardholder : "required", cardtype : "required", 
                        cardnumber: {creditcard2: function(){ return $('#cardtype').val(); }}, monthexpiry: "required", 
                        yearexpiry: "required", address_cc: "required", city_cc: "required", state_cc : "required", 
                        country_cc : "required", mobilephone: "number", number: "number", userkbca: "required",
                    },
                    messages: {
                        email: "not valid email", cardtype : "select card type", cardnumber : "not valid card number",
                    }
                });
            }else{
                $('#container-method-creditcard input').val('');
                $('#container-method-creditcard textarea').text('');
                                
                $.extend(true, $settings, {
                    rules: {
                            firstname: {}, lastname: {}, email: {}, mobilephone : {}, 
                            address: {}, city: {}, state : {}, country : {}, 
                            arrivaldate : {}, cardholder : {}, cardtype : {}, 
                            cardnumber: {creditcard2:{}}, monthexpiry: {}, 
                            yearexpiry: {}, address_cc: {}, city_cc: {}, state_cc : {}, 
                            country_cc : {}, mobilephone: {}, number: {}, userkbca: {},
                    },
                    messages: {
                        email: {}, cardtype : {}, cardnumber : {},
                    }
                });

            }
        });
        $('#method-skippayment')[0].click();


                        // $(document).ready(function() {
                            // $(".container-method").hide();
                            // $("#transfer-policies").hide(); 
                            // $("#klikbca-policies").hide();
                            // $(".comp-transfer").attr('disabled',true); 
                            // $(".comp-kbca").attr('disabled',true);
                            // $("#method-skippayment").trigger('click');
                            
                            // $(document).trigger({ type: 'click', target: $('#method-skippayment')[0] });
                            // $("input.payment-method").on('click',function(){
                                // var elem = $(this);

                                // if(elem.val() == "creditcard"){
                                //     $(".comp-creditcard").removeAttr("disabled");
                                //     $(".comp-transfer").attr('disabled',true);
                                    
                                //     $("#container-method-creditcard").show();
                                //     $("#container-method-bt").hide();
                                    
                                //     $("#transfer-policies").hide(); $("#klikbca-policies").hide();                       
                                // }else if(elem.val() != "creditcard"){
                                //     $(".comp-transfer").removeAttr("disabled");	$(".comp-creditcard").attr('disabled',true);
                                    
                                //     $("#container-method-bt").show();
                                //     $("#container-method-creditcard").hide();
                                //     if(elem.val()  == "banktransfer"){
                                //         $("#transfer-policies").show(); 
                                //         $("#klikbca-policies").hide(); $("#container-method-kbca").hide(); $(".comp-kbca").attr('disabled',true);
                                //         $(".banks").show();  $(".kbcas").hide();
                                //     }else if(elem.val()  == "klikbca"){
                                //         $("#klikbca-policies").show(); $("#container-method-kbca").show(); $(".comp-kbca").attr('disabled',false);
                                //         $("#transfer-policies").hide(); 
                                //         $(".kbcas").show(); $(".banks").hide(); 
                                //     }
                                // }
                            // });
                        // });
                    </script>
<?php //echo "<script>console.log('token:".$_SESSION['tokenSession']."');</script>"; ?>