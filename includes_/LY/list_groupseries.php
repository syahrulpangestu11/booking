<div class="center" id="title-groupseries"><h2><span class="grey">Group</span> <span class="blue">Series</span></h2></div>

<?php 
//require_once("includes/nu/popup.country.php");
require_once("includes/function.randomstring.php");

//PAGING
$batas=5; 

$halaman= isset($_GET['gs_page']) ? $_GET['gs_page'] :'';
if(empty($halaman)){ 
	$posisi=0; $halaman=1;
}else{ 
	$posisi = ($halaman-1) * $batas; 
}
$no=$posisi+1;
?>

<style>
/*
#list-groupseries > li {width: 150px; height: 300px; margin: 0 4px 7px 0; padding: 0;}
#list-groupseries > li:nth-child(5n) {margin: 0 0 7px 0;}
#list-groupseries > li .pict {height: 140px;}
#list-groupseries > li .price {
	position: absolute; top: 10px; left: -3px;
	padding: 5px;
	background: #D20909; color: #fff;
	box-shadow: 1px 1px 5px #000;
}
#list-groupseries > li .text {padding: 10px;}
#list-groupseries > li .fl_bottom {position: absolute; bottom: 10px; width: 165px;}
*/
#container #content #right-content #list-groupseries li {padding: 10px 0; border-bottom: 1px solid #ddd;}
#container #content #right-content #list-groupseries li:first-of-type {padding-top: 0;}
#container #content #right-content #list-groupseries li:last-of-type {/*padding-bottom: 0;*/ border-bottom: none;}
#container #content #right-content #list-groupseries li .pict {width: 100px; height: 100px; margin: 0 5px 0 0;}
#container #content #right-content #list-groupseries li .col-2 {width: 395px;}	
</style>

<ul class="content-list white-box border-box" id="list-groupseries">
<?php	/*SHOW LIST PACKAGE*/
	$j=0;
	$todaydate=date("Y-m-d");

	if($uri2=="" or empty($uri2)){	// [homepage] show package in the homepage selain free and easy
		/*$s_groupseries="select packagename, package.packageoid, package.picture, package.description, package.facilities, package.headline, package_pax, packageroom.extrabed, package.hoteloid, packageroom.roomoid, cityname from package inner join hotel using (hoteloid) inner join packageroom using (packageoid) inner join packagerate using (packagedtloid) inner join city using (cityoid) where packagestatus='1' AND citystatus = '1' and packagerate.endbook>='$todaydate' AND packagename NOT LIKE '%free%' group by packagename ORDER BY rand()";
		$limit = " limit 4 ";
		$style2 = " display:none; ";*/
	}else if( $uri2=="package"  and !empty($uri3)){
		$s_groupseries="
			SELECT gp.*, c.cityname 
			FROM grouppackage gp
		 	INNER JOIN city c USING (cityoid) 
		 	WHERE gp.groupstatus IN ('1','2') 
		 	AND c.citystatus = '1'
		 	AND cityname='$decode_city' 
		 	AND gp.endbook>='$todaydate' $destination_query 
		 	GROUP BY grouppackageoid
	 	";	
		$limit = " LIMIT $posisi,$batas ";
	}else if( $uri2=="searchpackage" or $uri2=="searchtours" ){			// [package] show package in the selected city show all
		$destination_query = 
			($destination != "") ? " AND cityoid IN (
									SELECT cityoid FROM city 
									INNER JOIN grouppackage USING (cityoid) 
									WHERE cityname='".$destination."' 
									GROUP BY cityoid 
									ORDER BY cityname) "
								 : "" ;
								  
		$s_groupseries="
			SELECT gp.*, c.cityname 
			FROM grouppackage gp
		 	INNER JOIN city c USING (cityoid) 
		 	WHERE gp.groupstatus IN ('1','2') 
		 	AND c.citystatus = '1' 
		 	AND gp.endbook>='$todaydate' $destination_query 
		 	GROUP BY grouppackageoid
	 	"; 
		$limit = " LIMIT $posisi,$batas ";
	}else if( $uri2=="search" ){	// [search]
		$search_q = $_REQUEST['q'];
		$s_groupseries="
			SELECT gp.*, c.cityname 
			FROM grouppackage gp
		 	INNER JOIN city c USING (cityoid) 
		 	WHERE gp.groupstatus IN ('1','2') 
		 	AND c.citystatus = '1'
		 	AND groupname LIKE '%$search_q%' 
		 	AND gp.endbook>='$todaydate' $destination_query 
		 	GROUP BY grouppackageoid
	 	";
		$limit = " LIMIT $posisi,$batas ";
		
	}else{	// [groupseries] show package in the selected city limit 10
		$destination_query = 
			($destination != "") ? " AND cityoid IN (
									SELECT cityoid FROM city 
									INNER JOIN grouppackage USING (cityoid) 
									WHERE cityname='".$destination."' 
									GROUP BY cityoid 
									ORDER BY cityname) "
								 : "" ;
								  
		$s_groupseries="
			SELECT gp.*, c.cityname 
			FROM grouppackage gp
		 	INNER JOIN city c USING (cityoid) 
		 	WHERE gp.groupstatus IN ('1','2') 
		 	AND c.citystatus = '1' 
		 	AND gp.endbook>='$todaydate' $destination_query 
		 	GROUP BY grouppackageoid
	 	"; 
		$limit = " LIMIT $posisi,$batas ";
	}
	$q_groupseries=mysqli_query($conn, $s_groupseries.$limit) or die(mysqli_error());
	
	$num_groupseries=mysqli_num_rows(mysqli_query($conn, $s_groupseries));
	$style1 = ($num_groupseries <= $batas) ? " display:none; " : "" ;
	if($num_groupseries <= 0) { //untuk judul search
		?><style>
		#title-groupseries, #list-groupseries{display: none;} 
		</style><?php
	}
	
	while($group_package=mysqli_fetch_array($q_groupseries)){
		$grouppackageoid = $group_package['grouppackageoid'];
		$encode_grouppackageoid = randomString(4).$grouppackageoid.randomString(2);
		
		$group_package['groupname'] = mysqli_real_escape_string($group_package['groupname']);	
		
		$group_name=$group_package['groupname'];
		$encode_group_name=str_replace(' ', '-', $group_name);
		
		include('includes/LY/new.minrate.groupseries.php');
		$display_price = ($currencycode == "IDR") ? substr_replace($nominal_minrate, "K", -4) : $nominal_minrate ;
		$show_minrate=$currencycode." <br><h3>".$display_price."</h3>";
		
		
		/* menghitung jumlah package-------------------------------------------------------------------------------------*/
		/*
		$s_countpkg="
			SELECT COUNT(DISTINCT grouppackageoid) AS jmlpkg, MIN(gr.departuredate) AS mindate 
			FROM grouppackage 
			INNER JOIN grouprate gr USING (grouppackageoid) 
			INNER JOIN groupratetype grt USING (groupratetypeoid) 
			INNER JOIN city USING (cityoid) 
			WHERE gr.grouppackageoid='$group_package[grouppackageoid]' 
			AND gp.groupstatus IN ('1','2') 
			AND ( 
				(gp.startbook<='$todaydate' AND gp.endbook>='$todaydate')
				OR 
				gp.startbook>='$todaydate'
			)
		";
		$q_countpkg=mysqli_query($conn, $s_countpkg); 
		$countpkg=mysqli_fetch_array($q_countpkg);
		
		
		/*----------------link ke halaman detail package yang berisi list hotel----------------*
		$checkin_date=strtotime($checkin);
		$mindate_pkg=strtotime($countpkg['mindate']);
		if($checkin_date<=$mindate_pkg){
			$tgl_checkin=$countpkg['mindate']; 
			$checkin_new=$countpkg['mindate'];
		}else{
			$checkin_new=$checkin; 
		}
		if($night<$minnight){
			$night=$minnight; 
		}
		
		if($countpkg['jmlpkg']==1){
			$href= $base_url."/bookpackage/".$packagedtloid."/".$checkin_new."/".$night."/".$adult."/".$child."/".$getpackage;
		}else{
			if($uri2=="searchpackage"){	// [homepage]  button show all package
				$href= $base_url."/listpackage/".$encode_group_name."/".$checkin_new."/".$night."/".$destination."/".$adult."/".$child."/".$getpackage;
			}else{
				$href=$base_url."/listpackage/".$encode_group_name;
			} 
		}*/
		/*if($uri2=="searchpackage"){	// [homepage]  button show all package
			$href= $base_url."/groupseries/book/".$encode_grouppackageoid."/".$encode_group_name."/".$checkin_new."/".$night."/".$destination."/".$adult."/".$child."/".$getpackage;
		}else{
			$href = $base_url."/groupseries/book/".$encode_grouppackageoid."/".$encode_group_name;
		}*/
		$href = "#";
		
		//when image doesn't exist, use default-thumb.png
		$pict = ($group_package['picture'] != "") ? $group_package['picture'] : $base_url."/images/default-thumb.png";
		//$pict = $group_package['picture'];
		$headline = $group_package['headline'];  
		$facilities = $group_package['facilities']; 
		$description = $group_package['description']; 
		$destination_label = $group_package['cityname'];
		
		if(!empty($headline)){
			$showdetail = $headline; 
		}else{
			$showdetail = "<div class='readmore-less'>".$description."</div>"; 
		}
		$j++; //untuk jumlah row di homepage
		unset($get_min_rate);
		
		
		$group_name = stripslashes($group_name);
		
		if($flag_package == "homepage"){
			/*
			?>
			<li><a direct="<?=$href;?>" id="launch">
				<span class="title">
					<div class="destination-label"><?=$destination_label;?></div>
					<?php echo $group_name; ?>
				</span>
				<div class="rate tooltip blue-tooltip">
					from
					<?php echo $show_minrate; ?>/pax*
					<span><?php echo $show_real_min_rate; ?></span>
				</div>
				<div class="pict">
					<img src="<?=$pict;?>"  class="nailthumb-container square" onerror="ImgProblem(this)"><!-- onerror="ImgProblem(this)" -->
					
				</div>
				<article class="facilities">
					<?=$showdetail;?>
				</article>
			</a></li>	
			<?php
			if($j==2){
				?></ul>
				<div id="banner-2">
					<ul class="rslides">
					    <?php 
						$sql = "SELECT * FROM imgbanner WHERE status='1' AND position = '2'";		
						$sqlslide = mysqli_query($conn, $sql) or die(mysqli_error());
						include ("includes/nu/slideshow_query.php");
					    ?>
					</ul>	
				</div><ul class="list" id="list-package">
				<?php
			}//end if $j
		}//end if $flag_package
		
		
		else if($flag_package == "freeandeasy"){ //if $flag_package free and easy
			?>
			<li><a direct="<?=$href;?>" id="launch">
				<div class="rate tooltip blue-tooltip">
					from
					<?php echo $show_minrate; ?>/pax*
					<span><?php echo $show_real_min_rate; ?></span>
				</div>
				<div class="pict">
					<img src="<?=$pict; ?>"  class="nailthumb-container square" onerror="ImgProblem(this)"><!-- onerror="ImgProblem(this)" -->
				</div>
				<div class="title"><span><?php echo $group_name; ?></span></div>
			</a></li>
			<?php
			*/
		}else{	
			?>
			<!--
			<li class="inline-block top white-box border-box">
				<div class="thumb pict"><img src="<?=$pict;?>"></div>
				<div class="price">from <?=$show_minrate;?> /pax*</div>
				<div class="text">
					<div class="title center"><?=$group_name;?></div>
					<div class="small-desc"><?=$showdetail;?></div>
					<div class="center fl_bottom">
						<a href="#" class="button">BOOK</a>
					</div>
				</div>
			</li>
			-->
			<li class="top">
				<div class="thumb pict inline-block top">
					<img src="<?=$pict;?>">
				</div>
				<div class="col-2 inline-block top">
					<div class="title border-box">
						<img src="<?=$base_url;?>/images/blank.gif" class="flag flag-id" alt="Czech Republic" />
						<?=$group_name;?>
					</div>
					<div class="small-desc"><?=$showdetail;?></div>
				</div>
				<div class="fl_right inline-block right">
					<div class="price">
						from <?=$show_minrate;?> /pax
					</div>
					<div class="bottom-right">
						<a href="<?=$href;?>" class="button book inline-block">BOOK</a>
					</div>
				</div>
			</li>
			<?php
		}//end else $flag_package

	} /*----------- while group name package -----------*/
/*} ----------- while group city / destination-----------*/
?>
</ul>

<?php /*if($uri2=="" or empty($uri2)){	// [homepage]  button show all package ?>
	<br>
	<div class="display-block clear margin-top-minus-10">
		<a href="<?php echo $base_url; ?>/allpackage" class="btn-showall">view all packages</a>
	</div>
	<br>
<?php } */?>


<!-- PAGING part2 -->
<div class="white-box berder-box clear" style="<?=$style1.$style2;?>">
	<?php 
	//$file=$uri3."&";
	//$file=$uri3."/?";
	if(!empty($uri4)){
		$file=$uri4."&";
	}else{
		$file="?";
	}

	$tampil2=$s_groupseries; 
	
	$hasil2=mysqli_query($conn, $tampil2); $jmldata=mysqli_num_rows($hasil2); 
	$jmlhalaman=ceil($jmldata/$batas);
	?>
	<div class="inline-block fl_left small-desc middle">
		<?php
		$crndata=mysqli_num_rows($q_groupseries);
		echo "Displaying ".($posisi+1)." to ".($posisi+$crndata)." of ".$jmldata." items in Group Series";
		?>
	</div>
	<div class="inline-block fl_right">
		<?php
		//link ke halaman sebelumnya (previous)
		if($halaman > 1){	$previous=$halaman-1; echo "<a href=".$file."np_page=1 > &lt;&lt; First&nbsp;&nbsp;</a><a href=".$file."np_page=$previous > &lt; Previous&nbsp;&nbsp;</a>";
		}else{	echo "&lt;&lt; First&nbsp;&nbsp; &lt; Previous&nbsp;&nbsp;";
		}
		$angka=($halaman > 3 ? " ... " : " ");
		for($i=$halaman-2;$i<$halaman;$i++){
		  if ($i < 1) continue;
		  $angka .= "<a href=".$file."np_page=$i  >$i&nbsp;</a> ";
		}
		$angka .= " <b>$halaman</b> ";
		for($i=$halaman+1;$i<($halaman+3);$i++){
		  if ($i > $jmlhalaman) break;
		  $angka .= "<a href=".$file."np_page=$i  >$i&nbsp;</a> ";
		}
		$angka .= ($halaman+2<$jmlhalaman ? " ...  
		          <a href=".$file."np_page=$jmlhalaman  >$jmlhalaman</a> " : " ");
		echo "$angka";
		//link kehalaman berikutnya (Next)
		if($halaman < $jmlhalaman){
			$next=$halaman+1;
			echo "<a href=".$file."np_page=$next  >Next &gt; &nbsp;&nbsp;</a><a href=".$file."np_page=$jmlhalaman  >Last &gt;&gt; &nbsp;&nbsp;</a> ";
		}else{ echo "&nbsp;&nbsp;Next &gt; &nbsp;&nbsp;Last &gt;&gt;";
		}
		?>
	</div>
</div>
