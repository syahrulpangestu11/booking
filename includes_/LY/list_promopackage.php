<div class="center" id="title-promopackage"><h2><span class="grey">Promotional</span> <span class="blue">Package</span></h2></div>

<?php 
//require_once("includes/nu/popup.country.php");
require_once("includes/function.randomstring.php");

//PAGING
$batas=5; 

$halaman= isset($_GET['pp_page']) ? $_GET['pp_page'] :'';
if(empty($halaman)){ 
	$posisi=0; $halaman=1;
}else{ 
	$posisi = ($halaman-1) * $batas; 
}
$no=$posisi+1;
?>

<style>
#container #content #right-content #list-promopackage li {padding: 10px 0; border-bottom: 1px solid #ddd;}
#container #content #right-content #list-promopackage li:first-of-type {padding-top: 0;}
#container #content #right-content #list-promopackage li:last-of-type {/*padding-bottom: 0;*/ border-bottom: none;}
#container #content #right-content #list-promopackage li .pict {width: 100px; height: 100px; margin: 0 5px 0 0;}
#container #content #right-content #list-promopackage li .col-2 {width: 395px;}	
</style>

<ul class="content-list white-box border-box" id="list-promopackage">
<?php	/*SHOW LIST PACKAGE*/
	$j=0;
	$todaydate=date("Y-m-d");

	if($uri2=="" or empty($uri2)){	// [homepage] show package in the homepage selain free and easy
		/*$s_promopackage="select packagename, package.packageoid, package.picture, package.description, package.facilities, package.headline, package_pax, packageroom.extrabed, package.hoteloid, packageroom.roomoid, cityname from package inner join hotel using (hoteloid) inner join packageroom using (packageoid) inner join packagerate using (packagedtloid) inner join city using (cityoid) where packagestatus='1' AND citystatus = '1' and packagerate.endbook>='$todaydate' AND packagename NOT LIKE '%free%' group by packagename ORDER BY rand()";
		$limit = " limit 4 ";
		$style2 = " display:none; ";*/
	}else if( $uri2=="package"  and !empty($uri3)){			
		$s_promopackage="
			SELECT pp.*, c.cityname 
			FROM promopackage pp
		 	INNER JOIN promopackagehotel pph USING (promopackageoid)
			INNER JOIN hotel h USING (hoteloid)
			INNER JOIN city c USING (cityoid)
			INNER JOIN promopackagerate ppr USING (promopackagehoteloid)
		 	WHERE pp.promopackagestatus IN ('1','2') 
		 	AND c.citystatus = '1' 
		 	AND cityname='$decode_city'
		 	AND pp.endbook>='$todaydate' 
		 	GROUP BY promopackageoid
	 	"; 
		$limit = " LIMIT $posisi,$batas ";
	}else if( $uri2=="searchpackage" or $uri2=="searchtours" ){			// [check-availability] show package in the selected city show all
		$destination_query = 
			($destination != "") ? " AND cityoid IN (
									SELECT cityoid FROM city 
									INNER JOIN hotel USING (cityoid)
									INNER JOIN promopackagehotel USING (hoteloid) 
									WHERE cityname='".$destination."' 
									GROUP BY cityoid 
									ORDER BY cityname) "
								 : "" ;
								  
		$s_promopackage="
			SELECT pp.*, c.cityname 
			FROM promopackage pp
		 	INNER JOIN promopackagehotel pph USING (promopackageoid)
			INNER JOIN hotel h USING (hoteloid)
			INNER JOIN city c USING (cityoid)
			INNER JOIN promopackagerate ppr USING (promopackagehoteloid)
		 	WHERE pp.promopackagestatus IN ('1','2') 
		 	AND c.citystatus = '1' 
		 	AND pp.endbook>='$todaydate' $destination_query 
		 	GROUP BY promopackageoid
	 	"; 
		$limit = " LIMIT $posisi,$batas ";
	}else if( $uri2=="search" ){	// [search]
		$search_q = $_REQUEST['q'];
		$s_promopackage="
			SELECT pp.*, c.cityname 
			FROM promopackage pp
		 	INNER JOIN promopackagehotel pph USING (promopackageoid)
			INNER JOIN hotel h USING (hoteloid)
			INNER JOIN city c USING (cityoid)
			INNER JOIN promopackagerate ppr USING (promopackagehoteloid)
		 	WHERE pp.promopackagestatus IN ('1','2') 
		 	AND c.citystatus = '1' 
		 	AND promopackagename LIKE '%$search_q%'
		 	AND pp.endbook>='$todaydate' $destination_query 
		 	GROUP BY promopackageoid
	 	";
		$limit = " LIMIT $posisi,$batas ";
		
	}else{	// [groupseries] show package in the selected city limit 10
		$destination_query = 
			($destination != "") ? " AND cityoid IN (
									SELECT cityoid FROM city 
									INNER JOIN hotel USING (cityoid)
									INNER JOIN promopackagehotel USING (hoteloid) 
									WHERE cityname='".$destination."' 
									GROUP BY cityoid 
									ORDER BY cityname) "
								 : "" ;
								  
		$s_promopackage="
			SELECT pp.*, c.cityname 
			FROM promopackage pp
		 	INNER JOIN promopackagehotel pph USING (promopackageoid)
			INNER JOIN hotel h USING (hoteloid)
			INNER JOIN city c USING (cityoid)
			INNER JOIN promopackagerate ppr USING (promopackagehoteloid)
		 	WHERE pp.promopackagestatus IN ('1','2') 
		 	AND c.citystatus = '1' 
		 	AND pp.endbook>='$todaydate' $destination_query 
		 	GROUP BY promopackageoid
	 	"; 
		$limit = " LIMIT $posisi,$batas ";
	}
	//echo $s_promopackage;
	$q_promopackage=mysqli_query($conn, $s_promopackage.$limit) or die(mysqli_error());
	
	$num_promopackage=mysqli_num_rows(mysqli_query($conn, $s_promopackage));
	$style1 = ($num_promopackage <= $batas) ? " display:none; " : "" ;
	if($num_promopackage <= 0) { //untuk judul search
		?><style>
			#title-promopackage, #list-promopackage{display: none;} 
		</style><?php
	}
	
	while($promopackage=mysqli_fetch_array($q_promopackage)){
		$promopackageoid = $promopackage['promopackageoid'];
		$encode_promopackageoid = randomString(4).$promopackageoid.randomString(2);
		
		$promopackage['promopackagename'] = mysqli_real_escape_string($promopackage['promopackagename']);	
		
		$promopackage_name=$promopackage['promopackagename'];
		$encode_promopackage_name=str_replace(' ', '-', $promopackage_name);
		
		include('includes/LY/new.minrate.promopackage.php');
		$display_price = ($currencycode == "IDR") ? substr_replace($nominal_minrate, "K", -4) : $nominal_minrate ;
		$show_minrate=$currencycode." <br><h3>".$display_price."</h3>";
		
		
		/* menghitung jumlah package-------------------------------------------------------------------------------------*/
		/*
		$s_countpkg="
			SELECT COUNT(DISTINCT grouppackageoid) AS jmlpkg, MIN(gr.departuredate) AS mindate 
			FROM grouppackage 
			INNER JOIN grouprate gr USING (grouppackageoid) 
			INNER JOIN groupratetype grt USING (groupratetypeoid) 
			INNER JOIN city USING (cityoid) 
			WHERE gr.grouppackageoid='$promopackage[grouppackageoid]' 
			AND gp.groupstatus='1' 
			AND ( 
				(gp.startbook<='$todaydate' AND gp.endbook>='$todaydate')
				OR 
				gp.startbook>='$todaydate'
			)
		";
		$q_countpkg=mysqli_query($conn, $s_countpkg); 
		$countpkg=mysqli_fetch_array($q_countpkg);
		
		
		/*----------------link ke halaman detail package yang berisi list hotel----------------*
		$checkin_date=strtotime($checkin);
		$mindate_pkg=strtotime($countpkg['mindate']);
		if($checkin_date<=$mindate_pkg){
			$tgl_checkin=$countpkg['mindate']; 
			$checkin_new=$countpkg['mindate'];
		}else{
			$checkin_new=$checkin; 
		}
		if($night<$minnight){
			$night=$minnight; 
		}
		
		if($countpkg['jmlpkg']==1){
			$href= $base_url."/bookpackage/".$packagedtloid."/".$checkin_new."/".$night."/".$adult."/".$child."/".$getpackage;
		}else{
			if($uri2=="searchpackage"){	// [homepage]  button show all package
				$href= $base_url."/listpackage/".$encode_promopackage_name."/".$checkin_new."/".$night."/".$destination."/".$adult."/".$child."/".$getpackage;
			}else{
				$href=$base_url."/listpackage/".$encode_promopackage_name;
			} 
		}*/
		/*if($uri2=="searchpackage"){	// [homepage]  button show all package
			$href= $base_url."/promotional-package/book/".$encode_promopackageoid."/".$encode_promopackage_name."/".$checkin_new."/".$night."/".$destination."/".$adult."/".$child."/".$getpackage;
		}else{
			$href = $base_url."/promotional-package/book/".$encode_promopackageoid."/".$encode_promopackage_name;
		}*/
		$href = "#";
		
		//when image doesn't exist, use default-thumb.png
		$pict = ($promopackage['picture'] != "") ? $promopackage['picture'] : $base_url."/images/default-thumb.png";
		//$pict = $promopackage['picture'];
		$headline = $promopackage['headline'];  
		$facilities = $promopackage['facilities']; 
		$description = $promopackage['description']; 
		$destination_label = $promopackage['cityname'];
		
		if(!empty($headline)){
			$showdetail = $headline; 
		}else{
			$showdetail = "<div class='readmore-less'>".$description."</div>"; 
		}
		$j++; //untuk jumlah row di homepage
		unset($get_min_rate);
		
		
		$promopackage_name = stripslashes($promopackage_name);
		
		if($flag_package == "homepage"){
			/*
			?>
			<li><a direct="<?=$href;?>" id="launch">
				<span class="title">
					<div class="destination-label"><?=$destination_label;?></div>
					<?php echo $promopackage_name; ?>
				</span>
				<div class="rate tooltip blue-tooltip">
					from
					<?php echo $show_minrate; ?>/pax*
					<span><?php echo $show_real_min_rate; ?></span>
				</div>
				<div class="pict">
					<img src="<?=$pict;?>"  class="nailthumb-container square" onerror="ImgProblem(this)"><!-- onerror="ImgProblem(this)" -->
					
				</div>
				<article class="facilities">
					<?=$showdetail;?>
				</article>
			</a></li>	
			<?php
			if($j==2){
				?></ul>
				<div id="banner-2">
					<ul class="rslides">
					    <?php 
						$sql = "SELECT * FROM imgbanner WHERE status='1' AND position = '2'";		
						$sqlslide = mysqli_query($conn, $sql) or die(mysqli_error());
						include ("includes/nu/slideshow_query.php");
					    ?>
					</ul>	
				</div><ul class="list" id="list-package">
				<?php
			}//end if $j
		}//end if $flag_package
		
		
		else if($flag_package == "freeandeasy"){ //if $flag_package free and easy
			?>
			<li><a direct="<?=$href;?>" id="launch">
				<div class="rate tooltip blue-tooltip">
					from
					<?php echo $show_minrate; ?>/pax*
					<span><?php echo $show_real_min_rate; ?></span>
				</div>
				<div class="pict">
					<img src="<?=$pict; ?>"  class="nailthumb-container square" onerror="ImgProblem(this)"><!-- onerror="ImgProblem(this)" -->
				</div>
				<div class="title"><span><?php echo $promopackage_name; ?></span></div>
			</a></li>
			<?php
			*/
		}else{	
			
			?>
			<!-- 
			<li class="list-package-child"><a direct="<?=$href;?>" id="launch">
				<div class="rate tooltip blue-tooltip">
					from
					<?php echo $show_minrate; ?>/pax*
					<span><?php echo $show_real_min_rate; ?></span>
				</div>
				<div class="pict">
					<img src="<?=$pict;?>"  class="nailthumb-container square" onerror="ImgProblem(this)">
				</div>
				<div class="destination-label"><?=$destination_label;?></div>
				<span class="title"><?=$promopackage_name;?></span>
				<article class="facilities">
					<?=$showdetail;?>
				</article>
			</a></li>
			 -->
			<li class="top">
				<div class="thumb pict inline-block top">
					<img src="<?=$pict;?>">
				</div>
				<div class="col-2 inline-block top">
					<div class="title border-box">
						<img src="<?=$base_url;?>/images/blank.gif" class="flag flag-id" alt="Czech Republic" />
						<?=$group_name;?>
					</div>
					<div class="small-desc"><?=$showdetail;?></div>
				</div>
				<div class="fl_right inline-block right">
					<div class="price">
						from <?=$show_minrate;?> /pax
					</div>
					<div class="bottom-right">
						<a href="<?=$href;?>" class="button book inline-block">BOOK</a>
					</div>
				</div>
			</li>
			<?php
		}//end else $flag_package

	} /*----------- while group name package -----------*/
/*} ----------- while group city / destination-----------*/
?>
</ul>

<?php /*if($uri2=="" or empty($uri2)){	// [homepage]  button show all package ?>
	<br>
	<div class="display-block clear margin-top-minus-10">
		<a href="<?php echo $base_url; ?>/allpackage" class="btn-showall">view all packages</a>
	</div>
	<br>
<?php } */?>


<!-- PAGING part2 -->
<div class="white-box border-box clear" style="<?=$style1.$style2;?>">
	<?php 
	//$file=$uri3."&";
	//$file=$uri3."/?";
	if(!empty($uri4)){
		$file=$uri4."&";
	}else{
		$file="?";
	}

	$tampil2=$s_promopackage; 
	
	$hasil2=mysqli_query($conn, $tampil2); $jmldata=mysqli_num_rows($hasil2); 
	$jmlhalaman=ceil($jmldata/$batas);
	?>
	<div class="inline-block fl_left small-desc middle">
		<?php
		$crndata=mysqli_num_rows($q_promopackage);
		echo "Displaying ".($posisi+1)." to ".($posisi+$crndata)." of ".$jmldata." items in Promotional Package";
		?>
	</div>
	<div class="inline-block fl_right">
		<?php
		//link ke halaman sebelumnya (previous)
		if($halaman > 1){	$previous=$halaman-1; echo "<a href=".$file."pp_page=1 > &lt;&lt; First&nbsp;&nbsp;</a><a href=".$file."pp_page=$previous > &lt; Previous&nbsp;&nbsp;</a>";
		}else{	echo "&lt;&lt; First&nbsp;&nbsp; &lt; Previous&nbsp;&nbsp;";
		}
		$angka=($halaman > 3 ? " ... " : " ");
		for($i=$halaman-2;$i<$halaman;$i++){
		  if ($i < 1) continue;
		  $angka .= "<a href=".$file."pp_page=$i  >$i&nbsp;</a> ";
		}
		$angka .= " <b>$halaman</b> ";
		for($i=$halaman+1;$i<($halaman+3);$i++){
		  if ($i > $jmlhalaman) break;
		  $angka .= "<a href=".$file."pp_page=$i  >$i&nbsp;</a> ";
		}
		$angka .= ($halaman+2<$jmlhalaman ? " ...  
		          <a href=".$file."pp_page=$jmlhalaman  >$jmlhalaman</a> " : " ");
		echo "$angka";
		//link kehalaman berikutnya (Next)
		if($halaman < $jmlhalaman){
			$next=$halaman+1;
			echo "<a href=".$file."pp_page=$next  >Next &gt; &nbsp;&nbsp;</a><a href=".$file."pp_page=$jmlhalaman  >Last &gt;&gt; &nbsp;&nbsp;</a> ";
		}else{ echo "&nbsp;&nbsp;Next &gt; &nbsp;&nbsp;Last &gt;&gt;";
		}
		?>
	</div>
</div>
