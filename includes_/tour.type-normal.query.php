<?php
$encoded_tourhoteloid = $_GET['tour'];
$tourhoteloid = substr($encoded_tourhoteloid, 4, strlen($encoded_tourhoteloid)-6);

$s_tour = "
	SELECT t.tourtypeoid, t.tourpricingoid, t.night, t.tourpict, minpax, tc.*, t.continentoid, t.countryoid, t.stateoid, t.cityoid 
	FROM tour AS t 
	INNER JOIN tourpricing AS tp USING (tourpricingoid) 
	INNER JOIN tourtype AS tt USING (tourtypeoid) 
	INNER JOIN tourcontent AS tc USING (touroid) 
	INNER JOIN tourhotel AS th USING (touroid) 
	WHERE th.tourhoteloid = '$tourhoteloid'
";
$q_tour = mysqli_query($conn, $s_tour) or die(mysqli_error()); $tour = mysqli_fetch_array($q_tour);

$name = $tour['name']; $headline = $tour['headline']; $description = $tour['description']; $pict = $tour['tourpict'];
$night = $tour['night']; $minpax = $tour['minpax'];
$tourtype = $tour['tourtypeoid']; $tourpricing = $tour['tourpricingoid'];
$itenerary = $tour['itenerary']; $termscondition = $tour['termscondition'];
$tour_continentoid = $tour['continentoid'];
$tour_countryoid = $tour['countryoid'];
$tour_stateoid = $tour['stateoid'];
$tour_cityoid = $tour['cityoid'];
?>