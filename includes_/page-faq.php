<div id="pgsch" class="container" style="padding-bottom:150px;">
    <div class="white-box border-box card-rounded" style="padding-bottom:50px;">
        <h2><span class="grey">Frequently Asked Questions (FAQ) </span></h2>	
        <ul class="ac-container">
            <?php
            $sql = "SELECT * FROM masterfaq f order by type";// WHERE f.publishedoid = '1'";
            $query = mysqli_query($conn, $sql) or die("<li>No Data</li>");
            $type = '';
            while($result = mysqli_fetch_array($query)){
                if($type<>$result['type']) {
                    $type= $result['type'];
                    ?>
                    
                    <li><br><h3><i class="far fa-circle" style="font-size: 16px;vertical-align: middle;margin-top: -3px;"></i>&nbsp;&nbsp;<?php echo $result['type'];?></h3></li>
                    <?php
                }
            ?>
            <li style="margin-left:25px;">
                <hr>
                <h4><?php echo $result['question'];?></h4>
                <div class="inline-block valign-top">
                    <b></b><?php echo $result['answer'];?>
                </div>
            </li>
            <?php 
            }
            ?>
        </ul>
</div>
</div>