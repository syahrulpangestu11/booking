<?php
 
class Paginator {
 
    private $_conn;
    private $_limit;
    private $_page;
    private $_query;
    private $_total;

    private $_data;
    private $_start;
    private $_end;
    
    public function __construct($data){
        $this->_data = $data;
        $this->_total = sizeof($data);
    }
    
    public function getData(){//($page=null,$limit=null) {
        // $Paginator->getPage($_GET['page'],$_GET['limit']);

        $def_limit =10;
        $def_page =1;
        
        $limit=(!empty($_GET["limit"])?$_GET["limit"]:'');
        $page=(!empty($_GET["page"])?$_GET["page"]:'');

        $limit = (is_null($limit)?$def_limit:(int)$limit);
        $page = (is_null($page)?$def_page:(int)$page);

        $limit = ($limit==0?$def_limit:$limit);
        $page = ($page==0?$def_page:$page);

        $this->_limit   = $limit;
        $this->_page    = $page;
        $this->_start    = ( ( $this->_page * $this->_limit ) - ($this->_limit -1) );
        $this->_end    = ( $this->_limit * $this->_page );

        if((int)$this->_total<(int)$this->_end){
            $this->_end    = $this->_total;
        }
        
        if ( $this->_limit == 'all' ) {
            $results      = $this->_data;
        } else {
            $results      = array();
            // echo '<br><br>'.$this->_start.'|'.$this->_end.'|'.$this->_total;
            for($i=((int)$this->_start)-1;$i<(int)$this->_end;$i++){
                // echo json_encode($this->_data[$i]);
                array_push($results,$this->_data[$i]);
            }
        }
     
        $result         = new stdClass();
        // number of page
        $result->page   = $this->_page;
        // total item from all page
        $result->total  = $this->_total; 
        // total item on this page
        $result->limit  = $this->_limit; 
        // number of the first item on this page
        $result->start  = $this->_start; 
        // number of the last item on this page
        $result->end  = $this->_end;
        // data of item on this page
        $result->data   = $results;
        
        return $result;
    }
    
        
    public function createLinks($list_class,$links=null) {
        // echo $Paginator->createLinks('pagination pagination-sm',$_GET['links'] ); 
        // $links is total of links to another page that can be seen and clicked.

        $def_links = 5;
        //print_r('$links='.$links.'|'.($links=='').'|'.empty($links).'|');
        $links = (is_null($links)?$def_links:(int)$links);
        $links = ($links==0?$def_links:$links);
                
        if ( $this->_limit == 'all' ) {
            return '';
        }
    
        $last       = ceil( $this->_total / $this->_limit );
    
        $start      = ( ( $this->_page - $links ) > 0 ) ? $this->_page - $links : 1;
        $end        = ( ( $this->_page + $links ) < $last ) ? $this->_page + $links : $last;
    
        $html       = '<ul class="' . $list_class . '">';
    
        $class      = ( $this->_page == 1 ) ? "disabled" : "";
        $html       .= '<li class="' . $class . '"><a href="javascript:void(0);" pagevalue="'.( $this->_page + 1 ).'" ><i class="fa fa-angle-double-left"></i>&nbsp;</a></li>';
    
        if ( $start > 1 ) {
            $html   .= '<li><a  href="javascript:void(0);" pagevalue="1" >1</a></li>';
            $html   .= '<li class="disabled"><span>...</span></li>';
        }
    
        for ( $i = $start ; $i <= $end; $i++ ) {
            $class  = ( $this->_page == $i ) ? "active" : "";
            $html   .= '<li class="' . $class . '"><a href="javascript:void(0);" pagevalue="'.$i.'">' . $i . '</a></li>';
        }
    
        if ( $end < $last ) {
            $html   .= '<li class="disabled"><span>...</span></li>';
            $html   .= '<li><a href="javascript:void(0);" pagevalue="'.$last.'" >' . $last . '</a></li>';
        }
    
        $class      = ( $this->_page == $last ) ? "disabled" : "";
        $html       .= '<li class="' . $class . '"><a href="javascript:void(0);" pagevalue="'.( $this->_page + 1 ).'" >&nbsp;<i class="fa fa-angle-double-right"></i></a></li>';
    
        $html       .= '</ul>';
        
        if((int)$this->_total===0){$html='';}
        return $html;
    }
    
    // public function __construct( $conn, $query, $query_total=null ) {

    // 	// $Paginator  = new Paginator($conn, $List->getQueryList(), $List->getQueryList('count') );
    // 	// $results    = $Paginator->getData($_GET['page'],$_GET['limit']);
    
    //     $this->_conn = $conn;
    //     $this->_query = $query;
        

    //     if(is_null($query_total)){
    //         $rs= $this->_conn->query( $this->_query );
    //         $this->_total = $rs->num_rows;
    //     }else{
    //         $rs= $this->_conn->query( $query_total );
    //         while($data = $rs->fetch_assoc())
    //         {
    //             $this->_total = $data['total'];
    //         }
    //     }
            
    // }
    
    // public function getData($page=null,$limit=null) {
    //     // $Paginator->getData($_GET['page'],$_GET['limit']);

    //     $def_limit =10;
    //     $def_page =1;
        
    //     $limit = (is_null($limit)?$def_limit:(int)$limit);
    //     $page = (is_null($page)?$def_page:(int)$page);

    //     $limit = ($limit==0?$def_limit:$limit);
    //     $page = ($page==0?$def_page:$page);

    //     $this->_limit   = $limit;
    //     $this->_page    = $page;
     
    //     if ( $this->_limit == 'all' ) {
    //         $query      = $this->_query;
    //     } else {
    //         $query      = $this->_query . " LIMIT " . ( ( $this->_page - 1 ) * $this->_limit ) . ", $this->_limit";
    //     }
    //     $rs             = $this->_conn->query( $query );// or die($conn->error);
     
    //     while ( $row = $rs->fetch_assoc() ) {
    //         $results[]  = $row;
    //     }
     
    //     $result         = new stdClass();

    //     // number of page
    //     $result->page   = $this->_page;

    //     // total item from all page
    //     $result->total  = $this->_total; 

    //     // total item on this page
    //     $result->limit  = $this->_limit; 

    //     // number of the first item on this page
    //     $result->start  = ( ( $this->_page * $this->_limit ) - ($this->_limit -1) ); 
        
    //     // number of the last item on this page
    //     $result->end  = ( $this->_limit * $this->_page );

    //     // data of item on this page
    //     $result->data   = $results;
        
    //     //print_r($query);
    //     return $result;
    // }


}
?>