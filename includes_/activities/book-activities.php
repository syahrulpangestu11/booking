<?php include("includes/activities/script.php"); ?>

<aside id="sidebar" class="inline-block">
	<div class="flat-box" id="search">
		<h3 class="grey">Search Another Hotels</h3>
		<?php include("includes/form.search.hotel.php"); ?>
	</div>
</aside>
    
<div id="right-content" class="inline-block">

	<div id="breadcrumb" class="white-box border-box top-right-box">
		<?php
		$breadcrumb = array($uri2,$uri4,$uri5,$uri6,$uri7,$uri8);
		foreach ($breadcrumb as $key => $value) {
			$value = str_replace("-", " ", $value);
			?>
			<a class="breadcrumb text" id="breadcrumb-<?=$key;?>" href="#"><?=$value;?></a> 	
			<?php
			if($key < count($breadcrumb)-1){ echo ">"; }
		} 
		?>
	</div>

	<div class="white-box border-box">
    <?php include('book-activities-detail.php'); ?>
    </div>
	
</div> <!-- end of #single-content -->
