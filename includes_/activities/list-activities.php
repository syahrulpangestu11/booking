<?php
	//PAGINATION
	if($uri2=="allitem"){
		$batas=10;
	}else{
		$batas=15;
	}
	$pgn_qs = "show-activities";
	include_once("includes/paging-php/pagination.config.php");
	include_once("includes/function-php/function.striphtmltags.php");

	//DESTINATION
	$destination_query = (!empty($destination)) ? " AND cityname='$destination' " : "" ;
	
	//SORT BY
	if($_REQUEST['sortby']=="oid-asc"){
		$orderby = " itemoid ASC ";
	}else if($_REQUEST['sortby']=="oid-desc"){
		$orderby = " itemoid DESC ";
	}else if($_REQUEST['sortby']=="name-asc"){
		$orderby = " itemname ASC ";
	}else if($_REQUEST['sortby']=="name-desc"){
		$orderby = " itemname DESC ";
	}else if($_REQUEST['sortby']=="price-desc"){
		$orderby = " minrate DESC ";
	}else{
		$orderby = " minrate ASC ";
	}
		
	//Main Query
	$s_item_part1="
		SELECT i.itemoid, i.itemname, i.picture, i.headline, i.description, i.itemtypeoid, c.cityname, state.statename, country.countryname, continent.continentname, i.multipricetypeoid,
		mp.multipriceoid, '1' AS currencyoid,
		
		CASE WHEN i.multipricetypeoid = 0 THEN i.currencyoid
		ELSE mp.currencyoid END AS real_currencyoid,
		
		CASE WHEN i.multipricetypeoid = 0 AND i.price > 0 THEN i.price
		WHEN i.multipricetypeoid = 0 AND i.adult > 0 THEN i.adult
		WHEN i.multipricetypeoid = 0 AND i.child > 0 THEN i.child
		WHEN i.multipricetypeoid <> 0 AND mp.rateprice > 0 THEN mp.rateprice
		WHEN i.multipricetypeoid <> 0 AND mp.rateadult > 0 THEN mp.rateadult
		ELSE mp.ratechild END AS real_minrate,
		MIN(
			CASE WHEN i.multipricetypeoid = 0 AND i.price > 0 THEN i.price
			WHEN i.multipricetypeoid = 0 AND i.adult > 0 THEN i.adult
			WHEN i.multipricetypeoid = 0 AND i.child > 0 THEN i.child
			WHEN i.multipricetypeoid <> 0 AND mp.rateprice > 0 THEN mp.rateprice
			WHEN i.multipricetypeoid <> 0 AND mp.rateadult > 0 THEN mp.rateadult
			ELSE mp.ratechild END
		)*(
			CASE WHEN (
				CASE WHEN i.multipricetypeoid = 0 THEN i.currencyoid
				ELSE mp.currencyoid END
			) = 1 THEN 1
			ELSE (SELECT nominal FROM bookkeeping WHERE fromcurrency = (
				CASE WHEN i.multipricetypeoid = 0 THEN i.currencyoid
				ELSE mp.currencyoid END
			) AND tocurrency = 1 ORDER BY dateinput DESC LIMIT 1)
			END
		) AS minrate
	";//WHEN i.multipricetypeoid <> 0 AND mp.child > 0 THEN mp.ratechild
	$s_item_part2="
		FROM item i
		INNER JOIN city c USING (cityoid)
		RIGHT JOIN state USING (stateoid)
		RIGHT JOIN country USING (countryoid) 
		RIGHT JOIN continent USING (continentoid)
		LEFT JOIN (
			SELECT * FROM multiprice
			WHERE multiprice.typeid = 'item'
		) mp ON i.itemoid = mp.idreference
	";
	$s_item_part3=" 
		WHERE i.status IN ('1','2')
	";
	$s_item_part4="
		GROUP BY i.itemoid
	";
	$s_item_part5="
		ORDER BY $orderby
	";
	
	if($uri2 == "activities"){
		$s_group_package=$s_item_part1.$s_item_part2.$s_item_part3.$destination_query.$s_item_part4.$s_item_part5;
		$limit = " LIMIT $posisi,$batas ";
	}
	
	$q_group_package=mysqli_query($conn, $s_group_package.$limit) or die(mysqli_error());
	$n_group_package=mysqli_num_rows(mysqli_query($conn, $s_group_package));
	if($n_group_package>0) {
		
		while($group_package=mysqli_fetch_array($q_group_package)){
			$group_name=$group_package['itemname'];
			$show_itemname = stripslashes($group_name);
			$encoded_itemname = str_replace('%2F', '_', urlencode( str_replace(" ", "-", $show_itemname)));
			
			$city = strtolower($group_package['cityname']); 
			$state = strtolower($group_package['statename']);
			$country = strtolower($group_package['countryname']);
			$continent = strtolower($group_package['continentname']);

			
			$item_minrate_currency = $group_package['currencyoid'];
			$item_minrate_price = number_format($group_package['minrate']);
			$unformatted_price = $group_package['minrate'];
			$display_price = ($item_minrate_currency == 1) ? substr_replace($item_minrate_price, "K", -4) : $item_minrate_price ;
			
			$ratecurrency = $group_package['real_currencyoid'];
			$rate = $group_package['real_minrate'];
			include('includes/convertcurrency.php');
			
			/*------------------------------------------------------------------------------*/
			$encode_group_name=str_replace('%2F', '_', urlencode($group_name));
			
			$itemoid = encodeID($group_package['itemoid']);
			$tanggal = date("Y-m-d",strtotime($default_checkin));
			$href= $base_url.'/'.$uri2.'/request/'.$continent."/".$country."/".$state."/".$city."/".$encoded_itemname."/".$itemoid;
			
			//when image doesn't exist, use default-thumb.png
			$pict = ($group_package['picture'] != "") ? $group_package['picture'] : $base_url."/images/default-thumb.png";
			$destination_label = $group_package['cityname'];
			$showdetail = (!empty($group_package['headline'])) ? $group_package['headline'] : $group_package['description'] ;
			$showdetail = stripHtmlTags($showdetail);
			
			$itemtype = ($group_package['itemtypeoid']=='0') ? "Activity" : "Transport" ;
			
			/********************************************************************************/
			?>
				<li class="product inline-block top white-box border-box overflow-hidden">
					<div class="thumb pict fake-link"><img class="lazy" src="<?=$pict;?>" alt="<?=$show_itemname;?>" title="<?=$show_itemname;?>"></div>
					<div class="ribbon center border-box fake-link"><h3><?=$show_itemname;?></h3></div>
                    <div class="detail">
                    	<div class="small-desc fake-link"><?=$showdetail;?></div>
                    </div>
                    <a href="<?=$href;?>">
                    <div class="tosca-box fake-link">
                        <div class="price inline-block"><h3>start from <span><?=$currencycode." ".$display_price;?></span> /pax</h3></div>
                        <h2>Book Now</h2>
                    </div>
                    </a>
				</li>	
            <?php   
			/********************************************************************************/
		}
	}

if(($n_group_package > $batas) and (!empty($uri2))){ 
	$pgn_tampil2=$s_group_package; 
	//$pgn_qs = "ot_page";
	$pgn_container_id = "pagination-ot";
	$pgn_title = "Activities";
	include("includes/paging-php/pagination.nav.php");
}
?>
