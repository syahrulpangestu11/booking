<style type="text/css">
body, table, p{
	font-family:"Calibri";
	font-size:12px;
}
h2{
	font-size:16px;
	margin:0; padding:0;
}

.title{ font-size:14px; font-weight:bold; background-color:#f45831; text-align:center; color:#FFFFFF; }

#lhl img{ 
	width:80px; height:110px; border:3px solid #FFF; margin-top:5px;
	-webkit-box-shadow: 1px 0.1px 8px 1px #CCC; -moz-box-shadow: 1px 0.1px 8px 1px #CCC; box-shadow: 1px 0.1px 8px 1px #CCC;
}

</style>

<?php
$s_data="select * from bookingmst inner join bookingstatus using (bookingstatusoid) inner join currency using (currencyoid) inner join member using (memberoid) where bookingmst.bookingoid='$bookingoid'"; $q_data=mysqli_query($conn, $s_data) or die ("SQL Error : ".$s_data);
$data=mysqli_fetch_array($q_data);

$s_cguest="select countryname from country inner join member using (countryoid)"; $q_cguest=mysqli_query($conn, $s_cguest) or die ("SQL Error : ".$s_cguest); $cguest=mysqli_fetch_array($q_cguest);

$s_hotel="select hotel.hotelname, hotel.hotelcode, hotel.hoteloid, picture, website from bookingroom inner join hotel using (hoteloid) group by hoteloid limit 1"; $q_hotel=mysqli_query($conn, $s_hotel) or die("SQL Error : ".$s_hotel);
$hotel=mysqli_fetch_array($q_hotel);
$hotelname=$hotel['hotelname'];
$hotelwebsite=$hotel['website'];
$hoteloid=$hotel['hoteloid'];
$hotelcode=$hotel['hotelcode'];
$hotelpicture=$hotel['picture'];

$s_cico="select min(date) as checkin, max(date) as checkout from bookingroomdtl inner join bookingroom using (bookingroomoid) inner join bookingmst using (bookingoid) where bookingoid='$bookingoid'"; $q_cico=mysqli_query($conn, $s_cico) or die("SQL Error : ".$s_cico);
$cico=mysqli_fetch_array($q_cico);
$checkindate=date("d F Y", strtotime($cico['checkin']));
$checkoutdate=date("d F Y", strtotime($cico['checkout']));

$guestemail=$data['email']; 
$guestname="$data[title] $data[firstname] $data[lastname]"; 
$bookingnumber=$data['bookingnumber'];
$bookingdate=date("d F Y H:i:s", strtotime($data['bookingdate']));

$grandtotal=number_format($data['grandtotal']);
$rpgrandtotal=$data['currencycode']." ".$grandtotal;

/*$word=strlen($data['cardnumber']); 
$batas=$word-4; $ccnumber=str_split($data['cardnumber'],1); $b=0;
for($c=0;$c<$word;$c++){ if($c<$batas){ $res[$c]="*"; }else{ $res[$c]="$ccnumber[$c]";} } 
$ccnumber=join("",$res);*/
?>
<p align='justify'>Dear <?php echo $guestname; ?>,<br><br>Thank you so much for your inquiry through our website. We are pleased to confirm your inquiry with below detail :</p>
<b>Name &amp; Contact Information :</b><br>
Name : <?php echo $guestname; ?><br>
Email : <?php echo $guestemail; ?><br>
Mobile Phone : <?php echo $data['mobile']; ?><br>
Phone / Fax : <?php echo $data['phone']; ?> / <?php echo $data['fax']; ?><br>
Address : <?php echo $data['address']; ?> <?php echo $data['city']; ?>, <?php echo $data['state']; ?> <?php echo $data['zipcode']; ?><br>
Country : <?php echo $cguest['countryname']; ?><br><br>
<b>Inquiry Details :</b><br>
<table id="blue-border" style="border-color: #f45831; border-style:solid; border-width:2px; " width="70%">
	<tr>
		<td valign="top"><div id="lhl"><img src="<?php echo"$hotelpicture"; ?> " onerror="ImgErrorSmall(this)" class="nailthumb-container" width="70px" height="100px"></div></td>
		<td valign="top">
        	<h2><?php echo"$hotelname"; ?></h2>
            Booking Number : <?php echo $bookingnumber; ?><br>
            Booking Status : <?php echo $data['statusname']; ?><br />
            Reservation Date : <?php echo $bookingdate; ?><br>
            Check In : <?php echo $checkindate; ?><br>
            Check Out : <?php echo $checkoutdate; ?><br>
            Arrival Time : <?php echo $data['arrivaltime']; ?><br />
            Note : <?php echo $data['note']; ?><br />
		<td>
	<tr>
</table>
<br />
<table width="100%" cellpadding="0" cellspacing="0">
    <tr align="center" class="title"><td>Room</td><td>Date</td><td>Rate</td><td>Note</td></tr>
<?php
$s_broom="select * from bookingroom inner join hotel using (hoteloid) inner join room using (roomoid) where bookingoid='$bookingoid' order by bookingroomoid"; $q_broom=mysqli_query($conn, $s_broom) or die ("SQL Error : ".$s_broom);
$gt_room=0;
while($broom=mysqli_fetch_array($q_broom)){
	$night=0;
	$s_detail="select * from bookingroomdtl inner join currency using (currencyoid) where bookingroomoid='$broom[bookingroomoid]'"; $q_detail=mysqli_query($conn, $s_detail) or die ("SQL Error : ".$s_detail);
	while($detail=mysqli_fetch_array($q_detail)){
		$night++;
		$diskon=$detail['discount']; 
		$tax=$detail['tax']; 
		$currency=$detail['currencycode'];
		$total=number_format($detail['total']);  $rptotal="$currency $total";
		$date=date("d F Y",strtotime($detail['date']));
		$note="";
		
		$gt_room=$gt_room+$detail['total'];
		
		if($detail['offerrateoid']!=0 or !empty($detail['offerrateoid'])){ // special offer
			$s_special="select offername from specialoffer inner join specialroom using (offeroid) inner join specialrate using (offerdtloid) where offerrateoid='$detail[offerrateoid]'"; $q_special=mysqli_query($conn, $s_special) or die($s_special); $special=mysqli_fetch_array($q_special);
			$note=$special['offername'];
			if($detail['total']==0){ $rpprice="free"; $rptotal="free";  }
		}else if($detail['packagerateoid']!=0 or !empty($detail['packagerateoid'])){
			$s_package="select packagename, night from package inner join packageroom using (packageoid) inner join packagerate using (packagedtloid) where packagerateoid='$detail[packagerateoid]'"; $q_package=mysqli_query($conn, $s_package) or die($s_package); $package=mysqli_fetch_array($q_package);
			$note=$package['packagename'];
			if($detail['total']==0 and $night<=$package['night']){ $rptotal="&bull;&bull;&bull;";  
			}else if($detail['total']==0 and $night>=$package['night']){  $rptotal="free";  }
		}
?>
		
        	<?php if($night==1){ ?><tr><td colspan="4"><hr /></td></tr><tr><td><?php echo $broom['name']; ?></td>
            <?php }else{ ?><tr><td>&nbsp;</td>
            <?php } ?>
            <td align="center"><?php echo $date; ?></td>
            <td align="center"><?php echo $rptotal; ?></td>
            <td align="center"><?php echo $note; ?></td>
        </tr>
<?php
	}
}
$formated_gt_room=number_format($gt_room);
$show_gt_room=$currency." ".$formated_gt_room;
?>
<tr><td colspan="4"><hr></td></tr>
<tr><td colspan="3" align="right"><b>Total Room : <?php echo $show_gt_room; ?></b></td></tr>
<tr><td colspan="4"><hr></td></tr>
</table>
<br />
<?php
$s_countitem="select count(bookingitemoid) as jmlitem from bookingitem where bookingoid='".$bookingoid."'";
$q_countitem=mysqli_query($conn, $s_countitem) or die ("SQL Error : ".$s_countitem);
$countitem=mysqli_fetch_array($q_countitem);
if($countitem['jmlitem']>0){
?>
<table width="100%" cellpadding="0" cellspacing="0">
	<tr align="center" class="title"><td>Item</td><td>Qty x Price</td><td>Total</td></tr>
<?php
	$s_item="select bookingitem.*, item.itemname, currency.currencycode from bookingitem inner join currency using (currencyoid) inner join bookingmst using (bookingoid) inner join item using (itemoid) where bookingoid='".$bookingoid."'";
	$q_item=mysqli_query($conn, $s_item) or die ("SQL Error : ".$s_item);
	$gt_item=0;
	while($item=mysqli_fetch_array($q_item)){
		$formated_price=number_format($item['price']);
		$show_price=$item['currencycode']." ".$formated_price;
		$formated_total=number_format($item['total']);
		$show_total=$item['currencycode']." ".$formated_total;
		
		$gt_item=$gt_item+$item['total'];
?>
	<tr>
    	<tr><td colspan="3"><hr /></td></tr>
    	<td><?php echo $item['itemname']; ?></td>
        <td align="center"><?php echo $item['qty']; ?> x <?php echo $show_price; ?></td>
        <td align="center"><?php echo $show_total; ?></td>
    </tr>
<?php
	}
	$formated_gt_item=number_format($gt_item);
	$show_gt_item=$item['currencycode']." ".$formated_gt_item;
?>
<tr><td colspan="4"><hr></td></tr>
<tr><td colspan="3" align="right"><b>Total Item : <?php echo $show_gt_item; ?></b></td></tr>
<tr><td colspan="4"><hr></td></tr>
</table>
<?php
}
?>
<h2>Grand Total : <?php echo $rpgrandtotal; ?></h2>
<p>
Please do not hesitate to contact us, we are always on hand to assist with your inquiry<br><br> Warm Regards,<br>Abbey Travel</p>