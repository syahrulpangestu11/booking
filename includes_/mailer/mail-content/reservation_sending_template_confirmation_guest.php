<?php

$mail = new PHPMailer(true); // the true param means it will throw exceptions on errors, which we need to catch
$mail->IsSMTP(); // telling the class to use SMTP
try {
    
    // $mail->SMTPDebug = 1;
    // //Ask for HTML-friendly debug output

    $sql_mailer = "SELECT * FROM mailercompany INNER JOIN mailertype USING (mailertypeoid) WHERE companyoid='1' and publishedoid='1'";
    $qrun_mailer = $db->query($sql_mailer);
    $run_mailer = $qrun_mailer->fetchAll(PDO::FETCH_ASSOC);
    foreach($run_mailer as $row_mailer){
        switch($row_mailer['mailertypeoid']){
            case 1 : $mail->SetFrom($_profile['SMTP_Username'], $row_mailer['name']); break;
            case 2 : $mail->AddAddress($row_mailer['email'], $row_mailer['name']); break;
            case 3 : $mail->AddCC($row_mailer['email'], $row_mailer['name']); break;
            case 4 : $mail->AddBCC($row_mailer['email'], $row_mailer['name']); break;
            case 5 : $mail->AddReplyTo($row_mailer['email'], $row_mailer['name']); break;
        }
    }
    
    // $mail->AddCC($agent_mail,$agent_name);
    $mail->AddAddress($guestemail,$guestname);
    $mail->Subject = $_profile['name']." [G] Booking Confirmation for [".$hotelname."] - [".$bookingnumber."]";
    $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!'; // optional - MsgHTML will create an alternate automatically
    $mail->MsgHTML($body);
    if($mail->Send()) {
        include('reservation_sending_template_confirmation_agent.php');
    }
    
} catch (phpmailerException $e) { echo"<b>Php Mailer Error [guest] :</b><br>"; echo $e->errorMessage();//Pretty error messages from PHPMailer
} catch (Exception $e) { echo"<b>All Error :</b><br>"; echo $e->getMessage(); //Boring error messages from anything else!
} 

?>