<?php

    $mail = new PHPMailer(true); // the true param means it will throw exceptions on errors, which we need to catch
    $mail->IsSMTP(); // telling the class to use SMTP
    try {
        $sql_mailer = "SELECT * FROM mailercompany INNER JOIN mailertype USING (mailertypeoid) WHERE companyoid='1' and publishedoid='1'";
        $qrun_mailer = $db->query($sql_mailer);
        $run_mailer = $qrun_mailer->fetchAll(PDO::FETCH_ASSOC);
        foreach($run_mailer as $row_mailer){
            switch($row_mailer['mailertypeoid']){
                case 1 : $mail->SetFrom($_profile['SMTP_Username'], $row_mailer['name']); break;
                case 2 : $mail->AddAddress($row_mailer['email'], $row_mailer['name']); break;
                case 3 : $mail->AddCC($row_mailer['email'], $row_mailer['name']); break;
                case 4 : $mail->AddBCC($row_mailer['email'], $row_mailer['name']); break;
                case 5 : $mail->AddReplyTo($row_mailer['email'], $row_mailer['name']); break;
            }
        }
        
        $mail->AddAddress($agent_mail,$agent_mail);
        $mail->Subject = $_profile['name']." [G] Booking Inquiry for [".$hotelname."] - [".$bookingnumber."]";
        $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!'; // optional - MsgHTML will create an alternate automatically
        $mail->MsgHTML($body_inquiry);
        
        unset($_SESSION['tokenSession']);
        unset($bookingoid);
        if($mail->Send()) {
            ?>
            <form name="mypost" action="<?php echo"$base_url/book/confirmation/$pagesuccess"; ?>" method="post">
                <input type="hidden" name="grandtotal" value="<?php echo $_SESSION['grandtotal']; ?>">
                <input type="hidden" name="currencyoid" value="<?php echo $_SESSION['currencytotal']; ?>">
                <input type="hidden" name="userkbca" value="<?php echo $_POST['userkbca']; ?>">
                <input type="hidden" name="bookingnumber" value="<?php echo $bookingnumber; ?>">
                <input type="hidden" name="paymentmethodoid" value="<?php echo $paymentmethodoid; ?>">
                <input type="hidden" name="bookingstatusoid" value="<?php echo $bookingstatusoid; ?>">
            </form>
            <script>
            document.forms["mypost"].submit();
            </script>
            <?php
        }
        
    } catch (phpmailerException $e) { echo"<b>Php Mailer Error [guest] :</b><br>"; echo $e->errorMessage();//Pretty error messages from PHPMailer
    } catch (Exception $e) { echo"<b>All Error :</b><br>"; echo $e->getMessage(); //Boring error messages from anything else!
    } 

?>