<?php
class HotelEmailTemplate extends PHPMailer{
    private $_template;
    private $_template_with_data;
    private $_email_type;
    private $_email_to;
    private $_email_array;
    private $_email_direction;
    private $_subject;
    private $_data;
    private $_body;
    private $_db;
    
    //$booking_number;$booking_min_checkin;$booking_max_checkout;$booking_currency;$booking_extra;$booking_grand_deposit;$booking_grand_balance;$booking_grand_cancellation;$booking_cancellation_reason;$booking_total_room_rate;$booking_total_extrabed_rate;$booking_grandtotal;$booking_email_date;$hotel_name;$hotel_address;$hotel_city;$hotel_email;$hotel_banner;$user_ip;$user_browser;$guest_firstname;$guest_lastname;$guest_country;$company_imglogo;$company_name;$company_address;$company_city;$company_state;$company_country;$company_phone;$company_mobile;$company_email;$company_web;$_start_detail;$detail_no;$detail_adult;$detail_child;$detail_room_total;$detail_country;$detail_occupancy;$detail_checkin;$detail_checkout;$detail_room;$detail_extrabed;$detail_note;$detail_inclusion;$detail_number_room;$detail_breakfast;$detail_total;$_end_detail;$_start_detail_extra;$detail_extra_no;$detail_extra_name;$detail_extra_qty;$detail_extra_rate;$detail_extra_total;$_end_detail_extra;
    /*
        $booking_number;$booking_min_checkin;$booking_max_checkout;$booking_currency;$booking_extra;$booking_grand_deposit;$booking_grand_balance;$booking_grand_cancellation;
        $booking_cancellation_reason;$booking_total_room_rate;$booking_total_extrabed_rate;$booking_grandtotal;$booking_email_date;
        
        $hotel_name;$hotel_address;$hotel_city;$hotel_email;$hotel_banner;
        
        $user_ip;$user_browser;
        
        $guest_firstname;$guest_lastname;$guest_country;
        
        $company_imglogo;$company_name;$company_address;$company_city;$company_state;$company_country;$company_phone;$company_mobile;$company_email;$company_web;
        
        $_start_detail;
        $detail_no;$detail_adult;$detail_child;$detail_room_total;$detail_country;$detail_occupancy;$detail_checkin;$detail_checkout;$detail_room;
        $detail_extrabed;$detail_note;$detail_inclusion;$detail_number_room;$detail_breakfast;$detail_total;
        $_end_detail;
        
        $_start_detail_extra;
        $detail_extra_no;$detail_extra_name;$detail_extra_qty;$detail_extra_rate;$detail_extra_total;
        $_end_detail_extra;
    */

    public function __construct($db, $email_type, $email_to, $data, $mailer){
        $this->_db=$db;
        $this->_email_type=$email_type;
        $this->_email_to=$email_to;
        $this->_email_array=$mailer;
        $this->_data=$data;
    }
    
    function setBody($body){ $this->_body=$body;}
    function setSubject($subject){ $this->_subject=$subject;}
    function getTemplate(){ $this->getTemplateWithData(); echo $this->_template_with_data; }

    function sendTemplate($nextTemplateArray,$callBack){
        // Func::logJS();
        if(is_array($nextTemplateArray))
        foreach($nextTemplateArray as $key => $value) if($key>0) $newNextTemplateArray[] = $value;
        $mail = new PHPMailer(true); // the true param means it will throw exceptions on errors, which we need to catch
        $mail->IsSMTP(); // telling the class to use SMTP
        try {
            $mail->SMTPDebug = 1; //Ask for HTML-friendly debug output
        // Func::logJS($this->_email_array,'filterMailer');
            $hasAddress= false;
            foreach($this->_email_array as $data){
                foreach($data as $row_mailer){
                    switch($row_mailer['mailertype']){
                        case 'from' : if($row_mailer['email']!='')$mail->SetFrom($row_mailer['email'], $row_mailer['name']); break;
                        case 'to' : if($row_mailer['email']!=''){$mail->AddAddress($row_mailer['email'], $row_mailer['name']); $hasAddress= true;} break;
                        case 'cc' : if($row_mailer['email']!='')$mail->AddCC($row_mailer['email'], $row_mailer['name']); break;
                        case 'bcc' : if($row_mailer['email']!='')$mail->AddBCC($row_mailer['email'], $row_mailer['name']); break;
                        case 'ReplyTo' : if($row_mailer['email']!='')$mail->AddReplyTo($row_mailer['email'], $row_mailer['name']); break;
                    }
                }
            }
            $mail->Subject = $this->_subject;
            $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!'; // optional - MsgHTML will create an alternate automatically
            $mail->MsgHTML($this->_body);
            // die();
            if($hasAddress){
                if($mail->Send()) {
                    $callBack($newNextTemplateArray,true,'Email Sent to '.$this->_email_to);
                }
            }else{
                $callBack($newNextTemplateArray,false,'No '.$this->_email_to.' Email Address (No Receiver)');
            }
        } catch (phpmailerException $e) { 
            $callBack($newNextTemplateArray,false,'<b>Php Mailer Error [to:'.$this->_email_to.'] :</b><br>'.$e->errorMessage());
        } catch (Exception $e) { 
            $callBack($newNextTemplateArray,false,'<b>All Error [to:'.$this->_email_to.'] :</b><br>'.$e->errorMessage());
        } 
    }
    // -------------------------------------------

    function getQuery(){ 
        $query = $this->_db->query("SELECT * FROM mastertemplate_email WHERE masterprofileoid = 1 AND publishedoid = 1 
                                            AND email_type = '$this->_email_type' AND email_to = '$this->_email_to'");

        $result = $query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    function filterMailer(){
        $mailer = $this->_email_array;
        $mailer_new = array();
        foreach($mailer as $key => $value){
            $new_value = array();
            if($key=='company') {
                foreach($value as $key4 => $value4) {
                    if($value4['mailertype']=='from')  $mailer[$key][$key4]['email'] = $this->From;
                    $new_value[] = $value4;
                }
            }else{
                foreach($this->_email_direction as $key2 => $value2){
                    if(in_array($key,$value2)) {
                        foreach($value as $data){
                            if($data['mailertype']==$key2) $new_value[] = $data;
                        }
                    }
                }
            }
            if(count($new_value)>0) 
            $mailer_new[$key]=$new_value;
        }
        // logJS($mailer_new,'filterMailer');
        $this->_email_array = $mailer_new;
    }

    private function getTemplateWithData(){
        $result = $this->getQuery();
        $this->_template = $result['email_body'];

        $tmp = array();
        $tmp['to'] = explode(',',$result['email_to']);
        $tmp['cc'] = explode(',',$result['email_cc']);
        $this->_email_direction = $tmp;

        $this->filterMailer();

        $_template = $this->_template;

        foreach($this->_data as $key => $dataArray){
            $isLoop = preg_match('/^[_](.*)$/', $key, $dataLoop);
            $dataName = ( $isLoop ? $dataLoop[1] : $key );

            if($isLoop){
                $_template = $this->replaceDataLoop($dataName,$dataArray,$_template);
            }else{
                $_template = $this->replaceDataArray($dataName,$dataArray,$_template);
            }

        }
        
        $this->_template_with_data = $_template;
    }

    private function replaceDataArray($dataName,$dataArray,$_template){

        //if company then remove prefix
        $regex = ( $dataName == 'company' ? '/[\A$]'.$dataName.'_(\w+)[;\z]/' : '/[\A$]('.$dataName.'_\w+)[;\z]/' );

        preg_match_all($regex, $_template, $_template_match);
        
        foreach($_template_match[0] as $key => $value){ 
            $_template = str_replace($value,$dataArray[$_template_match[1][$key]],$_template); 
        }

        return $_template;
    }

    private function replaceDataLoop($dataName,$dataArray,$_template){

        preg_match_all('/[$]_start_'.$dataName.';(.*?)[$]_end_'.$dataName.';/sm', $_template, $_template_match_loop);
        preg_match_all('/[\A$]'.$dataName.'_(\w+)[;\z]/', $_template_match_loop[1][0], $_template_match_loop_item);

        $_template = preg_replace('/[$]_start_'.$dataName.';(.*?)[$]_end_'.$dataName.';/sm', '$_loop_'.$dataName.'_here;', $_template); 
        $html_loop = ''; 

        foreach($dataArray as $data){
            $template_loop = $_template_match_loop[1][0];

            foreach($_template_match_loop_item[0] as $key => $value){ 
                $template_loop = str_replace($value,$data[$_template_match_loop_item[1][$key]],$template_loop);  
            }

            $html_loop .=  $template_loop; 
        }
        
        $_template = str_replace('$_loop_'.$dataName.'_here;', $html_loop, $_template);

        return $_template;
    }

}
?>