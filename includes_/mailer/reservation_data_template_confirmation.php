<?php


    $s_data="SELECT booking.*, customer.*, currency.*, bookingstatus.note as status, bookingpayment.*, country.countryname 
    FROM booking 
        inner join bookingstatus using (bookingstatusoid) 
        inner join currency using (currencyoid) 
        inner join customer using (custoid) 
        inner join country using (countryoid) 
        left join bookingpayment using (bookingoid) 
    WHERE booking.bookingoid = '".$bookingoid."' group by booking.bookingoid";
    $q_data = $db->query($s_data);
    $data = $q_data->fetch(PDO::FETCH_ASSOC);
    // echo "<script>console.log(".json_encode($data).");</script>"; 

    $s_hotel = "SELECT h.hoteloid, h.hotelname, min(checkin) as mincheckin,  max(checkout) as maxcheckout,sum(roomtotal) as roomtotal, 
    sum(extrabedtotal) as extrabedtotal, ct.cityname , h.address   ,h.email as hotelemail,h.banner
    FROM bookingroom 
    inner join roomoffer ro using (roomofferoid)   
    inner join room r using (roomoid)  
    inner join hotel h using (hoteloid) 
    inner join city ct using (cityoid)
    WHERE bookingoid = '".$bookingoid."'
    group by h.hoteloid";
    $q_hotel = $db->query($s_hotel);
    $hotel = $q_hotel->fetch(PDO::FETCH_ASSOC);
    // echo "<script>console.log(".json_encode($hotel).");</script>"; 
            
    $s_room_promo = "SELECT br.*, r.hoteloid, ro.name as roomname, r.adult as maxadult, ch.name as channelname, 
        count(bookingroomoid) as jmlroom, sum(total) as total, sum(br.extrabed) as extrabed, c.currencycode, checkin, checkout ,b.note,
        sum(br.adult) as adulttotal, sum(br.child) as childtotal, IFNULL(p.name,'') as promotion
    FROM bookingroom br
        inner join currency c using (currencyoid)
        inner join booking b using (bookingoid) 
        left join promotion p using (promotionoid)
        left join promotiontype pt using (promotiontypeoid)
        inner join roomoffer ro using (roomofferoid)
        inner join offertype ot using (offertypeoid)
        inner join room r using (roomoid)
        inner join channel ch using (channeloid)
    WHERE b.bookingoid = '".$bookingoid."' 
    group by r.roomoid, ro.roomofferoid, channeloid, promotionoid, checkin, checkout";
    // echo "<script>console.log(".json_encode($s_room_promo).");</script>"; 
    $q_room_promo = $db->query($s_room_promo);
    $rp_data = $q_room_promo->fetchAll(PDO::FETCH_ASSOC);
    // echo "<script>console.log(".json_encode($q_room_promo).");</script>"; 
    // echo "<script>console.log(".json_encode($rp_data).");</script>"; 

    $s_cancellation = "SELECT cp.description 
    FROM cancellationpolicy cp
        left join promotion p using (cancellationpolicyoid) 
        inner join bookingroom br using (promotionoid) 
        inner join booking b using (bookingoid) 
    WHERE b.bookingoid = '".$bookingoid."' 
    group by cp.cancellationpolicyoid";
    $q_cancellation = $db->query($s_cancellation);
    $cancellation_data = $q_cancellation->fetchAll(PDO::FETCH_ASSOC);
    $cancellation_note = '';
    foreach($cancellation_data as $cancellation){ $cancellation_note .= "<p>".$cancellation['description']."</p>"; }

    // ----------------------------------------------------------------------------------------------------------------

    $arrDataBooking = array();
    $arrDataBooking['booking_cancellation'] = $cancellation_note;
    $arrDataBooking['booking_min_checkin'] = date("d F Y", strtotime($hotel['mincheckin']));
    $arrDataBooking['booking_max_checkout'] = date("d F Y", strtotime($hotel['maxcheckout']));
    $arrDataBooking['booking_number'] = $data['bookingnumber'];
    $arrDataBooking['booking_email_date'] = date("d F Y");
    $arrDataBooking['booking_total_room_rate'] = $data['currencycode']." ".number_format($hotel['roomtotal']);
    $arrDataBooking['booking_total_extrabed_date'] = ($hotel['extrabedtotal']>0?date("d F Y", strtotime($hotel['mincheckin'])).' - '.date("d F Y", strtotime($hotel['maxcheckout'])):'-');
    $arrDataBooking['booking_total_extrabed_rate'] = ($hotel['extrabedtotal']>0?$data['currencycode']." ".number_format($hotel['extrabedtotal']):'-');
    $arrDataBooking['booking_grand_total'] = $data['currencycode']." ".number_format($data['grandtotal']);
    $arrDataBooking['booking_currency'] = $data['currencycode'];//
    $arrDataBooking['booking_extra'] = '';//
    $arrDataBooking['booking_grand_deposit'] = '';//
    $arrDataBooking['booking_grand_balance'] = '';//
    $arrDataBooking['booking_grand_cancellation'] = '';//
    $arrDataBooking['booking_cancellation_reason'] = '';//
    // $arrDataBooking['booking_hotel_collect'] = $data['currencycode']." ".number_format($data['hotelcollect']);//new

    $arrDataUser = array();
    $arrDataUser['user_ip'] = $data['user_ip'];//
    $arrDataUser['user_browser'] = $data['user_browser'];//

    $arrDataHotel = array();
    $arrDataHotel['hotel_banner'] = (!empty($hotel['banner'])?$hotel['banner']:'https://www.thebuking.com/ibe/image/header.jpg');//new
    $arrDataHotel['hotel_name'] = $hotel['hotelname'];
    $arrDataHotel['hotel_email'] = $hotel['hotelemail'];//
    $arrDataHotel['hotel_address'] = $hotel['address'];
    $arrDataHotel['hotel_city'] = $hotel['cityname'];

    $arrDataGuest = array();
    $arrDataGuest['guest_email'] =  $data['email'];//
    $arrDataGuest['guest_phone'] =  $data['phone'];//
    $arrDataGuest['guest_firstname'] = $data['firstname'];
    $arrDataGuest['guest_lastname'] =  $data['lastname'];
    $arrDataGuest['guest_country'] =  $data['countryname'];

    $arrDataRoom = array(); $num=0;
    foreach($rp_data as $rp){
        $tmp = array();$num++;
        $tmp['no'] = $num;//
        $tmp['country'] = $data['countryname'];
        $tmp['occupancy'] = $rp['maxadult'];
        $tmp['adult'] = $rp['adulttotal'];//new
        $tmp['child'] = $rp['childtotal'];//new
        $tmp['checkin'] = date("d F Y", strtotime($rp['checkin']));
        $tmp['checkout'] = date("d F Y", strtotime($rp['checkout']));
        $tmp['room'] =  $rp['roomname'].(empty($rp['promotion'])?'':' ('.$rp['promotion'].')');
        $tmp['note'] = $rp['note'];
        $tmp['extrabed'] = $rp['extrabed'];
        $tmp['inclusion'] = (empty($rp['extrabed'])?'':$rp['extrabed'].' Extrabed(s)');
        $tmp['number_room'] = $rp['jmlroom'];
        $tmp['breakfast'] = ($rp['breakfast']=='y'?'Yes':'No');
        $tmp['room_total'] = $rp['currencycode']." - ".number_format($rp['roomtotal']);
        $tmp['total'] = $rp['currencycode']." - ".number_format($rp['total']);
        array_push($arrDataRoom, $tmp);
    }

    $arrDataExtra = array();

    $arrDataTemplate = array();
    $arrDataTemplate['company'] = $_profile;
    $arrDataTemplate['booking'] = $arrDataBooking;
    $arrDataTemplate['user'] = $arrDataUser;
    $arrDataTemplate['hotel'] = $arrDataHotel;
    $arrDataTemplate['guest'] = $arrDataGuest;
    $arrDataTemplate['_detail'] = $arrDataRoom;
    $arrDataTemplate['_detail_extra'] = $arrDataExtra;


?>