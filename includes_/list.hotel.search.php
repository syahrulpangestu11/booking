
<?php //LIST HOTEL

// include('../conf/baseurl.php'); 
include('../conf/connection.php'); 
include("function.star.php"); 
include_once("function.randomstring.php");

require_once 'class/Paginator.class.php'; 
require_once 'class/models/SearchHotel.class.php';

//(ALL) Load DB and XML: Hotel,Room,Allotment,Rate ---------------------------------
$ListNew = new SearchHotel($conn);
$all_hotels = $ListNew->getData($channeloid);
$filter_hotels = $ListNew->getFilterData($all_hotels);

//(SELECTED) Paging Array and Load DB: Photo,Content ---------------------------------
$Paginator = new Paginator($filter_hotels);
$show_hotels = $Paginator->getData();
$ListNew->loadPhotoContent($show_hotels,$_GET['uri4']);
//echo '<script>console.log('.json_encode($show_hotels).');</script>';

?>

<ul class="content-list white-box border-box" id="list-hotel">
<?php
    foreach($show_hotels->data as $hotel){
        
        $parameters = ( isset($uri4) or !empty($uri4) or $uri4 != "" ) ? "/".$uri4 : "";
        $href_hotel = $base_url_."/hotel/"
                        .strtolower($hotel['continent'])."/".strtolower($hotel['country'])
                        ."/".strtolower($hotel['state'])."/".strtolower($hotel['city'])
                        ."/".str_replace(" ", "-", $hotel['hotelname'] )."/"
                        .randomString(4).$hotel['hoteloid'].randomString(2).$parameters;
        //$href_hotel = $hotel['hotelhref']; 
        ?>
            <li id="h-<?=$hotel['hoteloid']?>" style="overflow:auto;">
            <div class="row-fluid">
                <div class="col-md-3 inline-block top center">
                    <a href="<?=$href_hotel;?>">
                    <div class="pict thumb"><img src="<?=$hotel['hotelpicture'];?>"></div>
                    </a>
                    <a href="#googlemapsview" class="button view-map nyroModal"> View Map</a>
                    <!-- Go to www.addthis.com/dashboard to customize your tools -->
                    <div class="addthis_sharing_toolbox"></div>
                    <!-- Go to www.addthis.com/dashboard to customize your tools -->
                    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-543ce7af1ad924c7" async></script>

                </div>
                <div class="col-md-6 inline-block top">
                    <a href="<?=$href_hotel;?>">
                        <div>
                            <span class="title"><?=$hotel['hotelname'];?></span>
                            <div class="star inline-block top"><?php starRating($hotel['stars'], 2);?></div>
                        </div>
                        <div><i class="small-desc"><?=$hotel['address'];?></i></div>
                    </a>
                    <div> 
                        <div class="inline-block">
                            <?=$hotel['city'];?> . <span class="capitalize"><?=$hotel['hoteltype'];?></span> - &quot;There are 4 people looking at this hotel.&quot;
                        </div>
                    </div>
                    <div><?=$hotel['hotelheadline'];?></div>
                </div>
                
                <div class="col-md-3 inline-block top">
                    <div class="price">
                        start from 
                        <h4><span class="strikethrough light-grey">
                            <?php //if($ratenet > $hotel['minrate']){ echo $hotel['currencycode']."&nbsp;". $hotel['shownetrate']."<br>";  }?>
                        </span></h4>
                        <h2 class="rate">
                            <span class="grey"><?=$hotel['currencycode'];?></span>
                            <span class="blue"><?=$hotel['hotel_minrate_final'];?></span>
                        </h2>
                    </div>
                    <div class="bottom-right"><a class="button book" href="<?=$href_hotel;?>">BOOK NOW</a></div>
                </div>
            </div>
                
            <div class="row-fluid">
                <div class="col-md-12 listroom">
                <ul>
                    <?php
                        $qx = 1;
                        foreach($hotel['rooms'][0] as $key => $name){
                            //foreach($hotel['rooms'] as $key => $name){
                            echo "<li><a href='".$href_hotel."'>";
                            //echo "<span class='tosca-bold fl_right'>".$hotel['rates'][$key]."</span>";
                            echo "<span class='tosca-bold fl_right'>".$hotel['rates'][0][$key]."</span>";
                            echo "<span class='tosca-bold'>".$name."</span>";
                            echo "</a></li>";
                            if($qx == 5) break;
                            $qx++;
                        }
                    ?>
                </ul>
                </div>
            </div>
            </li>
    <?php
    }
    
    if($show_hotels->total == 0){
    ?>
        <img src="<?=$base_url;?>/images/warning.png" class="inline-block top" />
        <div class="inline-block top">Sorry, no results match your criteria.</div>
    <?php
    }
    ?>
</ul>

<?php echo $Paginator->createLinks('pagination pagination-sm'); ?>

