<?php
include('../conf/connection.php');
include('../conf/xmlload.php');

$todaydate = date("Y-m-d"); 
$tknSession = $_GET['tkn']; 
$type = $_GET['type'];
$offerroomoid = $_GET['id']; 
$id = $_GET['id']; 
$delid = $_GET['delid'];
$adult = $_GET['adult']; $child = $_GET['child'];  
$numroom = $_GET['numroom'];
$roomoid = $_GET['roomoid']; 
$jmlroom = $_GET['jmlroom'];  

$night = $_GET['night'];
$checkin = date('Y-m-d', strtotime($_GET['checkin'])); 

$total = 0;

$s_date="select date_add('$checkin', interval $night day) as getDate"; $q_date=mysqli_query($conn, $s_date) or die ("SQL Error : ".$s_date); $a_date=mysqli_fetch_array($q_date); 
$checkout=$a_date['getDate'];

$s_promotion = "select p.*, pa.*, r.* from promotion p 
inner join discounttype dt using (discounttypeoid)
inner join discountapply da using (discountapplyoid)
inner join promotionapply pa using (promotionoid)
inner join room r using (roomoid)
inner join rateplan rp using (rateplanoid)
inner join channel c using (channeloid)
where
p.publishedoid = '1' and pa.promotionapplyoid = '".$id."'
group by p.promotionoid
";
$q_promotion = mysqli_query($conn, $s_promotion) or mysqli_error(); 
$promotion = mysqli_fetch_array($q_promotion);

	$hoteloid = $promotion['hoteloid'];
	$roomoid = $promotion['roomoid'];
	$rateplanoid = $promotion['rateplanoid'];
	$channeloid = $promotion['channeloid'];
	$promotionoid = $promotion['promotionoid'];
	$discounttype = $promotion['discounttypeoid'];
	$discountapply = $promotion['discountapplyoid'];
	$applyvalue = explode(',', $promotion['applyvalue']);
	$discountvalue = $promotion['discountvalue'];
	
/*-----------------------------------------------------------------------*/
if(!isset($delid) or $delid==0){
	$s_getid = "select bookingtempoid from bookingtempdtl inner join bookingtemp using (bookingtempoid) where session_id = '".$tknSession."' and promotionoid = '".$promotionoid."' and roomoid = '".$roomoid."' and rateplanoid = '".$rateplanoid."' and channeloid = '".$channeloid."' group by bookingtempoid";
	$q_getid = mysqli_query($conn, $s_getid) or mysqli_error(); 
	while($getid=mysqli_fetch_array($q_getid)){
		$s_delete = "delete bookingtemp, bookingtempdtl from bookingtemp, bookingtempdtl where bookingtemp.bookingtempoid = bookingtempdtl.bookingtempoid and bookingtemp.bookingtempoid = '".$getid['bookingtempoid']."'";
		$q_delete = mysqli_query($conn, $s_delete) or mysqli_error(); 
	}
}else{
	$s_delete = "delete bookingtemp, bookingtempdtl from bookingtemp, bookingtempdtl where bookingtemp.bookingtempoid = bookingtempdtl.bookingtempoid and bookingtemp.bookingtempoid = '".$delid."'";
	$q_delete = mysqli_query($conn, $s_delete) or mysqli_error(); 
}
/*-----------------------------------------------------------------------*/

	if($adult == 0  and $child ==0){ 
		$adult = $promotion['adult']; $child = $promotion['child']; 
	}
	
	//include('fnc.deleterow-hotel.php');

	if($promotion['discounttypeoid'] == 4){
		$night_after = $night + $promotion['discountvalue'];
	}else{
		$night_after = $night;
	}
	
	for($r = 1; $r<=$jmlroom; $r++){
		$total_room = 0;
		
		if($_GET['numroom']==0){
			$numroom = $r; 
		}
		
		echo "<div dtl=".$type."-".$id."-".$numroom.">";
		
		for($n = 0; $n<$night_after; $n++){
			unset($rate, $rate_after, $discountpercent, $discount);
			
			$dateformat = explode('/',date("D/Y-m-d",strtotime($checkin." +".$n." day")));
			$day = $dateformat[0];
			$date = $dateformat[1];
			
			$query_tag = '//hotel[@id="'.$hoteloid.'"]/room[@id="'.$roomoid.'"]/rate[@date="'.$date.'"]/rateplan[@type="'.$rateplanoid.'"]/channel[@type="'.$channeloid.'"]';
			
			foreach($xml->xpath($query_tag) as $xml_rate){
				$rate = $xml_rate->single;
				$currency = $xml_rate->currency;
				$breakfast = $rate->breakfast;
			}
			if( 
				$discountapply == '1' or
				($discountapply == '2' and in_array(($n+1), $applyvalue)) or
				($discountapply == '3' and in_array($day, $applyvalue)) or
				($discountapply == '4' and $n = 0) or
				($discountapply == '5' and $n = ($night - 1))
			){
				if($discounttype == '1'){
					$discountnote = $discountvalue."%";
					$discount = $rate * $discountvalue / 100;
				}else if($discounttype == '3'){
					$discountnote = $discountvalue;
					$discount = $discountvalue;
				}
				
				$rate_after = $rate - $discount;
			}else if(($n+1) > $night){
				$rate = 0;
				$rate_after = 0;
			}else{
				$rate_after = $rate;
			}
			
			$total_room = $total_room + $rate_after;
			
			$time = date('Y-m-d H:i:s');
			
			if($n == 0 ){
				$s_temp = "insert into bookingtemp (submittime, session_id, roomoid, rateplanoid, channeloid, promotionoid, checkin, night, adult, child, flag, breakfast) values ('".$time."', '".$tknSession."', '".$roomoid."', '".$rateplanoid."', '".$channeloid."', '".$promotionoid."', '".$checkin."', '".$night_after."', '".$adult."', '".$child."', '0', '".$breakfast."')";
				$q_temp = mysqli_query($conn, $s_temp) or mysqli_error();
				$temp_id = mysqli_insert_id();
			}
			
			$s_tempdtl = "insert into bookingtempdtl (bookingtempoid, date, rate, discount, rateafter, total, currencyoid) values ('".$temp_id."', '".$date."', '".$rate."', '".$discountnote."', '".$rate_after."', '".$rate_after."', '".$currency."')";
			$q_tempdtl = mysqli_query($conn, $s_tempdtl) or mysqli_error();
			
			?>
			<input type="hidden" box="bookingtemp" tmp="<?php echo $type; ?>-<?php echo $id; ?>" value="<?php echo $temp_id; ?>"/>
            <?php
			?>
			<input type="hidden" value="offerroomoid-<?php echo $date;  ?>/<?php echo $date;  ?>/<?php echo $offerroomoid;  ?>/<?php echo $rate;  ?>/<?php echo $discount;  ?>">
            <?php
			/*
			$s_tax = "select t.* from tax t inner join taxtype tt using (taxtypeoid) inner join hotel h using (hoteloid) where h.hoteloid = '".$hoteloid."' and (salefrom <= '".$date."' and saleto >= '".$date."') and fee = 'y' and publishedoid = '1'";
			$q_tax = mysqli_query($conn, $s_tax) or mysqli_error(); 
			$tax = mysqli_fetch_array($q_tax);
			*/
			
		} // for night
		$checkout = date("Y-m-d",strtotime($checkin." +".$night." day"));
		$s_temp = "update bookingtemp set roomtotal='".$total_room."', total='".$total_room."', currencyoid = '".$currency."', checkout = '".$checkout."' where bookingtempoid = '".$temp_id."'";
		$q_temp = mysqli_query($conn, $s_temp) or mysqli_error();
		?>
        	<input type="hidden" box="totalroom" value="<?php echo $total_room;  ?>">
			<input type="hidden" box="totalcurrency" value="IDR">
        <?php
		echo"</div>";
		$total = $total + $total_room;
		
	} // for jumlah room
	
?>