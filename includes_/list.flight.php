<div id="resultflight" class="clear white-box">
	
	<div align="right">
		<ul class='tabs'>
			<li><a href='#tabs-depart'><span class="tabs-depart-label"></span></a></li>
			<li><a href='#tabs-return'><span class="tabs-return-label"></span></a></li>
		</ul>
	</div>
	
	<table class="list-resultflight table" cellspacing="0">
		<thead align="center">
			<tr class="table-row title">
				<td class="table-cell" colspan="2">Flights</td>
				<td class="table-cell">Depart</td>
				<td class="table-cell">Arrival</td>
				<td class="table-cell">Transit</td>
				<td class="table-cell">Duration</td>
				<td class="table-cell" colspan="2">Price</td>
			</tr>
		</thead>
		<tbody align="center" class="tabs-content" id='tabs-depart'>
			<tr class="table-row"><td class="table-cell" colspan="6">Please specify your flight detail.</td></tr>
		</tbody>
		<tbody align="center" class="tabs-content" id='tabs-return'>
			<tr class="table-row"><td class="table-cell" colspan="6">Please specify your flight detail.</td></tr>
		</tbody>
	</table>
</div>