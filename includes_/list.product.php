<?php
$s_product="select productcontent.name, product.productoid, product.productpict, getpriceact, productcontent.description, productcontent.facilities, productcontent.headline, cityname from product  inner join city using (cityoid) inner join productcontent using (productoid) where publishedoid ='1' and (product.startdate<='$todaydate' and product.enddate>='$todaydate') AND (productcontent.headline IS NOT NULL AND productcontent.headline <> '') group by productoid ORDER BY rand() LIMIT 4";
$q_product = mysqli_query($conn, $s_product) or die(mysqli_error());
?>
<style>
#container #content #right-content #list-product li {padding: 10px 0; border-bottom: 1px solid #ddd;}
#container #content #right-content #list-product li:first-of-type {padding-top: 0;}
#container #content #right-content #list-product li:last-of-type {padding-bottom: 0; border-bottom: none;}
#container #content #right-content #list-product li .pict {width: 100px; height: 100px; margin: 0 5px 0 0;}
#container #content #right-content #list-product li .col-2 {width: 430px;}	
</style>


<ul class="content-list" id="list-product">
	<?php
	while($product = mysqli_fetch_array($q_product)){
		$productoid = $product['productoid']; $name = $product['name']; $headline = $product['headline']; $pict = $product['productpict']; $price = "IDR 200K";
		$priceact = $product['getpriceact'];
		include("minrate.product.php");
		
		if($page == "home"){
			?>
			<li class="inline-block top white-box border-box">
				
				<div class="thumb pict">
					<img src="<?=$pict;?>">
				</div>
				<div class="title center border-box"><?=$name;?></div>
				<div class="small-desc"><?=$headline;?></div>
				<div class="border-box center">
					<div class="price inline-block">from <?=$showprice;?></div>
					<a href="#" class="button inline-block">BOOK</a>
				</div>
			</li>
			<?php 
		}else if($page == "search-tour"){
			?>
			<li class="top">
				<div class="thumb pict inline-block top">
					<img src="<?=$pict;?>">
				</div>
				<div class="col-2 inline-block top">
					<div class="title border-box">
						<img src="<?=$base_url;?>/images/blank.gif" class="flag flag-id" alt="Czech Republic" />
						<?=$name;?>
					</div>
					<div class="small-desc"><?=$headline;?></div>
				</div>
				<div class="fl_right inline-block right">
					<div class="price">
						from
						<h2 class="rate">
							<span class="grey"><?=$currencycode;?></span>
							<span class="blue"><?=miniThousand($displayprice);?></span>
						</h2>
					</div>
					<div class="bottom-right">
						<a href="#" class="button book inline-block">BOOK</a>
					</div>
				</div>
			</li>
			<?php
		}
	}
	?>
</ul>