<?php
include("function.filter.php");
include("tour.list.query.php");
?>

<aside id="sidebar" class="inline-block">
	<div class="white-box border-box" id="search">
		<h3>Your Search</h3>
		<?php include("form.search.tour.php"); ?>
	</div>
	
	<div class="white-box border-box" id="filters">
		<div id="filters-overlay" style="visibility: hidden;">
			<div class="center middle sticky-message border-box" id="message">
				<span class="title">
					Retrieving result, <br>
					please wait... <br>
				</span>
				<img id="loading" src="<?=$base_url;?>/images/loading2.gif" />
			</div>
		</div>
		
		<h3>Filter Your Results</h3>
		<div class="filter-wrapper clear border-bottom">
			<span class="title-blue">Tour Name Contains</span>
			<a link="<?=querystring_remove("tourname");?>" class="reset small-desc maroon text fl_right" style="display: none;">RESET</a>
			<input type="text" name="tourname" id="q-tourname" class="top filter-name" value="<?=$_GET['tourname'];?>"/>
			<input type="button" id="submit-tourname" class="general-submit top" value="GO" />
		</div>
		
		<div class="filter-wrapper clear border-bottom">
			<span class="title-blue">Price Group</span>
			<?php
			for($pg = 1; $pg<=4; $pg++){
				if($pg == 1){
					$pg_label = "less than IDR 999,999";
				}else if($pg == 2){
					$pg_label = "IDR 1,000,000 - 2,999,999";
				}else if($pg == 3){
					$pg_label = "IDR 3,000,000 - 4,999,999";
				}else if($pg == 4){
					$pg_label = "above 5,000,000";
				}
				$jml_hotel = "12";
				?>
				<div>
					<input type="checkbox" id="cb-price-<?=$pg;?>" value="<?=$pg;?>" class="cb-price"/>
					<label for="cb-price-<?=$pg;?>">
						<?=$pg_label;?>
						<span class="small-desc">(<?=$jml_hotel;?>)</span>
					</label>
				</div>
				<?php
				}
			?>
			<input type="hidden" id="selected-price" value="" />
		</div>
		
		
		
		
		<div class="filter-wrapper clear border-bottom">
			<span class="title-blue">Accommodation Type</span>
			<?php
			$sql_at = "SELECT * FROM hoteltype";
			$run_at = mysqli_query($conn, $sql_at) or die(mysqli_error());
			while($row_at = mysqli_fetch_array($run_at)){
				$category = $row_at['category'];
				?>
				<div><input type="checkbox"/><label><?=$category;?></label></div>
				<?php
			}
			?>
		</div>
		
		<div class="clear border-bottom">
			<span class="title-blue">Facilities</span>
			<div><input type="checkbox"/><label>Swimming Pool (1397)</label></div>
			<div><input type="checkbox"/><label>Internet (1386)</label></div>
			<div><input type="checkbox"/><label>Gym/Fitness (190)</label></div>
			<div><input type="checkbox"/><label>Car Park (1539)</label></div>
			<div><input type="checkbox"/><label>Spa/Sauna (701)</label></div>
			<div><input type="checkbox"/><label>Business Facilities	(501)</label></div>
			<div><input type="checkbox"/><label>Family/Child Friendly	(894)</label></div>
			<div><input type="checkbox"/><label>Restaurant	(976)</label></div>
			<div><input type="checkbox"/><label>Non Smoking Rooms	(884)</label></div>
			<div><input type="checkbox"/><label>Smoking Area	(793)</label></div>
			<div><input type="checkbox"/><label>Airport Transfer	(1354)</label></div>
			<div><input type="checkbox"/><label>Pets Allowed	(142)</label></div>
			<div><input type="checkbox"/><label>Disabled Facilities	(72)</label></div>
			<div><input type="checkbox"/><label>Golf Course (On Site)	(26)</label></div>
			<div><input type="checkbox"/><label>Nightclub	(18)</label></div>
		</div>
		
		<div class="clear border-bottom">
			<span class="title-blue">Area</span>
			<div><input type="checkbox"/><label>Kuta	(101)</label></div>
			<div><input type="checkbox"/><label>Ubud	(303)</label></div>
			<div><input type="checkbox"/><label>Seminyak	(281)</label></div>
			<div><input type="checkbox"/><label>Legian	(113)</label></div>
			<div><input type="checkbox"/><label>Nusa Dua / Benoa	(70)</label></div>
			<div><input type="checkbox"/><label>Sanur	(116)</label></div>
			<div><input type="checkbox"/><label>Denpasar	(71)</label></div>
			<div><input type="checkbox"/><label>Jimbaran	(57)</label></div>
		</div>
		
		<div class="clear">
			<span class="title-blue">Hotel chains &amp; brands</span>
			<div><input type="checkbox"/><label>Aston International Hotels, Resorts &amp; Residences (1)</label>
			<div><input type="checkbox"/><label>JW Marriott Hotels &amp; Resorts (1)</label></div>
			<div><input type="checkbox"/><label>Meritus (1)</label></div>
			<div><input type="checkbox"/><label>Shangri La (1)</label></div>
			<div><input type="checkbox"/><label>Sheraton (1)</label></div>
		</div>
		
	</div>
	<?php include("js_filter.search.php"); ?>
</aside>

<div id="right-content" class="inline-block">
	<!-- 
	<div class="white-box border-box">
		Sort By :
	</div>
	 -->
	<div class="white-box border-box">
		<?php include("tour.list.php"); ?>
	</div>
	<!--
	<div class="white-box border-box">
		<?php //include("list.activities.php"); ?>
	</div>
	-->
</div>
