<?php
		// echo "<script>console.log('save.data.hotel : ".$_SESSION['booking_inquiry']."|".($_SESSION['booking_inquiry']=='y')."');</script>"; 

	if($_SESSION['booking_inquiry']=='y'){
		$inq = $db->run("select * from bookingtemp where session_id=?;",array($_SESSION['tokenSession']))->fetch();
		// $iq = "INSERT into bookingroom (bookingoid, roomofferoid, channeloid, promotionoid, checkin, checkout, checkoutr, night, adult, child, 
		// 		roomtotal, extrabedtotal, total, currencyoid, breakfast, extrabed, hotel,room, roomtotalr, totalr ) 
		// 		VALUES  ('".$bookingoid."', '0', '".$inq['channeloid']."', '0', '".$inq['checkin']."', '".$inq['checkout']."', '".$inq['checkout']."', 
		// 		'".$inq['night']."', '".$inq['adult']."', '".$inq['child']."', '0', '0' , '0', 
		// 		'".$inq['currencyoid']."', 'n', '0', '".$inq['hotel']."', '', '0', '0')";
		
		$q_temp = $db->run("INSERT into bookingroom (bookingoid, roomofferoid, channeloid, promotionoid, checkin, checkout, checkoutr, night, adult, child, 
						currencyoid, breakfast, extrabed, hotel ) 
					VALUES  (:bookingoid, :roomofferoid, :channeloid, :promotionoid, :checkin, :checkoutnow, checkoutr, 
					:adult, :adult, :child, :currencyoid, :breakfast, ;extrabed, :hotel)", array(
						':bookingoid'=> $bookingoid,
						':roomofferoid'=> 0,
						':channeloid'=> $inq['channeloid'],
						':promotionoid'=> 0,
						':checkin'=> $inq['checkin'],
						':checkoutnow'=> $inq['checkout'],
						':checkoutr'=> $inq['checkout'],
						':night'=> $inq['night'],
						':adult'=> $inq['adult'],
						':child'=> $inq['child'],
						':currencyoid'=> $inq['currencyoid'],
						':breakfast'=> 'n',
						':extrabed'=> 0,
						':hotel'=> $inq['hotel']))->rowCount();
	}

	if($_SESSION['booking_inquiry']=='n'){
		$d_today = date("Y-m-d");
		$ttl = $db->run("SELECT sum(total) as grandtotal,currencyoid,value FROM
						(SELECT total, currencyoid, 
							CASE 
								WHEN hc.value IS NULL 
								THEN 
									(SELECT IFNULL(value,5) FROM hotelcommission WHERE hoteloid=0 LIMIT 1)
								ELSE hc.value
							END as value
						FROM bookingtemp inner join  roomoffer using (roomofferoid) 
							inner join room using (roomoid) inner join hotel using (hoteloid) 
							left join hotelcommission hc 
								on hc.hoteloid=hotel.hoteloid and startdate<=? and enddate>=?
						WHERE session_id =? 
						GROUP BY session_id ORDER BY hc.priority asc LIMIT 1) commission",array($d_today,$d_today,$_SESSION['tokenSession']))->fetch();
		$grandtotal = $ttl['grandtotal'];
		$currencyoid = $ttl['currencyoid'];
		$commissionpercentage = $ttl['value'];

		$commission = $grandtotal * $commissionpercentage / 100;
		$hotelcollect = $grandtotal - $commission;
		
		// logJS($ttl);logJS($grandtotal,'grandtotal');logJS($commissionpercentage,'commissionpercentage');logJS($commission,'commission');
		
		$q_temp = $db->run("UPDATE booking set grandtotal=:grandtotalnow,  grandtotalr=:grandtotalr,currencyoid = :currencyoid, 
					hotelcollect = :hotelcollect, gbhcollect = :gbhcollect, gbhpercentage = :gbhpercentage where bookingoid = :bookingoid", array(
						':grandtotalnow'=> $grandtotal,
						':grandtotalr'=> $grandtotal,
						':currencyoid'=> $currencyoid,
						':hotelcollect'=> $hotelcollect,
						':gbhcollect'=> $commission,
						':gbhpercentage'=> $commissionpercentage,
						':bookingoid'=> $bookingoid))->rowCount();

		// Func::logJS($pagesuccess,'$pagesuccess');
		// Func::logJS($bookingstatusoid,'$bookingstatusoid');
		if($bookingstatusoid==3){
			$q_temp = $db->run("UPDATE booking set granddeposit=:granddeposit where bookingoid = :bookingoid", array(
				':granddeposit'=> $grandtotal,
				':bookingoid'=> $bookingoid))->rowCount();
		}
		// die();

		/************************************************************************************/

		$q_temp = $db->run("SELECT bookingtemp.*  from bookingtemp inner join roomoffer using (roomofferoid) inner join room using (roomoid) WHERE session_id =?",array($_SESSION['tokenSession']))->fetchAll();

		foreach($q_temp as $temp){
		
			$dtlroom = $db->run("INSERT into bookingroom ( bookingoid, roomofferoid, channeloid, promotionoid, checkin, checkout, checkoutr, 
					night, adult, child, roomtotal, extrabedtotal, total, currencyoid, breakfast, extrabed, hotel,room, roomtotalr, totalr ) 
				values (:bookingoid, :roomofferoid, :channeloid, :promotionoid, :checkin, :checkoutnow, :checkoutr, :night, :adult, :child,
					:roomtotalnow, :extrabedtotal , :totalnow, :currencyoid, :breakfast, :extrabed, :hotel, :room, :roomtotalr, :totalr)", array(
				':bookingoid'=> $bookingoid,
				':roomofferoid'=> $temp['roomofferoid'],
				':channeloid'=> $temp['channeloid'],
				':promotionoid'=> $temp['promotionoid'],
				':checkin'=> $temp['checkin'],
				':checkoutnow'=> $temp['checkout'],
				':checkoutr'=> $temp['checkout'],
				':night'=> $temp['night'],
				':adult'=> $temp['adult'],
				':child'=> $temp['child'],
				':extrabedtotal'=> $temp['extrabedtotal'],
				':roomtotalnow'=> $temp['roomtotal'],
				':roomtotalr'=> $temp['roomtotal'],
				':totalnow'=> $temp['total'],
				':totalr'=> $temp['total'],
				':currencyoid'=> $temp['currencyoid'],
				':breakfast'=> $temp['breakfast'],
				':extrabed'=> $temp['extrabed'],
				':hotel'=> $temp['hotel'],
				':room'=> $temp['room']))->rowCount();
			$bookingroomoid =$db->lastInsertId();

			
			$q_tempdtl = $db->run("SELECT * from bookingtempdtl where bookingtempoid =?",array($temp['bookingtempoid']))->fetchAll();
			foreach($q_tempdtl as $tempdtl){
				$q_savedtl = $db->run("insert into bookingroomdtl (bookingroomoid, date, rate, discount, rateafter, extrabed, total, currencyoid) 
										values (:bookingroomoid, :date, :ratenow, :discount, :rateafter, :extrabed, :total, :currencyoid)", array(
											':bookingroomoid'=> $bookingroomoid,
											':date'=> $tempdtl['date'],
											':ratenow'=> $tempdtl['rate'],
											':discount'=> $tempdtl['discount'],
											':rateafter'=> $tempdtl['rateafter'],
											':extrabed'=> $tempdtl['extrabed'],
											':total'=> $tempdtl['total'],
											':currencyoid'=> $tempdtl['currencyoid']))->rowCount();
			}
			
			//mysqli_query($conn, "delete bookingtemp, bookingtempdtl from bookingtemp, bookingtempdtl where bookingtemp.bookingtempoid = bookingtempdtl.bookingtempoid and bookingtemp.bookingtempoid = '".$temp['bookingtempoid']."'");
		}
	}
	// Func::logJS();
	// die();
	?>