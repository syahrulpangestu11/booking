<script>
	function shrinkDesc(jmlChar, elemSelector){
		var elem = elemSelector;
		var elemHtmlLength = elem.html().length;
		var elemText = elem.text();
		var elemTextShrinked = elemText.substr(0, jmlChar) + "...";
		if(elemHtmlLength > jmlChar){
			elem.html(elemTextShrinked);
		}
		return elemText;
	}
	$(document).ready(function(){
		$("#list-product-hotel > li .title").each(function(){
			shrinkDesc(35, $(this));
		});
		$("#list-product-hotel > li .small-desc").each(function(){
			shrinkDesc(85, $(this));
		});
		
		$(".button.toggle").click(function(){
			$("#dates-details").toggle();
			$("#search-another").toggle();
		});
	});
</script>

<div id="breadcrumb" class="white-box border-box">
	<?php
	if($tour_cityoid != 0){
		$uri_continent = $uri3;
		$uri_country = $uri4;
		$uri_state = $uri5;
		$uri_city = $uri6;
		$uri_tourname = $uri7;
		$breadcrumb = array($uri2,$uri_continent,$uri_country,$uri_state,$uri_city,$uri_tourname);
	}else if($tour_stateoid != 0){
		$uri_continent = $uri3;
		$uri_country = $uri4;
		$uri_state = $uri5;
		$uri_tourname = $uri6;
		$breadcrumb = array($uri2,$uri_continent,$uri_country,$uri_state,$uri_tourname);
	}else if($tour_countryoid != 0){
		$uri_continent = $uri3;
		$uri_country = $uri4;
		$uri_tourname = $uri5;
		$breadcrumb = array($uri2,$uri_continent,$uri_country,$uri_tourname);
	}else if($tour_continentoid != 0){
		$uri_continent = $uri3;
		$uri_tourname = $uri4;
		$breadcrumb = array($uri2,$uri_continent,$uri_tourname);
	}
	foreach ($breadcrumb as $key => $value) {
		if(end($breadcrumb)==$value){
			$gt = "";
		}else{
			$gt = " &gt; ";
		}
		$value = str_replace("-", " ", $value);
		?>
		<a class="breadcrumb text" id="breadcrumb-<?=$key;?>" href="#"><?=$value;?></a><?=$gt;?>	
		<?php
	} 
	?>
</div>

<div class="white-box border-box clear">

	<div id="left-content" class="inline-block border-right top">
		<img src="<?=$pict;?>" id="product-pict" />
		<div class="title border-bottom"><?=$name;?></div>
		<div class="small-desc"><?=$headline;?></div>
	</div>

	<div id="right-content" class="inline-block">
		<b>Description</b>
		<div><?=$description;?></div>
		<b>Itenerary</b>
		<div class="readmore"><?=$itenerary;?></div>
		<b>Terms</b>
		<div><?=$termscondition;?></div>
		<div class="inline-block border-right">
			<b>Night : </b> <?=$night;?>
		</div>
		<div class="inline-block">
			<b>Minimum Pax : </b> <?=$minpax;?>
		</div>
	</div>
</div>

<br>

<?php
function nounSingPlur($n, $nountype){
	if($n > 1){
		if($nountype == 1){
			$noun = "s";
		}else if($nountype == 2){
			$noun = "es";
		}
	}else{
		$noun = "";
	}
	return $noun;
}
?>

<ul class="content-list" id="list-product-hotel">
	<li id="info" class="inline-block top white-box border-box">
		<div id="dates-details">
			<div class="center">Showing available hotel in tour</div>
			<div><img src="<?=$base_url;?>/images/location-g.png"><?=$keyword;?></div>
			<div><img src="<?=$base_url;?>/images/calendar-g.png"><?=$default_checkin;?></div>
			<div><img src="<?=$base_url;?>/images/night-g.png"><?=$night;?> night<?=nounSingPlur($night, 1);?></div>
			<div><img src="<?=$base_url;?>/images/adult-g.png"><?=$adult;?> adult<?=nounSingPlur($adult, 1);?></div>
			<div><img src="<?=$base_url;?>/images/child-g.png"><?=$child;?> child<?=nounSingPlur($child, 1);?></div>
			<div class="center fl_bottom"><a class="button toggle" id="change-details">Change</a></div>
		</div>
		<div id="search-another" style="display:none;">
			<div class="center">Search another tour</div>
			<div id="search"><?php include("form.search.tour.php"); ?></div>
			<div class="center fl_bottom"><a class="button toggle" id="change-details">Cancel</a></div>
		</div>
	</li>
	
	
<?php
$s_hotel = "select th.hoteloid, th.tourhoteloid, h.hotelname, h.stars from tourhotel as th inner join hotel as h using (hoteloid) inner join tour as t using (touroid) where h.publishedoid = '1' and t.publishedoid = '1' and t.touroid = '$touroid'";
$q_hotel = mysqli_query($conn, $s_hotel) or die(mysqli_error()); 
while($hotel = mysqli_fetch_array($q_hotel)){
	$hoteloid = $hotel['hoteloid']; $hotel_name = $hotel['hotelname']; $star = $hotel['stars']; 
	$tourhoteloid = $hotel['tourhoteloid'];
	$s_room_rate = "select thr.roomoid, tn.tournormaloid, tnp.tournormalpriceoid, netrate, tnp.taxoid, c.currencyoid, c.currencycode, thr.pax from tournormalprice as tnp inner join currency as c using (currencyoid) inner join tax as tx using (taxoid) inner join tournormal as tn using (tournormaloid) inner join  tourhotelroom as thr inner join room as r using (roomoid) inner join hotel as h using (hoteloid) inner join tourhotel as th using (tourhoteloid) inner join tour as t using (touroid) where thr.tourhoteloid = '$tourhoteloid' and ('$todaydate'>=tn.startbook and '$todaydate'<=tn.endbook) and ('$checkin'>=tn.startdate and '$checkin'<=tn.enddate) order by tn.priority asc, netrate asc limit 1";
	// check ada rate pada tanggal tsc
	$numrow = mysqli_num_rows(mysqli_query($conn, $s_room_rate));
	if($numrow == 1){
		$q_room_rate = mysqli_query($conn, $s_room_rate) or die(mysqli_error());
		$room_rate = mysqli_fetch_array($q_room_rate);
		$ratecurrency = $room_rate['currencyoid'];
		if($tourpricing == "1"){
			$rate = $room_rate['netrate']; 
		}else if($tourpricing == "2"){
			$getpackage = ceil($adult / $room_rate['pax']);
			$rate = ($room_rate['netrate'] * $getpackage) / $adult;
		}
		include("convertcurrency.php");
		// Hotel Minimal Rate Tour
		$formated_rate = number_format($rate);
		$displayprice = $currencycode." ".$formated_rate;
		$displayprice_fnc = $formated_rate;
		$displayprice_short = ($currencycode == "IDR") ? substr_replace($formated_rate, "K", -4) : $formated_rate ;
		$showprice = $currencycode." ".$displayprice_short;
		
		// Hotel Pict
		$s_hotelphoto = "SELECT * FROM hotelphoto hp WHERE hoteloid = '$hoteloid' AND hp.flag = 'main'";
		$q_hotelphoto = mysqli_query($conn, $s_hotelphoto) or die(mysqli_error()); $r_hotelphoto = mysqli_fetch_array($q_hotelphoto);
		$hotel_pict = $r_hotelphoto['photourl'];
		
		// Hotel Content
		$s_hotelcontent = "SELECT * FROM hotelcontent hc WHERE hoteloid = '$hoteloid' AND (hc.languageoid = '$languageoid' or hc.languageoid = 1)";
		$q_hotelcontent = mysqli_query($conn, $s_hotelcontent); $r_hotelcontent = mysqli_fetch_array($q_hotelcontent);
		$hotel_headline = !empty($r_hotelcontent['headline']) ? $r_hotelcontent['headline'] : "" ;
		
		// Hotel Link
		include_once("function.randomstring.php");
		$encoded_tourhoteloid = randomString(4).$tourhoteloid.randomString(2);
		$param_append = "tour=".$encoded_tourhoteloid."&destinationref=".$destinationtype;
		$uri2 = "tourdetail";
		if($tour_cityoid != 0){
			$uri_continent = $uri3;
			$uri_country = $uri4;
			$uri_state = $uri5;
			$uri_city = $uri6;
			$uri_tourname = $uri7;
			$uri_touroid = $uri8;
			$parameters = $uri9;
			$href_tour = $base_url."/".$uri2."/".$uri_continent."/".$uri_country."/".$uri_state."/".$uri_city."/".$uri_tourname."/".$uri_touroid."/".$parameters;
		}else if($tour_stateoid != 0){
			$uri_continent = $uri3;
			$uri_country = $uri4;
			$uri_state = $uri5;
			$uri_tourname = $uri6;
			$uri_touroid = $uri7;
			$parameters = $uri8;
			$href_tour = $base_url."/".$uri2."/".$uri_continent."/".$uri_country."/".$uri_state."/".$uri_tourname."/".$uri_touroid."/".$parameters;
		}else if($tour_countryoid != 0){
			$uri_continent = $uri3;
			$uri_country = $uri4;
			$uri_tourname = $uri5;
			$uri_touroid = $uri6;
			$parameters = $uri7;
			$href_tour = $base_url."/".$uri2."/".$uri_continent."/".$uri_country."/".$uri_tourname."/".$uri_touroid."/".$parameters;
		}else if($tour_continentoid != 0){
			$uri_continent = $uri3;
			$uri_tourname = $uri4;
			$uri_touroid = $uri5;
			$parameters = $uri6;
			$href_tour = $base_url."/".$uri2."/".$uri_continent."/".$uri_tourname."/".$uri_touroid."/".$parameters; 
		}
		
		if(empty($parameters)){ $param_append = '?'.$param_append; }else{ $param_append = '&'.$param_append; }
		$href_tour = $href_tour.$param_append;
?>
		<?php
		//---SEMENTARA---//
		//for ($i=0; $i < 10; $i++) { 
		?>
		<li class="inline-block top white-box border-box">
			<div class="thumb pict"><img src="<?=$hotel_pict;?>"></div>
			<div class="price"><?=$showprice;?> /pax</div>
			<div class="text">
				<div class="title center"><?=$hotel_name;?></div>
				<div class="small-desc"><?=$hotel_headline;?></div>
				<div class="center fl_bottom">
					<a href="<?=$href_tour;?>" class="button">BOOK</a>
				</div>
			</div>
		</li>
		<?php
		//---SEMENTARA---//
		//}
		?>
<?php
	}else{
		// tidak ada rate pada tanggal tsb.
	}
} // while tour hotel
?>
</ul>