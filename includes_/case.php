<?php
	echo "<script>console.log('case');</script>"; 
	include("includes/parameter.php");
include("includes/js_datepicker.php");
include("includes/js_autocomplete.php");
if($uri2 == "" or !isset($uri2) or empty($uri2) or $uri2 == "index.php"){
	$page = "home";
	include("includes/page.home.php");
}else if($uri2=="search"){
	if($uri3 == "hotel" or $uri3 == "hotels"){
		$page = "search-hotel";
		include("includes/page.search.hotel.php");
	}else if($uri3 == "tour"){
		$page = "search-tour";
		include("includes/page.search.tour.php");
	}else if($uri3 == "flight"){
		$page = "search-flight";
		include("includes/page.search.flight.php");
	}else if($uri3 == "flight-int"){
		$page = "search-flight-int";
		include("includes/page.search.flight-int.php");
	}else if($uri3 == "activities"){
		$page = "search-activities";
		include("includes/page.search.activities.php");
	}
}else if($uri2=="hotel" or $uri2 == "hotels"){
	$page = "detail-hotel";
	include("includes/function.set-session.php");
	include("includes/page.detail.hotel.php");
}else if($uri2=="tourdetail" or $uri2 == "toursdetails"){
	include("includes/function.set-session.php");
	include("includes/tour.type-normal.php");
}else if($uri2=="tour" or $uri2 == "tours"){
	$page = "detail-tour";
	include("includes/function.set-session.php");
	include("includes/tour.list-detail.php");
}else if($uri2=="book"){
	$page = "book";
	if($uri3=="guest-detail"){
		include("includes/page.guest-detail.php");
	}else if($uri3=="payment"){
		include("includes/page.payment-detail.php");
	}else if($uri3=="process"){
		echo "<script>console.log('if');</script>"; 
		include("includes/save.process.php");
	}else if($uri3=="confirmation"){
		include("includes/page.confirmation.php");
		// if($uri4 == "bank-transfer"){
		// 	include("includes/success.bank-transfer.php");
		// }else if($uri4 == "klikbca"){
		// 	include("includes/success.klik-bca.php");
		// }else{
		// 	include("includes/page.confirmation.php");
		// }
		
	}else if($uri3=="flight-detail"){
		include("includes/function.set-session.flight.php");
		include("includes/page.flight-detail.php");
	}
}else if($uri2=="news"){
	include("includes/page.news.php");

}else if($uri2=="activities"){
	include("includes/function-php/function.randomstring.php");
	if($uri3 == "request"){
		include("includes/activities/book-activities.php");
	}else{
		include("includes/activities/page-activities.php");
	}
	
}else if($uri2=="faq"){
	include("includes/page-faq.php");
}else if($uri2=="about-us"){
	include("includes/page-about-us.php");
}else if($uri2=="terms-condition"){
	include("includes/page-terms-condition.php");
}else if($uri2=="privacy-policy"){
	include("includes/page-privacy-policy.php");
}else if($uri2=="contact-us"){
	if($uri3 == "submit"){
		include("includes/page-contact-us-sending.php");
	}else if($uri3 == "confirmation"){
		include("includes/page-contact-us-confirmation.php");
	}else{
		include("includes/page-contact-us.php");
	}
}else if($uri2=="member"){
	switch($uri3){
		case "verification" : include("includes/member/login-verification.php"); break;
		case "log-out" : include("includes/member/logout.php"); break;
		default : include("includes/member/page-member.php"); break;
	}
}else if($uri2=="thing-to-do"){
	include("component/page/page-thing-to-do.php");
}

?>
