<?php
$s_package = "select packageoid, packageroomoid from package  inner join hotel using (hoteloid) inner join packageroom using (packageoid) inner join room using (roomoid) inner join packagerate using (packageroomoid) where room.roomoid = '$roomoid' and package.publishedoid = '1' and (packagerate.startbook<='$todaydate' and packagerate.endbook>='$todaydate') and (packagerate.startdate<='$checkin' and packagerate.enddate>='$checkout') and package.night>='$night'  group by packageoid, packageroomoid";
$q_package = mysqli_query($conn, $s_package) or mysqli_error();
while($package = mysqli_fetch_array($q_package)){
	$packageoid = $package['packageoid']; $packageroomoid = $package['packageroomoid'];
	$total= 0;
	for($n=0;$n<$night;$n++){
		include('function.date.php');
		if($n==0){
			$s_rate_package = "select packagerateoid, multipricetypeoid from packagerate inner join published using (publishedoid) inner join packageroom using (packageroomoid) where packageroom.packageroomoid = '$packageroomoid' and (packagerate.startbook<='$todaydate' and packagerate.endbook>='$todaydate') and (packagerate.startdate<='$checkin' and packagerate.enddate>='$checkout') order by priority limit 1";
			$q_rate_package = mysqli_query($conn, $s_rate_package) or mysqli_error(); $rate_package = mysqli_fetch_array($q_rate_package);
			$packagerateoid = $rate_package['packagerateoid']; $multipricetypeoid = $rate_package['multipricetypeoid'];
			
			$s_market_price = "select price, discount, marketoid, currency.currencyoid from multiprice inner join market using (marketoid) inner join currency using (currencyoid) where typeid = 'package' and idreference = '$offerrateoid' and multipricetypeoid = '$multipricetypeoid' limit 1";
			/*inner join marketcountry using (marketoid) inner join country using (countryoid)*/
			$q_market_price = mysqli_query($conn, $s_market_price) or mysqli_error(); $market_price = mysqli_fetch_array($q_market_price);
			$rate = $market_price['price']; $ratecurrency = $market_price['currencyoid'];
			include("convertcurrency.php");
		}else{
			$rate = 0;
		}
		$total = $total + $rate;
	}
	if($total > 0){
		$countrate ++;
		include('function.package-content.php');
		$totalpernight = $total / $night;
?>
<li class="table-row">
		<div class="table-cell top">
			<span class="title"><?=$room_name;?> - <?=$name_package;?></span>
			<div class="thumb pict"><img src="<?=$room_pict;?>"></div>
			<a href="#">room info</a>
		</div>
		<div class="table-cell top center">
			<?php
			for($i=1;$i<=$room_adult;$i++){ ?>
				<img src="<?=$base_url;?>/images/adult.png">
				<?php
			}
			for($i=1;$i<=$room_child;$i++){ ?>
				<img src="<?=$base_url;?>/images/child.png">
				<?php
			}
			?>
		</div>
		<div class="table-cell top center">
			<h3><span class="grey"><?php echo $currencycode;  ?></span> <span class="blue"><?php echo number_format($totalpernight); ?></span></h3>
		</div>
		<div class="table-cell top center">
			<input type="hidden" name="roomoid" id="roomoid" value="<?php echo $roomoid; ?>">
			<input type="hidden" name="packageroomoid" id="packageroomoid" value="<?php echo $packageroomoid; ?>">
			<select room="number">
				<?php for($r=0;$r<=$default_allotment;$r++) { ?>
				<option value="<?php echo $r; ?>"><?php echo $r; ?></option>
				<?php } ?>
			</select>
			<div></div>
		</div>
	</li>
<?php
	}
}
?>