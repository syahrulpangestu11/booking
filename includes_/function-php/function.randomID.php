<?php
	function encodeID($realID){
		$fakeID = randomString(4).$realID.randomString(2);
		return $fakeID;
	}
	function decodeID($fakeID){
		$realID = substr($fakeID, 4, strlen($fakeID)-6);
		return $realID;
	}
?>
