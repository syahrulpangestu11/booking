<?php //print_r($_POST); ?>
<?php 
include("js_validate.php"); 

$hoteloid = substr($_POST['hoteloid'], 4, strlen($_POST['hoteloid'])-6);

$s_hr = "SELECT * FROM hotel h INNER JOIN room r USING (hoteloid) INNER JOIN city USING (cityoid) WHERE h.hoteloid = '$hoteloid'";
$q_hr = mysqli_query($conn, $s_hr) or die(mysqli_error());
$r_hr = mysqli_fetch_array($q_hr);
	$hotel_name = $r_hr['hotelname'];
	$hotel_address = $r_hr['address'];
	$hotel_area = $r_hr['cityname']; 
	$room_name = $r_hr['name'];
	$room_pict = $r_hr['roompict'];

	// Func::logJS($r_hr);
	
	// Hotel Pict
	// $s_hotelphoto = "SELECT * FROM hotelphoto hp WHERE hoteloid = '$hoteloid' AND hp.flag = 'main'";
	$s_hotelphoto = "SELECT '-' AS hotelphotooid, hoteloid, banner AS photourl, banner AS thumbnailurl, 
	'main' AS flag, 'n' AS flag_flexible_rate, 'hotel-banner' AS ref_table, hoteloid AS ref_id 
	FROM hotel
	where hoteloid = '".$hoteloid."'";
	$q_hotelphoto = mysqli_query($conn, $s_hotelphoto) or die(mysqli_error());
	$r_hotelphoto = mysqli_fetch_array($q_hotelphoto);
	$hotel_pict = $r_hotelphoto['photourl'];

	// logJS($_POST); 
	if($_POST['inquiry']=='y'){
		$adult = $_POST['based_adult']; $child = $_POST['based_child']; $night = $_POST['based_night'];
		$checkin = date('Y-m-d', strtotime($_POST['based_checkin']));
		$checkout = date('Y-m-d', strtotime($_POST['based_checkin'].' +'.$night.' day'));

		$query_bt = "insert into bookingtemp (submittime, session_id, roomofferoid, channeloid, promotionoid, 
					checkin, checkout, night, adult, child, flag, breakfast, extrabed, hotel, room, currencyoid ) 
					values ('".date('Y-m-d H:i:s')."', '".$_SESSION['tokenSession']."', '', '".$channeloid."', '', 
					'".$checkin."', '".$checkout."', '".$night."', '".$adult."', '".$child."', '0', '', '', 
					'".$hotel_name."','', '1')";
		// echo "<script>console.log(".json_encode($query_bt).");</script>";
		$qbt_temp = mysqli_query($conn, $query_bt) or mysqli_error();
	}
?>
<style>
	/*.border-right {margin-right: 20px; padding-right: 20px;}*/
	
	#hotel-info {padding: 10px;}
	#hotel-pict {width: 100%;}
	
	.room-pict {width: 70px; height: 70px;}
	.room-detail {margin: 0 0 0 0px;}
	
	#email {width: 150px;}
	#address, #note {height: 60px; width: 180px;}
	#city {margin-bottom: 5px;}
	
	#guest-form #guest-detail > .inline-block > div {margin-bottom:5px;}
</style>

<br>
<form id="guest-form" action="<?=$base_url;?>/book/payment" method="post">
	<!--<h1><span class="grey">Reservation</span> <span class="blue">Summary</span></h1>-->
	<input type="hidden" name="hoteloid" value="<?php echo $_POST['hoteloid']; ?>">
	<input type="hidden" name="inquiry" value="<?php echo $_POST['inquiry']; ?>">

	<div class="clear">
		<div id="pgsch" class="container">	
			<div id="sidebar">
				<div class="card-rounded white-box border-box" style="padding: 0;">
					<img src="<?=$hotel_pict;?>" id="hotel-pict" />
					<div id="hotel-info">
						<h3 class="default"><span class="maroon"><?=$hotel_name;?></span></h3>
						<i class="small-desc"><?=$hotel_address;?></i>
					</div>
				</div>
			</div>
			
			<div id="right-content"><!-- top-right-box-->
				<div class="card-rounded white-box border-box">
					<h2 style="margin-top:0;"><span class="grey">Reservation</span> <span class="blue">Summary</span></h2>
					<?php include('page.guest-detail.summary.php'); ?>
				</div>
				<!-- <h1><span class="grey">Guest</span> <span class="blue">Detail</span></h1> 
				 class="white-box border-box"-->
				<div id="guest-detail" class="card-rounded white-box border-box">
					<div class="col-md-7 col-sm-6 col-xs-12 border-right">
					<h2><span class="grey">Guest</span> <span class="blue">Detail</span></h2>
						
						<div>
							<b class="form-group-title">Name :</b>
							<select name="title" id="title">
								<?php
									$s_title = "select * from title "; $q_title = mysqli_query($conn, $s_title) or die(mysqli_error());
									while($title = mysqli_fetch_array($q_title)){
								?>
									<option value="<?=$title['titleoid'];?>"><?=$title['name'];?></option>
								<?php
									}
								?>
							</select>
							<input type="text" name="firstname" id="firstname" placeholder="First Name" >
							<input type="text" name="lastname" id="lastname" placeholder="Last Name" ><br>
						</div>
						
						<div>
							<b class="form-group-title">Contact Information :</b>
							<input type="text" name="email" id="email" placeholder="Email" >
							<input type="text" name="mobilephone" id="mobilephone" placeholder="Mobile Phone" ><span id="errmsg"></span>
							<input type="text" name="phone" id="phone" placeholder="Phone Number"><br>
							<i class="small-desc">Phone number format must begin with your country code. Example : 6212345678</i><br>
						</div>
						
						<div>
							<b class="form-group-title">Address :</b>
							<div class="inline-block top">
								<textarea name="address" id="address" placeholder="Address" ></textarea>
							</div>
							<div class="inline-block top">
								<div>
									<input type="text" name="city" id="city" placeholder="City" >
									<input type="text" name="state" id="state" placeholder="State" >
								</div>
								<div>
									<input type="text" name="zipcode" id="zipcode" placeholder="ZIP Code"><br>
								</div>
							</div>
						</div>
						
						<div>
							<b class="form-group-title">Country :</b>
							<select name="countryoid" id="country" >
								<option value="" selected>--- please select your country ---</option>
								<?php 
								$s_country="select countryoid, countryname from country order by countryname"; $q_country=mysqli_query($conn, $s_country) or die ("SQL Error : ".$s_country);
								while($country=mysqli_fetch_array($q_country)){
									if(empty($_SESSION['nationallity'])){
										if($country['countryname']=="Indonesia"){ $selected=" selected='selected' "; }else{ $selected=""; }
									}else{
										if($country['countryoid']==$_SESSION['nationallity']){ $selected=" selected='selected' "; }else{ $selected=""; }
									}
								?>
								<option value="<?php echo $country['countryoid']; ?>" <?php echo $selected; ?>><?php echo $country['countryname']; ?></option>
								<?php
								}
								?>
							</select><br /><br />
						</div>
					</div> <!-- end of .inline-block.border-right -->
					
					
					<div class="col-md-5 col-sm-6 col-xs-12 top">
					
						<h2><span class="grey">Other</span> <span class="blue">Information</span></h2>
						
						<div style="display: inline-block; margin-right: 20px; display:none;">
							<b>Arrival Date :</b><br>
							<input type="text" name="arrivaldate" id="arrivaldate" readonly="readonly"/ ><br />
							<b>Arrival Time :</b><br>
							<input type="text" name="arrivaltime" id="arrivaltime" /><br />
						</div>
						<div style="display: inline-block; vertical-align: top;">
							<b>Note :</b><br><textarea name="note" id="note" placeholder="note, comment or request"></textarea>
						</div>
						<?php 
						echo '<input type="hidden" name="paymenturl" value="'.((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://').$_SERVER['HTTP_HOST'].$_SERVER["REQUEST_URI"].'"/>';
						?>
						<br><br>
						<input type="submit" name="submit" value="CONTINUE &gt;&gt;" class="general-submit fl_left" />
					</div> <!-- end of .inline-block -->
				</div> <!-- end of .white-box -->
			</div> <!-- end of .white-box -->
			
		</div>
	</div>
	
    <br />
	
	
	<br>
	<!-- <div class="white-box border-box clear"></div> -->
    
</form> <!-- end of form#guest-form -->

<?php echo "<script>console.log('token:".$_SESSION['tokenSession']."');</script>"; ?>	