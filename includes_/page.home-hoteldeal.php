<?php
	$gethotel = 0;
	include("list.hotel.query.php");
	while($hotel = mysqli_fetch_array($q_hotel)){
		
		$hoteloid = $hotel['hoteloid']; 
		$hotelname = $hotel['hotelname']; 
		$star = $hotel['stars'];  
		$address = $hotel['address']; 
		$city = $hotel['cityname']; 
		$state = $hotel['statename'];
		$country = $hotel['countryname'];
		$continent = $hotel['continentname'];
		$hoteltype = $hotel['category']; 
		
		/*------------------------- Hotel Picture -------------------------*/
		$s_hotelphoto = "SELECT * FROM hotelphoto hp WHERE hoteloid = '$hoteloid' AND hp.flag = 'main' AND hp.ref_table = 'hotel'";
		$q_hotelphoto = mysqli_query($conn, $s_hotelphoto) or die(mysqli_error());
		$r_hotelphoto = mysqli_fetch_array($q_hotelphoto);
		$hotelpict = $r_hotelphoto['photourl'];
		
		include('list.hotel.rate.promotion.php');
		if($minrate > 0){
?>
            <div class="inline-block threeinrow">
                <h3><?php echo $minrate_promoname; ?></h3>
                <img class="inline-block middle" src="<?php echo $hotelpict; ?>" />
                <div class="inline-block middle detail">
                    <div><?php echo $hotelname; ?></div>
                    <div><?php echo $currencycode."&nbsp;". $shownetrate; ?></div>
                    <div><?php echo $currencycode."&nbsp;". $hotel_minrate_final; ?></div>
                    <div>/ night</div>
                    <input type="button" class="button" value="book" onclick="document.location.href='<?php echo $href_hotel; ?> '" />
                </div>
            </div>
<?php
			if($gethotel == 3){ break; }
		}
	}
?>