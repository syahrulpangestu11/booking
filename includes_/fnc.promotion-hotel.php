<?php
	error_reporting(E_ALL ^ E_NOTICE);
	include('../conf/connection.php');
	// include('../conf/xmlload.php');

	$todaydate = date("Y-m-d");
	$tknSession = $_GET['tkn'];
	$type = $_GET['type'];
	$id = $_GET['promoapply'];
	$delid = $_GET['delid'];
	$adult = $_GET['adult']; $child = $_GET['child'];
	$numroom = $_GET['numroom'];
	$roomoid = $_GET['roomoid'];
	$roomofferoid = $_GET['roomofferoid'];
	$channeloid = $_GET['channeloid'];
	$booked_room = $_GET['jmlroom'];
	$with_extrabed = $_GET['eb'];
	$typeid=($type=='bar'?$roomofferoid:$roomofferoid.'-'.$id);
	if($with_extrabed != 1){ $with_extrabed = 0;	}

	$night = $_GET['night'];
	$checkin = date('Y-m-d', strtotime($_GET['checkin']));

	$s_date="select date_add('$checkin', interval $night day) as getDate";
	$q_date=mysqli_query($conn, $s_date) or die ("SQL Error : ".$s_date);
	$a_date=mysqli_fetch_array($q_date);

	$checkout=$a_date['getDate'];
	// echo "<script>console.log(\"GET-adult:". $_GET['adult']."\");</script>";

	if($type == "bar"){
		$s_promotion = "select '0' as promotionoid, '0' as discounttypeoid, '0' as discountapplyoid, '0' as applyvalue, '0' as discountvalue, h.hoteloid,
		'".$channeloid."' as channeloid,
		ro.roomofferoid, ro.roomoid, ro.offertypeoid,
		r.adult, r.child,h.hotelcode, h.hotelname, r.name as roomname
		from hotel h
		inner join room r using (hoteloid)
		inner join roomoffer ro using (roomoid)
		inner join offertype ot using (offertypeoid)
		where
		r.publishedoid = '1' and ro.roomofferoid = '".$roomofferoid."'
		group by ro.roomofferoid
		";
	}else{
		$s_promotion = "select p.promotionoid, p.discounttypeoid, p.discountapplyoid, p.applyvalue, p.discountvalue, p.hoteloid,
		pa.channeloid,
		ro.roomofferoid, ro.roomoid, ro.offertypeoid,
		r.adult, r.child,h.hotelcode, h.hotelname, r.name as roomname
		from promotion p
		inner join hotel h using (hoteloid)
		inner join discounttype dt using (discounttypeoid)
		inner join discountapply da using (discountapplyoid)
		inner join promotionapply pa using (promotionoid)
		inner join roomoffer ro using (roomofferoid)
		inner join offertype ot using (offertypeoid)
		inner join room r using (roomoid)
		inner join channel c using (channeloid)
		where
		p.publishedoid = '1' and pa.promotionoid = '".$id."' and ro.roomofferoid = '".$roomofferoid."'
		group by p.promotionoid
		";
	}
	$q_promotion = mysqli_query($conn, $s_promotion) or mysqli_error();
	$promotion = mysqli_fetch_array($q_promotion);

	$promotionoid = $promotion['promotionoid'];
	$hoteloid = $promotion['hoteloid'];
	$hotelname = $promotion['hotelname'];
	$roomname = $promotion['roomname'];
	$hotelcode = $promotion['hotelcode'];
	$roomoid = $promotion['roomoid'];
	$roomoffer = $promotion['roomofferoid'];
	$offertype = $promotion['offertypeoid'];
	$channeloid = $promotion['channeloid'];


	$discounttype = $promotion['discounttypeoid'];
	$discountapply = $promotion['discountapplyoid'];
	$applyvalue = explode(',', $promotion['applyvalue']);
	$discountvalue = $promotion['discountvalue'];

	if($adult == 0  and $child ==0){
		$adult = $promotion['adult']; $child = $promotion['child'];
	}


	if($promotion['discounttypeoid'] == 4){
		$night_after = $night + $promotion['discountvalue'];
	}else{
		$night_after = $night;
	}

	$total = 0;

	/*-----------------------------------------------------------------------*/
	if(!isset($delid) or $delid==0){
		$s_getid = "select bookingtempoid from bookingtempdtl inner join bookingtemp using (bookingtempoid) where session_id = '".$tknSession."' and promotionoid = '".$promotionoid."' and roomofferoid = '".$roomoffer."' and channeloid = '".$channeloid."' group by bookingtempoid";
		$q_getid = mysqli_query($conn, $s_getid) or mysqli_error();
		while($getid=mysqli_fetch_array($q_getid)){
			$s_delete = "delete bookingtemp, bookingtempdtl from bookingtemp, bookingtempdtl where bookingtemp.bookingtempoid = bookingtempdtl.bookingtempoid and bookingtemp.bookingtempoid = '".$getid['bookingtempoid']."'";
			$q_delete = mysqli_query($conn, $s_delete) or mysqli_error();
		}
	}else{
		$s_delete = "delete bookingtemp, bookingtempdtl from bookingtemp, bookingtempdtl where bookingtemp.bookingtempoid = bookingtempdtl.bookingtempoid and bookingtemp.bookingtempoid = '".$delid."'";
		$q_delete = mysqli_query($conn, $s_delete) or mysqli_error();
	}
	/*-----------------------------------------------------------------------*/


	for($r = 1; $r<=$booked_room; $r++){

		$total_room = 0;
		$total_roomonly = 0;
		$total_extrabed = 0;

		if($_GET['numroom']==0){ $numroom = $r; }

		echo "<div dtl=".$type."-".$typeid."-".$numroom.">";

		for($n = 0; $n<$night_after; $n++){

			$discountnote = '';

			$dateformat = explode('/',date("D/Y-m-d",strtotime($checkin." +".$n." day")));
			$day = $dateformat[0];
			$date = $dateformat[1];

      if(file_exists("../myhotel/data-xml/".$hotelcode.".xml")){
          $xml = simplexml_load_file("../myhotel/data-xml/".$hotelcode.".xml");
					$query_tag = '//hotel[@id="'.$hoteloid.'"]/masterroom[@id="'.$roomoid.'"]/rate[@date="'.$date.'"]/rateplan[@id="'.$roomoffer.'"]/channel[@type="'.$channeloid.'"]';
					foreach($xml->xpath($query_tag) as $xml_rate){
						$rate = $xml_rate->double;
						$currency = $xml_rate->currency;
						$extrabed = $xml_rate->extrabed;
						$breakfast = $xml_rate->breakfast;
					}
      }

			if(empty($currency)){ $currency= '1'; }
			if(
					$discountapply == '1' or
					($discountapply == '2' and in_array(($n+1), $applyvalue)) or
					($discountapply == '3' and in_array($day, $applyvalue)) or
					($discountapply == '4' and $n = 0) or
					($discountapply == '5' and $n = ($night - 1))
				){
					if($discounttype == '1'){
						$discountnote = $discountvalue."%";
						$discount = $rate * $discountvalue / 100;
					}else if($discounttype == '3'){
						$discountnote = $discountvalue;
						$discount = $discountvalue;
					}else{
						$discountnote = '';
						$discount = 0;
					}

					$rate_after = $rate - $discount;
			}else if(($n+1) > $night){
				$rate = 0;
				$rate_after = 0;
			}else{
				$rate_after = $rate;
			}

			$total_roomonly = $total_roomonly + $rate_after;

			if($with_extrabed == 0){ $extrabed = 0; }
			$total_extrabed = $total_extrabed + $extrabed;

			$rate_final = $rate_after + $extrabed;
			$total_room = $total_room + $rate_final;

			/*-----------------------------------------------------------------------*/
			$time = date('Y-m-d H:i:s');
			if($n == 0 ){
				$s_temp = "insert into bookingtemp (submittime, session_id, roomofferoid, channeloid, promotionoid, 
							checkin, night, adult, child, flag, breakfast, extrabed,
							hotel, room ) 
							values ('".$time."', '".$tknSession."', '".$roomoffer."', '".$channeloid."', '".$promotionoid."', 
							'".$checkin."', '".$night_after."', '".$adult."', '".$child."', '0', '".$breakfast."', '".$with_extrabed."', 
							'".$hotelname."', '".$roomname."')";
				// echo "<script>console.log(\"insert:".$s_temp."\");</script>";

				$q_temp = mysqli_query($conn, $s_temp) or mysqli_error();
				$temp_id = mysqli_insert_id($conn);
			}

			$s_tempdtl = "insert into bookingtempdtl (bookingtempoid, date, rate, discount, rateafter, extrabed, total, currencyoid) values ('".$temp_id."', '".$date."', '".$rate."', '".$discountnote."', '".$rate_after."', '".$extrabed."', '".$rate_final."', '".$currency."')";
			// echo "<script>console.log(\"insert:".$s_tempdtl."\");</script>";
			$q_tempdtl = mysqli_query($conn, $s_tempdtl) or mysqli_error();
			/*-----------------------------------------------------------------------*/
?>
			<input type="hidden" box="bookingtemp" tmp="<?php echo $type; ?>-<?php echo $typeid; ?>-<?php echo $numroom; ?>" value="<?php echo $temp_id; ?>"/>
			<input type="hidden" value="offerroomoid-<?php echo $date;  ?>/<?php echo $date;  ?>/<?php echo $rate;  ?>/<?php echo $discount;  ?>">
<?php

		} // night

		$checkout = date("Y-m-d",strtotime($checkin." +".$night." day"));

		$s_temp = "update bookingtemp set roomtotal='".$total_roomonly."', extrabedtotal='".$total_extrabed."', total='".$total_room."', currencyoid = '".$currency."', checkout = '".$checkout."' where bookingtempoid = '".$temp_id."'";
		// echo "<script>console.log(\"update:".$s_temp."\");</script>";
		$q_temp = mysqli_query($conn, $s_temp) or mysqli_error();

		$total = $total + $total_room;
?>
        <input type="hidden" box="totalroom" tmp="<?php echo $type; ?>-<?php echo $typeid; ?>" room="<?php echo $numroom; ?>" value="<?php echo $total_room;  ?>" night="<?php echo $night;?>">
        <input type="hidden" box="totalcurrency" tmp="<?php echo $type; ?>-<?php echo $typeid; ?>" room="<?php echo $numroom; ?>" value="IDR">
        <input type="hidden" box="extrabed" tmp="<?php echo $type; ?>-<?php echo $typeid; ?>-<?php echo $numroom; ?>" value="<?php echo $total_extrabed; ?>">
		</div>
<?php
	} // booked room
?>
