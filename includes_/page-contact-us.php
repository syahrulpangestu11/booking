<div id="single-content">	
    <div class="white-box border-box top-right-box">
    	<h2 class="default inline-block bottom-space"><span class="grey">Contact</span> <span class="blue">Us</span></h2>
	</div>
 	<div class="inline-block top">
        <div class="right-flat-box right-flat-box two_of_three squareborder border-box">
            <form id="guest-form" action="<?=$base_url;?>/contact-us/submit" method="post">
            <div id="contact-form">
                <h1 class="cpurple">Send Us Your Inquiry!</h1>
                <ul class="block">
                    <li>
                        <ul class="inline-block">
                            <li>
                                <label>First Name : </label>
                                <span><input type="text" name="firstname" id="firstname"></span>
                            </li>
                            <li>
                                <label>Last Name : </label>
                                <span><input type="text" name="lastname" id="lastname"></span>
                            </li>
                            <li>
                                <label>Email Address : </label>
                                <span><input type="text" name="email" id="email"></span>
                            </li>
                        </ul>
                    </li>     
                    <li>
                        <ul class="inline-block">
                            <li>
                                <label>Category : </label>
                                <span>
                                    <select name="category" id="category">
                                        <option selected="selected" value="0">--- Choose Category ---</option>
                                        <option value="Amend an Existing Reservation">Amend an Existing Reservation</option>
                                        <option value="Check the Status of an Existing Reservation">Check the Status of an Existing Reservation</option>
                                        <option value="Report a Problem with Site or Technical Error">Report a Problem with Site or Technical Error</option>
                                    </select>
                                </span>
                            </li>
                            <li>
                                <label>Booking Number <small>(optional)</small> : </label>
                                <span><input type="text" name="bookingnumber" id="bookingnumber"></span>
                            </li>
                        </ul> 
                    </li>
                    <li>
                        <ul class="inline-block">
                            <li>
                                <label>Inquiry : </label>
                                <span><textarea name="inquiry"></textarea></span>
                            </li>
                        </ul>
                    </li> 
                    <li>
                        <div class="clear"> 
                            <style>
                                #recaptcha_widget_div input{
                                    height:auto;
                                    font-size: 12px !important;
                                    border-radius: 0px;
                                }
                            </style>
                            <script type="text/javascript">
                                var RecaptchaOptions = {
                                    theme : 'white'
                                };
                            </script>
                            
                            <?php
                              require_once('includes/recaptcha-php-1.11/recaptchalib.php');
                              $publickey = "6LfvsgsTAAAAAPOYfauQvHKnzEAS2ATcXYOmuemo";
                              echo recaptcha_get_html($publickey);
                            ?>
                        </div>
                    </li>  
                    <li>
                        <input type="submit" name="submit" value="SUBMIT" class="general-submit">
                    </li>
                </ul>
            </div>
            </form>
        </div>
    </div>
    <div class="inline-block top fl_right">
        <div class="right-flat-box right-flat-box white-box border-box">
			<h1 class="cpurple">Maps</h1>
            <iframe width="520" height="338" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=Alila%20Seminyak%2C%20Denpasar%2C%20Bali%2C%20Indonesia&key=AIzaSyCuEPTN_7mOR6y3qPPM0LIVBdW8r_xubUw" allowfullscreen></iframe>
        </div>
    </div>
</div>    