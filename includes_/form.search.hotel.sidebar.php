<!-- <style>#checkin.hasDatepicker:focus{border:0;box-shadow:none;color: transparent;text-shadow: 0 0 0 #c22a25;}</style> -->
<form action="<?=$base_url; ?>/search/hotel/" method="get">
	<div class="form-group center">
        <div class="col-md-12 col-xs-12">
    	    <label for="q" class="">Destination</label>
            <input autocomplete="off" name="q" id="q-hotel" class="search-query form-control input-rounded" placeholder="Hotel, city, attraction, town" type="text" value="<?=$default_keyword;?>" >
        </div>
        <div class="clear"></div>
    </div>
	<div class="">
        <div id="div-check-in" class="form-group col-md-7 col-xs-12 text-center">
            <label for="div-check-in" class="">Check-In</label>
            <!-- <div class="field_decor_div_1"></div>
            <div class="field_decor_div_2">
                <span class="field_decor_span" >AUGUST, 2018</span>
                <hr class="field_decor_hr" >
            </div>
            <div> -->
                    <input type="text" name="checkin" id="checkin" class="form-control input-rounded" value="<?php echo $default_checkin; ?>" autocomplete="off">
            <!-- </div>
            <div class="field_decor_div_3"><i class="fa fa-chevron-down"></i></div> -->
        </div>
        <div id="div-night" class="form-group col-md-5 col-xs-4 text-center">
            <label for="div-night" class="">Night</label>
            <!-- <div class="field_decor_div_1"></div>
            <div class="field_decor_div_2">
                <span>&nbsp;</span>
                <hr class="field_decor_hr" >
            </div> -->
            <select name="night" class="form-control input-rounded" id="night">
            <?php 
            $requirenight = isset($requirenight) ? $requirenight : 1;
            for($n=$requirenight;$n<=15;$n++){ 
                if($n==$night){ $selected="selected='selected'";  }else{ $selected=""; } ?>
                <option value="<?php echo $n; ?>" <?php echo $selected; ?>><?php echo $n; ?></option>
                <?php 
            } ?>
            </select>
            <!-- <div class="field_decor_div_3"><i class="fa fa-chevron-down"></i></div> -->
		</div>
    <!-- </div>
	<div class="form-group row"> -->
        <div id="div-promo-code" class="form-group col-md-7 col-xs-4">
            <label for="destination" class=" text-center">Promo Code</label>
            <input type="text" name="promo" id="promo" class="form-control input-rounded" value="">
        </div>
        <div class="form-group col-md-5 col-xs-4">
            <label for="adult" class=" text-center">Adult</label>
            <select name="adult" class="form-control input-rounded" id="adult">
                <?php 
            $requireadult = isset($requireadult) ? $requireadult : 1;
            for($a=$requireadult;$a<=10;$a++){ 
                if($a==$adult){ $selected="selected='selected'";  }else{ $selected=""; } ?>
                <option value="<?php echo $a; ?>" <?php echo $selected; ?>><?php echo $a; ?></option>
                <?php 
            } ?>
            </select>
        </div>
        <!-- form-group   -->
    </div>
	<div class="form-group" style="display:none;">
        <label for="child" class="col-md-6 col-xs-6">Child:</label>
        <div class="col-md-6 col-xs-6">
            <div class="field_decor_div_1"></div>
            <div class="field_decor_div_2">
                <span>&nbsp;</span>
                <hr class="field_decor_hr" >
            </div>
            <select name="child" class="form-control" id="child">
            <?php 
            $requirechild = isset($requirechild) ? $requirechild : 0;
            for($c=$requirechild;$c<=3;$c++){ 
                if($c==$child){ $selected="selected='selected'";  }else{ $selected=""; } ?>
                <option value="<?php echo $c; ?>" <?php echo $selected; ?>><?php echo $c; ?></option>
                <?php 
            } ?>
            </select>
            <div class="field_decor_div_3"><i class="fa fa-chevron-down"></i></div>
		</div>
    </div>
	<div class="">
        <div class="col-md-12 col-xs-12">
            <!-- <button type="submit" id="submit" class="btn ">Search Hotels</button> -->
            <!-- btn-warning -->
            <button type="submit" onclick="showLoadingPage()" id="submit" class="btn btn-red btn-rounded" style="">SEARCH HOTEL</button>
        </div>
    </div>
</form>
