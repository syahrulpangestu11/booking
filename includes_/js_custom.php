<script>
	//Language
	$(document).ready(function(){
		$("select#languageoid").change(function() {
			$(this).parent("form").submit();
		});

		var windowWidth = $(window).width();
		var windowHeight = $(window).height();
		if(windowWidth<=736){ //cek apakah mobile
			windowHeightWS = $('#form-overlay').outerHeight();
		}else{
			windowHeightWS = windowWidth*7/16;
		}
		if(windowHeight>=windowHeightWS){
			windowHeight = windowHeightWS;
		}
		$('.carousel-inner > .item').css({
			'width': windowWidth + 'px',
			'height': windowHeight + 'px'
		});

		$('#myCarousel').on('slide.bs.carousel', function () {
			$(this).find('.thumbnailz').each(function(){
				var thisElem = $(this);
				var thisImg = thisElem.find('img');
				var elemWidth = thisElem.width();
				var elemHeight = thisElem.height();
				var elemOrientation = (elemWidth>elemHeight) ? "l" : "p";
				var imgWidth = thisImg.width();
				var imgHeight = thisImg.height();
				var imgOrientation = (imgWidth>imgHeight) ? "l" : "p";

				if((elemOrientation=="l" && imgOrientation=="l" && imgWidth<elemWidth)
					||
					(elemOrientation=="l" && imgOrientation=="p" && imgWidth<elemWidth)
					||
					(elemOrientation=="p" && imgOrientation=="p" && imgWidth<elemWidth)){
						thisImg.addClass('portrait');
				}
			});
		});

		//initialize swiper when document ready
		var mySwiper = new Swiper ('.card-container-v1', {
			slidesPerView: 5,
			spaceBetween: 40,
			pagination: {
				el: '#card-v1-pagination',
				clickable: true,
			},
			navigation: {
					nextEl: '#card-v1-button-next',
					prevEl: '#card-v1-button-prev',
				},
			breakpoints: {
				1024: {
					width: 400,
				slidesPerView: 2,
				spaceBetween: 30,
				},
				768: {
					width: 400,
				slidesPerView: 2,
				spaceBetween: 30,
				},
				640: {
					width: 400,
				slidesPerView: 2,
				spaceBetween: 20,
				},
				320: {
					width: 400,
				slidesPerView: 2,
				spaceBetween: 20,
				}
			}
		});

		//initialize swiper when document ready
		var mySwiper2 = new Swiper ('.card-container-v2', {
			slidesPerView: 4,
			spaceBetween: 20,
			pagination: {
				el: '#card-v2-pagination',
				clickable: true,
			},
			navigation: {
					nextEl: '#card-v2-button-next',
					prevEl: '#card-v2-button-prev',
				},
			breakpoints: {
				1024: {
					width: 500,
				slidesPerView: 2,
				spaceBetween: 30,
				},
				768: {
					width: 500,
				slidesPerView: 2,
				spaceBetween: 30,
				},
				640: {
					width: 500,
				slidesPerView: 2,
				spaceBetween: 20,
				},
				320: {
					width: 500,
				slidesPerView: 2,
				spaceBetween: 20,
				}
			}
		});

		var galleryThumbs = new Swiper('.gallery-thumbs', {
			spaceBetween: 10,
			slidesPerView: 10,
			loop: true,
			freeMode: true,
			loopedSlides: 5, //looped slides should be the same
			watchSlidesVisibility: true,
			watchSlidesProgress: true,
		});
		var galleryTop = new Swiper('.gallery-top', {
			spaceBetween: 10,
			autoplay: {
				delay: 3000,
			},
			loop:true,
			loopedSlides: 5, //looped slides should be the same
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
			thumbs: {
				swiper: galleryThumbs,
			},
		});

		var miniSwiper = new Swiper('.mini-swiper', {
			slidesPerView: 1,
			pagination: {
				el: '.swiper-pagination',
				clickable: true,
				// renderBullet: function (index, className) {
				// return '<span class="' + className + '">' + (index + 1) + '</span>';
				// },
			},
			autoplay: {
				delay: 2000,
			},
		});

		$('.img-link-wrapper').each(function(){
			var thisElem = $(this);
			var cardContentWrapper = thisElem.closest('.card-v3').find('.card-content-wrapper');
			if(cardContentWrapper.height() > thisElem.height()){
				thisElem.find('.img-link').height(cardContentWrapper.height());
			}
		});

	});
	
	
	
	//Toggle
	$('.toggle-display').click(function(e){
		e.preventDefault();

		//get collapse content selector
		var collapse_content_selector = $(this).attr('target');					
		
		//make the collapse content to be shown or hide
		var toggle_switch = $(this);
		// $(collapse_content_selector).toggle(function(){
		$(collapse_content_selector).slideToggle( 100, function() {
			var currentElemHeight = toggle_switch.closest('.card-v3').height();
			var currentImgLink = toggle_switch.closest('.card-v3').find('.img-link');
			if($(this).css('display')=='none'){
				currentImgLink.height(135);
			}else{
				currentImgLink.height(currentElemHeight);
			}
				setTimeout(function(){
					currentImgLink.find('.thumbnailz').each(function(){
						var thisElem = $(this);
						var thisImg = thisElem.find('img');
						var elemWidth = thisElem.width();
						var elemHeight = thisElem.height();
						var elemOrientation = (elemWidth>elemHeight) ? "l" : "p";
						var imgWidth = thisImg.width();
						var imgHeight = thisImg.height();
						var imgOrientation = (imgWidth>imgHeight) ? "l" : "p";

						if((elemOrientation=="l" && imgOrientation=="l" && imgWidth<elemWidth)
							||
							(elemOrientation=="l" && imgOrientation=="p" && imgWidth<elemWidth)
							||
							(elemOrientation=="p" && imgOrientation=="p" && imgWidth<elemWidth)){
								thisImg.addClass('portrait');
						}else{
							thisImg.removeClass('portrait');
						}
					});
				}, 150);
			
			
			
		});
	});
	
	
	//Star rating
	function starRating(star_id, star_value) {
		var selector = "div#" + star_id;
	    $(selector).raty({
	        readOnly 	: true,
	        half	 	: false,
	        precision  	: true,
	        target     	: '',
	        targetType 	: 'hint',/* 'number' or 'hint' */
	        targetFormat: 'Your score: {score}',
	        targetText  : 'none',
	        targetKeep  : true,
	        score	 	: star_value,
	        hints   	: ['Terrible', 'Poor', 'Average', 'Good', 'Excellent'],
	        noRatedMsg 	: 'Not rated yet',
	        starHalf  	: 'star-half.png',
	        starOn  	: 'star-on.png',
	        starOff 	: 'star-off.png'
	    });
	}


	document.addEventListener("DOMContentLoaded", function(event) { 

		var addImageOrientationClass = function(img) {
			var elemWidth = img.parentNode.offsetWidth;
			var elemHeight = img.parentNode.offsetHeight;
			var elemOrientation = (elemWidth>elemHeight) ? "l" : "p";
			var imgWidth = img.clientWidth;
			var imgHeight = img.clientHeight;
			var imgOrientation = (imgWidth>imgHeight) ? "l" : "p";
			
			if((elemOrientation=="l" && imgOrientation=="l" && imgWidth<elemWidth)
			||
			(elemOrientation=="l" && imgOrientation=="p" && imgWidth<elemWidth)
			||
			(elemOrientation=="p" && imgOrientation=="p" && imgWidth<elemWidth)){
				img.classList.add("portrait");			
			}
		}

		// Add "portrait" class to thumbnail images that are portrait orientation
		var images = document.querySelectorAll(".thumbnailz img");
		for(var i=0; i<images.length; i++) {
			if(images[i].complete) {
				addImageOrientationClass(images[i]);
			} else {
				images[i].addEventListener("load", function(evt) {
				addImageOrientationClass(evt.target);
				});
			}
		}

	});

	$(window).ready(function(){
			
		$(".thumbnailz img").each(function() {
			var elem = $(this);
			var elem_src = elem.attr("src");
			
			//--- Fungsi mereplace image jika src kosong
			if(elem_src.length == 0){
				elem.attr({
					"src": "<?php echo"$base_url"; ?>/images/default-thumb.png",
					"title": "Image for this item are not found."
				});
			}//end of //--- Fungsi mereplace image jika src tidak ditemukan
			
			//--- Fungsi mereplace image jika src tidak ditemukan
			elem.error(function() {
				//alert('Handler for .error() called.');
				elem.attr({
					"src": "<?php echo"$base_url"; ?>/images/default-thumb.png",
					"title": "Image for this item are not found."
				});
			});// end of //--- Fungsi mereplace image jika src tidak ditemukan
		});//end of $(".thumb img").each(function() {
		
	});


	if($(window).width() > 980){
		$(window).scroll(function (event) {
			var contentPaddingTop = $('#pgdtl').css('padding-top').slice(0,-2);
			var headerHeight = $(".main-header").outerHeight(true);
			// var top = $('.banner').outerHeight(true);;
			var top = contentPaddingTop - headerHeight;
			var formCA = $('#sidebar > .wrapper-floating');
			var propertyCenterHeight = $('#right-content').outerHeight(true);

			var y = $(this).scrollTop();
			var w = window.innerWidth ? window.innerWidth : $(window).width();
			if(propertyCenterHeight > formCA.outerHeight(true)){
				if (y >= top && w >= 980) {
					var wp = $('#sidebar').width();
					formCA.attr('style', "position:fixed; top: 0; width: "+wp+"px; margin-top: "+headerHeight+"px" );
					
					var formCAOffset = formCA.offset();
					var formCABottomOffset = formCAOffset.top + formCA.outerHeight(true);
					var footerTopOffset = $('.footer-wrapper').offset().top + 40;

					if(formCABottomOffset <= footerTopOffset){
						// formCA.find('#promocode').val('aaa');
					}else{
						// formCA.find('#promocode').val('bbb');
						$('#sidebar').attr('style', "position: relative; bottom: 0; height:"+propertyCenterHeight+"px;" );
						formCA.attr('style', "position: absolute; bottom: 0; width: "+wp+"px;" );
					}
				} else {
					formCA.attr('style', "");
				}
			}
		});
	}else{
		$('#sidebar #search .card-wrapper, #sidebar #hotels-nearby .card-wrapper, #sidebar #mini-map .card-wrapper, #sidebar #filters .card-wrapper').css({'display':'none'});
		$('#sidebar #search .card-section-title, #sidebar #hotels-nearby .card-section-title, #sidebar #mini-map .card-section-title, #sidebar #filters .card-section-title').click(function(){
			$(this).parent().find('.card-wrapper').slideToggle(500);
		});
	}
			

</script>