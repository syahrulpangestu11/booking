<div id="pgsch" class="container">
	<div class="row">
<?php
include("function.filter.php");
include_once("function.randomstring.php");

//include("list.hotel.query.php");
?>
<?php //LIST HOTEL
	require_once 'class/Paginator.class.php'; 
	require_once 'class/SearchHotel.class.php';

	//(ALL) Load DB and XML: Hotel,Room,Allotment,Rate ---------------------------------
	$List = new SearchHotel($conn,$_profile);
	$all_hotels = $List->getData($channeloid);
	$filter_hotels = $List->getFilterData($all_hotels);

	//(SELECTED) Paging Array and Load DB: Photo,Content ---------------------------------
	$Paginator = new Paginator($filter_hotels);
	$show_hotels = $Paginator->getData();
	$show_hotels = $List->loadPhotoContent($show_hotels);
	// echo "<pre>";
	// // print_r($hahaha);
	// echo $hahaha->data;
	// echo "</pre>";
	//echo '<script>console.log('.json_encode($show_hotels).');</script>';

	//Variable for FILTER HOTEL ---------------------------------
	$hoteloid_all = array(); $chainoid_all = array(); 
	$array_stars = array(); $array_hoteltype = array(); $array_minrate = array(); $array_chain = array();
	$hoteltype_names=array();
	foreach($all_hotels as $hotel){ 
		array_push($hoteloid_all,$hotel['hoteloid']); 
		array_push($array_stars,$hotel['stars']); 
		array_push($array_hoteltype,$hotel['hoteltypeoid']); 
		array_push($array_chain,$hotel['chainoid']); 
		array_push($array_minrate,$hotel['minrate']); 
		$hoteltype_names[$hotel['hoteltypeoid']]=$hotel['hoteltype'];
	}

	$filter_stars = array_count_values($array_stars);
	$filter_hoteltype = array_count_values($array_hoteltype);
	$filter_chain = array_count_values($array_chain);

	asort($filter_hoteltype);
	asort($filter_chain);

	foreach($filter_chain as $key => $val){ array_push($chainoid_all,$key); }

	
?>

<aside id="sidebar" class="col-md-3 col-sm-12">
<!-- flat-box -->
	<div class="card-rounded white-box" id="search">
		<div class="card-section-title text-center" ><b>Search</b> Another Hotel</div>
		<div class="card-wrapper">
			<?php //include("form.search.hotel.php"); ?>
			<?php include("form.search.hotel.sidebar.php"); ?>
		</div>
	</div>
	
	<div class="card-rounded white-box" id="filters">		
		
		<div class="card-section-title text-center" ><b>Filter</b> By</div>
		
		<div class="card-wrapper">
			<div class="filter-wrapper clear border-bottom" id="q-hotelname-wrapper">
				<span class="title">Hotel Name Contains</span>
				<a link="<?=querystring_remove("hotelname");?>" class="reset small-desc maroon text fl_right" style="display: none;">RESET</a>
				<div class="row">
					<div class="col-md-9 col-sm-9 col-xs-9">
						<input type="text" name="hotelname" id="q-hotelname" class="top filter-name form-control" value="<?=$_GET['hotelname'];?>"/>
					</div>
					<div class="col-md-3 col-sm-3 col-xs-3">
						<button id="submit-hotelname" class="general-submit top btn btn-red btn-rounded" >
							<i class="fa fa-search"></i>
						</button>
					</div>
				</div>
			</div>
			
			<div class="filter-wrapper clear border-bottom">
				<span class="title">Star Rating</span>
				<?php $reset_stars = querystring_remove("stars"); ?>
				<a link="<?=$reset_stars;?>" class="reset small-desc maroon text fl_right" style="display: none;">RESET</a>
				<?php 
				for($star = 5; $star>=0; $star--){
					
					$star_id = "star-".$star;
					$target_id = "target-".$star;
					
					$n_h_star=(isset($filter_stars[$star])?$filter_stars[$star]:0);
					
					$jml_hotel = "<span class='small-desc'> (".$n_h_star.") </span>";			
					
					if($star == 0){
						$note = "unrated ".$jml_hotel;
					}else if($star == 1){
						$note = $star." star ".$jml_hotel;
					}else{
						$note = $star." stars ".$jml_hotel;
					}
					
					$checked = checkedCB($_GET['stars'], $star);
					?>
					<div>
						<input type="checkbox" value="<?=$star;?>" id="cb-star-<?=$star;?>" class="cb-star inline-block" <?=$checked;?> />
						<label for="cb-star-<?=$star;?>">
							<div class="top">
							<?=starRating($star, 2)." &nbsp;".$note;?>
							</div>
						</label>
					</div>
					<?php
				}
				?>
				<input type="hidden" name="selected_star" id="selected-star" value="" />
			</div>
					
			<div class="filter-wrapper clear border-bottom">
				<span class="title">Price Group</span>
				<?php
				$pg1 = 0; $pg2 = 0; $pg3 = 0; $pg4 = 0; $pg5 = 0;
				foreach($all_hotels as $key => $val){
					if($val['minrate'] <= 499999){
						$pg1++;
					}else if($val['minrate'] <= 999999){
						$pg2++;
					}else if($val['minrate'] <= 1499999){
						$pg3++;
					}else if($val['minrate'] <= 1999999){
						$pg4++;
					}else{
						$pg5++;
					}
				}
				
				for($pg = 1; $pg<=5; $pg++){
					if($pg == 1){
						$pg_label = "less than IDR 499,999";
						$jml_hotel = $pg1;
					}else if($pg == 2){
						$pg_label = "IDR 500,000 - 999,999";
						$jml_hotel = $pg2;
					}else if($pg == 3){
						$pg_label = "IDR 1,000,000 - 1,499,999";
						$jml_hotel = $pg3;
					}else if($pg == 4){
						$pg_label = "IDR 1,500,000 - 1,999,999";
						$jml_hotel = $pg4;
					}else if($pg == 5){
						$pg_label = "2,000,000 +";
						$jml_hotel = $pg5;
					}
					//$jml_hotel = "12";
					$checked = checkedCB($_GET['price'], $pg);
					?>
					<div>
						<input type="checkbox" id="cb-price-<?=$pg;?>" value="<?=$pg;?>" class="cb-price" <?=$checked;?> />
						<label for="cb-price-<?=$pg;?>">
							<?=$pg_label;?>
							<span class="small-desc">(<?=$jml_hotel;?>)</span>
						</label>
					</div>
					<?php
					}
				?>
				<input type="hidden" id="selected-price" value="" />
			</div>
			
			<?php if(sizeof($filter_hoteltype)>0){ ?>
			<div class="filter-wrapper clear border-bottom">
				<span class="title">Accommodation Type</span>
				<a link="<?=querystring_remove("hoteltype");?>" class="reset small-desc maroon text fl_right" style="display: none;">RESET</a>
				<?php
				
				foreach($filter_hoteltype as $key=>$val){
					$hoteltypeoid = $key;//$row_at['hoteltypeoid'];
					$category = $hoteltype_names[$key];//$row_at['category'];;//
					$jml_hotel=(isset($filter_hoteltype[$hoteltypeoid])?$filter_hoteltype[$hoteltypeoid]:0);
					$checked = checkedCB($_GET['hoteltype'], $hoteltypeoid);
					?>
					<div>
						<input type="checkbox" id="cb-hoteltype-<?=$hoteltypeoid;?>" value="<?=$hoteltypeoid;?>" class="cb-hoteltype" <?=$checked;?> />
						<label for="cb-hoteltype-<?=$hoteltypeoid;?>" class="capitalize">
							<?=$category;?>
							<span class="small-desc">(<?=$jml_hotel;?>)</span>
						</label>
					</div>
					<?php
				}
				?>
				<input type="hidden" id="selected-hoteltype" value="" />
			</div>
			<?php } ?>
			
			<?php if(sizeof($hoteloid_all)>0){ ?>
			<div class="filter-wrapper clear border-bottom">
				<span class="title">Facilities</span>
				<a link="<?=querystring_remove("facilities");?>" class="reset small-desc maroon text fl_right" style="display: none;">RESET</a>
				<?php
				
					$s_amenities=$List->getQueryFacilities($hoteloid_all);
					$q_amenities = mysqli_query($conn, $s_amenities) or die(mysqli_error());
					while($r_amenities = mysqli_fetch_array($q_amenities)){
						$amenities_oid = $r_amenities['facilitiesoid'];
						$amenities_name = $r_amenities['name'];
						$jml_hotel = $r_amenities['jmlhotel'];
						$checked = checkedCB($_GET['facilities'], $amenities_oid);
						?>
						<div>
							<input type="checkbox" id="cb-facilities-<?=$amenities_oid;?>" value="<?=$amenities_oid;?>" class="cb-facilities" <?=$checked;?> />
							<label for="cb-facilities-<?=$amenities_oid;?>" class="capitalize">
								<?=$amenities_name;?>
								<span class="small-desc">(<?=$jml_hotel;?>)</span>
							</label>
						</div>	
						<?php
					}
				?>
				<input type="hidden" id="selected-facilities" value="" />
			</div>
			<?php } ?>

			<?php
			if(!empty($s_area)){ //hanya ditampilkan jika area tidak kosong
			?>
			<div class="filter-wrapper clear border-bottom">
				<span class="title">Area</span>
				<a link="<?=querystring_remove("area");?>" class="reset small-desc maroon text fl_right" style="display: none;">RESET</a>
				<?php
				$q_area = mysqli_query($conn, $s_area) or die(mysqli_error());
				while($r_area = mysqli_fetch_array($q_area)){
					//$area_oid = $area_type."-".$r_area['oid'];
					$area_oid = $r_area['oid'];
					$area_name = $r_area['name'];
					$jml_hotel = $r_area['jmlhotel'];
					$checked = checkedCB($_GET['area'], $area_oid);
					
					$jml_hotel = 0;
					foreach($arrListHotel as $key => $val){
						if($val['country'] == $area_name){
							$jml_hotel++;
						}else if($val['state'] == $area_name){
							$jml_hotel++;
						}else if($val['city'] == $area_name){
							$jml_hotel++;
						}
					}
					?>
					<div>
						<input type="checkbox" id="cb-area-<?=$area_oid;?>" value="<?=$area_oid;?>" class="cb-area" <?=$checked;?> />
						<label for="cb-area-<?=$area_oid;?>" class="capitalize">
							<?=$area_name;?>
							<span class="small-desc">(<?=$jml_hotel;?>)</span>
						</label>
					</div>
					<?php
				}
				?>
				<input type="hidden" id="selected-area" value="" />
			</div>
			<?php
			}
			?>
			
			
			<div class="filter-wrapper clear">
				<?php if(sizeof($chainoid_all)>0){ ?>
				<span class="title">Hotel chains &amp; brands</span>
				<a link="<?=querystring_remove("chain");?>" class="reset small-desc maroon text fl_right" style="display: none;">RESET</a>
				<?php
				
					$s_chain= $List->getQueryChain($chainoid_all);
					$q_chain = mysqli_query($conn, $s_chain) or die(mysqli_error());
					while($r_chain = mysqli_fetch_array($q_chain)){
						$chain_oid = $r_chain['chainoid']; 
						$chain_name = $r_chain['name'];
						$jml_hotel=(isset($filter_chain[$r_chain['chainoid']])?$filter_chain[$r_chain['chainoid']]:0);
						$checked = checkedCB($_GET['chain'], $chain_oid);
						?>
						<div>
							<input type="checkbox" id="cb-chain-<?=$chain_oid;?>" value="<?=$chain_oid;?>" class="cb-chain" <?=$checked;?> />
							<label for="cb-chain-<?=$chain_oid;?>" class="capitalize">
								<?=$chain_name;?>
								<span class="small-desc">(<?=$jml_hotel;?>)</span>
							</label>
						</div>
						<?php
					}
				?>
				<input type="hidden" id="selected-chain" value="" />
			</div>
			<?php } ?>
		</div>
		
		
	</div> <!-- end of .white-box -->
	
</aside>

<div id="right-content" class="col-md-9 col-sm-12">

	<div class="card-rounded white-box border-box" >
		<div class="clear">
			<div id="properties-found-text" class="inline-block">
				<?php
				$long_checkin = date("d M y", strtotime($checkin)); 
				$long_checkout = date("d M y", strtotime($checkout));
				?>
				<?php if(!empty($keyword)){?>
				<span class="title-blue"><?=$keyword;?>:</span> 
				<?php } /*
				on
				<b class="title-blue"><?=$long_checkin;?></b>
				to
				<b class="title-blue"><?=$long_checkout;?></b>
				for
				<span class="title-blue"><?=$adult;?></span>
				adult and
				<span class="title-blue"><?=$child;?></span>
				child
				*/ ?>
				<span class="title-blue"><?=$show_hotels->total;?></span>
				properties found
			</div>
			<div class="inline-block" id="sortbar">
				<!--<span>Sort By:</span>-->
                <ul>
                	<li class="great"><a>Recommended</a></li>
                	<li class="haschild"><a>Price</a>
                        <ul>
							<li class="sort-by" to="minrate-asc"><a>Lower to Higher</a></li>
                            <li class="sort-by" to="minrate-desc"><a>Higher to Lower</a></li>
                        </ul>
                    </li>
                	<li class="haschild"><a>Stars</a>
                        <ul>
                            <li><a class="sort-by" to="stars-asc">Stars (1-5)</a></li>
                            <li><a class="sort-by" to="stars-desc">Stars (5-1)</a></li>
                        </ul>
                    </li>
                	<li class="haschild"><a>Hotel Name</a>
                        <ul>
                            <li><a class="sort-by" to="hotelname-asc">Hotel Name (A-Z)</a></li>
                            <li><a class="sort-by" to="hotelname-desc">Hotel Name (Z-A)</a></li>
                        </ul>
                    </li>
                	<!-- <li class="haschild"><a>Review Score</a>
                        <ul>
                            <li><a class="sort-by" to="reviewed-desc">From Greatest Score</a></li>
                            <li><a class="sort-by" to="reviewed-asc">From Lowest Score</a></li>
                        </ul>
                    </li> -->
                </ul>
				<select name="sort_by" id="sort-by" style="display:none">
					<?php
					//$order_by = array("hotelname","hoteltype","stars");
					$order_by = array(
						'hotelname-asc' => "Hotel Name (A-Z)" ,
						'hotelname-desc' => "Hotel Name (Z-A)" , 
						'hoteltype.category' => "Accommodation Type" ,
						'stars-asc' => "Hotel Stars (0-5)" ,
						'stars-desc' => "Hotel Stars (5-0)" , 
						'minrate-asc' => "Lower to Higher" ,
						'minrate-desc' => "Higher to Lower" , 
						'reviewed-asc' => "From Lowest Score" ,
						'reviewed-desc' => "From Greatest Score" , 
					);
					foreach ($order_by as $key => $value) {
						$selected = ($_GET['sortby'] == $key) ? " selected " : "" ;
						?>
						<option value="<?=$key;?>" <?=$selected;?> ><?=$value;?></option>
						<?php
					}
					?>
				</select>
			</div> <!-- end of .fl_right -->
		</div> <!-- end of .clear -->
	</div>
	
	<?php //include("list.hotel.php"); ?>
	
	<div id="div-list-hotel">

		<div id="filters-overlay" style="visibility: hidden;">
			<div class="center middle sticky-message border-box" id="message">
				<span class="title">
					Retrieving result, <br>
					please wait... <br>
				</span>
				<img id="loading" src="<?=$base_url;?>/images/giphy-loading.gif" />
			</div>
		</div>
	
		<ul class="content-list" id="list-hotel">
		<?php
			foreach($show_hotels->data as $hotel){
				//$href_hotel = $hotel['hotelhref']; 
				
				$isInquiry = ($_profile['inquiry_status']=='y'&&$hotel['maxavailibility']==0?true:false);
				$parameters = ( isset($uri4) or !empty($uri4) or $uri4 != "" ) ? "/".$uri4 : "";
				$href_hotel = $base_url."/hotel/"
								.strtolower($hotel['continent'])."/".strtolower($hotel['country'])
								."/".strtolower($hotel['state'])."/".strtolower($hotel['city'])
								."/".str_replace(" ", "-", $hotel['hotelname'] )."/"
								.randomString(4).$hotel['hoteloid'].randomString(2).$parameters;
				?>
					<li id="h-<?=$hotel['hoteloid']?>" class="card-v3 card-rounded white-box border-box">
							<div class="col-md-3 col-sm-3 col-xs-4 img-link-wrapper">
								<a href="<?=$href_hotel;?>" class="img-link">
									<div class="thumbnailz"><img src="<?=$hotel['hotelpicture'];?>"></div>
								</a>
							</div>
							<div class="col-md-9 col-sm-9 col-xs-8 card-content-wrapper">
								<div class="row">
									<div class="col-md-9 top card-placeholder-1">
										<a href="<?=$href_hotel;?>" class="primary-theme">
											<div class="title"><?=$hotel['hotelname'];?></div>
										</a>
										<div><span class="badge badge-red capitalize"><?=$hotel['hoteltype'];?></span> &nbsp; <?php starRating($hotel['stars'], 1);?></div>
										<!-- <a class="primary-theme" href="#googlemapsview"><i class="fa fa-map-marker"></i> <?=$hotel['city'];?></a>  -->
										<div class="light-grey hotel-address-wrapper">
											<i class="fa fa-map-marker"></i> <span class="small-desc"><?=$hotel['address'];?></span>
										</div>
										<div class="hidden-xs">
											<i class="fa fa-info"></i> <span class="small-desc">There are <?=rand(1,9);?> people looking at this hotel.</span>
										</div>
										<div class="hidden-xs">
											<span class="small-desc" style="color:#f58803;"><?=($isInquiry?'<i class="fa fa-question"></i> '.$_profile['inquiry_wording']:'');?></span><br>

											<a href="#" target="#listroom-<?=$hotel['hoteloid'];?>" class="toggle-display primary-theme small-desc"><i class="fa fa-angle-double-down"></i> Show Room Rates</a>
										</div>
										<!-- <div><?=$hotel['hotelheadline'];?></div> -->
										<!-- <a href="#googlemapsview" class="button view-map nyroModal"> View Map</a> -->
										<!-- Go to www.addthis.com/dashboard to customize your tools -->
										<!-- <div class="addthis_sharing_toolbox"></div> -->
										<!-- Go to www.addthis.com/dashboard to customize your tools -->
										<!-- <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-543ce7af1ad924c7" async></script> -->
									</div>
									
									<div class="col-md-3 card-placeholder-2 top">
										<div class="price hidden-xs">
											start from 
											<h5 class="strikethrough-rate"><span class="strikethrough light-grey">
												<?php if($hotel['netrate'] > $hotel['minrate']){ echo $hotel['currencycode']."&nbsp;". $hotel['shownetrate']."<br>";  }?>
											</span></h5>
											<h3 class="rate">
												<span class="grey"><?=$hotel['currencycode'];?></span>
												<span class="blue"><?=$hotel['hotel_minrate_final'];?></span>
											</h3>
										</div>
										<div class="price-mobile hidden-xl hidden-lg hidden-md hidden-sm">
											<i class="fa fa-dollar-sign"></i> start from 
											<!-- <h5 class="strikethrough-rate"><span class="strikethrough light-grey">
												<?php if($hotel['netrate'] > $hotel['minrate']){ echo $hotel['currencycode']."&nbsp;". $hotel['shownetrate']."<br>";  }?>
											</span></h5> -->
											<span class="rate">
												<b><?=$hotel['currencycode'];?></b>
												<b><?=$hotel['hotel_minrate_final'];?></b>
											</span>
											
										</div>
										<div class="bottom-right hidden-xs">
											<a class="btn btn-<?=($isInquiry?'orange':'red');?>" onclick="showLoadingPage()" href="<?=$href_hotel;?>"><?=($isInquiry?'INQUIRY':'BOOK NOW');?></a>
										</div>
									</div>
									<div class="clear"></div>
								</div>

								<div id="listroom-<?=$hotel['hoteloid'];?>" class="listroom row" style="display:none;">
									<ul>
										<?php
											$qx = 1;
											foreach($hotel['rooms'][0] as $key => $name){
												//foreach($hotel['rooms'] as $key => $name){
												echo "<li><a href='".$href_hotel."'>";
												//echo "<span class='tosca-bold fl_right'>".$hotel['rates'][$key]."</span>";
												echo "<span class='gray-bold fl_right'>".$hotel['rates'][0][$key]."</span>";
												echo "<span class='gray-bold'>".$name."</span>";
												echo "</a></li>";
												if($qx == 5) break;
												$qx++;
											}
										?>
									</ul>
								</div>

							</div>
							<div class="clear"></div>
					</li>
			<?php
			}
			
			if((int)$show_hotels->total === 0){
			?>
				<img src="<?=$base_url;?>/images/warning.png" class="inline-block top" />
				<div class="inline-block top">Sorry, no results match your criteria.</div>
			<?php
			}
			?>
		</ul>
			
		<div class="text-center">
			<?php echo $Paginator->createLinks('pagination pagination-sm card-rounded'); ?>
		</div>

	</div>

</div>

</div>
</div>

<?php include("js_filter.search.php"); ?>

<script>
$(function(){
	$('#checkin').datepicker();
	$("#sortbar li.haschild").hover(function(){
		$(this).children("ul").slideDown("fast");
	},function(){
		$(this).children("ul").slideUp("fast");
	});
	
	$("a.sort-by").click(function(){
		var to = $(this).attr("to");
		$("#sort-by").val(to).change();
	});

})
</script>