<?php
$s_q_field1 = "
	SELECT tc.name, tc.description, tc.headline, t.touroid, t.tourpict, t.tourtypeoid, t.tourpricingoid, t.cityoid, t.stateoid, t.countryoid, t.continentoid,
	cityname, statename, countryname, continentname
";
$s_q_table1 = " FROM tour t INNER JOIN tourcontent tc USING (touroid) ";

// "q" query string
$s_q_table2 = "  
	LEFT JOIN city ON t.cityoid = city.cityoid 
	LEFT JOIN state ON t.stateoid = state.stateoid 
	LEFT JOIN country ON t.countryoid = country.countryoid 
	LEFT JOIN continent ON t.continentoid = continent.continentoid 
"; 
$s_q_condition1 = " t.publishedoid ='1' ";
$s_q_condition2 = " (continentname LIKE '%$keyword%' OR countryname LIKE '%$keyword%' OR statename LIKE '%$keyword%' OR cityname LIKE '%$keyword%' OR tc.name LIKE '%$keyword%' ) ";

$s_q_date1 = " (t.startdate<='$todaydate' and t.enddate>='$todaydate') ";
//$s_q_hdln1 = " (tc.headline IS NOT NULL AND tc.headline <> '') ";

// "area" query string
$s_area1 = "SELECT * FROM continent WHERE continentname = '$keyword'";
$q_area1 = mysqli_query($conn, $s_area1);
$n_area1 = mysqli_num_rows($q_area1);
if($n_area1 > 0){
	$r_area1 = mysqli_fetch_array($q_area1);
	$p_area_name = $r_area1['continentname'];
	$area_type = "country";
	$s_area = " SELECT countryoid AS oid, countryname AS name, COUNT(touroid) AS jmltour FROM tour 
				RIGHT JOIN city USING (cityoid) RIGHT JOIN state USING (stateoid) 
			 	RIGHT JOIN country USING (countryoid) RIGHT JOIN continent USING (continentoid) 
			 	WHERE continentname = '$p_area_name' GROUP BY countryoid ";
}else{
	$s_area2 = "SELECT * FROM country WHERE countryname = '$keyword'";
	$q_area2 = mysqli_query($conn, $s_area2);
	$n_area2 = mysqli_num_rows($q_area2);
	if($n_area2 > 0){
		$r_area2 = mysqli_fetch_array($q_area2);
		$p_area_name = $r_area2['countryname'];
		$area_type = "state";
		$s_area = " SELECT stateoid AS oid, statename AS name, COUNT(touroid) AS jmltour FROM tour 
					RIGHT JOIN city USING (cityoid) RIGHT JOIN state USING (stateoid) 
				 	RIGHT JOIN country USING (countryoid) 
				 	WHERE countryname = '$p_area_name' GROUP BY stateoid ";
	}else{
		$s_area3 = "SELECT * FROM state WHERE statename = '$keyword'";
		$q_area3 = mysqli_query($conn, $s_area3);
		$n_area3 = mysqli_num_rows($q_area3);
		if($n_area3 > 0){
			$r_area3 = mysqli_fetch_array($q_area3);
			$p_area_name = $r_area3['statename'];
			$area_type = "city";
			$s_area = " SELECT cityoid AS oid, cityname AS name, COUNT(touroid) AS jmltour FROM tour 
						RIGHT JOIN city USING (cityoid) RIGHT JOIN state USING (stateoid) 
					 	WHERE statename = '$p_area_name' GROUP BY cityoid ";
		}else{
			$area_type = "";
			$s_area = "";
		}
	}
}

// inline queries
$iq_tourname = (!empty($_GET['tourname'])) ? " AND tc.name LIKE '%".$_GET['tourname']."%' " : "" ;
$iq_area = (!empty($_GET['area'])) ? " AND ".$area_type."oid IN (".$_GET['area'].") " : "" ;

// sort
if(!empty($_GET['sortby'])){
	$exploded_sortby = explode("-", $_GET['sortby']);
	$sort_field = $exploded_sortby[0];
	$sort_order = $exploded_sortby[1];
	$sort_by = " ORDER BY ".$sort_field." ".$sort_order ;
}else{
	$sort_by = ""; 
}


if($page == "home"){
	$s_tour = 	$s_q_field1.$s_q_table1.$s_q_table2."
				WHERE 
				".$s_q_condition1." AND ".$s_q_date1." 
				GROUP BY t.touroid LIMIT 4";	
}else if ($page == "search-tour"){
	$s_tour =  	$s_q_field1.$s_q_table1.$s_q_table2."
				WHERE 
				".$s_q_condition1." AND ".$s_q_condition2." AND ".$s_q_date1.$iq_tourname.$iq_area."
				GROUP BY t.touroid ".$sort_by;	
}else if($page == "detail-tour"){
	/*
	$s_tour = "SELECT tourcontent.name, tourcontent.description, tourcontent.headline, A.touroid, tourpict, cityname, A.tourtypeoid, A.tourpricingoid, cityname, statename, countryname, continentname 
				FROM tour as A INNER JOIN tourcontent using (touroid)  inner joinINNER JOIN city using (cityoid) INNER JOIN state using (stateoid) INNER JOIN country USING (countryoid) INNER JOIN continent USING (continentoid) 
				WHERE cityname = '$tour_area' GROUP BY A.touroid"; //AND A.touroid <> '$touroid'	
	 */
}

$q_tour = mysqli_query($conn, $s_tour) or die(mysqli_error());

$n_tour = mysqli_num_rows($q_tour);

?>