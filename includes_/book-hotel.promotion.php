<?php
	$roomtype = array();
	$filter_periode_sale = "(salefrom <= '".$todaydate."' and saleto >= '".$todaydate."')";
	$filter_periode_book = "(bookfrom <= '".$checkin."' and bookto >= '".$checkout."')";
	$checkinday = date("D",strtotime($checkin));
	$todayday = date("D",strtotime($todaydate));

	$get_column_general = "select p.promotionoid, p.priority, p.name as promoname, p.discounttypeoid, p.discountapplyoid, p.applyvalue, p.discountvalue, p.minstay, p.maxstay,
	pt.type,
	pa.promotionapplyoid,
	ro.name as roomname, ro.roomofferoid,
	r.roomoid, r.name as roommaster, r.adult, r.child, r.description, r.roomsize,
	h.hoteloid, h.hotelcode";
	$get_column_roomoffer = "select ro.roomofferoid, r.roomoid, h.hoteloid, h.hotelcode";

	$s_promo = " from promotion p
	inner join promotiontype pt using (promotiontypeoid)
	inner join discounttype dt using (discounttypeoid)
	inner join hotel h using (hoteloid)
	inner join promotionapply pa using (promotionoid)
	inner join roomoffer ro using (roomofferoid)
	inner join offertype ot using (offertypeoid)
	inner join room r using (roomoid)
	inner join channel ch using (channeloid)
	where h.hoteloid = '".$hoteloid."' and ch.channeloid = '".$channeloid."'
	and ".$filter_periode_sale."  and ".$filter_periode_book."
	and p.minstay <= '".$night."'
	and p.displayed like '%".$todayday."%' and p.checkinon like '%".$checkinday."%'
	and p.publishedoid = '1' and ro.publishedoid = '1' and r.publishedoid = '1' ";

	$group_by = "group by ro.roomofferoid";
	$order_priority = "order by p.priority";

	$ro_oid = array();

	$syntax_get_roomoffer = $get_column_roomoffer.$s_promo.$group_by;
	$q_get_roomoffer = mysqli_query($conn, $syntax_get_roomoffer) or mysqli_error();
	while($gr = mysqli_fetch_array($q_get_roomoffer)){

		$hotelcode = $gr['hotelcode'];
		$hoteloid = $gr['hoteloid'];
		$roomoid = $gr['roomoid'];
		$roomoffer = $gr['roomofferoid'];
		$rate[$roomoffer] = array();
		$rate_extrabed[$roomoffer] = array();
		$limitallotment[$roomoffer] = array();
		
		if(file_exists("data-xml/".$hotelcode.".xml")){
            $xml = simplexml_load_file("data-xml/".$hotelcode.".xml");

    		//Hotel Photo Main
    		$s_rp = "SELECT photourl FROM hotelphoto hp WHERE hp.hoteloid = '".$hoteloid."' AND hp.flag = 'main' AND ref_table = 'room' AND ref_id = '".$gr['roomoid']."'";
    		//echo $s_rp."<br>";
    		$q_rp = mysqli_query($conn, $s_rp);
    		$r_rp = mysqli_fetch_array($q_rp);
    		$room_pict = $r_rp['photourl'];
    		$picture[$roomoffer] = $room_pict;
    
    		for($n=0;$n<$night;$n++){
    			$date = date("Y-m-d",strtotime($checkin." +".$n." day"));
    
    			$query_tag = '//hotel[@id="'.$hoteloid.'"]/masterroom[@id="'.$roomoid.'"]/rate[@date="'.$date.'"]/rateplan[@id="'.$roomoffer.'"]/channel[@type="'.$channeloid.'"]';
    			$single=0;$extrabed=0;$allotment=0;
    			foreach($xml->xpath($query_tag) as $tagrate){
    				$single = (float)$tagrate->double;
    				$extrabed = (float)$tagrate->extrabed;
    				$currency = $tagrate->currency;
    				$allotment = $tagrate->allotment;
    			}
    
    			/*zc*/
    			$query_tag = '//hotel[@id="'.$hoteloid.'"]/masterroom[@id="'.$roomoid.'"]/rate[@date="'.$date.'"]/allotment/channel[@type="'.$channeloid.'"]';
    			foreach($xml->xpath($query_tag) as $tagrate){
    				$allotment = $tagrate[0];
    			}
    
    			$s_occupied = "select count(bookingroomoid) as occupied
    			from bookingroom
    			inner join roomoffer ro using (roomofferoid)
    			inner join offertype ot using (offertypeoid)
    			inner join room r using (roomoid)
    			inner join channel ch using (channeloid)
    			inner join hotel using (hoteloid)
    			inner join booking using (bookingoid)
    			inner join bookingroomdtl using (bookingroomoid)
    			where ro.roomofferoid = '".$roomoffer."' and ch.channeloid = '".$channeloid."' and date = '".$date."'";
    			$q_occupied = mysqli_query($conn, $s_occupied) or die(mysqli_error());
    			$r_occupied = mysqli_fetch_array($q_occupied);
    			$occupied = $r_occupied['occupied'];
    
    			$available = $allotment - $occupied;
    			//echo "(".$n.")".$available."-";
    
    			if($available > 0 and $single > 0){
    				if($n == 0){ array_push($ro_oid,$roomoffer); $limitallotment[$roomoffer] = $available;  }
    				if($available < $limitallotment[$roomoffer]){ $limitallotment[$roomoffer] = $available; }
    				if(!in_array($single, $rate[$roomoffer])){ $rate[$roomoffer][] = $single; }
    				if(!in_array($extrabed, $rate_extrabed[$roomoffer])){ $rate_extrabed[$roomoffer][] = $extrabed; }
    			}else{
    				unset($rate[$roomoffer]);
    				unset($rate_extrabed[$roomoffer]);
    				unset($picture[$roomoffer]);
    				unset($limitallotment[$roomoffer]);
    				if(($key = array_search($roomoffer, $ro_oid)) !== false) {
    					unset($ro_oid[$key]);
    				}
    				break;
    			}
    			//echo "(".$n.")". $extrabed."-";
    		}
        }
	}

	if(count($ro_oid) > 0){
		$list_ro_id = implode(",", $ro_oid);
		$selected_roomoffer = " and ro.roomofferoid in (".$list_ro_id.") ";
	}

	$syntax_get_promotion = $get_column_general.$s_promo.$selected_roomoffer.$order_priority;
	$q_get_promotion = mysqli_query($conn, $syntax_get_promotion) or mysqli_error();
	while($promo = mysqli_fetch_array($q_get_promotion)){
		$discounttype = $promo['discounttypeoid'];
		$discountvalue = $promo['discountvalue'];
		$roomoid = $promo['roomoid'];
		$minstay = $promo['minstay'];
		$maxstay = $promo['maxstay'];

		if($maxstay==0 or ($maxstay!=0 and $night<=$maxstay)) {
			if(sizeof($rate)>0){
				foreach($rate[$promo['roomofferoid']] as $key => $single){
					$row_id= $promo['roomoid'].'-'.$promo['roomofferoid'].'-'.$promo['promotionoid'];

					if($discounttype == '1'){
						$discountnote = $discountvalue."%";
						$discount = $single * $discountvalue / 100;
					}else if($discounttype == '3'){
						$discountnote = $discountvalue;
						$discount = $discountvalue;
					}else{
						$discount = 0;
					}

					$rate_after = $single - $discount;

					$roomrate = number_format($rate_after);
					$barrate = number_format($single);
					$extrabedrate = number_format($rate_extrabed[$promo['roomofferoid']][$key]);

					$countrate++;

					if($limitallotment[$promo['roomofferoid']] > 5){
						$default_allotment = 5;
					}else{
						$default_allotment = $limitallotment[$promo['roomofferoid']];
					}
					$currencycode = "IDR";

	?>
					<li class="table-row">
						<div class="table-cell top">
							<span class="title"><?php echo $promo['roomname']; ?><br />(<?php echo $promo['promoname']; ?>)</span>
							<div class="thumb pict"><img src="<?php echo $picture[$promo['roomofferoid']];?>"></div>
							<div>
								<strong>Max Occupancy : </strong>
								<?php
								for($i=1;$i<=$promo['adult'];$i++){ ?>
									<img src="<?=$base_url;?>/images/adult.png">
									<?php
								}
								for($i=1;$i<=$promo['child'];$i++){ ?>
									<img src="<?=$base_url;?>/images/child.png">
									<?php
								}
								?>
							</div>
							<a class="nyroModal" href="<?php echo $base_url; ?>/thebuking/popup/room-type.php?d=<?php echo $roomoid; ?>">room info</a>

						</div>
						<div class="table-cell top center">
							<h4 style="text-decoration:line-through;"><span class="grey"><?php echo $currencycode;  ?></span> <span class="grey"><?php echo $barrate; ?></span></h4>
							<h3><span class="grey"><?php echo $currencycode;  ?></span> <span class="blue"><?php echo $roomrate; ?></span></h3>
							<?php
								if($extrabedrate > 0){
							?>
							<b><span class="grey">need extrabed?</span></b>
							<h4><span class="blue" style="font-family:Tahoma, Geneva, sans-serif">+</span><span class="grey"><?php echo $currencycode;  ?></span> <span class="blue"><?php echo $extrabedrate;  ?></span></h4>
							<?php
								}
							?>
						</div>
						<div class="table-cell top center">
							<input type="hidden" name="roomoid" id="roomoid" value="<?php echo $promo['roomoid']; ?>">
							<input type="hidden" name="promotionapplyoid" id="promoapply" value="<?php echo $promo['promotionapplyoid']; ?>">
							<select room="number">
								<?php for($r=0;$r<=$default_allotment;$r++) { ?>
								<option value="<?php echo $r; ?>"><?php echo $r; ?></option>
								<?php } ?>
							</select>
							<div></div>
						</div>
					</li>
	<?php
				}
			}
		}
	}
?>
