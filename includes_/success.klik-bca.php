<style type="text/css">
.confirm { background-color: #f2fbe5; border: 1px solid #c7e4a0; padding: 20px; -moz-border-radius: 5px; -webkit-border-radius: 5px; -khtml-border-radius: 5px; border-radius: 5px; margin: 10px 0; color: #333; }
</style>
<?php
	$rpgrandtotal = number_format($_POST['grandtotal']);
	$showcurrency	 = mysqli_fetch_array(mysqli_query($conn, "select currencycode from currency where currencyoid='".$_POST['currencyoid']."'"));
	$currencycode	= $showcurrency['currencycode'];
	$get_sub = substr($_POST['bookingnumber'], 4,11);
?>
<div class="white-box border-box top-right-box" id="transfer-policies">
	<h2><span class="grey">Please complete the payment </span><span class="blue">with KlikBCA</span></h2>
	<div class="confirm">
		<h3>Your transaction has been recorded in KlikBCA, please make payment on KlikBCA.com immediately by login using your KlikBCA UserID.</h3>
	</div>

   <h2><span class="grey">Please follow these steps to </span><span class="blue">complete your payment.</span></h2>
    <ol style="padding-left:25px;">      
        <li>Visit KlikBCA website in <strong>http://www.klikbca.com/</strong></li>
        <li>Login using your user ID. <strong>(<?php echo $_POST['userkbca']; ?>)</strong></li>
        <li>Go to menu <strong>' e-Commerce Payment'</strong></li>
        <li>Choose category <strong>'Tour / Travel / Hotel'</strong></li>
        <li>Choose company name <strong>'GOBALIHOLIDAY.COM'</strong></li>
        <li>Click button <strong>'Continue'</strong></li>
        <li>Choose transaction with booking code <strong> '<?php echo $get_sub; ?>'</strong> then make payment.</li>
		<li>After you have completed the payment, <strong>you will receive an e-mail within 5 minutes</strong> containing your <strong>Hotel Voucher for Hotel purchase or Booking Code</strong></li>
        <li>For users of Apple Macintosh Operating System is recommended not to use the Google Chrome browser, please use another browser (eg. Safari)</li>
    </ol>
</div>