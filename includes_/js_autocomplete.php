<link rel="stylesheet" href="<?=$base_url;?>/scripts/typeahead-autocomplete/typeahead.css">
<script src="<?=$base_url;?>/scripts/typeahead-autocomplete/hogan-2.0.0.js"></script>
<script type="text/javascript" src="<?=$base_url;?>/scripts/typeahead-autocomplete/typeahead.js"></script>
<script>
	// single dataset
	$(document).ready(function(){
		<?php /*
		var arrDestination_basic = [
		  	<?php
			$s_ac = "SELECT continentname AS name FROM continent UNION SELECT countryname AS name FROM country UNION SELECT statename AS name FROM state UNION SELECT cityname AS name FROM city";
			$q_ac = mysqli_query($conn, $s_ac); $x = 0;
			while($r_ac = mysqli_fetch_array($q_ac)){
				if($x == 0){
					$x = 1;
					$suggestion = "'".$r_ac['name']."'";
				}else{
					$suggestion = ", '".$r_ac['name']."'";
				}
				echo $suggestion;
				//echo "<h2>".$suggestion."</h2>";
			}
			?>
		];
		
		
		var arrDestination = [
		  	<?php
			$s_ac = "	
				SELECT continentname AS name, COUNT(hoteloid) AS jmlhotel FROM hotel 
					RIGHT JOIN city USING (cityoid) RIGHT JOIN state USING (stateoid) 
				 	RIGHT JOIN country USING (countryoid) RIGHT JOIN continent USING (continentoid) 
				 	GROUP BY continentoid 
				UNION 
				SELECT countryname AS name, COUNT(hoteloid) AS jmlhotel FROM hotel 
					RIGHT JOIN city USING (cityoid) RIGHT JOIN state USING (stateoid) 
				 	RIGHT JOIN country USING (countryoid) 
				 	GROUP BY countryoid
				UNION 
				SELECT statename AS name, COUNT(hoteloid) AS jmlhotel FROM hotel 
					RIGHT JOIN city USING (cityoid) RIGHT JOIN state USING (stateoid) 
				 	GROUP BY stateoid
				UNION 
				SELECT cityname AS name, COUNT(hoteloid) AS jmlhotel FROM hotel 
					RIGHT JOIN city USING (cityoid)
				 	GROUP BY cityoid
			";
			$q_ac = mysqli_query($conn, $s_ac) or die(mysqli_error()); 
			$x = 0;
			while($r_ac = mysqli_fetch_array($q_ac)){
				$name = $r_ac['name'];
				$jmlhotel = $r_ac['jmlhotel'];
				$note = ($jmlhotel > 1) ? " hotels" : " hotel";
				$json_template = '
					{
						"name": "'.$name.'",
					    "jmlhotel": "'.$jmlhotel.'",
					    "value": "'.$name.'",
					    "note": "'.$note.'"
					}
				
				';
				
				if($x == 0){
					$x = 1;
					$suggestion = $json_template;
				}else{
					$suggestion = ", ".$json_template;
				}
				echo $suggestion;
			}
			?>
		];
		*/ ?>
		
		<?php
		function storeToArray($conn, $s_ac){
			$q_ac = mysqli_query($conn, $s_ac) or die(mysqli_error()); 
			$x = 0;
			while($r_ac = mysqli_fetch_array($q_ac)){
				$name = $r_ac['name'];
				$jmlhotel = $r_ac['jmlhotel'];
				$note = ($jmlhotel > 1) ? " hotels" : " hotel";
				$json_template = '
					{
						"name": "'.$name.'",
					    "jmlhotel": "'.$jmlhotel.'",
					    "value": "'.$name.'",
					    "note": "'.$note.'"
					}
				
				';
				
				if($x == 0){
					$x = 1;
					$suggestion = $json_template;
				}else{
					$suggestion = ", ".$json_template;
				}
				echo $suggestion;
			}
		}
		
		function typeaheadOptions($jenis){
			
		}
		?>
		
		var arr_continent = [
		  	<?php
			$s_ac = "
				SELECT continentname AS name, COUNT(hoteloid) AS jmlhotel FROM hotel 
				RIGHT JOIN city USING (cityoid) RIGHT JOIN state USING (stateoid) 
			 	RIGHT JOIN country USING (countryoid) RIGHT JOIN continent USING (continentoid) 
				WHERE hotel.hotelstatusoid='1' and hotel.publishedoid='1' 
			 	GROUP BY continentoid 
			";
			storeToArray($conn, $s_ac);
			?>
		];
		
		
		var arr_country = [
		  	<?php
			$s_ac = "
				SELECT countryname AS name, COUNT(hoteloid) AS jmlhotel FROM hotel 
				RIGHT JOIN city USING (cityoid) RIGHT JOIN state USING (stateoid) 
			 	RIGHT JOIN country USING (countryoid) 
				WHERE hotel.hotelstatusoid='1' and hotel.publishedoid='1' 
			 	GROUP BY countryoid
			";
			storeToArray($conn, $s_ac);
			?>
		];
		
		var arr_state = [
		  	<?php
			$s_ac = "
				SELECT statename AS name, COUNT(hoteloid) AS jmlhotel FROM hotel 
				RIGHT JOIN city USING (cityoid) RIGHT JOIN state USING (stateoid) 
				WHERE hotel.hotelstatusoid='1' and hotel.publishedoid='1' 
			 	GROUP BY stateoid
			";
			storeToArray($conn, $s_ac);
			?>
		];
		
		var arr_city = [
		  	<?php
			$s_ac = "
				SELECT cityname AS name, COUNT(hoteloid) AS jmlhotel FROM hotel 
				RIGHT JOIN city USING (cityoid)
				WHERE hotel.hotelstatusoid='1' and hotel.publishedoid='1' 
			 	GROUP BY cityoid
			";
			storeToArray($conn, $s_ac);
			?>
		];
		
		var arr_hotel = [
		  	<?php
			$s_ac = "
				SELECT hotelname AS name, stars AS jmlhotel FROM hotel 
				WHERE hotel.hotelstatusoid='1' and hotel.publishedoid='1' 
			 	GROUP BY hoteloid LIMIT 5
			";
			storeToArray($conn, $s_ac);
			?>
		];
		
		
		$('#q-hotel').typeahead([
			<?php 
			$array_jenis = array('continent','country','state','city','hotel');
			$x=0;
			foreach ($array_jenis as $key => $value) {
				//mengubah ke bentuk jamak , misal: country->countries
				if(substr($value, -1,1) == "y"){
					$append = "ies";
					$header = substr($value, 0, strlen($value)-1).$append;
				}else{
					$append = "s";
					$header = $value.$append;
				}
				
				if($value == "hotel"){
					$typeahead = "{
					  	name: '".$value."',
					  	header: '<div class=\"header capitalize\"><i>".$header."</i></div>',
					  	local: arr_".$value.",
					  	limit: 5,
						template: [
						'<div class=\"jmlhotel jmlstar fl_right title-blue\" jml=\"{{jmlhotel}}\"></div>',
						'<div class=\"\">{{name}}</div>'
						].join(''),
						engine: Hogan
					}";	
				}else{
					$typeahead = "{
					  	name: '".$value."',
					  	header: '<div class=\"header capitalize\"><i>".$header."</i></div>',
					  	local: arr_".$value.",
					  	limit: 5,
						template: [
						'<div class=\"jmlhotel fl_right title-blue\"><b>{{jmlhotel}}</b> {{note}}</div>',
						'<div class=\"\">{{name}}</div>'
						].join(''),
						engine: Hogan
					}";
				}
				
				
				//menambah koma utk options ke 2,3, dst
				if($x == 0){
					$x = 1;
					$typeahead_options = $typeahead;
				}else{
					$typeahead_options = ", ".$typeahead;
				}
				echo $typeahead_options;
			}
			?>
		]);
		
		$("#q-hotel").keyup(function(){
			$(".jmlstar").each(function(){
				var elem = $(this);
				var star = elem.attr("jml");
				var x = "";
				for(var a=0; a<star; a++){
					x = x + "<img src='<?=$base_url;?>/images/star-on.png'>";
				}
				elem.html(x);
			});
		});
		
		
		$('#q-tour').typeahead([
			<?php 
			$array_jenis = array('continent','country','state','city');
			$x=0;
			foreach ($array_jenis as $key => $value) {
				//mengubah ke bentuk jamak , misal: country->countries
				if(substr($value, -1,1) == "y"){
					$append = "ies";
					$header = substr($value, 0, strlen($value)-1).$append;
				}else{
					$append = "s";
					$header = $value.$append;
				}
			
				$typeahead = "{
				  	name: '".$value."',
				  	header: '<div class=\"header capitalize\"><i>".$header."</i></div>',
				  	local: arr_".$value.",
				  	limit: 5,
					template: [
					'<div class=\"jmlhotel fl_right title-blue\"><b>{{jmlhotel}}</b> {{note}}</div>',
					'<div class=\"\">{{name}}</div>'
					].join(''),
					engine: Hogan
				}";
				
				//menambah koma utk options ke 2,3, dst
				if($x == 0){
					$x = 1;
					$typeahead_options = $typeahead;
				}else{
					$typeahead_options = ", ".$typeahead;
				}
				echo $typeahead_options;
			}
			?>
		]);
		
		
		<?php
		/*
		$('#q-hotel').typeahead([
		/*
		{
		  	name: 'destination',
			//local: ['Alaska', 'Zefanya', 'Qwerty']
		  	header: "<div>Destination</div>",
		  	local: arrDestination
			template: [
			'<div class="fl_right title-blue"><b>{{jmlhotel}}</b> {{note}}</div>',
			'<div class="">{{name}}</div>'
			].join(''),
			engine: Hogan
		},
		/
		{
		  	name: 'continent',
		  	header: "<div class='header small-desc'>Continent</div>",
		  	local: arrContinent,
			template: [
			'<div class="fl_right title-blue"><b>{{jmlhotel}}</b> {{note}}</div>',
			'<div class="">{{name}}</div>'
			].join(''),
			engine: Hogan
		},
		{
		  	name: 'country',
		  	header: "<div class='header small-desc'>Country</div>",
		  	local: arrCountry,
			template: [
			'<div class="fl_right title-blue"><b>{{jmlhotel}}</b> {{note}}</div>',
			'<div class="">{{name}}</div>'
			].join(''),
			engine: Hogan
		},
		{
		  	name: 'state',
		  	header: "<div class='header small-desc'>State</div>",
		  	local: arrState,
			template: [
			'<div class="fl_right title-blue"><b>{{jmlhotel}}</b> {{note}}</div>',
			'<div class="">{{name}}</div>'
			].join(''),
			engine: Hogan
		},
		{
		  	name: 'city',
		  	header: "<div class='header small-desc'>City</div>",
		  	local: arrCity,
			template: [
			'<div class="fl_right title-blue"><b>{{jmlhotel}}</b> {{note}}</div>',
			'<div class="">{{name}}</div>'
			].join(''),
			engine: Hogan
		}
		]);
		
		
		
		$('#q-tour').typeahead({
			name: 'tour',
			prefetch: '<?=$base_url;?>/scripts/typeahead-autocomplete/repos.json',
			//local: arrDestination,
			
			template: [
			'<div class="fl_right small-desc">{{language}}</div>',
			'<h5 class="default">{{name}}</h5>',
			'<div>{{description}}</div>'
			].join(''),
			engine: Hogan
		});
		*/?>
		
		/*
		template: [
			'<div class="repo-language">{{language}}</div>',
			'<div class="repo-name">{{name}}</div>'
			].join(''), 
		*/
	});	
</script>