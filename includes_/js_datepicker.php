<!--<link rel="stylesheet" href="<?php echo"$base_url"; ?>/scripts/datepicker/themes/base/jquery.ui.all.css">-->
<!-- <link rel="stylesheet" href="<?php echo"$base_url"; ?>/scripts/datepicker/themes/base/jquery.ui.theme.css">
<link rel="stylesheet" href="<?php echo"$base_url"; ?>/scripts/datepicker/themes/base/jquery.ui.core.css">
<link rel="stylesheet" href="<?php echo"$base_url"; ?>/scripts/datepicker/themes/base/jquery.ui.datepicker.css">
<script src="<?php echo"$base_url"; ?>/scripts/datepicker/ui/jquery.ui.core.js"></script>
<script src="<?php echo"$base_url"; ?>/scripts/datepicker/ui/jquery.ui.widget.js"></script>
<script src="<?php echo"$base_url"; ?>/scripts/datepicker/ui/jquery.ui.datepicker.js"></script>    -->

  <!-- <script src="//code.jquery.com/jquery-1.12.4.js"></script> -->

	<!-- /*zc*/ -->
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script type="text/javascript">
//$(function() {
$(document).ready(function(){
// console.log('js_datepicker');
	/*zc*/
	var dates = $( "#checkout, #checkin" ).datepicker({
		defaultDate: "+0", changeMonth: true, numberOfMonths: 1, changeYear: true,minDate: 0,
		dateFormat:"dd MM yy",
		onSelect: function( selectedDate ) {
      //console.log('select');
      if(this.id == "checkin"){
        var tci= new Date($("#checkin").val());
        $('.field_decor_span').text(tci.toLocaleString("en-us", { month: "short" }).toUpperCase()+', '+ tci.getFullYear());
      }
			var option = this.id == "checkin" ? "minDate" : "maxDate",
				instance = $( this ).data( "datepicker" );
				date = $.datepicker.parseDate( instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings );
				dates.not( this ).datepicker( "option", option, date );;
			if($("#checkin").val() == $("#checkout").val()) { //if value from #from same with #to or value #from more than value of #to
				var x = $("#checkin").val(); var temp = new Array(); temp = x.split(" "); var d = parseInt(temp[0]); var m = temp[1]; var y = parseInt(temp[2]);
				switch(m) {
					case "January"	: if(d==31) { d = 1; m = "February"; }else { d = d+1; } break;
					case "February"	: if(y%4 == 0) { if(d==29) { d = 1; m = "March"; }else { d = d+1; } }else { if(d==28) { d = 1; m = "March"; }else { d = d+1; } } break;
					case "March"	: if(d==31) { d = 1; m = "April"; }else { d = d+1; } break;
					case "April"	: if(d==30) { d = 1; m = "May"; }else { d = d+1; } break;
					case "May"		: if(d==31) { d = 1; m = "June"; }else { d = d+1; } break;
					case "June"		: if(d==30) { d = 1; m = "July"; }else { d = d+1; } break;
					case "July"		: if(d==31) { d = 1; m = "August"; }else { d = d+1; }break;
					case "August"	: if(d==31) { d = 1; m = "September"; }else { d = d+1; } break;
					case "September": if(d==30) { d = 1; m = "October"; }else { d = d+1; } break;
					case "October"	: if(d==31) { d = 1; m = "November"; }else { d = d+1; } break;
					case "November"	: if(d==30) { d = 1; m = "December"; }else { d = d+1; } break;
					case "December"	: if(d==31) { d = 1; m = "January"; y = y + 1; }else { d = d+1;} break;
				}$("#checkout").val(d+" "+m+" "+y);
			}
		}
	});

  var tci= new Date();
  $('.field_decor_span').text(tci.toLocaleString("en-us", { month: "short" }).toUpperCase()+', '+ tci.getFullYear());

	$( "#departflight" ).datepicker({
      defaultDate: "+0",
      dateFormat: "yy-mm-dd",
      minDate: 0,
      changeMonth: true,
      numberOfMonths: 1,
      onClose: function( selectedDate ) {
        $( "#returnflight" ).datepicker( "option", "minDate", selectedDate );
      }
    });

    $( "#returnflight" ).datepicker({
      defaultDate: "+0",
      dateFormat: "yy-mm-dd",
      minDate: 0,
      changeMonth: true,
      numberOfMonths: 1,
      onClose: function( selectedDate ) {
        $( "#departflight" ).datepicker( "option", "maxDate", selectedDate );
      }
    });
});
</script>
