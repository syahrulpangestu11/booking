<?php 
include_once("includes/function-php/function.merge_querystring.php");

	?>
	</ul> <!-- menutup div sebelumnya utk menyisipkan pagination -->
	<div id="<?=$pgn_container_id;?>" class="white-box border-box center clear pagination-container">
		<?php
		if(!empty($_GET)){
			$current_url=getUrl();
		}else{
			$current_url=getUrl()."/";
		}
		
		$tampil2=$pgn_tampil2;
		$hasil2=mysqli_query($conn, $tampil2);
		$hasil2_limited=mysqli_query($conn, $tampil2.$limit);
		$jmldata=mysqli_num_rows($hasil2); 
		$jmlhalaman=ceil($jmldata/$batas);
		?>
		<div class="inline-block fl_left small-desc middle">
			<?php
			$crndata=mysqli_num_rows($hasil2_limited);
			echo "Displaying ".($posisi+1)." to ".($posisi+$crndata)." of ".$jmldata." product in ".$pgn_title;
			?>
		</div>
		<div class="inline-block fl_right">
			<?php
			//link ke halaman sebelumnya (previous)
			if($halaman > 1){
				$previous=$halaman-1; 
				$link_first = merge_querystring($current_url,'?'.$pgn_qs.'=1') /*/$file."'.$pgn_qs.'=1"//*/;
				$link_prev = merge_querystring($current_url,'?'.$pgn_qs.'='.$previous) /*/$file."'.$pgn_qs.'=".$previous//*/;
				?>
				<div class="nav inline-block"><a href="<?=$link_first;?>" >&lt;&lt; First</a></div>
				<div class="nav inline-block"><a href="<?=$link_prev;?>" >&lt; Previous</a></div>
				<?php
			}else{
				/*/ ?>
				<div class="nav inline-block">&lt;&lt; First</div>
				<div class="nav inline-block">&lt; Previous</div>
				<?php //*/
			}
			$angka=($halaman > 3 ? " ... " : " ");
			for($i=$halaman-2;$i<$halaman;$i++){
				if ($i < 1) continue;
				$link_angka = merge_querystring($current_url,'?'.$pgn_qs.'='.$i);//$file.''.$pgn_qs.'='.$i;
			  	$angka .= '<div class="nav inline-block"><a href="'.$link_angka.'">'.$i.'</a></div>';
			}
			$angka .= '<div class="nav active inline-block">'.$halaman.'</div>';
			for($i=$halaman+1;$i<($halaman+3);$i++){
			  	if ($i > $jmlhalaman) break;
				$link_angka = merge_querystring($current_url,'?'.$pgn_qs.'='.$i);//$file.''.$pgn_qs.'='.$i;
			  	$angka .= '<div class="nav inline-block"><a href="'.$link_angka.'">'.$i.'</a></div>';
			}
			$link_angka = merge_querystring($current_url,'?'.$pgn_qs.'='.$jmlhalaman);//$file.''.$pgn_qs.'='.$i;
			$angka .= ($halaman+2<$jmlhalaman ? ' ... 
			          <div class="nav inline-block"><a href="'.$link_angka.'">'.$jmlhalaman.'</a></div>' : '');
			echo $angka;
			//link kehalaman berikutnya (Next)
			if($halaman < $jmlhalaman){
				$next=$halaman+1;
				$link_last = merge_querystring($current_url,'?'.$pgn_qs.'='.$jmlhalaman) /*/$file."'.$pgn_qs.'=".$jmlhalaman//*/;
				$link_next = merge_querystring($current_url,'?'.$pgn_qs.'='.$next) /*/$file."'.$pgn_qs.'=".$next//*/;
				?>
				<div class="nav inline-block"><a href="<?=$link_next;?>">Next &gt;</a></div>
				<div class="nav inline-block"><a href="<?=$link_last;?>">Last &gt;&gt;</a></div>
				<?php
			}else{
				/*/ ?>
				<div class="nav inline-block">Next &gt;</div>
				<div class="nav inline-block">Last &gt;&gt;</div>
				<?php //*/
			}
			?>
		</div>
	</div>
	<ul><!-- lanjutan div sebelumnya utk menyisipkan pagination -->
