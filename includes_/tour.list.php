<?php
//---//SEMENTARA//---//
if($uri2=="search"){
	$jenis_koneksi = "open";
	include("conf/connection.liburyuk.php");
	include("LY/list_groupseries.php");
	include("LY/list_package.php");
	include("LY/list_promopackage.php");
	$jenis_koneksi = "close";
	include("conf/connection.liburyuk.php");
	?>
	<script>
		$(document).ready(function(){
			$("#list-product").parent().removeClass("white-box");
		});
	</script>
	<?php
}else{
//---//SEMENTARA//---//



include("tour.list.query.php");
?>

<ul class="content-list" id="list-product">
	<?php
	while($tour = mysqli_fetch_array($q_tour)){
		$touroid = $tour['touroid']; $name = $tour['name']; $pict = $tour['tourpict']; $headline = !empty($tour['headline']) ? $tour['headline'] : "" ;
		$tourtype = $tour['tourtypeoid'];
		$tourpricing = $tour['tourpricingoid'];
		$tour_cityoid = $tour['cityoid']; $city = $tour['cityname']; 
		$tour_stateoid = $tour['stateoid']; $state = $tour['statename'];
		$tour_countryoid = $tour['countryoid']; $country = $tour['countryname'];
		$tour_continentoid = $tour['continentoid']; $continent = $tour['continentname'];
		
		// Hotel Link
		include_once("function.randomstring.php");
		$ec_continent = str_replace(" ", "-", strtolower($continent) );
		$ec_country = str_replace(" ", "-", strtolower($country) );
		$ec_state = str_replace(" ", "-", strtolower($state) );
		$ec_city = str_replace(" ", "-", strtolower($city) );
		$ec_tourname2 = str_replace(" ", "-", $name );
		$ec_tourname = str_replace("/", "-", $ec_tourname2 );
		$ec_touroid = randomString(4).$touroid.randomString(2);
		$parameters = ( isset($uri4) or !empty($uri4) or $uri4 != "" ) ? "/".$uri4 : "";
		
		
		if($tour_cityoid != 0){
			$href_tour = $base_url."/tour/".$ec_continent."/".$ec_country."/".$ec_state."/".$ec_city."/".$ec_tourname."/".$ec_touroid.$parameters;
		}else if($tour_stateoid != 0){
			$href_tour = $base_url."/tour/".$ec_continent."/".$ec_country."/".$ec_state."/".$ec_tourname."/".$ec_touroid.$parameters;
		}else if($tour_countryoid != 0){
			$href_tour = $base_url."/tour/".$ec_continent."/".$ec_country."/".$ec_tourname."/".$ec_touroid.$parameters;
		}else if($tour_continentoid != 0){
			$href_tour = $base_url."/tour/".$ec_continent."/".$ec_tourname."/".$ec_touroid.$parameters; 
		}
		//$href_tour = $base_url."/tour/".$ec_continent."/".$ec_country."/".$ec_state."/".$ec_tourname."/".$ec_touroid.$parameters;
		
		include("tour.list.minrate.php");
		
		if($page == "home"){
			?>
			<li class="inline-block top white-box border-box">
				<div class="thumb pict">
					<img src="<?=$pict;?>">
				</div>
				<div class="title center border-box"><?=$name;?></div>
				<div class="small-desc"><?=$headline;?></div>
				<div class="border-box center">
					<div class="price inline-block">from <?=$showprice;?> /pax</div>
					<a href="<?=$href_tour;?>" class="button inline-block">BOOK</a>
				</div>
			</li>
			<?php 
		}else if($page == "search-tour"){
			?>
			<li class="top">
				<div class="thumb pict inline-block top">
					<img src="<?=$pict;?>">
				</div>
				<div class="col-2 inline-block top">
					<div class="title border-box">
						<img src="<?=$base_url;?>/images/blank.gif" class="flag flag-id" alt="Czech Republic" />
						<?=$name;?>
					</div>
					<div class="small-desc"><?=$headline;?></div>
				</div>
				<div class="fl_right inline-block right">
					<div class="price">
						from
						<h2 class="rate"> 
							<span class="grey"><?=$currencycode;?></span>
							<span class="blue"><?=miniThousand($displayprice_fnc);?></span>
						</h2>
						/pax
					</div>
					<div class="bottom-right">
						<a href="<?=$href_tour;?>" class="button book inline-block">BOOK</a>
					</div>
				</div>
			</li>
			<?php
		}
	}
	?>
</ul>



<?php
} //---//SEMENTARA//---//
?>