<?php

if($bookingstatus==4){
    $hoteloid=$_SESSION['booking_hoteloid'];
    // $q_stmt="SELECT `hotelcode` FROM `hotel` WHERE hoteloid = '$hoteloid'";
	// $stmt=mysqli_query($conn, $q_stmt) or die("ERROR : ".$q_stmt);
    // $r_ho= mysqli_fetch_array($stmt);
    $stmt = $db->prepare("SELECT `hotelcode` FROM `hotel` WHERE hoteloid = :id");
    $stmt->execute(array(':id' => $hoteloid));
    $r_ho = $stmt->fetch(PDO::FETCH_ASSOC);
    $hotelcode = $r_ho['hotelcode'];
    // echo "<script>console.log(".json_encode($r_ho).")</script>";


    $stmt = $db->prepare("SELECT `roomoid`, `bookingroomoid` FROM `roomoffer` INNER JOIN `bookingroom` USING(`roomofferoid`) WHERE `bookingoid` = :a");
    $stmt->execute(array(':a' => $bookingoid));
    $r_ro = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
    // $q_stmt="SELECT `roomoid`, `bookingroomoid` FROM `roomoffer` INNER JOIN `bookingroom` USING(`roomofferoid`) WHERE `bookingoid` = '$bookingoid'";
	// $stmt=mysqli_query($conn, $q_stmt) or die("ERROR : ".$q_stmt);
    // echo "<script>console.log(".json_encode($r_ro).")</script>";

    foreach($r_ro as $ar_ro){
    // while($ar_ro = mysqli_fetch_array($stmt)){
        // echo "<script>console.log(".json_encode($ar_ro).")</script>";
        $vroomoid = $ar_ro['roomoid'];
        $bookingroomoid = $ar_ro['bookingroomoid'];

        $stmt = $db->prepare("SELECT * FROM `bookingroomdtl` WHERE bookingroomoid = :id");
        $stmt->execute(array(':id' => $bookingroomoid));
        $r_brd = $stmt->fetchAll(PDO::FETCH_ASSOC);
        
        // $q_stmt="SELECT * FROM `bookingroomdtl` WHERE bookingroomoid =  '$bookingroomoid'";
        // // echo "<script>console.log(".json_encode($q_stmt).")</script>";
        // $stmt=mysqli_query($conn, $q_stmt) or die("ERROR : ".$q_stmt);
        // // $r_brd= mysqli_fetch_array($stmt);
        
        foreach($r_brd as $a_brd){
        // while($a_brd = mysqli_fetch_array($stmt)){
            // echo "<script>console.log(".json_encode($a_brd).")</script>";
            $vdate = $a_brd['date'];

            $path_xml_hotel = "myhotel/data-xml/".$hotelcode.".xml";
            // echo "<script>console.log(".json_encode($path_xml_hotel).")</script>";

            $xml = new DomDocument();
            $xml->preserveWhitespace = false;
            $xml->load($path_xml_hotel);
            $xpath = new DomXpath($xml);
            $root = $xml->documentElement;
            $xml_file =  simplexml_load_file($path_xml_hotel);

            $query_tag_allotment_channel = '//hotel[@id="'.$hoteloid.'"]//masterroom[@id="'.$vroomoid.'"]/rate[@date="'.$vdate.'"]/allotment/channel[@type="1"]';
            $query_tag_allotment_channel_value = $query_tag_allotment_channel.'/text()';
            // echo "<script>console.log(".json_encode($query_tag_allotment_channel_value).")</script>";
            $xml_run = $xml_file->xpath($query_tag_allotment_channel_value);
            // echo "<script>console.log(".json_encode($xml_run).")</script>";

            foreach($xml_run as $tagallotment){
                // echo "<script>console.log(".json_encode($tagallotment,'$tagallotment').")</script>";
                $allotment = (int)$tagallotment[0];
            }

            $new_allotment = $allotment - 1;
            // echo "<script>console.log(".json_encode($new_allotment,'$new_allotment').")</script>";

            $check_tag_allotment_channel = $xpath->query($query_tag_allotment_channel);
            $check_tag_allotment_channel->item(0)->nodeValue = $new_allotment;

            $xml->formatOutput = true;
            $xml->save($path_xml_hotel) or die("Error");
        }
    }

    // die();
}
?>