<link rel="stylesheet" type="text/css" href="<?php echo $base_url; ?>/scripts/bootstrap-switch/bootstrap-switch.css">
<div id="pgdtl" class="container">
<div class="row">
<?php
	$hoteloid = substr($uri8, 4, strlen($uri8)-6);
	
	//--------------------------------
	require_once 'class/DetailHotel.class.php';
    require_once('conf/connection_withpdo.php');
	$Detail = new DetailHotel($conn,$hoteloid) ;
	$Detail->setPDO($db);

	$row_h = $Detail->getDetail();//echo json_encode($row_h);
	// $hotel_name = $row_h['hotelname'];
	// $hotel_address = $row_h['address'];
	// $hotel_stars = $row_h['stars'];
	// $hotel_totalrom = $row_h['totalroom'];
	// $hotel_website = $row_h['website'];
	// $hotel_area = $row_h['cityname'];
	// $hotel_description = $row_h['description'];
	// $hotel_latitude = $row_h['latitude'];
	// $hotel_longitude = $row_h['longitude'];

	// $ROOMRATES = $Detail->getRoomRates(0,$channeloid);
	// $ROOMRATES_PROMO = $Detail->getRoomRates(1,$channeloid);
	// $countTotalRoom = sizeof($ROOMRATES);
	
	//--------------------------------
	$arrayLoadRates = $Detail->ibe_loadrates();
	//Func::logJS($arrayLoadRates,'ibeLoadRates');

	$countTotalRoom = sizeof($arrayLoadRates['roomoffertotal_min']);

	//--------------------------------
	
	// foreach ($ROOMRATES_PROMO as $promo) { array_push($ROOMRATES,$promo); }
	
?>
<style>
</style>
<?php //include("js_magnific-popup.php"); ?>
<!-- class="col-md-4 col-xs-12 inline-block" -->
<aside id="sidebar" class="col-md-3 col-sm-12">
<!-- flat-box -->
	<div class="wrapper-floating">
		<div class="card-rounded white-box" id="search">
			<div class="card-section-title text-center">Search Another Hotels</div>
			<div class="card-wrapper">
				<?php //include("form.search.hotel.php"); ?>
				<?php include("form.search.hotel.sidebar.php"); ?>
			</div>
		</div>
		<!-- flat-box -->
		<div class="card-rounded white-box hidden-sm hidden-xs" id="hotels-nearby">
			<div class="card-section-title text-center">
				<!-- <span class="grey">Hotels Nearby</span> -->
				Hotels Nearby
			</div>
			<div class="card-wrapper">
				<div class="swiper-container mini-swiper">
					<div class="swiper-wrapper">
						<?php
						$bup_hoteloid = $hoteloid;
						//hotel nearby
						include("list.hotel.php");
						$hoteloid = $bup_hoteloid;
						?>
					</div>
					<!-- Add Arrows -->
					<!-- <div class="swiper-button-next swiper-button-white"></div>
					<div class="swiper-button-prev swiper-button-white"></div> -->
					<!-- <div id="card-v2-button-prev" class="card-button-prev hidden-sm hidden-xs"><i class="fa fa-angle-left"></i></div>
					<div id="card-v2-button-next" class="card-button-next hidden-sm hidden-xs"><i class="fa fa-angle-right"></i></div> -->
					<div class="swiper-pagination"></div>
				</div>


						<script>
							$(document).ready(function(){
								$("#hotels-nearby #list-hotel").removeClass("white-box");

								var selectJumlahRoom = $("#room-availability select[room='number']");
								/*
								selectJumlahRoom.parent("div").css({"position":"relative"});
								selectJumlahRoom.after("<div style='position:absolute; width: 100%; height:100%; background:#f00;'>asd</div>")
								*/
								selectJumlahRoom.after(' <i class="color-red fa fa-spinner fa-pulse" style="visibility:hidden;"></i>');
								selectJumlahRoom.change(function(){
									var elem = $(this);
									//elem.parent("div").css({ "background": "#f00" });
									elem.next(".fa-spinner").css({"visibility":"visible"});
									setTimeout(function () {
										elem.next(".fa-spinner").css({"visibility":"hidden"});
									}, 1200);
								});
							});
						</script>

			</div>
		</div>
		
		<!-- flat-box -->
		<div class="card-rounded white-box hidden-sm hidden-xs" id="mini-map">
		<!-- grey -->
			<div class="card-section-title text-center">Maps</div>
			<div class="card-wrapper">
				<br>
			<!-- <a href="#"><div id="static_map"></div></a> -->
			<!-- width="250"  -->
				<iframe
					height="170"
					frameborder="0"
					scrolling="no"
					marginheight="0"
					marginwidth="0"
					style="width:100%;"
					src="https://maps.google.com/maps?q=+<?php echo $Detail->_hotellatitude;?>+,+<?php echo $Detail->_hotellongitude;?>+&hl=es;z=14&amp;output=embed"
					>
				</iframe>
				
			</div>
		</div>
	</div>
</aside>

<div id="right-content" class="col-md-9 col-sm-12">
	<div id="breadcrumb" class="card-rounded white-box border-box hidden-sm hidden-xs">
		<?php
		$breadcrumb = array($uri2,$uri3,$uri4,$uri5,$uri6,$uri7);
		foreach ($breadcrumb as $key => $value) {
			$value = str_replace("-", " ", $value);
			?>
			<a class="breadcrumb text" id="breadcrumb-<?=$key;?>" href="#"><?=$value;?></a>
			<?php
			if($key < count($breadcrumb)-1){ echo '<i class="fa fa-angle-right"></i>'; }
		}
		?>
	</div>

	<div class="card-rounded white-box border-box">
		<div id="hotel-info" class="clear">
			<div class="fl_left">
				<h3 class="default inline-block bottom-space" id="hoteltitle">
					<?=$Detail->_hotelname;?>
				</h3>
				&nbsp;
				<?php starRating($Detail->_hotelstar, 2); ?>

				<div class="bottom-space">
					<i class="desc"><?=$Detail->_hoteladdress;?>
					<!-- (<a href="#">show on map</a>) -->
					</i>
				</div>

				<div class="bottom-space">
					<i class="fa fa-map-marker"></i>
					<b>Hotel Area :</b> <?=$Detail->_hotelcity;?>
					&nbsp; &nbsp;
					<i class="fa fa-bed"></i>
					<b>Number of Rooms :</b> <?=$Detail->_hotelrooms;?>
				</div>
			</div>

			<div class="fl_right">
				<a href="#room-availability" class="btn btn-red">BOOK NOW</a>
			</div>
		</div>

        <!-- <div id="shortcut">
        	<ul>
                <li><a href='#hotel-profile'>Profile</a></li>
            	<li><a href="#room-availability">Rates &amp; Availability</a></li>
                <li><a href='#hotel-facilities'>Facilities</a></li>
                <li><a href="#">All Reviews</a></li>
            </ul>
		</div> -->


		<div id="hotel-gallery" class="border-box inline-block" style="">
			<?php include("page.detail.hotel.gallery.php");?>
			
		</div>

	</div>

    <div class="card-rounded white-box border-box" id="hotel-profile">
	    <h3 class="border-bottom section-title-v1 row"><span class="grey">Profile</span></h3>
		<div>
    		<?=$Detail->_hoteldesc;?>
        </div>
    </div>

	<div class="card-rounded white-box border-box" id="room-availability">
		<h3 class="border-bottom section-title-v1 row"><span class="grey">Rates &amp; Availability</span></h3>
		<div class="clear wrapper-dates">
			<div class="col-md-12">
				<?php echo date("D d F Y", strtotime($checkin)); ?> - <?php echo date("D d F Y", strtotime($checkout)); ?> for <?php echo $night; ?> nights
				&nbsp;
				<a class="toggle-display btn btn-warning" target="#availability-hotel"><i class="fa fa-chevron-down"></i> Change Dates</a>
				
				<?php if(empty($_REQUEST['groupby']) or $_REQUEST['groupby'] == "room"){ $checked = "checked"; $labeltext = "OFFERS LIST";  }else{ $checked = ""; $labeltext = "ROOM LIST"; } ?>
                    <!-- <input type="checkbox" name="group-by-switch" <?=$checked?> data-off-text="OFFERS LIST" data-on-text="ROOM LIST" data-label-text="<?=$labeltext?>" data-size="normal" /> -->
                    <input type="hidden" name="groupbyswitch" value="<?=$_REQUEST['groupby']?>" />
			</div>
			<div class="col-md-12" id="availability-hotel" style="display: none;"><?php include("form.availability.hotel.php"); ?></div>
		</div>
		<div>
			<form name="book_room" method="post" action="<?=$base_url;?>/book/guest-detail">
				<input type="hidden" name="hoteloid" value="<?php echo $uri8; ?>">
				<input type="hidden" name="based_adult" value="<?php echo $adult; ?>">
				<input type="hidden" name="based_child" value="<?php echo $child; ?>">
				<input type="hidden" name="based_night" value="<?php echo $night; ?>">
				<input type="hidden" name="based_checkin" value="<?php echo $checkin; ?>">
				<input type="hidden" name="inquiry" value="<?php echo (($_profile['inquiry_status']=='y'&& $countTotalRoom==0)?'y':'n');?>">
				<div class="row">
					<div class="inline-block top col-lg-9 col-md-12 col-sm-12 col-xs-12">
						<ul id="room-list" class="content-list" >
							<li class="title border-bottom hidden-sm hidden-xs">
								<div class="col-sm-5 middle">Room Type</div>
								<div class="col-sm-3 middle">Rate per Night</div>
								<div class="col-sm-4 middle">Rooms</div>
								<div class="clear"></div>
							</li>
							<?php
							include('book-hotel.showpackage.php');
							?>
						</ul>
						<div class="show-more-room" show="0"><b>&or;</b> &nbsp; Show more room type &nbsp; <b>&or;</b></div>
						<script>
						$(function(){
							var tbl = $("#room-availability #room-list .room-item");
							var count = 0;
							tbl.each(function(index, element) {
								var tblrow = $(this);
								if(count > 5){ tblrow.hide(); }
								count++;
							});
							if(count > 5){ $(".show-more-room").show(); }

							$(".show-more-room[show='0']").on('click',function(){
								var count = 0;
								tbl.each(function(index, element) {
									var tblrow = $(this);
									if(count > 5){ tblrow.show(); }
									count++;
								});
								$(".show-more-room").attr('show', "1");
								$(".show-more-room").html("<b>&and;</b> &nbsp; Show less room type &nbsp; <b>&and;</b>");
							});
							$(".show-more-room[show='1']").on('click',function(){
								var count = 0;
								tbl.each(function(index, element) {
									var tblrow = $(this);
									if(count > 5){ tblrow.hide(); }
									count++;
								});
								$(".show-more-room").attr('show', "0");
								$(".show-more-room").html("<b>&or;</b> &nbsp; Show more room type &nbsp; <b>&or;</b>");
							});
						});
						</script>
					</div>
					<div class="inline-block top col-lg-3 col-md-12 col-sm-12 col-xs-12" id="book-summary">
						<h3>Booking Summary</h3>
						<div id="book-summary-dtl" class="left"></div>
						<!-- <h3>Total price</h3> -->
						<h4>Total Price :</h4>
						<!-- <h2><span class="grey"></span> <span class="blue"></span></h2> -->
						<h2><span class="grey"></span> <span class="blue">0</span></h2>
						<div id="textbox">

						</div>

						<div class="right">
							<!-- <a class="button book" href="<?=$base_url;?>/book/guest-detail">BOOK</a> -->
							<?php
								if($_profile['inquiry_status']=='y'&& $countTotalRoom==0){
									?>  <input type="submit" onclick="showLoadingPage()" class="general-submit btn-red" value="INQUIRY"> <?php
								}else if($_profile['inquiry_status']=='n'&& $countTotalRoom==0){
									?>  <input type="submit" class="general-submit " style="background:grey;" disabled value="NOT AVAILABLE"> <?php
								}else{
									?>  <input type="submit" onclick="showLoadingPage()" class="general-submit" value="BOOK NOW"> <?php
								}
							?>
						</div>
					</div>
				</div>
			</form>

		</div>
	</div>

    <div class="card-rounded white-box border-box" id="hotel-facilities">
	    <h3 class="border-bottom section-title-v1 row"><span class="grey">Facilities</span></h3>
		<div>
    		<?php
			$q_amenities_cat =$Detail->getFacilityCategories();
			// Func::logJS($q_amenities_cat,'$q_amenities_cat');
			if(is_array($q_amenities_cat))
			foreach($q_amenities_cat as $r_amenities_cat){

				$amenities_categoryoid = $r_amenities_cat['facilitiestypeoid'];
				$amenities_category = $r_amenities_cat['name'];
				?>
				<div class="amenities-category">
					<b><?=$amenities_category;?></b>
				</div>

				<ul class="content-list inline-block top">
				<?php
				
				$q_amenities = $Detail->getFacilities($amenities_categoryoid);
				$n_amenities = sizeof($q_amenities);

				$max_row = ceil($n_amenities / 3);
				$row = 0;
				foreach($q_amenities as $r_amenities){
					
					$row++;
					$amenities_oid = $r_amenities['amenitiesoid'];
					$amenities_name = $r_amenities['name'];
					$have_amenities = ( $r_amenities['jmlhotel'] == 1 ) ? "checked" : "unchecked hidden-xs hidden-sm" ;
					// $have_amenities_icon = ( $r_amenities['jmlhotel'] == 1 ) ? '<i class="fa fa-check-circle"></i>' : '' ;
					if(( $r_amenities['jmlhotel'] == 1 )){
					?>
					
					<li class="<?=$have_amenities;?> col-md-3 col-sm-6 col-xs-12"><?=$amenities_name;?></li>
					<?php 
					}
					if($row % $max_row == 0){
						?>
							</ul><ul class="content-list inline-block top">
						<?php
					}
				}
				?>
					<div class="clear"></div>
				</ul>

				<?php
			}
			?>
        </div>
    </div>

</div> <!-- end of #single-content -->
</div>
</div>


<?php
include("js_gmap.php");
include("js_smooth-scrolling.php");
?>

<script  type="text/javascript" language="javascript" src="<?=$base_url;?>/scripts/bootstrap-switch/bootstrap-switch.js"></script>
<script>
	$(function(){
		// jquery code here ...
		
	// $('input[name="group-by-switch"]').bootstrapSwitch();
	// $('input[name="group-by-switch"]').on('switchChange.bootstrapSwitch', function(event, state) {
	// 	setTimeout(function(){ reloadGroup(state); }, 200);
	// });
	
	function reloadGroup(state){
		$elem=$('input[name="groupbyswitch"]');
		console.log(state);
		if(state == false){
			$elem.val('promotion');
		}else{
			$elem.val('room');
		}
		redirectNewUrl('groupby', $elem.val())
		// $('form.formCheckAvailability').submit();
	}
	
	})

	
	
	function redirectNewUrl(key, val){ 
		
		var arrparam = window.location.search.split('?')[1].split('&');
		var newparam = '?';
		var stsparam = false;
		for(var i=0;i<arrparam.length;i++){
			var res = arrparam[i].split('=');
			if(i!=0) newparam += '&';
			if(res[0]==key) {
				newparam += key+'='+val;
				stsparam = true;
			}else{
				newparam += arrparam[i];
			}
		}
		if(!stsparam) newparam += '&'+key+'='+val;
		var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + newparam; 


		$("#filters-overlay").css({"visibility":"visible"});
		setTimeout(function () {
			document.location.href = newurl;
		}, 1500);
		
	}
</script>

