<?php
	$bookingnumber = $uri4;

	$query = "select b.bookingnumber, b.bookingoid, b.bookingtime, DATE_FORMAT(bookingtime, '%W, %d %M %Y at %H:%i:%s') as bookdate, b.note, 
	sum(roomtotal) as roomtotal, sum(extrabedtotal) as extrabedtotal, b.grandtotal, b.hotelcollect, b.gbhcollect,
	bs.note as status, 
	cur.currencycode, 
	CONCAT(firstname, ' ', lastname) as guestname, c.address, c.city, c.state, c.zipcode, c.mobilephone, c.email,
	ctr.countryname,
	h.hotelname
	from booking b 
	inner join bookingstatus bs using (bookingstatusoid)
	inner join currency cur using (currencyoid)  
	inner join customer c using (custoid) 
	inner join country ctr using (countryoid) 
	inner join bookingroom br using (bookingoid) 
	inner join roomoffer ro using (roomofferoid) 
	inner join room r using (roomoid) 
	inner join hotel h using (hoteloid) 
	left join bookingpayment using (bookingoid) 
	where b.bookingnumber = '".$bookingnumber."'
	group by b.bookingoid";
	try {
		$stmt = mysqli_query($conn, $query);		
		$row_count = mysqli_num_rows($stmt);
		if($row_count > 0) {
			while($row = mysqli_fetch_assoc($stmt)){
				$bookingoid = $row['bookingoid'];
				$grandtotal = $row['currencycode']." ".number_format($row['grandtotal']);
				$hotelcollect = $row['currencycode']." ".number_format($row['hotelcollect']);
				$gbhcollect = $row['currencycode']." ".number_format($row['gbhcollect']);
				$roomtotal = $row['currencycode']." ".number_format($row['roomtotal']);
				$extrabedtotal = $row['currencycode']." ".number_format($row['extrabedtotal']);
				if($row['note']!=""){ $note = $row['note']; } else{ $note = "n/a"; }
?>
<section class="content white-box border-box">
    <div class="row">
        <div class="box box-form">
            <div class="box-body">
                <div id="detail-booking">
                    <div class="text-center">
                        <h1><?php echo $row['guestname']; ?></h1>
                        <span><?php echo $row['address']; ?>, <?php echo $row['city']; ?>, <?php echo $row['state']; ?>, <?php echo $row['zipcode']; ?>, <?php echo $row['countryname']; ?></span><br>
                        <span class="text-turqoise">&#9742; <?php echo $row['mobilephone']; ?> &nbsp;&nbsp; &#9993; <?php echo $row['email']; ?></span><br>
                        <span><?php echo $row['bookdate']; ?> &nbsp;&nbsp; Booking Number : <strong class="text-purple"><?php echo $row['bookingnumber']; ?></strong></span>
                        <br>
                        <br>
                        <h2><?php echo $row['hotelname']; ?></h2>
                    </div>
                    <div>
						<?php
                        $ar_roomname = array();
                        $ar_checkin = array();
                        $ar_checkout = array();
                        $ar_numberroom = array();
                        $ar_maxoccupancy = array();
                        $ar_total = array();
                        $ar_breakfast = array();
                        $ar_extrabed = array();
                        
                        $q_detail = "select br.*, r.hoteloid, ro.name as roomname, r.adult as maxadult, ch.name as channelname, count(bookingroomoid) as jmlroom, sum(total) as total, sum(br.extrabed) as extrabed, c.currencycode, checkin, checkout 
                        from bookingroom br
                        inner join currency c using (currencyoid)
                        inner join booking b using (bookingoid) 
                        left join promotion p using (promotionoid)
                        left join promotiontype pt using (promotiontypeoid)
                        inner join roomoffer ro using (roomofferoid)
                        inner join offertype ot using (offertypeoid)
                        inner join room r using (roomoid)
                        inner join channel ch using (channeloid)
                        where b.bookingoid = '".$bookingoid."' 
                        group by r.roomoid, ro.roomofferoid, channeloid, promotionoid, checkin, checkout";
                        try {
                            $stmt = mysqli_query($conn, $q_detail);		
                            $row_count = mysqli_num_rows($stmt);
                            if($row_count > 0) {
                                while($rp = mysqli_fetch_assoc($stmt)){
                                    array_push($ar_roomname, $rp['roomname']);
                                    array_push($ar_checkin, date("d/M/Y", strtotime($rp['checkin'])));
                                    array_push($ar_checkout, date("d/M/Y", strtotime($rp['checkout'])));
                                    array_push($ar_numberroom, $rp['jmlroom']);
                                    array_push($ar_maxoccupancy, $rp['maxadult']);
                                    array_push($ar_total, $rp['currencycode']." - ".number_format($rp['total']));
                                    array_push($ar_breakfast, $rp['breakfast']);
                                    array_push($ar_extrabed, $rp['extrabed']);
                                }
                            }
                        }catch(PDOException $ex) {
                            echo "Invalid Query";
                            print($ex);
                            die();
                        }
                        ?>
                    	<table class="table table-striped">
                        	<thead>
                                <tr>
                                    <th>Room Type</th>
                                    <th>Check In</th>
                                    <th>Check Out</th>
                                    <th>No. of Rooms</th>
                                    <th>Max. Occupancy</th>
                                    <th>Breakfast Included</th>
                                    <th>No. of Extra Bed</th>
                                </tr>
                            </thead>
                            <tbody>
							<?php
                            foreach($ar_roomname as $key => $value){
                                echo"
                                <tr>
                                    <td>".$value."</td>
                                    <td>".$ar_checkin[$key]."</td>
                                    <td>".$ar_checkout[$key]."</td>
                                    <td class='text-center'>".$ar_numberroom[$key]."</td>
                                    <td class='text-center'>".$ar_maxoccupancy[$key]."</td>
                                    <td class='text-center'>".$ar_breakfast[$key]."</td>
                                    <td class='text-center'>".$ar_extrabed[$key]."</td>
                                </tr>
                                ";
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <div>
                        <table class="table table-border table-fill">
                            <tr>
                                <td><strong>Total Cost Confirmed with Guest</strong></td>
                                <td>
                                    <table class="ch-table">
                                        <tr>
                                            <td>Room Rate</td>
                                            <td><?php echo $roomtotal; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Extra Bed Rate</td>
                                            <td><?php echo $extrabedtotal; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Total Confirmed to Guest</td>
                                            <td><?php echo $grandtotal; ?></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
					</div>
                    <div>
                        <table class="table table-border table-fill">
                            <tr><td><i class="fa fa-sticky-note"></i> <strong>Guest Note</strong></td></tr>
                            <tr><td><?php echo $note; ?></td></tr>
                        </table>
                        <table class="table table-border table-fill">
                            <tr><td><i class="fa fa-warning"></i> <strong>Cancellation Policies</strong></td></tr>
                            <?php
                            try {
                                $stmt = mysqli_query($conn, "select cp.description 
                                from cancellationpolicy cp
                                left join promotion p using (cancellationpolicyoid) 
                                inner join bookingroom br using (promotionoid) 
                                inner join booking b using (bookingoid) 
                                where b.bookingoid = '".$bookingoid."' 
                                group by cp.cancellationpolicyoid");		
                                $row_count = mysqli_num_rows($stmt);
                                if($row_count > 0) {
                                    while( $rp = mysqli_fetch_assoc($stmt)){
                                        echo "<tr><td>".$rp['description']."</td></tr>";
                                    }
                                }
                            }catch(PDOException $ex) {
                                echo "Invalid Query";
                                print($ex);
                                die();
                            }
                            ?>
                        </table>
                        <div class="clear"></div>
					</div>                      
                <?php			
                            }
                        }
                    }catch(PDOException $ex) {
                        echo "Invalid Query";
                        print($ex);
                        die();
                    }
                ?>
                </div>
                
			</div>
       </div>
    </div>
</section>