<div class="white-box border-box">
    <h2><span class="grey"><i class="fa fa-user"></i></span> <span class="blue">My Profile</span></h2>
    <form method="post" action="<?php echo $base_url; ?>/member/save-my-profile">
    <div class="form">
        <div class="form-group">
            <label>Title</label>
            <span>
            <select name="title">
                <?php
                    $s_title = "select * from title "; 
                    $q_title = mysqli_query($conn, $s_title) or die(mysqli_error());
                    while($title = mysqli_fetch_array($q_title)){
                        if($title['title']==$member['title']){ $selected=" selected='selected' "; }else{ $selected=""; }
                ?>
                <option value="<?=$title['titleoid'];?>" <?php echo $selected; ?>><?=$title['name'];?></option>
                <?php
                    }
                ?>
            </select>
            </span>
        </div>
        <div class="form-group">
            <label>First Name</label>
            <span>
            <input type="text" class="md-6" name="firstname" placeholder="Your first name" value="<?php echo $member['firstname']; ?>" required="required"/>
            </span>
        </div>
        <div class="form-group">
            <label>Last Name</label>
            <span>
            <input type="text" class="md-6" name="lastname" placeholder="Your last name" value="<?php echo $member['lastname']; ?>" required="required"/>
            </span>
        </div>
        <div class="form-group">
            <label><i class="fa fa-envelope"></i> E-mail</label>
            <span>
            <input type="email" class="md-3" name="email" placeholder="Your e-mail address" value="<?php echo $member['email']; ?>" required="required"/>
            </span>
        </div>
        <div class="form-group">
            <label>Mobile Number</label>
            <span>
            <input type="text" class="md-3" name="mobile" placeholder="Your mobile phone number" value="<?php echo $member['mobilephone']; ?>" required="required"/>
            </span>
        </div>
        <div class="form-group">
            <label>Country</label>
            <span>
            <select name="country">
            <?php 
                $s_country="select countryoid, countryname from country order by countryname"; $q_country=mysqli_query($conn, $s_country) or die ("SQL Error : ".$s_country);
                while($country=mysqli_fetch_array($q_country)){
                    if(empty($_SESSION['nationallity'])){
                        if($country['countryname']=="Indonesia"){ $selected=" selected='selected' "; }else{ $selected=""; }
                    }else{
                        if($country['countryoid']==$member['countryoid']){ $selected=" selected='selected' "; }else{ $selected=""; }
                    }
                ?>
                <option value="<?php echo $country['countryoid']; ?>" <?php echo $selected; ?>><?php echo $country['countryname']; ?></option>
                <?php
                }
                ?>
            </select>
            </span>
        </div>
    </div>
    <div class="form">
        <h3 class="sticky-title turqoise"><span><i class="fa fa-lock"></i> Change Your Password</span></h3>
        <div class="form-group">
            <label>Current Password</label>
            <span>
            <input type="password" class="md-3" name="oldpassword" placeholder="Your current password" />
            </span>
        </div>
        <div class="form-group">
            <label>New Password</label>
            <span>
            <input type="password" class="md-3" name="newpassword" placeholder="Your new password" />
            </span>
        </div>
        <div class="form-group text-right">
			<span>
                <button type="submit" class="purple" name="submit-profile">Update My Profile</button>
            </span>
        </div>
    </div>
    </form>
</div>
