<div class="box border-box">
    <h3>Edit My Card</h3>
    <form name="form-edit-card" method="post" action="#">
    <input type="hidden" name="member" value="<?php echo $_SESSION['rove1meid']; ?>" />
    <div class="form">
        <div class="form-group" style="text-align:right;"><h2 id="cc-number"></h2></div>
        <div class="form-group">
        	<label>Expiration Date</label>
            <span>
                <select name="month" id="month">
                    <?php
                        $months = array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December');
                        for($m=1; $m<=12; $m++){
                            if($m==date('m')){ $selected="selected='selected'"; }else{ $selected=""; }
                             echo "<option value='".$m."' ".$selected.">".$months[$m]."</option>"; 
                        }
                    ?>
                </select>
                &nbsp;
                <select name="year">
                    <?php 
                        for($i=date('Y'); $i<=(date('Y')+5); $i++){
                            if($i==date('Y')){ $selected="selected='selected'"; }else{ $selected=""; }
                            echo "<option value='".$i."' ".$selected.">".$i."</option>"; 
                        } 
                     ?>
                </select>
            </span>
        </div>
        <div class="form-group text-right">
			<span>
                <button type="button" class="purple" name="submit-card">Add New Card</button>
            </span>
        </div>
    </div>
    </form>
</div>
