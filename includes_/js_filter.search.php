<script>
$(document).ready(function(){
	
	/*zc*/
	function setParamURL(key, val){
		var arrparam = window.location.search.split('?')[1].split('&');
		var newparam = '?';
		var stsparam = false;
		for(var i=0;i<arrparam.length;i++){
			var res = arrparam[i].split('=');
			if(i!=0) newparam += '&';
			if(res[0]==key) {
				newparam += key+'='+val;
				stsparam = true;
			}else{
				newparam += arrparam[i];
			}
		}
		if(!stsparam) newparam += '&'+key+'='+val;
		var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + newparam; 
		window.history.pushState({ path: newurl }, '', newurl);
		return newparam;
	}

	//fungsi
	function updateSelectedCheckbox(cbSelector, inputSelector) {         
	    var allVals = [];
	    cbSelectorChecked = cbSelector+":checked";
	    $(cbSelectorChecked).each(function() {
			allVals.push($(this).val());
	    });
	    $(inputSelector).val(allVals);
	    $(inputSelector).change();
	}
	
	
	function appendUrl(parameterUrl, triggerElem){ 
		var elemVal = triggerElem.val();
		//$('#div-list-hotel').load('includes/test.php');
		// var newParam = setParamURL(parameterUrl,elemVal);
		
		// $("#filters-overlay").css({"visibility":"visible"});
		// setTimeout(function () {
		// 	$('#content').load('includes/page.search.hotel.php'+newParam, function(e){
		// 		e.preventDefault();
		// 		$("#ui-datepicker-div").remove();
		// 	});
		// }, 2000);
		//------------------------------------------------------------
		var currentUrl = "<?=$base_url."/".$uri2."/".$uri3."/".$uri4;?>";
		var parameterUrlSelected = "&"+parameterUrl+"=";
		if(currentUrl.indexOf(parameterUrlSelected) == -1){
			var redirectUrl = currentUrl + parameterUrlSelected + elemVal;
		}else{
			var indexBeforeStars = currentUrl.indexOf(parameterUrlSelected);
			var indexAfterStars = currentUrl.indexOf("&", indexBeforeStars + 1);
			if(indexAfterStars == -1){
				var redirectUrl = currentUrl.substr(0, indexBeforeStars) + parameterUrlSelected + elemVal;
			}else{
				var redirectUrl = currentUrl.substr(0, indexBeforeStars) + parameterUrlSelected + elemVal + currentUrl.substr(indexAfterStars, currentUrl.length - indexAfterStars);
			}
		}
		
		$("#filters-overlay").css({"visibility":"visible"});
		setTimeout(function () {
			document.location.href = redirectUrl;
		}, 1500);
		
	}
	
	//LOADING MESSAGE
	// if (!!$('.sticky-message').offset()) { // make sure ".sticky-message" element exists
    // 	var stickyTop = $('.sticky-message').offset().top; // returns number
    // 	//var stickyTop = 350; // returns number
    // 	$(window).scroll(function(){ // scroll event
    //   		var windowTop = $(window).scrollTop(); // returns number 
    //   		if (stickyTop < windowTop){
    //     		$('.sticky-message').css({ position: 'fixed', top: '50%', left: '50%', transform: 'translate(50%,-50%)' });
    //   		}else{
    //     		$('.sticky-message').css({ position: 'static', top: 'unset', left: 'unset', transform: 'translateY(50%)' });
    //   		}
    // 	});
  	// }
	
	$(".filter-wrapper").each(function(){
		var elem = $(this);
		var elemCB = elem.children("div").children("input[type='checkbox']");
		var elemFN = elem.children(".filter-name");
		
		//fungsi RESET
		elem.children(".reset").click(function(){
			$("#filters-overlay").css({"visibility":"visible"});
			var redirectUrl = $(this).attr("link");
			setTimeout(function () {
				document.location.href = redirectUrl;
			}, 2000);
		});
		
		//menampilkan reset
		if(elemFN.length > 0){ //if element exist
			elem.ready(function(){
				if(elemFN.val() != ""){
					elem.on("hover", function(){
						elem.children(".reset").toggle();
					});
				}
			});
		}else{
			elem.on("hover", function(){
				var n = 0;
				elemCB.each(function(){
					if (this.checked == true) {
						n++;
				    }
				});
				if(n>0){		
					elem.children(".reset").toggle();
				}//end if(n>0)
			});
		}
	});
	
	
	//SORT BY
	$(".sort-by").click(function(){
		$("#sort-by").val($(this).attr('to')).trigger('change');
	});
	$("#sort-by").change(function(){
		appendUrl("sortby", $(this) );
		//setParamURL("sortby",$(this).val());
	});
	
	//ganti BG checked element
	$(".filter-wrapper").ready(function(){
		var elem = $(".filter-wrapper input[type=checkbox]:checked");
		elem.parent("div").css({"background-color": "#eee"});
	});
	
	
//--- FUNGSI per FILTER --- HOTEL
	//HOTELNAME
	$("#submit-hotelname").click(function(){
		appendUrl("hotelname", $(this).closest('#q-hotelname-wrapper').find('#q-hotelname') );
	});
	$("#q-hotelname").keyup(function(e){
		if(e.keyCode == 13){
			appendUrl("hotelname", $(this) );
		}
	});
	
	
	//PRICE
	$('.cb-price').click(function(){
   		updateSelectedCheckbox(".cb-price","#selected-price");
   	});
	$("#selected-price").change(function(){
		appendUrl("price", $(this) );
	});

	//STARS
	$('.cb-star').click(function(){
   		updateSelectedCheckbox(".cb-star","#selected-star");
   	});
	$("#selected-star").change(function(){
		appendUrl("stars", $(this) );
	});
	
	
	//HOTELTYPE
	$('.cb-hoteltype').click(function(){
   		updateSelectedCheckbox(".cb-hoteltype","#selected-hoteltype");
   	});
	$("#selected-hoteltype").change(function(){
		appendUrl("hoteltype", $(this) );
	});
	
	
	//FACILITIES
	$('.cb-facilities').click(function(){
   		updateSelectedCheckbox(".cb-facilities","#selected-facilities");
   	});
	$("#selected-facilities").change(function(){
		appendUrl("facilities", $(this) );
	});
	
	
	//AREA
	$('.cb-area').click(function(){
   		updateSelectedCheckbox(".cb-area","#selected-area");
   	});
	$("#selected-area").change(function(){
		appendUrl("area", $(this) );
	});
	
	
	//CHAIN
	$('.cb-chain').click(function(){
   		updateSelectedCheckbox(".cb-chain","#selected-chain");
   	});
	$("#selected-chain").change(function(){
		appendUrl("chain", $(this) );
	});
	
	
//--- FUNGSI per FILTER --- TOUR
	//HOTELNAME
	$("#submit-tourname").click(function(){
		appendUrl("tourname", $(this).prev() );
	});
	$("#q-tourname").keyup(function(e){
		if(e.keyCode == 13){
			appendUrl("tourname", $(this) );
		}
	});
	
	
	<?php /*
	$("form#filter-search-hotel").submit(function(){
		var elem = $(this);
		
		var stars = $("#selected-star").val();
		var dataString = 'stars=' + stars;
		$.ajax({
			type: "POST",
			url: "<?=$base_url;?>/includes/ajax.filter.search.hotel.php",
			data: dataString,
			cache: false,
			success: function(html){
			
			}
		}); //end of ajax
		
	});
	*/ ?>

	$('.pagination > li:not(.disabled):not(.active) > a').click(function () {
			var thisElem = $(this);
			var pageValueAttr = thisElem.attr('pagevalue');
			var urlWithPageNumber = setParamURL('page',pageValueAttr);
			// alert(urlWithPageNumber);
			document.location.href = urlWithPageNumber;
		});

});
</script>