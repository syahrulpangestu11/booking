<?php
	error_reporting(E_ALL ^ E_NOTICE);
	include('../conf/connection.php');
	// include('../conf/xmlload.php');

	$tknSession = $_GET['tkn'];
	$promoapply = $_GET['promoapply'];
	$roomoid = $_GET['roomoid'];
	$roomofferoid = $_GET['roomofferoid'];
	$channeloid = $_GET['channeloid'];
	$checkin = date('Y-m-d', strtotime($_GET['checkin']));
	$night = $_GET['night'];
	$type = $_GET['type'];

	if((!empty($promoapply) or $promoapply != 0) and $type != 'bar'){
		$s_id = "select h.hoteloid, r.roomoid, ro.roomofferoid, '".$channeloid."' as channeloid,h.hotelcode
		from promotion p
		inner join promotiontype pt using (promotiontypeoid)
		inner join discounttype dt using (discounttypeoid)
		inner join hotel h using (hoteloid)
		inner join promotionapply pa using (promotionoid)
		inner join roomoffer ro using (roomofferoid)
		inner join offertype ot using (offertypeoid)
		inner join room r using (roomoid)
		inner join channel ch using (channeloid)
		where p.promotionoid = '".$promoapply."' and  ro.roomofferoid = '".$roomofferoid."' and  r.roomoid = '".$roomoid."'";
	}else{
		$s_id = "select h.hoteloid, r.roomoid, ro.roomofferoid, '".$channeloid."' as channeloid,h.hotelcode
		from hotel h
		inner join room r using (hoteloid)
		inner join roomoffer ro using (roomoid)
		inner join offertype ot using (offertypeoid)
		where ro.roomofferoid = '".$roomofferoid."' and  r.roomoid = '".$roomoid."'";
	}
	$q_id = mysqli_query($conn, $s_id) or mysqli_error();
	$a_id = mysqli_fetch_array($q_id);
	// echo json_encode($s_id);
	// echo json_encode($a_id);

	$hoteloid = $a_id['hoteloid'];
	$hotelcode = $a_id['hotelcode'];
	$roomoid = $a_id['roomoid'];
	$roomoffer = $a_id['roomofferoid'];
	$channeloid = $a_id['channeloid'];

	if(file_exists("../myhotel/data-xml/".$hotelcode.".xml")){
		$xml = simplexml_load_file("../myhotel/data-xml/".$hotelcode.".xml");
		for($n=0; $n<$night; $n++){
			$date = date("Y-m-d",strtotime($checkin." +".$n." day"));

			$query_tag = '//hotel[@id="'.$hoteloid.'"]/masterroom[@id="'.$roomoid.'"]/rate[@date="'.$date.'"]/rateplan[@id="'.$roomoffer.'"]/channel[@type="'.$channeloid.'"]';
			$query_allotment = '//hotel[@id="'.$hoteloid.'"]/masterroom[@id="'.$roomoid.'"]/rate[@date="'.$date.'"]/allotment/channel[@type="'.$channeloid.'"]';
			foreach($xml->xpath($query_tag) as $tagrate){
				$extrabed = (float)$tagrate->extrabed;
				$blackout = $tagrate->blackout;
				$cta = $tagrate->cta;
				$ctd = $tagrate->ctd;
			}
			$allotment = $xml->xpath($query_allotment)[0];


			if($extrabed > 0){
				$extrabedavailable = true;
			}else{
				$extrabedavailable = false;
			}

			$closeout='n';
			$n_last=(int)$night-1;
			$xpath_closeout = '//hotel[@id="'.$hoteloid.'"]/masterroom[@id="'.$roomoid.'"]/rate[@date="'.$date.'"]/closeout/text()';
			foreach($xml->xpath($xpath_closeout) as $tagcloseout){
					$closeout = $tagcloseout;
			}
			if(!isset($closeout)) $closeout='n';

			if(($closeout=='n' && $blackout=='n' && !($n==0 &&$cta=='y') && !($n==$n_last && $ctd=='y'))
				and isset($allotment) and $allotment > 0){
				// $s_occupied = "select count(bookingroomoid) as occupied
				// from bookingroom
				// inner join roomoffer ro using (roomofferoid)
				// inner join offertype ot using (offertypeoid)
				// inner join room r using (roomoid)
				// inner join channel ch using (channeloid)
				// inner join hotel using (hoteloid)
				// inner join booking using (bookingoid)
				// inner join bookingroomdtl using (bookingroomoid)
				// where ro.roomofferoid = '".$roomoffer."' and ch.channeloid = '".$channeloid."' and date = '".$date."'";
				// //echo $s_occupied;
				// $q_occupied = mysqli_query($conn, $s_occupied) or die(mysqli_error());
				// $r_occupied = mysqli_fetch_array($q_occupied);
				// $occupied = $r_occupied['occupied'];

				// $available = $allotment - $occupied;
				$available = $allotment;

				if($n == 0){
					$roomavailable = $available;
				}else{
					if($available < $roomavailable){ $roomavailable = $available; }
				}

				if($available == 0){ $roomavailable = 0; break; }

			}else{
				$roomavailable = 0; break;
			}

		}
	}

	echo $roomavailable;
	echo "-";
	echo $extrabedavailable;
	//below this only debug
	// echo "-";
	// echo $night;
	// echo "-";
	// echo $allotment;
	// echo "-";
	// echo $occupied;


?>
