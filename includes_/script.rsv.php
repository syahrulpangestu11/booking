<script type="text/javascript">
Number.prototype.formatMoney = function(c, d, t){
var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};
function isNumber(n) { return !isNaN(parseFloat(n)) && isFinite(n); }

$(function(){

	var bookbutton = $('input.general-submit');
	$(document).ready(function(){
		if(bookbutton.val()!='INQUIRY'){
			bookbutton.attr('disabled','disabled');
		}else{
			bookbutton.removeAttr('disabled');
		}
	});


	$('select[room="number"]').on('change',function(){
		elem = $(this);
		var jmlroom = parseFloat($(this).val());
		//console.log($(this).parent().children('input'));
		var roomoid = $(this).parent().children('input[id=roomoid]').val();
		var roomofferoid = $(this).parent().children('input[id=roomofferoid]').val();
		var channeloid = $(this).parent().children('input[id=channeloid]').val();
		var type = $(this).parent().children('input[name=promotionapplyoid]').attr('id');
		var id = $(this).parent().children('input[name=promotionapplyoid]').val();
		var placedetail =  $(this).parent().children('div').eq(0);
    var typeid = (type=='bar'?roomofferoid:roomofferoid+'-'+id);
		// if(id==''){id=roomofferoid;}

		var loading = $(this).next('.fa-spinner');
		loading.css('visibility','visible');
		// setTimeout(function () {
		// 	loading.css('visibility','hidden');
		// }, 1200);

// console.log('<?php echo"$base_url"; ?>/includes/fnc.check-allotment.php?jmlroom='+jmlroom+'&type='+type+'&roomoid='+roomoid+'&roomofferoid='+roomofferoid+'&channeloid='+channeloid+'&checkin=<?php echo $checkin;  ?>&night=<?php echo $night; ?>&promoapply='+id+'&tkn=<?php echo $_SESSION['tokenSession'];  ?>');
// return false;
		if(jmlroom>0){
			var client = new XMLHttpRequest;
			client.onreadystatechange = function() {
        //console.log(this);
				// console.log('responseText :');
				// console.log(this.responseText);
				
		    	if(this.readyState == 4 && this.status==200){
					loading.css('visibility','hidden');
					response = this.responseText.split('-');
					var sisa = parseFloat(response[0]);
					var extrabed = parseFloat(response[1]);
					if(sisa > 0){
						if(sisa >= jmlroom){
							var client = new XMLHttpRequest;
							client.onreadystatechange = function() {
						    	if(this.readyState == 4 && this.status==200){
									var content = this.responseText;
									placedetail.html(content);
									$('.room_input').on('change',function(){ checkInputRoom($(this)) });
								}	
							}
							client.open('GET', '<?php echo"$base_url"; ?>/includes/fnc.input-detail.php?jmlroom='+jmlroom+'&roomoid='+roomoid
										+'&roomofferoid='+roomofferoid+'&channeloid='+channeloid+'&checkin=<?php echo $checkin;  ?>&night=<?php echo $night; ?>&type='
										+type+'&promoapply='+id+'&eb='+extrabed+'&adult='+$('#adult').val(), true);
							client.send(null);
							//console.log('CheckRate #1');
							CheckRate(elem,type,id,roomofferoid,channeloid,'<?php echo $checkin;  ?>','<?php echo $night;  ?>',jmlroom,roomoid,0,1,0,0,0,loading);
						}else{
							alert('sorry, only '+jmlroom+' -- '+sisa+' room(s) available');
							elem.val(sisa);
							//console.log('CheckRate #2');
							CheckRate(elem,type,id,roomofferoid,channeloid,'<?php echo $checkin;  ?>','<?php echo $night;  ?>',sisa,roomoid,0,0,0,0,0,loading);
						}
					}else{ elem.val('0'); alert('no room available'); }
				}
			}
			client.open('GET', '<?php echo"$base_url"; ?>/includes/fnc.check-allotment.php?jmlroom='+jmlroom+'&type='+type+'&roomoid='+roomoid+'&roomofferoid='+roomofferoid+'&channeloid='+channeloid+'&checkin=<?php echo $checkin;  ?>&night=<?php echo $night; ?>&promoapply='+id+'&tkn=<?php echo $_SESSION['tokenSession'];  ?>', true);
			client.send(null);
		}else{
			//console.log('CheckRate #3');
			CheckRate(elem,type,id,roomofferoid,channeloid,'<?php echo $checkin;  ?>','<?php echo $night;  ?>',jmlroom,roomoid,0,0,0,0,0,loading);
			placedetail.html('');
			$('p[for="'+type+"-"+typeid+'"]').remove(); $('div[dtl="'+type+"-"+typeid+'"]').remove();
			totalRoom();
		}
	});

	function checkInputRoom($this){
		$box = $this.parents('.each-room-detail'); $needUpdate = false;
		$box.find('.button').hide();
		$box.find('.room_input').each(function(){ 
			$oldData = $(this).attr('old-data');
			if($(this).attr('name')=='extrabed'){
				if($(this).is(':checked') && $oldData=='0') $needUpdate = true; 
				if(!$(this).is(':checked') && $oldData=='1') $needUpdate = true; 
			}else{
				if($(this).val()!=$oldData) $needUpdate = true; 
			}
			// console.log($(this).attr('name')+'|'+$(this).val()+'|'+$oldData+'|'+$needUpdate);
		})
		if($needUpdate) $box.find('.button').show();
	}
	
	function setInputRoom_oldData($this){
		$box = $this.parents('.each-room-detail'); $this.hide();
		$box.find('.room_input').each(function(){ 
			if($(this).attr('name')=='extrabed'){
				$(this).attr('old-data',($(this).is(':checked')?'1':'0')); 
			}else{
				$(this).attr('old-data',$(this).val()); 
			}
		})

	}

	function totalRoom(){

		var placecurrency = $('#book-summary').children('h2').eq(0).children('span.grey');
		var placetotal = $('#book-summary').children('h2').eq(0).children('span.blue');
		var currency = $("input[box='totalcurrency']").val();
		var gt_room = 0;
		$("input[box='totalroom']").each(function(){
			total = parseFloat($(this).val());
			gt_room = gt_room + total;
		});
		formated_gt_room	= (gt_room).formatMoney(0, '.', ',');
		placetotal.html(formated_gt_room);
		if(gt_room > 0){
			 bookbutton.removeAttr('disabled');
			 placecurrency.html(currency);
			 placetotal.html(formated_gt_room);
		}else{
			if(bookbutton.val()!='INQUIRY'){
				bookbutton.attr('disabled','disabled');
			}else{
				bookbutton.removeAttr('disabled');
			}
			placecurrency.html("");
			placetotal.html("");
		}
	}

	function nameDetail(elem,type,typeid){

		var jmlroom = elem.val();
		// var nameroom = elem.parent().parent('li').children('div').eq(0).children('span[class=title]').html();
		var nameroom = elem.parents('.room-item').find('h4').html()+' - '+elem.parents('.room-option').find('h5').html();
		var boxdetail = $('#book-summary').children('div[id=book-summary-dtl]');
		if($('p[for="'+type+"-"+typeid+'"]').html() == null && jmlroom != '0'){
			boxdetail.append($('<p for="'+type+"-"+typeid+'"><span class="maintitle">'+nameroom+'</span></p>'));
			$('input[box="totalroom"][tmp='+type+'-'+typeid+']').each(function(){
				var nr = $(this).attr('room');
				var tr = $(this).val();
				var tc = $('input[box="totalcurrency"][tmp='+type+'-'+typeid+'][room='+nr+']').val();
				var nt = $(this).attr('night'); var sp = tr/nt;
				sp_f = (parseFloat(sp)).formatMoney(0, '.', ',');
				tr_f = (parseFloat(tr)).formatMoney(0, '.', ',');
				$('p[for="'+type+"-"+typeid+'"]').append('<span class="subtitle" for="'+type+'-'+typeid+'-'+nr+'"><b>#'+nr+':</b> '+nt+' night(s) x '+tc+' '+sp_f+'</span><span class="subprice">'+tc+' '+tr_f+'</span>');
			});
		}else{
			$('p[for="'+type+"-"+typeid+'"]').html('<span class="maintitle">'+nameroom+'</span>');
			$('input[box="totalroom"][tmp='+type+'-'+typeid+']').each(function(){
				var nr = $(this).attr('room');
				var extra = $('input[box="extrabed"][tmp='+type+'-'+typeid+'-'+nr+']').val();
				var tr = $(this).val(); tr = tr - extra;
				var tc = $('input[box="totalcurrency"][tmp='+type+'-'+typeid+'][room='+nr+']').val();
				var nt = $(this).attr('night'); var sp = tr/nt;
				sp_f = (parseFloat(sp)).formatMoney(0, '.', ',');
				tr_f = (parseFloat(tr)+parseFloat(extra)).formatMoney(0, '.', ',');
				extra_f = (parseFloat(extra)).formatMoney(0, '.', ',');
				$('p[for="'+type+"-"+typeid+'"]').append('<span class="subtitle" for="'+type+'-'+typeid+'-'+nr+'"><b>#'+nr+':</b> '+nt+' night(s) x '+tc+' '+sp_f+'</span><span class="subprice">'+tc+' '+tr_f+'</span>');
				if(extra > 0){ $('p[for="'+type+"-"+typeid+'"] span.subtitle[for="'+type+"-"+typeid+'-'+nr+'"]').after('<span class="subextra">&nbsp; +extrabed : '+tc+' '+extra_f+'</span>'); }
			});

		}
	}

	function CheckRate(elem,type,id,roomofferoid,channeloid,checkin,night,jmlroom,roomoid,delid,adult, child, numroom, eb,loading){
		var textbox = $('#textbox');
		var client = new XMLHttpRequest;
		var typeid=(type=='bar'?roomofferoid:roomofferoid+'-'+id);
		if(loading!=undefined){loading.css('visibility','visible')}
		// console.log('typeid');
		// console.log(typeid);

		client.onreadystatechange = function() {
	    	if(this.readyState == 4 && this.status==200){
				var content = this.responseText;
				if(numroom == 0){
					if($('div[dtl="'+type+"-"+typeid+'"]').html() == null){
						textbox.append($('<div dtl="'+type+"-"+typeid+'">'+content+'</div>'));
					}else{
						 $('div[dtl="'+type+"-"+typeid+'"]').html('');
						 $('div[dtl="'+type+"-"+typeid+'"]').html(content);
					}
				}else{
					$('div[dtl="'+type+"-"+typeid+"-"+numroom+'"]').remove();
					$('div[dtl="'+type+"-"+typeid+'"]').append(content);
				}
				totalRoom();
				nameDetail(elem,type,typeid);
			}
			if(loading!=undefined){loading.css('visibility','hidden')}
		}
		//console.log('?promoapply='+id+'&jmlroom='+jmlroom+'&roomoid='+roomoid+'&roomofferoid='+roomofferoid+'&channeloid='+channeloid+'&checkin='+checkin+'&night='+night+'&tkn=<?php echo $_SESSION['tokenSession']; ?>&type='+type+'&delid='+delid+'&adult='+adult+'&child='+child+'&numroom='+numroom+'&eb='+eb);
		client.open('GET', '<?php echo"$base_url"; ?>/includes/fnc.promotion-hotel.php?promoapply='+id+'&jmlroom='+jmlroom+'&roomoid='+roomoid+'&roomofferoid='+roomofferoid+'&channeloid='+channeloid+'&checkin='+checkin+'&night='+night+'&tkn=<?php echo $_SESSION['tokenSession']; ?>&type='+type+'&delid='+delid+'&adult='+adult+'&child='+child+'&numroom='+numroom+'&eb='+eb, true);
		client.send(null);
	}
	//$('a[btn="edit"]').on('click',function(){
	$(document).on('click', 'a[btn="edit"]',function(){
		positiontype = $(this).parent().parent().parent().parent().parent();
		var $this = $(this);
		var elem = positiontype.children('select[room=number]');
		var roomoid = positiontype.children('input[id=roomoid]').val();
		var roomofferoid = positiontype.children('input[id=roomofferoid]').val();
		var channeloid = positiontype.children('input[id=channeloid]').val();
		var type = positiontype.children('input[name=promotionapplyoid]').attr('id');
		var id = positiontype.children('input[name=promotionapplyoid]').val();
    var typeid=(type=="bar"?roomofferoid:roomofferoid+'-'+id);
    // if(id==''){id=roomofferoid;}

		var loading = $(this).next('.fa-spinner');
		loading.css('visibility','visible');
		// setTimeout(function () {
		// 	loading.css('visibility','hidden');
		// }, 1200);

		positiondate = $(this).parent().parent().parent().children('div').eq(1).children('div').eq(0);
		d= positiondate.children('select[dayc=dayc]').val(); m= positiondate.children('select[monthc=monthc]').val(); y= positiondate.children('select[yearc=yearc]').val();
		var checkin = y+'-'+m+'-'+d;
		nr = positiondate.children('select[dayc=dayc]').attr('name');
	   	var nra =nr.split('-'); var numroom = nra[1];
		var night = $(this).parent().parent().parent().children('div').eq(2).children('div').eq(0).children('div').eq(0).children('select[nightc=nightc]').val();
		var adult = $(this).parent().parent().parent().children('div').eq(2).children('div').eq(1).children('div').eq(0).children('select[adult=adultc]').val();
		var child = $(this).parent().parent().parent().children('div').eq(2).children('div').eq(2).children('div').eq(0).children('select[child=childc]').val();
		var jmlroom = parseFloat('1'); 
		var delid = $('input[box="bookingtemp"][tmp='+type+'-'+typeid+'-'+numroom+']').val();
		var eb = $(this).parent().parent().children('div').eq(3).children('input[type=checkbox][name=extrabed]:checked').val();
		var client = new XMLHttpRequest;
		client.onreadystatechange = function() {
			
			loading.css('visibility','hidden');
	    	if(this.readyState == 4 && this.status==200){
				var sisa = parseFloat(this.responseText);
				if(sisa >= jmlroom){
					setInputRoom_oldData($this);
					//console.log('CheckRate #4');
					CheckRate(elem,type,id,roomofferoid,channeloid,checkin,night,jmlroom,roomoid,delid,adult,child,numroom, eb,loading); 
				}else{ elem.val('0'); 
					alert('no room available');
				}
			}
		}
		client.open('GET', '<?php echo"$base_url"; ?>/includes/fnc.check-allotment.php?jmlroom='+jmlroom+'&type='+type+'&roomoid='+roomoid+'&roomofferoid='+roomofferoid+'&channeloid='+channeloid+'&checkin='+checkin+'&night='+night+'&promoapply='+id+'&tkn=<?php echo $_SESSION['tokenSession'];  ?>', true);
		client.send(null);
	});

	$(window).bind("pageshow", function() {
		var form = $('form[name="book_room"]');
		// let the browser natively reset defaults
		form[0].reset();
	});
});
</script>
