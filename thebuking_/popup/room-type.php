

<style>
    .popup_roomtype{font: normal 12px "Trebuchet MS","PTSansRegular", Calibri, Arial,Helvetica,sans-serif;	color:#333;}
    .popup_roomtype span.title{ font-weight:bold; display:block; padding-top:10px;}
    .popup_roomtype .popup-img{	display:inline-block; padding:10px; vertical-align:top;}
    .popup_roomtype .popup-img img{	width:330px; height:180px; padding:2px;	border:2px solid #63BE8C;}
    .popup_roomtype .popup-desc{ display:inline-block; max-width:400px; padding:10px; vertical-align:top;}
    .popup_roomtype ul.inline-block { display: inline-block; margin:0;}
    .popup_roomtype ul.inline-block li { padding:2px; display:block; }
    .popup_roomtype ul.inline-block li:before {	content:&bull;}
    .popup_roomtype h2 { padding: 0 10px;}

    .popup_roomtype td{vertical-align: top;width:50%;}

    .popup_roomtype .popup-img .gallery{border-radius: 10px; overflow: hidden;}
    .popup_roomtype .popup-img .gallery .carousel .item{
        min-height: 180px; /* Prevent carousel from being distorted if for some reason image doesn't load */
    }
    .popup_roomtype .popup-img .gallery .carousel .item img{
        margin: 0 auto; /* Align slide image horizontally center */
        padding:0;	border:0; 
    }
</style>
<?php
	$roomoid = $_GET['d'];
	include('../../conf/connection.php'); 
	
	$s_room = "select r.roomoid, r.name as roommaster, r.adult, r.child, r.description, r.roomsize, v.name as view from room r left join view v using (viewoid) where r.roomoid = '".$roomoid."'";
	$q_room = mysqli_query($conn, $s_room) or die();
    $room = mysqli_fetch_array($q_room);
	
	//Hotel Photo Main
	$s_rp = "SELECT photourl,thumbnailurl FROM hotelphoto hp WHERE #hp.flag = 'main' AND 
                    ref_table = 'room' AND ref_id = '".$roomoid."'";
	$q_rp = mysqli_query($conn, $s_rp);
    $arrRP = array();
    while($hotelphoto = mysqli_fetch_array($q_rp)){ $arrRP[]=$hotelphoto;}
    // echo '<script>console.log('.json_encode($arrRP).');</script>';
    //$r_rp = mysqli_fetch_array($q_rp);
	// $room_pict = $arrRP[0]['photourl'];//$r_rp['photourl'];

?>
<div id="white-popup-block" class="mfp-hide popup_roomtype">
    <h2><?php echo $room['roommaster']; ?></h2>
    <table>
        <tr>
            <td>
                <div class="popup-img">
                    <!-- <div class="thumb pict"> <img src="<?php echo $room_pict;?>"> </div>
                    -->
                    <div class="gallery">
                        <div class="bs-example">
                            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                <!-- Carousel indicators -->
                                <ol class="carousel-indicators">                                    
                                    <?php $slideto=0; foreach($arrRP as $hotelphoto){ ?>
                                        <li data-target="#myCarousel" data-slide-to="<?=$slideto;?>" class="<?=($slideto==0?'active':'');?>"></li>
                                    <?php $slideto++; } ?>   
                                </ol>   
                                <!-- Wrapper for carousel items -->
                                <div class="carousel-inner">
                                    <?php $slideto=0; foreach($arrRP as $hotelphoto){ ?>
                                        <div class="item <?=($slideto==0?'active':'');?>"><img src="<?=$hotelphoto['photourl'];?>" ></div>
                                    <?php $slideto++; } ?>    
                                </div>
                                <!-- Carousel controls -->
                                <a class="carousel-control left" href="#myCarousel" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left"></span>
                                </a>
                                <a class="carousel-control right" href="#myCarousel" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <span class="title">Size :</span> <?php echo $room['roomsize']; ?> <br />
                    <span class="title">Bed(s) :</span>
                    <?php
                        $s_roombed = "select rb.bedoid, b.name as bedname from roombed rb inner join room r using (roomoid) inner join bed b using (bedoid) where r.roomoid='".$roomoid."'";
                        $q_roombed = mysqli_query($conn, $s_roombed) or die();
                        while($roombed = mysqli_fetch_array($q_roombed)){
                            echo $roombed['bedname'];
                        }
                    ?>
                    <br />
                    <span class="title">View :</span> <?php echo $room['view']; ?> <br />
                </div>
            </td>
            <td>
                <div class="popup-desc">
                    <span class="title">Description :</span>
                    <?php echo $room['description']; ?> <br />
                    <span class="title">Room Feature :</span>
                    <ul class="inline-block top">
                    <?php
                    $s_amenities = "SELECT f.facilitiesoid, f.name , COUNT(roomoid) AS jmlroom 
                    FROM facilities f 
                    INNER JOIN facilitiestype ft USING (facilitiestypeoid) 
                    INNER JOIN roomfacilities rf USING (facilitiesoid) 
                    INNER JOIN room r USING (roomoid) 
                    WHERE ft.facilitiestypeoid = '3' and r.roomoid = '".$roomoid."'
                    GROUP BY facilitiesoid ORDER BY name ASC";                                    
                    $q_amenities = mysqli_query($conn, $s_amenities) or die();
                    $n_amenities = mysqli_num_rows($q_amenities);
                    
                    $max_row = ceil($n_amenities / 3);
                    $row = 0;
                    while($r_amenities = mysqli_fetch_array($q_amenities)){
                        $row++;
                        $amenities_name = $r_amenities['name'];
                        $have_amenities = ( $r_amenities['jmlroom'] == 1 ) ? "checked" : "unchecked" ; 
                        ?>
                        <li><?=$amenities_name;?></li>
                        <?php
                        /*if($row % $max_row == 0){
                            ?>
                                </ul><ul class="content-list inline-block top">
                            <?php
                        }*/
                    }
                    ?>
                    </ul>
                </div>
            </td>
        </tr>
    </table>
</div>
