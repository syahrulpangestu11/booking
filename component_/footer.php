<!-- <div class="row">
	<div class="col-md-3 col-xs-12">
    	<h2 class="orange">Subscribe our Newsletter</h2>
        <form>
        	<div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-envelope-o"></i></div>
                    <input type="text" class="form-control" id="email" placeholder="Enter your email here">
                </div>
			</div>
        </form>
        Subscribe to get our latest deals, promo &amp; news from Travel and Trip
    </div>
	<div class="col-md-2 col-xs-12">
    	<h3>Populer Hotels</h3>
        <ul id="footer-hotel-populer">
            <li><a href="<?php echo $base_url;?>/search/hotel/?q=Bali">Hotels in Bali</a></li>
            <li><a href="<?php echo $base_url;?>/search/hotel/?q=Yogyakarta">Hotels in Yogyakarta</a></li>
            <li><a href="<?php echo $base_url;?>/search/hotel/?q=Jakarta">Hotels in Jakarta</a></li>
            <li><a href="<?php echo $base_url;?>/search/hotel/?q=Bandung">Hotels in Bandung</a></li>
            <li><a href="<?php echo $base_url;?>/search/hotel/?q=Malang">Hotels in Malang</a></li>
            <li><a href="<?php echo $base_url;?>/search/hotel/?q=Makasar">Hotels in Makasar</a></li>
        </ul>
    </div>
	<div class="col-md-2 col-xs-12">
    	<h3>About Travel and Trip</h3>
        <ul class="list-unstyled">
        	<li><a href="<?php echo $base_url;?>/about-us">About Us</a></li>
            <li><a href="<?php echo $base_url;?>/contact-us">Contact Us</a></li>
            <li><a href="<?php echo $base_url;?>/faq">FAQ</a></li>
        </ul>
    </div>
	<div class="col-md-2 col-xs-12">
    	<h3>Other</h3>
        <ul class="list-unstyled">
        	<li><a href="#">Blog</a></li>
            <li><a href="#">Travel Guide</a></li>
            <li><a href="#">Register your hotel</a></li>
            <li><a href="#">Privacy Policy</a></li>
            <li><a href="#">Terms &amp; Condition</a></li>
        </ul>
    </div>
	<div class="col-md-3 col-xs-12 text-center">
		<h3><i class="fa fa-user-circle"></i>&nbsp;&nbsp;Customer Service 24 hours</h3>
        <ul class="list-inline social-media">
            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
        </ul>
    </div>
</div>
<div class="row">
	<div class="col-xs-12 text-center">
    	Secure transaction on Travel and Trip<br>
		<img src="https://www.thebuking.com/v2/image/comodo_secure_seal_100x85_transp.png">
    </div>
</div>
<div class="row" style="margin-top:50px;">
	<div class="col-xs-12 text-center">
		&copy; 2017 Travel and Trip. All rights reserved.
    </div>
</div> -->
<div class="">
	<div class="col-md-4 text-sm-center">
        <img class="img-brand" src="<?php echo $_profile['imglogo2'];?>">
        <p><?php echo $_profile['description'];?></p>
    </div>
    <div class="col-md-8">
        <div class="">
            <div class="col-md-4 col-sm-4 hidden-xs">
                <h3>Popular Hotel</h3>
                <ul id="footer-hotel-populer" class=list-unstyled>
                    <li><a href="<?php echo $base_url;?>/search/hotel/?q=Bali">Hotels in Bali</a></li>
                    <li><a href="<?php echo $base_url;?>/search/hotel/?q=Yogyakarta">Hotels in Yogyakarta</a></li>
                    <li><a href="<?php echo $base_url;?>/search/hotel/?q=Jakarta">Hotels in Jakarta</a></li>
                    <li><a href="<?php echo $base_url;?>/search/hotel/?q=Bandung">Hotels in Bandung</a></li>
                    <li><a href="<?php echo $base_url;?>/search/hotel/?q=Semarang">Hotels in Semarang</a></li>
                    <li><a href="<?php echo $base_url;?>/search/hotel/?q=Malang">Hotels in Malang</a></li>
                    <li><a href="<?php echo $base_url;?>/search/hotel/?q=Makassar">Hotels in Makassar</a></li>
                    <li><a href="<?php echo $base_url;?>/search/hotel/?q=Palembang">Hotels in Palembang</a></li>
                    <li><a href="<?php echo $base_url;?>/search/hotel/?q=Lombok">Hotels in Lombok</a></li>
                </ul>
            </div>
            <div class="col-md-4 col-sm-4 hidden-xs">
                <h3>Others</h3>
                <ul class="list-unstyled">
                    <li><a href="#" style="display:none;">Blog</a></li>
                    <!-- <li><a href="#">Travel Guide</a></li> -->
                    <li><a href="#" style="display:none;">Register Your Hotel</a></li>
                    <li><a href="<?php echo $base_url;?>/privacy-policy">Privacy Policy</a></li>
                    <li><a href="<?php echo $base_url;?>/terms-condition">Terms &amp; Condition</a>
                    <li><a href="<?php echo $base_url;?>/about-us">About Us</a></li></li>
                    <li><a href="<?php echo $base_url;?>/faq">FAQ</a></li>
                </ul>

                <h3>Secure Transaction</h3>
                <img src="<?php echo $base_url;?>/images/comodo_secure_seal_76x26_transp.png">
            </div>
            <div class="col-md-4 col-sm-4">
                <div id="footer-contact-us">
                    <h3>Contact US</h3>
                    <ul class="list-unstyled">
                        <li><a href="mailto:<?php echo $_profile['email'];?>"><i class="fa fa-envelope"></i>&nbsp;&nbsp;<?php echo $_profile['email'];?></a></li>
                        <li><a href="tel:<?php echo $_profile['phone'];?>"><i class="fa fa-phone"></i>&nbsp;&nbsp;<?php echo Func::setSpaceEachThird($_profile['phone']);?></a></li>
                        <li><a href="https://wa.me/<?php echo preg_replace( '/[^0-9]/', '', $_profile['whatsapp']);?>"><i class="fab fa-whatsapp"></i>&nbsp;&nbsp;<?php echo Func::setSpaceEachThird($_profile['whatsapp']);?></a></li>
                        <li><a href="#"><i class="fa fa-map-marker"></i>&nbsp;&nbsp;<?php echo $_profile['address'];?></a></li>
                    </ul>
                </div>
                <!-- <div class="hidden-xl hidden-lg hidden-md hidden-sm icons-link" id="icon-contact-us">
                    <div class="col-xs-3 text-center"><a href="#"><i class="fa fa-lg fa-envelope"></i></a></div>
                    <div class="col-xs-3 text-center"><a href="#"><i class="fa fa-lg fa-phone"></i></a></div>
                    <div class="col-xs-3 text-center"><a href="#"><i class="fab fa-lg fa-whatsapp"></i></a></div>
                    <div class="col-xs-3 text-center"><a href="#"><i class="fa fa-lg fa-map-marker"></i></a></div>
                    <div class="clear"></div>
                </div> -->
                <!-- <h3>Subscribe Our Newsletter</h3>
                <form>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-envelope-o"></i></div>
                            <input type="text" class="form-control" id="email" placeholder="Enter your email here">
                        </div>
                    </div>
                </form>
                <p>Subscribe to get our latest deals, promo &amp; news from Travel and Trip</p> -->
                
                
                <div class="hidden-xs">
                    <h3>Follow Us</h3>
                    <ul class="list-unstyled">
                        <?php
                            $_socialmedia = explode(",", $_profile['socialmediaurl']);
                            foreach($_socialmedia as $sosmed){
                                if(preg_match('(facebook|instagram|twitter|pinterest|tumblr|youtube|tripadvisor)', $sosmed, $match)){
                                    ?>
                                    <li><a href="<?php echo $sosmed;?>" target="_blank"><i class="fab fa-<?php echo $match[0];?>"></i>&nbsp;&nbsp;<?php echo ucwords($match[0]);?></a></li>
                                    <?php

                                }
                            }
                        ?>
                        <!-- <li><a href="#"><i class="fab fa-facebook"></i>&nbsp;&nbsp;Facebook</a></li>
                        <li><a href="#"><i class="fab fa-instagram"></i>&nbsp;&nbsp;Instagram</a></li>
                        <li><a href="#"><i class="fab fa-twitter"></i>&nbsp;&nbsp;Twitter</a></li> -->
                    </ul>
                </div>
                <div class="hidden-xl hidden-lg hidden-md hidden-sm icons-link row">
                    <div class="col-xs-5"><h3 style="margin: 6px 0 0 0;">Follow Us</h3></div>
                    <div class="col-xs-7 text-center ">
                        <div class="col-xs-4 text-center"><a href="#"><i class="fab fa-2x fa-facebook"></i></a></div>
                        <div class="col-xs-4 text-center"><a href="#"><i class="fab fa-2x fa-instagram"></i></a></div>
                        <div class="col-xs-4 text-center"><a href="#"><i class="fab fa-2x fa-twitter"></i></a></div>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>
</div>
