<?php
	include('includes/function-php/function.randomID.php');

	//LANGUAGE
	if(empty($_SESSION['language'])){
		$_SESSION['language'] = "1";
	}
	if(!empty($_POST['languageoid'])){
		$_SESSION['language'] = $_POST['languageoid'];
	}
	$selected_language = $_SESSION['language'];
	$fallback_language = "1";

	$default_meta_title = $_profile['name'];
	$default_meta_description = $_profile['description'];
	$default_meta_keywords = "";
	$default_meta_robots = "All";

	if(empty($uri2) or !isset($uri2) or $uri2=="" or $uri2 == "index.php"){
		$meta_title = $default_meta_title;
		$meta_description = $default_meta_description;
		$meta_keywords = $default_meta_keywords;
		$meta_robots = $default_meta_robots;
	}else if($uri2 == "search"){
		$meta_title = $_GET['q']." - search result - ".$default_meta_title;
		$meta_description = $default_meta_description;
		$meta_keywords = $default_meta_keywords;
		$meta_robots = $default_meta_robots;
	}else{
		$meta_link = "/".$uri2;
		$s_meta = "SELECT * FROM metatag mt WHERE mt.status = '1' AND mt.linktype = 'page' AND mt.link = '$meta_link'";

		//$q_meta = mysqli_query($conn, $s_meta) or die(mysqli_error());

		//$r_meta = mysqli_fetch_array($q_meta);
			$meta_title = ($r_meta['metatitle'] != "" or !empty($r_meta['metatitle'])) ? $r_meta['metatitle'] : $default_meta_title ;
			$meta_description = ($r_meta['metadescription'] != "" or !empty($r_meta['metadescription'])) ? $r_meta['metadescription'] : $default_meta_description ;
			$meta_keywords = ($r_meta['metakeywords'] != "" or !empty($r_meta['metakeywords'])) ? $r_meta['metakeywords'] : $default_meta_keywords ;
			$meta_robots = ($r_meta['metarobots'] != "" or !empty($r_meta['metarobots'])) ? $r_meta['metarobots'] : $default_meta_robots ;
	}
?>
