<?php 
	include("includes/active_login_check.php");
?>
<header class="main-header">
    <div class="red-info" align="center" style="background-color:#F2DEDE; padding:10px;">
        <span style="color:#A94842"><b>Warning :</b> This page is only for DEMO purposes. all reservation will be ignored.</span>
    </div>
	<div class="container">


        <nav class="navbar navbar-default <?php //echo ($uri2===""?"navbar-transparant":""); ?>">

            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="<?php echo $base_url; ?>" style="height:auto;">
                  <!-- bukingo -->
                  <!-- <img src="images/argond-logo-1.png" class="d-inline-block align-top" alt="" /> -->
                  <!-- <ul class="list-inline"> -->
                    <img src="<?php echo $_profile['imglogo'];?>" width="" height="" class="header-logo" alt="">
                    <!-- <li><img src="<?php echo $base_url;?>/images/argond-logo-v2.png" class="" alt=""></li> -->
                <!-- </ul> -->
                  
              </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                

                <ul class="nav navbar-nav navbar-right">
                    
                    <div class="header-bar hidden-xs">
                        <div class="col-sm-8">
                            <ul class="list-inline">
                                <li><a href="mailto:<?php echo $_profile['email'];?>"><?php echo $_profile['email'];?></a></li>
                                <li><a href="tel:<?php echo $_profile['phone'];?>"><?php echo Func::setSpaceEachThird($_profile['phone']);?></a></li>
                            </ul>
                        </div>
                        <div class="col-sm-4 text-right">
                            <ul class="list-inline">
                                <li class="dropdown user user-menu"><?php
                                if($_profile['frontend_account']=='agent'){
                                    if(isset($_SESSION['_agent_oid'])){
                                        ?>
                                        <a class="dropdown-toggle" data-toggle="dropdown">
                                            <i class="glyphicon glyphicon-user"></i>
                                            <span><?php echo $_SESSION['_agent_initial']; ?> <i class="caret"></i></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li><a href="<?php echo $base_url;?>/agent/profile" target="_blank">Profile</a></li>
                                            <li><a href="<?php echo $base_url;?>/agent/logout">Sign out</a>
                                            </li>
                                        </ul>
                                        <?php
                                    }else{
                                        ?>
                                        <a href="<?php echo $base_url;?>/agent/login">Agent Sign In</a>
                                        <?php
                                    }

                                }
                                if($_profile['frontend_account']=='member'){
                                    if(isset($_SESSION['_member_oid'])){
                                        ?>
                                        <a class="dropdown-toggle" data-toggle="dropdown">
                                            <i class="glyphicon glyphicon-user"></i>
                                            <span><?php echo $_SESSION['_member_initial']; ?> <i class="caret"></i></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li><a href="<?php echo $base_url;?>/member/profile" target="_blank">Profile</a></li>
                                            <li><a href="<?php echo $base_url;?>/member/logout">Sign out</a>
                                            </li>
                                        </ul>
                                        <?php
                                    }else{
                                        ?>
                                        <a href="<?php echo $base_url;?>/member/login">Member Sign In</a>
                                        <?php
                                    }
                                    
                                }
                                ?>
                                </li>
                                <li style="display:none"><a href="#">Register</a></li>
                            </ul>
                        </div>   
                        <div class="clear"></div>   
                    </div>

                    <li class="active"><a href="<?php echo $base_url; ?>"><i class="hidden-xl hidden-lg hidden-md hidden-sm menu-icon fa fa-compass"></i>Home</a></li>
                    <li><a href="<?php echo $base_url;?>/search/hotel/?q="><i class="hidden-xl hidden-lg hidden-md hidden-sm menu-icon fa fa-hotel"></i>Hotels</a></li>
                    <!-- <li><a href="#"><i class="hidden-xl hidden-lg hidden-md hidden-sm menu-icon fa fa-car"></i>Cars &amp; Transport</a></li>
                    <li><a href="#"><i class="hidden-xl hidden-lg hidden-md hidden-sm menu-icon fa fa-spa"></i>Spa &amp; Wellness</a></li>
                    <li><a href="#"><i class="hidden-xl hidden-lg hidden-md hidden-sm menu-icon fa fa-utensils"></i>Restaurants</a></li> -->
                    <!-- <li><a href="<?php echo $base_url; ?>/thing-to-do"><i class="hidden-xl hidden-lg hidden-md hidden-sm menu-icon fa fa-hiking"></i>Thing To Do</a></li> -->
                </ul>
                
            </div><!-- /.navbar-collapse -->
		</nav>
	</div>
</header>