
	<h3 class="section-title"><b>Top Reviewed</b> Hotels</h3>
	<hr>
	
	<div class="card-wrapper">
		<!-- Slider main container -->
		<div class="swiper-container card-container-v2">
			<!-- Additional required wrapper -->
			<div class="swiper-wrapper">
				<!-- Slides -->
				<?php
				$images = array(
					'De Java Hotel' => 'https://images.unsplash.com/photo-1488805990569-3c9e1d76d51c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=52ab20c13e8729e918954bede90d8d8a&auto=format&fit=crop&w=1350&q=20',
					'Baleka Resort' => 'https://images.unsplash.com/photo-1500930287596-c1ecaa373bb2?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=a6e1eba5ba311d6ae02157f41968a359&auto=format&fit=crop&w=1350&q=20',
					'Nagisa Bayview Villas' => 'https://images.unsplash.com/photo-1506813211037-0b52e02d19b7?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=14958ed6c5d64753f9553651dff7ffdf&auto=format&fit=crop&w=634&q=20',
					'Limasatu Resort' => 'https://images.unsplash.com/photo-1530479920821-9199623d027f?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=f868a199b696fd8281fba76df54022d8&auto=format&fit=crop&w=675&q=20',
					'The Bellevue Suites' => 'https://images.unsplash.com/photo-1489171078254-c3365d6e359f?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=fe3391048aa6a5e7e0675015330416e8&auto=format&fit=crop&w=1489&q=20',
					'Wapa Di Ume' => 'https://images.unsplash.com/photo-1505576391880-b3f9d713dc4f?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=f413419441319fa33a2409150be91aca&auto=format&fit=crop&w=634&q=20',
					'The Sintesa Residence' => 'https://images.unsplash.com/photo-1455587734955-081b22074882?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=4c92989598f74e918d8b79d6070a7888&auto=format&fit=crop&w=1350&q=20'
				);
				foreach ($images as $key => $value) {
					?>
					<div class="swiper-slide">
						<div class="card-rounded card-v2">
							<div class="thumbnailz thumb-sm">
									<img src="<?=$value;?>" alt="Indonesia">
							</div>
							<div class="card-content">
								<h4 class="card-title-container"><?=$key;?></h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vehicula fringilla ipsum. </p>
								<div class="row">
									<div class="col-xs-5 card-label"><i class="fa fa-map-marker"></i> &nbsp; Jakarta, Indonesia</div>
									<div class="col-xs-7 card-label"><i class="fa fa-dollar-sign"></i> &nbsp; from IDR 500K</div>
									<div class="clear"></div>
								</div>
							</div>
							<div class="text-center">
								<a href="#" class="btn btn-red">Book Now</a>
							</div>
						</div>
					</div>
					<?php 
				} ?>
			</div>
			<!-- If we need pagination -->
			<!-- <div id="card-v2-pagination" class="swiper-pagination"></div> -->

		</div>

		<div id="card-v2-button-prev" class="card-button-prev hidden-sm hidden-xs"><i class="fa fa-2x fa-angle-left"></i></div>
		<div id="card-v2-button-next" class="card-button-next hidden-sm hidden-xs"><i class="fa fa-2x fa-angle-right"></i></div>
	</div>