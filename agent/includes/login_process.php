<?php
$username = isset($_POST['user']) ? $_POST['user'] : $_COOKIE['_agent_u'];
$password = isset($_POST['pass']) ? $_POST['pass'] : $_COOKIE['_agent_p'];
$remember = isset($_POST['remember']) ? $_POST['remember'] : '';

setcookie("_agent_u",$username);
setcookie("_agent_p",$password);
$_SESSION['_agent_remember'] = $remember;

try {

	$stmt = $db->prepare("SELECT * FROM agent WHERE email = :usr AND password = :pwd AND publishedoid = '1'");
	$stmt->execute(array(':usr' => $username, ':pwd' => sha1($password)));
	$foundagent = $stmt->rowCount();

	if($foundagent > 0){
		$agent = $stmt->fetch(PDO::FETCH_ASSOC);

		$_SESSION['_agent_initial'] = $agent['agentname'];
		$_SESSION['_agent_oid'] = $agent['agentoid'];

		if(empty($agent['picture'])){
			$_SESSION['_agent_userpict'] = $base_url."/images/avatar5.png";
		}else{
			$_SESSION['_agent_userpict'] = $agent['picture'];
		}

		// echo"<script>window.location.href = '".$base_url."/dashboard'</script>";
		echo"<script>window.location.href = '".str_replace('/agent', '', $base_url)."/'</script>";

	// USER DOESN'T EXIST
	}else{
		echo"<script>window.location.href = '".$base_url."/loginerror'</script>";
	}


}catch(PDOException $ex) {
	echo "Invalid Query";
	die();
}
?>
