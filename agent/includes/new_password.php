<body class="login">
<div id="header">
    <div class="left">
    	<div class="main-logo"><a href="<?=str_replace('/agent', '', $base_url);?>"><img src="<?php echo"$base_url"; ?>/images/logo.png" alt="<?=$_profile['name'];?>"></a></div>
        <ul>
        	<li><a href="<?=str_replace('/agent', '', $base_url);?>"><?=$_profile['web'];?></a></li>
        </ul>
    </div>
    <div class="clear"></div>
</div>
<div id="container">
	<div class="relative">
        <div id="loginform" class="relative">
            <h2>New Password</h2>
    		<a class="close" href="<?php echo"$base_url"; ?>" title="Back to home"><i class="fa fa-times"></i></a>
            <p>Please enter twice your new password</p>
            <form method="post" action="<?php echo"$base_url"; ?>/newpwdprocess">
            	<input type="hidden" name="for" value="<?php echo $_GET['for'];?>">
            	<input type="hidden" name="key" value="<?php echo $_GET['key'];?>">
                <div class="form-group">
                    <input type="password" class="textbox" name="newpwd" id="newpwd" placeholder="New Password" autocomplete="off">
                </div>
                <div class="form-group">
                    <input type="password" class="textbox" name="repeatpwd" placeholder="Repeat New Password" autocomplete="off">
                </div>
                <div class="form-group">
                    <input type="submit" class="bt-submit" value="Validate">
                </div>
            </form>
            <div class="form-group font-small" style="padding-top:10px">
                <a href="<?php echo"$base_url"; ?>/login">Back to Login</a>
            </div>
        </div>
    </div>
</div>
<script>
	$(function() {
		jQuery.validator.addMethod("noSpace", function(value, element) { return value.indexOf(" ") < 0 && value != ""; }, "No space please and don't leave it empty");
		
		$("#loginform form").validate({
			rules: {
			   newpwd: { required: true, minlength: 6, noSpace: true }, 
			   repeatpwd: { required: true, equalTo: "#newpwd", minlength: 6, noSpace: true }
			}
		});
	});
</script>
</body>