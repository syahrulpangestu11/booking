<section class="content-header">
    <h1>Agent Profile</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-user-o"></i> Agent Profile</a></li>
    </ol>
</section>
<section class="content">
	<?php
		$s_agent = "select * from agent where agentoid = '".$_SESSION['_oid']."'"; 
		$stmt = $db->query($s_agent);
		$agent = $stmt->fetch(PDO::FETCH_ASSOC);
	?>
    <div class="box">
    	<div class="row">
            <div class="col-md-6">
                <div class="form-group col-md-12">
                    <label>Agent Pic</label>
                    <input type="text" class="form-control" name="pic" value="<?=$agent['agentpic']?>" required>
                </div>
                <div class="form-group col-md-12">
                    <label>Agent Name</label>
                    <input type="text" class="form-control" name="name" value="<?=$agent['agentname']?>" required>
                </div>
                <div class="form-group col-md-12">
                    <label>Mobile</label>
                    <input type="text" class="form-control" name="mobile" value="<?=$agent['mobile']?>" required>
                </div>
                <div class="form-group col-md-12">
                    <label>Phone</label>
                    <input type="text" class="form-control" name="phone" value="<?=$agent['phone']?>" required>
                </div>
                <div class="form-group col-md-12">
                    <label>Address</label>
                    <textarea class="form-control" name="address" required="required"><?=$agent['address']?></textarea>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group col-md-12">
                    <label>Email</label>
                    <input type="text" class="form-control" name="email" value="<?=$agent['email']?>" required>
                </div>
                <div class="form-group col-md-12">
                    <label>Password</label>
                    <input type="password" class="form-control" name="password">
                </div>
            </div>
        </div>
    </div>
</section>