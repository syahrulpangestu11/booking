<link rel="stylesheet" href="<?php echo $base_url; ?>/lib/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo $base_url; ?>/lib/admin-lte/css/AdminLTE.min.css">
<link href="<?php echo $base_url?>/css/style.css?v=1" rel="stylesheet" type="text/css">
<script src="<?php echo $base_url; ?>/lib/bootstrap/js/bootstrap.min.js"></script>
<style type="text/css">
	h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 { 
		font-family: inherit; 
		font-weight: 600;
		line-height: inherit;
		color: inherit;
		margin-bottom:10px!important;
	}
	.wrapper {
		position: inherit;
		overflow: hidden!important;
	}
	.left-side {
		padding-top: inherit;
	}
	.sidebar > .sidebar-menu li > a:hover {
		background-color: rgba(72, 115, 175, 0.26);
	}
	.sidebar .sidebar-menu .treeview-menu {
		background-color: rgb(14, 26, 43);
	}
	.sidebar .sidebar-menu .treeview-menu .treeview-menu-child {
		background-color: rgba(0, 0, 0, 0.5);
	}
	.sidebar > .sidebar-menu li.active > a {
		background-color: rgba(197, 45, 47, 0.55);
	}
	.sidebar > .sidebar-menu > li.treeview.active > a {
		background-color: inherit;
	}
	.sidebar > .sidebar-menu > li.treeview.active li.treeview-child.active > a {
		background-color: inherit;
	}
	.sidebar .sidebar-menu > li > a > .fa {
		width: 28px;
		font-size: 16px;
	}
	.sidebar-menu, .main-sidebar .user-panel, .sidebar-menu>li.header{
		white-space:normal!important;
	}
	.form-group input[type=text]{
		width:100%!important;
	}
	
	.btn-xs{ padding : 1px 5px!important; }
	
	.list-room{
		margin-bottom:10px;
		padding:10px 0;
		border: 1px solid #e0e8f5;
		background-color: rgba(228, 251, 251, 0.38);	
	}
	
	div#box-summary-reservation{
		border: 1px solid #2f3b4c;
	}
	div#box-summary-reservation > div.row{
		margin:0;
	}
	div#box-summary-reservation > div#title{
		background-color:#2f3b4c;
		color:#fff;
	}
	div#box-summary-reservation button[type=submit]{
		width:100%;
		font-size:1.1em;
		margin-bottom:5px;
	}
	
	ul#breakdown-total > li{
		border-bottom:1px solid #2f3b4c;
		padding:5px 0;
	}
	
	div#payment-option > div{
		border:1px solid #ddd;
		border-top:none;
		padding:10px;
	}
	
</style>
<?php
	switch($uri2){
		case 'profile-save' : include('agent-profile/profile-save.php'); break;
		case 'profile' : include('agent-profile/profile.php'); break;
		case 'list-hotel' : include('agent-hotel/list-hotel.php'); break;
		case 'reservation' :
			switch($uri3){
				case "confirmation" : include('reservation/payment_proof.php'); break;
				case "confirmation-save" : include('reservation/payment_proof_save.php'); break;
				case "detail" : include('reservation/detail.php'); break;
				default :  include('reservation/list-reservation.php'); break;
			}
			break;
		case 'booking' : 
			switch($uri3){
				case "payment" : include('booking/agent-payment.php'); break;
				case "process" : include('booking/agent-save-reservation.php'); break;
				case "confirmation" : include('booking/agent-confirmation.php'); break;
				default : include('booking/agent-reservation.php'); break;
			}
			break;
		default:
		
			echo "<h1 style='text-align:center'>You are not authorized for this page.</h1>"; 
		break;
	}
?>