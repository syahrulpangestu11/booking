<?php
	if(empty($_REQUEST['status']) and empty($_REQUEST['startdate']) and empty($_REQUEST['enddate'])){
		$start = date("d F Y",  strtotime(date("Y-m-d")." -7 day" ));
		$end = date("d F Y",  strtotime(date("Y-m-d")." +1 month" ));
		$status = "-";
		$viewby = "bookingtime";
	}else{
		$start = date("d F Y", strtotime($_REQUEST['startdate']));
		$end = date("d F Y", strtotime($_REQUEST['enddate']));
		$status = $_REQUEST['status'];
		$viewby = $_REQUEST['viewby'];
	}
include("js.php");
$q = $_REQUEST['q'];
?>
<style>
	form button{float:right;margin:5px;}
	form .form-control{width:100%;}
</style>
<section class="content-header">
    <h1>
        RESERVATION
    </h1>
    <ol class="breadcrumb">
        <li class="active"><a href="#"><i class="fa fa-shopping-cart"></i> RESERVATION</a></li>
    </ol>
</section>
<section class="content">
		    <div class="row">
		        <div class="box box-form">
		            <div class="box-body">
		                <div class="">
		                    <form method="get" enctype="multipart/form-data" id="data-input" action="<?php echo $base_url; ?>/reservation/">
														<input type="hidden" name="page">
		                    		<label>Booking Code  :</label> &nbsp;&nbsp;
														<input type="text" name="q" class="input-text" value="<?=$q;?>">
				                    &nbsp;&nbsp;
                                    <label>From</label>&nbsp;&nbsp;
                                    <input type="text" name="startdate" id="startdate" placeholder="" value="<?php echo $start; ?>">&nbsp;&nbsp;
                                    <label>To</label>&nbsp;&nbsp;
                                    <input type="text" name="enddate" id="enddate" placeholder="" value="<?php echo $end; ?>">
				                    &nbsp;&nbsp;
                                    <br><br>
                                    <label>Filter By</label>&nbsp;&nbsp;
                                    <select name="viewby" class="input-select">
                                    	<?php if($viewby == "bookingtime"){ $selected = 'selected = "selected"';  }else{ $selected = ''; } ?>
                                    	<option value="bookingtime" <?php echo $selected;?> >Booking Date</option>
                                    	<?php if($viewby == "checkin"){ $selected = 'selected = "selected"';  }else{ $selected = ''; } ?>
                                    	<option value="checkin" <?php echo $selected;?> >Stay Date</option>
									</select>
                                    &nbsp;&nbsp;
                                    <label>Reservation Status</label> &nbsp;
                                    <select name="status" class="input-select">
                                    <?php if($status == "all"){ $selected = 'selected = "selected"';  }else{ $selected = ''; } ?>
                                    	<option style="text-transform:capitalize;"  value="all" <?php echo $selected;?> >show all status</option>
                                    <?php
                                        try {
                                            $stmt = $db->query("select * from bookingstatus where publishedoid = '1'");
                                            $r_status = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                            foreach($r_status as $row){
												if($row['bookingstatusoid'] == $status and ctype_alpha($status) == false){ $selected = 'selected = "selected"';  }else{ $selected = ''; }
												echo"<option style='text-transform:capitalize;' value='".$row['bookingstatusoid']."' ".$selected.">  ".$row['note']."</option>";
											}
										}catch(PDOException $ex) {
                                            echo "Invalid Query";
                                            die();
                                        }
                                    
                                    ?>   
                                    </select> &nbsp;&nbsp;
                                    <button type="submit" class="small-button blue">Search</button> &nbsp;
                                    <button type="button" class="small-button green" id="export">Export to Excel</button>
		                </form>
		                </div>
		            </div><!-- /.box-body -->
		       </div>
		    </div>

		    <div class="row">
		        <div class="box">
		            <div id="data-box" class="box-body">
		                <div class="loader">Loading...</div>
		            </div><!-- /.box-body -->
		       </div>
		    </div>
</section>
