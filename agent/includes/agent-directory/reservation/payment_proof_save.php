<?php
	include("../../../../conf/connection.php");

    $agentoid = $_SESSION['_agent_oid'];
    $agent = $_SESSION['_agent_initial'];
    $bookingpaymentproofoid = $_POST['bookingpaymentproofoid'];
    $bookingnumber = $_POST['bookingnumber'];
    $bookingoid = $_POST['bookingoid'];
	$oldpict = $_POST['oldpicture'];
	
	$allowedTypes = array("image/jpg", "image/jpeg", "image/gif", "image/png");
	$allowedExts = array("jpg", "jpeg", "gif", "png");

	$prepare = "bookingoid, status, uploaded_at, uploaded_by";
	$values = ":a, :b, :c, :d";
	$execute = array(':a' => $bookingoid, ':b' => 'pending', ':c' => date("Y-m-d H:i:s"), ':d' => $agent);

	$notError = true;
	if((!empty($_FILES["picture"]["name"]) or $_FILES["picture"]["name"]!='')){
		$fileerror = $_FILES["picture"]["error"] ;
		$filetype = strtolower($_FILES["picture"]["type"]);
		$filename_ori = strtolower($_FILES["picture"]["name"]);
		$filename_array = explode(".", $filename_ori);
		$ext = ".".end($filename_array);
		$filename = str_replace($ext, "", $filename_ori);
		$uploadedname = $bookingnumber;//"default_".$filename."-".date("Y_m_d_H_i_s");
        $imagename = $uploadedname . $ext; 
		$statusError = ($fileerror > 0 && $fileerror != 4 ? true:false);

		if($statusError  && !in_array(end($filename_array), $allowedExts)  && !in_array($filetype, $allowedTypes)){
			$notError = false;
			echo"<script>window.location.href = '".$base_url."/reservation/confirmation/?no=".$bookingnumber."&error=image'</script>";
			echo "0";
			die();
		}

        if($filename!=''){
            $source = $_FILES['picture']['tmp_name'];
            $folder = "webassets/images/paymentconfirmation/";
            $target = $folder.$imagename;
            $sts = move_uploaded_file($source, $_document_root."/" . $target);
            $prepare .= ", imgproof";
            $values .= ", :e";
            $execute[':e']= ($filename!=""?str_replace('/agent',"",$base_url)."/".$folder.$imagename:$oldpict);
        }
	}
	if($notError){
		try {
			//add proof
			$stmt = $db->prepare("INSERT INTO bookingpaymentproof (".$prepare.") VAlUES(".$values.")");
			$stmt->execute($execute);

			//delete old data (replace)
            if(!empty($bookingpaymentproofoid)){
                $stmt2 = $db->prepare("DELETE FROM bookingpaymentproof WHERE bookingpaymentproofoid = ? ");
                $stmt2->execute([$bookingpaymentproofoid]);
                $deleted = $stmt2->rowCount();
            }

			//add notif
			$stmt3 = $db->query("select useroid from users where userstypeoid in (select userstypeoid from userstype where usertype='Marketing Manager')");
			$result3 = $stmt3->fetchAll(PDO::FETCH_ASSOC);
			if(count($result3)>0){
				foreach($result3 as $row){
					$stmt4 = $db->prepare("INSERT INTO notif (useroid,message,link,readstatus,created_at,created_by) VAlUES(:a,:b,:c,:d,:e,:f)");
					$stmt4->execute(array(':a'=>$row['useroid'],':b'=>$agent.' have been uploaded a Payment Confirmation (Photo) for reservation '.$bookingnumber.'.',
						':c'=>str_replace('/agent',"/myhotel",$base_url).'/booking-all/?no='.$bookingnumber,
						':d'=>'n',':e'=> date("Y-m-d H:i:s"),':f'=>$agent));
				}
			}

			//change booking status
			$stmt5 = $db->prepare("UPDATE booking set bookingstatusoid=:a where bookingnumber=:oid");
			$stmt5->execute(array(':a'=>'9', ':oid'=>$bookingnumber));

			echo"<script>window.location.href = '".$base_url."/reservation/confirmation/?no=".$bookingnumber."'</script>";
		}catch(PDOException $ex) {
			// echo"<script>console.log(".json_encode($prepare).");</script>";
			// echo"<script>console.log(".json_encode($values).");</script>";
			// echo"<script>console.log(".json_encode($execute).");</script>";
			// echo"<script>console.log(".json_encode($ex).");</script>";
			// echo"<script>console.log('deleted:".json_encode($ex)."');</script>";
			echo"<script>window.location.href = '".$base_url."/reservation/confirmation/?no=".$bookingnumber."&error=saving'</script>";
			echo "0";
			die();
		}
	}
	echo "1";
?>