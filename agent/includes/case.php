<?php
if($uri2=="login"){
	include("login.php");
}else if($uri2=="loginprocess"){
	include("login_process.php");
}else if($uri2=="loginerror"){
	$message = '<div class="alert-danger">Incorrect Username or Password</div>';
	include("login.php");
}else if($uri2=="logout"){
	include("logout.php");
}else if($uri2=="reset"){
	include("reset.php");
}else if($uri2=="resetprocess"){
	include("reset_process.php");
}else if($uri2=="resetsuccess"){
	include("reset_success.php");
}else if($uri2=="resetfailed"){
	include("reset_failed.php");
}else if($uri2=="newpwd"){
	include("new_password.php");
}else if($uri2=="newpwdprocess"){
	include("new_password_process.php");
}else if($uri2=="newpwdsuccess"){
	include("new_password_success.php");
}else if($uri2=="newpwdfailed"){
	include("new_password_failed.php");
}else{
	include("active_login_check.php");

	include("header.php");
	include("section.php");
	include("footer.php");

    echo "</body>";
}
?>
