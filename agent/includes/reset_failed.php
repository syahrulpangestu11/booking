<body class="login">
<div id="header">
    <div class="left">
    	<div class="main-logo"><a href="<?=str_replace('/agent', '', $base_url);?>"><img src="<?php echo"$base_url"; ?>/images/logo.png" alt="<?=$_profile['name'];?>"></a></div>
        <ul>
        	<li><a href="<?=str_replace('/agent', '', $base_url);?>"><?=$_profile['web'];?></a></li>
        </ul>
    </div>
    <div class="clear"></div>
</div>
<div id="container">
	<div class="relative">
        <div id="loginform" class="relative">
            <h2>Failed</h2>
    		<a class="close" href="<?php echo"$base_url"; ?>" title="Back to home"><i class="fa fa-times"></i></a>
            <p>Sorry! We do not recognize you in our system.<br>Please fill the correct username and email account or contact <a href="mailto:support@thebuking.com">support@thebuking.com</a> to assist you.</p>
            <div class="form-group font-small" style="padding-top:10px">
                <a href="<?php echo"$base_url"; ?>/login">Back to Login</a>
            </div>
        </div>
    </div>
</div>
</body>