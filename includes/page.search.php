<?php /*
<script src="<?=$base_url;?>/scripts/no-ui-slider/jquery.nouislider.min.js"></script>
<link href="<?=$base_url;?>/scripts/no-ui-slider/nouislider.fox.css" /> 
<div class="noUiSlider">
			<span></span>
		</div>
		<script>
			$(document).ready(function(){
				$(".noUiSlider").noUiSlider({
				    range: [20, 100]
				   ,start: [40, 80]
				   ,step: 20
				   ,slide: function(){
				      var values = $(this).val();
				      $("span").text(
				         values[0] +
				         " - " +
				         values[1]
				      );
				   }
				});
			});
		</script>
*/?>

<aside id="sidebar" class="inline-block">
	<div class="white-box border-box" id="search">
		<h3>Your Search</h3>
		<?php include("form.search.hotel.php"); ?>
	</div>
	
	<div class="white-box border-box">
		<h3>Filter Your Results</h3>
		<div class="clear border-bottom">
			<span class="title-blue">Hotel Name Contains</span>
			<input type="text" name="hotelname" class="top" />
			<input type="submit" class="general-submit top" value="GO" />
		</div>
				
		<div class="clear border-bottom">
			<span class="title-blue">Star Rating</span>
		<?php 
		include "scripts/star-rating/jquery.raty.php";  
		
		for($star = 5; $star>=0; $star--){
			$star_id = "star-".$star;
			$target_id = "target-".$star;
			$jml_hotel = 12;
			
			if($star == 0){
				$note = "unrated (".$jml_hotel.")";
			}else if($star == 1){
				$note = $star." star (".$jml_hotel.")";
			}else{
				$note = $star." stars (".$jml_hotel.")";
			}
			?>
            <script>
            	$(document).ready(function(){
            		starRating("<?=$star_id;?>", "<?=$star;?>");
            	});
            </script>
            <div>
            	<input type="checkbox" value="star-<?=$star;?>" id="cb-star-<?=$star;?>" class="inline-block"/>
            	<label for="cb-star-<?=$star;?>">
            		<div id="<?=$star_id;?>" class="inline-block"></div>
            		<div id="<?=$target_id;?>" class="inline-block"></div>
            		<?=$note;?>
            	</label>
        	</div>
        	<?php
        }
		?>
        </div>
				
		<div class="clear border-bottom">
			<span class="title-blue">Price Group</span>
			<div><input type="checkbox"/><label>less than IDR 499,999</label></div>
			<div><input type="checkbox"/><label>IDR 500,000 - 999,999</label></div>
			<div><input type="checkbox"/><label>IDR 1,000,000 - 1,499,999</label></div>
			<div><input type="checkbox"/><label>IDR 1,500,000 - 1,999,999</label></div>
			<div><input type="checkbox"/><label>2,000,000 +</label></div>
		</div>
		
		<div class="clear border-bottom">
			<span class="title-blue">Accommodation Type</span>
			<?php
			$sql_at = "SELECT * FROM hoteltype";
			$run_at = mysqli_query($conn, $sql_at) or die(mysqli_error());
			while($row_at = mysqli_fetch_array($run_at)){
				$category = $row_at['category'];
				?>
				<div><input type="checkbox"/><label><?=$category;?></label></div>
				<?php
			}
			?>
		</div>
		
		<div class="clear border-bottom">
			<span class="title-blue">Facilities</span>
			<div><input type="checkbox"/><label>Swimming Pool (1397)</label></div>
			<div><input type="checkbox"/><label>Internet (1386)</label></div>
			<div><input type="checkbox"/><label>Gym/Fitness (190)</label></div>
			<div><input type="checkbox"/><label>Car Park (1539)</label></div>
			<div><input type="checkbox"/><label>Spa/Sauna (701)</label></div>
			<div><input type="checkbox"/><label>Business Facilities	(501)</label></div>
			<div><input type="checkbox"/><label>Family/Child Friendly	(894)</label></div>
			<div><input type="checkbox"/><label>Restaurant	(976)</label></div>
			<div><input type="checkbox"/><label>Non Smoking Rooms	(884)</label></div>
			<div><input type="checkbox"/><label>Smoking Area	(793)</label></div>
			<div><input type="checkbox"/><label>Airport Transfer	(1354)</label></div>
			<div><input type="checkbox"/><label>Pets Allowed	(142)</label></div>
			<div><input type="checkbox"/><label>Disabled Facilities	(72)</label></div>
			<div><input type="checkbox"/><label>Golf Course (On Site)	(26)</label></div>
			<div><input type="checkbox"/><label>Nightclub	(18)</label></div>
		</div>
		
		<div class="clear border-bottom">
			<span class="title-blue">Area</span>
			<div><input type="checkbox"/><label>Kuta	(101)</label></div>
			<div><input type="checkbox"/><label>Ubud	(303)</label></div>
			<div><input type="checkbox"/><label>Seminyak	(281)</label></div>
			<div><input type="checkbox"/><label>Legian	(113)</label></div>
			<div><input type="checkbox"/><label>Nusa Dua / Benoa	(70)</label></div>
			<div><input type="checkbox"/><label>Sanur	(116)</label></div>
			<div><input type="checkbox"/><label>Denpasar	(71)</label></div>
			<div><input type="checkbox"/><label>Jimbaran	(57)</label></div>
		</div>
		
		<div class="clear">
			<span class="title-blue">Hotel chains &amp; brands</span>
			<div><input type="checkbox"/><label>Aston International Hotels, Resorts & Residences (1)
			<div><input type="checkbox"/><label>JW Marriott Hotels &amp; Resorts (1)</label></div>
			<div><input type="checkbox"/><label>Meritus (1)</label></div>
			<div><input type="checkbox"/><label>Shangri La (1)</label></div>
			<div><input type="checkbox"/><label>Sheraton (1)</label></div>
		</div>
		
	</div>
	
</aside>

<div id="right-content" class="inline-block">
	<!--
	<div class="white-box border-box">
		Soert By :
	</div>
	-->
	
	
	<?php include("list.hotel.php"); ?>
	
</div>

