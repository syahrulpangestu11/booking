<?php
function starRating($star, $type){
	global $base_url;
	/*
	- type 1 -> star only
	- type 2 -> star with off star
	*/
	if($star != "0" ){
		echo "<span>";
		for($i=1;$i<=$star;$i++){
			// echo "<img src='".$base_url."/images/star-on.png'>";
			echo '<i class="fa fa-star" style="color: #fbbc05;"></i>';
		}
		if($type == 2){
			$sisa_star = 5 - $star;
			for($i=1;$i<=$sisa_star;$i++){
				// echo "<img src='".$base_url."/images/star-off.png'>";
				echo '<i class="fa fa-star" style="color: #ddd;"></i>';
			}
		}
		echo "</span>";
	}else{
		//$star_style = "display:none;";
	}
}
?>