<?php
// "q" query string
$s_q_part1 = "  INNER JOIN city USING (cityoid) INNER JOIN state USING (stateoid) 
				INNER JOIN country USING (countryoid) INNER JOIN continent USING (continentoid) "; 
$s_q_part2 = "  (continentname LIKE '%$keyword%' OR countryname LIKE '%$keyword%' OR statename LIKE '%$keyword%' OR cityname LIKE '%$keyword%' OR hotelname LIKE '%$keyword%' ) ";

// "facilities" query string
if(!empty($_GET['facilities'])){
	$s_amenities_j = " INNER JOIN hotelfacilities USING (hoteloid) ";
}

// "area" query string
$s_area1 = "SELECT * FROM continent WHERE continentname = '$keyword'";
$q_area1 = mysqli_query($conn, $s_area1);
$n_area1 = mysqli_num_rows($q_area1);
if($n_area1 > 0){
	$r_area1 = mysqli_fetch_array($q_area1);
	$p_area_name = $r_area1['continentname'];
	$area_type = "country";
	$s_area = " SELECT countryoid AS oid, countryname AS name, COUNT(hoteloid) AS jmlhotel FROM hotel 
				RIGHT JOIN city USING (cityoid) RIGHT JOIN state USING (stateoid) 
			 	RIGHT JOIN country USING (countryoid) RIGHT JOIN continent USING (continentoid) 
			 	WHERE continentname = '$p_area_name' GROUP BY countryoid ";
}else{
	$s_area2 = "SELECT * FROM country WHERE countryname = '$keyword'";
	$q_area2 = mysqli_query($conn, $s_area2);
	$n_area2 = mysqli_num_rows($q_area2);
	if($n_area2 > 0){
		$r_area2 = mysqli_fetch_array($q_area2);
		$p_area_name = $r_area2['countryname'];
		$area_type = "state";
		$s_area = " SELECT stateoid AS oid, statename AS name, COUNT(hoteloid) AS jmlhotel FROM hotel 
					RIGHT JOIN city USING (cityoid) RIGHT JOIN state USING (stateoid) 
				 	RIGHT JOIN country USING (countryoid) 
				 	WHERE countryname = '$p_area_name' GROUP BY stateoid ";
	}else{
		$s_area3 = "SELECT * FROM state WHERE statename = '$keyword'";
		$q_area3 = mysqli_query($conn, $s_area3);
		$n_area3 = mysqli_num_rows($q_area3);
		if($n_area3 > 0){
			$r_area3 = mysqli_fetch_array($q_area3);
			$p_area_name = $r_area3['statename'];
			$area_type = "city";
			$s_area = " SELECT cityoid AS oid, cityname AS name, COUNT(hoteloid) AS jmlhotel FROM hotel 
						RIGHT JOIN city USING (cityoid) RIGHT JOIN state USING (stateoid) 
					 	WHERE statename = '$p_area_name' GROUP BY cityoid ";
		}else{
			$area_type = "";
			$s_area = "";
		}
	}
}


// inline queries
$iq_hotelname = (!empty($_GET['hotelname'])) ? " AND hotel.hotelname LIKE '%".$_GET['hotelname']."%' " : "" ;
$iq_stars = (!empty($_GET['stars'])) ? " AND hotel.stars IN (".$_GET['stars'].") " : "" ;
$iq_hoteltype = (!empty($_GET['hoteltype'])) ? " AND hoteltype.hoteltypeoid IN (".$_GET['hoteltype'].") " : "" ;
$iq_amenities = (!empty($_GET['facilities'])) ? " AND hotelfacilities.facilitiesoid IN (".$_GET['facilities'].") " : "" ;
$iq_area = (!empty($_GET['area'])) ? " AND ".$area_type."oid IN (".$_GET['area'].") " : "" ;
$iq_chain = (!empty($_GET['chain'])) ? " AND chainoid IN (".$_GET['chain'].") " : "" ;

// sort
if(!empty($_GET['sortby'])){
	$exploded_sortby = explode("-", $_GET['sortby']);
	$sort_field = $exploded_sortby[0];
	$sort_order = $exploded_sortby[1];
	$sort_by = " ORDER BY ".$sort_field." ".$sort_order ;
	
}else{
	$sort_by = ""; 
}


if($page == "home"){
	$s_hotel = "SELECT hotel.*, cityname, statename, countryname, continentname, hoteltype.category 
				FROM hotel inner join hoteltype using (hoteltypeoid) 
				".$s_q_part1."
				WHERE hotel.publishedoid in (1) and 
				".$s_q_part2." 
				GROUP BY hoteloid LIMIT 5";	
}else if ($page == "search-hotel"){
	$s_hotel = "SELECT hotel.*, cityname, statename, countryname, continentname, hoteltype.category 
				FROM hotel INNER JOIN hoteltype USING (hoteltypeoid)
				".$s_amenities_j.$s_q_part1."
				WHERE hotel.publishedoid in (1) and hotel.hotelstatusoid in (1) and 
				".$s_q_part2.$iq_hotelname.$iq_stars.$iq_hoteltype.$iq_amenities.$iq_area.$iq_chain."
				GROUP BY hoteloid ".$sort_by;	
				echo '<input type="hidden" id="zc_test" value="'.$s_hotel.'">';
}else if($page == "detail-hotel"){
	$s_hotel = "SELECT hotel.*, cityname, statename, countryname, continentname, hoteltype.category 
				FROM hotel inner join hoteltype using (hoteltypeoid) inner join city using (cityoid) inner join state using (stateoid) 
				INNER JOIN country USING (countryoid) INNER JOIN continent USING (continentoid) 
				WHERE  hotel.publishedoid in (1) and cityname = '$hotel_area' GROUP BY hoteloid"; //AND hoteloid <> '$hoteloid'	
}

print_r($s_hotel);

$q_hotel = mysqli_query($conn, $s_hotel) or die(mysqli_error());

$n_hotel = mysqli_num_rows($q_hotel);
?>