<!-- <script type="text/javascript" src="<?=$base_url;?>/scripts/jssor/jssor.js"></script>
<script type="text/javascript" src="<?=$base_url;?>/scripts/jssor/jssor.slider.js"></script> -->
<script>
	jQuery(document).ready(function ($) {
		var _SlideshowTransitions_v2 = [
			{$Duration: 1200, x: 0.3, $During: { $Left: [0.3, 0.7] }, $Easing: { $Left: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear }, $Opacity: 2 }
			];
		var options_v2 = {
			$AutoPlay: true,                                    
			$AutoPlayInterval: 1500,  
			$PauseOnHover: 1,
			$DragOrientation: 3, 
			$ArrowKeyNavigation: true,   
			$SlideDuration: 800,     
			$SlideshowOptions: {                       
				$Class: $JssorSlideshowRunner$,    
				$Transitions: _SlideshowTransitions_v2,    
				$TransitionsOrder: 1,             
				$ShowLink: true
			},
			$ArrowNavigatorOptions: {     
				$Class: $JssorArrowNavigator$,    
				$ChanceToShow: 1                    
			},

			$ThumbnailNavigatorOptions: {          
				$Class: $JssorThumbnailNavigator$,     
				$ChanceToShow: 2,              
				$ActionMode: 1,                   
				$SpacingX: 8,                    
				$DisplayPieces: 10,           
				$ParkingPosition: 360           
			}
		};
		//$AutoPlay: 0,
	    var options_v3 = { $AutoPlay: 1,
            $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$,
                $ChanceToShow: 2
            }
		};
		var jssor_slider1_v2 = new $JssorSlider$("slider1_container_v2", options_v3);
		//var jssor_slider1_v2 = new $JssorSlider$("slider1_container_v2");
		// function ScaleSlider_v2() {
		// 	var parentWidth = jssor_slider1_v2.$Elmt.parentNode.clientWidth;
		// 	if (parentWidth)
		// 		jssor_slider1_v2.$ScaleWidth(Math.max(Math.min(parentWidth, 800), 300));
		// 	else
		// 		window.setTimeout(ScaleSlider_v2, 30);
		// }
		// ScaleSlider_v2();
		// if (!navigator.userAgent.match(/(iPhone|iPod|iPad|BlackBerry|IEMobile)/)) {
		// 	$(window).bind('resize', ScaleSlider_v2);
		// }
		
		jssor_slider1_init_v4 = function (containerId) {
        var options = {
            $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$,
                $ChanceToShow: 2
            }
        };
        var jssor_slider1_v4 = new $JssorSlider$(containerId, options);
    	};
	});
</script>     
<?php 
$s_hotelphoto_v2 = "SELECT * FROM hotelphoto hp WHERE hp.hoteloid = '".$hoteloid."' AND ref_table = 'hotel'";
$q_hotelphoto_v2 = mysqli_query($conn, $s_hotelphoto_v2);
echo mysqli_num_rows($q_hotelphoto_v2);
?>
 <!-- style="position: relative; top: 0px; left: 0px; height: 456px; overflow: hidden;" -->
<div id="slider1_container_v4" style="position: relative; top: 0px; left: 0px; height: 456px; overflow: hidden;">
<!-- style="cursor: move; position: absolute; left: 0px; top: 0px; height: 356px; overflow: hidden;" -->
    <div u="slides" class="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; height: 356px; overflow: hidden;">
        <?php
        while($hotelphoto = mysqli_fetch_array($q_hotelphoto_v2)){
		?>
            <div>
                <img u="image"  src="<?php echo $hotelphoto['photourl']; ?>" />
				<!-- <img u="thumb" src="<?php echo $hotelphoto['thumbnailurl']; ?>" /> -->
				<h3>test</h3>
            </div>
        <?php
        }
        ?>            
    </div>
     <!-- Arrow Left -->
    <span u="arrowleft" class="jssora05l"></span>
    <!-- Arrow Right -->
	<span u="arrowright" class="jssora05r"></span>
	
    <script>jssor_slider1_init_v4('slider1_container_v4');</script>
     <!-- <div u="thumbnavigator" class="jssort01" style="position: absolute; height: 100px; left:0px; bottom: 0px;">
     	<div u="slides" style="cursor: move;">
				<div u="prototype" class="p" style="position: absolute; width: 72px; height: 72px; top: 0; left: 0;">
                    <div class=w><thumbnailtemplate style=" width: 100%; height: 100%; border: none;position:absolute; top: 0; left: 0;"></thumbnailtemplate></div>
                    <div class=c>
                    </div>
				</div>
            //Thumbnail Item Skin End
        </div>
	</div> -->
</div>