<form action="<?=$base_url; ?>/search/hotel/" method="get">
	<div class="">

        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                    <h3 style="color: black">Book Unique places to stay and things to do</h3>
            <label for="destination" style="color: black">destination</label>
            <div>
<!--                 <div class="field_decor_div_1"></div>
                <div class="field_decor_div_2">
                    <span>&nbsp;</span>
                    <hr class="field_decor_hr" >
                </div> -->
                <input autocomplete="off" name="q" id="q-hotel" class="search-query form-control" placeholder="Hotel, city, attraction, town" type="text" value="<?=$default_keyword;?>" >
<!--                 <div class="field_decor_div_3">
                    <i class="fa fa-chevron-down"></i>
                </div>
 -->            </div>
        </div>
        <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6  form-group-checkin">
    	    <label for="check-in" style="color: black">check in</label>
            <div>
<!--                 <div class="field_decor_div_1"></div>
                <div class="field_decor_div_2">
                    <span class="field_decor_span" >AUGUST, 2018</span>
                    <hr class="field_decor_hr" >
                </div> -->
                <input type="text" name="checkin" id="checkin" class="form-control" value="<?php echo $default_checkin; ?>">
<!--                 <div class="field_decor_div_3">
                    <i class="fa fa-chevron-down"></i>
                </div>
 -->            </div>
        </div>
    <!-- </div>
	<div class="form-group row"> -->
        <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6  form-group-night">
            <label for="nigth" style="color: black">night</label>
            <div>
<!--                 <div class="field_decor_div_1"></div>
                <div class="field_decor_div_2">
                    <span>&nbsp;</span>
                    <hr class="field_decor_hr" >
                </div> -->
                <select name="night" class="form-control" id="night">
                <?php 
                $requirenight = isset($requirenight) ? $requirenight : 1;
                for($n=$requirenight;$n<=15;$n++){ 
                    if($n==$night){ $selected="selected='selected'";  }else{ $selected=""; } ?>
                    <option value="<?php echo $n; ?>" <?php echo $selected; ?>><?php echo $n; ?></option>
                    <?php 
                } ?>
                </select>
<!--                 <div class="field_decor_div_3">
                    <i class="fa fa-chevron-down"></i>
                </div>
 -->            </div>
        </div>
        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12  form-group-adult">
            <label for="adult" style="color: black">guest</label>
            <div>
<!--                 <div class="field_decor_div_1"></div>
                <div class="field_decor_div_2">
                    <span>&nbsp;</span>
                    <hr class="field_decor_hr" >
                </div> -->
                <select name="adult" class="form-control" id="adult">
                <?php 
                $requireadult = isset($requireadult) ? $requireadult : 1;
                for($a=$requireadult;$a<=10;$a++){ 
                    if($a==$adult){ $selected="selected='selected'";  }else{ $selected=""; } ?>
                    <option value="<?php echo $a; ?>" <?php echo $selected; ?>><?php echo $a; ?></option>
                    <?php 
                } ?>
                </select>
            </div>
        </div>
    </div>
	<div class="form-group row" style="display:none">
        <label for="child" class="col-md-6 col-xs-6">Child:</label>
        <div class="col-md-6 col-xs-6">
            <div class="field_decor_div_1"></div>
            <div class="field_decor_div_2">
                <span>&nbsp;</span>
                <hr class="field_decor_hr" >
            </div>
            <select name="child" class="form-control" id="child">
            <?php 
            $requirechild = isset($requirechild) ? $requirechild : 0;
            for($c=$requirechild;$c<=3;$c++){ 
                if($c==$child){ $selected="selected='selected'";  }else{ $selected=""; } ?>
                <option value="<?php echo $c; ?>" <?php echo $selected; ?>><?php echo $c; ?></option>
                <?php 
            } ?>
            </select>
            <div class="field_decor_div_3">
                <i class="fa fa-chevron-down"></i>
            </div>
		</div>
    </div>
	<div class="">
    	<!--form-group  <label for="destination" class="col-xs-12">Promo code:</label>
        <div class="col-md-6 col-xs-6">
        	<input type="text" name="promo" id="promo" class="form-control" value="">
    	</div> -->
        <div class="form-group col-md-12 col-xs-12">
            <!-- <button type="submit" id="submit" class="btn ">Search Hotels</button> -->
            <button type="submit" onclick="showLoadingPage()" id="submit" class="btn">book your trip</button>
            <!-- btn-warning -->
        </div>
    </div>
</form>
