<script>
	// Maps
	var url = GMaps.staticMapURL({
		// size: [210, 130],
		size: [212, 150],
		zoom: 13,
	    lat: -8.730872,
		lng: 115.181437,
	    markers: [
		    {lat: -8.730872, lng: 115.181437, size: 'small'}
		]
	});
	
	$('<img/>').attr('src', url).appendTo('#static_map');
		
	var map = new GMaps({
	    el: '#basic_map',
	    width: 800,
	    height: 200,
	    lat: -8.730872,
	    lng: 115.181437,
	    zoom: 10,
	    zoomControl : true,
	    zoomControlOpt: {
	        style : 'SMALL',
	        position: 'TOP_LEFT'
	    },
	    panControl : true,
	});
</script>