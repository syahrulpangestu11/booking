<?php

	$filter_periode_sale = "(salefrom <= '".$todaydate."' and saleto >= '".$todaydate."')";

	$filter_periode_book = "(bookfrom <= '".$checkin."' and bookto >= '".$checkout."')";
	$s_promo = "select promotion.*, promotion.name as promoname, pt.type, dt.labelquestion from promotion inner join promotiontype pt using (promotiontypeoid) inner join discounttype dt using (discounttypeoid) inner join hotel h using (hoteloid) inner join promotionapply pa using (promotionoid) inner join room r using (roomoid) inner join rateplan rp using (rateplanoid) inner join channel ch using (channeloid) where h.hoteloid = '".$hoteloid."' and ch.channeloid = '".$channeloid."' and ".$filter_periode_sale."  and ".$filter_periode_book." and promotion.publishedoid = '1' group by promotionoid order by priority";
	
	$q_promo = mysqli_query($conn, $s_promo) or mysqli_error();
	while($promo = mysqli_fetch_array($q_promo)){
		$promoname = $promo['promoname'];
		$promooid = $promo['promotionoid'];
		$discounttype = $promo['discounttypeoid'];
		$discountapply = $promo['discountapplyoid'];
		$applyvalue = explode(',', $promo['applyvalue']);
		$discountvalue = $promo['discountvalue'];
		
		$s_promo_room = "select pa.promotionapplyoid, pa.roomoid, pa.rateplanoid, pa.channeloid as channelname, r.name as roomname, rp.name as planname, r.adult, r.child from promotionapply pa inner join promotion using (promotionoid) inner join hotel h using (hoteloid) inner join room r using (roomoid) inner join rateplan rp using (rateplanoid) inner join channel ch using (channeloid) where promotionoid = '".$promooid."' and ch.channeloid = '".$channeloid."'";
		$q_promo_room = mysqli_query($conn, $s_promo_room) or mysqli_error();
		while($promo_room = mysqli_fetch_array($q_promo_room)){
			$roomoid = $promo_room['roomoid'];	
			$roomname = $promo_room['roomname']." - ".$promo_room['planname'];	
			$roomadult = $promo_room['adult'];
			$roomchild = $promo_room['child'];
			$rateplanoid = $promo_room['rateplanoid'];	
			$promotionapplyoid = $promo_room['promotionapplyoid'];	
			
			$total = 0;
			$total_after = 0;

			for($n=0;$n<$night;$n++){
				unset($rate); unset($rate_after);
				
				$date = date("Y-m-d",strtotime($checkin." +".$n." day"));
				
				$query_tag = '//hotel[@id="'.$hoteloid.'"]/room[@id="'.$roomoid.'"]/rate[@date="'.$date.'"]/rateplan[@type="'.$rateplanoid.'"]/channel[@type="'.$channeloid.'"]';
					
				foreach($xml->xpath($query_tag) as $tagrate){
					$rate = $tagrate->single;
					$currency = $tagrate->currency;
					if( 
						$discountapply == '1' or
						($discountapply == '2' and in_array(($n+1), $applyvalue)) or
						($discountapply == '3' and in_array($day, $applyvalue)) or
						($discountapply == '4' and $n = 0) or
						($discountapply == '5' and $n = ($night - 1))
					){
						if($discounttype == '1'){
							$discountnote = $discountvalue."%";
							$discount = $rate * $discountvalue / 100;
						}else if($discounttype == '3'){
							$discountnote = $discountvalue;
							$discount = $discountvalue;
						}
						
						$rate_after = $rate - $discount;
					}else if(($n+1) > $night){
						$rate = 0;
						$rate_after = 0;
					}else{
						$rate_after = $rate;
					}
					
					$total = $total + $rate;
					$total_after = $total_after + $rate_after;
				}
				
				if($rate == 0){
					$total = 0;
					$total_after = 0;
					break;
				}
			}
			
			if($total_after > 0){
				$countrate++;
				$totalpernight = $total / $night;
				$totalpernight_after = $total_after / $night;
				$s_currency = "select currencycode from currency where currencyoid = '".$currency."'";
				$q_currency = mysqli_query($conn, $s_currency);
				$c = mysqli_fetch_array($q_currency);
				$currencycode = $c['currencycode'];
?>
	<li class="table-row">
		<div class="table-cell top">
			<span class="title"><?php echo $roomname; ?><br />(<?php echo $promoname; ?>)</span>
			<div class="thumb pict"><img src="<?=$room_pict;?>"></div>
			<a href="#">room info</a>
		</div>
		<div class="table-cell top center">
			<?php
			for($i=1;$i<=$roomadult;$i++){ ?>
				<img src="<?=$base_url;?>/images/adult.png">
				<?php
			}
			for($i=1;$i<=$roomchild;$i++){ ?>
				<img src="<?=$base_url;?>/images/child.png">
				<?php
			}
			?>
		</div>
		<div class="table-cell top center">
			<h4 style="text-decoration:line-through;"><span class="grey"><?php echo $currencycode;  ?></span> <span class="grey"><?php echo number_format($totalpernight); ?></span></h4>
			<h3><span class="grey"><?php echo $currencycode;  ?></span> <span class="blue"><?php echo number_format($totalpernight_after); ?></span></h3>
		</div>
		<div class="table-cell top center">
			<input type="hidden" name="roomoid" id="roomoid" value="<?php echo $roomoid; ?>">
			<input type="hidden" name="promotionapplyoid" id="promotionapplyoid" value="<?php echo $promotionapplyoid; ?>">
			<select room="number">
				<?php $default_allotment = 10; for($r=0;$r<=$default_allotment;$r++) { ?>
				<option value="<?php echo $r; ?>"><?php echo $r; ?></option>
				<?php } ?>
			</select>
			<div></div>
		</div>
	</li>
<?php
			
			} // if total > 0
			
		} //promoroom
	} //promo

?>