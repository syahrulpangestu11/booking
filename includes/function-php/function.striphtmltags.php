<?php
function stripHtmlTags($string){
	$stripped = str_replace("<br>", " ", $string);
	$stripped = str_replace("<br/>", " ", $stripped);
	$stripped = str_replace("<br />", " ", $stripped);
	$stripped = strip_tags($stripped);
	return $stripped;
}
?>