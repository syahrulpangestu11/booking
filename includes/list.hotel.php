 <!-- Nyro Modal -->
<link rel="stylesheet" href="<?=$base_url;?>/scripts/nyromodal/styles/nyroModal.css" type="text/css" media="screen" />
<script type="text/javascript" src="<?=$base_url;?>/scripts/nyromodal/jquery.nyroModal.custom.min.js"></script>
<script type="text/javascript">
$(function() {
  $('.nyroModal').nyroModal();
});
</script>
<style>#map-canvas { height: 400px; width:600px; }</style>
<script type="text/javascript">
  function initialize() {
	var mapOptions = {
	  center: { lat: -8.730165, lng: 115.178646},
	  zoom: 17
	};
	var map = new google.maps.Map(document.getElementById('map-canvas'),
		mapOptions);
	var marker = new google.maps.Marker({
	  position: { lat: -8.730165, lng: 115.178646},
	  map: map,
	  title: 'Fave ByPass Kuta'
	});
  }
  google.maps.event.addDomListener(window, 'load', initialize);
</script>

<div id="googlemapsview" style="position:absolute; left:-9999px;"><div id="map-canvas"></div></div>

<?php
	//include("list.hotel.query.php");
	require_once 'class/Paginator.class.php';
	require_once 'class/SearchHotel.class.php';
	require_once 'class/DetailHotel.class.php';

	if($page == "home"){
		$Top = new SearchHotel($conn);
		// $q_hotel = mysqli_query($conn, $Top->getQueryTopReviewed()) or die(mysqli_error());
		$q_hotel = mysqli_query($conn, $Top->getQueryTopStar()) or die(mysqli_error());
		$n_hotel = mysqli_num_rows($q_hotel);
	}else if ($page == "search-hotel"){
		// for now, this condition is unused
		$List = new SearchHotel($conn) ;
		$Paginator  = new Paginator($conn,
									$List->getQueryList(),
									$List->getQueryList('count') );
		$results    = $Paginator->getData($_GET['page'],$_GET['limit']);
		$q_hotel = $results->data;
		$n_hotel = $results->total;

	}else if($page == "detail-hotel"){
		$Nearby = new DetailHotel($conn,substr($uri8, 4, strlen($uri8)-6));
		//$q_hotel = mysqli_query($conn, $Nearby->getQueryNearby()) or die(mysqli_error());
		//$n_hotel = mysqli_num_rows($q_hotel);
		$q_hotel=$Nearby->getNearbyHotels();
		$n_hotel=sizeof($q_hotel);
	}

?>

<?php if($page != "detail-hotel" and $page != "home"){ ?>
<ul class="content-list white-box border-box" id="list-hotel">
<?php } ?>

<?php
	$gethotel = 0;
	$gethotel = $n_hotel;

	//migrate result from class
	//while($hotel = mysqli_fetch_array($q_hotel)){
	foreach($q_hotel as $hotel){
		// echo '<script>console.log('.json_encode($hotel).')</script>';
		$hoteloid = $hotel['hoteloid'];
		$hotelname = $hotel['hotelname'];
		$hotelcode = $hotel['hotelcode'];
		$star = $hotel['stars'];
		$address = $hotel['address'];
		$number_of_rooms = $hotel['number_of_rooms'];

		$hoteldescription = $hotel['description'];

		$city = $hotel['cityname'];
		$state = $hotel['statename'];
		$country = $hotel['countryname'];
		$continent = $hotel['continentname'];
		$hoteltype = $hotel['category'];

		/*------------------------- Hotel Picture -------------------------*/

		//migrate result from class
		//$s_hotelphoto = "SELECT * FROM hotelphoto hp WHERE hoteloid = '$hoteloid' AND hp.flag = 'main' AND hp.ref_table = 'hotel'";
		$ItemHotel = new DetailHotel($conn,$hoteloid);
		// $s_hotelphoto = $ItemHotel->getQueryPicture();
		// $q_hotelphoto = mysqli_query($conn, $s_hotelphoto) or die(mysqli_error());
		// $r_hotelphoto = mysqli_fetch_array($q_hotelphoto);
		$r_hotelphoto = $ItemHotel->getPhoto();
		$hotelpict = $r_hotelphoto['photourl'];


		if(file_exists("myhotel/data-xml/".$hotel['hotelcode'].".xml")){
			$xml =  simplexml_load_file("myhotel/data-xml/".$hotel['hotelcode'].".xml");

			
		// if(file_exists("../travelandtrip/data-xml/tarif.xml")){
		// 	// zc
		// 	// if(file_exists("../myhotel/data-xml/".$hotelcode.".xml")){
		// 	// $xml =  simplexml_load_file("../myhotel/data-xml/".$hotelcode.".xml");
		// 	$xml =  simplexml_load_file("../travelandtrip/data-xml/tarif.xml");

			include('list.hotel.rate.promotion.php');
		}else{
			$minrate = 0;
		}

		if($page == "home" and $minrate > 0){
		?>
			<?php /*/ ?>
			<li class="border-box">
			<a href="<?=$href_hotel;?>" class="clear">
				<div class="pict inline-block top thumb">
					<img src="<?=$hotelpict;?>">
				</div>

				<div class="inline-block top">
					<div><span class="title"><?=$hotelname;?></span></div>
					<div><?=$city;?></div>
				</div>
				<div class="fl_right inline-block right">
					<div class="star">
						<?=starRating($star, 1);?>
					</div>
					<div class="price">
						start from
						<h4><span class="strikethrough">
							<?php if($ratenet > $minrate){ echo $currencycode."&nbsp;". $shownetrate."<br>";  }?>
						</span></h4>
						<h2 class="rate">
							<span class="grey"><?=$currencycode;?></span>
							<span class="blue"><?=$hotel_minrate_final;?></span>
						</h2>
					</div>
				</div>
			</a>
			</li>
			<?php //*/ ?>
			
			<div class="swiper-slide">
				<div class="card-rounded card-v2">
					<div class="thumbnailz thumb-sm">
							<img src="<?=$hotelpict;?>" alt="Indonesia">
					</div>
					<div class="card-content">
						<h4 class="card-title-container"><?=$hotelname;?></h4>
						<p><?=$hoteldescription;?></p>
						<div class="row">
							<div class="col-xs-5 card-label"><i class="fa fa-map-marker"></i> &nbsp; <?=$city;?>, <?=$state;?></div>
							<div class="col-xs-7 card-label"><i class="fa fa-dollar-sign"></i> &nbsp; from <?=$currencycode;?> <?=$hotel_minrate_final;?></div>
							<div class="clear"></div>
						</div>
					</div>
					<div class="text-center">
						<a href="<?=$href_hotel;?>" class="btn btn-light-green">Book Now</a>
					</div>
				</div>
			</div>
		<?php
			if($gethotel == 5){ break;}
		}else if($page == "search-hotel" and $minrate > 0){
		?> 
			<li id="h-<?=$hoteloid?>">
				<div class="inline-block top center">
                	<a href="<?=$href_hotel;?>">
					<div class="pict thumb"><img src="<?=$hotelpict;?>"></div>
                    </a>
					<a href="#googlemapsview" class="button view-map nyroModal"> View Map</a>
                    <!-- Go to www.addthis.com/dashboard to customize your tools -->
					<div class="addthis_sharing_toolbox"></div>
                    <!-- Go to www.addthis.com/dashboard to customize your tools -->
					<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-543ce7af1ad924c7" async></script>

				</div>
				<div class="col-2 inline-block top">
                <a href="<?=$href_hotel;?>">
					<div>
						<span class="title"><?=$hotelname;?></span>
						<div class="star inline-block top"><?php starRating($star, 2);?></div>
					</div>
					<div><i class="small-desc"><?=$address;?></i></div>
                </a>
					<div>
						<div class="inline-block">
                        	<?=$city;?> . <span class="capitalize"><?=$hoteltype;?></span> - &quot;There are 4 people looking at this hotel.&quot;
                        </div>
					</div>
					<div><?=$hotel_headline;?></div>
				</div>

				<div class="col-3 inline-block top">
					<div class="price">
						start from
						<h4><span class="strikethrough light-grey">
							<?php if($ratenet > $minrate){ echo $currencycode."&nbsp;". $shownetrate."<br>";  }?>
						</span></h4>
						<h2 class="rate">
							<span class="grey"><?=$currencycode;?></span>
							<span class="blue"><?=$hotel_minrate_final;?></span>
						</h2>
					</div>
					<div class="bottom-right"><a class="button book" href="<?=$href_hotel;?>">BOOK NOW</a></div>
				</div>
    			<div class="listroom">
                <ul>
               		<?php
               		    $qx = 1;
						foreach($promoname as $key => $name){
							echo "<li><a href='".$href_hotel."'>";
							echo "<span class='tosca-bold fl_right'>".$price[$key]."</span>";
							echo "<span class='tosca-bold'>".$name."</span>";
							echo "</a></li>";
							if($qx == 5) break;
							$qx++;
						}
					?>
                </ul>
                </div>
			</li>
        <?php
		}else if($page == "detail-hotel" and $minrate > 0){ //detail hotel
		?>				

			<!-- <div class="swiper-slide">
				<div class="pict top thumb"><img class="" src="<?=$hotelpict;?>"></div>
				<div class="inline-block top" >
					<div>
						<div class="title inline-block">
							<a href="<?=$href_hotel;?>" class="text"><?=$hotelname;?></a>
							<div class="star top" style="display:inline !important;"><?=starRating($star, 1);?></div>
						</div>
					</div>
					<div class="price">
						<div class="inline-block top">from</div>
						<div class="inline-block top fl_right">
							<h4 class="rate">
								<span class="grey"><?=$currencycode;?></span>
								<span class="blue"><?=$hotel_minrate;?></span>
							</h4>
						</div>
					</div>
					<div class="address " style="clear: both;font-style: italic;font-size: 12px; margin-bottom:5px;">
						<?=$address;?>
					</div>
					<div class="detail" style="clear: both;font-size: 12px;">
						<span><b>Hotel Area :</b> <?=$city;?></span><br/>
						<span><b>Number of Room :</b> <?=$number_of_rooms;?></span>
					</div>
				</div>
			</div> -->


			<div class="swiper-slide">
				<div class="card-v2">
					<div class="thumbnailz thumb-sm">
							<img src="<?=$hotelpict;?>" alt="Indonesia">
					</div>
					<div class="card-content">
						<h4 class="card-title-container"><?=$hotelname;?></h4>
						<p class="star top" style=""><?=starRating($star, 1);?></p>
						<div class="row">
							<!-- <div class="col-xs-12 card-label"><i class="fa fa-map-marker"></i> &nbsp; <?=$city;?></div> -->
							<div class="col-xs-12 card-label"><i class="fa fa-hotel"></i> &nbsp; <?=$number_of_rooms;?></div>
							<div class="col-xs-12 card-label"><i class="fa fa-dollar-sign"></i> &nbsp; from <?=$currencycode;?> <?=$hotel_minrate;?></div>
							<div class="clear"></div>
						</div>
					</div>
					<div class="text-center">
						<a href="<?=$href_hotel;?>" class="btn btn-red">Book Now</a>
					</div>
				</div>
			</div>

		<?php
			if($gethotel == 3){ break; }
		}
	}

	if($gethotel == 0){
	?>
        <img src="<?=$base_url;?>/images/warning.png" class="inline-block top" />
        <div class="inline-block top">Sorry, no results match your criteria.</div>
	<?php
	}
?>

<?php if($page != "detail-hotel"){ ?>
</ul>
<?php } ?>

<?php
	if($page == "search-hotel" and $minrate > 0){
		echo $Paginator->createLinks('pagination pagination-sm');
	}
?>
