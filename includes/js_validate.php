<!-- /* this file belong to guest-detail and payment-detail */ -->
<script src="<?php echo"$base_url"; ?>/scripts/validation/jquery-validate.js" type="text/javascript"></script>
<script src="<?php echo"$base_url"; ?>/scripts/validation/jquery-validate-creditcard.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
	// validate signup form on keyup and submit
	$("#paymentForm, #payment-form").validate({
		rules: {
			firstname: "required", lastname: "required", email: { required: true, email: true }, 
			mobilephone : "required", address: "required", city: "required", state : "required", country : "required", 
			arrivaldate : "required", cardholder : "required", cardtype : "required", 
			cardnumber: {creditcard2: function(){ return $('#cardtype').val(); }}, 
			monthexpiry: "required", yearexpiry: "required", address_cc: "required", city_cc: "required", state_cc : "required", 
			country_cc : "required", mobilephone: "number", number: "number", userkbca: "required",
		},
		messages: {
			email: "not valid email", cardtype : "select card type", cardnumber : "not valid card number",
		},
		// submitHandler: function(form, event) { 
		// 	console.log('submitHandler');
		// 	console.log(form);
		// 	event.preventDefault();
		// 	//submit via ajax
		// 	return false;
		// },
		// invalidHandler : function(form, event) { 
		// 	console.log('invalidHandler');
		// 	console.log(form);
		// 	event.preventDefault();
		// 	//submit via ajax
		// 	return false;
		// }
	});
	
	$("#mobile,#mobilephone,#phone").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        $("#errmsg").html("please input number only").show().fadeOut(3500);
               return false;
    }
   });
});
</script>
 
<!--<link rel="stylesheet" href="<?php echo"$base_url"; ?>/scripts/datepicker/themes/base/jquery.ui.all.css">-->
<link rel="stylesheet" href="<?php echo"$base_url"; ?>/scripts/datepicker/themes/base/jquery.ui.theme.css">
<link rel="stylesheet" href="<?php echo"$base_url"; ?>/scripts/datepicker/themes/base/jquery.ui.core.css">
<link rel="stylesheet" href="<?php echo"$base_url"; ?>/scripts/datepicker/themes/base/jquery.ui.datepicker.css">
<script src="<?php echo"$base_url"; ?>/scripts/datepicker/ui/jquery.ui.core.js"></script>
<script src="<?php echo"$base_url"; ?>/scripts/datepicker/ui/jquery.ui.widget.js"></script>
<script src="<?php echo"$base_url"; ?>/scripts/datepicker/ui/jquery.ui.datepicker.js"></script>   
<script type="text/javascript">
$(function() {
	$( "#arrivaldate" ).datepicker();
});
</script>