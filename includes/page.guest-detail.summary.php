<div class="col-md-6 col-sm-6 col-xs-12 top border-right">
<?php

	$sum_name = array();
	$sum_jml = array();
	$sum_currency = array();
	$sum_total = array();
	$sum_deposit = array();
	$currencyoid='1';
	
	$s_data = "select bt.*, 
	r.hoteloid, r.roomoid,
	ro.name as roomname, ro.roomofferoid,
	ot.offername as planname, 
	ch.name as channelname, 
	count(bookingtempoid) as jmlroom, 
	sum(total) as totalgrand, 
	sum(deposit) as totaldeposit,
	c.currencycode, 
	p.name as promoname 
	from bookingtemp bt
	inner join roomoffer ro using (roomofferoid)
	inner join offertype ot using (offertypeoid) 
	inner join channel ch using (channeloid)
	inner join currency c using (currencyoid)
	left join promotion p using (promotionoid)
	inner join room r using (roomoid) 
	where (session_id = '$_SESSION[tokenSession]' and session_id <> '') 
	group by bt.roomofferoid, bt.channeloid, bt.promotionoid";
Func::logJS($s_data);
$q_data = mysqli_query($conn, $s_data) or die(mysqli_error());
Func::logJS($q_data);
while($promotion = mysqli_fetch_array($q_data)){
		if(!empty($promotion['currencyoid'])){
			$currencyoid=$promotion['currencyoid'];
		}
		if(!empty($promotion['promoname'])){
			$roomname = $promotion['roomname']." - ".$promotion['promoname'];
		}else{
			$roomname = $promotion['roomname'];
		}
		$hoteloid = $promotion['hoteloid'];
		$roomoid = $promotion['roomoid'];
		$roomoffer = $promotion['roomofferoid'];
		$channeloid = $promotion['channeloid'];
		$promotionoid = $promotion['promotionoid'];
		$total = $promotion['total'];
		array_push($sum_name,$roomname);
		array_push($sum_jml,$promotion['jmlroom']);
		array_push($sum_currency,$promotion['currencycode']);
		array_push($sum_total,$promotion['totalgrand']);
		array_push($sum_deposit,$promotion['totaldeposit']);

		$s_roomphoto = "SELECT thumbnailurl FROM hotelphoto hp WHERE hp.hoteloid = '".$hoteloid."' AND hp.flag = 'main' AND ref_table = 'room' and ref_id='".$roomoid."' ";
		$q_roomphoto = mysqli_query($conn, $s_roomphoto);
		$a_roomphoto = mysqli_fetch_array($q_roomphoto);
		$roomphoto = $a_roomphoto['thumbnailurl'];
		
?>
	<div>
			<h4><a href="#" class="text"><?=$roomname;?></a></h4>
			<div class="room-pict thumb inline-block top"><img src="<?=$roomphoto;?>"></div>
			<div class="room-detail inline-block top">
				<?php
				$r = 0;
				$s_temp = "select bt.checkin, bt.checkout, bt.adult, bt.child, bt.total, c.currencycode, case when extrabedtotal > 0 then 'with extrabed' else '' end as noteextrabed from bookingtemp bt
					inner join currency c using (currencyoid)
				 	where session_id = '".$_SESSION['tokenSession']."' and promotionoid = '".$promotionoid."' and roomofferoid = '".$roomoffer."' and channeloid = '".$channeloid."'";
				$q_temp = mysqli_query($conn, $s_temp) or die(mysqli_error());
				while($temp = mysqli_fetch_array($q_temp)){
					$r++;
					$checkindate = date('d F Y', strtotime($temp['checkin']));
					$checkoutdate = date('d F Y', strtotime($temp['checkout']));
				?>
				<div style="display: inline-block; padding-right: 50px;">
					<b>Room <?=$r;?> :</b>
					<br>
					<?=$checkindate;?> - <?=$checkoutdate;?> <i>(<?=$temp['night'];?> nights)</i>
					<br>
					<?=$temp['adult'];?> adults / <?=$temp['child'];?> child<br />
                    <?=$temp['noteextrabed'];?>
				</div>
				<div style="display: inline-block;">
					<h3>
        	<span class="grey"><?php echo $temp['currencycode'];?></span> 
            <span class="blue"><?php echo number_format($temp['total']);?></span>
			</h3>
				</div>
				<?php	
				}
				?>
			</div>	
			<div class="room-detail inline-block top" style="float: right;">
			
				<?php foreach($sum_name as $key => $value){
?>
		
		
		
<?php
		$grandtotalroom = $grandtotalroom + $sum_total[$key];
		$grandtotaldeposit = $grandtotaldeposit + $sum_deposit[$key];
		// $_SESSION['currencytotal'] = $total['currencyoid']; 
		$_SESSION['currencytotal'] = $currencyoid; 
		
	} ?>
			</div>	
		</div>
<?php
	}
	if(mysqli_num_rows($q_data)==0 && $_profile["inquiry_status"]=='y'){
		$_SESSION['currencytotal'] = $currencyoid; 
		$_SESSION['grandtotal'] = 0;
		
		$i_data = mysqli_query($conn, "select*from bookingtemp where session_id='$_SESSION[tokenSession]'") or die(mysqli_error());
		while($inq = mysqli_fetch_array($i_data)){
			$adult = $inq['adult']; $child = $inq['child']; $night = $inq['night'];
			$checkin = date('d M Y', strtotime($inq['checkin']));
			$checkout = date('d M Y', strtotime($inq['checkin'].'+'.$night.' day'));
		}
		?>
		<div>
				<h4 class="text">Any Room Available (Inquiry)</h4>
				<div class="room-detail inline-block top">
					<div>
						<?=$checkin;?> - <?=$checkout;?> <i>(<?=$night;?> nights)</i>
						<br>
						<?=$adult;?> adults / <?=$child;?> child<br />
					</div>
				</div>
			</div>
	<?php
	}

?>

<?php include('page.guest-detail.summary-extra.php'); ?>
</div> <!-- end of .inline-block -->

<div class="col-md-6 col-sm-6 col-xs-12 top">
<?php
// echo $grandtotalroom;
if(!(mysqli_num_rows($q_data)==0 && $_profile["inquiry_status"]=='y')){
	// 
	// echo "<script>console.log(".json_encode($sum_name).");</script>";
	// echo "<script>console.log(".json_encode($sum_jml).");</script>";
	// echo "<script>console.log(".json_encode($sum_total).");</script>";


	?>



	<h5>Total :</h5>
	<h3>
    	<span class="grey"><?php echo $sum_currency[$key];?></span>
        <span class="blue"><?php echo number_format($grandtotalroom + $grandtotalroom_extra); ?></span>
	</h3>
	<h5>Deposit :</h5>
	<h3>
    	<span class="grey"><?php echo $sum_currency[$key];?></span>
        <span class="blue"><?php echo number_format($grandtotaldeposit + $grandtotaldeposit_extra); ?></span>
	</h3>
	<h4>Grand Total :</h4>
	<h2>
		<?php $grandtotalroomakhir = $grandtotalroom + $grandtotalroom_extra;
			  $grandtotaldepositakhir = $grandtotaldeposit + $grandtotaldeposit_extra; ?>
    	<span class="grey"><?php echo $sum_currency[$key];?></span>
        <span class="blue"><?php echo number_format($grandtotaldepositakhir + $grandtotalroomakhir ); ?></span>
	</h2>

	<?php
	$_SESSION['grandtotals'] = $grandtotalroomakhir; 
	$_SESSION['grandtotaldeposits'] = $grandtotaldepositakhir;
	$_SESSION['grandtotalrs'] = $grandtotalroomakhir + $grandtotaldepositakhir; 
}
	?>
</div> <!-- end of .inline-block -->