<form action="<?=$base_url;?>/search/tour/" method="get">
	
	<div>Destination</div>
	<div><input autocomplete="off" name="q" id="q-tour" class="search-query border-box" placeholder="nama kota, region, distrik atau hotel" type="text" value="<?=$default_keyword;?>" ></div>
	
	<div class="inline-block top">
		<div>Arrival Date :</div>
		<div><input type="text" name="checkin" id="checkin" class="calinput" readonly value="<?php echo $default_checkin; ?>"></div>
	</div>
	
	<div class="inline-block top" style="display: none;">
		<label for="night">Night</label>
		<select name="night" id="night">
		<?php 
		$requirenight = isset($requirenight) ? $requirenight : 1;
		for($n=$requirenight;$n<=20;$n++){ 
			if($n==$night){ $selected="selected='selected'";  }else{ $selected=""; } ?>
			<option value="<?php echo $n; ?>" <?php echo $selected; ?>><?php echo $n; ?></option>
			<?php 
		} ?>
		</select>
	</div>
	
	<div class="inline-block top">
		<div for="adult">Adult</div>
		<div><select name="adult" id="adult">
		<?php 
		$requireadult = isset($requireadult) ? $requireadult : 1;
		for($a=$requireadult;$a<=20;$a++){ 
			if($a==$adult){ $selected="selected='selected'";  }else{ $selected=""; } ?>
			<option value="<?php echo $a; ?>" <?php echo $selected; ?>><?php echo $a; ?></option>
			<?php 
		} ?>
		</select>
		</div>
	</div>
	
	<div class="inline-block top">
		<div for="child">Child</div>
		<div><select name="child" id="child">
		<?php 
		$requirechild = isset($requirechild) ? $requirechild : 0;
		for($c=$requirechild;$c<=20;$c++){ 
			if($c==$child){ $selected="selected='selected'";  }else{ $selected=""; } ?>
			<option value="<?php echo $c; ?>" <?php echo $selected; ?>><?php echo $c; ?></option>
			<?php 
		} ?>
		</select>
		</div>
	</div>
	
	<div class="right inline-block top"><input type="submit" id="submit" class="search-submit" value="Cari Tours" /></div>
	
</form>