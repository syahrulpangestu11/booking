<?php
	$s_data="select booking.*, customer.*, currency.*, bookingstatus.note as status, bookingpayment.*, country.countryname 
	from booking 
	inner join bookingstatus using (bookingstatusoid) 
	inner join currency using (currencyoid) 
	inner join customer using (custoid) 
	inner join country using (countryoid) 
	left join bookingpayment using (bookingoid) 
	where booking.bookingoid = '".$bookingoid."' group by booking.bookingoid";
	$q_data = mysqli_query($conn, $s_data) or die ("SQL Error : ".$s_data);
	$data = mysqli_fetch_array($q_data);

	$bookingnumber = $data['bookingnumber'];
	$firstname = $data['firstname'];
	$lastname = $data['lastname'];
	$country = $data['countryname'];
	$grandtotal = $data['currencycode']." ".number_format($data['grandtotal']);
	$hotelcollect = $data['currencycode']." ".number_format($data['hotelcollect']);
	
	$guestname = $firstname." ".$lastname;
	$guestemail = $data['email'];
?>
<div id="email-content" style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">
    <div style="margin-bottom:10px;">
        <ul class="inline-block triplet center" style="list-style-type:none; padding:0; margin:0;">
            <li style="display:inline-block; vertical-align:top; width:32%;"><h1 style="font-size:18px;">thebuking.COM</h1></li>
            <li style="display:inline-block; vertical-align:top; width:32%;">
                <h1 style="font-size:18px;">Prepaid</h1>
                <h2 style="font-size:16px;">Activities Voucher</h2>
            </li>
            <li style="display:inline-block; vertical-align:top; width:32%;">
                <h2 style="font-size:16px;">New Booking</h2>
                <b>Please print and keep this voucher for your records.</b>
            </li>
        </ul>
    </div>
    <div class="bordered"  style="margin-bottom:10px;">
        <ul class="inline-block triplet" style="list-style-type:none; padding:0; margin:0;">
            <li style="display:inline-block; vertical-align:top; width:32%;"><h3 style="font-size:14px;">Reservation Information</h3></li>
            <li style="display:inline-block; vertical-align:top; width:32%; text-align:right;" class="right"><h3 style="font-size:14px;">Booking ID</h3></li>
            <li style="display:inline-block; vertical-align:top; width:32%; text-align:left;" class="right"><h1 style="font-size:18px;"><?php echo $bookingnumber; ?></h1></li>
        </ul>
    </div>
    <div  style="margin-bottom:10px;">
        <ul class="inline-block half" style="list-style-type:none; padding:0; margin:0;">
            <li style="display:inline-block; vertical-align:top; width:47%;">
                <h3 style="font-size:14px;"><?php echo $hotelname; ?></h3>
                <b>City :</b> <?php echo $cityname; ?>
            </li>
            <li style="display:inline-block; vertical-align:top; width:47%;">
                <table style="font-size:12px;">
                    <tr>
                        <td><b>Customer First Name</b></td>
                        <td><?php echo $firstname; ?></td>
					</tr>
                    <tr>
                        <td><b>Customer Last Name</b></td>
                        <td><?php echo $lastname; ?></td>
					</tr>
                    <tr>
                        <td><b>Country of Passport</b></td>
                        <td><?php echo $country; ?></td>
					</tr>
                </table>
            </li>
        </ul>
    </div>

    <div  style="margin-bottom:10px;">
    <table class="rsv-detail" width="100%" cellpadding="2" cellspacing="0" border="1"  bordercolor="#000000" style="font-size:12px;">
        <tr bgcolor="#E8E8E8" style="font-weight:bold; font-size:12px;">
            <td>Activities</td>
            <td>Qty</td>
            <td>Price</td>
            <td>Total</td>
        </tr>
        <?php
        $syntax_activities = "select bookingitem.*, item.itemname, item.description, item.minquantity, currency.currencycode, city.cityname from bookingitem inner join currency using (currencyoid) inner join bookingmst using (bookingoid) inner join item using (itemoid) inner join city using (cityoid) where b.bookingoid = '".$bookingoid."'"; 
        $query_activities = mysqli_query($conn, $syntax_activities) or die(mysqli_error());
        while($act = mysqli_fetch_array($query_activities)){
			
			$arrivaldate	= date("d F Y",strtotime($act['arrivaldateitem']));
			$currency		= $act['currencycode'];
			
			if(!empty($act['qty'])){
				$totalprice	= $act['qty'] * $act['priceqty'];
				
				array_push($quantity, $act['qty'].' pcs');
				array_push($price, $currency.' '.number_format($act['priceqty']));
				array_push($total, $currency.' '.number_format($totalprice));
			}
			if(!empty($act['adult'])){
				$totalprice	= $act['adult'] * $act['priceadult'];
				
				array_push($quantity, $act['adult'].' adult');
				array_push($price, $currency.' '.number_format($act['priceadult']));
				array_push($total, $currency.' '.number_format($totalprice));
			}
			if(!empty($act['child'])){
				$totalprice	= $act['child'] * $act['pricechild'];
				
				array_push($quantity, $act['child'].' children');
				array_push($price, $currency.' '.number_format($act['pricechild']));
				array_push($total, $currency.' '.number_format($totalprice));
			}
			
            echo"
            <tr>
                <td>".$act['itemname']."</td>
                <td>".$arrivaldate."</td>
                <td>".implode('<br>', $quantity)."</td>
                <td>".implode('<br>', $price)."</td>
                <td>".implode('<br>', $total)."</td>
            </tr>
            ";
        }
        ?>
    </table>
    </div>

    <div style="margin-bottom:10px;">
    <table class="rsv-detail" width="100%" cellpadding="2" cellspacing="0" border="1"  bordercolor="#000000" style="font-size:12px;">
        <tr bgcolor="#E8E8E8" style="font-weight:bold; font-size:12px;">
            <td>GRAND TOTAL<br /><h1  style="font-size:18px;"><?php echo $grandtotal; ?></h1></td>
            <td>	
                <b>Booked and Payable by </b><br>
                thebuking
            </td>
        </tr>
    </table>
    </div>
</div>