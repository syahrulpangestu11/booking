<div id="email-content" style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">
    <div style="margin-bottom:10px;">
        <ul class="inline-block triplet center" style="list-style-type:none; padding:0; margin:0;">
            <li style="display:inline-block; vertical-align:top; width:32%;"><h1 style="font-size:18px;">Travel and Trip</h1></li>
            <li style="display:inline-block; vertical-align:top; width:32%;">
                <h1 style="font-size:18px;">Prepaid</h1>
                <h2 style="font-size:16px;">Hotel Voucher</h2>
            </li>
            <li style="display:inline-block; vertical-align:top; width:32%;">
                <h2 style="font-size:16px;">New Booking</h2>
                <b>Please print and keep this voucher for your records.</b>
            </li>
        </ul>
    </div>
    <div class="bordered"  style="margin-bottom:10px;">
        <ul class="inline-block triplet" style="list-style-type:none; padding:0; margin:0;">
            <li style="display:inline-block; vertical-align:top; width:32%;"><h3 style="font-size:14px;">Reservation Information</h3></li>
            <li style="display:inline-block; vertical-align:top; width:32%; text-align:right;" class="right"><h3 style="font-size:14px;">Booking ID</h3></li>
            <li style="display:inline-block; vertical-align:top; width:32%; text-align:left;" class="right"><h1 style="font-size:18px;"><?php echo $bookingnumber; ?></h1></li>
        </ul>
    </div>
    <div  style="margin-bottom:10px;">
        <ul class="inline-block half" style="list-style-type:none; padding:0; margin:0;">
            <li style="display:inline-block; vertical-align:top; width:47%;">
                <h3 style="font-size:14px;"><?php echo $hotelname; ?></h3>
                <b>City :</b> <?php echo $cityname; ?>
            </li>
            <li style="display:inline-block; vertical-align:top; width:47%;">
                <table style="font-size:12px;">
                    <tr>
                        <td><b>Customer First Name</b></td>
                        <td><?php echo $firstname; ?></td>
					</tr>
                    <tr>
                        <td><b>Customer Last Name</b></td>
                        <td><?php echo $lastname; ?></td>
					</tr>
                    <tr>
                        <td><b>Country of Passport</b></td>
                        <td><?php echo $country; ?></td>
					</tr>
                </table>
            </li>
        </ul>
    </div>
    <div  style="margin-bottom:10px;">
    <table class="rsv-detail" width="100%" cellpadding="2" cellspacing="0" border="1"  bordercolor="#000000" style="font-size:12px;">
        <tr bgcolor="#E8E8E8" style="font-weight:bold; font-size:12px;">
            <td>Room Type</td>
            <td>Check In</td>
            <td>Check Out</td>
            <td>No. of Rooms</td>
            <td>Max. Occupancy</td>
            <td>Breakfast Included</td>
            <td>No. of Extra Bed</td>
        </tr>
        <?php
        foreach($ar_roomname as $key => $value){
            echo"
            <tr>
                <td>".$value."</td>
                <td>".$ar_checkin[$key]."</td>
                <td>".$ar_checkout[$key]."</td>
                <td class='center'>".$ar_numberroom[$key]."</td>
                <td class='center'>".$ar_maxoccupancy[$key]."</td>
                <td class='center'>".$ar_breakfast[$key]."</td>
                <td class='center'>".$ar_extrabed[$key]."</td>
            </tr>
            ";
        }
        ?>
    </table>
    </div>
    <div  style="margin-bottom:10px;">
    <b>Cancellation Policy</b><br>
    <?php
        $s_cancellation = "select cp.description 
        from cancellationpolicy cp
        left join promotion p using (cancellationpolicyoid) 
        inner join bookingroom br using (promotionoid) 
        inner join booking b using (bookingoid) 
        where b.bookingoid = '".$bookingoid."' 
        group by cp.cancellationpolicyoid";
        $q_cancellation = mysqli_query($conn, $s_cancellation) or die(mysqli_error());
        while($cancellation = mysqli_fetch_array($q_cancellation)){
            echo"<p>".$cancellation['description']."</p>";
        }
    ?>
    </div>
    <div style="margin-bottom:10px;">
        <table class="detail" width="100%" border="0" style="font-size:12px;">
            <tr><td>
                Room
                <table class="rsv-detail" width="100%" cellpadding="2" cellspacing="0" border="1"  bordercolor="#000000" style="font-size:12px;">
                    <tr bgcolor="#E8E8E8" style="font-weight:bold; font-size:12px;">
                        <td>From - To</td>
                        <td>Rate</td>
                    </tr>
                    <tr>
                        <td><?php echo $min_checkin; ?></td>
                        <td><?php echo $roomtotal; ?></td>
                    </tr>
                </table>
            </td>
            <td>
                Extra Bed
                <table class="rsv-detail" width="100%" cellpadding="2" cellspacing="0" border="1"  bordercolor="#000000" style="font-size:12px;">
                    <tr bgcolor="#E8E8E8" style="font-weight:bold; font-size:12px;">
                        <td>From - To</td>
                        <td>Rate</td>
                    </tr>
                    <tr>
                    <?php 
					if($hotel['extrabedtotal'] > 0){ 
					?>
                        <td><?php echo $min_checkin; ?></td>
                        <td><?php echo $extrabedtotal; ?></td>
                    <?php	
					}else{
					?>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    <?php
					}
					?>
                    </tr>
                </table>
            </td>
		</tr>
        </table>
    </div>
    <div style="margin-bottom:10px;">
    <table class="rsv-detail" width="100%" cellpadding="2" cellspacing="0" border="1"  bordercolor="#000000" style="font-size:12px;">
        <tr bgcolor="#E8E8E8" style="font-weight:bold; font-size:12px;">
            <td>GRAND TOTAL<br /><h1  style="font-size:18px;"><?php echo $hotelcollect; ?></h1></td>
            <td>	
                <b>Booked and Payable by </b><br>
                thebuking
            </td>
        </tr>
    </table>
    </div>
    <div>
    <b>Attention Hotel Staff</b><br />
    &bull; You need to ensure the following at checkin Guest holds hotel voucher with the correct reservation details<br />
    &bull; Guest has valid photo ID
    </div>
</div>