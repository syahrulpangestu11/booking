<?php

$mail_payment = new PHPMailer(true); // the true param means it will throw exceptions on errors, which we need to catch
$mail_payment->IsSMTP(); // telling the class to use SMTP
try {
    
    $sql_mailer = "SELECT * FROM mailercompany INNER JOIN mailertype USING (mailertypeoid) WHERE companyoid='1' and publishedoid='1'";
    $qrun_mailer = $db->query($sql_mailer);
    $run_mailer = $qrun_mailer->fetchAll(PDO::FETCH_ASSOC);
    foreach($run_mailer as $row_mailer){
        switch($row_mailer['mailertypeoid']){
            case 1 : $mail_payment->SetFrom($_profile['SMTP_Username'], $row_mailer['name']); break;
            case 2 : $mail_payment->AddAddress($row_mailer['email'], $row_mailer['name']); break;
            case 3 : $mail_payment->AddCC($row_mailer['email'], $row_mailer['name']); break;
            case 4 : $mail_payment->AddBCC($row_mailer['email'], $row_mailer['name']); break;
            case 5 : $mail_payment->AddReplyTo($row_mailer['email'], $row_mailer['name']); break;
        }
    }

    $mail_payment->AddAddress($agent_mail,$agent_name);
    $mail_payment->Subject = $_profile['name']." [A] Payment Guidelines for [".$hotelname."] - [".$bookingnumber."]";
    $mail_payment->AltBody = 'To view the message, please use an HTML compatible email viewer!'; // optional - MsgHTML will create an alternate automatically
    $mail_payment->MsgHTML($body_waiting_payment);
    unset($_SESSION['tokenSession']);
    unset($bookingoid);
    if($mail_payment->Send()) {
        ?>
        <form name="mypost" action="<?php echo"$base_url/book/confirmation/$pagesuccess"; ?>" method="post">
            <input type="hidden" name="grandtotal" value="<?php echo $_SESSION['grandtotal']; ?>">
            <input type="hidden" name="currencyoid" value="<?php echo $_SESSION['currencytotal']; ?>">
            <input type="hidden" name="userkbca" value="<?php echo $_POST['userkbca']; ?>">
            <input type="hidden" name="bookingnumber" value="<?php echo $bookingnumber; ?>">
            <input type="hidden" name="paymentmethodoid" value="<?php echo $paymentmethodoid; ?>">
            <input type="hidden" name="bookingstatusoid" value="<?php echo $bookingstatusoid; ?>">
        </form>
        <script>
        document.forms["mypost"].submit();
        </script>
        <?php
    }
    
} catch (phpmailerException $e) { echo"<b>Php Mailer Error [waiting] :</b><br>"; echo $e->errorMessage(); //Pretty error messages from PHPMailer
} catch (Exception $e) { echo"<b>All Error :</b><br>"; echo $e->getMessage(); //Boring error messages from anything else!
} 

?>