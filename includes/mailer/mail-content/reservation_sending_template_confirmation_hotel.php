<?php
	$mail_hotel = new PHPMailer(true); // the true param means it will throw exceptions on errors, which we need to catch
	$mail_hotel->IsSMTP(); // telling the class to use SMTP
	try {
		$sql_mailer = "SELECT * FROM mailercompany INNER JOIN mailertype USING (mailertypeoid) WHERE companyoid='1' and publishedoid='1'";
        $qrun_mailer = $db->query($sql_mailer);
        $run_mailer = $qrun_mailer->fetchAll(PDO::FETCH_ASSOC);
        foreach($run_mailer as $row_mailer){
			switch($row_mailer['mailertypeoid']){
				case 1 : $mail_hotel->SetFrom($_profile['SMTP_Username'], $row_mailer['name']); break;
				case 2 : $mail_hotel->AddAddress($row_mailer['email'], $row_mailer['name']); break;
				case 3 : $mail_hotel->AddCC($row_mailer['email'], $row_mailer['name']); break;
				case 4 : $mail_hotel->AddBCC($row_mailer['email'], $row_mailer['name']); break;
				case 5 : $mail_hotel->AddReplyTo($row_mailer['email'], $row_mailer['name']); break;
			}
		}


		$sql_hotelto = "SELECT * FROM hotelcontact INNER JOIN mailertype USING (mailertypeoid) WHERE hoteloid = '".$hoteloid."' and mailertype='to'";
        $stmt_hotelto = $db->query($sql_hotelto);
        $data_hotelto = $stmt_hotelto->fetchAll(PDO::FETCH_ASSOC);
        foreach($data_hotelto as $data){
            $mail_hotel->AddAddress($data['email'], $data['name']);
		}
		
		$mail_hotel->Subject = $_profile['name']." [H] Booking Confirmation for [".$hotelname."] - [".$bookingnumber."]";
		$mail_hotel->AltBody = 'To view the message, please use an HTML compatible email viewer!'; // optional - MsgHTML will create an alternate automatically
		$mail_hotel->MsgHTML($body_send_to_hotel);
		
        unset($_SESSION['tokenSession']);
        unset($bookingoid);
        if($mail_hotel->Send()) {
            ?>
            <form name="mypost" action="<?php echo"$base_url/book/confirmation/$pagesuccess"; ?>" method="post">
                <input type="hidden" name="grandtotal" value="<?php echo $_SESSION['grandtotal']; ?>">
                <input type="hidden" name="currencyoid" value="<?php echo $_SESSION['currencytotal']; ?>">
                <input type="hidden" name="userkbca" value="<?php echo $_POST['userkbca']; ?>">
                <input type="hidden" name="bookingnumber" value="<?php echo $bookingnumber; ?>">
                <input type="hidden" name="paymentmethodoid" value="<?php echo $paymentmethodoid; ?>">
                <input type="hidden" name="bookingstatusoid" value="<?php echo $bookingstatusoid; ?>">
            </form>
            <script>
            document.forms["mypost"].submit();
            </script>
            <?php
        }
    } catch (phpmailerException $e) { echo"<b>Php Mailer Error :</b><br>"; echo $e->errorMessage();  echo "<br>"; echo $e->getMessage();//Pretty error messages from PHPMailer
	} catch (Exception $e) { echo"<b>All Error :</b><br>"; echo $e->getMessage(); //Boring error messages from anything else!
	} 