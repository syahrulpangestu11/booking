<?php

    // require_once('conf/connection_withpdo.php');
    require_once('includes/mailer/class.phpmailer.php');
    require_once('includes/mailer/class.hotel-email-template.php');
    
	$main_query="SELECT customer.firstname, customer.lastname, customer.email, 
                    agent.email as agentemail, agent.agentname as agentname,
                    booking.hoteloid,booking.bookingnumber,booking.bookingstatusoid,hotel.hotelname
                from booking 
                    inner join hotel using (hoteloid) 
                    inner join bookingstatus using (bookingstatusoid) 
                    inner join currency using (currencyoid) 
                    inner join customer using (custoid) 
                    inner join country using (countryoid) 
                    left join agent using (agentoid) 
                    left join bookingpayment using (bookingoid) 
                where booking.bookingoid = '".$bookingoid."' group by booking.bookingoid";
                echo $main_query;
    $main_data = $db->run($main_query)->fetch();

    echo "<script>console.log(".json_encode($main_data).");</script>"; 
    
    $bookingstatusoid = $main_data['bookingstatusoid'];//
    $inquiry_status = ($bookingstatusoid==1?true:false);
	$bookingnumber = $main_data['bookingnumber'];
	$hotelname = $main_data['hotelname'];
    $hoteloid = $main_data['hoteloid'];
    
	$guestname = $main_data['firstname']." ".$main_data['lastname'];
	$guestemail = $main_data['email'];
	$agent_mail = $main_data['agentemail'];
	$agent_name = $main_data['agentname'];
    
    $sql_mailer = "SELECT email,name,mailertype FROM mailercompany INNER JOIN mailertype USING (mailertypeoid) WHERE companyoid='1' and publishedoid='1'";
    $sql_hotelto = "SELECT email,name,mailertype FROM hotelcontact INNER JOIN mailertype USING (mailertypeoid) WHERE hoteloid = '".$hoteloid."'";
    echo($sql_hotelto);
    $arrDataMailer= array();
    $arrDataMailer['company'] = $db->run($sql_mailer)->fetchAll();
    $arrDataMailer['hotel'] = $db->run($sql_hotelto)->fetchAll();
    $arrDataMailer['guest'] = Func::getEmailDirection($guestname,$guestemail,array('to','cc'));
    $arrDataMailer['agent'] = Func::getEmailDirection($agent_name,$agent_mail,array('to','cc'));

    $_SESSION['bookingnumber']=$bookingnumber;
    $_SESSION['paymentmethodoid']=$bookingnumber;
    $_SESSION['bookingstatusoid']=$bookingstatusoid;
    Func::logJS($arrDataMailer,'arrDataMailer');
    // die();
    $_SESSION['confirmation_url']=$base_url."/book/confirmation/".$pagesuccess;
    switch($bookingstatusoid){
        case 1: //inquiry
            include 'reservation_data_template_inquiry.php';
            logJS($arrDataTemplate); 

            ob_start();
            $template = new HotelEmailTemplate($db,'inquiry',(empty($agent_mail)?'guest':'agent'),$arrDataTemplate,$arrDataMailer);
            $template->setSubject($_profile['name']." [G] Booking Inquiry for [".$hotelname."] - [".$bookingnumber."]");
            $template->getTemplate();
            $templateBody = ob_get_clean();
            $template->setBody($templateBody);
            $template->sendTemplate(null,function($data,$stsSent,$msg){
                Func::logJS($msg,$stsSent);
                ?> 
                <form name="mypost" action="<?php echo $_SESSION['confirmation_url']; ?>" method="post">
                    <input type="hidden" name="grandtotal" value="<?php echo $_SESSION['grandtotal']; ?>">
                    <input type="hidden" name="currencyoid" value="<?php echo $_SESSION['currencytotal']; ?>">
                    <input type="hidden" name="bookingnumber" value="<?php echo $_SESSION['bookingnumber']; ?>">
                    <input type="hidden" name="paymentmethodoid" value="<?php echo $_SESSION['paymentmethodoid']; ?>">
                    <input type="hidden" name="bookingstatusoid" value="<?php echo $_SESSION['bookingstatusoid']; ?>">
                </form>
                <?php 
                    unset($_SESSION['tokenSession']); unset($_SESSION['grandtotal']);unset($_SESSION['currencytotal']);
                    unset($_SESSION['bookingnumber']); unset($_SESSION['paymentmethodoid']); unset($_SESSION['bookingstatusoid']);
                    unset($_SESSION['confirmation_url']);
                ?>
                <script>
                document.forms["mypost"].submit();
                </script>
                <?php
            }); 

            break;

        case 3: //waitingpayment
            include 'reservation_data_template_waitingpayment.php';

            ob_start();
            $template = new HotelEmailTemplate($db,'waiting payment',(empty($agent_mail)?'guest':'agent'),$arrDataTemplate,$arrDataMailer);
            $template->setSubject($_profile['name']." [G] Payment Guidelines for [".$hotelname."] - [".$bookingnumber."]");
            $template->getTemplate();
            $templateBody = ob_get_clean();
            $template->setBody($templateBody);
            $template->sendTemplate(null,function($data,$stsSent,$msg){
                Func::logJS($msg,$stsSent);
                ?> 
                <form name="mypost" action="<?php echo $_SESSION['confirmation_url']; ?>" method="post">
                    <input type="hidden" name="grandtotal" value="<?php echo $_SESSION['grandtotal']; ?>">
                    <input type="hidden" name="currencyoid" value="<?php echo $_SESSION['currencytotal']; ?>">
                    <input type="hidden" name="bookingnumber" value="<?php echo $_SESSION['bookingnumber']; ?>">
                    <input type="hidden" name="paymentmethodoid" value="<?php echo $_SESSION['paymentmethodoid']; ?>">
                    <input type="hidden" name="bookingstatusoid" value="<?php echo $_SESSION['bookingstatusoid']; ?>">
                </form>
                <?php 
                    unset($_SESSION['tokenSession']); unset($_SESSION['grandtotal']);unset($_SESSION['currencytotal']);
                    unset($_SESSION['bookingnumber']); unset($_SESSION['paymentmethodoid']);unset($_SESSION['bookingstatusoid']);
                    unset($_SESSION['confirmation_url']);
                ?>
                <script>
                document.forms["mypost"].submit();
                </script>
                <?php
            }); 
            break;

        case 4: //confirmation
            include 'reservation_data_template_confirmation.php';
            Func::logJS($arrayMailer);
            Func::logJS($arrDataTemplate);
        
            ob_start();
            $templateG = new HotelEmailTemplate($db,'confirmation','guest',$arrDataTemplate,$arrDataMailer);
            $templateG->setSubject($_profile['name']." [G] Booking Confirmation for [".$hotelname."] - [".$bookingnumber."]");
            $templateG->getTemplate();
            $body_send_to_guest = ob_get_clean();
            $templateG->setBody($body_send_to_guest);
        
            // ob_start();
            // $templateA = new HotelEmailTemplate($db,'confirmation','agent',$arrDataTemplate,$arrDataMailer);
            // $templateA->setSubject($_profile['name']." [A] Booking Confirmation for [".$hotelname."] - [".$bookingnumber."]");
            // $templateA->getTemplate();
            // $body_send_to_agent = ob_get_clean();
            // $templateA->setBody($body_send_to_agent);
        
            ob_start();
            $templateH = new HotelEmailTemplate($db,'confirmation','hotel',$arrDataTemplate,$arrDataMailer);
            $templateH->setSubject($_profile['name']." [H] Booking Confirmation for [".$hotelname."] - [".$bookingnumber."]");
            $templateH->getTemplate();
            $body_send_to_hotel = ob_get_clean();
            $templateH->setBody($body_send_to_hotel);
        
            $arrayMailer[]= $templateG;
            // $arrayMailer[]= $templateA;
            $arrayMailer[]= $templateH;
            
            // $templateG->sendTemplate($arrayMailer,function($templateA,$stsSent,$msg){
            $templateG->sendTemplate($arrayMailer,function($templateH,$stsSent,$msg){
                Func::logJS($msg,$stsSent);
                // $templateA[0]->sendTemplate($templateA,function($templateH,$stsSent,$msg){
                    // Func::logJS($msg,$stsSent);
                    $templateH[0]->sendTemplate($templateH,function($nextMailer,$stsSent,$msg){
                        Func::logJS($msg,$stsSent);
                        ?> 
                        <form name="mypost" action="<?php echo $_SESSION['confirmation_url']; ?>" method="post">
                            <input type="hidden" name="grandtotal" value="<?php echo $_SESSION['grandtotal']; ?>">
                            <input type="hidden" name="currencyoid" value="<?php echo $_SESSION['currencytotal']; ?>">
                            <input type="hidden" name="bookingnumber" value="<?php echo $_SESSION['bookingnumber']; ?>">
                            <input type="hidden" name="paymentmethodoid" value="<?php echo $_SESSION['paymentmethodoid']; ?>">
                            <input type="hidden" name="bookingstatusoid" value="<?php echo $_SESSION['bookingstatusoid']; ?>">
                        </form>
                        <?php 
                            unset($_SESSION['tokenSession']); unset($_SESSION['grandtotal']);unset($_SESSION['currencytotal']);
                            unset($_SESSION['bookingnumber']); unset($_SESSION['paymentmethodoid']);unset($_SESSION['bookingstatusoid']);
                            unset($_SESSION['confirmation_url']);
                        ?>
                        <script>
                        document.forms["mypost"].submit();
                        </script>
                        <?php
                    }); 
                // });
            });
    
            break;
    }


    // switch($bookingstatusoid){
    //     case 1: //inquiry
    //         include 'reservation_data_template_inquiry.php';
    //         logJS($arrDataTemplate); 

    //         ob_start();
    //         $template = new HotelEmailTemplate($db,'inquiry','agent',$arrDataTemplate);
    //         $html_template = $template->getTemplate();
    //         echo $html_template;
    //         // die();
    //         $body_inquiry = ob_get_clean();

    //         // die();
    //         include('reservation_sending_template_inquiry_agent.php');
    //         break;

    //     case 3: //waitingpayment
    //         include 'reservation_data_template_waitingpayment.php';

    //         ob_start();
    //         $template = new HotelEmailTemplate($db,'waiting payment','agent',$arrDataTemplate);
    //         $html_template = $template->getTemplate();
    //         echo $html_template;
    //         $body_waiting_payment = ob_get_clean();
            
    //         // die();
    //         include('reservation_sending_template_waitingpayment_agent.php');
    //         break;

    //     case 4: //confirmation
    //         include 'reservation_data_template_confirmation.php';
    //         echo "<script>console.log(".json_encode($arrDataTemplate).");</script>"; 

    //         ob_start();
    //         $template = new HotelEmailTemplate($db,'confirmation','guest',$arrDataTemplate);
    //         $html_template = $template->getTemplate();
    //         echo $html_template;
    //         $body = ob_get_clean();
            
    //         ob_start();
    //         $template = new HotelEmailTemplate($db,'confirmation','agent',$arrDataTemplate);
    //         $html_template = $template->getTemplate();
    //         echo $html_template;
    //         $body_send_to_agent = ob_get_clean();

    //         ob_start();
    //         $template = new HotelEmailTemplate($db,'confirmation','hotel',$arrDataTemplate);
    //         $html_template = $template->getTemplate();
    //         echo $html_template;
    //         $body_send_to_hotel = ob_get_clean();
            
    //         // die();
            
    //         include('reservation_sending_template_confirmation_guest.php');
    //         break;
    // }


?>