<?php

	ob_start();
	include 'mail-content/mail-contact-us.php';
	$body = ob_get_clean();

	require_once('includes/mailer/class.phpmailer.php');
	$mail = new PHPMailer(true); // the true param means it will throw exceptions on errors, which we need to catch
	$mail->IsSMTP(); // telling the class to use SMTP
	try {

		$sql_mailer = "SELECT * FROM mailer INNER JOIN mailertype USING (mailertypeoid) INNER JOIN mailermail USING (mailermailoid) WHERE mailerpage = 'contact-us' AND status='1'";
		$run_mailer = mysqli_query($conn, $sql_mailer);
		
		while($row_mailer=mysqli_fetch_array($run_mailer)){
			switch($row_mailer['mailertypeoid']){
				case 1 : $mail->SetFrom($row_mailer['email'], $row_mailer['name']); break;
				case 2 : $mail->AddAddress($row_mailer['email'], $row_mailer['name']); break;
				case 3 : $mail->AddCC($row_mailer['email'], $row_mailer['name']); break;
				case 4 : $mail->AddBCC($row_mailer['email'], $row_mailer['name']); break;
				case 5 : $mail->AddReplyTo($row_mailer['email'], $row_mailer['name']); break;
			}
		}
		$mail->AddAddress($recipient_email, $recipient_name);
	
		
		$mail->Subject = '[Contact Us thebuking.com] '.$category;
		$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!'; // optional - MsgHTML will create an alternate automatically
		$mail->MsgHTML($body);
	
		if($mail->Send()) {
			echo"<script>document.location.href='".$base_url."/contact-us/confirmation';</script>";
		}
		
    } catch (phpmailerException $e) { echo"<b>Php Mailer Error :</b><br>"; echo $e->errorMessage();  echo "<br>"; echo $e->getMessage();//Pretty error messages from PHPMailer
	} catch (Exception $e) { echo"<b>All Error :</b><br>"; echo $e->getMessage(); //Boring error messages from anything else!
	} 
?>