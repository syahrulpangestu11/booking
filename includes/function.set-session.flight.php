<?php
function randomToken($length) {
    $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    $randomToken = '';
    for ($i = 0; $i < $length; $i++) { $randomToken .= $characters[rand(0, strlen($characters) - 1)]; }
    return $randomToken;
}

if(!isset($_SESSION['tokenSession'])){
	$setSession = randomToken(30);
	
	$_SESSION['tokenSession'] = $setSession;
	$_SESSION['tokenTime'] = time();
	$_SESSION['tokenExpire'] = $_SESSION['tokenTime'] + 1200;
	
	$insert = "";
}else{
	$insert = "";
}

$searchlink = (isset($_REQUEST['searchurl']) and $_REQUEST['searchurl'] != "") ? $_REQUEST['searchurl'] : $base_url."/search/flight/";
?>
<script>
	$(function(){
		window.setInterval(function() {
			$.get("<?=$base_url?>/includes/session.check.flight.php", function(data) {
				if(data == "expired"){
					alert("Your session has expired.");
					document.location.href = "<?=$searchlink?>";
				}
			});
		}, 5000);
	});
</script>