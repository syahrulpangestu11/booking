<?php
/*----------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
	$array_customer = array(':title' => $_SESSION['title'], 
		':firstname' => $_SESSION['firstname'], 
		':lastname' => $_SESSION['lastname'], 
		':email' => $_SESSION['email'], 
		':address' => $_SESSION['address'], 
		':city' => $_SESSION['city'], 
		':state' => $_SESSION['state'], 
		':zipcode' => $_SESSION['zipcode'], 
		':countryoid' => $_SESSION['countryoid'], 
		':phone' => $_SESSION['phone'], 
		':mobilephone' => $_SESSION['mobilephone']);
	Func::logJS($array_customer);
	$scari_cust="SELECT count(custoid) as jmlcust, custoid FROM `customer` WHERE `title` = :title  
					AND `firstname` = :firstname  AND `lastname` = :lastname AND `email` = :email   
					AND `address` = :address AND `city` = :city  AND `state` = :state AND `zipcode` = :zipcode  
					AND `countryoid` = :countryoid AND `phone` = :phone  AND `mobilephone` = :mobilephone ";

	$cari_cust = $db->run($scari_cust, $array_customer)->fetch();

	$custoid = $cari_cust['custoid'];
	if($cari_cust['jmlcust']==0){
		$syntaq_save_guest="INSERT INTO `customer` 
								(`title`, `firstname`, `lastname`, `email`, `phone`, `mobilephone`, `address`, `city`, `state`, `countryoid`, `zipcode`,`password`) 
							VALUES (:title, :firstname, :lastname, :email, :phone, :mobilephone, :address, :city, :state, :countryoid, :zipcode, '')";

		$query_save_guest = $db->run($syntaq_save_guest, $array_customer)->rowCount();
		$custoid = $db->lastInsertId();
	}


	/*----------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
	$insert_bookingmst = "INSERT INTO booking (`bookingnumber`, `bookingtime`, `custoid`, `currencyoid`, `grandtotal`, `grandtotalr`, `granddeposit`, `note`, 
						`arrivaldate`, `arrivaltime`, `bookingstatusoid`, `agentoid`, `updated`, `updatedby`, `hoteloid`) 
					VALUES (:bookingnumber, :bookingtime, :custoid,  :currencyoid, :grandtotal, :grandtotalr, :granddeposit, :note, 
							:arrivaldate, :arrivaltime, :bookingstatusoid, :agentoid, :updated, :updatedby, :hoteloid)";
	
	$array_booking = array(':bookingnumber' => $bookingnumber, 
			':bookingtime' => $bookingtime, 
			':custoid' => $custoid, 
			':currencyoid' => $_SESSION['currencytotal'], 
			':grandtotal' => $_SESSION['grandtotals'], 
			':grandtotalr' => $_SESSION['grandtotalrs'],
			':granddeposit' =>$_SESSION['grandtotaldeposits'], 
			':note' => $_SESSION['note'], 
			':arrivaldate' => $arrivaldate, 
			':arrivaltime' => $_SESSION['arrivaltime'], 
			':bookingstatusoid' => $bookingstatus, 
			':agentoid' => '', 
			':updated' => $bookingtime, 
			':updatedby' => '', 
			':hoteloid' => $_SESSION['booking_hoteloid']);
	Func::logJS($array_booking);
	$query_bookingmst = $db->run($insert_bookingmst, $array_booking)->rowCount();
	Func::logJS($query_bookingmst);	
	$bookingoid = $db->lastInsertId();
	

	/*----------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

	if($pagesuccess!='inquiry'){
		if($_POST['payment_method'] == "sof"){
			$card = $db->run("SELECT cardoid from creditcard where cardcode='".$_POST['cardtype']."'")->fetch();

			// $insert_paydtl="INSERT into bookingpayment (`bookingoid`, `type`, `cardoid`, `cardnumber`, `cardholder`, `expmonth`, 
			// 				`expyear`, `paymentaddress`, `paymentcity`, `paymentstate`, `countryoid`) 
			// 			values ('".$bookingoid."', '".$_POST['payment_method']."','".$card['cardoid']."', '".$_POST['cardnumber']."', 
			// 			'".$_POST['cardholder']."', '".$_POST['month']."', '".$_POST['year']."', '".$_POST['address_cc']."', '".$_POST['city_cc']."', 
			// 			'".$_POST['state_cc']."', '".$_POST['country_cc']."')"; 

			$insert_paydtl2="INSERT into bookingpayment  
				(`bookingoid`, `type`, `cardoid`, `cardnumber`, `cardholder`, `expmonth`, `expyear`, `paymentaddress`, `paymentcity`, `paymentstate`, `countryoid`)  
				values (:bookingoid, :payment_method, :cardoid, :cardnumber, :cardholder, :expmonth, :expyear, :paymentaddress, :paymentcity, :paymentstate, :countryoid)";

			$array_credicard = array(
				':bookingoid' => $bookingoid, 
				':payment_method' => $_POST['payment_method'], 
				':cardoid' => $card['cardoid'], 
				':cardnumber' => $_POST['cardnumber'], 
				':cardholder' => $_POST['cardholder'], 
				':expmonth' => $_POST['month'], 
				':expyear' => $_POST['year'], 
				':paymentaddress' => $_POST['address_cc'], 
				':paymentcity' => $_POST['city_cc'], 
				':paymentstate' => $_POST['state_cc'], 
				':countryoid' => $_POST['country_cc']
			);

			$query_paydtl2 = $db->run($insert_paydtl2,$array_credicard)->rowCount();
		}else{
		// 	//$insert_paydtl="insert into bookingpayment (`bookingoid`, `type`, `titleoid`, `paymentfirstname`, `paymentlastname`, `paymentemail`, `paymentphone`, `paymentmobile`, `note`, `userklikbca`) values ('".$bookingoid."', '".$_POST['payment_method']."', '".$_POST['title']."', '".$_POST['firstname']."', '".$_POST['lastname']."', '".$_POST['email']."', '".$_POST['phone']."', '".$_POST['mobilephone']."', '".$_POST['note']."', '".$_POST['userkbca']."')"; 
		// 	// $insert_paydtl="INSERT into bookingpayment (`bookingoid`, `type`, `note`, `userklikbca`) VALUES ('".$bookingoid."', '".$_POST['payment_method']."', '".$_POST['note']."', '".$_POST['userkbca']."')"; 
			$insert_paydtl = "INSERT into bookingpayment (`bookingoid`, `type`, `note`) VALUES (:bookingoid, :type, :note)";
			$query_paydtl = $db->run($insert_paydtl, array(':bookingoid' => $bookingoid, ':type' => $_POST['payment_method'], ':note' => $_POST['note']))->rowCount();
		}
	}

	// Func::logJS();
	// die();
?> 