 	<h3 class="section-title"><b>Experiences</b></h3>
	<hr>

<?php
$sql_offers = "SELECT categoryextras.categoryname, extra.price, extra.picture, extra.description, extra.name FROM extra INNER JOIN categoryextras ON extra.categoryname=categoryextras.categoryname";

$run_offers = mysqli_query($conn, $sql_offers) or die(mysqli_error());
?>

<div class="card-wrapper">
	<!-- Slider main container -->
	<div class="swiper-container card-container-v2">
		<!-- Additional required wrapper -->
		<div class="swiper-wrapper">
			<!-- Slides -->
			<?php
			// foreach ($images as $key => $value) {
			while($row_offers = mysqli_fetch_array($run_offers)){
				?>
				<div class="swiper-slide">
					<div class="card-rounded card-v2">
						<div class="thumbnailz thumb-sm">
							<img src="<?=$row_offers['picture'];?>">
						</div>
						<div class="card-content">
							<P><?=$row_offers['categoryname'];?></P>
							<h4 class="card-title-container"><?=$row_offers['name'];?></h4>
							<p><?=$row_offers['description'];?></p>
							<div class="card-label">Rp.<?=$row_offers['price'];?></div>
						</div>
					</div>
   				</div>
				<?php 
			} ?>
		</div>
		<!-- If we need pagination -->
		<div id="card-v4-pagination" class="swiper-pagination hidden-sm hidden-xs"></div>
	</div>

</div>