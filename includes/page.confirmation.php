<?php
    $bookingnumber = $_REQUEST['bookingnumber'];
    $paymentmethodoid = $_REQUEST['paymentmethodoid'];
    $bookingstatusoid = $_REQUEST['bookingstatusoid'];;
    
    if(isset($_SESSION['_progress_booking'])){
        ?>
        <div id="pgsch" class="container" style="min-height:500px;">
            <div class="white-box border-box card-rounded top-right-box">
                <h2 style="margin-bottom:15px;">Thank You, <span class="blue"><?=$_SESSION['_agent_initial'];?></span></h2>
                We already submit your reservation with booking number <?=$bookingnumber;?>. Please check Your Email for futher information.
                
                <?php if($bookingstatusoid==3){?>
                    <!-- <div><br>
                        <b>Attention</b><br />
                        <p>Please upload the payment proof by click the link below here. Thanks</p>
                        <a href="<?=$base_url;?>/reservation/confirmation/?no=<?=$bookingnumber;?>">Upload Payment Proof</a>
                    </div> -->
                <?php } ?>

                <p style="margin-bottom:50px;">
                    <a href="<?=$base_url;?>"><h4><i class="fa fa-angle-left"></i>&nbsp;&nbsp;  Back</h4></a>
                </p>
                
            </div>
        </div>
<?php 
    
	    unset($_SESSION['_progress_booking']);
    }else{
        echo"<script>window.location.href = '".$base_url."/'</script>";
        die();
    }
?>