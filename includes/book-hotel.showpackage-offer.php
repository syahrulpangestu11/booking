<?php
$s_offer = "select offeroid, offerroomoid from specialoffer  inner join hotel using (hoteloid) inner join specialroom using (offeroid) inner join room using (roomoid) inner join specialrate using (offerroomoid) where room.roomoid = '$roomoid' and specialoffer.publishedoid = '1' and (specialrate.startbook<='$todaydate' and specialrate.endbook>='$todaydate') and (specialrate.startdate<='$checkin' and specialrate.enddate>='$checkout') group by offeroid, offerroomoid";
$q_offer = mysqli_query($conn, $s_offer) or mysqli_error();
while($offer = mysqli_fetch_array($q_offer)){
	$offeroid = $offer['offeroid']; $offerroomoid = $offer['offerroomoid']; $total = 0;
	for($n=0;$n<$night;$n++){
		include('function.date.php');
		$s_rate_offer = "select offerrateoid, multipricetypeoid from specialrate inner join published using (publishedoid) inner join specialroom using (offerroomoid) where specialroom.offerroomoid = '$offerroomoid' and (specialrate.startbook<='$todaydate' and specialrate.endbook>='$todaydate') and (specialrate.startdate<='$checkin' and specialrate.enddate>='$checkout') order by priority limit 1";
		$q_rate_offer = mysqli_query($conn, $s_rate_offer) or mysqli_error(); $rate_offer = mysqli_fetch_array($q_rate_offer);
		$offerrateoid = $rate_offer['offerrateoid']; $multipricetypeoid = $rate_offer['multipricetypeoid'];
		
		$s_market_price = "select price, discount, marketoid, currency.currencyoid from multiprice inner join market using (marketoid) inner join currency using (currencyoid) where typeid = 'offer' and idreference = '$offerrateoid' and multipricetypeoid = '$multipricetypeoid' limit 1";
		/*inner join marketcountry using (marketoid) inner join country using (countryoid) */
		$q_market_price = mysqli_query($conn, $s_market_price) or mysqli_error(); $market_price = mysqli_fetch_array($q_market_price);
		if(!empty($market_price['price']) or $market_price['price'] > 0){
			$rate = $market_price['price']; $ratecurrency = $market_price['currencyoid'];
			include("convertcurrency.php");
		}else{
			include('function.rate-bar.php');
			$discount = $market_price['percent']; $discountprice = $rate * $discount / 100; $rate = $rate - $discountprice;
		}
		$total = $total + $rate;
	}
	if($total > 0){
		$countrate++;
		include('function.specialoffer-content.php');
		$totalpernight = $total / $night;
?>
	<li class="table-row">
		<div class="table-cell top">
			<span class="title"><?=$room_name;?> - <?=$name_specialoffer;?></span>
			<div class="thumb pict"><img src="<?=$room_pict;?>"></div>
			<a href="#">room info</a>
		</div>
		<div class="table-cell top center">
			<?php
			for($i=1;$i<=$room_adult;$i++){ ?>
				<img src="<?=$base_url;?>/images/adult.png">
				<?php
			}
			for($i=1;$i<=$room_child;$i++){ ?>
				<img src="<?=$base_url;?>/images/child.png">
				<?php
			}
			?>
		</div>
		<div class="table-cell top center">
			<h3><span class="grey"><?php echo $currencycode;  ?></span> <span class="blue"><?php echo number_format($totalpernight); ?></span></h3>
		</div>
		<div class="table-cell top center">
			<input type="hidden" name="roomoid" id="roomoid" value="<?php echo $roomoid; ?>">
			<input type="hidden" name="offerroomoid" id="offerroomoid" value="<?php echo $offerroomoid; ?>">
			<select room="number">
				<?php for($r=0;$r<=$default_allotment;$r++) { ?>
				<option value="<?php echo $r; ?>"><?php echo $r; ?></option>
				<?php } ?>
			</select>
			<div></div>
		</div>
	</li>
<?php
	}
} 
?>