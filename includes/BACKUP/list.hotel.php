<?php /*
<!-- jPagination -->
<script type="text/javascript" language="javascript" src="<?=$base_url;?>/scripts/jpaging.js"></script>
<input type='hidden' id='current_page' />
<input type='hidden' id='show_per_page' />
<!-- -------------------- -->	
*/ ?>


<ul class="content-list white-box" id="list-hotel">
	<?php
	
	$s_hotel="SELECT * FROM hotel INNER JOIN city USING (cityoid)  LIMIT 4";	
	
	$q_hotel=mysqli_query($conn, $s_hotel.$limit) or die("SQL Error : ".$s_hotel);
	$num_hotel=mysqli_num_rows(mysqli_query($conn, $s_hotel));
	while($hotel=mysqli_fetch_array($q_hotel)){
		
		$pict = ($hotel['picture'] != "") ? $hotel['picture'] : $base_url."/images/default-thumb.png";
		$hoteloid = $hotel['hoteloid'];
		$hotel_name = $hotel['hotelname'];
		$hotel_city = $hotel['cityname'];
		$hotel_address = $hotel['address'];
		$hotel_stars = $hotel['stars'];
		?>
			<li class="border-box">
			<a href="" class="clear">
				<div class="pict inline-block top thumb">
					<img src="<?=$pict;?>">
				</div>
				
				<div class="info inline-block top">
					<div><?=$hotel_name;?></div>
					<div><?=$hotel_city;?></div>
				</div>
				<div class="info2 inline-block right">	
					<div class="star">
						<?php
						if($hotel_stars != "" ){
							for($i=1;$i<=$hotel_stars;$i++){ ?>
								<img src="<?=$base_url;?>/images/star.png">
								<?php
							}
						}else{
							?><style> .star{display:none;}</style><?php
						}
						?>
					</div>
					<div class="price">
						mulai dari 
						<?php include('minrate.hotel.php'); ?>
						<h2><?php echo $currencycode; ?> <span class="maroon"><?php echo number_format($minrate);  ?></span></h2>
					</div>
				</div>
			
				
			</a>
			</li>
			<?php
		}
		?>
	<div id='page_navigation' class="paging" style="<?=$style1.$style2;?>"></div>

</ul>