<ul class="content-list">
	<?php
		$sql_destination = "SELECT stateoid, statename FROM state INNER JOIN city USING (stateoid) INNER JOIN hotel USING (cityoid) GROUP BY stateoid ORDER BY rand() LIMIT 8";
		$run_destination = mysqli_query($conn, $sql_destination) or die(mysqli_error());
		while($row_destination = mysqli_fetch_array($run_destination)){
			$cityname = $row_destination['statename'];
			$citypict = $row_destination['statepict'];
			?>
			<li class="one_quarter white-box border-box">
				<div class="pict thumb"><img src="<?=$citypict;?>" alt=""></div>
				<div class="label"><?=$cityname;?></div>
			</li>
			<?php 
		}
		?>
</ul>