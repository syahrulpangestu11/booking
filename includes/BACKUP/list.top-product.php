<?php
$s_product="select productcontent.name, product.productoid, product.productpict, getpriceact, productcontent.description, productcontent.facilities, productcontent.headline, cityname from product  inner join city using (cityoid) inner join productcontent using (productoid) where publishedoid ='1' and (product.startdate<='$todaydate' and product.enddate>='$todaydate') AND (productcontent.headline IS NOT NULL AND productcontent.headline <> '') group by productoid ORDER BY rand() LIMIT 4";
$q_product = mysqli_query($conn, $s_product) or die(mysqli_error());
?>
<ul class="content-list" id="product">
	<?php
	while($product = mysqli_fetch_array($q_product)){
		$productoid = $product['productoid']; $name = $product['name']; $headline = $product['headline']; $pict = $product['productpict']; $price = "IDR 200K";
		$priceact = $product['getpriceact'];
		include("minrate.product.php");
	?>
		<li class="inline-block top white-box border-box">
			<div class="thumb pict"><img src="<?=$pict;?>"></div>
			<div class="title center border-box"><?=$name;?></div>
			<div class="small-desc"><?=$headline;?></div>
			<div class="border-box center">
				<div class="price inline-block maroon"><?=$showprice;?></div>
				<a href="#" class="button inline-block">BOOK</a>
			</div>
		</li>
	<?php 
	}
	?>
</ul>