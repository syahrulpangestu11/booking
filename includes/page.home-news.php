<?php
	$s_news = "select n.newsoid, n.title, n.headline, n.pict_thumbnail from news n inner join published p using (publishedoid) where publishedoid = '1' order by created";
	$q_news = mysqli_query($conn, $s_news);
	while($news = mysqli_fetch_array($q_news)){
		$encoded_title = str_replace('%2F', '_', urlencode(str_replace(" ", "-", strtolower($news['title']))));
		$href_news = $base_url."/news/".$encoded_title."-".$news['newsoid'];
?>
        <div>
            <img class="inline-block middle" style="width:100px; height:100px;" src="<?php echo $news['pict_thumbnail']; ?>" />
            <div class="inline-block middle detail">
                <div><?php echo $news['title']; ?></div>
                <div><?php echo $news['headline']; ?></div>
                <div><a href="<?php echo $href_news; ?>">Read More</a></div>
            </div>
        </div>
<?php
	}
?>