 	<h3 class="section-title"><b>Destination</b> to Pick</h3>
	<hr>
<?php
$sql_destination = "SELECT cityoid AS oid, cityname AS name, citypict AS pict, count(cityoid) as jml
					FROM city c INNER JOIN hotel h USING (cityoid) GROUP BY cityoid ORDER BY jml DESC LIMIT 8;";
$run_destination = mysqli_query($conn, $sql_destination) or die(mysqli_error());
/*/ 
while($row_destination = mysqli_fetch_array($run_destination)){
	$destination_name = $row_destination['name'];
	$destination_pict = $row_destination['pict'];
		$s_counthotel = "SELECT count(h.hoteloid) as jmlhotel from hotel h inner join published p using (publishedoid) inner join city c using (cityoid) inner join state s using (stateoid) where s.stateoid='".$row_destination['oid']."' and p.publishedoid='1'";
		$r_counthotel = mysqli_query($conn, $s_counthotel) or die(mysqli_error());
		$counthotel = mysqli_fetch_array($r_counthotel);
		
	?>
	<div class="col-md-6 col-xs-12 white-box border-box">
		<div class="pict thumb"><img src="<?=$destination_pict;?>"></div>
		<h3><?=$destination_name;?></div>
		<div><?php if($counthotel['jmlhotel']>0){ echo $counthotel['jmlhotel']." hotels"; } ?></div>
	</div>
	<?php 
}
//*/ 
?>

<div class="card-wrapper">
	<!-- Slider main container -->
	<div class="swiper-container card-container-v1">
		<!-- Additional required wrapper -->
		<div class="swiper-wrapper">
			<!-- Slides -->
			<?php
			/*/ 
			$images = array(
				'Bali' => 'https://images.unsplash.com/photo-1532186651327-6ac23687d189?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=478958d80dcf773693ba21349b42fa15&auto=format&fit=crop&w=687&q=20',
				'Magelang' => 'https://images.unsplash.com/photo-1512567522909-4af064e392aa?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=04f1d0f1a6c1399f4d7e180b9e7ccb76&auto=format&fit=crop&w=1350&q=20',
				'Malang' => 'https://images.unsplash.com/photo-1528214968864-8dd00782fa9e?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=c5de3a8afe2491fd4203530218430201&auto=format&fit=crop&w=1392&q=20',
				'Padar Island' => 'https://images.unsplash.com/photo-1539087299082-cc311701feee?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=f07c2cc8c84355850369b1eff115078c&auto=format&fit=crop&w=1350&q=20',
				'Surabaya' => 'https://images.unsplash.com/photo-1529563021893-cc83c992d75d?ixlib=rb-0.3.5&s=3ea956c8ef1d5c6c69bdb1428966a7c9&auto=format&fit=crop&w=634&q=20',
				'Komodo' => 'https://images.unsplash.com/photo-1521791772602-45ed00c1226f?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=6f2a221508ef8c827a3e86ee69f88e16&auto=format&fit=crop&w=2133&q=20',
				'Jakarta' => 'https://images.unsplash.com/photo-1540293923757-f737135a202f?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=0f38ad9414a5321898bd470a5443fedb&auto=format&fit=crop&w=564&q=20',
			);
			//*/ 
			// foreach ($images as $key => $value) {
			while($row_destination = mysqli_fetch_array($run_destination)){
				$url_destination = $base_url."/search/hotel/?q=".urlencode($row_destination['name']);
				?>
				<div class="swiper-slide">
					<a href="<?=$url_destination;?>" rel="noopener noreferrer">
					<div class="card-v1 card-rounded text-float-container ">
						<div class="thumbnailz">
								<img src="<?=$row_destination['pict'];?>" alt="<?=$row_destination['name'];?>" title="<?=$row_destination['name'];?>">
						</div>
						<div class="text-float-centered card-title-container"><?=$row_destination['name'];?></div>
					</div>
					</a>
				</div>
				<?php 
			} ?>
		</div>
		<!-- If we need pagination -->
		<div id="card-v1-pagination" class="swiper-pagination hidden-sm hidden-xs"></div>
	</div>

	<div id="card-v1-button-prev" class="card-button-prev hidden-sm hidden-xs"><i class="fa fa-2x fa-angle-left"></i></div>
	<div id="card-v1-button-next" class="card-button-next hidden-sm hidden-xs"><i class="fa fa-2x fa-angle-right"></i></div>
</div>