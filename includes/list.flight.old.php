<ul class="content-list" id="promo-flight">
	<div class="clear white-box" id="departure">
		Dari 
		<select name="">
			<?php
			//$sql2="SELECT DISTINCT city.* FROM hotel INNER JOIN city USING(cityoid) ORDER BY cityname"; 
			$sql2="SELECT stateoid AS oid, statename AS name, statepict AS pict FROM state WHERE statepict <> ''";
			$run2=mysqli_query($conn, $sql2) or die($sql."<br><br>".mysqli_error());
			while($row2=mysqli_fetch_array($run2)){
				$selected = ($destination==$row2['name']) ? " selected " : "" ;
				?>
				<option value="<?=$row2['name'];?>" <?=$selected;?>><?=$row2['name'];?></option>
				<?php
			} ?>
		</select>
	</div>
	<?php
	$sql_flight = "SELECT stateoid AS oid, statename AS name, statepict AS pict FROM state WHERE statepict <> '' ORDER BY rand() LIMIT 8";
	$run_flight = mysqli_query($conn, $sql_flight) or die(mysqli_error());
	while($row_flight = mysqli_fetch_array($run_flight)){
		$flight_name = $row_flight['name'];
		$flight_pict = $row_flight['pict'];
		?>
		<li class="white-box border-box inline-block top">
			<div class="pict thumb inline-block top"><img src="<?=$flight_pict;?>" alt=""></div>
			<div class="desc inline-block top">
				<div><?=$flight_name;?></div>
				<div>from <h3>IDR <span class="blue rate">245<span class="thousand">,000</span></span></h3></div>
			</div>
		</li>
		<?php 
	}
	?>
</ul>