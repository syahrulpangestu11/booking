<?php
include('../conf/connection.php');

	$roomoid = $_GET['roomoid'];
	$roomofferoid = $_GET['roomofferoid'];
	$channeloid = $_GET['channeloid'];
	$jmlroom = $_GET['jmlroom'];
	$adult = $_GET['adult'];
	$checkin = date('Y-m-d', strtotime($_GET['checkin']));
	$eq = explode('-', $checkin);
	$d_date = $eq[2]; $m_date = $eq[1]; $y_date = $eq[0];
	$minyear = date('Y');
	$min_night = 1;
	$night = $_GET['night'];

	$promoapply = $_GET['promoapply'];
	$type = $_GET['type'];

	$extrabed = $_GET['eb'];

	if((!empty($promoapply) or $promoapply != 0) and $type == 'promoapply'){
		$s_promotion = "select p.minstay, r.adult, r.child from promotion p
		inner join promotiontype pt using (promotiontypeoid)
		inner join discounttype dt using (discounttypeoid)
		inner join hotel h using (hoteloid)
		inner join promotionapply pa using (promotionoid)
		inner join roomoffer ro using (roomofferoid)
		inner join offertype ot using (offertypeoid)
		inner join room r using (roomoid)
		inner join channel ch using (channeloid)
		where pa.promotionapplyoid = '".$promoapply."'";
	}elseif((!empty($promoapply) or $promoapply != 0) and $type == 'packageapply'){
		$s_promotion = "select r.adult, r.child from package p
		inner join discounttype dt using (discounttypeoid)
		inner join hotel h using (hoteloid)
		inner join packageapply pa using (packageoid)
		inner join roomoffer ro using (roomofferoid)
		inner join offertype ot using (offertypeoid)
		inner join room r using (roomoid)
		inner join channel ch using (channeloid)
		where pa.packageapplyoid = '".$promoapply."'";
		// echo($s_promotion);
	}else{
		$s_promotion = "select '1' as minstay, r.adult, r.child from hotel h
		inner join room r using (hoteloid)
		inner join roomoffer ro using (roomoid)
		inner join offertype ot using (offertypeoid)
		where r.roomoid = '".$roomoid."'";
	}
	$q_promotion = mysqli_query($conn, $s_promotion) or mysqli_error();
	$promotion = mysqli_fetch_array($q_promotion);
	if($type == 'packageapply'){
	$min_night = '1';
	}else{
	$min_night = $promotion['minstay'];
	}
	$adultpax = $promotion['adult'];
	$childpax = $promotion['child'];
	$select_adult = ($adultpax >= $adult ? $adult : 1);

for($r=1;$r<=$jmlroom;$r++){
	if(!isset($p)){$p=$r;}
?>
<div class="each-room-detail">
	<div class="title left"><b>Room <?php echo $r; ?> :</b></div>
	<div class="clear left">
		<span>Check In :</span>
		 <div>
		 	<select name="day-<?php echo "$r"; ?>" id="day-<?php echo "$r"; ?>" dayc="dayc" class="room_day room_input" old-data="<?=(int)$d_date;?>">
				<?php  for($h=1; $h<=31; $h++){ if($h==$d_date){ $selected="selected='selected'"; }else{ $selected=""; } echo "<option value='".$h."' ".$selected.">".$h."</option>"; } ?>
			</select>
			<select name="month-<?php echo "$r"; ?>" id="month-<?php echo "$r"; ?>" monthc="monthc"  class="room_month room_input" old-data="<?=(int)$m_date;?>">
				<?php
					$months = array(1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr', 5 => 'May', 6 => 'Jun', 7 => 'Jul', 8 => 'Aug', 9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec');
					for($i=1; $i<=12; $i++){ if($i==$m_date){ $selected="selected='selected'"; }else{ $selected=""; } echo "<option value='".$i."' ".$selected.">".$months[$i]."</option>"; }
				?>
			</select>
			<select name="year-<?php echo "$r"; ?>" id="year-<?php echo "$r"; ?>" yearc="yearc" class="room_year room_input" old-data="<?=(int)$y_date;?>">
				<?php for($i=$minyear; $i<=($minyear+2); $i++){ if($i==$y_date){ $selected="selected='selected'"; }else{ $selected=""; } echo "<option value='".$i."' ".$selected.">".$i."</option>"; } ?>
			</select>
		</div>
	</div>
	<div class="clear left">
		<div class="inline-block">
			<span>Night :</span>
			<div>
				<select name="<?php echo "night-$p"; ?>" id="<?php echo "night-$p"; ?>" nightc="nightc" class="room_night room_input" old-data="<?=$night;?>">
					<?php
					for($n=$min_night;$n<=30;$n++){
						if($n == $night){ $selected = "selected"; }else{ $selected = ""; }
					?>
						<option value="<?php echo $n; ?>" <?php echo $selected; ?>><?php echo $n; ?></option>
					<?php
					}
					?>
				</select>
			</div>
		</div>
		<div class="inline-block">
			<span>Adult :</span>
			<div>
				<select name="<?php echo "adult-$p"; ?>" id="<?php echo "adult-$p"; ?>" adult="adultc" class="room_adult room_input" old-data="<?=$select_adult;?>">
					<?php
					for($a=0; $a<=$adultpax; $a++){
						//if($a == 0){ $selected = "selected"; }else{ $selected = ""; }
						if($a == $select_adult){ $selected = "selected"; }else{ $selected = ""; }
					?>
						<option value="<?php echo $a; ?>" <?php echo $selected; ?> ><?php echo $a; ?></option>
					<?php
					}
					?>
				</select>
			</div>
		</div>
		<div class="inline-block">
			<span>Child :</span>
			<div>
				<select name="<?php echo "child-$p"; ?>" id="<?php echo "child-$p"; ?>" child="childc" class="room_child room_input" old-data="0">
					<?php
					for($c=0; $c<=$childpax; $c++){
						if($c==0){ $selected = "selected"; }else{ $selected = ""; }
					?>
						<option value="<?php echo $c; ?>" <?php echo $selected; ?>><?php echo $c; ?></option>
					<?php
                    }
					?>
				</select>
			</div>
		</div>
        <?php if($extrabed == 1){ ?>
		<div class="inline-block">
			<input type="checkbox" name="extrabed" value="1" class="extrabed room_extrabed room_input" old-data="0"/>
			<div class="inline-block">Extra Bed</div>
		</div>
        <?php } ?>
        <div class="">
			<a class="button edit" btn="edit" style="margin-top: 5px;background:red;padding: 5px 10px;display:none;"><i class="fa fa-edit"></i>&nbsp;Update Price</a>
			<!-- <img src="https://thebuking.com/dev/travelandtrip/images/loading-mini.gif" style="display:none; width:24px; height:24px;"> -->
			<i class="color-red fa fa-spinner fa-pulse" style="visibility:hidden;"></i>
		</div>
	</div>
</div>
<?php
}
?>
