<?php
function miniThousand($rate) {
	if(strlen($rate) > 5){
		$rate_first = substr($rate, 0, strlen($rate)-4);
		$rate_second = substr($rate, -4);
		$rate_final = $rate_first."<span class='thousand'>".$rate_second."</span>"; 
	}else{
		$rate_final = $rate;
	}
	return $rate_final;
}
?>