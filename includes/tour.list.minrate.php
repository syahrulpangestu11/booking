<?php
//echo "- type : ".$tourtype." / pricing : ".$tourpricing."-<br>";
if($tourtype == "1"){ // normal package
	$s_minrate = "select netrate, tnp.taxoid, c.currencyoid, c.currencycode, thr.pax from tournormalprice as tnp inner join currency as c using (currencyoid) inner join tax as tx using (taxoid) inner join tournormal as tn using (tournormaloid) inner join tourhotelroom as thr using (tourhotelroomoid) inner join tourhotel as th using (tourhoteloid) inner join tour as t using (touroid) where t.touroid = '".$touroid."' and (  (tn.startbook<='$todaydate' and tn.endbook>='$todaydate') or tn.startbook>='$todaydate') order by netrate asc limit 1";
	$q_minrate = mysqli_query($conn, $s_minrate) or die(mysqli_error()); $minrate = mysqli_fetch_array($q_minrate);
	$ratecurrency = $minrate['currencyoid'];
	if($tourpricing == "1"){
		$rate = $minrate['netrate']; 
	}else if($tourpricing == "2"){
		$getpackage = ceil($adult / $minrate['pax']);
		$rate = ($minrate['netrate'] * $getpackage) / $adult;
	}
}else if($tourtype == "2"){ // group series package
	$s_minrate = "select netrate, tgp.taxoid, c.currencyoid, c.currencycode from tourgroupprice as tgp inner join currency as c using (currencyoid) inner join tax as tx using (taxoid) inner join tourratetype as trt using (tourratetypeoid) inner join tourgroup as tg using (tourgroupoid) inner join tourgroupdate as tgd using (tourgroupoid) inner join  tour as t using (touroid) where t.touroid = '".$touroid."' and (tgd.departdate>='$todaydate') and trt.category = 'adult' order by netrate asc limit 1";
	$q_minrate = mysqli_query($conn, $s_minrate) or die(mysqli_error()); $minrate = mysqli_fetch_array($q_minrate);
	$ratecurrency = $minrate['currencyoid'];
	$rate = $minrate['netrate'];
}else if($tourtype == "3"){ // promotional package
	$s_minrate = "select netrate, tpp.taxoid, c.currencyoid, c.currencycode from tourpromoprice as tpp inner join currency as c using (currencyoid) inner join tax as tx using (taxoid) inner join tourratetype as trt using (tourratetypeoid) inner join tourpromodate as tpd using (tourpromodateoid) inner join tourpromo as tp using (tourpromooid) inner join  tour as t using (touroid) where t.touroid = '".$touroid."' and (  (tpd.startbook<='$todaydate' and tpd.endbook>='$todaydate') or tpd.startbook>='$todaydate') and trt.category = 'adult' order by netrate asc limit 1";
	$q_minrate = mysqli_query($conn, $s_minrate) or die(mysqli_error()); $minrate = mysqli_fetch_array($q_minrate);
	$ratecurrency = $minrate['currencyoid'];
	$rate = $minrate['netrate'];
}
//echo $s_minrate;
//echo "<br>- rate : ".$rate." / currency : ".$ratecurrency."-";
include("convertcurrency.php");
$formated_rate = number_format($rate);
$displayprice = $currencycode." ".$formated_rate;
$displayprice_fnc = $formated_rate;
$displayprice_short = ($currencycode == "IDR") ? substr_replace($formated_rate, "K", -4) : $formated_rate ;
$showprice = $currencycode." ".$displayprice_short;
//echo "<br>- show price : ".$showprice." / display price : ".$displayprice."-";
?>