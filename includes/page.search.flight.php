<div style="position: relative">
<?php
include("parameter.flight.php");
?>

<style>
.hide{display:none;}
#blocking{position:absolute; width:100%; height:2000px; background-color:rgba(255,255,255,0.8); text-align:center; padding-top:50px; z-index:1000;}
#blocking .blocking-box{background-color:#fecb20; display:inline-block; padding:30px; color:#ffffff; font-size:16px;}
.list-resultflight{margin:10px 0; width:100%;}
optgroup{font-style:normal;}
.table .table-row.title{background: #000000;color: #fff;}
.table .table-row .table-cell{padding: 5px 10px; border: 1px solid #ddd; border-top: none; border-right: none;}
.table .table-row .table-cell:last-child{border-right: 1px solid #ddd;}
#flight_choosed{border: 3px solid #004E75;}
#flight_choosed ul{margin:0; padding:0; list-style:none; background-color:rgba(0, 102, 153, 0.16);}
#flight_choosed li{display:inline-block; padding:10px; text-align: center; vertical-align:middle; min-width:15%;}
#flight_choosed li:nth-child(3){min-width:20%;}
#choose_depart > div.small-desc, #choose_return > div.small-desc{padding-top:8px; padding-left:8px; padding-right:8px;}
#choose_total{padding-left:20px;}
</style>

<div id="blocking" class="hide">
	<div class="blocking-box">
	<span>Collecting data,<br>please wait...</span><br>
	<img src="<?=$base_url?>/images/loading2.gif" />
	</div>
</div>

<aside id="sidebar" class="inline-block">
	<div class="white-box border-box" id="search">
		<h3>Your Search</h3>
		<?php include("form.search.flight.php"); ?>
	</div>
	
	<div class="white-box border-box" id="filters">
		<style>
			#filters {position: relative;}
			#filters-overlay {
				position: absolute;
				top: 0; left: 0;
				z-index: 1000;
				width: 100%; height: 100%;
				background: rgba(255,255,255,0.7) url(<?=$base_url;?>/images/square_bg.png);
			}
			#filters-overlay #message-wrapper {position: relative;}
			#filters-overlay #message {
				width: 230px;
				top: 0;
				padding: 10px;
				background-color: #FECB20; color: #fff;
			}
			
		</style>
		
		<div id="filters-overlay" style="visibility: hidden;">
			<div class="center middle sticky-message border-box" id="message">
				<span class="title">
					Retrieving result, <br>
					please wait... <br>
				</span>
				<img id="loading" src="<?=$base_url;?>/images/loading2.gif" />
			</div>
		</div>
		
		<h3>Filter Your Results</h3>
		<div class="filter-wrapper clear border-bottom">
			
		</div>
	</div> <!-- end of .white-box -->
	
</aside>

<div id="right-content" class="inline-block">
	
	<div class="white-box border-box" id="flight_head">
		<div class="clear">
			<div class="flight_title"></div>
			<div class="flight_person" class="inline-block"></div>
			<div class="flight_trip" class="inline-block"></div>
		</div> <!-- end of .clear -->
	</div>
	
	<div class="white-box border-box">
		<div class="clear">
			<div class="fl_left inline-block">
				Found <span class="title-blue" id="num_of_flight">0</span>
				flights
			</div>
			<div class="fl_right inline-block">
				Sort By: 
				<select name="sort_by" id="sort-by">
					<?php
					//$order_by = array("hotelname","hoteltype","stars");
					$order_by = array(
						'priceasc' => "Lowest Price" ,
						'pricedesc' => "Highest Price" , 
						'departureasc' => "Departure Time" ,
						'arrivalasc' => "Arrival Time" , 
					);
					foreach ($order_by as $key => $value) {
						$selected = ($fl_sortflight == $key) ? " selected " : "" ;
						?>
						<option value="<?=$key;?>" <?=$selected;?> ><?=$value;?></option>
						<?php
					}
					?>
				</select>
			</div> <!-- end of .fl_right -->
		</div> <!-- end of .clear -->
	</div>
	
	<form id="guest-form" action="<?=$base_url;?>/book/flight-detail" method="post">
	<div class="white-box border-box" id="flight_choosed" style="display: none">
		<div class="clear">
			<div id="choose_depart"></div>
			<?php if($fl_typeflight == "roundtrip" || $fl_returnflight != ""){?>
			<div class="filter-wrapper clear border-bottom"></div>
			<div id="choose_return"></div>
			<?php }?>
			<div class="filter-wrapper clear border-bottom"></div>
			<h2 id="choose_total"></h2>
		</div> <!-- end of .clear -->
	</div>
	<div>
		<input type="hidden" name="searchurl" value="<?=$base_url."/".$uri2."/".$uri3."/".$uri4?>" />
		
		<input type="hidden" id="d_code" name="d_code" />
		<input type="hidden" id="d_img" name="d_img" />
		<input type="hidden" id="d_alt" name="d_alt" />
		<input type="hidden" id="d_date" name="d_date" />
		<input type="hidden" id="d_flnum" name="d_flnum" />
		<input type="hidden" id="d_from" name="d_from" />
		<input type="hidden" id="d_to" name="d_to" />
		<input type="hidden" id="d_dtime" name="d_dtime" />
		<input type="hidden" id="d_atime" name="d_atime" />
		<input type="hidden" id="d_dur" name="d_dur" />
		<input type="hidden" id="d_cadlt" name="d_cadlt" />
		<input type="hidden" id="d_cchld" name="d_cchld" />
		<input type="hidden" id="d_cinft" name="d_cinft" />
		<input type="hidden" id="d_padlt" name="d_padlt" />
		<input type="hidden" id="d_pchld" name="d_pchld" />
		<input type="hidden" id="d_pinft" name="d_pinft" />
		<input type="hidden" id="d_via" name="d_via" />
		<input type="hidden" id="d_apt" name="d_apt" />
		
		<input type="hidden" id="r_code" name="r_code" />
		<input type="hidden" id="r_img" name="r_img" />
		<input type="hidden" id="r_alt" name="r_alt" />
		<input type="hidden" id="r_date" name="r_date" />
		<input type="hidden" id="r_flnum" name="r_flnum" />
		<input type="hidden" id="r_from" name="r_from" />
		<input type="hidden" id="r_to" name="r_to" />
		<input type="hidden" id="r_dtime" name="r_dtime" />
		<input type="hidden" id="r_atime" name="r_atime" />
		<input type="hidden" id="r_dur" name="r_dur" />
		<input type="hidden" id="r_cadlt" name="r_cadlt" />
		<input type="hidden" id="r_cchld" name="r_cchld" />
		<input type="hidden" id="r_cinft" name="r_cinft" />
		<input type="hidden" id="r_padlt" name="r_padlt" />
		<input type="hidden" id="r_pchld" name="r_pchld" />
		<input type="hidden" id="r_pinft" name="r_pinft" />
		<input type="hidden" id="r_via" name="r_via" />
		<input type="hidden" id="r_apt" name="r_apt" />
	</div>
	</form>
	
	<?php include("list.flight.php"); ?>
	<?php include("js_search.flight.php"); ?>
	<?php
	//tiket.com
	include("flightdata/tiket.com/init.php");
	include("flightdata/tiket.com/js_listcountry.php");
	include("flightdata/tiket.com/js_searchflight.php");
	?>
</div>
</div>