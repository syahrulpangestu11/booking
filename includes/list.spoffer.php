 	<h3 class="section-title"><b>SPECIAL OFFERS</b></h3>
	<hr>
 
<?php

$sql_offers = "select 'promotion' as promotype, saleto, salefrom, promotionoid as oid, promoimage as image, name, hoteloid from promotion union select 'package' as promotype, saleto, salefrom, packageoid as oid, packageimage as image, name, hoteloid from package;";
// $sql_offers = "SELECT hoteloid AS oid, name AS name, promoimage AS pict, count(hoteloid) as jml
// 					FROM promotion p INNER JOIN package a USING (hoteloid) GROUP BY hoteloid ORDER BY jml DESC LIMIT 3;";
$run_offers = mysqli_query($conn, $sql_offers) or die(mysqli_error());
?>

<div class="card-wrapper">
	<!-- Slider main container -->
	<div class="swiper-container card-container-v2 ">
		<!-- Additional required wrapper -->
		<div class="swiper-wrapper">
			<!-- Slides -->
			<?php
			// foreach ($images as $key => $value) {
			while($row_offers = mysqli_fetch_array($run_offers)){
				?>
				<div class="swiper-slide">
					<div class="card-v4 card-rounded-v4 text-float-container">
						<div class="thumbnailz">
							<img src="<?=$row_offers['image'];?>" alt="<?=$row_offers['name'];?>" title="<?=$row_offers['name'];?>">
						</div>
						<div class="text-float-centered-down card-title-container" style="text-align: center;color: white"><?=$row_offers['name'];?>
							<br>
						<div class="text-float-centered-down card-title-container-lg" style="text-align: center;color: white"><?=$row_offers['description'];?>
						</div>
							<a href="#" class="btn btn-primary">Book Now</a>
						</div>
					</div>
   				</div>
				<?php 
			} ?>
		</div>
		<!-- If we need pagination -->
		<div id="card-v4-pagination" class="swiper-pagination hidden-sm hidden-xs"></div>
	</div>

</div>