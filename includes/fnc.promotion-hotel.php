<?php
	error_reporting(E_ALL ^ E_NOTICE);
	include('../conf/connection.php');
	include('../conf/connection_withpdo.php');
	// include('../conf/xmlload.php');

	$todaydate = date("Y-m-d");
	$tknSession = $_GET['tkn'];
	$type = $_GET['type'];
	$qty = $_GET['qty'];
	$stock = $_GET['stock'];
	$extraname = $_GET['extraname'];
	$id = $_GET['promoapply'];
	$delid = $_GET['delid'];
	$adult = $_GET['adult']; $child = $_GET['child'];
	$numroom = $_GET['numroom'];
	$roomoid = $_GET['roomoid'];
	$roomofferoid = $_GET['roomofferoid'];
	$channeloid = $_GET['channeloid'];
	$booked_room = $_GET['jmlroom'];
	$with_extrabed = $_GET['eb'];
	if($type == 'bar'){
		$typeid= $roomofferoid;
	}else if($type == "promoapply" or $type == "packageapply"){
		$typeid= $roomofferoid.'-'.$id;
	}else{
		$typeid = $id;
	}
	if($with_extrabed != 1){ $with_extrabed = 0;	}

	$night = $_GET['night'];
	// $checkin_extra = date('Y-m-d', strtotime($_GET['checkin_extra']));
	$checkin = date('Y-m-d', strtotime($_GET['checkin']));
	if($type == "bar" or $type == "packageapply" or $type == "promoapply"){
	// echo "izza";
	$s_date="select date_add('$checkin', interval $night day) as getDate";
	$q_date=mysqli_query($conn, $s_date) or die ("SQL Error : ".$s_date);
	$a_date=mysqli_fetch_array($q_date);

	$list_allotment = array();
	$checkout=$a_date['getDate'];
	}	
	
	// echo "<script>console.log(\"GET-adult:". $_GET['adult']."\");</script>";
	// echo "<script>console.log(\"GET-extraname:". $_GET['extraname']."\");</script>";	
	if($type == "bar"){
		$s_promotion = "select '0' as promotionoid, '0' as discounttypeoid, '0' as discountapplyoid, '0' as applyvalue, '0' as discountvalue, h.hoteloid,
		'".$channeloid."' as channeloid,
		ro.roomofferoid, ro.roomoid, ro.offertypeoid,
		r.adult, r.child,h.hotelcode, h.hotelname, r.name as roomname
		from hotel h
		inner join room r using (hoteloid)
		inner join roomoffer ro using (roomoid)
		inner join offertype ot using (offertypeoid)
		where
		r.publishedoid = '1' and ro.roomofferoid = '".$roomofferoid."'
		group by ro.roomofferoid
		";
	}elseif($type == "packageapply"){
		$s_promotion = "select pa.packageapplyoid, pr.packageoid, pr.name AS packagename, pr.stay, pr.multiply, pr.discountapplyoid, pr.applyvalue, pr.discounttypeoid, pr.discountvalue, pr.priority, ro.roomofferoid, ro.name AS roomoffername, h.hotelcode, h.hoteloid, pa.channeloid, h.hotelname, r.roomoid, r.adult, r.child, pr.depositoid, pr.depositvalue, pr.termcondition as promoterm, pr.typepackage, pr.overbooking, pr.alwaysshown, pr.multiplier, pr.multiplier_val, r.name as roomname 
			from packageapply pa 
			inner join roomoffer ro using (roomofferoid) 
			inner join room r using (roomoid) 
			inner join package pr using (packageoid) 
			inner join hotel h on h.hoteloid = pr.hoteloid
			inner join channel c using (channeloid)
			where 
			pr.publishedoid = '1' and pa.packageoid = '".$id."' and ro.roomofferoid = '".$roomofferoid."' 
			group by pa.packageoid";
	}elseif ($type == "extraapply") {
		$s_promotion = "select e.* from extra e where e.publishedoid = '1' and e.extraoid = '".$id."'";
			// echo($s_promotion);
	}else{
		$s_promotion = "select p.promotionoid, p.discounttypeoid, p.discountapplyoid, p.applyvalue, p.discountvalue, p.hoteloid,
		pa.channeloid,
		ro.roomofferoid, ro.roomoid, ro.offertypeoid,
		r.adult, r.child,h.hotelcode, h.hotelname, r.name as roomname
		from promotion p
		inner join hotel h using (hoteloid)
		inner join discounttype dt using (discounttypeoid)
		inner join discountapply da using (discountapplyoid)
		inner join promotionapply pa using (promotionoid)
		inner join roomoffer ro using (roomofferoid)
		inner join offertype ot using (offertypeoid)
		inner join room r using (roomoid)
		inner join channel c using (channeloid)
		where
		p.publishedoid = '1' and pa.promotionoid = '".$id."' and ro.roomofferoid = '".$roomofferoid."'
		group by p.promotionoid
		";
	}
		// echo $s_promotion;
	

	$q_promotion = mysqli_query($conn, $s_promotion) or mysqli_error();
	$promotion = mysqli_fetch_array($q_promotion);

	if($type == "extraapply"){
		$extraoid		= $promotion['extraoid'];
		$extraname		= $promotion['name'];
		$price			= $promotion['price'];
		$discountvalue	= $promotion['discountvalue'];
		$currency       = $promotion['currencyoid'];
		$discount 		= $price * $discountvalue / 100;
		$total 			= $price - $discount;
		$totalextra		= $night * $total;

		if(!isset($delid) or $delid==0){
			
			$s_getid ="select bookingtempextraoid from bookingtempextradtl inner join bookingtempextra using (bookingtempextraoid) where session_id = '".$tknSession."' and extraoid = '".$extraoid."' group by bookingtempextraoid";
				// echo($s_getid);
			$q_getid = mysqli_query($conn, $s_getid) or mysqli_error();
			while($getid=mysqli_fetch_array($q_getid)){
				
				$s_delete = "delete bookingtempextra, bookingtempextradtl from bookingtempextra, bookingtempextradtl where bookingtempextra.bookingtempextraoid = bookingtempextradtl.bookingtempextraoid and bookingtempextra.bookingtempextraoid = '".$getid['bookingtempextraoid']."'";
				// echo($s_delete);
				$q_delete = mysqli_query($conn, $s_delete) or mysqli_error();
			}	
		}else{
		
			$s_delete = "delete bookingtempextra, bookingtempextradtl from bookingtempextra, bookingtempextradtl where bookingtempextra.bookingtempextraoid = bookingtempextradtl.bookingtempextraoid and bookingtempextra.bookingtempextraoid = '".$delid."'";
			// echo($s_delete);
			$q_delete = mysqli_query($conn, $s_delete) or mysqli_error();
		}
	}else if($type == "packageapply"){

		$promotionoid = $promotion['packageoid'];
		$packageapplyoid 	= $promotion['packageapplyoid'];
		$hoteloid = $promotion['hoteloid'];
		$hotelname = $promotion['hotelname'];
		$roomname = $promotion['roomname'];
		$hotelcode = $promotion['hotelcode'];
		$roomoid = $promotion['roomoid'];
		$roomoffer = $promotion['roomofferoid'];
		$offertype = $promotion['offertypeoid'];
		$channeloid = $promotion['channeloid'];

		$package_night	= $promotion['stay'];
		$package_multiply = $promotion['multiply'];
		$apply_multiply	= $promotion['apply_multiply'];
		$discounttype = $promotion['discounttypeoid'];
		$discountapply = $promotion['discountapplyoid'];
		$applyvalue = explode(',', $promotion['applyvalue']);
		$discountvalue = $promotion['discountvalue'];

		if($adult == 0  and $child ==0){
			$adult = $promotion['adult']; $child = $promotion['child'];
		}


		if($promotion['discounttypeoid'] == 4){
			$night_after = $night + $promotion['discountvalue'];
		}else{
			$night_after = $night;
		}

		$total = 0;	

		/*-----------------------------------------------------------------------*/
		if(!isset($delid) or $delid==0){
			$s_getid = "select bookingtempoid from bookingtempdtl inner join bookingtemp using (bookingtempoid) where session_id = '".$tknSession."' and packageoid = '".$promotionoid."' and roomofferoid = '".$roomoffer."' and channeloid = '".$channeloid."' group by bookingtempoid";
			// echo($s_getid);
			$q_getid = mysqli_query($conn, $s_getid) or mysqli_error();
			while($getid=mysqli_fetch_array($q_getid)){
				
				$s_delete = "delete bookingtemp, bookingtempdtl from bookingtemp, bookingtempdtl where bookingtemp.bookingtempoid = bookingtempdtl.bookingtempoid and bookingtemp.bookingtempoid = '".$getid['bookingtempoid']."'";
				$q_delete = mysqli_query($conn, $s_delete) or mysqli_error();
			}	
		}else{

			$s_delete = "delete bookingtemp, bookingtempdtl from bookingtemp, bookingtempdtl where bookingtemp.bookingtempoid = bookingtempdtl.bookingtempoid and bookingtemp.bookingtempoid = '".$delid."'";
			$q_delete = mysqli_query($conn, $s_delete) or mysqli_error();
		}
		/*-----------------------------------------------------------------------*/
	}else{
		$promotionoid = $promotion['promotionoid'];
		$hoteloid = $promotion['hoteloid'];
		$hotelname = $promotion['hotelname'];
		$roomname = $promotion['roomname'];
		$hotelcode = $promotion['hotelcode'];
		$roomoid = $promotion['roomoid'];
		$roomoffer = $promotion['roomofferoid'];
		$offertype = $promotion['offertypeoid'];
		$channeloid = $promotion['channeloid'];


		$discounttype = $promotion['discounttypeoid'];
		$discountapply = $promotion['discountapplyoid'];
		$applyvalue = explode(',', $promotion['applyvalue']);
		$discountvalue = $promotion['discountvalue'];

		if($adult == 0  and $child ==0){
			$adult = $promotion['adult']; $child = $promotion['child'];
		}


		if($promotion['discounttypeoid'] == 4){
			$night_after = $night + $promotion['discountvalue'];
		}else{
			$night_after = $night;
		}

		$total = 0;	

		/*-----------------------------------------------------------------------*/
		if(!isset($delid) or $delid==0){
			$s_getid = "select bookingtempoid from bookingtempdtl inner join bookingtemp using (bookingtempoid) where session_id = '".$tknSession."' and promotionoid = '".$promotionoid."' and roomofferoid = '".$roomoffer."' and channeloid = '".$channeloid."' group by bookingtempoid";
			$q_getid = mysqli_query($conn, $s_getid) or mysqli_error();
			while($getid=mysqli_fetch_array($q_getid)){
				$s_delete = "delete bookingtemp, bookingtempdtl from bookingtemp, bookingtempdtl where bookingtemp.bookingtempoid = bookingtempdtl.bookingtempoid and bookingtemp.bookingtempoid = '".$getid['bookingtempoid']."'";
				$q_delete = mysqli_query($conn, $s_delete) or mysqli_error();
			}	
		}else{
			$s_delete = "delete bookingtemp, bookingtempdtl from bookingtemp, bookingtempdtl where bookingtemp.bookingtempoid = bookingtempdtl.bookingtempoid and bookingtemp.bookingtempoid = '".$delid."'";
			$q_delete = mysqli_query($conn, $s_delete) or mysqli_error();
		}
		/*-----------------------------------------------------------------------*/
	}

	for($r = 1; $r<=$booked_room; $r++){

		if($type == "packageapply"){
			$total_room = 0;
			$total_roomonly = 0;

			$totalperroom = 0; $total_roomcharge = 0; $total_extrabed = 0;
			$totalratepackage = 0; $totalroomcharge = 0; $totalpackage = 0;
			if($_GET['numroom']==0){ $numroom = $r; }

			echo "<div dtl=".$type."-".$typeid."-".$numroom.">";

			$allow_package = true;
			$get_price_package = true;

			$get_package = 0;
			if($package_multiply == "y"){
				$number_get_package = floor($night / $package_night);
				// echo 'package_multi';
			}else{
				$number_get_package = 1;
				// echo 'tidak package_multi';
			}

			for($n=0; $n < $night; $n++){	
				$allotment=0; $extrabed=0; $rate=0; $ratecharge=0; $additional_fee=0; $currency=''; $closeout='n'; $disc_note = '';
				/*-----------------------------------------------*/
				$dateformat = explode('/',date("D/Y-m-d",strtotime($checkin." +".$n." day")));
				$day = $dateformat[0];
				$date = $dateformat[1];
				
      			if(file_exists("../myhotel/data-xml/".$hotelcode.".xml")){
      				// echo $hotelcode;
         			$xml = simplexml_load_file("../myhotel/data-xml/".$hotelcode.".xml");
					// $query_tag = '//hotel[@id="'.$hoteloid.'"]/masterroom[@id="'.$roomoid.'"]/rate[@date="'.$date.'"]/rateplan[@id="'.$roomoffer.'"]/channel[@type="'.$channeloid.'"]';
					// foreach($xml->xpath($query_tag) as $tagallotment){
					// 	$allotment = $tagallotment[0];
					// }		
					$query_tag = '//hotel[@id="'.$hoteloid.'"]/masterroom[@id="'.$roomoid.'"]/rate[@date="'.$date.'"]/rateplan[@id="'.$roomoffer.'"]/channel[@type="'.$channeloid.'"]';
						foreach($xml->xpath($query_tag) as $xml_rate){
							$rate = $xml_rate->double;
							$currency = $xml_rate->currency;
							$extrabed = $xml_rate->extrabed;
							$breakfast = $xml_rate->breakfast;
						}

						// echo($rate);
      			}

    //				

				if($promotion['overbooking'] == "y" or ($promotion['overbooking'] == "n" and $rate > 0)){

					// echo $rate;
					// echo $promotion['typepackage'];
					if(($package_multiply == "y" and $n%$package_night == 0 and $get_package < $number_get_package) or ($package_multiply == "n" and $n == 0)){
						$allow_package = true;
						$get_price_package = true;
						// echo 'kondisi 1';
					}else if(($package_multiply == "y" and $n%$package_night > 0 and $allow_package == true) or ($package_multiply == "n" and $n < $package_night)){
						// echo 'kondisi 2';
						if($promotion['typepackage'] != "1"){
							$allow_package = true;
							$get_price_package = false; 
							// echo 'kondisi 2.1';
						}
					}else{
						$allow_package = false;
						$get_price_package = false;
						// echo 'kondisi 3';
					}
						// break;
					/* get package rate -----------------------*/
					$package_rate = 0;
					if($allow_package == true and $get_price_package == true){
						// echo($date);
						// break;
						$stmt = $db->prepare("select rate from packagerate where packageapplyoid = :a and (startdate <= :b and enddate >=:c) order by priority asc limit 1");
						$stmt->execute(array(':a' => $packageapplyoid, ':b' => $date, ':c' => $date));
						$found_rate_package = $stmt->rowCount();
						if($found_rate_package > 0){
							$result 			= $stmt->fetch(PDO::FETCH_ASSOC);
							$package_rate		= $result['rate'];
							if($promotion['typepackage'] == "1"){
								$rate	= $package_rate / $package_night;
							}else if($promotion['typepackage'] == "2"){
								$rate	= $package_rate;
								// echo($rate);?><br><?php
								// echo($package_rate);?><br><?php
								// echo($package_night);
							}
							if($n % $package_night ==  '0'){
								$get_package++;
							}
							$package_status		= "Yes";
						}else if($n == 0 and $found_rate_package == 0){
							$zero = true;
						}else{
							$allow_package	= false;
							$get_price_package = false;
						}
					}else if($allow_package == true and $get_price_package == false and $promotion['typepackage'] != 3){
						$rate = 0;
						$package_status		= "Yes";
						// echo($rate);
					}else{
						$additional_fee = $rate * $promotion['multiplier_val'] / 100;
						if($promotion['multiplier'] == "plus"){
							$rate = $rate + $additional_fee;
						}else if($promotion['multiplier'] == "minus"){
							$rate = $rate - $additional_fee;
						}
						$package_status		= "No";
					}

					if($allow_package == true and $promotion['typepackage'] == '3'){
						if($n == 0){
							$additional_fee = $rate * $promotion['multiplier_val'] / 100;
							if($promotion['multiplier'] == "plus"){
								$rate = $rate + $additional_fee;
							}else if($promotion['multiplier'] == "minus"){
								$rate = $rate - $additional_fee;
							}
						}
						$rate = $package_rate + $rate;
						$package_status		= "Yes";
					}

					// echo $totalpernight;
					$totalpernight = $rate;	
					$roomcharge = $totalpernight;//$rate_after
					$rate_after = $totalpernight;//$

					$total_roomcharge += $totalpernight;//$total_roomonly
					$total_roomonly += $totalpernight;//$
					// echo($total_roomonly);
				/*------------------------------------------*/
					if($with_extrabed == 0){ $extrabed = 0; }
					$totalpernight = $totalpernight + $extrabed;//$rate_final
					$rate_final = $totalpernight + $extrabed;//$
					
					$total_extrabed += $extrabed;//$total_extrabed
				/*------------------------------------------*/
				   	$totalperroom += $totalpernight;//$total_room
					$total_room += $totalpernight;//$
					/*-------------------------------------------*/

					// $total_roomonly = $total_roomonly + $rate_after;
					// $total_extrabed = $total_extrabed + $extrabed;
					// $rate_final = $rate_after + $extrabed;
					// $total_room = $total_room + $rate_final;
					/*-----------------------------------------------------------------------*/ 
			

					//echo "[".$xml_date."] ".$rate." - ".$roomcharge." - ".$totalpernight." (".$get_price_package.")<br>";

					/*----------------------------------------------------------*/

				}else{
					$zero = true;
					// echo 'hallo';
					break;
				}
				$time = date('Y-m-d H:i:s');
				// echo $rate;
				if($n == 0 ){
					$s_temp = "insert into bookingtemp (submittime, session_id, roomofferoid, channeloid, packageoid, 
							checkin, night, adult, child, flag, breakfast, extrabed,
							hotel, room ) 
							values ('".$time."', '".$tknSession."', '".$roomoffer."', '".$channeloid."', '".$promotionoid."', 
							'".$checkin."', '".$night_after."', '".$adult."', '".$child."', '0', '".$breakfast."', '".$with_extrabed."',  
							'".$hotelname."', '".$roomname."')";
					// echo "<script>console.log(\"insert:".$s_temp."\");</script>";
					// echo($s_temp);
					$q_temp = mysqli_query($conn, $s_temp) or mysqli_error();
					$temp_id = mysqli_insert_id($conn);
					// echo($temp_id);
				}

				// echo($rate_final);	
				$s_tempdtl = "insert into bookingtempdtl (bookingtempoid, date, rate, discount, rateafter, extrabed, total, currencyoid) values ('".$temp_id."', '".$date."', '".$rate."', '".$discountnote."', '".$rate_after."', '".$extrabed."', '".$rate_final."', '".$currency."')";
				// echo "<script>console.log(\"insert:".$s_tempdtl."\");</script>";
				// echo($s_tempdtl);
				$q_tempdtl = mysqli_query($conn, $s_tempdtl) or mysqli_error();
			} ?>

			<input type="hidden" box="bookingtemp" tmp="<?php echo $type; ?>-<?php echo $typeid; ?>-<?php echo $numroom; ?>" value="<?php echo $temp_id; ?>"/>
			<input type="hidden" value="offerroomoid-<?php echo $date;  ?>/<?php echo $date;  ?>/<?php echo $rate;  ?>/<?php echo $discount;  ?>">
  <?php 	
  			$checkout = date("Y-m-d",strtotime($checkin." +".$night." day"));
  			// echo($checkout);
			$s_temp = "update bookingtemp set roomtotal='".$total_roomonly."', extrabedtotal='".$total_extrabed."', total='".$total_room."', currencyoid = '".$currency."', checkout = '".$checkout."' where bookingtempoid = '".$temp_id."'";
			// echo($s_temp);
			// echo "<script>console.log(\"update:".$s_temp."\");</script>";
			$q_temp = mysqli_query($conn, $s_temp) or mysqli_error();

			$total = $total + $total_room;
       }else{
			$total_room = 0;
			$total_roomonly = 0;
			$total_extrabed = 0;

			if($_GET['numroom']==0){ $numroom = $r; }

			echo "<div dtl=".$type."-".$typeid."-".$numroom.">";

			for($n = 0; $n<$night_after; $n++){

				$discountnote = '';

				$dateformat = explode('/',date("D/Y-m-d",strtotime($checkin." +".$n." day")));
				$day = $dateformat[0];
				$date = $dateformat[1];

      		if(file_exists("../myhotel/data-xml/".$hotelcode.".xml")){
      			// echo $hotelcode;
          		$xml = simplexml_load_file("../myhotel/data-xml/".$hotelcode.".xml");
					$query_tag = '//hotel[@id="'.$hoteloid.'"]/masterroom[@id="'.$roomoid.'"]/rate[@date="'.$date.'"]/rateplan[@id="'.$roomoffer.'"]/channel[@type="'.$channeloid.'"]';
					foreach($xml->xpath($query_tag) as $xml_rate){
						$rate = $xml_rate->double;
						$currency = $xml_rate->currency;
						$extrabed = $xml_rate->extrabed;
						$breakfast = $xml_rate->breakfast;
						// echo "<script>console.log(\"$rate1:".$rate."\");</script>";
					}
      		}

			if(empty($currency)){ $currency= '1'; }
			if(
					$discountapply == '1' or
					($discountapply == '2' and in_array(($n+1), $applyvalue)) or
					($discountapply == '3' and in_array($day, $applyvalue)) or
					($discountapply == '4' and $n = 0) or
					($discountapply == '5' and $n = ($night - 1))
				){
					if($discounttype == '1'){
						$discountnote = $discountvalue."%";
						$discount = $rate * $discountvalue / 100;
					}else if($discounttype == '3'){
						$discountnote = $discountvalue;
						$discount = $discountvalue;
					}else{
						$discountnote = '';
						$discount = 0;
					}

					$rate_after = $rate - $discount;
			}else if(($n+1) > $night){
				$rate = 0;
				$rate_after = 0;
			}else{
				$rate_after = $rate;
			}

			$total_roomonly = $total_roomonly + $rate_after;

			if($with_extrabed == 0){ $extrabed = 0; }
			$total_extrabed = $total_extrabed + $extrabed;

			$rate_final = $rate_after + $extrabed;
			$total_room = $total_room + $rate_final;

			/*-----------------------------------------------------------------------*/ 
				$time = date('Y-m-d H:i:s');
				if($n == 0 ){
					
					$s_temp = "insert into bookingtemp (submittime, session_id, roomofferoid, channeloid, promotionoid, 
							checkin, night, adult, child, flag, breakfast, extrabed,
							hotel, room ) 
							values ('".$time."', '".$tknSession."', '".$roomoffer."', '".$channeloid."', '".$promotionoid."', 
							'".$checkin."', '".$night_after."', '".$adult."', '".$child."', '0', '".$breakfast."', '".$with_extrabed."', 
							'".$hotelname."', '".$roomname."')";
					// echo "<script>console.log(\"insert:".$s_temp."\");</script>";

					$q_temp = mysqli_query($conn, $s_temp) or mysqli_error();
					$temp_id = mysqli_insert_id($conn);
				}

				$s_tempdtl = "insert into bookingtempdtl (bookingtempoid, date, rate, discount, rateafter, extrabed, total, currencyoid) values ('".$temp_id."', '".$date."', '".$rate."', '".$discountnote."', '".$rate_after."', '".$extrabed."', '".$rate_final."', '".$currency."')";
				// echo "<script>console.log(\"insert:".$s_tempdtl."\");</script>";
				// echo($s_tempdtl);
				$q_tempdtl = mysqli_query($conn, $s_tempdtl) or mysqli_error();
			/*-----------------------------------------------------------------------*/
			}?>
			<input type="hidden" box="bookingtemp" tmp="<?php echo $type; ?>-<?php echo $typeid; ?>-<?php echo $numroom; ?>" value="<?php echo $temp_id; ?>"/>
			<input type="hidden" value="offerroomoid-<?php echo $date;  ?>/<?php echo $date;  ?>/<?php echo $rate;  ?>/<?php echo $discount;  ?>">
<?php
		$checkout = date("Y-m-d",strtotime($checkin." +".$night." day"));

		$s_temp = "update bookingtemp set roomtotal='".$total_roomonly."', extrabedtotal='".$total_extrabed."', total='".$total_room."', currencyoid = '".$currency."', checkout = '".$checkout."' where bookingtempoid = '".$temp_id."'";
		// echo "<script>console.log(\"update:".$s_temp."\");</script>";
		$q_temp = mysqli_query($conn, $s_temp) or mysqli_error();

		$total = $total + $total_room;
 	} // night
 	 ?>
	<input type="hidden" box="totalroom" tmp="<?php echo $type; ?>-<?php echo $typeid; ?>" room="<?php echo $numroom; ?>" value="<?php echo $total_room;  ?>" night="<?php echo $night;?>">
        <input type="hidden" box="totalcurrency" tmp="<?php echo $type; ?>-<?php echo $typeid; ?>" room="<?php echo $numroom; ?>" value="IDR">
        <input type="hidden" box="extrabed" tmp="<?php echo $type; ?>-<?php echo $typeid; ?>-<?php echo $numroom; ?>" value="<?php echo $total_extrabed; ?>">		
        
		</div>
<?php
	} // booked room

	if($type == "extraapply"){
		if($night <= $stock){
 		if($_GET['numroom']==0){ $numroom = $r; }
 		echo "<div dtl=".$type."-".$typeid.">";
 		for($n = 0; $n<$night; $n++){
		$time = date('Y-m-d H:i:s');
		try{
			if($n == 0 ){

		$s_temp = "insert into bookingtempextra (submittime, session_id, extra, extraoid, checkin, qty, currencyoid, price, total)  VALUES ('".$time."', '".$tknSession."', '".$extraname."', '".$extraoid."', '".$checkin."', '".$night."', '".$currency."', '".$price."', '".$totalextra."' )";
		$q_temp = mysqli_query($conn, $s_temp) or mysqli_error();
		$temp_id = mysqli_insert_id($conn);
		echo "<script>console.log(\"insert:".$s_temp."\");</script>";
		echo "<script>console.log(\"id:".$temp_id."\");</script>";
		}
	}catch(PDOException $ex) {
		echo "Invalid Query";
		die();
	}
		$s_tempdtl = "insert into bookingtempextradtl (bookingtempextraoid, date, price, discount, rateafter, total, currencyoid) values ('".$temp_id."', '".$date."', '".$price."', '".$discountvalue."', '".$total."', '".$totalextra."', '".$currency."')";
				echo "<script>console.log(\"insert:".$s_tempdtl."\");</script>";
				// echo($s_tempdtl);
		$q_tempdtl = mysqli_query($conn, $s_tempdtl) or mysqli_error();
		
		
	
		}?>
	<input type="hidden" box="bookingtemp" tmp="<?php echo $type; ?>-<?php echo $typeid; ?>" value="<?php echo $temp_id; ?>"/>
	<input type="hidden" value="offerroomoid-<?php echo $checkin; ?>/<?php echo $checkin;  ?>/<?php echo $price;  ?>">
	<!-- <?php
	$stockakhir = $stock - $night;
	$s_temp = "update extra set stock='".$stockakhir."' where extraoid = '".$id."'";
		// echo "<script>console.log(\"update:".$s_temp."\");</script>";
		$q_temp = mysqli_query($conn, $s_temp) or mysqli_error();

	?> -->
 	<input type="hidden" box="totalroom" tmp="<?php echo $type; ?>-<?php echo $typeid; ?>" room="<?php echo $numroom; ?>" value="<?php echo $totalextra;  ?>" night="<?php echo $night;?>">
        <input type="hidden" box="totalcurrency" tmp="<?php echo $type; ?>-<?php echo $typeid; ?>" room="1" value="IDR">
	<?php }else{
		// echo("arg1");
	}

}
?>