<?php

	unset($allminrate); unset($promoname); unset($price); unset($rate);

	$todaydate='2018-07-01';
	$filter_periode_sale = "(salefrom <= '".$todaydate."' and saleto >= '".$todaydate."')";
	$filter_periode_book = "(bookfrom <= '".$checkin."' and bookto >= '".$checkout."') and (DATE_FORMAT(DATE_ADD('".$checkin."', INTERVAL -1 * min_bef_ci DAY), '%Y-%m-%d') >= '".$todaydate."') and IF(max_bef_ci>0,(DATE_FORMAT(DATE_ADD('".$checkin."', INTERVAL -1 * max_bef_ci DAY), '%Y-%m-%d') <= '".$todaydate."'),TRUE) = TRUE";
	$checkinday = date("D",strtotime($checkin));
	$s_promo = "select p.promotionoid, p.priority, p.name as promoname, p.discounttypeoid, p.discountapplyoid, p.applyvalue, p.discountvalue,
	pt.type, ro.name as roomname, ro.roomofferoid, r.roomoid, h.hotelname
	from promotion p
	inner join promotiontype pt using (promotiontypeoid)
	inner join discounttype dt using (discounttypeoid)
	inner join hotel h using (hoteloid)
	inner join promotionapply pa using (promotionoid)
	inner join roomoffer ro using (roomofferoid)
	inner join offertype ot using (offertypeoid)
	inner join room r using (roomoid)
	inner join channel ch using (channeloid)
	where h.hoteloid = '".$hoteloid."' and ch.channeloid = '".$channeloid."'
	and ".$filter_periode_sale."  and ".$filter_periode_book."
	and p.displayed like '%".$todayday."%' and p.checkinon like '%".$checkinday."%'
	and p.publishedoid = '1' and ro.publishedoid = '1' and r.publishedoid = '1' and ch.channeloid = '".$channeloid."'";
	//order by p.priority limit 3
	//echo $s_promo;
	$q_promo = mysqli_query($conn, $s_promo) or mysqli_error();
	$q_promo_2 = mysqli_query($conn, $s_promo) or mysqli_error();
	$ro_oid = array();

	while($promo = mysqli_fetch_array($q_promo)){

		$val_array = $promo['roomoid']."-".$promo['roomofferoid'];
		if(!in_array($val_array, $ro_oid)){
			array_push($ro_oid, $val_array);

		}
	}

	/*-----------------------------------------------*/
	$s_rooms = "select ro.name as roomname, ro.roomofferoid, r.roomoid, h.hotelname
	from hotel h
	inner join room r using (hoteloid)
	inner join roomoffer ro using (roomoid)
	inner join offertype ot using (offertypeoid)
	where h.hoteloid = '".$hoteloid."'
	and ro.publishedoid = '1' and r.publishedoid = '1'";
	$q_rooms = mysqli_query($conn, $s_rooms) or mysqli_error();
	$q_rooms_2 = mysqli_query($conn, $s_rooms) or mysqli_error();

	while($rooms = mysqli_fetch_array($q_rooms)){
		$val_array = $rooms['roomoid']."-".$rooms['roomofferoid'];
		if(!in_array($val_array, $ro_oid)){
			array_push($ro_oid, $val_array);

		}
	}
	/*-----------------------------------------------*/

	foreach($ro_oid as $value){
		$ro = explode('-',$value);
		$roomoid = $ro[0];
		$roomoffer = $ro[1];
		$rate[$roomoffer] = array();

		for($n=0;$n<$night;$n++){

			$date = date("Y-m-d",strtotime($checkin." +".$n." day"));
			$query_tag = '//hotel[@id="'.$hoteloid.'"]/masterroom[@id="'.$roomoid.'"]/rate[@date="'.$date.'"]/rateplan[@id="'.$roomoffer.'"]/channel[@type="'.$channeloid.'"]';
			$single=0;
			foreach($xml->xpath($query_tag) as $tagrate){
				$single = (float)$tagrate->double;
				$currency = (float)$tagrate->currency;
				if(!in_array($single, $rate[$roomoffer]) and $single > 0){
					$rate[$roomoffer][] = $single;//."-".$roomoffer;
				}
			}

			/*-DATA EXTERNAL---
			$q_file = mysqli_query($conn, "SELECT `gfilename` FROM `log_import`") or mysqli_error();
			while($files = mysqli_fetch_array($q_file)){
				$gfilename = $files['gfilename'];
				$gfileurl = "extranet/data-xml/external/".$gfilename;

				if(file_exists($gfileurl)){
					$xmlext = simplexml_load_file($gfileurl);
					$query_tag = '//hotel[@id="'.$hoteloid.'"]/masterroom[@id="'.$roomoid.'"]/rate[@date="'.$date.'"]/rateplan[@id="'.$roomoffer.'"]/channel[@type="'.$channeloid.'"]';
					foreach($xmlext->xpath($query_tag) as $tagrate) {
						$single = (float)$tagrate->single;
						$currency = (float)$tagrate->currency;
						if(!in_array($single, $rate[$roomoffer]) and $single > 0){
							$rate[$roomoffer][] = $single;//."-".$roomoffer;
						}
					}
				}
			}
			*/

		} // for night
	} // foreach room offer id

	$roomcurrency = "IDR";
	$allminrate = array(); $allnetrate = array(); $offername = array(); $promoname = array(); $price = array();

	while($promo = mysqli_fetch_array($q_promo_2)){
		$roomoffer = $promo['roomofferoid'];
		$discounttype = $promo['discounttypeoid'];
		$discountvalue = $promo['discountvalue'];

 		foreach($rate[$promo['roomofferoid']] as $single){
			if($discounttype == '1'){
				$discountnote = $discountvalue."%";
				$discount = $single * $discountvalue / 100;
			}else if($discounttype == '3'){
				$discountnote = $discountvalue;
				$discount = $discountvalue;
			}else{
				$discount = 0;
			}

			$rate_after = $single - $discount;
			array_push($allminrate, $rate_after);
			array_push($allnetrate, $single);

			$promorate = number_format($rate_after, 0, ".", ",");

			array_push($promoname, $promo['roomname']." ".$promo['promoname']);
			array_push($offername, $promo['promoname']);
			array_push($price, $roomcurrency." ".$promorate);
		}
	}

	/*----------------------------------------------*/
	while($rooms = mysqli_fetch_array($q_rooms_2)){
		$roomoffer = $rooms['roomofferoid'];
		$discounttype = $promo['discounttypeoid'];
		$discountvalue = $promo['discountvalue'];

 		foreach($rate[$rooms['roomofferoid']] as $single){

			$rate_after = $single;
			array_push($allminrate, $rate_after);
			array_push($allnetrate, $single);

			$promorate = number_format($rate_after, 0, ".", ",");

			array_push($promoname, $rooms['roomname']);
			array_push($offername, 'Best Flexible Rate');
			array_push($price, $roomcurrency." ".$promorate);
		}
	}
	/*----------------------------------------------*/

	if(count($allminrate)>0){
		$minrate = min($allminrate);
		$hotel_minrate = number_format($minrate);

		$key = array_search($minrate,$allminrate);
		$netrate = $allnetrate[$key];
		$shownetrate = number_format($netrate);
		$minrate_promoname = $offername[$key];

		if(strlen($hotel_minrate) > 5){
			$hotel_minrate_first = substr($hotel_minrate, 0, strlen($hotel_minrate)-4);
			$hotel_minrate_second = substr($hotel_minrate, -4);
			$hotel_minrate_final = $hotel_minrate_first."<span class='thousand'>".$hotel_minrate_second."</span>";

			// Hotel Link
			include_once("includes/function.randomstring.php");
			$ec_continent = strtolower($continent);
			$ec_country = strtolower($country);
			$ec_state = strtolower($state);
			$ec_city = strtolower($city);
			$ec_hotelname = str_replace(" ", "-", $hotelname );
			$ec_hoteloid = randomString(4).$hoteloid.randomString(2);
			$parameters = ( isset($uri4) or !empty($uri4) or $uri4 != "" ) ? "/".$uri4 : "";
			$href_hotel = $base_url."/hotel/".$ec_continent."/".$ec_country."/".$ec_state."/".$ec_city."/".$ec_hotelname."/".$ec_hoteloid.$parameters;

			$gethotel++;

		}else{
			$hotel_minrate_final = $hotel_minrate;
		}
	}else{
		$minrate = 0;
	}

	$currencycode = "IDR";

?>
