<?php
include("tour.type-normal.query.php");
include('fnc-tour/normal.script.php');
$sql_h = "
	SELECT h.*, cityname, statename, countryname, continentname, ht.category 
	FROM hotel h 
	inner join hoteltype ht using (hoteltypeoid) 
	inner join city using (cityoid) 
	inner join state using (stateoid) 
	INNER JOIN country USING (countryoid) 
	INNER JOIN continent USING (continentoid) 
	INNER JOIN tourhotel USING (hoteloid) 
	INNER JOIN tour USING (touroid) 
	WHERE  tourhotel.tourhoteloid = '".$tourhoteloid."' 
	group by hoteloid";
//$sql_h = "SELECT * FROM hotel h INNER JOIN hotelphoto hp USING (hoteloid) WHERE h.hoteloid = '".$hoteloid."' AND hp.flag = 'main'";
$run_h = mysqli_query($conn, $sql_h);
$row_h = mysqli_fetch_array($run_h);
	$hoteloid= $row_h['hoteloid'];
	$hotel_name = $row_h['hotelname'];
	$hotel_address = $row_h['address'];
	$hotel_stars = $row_h['stars'];
	$hotel_totalrom = $row_h['totalroom'];
	$hotel_website = $row_h['website'];
	$hotel_area = $row_h['cityname'];
	
	//Hotel Photo Main
	$s_hotelphoto = "SELECT * FROM hotelphoto hp INNER JOIN hotelphotocontent hpc USING (hotelphotooid) WHERE hp.hoteloid = '".$hoteloid."' AND hp.flag = 'main'";
	$q_hotelphoto = mysqli_query($conn, $s_hotelphoto);
	$r_hotelphoto = mysqli_fetch_array($q_hotelphoto);
	$hotel_pict = $r_hotelphoto['photourl'];
	
	// Hotel Content
	$s_hotelcontent_part1 = "SELECT * FROM hotelcontent hc WHERE hoteloid = '$hoteloid' AND hc.languageoid = '";
	$s_hotelcontent_part2 = "'";
	$s_hotelcontent_selected = $s_hotelcontent_part1.$selected_language.$s_hotelcontent_part2; 
	$n_hotelcontent_selected = mysqli_num_rows(mysqli_query($conn, $s_hotelcontent_selected));
	$s_hotelcontent_fallback = $s_hotelcontent_part1.$fallback_language.$s_hotelcontent_part2;
	$n_hotelcontent_fallback = mysqli_num_rows(mysqli_query($conn, $s_hotelcontent_fallback));
	//echo $n_hotelcontent_selected." - ".$n_hotelcontent_fallback;
	$s_hotelcontent = ($n_hotelcontent_selected > 0) ? $s_hotelcontent_selected : $s_hotelcontent_fallback ;	
	$q_hotelcontent = mysqli_query($conn, $s_hotelcontent);
	$r_hotelcontent = mysqli_fetch_array($q_hotelcontent);
	$hotel_headline = !empty($r_hotelcontent['headline']) ? "<q>".$r_hotelcontent['headline']."</q>" : "" ;
	$hotel_description = $r_hotelcontent['description'];
	$hotel_facilities = $r_hotelcontent['facilities'];
	
	
?>
<style>
	.small-thumb {width: 65px; height: 65px;}
	#tour-name {
		padding: 0; margin: 0 0 0 0 !important; 
		background-color: #fff; 
		background-image: url(<?=$base_url;?>/images/square_bg.png), url(<?=$base_url;?>/images/square_bg.png); 
		color: #848484;
	}
	#tour-name .small-thumb {width: 50px; height: 50px; margin: 0 5px 0 0;}
	
	#hotel-pict {margin: 0 10px 0 0;}
</style>

<div id="single-content">
	<div id="breadcrumb" class="white-box border-box">
		<?php
		if($tour_cityoid != 0){
			$uri_continent = $uri3;
			$uri_country = $uri4;
			$uri_state = $uri5;
			$uri_city = $uri6;
			$uri_tourname = $uri7;
			$breadcrumb = array($uri2,$uri_continent,$uri_country,$uri_state,$uri_city,$uri_tourname,$hotel_name);
		}else if($tour_stateoid != 0){
			$uri_continent = $uri3;
			$uri_country = $uri4;
			$uri_state = $uri5;
			$uri_tourname = $uri6;
			$breadcrumb = array($uri2,$uri_continent,$uri_country,$uri_state,$uri_tourname,$hotel_name);
		}else if($tour_countryoid != 0){
			$uri_continent = $uri3;
			$uri_country = $uri4;
			$uri_tourname = $uri5;
			$breadcrumb = array($uri2,$uri_continent,$uri_country,$uri_tourname,$hotel_name);
		}else if($tour_continentoid != 0){
			$uri_continent = $uri3;
			$uri_tourname = $uri4;
			$breadcrumb = array($uri2,$uri_continent,$uri_tourname,$hotel_name);
		}
		//$breadcrumb = array($uri2,$uri3,$uri4,$uri5,$uri6,$uri7);
		foreach ($breadcrumb as $key => $value) {
			if(end($breadcrumb)==$value){
				$gt = "";
			}else{
				$gt = " &gt; ";
			}
			$value = str_replace("-", " ", $value);
			?>
			<a class="breadcrumb text" id="breadcrumb-<?=$key;?>" href="#"><?=$value;?></a><?=$gt;?>	
			<?php
		} 
		?>
	</div>
	
	<div id="tour-name" class="white-box border-box clear">
		<div class="thumb inline-block small-thumb"><img src="<?=$pict;?>" /></div>
		<div class="inline-block middle">
			<h1><span class="grey"><?=$name;?></span></h1>
		</div>
	</div>
	
	<div class="white-box border-box">
		<div class="clear">
			<div class="thumb inline-block border-box top fl_left small-thumb" id="hotel-pict"><img src="<?=$hotel_pict;?>" /></div>
			<div class="fl_left">
				<h3 class="default inline-block">
				
					<?php
					echo $hotel_name."&nbsp;";
					starRating($hotel_stars, 2); 
					?>
				</h3>
				
				<div>
					<i class="desc"><?=$hotel_address;?> (<a href="#" class="text">show on map</a>)</i>			
				</div>
				<div>
					<b>Hotel Area :</b> <?=$hotel_area;?>
					&nbsp; &nbsp;
					<b>Number of Rooms :</b> <?=$hotel_totalrom;?>
				</div>
			</div>
			
			<div class="fl_right">
				<a href="#" class="button book">More Info</a>
			</div>
		</div>
		
	</div>
		
	
	<div class="white-box border-box" id="room-availability">
		<h3><span class="grey">Rates &amp;</span> <span class="blue"> Availability</span></h3>			
		<div class="clear border-bottom">
			<div class="inline-block">
				<?php echo date("D d F Y", strtotime($checkin)); ?> - <?php echo date("D d F Y", strtotime($checkout)); ?> for <?php echo $night; ?> nights
				<a class="toggle-display button expand" target="#availability-hotel">Change Dates</a>
			</div>
			<div class="inline-block" id="availability-hotel" style="display: none;"><?php include("form.availability.tour-hotel.php"); ?></div>
		</div>
		<div>
			<form name="book_room" method="post" action="<?=$base_url;?>/book/guest-detail">
				<input type="hidden" name="hoteloid" value="<?php echo $uri8; ?>">
				<div class="inline-block top">
					<ul class="content-list table">
						<li class="table-row title">
							<div class="table-cell center middle">Room Type</div>
							<div class="table-cell center middle">Max Occupancy</div>
							<div class="table-cell center middle">Rate<br>per pax</div>
							<div class="table-cell center middle">No. of Pax</div>
						</li>
						<?php	include('tour.type-normal.list.php'); ?>
					</ul>
				</div>
				
				<div class="inline-block top" id="book-summary" style="width:175px;">
					<h3>Booking Summary</h3>
					<div id="book-summary-dtl" class="border-bottom left"></div>
					<h3>Total price</h3>
					<h2><span class="grey"></span> <span class="blue"></span></h2>
					<div id="textbox"></div>
					<div class="right">
						<!-- <a class="button book" href="<?=$base_url;?>/book/guest-detail">BOOK</a> -->
						<input type="submit" class="general-submit" value="BOOK">
					</div>
				</div>
			</form>
			
		</div>
	</div>
	
	
	<div id="hotel-detail" class="border-box">
		<nav>
			<ul class='tabs'>
			    <li class="inline-block"><a href='#tour-description'><h4>DESCRIPTION</h4></a></li>
			    <li class="inline-block"><a href='#tour-itenerary'><h4>ITENERARY</h4></a></li>
			    <li class="inline-block"><a href='#tour-terms'><h4>TERMS</h4></a></li>
			</ul>
		</nav>
		
		<div class="tabs-content" id='tour-description'>  
			<?=$description;?>
		</div>
		
		<div class="tabs-content" id='tour-itenerary'>
			<?=$itenerary;?>
		</div>
		
		<div class="tabs-content" id='tour-terms'>  
			<?=$termscondition;?>
		</div>
	</div>
	
	
</div> <!-- end of #single-content -->