<?php
include("parameter.flight.php");
?>

<form id="checkflightform" action="<?=$base_url;?>/search/flight/" method="get"><!-- http://online.abbey.travel/flight/ -->

	<div>
		<input type="radio" name="typeflight" class="inline-block top" value="roundtrip" <?=($fl_typeflight!='oneway') ? 'checked="checked"' : ''?>> <div class="inline-block top">Round Trip</div>
		<input type="radio" name="typeflight" class="inline-block top" value="oneway" <?=($fl_typeflight=='oneway') ? 'checked="checked"' : ''?>> <div class="inline-block top">One Way</div>
		<!-- <input type="radio" id="typeflight" name="typeflight"> Multi Destination -->
	</div>
	<div class="inline-block top">
		<div>Origin</div>
		<!--<input type="text" name="fromflight" id="inputflight" placeholder="Origin">-->
		<select name="fromflight" class="inputflight"></select>
	</div>
	<div class="inline-block top">
		<div>Destination</div>
		<!-- <input type="text" name="toflight" id="inputflight"  placeholder="Destination"> -->
		<select name="toflight" class="inputflight"></select>
	</div>
	
	<div class="inline-block top">
		<div>Depart</div>
		<div><input type="text" name="departflight" id="departflight" class="calinput" readonly value="<?=(!empty($fl_departflight)) ? $fl_departflight : date("Y-m-d")?>"></div>
	</div>
	<div class="inline-block top">
		<div class="roundtrip">Return</div>
		<div class="roundtrip"><input type="text" name="returnflight" id="returnflight" class="calinput" readonly value="<?=(!empty($fl_returnflight)) ? $fl_returnflight : ""?>"></div>
	</div>


	<div class="inline-block top">
		<div class="inline-block">
			<div>Adult</div>
			<div><select name="adultflight" id="selectflight"><?php for($a=1;$a<=10;$a++){ if($a==$fl_adultflight){ $selected="selected='selected'";  }else{ $selected=""; } ?><option value="<?php echo $a; ?>" <?php echo $selected; ?>><?php echo $a; ?></option><?php } ?> </select></div>
		</div>
		<div class="inline-block">
			<div>Child</div>
			<div><select name="childflight" id="selectflight"><?php for($a=0;$a<=10;$a++){ if($a==$fl_childflight){ $selected="selected='selected'";  }else{ $selected=""; } ?><option value="<?php echo $a; ?>" <?php echo $selected; ?>><?php echo $a; ?></option><?php } ?> </select></div>
		</div>
		<div class="inline-block">
			<div>Infant</div>
			<div><select name="infantflight" id="selectflight"><?php for($a=0;$a<=10;$a++){ if($a==$fl_infantflight){ $selected="selected='selected'";  }else{ $selected=""; } ?><option value="<?php echo $a; ?>" <?php echo $selected; ?>><?php echo $a; ?></option><?php } ?> </select></div>
		</div>
	</div>
	
	<div class="right inline-block top"><input type="submit" id="searchflight" class="search-submit" value="Search Flight" /></div>

</form>
<script>
	$(function(){
		//****Switch RoundTrip & OneWay****//
		$("input[name='typeflight']").change(function(){
			if(this.checked == true){
				var type = $(this).val();
				if(type == "roundtrip"){
					$(".roundtrip").show();
				}else if(type == "oneway"){
					$(".roundtrip").hide();
					$("#returnflight").val("");
				}
			}
		});
		$("input[name='typeflight']").trigger('change');
		
		$("#checkflightform").submit(function(e){
			var type = $("input:radio[name='typeflight']:checked").val();
			var retval = $("#returnflight");
			if(type == "roundtrip" && retval.val() == ""){
				alert("Please fill the return date field.");
				retval.focus();
				return false;
			}else{
				return true;
			}
		});
	});
</script>
<?php
	//tiket.com
	include("flightdata/tiket.com/init.php");
	include("flightdata/tiket.com/js_listcountry.php");
?>