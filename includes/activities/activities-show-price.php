<ul class="block price">
<?php
	function priceAfterDiscount($price, $discount){
		$price_after_discount	= $price - (($price * $discount)/100);
		return $price_after_discount;
	}
	
	function convertCurrency($from, $to, $price){
		if($from != $to){
			$s_bookkeeping	= "select nominal from bookkeeping where fromcurrency = '".$from."' and tocurrency = '".$to."' and status='1' order by dateinput desc";
			$q_bookkeeping	= mysqli_query($conn, $s_bookkeeping) or die(mysqli_error());
			$found			= mysqli_num_rows($q_bookkeeping);
			if($found > 0){
				$r_bookkeeping	= mysqli_fetch_array($q_bookkeeping);
				$kurs	= $r_bookkeeping['nominal'];
			}else{
				$kurs	= 1;
			}
		}else{
			$kurs	= 1;
		}
		
		$s_currency	= "select currencycode from currency where currencyoid = '".$to."'";
		$q_currency	= mysqli_query($conn, $s_currency) or die(mysqli_error());
		$currency	= mysqli_num_rows($q_currency);
		
		$price_converted	= $price * $kurs;
		return $price_converted;
	}
	
	function priceNominal($currencyoid, $price){
		$s_currency	= "select currencycode from currency where currencyoid = '".$currencyoid."'";
		$q_currency	= mysqli_query($conn, $s_currency) or die(mysqli_error());
		$currency	= mysqli_fetch_array($q_currency);
		
		$price_nominal		= $currency['currencycode']." ".number_format($price);
		return $price_nominal;
	}

	$minpax		= $item['minquantity'];
	$discount	= $item['discount'];
	$multiprice	= $item['multipricetypeoid'];
	
	$price_real_item	= 0;
	$price_real_adult	= 0;
	$price_real_child	= 0;
	
	if($item['multipricetypeoid'] == 0){
		if($item['price'] > 0){	$price_real_item	= $item['price'];	}
		if($item['adult'] > 0){	$price_real_adult	= $item['adult'];	}
		if($item['child'] > 0){	$price_real_child	= $item['child'];	}
	}else{
		$syntax_multiprice	= "SELECT * FROM multiprice INNER JOIN currency USING (currencyoid) WHERE typeid = 'item' AND idreference = '".$item['itemoid']."' AND multipricetypeoid = '".$item['multipricetypeoid']."'";
		$query_multiprice	= mysqli_query($conn, $syntax_multiprice) or die(mysqli_error());
		$mp					= mysqli_fetch_array($query_multiprice);
		
		if($mp['rateprice'] > 0){	$price_real_item	= $mp['rateprice'];	}
		if($mp['rateadult'] > 0){	$price_real_adult	= $mp['rateadult'];	}
		if($mp['ratechild'] > 0){	$price_real_child	= $mp['ratechild'];	}

		$currencyoid	= $mp['currencyoid'];
		$currency		= $mp['currencycode'];	
	}

	if($price_real_item > 0){
		$price_real_item		= convertCurrency($currencyoid, $default_currencyoid, $price_real_item);
		$price_after_discount	= priceAfterDiscount($price_real_item, $discount);
		$price_final			= priceNominal($default_currencyoid, $price_after_discount);
		$price_real_item		= $price_after_discount;		
		echo '<li>Price : '.$price_final.'</li>';
	}
	if($price_real_adult > 0){
		$price_real_adult		= convertCurrency($currencyoid, $default_currencyoid, $price_real_adult);
		$price_after_discount	= priceAfterDiscount($price_real_adult, $discount);
		$price_final			= priceNominal($default_currencyoid, $price_after_discount);
		$price_real_adult		= $price_after_discount;		
		echo '<li>Adult : '.$price_final.'</li>';
	}
	if($price_real_child > 0){
		$price_real_child		= convertCurrency($currencyoid, $default_currencyoid, $price_real_child);
		$price_after_discount	= priceAfterDiscount($price_real_child, $discount);
		$price_final			= priceNominal($default_currencyoid, $price_after_discount);
		$price_real_child		= $price_after_discount;		
		echo '<li>Children : '.$price_final.'</li>';
	}
?>
</ul>