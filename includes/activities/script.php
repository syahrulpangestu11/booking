<script type="text/javascript">
$(function(){

	$('body').on('change','select[name="qty[]"], select[name="adult[]"], select[name="children[]"]', function(e){
		multiprice	= $(this).attr('mptype');
		if(multiprice == '0'){
			total = singlePricing($(this));
		}
	});
	
	function singlePricing(elem){
		qty		= elem.val();
		price	= elem.attr('prc');
		total	= qty * price;
		
		return total;
	}
	
});
</script>