<?php
	$ci = explode('-', $checkin);
	$dt	= $ci[2];
	$mt	= $ci[1];
	$yr	= $ci[0];

	$start_year	= date("Y");
	
	$numberofday = cal_days_in_month(CAL_GREGORIAN, $mt, $yr); // 31
	if($mt == date("m") and $yr == date("Y")){
		$start_date = date("d");
	}else{
		$start_date = 1;
	}

?>
<ul class="block">
    <li>
    <?php
        $limitpax	= 20;
    	/* quantity ----------------------------------------------------*/
        if($price_real_item <= 0){
            echo '<input type="hidden" name="qty[]" prc="0" value="0">';
        }else{
            echo 'Quantity : <select name="qty[]" prc="'.$price_real_item.'" mptype = "'.$multiprice.'" >';
            for($i = $minpax; $i <= $limitpax; $i++){
                echo '<option value="'.$i.'">'.$i.'</option>';
            }
            echo '</select>&nbsp;&nbsp;';
        }
    	/* adult ---------------------------------------------------*/
        if($price_real_adult <= 0){
            echo '<input type="hidden" name="adult[]" prc="0" value="0">';
        }else{
            echo 'Adult : <select name="adult[]" prc="'.$price_real_adult.'" mptype = "'.$multiprice.'">';
            for($i = $minpax; $i <= $limitpax; $i++){
                echo '<option value="'.$i.'">'.$i.'</option>';
            }
            echo '</select>&nbsp;&nbsp;';
        }
    	/* children ----------------------------------------------------*/
        if($price_real_child <= 0){
            echo '<input type="hidden" name="child[]" prc="0" value="0">';
        }else{
            echo 'Children : <select name="child[]" prc="'.$price_real_child.'" mptype = "'.$multiprice.'">';
            for($i = $minpax; $i <= $limitpax; $i++){
                echo '<option value="'.$i.'">'.$i.'</option>';
            }
            echo '</select>&nbsp;&nbsp;';
        }
    
    ?>
    </li>
    <li>
    Arrival Date :
    <select name="day[]">
        <?php 
            for($i=$start_date; $i<=$numberofday; $i++){
                if($i==$dt){ $selected="selected='selected'"; }else{ $selected=""; }
                echo "<option value='".$i."' ".$selected.">".$i."</option>";
            }
        ?>
    </select>
    <select name="month[]">
        <?php 
            $months = array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December');
            for($i=intval(date("m")); $i<=12; $i++){
                if($i==$mt){ $selected="selected='selected'"; }else{ $selected=""; }
                echo "<option value='".$i."' ".$selected.">".$months[$i]."</option>";
            }
        ?>
    </select>
    <select name="year[]">
        <?php 
            for($i=$start_year; $i<=($start_year+1); $i++){
                if($i==$yr){ $selected="selected='selected'"; }else{ $selected=""; }
                echo "<option value='".$i."' ".$selected.">".$i."</option>";
            }
        ?>
    </select>
    </li>
    <li class="price-breakdown">
    </li>
</ul>