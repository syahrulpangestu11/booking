<?php
/*------------------------------------------------------------------------------------------------------------------------------------*/
$s_roomlist = "select thr.tourhotelroomoid, thr.roomoid, thr.pax from tournormalprice as tnp inner join currency as c using (currencyoid) inner join tax as tx using (taxoid) inner join tournormal as tn using (tournormaloid) inner join  tourhotelroom as thr inner join room as r using (roomoid) inner join hotel as h using (hoteloid) inner join tourhotel as th using (tourhoteloid) inner join tour as t using (touroid) where thr.tourhoteloid = '$tourhoteloid' and ('$todaydate'>=tn.startbook and '$todaydate'<=tn.endbook) and ('$checkin'>=tn.startdate and '$checkin'<=tn.enddate) group by thr.tourhotelroomoid order by tn.priority asc, netrate asc";
$numrow = mysqli_num_rows(mysqli_query($conn, $s_roomlist));
if($numrow >= 1){
	$q_roomlist = mysqli_query($conn, $s_roomlist) or die(mysqli_error());
	while($roomlist = mysqli_fetch_array($q_roomlist)){
		$tourroomoid = $roomlist['tourhotelroomoid'];
		$sroom = "select r.name, r.roompict, r.adult, r.child from room as r inner join tourhotelroom as thr using (roomoid) where tourhotelroomoid = '$tourroomoid'";
		$qroom = mysqli_query($conn, $sroom) or die(mysqli_error()); $room = mysqli_fetch_array($qroom);
		$room_name = $room['name']; $room_pict  = $room['roompict']; $room_adult = $room['adult']; $room_child = $room['child'];
		include("fnc-tour/normal.get-rate.php");
?>
		<li class="table-row">
			<div class="table-cell top">
				<span class="title"><?=$room_name;?></span>
				<div class="thumb pict"><img src="<?=$room_pict;?>"></div>
				<a href="#">room info</a>
			</div>
			<div class="table-cell top center">
			<?php
			for($i=1;$i<=$room_adult;$i++){ ?>
				<img src="<?=$base_url;?>/images/adult.png">
				<?php
			}
			for($i=1;$i<=$room_child;$i++){ ?>
				<img src="<?=$base_url;?>/images/child.png">
				<?php
			}
			?>
			</div>
			<div class="table-cell top center">
				<h2><span class="grey"><?php echo $currencycode;  ?></span> <span class="blue"><?php echo number_format($priceperpax); ?></span></h2>
			</div>
			<div class="table-cell top center">
				<input type="hidden" name="tourroomoid" id="tourroomoid" value="<?php echo $tourroomoid; ?>">
				<select tour="number">
					<?php for($r=0;$r<=30;$r++) { ?>
					<option value="<?php echo $r; ?>"><?php echo $r; ?></option>
					<?php } ?>
				</select>
			</div>
		</li>
<?php
	}
} // if numrow == 0
/*------------------------------------------------------------------------------------------------------------------------------------*/
?>