<?php

class SearchHotel {

    private $_keyword;
    private $_checkin;
    private $_night;
    private $_adult;
    private $_promo;
    private $_hotelname;
    private $_hoteltypes;
    private $_stars;
    private $_ranges;
    private $_facilities;
    private $_chains;

    private $_page_number;
    private $_sort_by;
    private $_conn;
    private $_profile;
    private $_inquiry;

    private $_continentoid;
    private $_countryoid;
    private $_stateoid;
    private $_cityoid;
    private $_hoteloid;

    function __construct($conn, $profile=null, $hoteloid=null,
                        $cityoid=null, $stateoid=null, $countryoid=null, $continentoid=null) {

        $this->_conn = $conn;
        $this->_profile = $profile;
        $this->_inquiry = $profile['inquiry_status'];

        if(!is_null($hoteloid)) $this->_hoteloid = mysqli_real_escape_string($conn,$hoteloid);
        if(!is_null($cityoid)) $this->cityoid = mysqli_real_escape_string($conn,$cityoid);
        if(!is_null($stateoid)) $this->stateoid = mysqli_real_escape_string($conn,$stateoid);
        if(!is_null($countryoid)) $this->countryoid = mysqli_real_escape_string($conn,$countryoid);
        if(!is_null($continentoid)) $this->_hoteloid = mysqli_real_escape_string($conn,$continentoid);


        if(!empty($_GET["q"])) $this->_keyword = mysqli_real_escape_string($conn,$_GET["q"]);
        if(!empty($_GET["adult"])) $this->_adult = mysqli_real_escape_string($conn,$_GET["adult"]);
        if(!empty($_GET["promo"])) $this->_promo = mysqli_real_escape_string($conn,$_GET["promo"]);
        if(!empty($_GET["hotelname"])) $this->_hotelname = mysqli_real_escape_string($conn,$_GET["hotelname"]);
        if(!empty($_GET["hoteltype"])) $this->_hoteltypes = mysqli_real_escape_string($conn,$_GET["hoteltype"]);
        if(!empty($_GET["stars"])) $this->_stars = mysqli_real_escape_string($conn,$_GET["stars"]);
        if(!empty($_GET["price"])) $this->_ranges = mysqli_real_escape_string($conn,$_GET["price"]);
        if(!empty($_GET["facilities"])) $this->_facilities = mysqli_real_escape_string($conn,$_GET["facilities"]);
        if(!empty($_GET["chain"])) $this->_chains = mysqli_real_escape_string($conn,$_GET["chain"]);


        if(!empty($_GET["checkin"])) {
            $this->_checkin = mysqli_real_escape_string($conn,$_GET["checkin"]);
        }else{
            $this->_checkin = (string)date("Y-m-d", strtotime("+1 day"));
        }

        if(!empty($_GET["night"])) {
            $this->_night = mysqli_real_escape_string($conn,$_GET["night"]);
        }else{
            $this->_night = 2;
        }

        if(!empty($_GET["page"])) $this->_page_number = mysqli_real_escape_string($conn,$_GET["page"]);

        $sort_by = (!empty($_GET['sortby']))?str_replace('-',' ',$_GET['sortby']):'';
        $this->_sort_by = mysqli_real_escape_string($conn,$sort_by);

    }


    function getQueryTopReviewed(){
        $query = "SELECT hotelname, hoteloid, hotelcode, stars,
                        hotel.address, cityname,
                        AVG(hotelreview.star) AS average_review,
                        COUNT(hotelreview.reviewoid) as total_review
                    FROM hotel
                        INNER JOIN hotelreview USING(hoteloid)
                        INNER JOIN city USING (cityoid)
                    WHERE hotel.publishedoid in (1)
                        AND hotel.hotelstatusoid in (1)
                    GROUP BY hoteloid
                    ORDER BY average_review, total_review
                    LIMIT 5";
        return $query;
    }

    function getQueryTopStar(){
        $query = "SELECT hotelname, hoteloid, hotelcode, stars,
                        hotel.address, cityname, statename, description
                    FROM hotel
                        INNER JOIN city USING (cityoid)
                        INNER JOIN state USING (stateoid)
                    WHERE hotel.publishedoid in (1)
                        AND hotel.hotelstatusoid in (1)
                    GROUP BY hoteloid
                    ORDER BY stars DESC
                    LIMIT 8";
        return $query;
    }


    function getQueryContent($hoteloid){
        $query = "SELECT * FROM hotelcontent hc
                    WHERE hoteloid IN (".join(',',$hoteloid).") AND hc.languageoid = 1";
        return $query;
    }

    function getQueryPhoto($hoteloid){
        // $query = "SELECT *
        //             FROM hotelphoto hp
        //             WHERE hp.flag = 'main'
        //                 AND hp.ref_table = 'hotel'
        //                 AND hoteloid in (".join(',',$hoteloid).")";
        $query = "SELECT '-' AS hotelphotooid, hoteloid, banner AS photourl, banner AS thumbnailurl, 
                    'main' AS flag, 'n' AS flag_flexible_rate, 'hotel-banner' AS ref_table, hoteloid AS ref_id 
                    FROM hotel
                    where hoteloid IN (".join(',',$hoteloid).")";
        return $query;
    }

    function getQueryRoom($hoteloid){
        $query = "SELECT ro.name as roomname, ro.roomofferoid, r.roomoid, r.hoteloid
                    FROM room r
                    INNER JOIN roomoffer ro using (roomoid)
                    WHERE r.hoteloid in (".join(',',$hoteloid).") and ro.publishedoid = '1' and r.publishedoid = '1'
                    ORDER BY r.hoteloid";
        return $query;
    }
    function getQueryType($hoteltypeoid){
        $query = "SELECT hoteltypeoid, category, icon
                        FROM hoteltype
                        WHERE hoteltypeoid in (".join(',',$hoteltypeoid).")
                        ";//ORDER BY category";
        return $query;
    }

    function getQueryFacilities($hoteloid){
        $query = "SELECT*FROM
                        (SELECT facilities.facilitiesoid, facilities.name, COUNT(hoteloid) AS jmlhotel
                        FROM facilities
                            LEFT JOIN (SELECT hoteloid, facilitiesoid FROM hotel
                                INNER JOIN hotelfacilities USING (hoteloid)
                                WHERE hoteloid in (".join(',',$hoteloid).") ) HOTEL_TABLE
                            USING (facilitiesoid)
                        GROUP BY facilitiesoid
                        ORDER BY jmlhotel DESC, name ASC) TEMP_TABLE
                    WHERE jmlhotel>0
                    ORDER BY jmlhotel DESC,name ASC
                    LIMIT 15";//
        return $query;
    }
    function getQueryChain($chainoid){
        $query="SELECT chainoid, name
                    FROM chain
                    WHERE chainoid in (".join(',',$chainoid).")
                    ";//GROUP BY chainoid";
        return $query;
    }
    function getQueryReviewedStar($hoteloid){
        $query="SELECT avg(star) as reviewedstar, hoteloid
                FROM hotelreview
                where hoteloid in (".join(',',$hoteloid).")
                group by hoteloid
                    ";//GROUP BY chainoid";
        return $query;
    }
    function getQueryCheckFacility($hoteloid){
        $query="SELECT hoteloid
                FROM hotel
                INNER JOIN hotelfacilities USING (hoteloid)
                WHERE hotel.hoteloid  IN (".join(',',$hoteloid).")
                    AND hotelfacilities.facilitiesoid IN (".$this->_facilities.")";
        return $query;
    }
    function getQueryPromo($channeloid,$hoteloid){
        $todaydate='2018-07-01';$todayday="Sun";
        $todaydate=date("Y-m-d");
        $checkin = date("Y-m-d",strtotime($this->_checkin." +0 day"));
        $checkout = date("Y-m-d",strtotime($this->_checkin." +".$this->_night." day"));
        $checkinday = date("D",strtotime($checkin));
        $query = "SELECT p.promotionoid, p.priority, p.name as promoname, p.discounttypeoid,
                    p.discountapplyoid, p.applyvalue, p.discountvalue, 
                    #pt.type, 
                    ro.name as roomname,ro.roomofferoid, r.roomoid, h.hotelname, h.hoteloid
                    FROM promotion p
                        #inner join promotiontype pt using (promotiontypeoid)
                        inner join discounttype dt using (discounttypeoid)
                        inner join hotel h using (hoteloid)
                        inner join promotionapply pa using (promotionoid)
                        inner join roomoffer ro using (roomofferoid)
                        inner join offertype ot using (offertypeoid)
                        inner join room r using (roomoid)
                        inner join channel ch using (channeloid)
                    WHERE h.hoteloid in (".join(',',$hoteloid).") and ch.channeloid = '".$channeloid."'
                        and (salefrom <= '".$todaydate."' and saleto >= '".$todaydate."')
                        and (bookfrom <= '".$checkin."' and bookto >= '".$checkout."')
                        and (DATE_FORMAT(DATE_ADD('".$checkin."', INTERVAL -1 * min_bef_ci DAY), '%Y-%m-%d') >= '".$todaydate."')
                        and IF(max_bef_ci>0,(DATE_FORMAT(DATE_ADD('".$checkin."', INTERVAL -1 * max_bef_ci DAY), '%Y-%m-%d') <= '".$todaydate."'),TRUE) = TRUE
                        and p.displayed like '%".$todayday."%' and p.checkinon like '%".$checkinday."%'
                        and p.minstay <= ".$this->_night." 
                        and IF(p.maxstay >0,(p.maxstay >= ".$this->_night." ),TRUE) = TRUE
                        and p.publishedoid = '1' and ro.publishedoid = '1' and r.publishedoid = '1'
                        and ch.channeloid = '".$channeloid."'
                    ORDER BY h.hoteloid";
        // echo $query.'<br>';
        // Func::logJS($query);
        return $query;
    }

    function getQueryList($select=null){
        /* SELECT */
        //$query = "SELECT hotel.*,
        $query = "SELECT hotel.hoteloid, hotel.chainoid, hotel.hotelcode, hotel.hotelname,
                hotel.address, hotel.cityoid, hotel.latitude, hotel.longitude, hotel.gmap,
                hotel.stars, hotel.hoteltypeoid, hotel.description,
                cityname, statename, continentname, countryname, hoteltype.category";// ";

        if(!is_null($select)){
            if($select=='count') $query = "SELECT count(*) as total ";
            if($select=='idx') $query = "SELECT hotel.hoteloid ";
        }

        /* FROM */
        $query .= " FROM hotel ";

        /* JOIN */
        $query .= " INNER JOIN hoteltype USING (hoteltypeoid) ";
        //$query .= (!empty($this->_facilities)) ? " INNER JOIN hotelfacilities USING (hoteloid) " : "" ;
        $query .= " INNER JOIN city USING (cityoid)
                    INNER JOIN state USING (stateoid)
                    INNER JOIN country USING (countryoid)
                    INNER JOIN continent USING (continentoid) ";

        /* WHERE */
        $query .= " WHERE hotel.publishedoid in (1)
                    and hotel.hotelstatusoid in (1)
                    and (continentname LIKE '%$this->_keyword%'
                    OR countryname LIKE '%$this->_keyword%'
                    OR statename LIKE '%$this->_keyword%'
                    OR cityname LIKE '%$this->_keyword%'
                    OR hotelname LIKE '%$this->_keyword%') ";
        $query .= (!empty($this->_continentoid)) ? " AND continent.continentoid IN (".$this->_continentoid.") " : "" ;
        $query .= (!empty($this->_cityoid)) ? " AND city.cityoid IN (".$this->_cityoid.") " : "" ;
        $query .= (!empty($this->_stateoid)) ? " AND state.stateoid IN (".$this->_stateoid.") " : "" ;
        $query .= (!empty($this->_countryoid)) ? " AND country.countryoid IN (".$this->_countryoid.") " : "" ;
        $query .= (!empty($this->_hotelname)) ? " AND hotel.hotelname LIKE '%".$this->_hotelname."%' " : "" ;
        //$query .= (!empty($this->_stars)) ? " AND hotel.stars IN (".$this->_stars.") " : "" ;
        //$query .= (!empty($this->_hoteltypes)) ? " AND hoteltype.hoteltypeoid IN (".$this->_hoteltypes.") " : "" ;
        //$query .= (!empty($this->_facilities)) ? " AND hotelfacilities.facilitiesoid IN (".$this->_facilities.") " : "" ;
        //$query .= (!empty($this->_chains)) ? " AND chainoid IN (".$this->_chains.") " : "" ;

        /* GROUP */
        $query .= " GROUP BY hoteloid ";

        /* ORDER */
        if(!empty($this->_sort_by)){
            $field =explode(" ",$this->_sort_by)[0];
            if($field!='minrate'&&$field!='reviewed'){
                $query .= " ORDER BY ".$this->_sort_by;
            }
        }

        return $query;
    }

    function loadPhotoContent($data_hotels){
        $hotels=$data_hotels->data;
        $ids_hotel = array(); $results_photo = array(); $results_content = array();
        foreach($hotels as $hotel){ array_push($ids_hotel,$hotel['hoteloid']); }

        if(sizeof($ids_hotel)>0){
            //load DB: get photo ---------------------------------
            $res = $this->_conn->query($this->getQueryPhoto($ids_hotel));// or die($conn->error);
            while ( $row = $res->fetch_assoc() ) { $results_photo[]  = $row; }
            $res->free(); unset($res);
            //echo json_encode($results_photo);

            //load DB: get content ---------------------------------
            $res = $this->_conn->query($this->getQueryContent($ids_hotel));// or die($conn->error);
            while ( $row = $res->fetch_assoc() ) { $results_content[]  = $row; }
            $res->free(); unset($res);
            //echo json_encode($results_content);

            foreach($hotels as $key => $hotel){
                foreach($results_photo as $photo){
                    if($hotels[$key]['hoteloid']==$photo['hoteloid']){
                        $hotels[$key]['hotelpicture']=$photo['photourl'];
                    }
                }
                foreach($results_content as $content){
                    if($hotel['hoteloid']==$content['hoteloid']){
                        $hotels[$key]['hotelheadline']=!empty($content['headline']) ? "&quot;".$content['headline']."&quot;" : "" ;
                    }
                }
            }
            $data_hotels->data=$hotels;
        }
        return $data_hotels;
    }
    function getFilterData($all_hotels){
        $results = array();

        $this->_stars_array = explode(',',$this->_stars);
        $this->_hoteltypes_array = explode(',',$this->_hoteltypes);
        $this->_chains_array = explode(',',$this->_chains);
        $this->_ranges_array = explode(',',$this->_ranges);

        //filter by star,type,chain,price
        foreach($all_hotels as $a => $hotel){
            $sts=true;

            // print_r($_stars_array);
            if(!empty($this->_stars)){ if(!in_array($hotel['stars'],$this->_stars_array)) { $sts=false; } }
            if(!empty($this->_hoteltypes)){ if(!in_array($hotel['hoteltypeoid'],$this->_hoteltypes_array)) { $sts=false; } }
            if(!empty($this->_chains)){ if(!in_array($hotel['chainoid'],$this->_chains_array)) { $sts=false; } }
            if(!empty($this->_ranges)){
                $key=(int)$this->_ranges;
                $val=$hotel['minrate'];
                if(in_array(1,$this->_ranges_array))if($val > 499999||$val <= 0) $sts=false;
                if(in_array(2,$this->_ranges_array))if($val > 999999||$val <= 499999) $sts=false;
                if(in_array(3,$this->_ranges_array))if($val > 1499999||$val <= 999999) $sts=false;
                if(in_array(4,$this->_ranges_array))if($val > 1999999||$val <= 1499999) $sts=false;
                if(in_array(5,$this->_ranges_array))if($val <= 1999999) $sts=false;
            }
            if($sts) array_push($results,$hotel);
        }

        $ids_hotel = array();
        foreach($results as $hotel){ array_push($ids_hotel,$hotel['hoteloid']); }

        //load DB: check facilities ---------------------------------
        if(!empty($this->_facilities) && sizeof($ids_hotel)>0){
            $res = $this->_conn->query($this->getQueryCheckFacility($ids_hotel));// or die($conn->error);
            while ( $row = $res->fetch_assoc() ) { $results_facility[]  = $row; }
            $res->free(); unset($res); //echo json_encode($results_photo);
            foreach($results as $a => $hotel){
                $sts=false;
                foreach($results_facility as $key => $value){
                    if($value['hoteloid']==$hotel['hoteloid']){$sts=true;}
                }
                if(!$sts){ $results[$a]=''; }
            }
        }

        //remove empty array
        $results = array_filter($results);

        //get reviewed score
        if(sizeof($ids_hotel)>0){
            $res = $this->_conn->query($this->getQueryReviewedStar($ids_hotel));// or die($conn->error);
            while ( $row = $res->fetch_assoc() ) { $results_reviewed[]  = $row; }
            $res->free(); unset($res);
            if(sizeof($results)>0)
            foreach($results as $a => $hotel){
                if(is_array($results_reviewed))
                if(sizeof($results_reviewed)>0)
                foreach($results_reviewed as $key => $value){
                    if($value['hoteloid']==$hotel['hoteloid']){
                        $results[$a]['reviewed']=$value['reviewedstar'];
                    }
                }
            }
        }

        //sort data
        if(!empty($this->_sort_by)){
            $sort_by = explode(" ",$this->_sort_by);
            $sort_field = $sort_by[0];
            $sort_order = $sort_by[1];
            $sort=array();
            foreach($results as $key => $value){
                $sort[$key]=$value[$sort_field];
            }
            if($sort_order=='asc'){
                array_multisort($sort,SORT_ASC,$results);
            }else{
                array_multisort($sort,SORT_DESC,$results);
            }
        }
        return $results;
    }
    function getData($channeloid){
        //load DB: get list of hoteloid from filter ---------------------------------
        $res = $this->_conn->query($this->getQueryList());// or die($conn->error);
        while ( $row = $res->fetch_assoc() ) { $results_hotel[]  = $row; }
        $res->free(); unset($res);
        // echo json_encode($results_hotel);
        //load DB: get active's room from array hoteloid ---------------------------------
        $ids_hotel = array();
        foreach($results_hotel as $hotel){ array_push($ids_hotel,$hotel['hoteloid']); }

        if(sizeof($ids_hotel)>0){
            $resRoom = $this->_conn->query($this->getQueryRoom($ids_hotel));// or die($conn->error);
            while ( $row = $resRoom->fetch_assoc() ) { $results_room[]  = $row; }
            $resRoom->free(); unset($resRoom);
        }
        //echo json_encode($results_room);

        $tmp_hotels = array();
        foreach($results_hotel as $hotel){

            //Load XML : Allotment & Rate ---------------------------------
            $tmp_rooms = array();
        // echo json_encode(simplexml_load_file("data-xml/".$hotel['hotelcode'].".xml"));
            if(file_exists("myhotel/data-xml/".$hotel['hotelcode'].".xml")){
                $xml =  simplexml_load_file("myhotel/data-xml/".$hotel['hotelcode'].".xml");
                //check file per hotel by $hotel['hotelcode'] 
                // echo '<br>$hotelcode:'.$hotel['hotelcode'].'';

                foreach($results_room as $room){
                    if($room['hoteloid']==$hotel['hoteloid']){
                        //loop total stay date
                        $tmp_rates=array();
                        for($n=0;$n<(int)$this->_night;$n++){
                            $n_last=(int)$this->_night-1;
                            $date = date("Y-m-d",strtotime($this->_checkin." +".$n." day"));
                            if (is_array($xml) || is_object($xml)){
                                $query_rate = '//hotel[@id="'.$room['hoteloid'].'"]/masterroom[@id="'.$room['roomoid'].'"]/rate[@date="'.$date.'"]';
                                $xpath_allotment = $xml->xpath($query_rate.'/allotment/channel[@type="'.$channeloid.'"]')[0];
                                $xpath_rateplan = $xml->xpath($query_rate.'/rateplan[@id="'.$room['roomofferoid'].'"]/channel[@type="'.$channeloid.'"]')[0];
                                
                                $closeout='n';
                                $xpath_closeout = '//hotel[@id="'.$room['hoteloid'].'"]/masterroom[@id="'.$room['roomoid'].'"]/rate[@date="'.$date.'"]/closeout/text()';
                                foreach($xml->xpath($xpath_closeout) as $tagcloseout){
                                    $closeout = $tagcloseout;
                                }
                                if(!isset($closeout)) $closeout='n';

                                if((int)$xpath_rateplan->double>0){
                                    $tmp=array();
                                    $tmp['date']=$date;
                                    $tmp['closeout']=$closeout;
                                    $tmp['allotment'] = (string)$xpath_allotment[0];
                                    //$tmp['rateplan'][]=$xpath_rateplan;
                                    foreach($xpath_rateplan as $key => $value){
                                        //echo $key.':'.$value.'<br>';
                                        $tmp[$key] = (string)$value;
                                    }
                                    // Func::logJS($tmp,'loop xml');
                                    if($closeout=='n' && $tmp['blackout']=='n' && !($n==0 && $tmp['cta']=='y') && !($n==$n_last && $tmp['ctd']=='y') ){
                                        array_push($tmp_rates,$tmp);
                                    }
                                }
                                
                            }
                        }
                        if(sizeof($tmp_rates)==(int)$this->_night){
                            $tmp_allotment=(int)$tmp_rates[0]['allotment'];

                            $tmp=array();
                            $tmp['roomoid'] = $room['roomoid'];
                            $tmp['roomofferoid'] = $room['roomofferoid'];
                            $tmp['roomname'] = $room['roomname'];
                            $tmp['allotment'] = $tmp_allotment;

                            //only need first[0] date rates
                            foreach($tmp_rates[0] as $key => $value){
                                $tmp[$key] = (string)$value;
                            }

                            //get available allotment
                            // if($tmp_allotment>0){
                            //     $r_occupied = $this->getRoomOccupied( $room['roomofferoid'],date("Y-m-d",strtotime($this->_checkin)),$channeloid);
                            //     $available = $tmp_allotment  - (int)$r_occupied['occupied'];
                            //     $tmp['allotment'] = $available;
                            // }

                            if($this->_inquiry=='y' || ($this->_inquiry=='n' && (int)$tmp['allotment']>0) ){
                                array_push($tmp_rooms,$tmp);
                            }
                        }
                        // echo json_encode($tmp_rooms);
                    }
                }
            }


            //load DB: get list of promo ---------------------------------

            if(sizeof($tmp_rooms)>0){
                $sts=true;
                if(!empty($this->_stars)){ if($this->_stars!=$hotel['stars']) { $sts=false; } }
                if(!empty($this->_hoteltypes)){ if($this->_hoteltypes!=$hotel['hoteltypeoid']) { $sts=false; } }
                if(!empty($this->_chains)){ if($this->_chains!=$hotel['chainoid']) { $sts=false; } }
                if(!empty($this->_ranges)){
                    $key=(int)$this->_ranges;
                    $val=$hotel['minrate'];
                    if($key===1)if($val > 499999||$val <= 0) $sts=false;
                    if($key===2)if($val > 999999||$val <= 499999) $sts=false;
                    if($key===3)if($val > 1499999||$val <= 999999) $sts=false;
                    if($key===4)if($val > 1999999||$val <= 1499999) $sts=false;
                    if($key===5)if($val <= 1999999) $sts=false;
                }

                $roomcurrency = "IDR";
                $allminrate = array(); $allnetrate = array();   $allallotment = array();  
                $offername = array(); $offerid = array(); $promoname = array(); $price = array();

                if($sts){
                    $resPromo = $this->_conn->query($this->getQueryPromo($channeloid,array($hotel['hoteloid'])));// or die($conn->error);
                    while ( $row = $resPromo->fetch_assoc() ) { $results_promo[]  = $row; }
                    $resPromo->free(); unset($resPromo);
                    //echo json_encode($results_promo);

                if(is_array($results_promo))
                if(sizeof($results_promo)>0)
                    foreach ($results_promo as $p => $promo) {
                        $roomoffer = $promo['roomofferoid'];
                        $discounttype = $promo['discounttypeoid'];
                        $discountapply = $promo['discountapplyoid'];
                        $applyvalue = $promo['applyvalue'];
                        $discountvalue = $promo['discountvalue'];
                        foreach ($tmp_rooms as $r => $room) {
                            if($room['roomofferoid']==$promo['roomofferoid']){
                                $single = $room['double'];
                                if($discountapply=='1'){
                                    if($discounttype == '1'){
                                        $discountnote = $discountvalue."%";
                                        $discount = $single * $discountvalue / 100;
                                    }else if($discounttype == '3'){
                                        $discountnote = $discountvalue;
                                        $discount = $discountvalue;
                                    }else if($discounttype == '2'){
                                        $discountnote = $discountvalue.'/Booking';
                                        $discount = $discountvalue/(int)$this->_night;
                                    }else{
                                        $discount = 0;
                                    }
                                }elseif($discountapply=='2'){
                                    $arrApply = explode(',',$discountapply);
                                    $arrNote = array();
                                    $arrRate = array();
                                    for($i=1;$i<=(int)$this->_night;$i++){
                                        $normal = true;
                                        for($j=0;$j<count($arrApply);$j++){ if($arrApply[$j]==$i){ $normal=false;} }
                                        if($normal){
                                            
                                        }else{

                                        }
                                    }
                                }
                                // ------------------------- pending ------------------------
                                $rate_after = $single - $discount;
                                // Func::logJS($promo['promoname'].'|'.$rate_after,'promo');
                                array_push($allallotment, $room['allotment']);
                                array_push($allminrate, $rate_after);
                                array_push($allnetrate, $single);
                                array_push($promoname, $promo['roomname']." ".$promo['promoname']);
                                array_push($offername, $promo['promoname']);
                                array_push($offerid, $promo['roomofferoid']);
                                array_push($price, $roomcurrency." ".number_format($rate_after, 0, ".", ","));
                            }
                        }
                    }
                }
                foreach ($tmp_rooms as $r => $room) {
                    array_push($allallotment, $room['allotment']);
                    array_push($allminrate, $room['double']);
                    array_push($allnetrate, $room['double']);
                    array_push($promoname, $room['roomname']);
                    array_push($offername, 'Best Flexible Rate');
                    array_push($offerid, $room['roomofferoid']);
                    array_push($price, $roomcurrency." ".number_format($room['double'], 0, ".", ","));
                }

                // logJS($allminrate,'allminrate');
                if(count($allminrate)>0){
                    $minrate = min($allminrate);
                    $hotel_minrate = number_format($minrate);
                    $key = array_search($minrate,$allminrate);
                    $netrate = $allnetrate[$key];
                    $minrate_promoname = $offername[$key]; //unused
                    $shownetrate = number_format($netrate);
                    
                    $max_availibility = 0; foreach($allallotment as $allot){ if($allot>$max_availibility)$max_availibility=$allot; }

                    if(strlen($hotel_minrate) > 5){
                        $hotel_minrate_first = substr($hotel_minrate, 0, strlen($hotel_minrate)-4);
                        $hotel_minrate_second = substr($hotel_minrate, -4);
                        $hotel_minrate_final = $hotel_minrate_first."<span class='thousand'>".$hotel_minrate_second."</span>";
                    }else{
                        $hotel_minrate_final = $hotel_minrate;
                    }
                }else{
                    $minrate = 0;
                }

                $currencycode = "IDR";

                // Func::logJS($minrate,'minrate');
                if($minrate>0){
                    

                    $tmp=array();
                    $tmp['hoteloid'] = $hotel['hoteloid'];
                    $tmp['hotelcode'] = $hotel['hotelcode'];
                    $tmp['hotelname'] = $hotel['hotelname'];
                    $tmp['chainoid'] = $hotel['chainoid'];
                    $tmp['hoteltypeoid'] = $hotel['hoteltypeoid'];
                    $tmp['hoteltype'] = $hotel['category'];
                    $tmp['stars'] = $hotel['stars'];
                    $tmp['address'] = $hotel['address'];
                    $tmp['city'] = $hotel['cityname'];
                    $tmp['state'] = $hotel['statename'];
                    $tmp['country'] = $hotel['countryname'];
                    $tmp['continent'] = $hotel['continentname'];

                    $tmp['reviewed'] = 0;
                    // $tmp['hotelpicture'] = '';
                    // $tmp['hotelheadline'] = '';
                    // $tmp['hotelhref'] = '';

                    $tmp['netrate'] = $netrate;
                    $tmp['minrate'] = $minrate;
                    $tmp['maxavailibility'] = $max_availibility;
                    $tmp['currencycode'] = $currencycode;
                    $tmp['shownetrate'] = $shownetrate;
                    $tmp['hotel_minrate_final'] = $hotel_minrate_final;
                    $tmp['allotment'][] = $allallotment;
                    $tmp['rooms'][] = $promoname;
                    $tmp['rates'][] = $price;
                    array_push($tmp_hotels,$tmp);
                }
            }
        }
        //echo '<br><br><br>'.json_encode($tmp_hotels);
        // Func::logJS($tmp_hotels);
        return $tmp_hotels;
    }
    
    function getRoomOccupied($roomoffer,$date,$channeloid){
        $query = "SELECT count(bookingroomoid) as occupied
                FROM bookingroom
                  INNER JOIN roomoffer ro using (roomofferoid)
                  INNER JOIN offertype ot using (offertypeoid)
                  INNER JOIN room r using (roomoid)
                  INNER JOIN channel ch using (channeloid)
                  INNER JOIN hotel using (hoteloid)
                  INNER JOIN booking using (bookingoid)
                  INNER JOIN bookingroomdtl using (bookingroomoid)
                WHERE ro.roomofferoid = '".$roomoffer."'
                    AND ch.channeloid = '".$channeloid."'
                    AND date = '".$date."';";
        $results = $this->_conn->query($query);// or die($conn->error);
        while ( $row = $results->fetch_assoc() ) { $results_array[]  = $row; }
        $results->free(); unset($results);
        return $results_array;
    }
    

}
