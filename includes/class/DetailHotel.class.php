<?php



class DetailHotel {

    public $_channeloid=1;

    private $_conn;
    private $_conn_pdo;
    // private $_hoteloid;
    // private $_chainoid;
    private $_checkin;
    private $_night;
    

    // ----- IBE CHECK UNIT

    public $_hoteloid;
    public $_hotelcode;
    public $_chainoid;
    public $_hotelname;
    public $_hotelstar;
    public $_hoteldesc;
    public $_hoteladdress;
    public $_hotelemail;
    public $_hotelphone;
    public $_hotelwebsite;
    public $_hotelcity;
    public $_hotel_wa_notif;
    public $_hotelicon;

    public $_hotellatitude;
    public $_hotellongitude;

    public $_hotelrooms;
    public $_hotelstatusoid;
    public $_hotelpaymentmethod;
    public $_hotelipgsetting;
    public $_hoteltemplate;
    public $_autoexpand;
    public $_default_auto_night;
    public $_default_show_bar;
    public $_default_sort_by;
    public $_default_show_room;
    public $_default_show_extra;

    public $_default_groupby;

    public $_socialmedia_url;
    public $_fblike_script;

    public $_hotelgooglemapsurl;

    public $_hotellogo;
    public $_hotelshowlogo;

    // public $_ibeparamroom;
    // public $_ibeparamperson;

    public $_hotelbanner;
    public $_fb;
    public $_subscribe_feature;

    // ----- IBE PARAMETERS
    public $_param_arrival;
    public $_param_departure;
    public $_param_arrivalname;
    public $_param_todaytime;
    public $_param_todayname;
    public $_param_today;
    public $_param_waiting_checkin;
    public $_param_default_arrival;
    public $_param_default_departure;
    public $_param_default_night;

    // ----- IBE LIST BREAKDOWN
    
    private $_listBreakdownDate;
    private $_listBreakdownRate;
    private $_listBreakdownCurrency;
    private $_listBreakdownDiscStat;
    private $_listBreakdownTotal;

    // ----- IBE LIST PACKAGE & PROMOTION

    private $_promotionid;
    private $_promotionname;
    private $_promotiontype;
    private $_typepromo;
    private $_promotionnight;
    private $_promotiontotal;
    private $_promotiontotal_min;
    private $_promotionimage;
    private $_promotionheadline;

    function setPDO($_conn_pdo) {
        $this->_conn_pdo = $_conn_pdo;
    }
    
    function __construct($conn, $hoteloid) {

        $this->_conn = $conn;
        $this->_channeloid = 1;
        $this->_hoteloid = $hoteloid;

        if(!empty($_GET["checkin"])) {
            $this->_checkin = mysqli_real_escape_string($conn,$_GET["checkin"]);
        }else{
            $this->_checkin = (string)date("Y-m-d", strtotime("+1 day"));
        }

        if(!empty($_GET["night"])) {
            $this->_night = mysqli_real_escape_string($conn,$_GET["night"]);
        }else{
            $this->_night = 2;
        }

    }
    //SINGLE ROW----------------------------------------------
    function getDetail(){
        // $query = "SELECT hotel.*, cityname, statename, countryname, hoteltype.category
        //             FROM hotel
        //                 INNER JOIN hoteltype USING (hoteltypeoid)
        //                 INNER JOIN city USING (cityoid)
        //                 INNER JOIN state USING (stateoid)
        //                 INNER JOIN country USING (countryoid)
        //             WHERE hotel.publishedoid in (1)
        //                 and hotel.hotelstatusoid in (1)
        //                 and hotel.hoteloid='".$this->_hoteloid."'
        //             GROUP BY hoteloid";
        // $run_h = mysqli_query($this->_conn, $query);
        // $row_h = mysqli_fetch_array($run_h);
        // $this->_chainoid=$row_h['chainoid'];
        // return $row_h;
        // -------------------
        $db= $this->_conn_pdo;
        $hoteloid= $this->_hoteloid;
    // $hotelcode = $_GET['hcode'];
    try {
        // $syntax_hotel = "select * from hotel inner join city using(cityoid) where hotelcode = '".$hotelcode."'";
        $syntax_hotel = "select * from hotel inner join city using(cityoid) where hoteloid = '".$hoteloid."'";
    //    Func::logJS($syntax_hotel);
        $stmt   = $db->query($syntax_hotel);
        $row_count = $stmt->rowCount();
        if($row_count > 0) {
            $result_hotel = $stmt->fetchAll(PDO::FETCH_ASSOC);
            foreach($result_hotel as $row){
                // Func::logJS($row);
                $this->_hoteloid        = $row['hoteloid'];
                $this->_hotelcode       = $row['hotelcode'];
                $this->_chainoid        = $row['chainoid'];
                $this->_hotelname       = $row['hotelname'];
                $this->_hotelstar       = $row['stars'];
                $this->_hoteldesc       = $row['description'];
                $this->_hoteladdress    = $row['address'];
                $this->_hotelemail      = $row['email'];
                $this->_hotelphone      = $row['phone'];
                $this->_hotelwebsite    = $row['website'];
                $this->_hotelcity       = $row['cityname'];
                $this->_hotel_wa_notif  = $row['wa_notif'];
                $this->_hotelicon       = $row['favicon'];

                $this->_hotellatitude       = $row['latitude'];
                $this->_hotellongitude      = $row['longitude'];

                $this->_hotelrooms          = $row['number_of_rooms'];
                $this->_hotelstatusoid      = $row['hotelstatusoid'];
                $this->_hotelpaymentmethod  = $row['paymentmethodoid'];
                $this->_hotelipgsetting = $row['ipgsetting'];
                $this->_hoteltemplate       = $row['template'];
                $this->_autoexpand          = $row['autoexpand'];
                $this->_default_auto_night  = $row['auto_night'];
                $this->_default_show_bar    = $row['show_bar'];
                $this->_default_sort_by = $row['sort_by'];
                $this->_default_show_room   = $row['show_room'];
                $this->_default_show_extra  = $row['show_extra'];
                
                $this->_default_groupby = $row['list_groupby'];
                if(!empty($this->_default_groupby) and !isset($_REQUEST['groupby'])){
                    $_REQUEST['groupby'] = $this->_default_groupby;
                }

                $this->_socialmedia_url = $row['socialmedia_url'];
                $this->_fblike_script = $row['fblike_script'];

                if(!empty($this->_hotellatitude) and !empty($this->_hotellongitude)){
                    $this->_hotelgooglemapsurl = "https://www.google.com/maps/embed/v1/place?q=".$this->_hotellatitude.",".$this->_hotellongitude."+&key=AIzaSyBFcO-3vNMI3e5W82jQT_nFTK2pXebZ4AE&center=".$this->_hotellatitude.",".$this->_hotellongitude."&zoom=10";
                }else{
                    $this->_hotelgooglemapsurl = "https://www.google.com/maps/embed/v1/place?q=".$this->_hotelname.", ".$this->_hotelcity."+&key=AIzaSyBFcO-3vNMI3e5W82jQT_nFTK2pXebZ4AE&zoom=10";
                }

                $this->_hotellogo = $row['logo'];
                $this->_hotelshowlogo = $row['showlogo'];

                // $this->_ibeparamroom = $row['ibeparam_room'];
                // $this->_ibeparamperson = $row['ibeparam_person'];

                if(!empty($row['banner'])){
                    $this->_hotelbanner     = $row['banner'];
                }else{
                    $this->_hotelbanner     = 'https://thebuking.com/ibe/image/header.jpg';
                }

                // if(!isset($_GET['t']) or (isset($_GET['t']) and empty($_GET['t']))){ $_GET['t'] = $hoteltemplate; }

                // if(isset($_GET['fb']) and $_GET['fb']=='1' and $_GET['t']=='1'){
                //  $this->_fb = 1;
                // }else{
                //  $this->_fb = 0;
                // }
                    $this->_fb = 0;
                    /*
                if($autoexpand == "y"){
                ?>
                    <style type="text/css">.room-item ul > li.another-promo, #room > ul.block > li.another-promo{ display:block!important; }</style>
                <?php
                }else{
                ?>
                    <style type="text/css">.room-item ul > li.another-promo, #room > ul.block > li.another-promo{ display:none!important; }</style>
                <?php
                }
                */

                if($this->_hotelstatusoid != '10' and $this->_hotelstatusoid != '1'){
                    // header("Location: https://thebuking.com/404");
                    die();
                }
            }

            // $s_existed_hotel_feature = "SELECT GROUP_CONCAT(featureoid) as existed from `hotelfeature` where `hoteloid` = '".$hoteloid."'";
            // $stmt = $db->query($s_existed_hotel_feature);
            // $r_existed_hotel_feature = $stmt->fetch(PDO::FETCH_ASSOC);
            // if(!empty($r_existed_hotel_feature['existed'])){
            //  $this->_subscribe_feature = explode(',', $r_existed_hotel_feature['existed']);
            // }else{
            //  $this->_subscribe_feature = array();
            // }
                $this->_subscribe_feature = array();
                // ----------------------------------------
            
                
        }else{
            // header("Location: https://thebuking.com/ibe/index.php?page=checkavailability&hcode=160104001");
            die();
        }
    }catch(PDOException $ex) {
        echo "<span class='notification'>Invalid Query</span>";
        die();
    }
    }

    function getPhoto(){
        // $query = "SELECT *
        //             FROM hotelphoto hp
        //             WHERE hp.flag = 'main'
        //                 AND hp.ref_table = 'hotel'
        //                 AND hoteloid = '".$this->_hoteloid."'";
        $query = "SELECT '-' AS hotelphotooid, hoteloid, banner AS photourl, banner AS thumbnailurl, 
                    'main' AS flag, 'n' AS flag_flexible_rate, 'hotel-banner' AS ref_table, hoteloid AS ref_id 
                    FROM hotel
                    where hoteloid = '".$this->_hoteloid."'
                    ";
        $result = mysqli_query($this->_conn, $query);
        $row = mysqli_fetch_array($result);
        return $row;
    }

    function getCurrency($currencyoid){
        $query = "SELECT *
                    FROM currency
                    WHERE publishedoid='1' AND currencyoid = '".$currencyoid."'";
        $result = mysqli_query($this->_conn, $query);
        $row = mysqli_fetch_array($result);
        return $row;
    }

    function getCurrencyList(){

        $query = "SELECT *
                    FROM currency
                    WHERE publishedoid='1'";

        $results = $this->_conn->query($query);// or die($conn->error);
        while ( $row = $results->fetch_assoc() ) { $results_array[]  = $row; }
        $results->free(); unset($results);
        return $results_array;
    }

    function getPhotoRoom($roomoid){
        $query = "SELECT photourl
              FROM hotelphoto hp
              WHERE hp.hoteloid = '".$this->_hoteloid."'
                  AND hp.flag = 'main'
                  AND ref_table = 'room'
                  AND ref_id = '".$roomoid."'";
        $result = mysqli_query($this->_conn, $query);
        $row = mysqli_fetch_array($result);
        return $row;
    }

    function getGallery(){
        $query = "SELECT *
                    FROM hotelphoto hp
                    WHERE hp.ref_table = 'hotel'
                        AND hoteloid = '".$this->_hoteloid."'";
        $results = $this->_conn->query($query);// or die($conn->error);
        while ( $row = $results->fetch_assoc() ) { $results_array[]  = $row; }
        $results->free(); unset($results);
        return $results_array;
    }

    //MULTI ROW----------------------------------------------
    function getFacilityCategories(){
        $query = "SELECT ft.*
                    FROM facilitiestype ft
                        INNER JOIN facilities f USING (facilitiestypeoid)
                    WHERE facilitiesoid
                        IN (SELECT facilitiesoid
                            FROM hotelfacilities hf
                                INNER JOIN hotel h USING (hoteloid)
                            WHERE h.hoteloid = '".$this->_hoteloid."')
                    GROUP BY facilitiestypeoid";
        // Func::logJS($query,'getFacilityCategories query');
        $results = $this->_conn->query($query);// or die($conn->error);
        while ( $row = $results->fetch_assoc() ) { $results_array[]  = $row; }
        $results->free(); unset($results);
        return $results_array;
    }

    function getFacilities($facilities_categoryoid){
        //this function should be no needed in the future or replace with better logic
        $query = "SELECT f.facilitiesoid, f.name , COUNT(hoteloid) AS jmlhotel
                    FROM facilities f
                        INNER JOIN facilitiestype ft USING (facilitiestypeoid)
                        LEFT JOIN (
                            SELECT hoteloid, facilitiesoid FROM hotel h INNER JOIN hotelfacilities hf USING (hoteloid)
                            WHERE h.hoteloid = '".$this->_hoteloid."'
                        ) TEMP_TABLE
                        USING (facilitiesoid)
                    WHERE ft.facilitiestypeoid = '".$facilities_categoryoid."'
                    GROUP BY facilitiesoid ORDER BY name ASC";
        $results = $this->_conn->query($query);// or die($conn->error);
        while ( $row = $results->fetch_assoc() ) { $results_array[]  = $row; }
        $results->free(); unset($results);
        return $results_array;
    }

    function getNearbyHotels(){
        $query = "SELECT hotel.hotelname, hotel.hoteloid, hotel.hotelcode,
                        hotel.stars, hotel.address,hotel.number_of_rooms,city.cityname
                    FROM hotel as hotel
                        INNER JOIN city using(cityoid)
                        INNER JOIN hotel as basedon using(cityoid)
                    WHERE  hotel.publishedoid in (1)
                        AND hotel.hotelstatusoid in (1)
                        AND basedon.hoteloid = '".$this->_hoteloid."'
                    GROUP BY hoteloid
                    LIMIT 10";
        $results = $this->_conn->query($query);// or die($conn->error);
        while ( $row = $results->fetch_assoc() ) { $results_array[]  = $row; }
        $results->free(); unset($results);
        return $results_array;
    }


    function getRoomOffer($roomofferoids){
        $list_ro_id='';
        if(count($roomofferoids) > 0){ $list_ro_id = implode(",", $roomofferoids); }

        $query = "SELECT ro.name as roomname, ro.roomofferoid,
                        r.roomoid, r.name as roommaster, r.adult, r.child, r.description, r.roomsize,
                        h.hoteloid, h.hotelcode
                    FROM hotel h
                        INNER JOIN room r using (hoteloid)
                        INNER JOIN roomoffer ro using (roomoid)
                        INNER JOIN offertype ot using (offertypeoid)
                    WHERE h.hoteloid = '".$this->_hoteloid."'
                        AND ro.publishedoid = '1'
                        AND r.publishedoid = '1'
                        AND ro.roomofferoid in (".$list_ro_id.")";
            //echo '<script>console.log('.json_encode($query).');</script>';
        $results = $this->_conn->query($query);// or die($conn->error);
        while ( $row = $results->fetch_assoc() ) { $results_array[]  = $row; }
        $results->free(); unset($results);
        return $results_array;
    }

    function getRoomOffer_Promo($roomofferoids,$channeloid){
        $todaydate=(date("Y-m-d") >= "2013-02-15") ? date("Y-m-d") : "2013-02-15" ;
        $checkin = date("Y-m-d",strtotime($this->_checkin));
        $checkinday = date("D",strtotime($this->_checkin));
        $todayday = date("D",strtotime($todaydate));
            $tmp = new DateTime($this->_checkin);
            $tmp->modify('+'.$this->_night.' day');
            $checkout = $tmp->format('Y-m-d');

        $list_ro_id='';
        if(count($roomofferoids) > 0){ $list_ro_id = implode(",", $roomofferoids); }

        $query = "SELECT p.promotionoid, p.priority, p.name as promoname, p.discounttypeoid, p.discountapplyoid,
                      p.applyvalue, p.discountvalue, p.minstay, p.maxstay, pt.type, pa.promotionapplyoid,
                        ro.name as roomname, ro.roomofferoid, r.roomoid, r.name as roommaster,
                      r.adult, r.child, r.description, r.roomsize, h.hoteloid,h.hotelcode
                  FROM promotion p
                        inner join promotiontype pt using (promotiontypeoid)
                        inner join discounttype dt using (discounttypeoid)
                        inner join hotel h using (hoteloid)
                        inner join promotionapply pa using (promotionoid)
                        inner join roomoffer ro using (roomofferoid)
                        inner join offertype ot using (offertypeoid)
                        inner join room r using (roomoid)
                        inner join channel ch using (channeloid)
                    WHERE h.hoteloid = '".$this->_hoteloid."'
                      and ch.channeloid = '".$channeloid."'
                      and (salefrom <= '".$todaydate."' and saleto >= '".$todaydate."')
                      and (bookfrom <= '".$checkin."' and bookto >= '".$checkout."')
                        and p.minstay <= '".$this->_night."'
                        and p.displayed like '%".$todayday."%' and p.checkinon like '%".$checkinday."%'
                        and p.publishedoid = '1' and ro.publishedoid = '1' and r.publishedoid = '1'
                      and ro.roomofferoid in (".$list_ro_id.")
                  ORDER BY p.priority";
                  // echo '<script>console.log('.json_encode($query).');</script>';
        $results = $this->_conn->query($query);// or die($conn->error);
        while ( $row = $results->fetch_assoc() ) { $results_array[]  = $row; }
        $results->free(); unset($results);
        return $results_array;
    }

    function getRoomOfferAll(){
        $query = "SELECT ro.roomofferoid, r.roomoid, h.hoteloid, h.hotelcode
                  FROM hotel h
                      INNER JOIN room r using (hoteloid)
                      INNER JOIN roomoffer ro using (roomoid)
                      INNER JOIN offertype ot using (offertypeoid)
                  WHERE h.hoteloid = '".$this->_hoteloid."'
                       AND ro.publishedoid = '1'
                       AND r.publishedoid = '1'
                 GROUP BY ro.roomofferoid";
        $results = $this->_conn->query($query);// or die($conn->error);
        while ( $row = $results->fetch_assoc() ) { $results_array[]  = $row; }
        $results->free(); unset($results);
        return $results_array;
    }
    function getRoomOfferAll_Promo($channeloid){
        $todaydate=(date("Y-m-d") >= "2013-02-15") ? date("Y-m-d") : "2013-02-15" ;
        $checkin = date("Y-m-d",strtotime($this->_checkin));
        $checkinday = date("D",strtotime($this->_checkin));
        $todayday = date("D",strtotime($todaydate));
            $tmp = new DateTime($this->_checkin);
            $tmp->modify('+'.$this->_night.' day');
            $checkout = $tmp->format('Y-m-d');

        $query = "SELECT ro.roomofferoid, r.roomoid, h.hoteloid,h.hotelcode
                  FROM promotion p
                        inner join promotiontype pt using (promotiontypeoid)
                        inner join discounttype dt using (discounttypeoid)
                        inner join hotel h using (hoteloid)
                        inner join promotionapply pa using (promotionoid)
                        inner join roomoffer ro using (roomofferoid)
                        inner join offertype ot using (offertypeoid)
                        inner join room r using (roomoid)
                        inner join channel ch using (channeloid)
                    WHERE h.hoteloid = '".$this->_hoteloid."'
                      and ch.channeloid = '".$channeloid."'
                        and (salefrom <= '".$todaydate."' and saleto >= '".$todaydate."')
                      and (bookfrom <= '".$checkin."' and bookto >= '".$checkout."')
                        and p.minstay <= '".$this->_night."'
                        and p.displayed like '%".$todayday."%'
                      and p.checkinon like '%".$checkinday."%'
                        and p.publishedoid = '1'
                      and ro.publishedoid = '1'
                      and r.publishedoid = '1'
                  GROUP BY ro.roomofferoid";
            // echo '<script>console.log('.json_encode($query).');</script>';
        $results = $this->_conn->query($query);// or die($conn->error);
        while ( $row = $results->fetch_assoc() ) { $results_array[]  = $row; }
        $results->free(); unset($results);
        return $results_array;
    }

    function getRoomOccupied($roomoffer,$date,$channeloid){
        $query = "SELECT count(bookingroomoid) as occupied
                FROM bookingroom
                  INNER JOIN roomoffer ro using (roomofferoid)
                  INNER JOIN offertype ot using (offertypeoid)
                  INNER JOIN room r using (roomoid)
                  INNER JOIN channel ch using (channeloid)
                  INNER JOIN hotel using (hoteloid)
                  INNER JOIN booking using (bookingoid)
                  INNER JOIN bookingroomdtl using (bookingroomoid)
                WHERE ro.roomofferoid = '".$roomoffer."'
                    AND ch.channeloid = '".$channeloid."'
                    AND date = '".$date."'";
            //  echo '<script>console.log('.json_encode($query).');</script>';
        $results = $this->_conn->query($query);// or die($conn->error);
        while ( $row = $results->fetch_assoc() ) { $results_array[]  = $row; }
        $results->free(); unset($results);
        return $results_array;
    }

    function getRoomRates($promo_status,$channeloid){
            $ROOMRATES = array();
            $ROOMRATES['roomofferoid'] = array();

            if($promo_status){
            $q_get_roomoffer=$this->getRoomOfferAll_Promo($channeloid);
            }else{
            $q_get_roomoffer=$this->getRoomOfferAll();
            }
            // echo '<script>console.log('.json_encode($q_get_roomoffer).');</script>';

            if (is_array($q_get_roomoffer) || is_object($q_get_roomoffer))
            foreach($q_get_roomoffer as $gr){
                $hotelcode = $gr['hotelcode'];
                $roomoid = $gr['roomoid'];
                $roomoffer = $gr['roomofferoid'];

                $ROOMRATES[$roomoffer]['hoteloid'] = $gr['hoteloid'];
                $ROOMRATES[$roomoffer]['roomoid'] = $gr['roomoid'];
                $ROOMRATES[$roomoffer]['roomofferoid'] = $gr['roomofferoid'];
                $ROOMRATES[$roomoffer]['rate'] = array();
                $ROOMRATES[$roomoffer]['currency'] = array();
                $ROOMRATES[$roomoffer]['rate_extrabed'] = array();
                $ROOMRATES[$roomoffer]['limitallotment'] = array();

                //Hotel Room Photo Main
                 $r_rp = $this->getPhotoRoom($gr['roomoid']);
                $room_pict = $r_rp['photourl'];
                $picture[$roomoffer] = $room_pict;

                $ROOMRATES[$roomoffer]['picture'] = $r_rp['photourl'];
          
                // echo '<script>console.log('.json_encode("../data-xml/".$hotelcode.".xml").');</script>';
                // echo '<script>console.log('.json_encode(file_exists("../en/data-xml/".$hotelcode.".xml")).');</script>';
                if(file_exists("myhotel/data-xml/".$hotelcode.".xml")){
                $xml = simplexml_load_file("myhotel/data-xml/".$hotelcode.".xml");
                // echo '<script>console.log('.json_encode($xml).');</script>';
                for($n=0;$n<$this->_night;$n++){

                        $date = date("Y-m-d",strtotime($this->_checkin." +".$n." day"));
                        $single=0;$extrabed=0;$allotment=0;

                        // $query_tag = '//hotel[@id="'.$this->_hoteloid.'"]/masterroom[@id="'.$roomoid.'"]/rate[@date="'.$date.'"]/rateplan[@id="'.$roomoffer.'"]/channel[@type="'.$channeloid.'"]';
                        // if(!empty($xml))
                        // foreach($xml->xpath($query_tag) as $tagrate){
                        // echo '<script>console.log('.json_encode($tagrate).');</script>';
                            
                        //  $single = (float)$tagrate->double;
                        //  $extrabed = (float)$tagrate->extrabed;
                        //  $currency = $tagrate->currency;
                        // }
                        // /*zc*/
                        // $query_tag2 = '//hotel[@id="'.$this->_hoteloid.'"]/masterroom[@id="'.$roomoid.'"]/rate[@date="'.$date.'"]/allotment/channel[@type="'.$channeloid.'"]';
                        // if(!empty($xml)){
                        // echo '<script>console.log("xml:");</script>';
                        // echo '<script>console.log('.json_encode($xml->xpath($query_tag2)).');</script>';
                        // echo '<script>console.log("allot:");</script>';
                        // foreach($xml->xpath($query_tag2) as $tagrate){
                        // echo '<script>console.log('.json_encode($tagrate).');</script>';
                        // $allotment = $tagrate[0];
                        //     }
                        // }
                        

                        $query_rate = '//hotel[@id="'.$this->_hoteloid.'"]/masterroom[@id="'.$roomoid.'"]/rate[@date="'.$date.'"]';
                        if (is_array($xml) || is_object($xml)){
                            $xpath_allotment = $xml->xpath($query_rate.'/allotment/channel[@type="'.$channeloid.'"]')[0];
                            $xpath_rateplan = $xml->xpath($query_rate.'/rateplan[@id="'.$roomoffer.'"]/channel[@type="'.$channeloid.'"]')[0];
                            // echo '<script>console.log('.json_encode($xpath_allotment).');</script>';
                            // echo '<script>console.log('.json_encode($xpath_rateplan).');</script>';
                            $allotment = (string)$xpath_allotment[0];
                            if((int)$allotment>0 && (int)$xpath_rateplan->double>0){
                                $single = (float)$xpath_rateplan->double;
                                $extrabed = (float)$xpath_rateplan->extrabed;
                                $currency = $xpath_rateplan->currency;
                            }
                        }

                        // echo '<script>console.log("('.$n.'):'.$this->_hoteloid."|".$roomoid."|".$roomoffer."|".$date."|".$allotment."|".$channeloid.'");</script>';
                        // $r_occupied = $this->getRoomOccupied($roomoffer,$date,$channeloid);
                        // $occupied = (int)$r_occupied['occupied'];
                        // echo '<script>console.log('.json_encode($r_occupied).');</script>';

                        // $available = $allotment - $occupied;
                        $available = $allotment;//- $occupied;
                        // $available = $allotment;// - $occupied;
                        //echo "(".$n.")".$available."-";

                        // echo '<script>console.log("('.$n.')'.$available."|".$allotment."|".$occupied."|".$single.'");</script>';
                        if($available > 0 and $single > 0){

                            if($n == 0){
                                 array_push($ROOMRATES['roomofferoid'],$roomoffer);
                                $ROOMRATES[$roomoffer]['limitallotment'] = $available;
                            }
                            if($available < $ROOMRATES[$roomoffer]['limitallotment']){
                                $ROOMRATES[$roomoffer]['limitallotment'] = $available;
                            }
                            if(!in_array($single, $ROOMRATES[$roomoffer]['rate'])){
                                array_push($ROOMRATES[$roomoffer]['currency'],$currency);
                                array_push($ROOMRATES[$roomoffer]['rate'],$single);
                             }

                            if(!in_array($extrabed, $ROOMRATES[$roomoffer]['rate_extrabed'])){
                                array_push($ROOMRATES[$roomoffer]['rate_extrabed'],$extrabed);
                            }

                        }else{
                            // unset($ROOMRATES[$roomoffer]['currency']);
                            // unset($ROOMRATES[$roomoffer]['rate']);
                            // unset($ROOMRATES[$roomoffer]['rate_extrabed']);
                            // unset($ROOMRATES[$roomoffer]['picture']);
                            // unset($ROOMRATES[$roomoffer]['limitallotment']);
                            unset($ROOMRATES[$roomoffer]);

                            if(($key = array_search($roomoffer, $ROOMRATES['roomofferoid'])) !== false) {
                                unset($ROOMRATES['roomofferoid'][$key]);
                            }
                            break;
                        }
                        //echo "(".$n.")". $extrabed."-";
                        
                    }
          }
        }
        //  echo '<script>console.log('.json_encode($ROOMRATES).');</script>';
        if(sizeof($ROOMRATES['roomofferoid'])>0){
          $currencyList=$this->getCurrencyList();
          foreach ($ROOMRATES as $key1 => $roomoffer) {
            if($key1!='roomofferoid'){
              $ROOMRATES[$key1]['currencycode']=array();
              foreach ($ROOMRATES[$key1]['currency'] as $code) {
                foreach ($currencyList as $curr) {
                  if($curr['currencyoid']==$code){
                    array_push($ROOMRATES[$key1]['currencycode'],$curr['currencycode']);
                  }
                }
              }
            }
          }

        //   echo '<script>console.log('.json_encode($ROOMRATES).');</script>';
  
          $AVAILABLERATES=array();
          if($promo_status){
          $AVAILABLERATES = $this->getAvailableRate_Promo($ROOMRATES,$channeloid);
        }else{
          $AVAILABLERATES = $this->getAvailableRate($ROOMRATES,$channeloid);
        }
          return $AVAILABLERATES;
        }else{
          unset($ROOMRATES['roomofferoid']);
          return $ROOMRATES;
        }
    }

    function getAvailableRate($ROOMRATES,$channeloid){
        $AVAILABLE_RATES = array();
        $count_AVAILABLE_RATES = 0;
        $ROOMRATES_OFFER = $this->getRoomOffer($ROOMRATES['roomofferoid']);
        // echo '<script>console.log('.json_encode($ROOMRATES_OFFER).');</script>';
        if(sizeof($ROOMRATES_OFFER)>0){
            foreach($ROOMRATES_OFFER as $room){
                if(sizeof($ROOMRATES[$room['roomofferoid']]['rate'])>0){
                    $row_id = $room['roomoid'].'-'.$room['roomofferoid'].'-N';
                    $single = $ROOMRATES[$room['roomofferoid']]['rate'][0];
                    $extrabedrate = number_format($ROOMRATES[$room['roomofferoid']]['rate_extrabed'][0]);
                    $currencycode = $ROOMRATES[$room['roomofferoid']]['currencycode'][0]; // 'IDR';
                    $picture = $ROOMRATES[$room['roomofferoid']]['picture'];
                    
                    $discount = 0;
                    $rate_after = $single - $discount;
                    $roomrate = number_format($rate_after);
                    $barrate = number_format($single);
                    $countrate++;

                    if($ROOMRATES[$room['roomofferoid']]['limitallotment'] > 5){
                        $default_allotment = 5;
                    }else{
                        $default_allotment = $ROOMRATES[$room['roomofferoid']]['limitallotment'];
                    }

                    $AVAILABLE_RATES[$count_AVAILABLE_RATES]['channeloid']=$channeloid;
                    $AVAILABLE_RATES[$count_AVAILABLE_RATES]['promo_status']=0;
                    $AVAILABLE_RATES[$count_AVAILABLE_RATES]['promoname']='';
                    $AVAILABLE_RATES[$count_AVAILABLE_RATES]['discounttypeoid']='';
                    $AVAILABLE_RATES[$count_AVAILABLE_RATES]['discountvalue']=0;
                    $AVAILABLE_RATES[$count_AVAILABLE_RATES]['discountnote']='';

                    $AVAILABLE_RATES[$count_AVAILABLE_RATES]['roomname']=$room['roomname'];
                    $AVAILABLE_RATES[$count_AVAILABLE_RATES]['adult']=$room['adult'];
                    $AVAILABLE_RATES[$count_AVAILABLE_RATES]['child']=$room['child'];
                    $AVAILABLE_RATES[$count_AVAILABLE_RATES]['roomoid']=$room['roomoid'];
                    $AVAILABLE_RATES[$count_AVAILABLE_RATES]['roomofferoid']=$room['roomofferoid'];
                    $AVAILABLE_RATES[$count_AVAILABLE_RATES]['promotionapplyoid']=$room['promotionapplyoid'];
                    $AVAILABLE_RATES[$count_AVAILABLE_RATES]['row_id']=$row_id;
                    $AVAILABLE_RATES[$count_AVAILABLE_RATES]['discount']=0;
                    $AVAILABLE_RATES[$count_AVAILABLE_RATES]['single']=$single;
                    $AVAILABLE_RATES[$count_AVAILABLE_RATES]['rate_after']=$rate_after;
                    $AVAILABLE_RATES[$count_AVAILABLE_RATES]['roomrate']=$roomrate;
                    $AVAILABLE_RATES[$count_AVAILABLE_RATES]['barrate']=$barrate;
                    $AVAILABLE_RATES[$count_AVAILABLE_RATES]['extrabedrate']=$extrabedrate;
                    $AVAILABLE_RATES[$count_AVAILABLE_RATES]['currencycode']= $currencycode;
                    $AVAILABLE_RATES[$count_AVAILABLE_RATES]['picture']=$picture;
                    $AVAILABLE_RATES[$count_AVAILABLE_RATES]['default_allotment']=$default_allotment;
                    $count_AVAILABLE_RATES++;
                    // foreach($ROOMRATES[$room['roomofferoid']]['rate'] as $key => $single){
                    //     $row_id = $room['roomoid'].'-'.$room['roomofferoid'].'-N';
                    //     $discount = 0;
                    //     $rate_after = $single - $discount;
                    //     $roomrate = number_format($rate_after);
                    //     $barrate = number_format($single);
                    //     $extrabedrate = number_format($ROOMRATES[$room['roomofferoid']]['rate_extrabed'][$key]);
                    //     $currencycode = $ROOMRATES[$room['roomofferoid']]['currencycode'][$key]; // 'IDR';
                    //     $picture = $ROOMRATES[$room['roomofferoid']]['picture'];
                    //     $countrate++;

                    //     if($ROOMRATES[$room['roomofferoid']]['limitallotment'] > 5){
                    //         $default_allotment = 5;
                    //     }else{
                    //         $default_allotment = $ROOMRATES[$room['roomofferoid']]['limitallotment'];
                    //     }

                    //     $AVAILABLE_RATES[$count_AVAILABLE_RATES]['channeloid']=$channeloid;
                    //     $AVAILABLE_RATES[$count_AVAILABLE_RATES]['promo_status']=0;
                    //     $AVAILABLE_RATES[$count_AVAILABLE_RATES]['promoname']='';
                    //     $AVAILABLE_RATES[$count_AVAILABLE_RATES]['discounttypeoid']='';
                    //     $AVAILABLE_RATES[$count_AVAILABLE_RATES]['discountvalue']=0;
                    //     $AVAILABLE_RATES[$count_AVAILABLE_RATES]['discountnote']='';

                    //     $AVAILABLE_RATES[$count_AVAILABLE_RATES]['roomname']=$room['roomname'];
                    //     $AVAILABLE_RATES[$count_AVAILABLE_RATES]['adult']=$room['adult'];
                    //     $AVAILABLE_RATES[$count_AVAILABLE_RATES]['child']=$room['child'];
                    //     $AVAILABLE_RATES[$count_AVAILABLE_RATES]['roomoid']=$room['roomoid'];
                    //     $AVAILABLE_RATES[$count_AVAILABLE_RATES]['roomofferoid']=$room['roomofferoid'];
                    //     $AVAILABLE_RATES[$count_AVAILABLE_RATES]['promotionapplyoid']=$room['promotionapplyoid'];
                    //     $AVAILABLE_RATES[$count_AVAILABLE_RATES]['row_id']=$row_id;
                    //     $AVAILABLE_RATES[$count_AVAILABLE_RATES]['discount']=0;
                    //     $AVAILABLE_RATES[$count_AVAILABLE_RATES]['single']=$single;
                    //     $AVAILABLE_RATES[$count_AVAILABLE_RATES]['rate_after']=$rate_after;
                    //     $AVAILABLE_RATES[$count_AVAILABLE_RATES]['roomrate']=$roomrate;
                    //     $AVAILABLE_RATES[$count_AVAILABLE_RATES]['barrate']=$barrate;
                    //     $AVAILABLE_RATES[$count_AVAILABLE_RATES]['extrabedrate']=$extrabedrate;
                    //     $AVAILABLE_RATES[$count_AVAILABLE_RATES]['currencycode']= $currencycode;
                    //     $AVAILABLE_RATES[$count_AVAILABLE_RATES]['picture']=$picture;
                    //     $AVAILABLE_RATES[$count_AVAILABLE_RATES]['default_allotment']=$default_allotment;
                    //     $count_AVAILABLE_RATES++;
                    // }
                }
            }
        }
        return $AVAILABLE_RATES;
    }

     function getAvailableRate_Promo($ROOMRATES_PROMO,$channeloid){
        $AVAILABLE_RATES_PROMO = array();
        $count_AVAILABLE_RATES_PROMO = 0;
            $ROOMRATES_PROMO_OFFER = $this->getRoomOffer_Promo($ROOMRATES_PROMO['roomofferoid'],$channeloid);
        // echo '<script>console.log('.json_encode($ROOMRATES_PROMO_OFFER).');</script>';
        if(sizeof($ROOMRATES_PROMO_OFFER)>0)
            foreach($ROOMRATES_PROMO_OFFER as $promo){
                $minstay = $promo['minstay'];
                $maxstay = $promo['maxstay'];
                if($maxstay==0 or ($maxstay!=0 and $this->_night<=$maxstay)) {
                    if(sizeof($ROOMRATES_PROMO[$promo['roomofferoid']]['rate'])>0){
                        foreach($ROOMRATES_PROMO[$promo['roomofferoid']]['rate'] as $key => $single){
                            $row_id= $promo['roomoid'].'-'.$promo['roomofferoid'].'-'.$promo['promotionoid'];
                            $discounttype = $promo['discounttypeoid'];
                            $discountvalue = $promo['discountvalue'];
                            if($discounttype == '1'){
                                $discountnote = $discountvalue."%";
                                $discount = $single * $discountvalue / 100;
                            }else if($discounttype == '3'){
                                $discountnote = $discountvalue;
                                $discount = $discountvalue;
                            }else{
                                $discount = 0;
                            }

                            $rate_after = $single - $discount;
                            $roomrate = number_format($rate_after);
                            $barrate = number_format($single);
                            $extrabedrate = number_format($ROOMRATES_PROMO[$promo['roomofferoid']]['rate_extrabed'][$key]);
                            $currencycode = $ROOMRATES_PROMO[$promo['roomofferoid']]['currencycode'][$key]; // 'IDR';
                            $picture = $ROOMRATES_PROMO[$promo['roomofferoid']]['picture'];
                            $countrate++;

                            if($ROOMRATES_PROMO[$promo['roomofferoid']]['limitallotment'] > 5){
                                $default_allotment = 5;
                            }else{
                                $default_allotment = $ROOMRATES_PROMO[$promo['roomofferoid']]['limitallotment'];
                            }
                $AVAILABLE_RATES_PROMO[$count_AVAILABLE_RATES_PROMO]['channeloid']=$channeloid;
                $AVAILABLE_RATES_PROMO[$count_AVAILABLE_RATES_PROMO]['promo_status']=1;
                            $AVAILABLE_RATES_PROMO[$count_AVAILABLE_RATES_PROMO]['promoname']=$promo['promoname'];
                            $AVAILABLE_RATES_PROMO[$count_AVAILABLE_RATES_PROMO]['discounttypeoid']=$promo['discounttypeoid'];
                            $AVAILABLE_RATES_PROMO[$count_AVAILABLE_RATES_PROMO]['discountvalue']=$promo['discountvalue'];
                            $AVAILABLE_RATES_PROMO[$count_AVAILABLE_RATES_PROMO]['discountnote']=$discountnote;

                            $AVAILABLE_RATES_PROMO[$count_AVAILABLE_RATES_PROMO]['roomname']=$promo['roomname'];
                            $AVAILABLE_RATES_PROMO[$count_AVAILABLE_RATES_PROMO]['adult']=$promo['adult'];
                            $AVAILABLE_RATES_PROMO[$count_AVAILABLE_RATES_PROMO]['child']=$promo['child'];
                $AVAILABLE_RATES_PROMO[$count_AVAILABLE_RATES_PROMO]['roomofferoid']=$promo['roomofferoid'];
                            $AVAILABLE_RATES_PROMO[$count_AVAILABLE_RATES_PROMO]['roomoid']=$promo['roomoid'];
                            $AVAILABLE_RATES_PROMO[$count_AVAILABLE_RATES_PROMO]['promotionapplyoid']=$promo['promotionapplyoid'];
                            $AVAILABLE_RATES_PROMO[$count_AVAILABLE_RATES_PROMO]['row_id']=$row_id;
                            $AVAILABLE_RATES_PROMO[$count_AVAILABLE_RATES_PROMO]['discount']=$discount;
                            $AVAILABLE_RATES_PROMO[$count_AVAILABLE_RATES_PROMO]['single']=$single;
                            $AVAILABLE_RATES_PROMO[$count_AVAILABLE_RATES_PROMO]['rate_after']=$rate_after;
                            $AVAILABLE_RATES_PROMO[$count_AVAILABLE_RATES_PROMO]['roomrate']=$roomrate;
                            $AVAILABLE_RATES_PROMO[$count_AVAILABLE_RATES_PROMO]['barrate']=$barrate;
                            $AVAILABLE_RATES_PROMO[$count_AVAILABLE_RATES_PROMO]['extrabedrate']=$extrabedrate;
                            $AVAILABLE_RATES_PROMO[$count_AVAILABLE_RATES_PROMO]['currencycode']=$currencycode;
                            $AVAILABLE_RATES_PROMO[$count_AVAILABLE_RATES_PROMO]['picture']=$picture;
                            $AVAILABLE_RATES_PROMO[$count_AVAILABLE_RATES_PROMO]['default_allotment']=$default_allotment;
                            $count_AVAILABLE_RATES_PROMO++;
                        }
                    }
                }
            }
            return $AVAILABLE_RATES_PROMO;
       }
       
    // #IBE - start --------------------------------------
    
        function ibeFlexibleRates($p,$ix,$promo_code,$roomoid,$rooid,$allowzerotrx,$totalpernight,$totalperroom,$allprice){
                        
            // $this from Class named Detail Hotel
            $db = $this->_conn_pdo;
            $hoteloid=$this->_hoteloid;
            $hotelcode=$this->_hotelcode;
            $channeloid = $this->_channeloid;

            $arrival = $this->_param_arrival;
            $departure = $this->_param_departure;
            $arrivalname = $this->_param_arrivalname;
            $todaytime = $this->_param_todaytime;
            $todayname = $this->_param_todayname;
            $today = $this->_param_today;
            $waiting_checkin = $this->_param_waiting_checkin;
            $default_arrival = $this->_param_default_arrival;
            $default_departure = $this->_param_default_departure;
            $default_night = $this->_param_default_night;

            $listBreakdownDate=$this->_listBreakdownDate;
            $listBreakdownRate=$this->_listBreakdownRate;
            $listBreakdownCurrency=$this->_listBreakdownCurrency;
            $listBreakdownDiscStat=$this->_listBreakdownDiscStat;
            $listBreakdownTotal=$this->_listBreakdownTotal;

            $xml = simplexml_load_file("myhotel/data-xml/".$hotelcode.".xml");
            
            // $pc from Class named PromoCode
            $pc_discount_type=$promo_code->pc_discount_type;
            $pc_discount=$promo_code->pc_discount;
            $applybardiscount=$promo_code->applybardiscount;

            $passed_checkin = false;
            $n = 0;
            $default_allotment = 0;
            $night = $default_night;
            do{
                $arrival    = date("Y-m-d",strtotime($default_arrival." +".$n." day"));
                $query_tag_cta  = '//hotel[@id="'.$hoteloid.'"]/masterroom[@id="'.$roomoid.'"]/rate[@date="'.$arrival.'"]/rateplan[@id="'.$rooid.'"]/channel[@type="'.$channeloid.'"]';
                foreach($xml->xpath($query_tag_cta) as $tag_cta){ 
                    $cta = $tag_cta->cta;
                }
                
                //STAAH
                // if($staah_connect){
                //     //$stmt = $db->prepare("SELECT cta FROM `staah_rateplan` WHERE hoteloid=:a AND roomoid=:b AND roomofferoid=:c AND sdate=:d");
                //     //$stmt->execute(array(':a' => $hoteloid, ':b' => $roomoid, ':c' => $rooid, ':d' => $arrival));
                //     //$r_cq = $stmt->fetch(PDO::FETCH_ASSOC);
                //  $r_cq['cta'] = $hrateplan_arr[$roomoid][$rooid][$arrival][2];
                //     if(!empty($r_cq['cta'])){ $cta = $r_cq['cta']; }
                // }
                    
                if(isset($cta) and $cta == "y"){ $n++; }else{ $passed_checkin = true; }
                if($n > $night){ $passed_checkin = true; }
            }while($passed_checkin == false);
            $arrivalname    = date('D', strtotime($arrival));

            $zero = false;
            $night = $default_night;

            for($n=0; $n<$night; $n++){
                $xml_date = date("Y-m-d",strtotime($arrival." +".$n." day"));
                $breakdate_name = date('D',strtotime($xml_date));

                $allotment=''; $extrabed=''; $rate=''; $currency=''; $closeout = 'n'; $blackout='n';

                $query_tag = '//hotel[@id="'.$hoteloid.'"]/masterroom[@id="'.$roomoid.'"]/rate[@date="'.$xml_date.'"]/allotment/channel[@type="'.$channeloid.'"]/text()';
                foreach($xml->xpath($query_tag) as $tagallotment){
                    $allotment = (int)$tagallotment[0];
                }

                $query_tag_closeout = '//hotel[@id="'.$hoteloid.'"]/masterroom[@id="'.$roomoid.'"]/rate[@date="'.$xml_date.'"]/closeout/text()';
                foreach($xml->xpath($query_tag_closeout) as $tagcloseout){
                    $closeout = $tagcloseout;
                }

                $query_tag = '//hotel[@id="'.$hoteloid.'"]/masterroom[@id="'.$roomoid.'"]/rate[@date="'.$xml_date.'"]/rateplan[@id="'.$rooid.'"]/channel[@type="'.$channeloid.'"]';
                foreach($xml->xpath($query_tag) as $tagrate){
                    $extrabed = (float)$tagrate->extrabed;
                    $rate = (float)$tagrate->double;
                    $currency = $tagrate->currency;

                    list($blackout, $cta, $ctd) = array($tagrate->blackout , $tagrate->cta , $tagrate->ctd);
                }
                
                //STAAH
                // if($staah_connect){
                //     //$stmt = $db->prepare("SELECT allotment, closeoutroom FROM `staah_roomtype` WHERE hoteloid=:a AND roomoid=:b AND sdate=:c");
                //     //$stmt->execute(array(':a' => $hoteloid, ':b' => $roomoid, ':c' => $xml_date));
                //     //$r_cq = $stmt->fetch(PDO::FETCH_ASSOC);
                //  $r_cq['allotment'] = $hroomtype_arr[$roomoid][$xml_date][0];
                //  $r_cq['closeoutroom'] = $hroomtype_arr[$roomoid][$xml_date][1];
                //     if(!empty($r_cq['allotment']) || $r_cq['allotment']=='0'){ $allotment = $r_cq['allotment']; }
                //     if(!empty($r_cq['closeoutroom'])){ $closeout = $r_cq['closeoutroom']; }
                    
                //     //$stmt = $db->prepare("SELECT rate, closeoutrate, cta, ctd FROM `staah_rateplan` WHERE hoteloid=:a AND roomoid=:b AND roomofferoid=:c AND sdate=:d");
                //     //$stmt->execute(array(':a' => $hoteloid, ':b' => $roomoid, ':c' => $rooid, ':d' => $xml_date));
                //     //$r_cq = $stmt->fetch(PDO::FETCH_ASSOC);
                //  $r_cq['closeoutrate'] = $hrateplan_arr[$roomoid][$rooid][$xml_date][1];
                //  $r_cq['cta'] = $hrateplan_arr[$roomoid][$rooid][$xml_date][2];
                //  $r_cq['ctd'] = $hrateplan_arr[$roomoid][$rooid][$xml_date][3];
                //  $r_cq['rate'] = $hrateplan_arr[$roomoid][$rooid][$xml_date][0];
                //     if(!empty($r_cq['closeoutrate'])){ $blackout = $r_cq['closeoutrate']; }
                //     if(!empty($r_cq['cta'])){ $cta = $r_cq['cta']; }
                //     if(!empty($r_cq['ctd'])){ $ctd = $r_cq['ctd']; }
                //     if(!empty($r_cq['rate'])){ $rate = $r_cq['rate']; }
                // }

                $blakout_check = 0; $cta_check = 0; $ctd_check = 0;
                if((isset($blackout) and $blackout == "y") or $closeout == 'y'){
                    $blakout_check = 1;
                }

                if($n == $night-1){
                    $departure  = date("Y-m-d",strtotime($arrival." +".$night." day"));
                    $query_tag_ctd  = '//hotel[@id="'.$hoteloid.'"]/masterroom[@id="'.$roomoid.'"]/rate[@date="'.$departure.'"]/rateplan[@id="'.$rooid.'"]/channel[@type="'.$channeloid.'"]';
                    foreach($xml->xpath($query_tag_ctd) as $tag_ctd){
                        if($tag_ctd->ctd == "y"){ $night++; }
                    }
                    
                    //STAAH
                    // if($staah_connect){
                    //     //$stmt = $db->prepare("SELECT ctd FROM `staah_rateplan` WHERE hoteloid=:a AND roomoid=:b AND roomofferoid=:c AND sdate=:d");
                    //     //$stmt->execute(array(':a' => $hoteloid, ':b' => $roomoid, ':c' => $rooid, ':d' => $departure));
                    //     //$r_cq = $stmt->fetch(PDO::FETCH_ASSOC);
                    //  $r_cq['ctd'] = $hrateplan_arr[$roomoid][$rooid][$departure][3];
                    //     if(!empty($r_cq['ctd']) && $r_cq['ctd']=="y"){ 
                    //         if($cz == 0){
                    //             $night++;
                    //         }
                    //     }else if(!empty($r_cq['ctd']) && $r_cq['ctd']=="n"){
                    //         if($cz == 1){
                    //             $night--;
                    //         }
                    //     }
                    // }
                }


                if($extrabed > 0){
                    $extrabedavailable = "Available";
                }else{
                    $extrabedavailable = "Not available";
                }

                // $s_currency = "SELECT `currencycode`, `currencytitle` FROM `currency` WHERE `currencyoid`='".$currency."'";
                // $stmt = $db->query($s_currency);
                // $a_currency = $stmt->fetchAll(PDO::FETCH_ASSOC);
                // foreach($a_currency as $r_currency){
                //     $currencyname = $r_currency['currencycode'];
                // }
                $currencyname = Curr::getCurrencyCode($db,$currency);
                
                //TEMPORARY OWNER
                if($allowzerotrx == '1'){
                    $tmprate = $rate;
                    $rate = 1;
                }

                if($allotment > 0 and $rate > 0 and $blakout_check == 0){
                        
                    //TEMPORARY OWNER
                    if($allowzerotrx == '1'){
                        $rate = $tmprate;
                    }
                        
                    $disc_status = "No";
                    $discapplyid = "";
                    $totalpernight = $rate;
                    $totalpernight = Func::promocodediscount($applybardiscount, $pc_discount_type, $pc_discount, $totalpernight);

                    if($n==0) $default_allotment = $allotment;
                    $totalperroom += $totalpernight;
                    $listBreakdownDate[$ix][$p][$n] = $xml_date;
                    $listBreakdownRate[$ix][$p][$n] = $rate;
                    $listBreakdownCurrency[$ix][$p][$n] = $currencyname;
                    $listBreakdownDiscStat[$ix][$p][$n] = $disc_status;
                    $listBreakdownTotal[$ix][$p][$n] = $totalpernight;
                }else{
                    $zero = true;
                    break;
                }
            }
            
            $this->_listBreakdownDate = $listBreakdownDate;
            $this->_listBreakdownRate = $listBreakdownRate;
            $this->_listBreakdownCurrency = $listBreakdownCurrency;
            $this->_listBreakdownDiscStat = $listBreakdownDiscStat;
            $this->_listBreakdownTotal =  $listBreakdownTotal;
            
            $arrayFlexibleRates = array();
            $arrayFlexibleRates['cta'] = $cta;
            $arrayFlexibleRates['zero'] = $zero;
            $arrayFlexibleRates['night'] = $night;
            $arrayFlexibleRates['arrivalname'] = $arrivalname;
            $arrayFlexibleRates['totalperroom'] = $totalperroom;
            $arrayFlexibleRates['allotment'] = $default_allotment;
            return $arrayFlexibleRates;
        }
        
        function ibePromotionRates($p,$ix,$promo_code,$roomoid,$rooid,$allowzerotrx,$totalpernight,$totalperroom,$allprice){
                
            // $this from Class named Detail Hotel
            $db = $this->_conn_pdo;
            $night=$this->_night;
            $hoteloid=$this->_hoteloid;
            $hotelcode=$this->_hotelcode;
            $channeloid = $this->_channeloid;
            
            $arrival = $this->_param_arrival;
            $departure = $this->_param_departure;
            $arrivalname = $this->_param_arrivalname;
            $todaytime = $this->_param_todaytime;
            $todayname = $this->_param_todayname;
            $today = $this->_param_today;
            $waiting_checkin = $this->_param_waiting_checkin;
            $default_arrival = $this->_param_default_arrival;
            $default_departure = $this->_param_default_departure;
            $default_night = $this->_param_default_night;

            $listBreakdownDate=$this->_listBreakdownDate;
            $listBreakdownRate=$this->_listBreakdownRate;
            $listBreakdownCurrency=$this->_listBreakdownCurrency;
            $listBreakdownDiscStat=$this->_listBreakdownDiscStat;
            $listBreakdownTotal=$this->_listBreakdownTotal;
            
            $promotionid = $this->_promotionid;
            $promotionname = $this->_promotionname;
            $promotiontype = $this->_promotiontype;
            $typepromo = $this->_promotionnight;
            $promotionnight = $this->_promotionnight;
            $promotiontotal = $this->_promotiontotal;
            $promotionimage = $this->_promotionimage;
            $promotionheadline = $this->_promotionheadline;

            // $pc from Class named PromoCode
            $promocode=$promo_code->promocode;
            $pc_discount_type=$promo_code->pc_discount_type;
            $pc_discount=$promo_code->pc_discount;
            $applybardiscount=$promo_code->applybardiscount;

            $xml = simplexml_load_file("myhotel/data-xml/".$hotelcode.".xml");

            if($promocode == "thebuking"){
                $query_join_promocode = "INNER JOIN (select 'y' as applydiscount, 'y' as applycommission) x";
                $query_shown_nopc = "and pr.shown_no_pc = 'y' ";
            }else{
                $query_join_promocode = " INNER JOIN (select id, applydiscount, applycommission from promocode prm inner join promocodeapply pca using (promocodeoid) where referencetable = 'promotion' and promocode like '".$promocode."' and (pca.hoteloid = '".$hoteloid."' or prm.hoteloid = '".$hoteloid."')  and publishedoid = 1) x on x.id = pr.promotionoid ";
                $query_shown_nopc = "";
            }
            
            // Direct Promotion
            // if(isset($_REQUEST['promotion']) and !empty($_REQUEST['promotion'])){
            //     $query_direct_promotion = "AND lower(pr.name) like '%".strtolower(htmlspecialchars_decode($_REQUEST['promotion']))."%'";
            //     $direct_promotion = true;
            // }else{
            //     $query_direct_promotion = "";
            //     $direct_promotion = false;
            // }
            $query_direct_promotion = "";
            $direct_promotion = false;
            
            $s_id = "
            SELECT pa.promotionapplyoid, pr.promotiontypeoid, pr.promotionoid, pr.name AS promoname, pr.headline, pr.minstay, pr.maxstay, pr.discountapplyoid, 
                pr.applyvalue, pr.discounttypeoid, pr.discountvalue, cp.name AS cpname, cp.description AS cpdesc, pr.priority, da.name AS discname, 
                dt.name AS disctype, pt.type AS promotype, pt.description AS promotypedesc, ro.roomofferoid, ro.name AS roomoffername, x.applydiscount, 
                x.applycommission, pr.promoimage
            FROM promotionapply pa
            INNER JOIN `roomoffer` ro using (roomofferoid)
            INNER JOIN `room` r using (roomoid)
            INNER JOIN `promotion` pr USING (`promotionoid`)
            INNER JOIN `discountapply` da USING (`discountapplyoid`)
            INNER JOIN `discounttype` dt USING (`discounttypeoid`)
            INNER JOIN `promotiontype` pt USING (`promotiontypeoid`)
            LEFT JOIN `cancellationpolicy` cp USING (`cancellationpolicyoid`)
            ".$query_join_promocode."
            WHERE pr.hoteloid = '".$hoteloid."' ".$query_direct_promotion." 
            AND pr.salefrom <= '".$today."' AND pr.saleto >= '".$today."'
            AND pr.bookfrom <= '".$arrival."' AND pr.bookto >= '".$arrival."'
            AND pr.bookfrom <= '".$departure."' AND pr.bookto >= '".$departure."'
            AND pr.displayed LIKE '%".$todayname."%'
            AND pr.checkinon LIKE '%".$arrivalname."%'
            AND ((pr.timefrom <= '".$todaytime."' AND pr.timeto >= '".$todaytime."') or (pr.timefrom >= '".$todaytime."' AND pr.timeto >= '".$todaytime."' 
                and pr.timefrom > pr.timeto) or (pr.timefrom <= '".$todaytime."' AND pr.timeto <= '".$todaytime."'and pr.timefrom > pr.timeto))
            AND pr.minstay <= '".$night."'
            AND pr.min_bef_ci <= '".($waiting_checkin+1)."' 
                AND CASE WHEN pr.max_bef_ci_today=1 THEN '".($waiting_checkin+1)."'='0' ELSE (pr.max_bef_ci = '0' or pr.max_bef_ci >= '".($waiting_checkin+1)."') END
            AND cp.publishedoid = '1' AND pr.publishedoid = '1' AND da.publishedoid = '1' AND dt.publishedoid = '1' AND pt.publishedoid = '1' 
                and pa.roomofferoid = '".$rooid."' and pa.channeloid = '".$channeloid."' and pa.publishedoid = '1' ".$query_shown_nopc."
            AND
                (pr.promotionoid IN (select pg.promotionoid from promotiongeo pg inner join promotion p using (promotionoid) 
                    where countryoid = '".$_SESSION['tb_ipuser_countryoid']."' and p.hoteloid = '".$hoteloid."')
                OR
                promotionoid not in (select distinct(promotionoid) from promotiongeo inner join promotion p using (promotionoid) 
                    where p.hoteloid = '".$hoteloid."'))
            AND
                (pr.promotionoid not in (select distinct(pgn.promotionoid) from promotiongeo_noshow pgn inner join promotion p using (promotionoid) 
                    where countryoid = '".$_SESSION['tb_ipuser_countryoid']."' and p.hoteloid = '".$hoteloid."'))
            GROUP BY pr.promotionoid";

            Func::logJS($s_id,'promo query');
            $stmt   = $db->query($s_id);
            $a_id = $stmt->fetchAll(PDO::FETCH_ASSOC);
            // Func::logJS($a_id,'promo result');
            foreach($a_id as $a_idx){
                $promooid = $a_idx['promotionoid'];
                $promoname = $a_idx['promoname'];
                $promotypeoid = $a_idx['promotiontypeoid'];
                $promoimage = $a_idx['promoimage'];
                $promoheadline = $a_idx['headline'];
                $minstay = $a_idx['minstay'];
                $maxstay = $a_idx['maxstay'];
                $applyvalue = $a_idx['applyvalue'];
                $disctypeid = $a_idx['discounttypeoid'];
                $discvalue = $a_idx['discountvalue'];
                $discapplyid = $a_idx['discountapplyoid'];
                $cpname = $a_idx['cpname'];
                $cpdesc = $a_idx['cpdesc'];
                $priority = $a_idx['priority'];
                $discname = $a_idx['discname'];
                $disctype = $a_idx['disctype'];
                $promotype = $a_idx['promotype'];
                $promotypedesc = $a_idx['promotypedesc'];
                $applydiscount = $a_idx['applydiscount'];
                $applycommission = $a_idx['applycommission'];
                $rooid = $a_idx['roomofferoid'];

                if($maxstay==0 or ($maxstay!=0 and $night<=$maxstay)) {
                    $totalperroom = 0;

                    if(!empty($tmp_night)){ $night = $tmp_night; }

                    if($disctypeid == 4 and $discapplyid == 1){
                        $tmp_night = $night;
                        $night += $discvalue;
                    }

                    $promoNight = array();
                    $night = $default_night;
                    for($n=0; $n<$night; $n++){
                        $xml_date = date("Y-m-d",strtotime($arrival." +".$n." day"));
                        $breakdate_name = date('D',strtotime($xml_date));

                        $allotment=''; $extrabed=''; $rate=''; $currency=''; $closeout='n'; $blackout='n';

                        $query_tag = '//hotel[@id="'.$hoteloid.'"]/masterroom[@id="'.$roomoid.'"]/rate[@date="'.$xml_date.'"]/allotment/channel[@type="'.$channeloid.'"]/text()';
                        // Func::logJS($query_tag,'$query_tag');
                        foreach($xml->xpath($query_tag) as $tagallotment){
                            $allotment = (int)$tagallotment[0];
                        }
        
                        $query_tag_closeout = '//hotel[@id="'.$hoteloid.'"]/masterroom[@id="'.$roomoid.'"]/rate[@date="'.$xml_date.'"]/closeout/text()';
                        foreach($xml->xpath($query_tag_closeout) as $tagcloseout){
                            $closeout = $tagcloseout;
                        }
        
                        $query_tag = '//hotel[@id="'.$hoteloid.'"]/masterroom[@id="'.$roomoid.'"]/rate[@date="'.$xml_date.'"]/rateplan[@id="'.$rooid.'"]/channel[@type="'.$channeloid.'"]';
                        foreach($xml->xpath($query_tag) as $tagrate){
                            $extrabed = (float)$tagrate->extrabed;
                            $rate = (float)$tagrate->double;
                            $currency = $tagrate->currency;
        
                            list($blackout, $cta, $ctd) = array($tagrate->blackout , $tagrate->cta , $tagrate->ctd);
                        }
                
                        //STAAH
                        // if($staah_connect){
                        //     //$stmt = $db->prepare("SELECT allotment, closeoutroom FROM `staah_roomtype` WHERE hoteloid=:a AND roomoid=:b AND sdate=:c");
                        //     //$stmt->execute(array(':a' => $hoteloid, ':b' => $roomoid, ':c' => $xml_date));
                        //     //$r_cq = $stmt->fetch(PDO::FETCH_ASSOC);
                        //  $r_cq['allotment'] = $hroomtype_arr[$roomoid][$xml_date][0];
                        //  $r_cq['closeoutroom'] = $hroomtype_arr[$roomoid][$xml_date][1];
                        //     if(!empty($r_cq['allotment']) || $r_cq['allotment']=='0'){ $allotment = $r_cq['allotment']; }
                        //     if(!empty($r_cq['closeoutroom'])){ $closeout = $r_cq['closeoutroom']; }
                            
                        //     //$stmt = $db->prepare("SELECT rate, closeoutrate, cta, ctd FROM `staah_rateplan` WHERE hoteloid=:a AND roomoid=:b AND roomofferoid=:c AND sdate=:d");
                        //     //$stmt->execute(array(':a' => $hoteloid, ':b' => $roomoid, ':c' => $rooid, ':d' => $xml_date));
                        //     //$r_cq = $stmt->fetch(PDO::FETCH_ASSOC);
                        //  $r_cq['closeoutrate'] = $hrateplan_arr[$roomoid][$rooid][$xml_date][1];
                        //  $r_cq['cta'] = $hrateplan_arr[$roomoid][$rooid][$xml_date][2];
                        //  $r_cq['ctd'] = $hrateplan_arr[$roomoid][$rooid][$xml_date][3];
                        //  $r_cq['rate'] = $hrateplan_arr[$roomoid][$rooid][$xml_date][0];
                        //     if(!empty($r_cq['closeoutrate'])){ $blackout = $r_cq['closeoutrate']; }
                        //     if(!empty($r_cq['cta'])){ $cta = $r_cq['cta']; }
                        //     if(!empty($r_cq['ctd'])){ $ctd = $r_cq['ctd']; }
                        //     if(!empty($r_cq['rate'])){ $rate = $r_cq['rate']; }
                        // }
        
                        $blakout_check = 0; $cta_check = 0; $ctd_check = 0;
                        if((isset($blackout) and $blackout == "y") or $closeout == 'y'){
                            $blakout_check = 1;
                        }
        
                        if($n == $night-1){
                            $departure  = date("Y-m-d",strtotime($arrival." +".$night." day"));
                            $query_tag_ctd  = '//hotel[@id="'.$hoteloid.'"]/masterroom[@id="'.$roomoid.'"]/rate[@date="'.$departure.'"]/rateplan[@id="'.$rooid.'"]/channel[@type="'.$channeloid.'"]';
                            foreach($xml->xpath($query_tag_ctd) as $tag_ctd){
                                if($tag_ctd->ctd == "y"){ $night++; }
                            }
                    
                            //STAAH
                            // if($staah_connect){
                            //     //$stmt = $db->prepare("SELECT ctd FROM `staah_rateplan` WHERE hoteloid=:a AND roomoid=:b AND roomofferoid=:c AND sdate=:d");
                            //     //$stmt->execute(array(':a' => $hoteloid, ':b' => $roomoid, ':c' => $rooid, ':d' => $departure));
                            //     //$r_cq = $stmt->fetch(PDO::FETCH_ASSOC);
                            //  $r_cq['ctd'] = $hrateplan_arr[$roomoid][$rooid][$departure][3];
                            //     if(!empty($r_cq['ctd']) && $r_cq['ctd']=="y"){ 
                            //         if($cz == 0){
                            //             $night++;
                            //         }
                            //     }else if(!empty($r_cq['ctd']) && $r_cq['ctd']=="n"){
                            //         if($cz == 1){
                            //             $night--;
                            //         }
                            //     }
                            // }
                        }

                        if($extrabed > 0){
                            $extrabedavailable = "Available";
                        }else{
                            $extrabedavailable = "Not available";
                        }

                        // $s_currency = "SELECT `currencycode`, `currencytitle` FROM `currency` WHERE `currencyoid`='".$currency."'";
                        // $stmt = $db->query($s_currency);
                        // $a_currency = $stmt->fetchAll(PDO::FETCH_ASSOC);
                        // foreach($a_currency as $r_currency){
                        //     $currencyname = $r_currency['currencycode'];
                        // }
                        $currencyname = Curr::getCurrencyCode($db,$currency);
                        
                        //TEMPORARY OWNER
                        if($allowzerotrx == '1'){
                            $tmprate = $rate;
                            $rate = 1;
                        }

                        if($allotment > 0 and $blakout_check == 0 and $rate > 0){
                            
                            //TEMPORARY OWNER
                            if($allowzerotrx == '1'){
                                $rate = $tmprate;
                            }
                            
                            if($applyvalue == ""
                                or ($applyvalue != "" and Func::matchday($applyvalue, $breakdate_name) == true)
                                or ($applyvalue != "" and Func::matchnight($applyvalue, ($n+1)) == true)){
                                if($disctypeid == 4){
                                    if($discapplyid == 1 and $n < $tmp_night){
                                        $disc_status = "No";
                                    }else if($discapplyid == 4 and $n >= $discvalue){
                                        $disc_status = "No";
                                    }else if($discapplyid == 5 and $n < ($night - $discvalue)){
                                        $disc_status = "No";
                                    }else{
                                        $disc_status = "Yes";
                                    }
                                }else if($discapplyid == 4 and $n > 0){
                                    $disc_status = "No";
                                }else if($discapplyid == 5 and $n < ($night-1)){
                                    $disc_status = "No";
                                }else{
                                    $disc_status = "Yes";
                                }
                            }else{
                                $disc_status = "No";
                            }

                            $totalpernight = $rate;
                            if($disctypeid == 1){
                                if($disc_status == "Yes"){
                                    $totalpernight = $rate - ($discvalue/100 * $rate);
                                }
                            }
                            if($disctypeid == 3){
                                if($disc_status == "Yes"){
                                    $totalpernight = $rate - $discvalue;
                                }
                            }

                            if($disctypeid == 4){
                                if($disc_status == "Yes"){
                                    $totalpernight = 0;
                                }
                            }

                            $totalpernight = Func::promocodediscount($applydiscount, $pc_discount_type, $pc_discount, $totalpernight);
                            // if($loyaltyprogram == true){
                            //     if(in_array($promooid, $loyaltypromotion) and $disc_status == "Yes"){
                            //         $totalpernight = $totalpernight - ($totalpernight * $membership_discount / 100);
                            //     }
                            // }

                            $totalperroom += $totalpernight;

                            $promoNight[$n]['xml_date'] = $xml_date;
                            $promoNight[$n]['allotment'] = $allotment;
                            $promoNight[$n]['extrabedavailable'] = $extrabedavailable;
                            $promoNight[$n]['rate'] = $rate;
                            $promoNight[$n]['currencyname'] = $currencyname;
                            $promoNight[$n]['breakdate_name'] = $breakdate_name;
                            $promoNight[$n]['discapplyid'] = $discapplyid;
                            $promoNight[$n]['disc_status'] = $disc_status;
                            $promoNight[$n]['totalpernight'] = $totalpernight;
                        }
                    }

                    if($disctypeid == 2){
                        $totalperroom = $totalperroom - $discvalue;
                        foreach($promoNight as $j => $value2){
                            $promoNight[$j]['totalpernight'] = $totalperroom / count($promoNight);
                        }
                    }

                    $show = "No";
                    foreach($promoNight as $j => $value2){
                        if($value2['disc_status'] == "Yes"){
                            $show = "Yes"; break;
                        }
                    }
                    
                    //TEMPORARY OWNER
                    if($allowzerotrx == '1'){
                        $tmptotalperroom = $totalperroom;
                        $totalperroom = 1;
                    }

                    if($show == "Yes" and $totalperroom > 0){
                
                        //TEMPORARY OWNER
                        if($allowzerotrx == '1'){
                            $totalperroom = $tmptotalperroom;
                        }

                        if(empty($promoimage)){
                            $promoimage = $r_room['photourl'];
                        }


                        $p++;
                        $promotionid[$ix][$p]       = $promooid;
                        $promotionname[$ix][$p]     = $promoname;
                        $promotiontype[$ix][$p]     = 'promo';
                        $typepromo[$ix][$p]         = $promotypeoid;
                        $promotionnight[$ix][$p]    = $night;
                        $promotiontotal[$ix][$p]    = $totalperroom;
                        $promotionimage[$ix][$p]    = $promoimage;
                        $promotionheadline[$ix][$p] = $promoheadline;

                        array_push($allprice, $totalperroom/$night);

                        foreach($promoNight as $j => $value2){
                            $listBreakdownDate[$ix][$p][$j] = $value2['xml_date'];
                            $listBreakdownRate[$ix][$p][$j] = $value2['rate'];
                            $listBreakdownCurrency[$ix][$p][$j] = $value2['currencyname'];
                            $listBreakdownDiscStat[$ix][$p][$j] = $value2['disc_status'];
                            $listBreakdownTotal[$ix][$p][$j] = $value2['totalpernight'];
                        }
                    }
                }
            } // room offer promo

            if($direct_promotion == true and count($promotionid) == 0){
                $direct_promotion = false;
            }

            $this->_listBreakdownDate = $listBreakdownDate;
            $this->_listBreakdownRate = $listBreakdownRate;
            $this->_listBreakdownCurrency = $listBreakdownCurrency;
            $this->_listBreakdownDiscStat = $listBreakdownDiscStat;
            $this->_listBreakdownTotal =  $listBreakdownTotal;

            $this->_promotionid = $promotionid;
            $this->_promotionname = $promotionname;
            $this->_promotiontype = $promotiontype;
            $this->_promotionnight = $typepromo;
            $this->_promotionnight = $promotionnight;
            $this->_promotiontotal = $promotiontotal;
            $this->_promotionimage = $promotionimage;
            $this->_promotionheadline = $promotionheadline;

            $arrayPromotionRates= array();
            $arrayPromotionRates['allprice']= $allprice;
            $arrayPromotionRates['totalperroom']= $totalperroom;
            $arrayPromotionRates['direct_promotion']= $direct_promotion;
            //$_SESSION['tb_ipuser_countryoid']
            return $arrayPromotionRates;
        }
        function ibePromotionAllRates($p,$ix,$promo_code,$roomoid,$rooid,$allowzerotrx,$totalpernight,$totalperroom,$allprice){
                
            // $this from Class named Detail Hotel
            $db = $this->_conn_pdo;
            $night=$this->_night;
            $hoteloid=$this->_hoteloid;
            $hotelcode=$this->_hotelcode;
            $channeloid = $this->_channeloid;
            
            $arrival = $this->_param_arrival;
            $departure = $this->_param_departure;
            $arrivalname = $this->_param_arrivalname;
            $todaytime = $this->_param_todaytime;
            $todayname = $this->_param_todayname;
            $today = $this->_param_today;
            $waiting_checkin = $this->_param_waiting_checkin;
            $default_arrival = $this->_param_default_arrival;
            $default_departure = $this->_param_default_departure;
            $default_night = $this->_param_default_night;

            $listBreakdownDate=$this->_listBreakdownDate;
            $listBreakdownRate=$this->_listBreakdownRate;
            $listBreakdownCurrency=$this->_listBreakdownCurrency;
            $listBreakdownDiscStat=$this->_listBreakdownDiscStat;
            $listBreakdownTotal=$this->_listBreakdownTotal;
            
            $promotionid = $this->_promotionid;
            $promotionname = $this->_promotionname;
            $promotiontype = $this->_promotiontype;
            $typepromo = $this->_promotionnight;
            $promotionnight = $this->_promotionnight;
            $promotiontotal = $this->_promotiontotal;
            $promotionimage = $this->_promotionimage;
            $promotionheadline = $this->_promotionheadline;

            // $pc from Class named PromoCode
            $promocode=$promo_code->promocode;
            $pc_discount_type=$promo_code->pc_discount_type;
            $pc_discount=$promo_code->pc_discount;
            $applybardiscount=$promo_code->applybardiscount;

            $xml = simplexml_load_file("myhotel/data-xml/".$hotelcode.".xml");

            if($promocode == "thebuking"){
                $query_join_promocode = "INNER JOIN (select 'y' as applydiscount, 'y' as applycommission) x";
                $query_shown_nopc = "and pr.shown_no_pc = 'y' ";
            }else{
                $query_join_promocode = " INNER JOIN (select id, applydiscount, applycommission from promocode prm inner join promocodeapply pca using (promocodeoid) where referencetable = 'promotion' and promocode like '".$promocode."' and (pca.hoteloid = '".$hoteloid."' or prm.hoteloid = '".$hoteloid."')  and publishedoid = 1) x on x.id = pr.promotionoid ";
                $query_shown_nopc = "";
            }
            
            // Direct Promotion
            // if(isset($_REQUEST['promotion']) and !empty($_REQUEST['promotion'])){
            //     $query_direct_promotion = "AND lower(pr.name) like '%".strtolower(htmlspecialchars_decode($_REQUEST['promotion']))."%'";
            //     $direct_promotion = true;
            // }else{
            //     $query_direct_promotion = "";
            //     $direct_promotion = false;
            // }
            $query_direct_promotion = "";
            $direct_promotion = false;
            
            $s_id = "
            SELECT pa.promotionapplyoid, pr.promotiontypeoid, pr.promotionoid, pr.name AS promoname, pr.headline, pr.minstay, pr.maxstay, pr.discountapplyoid, 
                pr.applyvalue, pr.discounttypeoid, pr.discountvalue, cp.name AS cpname, cp.description AS cpdesc, pr.priority, da.name AS discname, 
                dt.name AS disctype, pt.type AS promotype, pt.description AS promotypedesc, ro.roomofferoid, ro.name AS roomoffername, x.applydiscount, 
                x.applycommission, pr.promoimage
            FROM promotionapply pa
            INNER JOIN `roomoffer` ro using (roomofferoid)
            INNER JOIN `room` r using (roomoid)
            INNER JOIN `promotion` pr USING (`promotionoid`)
            INNER JOIN `discountapply` da USING (`discountapplyoid`)
            INNER JOIN `discounttype` dt USING (`discounttypeoid`)
            INNER JOIN `promotiontype` pt USING (`promotiontypeoid`)
            LEFT JOIN `cancellationpolicy` cp USING (`cancellationpolicyoid`)
            ".$query_join_promocode."
            WHERE pa.hoteloid = '".$hoteloid."' ".$query_direct_promotion." 
            AND pr.salefrom <= '".$today."' AND pr.saleto >= '".$today."'
            AND pr.bookfrom <= '".$arrival."' AND pr.bookto >= '".$arrival."'
            AND pr.bookfrom <= '".$departure."' AND pr.bookto >= '".$departure."'
            AND pr.displayed LIKE '%".$todayname."%'
            AND pr.checkinon LIKE '%".$arrivalname."%'
            AND ((pr.timefrom <= '".$todaytime."' AND pr.timeto >= '".$todaytime."') or (pr.timefrom >= '".$todaytime."' AND pr.timeto >= '".$todaytime."' 
                and pr.timefrom > pr.timeto) or (pr.timefrom <= '".$todaytime."' AND pr.timeto <= '".$todaytime."'and pr.timefrom > pr.timeto))
            AND pr.minstay <= '".$night."'
            AND pr.min_bef_ci <= '".($waiting_checkin+1)."' 
                AND CASE WHEN pr.max_bef_ci_today=1 THEN '".($waiting_checkin+1)."'='0' ELSE (pr.max_bef_ci = '0' or pr.max_bef_ci >= '".($waiting_checkin+1)."') END
            AND cp.publishedoid = '1' AND pr.publishedoid = '1' AND da.publishedoid = '1' AND dt.publishedoid = '1' AND pt.publishedoid = '1' 
                and pa.roomofferoid = '".$rooid."' and pa.channeloid = '".$channeloid."' and pa.publishedoid = '1' ".$query_shown_nopc."
            AND
                (pr.promotionoid IN (select pg.promotionoid from promotiongeo pg inner join promotion p using (promotionoid) 
                    where countryoid = '".$_SESSION['tb_ipuser_countryoid']."' and p.hoteloid = '".$hoteloid."')
                OR
                promotionoid not in (select distinct(promotionoid) from promotiongeo inner join promotion p using (promotionoid) 
                    where p.hoteloid = '".$hoteloid."'))
            AND
                (pr.promotionoid not in (select distinct(pgn.promotionoid) from promotiongeo_noshow pgn inner join promotion p using (promotionoid) 
                    where countryoid = '".$_SESSION['tb_ipuser_countryoid']."' and p.hoteloid = '".$hoteloid."'))
            GROUP BY pr.promotionoid";

            Func::logJS($s_id,'promo query');
            $stmt   = $db->query($s_id);
            $a_id = $stmt->fetchAll(PDO::FETCH_ASSOC);
            // Func::logJS($a_id,'promo result');
            foreach($a_id as $a_idx){
                $promooid = $a_idx['promotionoid'];
                $promoname = $a_idx['promoname'];
                $promotypeoid = $a_idx['promotiontypeoid'];
                $promoimage = $a_idx['promoimage'];
                $promoheadline = $a_idx['headline'];
                $minstay = $a_idx['minstay'];
                $maxstay = $a_idx['maxstay'];
                $applyvalue = $a_idx['applyvalue'];
                $disctypeid = $a_idx['discounttypeoid'];
                $discvalue = $a_idx['discountvalue'];
                $discapplyid = $a_idx['discountapplyoid'];
                $cpname = $a_idx['cpname'];
                $cpdesc = $a_idx['cpdesc'];
                $priority = $a_idx['priority'];
                $discname = $a_idx['discname'];
                $disctype = $a_idx['disctype'];
                $promotype = $a_idx['promotype'];
                $promotypedesc = $a_idx['promotypedesc'];
                $applydiscount = $a_idx['applydiscount'];
                $applycommission = $a_idx['applycommission'];
                $rooid = $a_idx['roomofferoid'];

                if($maxstay==0 or ($maxstay!=0 and $night<=$maxstay)) {
                    $totalperroom = 0;

                    if(!empty($tmp_night)){ $night = $tmp_night; }

                    if($disctypeid == 4 and $discapplyid == 1){
                        $tmp_night = $night;
                        $night += $discvalue;
                    }

                    $promoNight = array();
                    $night = $default_night;
                    for($n=0; $n<$night; $n++){
                        $xml_date = date("Y-m-d",strtotime($arrival." +".$n." day"));
                        $breakdate_name = date('D',strtotime($xml_date));

                        $allotment=''; $extrabed=''; $rate=''; $currency=''; $closeout='n'; $blackout='n';

                        $query_tag = '//hotel[@id="'.$hoteloid.'"]/masterroom[@id="'.$roomoid.'"]/rate[@date="'.$xml_date.'"]/allotment/channel[@type="'.$channeloid.'"]/text()';
                        // Func::logJS($query_tag,'$query_tag');
                        foreach($xml->xpath($query_tag) as $tagallotment){
                            $allotment = (int)$tagallotment[0];
                        }
        
                        $query_tag_closeout = '//hotel[@id="'.$hoteloid.'"]/masterroom[@id="'.$roomoid.'"]/rate[@date="'.$xml_date.'"]/closeout/text()';
                        foreach($xml->xpath($query_tag_closeout) as $tagcloseout){
                            $closeout = $tagcloseout;
                        }
        
                        $query_tag = '//hotel[@id="'.$hoteloid.'"]/masterroom[@id="'.$roomoid.'"]/rate[@date="'.$xml_date.'"]/rateplan[@id="'.$rooid.'"]/channel[@type="'.$channeloid.'"]';
                        foreach($xml->xpath($query_tag) as $tagrate){
                            $extrabed = (float)$tagrate->extrabed;
                            $rate = (float)$tagrate->double;
                            $currency = $tagrate->currency;
        
                            list($blackout, $cta, $ctd) = array($tagrate->blackout , $tagrate->cta , $tagrate->ctd);
                        }
                
                        //STAAH
                        // if($staah_connect){
                        //     //$stmt = $db->prepare("SELECT allotment, closeoutroom FROM `staah_roomtype` WHERE hoteloid=:a AND roomoid=:b AND sdate=:c");
                        //     //$stmt->execute(array(':a' => $hoteloid, ':b' => $roomoid, ':c' => $xml_date));
                        //     //$r_cq = $stmt->fetch(PDO::FETCH_ASSOC);
                        //  $r_cq['allotment'] = $hroomtype_arr[$roomoid][$xml_date][0];
                        //  $r_cq['closeoutroom'] = $hroomtype_arr[$roomoid][$xml_date][1];
                        //     if(!empty($r_cq['allotment']) || $r_cq['allotment']=='0'){ $allotment = $r_cq['allotment']; }
                        //     if(!empty($r_cq['closeoutroom'])){ $closeout = $r_cq['closeoutroom']; }
                            
                        //     //$stmt = $db->prepare("SELECT rate, closeoutrate, cta, ctd FROM `staah_rateplan` WHERE hoteloid=:a AND roomoid=:b AND roomofferoid=:c AND sdate=:d");
                        //     //$stmt->execute(array(':a' => $hoteloid, ':b' => $roomoid, ':c' => $rooid, ':d' => $xml_date));
                        //     //$r_cq = $stmt->fetch(PDO::FETCH_ASSOC);
                        //  $r_cq['closeoutrate'] = $hrateplan_arr[$roomoid][$rooid][$xml_date][1];
                        //  $r_cq['cta'] = $hrateplan_arr[$roomoid][$rooid][$xml_date][2];
                        //  $r_cq['ctd'] = $hrateplan_arr[$roomoid][$rooid][$xml_date][3];
                        //  $r_cq['rate'] = $hrateplan_arr[$roomoid][$rooid][$xml_date][0];
                        //     if(!empty($r_cq['closeoutrate'])){ $blackout = $r_cq['closeoutrate']; }
                        //     if(!empty($r_cq['cta'])){ $cta = $r_cq['cta']; }
                        //     if(!empty($r_cq['ctd'])){ $ctd = $r_cq['ctd']; }
                        //     if(!empty($r_cq['rate'])){ $rate = $r_cq['rate']; }
                        // }
        
                        $blakout_check = 0; $cta_check = 0; $ctd_check = 0;
                        if((isset($blackout) and $blackout == "y") or $closeout == 'y'){
                            $blakout_check = 1;
                        }
        
                        if($n == $night-1){
                            $departure  = date("Y-m-d",strtotime($arrival." +".$night." day"));
                            $query_tag_ctd  = '//hotel[@id="'.$hoteloid.'"]/masterroom[@id="'.$roomoid.'"]/rate[@date="'.$departure.'"]/rateplan[@id="'.$rooid.'"]/channel[@type="'.$channeloid.'"]';
                            foreach($xml->xpath($query_tag_ctd) as $tag_ctd){
                                if($tag_ctd->ctd == "y"){ $night++; }
                            }
                    
                            //STAAH
                            // if($staah_connect){
                            //     //$stmt = $db->prepare("SELECT ctd FROM `staah_rateplan` WHERE hoteloid=:a AND roomoid=:b AND roomofferoid=:c AND sdate=:d");
                            //     //$stmt->execute(array(':a' => $hoteloid, ':b' => $roomoid, ':c' => $rooid, ':d' => $departure));
                            //     //$r_cq = $stmt->fetch(PDO::FETCH_ASSOC);
                            //  $r_cq['ctd'] = $hrateplan_arr[$roomoid][$rooid][$departure][3];
                            //     if(!empty($r_cq['ctd']) && $r_cq['ctd']=="y"){ 
                            //         if($cz == 0){
                            //             $night++;
                            //         }
                            //     }else if(!empty($r_cq['ctd']) && $r_cq['ctd']=="n"){
                            //         if($cz == 1){
                            //             $night--;
                            //         }
                            //     }
                            // }
                        }

                        if($extrabed > 0){
                            $extrabedavailable = "Available";
                        }else{
                            $extrabedavailable = "Not available";
                        }

                        // $s_currency = "SELECT `currencycode`, `currencytitle` FROM `currency` WHERE `currencyoid`='".$currency."'";
                        // $stmt = $db->query($s_currency);
                        // $a_currency = $stmt->fetchAll(PDO::FETCH_ASSOC);
                        // foreach($a_currency as $r_currency){
                        //     $currencyname = $r_currency['currencycode'];
                        // }
                        $currencyname = Curr::getCurrencyCode($db,$currency);
                        
                        //TEMPORARY OWNER
                        if($allowzerotrx == '1'){
                            $tmprate = $rate;
                            $rate = 1;
                        }

                        if($allotment > 0 and $blakout_check == 0 and $rate > 0){
                            
                            //TEMPORARY OWNER
                            if($allowzerotrx == '1'){
                                $rate = $tmprate;
                            }
                            
                            if($applyvalue == ""
                                or ($applyvalue != "" and Func::matchday($applyvalue, $breakdate_name) == true)
                                or ($applyvalue != "" and Func::matchnight($applyvalue, ($n+1)) == true)){
                                if($disctypeid == 4){
                                    if($discapplyid == 1 and $n < $tmp_night){
                                        $disc_status = "No";
                                    }else if($discapplyid == 4 and $n >= $discvalue){
                                        $disc_status = "No";
                                    }else if($discapplyid == 5 and $n < ($night - $discvalue)){
                                        $disc_status = "No";
                                    }else{
                                        $disc_status = "Yes";
                                    }
                                }else if($discapplyid == 4 and $n > 0){
                                    $disc_status = "No";
                                }else if($discapplyid == 5 and $n < ($night-1)){
                                    $disc_status = "No";
                                }else{
                                    $disc_status = "Yes";
                                }
                            }else{
                                $disc_status = "No";
                            }

                            $totalpernight = $rate;
                            if($disctypeid == 1){
                                if($disc_status == "Yes"){
                                    $totalpernight = $rate - ($discvalue/100 * $rate);
                                }
                            }
                            if($disctypeid == 3){
                                if($disc_status == "Yes"){
                                    $totalpernight = $rate - $discvalue;
                                }
                            }

                            if($disctypeid == 4){
                                if($disc_status == "Yes"){
                                    $totalpernight = 0;
                                }
                            }

                            $totalpernight = Func::promocodediscount($applydiscount, $pc_discount_type, $pc_discount, $totalpernight);
                            // if($loyaltyprogram == true){
                            //     if(in_array($promooid, $loyaltypromotion) and $disc_status == "Yes"){
                            //         $totalpernight = $totalpernight - ($totalpernight * $membership_discount / 100);
                            //     }
                            // }

                            $totalperroom += $totalpernight;

                            $promoNight[$n]['xml_date'] = $xml_date;
                            $promoNight[$n]['allotment'] = $allotment;
                            $promoNight[$n]['extrabedavailable'] = $extrabedavailable;
                            $promoNight[$n]['rate'] = $rate;
                            $promoNight[$n]['currencyname'] = $currencyname;
                            $promoNight[$n]['breakdate_name'] = $breakdate_name;
                            $promoNight[$n]['discapplyid'] = $discapplyid;
                            $promoNight[$n]['disc_status'] = $disc_status;
                            $promoNight[$n]['totalpernight'] = $totalpernight;
                        }
                    }

                    if($disctypeid == 2){
                        $totalperroom = $totalperroom - $discvalue;
                        foreach($promoNight as $j => $value2){
                            $promoNight[$j]['totalpernight'] = $totalperroom / count($promoNight);
                        }
                    }

                    $show = "No";
                    foreach($promoNight as $j => $value2){
                        if($value2['disc_status'] == "Yes"){
                            $show = "Yes"; break;
                        }
                    }
                    
                    //TEMPORARY OWNER
                    if($allowzerotrx == '1'){
                        $tmptotalperroom = $totalperroom;
                        $totalperroom = 1;
                    }

                    if($show == "Yes" and $totalperroom > 0){
                
                        //TEMPORARY OWNER
                        if($allowzerotrx == '1'){
                            $totalperroom = $tmptotalperroom;
                        }

                        if(empty($promoimage)){
                            $promoimage = $r_room['photourl'];
                        }


                        $p++;
                        $promotionid[$ix][$p]       = $promooid;
                        $promotionname[$ix][$p]     = $promoname;
                        $promotiontype[$ix][$p]     = 'promo';
                        $typepromo[$ix][$p]         = $promotypeoid;
                        $promotionnight[$ix][$p]    = $night;
                        $promotiontotal[$ix][$p]    = $totalperroom;
                        $promotionimage[$ix][$p]    = $promoimage;
                        $promotionheadline[$ix][$p] = $promoheadline;

                        array_push($allprice, $totalperroom/$night);

                        foreach($promoNight as $j => $value2){
                            $listBreakdownDate[$ix][$p][$j] = $value2['xml_date'];
                            $listBreakdownRate[$ix][$p][$j] = $value2['rate'];
                            $listBreakdownCurrency[$ix][$p][$j] = $value2['currencyname'];
                            $listBreakdownDiscStat[$ix][$p][$j] = $value2['disc_status'];
                            $listBreakdownTotal[$ix][$p][$j] = $value2['totalpernight'];
                        }
                    }
                }
            } // room offer promo

            if($direct_promotion == true and count($promotionid) == 0){
                $direct_promotion = false;
            }

            $this->_listBreakdownDate = $listBreakdownDate;
            $this->_listBreakdownRate = $listBreakdownRate;
            $this->_listBreakdownCurrency = $listBreakdownCurrency;
            $this->_listBreakdownDiscStat = $listBreakdownDiscStat;
            $this->_listBreakdownTotal =  $listBreakdownTotal;

            $this->_promotionid = $promotionid;
            $this->_promotionname = $promotionname;
            $this->_promotiontype = $promotiontype;
            $this->_promotionnight = $typepromo;
            $this->_promotionnight = $promotionnight;
            $this->_promotiontotal = $promotiontotal;
            $this->_promotionimage = $promotionimage;
            $this->_promotionheadline = $promotionheadline;

            $arrayPromotionAllRates= array();
            $arrayPromotionAllRates['allprice']= $allprice;
            $arrayPromotionAllRates['totalperroom']= $totalperroom;
            $arrayPromotionAllRates['direct_promotion']= $direct_promotion;
            //$_SESSION['tb_ipuser_countryoid']
            return $arrayPromotionAllRates;
        }
         function ibePackageRates($p,$ix,$promo_code,$roomoid,$rooid,$allowzerotrx,$totalpernight,$totalperroom,$allprice){
                
            // $this from Class named Detail Hotel
            $db = $this->_conn_pdo;
            $night=$this->_night;
            $hoteloid=$this->_hoteloid;
            $hotelcode=$this->_hotelcode;
            $channeloid = $this->_channeloid;
            
            $arrival = $this->_param_arrival;
            $departure = $this->_param_departure;
            $arrivalname = $this->_param_arrivalname;
            $todaytime = $this->_param_todaytime;
            $todayname = $this->_param_todayname;
            $today = $this->_param_today;
            $waiting_checkin = $this->_param_waiting_checkin;
            $default_arrival = $this->_param_default_arrival;
            $default_departure = $this->_param_default_departure;
            $default_night = $this->_param_default_night;

            $listBreakdownDate=$this->_listBreakdownDate;
            $listBreakdownRate=$this->_listBreakdownRate;
            $listBreakdownCurrency=$this->_listBreakdownCurrency;
            $listBreakdownDiscStat=$this->_listBreakdownDiscStat;
            $listBreakdownTotal=$this->_listBreakdownTotal;
            
            $promotionid = $this->_promotionid;
            $promotionname = $this->_promotionname;
            $promotiontype = $this->_promotiontype;
            $typepromo = $this->_promotionnight;
            $promotionnight = $this->_promotionnight;
            $promotiontotal = $this->_promotiontotal;
            $promotionimage = $this->_promotionimage;
            $promotionheadline = $this->_promotionheadline;

            // $pc from Class named PromoCode
            $promocode=$promo_code->promocode;
            $pc_discount_type=$promo_code->pc_discount_type;
            $pc_discount=$promo_code->pc_discount;
            $applybardiscount=$promo_code->applybardiscount;

            $xml = simplexml_load_file("myhotel/data-xml/".$hotelcode.".xml");

            if($promocode == "thebuking"){
                $query_join_promocode_pkg = "INNER JOIN (select 'y' as applydiscount, 'y' as applycommission) x";
                $query_shown_nopc = "and pr.shown_no_pc = 'y' ";
            }else{
                $query_join_promocode_pkg = " INNER JOIN (select id, applydiscount, applycommission from promocode prm inner join promocodeapply pca using (promocodeoid) where referencetable = 'package' and promocode like '".$promocode."' and (pca.hoteloid = '".$hoteloid."' or prm.hoteloid) and publishedoid = 1) x on x.id = pr.packageoid ";
                $query_shown_nopc = "";
            }
            
            // Direct Promotion
            // if(isset($_REQUEST['promotion']) and !empty($_REQUEST['promotion'])){
            //     $query_direct_promotion = "AND lower(pr.name) like '%".strtolower(htmlspecialchars_decode($_REQUEST['promotion']))."%'";
            //     $direct_promotion = true;
            // }else{
            //     $query_direct_promotion = "";
            //     $direct_promotion = false;
            // }
            // $query_direct_promotion = "";
            // $direct_promotion = false;
            
            $s_package = "SELECT pa.packageapplyoid, pr.packageoid, pr.name AS packagename, pr.headline,  pr.packageimage, pr.stay, pr.multiply, pr.discountapplyoid, pr.applyvalue, pr.discounttypeoid, pr.discountvalue, cp.name AS cpname, cp.description AS cpdesc, pr.priority, ro.roomofferoid, ro.name AS roomoffername, h.hoteloid, h.hotelname, r.roomoid, r.adult, r.child, pr.depositoid, pr.depositvalue, x.applydiscount, x.applycommission, cp.cancellationpolicyoid, cp.name as cancellationname, cp.termcondition, cp.cancellationday, cp.cancellationtype, cp.night as cancellationtypenight, cp.description as cancellationpolicy, pr.termcondition as promoterm, pr.typepackage, pr.overbooking, pr.alwaysshown, pr.multiplier, pr.multiplier_val
            FROM packageapply pa
            INNER JOIN `roomoffer` ro using (roomofferoid)
            INNER JOIN `room` r using (roomoid)
            INNER JOIN `package` pr USING (`packageoid`)
            INNER JOIN `hotel` h on h.hoteloid = pr.hoteloid
            INNER JOIN `cancellationpolicy` cp USING (`cancellationpolicyoid`)
            ".$query_join_promocode_pkg."
            WHERE pr.hoteloid = '".$hoteloid."'
            AND pa.roomofferoid = '".$rooid."'
            AND pr.salefrom <= '".$today."' AND pr.saleto >= '".$today."'
            AND pr.bookfrom <= '".$arrival."' AND pr.bookto >= '".$arrival."'
            AND pr.displayed LIKE '%".$todayname."%'
            AND pr.checkinon LIKE '%".$arrivalname."%'
            AND ((pr.timefrom <= '".$todaytime."' AND pr.timeto >= '".$todaytime."') or (pr.timefrom >= '".$todaytime."' AND pr.timeto >= '".$todaytime."' and pr.timefrom > pr.timeto) or (pr.timefrom <= '".$todaytime."' AND pr.timeto <= '".$todaytime."'and pr.timefrom > pr.timeto))
            AND ((pr.stay <= '".$night."' and pr.alwaysshown = 'n' and pr.typepackage != '2') or (pr.alwaysshown = 'y' and pr.typepackage = '2'))
            AND pr.min_bef_ci <= '".$waiting_checkin."' AND (pr.max_bef_ci = '0' or pr.max_bef_ci >= '".$waiting_checkin."')
            AND cp.publishedoid = '1' AND pr.publishedoid = '1' and pa.channeloid = '".$channeloid."' and pa.publishedoid = '1' ".$query_shown_nopc."
            AND
                (pr.packageoid IN (select pg.packageoid from packagegeo pg inner join package p using (packageoid) where countryoid = '".$_SESSION['tb_ipuser_countryoid']."' and p.hoteloid = '".$hoteloid."')
            OR
            packageoid not in (select distinct(packageoid) from packagegeo inner join package p using (packageoid) where p.hoteloid = '".$hoteloid."'))
            AND
                (pr.packageoid not in (select distinct(pgn.packageoid) from packagegeo_noshow pgn inner join package p using (packageoid) where countryoid = '".$_SESSION['tb_ipuser_countryoid']."' and p.hoteloid = '".$hoteloid."'))
            GROUP BY pr.packageoid";

            // Func::logJS($s_package,'package query');
            $stmt          = $db->query($s_package);
            $list_package   = $stmt->fetchAll(PDO::FETCH_ASSOC);
            // Func::logJS($s_package,'package result');
            foreach($list_package as $package){
                $packageoid         = $package['packageoid'];
                $package_night      = $package['stay'];
                $package_multiply   = $package['multiply'];
                $roomoid            = $package['roomoid'];
                $rooid              = $package['roomofferoid'];
                $packageapplyoid    = $package['packageapplyoid'];
                $packageimage       = $package['packageimage'];
                $packageheadline    = $package['headline'];
                $applydiscount = $package['applydiscount'];
                $applycommission = $package['applycommission'];

                $totalperroom = 0;
                $totalratepackage = 0; $totalpackage = 0;
                $zero = false;

                /* check CTA & CTD --------------------------------------------- */
                $show = "Yes";
                $passed_checkin = false;
                $n = 0;
                do{
                    $arrival    = date("Y-m-d",strtotime($default_arrival." +".$n." day"));
                    $query_tag_cta  = '//hotel[@id="'.$hoteloid.'"]/masterroom[@id="'.$roomoid.'"]/rate[@date="'.$arrival.'"]/rateplan[@id="'.$rooid.'"]/channel[@type="'.$channeloid.'"]';
                    foreach($xml->xpath($query_tag_cta) as $tag_cta){ $cta = $tag_cta->cta;}

                    //STAAH
                    // if($staah_connect){
                    //     //$stmt = $db->prepare("SELECT cta FROM `staah_rateplan` WHERE hoteloid=:a AND roomoid=:b AND roomofferoid=:c AND sdate=:d");
                    //     //$stmt->execute(array(':a' => $hoteloid, ':b' => $roomoid, ':c' => $rooid, ':d' => $arrival));
                    //     //$r_cq = $stmt->fetch(PDO::FETCH_ASSOC);
                    //     $r_cq['cta'] = $hrateplan_arr[$roomoid][$rooid][$arrival][2];
                    //     if(!empty($r_cq['cta'])){ $cta = $r_cq['cta']; }
                    // }

                    if(isset($cta) and $cta == "y"){ $n++; }else{ $passed_checkin = true; }
                }while($passed_checkin == false);
                $arrivalname    = date('D', strtotime($arrival));

                if($package['alwaysshown'] and $default_night < $package['stay']){
                    $night = $package['stay'];
                }else{
                    $night = $default_night;
                }

                $allow_package = true;
                $get_price_package = true;

                $get_package = 0;
                if($package_multiply == "y"){
                    $number_get_package = floor($night / $package_night);
                }else{
                    $number_get_package = 1;
                }

                for($n=0; $n<$night; $n++){
                    $allotment=''; $extrabed=''; $rate=''; $currency=''; $closeout='n'; $totalpernight = 0;

                    $xml_date = date("Y-m-d",strtotime($arrival." +".$n." day"));
                    $breakdate_name = date('D',strtotime($xml_date));

                    $query_tag = '//hotel[@id="'.$hoteloid.'"]/masterroom[@id="'.$roomoid.'"]/rate[@date="'.$xml_date.'"]/allotment/channel[@type="'.$channeloid.'"]/text()';
                    foreach($xml->xpath($query_tag) as $tagallotment){
                        $allotment = (int)$tagallotment[0];
                    }

                    $query_tag_closeout = '//hotel[@id="'.$hoteloid.'"]/masterroom[@id="'.$roomoid.'"]/rate[@date="'.$xml_date.'"]/closeout/text()';
                    foreach($xml->xpath($query_tag_closeout) as $tagcloseout){
                        $closeout = $tagcloseout;
                    }

                    $query_tag = '//hotel[@id="'.$hoteloid.'"]/masterroom[@id="'.$roomoid.'"]/rate[@date="'.$xml_date.'"]/rateplan[@id="'.$rooid.'"]/channel[@type="'.$channeloid.'"]';
                    foreach($xml->xpath($query_tag) as $tagrate){
                        $extrabed = (float)$tagrate->extrabed;
                        $rate = (float)$tagrate->double;
                        $currency = $tagrate->currency;

                        list($blackout, $cta, $ctd) = array($tagrate->blackout , $tagrate->cta , $tagrate->ctd);
                    }

                    //STAAH
                    if($staah_connect){
                        //$stmt = $db->prepare("SELECT allotment, closeoutroom FROM `staah_roomtype` WHERE hoteloid=:a AND roomoid=:b AND sdate=:c");
                        //$stmt->execute(array(':a' => $hoteloid, ':b' => $roomoid, ':c' => $xml_date));
                        //$r_cq = $stmt->fetch(PDO::FETCH_ASSOC);
                        $r_cq['allotment'] = $hroomtype_arr[$roomoid][$xml_date][0];
                        $r_cq['closeoutroom'] = $hroomtype_arr[$roomoid][$xml_date][1];
                        if(!empty($r_cq['allotment']) || $r_cq['allotment']=='0'){ $allotment = $r_cq['allotment']; }
                        if(!empty($r_cq['closeoutroom'])){ $closeout = $r_cq['closeoutroom']; }

                        //$stmt = $db->prepare("SELECT rate, closeoutrate, cta, ctd FROM `staah_rateplan` WHERE hoteloid=:a AND roomoid=:b AND roomofferoid=:c AND sdate=:d");
                        //$stmt->execute(array(':a' => $hoteloid, ':b' => $roomoid, ':c' => $rooid, ':d' => $xml_date));
                        //$r_cq = $stmt->fetch(PDO::FETCH_ASSOC);
                        $r_cq['closeoutrate'] = $hrateplan_arr[$roomoid][$rooid][$xml_date][1];
                        $r_cq['cta'] = $hrateplan_arr[$roomoid][$rooid][$xml_date][2];
                        $r_cq['ctd'] = $hrateplan_arr[$roomoid][$rooid][$xml_date][3];
                        $r_cq['rate'] = $hrateplan_arr[$roomoid][$rooid][$xml_date][0];
                        if(!empty($r_cq['closeoutrate'])){ $blackout = $r_cq['closeoutrate']; }
                        if(!empty($r_cq['cta'])){ $cta = $r_cq['cta']; }
                        if(!empty($r_cq['ctd'])){ $ctd = $r_cq['ctd']; }
                        if(!empty($r_cq['rate'])){ $rate = $r_cq['rate']; }
                    }

                    $blakout_check = 0; $cta_check = 0; $ctd_check = 0;
                    if((isset($blackout) and $blackout == "y") or $closeout == 'y'){
                        $blakout_check = 1;
                    }
                    if($n == $night-1){
                        $departure  = date("Y-m-d",strtotime($arrival." +".$night." day"));
                        $query_tag_ctd  = '//hotel[@id="'.$hoteloid.'"]/masterroom[@id="'.$roomoid.'"]/rate[@date="'.$departure.'"]/rateplan[@id="'.$rooid.'"]/channel[@type="'.$channeloid.'"]';
                        foreach($xml->xpath($query_tag_ctd) as $tag_ctd){
                            if($tag_ctd->ctd == "y"){ $night++; }
                        }

                        //STAAH
                        if($staah_connect){
                    //$stmt = $db->prepare("SELECT ctd FROM `staah_rateplan` WHERE hoteloid=:a AND roomoid=:b AND roomofferoid=:c AND sdate=:d");
                    //$stmt->execute(array(':a' => $hoteloid, ':b' => $roomoid, ':c' => $rooid, ':d' => $departure));
                    //$r_cq = $stmt->fetch(PDO::FETCH_ASSOC);
                            $r_cq['ctd'] = $hrateplan_arr[$roomoid][$rooid][$departure][3];
                            if(!empty($r_cq['ctd']) && $r_cq['ctd']=="y"){
                                if($cz == 0){
                            $night++;
                                }
                            }else if(!empty($r_cq['ctd']) && $r_cq['ctd']=="n"){
                                if($cz == 1){
                                    $night--;
                                }
                            }
                        }
                    }

                    if($extrabed > 0){
                        $extrabedavailable = "Available";
                    }else{
                        $extrabedavailable = "Not available";
                    }

                    $s_currency = "SELECT `currencycode`, `currencytitle` FROM `currency` WHERE `currencyoid`='".$currency."'";
                    $stmt = $db->query($s_currency);
                    $a_currency = $stmt->fetchAll(PDO::FETCH_ASSOC);
                    foreach($a_currency as $r_currency){
                        $currencyname = $r_currency['currencycode'];
                    }
                    $currencyname = empty($currencyname) ? 'IDR' : $currencyname;

                    //TEMPORARY OWNER
                    if($allowzerotrx == '1'){
                        $tmprate = $rate;
                        $rate = 1;
                    }

                    if(($package['overbooking'] == "y" and $blakout_check == 0) or ($package['overbooking'] == "n" and $allotment > 0 and $blakout_check == 0 and $rate > 0)){

                        //TEMPORARY OWNER
                        if($allowzerotrx == '1'){
                            $rate = $tmprate;
                        }

                        if(($package_multiply == "y" and $n%$package_night == 0 and $get_package < $number_get_package) or ($package_multiply == "n" and $n == 0)){
                            $allow_package = true;
                            $get_price_package = true;
                        }else if(($package_multiply == "y" and $n%$package_night > 0 and $allow_package == true) or ($package_multiply == "n" and $n < $package_night)){
                            if($package['typepackage'] != "1"){
                                $allow_package = true;
                                $get_price_package = false;
                            }
                        }else{
                            $allow_package = false;
                            $get_price_package = false;
                        }

                        /* get package rate -----------------------*/
                        $package_rate = 0;
                        if($allow_package == true and $get_price_package == true){
                            $stmt = $db->prepare("select rate from packagerate where packageapplyoid = :a and (startdate <= :b and enddate >=:c) order by priority asc limit 1");
                            $stmt->execute(array(':a' => $packageapplyoid, ':b' => $xml_date, ':c' => $xml_date));
                            $found_rate_package = $stmt->rowCount();
                            if($found_rate_package > 0){
                                $result             = $stmt->fetch(PDO::FETCH_ASSOC);
                                $package_rate       = $result['rate'];
                                if($package['typepackage'] == "1"){
                                    $rate   = $package_rate / $package_night;
                                }else if($package['typepackage'] == "2"){
                                    $rate   = $package_rate;
                                }
                                if($n % $package_night ==  0){
                                    $get_package++;
                                }
                            }else if($n == 0 and $found_rate_package == 0){
                                $zero = true;
                            }else{
                                $allow_package  = false;
                                $get_price_package = false;
                            }
                        }else if($allow_package == true and $get_price_package == false and $package['typepackage'] != 3){
                            $rate = 0;
                        }else{
                            $additional_fee = $rate * $package['multiplier_val'] / 100;
                            if($package['multiplier'] == "plus"){
                                $rate = $rate + $additional_fee;
                            }else if($package['multiplier'] == "minus"){
                                $rate = $rate - $additional_fee;
                            }
                        }

                        if($allow_package == true and $package['typepackage'] == '3'){
                            if($n == 0){
                                $additional_fee = $rate * $package['multiplier_val'] / 100;
                                if($package['multiplier'] == "plus"){
                                        $rate = $rate + $additional_fee;
                                }else if($package['multiplier'] == "minus"){
                                    $rate = $rate - $additional_fee;
                                }
                            }
                            $rate = $package_rate + $rate;
                        }

                        $totalpernight = $rate;
                        $totalpernight = Func::promocodediscount($applydiscount, $pc_discount_type, $pc_discount, $totalpernight);

                        if($loyaltyprogram == true){
                            if(in_array($promooid, $loyaltypromotion) and $disc_status == "Yes"){
                                $totalpernight = $totalpernight - ($totalpernight * $membership_discount / 100);
                            }
                        }

                        $totalperroom += $totalpernight;

                        $promoNight[$n]['xml_date']         = $xml_date;
                        $promoNight[$n]['allotment']        = $allotment;
                        $promoNight[$n]['extrabedavailable'] = $extrabedavailable;
                        if($allow_package == true and $n > 0){
                            $promoNight[$n]['rate']             = 0;
                            $promoNight[$n]['totalpernight']    = 0;
                        }else{
                            $promoNight[$n]['rate']             = $rate;
                            $promoNight[$n]['totalpernight']    = $totalpernight;
                        }
                        $promoNight[$n]['currencyname']     = $currencyname;
                        $promoNight[$n]['breakdate_name']   = $breakdate_name;
                        $promoNight[$n]['disc_status']      = "Yes";

                        if($allow_package == true){
                            $totalratepackage+=$rate;
                            $totalpackage+=$totalpernight;
                            $promoNight[0]['rate'] =    $totalratepackage;
                            $promoNight[0]['totalpernight'] = $totalpackage;
                        }
                    }else{
                        $zero = true;
                        break;
                    }
                        }

                        //TEMPORARY OWNER
                    if($allowzerotrx == '1'){
                            $tmptotalperroom = $totalperroom;
                            $totalperroom = 1;
                    }

                    if($show == "Yes" and $totalperroom > 0 and $zero == false){

                    //TEMPORARY OWNER
                        if($allowzerotrx == '1'){
                            $totalperroom = $tmptotalperroom;
                        }

                        if(empty($packageimage)){ $packageimage = $r_room['photourl']; }

                        $p++;
                        $promotionid[$ix][$p]       = $packageoid;
                        $promotionname[$ix][$p]     = $package['packagename'];
                        $promotiontype[$ix][$p]     = 'package';
                        $typepromo[$ix][$p]         = 0;
                        $promotionnight[$ix][$p]    = $night;
                        $promotiontotal[$ix][$p]    = $totalperroom;
                        $promotionimage[$ix][$p]    = $packageimage;
                        $promotionheadline[$ix][$p] = $packageheadline;

                        array_push($allprice, $totalperroom/$night);

                        foreach($promoNight as $j => $value2){
                                $listBreakdownDate[$ix][$p][$j]     = $value2['xml_date'];
                                $listBreakdownRate[$ix][$p][$j]     = $value2['rate'];
                                $listBreakdownCurrency[$ix][$p][$j] = $value2['currencyname'];
                                $listBreakdownDiscStat[$ix][$p][$j] = $value2['disc_status'];
                                $listBreakdownTotal[$ix][$p][$j]    = $value2['totalpernight'];
                        }
                    }
            } // room offer promo

            if($direct_promotion == true and count($promotionid) == 0){
                $direct_promotion = false;
            }

            $this->_listBreakdownDate = $listBreakdownDate;
            $this->_listBreakdownRate = $listBreakdownRate;
            $this->_listBreakdownCurrency = $listBreakdownCurrency;
            $this->_listBreakdownDiscStat = $listBreakdownDiscStat;
            $this->_listBreakdownTotal =  $listBreakdownTotal;

            $this->_promotionid = $promotionid;
            $this->_promotionname = $promotionname;
            $this->_promotiontype = $promotiontype;
            $this->_promotionnight = $typepromo;
            $this->_promotionnight = $promotionnight;
            $this->_promotiontotal = $promotiontotal;
            $this->_promotionimage = $promotionimage;
            $this->_promotionheadline = $promotionheadline;

            $arrayPackageRates= array();
            $arrayPackageRates['allprice']= $allprice;
            $arrayPackageRates['totalperroom']= $totalperroom;
            // $arrayPackageRates['direct_promotion']= $direct_promotion;
            //$_SESSION['tb_ipuser_countryoid']
            return $arrayPackageRates;
        }

        function ibe_parameters(){
            // include('../conf/xmlload.php');
            date_default_timezone_set("Asia/Jakarta");
            $today = date('Y-m-d');
            
            $db = $this->_conn_pdo;
            $hoteloid=$this->_hoteloid;
            $hotelcode=$this->_hotelcode;
            $chainoid = $this->_chainoid;
            $channeloid = $this->_channeloid;//1
            // $status = 1;
        
            //----------------FILE PARAMETER----------------//
            
            if(isset($this->_night) and $this->_night > 0){
                $addtocheckout = $this->_night;
            }else{
                $addtocheckout = $this->_default_auto_night;
            }
    
            if(is_array($addtocheckout)){
                $addtocheckout = $addtocheckout[0];
            }

            // if(isset($_REQUEST['requirementcheckin']) and is_numeric($_REQUEST['requirementcheckin'])){
            //     $addtocheckin = $_REQUEST['requirementcheckin'];
            // }else{
            //     $addtocheckin = 0;
            // }
    
            $addtocheckin = 0;
    
            $arrival    = date("Y-m-d",strtotime($this->_checkin));
            $departure  = date("Y-m-d",strtotime($this->_checkin." +".$addtocheckin." day"));
    
            $date_today = new DateTime($today);
            $date_arrival = new DateTime($arrival);
        
            //Allow Backdate
            $di = new DateInterval('P1D');
            $di->invert = 1;
            $c_date_today = $date_today;
            $c_date_today->add($di);
    
            $arrival    = ((isset($arrival) and !empty($arrival)) and ($c_date_today <= $date_arrival)) 
                            ? date("Y-m-d", strtotime($arrival)) : date("Y-m-d",strtotime(date('Y-m-d').' +'.$addtocheckin.' day'));
        
            $date_arrival = new DateTime($arrival);
            $date_departure = new DateTime($departure);
        
            
            $departure  = ((isset($departure) and !empty($departure)) and !isset($this->_night) and ($date_arrival < $date_departure)) 
                            ? date("Y-m-d", strtotime($departure)) : date("Y-m-d",strtotime($arrival.' +'.$addtocheckout.' day'));
        
            $show_arrival = date("j/m/Y", strtotime($arrival));
            $show_departure = date("j/m/Y", strtotime($departure));
        
            // $_SESSION['current_arrival'] = $show_arrival;
            // $_SESSION['current_departure']   = $show_departure;
        
            //----------------DEFAULT PARAMETER----------------//
            
                $default_arrival    = $arrival;
                $default_departure  = $departure;
                $default_night      = Func::differenceDate($default_arrival, $default_departure);
            
            //----------------BY ROOMTYPE----------------//
            
            $today = date('Y-m-d');
            $todayname = date('D');
            $todaytime = date('H:i:s');
            $arrivalname = date('D', strtotime($arrival));
            $waiting_checkin = Func::differenceDate($today, $arrival);

            $this->_param_arrival   = $arrival;
            $this->_param_departure = $departure;
            $this->_param_arrivalname   = $arrivalname;
            $this->_param_todaytime = $todaytime;
            $this->_param_todayname = $todayname;
            $this->_param_today = $today;
            $this->_param_waiting_checkin   = $waiting_checkin;
            $this->_param_default_arrival   = $default_arrival;
            $this->_param_default_departure = $default_departure;
            $this->_param_default_night = $default_night;
            
            
            $this->_listBreakdownDate = array();
            $this->_listBreakdownRate = array();
            $this->_listBreakdownCurrency = array();
            $this->_listBreakdownDiscStat = array();
            $this->_listBreakdownTotal =  array();
            
            $this->_promotionid = array();
            $this->_promotionname = array();
            $this->_promotiontype = array();
            $this->_typepromo = array();
            $this->_promotionnight = array();
            $this->_promotiontotal = array();
            $this->_promotiontotal_min = array();
            $this->_promotionimage = array();
            $this->_promotionheadline = array();

        }

       function ibe_loadrates(){
        // include('../conf/xmlload.php');
        
        $db = $this->_conn_pdo;
        $hoteloid=$this->_hoteloid;
        $hotelcode=$this->_hotelcode;
        $chainoid = $this->_chainoid;
        $channeloid = $this->_channeloid;//1

        //----------------FILE PARAMETER----------------//
        $this->ibe_parameters();
        
        $arrival = $this->_param_arrival;
        $departure = $this->_param_departure;
        $waiting_checkin = $this->_param_waiting_checkin;
        $arrivalname = $this->_param_arrivalname;
        $todaytime = $this->_param_todaytime;
        $todayname = $this->_param_todayname;
        $today = $this->_param_today;
        $waiting_checkin = $this->_param_waiting_checkin;
        $default_arrival = $this->_param_default_arrival;
        $default_departure = $this->_param_default_departure;
        $default_night = $this->_param_default_night;

        //-- PatchStaah
        // include_once("patch_staah.php");
        // $staah_connect = getHotelStaahConnect($hoteloid);
        // if($staah_connect){
        //  $stmt = $db->prepare("SELECT roomoid, sdate, allotment, closeoutroom FROM `staah_roomtype` WHERE hoteloid=:a AND `sdate` >= :b AND `sdate` <= :c");
        //  $stmt->execute(array(':a' => $hoteloid, ':b' => $arrival, ':c' => $departure));
        //  $r_cq = $stmt->fetchAll(PDO::FETCH_ASSOC);
        //  $hroomtype_arr = array();
        //  foreach($r_cq as $rcqx){
        //      if(empty($rcqx['closeoutroom'])) $rcqx['closeoutroom'] = "n";
        //      $hroomtype_arr[$rcqx['roomoid']][$rcqx['sdate']] = array($rcqx['allotment'],$rcqx['closeoutroom']);
        //  }
        //  $stmt = $db->prepare("SELECT roomoid, roomofferoid, sdate, rate, closeoutrate, cta, ctd FROM `staah_rateplan` WHERE hoteloid=:a AND `sdate` >= :b AND `sdate` <= :c");
        //  $stmt->execute(array(':a' => $hoteloid, ':b' => $arrival, ':c' => $departure));
        //  $r_cq = $stmt->fetchAll(PDO::FETCH_ASSOC);
        //  $hrateplan_arr = array();
        //  foreach($r_cq as $rcqx){
        //      if(empty($rcqx['cta'])) $rcqx['cta'] = "n";
        //      if(empty($rcqx['ctd'])) $rcqx['ctd'] = "n";
        //      if(empty($rcqx['closeoutrate'])) $rcqx['closeoutrate'] = "n";
        //      $hrateplan_arr[$rcqx['roomoid']][$rcqx['roomofferoid']][$rcqx['sdate']] = array($rcqx['rate'],$rcqx['closeoutrate'],$rcqx['cta'],$rcqx['ctd']);
        //  }
        //  $r_cq = array();
        // }
        //-- END PatchStaah
            
        //----------------PROMO CODE----------------//
        $pc = new PromoCode($db,$hoteloid,'');
        // Func::logJS($pc,'PromoCode');
        // $notification_promocode=$pc->notification_promocode;
        // $pc_commission_type=$pc->pc_commission_type;
        // $pc_commission=$pc->pc_commission;
        $pc_discount_type=$pc->pc_discount_type;
        $pc_discount=$pc->pc_discount;
        $applybar=$pc->applybar;
        $applybardiscount=$pc->applybardiscount;
        $applybarcommission=$pc->applybarcommission;
        $show_promocode=$pc->show_promocode;
        $show_promocode_name=$pc->show_promocode_name;
        $promocode=$pc->promocode;
        $promocodeoid=$pc->promocodeoid;
        //----------------TEMPORARY OWNER----------------//
        $stmt = $db->prepare("SELECT allowzerotrx FROM `promocode` WHERE hoteloid=:a AND promocode=:b");
        $stmt->execute(array(':a' => $hoteloid, ':b' => $promocode));
        $azxs = $stmt->fetch(PDO::FETCH_ASSOC);
        $allowzerotrx = $azxs['allowzerotrx'];
        //----------------ROOM HOTEL----------------//
        $s_id = "select r.roomoid, r.name AS roomname, h.hoteloid, r.roomsize, v.name as roomview, r.adult, bedroom.bedconfiguration
                from hotel h
                inner join room r using (hoteloid)
                left join view v using (viewoid)
                inner join roomoffer ro using (roomoid)
                inner join offertype ot using (offertypeoid)
                left join (select GROUP_CONCAT(bed.name SEPARATOR ' / ') as bedconfiguration, roomoid from bed inner join roombed using (bedoid) group by roomoid) as bedroom on bedroom.roomoid = r.roomoid
                where h.hoteloid = '".$hoteloid."' and r.publishedoid = '1' and ro.publishedoid = '1' and ot.publishedoid = '1'
                group by r.roomoid";
        $stmt   = $db->query($s_id);
        $a_id = $stmt->fetchAll(PDO::FETCH_ASSOC);
        //----------------Array----------------//

        $allprice = array();
        
        $roomid = array();
        $roomnames = array();
        $roomofferid = array();
        $roomoffername = array();
        $roomoffersize = array();
        $roomofferview = array();
        $roomoffertotal_min = array();
        $roomofferpicture = array();
        $roomofferpictureall = array();
        $roomofferfcl = array();
        $roomofferadult = array();
        $roomofferbed = array();
        $roomofferallotment = array();
                
        foreach($a_id as $a_idx){
            $roomoid = $a_idx['roomoid'];
            $roomname = $a_idx['roomname'];
            $roomsize = $a_idx['roomsize'];
            $roomview = $a_idx['roomview'];
            $roomadult = $a_idx['adult'];
            $roombed = $a_idx['bedconfiguration'];
        
            //----------------ROOM MAIN PHOTO----------------//
            $s_rp = "SELECT photourl FROM hotelphoto hp WHERE hp.hoteloid = '".$hoteloid."' AND hp.flag = 'main' AND ref_table = 'room' AND ref_id = '".$roomoid."'";
            $stmt   = $db->query($s_rp);
            $r_room = $stmt->fetch(PDO::FETCH_ASSOC);
        
            //----------------ROOM ALL PHOTO----------------//
            $sRoomPhotosAll = "SELECT r.roomoid, photourl, thumbnailurl FROM hotelphoto hp INNER JOIN room r ON hp.ref_id = r.roomoid WHERE hp.hoteloid = '".$hoteloid."' AND ref_table = 'room' AND ref_id = '".$roomoid."'";
            $stmt   = $db->query($sRoomPhotosAll);
            $rRoomPhotosAll = $stmt->fetchAll(PDO::FETCH_ASSOC);
        
            //----------------ROOM FACILITIES----------------//
            $s_rf = "SELECT GROUP_CONCAT(facilitiesoid) as roomfacilities FROM roomfacilities WHERE roomoid = '".$roomoid."'";
            $stmt   = $db->query($s_rf);
            $r_facilities = $stmt->fetch(PDO::FETCH_ASSOC);
            $roomfacilities = explode(',',$r_facilities['roomfacilities']);
        
            $sb_id = "select ro.roomofferoid, ro.name AS roomoffername, ot.offername AS offertypename, h.show_bar, ro.show_bar as ro_show_bar
                    from hotel h
                    inner join room r using (hoteloid)
                    inner join roomoffer ro using (roomoid)
                    inner join offertype ot using (offertypeoid)
                    where h.hoteloid = '".$hoteloid."' and r.roomoid = '".$roomoid."' and r.publishedoid = '1' and ro.publishedoid = '1' and ot.publishedoid = '1'";
            $stmt   = $db->query($sb_id);
            $ab_id  = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $h_show_bar = 'y';

            //----------------ROOM OFFER----------------//
            foreach($ab_id as $ab_idx){
                $p = 0;
        
                $rooid = $ab_idx['roomofferoid'];
                $roname = $ab_idx['roomoffername'];
                $offertypename = $ab_idx['offertypename'];
                $totalperroom = 0;
        
                $h_show_bar = $ab_idx['show_bar'];
                $ro_show_bar = $ab_idx['ro_show_bar'];
        
                $ix = $rooid;
                
                /*---------------- #start IBE FLEXIBLE RATES : check Allotment, Rate, Extrabed, CTA, CTD, Closeout --------------------------------------------- */
                $arrayFlexibleRates= $this->ibeFlexibleRates($p,$ix,$pc,$roomoid,$rooid,$allowzerotrx,$totalpernight,$totalperroom,$allprice);
                // Func::logJS($arrayFlexibleRates,'ibeFlexibleRates');

                $cta = $arrayFlexibleRates['cta'];
                $zero = $arrayFlexibleRates['zero'];
                $night = $arrayFlexibleRates['night'];
                $arrivalname = $arrayFlexibleRates['arrivalname'];
                $totalperroom = $arrayFlexibleRates['totalperroom'];
                $roomofferallotment[$rooid] = $arrayFlexibleRates['allotment'];
                /*---------------- #end IBE FLEXIBLE RATES --------------------------------------------- */
        
                $applybar = 'y';
                if($zero == false){
                    if($applybar == 'y'){
                        $night_bar          = $night;
                        $totalperroom_bar   = $totalperroom;
                        $photourl_bar       = $r_room['photourl'];
                    }

                    /*---------------- #start IBE PROMOTION RATES --------------------------------------------- */
                    $arrayPromotionRates= $this->ibePromotionRates($p,$ix,$pc,$roomoid,$rooid,$allowzerotrx,$totalpernight,$totalperroom,$allprice);
                    // Func::logJS($arrayPromotionRates,'arrayPromotionRates');

                    $direct_promotion = $arrayPromotionRates['direct_promotion'];
                    $totalperroom = $arrayPromotionRates['totalperroom'];            
                    $allprice = $arrayPromotionRates['allprice'];
                    /*---------------- #end IBE PROMOTION RATES --------------------------------------------- */
                    $arrayPromotionAllRates= $this->ibePromotionAllRates($p,$ix,$pc,$roomoid,$rooid,$allowzerotrx,$totalpernight,$totalperroom,$allprice);
                    // Func::logJS($arrayPromotionRates,'arrayPromotionRates');

                    $direct_promotion = $arrayPromotionAllRates['direct_promotion'];
                    $totalperroom = $arrayPromotionAllRates['totalperroom'];            
                    $allprice = $arrayPromotionAllRates['allprice'];
        
                    /* PACKAGE ========================================= */
                    
                     $arrayPackageRates= $this->ibePackageRates($p,$ix,$pc,$roomoid,$rooid,$allowzerotrx,$totalpernight,$totalperroom,$allprice);
                    // Func::logJS($arrayPromotionRates,'arrayPromotionRates');

                    // $direct_promotion = $arrayPromotionRates['direct_promotion'];
                    $totalperroom = $arrayPackakeRates['totalperroom'];            
                    $allprice = $arrayPackageRates['allprice'];
                    /* ================================================= */
        
                    if($applybar == 'y' and $direct_promotion == false and $h_show_bar == 'y' and $ro_show_bar == 'y'){
                        $s_query_fr_photos = "select hp.photourl from hotelphoto hp where hp.ref_table = 'room' and hp.flag_flexible_rate = 'y' 
                            and hp.ref_id in (select roomoid from room where hoteloid = '".$hoteloid."') order by hotelphotooid desc limit 1";
                        $stmt = $db->query($s_query_fr_photos);
                        $found_fr_image = $stmt->rowCount();
                        if($found_fr_image > 0){
                            $fr_photos = $stmt->fetch(PDO::FETCH_ASSOC);
                            $photobar = $fr_photos['photourl'];
                        }else{
                            $photobar = $photourl_bar;
                        }
        
                        $stmt = $db->query("select * from barsetting where hoteloid = '".$hoteloid."'");
                        $barSettingFound = $stmt->rowCount();
                        $hotelbar = $stmt->fetch(PDO::FETCH_ASSOC);
                        if($barSettingFound>0){
                            $hotelBarName = $hotelbar['name'];
                        }else{
                            $hotelBarName = ($chainoid==64) ? "Daily Rate Available" : "Flexible Rate";
                        }
                        
                        $this->_promotionid[$ix][0]         = 0;
                        $this->_promotionname[$ix][0]       = $hotelBarName;
                        $this->_promotiontype[$ix][0]       = 'bar';
                        $this->_typepromo[$ix][0]           = 0;
                        $this->_promotionnight[$ix][0]      = $night_bar;
                        $this->_promotiontotal[$ix][0]      = $totalperroom_bar;
                        $this->_promotionimage[$ix][0]      = $photobar;
                        $this->_promotionheadline[$ix][0]   = $hotelbar['headline'];
        
                        array_push($allprice, $totalperroom_bar/$night_bar);
                    }
        
                } // zero (room allotment and rate are available)
        
        
                if(is_array($this->_promotiontotal[$ix]))
                if(count($this->_promotiontotal[$ix]) > 0){
                    array_push($roomid, $roomoid);
                    array_push($roomnames, $roomname);
                    array_push($roomofferid, $rooid);
                    array_push($roomoffername, $roname);
                    array_push($roomoffersize, $roomsize);
                    array_push($roomofferadult, $roomadult);
                    array_push($roomofferview, $roomview);
                    array_push($roomoffertotal_min, min($this->_promotiontotal[$ix]));
                    array_push($roomofferpicture, $r_room['photourl']);
                    array_push($roomofferpictureall, $rRoomPhotosAll);
                    array_push($roomofferfcl, $roomfacilities);
                    array_push($roomofferbed, $roombed);
                }
        
            } // foreach roomoffer
        } // foreach roomtype
        
        //----------------SORT BY----------------//
        switch($default_sort_by){
            case 'high price' : arsort($roomoffertotal_min); break;
            default : asort($roomoffertotal_min); break;
        }
        //----------------SHOW LIST----------------//
        $night = $default_night;
        $main_currency = Curr::getCurrencyCode($db);
        //----------------TEMPLATE----------------//
        // if($_GET['t'] == 1){
        //     include("component/list-group-by-room-t1.php");
        // }else if($_GET['t'] == 3){
        //     include("component/list-group-by-room-t3.php");
        // }else{
        //     include("component/list-group-by-room-t2.php");
        // }

        //----------------SHOW LIST----------------//
        $roomoffer_encrypt=array();
        foreach($roomoffertotal_min as $i => $value){
            array_push($roomoffer_encrypt, Func::randomString(4).$roomofferid[$i].Func::randomString(2));
        }
        
        // foreach($roomoffertotal_min as $i => $value){
        //     $ix = $roomofferid[$i];
        //     asort($promotiontotal[$ix]);
        //     foreach($promotiontotal[$ix] as $j => $value2){
        //         $result_term_headine= $this->getTermHeadline($term,$type,$promotionid[$ix][$j]);
        //         $promo_encrypt = Func::randomString(4).$promotionid[$ix][$j].Func::randomString(2);
        //         $type = $promotiontype[$ix][$j];
        //         if($type == "package"){ $term = 'termheadlinepackage';
        //         }else{ $term = 'termheadlinepromo'; }
        //         $loop++;
        //     }
        // }

        // Func::logJS($this->_promotionid,'arrayPromotionRates');
        $arrayLoadRates = array();
        $arrayLoadRates['roomoffertotal_min'] = $roomoffertotal_min;
        $arrayLoadRates['roomid'] = $roomid;
        $arrayLoadRates['roomnames'] = $roomnames;
        $arrayLoadRates['roomofferid'] = $roomofferid;
        $arrayLoadRates['roomoffername'] = $roomoffername;
        $arrayLoadRates['roomoffersize'] = $roomoffersize;
        $arrayLoadRates['roomofferadult'] = $roomofferadult;
        $arrayLoadRates['roomofferview'] = $roomofferview;
        $arrayLoadRates['roomofferpicture'] = $roomofferpicture;
        $arrayLoadRates['roomofferpictureall'] = $roomofferpictureall;
        $arrayLoadRates['roomofferfcl'] = $roomofferfcl;
        $arrayLoadRates['roomofferbed'] = $roomofferbed;
        $arrayLoadRates['roomoffer_encrypt'] = $roomoffer_encrypt;

        $arrayLoadRates['allotment_roomoffer'] = $roomofferallotment;

        $arrayLoadRates['allprice'] = $allprice;
        $arrayLoadRates['arrival'] = $arrival;
        $arrayLoadRates['night'] = $night;
        $arrayLoadRates['main_currency'] = $main_currency;

        $arrayLoadRates['show_promocode'] = $show_promocode;
        $arrayLoadRates['show_promocode_name'] = $show_promocode_name;
        $arrayLoadRates['pc_discount_type'] = $pc_discount_type;
        $arrayLoadRates['pc_discount'] = $pc_discount;
        $arrayLoadRates['wording_pcd'] = ($pc_discount_type == 'discount percentage'?" - ".$pc_discount."% OFF":" - <br>discount IDR ".number_format($pc_discount)."");
        
        $arrayLoadRates['promotionid'] = $this->_promotionid;
        $arrayLoadRates['promotionname'] = $this->_promotionname;
        $arrayLoadRates['promotiontype'] = $this->_promotiontype;
        $arrayLoadRates['typepromo'] = $this->_typepromo;
        $arrayLoadRates['promotionnight'] = $this->_promotionnight;
        $arrayLoadRates['promotiontotal'] = $this->_promotiontotal;
        $arrayLoadRates['promotionimage'] = $this->_promotionimage;
        $arrayLoadRates['promotionheadline'] = $this->_promotionheadline;

        $arrayLoadRates['totalperroom'] = $totalperroom;
        $arrayLoadRates['listBreakdownDate'] = $this->_listBreakdownDate;
        $arrayLoadRates['listBreakdownRate'] = $this->_listBreakdownRate;
        $arrayLoadRates['listBreakdownCurrency'] = $this->_listBreakdownCurrency;
        $arrayLoadRates['listBreakdownDiscStat'] = $this->_listBreakdownDiscStat;
        $arrayLoadRates['listBreakdownTotal'] = $this->_listBreakdownTotal;
        return $arrayLoadRates;
    }
       
    function getTermHeadline($term,$type,$id){
        $db=$this->_conn_pdo;
        $syntax_term_headine = "select th.*, i.* from termheadline th left join icon i using (iconoid) 
        inner join ".$term." thp using (termheadlineoid) where type='".$type."' and id='".$id."'";
        $stmt   = $db->query($syntax_term_headine);
        $result_term_headine = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result_term_headine;
    }
}
?>
