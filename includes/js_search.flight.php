<script>
	$(function(){		
		$("#sort-by").change(function(){
			//$("#blocking").removeClass("hide");
			//appendUrl("sortflight", $(this) );
			getData($(this).val());
		});
		
		$(".choose").live('click', function(){
			var $result = "";
			var $elem = $(this);
			
			var $td0 = $elem.parent().parent().attr("code");
			var $td1 = $elem.parent().parent().children("td:first-child").html();
			var $td1_1 = $elem.parent().parent().children("td:first-child").children("img");
			var $td2 = $elem.parent().parent().children("td:nth-child(2)").html();
			var $td3 = $elem.parent().parent().children("td:nth-child(3)").html();
			var $td4 = $elem.parent().parent().children("td:nth-child(4)").html();
			var $td6 = $elem.parent().parent().children("td:nth-child(6)").html();
			var $td7 = $elem.parent().parent().children("td:nth-child(7)").html();
			
			$td6 = $td6.replace('h','Hours').replace('j','Hours').replace('m','Minutes');
			
			var $p_adult = $elem.parent().parent().children("td:nth-child(7)").children("strong").attr("dt-a");
			var $p_child = $elem.parent().parent().children("td:nth-child(7)").children("strong").attr("dt-c");
			var $p_infant = $elem.parent().parent().children("td:nth-child(7)").children("strong").attr("dt-i");
			$price_det = "";
			$price_det += "<?=$fl_adultflight?> Adult x "+$p_adult;
			if($p_child != "0.00"){$price_det += " + <?=$fl_childflight?> Child x "+$p_child;}
			if($p_infant != "0.00"){$price_det += " + <?=$fl_infantflight?> Infant x "+$p_infant;}
			
			var $via = "via "+$elem.parent().parent().children("td:nth-child(5)").attr("dt-via");
			var $airport = $elem.parent().parent().children("td:nth-child(5)").attr("dt-apt");
			
			if($elem.attr('data-type') == "go"){
				$result += "<div code='"+$td0+"'>"+$td1+" &nbsp; &bull; <strong>Depart Flight</strong> (<?=date("D, d F Y",strtotime($fl_departflight))?>)</div>";
				$result += "<div><ul><li>"+$td2+"</li>";
				$result += "<li><?=$fl_fromflight?> to <?=$fl_toflight?></li>";
				$result += "<li>"+$td3+" - "+$td4+"<br><span>"+$td6+"</span></li>";
				$result += "<li>"+$td7+"</li></ul></div>";
				$result += "<div class='small-desc'>"+$airport+"</div>";
				$result += "<div class='small-desc'>"+$price_det+"</div>";				
				$result += "<div class='small-desc'>"+$via+"</div>";
				$("#choose_depart").html($result);
				
				$("#d_code").val($td0);
				$("#d_img").val(encodeURIComponent($td1_1.attr('src')));
				$("#d_alt").val($td1_1.attr('alt'));
				$("#d_date").val("<?=$fl_departflight?>");
				$("#d_flnum").val($td2);
				$("#d_from").val("<?=$fl_fromflight?>");
				$("#d_to").val("<?=$fl_toflight?>");
				$("#d_dtime").val($td3);
				$("#d_atime").val($td4);
				$("#d_dur").val($td6);
				$("#d_cadlt").val("<?=$fl_adultflight?>");
				$("#d_cchld").val("<?=$fl_childflight?>");
				$("#d_cinft").val("<?=$fl_infantflight?>");
				$("#d_padlt").val($p_adult);
				$("#d_pchld").val($p_child);
				$("#d_pinft").val($p_infant);
				$("#d_via").val($via);
				$("#d_apt").val($airport);
				
			}else if($elem.attr('data-type') == "ret"){
				$result += "<div code='"+$td0+"'>"+$td1+" &nbsp; &bull; <strong>Return Flight</strong> (<?=date("D, d F Y",strtotime($fl_returnflight))?>)</div>";
				$result += "<div><ul><li>"+$td2+"</li>";
				$result += "<li><?=$fl_toflight?> to <?=$fl_fromflight?></li>";
				$result += "<li>"+$td3+" - "+$td4+"<br><span>"+$td6+"</span></li>";
				$result += "<li>"+$td7+"</li></ul></div>";
				$result += "<div class='small-desc'>"+$airport+"</div>";
				$result += "<div class='small-desc'>"+$price_det+"</div>";				
				$result += "<div class='small-desc'>"+$via+"</div>";
				$("#choose_return").html($result);
				
				$("#r_code").val($td0);
				$("#r_img").val(encodeURIComponent($td1_1.attr('src')));
				$("#r_alt").val($td1_1.attr('alt'));
				$("#r_date").val("<?=$fl_returnflight?>");
				$("#r_flnum").val($td2);
				$("#r_from").val("<?=$fl_toflight?>");
				$("#r_to").val("<?=$fl_fromflight?>");
				$("#r_dtime").val($td3);
				$("#r_atime").val($td4);
				$("#r_dur").val($td6);
				$("#r_cadlt").val("<?=$fl_adultflight?>");
				$("#r_cchld").val("<?=$fl_childflight?>");
				$("#r_cinft").val("<?=$fl_infantflight?>");
				$("#r_padlt").val($p_adult);
				$("#r_pchld").val($p_child);
				$("#r_pinft").val($p_infant);
				$("#r_via").val($via);
				$("#r_apt").val($airport);
				
			}
			$price_depart = $("#choose_depart").children("div:nth-child(2)").children("ul").children("li:last-child").text();
			$price_return = $("#choose_return").children("div:nth-child(2)").children("ul").children("li:last-child").text();
			$price_depart = ($price_depart != "") ? $price_depart.replace(new RegExp(',', 'g'),'') : 0;
			$price_return = ($price_return != "") ? $price_return.replace(new RegExp(',', 'g'),'') : 0;
			$total = parseFloat($price_depart) + parseFloat($price_return);
			$("#choose_total").addClass("choose-total-bg");
			$("#choose_total").html("<span class='grey'>Total : </span> &nbsp; &nbsp; <span class='blue'>"+addSeparatorsNF($total, '.', '.', ',')+"</span><span class='fl_right'><a class='button book' id='booknow'>Book Now</a></span>");
			
			$("#flight_choosed").css('display', 'block');
		});
		
		$("#booknow").live("click",function(){
			$(this).closest("form").submit();
		});
	});
	
	$(document).ajaxStart(function() {
		$("#blocking").removeClass("hide");
	});
	
	$(document).ajaxStop(function() {
		$("#blocking").addClass("hide");
	});
	
	function appendUrl(parameterUrl, triggerElem){
		var elemVal = triggerElem.val();
		var currentUrl = "<?=$base_url."/".$uri2."/".$uri3."/".$uri4;?>";
		var parameterUrlSelected = "&"+parameterUrl+"=";
		if(currentUrl.indexOf(parameterUrlSelected) == -1){
			var redirectUrl = currentUrl + parameterUrlSelected + elemVal;
		}else{
			var indexBeforeStars = currentUrl.indexOf(parameterUrlSelected);
			var indexAfterStars = currentUrl.indexOf("&", indexBeforeStars + 1);
			if(indexAfterStars == -1){
				var redirectUrl = currentUrl.substr(0, indexBeforeStars) + parameterUrlSelected + elemVal;
			}else{
				var redirectUrl = currentUrl.substr(0, indexBeforeStars) + parameterUrlSelected + elemVal + currentUrl.substr(indexAfterStars, currentUrl.length - indexAfterStars);
			}
		}
		
		setTimeout(function () {
			document.location.href = redirectUrl;
		}, 2000);
	}
</script>