<!-- Nyro Modal -->
<link rel="stylesheet" href="<?=$base_url;?>/scripts/nyromodal/styles/nyroModal.css" type="text/css" media="screen" />
<script type="text/javascript" src="<?=$base_url;?>/scripts/nyromodal/jquery.nyroModal.custom.min.js"></script>
<script type="text/javascript">
$(function() {
  $('.nyroModal').nyroModal();
});
</script>                    
<style>#map-canvas { height: 400px; width:600px; }</style>
<script type="text/javascript">
  function initialize() {
	var mapOptions = {
	  center: { lat: -8.730165, lng: 115.178646},
	  zoom: 17
	};
	var map = new google.maps.Map(document.getElementById('map-canvas'),
		mapOptions);
	var marker = new google.maps.Marker({
	  position: { lat: -8.730165, lng: 115.178646},
	  map: map,
	  title: 'Fave ByPass Kuta'
	});
  }
  google.maps.event.addDomListener(window, 'load', initialize);
</script>

<div id="googlemapsview" style="position:absolute; left:-9999px;"><div id="map-canvas"></div></div>

<ul class="content-list white-box border-box" id="list-hotel">
<?php
	$gethotel = 0;
	include("list.hotel.query.php");
	while($hotel = mysqli_fetch_array($q_hotel)){
		
		$hoteloid = $hotel['hoteloid']; 
		$hotelname = $hotel['hotelname']; 
		$star = $hotel['stars'];  
		$address = $hotel['address']; 
		$city = $hotel['cityname']; 
		$state = $hotel['statename'];
		$country = $hotel['countryname'];
		$continent = $hotel['continentname'];
		$hoteltype = $hotel['category']; 
		
		/*------------------------- Hotel Picture -------------------------*/
		$s_hotelphoto = "SELECT * FROM hotelphoto hp WHERE hoteloid = '$hoteloid' AND hp.flag = 'main' AND hp.ref_table = 'hotel'";
		$q_hotelphoto = mysqli_query($conn, $s_hotelphoto) or die(mysqli_error());
		$r_hotelphoto = mysqli_fetch_array($q_hotelphoto);
		$hotelpict = $r_hotelphoto['photourl'];
		
		include('list.hotel.rate.promotion.mobile.php');
		
		if($page == "home" and $minrate > 0){
		?>
			<li class="border-box">
			<a href="<?=$href_hotel;?>" class="clear">
				<div class="pict inline-block top thumb">
					<img src="<?=$hotelpict;?>">
				</div>
				
				<div class="inline-block top">
					<div><span class="title"><?=$hotelname;?></span></div>
					<div><?=$city;?></div>
				</div>
				<div class="fl_right inline-block right">	
					<div class="star">
						<?=starRating($star, 1);?>
					</div>
					<div class="price">
						start from 
						<h4><span class="strikethrough">
							<?php if($ratenet > $minrate){ echo $currencycode."&nbsp;". $shownetrate."<br>";  }?>
						</span></h4>
						<h2 class="rate">
							<span class="grey"><?=$currencycode;?></span>
							<span class="blue"><?=$hotel_minrate_final;?></span>
						</h2>
					</div>
				</div>
			</a>
			</li>
		<?php
			if($gethotel == 5){ break;}	
		}else if($page == "search-hotel" and $minrate > 0){
		?>
			<li id="h-<?=$hoteloid?>">
				<div class="inline-block top center">
                	<a href="<?=$href_hotel;?>">
					<div class="pict thumb"><img src="<?=$hotelpict;?>"></div>
                    </a>
					<a href="#googlemapsview" class="button view-map nyroModal"> View Map</a>
                    <!-- Go to www.addthis.com/dashboard to customize your tools -->
					<div class="addthis_sharing_toolbox"></div>
                    <!-- Go to www.addthis.com/dashboard to customize your tools -->
					<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-543ce7af1ad924c7" async></script>

				</div>
				<div class="col-2 inline-block top">
                <a href="<?=$href_hotel;?>">
					<div>
						<span class="title"><?=$hotelname;?></span>
						<div class="star inline-block top"><?php starRating($star, 2);?></div>
					</div>
					<div><i class="small-desc"><?=$address;?></i></div>
                </a>
					<div> 
						<div class="inline-block">
                        	<?=$city;?> . <span class="capitalize"><?=$hoteltype;?></span> - &quot;There are 4 people looking at this hotel.&quot;
                        </div>
					</div>
					<div><?=$hotel_headline;?></div>
				</div>
				
				<div class="col-3 inline-block top">
					<div class="price">
						start from 
						<h4><span class="strikethrough light-grey">
							<?php if($ratenet > $minrate){ echo $currencycode."&nbsp;". $shownetrate."<br>";  }?>
						</span></h4>
						<h2 class="rate">
							<span class="grey"><?=$currencycode;?></span>
							<span class="blue"><?=$hotel_minrate_final;?></span>
						</h2>
					</div>
					<div class="bottom-right"><a class="button book" href="<?=$href_hotel;?>">BOOK NOW</a></div>
				</div>
    			<div class="listroom">
                <ul>
               		<?php
						foreach($promoname as $key => $name){
							echo "<li><a href='".$href_hotel."'>";
							echo "<span class='tosca-bold fl_right'>".$price[$key]."</span>";
							echo "<span class='tosca-bold'>".$name."</span>";
							echo "</a></li>";
						}
					?>
                </ul>
                </div>
			</li>
        <?php
		}else if($page == "detail-hotel" and $minrate > 0){ //detail hotel
		?>
			<li class="border-box inline-block top">
				<div class="pict inline-block top thumb"><img src="<?=$hotelpict;?>"></div>
				<div class="inline-block top" style="width:128px">
					<div>
						<div class="title inline-block"><a href="<?=$href_hotel;?>" class="text"><?=$hotelname;?></a></div>
						<div class="star top"><?=starRating($star, 1);?></div>
					</div>
					<div class="price">
						<div class="inline-block top">from</div>
						<div class="inline-block top fl_right">
							<h4 class="rate">
								<span class="grey"><?=$currencycode;?></span>
								<span class="blue"><?=$hotel_minrate;?></span>
							</h4>
						</div>
					</div>
				</div>
			</li>
		<?php 
			if($gethotel == 3){ break; }
		}
	}
	
	if($gethotel == 0){
	?>
        <img src="<?=$base_url;?>/images/warning.png" class="inline-block top" />
        <div class="inline-block top">Sorry, no results match your criteria.</div>
	<?php
	}
?>
</ul>