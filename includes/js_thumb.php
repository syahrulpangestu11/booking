<script>
	$(window).ready(function(){
		
		$(".thumb img").each(function() {
			var elem = $(this);
			var elem_src = elem.attr("src");
			
			//--- Fungsi mereplace image jika src kosong
			if(elem_src.length == 0){
				elem.attr({
					"src": "<?php echo"$base_url"; ?>/images/default-thumb.png",
					"title": "Image for this item are not found."
				});
			}//end of //--- Fungsi mereplace image jika src tidak ditemukan
			
			//--- Fungsi mereplace image jika src tidak ditemukan
			elem.error(function() {
				//alert('Handler for .error() called.');
				elem.attr({
					"src": "<?php echo"$base_url"; ?>/images/default-thumb.png",
					"title": "Image for this item are not found."
				});
			});// end of //--- Fungsi mereplace image jika src tidak ditemukan
		});//end of $(".thumb img").each(function() {
		
	});
	
	$(window).load(function(){
		
		setTimeout(function(){
		
			//--- Fungsi Thumbnail
			$(".thumb img").each(function(){
				$(this).on("load",function(){
					var elem = $(this);
					var elem_parent = $(this).parent(".thumb");
					var imgWidth = elem.width();
					var imgHeight = elem.height();
					
					//elem_parent.html(setMargin);
					if(imgWidth > imgHeight){ //jika image LANDSCAPE, heightnya 100%
						elem.css({ 
							"height" : "100%" 
						});
						var elem_parentWidth = elem_parent.width();
						var elemWidth = elem.width();
						var setMargin = ( elemWidth - elem_parentWidth ) / 2;
						elem.css({ 
							"margin-left" : "-"+setMargin+"px" 
						});
						if(elemWidth < elem_parentWidth){
							elem.css({
							"width" : "100%",
							"height" : "auto"
						});
						}
					}else{
						elem.css({
							"width" : "100%"
						});
						var elem_parentHeight = elem_parent.height();
						var elemHeight = elem.height();
						var setMargin = ( elemHeight - elem_parentHeight ) / 2;
						elem.css({ 
							"margin-top" : "-"+setMargin+"px" 
						});
					}
					
				});
				
				$(this).trigger("load");
			});
			
		
		},100);
		/*
		setTimeout(function(){
			$(".thumb img").trigger("load"); 
		},3000);
		*/
		//$(".thumb img").load();
	});
	
	
	<?php /*
		
		var $images = $(".thumb img")
		, imageCount = $images.length
		, counter = 0;
		
		// one instead of on, because it need only fire once per image
		$images.one("load",function(){
		
			// increment counter everytime an image finishes loading
		    counter++;
		    if (counter == imageCount) {
		
		    	// do stuff when all have loaded
				var elem = $images;
				var elem_parent = $images.parent(".thumb");
				var imgWidth = elem.width();
				var imgHeight = elem.height();
				var setMargin = ( elem.width() - elem_parent.width() ) / 2;
				//elem_parent.html(setMargin);
				if(imgWidth > imgHeight){
					elem.css({
						"height" : "100%"
						//"margin-left" : "-50px"
						//"position" : "absolute"
					});
					var elem_parentWidth = elem_parent.width();
					var elemWidth = elem.width();
					if(elemWidth < elem_parentWidth){
						elem.css({
						"width" : "100%",
						"height" : "auto"
						//"margin-left" : "-50px"
						//"position" : "absolute"
					});
					}
				}else{
					elem.css({
						"width" : "100%"
					});
				}
				

		     } 
		}).each(function () {
		    if (this.complete) {
		
		        // manually trigger load event in
		        // event of a cache pull
		        $(this).trigger("load");
		    }
		});
		*/ ?>
	
	
	<?php /*
	function ImgProblem(source){
		source.src = "<?php echo"$base_url"; ?>/images/default-thumb.png";
		source.onerror = "";
		return true;
	} */
	?>
</script>