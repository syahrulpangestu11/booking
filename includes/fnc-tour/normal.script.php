<script type="text/javascript">
Number.prototype.formatMoney = function(c, d, t){
var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};
function isNumber(n) { return !isNaN(parseFloat(n)) && isFinite(n); }

$(function(){
	$('select[tour="number"]').live('change',function(){ 
		elem = $(this);
		var paxpkg = parseFloat(elem.val());
		var tourroomoid = elem.parent().children('input[id=tourroomoid]').val();
		var checkin =  $('#checkin').val();
		var type = "tour";
		var boxdetail = $('#book-summary').children('div[id=book-summary-dtl]');
		if(paxpkg > 0){
			$.ajax({  
				type: 'POST', cache: false,
				url: '<?php echo"$base_url"; ?>/includes/fnc-tour/normal.funtion.get-rate.php',
				data: { paxpkg : paxpkg, checkin : checkin, tourroomoid : tourroomoid },
				success: function(html){
					if($('div[dtl="'+type+"-"+tourroomoid+'"]').html() == null){ 
						boxdetail.append($('<div dtl="'+type+"-"+tourroomoid+'">'+html+'</div>'));		
					}else{
						 //$('div[dtl="'+type+"-"+tourroomoid+'"]').html('');
						 $('div[dtl="'+type+"-"+tourroomoid+'"]').html(html); 
					}
					totalTour();
				}						 
			});
		}else if(paxpkg == 0){
			$('div[dtl="'+type+"-"+tourroomoid+'"]').remove();
			totalTour();
		}
	});
	
	function totalTour(){
		var placecurrency = $('#book-summary').children('h2').eq(0).children('span.grey'); 
		var placetotal = $('#book-summary').children('h2').eq(0).children('span.blue');
		var currency = $("input[box='totalcurrency']").val();
		var gt_tour = 0; $("input[box='totaltour']").each(function(){ total = parseFloat($(this).val()); gt_tour = gt_tour + total; });
		formated_gt_tour	= (gt_tour).formatMoney(0, '.', ',');  
		placecurrency.html(currency);
		placetotal.html(formated_gt_tour);
	}
});	
</script>  