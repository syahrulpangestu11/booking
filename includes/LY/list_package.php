<div class="center" id="title-normalpackage"><h2><span class="grey">Packages</span></h2></div>

<?php
//PAGING
$batas=5; 

$halaman= isset($_GET['np_page']) ? $_GET['np_page'] :'';
if(empty($halaman)){ 
	$posisi=0; $halaman=1;
}else{ 
	$posisi = ($halaman-1) * $batas; 
}
$no=$posisi+1;


?>
<ul class="content-list white-box border-box" id="list-product">
<?php	/*SHOW LIST PACKAGE*/
	$j=0;
	$todaydate=date("Y-m-d");

	if($uri2=="" or empty($uri2)){	// [homepage] show package in the homepage selain free and easy
		if($flag_package == "homepage"){
			$s_group_package="select packagename, package.packageoid, package.picture, package.description, package.facilities, package.headline, package_pax, packageroom.extrabed, package.hoteloid, packageroom.roomoid, cityname from package inner join hotel using (hoteloid) inner join packageroom using (packageoid) inner join packagerate using (packagedtloid) inner join city using (cityoid) where packagestatus IN ('1','2') AND citystatus = '1' and packagerate.endbook>='$todaydate' AND packagename NOT LIKE '%free%' group by packagename ORDER BY rand()";
			$limit = " limit 4 ";
		}else if($flag_package == "freeandeasy"){
			$s_group_package="select packagename, package.picture, package.description, package.facilities, package.headline, package_pax, cityname from package inner join hotel using (hoteloid) inner join packageroom using (packageoid) inner join packagerate using (packagedtloid) inner join city using (cityoid) where packagestatus IN ('1','2') AND citystatus = '1' and packagerate.endbook>='$todaydate' AND packagename LIKE '%free%' GROUP BY packagename ORDER BY rand()";
			$limit = " limit 3 ";
		}
		$style2 = " display:none; ";
	}else if( $uri2=="package"  and !empty($uri3)){			
		$s_group_package="select packagename, package.packageoid, package.picture, package.description, package.facilities, package.headline, package_pax, packageroom.extrabed, package.hoteloid, packageroom.roomoid, cityname from package inner join hotel using (hoteloid) inner join packageroom using (packageoid) inner join packagerate using (packagedtloid) inner join city using (cityoid) where packagestatus IN ('1','2') AND citystatus = '1' and packagerate.endbook>='$todaydate' and cityname='$decode_city' group by packagename";
		$limit = " LIMIT $posisi,$batas ";
	}else if( $uri2=="searchpackage" or $uri2=="searchtours" ){			// [package] show package in the selected city show all
		$destination_query = ($destination != "") ? "where cityname='".$destination."'" : ""; 
		
		$s_group_package="select packagename, package.packageoid, package.picture, package.description, package.facilities, package.headline, package_pax, packageroom.extrabed, package.hoteloid, packageroom.roomoid, cityname from package inner join hotel using (hoteloid) inner join packageroom using (packageoid) inner join packagerate using (packagedtloid) inner join city using (cityoid) where packagestatus IN ('1','2') and packagerate.endbook>='$todaydate' and 
		(packagerate.startdate<='$checkin' and packagerate.enddate>='$checkout')  
		AND citystatus = '1'
		and cityoid IN (select cityoid from city inner join hotel using (cityoid) inner join package using (hoteloid) $destination_query group by cityoid order by cityname) group by packagename";
		$limit = " LIMIT $posisi,$batas ";
	}else if( $uri2=="search" ){	// [search]
		$search_q = $_REQUEST['q'];
		$s_group_package="select DISTINCT packagename, package.packageoid, package.picture, package.description, package.facilities, package.headline, package_pax, packageroom.extrabed, package.hoteloid, packageroom.roomoid, cityname from package inner join hotel using (hoteloid) inner join packageroom using (packageoid) inner join packagerate using (packagedtloid) inner join city using (cityoid) where packagestatus IN ('1','2') AND citystatus = '1' and packagerate.endbook>='$todaydate' AND packagename LIKE '%$search_q%' group by packagename";
		$limit = " LIMIT $posisi,$batas ";
	}else{	// [allpackage] show package in the selected city limit 10
		$destination_query = ($destination != "") ? " and cityoid IN (select cityoid from city inner join hotel using (cityoid) inner join package using (hoteloid) where cityname='".$destination."' group by cityoid order by cityname) " : ""; 
		$s_group_package="select DISTINCT packagename, package.packageoid, package.picture, package.description, package.facilities, package.headline, package_pax, packageroom.extrabed, package.hoteloid, packageroom.roomoid, cityname from package inner join hotel using (hoteloid) inner join packageroom using (packageoid) inner join packagerate using (packagedtloid) inner join city using (cityoid) where packagestatus IN ('1','2') AND citystatus = '1' and packagerate.endbook>='$todaydate'  $destination_query group by packagename"; 
		 /*select DISTINCT packagename, package.picture  from package inner join hotel using (hoteloid) inner join city using (cityoid) where packagestatus IN ('1','2') group by packagename*/
		$limit = " LIMIT $posisi,$batas ";
	}
	$q_group_package=mysqli_query($conn, $s_group_package.$limit) or die(mysqli_error());
	$q_normal_package = $q_group_package;
	
	$num_group_package=mysqli_num_rows(mysqli_query($conn, $s_group_package));
	$style1 = ($num_group_package <= $batas) ? " display:none; " : "" ;
	if($num_group_package <= 0) { //untuk judul search
		?><style>
		#title-normalpackage, #list-product{display: none;} </style><?php
	}
	
	while($group_package=mysqli_fetch_array($q_group_package)){
		$group_package['packagename'] = mysqli_real_escape_string($group_package['packagename']);	
		
		$group_name=$group_package['packagename'];
		$encode_group_name=str_replace('%2F', '_', urlencode($group_name));
		
		include('includes/LY/new.minrate.package.php');
		//$display_price = ($currencycode == "IDR") ? substr_replace($nominal_minrate, "K", -4) : $nominal_minrate ;
		//$show_minrate=$currencycode." ".$display_price;
		$display_price = miniThousand($nominal_minrate);
		$show_minrate="<h2 class='rate'>
							<span class='grey'>".$currencycode."</span>
							<span class='blue'>".$display_price."</span>
						</h2>";
		/* menghitung jumlah package-------------------------------------------------------------------------------------*/
		$s_countpkg="select count(distinct packageoid) as jmlpkg, min(packagerate.startdate) as mindate from package inner join hotel using (hoteloid) inner join packageroom using (packageoid) inner join packagerate using (packagedtloid) inner join city using (cityoid) where packagename='$group_package[packagename]' and packagestatus IN ('1','2') and ( (startbook<='$todaydate' and endbook>='$todaydate') or startbook>='$todaydate')";
		$q_countpkg=mysqli_query($conn, $s_countpkg); $countpkg=mysqli_fetch_array($q_countpkg);
		/*----------------link ke halaman detail package yang berisi list hotel----------------*/
		$checkin_date=strtotime($checkin);
		$mindate_pkg=strtotime($countpkg['mindate']);
		if($checkin_date<=$mindate_pkg){ $tgl_checkin=$countpkg['mindate']; $checkin_new=$countpkg['mindate'];
		}else{ $checkin_new=$checkin; }
		if($night<$minnight){ $night=$minnight; }
		
		/*if($countpkg['jmlpkg']==1){
			$href= $base_url."/bookpackage/".$packagedtloid."/".$checkin_new."/".$night."/".$adult."/".$child."/".$getpackage;
		}else{
			if($uri2=="searchpackage"){	// [homepage]  button show all package
				$href= $base_url."/listpackage/".$encode_group_name."/".$checkin_new."/".$night."/".$destination."/".$adult."/".$child."/".$getpackage;
			}else{
				$href=$base_url."/listpackage/".$encode_group_name;
			} 
		}*/
		$href = "#";
		
		//when image doesn't exist, use default-thumb.png
		$pict = ($group_package['picture'] != "") ? $group_package['picture'] : $base_url."/images/default-thumb.png";
		//$pict = $group_package['picture'];
		$headline = $group_package['headline'];  $facilities = $group_package['facilities']; $description = $group_package['description']; $destination_label = $group_package['cityname'];
		if(!empty($headline)){ $showdetail = $headline; }else{ $showdetail = "<div class='readmore-less'>".$description."</div>"; }
		$j++; //untuk jumlah row di homepage
		unset($get_min_rate);
		
		
		$group_name = stripslashes($group_name);
			?>
			<li class="top">
				<div class="thumb pict inline-block top">
					<img src="<?=$pict;?>">
				</div>
				<div class="col-2 inline-block top">
					<div class="title border-box">
						<img src="<?=$base_url;?>/images/blank.gif" class="flag flag-id" alt="Czech Republic" />
						<?=$group_name;?>
					</div>
					<div class="small-desc"><?=$showdetail;?></div>
				</div>
				<div class="fl_right inline-block right">
					<div class="price">
						from <?=$show_minrate;?> /pax
					</div>
					<div class="bottom-right">
						<a href="<?=$href;?>" class="button book inline-block">BOOK</a>
					</div>
				</div>
			</li>
			<?php

	} /*----------- while group name package -----------*/
/*} ----------- while group city / destination-----------*/
?>
</ul>

<?php /*if($uri2=="" or empty($uri2)){	// [homepage]  button show all package ?>
	<br>
	<div class="display-block clear margin-top-minus-10">
		<a href="<?php echo $base_url; ?>/allpackage" class="btn-showall">view all packages</a>
	</div>
	<br>
<?php } */?>


<!-- PAGING part2 -->
<div class="white-box border-box center clear" style="<?=$style1.$style2;?>">
	<?php
	if(!empty($uri4)){
		$file=$uri4."&";
	}else{
		$file="?";
	}
	$tampil2=$s_group_package; 
	
	$hasil2=mysqli_query($conn, $tampil2); $jmldata=mysqli_num_rows($hasil2); 
	$jmlhalaman=ceil($jmldata/$batas);
	?>
	<div class="inline-block fl_left small-desc middle">
		<?php
		$crndata=mysqli_num_rows($q_normal_package);
		echo "Displaying ".($posisi+1)." to ".($posisi+$crndata)." of ".$jmldata." items in Package";
		?>
	</div>
	<div class="inline-block fl_right">
		<?php
		//link ke halaman sebelumnya (previous)
		if($halaman > 1){	$previous=$halaman-1; echo "<a href=".$file."np_page=1 > &lt;&lt; First&nbsp;&nbsp;</a><a href=".$file."np_page=$previous > &lt; Previous&nbsp;&nbsp;</a>";
		}else{	echo "&lt;&lt; First&nbsp;&nbsp; &lt; Previous&nbsp;&nbsp;";
		}
		$angka=($halaman > 3 ? " ... " : " ");
		for($i=$halaman-2;$i<$halaman;$i++){
		  if ($i < 1) continue;
		  $angka .= "<a href=".$file."np_page=$i  >$i&nbsp;</a> ";
		}
		$angka .= " <b>$halaman</b> ";
		for($i=$halaman+1;$i<($halaman+3);$i++){
		  if ($i > $jmlhalaman) break;
		  $angka .= "<a href=".$file."np_page=$i  >$i&nbsp;</a> ";
		}
		$angka .= ($halaman+2<$jmlhalaman ? " ...  
		          <a href=".$file."np_page=$jmlhalaman  >$jmlhalaman</a> " : " ");
		echo "$angka";
		//link kehalaman berikutnya (Next)
		if($halaman < $jmlhalaman){
			$next=$halaman+1;
			echo "<a href=".$file."np_page=$next  >Next &gt; &nbsp;&nbsp;</a><a href=".$file."np_page=$jmlhalaman  >Last &gt;&gt; &nbsp;&nbsp;</a> ";
		}else{ echo "&nbsp;&nbsp;Next &gt; &nbsp;&nbsp;Last &gt;&gt;";
		}
		?>
	</div>
</div>