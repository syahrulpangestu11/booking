
<?php

	// try{
	// }catch(Exception $e){ logJS($e); }

	require_once('conf/connection_withpdo.php');
	Func::logJS('process'); Func::logJS($_POST); Func::logJS($_SESSION);
	
	$verification = true;
	$bookingtime = date("Y-m-d H:i:s");
	$arrivaldate = date('Y-m-d', strtotime($_SESSION['arrivaldate']));
	// $paymentmethodoid = (int)$_POST['paymentmethodoid'];
	// $waiting_method_all = array(2,4);//doku,banktransfer
	// // if(!in_array($paymentmethodoid,$waiting_method_all)){
	// $strictPost = array('hiddenRecaptcha', 'g-recaptcha-response', 'recaptcha_response_field');
	// $secret		= '6Le0uYYUAAAAADscAB77U0Kvxe0qAIcSvWVb_hBu';
	// $captcha	= $_POST['g-recaptcha-response'];
	// $response	= file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret."&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']);
	// $json 		= json_decode($response, true); 
	// $verification	= $json['success'];
	// // }

	$pagesuccess = ($_SESSION['booking_inquiry'] == 'y' ? 'inquiry' : '');
	if(isset($_POST['payment_method'])) $pagesuccess = $_POST['payment_method'];

	$bookingstatus = 1;
	switch($pagesuccess){
		case "inquiry"		: $bookingstatus = 1; break;
		case "banktransfer"	: $bookingstatus = 3; break;
		case "skippayment"	: $bookingstatus = 4; break;
		case "sof"			: $bookingstatus = 4; break;
		case "doku"			: $bookingstatus = 3; break;
		// case "creditcard"	: $bookingstatus = 3; break;
		// case "klikbca"		: $bookingstatus = 3; break;
		default				: $verification = false; break;
	}
	$bookingstatusoid = $bookingstatus;

	if($verification == false){

		echo "<script> alert('Please enter correct captcha!'); document.location.href='".$base_url."/book/payment'; </script>\n"; 
	}else{
		$_SESSION['_progress_booking'] = 1;

		include("save.booking-number.php");

		include('save.data-guest.php');

		include('save.data-hotel.php');
			
		include('save.update-allotment.php');
		// die();
		include('mailer/reservation_sending_template.php');
	}
?>
