<ul class="content-list" id="list-activities">
<?php
if($uri2 == "" or empty($uri2)){
	$s_activities = "select activitiesoid, cityname, activitiestype.note, activitiespict from activities inner join activitiestype using (acttypeoid) inner join published using (publishedoid) inner join city using (cityoid) inner join state using (stateoid) where activities.publishedoid = '1' and (activities.startdate<='$todaydate' and activities.enddate>='$todaydate') group by activitiesoid limit 4";
}else{
	$s_activities = "select activitiesoid, cityname, activitiestype.note,  activitiespict from activities inner join activitiestype using (acttypeoid) inner join published using (publishedoid) inner join city using (cityoid) inner join state using (stateoid) inner join activitiesrate using (activitiesoid) where activities.publishedoid = '1' and (statename like '%$keyword%' or cityname like '%$keyword%') and (activitiesrate.startbook<='$checkin' and activitiesrate.endbook>='$checkout') group by activitiesoid";
	//echo $s_activities;
}
$q_activities = mysqli_query($conn, $s_activities) or die(mysqli_error());
while($activities = mysqli_fetch_array($q_activities)){
	$activitiesoid = $activities['activitiesoid'];
	include('minrate.activities.php');
	if($rate > 0){
		include('function.activities-content.php');
		$city = $activities['cityname']; $activitiestype = $activities['note']; $activitiespict = $activities['activitiespict'];
?>
	<li>
		<div class="pict inline-block top thumb">
			<img src="<?=$activitiespict;?>">
		</div>
		<div class="inline-block top">
			<div>
				<span class="title"><?=$activitiesname;?></span>
			</div>
			<div><span class="italic">Area :</span> <?=$city;?> </div>
			<div><span class="italic">Type :</span> <?=$activitiestype;?></div>
			<div><?php echo $activitiesheadline; ?></div>
			<div class="price">
				rates from
				<h3 class="inline-block"><?php echo $currencycode; ?> <span class="maroon"><?php echo number_format($minrate);  ?></span></h3>
			</div>
			<div><a class="button" href="<?=$base_url;?>/activities/<?=$activitiesoid;?>">VIEW</a></div>
		</div>
	</li>
<?php
	}
}
?>
</ul>