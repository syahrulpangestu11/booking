<div id="homepage">
  	 
	<section id="slider" class="clear">
        <?php include("slideshow.php"); ?>
	</section>
    
    <section class="section-wrapper bg-light-blue">
        <div class="container">
            <h3 class="section-title"><b>Top Reviewed</b> Hotels</h3>
            <hr>
            
            <div class="card-wrapper" id="list-hotel">
                <div class="swiper-container card-container-v2">
                    <div class="swiper-wrapper">
                        <?php 
                        // include("component/homepage/list-choosen-hotel.php"); 
                        include("list.hotel.php");
                        ?>
                    </div>
                </div>
                <div id="card-v2-button-prev" class="card-button-prev hidden-sm hidden-xs"><i class="fa fa-2x fa-angle-left"></i></div>
                <div id="card-v2-button-next" class="card-button-next hidden-sm hidden-xs"><i class="fa fa-2x fa-angle-right"></i></div>
            </div>
            <script>
            $(function(){
                $('#list-hotel .thousand').hide();
                $('#list-hotel .thousand').after('K');
            });
            </script>
        </div>
    </section>

	<section class="section-wrapper bg-white">
        <div class="container">
            <?php include("list.destination.php"); ?>
        </div>
    </section>

    <section class="section-wrapper bg-light-green" id="why-booking-with-us">
        <div class="container">
            <h3 class="section-title" style="color: white"><b>Why booking</b> with us?</h3>

            <div class="row icon-wrapper">
                <div class="col-sm-4 col-md-4 text-center icon-wrapper">
                    <i class="fa fa-hand-holding-usd pull-left"></i>
                    <!-- <div class="icon-title">No Hidden Costs</div> -->
                    <div class="text" style="margin-left: 10px;color:white">
                        <h4 style="margin-top:20px;">BEST RATES GUARANTEED</h4>
                    </div>
                </div>
                <div class="col-sm-4 col-md-4 text-center icon-wrapper">
                    <i class="fa fa-credit-card pull-left"></i>
                    <!-- <div class="icon-title">No Fees</div> -->
                    <div class="text" style="margin-left: 10px;color:white">
                        <h4 style="margin-top:20px;">COMPLIMENTARY CONCIRGE</h4>                    
                    </div>
                </div>
                <div class="col-sm-4 col-md-4 text-center icon-wrapper">
                    <i class="fa fa-check-double pull-left"></i>
                    <!-- <div class="icon-title">Instant Confirmation</div> -->
                    <div class="text" style="margin-left: 10px;color:white">
                        <h4 style="margin-top:20px;">HAND-PICKED PROPERTIES</h4>                    
                    </div>
                </div>
<!--                 <div class="col-sm-3 col-md-3 text-center icon-wrapper">
                    <i class="fa fa-calendar-alt"></i>
                    <div class="icon-title">Flexible Booking</div>
                    <p>You can book up to a whole year in advance or right up until the moment of your stay.</p>
				</div>
				<div class="clearfix"></div>
 -->			</div>
            <?php /*/ ?>
                <!-- width="560" height="315" -->
                <div class="col-md-6 col-xs-12">
                    <iframe style="width:100%;height:300px;" src="https://www.youtube.com/embed/TGofuaFgRrY" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen=""></iframe>
                </div>
                <div class="col-md-6 col-xs-12">
                    <div class="row"> <!--class="col-md-3 col-xs-12 text-center"> -->
                        <div class="col-md-3 col-xs-12">
                            <div class="bukingnohiddencost whybukingoicon"></div>
                        </div>
                        <div class="col-md-9 col-xs-12">
                            <h3>No Hidden Costs</h3>
                            <p>The price you see on the website is the price you pay</p>
                        </div>
                    </div>
                    <div class="row"> <!-- class="col-md-3 col-xs-12 text-center">-->
                        <div class="col-md-3 col-xs-12">
                            <div class="bukingnofees whybukingoicon"></div>
                        </div>
                        <div class="col-md-9 col-xs-12">
                            <h3>No Fees</h3>
                            <p>We don't charge you a booking, cancellation or modification fee</p>
                        </div>
                    </div>
                    <div class="row"> <!-- class="col-md-3 col-xs-12 text-center">-->
                        <div class="col-md-3 col-xs-12">
                            <div class="bukinginstantconfirmation whybukingoicon"></div>
                        </div>
                        <div class="col-md-9 col-xs-12">
                            <h3>Instant Confirmation</h3>
                            <p>Instant booking confirmation whether booking online or via the telephone</p>
                        </div>
                    </div>
                    <div class="row"> <!-- class="col-md-3 col-xs-12 text-center">-->
                        <div class="col-md-3 col-xs-12">
                            <div class="bukingflexiblebooking whybukingoicon"></div>
                        </div>
                        <div class="col-md-9 col-xs-12">
                            <h3>Flexible Booking</h3>
                            <p>You can book up to a whole year in advance or right up until the moment of your stay</p>
                        </div>
                    </div>
                </div>
                <?php //*/ ?>
        </div>
    </section>

    <section class="section-wrapper bg-batik bg-white">
        <div class="container">
            <?php include("list.spoffer.php"); ?>
        </div>
    </section>

    <section class="section-wrapper bg-white">
        <div class="container">
            <?php include("list.experiences.php"); ?>
        </div>
    </section>

    <section class="section-wrapper bg-sunset bg-white">
        <div class="container">

        </div>
    </section>

</div>