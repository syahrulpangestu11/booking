<div style="position: relative">
<?php
$d_code = (isset($_REQUEST['d_code'])) ? $_REQUEST['d_code'] : "";
$d_img = (isset($_REQUEST['d_img'])) ? $_REQUEST['d_img'] : "";
$d_alt = (isset($_REQUEST['d_alt'])) ? $_REQUEST['d_alt'] : "";
$d_date = (isset($_REQUEST['d_date'])) ? $_REQUEST['d_date'] : "";
$d_flnum = (isset($_REQUEST['d_flnum'])) ? $_REQUEST['d_flnum'] : "";
$d_from = (isset($_REQUEST['d_from'])) ? $_REQUEST['d_from'] : "";
$d_to = (isset($_REQUEST['d_to'])) ? $_REQUEST['d_to'] : "";
$d_dtime = (isset($_REQUEST['d_dtime'])) ? $_REQUEST['d_dtime'] : "";
$d_atime = (isset($_REQUEST['d_atime'])) ? $_REQUEST['d_atime'] : "";
$d_dur = (isset($_REQUEST['d_dur'])) ? $_REQUEST['d_dur'] : "";
$d_cadlt = (isset($_REQUEST['d_cadlt'])) ? $_REQUEST['d_cadlt'] : "";
$d_cchld = (isset($_REQUEST['d_cchld'])) ? $_REQUEST['d_cchld'] : "";
$d_cinft = (isset($_REQUEST['d_cinft'])) ? $_REQUEST['d_cinft'] : "";
$d_padlt = (isset($_REQUEST['d_padlt'])) ? $_REQUEST['d_padlt'] : "";
$d_pchld = (isset($_REQUEST['d_pchld'])) ? $_REQUEST['d_pchld'] : "";
$d_pinft = (isset($_REQUEST['d_pinft'])) ? $_REQUEST['d_pinft'] : "";
$d_via = (isset($_REQUEST['d_via'])) ? $_REQUEST['d_via'] : "";
$d_apt = (isset($_REQUEST['d_apt'])) ? $_REQUEST['d_apt'] : "";

$r_code = (isset($_REQUEST['r_code'])) ? $_REQUEST['r_code'] : "";
$r_img = (isset($_REQUEST['r_img'])) ? $_REQUEST['r_img'] : "";
$r_alt = (isset($_REQUEST['r_alt'])) ? $_REQUEST['r_alt'] : "";
$r_date = (isset($_REQUEST['r_date'])) ? $_REQUEST['r_date'] : "";
$r_flnum = (isset($_REQUEST['r_flnum'])) ? $_REQUEST['r_flnum'] : "";
$r_from = (isset($_REQUEST['r_from'])) ? $_REQUEST['r_from'] : "";
$r_to = (isset($_REQUEST['r_to'])) ? $_REQUEST['r_to'] : "";
$r_dtime = (isset($_REQUEST['r_dtime'])) ? $_REQUEST['r_dtime'] : "";
$r_atime = (isset($_REQUEST['r_atime'])) ? $_REQUEST['r_atime'] : "";
$r_dur = (isset($_REQUEST['r_dur'])) ? $_REQUEST['r_dur'] : "";
$r_cadlt = (isset($_REQUEST['r_cadlt'])) ? $_REQUEST['r_cadlt'] : "";
$r_cchld = (isset($_REQUEST['r_cchld'])) ? $_REQUEST['r_cchld'] : "";
$r_cinft = (isset($_REQUEST['r_cinft'])) ? $_REQUEST['r_cinft'] : "";
$r_padlt = (isset($_REQUEST['r_padlt'])) ? $_REQUEST['r_padlt'] : "";
$r_pchld = (isset($_REQUEST['r_pchld'])) ? $_REQUEST['r_pchld'] : "";
$r_pinft = (isset($_REQUEST['r_pinft'])) ? $_REQUEST['r_pinft'] : "";
$r_via = (isset($_REQUEST['r_via'])) ? $_REQUEST['r_via'] : "";
$r_apt = (isset($_REQUEST['r_apt'])) ? $_REQUEST['r_apt'] : "";
?>
<style>
.hide{display:none;}
#blocking{position:absolute; width:100%; height:2000px; background-color:rgba(255,255,255,0.8); text-align:center; padding-top:50px; z-index:1000;}
#blocking .blocking-box{background-color:#fecb20; display:inline-block; padding:30px; color:#ffffff; font-size:16px;}
.list-resultflight{margin:10px 0; width:100%;}
#flight_choosed{border: 3px solid #004E75;}
#flight_choosed ul{margin:0; padding:0; list-style:none; background-color:rgba(0, 102, 153, 0.16);}
#flight_choosed li{display:inline-block; padding:10px; text-align: center; vertical-align:middle; min-width:15%;}
#flight_choosed li:nth-child(3){min-width:20%;}
#choose_depart > div.small-desc, #choose_return > div.small-desc{padding-top:8px; padding-left:8px; padding-right:8px;}
#choose_total{padding-left:20px;}
#choose_total > span:first-child{padding-left:50%;} 

#email {width: 150px;}
#address, #note {height: 60px; width: 180px;}
#city {margin-bottom: 5px;}

#guest-form #guest-detail > .inline-block > div {margin-bottom:5px;}
</style>

<div id="blocking" class="hide">
	<div class="blocking-box">
	<span>Collecting data,<br>please wait...</span><br>
	<img src="<?=$base_url?>/images/loading2.gif" />
	</div>
</div>

<form id="guest-form" action="#" method="post"> <!-- <?=$base_url;?>/book/payment -->
	<div class="white-box border-box" id="flight_choosed">
		<div class="clear">
			<?php if($d_code != ""){?>
			<div id="choose_depart">
				<div code="<?=$d_code?>"><img src="<?=urldecode($d_img)?>" alt="<?=$d_alt?>"> &nbsp; &bull; <strong>Depart Flight</strong> (<?=date("D, d F Y",strtotime($d_date))?>)</div>
				<div>
					<ul>
						<li><?=$d_flnum?></li>
						<li><?=$d_from?> to <?=$d_to?></li>
						<li><?=$d_dtime?> - <?=$d_dtime?><br><span><?=$d_dur?></span></li>
						<li><strong dt-a="<?=$d_padlt?>" dt-c="<?=$d_pchld?>" dt-i="<?=$d_pinft?>">
							<?php 
							$d_x = str_replace(",", "", $d_padlt);
							$d_y = str_replace(",", "", $d_pchld);
							$d_z = str_replace(",", "", $d_pinft);
							$d_total = $d_x * $d_cadlt + $d_y * $d_cchld + $d_z * $d_cinft;
							echo number_format($d_total,2,".",",");
							?>
						</strong></li>
					</ul>
				</div>
				<div class="small-desc"><?=$d_apt?></div>
				<div class="small-desc">
					<?php
					echo $d_cadlt." Adult x ".$d_padlt;
					if($d_pchld != "0.00")
						echo " + ".$d_cchld." Child x ".$d_pchld;
					if($d_pinft != "0.00")
						echo " + ".$d_cinft." Infant x ".$d_pinft;
					?>
				</div>
				<div class="small-desc"><?=$d_via?></div>
			</div>
			<div class="filter-wrapper clear border-bottom"></div>
			<?php }?>
			<?php if($r_code != ""){?>
			<div id="choose_return">
				<div code="<?=$r_code?>"><img src="<?=urldecode($r_img)?>" alt="<?=$r_alt?>"> &nbsp; &bull; <strong>Return Flight</strong> (<?=date("D, d F Y",strtotime($r_date))?>)</div>
				<div>
					<ul>
						<li><?=$r_flnum?></li>
						<li><?=$r_from?> to <?=$r_to?></li>
						<li><?=$r_dtime?> - <?=$r_dtime?><br><span><?=$r_dur?></span></li>
						<li><strong dt-a="<?=$r_padlt?>" dt-c="<?=$r_pchld?>" dt-i="<?=$r_pinft?>">
							<?php 
							$r_x = str_replace(",", "", $r_padlt);
							$r_y = str_replace(",", "", $r_pchld);
							$r_z = str_replace(",", "", $r_pinft);
							$r_total = $r_x * $r_cadlt + $r_y * $r_cchld + $r_z * $r_cinft;
							echo number_format($r_total,2,".",",");
							?>
						</strong></li>
					</ul>
				</div>
				<div class="small-desc"><?=$r_apt?></div>
				<div class="small-desc">
					<?php
					echo $r_cadlt." Adult x ".$r_padlt;
					if($r_pchld != "0.00")
						echo " + ".$r_cchld." Child x ".$r_pchld;
					if($r_pinft != "0.00")
						echo " + ".$r_cinft." Infant x ".$r_pinft;
					?>
				</div>
				<div class="small-desc"><?=$r_via?></div>
			</div>
			<div class="filter-wrapper clear border-bottom"></div>
			<?php }?>
			<h2 id="choose_total" class="choose-total-bg">
				<span class="grey inline-block">Total : </span> &nbsp; &nbsp; 
				<span class="blue">
					<?php
					$total = $d_total + $r_total;
					echo number_format($total,2,".",",");
					?>
				</span>
			</h2>
		</div> <!-- end of .clear -->
	</div>
	
	<br />
	
    <!-- <h1><span class="grey">Guest</span> <span class="blue">Detail</span></h1> -->
	<div id="guest-detail" class="white-box border-box">
		<div class="inline-block border-right">
			<h2><span class="grey">Guest</span> <span class="blue">Detail</span></h2>
			
		    <div>
			    <b>Name :</b><br>
			    <select name="title" id="title">
			    	<?php
			    		$s_title = "select * from title "; $q_title = mysqli_query($conn, $s_title) or die(mysqli_error());
						while($title = mysqli_fetch_array($q_title)){
			    	?>
			    		<option value="<?=$title['titleoid'];?>"><?=$title['name'];?></option>
			    	<?php
						}
					?>
			    </select>
			    <input type="text" name="firstname" id="firstname" placeholder="First Name" >
			    <input type="text" name="lastname" id="lastname" placeholder="Last Name" ><br>
			</div>
			
		    <div>
			    <b>Contact Information :</b><br>
			    <input type="text" name="email" id="email" placeholder="Email" >
			    <input type="text" name="mobilephone" id="mobilephone" placeholder="Mobile Phone" ><span id="errmsg"></span>
			    <input type="text" name="phone" id="phone" placeholder="Phone Number"><br>
			    <i class="small-desc">Phone number format must begin with your country code. Example : 6212345678</i><br>
			</div>
			
			<div>
			    <b>Address :</b><br>
			    <div class="inline-block top">
			    	<textarea name="address" id="address" placeholder="Address" ></textarea>
			    </div>
			    <div class="inline-block top">
			    	<div>
				    	<input type="text" name="city" id="city" placeholder="City" >
				    	<input type="text" name="state" id="state" placeholder="State" >
				    </div>
				    <div>
				    	<input type="text" name="zipcode" id="zipcode" placeholder="ZIP Code"><br>
				    </div>
				</div>
			</div>
			
			<div>
				<b>Country :</b><br>
			    <select name="country" id="country" >
			    	<option value="" selected>--- please select your country ---</option>
			        <?php 
			        $s_country="select countryoid, countryname from country order by countryname"; $q_country=mysqli_query($conn, $s_country) or die ("SQL Error : ".$s_country);
			        while($country=mysqli_fetch_array($q_country)){
			        	if(empty($_SESSION['nationallity'])){
			        		if($country['countryname']=="Indonesia"){ $selected=" selected='selected' "; }else{ $selected=""; }
			        	}else{
			        		if($country['countryoid']==$_SESSION['nationallity']){ $selected=" selected='selected' "; }else{ $selected=""; }
			        	}
			        ?>
			        <option value="<?php echo $country['countryoid']; ?>" <?php echo $selected; ?>><?php echo $country['countryname']; ?></option>
			        <?php
			        }
			        ?>
			    </select><br /><br />
			</div>
		</div> <!-- end of .inline-block.border-right -->
		
		
	    <div class="inline-block top">
       	<?php for($i = 1; $i <= $d_cadlt; $i++){?>
        
            <h2><span class="grey">Adult</span> <span class="blue">Passenger <?=$i?></span></h2>
	    	
		    <div>
			    <b>Name :</b><br>
			    <select name="title" id="title">
			    	<?php
			    		$s_title = "select * from title "; $q_title = mysqli_query($conn, $s_title) or die(mysqli_error());
						while($title = mysqli_fetch_array($q_title)){
			    	?>
			    		<option value="<?=$title['titleoid'];?>"><?=$title['name'];?></option>
			    	<?php
						}
					?>
			    </select>
			    <input type="text" name="firstname" id="firstname" placeholder="First Name" >
			    <input type="text" name="lastname" id="lastname" placeholder="Last Name" ><br>
			</div>
			
			<div>
			    <b>Identity :</b><br>
			    <input type="text" name="idcard" id="idcard" placeholder="ID Card Number (KTP/SIM/Student Card)" size="35">
			    <input type="text" name="birthday" id="birthday" class="datepicker" placeholder="Birth Date" readonly="readonly" ><br>
			</div>
			
			<div>
				<b>Nationality :</b><br>
			    <select name="nationality" id="nationality" >
			    	<option value="" selected>--- please select your country ---</option>
			        <?php 
			        $s_country="select countryoid, countryname from country order by countryname"; $q_country=mysqli_query($conn, $s_country) or die ("SQL Error : ".$s_country);
			        while($country=mysqli_fetch_array($q_country)){
			        	if(empty($_SESSION['nationallity'])){
			        		if($country['countryname']=="Indonesia"){ $selected=" selected='selected' "; }else{ $selected=""; }
			        	}else{
			        		if($country['countryoid']==$_SESSION['nationallity']){ $selected=" selected='selected' "; }else{ $selected=""; }
			        	}
			        ?>
			        <option value="<?php echo $country['countryoid']; ?>" <?php echo $selected; ?>><?php echo $country['countryname']; ?></option>
			        <?php
			        }
			        ?>
			    </select>
			</div>
			<div class="border-bottom"></div>
		<?php }?>
		<?php for($i = 1; $i <= $d_cchld; $i++){?>
        
            <h2><span class="grey">Child</span> <span class="blue">Passenger <?=$i?></span></h2>
	    	
		    <div>
			    <b>Name :</b><br>
			    <select name="title" id="title">
			    	<?php
			    		$s_title = "select * from title "; $q_title = mysqli_query($conn, $s_title) or die(mysqli_error());
						while($title = mysqli_fetch_array($q_title)){
			    	?>
			    		<option value="<?=$title['titleoid'];?>"><?=$title['name'];?></option>
			    	<?php
						}
					?>
			    </select>
			    <input type="text" name="firstname" id="firstname" placeholder="First Name" >
			    <input type="text" name="lastname" id="lastname" placeholder="Last Name" ><br>
			</div>
			
			<div>
			    <b>Identity :</b><br>
			    <input type="text" name="birthday" id="birthday" class="datepicker" placeholder="Birth Date" readonly="readonly" ><br>
			</div>
			
			<div>
				<b>Nationality :</b><br>
			    <select name="nationality" id="nationality" >
			    	<option value="" selected>--- please select your country ---</option>
			        <?php 
			        $s_country="select countryoid, countryname from country order by countryname"; $q_country=mysqli_query($conn, $s_country) or die ("SQL Error : ".$s_country);
			        while($country=mysqli_fetch_array($q_country)){
			        	if(empty($_SESSION['nationallity'])){
			        		if($country['countryname']=="Indonesia"){ $selected=" selected='selected' "; }else{ $selected=""; }
			        	}else{
			        		if($country['countryoid']==$_SESSION['nationallity']){ $selected=" selected='selected' "; }else{ $selected=""; }
			        	}
			        ?>
			        <option value="<?php echo $country['countryoid']; ?>" <?php echo $selected; ?>><?php echo $country['countryname']; ?></option>
			        <?php
			        }
			        ?>
			    </select>
			</div>
			<div class="border-bottom"></div>
		<?php }?>
		<?php for($i = 1; $i <= $d_cinft; $i++){?>
        
            <h2><span class="grey">Infant</span> <span class="blue">Passenger <?=$i?></span></h2>
	    	
		    <div>
			    <b>Name :</b><br>
			    <select name="title" id="title">
			    	<?php
			    		$s_title = "select * from title "; $q_title = mysqli_query($conn, $s_title) or die(mysqli_error());
						while($title = mysqli_fetch_array($q_title)){
			    	?>
			    		<option value="<?=$title['titleoid'];?>"><?=$title['name'];?></option>
			    	<?php
						}
					?>
			    </select>
			    <input type="text" name="firstname" id="firstname" placeholder="First Name" >
			    <input type="text" name="lastname" id="lastname" placeholder="Last Name" ><br>
			</div>
			
			<div>
			    <b>Identity :</b><br>
			    <input type="text" name="birthday" id="birthday" class="datepicker" placeholder="Birth Date" readonly="readonly" ><br>
			</div>
			
			<div>
				<b>Nationality :</b><br>
			    <select name="nationality" id="nationality" >
			    	<option value="" selected>--- please select your country ---</option>
			        <?php 
			        $s_country="select countryoid, countryname from country order by countryname"; $q_country=mysqli_query($conn, $s_country) or die ("SQL Error : ".$s_country);
			        while($country=mysqli_fetch_array($q_country)){
			        	if(empty($_SESSION['nationallity'])){
			        		if($country['countryname']=="Indonesia"){ $selected=" selected='selected' "; }else{ $selected=""; }
			        	}else{
			        		if($country['countryoid']==$_SESSION['nationallity']){ $selected=" selected='selected' "; }else{ $selected=""; }
			        	}
			        ?>
			        <option value="<?php echo $country['countryoid']; ?>" <?php echo $selected; ?>><?php echo $country['countryname']; ?></option>
			        <?php
			        }
			        ?>
			    </select>
			</div>
			<div class="border-bottom"></div>
		<?php }?>
			<br>
            <input type="submit" name="submit" value="CONTINUE &gt;&gt;" class="general-submit fl_left" />
		</div> <!-- end of .inline-block -->
	</div> <!-- end of .white-box -->
</form>
</div>

<script src="<?php echo"$base_url"; ?>/scripts/datepicker/ui/jquery.ui.datepicker.original.js"></script>
<script>
	$(function() {
		$( ".datepicker" ).datepicker({
	      defaultDate: "+0",
	      dateFormat: "yy-mm-dd",
	      changeMonth: true,
	      numberOfMonths: 1
	    });
	});
</script>