<?php
function checkedCB( $getUrl , $cbValue ) {
	if( !empty($getUrl) ){
		$selected_get = $getUrl;
		$exploded_get = explode( "," , $selected_get );
		if(in_array($cbValue, $exploded_get)){
			$checked = " checked ";
		}else{
			$checked = "";
		}
	}else{
		$checked = "";
	}	
	return $checked;
}




/**
* Merge $vars with the current query string, overwriting any
* duplicate variables. Uses arg_seperator.output ini setting to
* determine what to use as an arg seperator.
*
* @param array $vars Variables to add into or replace in the query
* string. Just like $_GET (its merged with $_GET, actually).
*
* @return string A new query string
*
* Usage:
*
* <a href="<?php echo $_SERVER["PHP_SELF"] . "?" .
* querystring_merge("offset" => $offset + $limit); ?>">Next</a>
* 
* querystring_merge(array("offset" => "2,3,4"))
* 
*/
function querystring_merge ( $vars = array() )
{
	$sep = ini_get('arg_separator.output');
	$qs = "";
	foreach ( array_merge($_GET, $vars) as $k => $v ) {
		//$qs .= "$k=" . urlencode($v) . $sep;
		$qs .= "$k=" . str_replace(" ", "+", $v) . $sep;
	}
	
	return "?".substr($qs, 0, -1); /* trim off trailing $sep */
}





/**
* Remove $remove_vars from query string.
*
* @param mixed $remove_vars string of variable to remove, or array of
* variables
*
* @return string The querystring with the variables from $remove_vars
* removed.
*
* Usage:
* Suppose $_SERVER['QUERY_STRING'] looks like this: ?a=1&b=2&c=3:
*
* echo querystring_remove("a"); -> ?b=2&c=3 (a is removed)
* echo querystring_remove(array("a", "c")); -> ?b=2 (a and c are removed)
*/
function querystring_remove ( $remove_vars )
{
	if ( !is_array($remove_vars) ) {
		$remove_vars = array($remove_vars);
	}
	
	$sep = ini_get('arg_separator.output');
	$qs = "";
	foreach ( $_GET as $k => $v ) {
		if ( !in_array($k, $remove_vars) ) {
			//$qs .= "$k=" . urlencode($v) . $sep;
			$qs .= "$k=" . str_replace(" ", "+", $v) . $sep;
		}
	}
	
	return "?".substr($qs, 0, -1); /* trim off trailing $sep */
}
?>