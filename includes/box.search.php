<?php /*
<nav>
	<ul class='tabs'>
	    <li><a href='#tabs-hotel'><img src="<?=$base_url;?>/images/tabs/hotel.png"><span>hotel</span></a></li>
	    <li><a href='#tabs-flight'><img src="<?=$base_url;?>/images/tabs/flight.png"><span>flight</span></a></li>
	    <li><a href='#tabs-tour'><img src="<?=$base_url;?>/images/tabs/tour.png"><span>tour</span></a></li>
	    <li><a href='#tabs-package'><img src="<?=$base_url;?>/images/tabs/tour.png"><span>package</span></a></li>
	</ul>
</nav>

<div class="tabs-content" id='tabs-hotel'>  
	<?php include("form.search.hotel.php");?>
</div>

<div class="tabs-content" id='tabs-flight'>
    <?php include("form.search.flight.php"); ?>
</div>

<div class="tabs-content" id='tabs-tour'>  
	<?php include("form.search.tour.php");?>
</div>

<div class="tabs-content" id='tabs-package'>  
	<?php include("form.search.tour.php");?>
</div>

*/ ?>

<style>
	#wrapper-hotel-flight {
		margin: 0 0 0 20px; padding: 5px;
		background-color: #FFFDC2/*#DFF4FF*/;
		border-radius: 10px;
	}
</style>


<nav>
	<ul class='tabs'>
	    <li><a href='#tabs-your-travel'><img src="<?=$base_url;?>/images/tabs/hotel.png"><span>Pesan Perjalanan Anda</span></a></li>
	    <li><a href='#tabs-package'><img src="<?=$base_url;?>/images/tabs/tour.png"><span>Package</span></a></li>
	</ul>
</nav>

<div class="tabs-content" id='tabs-your-travel'>  

	<div id="search-type" class="border-bottom">
		<div class="inline-block top">
			<div>
				<input type="radio" name="search_type" value="hotel" id="radio-hotel" target="radio-content-hotel" checked>
				<label for="radio-hotel" class="title-blue">Hotel</label>
			</div>
			<div>
				<input type="radio" name="search_type" value="flight" id="radio-flight" target="radio-content-flight">
				<label for="radio-flight" class="title-blue">Flight</label>
			</div>
			<div>
				<input type="radio" name="search_type" value="tour" id="radio-tour" target="radio-content-opt-tour">
				<label for="radio-tour" class="title-blue">Optional Tour</label>
			</div>
		</div>
		<div class="inline-block top" id="wrapper-hotel-flight">
			<input type="radio" name="search_type" value="flight+hotel" id="radio-hotel-flight" target="radio-content-hotel-flight">
			<label for="radio-hotel-flight" class="title-blues">Flight + Hotel <img src="<?=$base_url;?>/images/tabs/hotel+flight.png"></label>
		</div>
	</div>
	
	<div id="search-type-content">
		<div class="radio-content" id='radio-content-hotel'>
			<h3 class="default">Hotel</h3>
			<?php include("form.search.hotel.php");?>
		</div>
		
		<div class="radio-content" id='radio-content-flight'>
			<h3 class="default">Flight</h3>
		    <?php include("form.search.flight.php"); ?>
		</div>
		
		<div class="radio-content" id='radio-content-opt-tour'>
			<h3 class="default">Optional Tour</h3>  
			<?php include("form.search.activities.php");?>
		</div>
		
		<div class="radio-content" id='radio-content-hotel-flight'>
			<h3 class="default">Hotel + Flight</h3>
			<?php include("form.search.hotel-flight.php"); ?>
		</div>
	</div>
</div>

<div class="tabs-content" id='tabs-package'>  
	<?php include("form.search.tour.php");?>
</div>

<script>
	$(document).ready(function(){
		$("#search-type input[type=radio]").each(function(){
			var elem = $(this);
			$('.radio-content').hide();
			$("#radio-content-hotel").show();
			elem.click(function(){
		        var radioTarget = $(this).attr('target');
		        $('.radio-content').hide();
		    	$('#'+radioTarget).show();   
			}); 
		});
		
		$("#radio-content-opt-tour form .search-submit").click(function(){
			//return false;
		});
	});
</script>