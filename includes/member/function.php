<script type="text/javascript">
$(function(){

	$('.nyroModal').nyroModal();
	
	function loadCardList(){
		$.ajax({
			url		: '<?php echo $base_url; ?>/includes/member/task/list-card.php',
			type	: 'post',
			data	: { moid : <?php echo $_SESSION['rove1meid']; ?> },
			success	: function(response) {
				$('table#list-card tbody').html(response);
			}
		});
	}

	$('body').on('click', 'button[name="submit-card"]', function (e){
		$.nmTop().close();
		form_card = $('form[name="form-add-card"]');
		$.ajax({
			url		: '<?php echo $base_url; ?>/includes/member/task/save-new-card.php',
			type	: 'post',
			data	: form_card.serialize(),
			success	: function(response) {
				if(response == "success"){
					loadCardList();
					form_card[0].reset();
				}
			}
		});
	});
	
	if($('table#list-card').size() > 0){
		loadCardList();
	}
	
	$('body').on('click', 'a.edit-card', function (e){
		
		ccn		= $(this).parent().parent().children('td').eq(0).html();
		expcc	= $(this).parent().parent().children('td').eq(2).html();
		
		$('#form-edit-card').find('h2#cc-number').html(ccn);
		expcc = expcc.split('/')
		$('#form-edit-card').find('select[name=month]').val(expcc[0]);
		$('#form-edit-card').find('select[name=year]').val(expcc[1]);
		
		$('#editcc_generate').nmCall();;
	});
	
	$('body').on('click', 'button[name="submit-card"]', function (e){
		$.nmTop().close();
		form_card = $('form[name="form-add-card"]');
		$.ajax({
			url		: '<?php echo $base_url; ?>/includes/member/task/save-new-card.php',
			type	: 'post',
			data	: form_card.serialize(),
			success	: function(response) {
				if(response == "success"){
					loadCardList();
					form_card[0].reset();
				}
			}
		});
	});
});	
</script>