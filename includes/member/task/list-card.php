<?php
	function maskingCard($cardnumber){
		$number_character = strlen($cardnumber); 
		$mask_card = str_repeat('&bull;', 4).substr($cardnumber, -3);
		return $mask_card;
	}

	if(!empty($_POST['moid'])){
		include('../../../conf/connection.php');
		
		$syntax_cc = "select cc.*, cctype.cardname from customercard cc inner join creditcard cctype using (cardoid) where custoid = '".$_POST['moid']."'";
		$query_cc = mysqli_query($conn, $syntax_cc) or die(mysqli_error());
		while($data = mysqli_fetch_assoc($query_cc)){
?>
		<tr>
			<td><i class="fa fa-credit-card-alt"></i> <?php echo maskingCard($data['cardnumber']); ?></td>
			<td><?php echo $data['cardholder']; ?></td>
			<td><?php echo $data['expmonth']; ?>/<?php echo $data['expyear']; ?></td>
			<td><a href="#" class="edit-card text-purple">[<i class="fa fa-search"></i> edit]</a> <a href="#" class="text-red">[<i class="fa fa-close"></i> delete]</a></td>
		</tr>
<?php	
		}
	}else{
		echo "not valid";
	}
?>