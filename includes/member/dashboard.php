<div class="white-box border-box">
    <h2><span class="grey"><i class="fa fa-user"></i></span> <span class="blue">My Profile</span></h2>
    <div class="form">
        <div class="form-group">
            <label>Title</label>
            <span>
            <select name="title">
                <?php
                    $s_title = "select * from title "; 
                    $q_title = mysqli_query($conn, $s_title) or die(mysqli_error());
                    while($title = mysqli_fetch_array($q_title)){
                ?>
                <option value="<?=$title['titleoid'];?>"><?=$title['name'];?></option>
                <?php
                    }
                ?>
            </select>
            </span>
        </div>
        <div class="form-group">
            <label>First Name</label>
            <span>
            <input type="text" class="md-6" name="firstname" placeholder="Your first name" />
            </span>
        </div>
        <div class="form-group">
            <label>Last Name</label>
            <span>
            <input type="text" class="md-6" name="lastname" placeholder="Your last name" />
            </span>
        </div>
        <div class="form-group">
            <label><i class="fa fa-envelope"></i> E-mail</label>
            <span>
            <input type="email" class="md-3" name="email" placeholder="Your e-mail address" />
            </span>
        </div>
        <div class="form-group">
            <label>Mobile Number</label>
            <span>
            <input type="text" class="md-3" name="mobile" placeholder="Your mobile phone number" />
            </span>
        </div>
        <div class="form-group">
            <label>Country</label>
            <span>
            <select name="country">
            <?php 
                $s_country="select countryoid, countryname from country order by countryname"; $q_country=mysqli_query($conn, $s_country) or die ("SQL Error : ".$s_country);
                while($country=mysqli_fetch_array($q_country)){
                    if(empty($_SESSION['nationallity'])){
                        if($country['countryname']=="Indonesia"){ $selected=" selected='selected' "; }else{ $selected=""; }
                    }else{
                        if($country['countryoid']==$_SESSION['nationallity']){ $selected=" selected='selected' "; }else{ $selected=""; }
                    }
                ?>
                <option value="<?php echo $country['countryoid']; ?>" <?php echo $selected; ?>><?php echo $country['countryname']; ?></option>
                <?php
                }
                ?>
            </select>
            </span>
        </div>
    </div>
    <div class="form">
        <h3 class="sticky-title turqoise"><span><i class="fa fa-lock"></i> Change Your Password</span></h3>
        <div class="form-group">
            <label>Current Password</label>
            <span>
            <input type="password" class="md-3" name="oldpassword" placeholder="Current password" />
            </span>
        </div>
        <div class="form-group">
            <label>New Password</label>
            <span>
            <input type="password" class="md-3" name="oldpassword" placeholder="Your new password" />
            </span>
        </div>
    </div>
</div>
