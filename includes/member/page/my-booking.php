<div class="white-box border-box">
    <h2><span class="grey"><i class="fa fa-shopping-bag"></i></span> <span class="blue">My Booking</span></h2>
    
    <table class="table table-striped">
    	<thead>
            <tr>
                <th>Booking Number</th>
                <th>Booking Date</th>
                <th>Total</th>
                <th>Status</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
        <?php
			$syntax_booking = "select b.bookingnumber, b.bookingoid, b.bookingtime, b.grandtotal, bs.note as status from booking b 
				inner join bookingstatus bs using (bookingstatusoid) 
				inner join customer c using (custoid) 
				left join bookingpayment using (bookingoid) 
				where c.custoid = '".$_SESSION['rove1meid']."' group by b.bookingoid";
			$query_booking = mysqli_query($conn, $syntax_booking) or die(mysqli_error());
			while($data = mysqli_fetch_assoc($query_booking)){
		?>
        	<tr>
            	<td><?php echo $data['bookingnumber']; ?></td>
                <td class="text-center"><?php echo date("d/M/Y", strtotime($data['bookingtime'])); ?></td>
                <td><?php echo number_format($data['grandtotal']); ?></td>
                <td><?php echo $data['status']; ?></td>
                <td><a href="<?php echo $base_url; ?>/member/booking/<?php echo $data['bookingnumber']; ?>" class="text-purple">[<i class="fa fa-search"></i> view detail]</a> <a href="#" class="text-red">[<i class="fa fa-close"></i> cancel]</a></td>
            </tr>
        <?php	
			}
		?>
        </tbody>
    </table>
</div>