<?php
function ccMasking($number, $maskingCharacter = 'x') {
    return substr($number, 0, 4) . str_repeat($maskingCharacter, strlen($number) - 8) . substr($number, -4);
}
?>
<div class="white-box border-box">
    <h2><span class="grey"><i class="fa fa-shopping-bag"></i></span> <span class="blue">My Booking</span></h2>
    
    <table id="list-card" class="table table-striped">
    	<thead>
            <tr>
                <th>Credit Card Number</th>
                <th>Cardholder&prime;s Name</th>
                <th>Expiration Date</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    <div class="form-group">
        <span>
            <a href="#form-add-card" class="nyroModal"><button type="submit" class="purple" name="add-card">Add New Card</button></a>
            <a href="#form-edit-card" id="editcc_generate"  style="disply:none;" class="nyroModal"></a>
        </span>
    </div>
    
    <div id="form-add-card" style="display:none">
    	<?php include("credit-card-add.php"); ?>
    </div>
    <div id="form-edit-card" style="display:none">
    	<?php include("credit-card-edit.php"); ?>
    </div>
</div>