<?php	if(!isset($_SESSION['rove1name']) and !isset($_SESSION['rove1email']) and !isset($_SESSION['rove1meid'])){	?>
		<script type="text/javascript">document.location.href="<?php echo $base_url; ?>";</script>
<?php	} 
	$syntax_member	= "select c.*, t.name as title from customer c inner join title t using (titleoid) where custoid = '".$_SESSION['rove1meid']."' and member = 'yes'";
	$query_member	= mysqli_query($conn, $syntax_member);
	$member = mysqli_fetch_assoc($query_member);
?>

<link rel="stylesheet" type="text/css" href="<?php echo $base_url; ?>/styles/style-member-area.css">
<link rel="stylesheet" href="<?php echo $base_url; ?>/scripts/nyromodal/styles/nyroModal.css" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo $base_url; ?>/scripts/nyromodal/jquery.nyroModal.custom.js"></script>
<?php 	include('function.php'); ?>
<div id="member-area">
	<aside id="side-menu">
        <ul class="list-menu block">
            <li>
                <a href="#">
                    <i class="fa fa-dashboard"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            <li <?php if($uri3 == "my-profile"){ echo "class='active'";} ?>>
                <a href="<?php echo $base_url; ?>/member/my-profile">
                    <i class="fa fa-smile-o"></i>
                    <span>My Profile</span>
                </a>
            </li>
            <li <?php if($uri3 == "my-booking"){ echo "class='active'";} ?>>
                <a href="<?php echo $base_url; ?>/member/my-booking">
                    <i class="fa fa-shopping-bag"></i>
                    <span>My Booking</span>
                </a>
            </li>
            <li <?php if($uri3 == "credit-card"){ echo "class='active'";} ?>>
                <a href="<?php echo $base_url; ?>/member/credit-card">
                    <i class="fa fa-credit-card"></i>
                    <span>Credit Card</span>
                </a>
            </li>
        </ul>
    </aside>
    
    <section id="content">
		<?php
		switch($uri3){
			case "my-profile"		: include("page/my-profile.php"); break;
			case "save-my-profile"	: include("task/save-my-profile.php"); break;
			case "my-booking"		: include("page/my-booking.php"); break;
			case "booking"			: include("page/detail-booking.php"); break;
			case "credit-card"		: include("page/credit-card.php"); break;
			default					: include("page/my-profile.php"); break;
		}
		?>
    </section>
</div>